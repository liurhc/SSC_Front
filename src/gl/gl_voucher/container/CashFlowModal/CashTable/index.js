import React, { Component } from "react";
import { InputItem } from '../../../../public/components/FormItems';
// import multiSelect from "tinper-bee/lib/multiSelect.js";
import {createPage, high,base,ajax ,toast} from 'nc-lightapp-front';
const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
  NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
  NCCheckbox:Checkbox,NCNumber,
  NCAutoComplete:AutoComplete,NCDropdown:Dropdown,NCPanel:Panel,NCForm:Form,NCButtonGroup:ButtonGroup
  } = base;

import CurrtypeGridRef from '../../../../../uapbd/refer/pubinfo/CurrtypeGridRef';
import CashflowTreeRef from '../../../../../uapbd/refer/fiacc/CashflowTreeRef';
import InnerCustSupplierForCustGridTreeRef from "../../../../../uapbd/refer/supplier/InnerCustSupplierForCustGridTreeRef";
  const { Refer } = high;
import './index.css';
const Option = Select.Option; 
const OptGroup = Select.OptGroup;
//let ComplexTable = multiSelect(Table);

export default class CashTable extends Component {
  static defaultProps = {
		confirmText: this.state.json['20021005card-000003'],/* 国际化处理： 确定*/
		cancelText: this.state.json['20021005card-000004'],/* 国际化处理： 取消*/
		show: false,
		title: '',
		content: '',
		closeButton: false,
		icon: 'icon-tishianniuchenggong',
		isButtonWhite: true,
		isButtonShow: true,
		multiSelect: {
			type: "checkbox",
			param: "key"
		  }
	};
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      selectedArr:[],
      ratequery:[],
      checkedAll:false,
			checkedArray: [
			
			],
    };
    this.columns = [
      {
        title: this.state.json['20021005card-000021'],/* 国际化处理： 分录号*/
        dataIndex: "detailindex",
        key: "detailindex",
        width:'5%',
        render: (text, record, index) => (
          <InputItem
              type="customer"
              name="attachment"
              onChange={(v)=>{
                if(v){
                  this.getRowData(v,index)
                }
              }}
              defaultValue={this.state.dataSource[index].detailindex?this.state.dataSource[index].detailindex.display:""}
					/>
        )
      },
      {
        title: this.state.json['20021005card-000022'],/* 国际化处理： 会计科目*/
        dataIndex: "pk_accasoa",
        key: "pk_accasoa",
        width:'10%',
        render: (text, record, index) => (
          <InputItem
              type="customer"
              name="attachment"
              isViewMode
              defaultValue={this.state.dataSource[index].pk_accasoa?this.state.dataSource[index].pk_accasoa.display:""}
					/>
        )
      },
      {
        title: this.state.json['20021005card-000023'],/* 国际化处理： 辅助核算*/
        dataIndex: "assid",
        key: "assid",
         width:'10%',
        render: (text, record, index) => (
          <InputItem
              type="customer"
              name="attachment"
              isViewMode
              defaultValue={this.state.dataSource[index].assid?this.state.dataSource[index].assid.display:''}
					/>
        )
      },
      {
        title: this.state.json['20021005card-000018'],/* 国际化处理： 内部单位*/
        dataIndex: "pk_innercorp",
        key: "pk_innercorp",
        width: "10%",
        render: (text, record, index) => (
          <InnerCustSupplierForCustGridTreeRef
            value =  {{
             refname: this.state.dataSource[index].pk_innercorp?this.state.dataSource[index].pk_innercorp.display:"", 
              refpk: this.state.dataSource[index].pk_innercorp?this.state.dataSource[index].pk_innercorp.value:""
            }}  
            onChange={(v)=>{
              let {dataSource } =this.state;
              dataSource[index].pk_innercorp.display=v.refname;
              dataSource[index].pk_innercorp.value=v.refpk;
              this.setState({
                dataSource
              })                             
            }}            
          />
        )
      },
      {
        title: this.state.json['20021005card-000020'],/* 国际化处理： 币种*/
        dataIndex: "pk_currtype",
        key: "pk_currtype",
        width: "10%",
        render: (text, record, index) => (
          <CurrtypeGridRef
            value =  {{
              refname:  this.state.dataSource[index].pk_currtype?this.state.dataSource[index].pk_currtype.display:"", 
              refpk:  this.state.dataSource[index].pk_currtype?this.state.dataSource[index].pk_currtype.value:""
            }}
            onChange={(v)=>{
              if(v){
                let {dataSource} = this.state;
                dataSource[index].pk_currtype.display = v.refname;
                dataSource[index].pk_currtype.value = v.refpk;
                 //获取财务核算账簿
                 let arr = this.props.saveData.details;
                 let pk_accountingbook = '';//财务核算账簿
                 let data = '';//日期
                 for(let i=0;i<arr.length;i++){
                   if(dataSource[index].detailindex.value==arr[i].detailindex.value){
                     pk_accountingbook=arr[i].pk_accountingbook.value;
                     data=arr[i].prepareddate.value;
                   }
                 }
                 let body = {pk_accountingbook:pk_accountingbook,pk_currtype: dataSource[index].pk_currtype.value,date:data};
                 let self=this;
                 let url = '/nccloud/gl/glpub/ratequery.do';
                 ajax({
                   url,
                   data:body,
                   success:(response)=>{
                    let {dataSource }  = this.state;
                    let excrate =response. data.excrate2;
                    dataSource[index].localamount.display =  dataSource[index].occamount.display*excrate;	
                    dataSource[index].localamount.value =  dataSource[index].occamount.display*excrate;
                    self.setState({
                        dataSource:dataSource,
                        count:dataSource.length,
                    });
                   }
                 })
              }
            }}
          />
        )
      },
      {
        title: this.state.json['20021005card-000024'],/* 国际化处理： 方向*/
        dataIndex: "direct",
        key: "direct",
        width: "5%",
        render: (text, record, index) => (
          <InputItem
              type="customer"
              name="attachment"
              isViewMode
              defaultValue={this.state.dataSource[index].direct?this.state.dataSource[index].direct.display:""}
					/>
        )
      },
      {
        title: this.state.json['20021005card-000025'],/* 国际化处理： 主表项*/
        dataIndex: "pk_cashflow_main",
        key: "pk_cashflow_main",
        width: "10%",
        render: (text, record, index) => (
          <CashflowTreeRef
            value =  {{
              refname: this.state.dataSource[index].pk_cashflow_main?this.state.dataSource[index].pk_cashflow_main.display:"", 
              refpk: this.state.dataSource[index].pk_cashflow_main?this.state.dataSource[index].pk_cashflow_main.value:''
            }}
            onChange={(v)=>{
                let {dataSource } =this.state;
                dataSource[index].pk_cashflow_main.display=v.refname;
                dataSource[index].pk_cashflow_main.value=v.refpk;
                this.setState({
                  dataSource
                })                    
            }}
          />
        )
      },
      {
        title: this.state.json['20021005card-000026'],/* 国际化处理： 附表项*/
        dataIndex: "pk_cashflow_ass",
        key: "pk_cashflow_ass",
        width: "10%",
        render: (text, record, index) => (
          <CashflowTreeRef
            value = {{
              refname: this.state.dataSource[index].pk_cashflow_ass?this.state.dataSource[index].pk_cashflow_ass.display:"", 
              refpk: this.state.dataSource[index].pk_cashflow_ass?this.state.dataSource[index].pk_cashflow_ass.value:"" 
            }}
            onChange={(v)=>{
              let { dataSource } =this.state;
              dataSource[index].pk_cashflow_ass.display=v.refname;
              dataSource[index].pk_cashflow_ass.value=v.refpk;
              this.setState({
                dataSource
              })                             
            }}
          />
        )
      },
      {
        title: this.state.json['20021005card-000006'],/* 国际化处理： 原币*/
        dataIndex: "occamount",
        key: "occamount",
        width: "10%",
        render: (text, record, index) => (
          <InputItem
              type="customer"
              name="attachment"
              defaultValue={this.state.dataSource[index].occamount?this.state.dataSource[index].occamount.display:''}
              onBlur={(v)=>{
                let {dataSource} = this.state;
                this.countAmount(v,dataSource,index);
              }}
						  onChange={(v) => {
                if(v){
                  let {dataSource} = this.state;
                   dataSource[index].occamount.display = v;	
                   dataSource[index].occamount.value = v;	
                    
                    this.setState({
                      dataSource
                    },()=>{
                      this.props.getTableState(dataSource)
                    })
                   
                }
                
							}}
					/>
        )
      },
      {
        title: this.state.json['20021005card-000027'],/* 国际化处理： 本币*/
        dataIndex: "address",
        key: "address",
        width: "10%",
        render: (text, record, index) => (
          <InputItem
							type="customer"
							defaultValue={this.state.dataSource[index].localamount?this.state.dataSource[index].localamount.value:''}
						  onChange={(v) => {
                let {dataSource} = this.state;
                dataSource[index].localamount.display = v;	
                dataSource[index].localamount.value = v;	
                this.setState({
                  dataSource:dataSource
                },()=>{
                  this.props.getTableState(dataSource)
                })
							}}
					/>
        )
      }
    ];
  }
  componentWillMount(){
        let data = this.props.dataSource;
        if(data.length>0){
          this.setState({
            dataSource:data
          })
        }   
  }
  componentWillReceiveProps(nextProps) {
    let data = nextProps.dataSource;
    let { checkedArray} =this.state;
    if(data.length>0){
      for(let i=0,len=data.length;i<len;i++){
        data[i].key=i;
        checkedArray.push(false)
      }
    }
    this.setState({
      dataSource:data,
      checkedArray
    })
  }
  //
  getRowData=(v,index)=>{
    let id = v;
    let self=this;
    let url = '/nccloud/gl/cashflow/loadcf.do';
    ajax({
      url,
      data:this.props.saveData,
      success:(response)=>{
        let { data }=response;
        let { dataSource } = self.state;
        for(let i=0;i<data.length;i++){
          if(id==data[i].detailindex.value){
            dataSource[index] = data[i]
            dataSource[index].key = index;
          }
        }
        self.setState({
            dataSource:dataSource,
            count:dataSource.length,
        });
      }
    })
  }
  

  countAmount =(v,dataSource,index)=>{
     let self=this;
      let arr = self.props.saveData.details;
      let pk_accountingbook = '';//财务核算账簿
      let data = '';//日期
          for(let i=0;i<arr.length;i++){
              if(dataSource[index].detailindex.value==arr[i].detailindex.value){
                 pk_accountingbook=arr[i].pk_accountingbook.value;
                  data=arr[i].prepareddate.value;
                }
          }
    let body = {pk_accountingbook:pk_accountingbook,pk_currtype: dataSource[index].pk_currtype.value,date:data};
    let url = '/nccloud/gl/glpub/ratequery.do';
      ajax({
             url,
             data:body,
             success:function(response) {
                let excrate =response.data.excrate2;
                  dataSource[index].localamount.display = v*excrate;	
                  dataSource[index].localamount.value = v*excrate;
                  self.setState({
                    dataSource:dataSource,
                    count:dataSource.length,
                });
              }
          })
  }




  getSelectedDataFunc = (data)=>{
    let arr=[];
		for(let i=0,len=data.length;i<len;i++){
			arr.push(data[i].key)
		}
		this.setState({
			selectedArr:arr
    })
    this.props.selectedArr(arr)
  }
  //删除
  handleDelate = (value) => {
    let dataSource = this.state.dataSource;
    let selectedArr = this.state.selectedArr;
    let localArr1 = [];
    let temp = []; //临时数组1 
		let temparray = [];//临时数组2 
    if(selectedArr.length>0){
      for(let i=0;i<selectedArr.length;i++){
        localArr1.push(dataSource[selectedArr[i]])
      }
			for (let i = 0; i < localArr1.length; i++) { 					
			  temp[localArr1[i].key] = true;//把数组B的值当成临时数组1的键并赋值为真 
			};
			for (let i = 0; i < dataSource.length; i++) { 
					if (!temp[dataSource[i].key]) { 
						temparray.push(dataSource[i]);//同时把数组A的值当成临时数组1的键并判断是否为真，如果不为真说明没重复，就合并到一个新数组里，这样就可以得到一个全新并无重复的数组 
					} ; 
			}; 
    }
    this.setState({
      dataSource:temparray
    })                
  }
  //归集
  imputationShow=(e)=>{
    let dataSource = this.state.dataSource;
    let selectedArr = this.state.selectedArr;
    if(selectedArr.length>0){
      for(let i=0;i<selectedArr.length;i++){
        dataSource[selectedArr[i]].occamount={//原币
          value:dataSource[selectedArr[i]].occamount.value*e.rate/100,
          display:dataSource[selectedArr[i]].occamount.display*e.rate/100
        }
        dataSource[selectedArr[i]].pk_innercorp={//内部单位
          value:e.innercust.value,
          display:e.innercust.display
        }
        dataSource[selectedArr[i]].pk_cashflow_main={//现金流量
          value:e.cashflow.value,
          display:e.cashflow.display
        }
        dataSource[selectedArr[i]].pk_currtype={//币种
          value:e.currtype.value,
          display:e.currtype.display
        }
      }
      this.setState({
        dataSource:dataSource
      }) 
      this.props.getTableState(dataSource)
    }else{
      toast({ content: this.state.json['20021005card-000028'], color: 'warning' });/* 国际化处理： 请选择归集项*/
    }              
  }
  getBodyWrapper = body => {
    // return (
    //   // <Animate
    //   //   transitionName="move"
    //   //   component="tbody"
    //   //   className={body.props.className}
    //   // >
    //   //   {body.props.children}
    //   // </Animate>
    // );
  };

  onAllCheckChange = () => {
		let self = this;
    let checkedArray = [];
    let { dataSource }=self.state;
    let arr=[];
		for (var i = 0; i < self.state.checkedArray.length; i++) {
      checkedArray[i] = !self.state.checkedAll;
      if(checkedArray[i]){
        arr.push(dataSource[i].key)
      }
		}
		self.setState({
		  checkedAll: !self.state.checkedAll,
      checkedArray: checkedArray,
      selectedArr:arr
    });
    self.props.selectedArr(arr)
	  };
	  onCheckboxChange = (text, record, index) => {
    let self = this;
    let arr=[];
    let { dataSource }=self.state;
		let checkedArray = self.state.checkedArray.concat();
		checkedArray[index] = !self.state.checkedArray[index];
		for (var i = 0; i < self.state.dataSource.length; i++) {
      if(checkedArray[i]){
        arr.push(dataSource[i].key)
      }
    }
		self.setState({
      checkedArray: checkedArray,
      selectedArr:arr
		});
    self.props.selectedArr(arr)
	  };


  renderColumnsMultiSelect(columns) {
		const { data,checkedArray } = this.state;
		const { multiSelect } = this.props;
		
		let select_column = {};
		let indeterminate_bool = false;
		// let indeterminate_bool1 = true;
		if (multiSelect && multiSelect.type === "checkbox") {
		  let i = checkedArray.length;
		  while(i--){
			  if(checkedArray[i]){
				indeterminate_bool = true;
				break;
			  }
		  }
		  let defaultColumns = [
			{
			  title: (
				<Checkbox
				  className="table-checkbox"
				  checked={this.state.checkedAll}
				  indeterminate={indeterminate_bool&&!this.state.checkedAll}
				  onChange={this.onAllCheckChange}
				/>
			  ),
			  key: "checkbox",
			  dataIndex: "checkbox",
			  width: "5%",
			  render: (text, record, index) => {
				return (
				  <Checkbox
					className="table-checkbox"
					checked={this.state.checkedArray[index]}
					onChange={this.onCheckboxChange.bind(this, text, record, index)}
				  />
				);
			  }
			}
		  ];
		  columns = defaultColumns.concat(columns);
		}
		return columns;
	  }


  render() {
    const { dataSource } = this.state;
    let columns = this.renderColumnsMultiSelect(this.columns);
    return (
      <div className="cash-content">
        {/* <Button
          className="editable-add-btn"
          type="ghost"
          onClick={this.handleAdd}
        >
          添加
        </Button> */}
        <Table
          data={dataSource}
          columns={columns}
          // onRowClick={this.clickRow}
          className = "cashTable"
         // getBodyWrapper={this.getBodyWrapper}
          getSelectedDataFunc={this.getSelectedDataFunc}
        //  multiSelect={multiObj}
        />
      </div>
    );
  }
}
