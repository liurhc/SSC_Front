import React, { Component } from 'react';
import { InputItem } from '../../../../public/components/FormItems';
import {high,base } from 'nc-lightapp-front';
const { Refer } = high;
const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
    NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
    NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCButtonGroup:ButtonGroup,NCModal:Modal
    } = base;
import CashflowTreeRef from '../../../../../uapbd/refer/fiacc/CashflowTreeRef';
import InnerCustSupplierForCustGridTreeRef from '../../../../../uapbd/refer/supplier/InnerCustSupplierForCustGridTreeRef';
import CurrtypeGridRef from '../../../../../uapbd/refer/pubinfo/CurrtypeGridRef';

export default class Imputation extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            modalSize: '',
            sendData:{
                rate:'',
                cashflow:{
                    display:'',
                    value:''
                },
                innercust:{
                    display:'',
                    value:''
                },
                currtype:{
                    display:'',
                    value:''
                }
            },
           
        };
        this.close = this.close.bind(this);
        this.open = this.open.bind(this);
    }
    componentWillReceiveProps(nextProps) {
        if(nextProps.show){
            this.setState({
                showModal:nextProps.show
            });
        }
    }
    //确定时
    confirm = ()=>{
        // this.props.onCancel(false);
        this.setState({
            showModal: false
        });
        this.props.confirm(this.state.sendData);
    }
    close() {
        this.setState({
            showModal: false
        });
        this.props.onCancel(false)
    }

    open() {
        this.setState({
            showModal: true
        });
    }

    render() {
        let { cashflow,currtype,innercust } =this.state.sendData;
        return (
            <div>
                <Modal show={ this.state.showModal } size="lg" onHide={ this.close }>
                    <Modal.Header closeButton>
                        <Modal.Title>{this.state.json['20021005card-000030']/* 国际化处理： 归集操作*/} </Modal.Title>
                    </Modal.Header >
                    <Modal.Body >
                        <Row>
                            <Col xs={4} md={4}>
                                <span>{this.state.json['20021005card-000017']/* 国际化处理： 现金流量项目*/}</span>
                            </Col>
                            <Col xs={8} md={8}>
                                <CashflowTreeRef
                                     value={{
                                     refname: cashflow.display, 
                                     refpk: cashflow.value 
                                     }}
                                     onChange={(v)=>{
                                        let {sendData} = this.state;
                                        sendData.cashflow.display = v.refname;
                                        sendData.cashflow.value = v.refpk;
                                        this.setState({
                                            sendData
                                        })                           
                                    }}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={4} md={4}>
                                <span>{this.state.json['20021005card-000018']/* 国际化处理： 内部单位*/}</span>
                            </Col>
                            <Col xs={8} md={8}>
                                <InnerCustSupplierForCustGridTreeRef
                                      value={{
                                        refname: innercust.display, 
                                        refpk: innercust.value 
                                        }}
                                        onChange={(v)=>{
                                           let {sendData} = this.state;
                                           sendData.innercust.display = v.refname;
                                           sendData.innercust.value = v.refpk;
                                           this.setState({
                                                sendData
                                           })                           
                                       }}
                                />
                                </Col>
                        </Row>
                        <Row>
                            <Col xs={4} md={4}>
                                <span>{this.state.json['20021005card-000019']/* 国际化处理： 比例*/}(%)</span>
                            </Col>
                            <Col xs={8} md={8}> 
                                <FormControl
                                   type="customer"
                                   value={this.state.sendData.rate}
                                   onChange={(v) => {
                                            let {sendData} = this.state;
                                            sendData.rate = v;
                                            this.setState({
                                                sendData
                                            })
                                            // this.props.getTableState(dataSource)
                                       }}     
                                />             
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={4} md={4}>
                                <span>{this.state.json['20021005card-000020']/* 国际化处理： 币种*/}</span>
                            </Col>
                            <Col xs={8} md={8}>
                                 <CurrtypeGridRef
                                     value={{
                                        refname: currtype.display, 
                                        refpk: currtype.value 
                                        }}
                                        onChange={(v)=>{
                                           let {sendData} = this.state;
                                           sendData.currtype.display = v.refname;
                                           sendData.currtype.value = v.refpk;
                                           this.setState({
                                            sendData
                                           })                           
                                       }}
                                />            
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={ this.confirm.bind(this) }> {this.state.json['20021005card-000003']/* 国际化处理： 确定*/} </Button>
                        <Button onClick={ this.close }> {this.state.json['20021005card-000004']/* 国际化处理： 取消*/} </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}
