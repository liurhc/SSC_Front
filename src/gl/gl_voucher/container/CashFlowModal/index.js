import React, { Component } from 'react';
import {high,base,ajax,toast} from 'nc-lightapp-front';
const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
    NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
    NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCButtonGroup:ButtonGroup,NCModal:Modal
    } = base;
import Imputation from './Imputation';//归集模态框组件
import CashTable from './CashTable';//主表格组件
const Option = Select.Option;
const OptGroup = Select.OptGroup;
export default class CashFlowModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            details:[],//分录信息
            modalDropup: true,
            dataSource:[],//现金流量信息
            count:"",
            tableIndex:'',//表格index
            tableData:[],
            ImputationShow:false,//归集判断
            selectedArr:[],//选中项
            analysistype:this.state.json['20021005card-000031']/* 国际化处理： 不分析*/
        };
        this.close = this.close.bind(this);
        this.open = this.open.bind(this);
        this.changeDropup = this.changeDropup.bind(this);
    }
    componentWillMount(){
        
    }
    componentWillReceiveProps(nextProps) {
        let { showModal }=this.state
        if(nextProps.show!=showModal){
            let cashArr = nextProps.cashFlow;
            let localArr=[];
            for(let i=0;i<cashArr.length;i++){
                let cashArrI=cashArr[i].cashflow;
                if(cashArrI&&Array.isArray(cashArrI)){
                    for(let j=0;j<cashArrI.length;j++){
                        localArr.push(cashArrI[j])
                    }
                }
            }
            localArr.map((item,i)=>{
                item.key=++i
            })
            this.setState({
                showModal:nextProps.show,
                details:nextProps.sendData.details,
                dataSource:localArr,
                count:localArr.length
            });
        }
    }
    //载入
    onLoad = ()=>{
        let self=this;
        let data = this.props.sendData;
        let url = '/nccloud/gl/cashflow/loadcf.do';
        ajax({
            url,
            data,
            success:function(response) {
                let  { data, message, success } = response; 
                self.state.dataSource=[];
                let dataSource=[];
                if(data&&data.length!=0){
                    for(let i=0,len=data.length;i<len;i++){
                        data[i].key=i+1
                        dataSource.push(data[i])
                    }
                    self.setState({
                        dataSource:dataSource,
                        count:dataSource.length,
                    });
                }
            }

        })
    }
    //分析
    onAnalysis = () => {
        let self=this;
        let { analysistype }=self.state
        let data ={ "analysistype":analysistype,"voucher":self.props.sendData};
        let url = '/nccloud/gl/cashflow/analysiscf.do';
        data.voucher.period.value="03";
        ajax({
            url,
            data,
            success:function (response) {
                let  { data, message, success } = response; 
                self.state.dataSource=[];
                let dataSource=[];
                if(data&&data.length!=0){
                    for(let i=0,len=data.length;i<len;i++){
                        data[i].key=i+1
                        dataSource.push(data[i])
                    }
                    self.setState({
                        dataSource:dataSource,
                        count:dataSource.length,
                    });
                }
            }
        })
    }
    //增加
    handleAdd = () => {
        const { dataSource } = this.state;
        const newData = {
            'direct':{"display":"0","value":"0","scale":2.0},
            "groupamount":{"display":"0","value":"0","scale":2.0},
            "groupcreditamount":{"display":"0","value":"0","scale":2.0},
            "localdebitamount":{"display":"0","value":"0","scale":2.0},
            "pk_unit":{"display":"","value":"","scale":0.0},
            "detailindex":{"display":'',"value":'',"scale":0.0},
            "pk_cashflow_main":{"display":"","value":"","scale":0.0},
            "pk_cashflow_ass":{"display":"","value":"","scale":0.0},
            "occamount":{"display":"0","value":"0","scale":2.0},
            "globaldebitamount":{"display":"0","value":"0","scale":2.0},
            "assid":{"display":"","value":"","scale":0.0},
            "globalamount":{"display":"0","value":"0","scale":2.0},
            "debitamount":{"display":"0","value":"0","scale":2.0},
            "pk_currtype":{"display":"","value":null,"scale":0.0},
            "localcreditamount":{"display":"","value":"","scale":2.0},
            "pk_accasoa":{"display":"","value":"","scale":0.0},
            "localamount":{"display":"","value":"","scale":2.0},
            "pk_innercorp":{"display":"","value":"","scale":0.0},
            "globalcreditamount":{"display":"","value":"","scale":2.0},
            "groupdebitamount":{"display":"","value":"","scale":2.0},
            "pk_detail":{"display":"","value":"","scale":0.0},
            "creditamount":{"display":"","value":"","scale":2.0}};
        this.setState({
          dataSource: [...dataSource, newData],
          count: count
        });
    };
    //删除
    handleDelate=()=>{
        if(this.state.selectedArr.length>0){
            let dataSource = this.state.dataSource;
            let selectedArr = this.state.selectedArr;
            let localArr1 = [];
            let temp = []; //临时数组1 
            let temparray = [];//临时数组2 
            if(selectedArr.length>0){
            for(let i=0;i<selectedArr.length;i++){
                localArr1.push(dataSource[selectedArr[i]-1])
            }
                    for (let i = 0; i < localArr1.length; i++) { 					
                    temp[localArr1[i].key] = true;//把数组B的值当成临时数组1的键并赋值为真 
                    };
                    for (let i = 0; i < dataSource.length; i++) { 
                            if (!temp[dataSource[i].key]) { 
                                temparray.push(dataSource[i]);//同时把数组A的值当成临时数组1的键并判断是否为真，如果不为真说明没重复，就合并到一个新数组里，这样就可以得到一个全新并无重复的数组 
                            } ; 
                    }; 
            }
            this.state.dataSource=temparray
            this.refs.sendDelate.handleDelate(this.state.tableIndex);
        }else{
        }
    }

    //确定
    getTableState=(e)=>{
        this.setState({
            tableData:e
        })
    }
    handleConfirm=()=>{
        let { sendData }=this.props
        let {details,dataSource}=this.state;
        for(let i=0;i<details.length;i++){
            details[i].cashflow=[]
        }
        for(let i=0;i<details.length;i++){
            for(let j=0;j<dataSource.length;j++){
                if(details[i].detailindex.value==dataSource[j].detailindex.value){
                    details[i].cashflow.push(dataSource[j])
                    details[i].expand.cashflowname.value=dataSource[j].pk_cashflow_main.display
                }
            }
        }
        sendData.details=details
        this.props.getCashFlow(sendData);
    }
  
    selectedArr = (e)=>{
        this.state.selectedArr=e
    }
    //归集
    onImputation = ()=>{
        if(this.state.selectedArr.length>0){
            this.setState({
                ImputationShow:true
            })
        }else{
            toast({ content: this.state.json['20021005card-000028'], color: 'warning' });/* 国际化处理： 请选择归集项*/
        }
    }
    close() {
        this.setState({
            showModal: false
        });
        this.props.onCancel(false)
    }

    open() {
        this.setState({
            showModal: true
        });
    }

    changeDropup(state) {
        this.setState({
            modalDropup: state
        });
    }

    handleChange=(v)=>{
        let { analysistype }=this.state;
        analysistype=v;
        this.setState({
            analysistype
        })
    }

    render() {
        let {ImputationShow}=this.state;
        return (
                 <Modal
                    show={ this.state.showModal }
                    onHide={ this.close }
                    animation={true}
                    size="xlg"
                    >
                    <Modal.Header closeButton>
                        <Modal.Title>{this.state.json['20021005card-000016']/* 国际化处理： 现金流量分析*/} </Modal.Title>
                    </Modal.Header >
                    <Modal.Body >
                    <div className="top-banner">
                        <Col md={6} xs={6} sm={6}>
                            <div className='gray'>{this.state.json['20021005card-000035']}：{this.state.json['20021005card-000036']/* 国际化处理： 当前凭证现金类科目发生净额,借方*/} 10000</div>
                        </Col>
                        <Col md={6} xs={6} sm={6}>
                            <div className="cash-content">
                                <div className='grayLight'>
                                    <span>{this.state.json['20021005card-000037']/* 国际化处理： 分配方式*/}：</span>
                                    <Select
                                        size="lg"
                                        defaultValue={this.state.json['20021005card-000031']}/* 国际化处理： 不分析*/
                                        style={{ width: 200, marginRight: 6,zIndex:999 }}
                                        onChange={this.handleChange}
                                    >
                                        <Option value="">&nbsp; </Option>
                                        <Option value={this.state.json['20021005card-000031']}>{this.state.json['20021005card-000031']/* 国际化处理： 不分析,不分析*/}</Option>
                                        <Option value={this.state.json['20021005card-000032']}>{this.state.json['20021005card-000032']/* 国际化处理： 金额对应,金额对应*/}</Option>
                                        <Option value={this.state.json['20021005card-000033']}>{this.state.json['20021005card-000033']/* 国际化处理： 比例分配,比例分配*/}</Option>
                                        <Option value={this.state.json['20021005card-000034']}>{this.state.json['20021005card-000034']/* 国际化处理： 月末结转,月末结转*/}</Option>
                                    </Select>
                                </div>
                            </div>
                        </Col>
                    </div>
                        <CashTable 
                            dataSource={this.state.dataSource} 
                            ref="sendDelate" 
                            getTableState={this.getTableState.bind(this)}
                            saveData={this.props.sendData}
                            selectedArr={this.selectedArr.bind(this)}
                        ></CashTable>
                    </Modal.Body>
                    <Modal.Footer>
                        <ButtonGroup style={{ margin: 10 }}>
                            <Button shape='border' onClick={this.onLoad.bind(this)}>{this.state.json['20021005card-000038']/* 国际化处理： 载入*/}</Button>
                            <Button shape='border' onClick={this.onAnalysis.bind(this)}>{this.state.json['20021005card-000039']/* 国际化处理： 分析*/}</Button>
                            <Button shape='border' onClick={this.onImputation.bind(this)}>{this.state.json['20021005card-000015']/* 国际化处理： 归集*/}</Button>
                            <Button shape='border' onClick={this.handleAdd.bind(this)}>{this.state.json['20021005card-000040']/* 国际化处理： 增行*/}</Button>
                            <Button shape='border' onClick={this.handleDelate.bind(this)}>{this.state.json['20021005card-000041']/* 国际化处理： 删除*/}</Button>
                            <Button shape='border' onClick={this.handleConfirm.bind(this)}>{this.state.json['20021005card-000003']/* 国际化处理： 确定*/}</Button>
                            <Button shape='border' onClick={this.close}>{this.state.json['20021005card-000004']/* 国际化处理： 取消*/}</Button>
                        </ButtonGroup>
                    </Modal.Footer>
                        <Imputation 
                            show={ImputationShow}
                            title={this.state.json['20021005card-000015']}/* 国际化处理： 归集*/
                            icon=""
                            className="im-index"
                            confirm={(e) => {
                                let ImputationShow = false;
                                let dataSource  = this.state.dataSource;
                                this.refs.sendDelate.imputationShow(e);
                                this.setState({ ImputationShow });
                            }}
                            onCancel={() => {
                                let ImputationShow = false;
                                this.setState({ ImputationShow });
                            }}
                    ></Imputation>
                </Modal>
        )
    }
}
