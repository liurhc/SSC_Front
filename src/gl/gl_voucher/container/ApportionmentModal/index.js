import React, { Component } from 'react';
import { high, base, ajax,getMultiLang } from 'nc-lightapp-front';
import './index.less'
const {
	NCFormControl: FormControl,
	NCDatePicker: DatePicker,
	NCButton: Button,
	NCRadio: Radio,
	NCBreadcrumb: Breadcrumb,
	NCRow: Row,
	NCCol: Col,
	NCTree: Tree,
	NCMessage: Message,
	NCIcon: Icon,
	NCLoading: Loading,
	NCTable: Table,
	NCSelect: Select,
	NCCheckbox: Checkbox,
	NCNumber,
	NCModal: Modal,
	NCForm: Form
} = base;
const { Refer } = high;
import ApportionRuleRef from '../../../refer/voucher/ApportionRuleRef';
import { CheckboxItem, RadioItem, TextAreaItem, ReferItem, InputItem } from '../../../public/components/FormItems';
const { NCFormItem: FormItem } = Form;
const format = 'YYYY-MM-DD';
export default class MsgModal extends Component {
	static defaultProps = {
		show: false,
		title: '',
		content: '',
		closeButton: false,
		icon: 'icon-tishianniuchenggong',
		isButtonWhite: true,
		isButtonShow: true,
		multiSelect: {
			type: 'checkbox',
			param: 'key'
		}
	};

	constructor(props) {
		super(props);
		this.state = {
			title_head:"",
			title_head:"",
			checkFormNow: false, //控制表单form回调
			loadItem: props.findRows,
			colName: [],
			colValue: [],
			json:{},
			appCount: '',
			amountScale:'',
			apport: { refname: '', refpk: '' } //分摊参照默认值
		};
	}

	componentWillMount() {
		let callback= (json) =>{
			this.setState({json:json},()=>{
				//initTemplate.call(this, this.props);
				// this.loadDept();
			})
		}
		getMultiLang({moduleId:'20021005card',domainName:'gl',currentLocale:'zh-CN',callback});
	}


	componentWillReceiveProps(nextProps) {
		this.setState({
			loadItem: nextProps.loadForm,
			appCount: nextProps.loadForm.amount,
			amountScale: nextProps.loadForm.amountScale,
            apport: { refname: '', refpk: '' }, //分摊参照,每次打开弹框都清空 liuhuit
            colName: [],
			colValue: [],
		});
	}
	//点击保存,修改值传至父组件
	modalMessage = () => {
		let self = this;
		self.setState({
			checkFormNow: true
		});
	};

	assureFormCallback = (isCheck, values, others) => {
		let self = this;
		let { loadItem } = self.state;
		values.map((item, i) => {
			loadItem[0][item.name].value = item.value;
		});
		self.props.onConfirm(loadItem);
		self.setState({
			checkFormNow: false
		});
	};

	apportionQuery = (apport) => {
		let self = this;
		let data = {
			pk_apportrule: apport
		};
		let url = '/nccloud/gl/voucher/apportionQuery.do';
		ajax({
			url,
			data,
			success: function(res) {
				let { colNames, value } = res.data;
				self.setState({
					colName: colNames,
					colValue: value
				});
				//this.updataView()
			}
		});
	};
	loadData = (amountValue) => {
		let { loadItem, apport, appCount,amountScale} = this.state;
		return [
			<div className="modal-form-item">
				<label class="item-title nc-theme-common-font-c">{this.state.json['20021005card-000005']}({this.state.json['20021005card-000006']/* 国际化处理： 分摊金额,原币*/})</label>
				<NCNumber
					value={appCount ? appCount : ''}
					scale={amountScale?amountScale:2}
					//disabled={true}
					onChange={(v) => {
						appCount = v;
						this.setState(
							{
								appCount
							},
							() => {
								//	self.props.onConfirm(listItem);
							}
						);
					}}
				/>
			</div>,
			<div className="modal-form-item">
				<label class="item-title nc-theme-common-font-c">{this.state.json['20021005card-000007']/* 国际化处理： 分摊规则*/}</label>
				<div className="item-content">
					<ApportionRuleRef
						value={apport}
						queryCondition={() => {
							return {
								prepareddate: loadItem.prepareddate,
								pk_accountingbook: loadItem.pk_accountingbook,
								pk_accasoa: loadItem.pk_accasoa,
								pk_unit: loadItem.pk_unit
							};
						}}
						onChange={(v) => {
							let apport = { refname: v.refname, refpk: v.refpk };
							this.setState({
								apport
							});
							if (v.refpk) {
								this.apportionQuery(v.refpk);
							}
						}}
					/>
				</div>
			</div>
		];
	};

	render() {
		let self = this;
		let { show, title, isButtonShow } = this.props;
		let  { loadItem, colValue, colName, appCount, apport } = self.state;
		return (
			<Modal
				className="simpleModal quicka"
				size="lg"
				show={show}
				onHide={() => {
					this.props.onCancel(true);
				}}
			>
				<Modal.Header closeButton>
					<Modal.Title>{title}</Modal.Title>
				</Modal.Header>
				<Modal.Body className="body_quick">
					{self.loadData(loadItem)}
					{colName.length > 0 && (
					colName.length < 4==true?

					(colName.length ==1?
					<div className="listTitleyi">
						{colName.map((e, i) => {
							return <span className="titleme">{e}</span>;
						})}							
					</div>:
					<div className="listTitle">
						{colName.map((e, i) => {
							return <span className="titleme">{e}</span>;
						})}							
					</div>):
					<div className="listTitlejia">
						{colName.map((e, i) => {
							return <span className="titleme">{e}</span>;
						})}							
					</div>
					)}
					{colValue.length > 0 &&
					colValue[1].length < 4 == true?
					colValue.length > 0 &&
					colValue[1].length == 1 ?
					colValue.map((e, i) => {
						return (
							<div className="listItemyi" id="why">
								{e.map((k, i) => {
									return <div className="itemm">{k}</div>;
								})}
							</div>
						)
					}):
						colValue.map((e, i) => {
							return (
								<div className="listItemjia" id="why">
									{e.map((k, i) => {
										return <div className="itemm">{k}</div>;
									})}
								</div>
							);
						}) :
						colValue.map((e, i) => {
							return (
								<div className="listItem" id="why">
									{e.map((k, i) => {
										return <div className="itemm">{k}</div>;
									})}
								</div>
							);
						})}
				</Modal.Body>
				{isButtonShow && (
					<Modal.Footer>
						<Button
							className="button-primary"
							onClick={() =>
								this.props.onConfirm({
									apportionAmount: appCount,
									pk_apprule: apport.refpk
								})}
						>
							{this.state.json['20021005card-000003']}
						</Button>
						<Button
							onClick={() => {
								this.props.onCancel(false);
							}}
						>
							{this.state.json['20021005card-000004']}
						</Button>
					</Modal.Footer>
				)}
			</Modal>
		);
	}
}
