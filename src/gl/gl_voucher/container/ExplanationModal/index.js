import React, { Component } from 'react';
//import moment from 'moment';
//import { Panel,PanelGroup,Label, Button, Radio,  Breadcrumb, Con, Row, Col, Tree, Message, Icon, Table, Modal,Checkbox } from 'tinper-bee';
//import Menu, { Item as MenuItem, Divider, SubMenu, MenuItemGroup } from 'bee-menus';
//import zhCN from 'rc-calendar/lib/locale/zh_CN';
// import Form from 'bee-form';
// import 'bee-form/build/Form.css';
import { high, base, ajax } from 'nc-lightapp-front';
const {
	NCFormControl: FormControl,
	NCDatePicker: DatePicker,
	NCButton: Button,
	NCRadio: Radio,
	NCBreadcrumb: Breadcrumb,
	NCRow: Row,
	NCCol: Col,
	NCTree: Tree,
	NCMessage: Message,
	NCIcon: Icon,
	NCLoading: Loading,
	NCTable: Table,
	NCSelect: Select,
	NCCheckbox: Checkbox,
	NCNumber,
	NCModal: Modal,
	NCForm: Form
} = base;
const { Refer } = high;
import { CheckboxItem, RadioItem, TextAreaItem, ReferItem, InputItem } from '../../../public/components/FormItems';
import './index.less';

const { NCFormItem: FormItem } = Form;
const format = 'YYYY-MM-DD';
export default class MsgModal extends Component {
	static defaultProps = {
		confirmText: this.state.json['20021005card-000003'],/* 国际化处理： 确定*/
		cancelText: this.state.json['20021005card-000004'],/* 国际化处理： 取消*/
		show: false,
		title: '',
		content: '',
		closeButton: false,
		icon: 'icon-tishianniuchenggong',
		isButtonWhite: true,
		isButtonShow: true,
		multiSelect: {
			type: 'checkbox',
			param: 'key'
		}
	};

	constructor(props) {
		super(props);
		this.state = {
			checkFormNow: false, //控制表单form回调
			startdate: {
				//制单日期
			},
			accountData: {
				rows: []
			},
			count: 4,
			checkedAll: false,
			checkedArray: [],
			contentMessage: {
				value: '',
				index: ''
			},
			pk_unit: '',
			show: false
		};
	}

	componentWillReceiveProps(nextProps) {
		let { pk_unit } = this.state;
		if (pk_unit != nextProps.pk_unit) {
			this.setState(
				{
					pk_unit: nextProps.pk_unit,
					show: nextProps.show
				},
				() => {
					if (this.state.pk_unit) {
						let dataCount = {
							pid: '',
							keyword: '',
							queryCondition: {
								pk_org: this.state.pk_unit
							},
							pageInfo: {
								pageSize: 10,
								pageIndex: -1
							}
						};
						ajax({
							url: '/nccloud/fipub/ref/SummaryRef.do',
							data: dataCount,
							success: (res) => {
								let { rows } = res.data;
								let { accountData, checkedArray } = this.state;
								let { success, data } = res;
								if (success) {
									// dataRows=data;
									rows.map((e, i) => {
										checkedArray.push(false);
									});
									accountData.rows = rows;
									this.setState(
										{
											accountData,
											checkedArray
										},
										() => {
										}
									);
								}
							}
						});
					}
				}
			);
		}
		this.setState({
			show: nextProps.show
		});
	}

	onAllCheckChange = () => {
		let self = this;
		let checkedArray = [];
		//let listData = self.state.data.concat();
		let selIds = [];
		// let id = self.props.multiSelect.param;
		for (var i = 0; i < self.state.checkedArray.length; i++) {
			checkedArray[i] = !self.state.checkedAll;
		}

		self.setState({
			checkedAll: !self.state.checkedAll,
			checkedArray: checkedArray
		});
	};
	onCheckboxChange = (text, record, index) => {
		let self = this;
		//let allFlag = false;
		let { contentMessage } = this.state;
		contentMessage.value = record.refname;
		//contentMessage.index=record.key;
		let checkedArray = self.state.checkedArray.concat();
		checkedArray[index] = !self.state.checkedArray[index];
		for (var i = 0; i < self.state.checkedArray.length; i++) {
			// if (!checkedArray[i]) {
			// allFlag = false;
			// break;
			// } else {
			// allFlag = true;
			// }
			if (i != index) {
				checkedArray[i] = false;
			}
		}
		self.setState({
			//checkedAll: allFlag,
			checkedArray: checkedArray,
			contentMessage
			// selIds: selIds
		});
		// self.props.onSelIds(selIds);
	};

	renderColumnsMultiSelect(columns) {
		const { data, checkedArray } = this.state;
		const { multiSelect } = this.props;

		let select_column = {};
		let indeterminate_bool = false;
		// let indeterminate_bool1 = true;
		if (multiSelect && multiSelect.type === 'checkbox') {
			let i = checkedArray.length;
			while (i--) {
				if (checkedArray[i]) {
					indeterminate_bool = true;
					break;
				}
			}
			let defaultColumns = [
				{
					// <Checkbox
					//   className="table-checkbox"
					//   checked={this.state.checkedAll}
					//   indeterminate={indeterminate_bool&&!this.state.checkedAll}
					//   onChange={this.onAllCheckChange}
					// />
					title: '',
					key: 'checkbox',
					dataIndex: 'checkbox',
					width: '5%',
					render: (text, record, index) => {
						return (
							<Checkbox
								className="table-checkbox"
								checked={this.state.checkedArray[index]}
								onChange={this.onCheckboxChange.bind(this, text, record, index)}
							/>
						);
					}
				}
			];
			columns = defaultColumns.concat(columns);
		}
		return columns;
	}

	checkForm = (flag, obj) => {
		let edit = obj;
		this.props.onSubmit(edit, this.props.opre);
		this.setState({
			checkFormNow: false,
			open1: true,
			open2: true,
			open3: true
		});
	};

	//选定常用摘要,传至父组件
	modalMessage = () => {
		let self = this;
		let checkedArray = [];
		let contentName = self.state.contentMessage;
		self.props.onConfirm(contentName);
		//清空true 修改为false;每次选定摘要初始化
		for (var i = 0; i < self.state.checkedArray.length; i++) {
			checkedArray[i] = false;
		}
		self.setState({
			checkedAll: false,
			checkedArray: checkedArray
		});
	};

	//日期控制
	handleInputChange = (obj, type, val) => {
		let { startdate } = this.state;
		if (type === 'begindate' || type === 'enddate') {
			//	val = moment(val);
		}
		startdate.value = val;
	};

	handleGTypeChange = () => {};
	//增加一行
	// addClick =() =>{
	// 	const {count,accountData}=this.state;
	// 	const newData=
	// 		{ a:`${count*10}`, b: "部门级", c: "财务", d: "操作",key:count };
	// 		this.setState({
	// 			accountData:{rows:[...accountData.rows, newData]} ,
	// 			count: count + 1
	// 		});
	// }

	loadRows = () => {
		let { checkFormNow, banks, accountData } = this.state;
		let accountRows = accountData.rows;
		let self = this;
		let accountColumns = [
			{ title: this.state.json['20021005card-000061'], dataIndex: 'refcode', key: 'a', width: 100 },/* 国际化处理： 编码*/
			//{ title: "组织类型", dataIndex: "b", key: "b", width: 100 },
			{ title: this.state.json['20021005card-000062'], dataIndex: 'refname', key: 'c', width: 200 }/* 国际化处理： 名称*/
			// {
			// 	title: "操作",
			// 	dataIndex: "d",
			// 	key: "d",
			// 	render(text, record, index) {
			// 		return (
			// 			<div className="explanationAction">
			// 			<a
			// 				href="#"
			// 				onClick={() => {
			// 					alert('这是第'+index+'列，内容为:'+text);
			// 				}}
			// 			>
			// 				选择
			// 			</a>
			// 			<a
			// 			href="#"
			// 			onClick={() => {
			// 				alert('这是第'+index+'列，内容为:'+text);
			// 			}}
			// 		>
			// 			修改
			// 		</a>
			// 		<a
			// 		href="#"
			// 		onClick={() => {
			// 			alert('这是第'+index+'列，内容为:'+text);
			// 		}}
			// 	>
			// 		删除
			// 	</a></div>
			// 		);
			// 	}
			// }
		];
		let columns = this.renderColumnsMultiSelect(accountColumns);
		return <Table bordered columns={columns} data={accountRows} />;
	};

	render() {
		let { title, content, confirmText, cancelText, closeButton, icon, isButtonWhite, isButtonShow } = this.props;
		let { show } = this.state;
		return (
			<Modal className="simpleModal combine wodewode" show={show} onHide={() => this.props.onCancel(true)}>
				<Modal.Header closeButton>
					<div className="title">{title || this.state.json['20021005card-000063']/* 国际化处理： 摘要*/}</div>
				</Modal.Header>

				<Modal.Body>{this.loadRows()}</Modal.Body>
				{isButtonShow && (
					<Modal.Footer>
						{/* <Button  className="btn-2 btn-insert" onClick={ this.addClick }>新增</Button> */}
						<Button className="button-primary" onClick={() => this.modalMessage()}>
							{confirmText}
						</Button>
						<Button
							onClick={() => {
								this.props.onCancel(true);
							}}
						>
							{cancelText}
						</Button>
					</Modal.Footer>
				)}
			</Modal>
		);
	}
}
