import React, { Component } from 'react';
import { high, base, ajax ,getMultiLang} from 'nc-lightapp-front';
const {
	NCFormControl: FormControl,
	NCDatePicker: DatePicker,
	NCButton: Button,
	NCRadio: Radio,
	NCBreadcrumb: Breadcrumb,
	NCRow: Row,
	NCCol: Col,
	NCTree: Tree,
	NCMessage: Message,
	NCIcon: Icon,
	NCLoading: Loading,
	NCTable: Table,
	NCSelect: Select,
	NCCheckbox: Checkbox,
	NCNumber,
	NCModal: Modal,
	NCForm: Form
} = base;
import Verify from '../../../verify/verify/vrify/exportVrify.js';
import { CheckboxItem, RadioItem, TextAreaItem, ReferItem, InputItem } from '../../../public/components/FormItems';
import './index.less';

const { NCFormItem: FormItem } = Form;
const format = 'YYYY-MM-DD';
export default class VrifyRefModal extends Component {
	static defaultProps = {
		show: false,
		title: '',
		content: '',
		closeButton: false,
		icon: 'icon-tishianniuchenggong',
		isButtonWhite: true,
		isButtonShow: true,
		multiSelect: {
			type: 'checkbox',
			param: 'key'
		}
	};

	constructor(props) {
		super(props);
		this.state = {
			loadItem: props.findRows,
			colName: [],
			colValue: [],
			dataFirst: {}, //参照核销数据
			appCount: '',
			pk_accasoa: '', //科目默认值
			period: '', //期间默认值
			assid: '', //辅助核算默认值
			pk_currtype: '', //币种
			balance: [], //子表展示信息
			json:{}
		};
	}

	componentWillMount() {
		let callback= (json) =>{
			this.setState({json:json},()=>{
			})
		}
		getMultiLang({moduleId:'20021005card',domainName:'gl',currentLocale:'zh-CN',callback});
	}
	componentWillReceiveProps(nextProps) {
		if (nextProps.show) {
			this.setState({
				dataFirst:nextProps.querydata
			})
		}
	}
	onRef = (ref) => {
		this.child = ref;
	};
	//确定
	verifyEnsure = (data) => {
		let backData = this.child.getData_CDMoney();
		if(backData.verifyFlag){
			if(backData.hasOwnProperty('m_voCreditDetails')){
				let creditdetails=[];
				backData.m_voCreditDetails.forEach((detail)=>{
					let creditdetail={};
					for(let key in detail){
						if(detail[key])
							creditdetail[key]=detail[key].value;
					}
					creditdetails.push(creditdetail);
				});
				backData.m_voCreditDetails=creditdetails;
			}
			if(backData.hasOwnProperty('m_voDebitDetails')){
				let debitdetails=[];
				backData.m_voDebitDetails.forEach((detail)=>{
					let debitdetail={};
					for(let key in detail){
						if(detail[key])
							debitdetail[key]=detail[key].value;
					}
					debitdetails.push(debitdetail);
				});
				backData.m_voDebitDetails=debitdetails;
			}
			
			this.props.updataAmount(backData);
		}
	};
	//点击保存,修改值传至父组件
	modalMessage = () => {
		let self = this;
		self.setState({
			checkFormNow: true
		});
	};

	assureFormCallback = (isCheck, values, others) => {
		let self = this;
		let { loadItem } = self.state;
		values.map((item, i) => {
			loadItem[0][item.name].value = item.value;
		});
		self.props.onConfirm(loadItem);
		self.setState({
			checkFormNow: false
		});
	};
	close=()=> {
        this.props.onCancel(false);
    }
	render() {
		let self = this;
		let {
			show,
			title,
			content,
			closeButton,
			icon,
			isButtonWhite,
			isButtonShow
		} = this.props;
		const { dataFirst } = self.state;
		const columns = [
			{ title: self.state.json['20021005card-000123'], dataIndex: 'dataType', key: 'dataType', width: 100 },/* 国际化处理： 数据类型*/
			{ title: self.state.json['20021005card-000077'], dataIndex: 'quantity', key: 'quantity', width: 100 },/* 国际化处理： 数量*/
			{ title: self.state.json['20021005card-000006'], dataIndex: 'amount', key: 'amount', width: 100 },/* 国际化处理： 原币*/
			{ title: self.state.json['20021005card-000027'], dataIndex: 'localamount', key: 'localamount', width: 100 }/* 国际化处理： 本币*/
		];
		return (
			<Modal show={show} onHide={this.close} id="vrifyModaltwo" fieldid="vrifyRef" size='xlg'>
				<Modal.Header closeButton fieldid="header-area">
					<Modal.Title fieldid={`${title}_title`}>{title}</Modal.Title>
					<span className="icon-wraper">
						{/* <i
							className="iconfont icon-shuaxin1"
							onClick={() =>
								this.props.onConfirm({
									apportionAmount: appCount,
									pk_apprule: apport.refpk
								})}
						/> */}
						{/* <i className="iconfont icon-guanbi" onClick={this.close} /> */}
					</span>
				</Modal.Header>
				<Modal.Body>
					<div className='voucherReferVerify'>
						<Verify
							data={dataFirst}
							voucherVerifyflag='1'
							onRef={this.onRef}
							onConfirm={(v) => {
							}}
						/>
					</div>
				</Modal.Body>
				{isButtonShow && (
					<Modal.Footer fieldid="bottom-area">
						<Button
							fieldid="vrifyRef"
							className="button-primary"
							onClick={this.verifyEnsure.bind(this)
							// () => {this.props.onCancel(false);}
							}
						>
							{this.state.json['20021005card-000003']}
						</Button>
					</Modal.Footer>
				)}
			</Modal>
		);
	}
}
