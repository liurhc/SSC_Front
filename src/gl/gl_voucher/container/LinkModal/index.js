import React, { Component } from 'react';
//import moment from 'moment';
//import { Panel,PanelGroup,Label, Button, Radio,  Breadcrumb, Con, Row, Col, Tree, Message, Icon, Table, Modal,Checkbox } from 'tinper-bee';
//import Menu, { Item as MenuItem, Divider, SubMenu, MenuItemGroup } from 'bee-menus';
//import zhCN from 'rc-calendar/lib/locale/zh_CN';
//import Form from 'bee-form';
//import 'bee-form/build/Form.css';
import { high, base, ajax ,getMultiLang} from 'nc-lightapp-front';
const {
	NCFormControl: FormControl,
	NCDatePicker: DatePicker,
	NCButton: Button,
	NCRadio: Radio,
	NCBreadcrumb: Breadcrumb,
	NCRow: Row,
	NCCol: Col,
	NCTree: Tree,
	NCMessage: Message,
	NCIcon: Icon,
	NCLoading: Loading,
	NCTable: Table,
	NCSelect: Select,
	NCCheckbox: Checkbox,
	NCNumber,
	NCModal: Modal,
	NCForm: Form,
	NCTooltip:Tooltip
} = base;
const { Refer } = high;
import ApportionRuleRef from '../../../refer/voucher/ApportionRuleRef';
//import Label from 'bee-label';
import { CheckboxItem, RadioItem, TextAreaItem, ReferItem, InputItem } from '../../../public/components/FormItems';

import { isObj } from '../../../public/components/oftenApi';

import { cutData } from '../Welcome/voucherOftenApi'

import './index.less';

const { NCFormItem: FormItem } = Form;
const format = 'YYYY-MM-DD';
export default class LinkModal extends Component {
	static defaultProps = {
		show: false,
		title: '',
		content: '',
		closeButton: false,
		icon: 'icon-tishianniuchenggong',
		isButtonWhite: true,
		isButtonShow: true,
		multiSelect: {
			type: 'checkbox',
			param: 'key'
		}
	};

	constructor(props) {
		super(props);
		this.state = {
			loadItem: props.findRows,
			colName: [],
			colValue: [],
			appCount: '',
			pk_accasoa: '', //科目默认值
			period: '', //期间默认值
			assid: '', //辅助核算默认值
			pk_currtype: '', //币种
			balance: [], //子表展示信息
			querydata: '',
			json:{}
			//apport:{refname:'',refpk:''}//分摊参照默认值
		};
	}

	componentWillMount() {
		let callback= (json) =>{
			this.setState({json:json},()=>{
				//initTemplate.call(this, this.props, this.state);
			})
		}
		getMultiLang({moduleId:'20021005card',domainName:'gl',currentLocale:'zh-CN',callback});
	}

	componentWillReceiveProps(nextProps) {
		// if (nextProps.show) {
		// 	this.apportionQuery(nextProps.querydata);
		// }
	}


	componentDidMount() {
		let { pk_accasoa, period, assid, pk_currtype } = this.props.sendData;
		if (pk_accasoa != this.state.pk_accasoa) {
			this.setState(
				{
					pk_accasoa: pk_accasoa,
					period: period,
					assid: assid,
					pk_currtype: pk_currtype,
					querydata: this.props.querydata
				},
				() => {
					let { querydata } = this.state;
					if (querydata) {
						let cutQueryData=[];
						cutData(cutQueryData,querydata.voucher.details)
						querydata.voucher.details=cutQueryData;
						this.apportionQuery(querydata);
					}
				}
			);
		}
	}
	//点击保存,修改值传至父组件
	modalMessage = () => {
		let self = this;
		self.setState({
			checkFormNow: true
		});
	};

	assureFormCallback = (isCheck, values, others) => {
		let self = this;
		let { loadItem } = self.state;
		values.map((item, i) => {
			loadItem[0][item.name].value = item.value;
		});
		self.props.onConfirm(loadItem);
		self.setState({
			checkFormNow: false
		});
	};

	apportionQuery = (apport) => {
		let self = this;
		let url = '/nccloud/gl/voucher/linkbal.do';
		ajax({
			url,
			data: apport,
			success: function(res) {
				let { balance, period } = res.data;
				if (balance && period) {
					self.setState({
						balance: balance,
						period: period
					});
				}
				//this.updataView()
			}
		});
	};
	loadData = (amountValue) => {
		let { pk_accasoa, pk_currtype, period, assid } = this.state;
		return (
			<ul className="info-lines">
				<li>
					<span className="nc-theme-common-font-c">{this.state.json['20021005card-000124']}：</span>
					<span className="nc-theme-title-font-c">{pk_accasoa}</span>
				</li>
				<li>
					<span className="nc-theme-common-font-c">{this.state.json['20021005card-000020']}：</span>
					<span className="nc-theme-title-font-c">{pk_currtype}</span>
				</li>
				<li>
					<span className="nc-theme-common-font-c">{this.state.json['20021005card-000125']}：</span>
					<span className="nc-theme-title-font-c">{period}</span>
				</li>
				<br />
				<li>
					<span className="nc-theme-common-font-c">{this.state.json['20021005card-000023']}：</span>
					<Tooltip inverse placement="bottom" overlay={<div>{assid}</div>}>
					<span className="assid_voucher nc-theme-title-font-c">{assid}</span>
					</Tooltip>
				</li>
			</ul>
		);
	};

	render() {
		let self = this;
		let {
			show,
			title,
			content,
			closeButton,
			icon,
			isButtonWhite,
			isButtonShow,appCount
		} = this.props;
		const { balance } = self.state;
		const columns = [
			{ title: this.state.json['20021005card-000123'], dataIndex: 'dataType', key: 'dataType', width: 150 },/* 国际化处理： 数据类型*/
			{ title: this.state.json['20021005card-000077'], dataIndex: 'quantity', key: 'quantity', width: 150 },/* 国际化处理： 数量*/
			{ title: this.state.json['20021005card-000006'], dataIndex: 'amount', key: 'amount', width: 150 },/* 国际化处理： 原币*/
			{ title: this.state.json['20021005card-000027'], dataIndex: 'localamount', key: 'localamount', width: 150 }/* 国际化处理： 本币*/
		];
		return (
			<Modal className="msg-modal simpleModal combine" 
				show={show}
				onHide={() => this.props.onCancel(true) }
                animation={true}
			>
				<Modal.Header closeButton>
					<Modal.Title>{title}</Modal.Title>
					{/* <div className="icon-wraper">
						<i
							className="iconfont icon-shuaxin1"
							onClick={() =>
								this.props.onConfirm({
									apportionAmount: appCount,
									pk_apprule: apport.refpk
								})}
						/>
						<i className="iconfont icon-guanbi" onClick={() => this.props.onCancel(true)} />
					</div> */}
				</Modal.Header>
				<Modal.Body>
					{self.loadData()}
					<div className="modal-table-wraper">
						<Table bordered columns={columns} data={balance} />
					</div>
				</Modal.Body>
				{isButtonShow && (
					<Modal.Footer>
						{/* <Button
							onClick={() =>
								this.props.onConfirm({
									apportionAmount: appCount,
									pk_apprule: apport.refpk
								})}
						>
							{confirmText}
						</Button> */}
						{/* <Button
							className="button-primary"
							onClick={() => {
								this.props.onCancel(false);
							}}
						>
							{cancelText}
						</Button> */}
					</Modal.Footer>
				)}
			</Modal>
		);
	}
}
