import React, { Component } from 'react';
import Welcome from '../../container/Welcome';
import { createPage, ajax, base, toast, cacheTools,getMultiLang } from 'nc-lightapp-front';

let tableId = 'gl_voucher';
let pageId = '20021005generate';
let searchId = 'query';
export default class GenerateVoucher extends Component {
    constructor(props) {
        super(props);
        this.state={
            datavoucher:{},
            json:{}
        }
    }

    componentWillMount() {
		let callback= (json) =>{
			this.setState({json:json},()=>{
				//initTemplate.call(this, this.props);
				// this.loadDept();
			})
		}
		getMultiLang({moduleId:'20021005card',domainName:'gl',currentLocale:'zh-CN',callback});
	}



    componentDidMount() {
        this.initTemplate(this.props);
        //取会计平台传参
        let checkedData=cacheTools.get('checkedData');
        let self=this;
        let url='/nccloud/gl/voucher/generateVoucher.do'
        ajax({
            url,
            data:checkedData,
            // data:[
            //    {pk_operatinglog:  "1001ZZ1000000000H8HW", groupid: "", rowId: "421040.68760b5bb200779"},
            //    {pk_operatinglog:  "1001Z31000000000IBUG", groupid: "", rowId: "421040.58b4372a8b957b2"}
            // ],
            async:true,
			success:function(response){
                if (response && response.data) {
                    self.props.transferTable.setTransferListValue(
                        "gl_voucher",
                        response.data
                    );
                }               
            }
        });
    }

    //左侧列表页面加载模板
    initTemplate(props) {
        //取应用id
        // let getParm = (parm) => {
        //     let appUrl = decodeURIComponent(window.location.href).split('?');
        //     if (appUrl && appUrl[1]) {
        //         let appPrams = appUrl[1].split('&');
        //         if (appPrams && appPrams instanceof Array) {
        //             let parmObj = {};
        //             appPrams.forEach(item => {
        //                 let key = item.split('=')[0];
        //                 let value = item.split('=')[1];
        //                 parmObj[key] = value;
        //             })
        //             return parmObj[parm];
        //         }
        //     }
        // }
        // let appid = getParm('ar') || '';
        props.createUIDom(
            {
                pagecode: '20021005generate', //页面id
                appid: '0001Z310000020021005' //注册按钮的id
            },
            function(data) {
                if (data) {
                    if (data.template) {
                        let meta = data.template;
                        props.meta.setMeta(meta);
                    }                   
                }
            }
        );
    }

    render() {
        let { datavoucher }=this.state;
        let { form, cardTable, button, transferTable, modal } = this.props;
        const { createTransferList } = transferTable;
        let { createButton } = button;
        
        return (
            <div className="nc-bill-transferList" style={{ backgroundColor: '#fff' }}>
                <div className='nc-bill-header-area'>
                    <div className='header-title-search-area'>
                        <h2 className='title-search-detail'>{this.state.json['20021005card-000115']/* 国际化处理： 凭证*/}</h2>
                    </div>
                    <div className='header-button-area'>
                        {createButton('return', {
                            name: this.state.json['20021005card-000295'],/* 国际化处理： 返回*/
                            onButtonClick: () => {
                                history.go(-1);
                            },
                        })}
                    </div> 
                </div>
                {
                    <div className='nc-bill-transferList-content'>
                        {
                           // 左侧列表页面
                            createTransferList({
                                headcode:'gl_voucher',
                                transferListId: 'gl_voucher',
                                onTransferItemSelected: (record, status) => { //转单缩略图被选中时的钩子函数 status:转单处理状态
                                   this.selectedLeftItem(record, 0, status);                               
                                },
                                onTransferItemClick: (record, index, status) => { //点击转单缩略图的钩子函数 status:转单处理状态
                                   this.selectedLeftItem(record, index, status);
                                }
                            })
                        }
                        {
                            //右侧卡片页面
                            /* <div className='nc-bill-card transferList-content-right'>
                            <div className='nc-bill-form-area'>
                                {createForm(this.transformFormId)}
                            </div>
                            </div> */
                            <Welcome  datavoucher={datavoucher}  />
                        }
                    </div>
                }
            </div>
        );

    }

    //选择左侧列表后，请求卡片页面需要的数据
    selectedLeftItem(record, index, status){
        let self=this;
        let { datavoucher }=self.state;
        let voucher={}
        voucher=record.head['gl_voucher'].rows[0].values;
        voucher.details=[];
        let bodydetail=record.body['gl_voucher_detail'].rows;
        for(let i=0;i<bodydetail.length;i++){
            voucher.details.push(bodydetail[i].values);
        }
      
        let paraInfo={};

        let url='/nccloud/gl/voucher/queryBookCombineInfo.do'
        ajax({
            url,
            data:{
                pk_accountingbook:voucher['pk_accountingbook'].value,
                date:voucher['prepareddate'].value
            },
           // async:true,
			success:function(response){
                if (response && response.data) {
                    let data={};
                    paraInfo=response.data;
                    data.voucher=voucher;
                    data.paraInfo=paraInfo;
                    datavoucher=data;
                    self.setState({
                        datavoucher
                    })
                }               
            }
        });
        //给卡片页面赋值
        //TODO
    }
}

GenerateVoucher = createPage({
	// initTemplate: initTemplate,
	// mutiLangCode: '2002'
})(GenerateVoucher);

ReactDOM.render(<GenerateVoucher />, document.querySelector('#app'));
