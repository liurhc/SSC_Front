import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax,getMultiLang ,createPageIcon} from 'nc-lightapp-front';
import { buttonClick, initTemplate, searchBtnClick, pageInfoClick, tableModelConfirm } from './events';
import { adjustuserItem, adjustuserComfirm } from './modal/adjustuser.js';
import { adjustItem, adjustComfirm } from './modal/adjust.js';
import HeaderArea from '../../../public/components/HeaderArea';
import './index.less';
/**
 * 凭证整理
 */
class VoucherAdjustList extends Component {
	constructor(props) {
		super(props);
		this.moduleId = '2002';
		this.searchId = '2002adjustquery';
		this.tableId = 'gl_voucher';
		this.state = {
			user_accountingbook: '',
			adjust_accountingbook: '',
			adjust_year: '',
			adjust_period: '',
			adjust_vouchertype: '',
			adjust_ordertype: '1',
			adjust_beginNo: '',
			adjust_endNo: '',
			json: {},
			inlt: null
		};
	}

	componentWillMount() {
		let callback = (json, status, inlt) => {
			if (status) {
				initTemplate.call(this, this.props, json, inlt);
				// setDefData('mutilangJson_list', mutilangJson_list, json);
				// setDefData('mutilangInlt_list', mutilangJson_list, inlt);
				this.setState({ json, inlt })
			}
		}
		getMultiLang({ moduleId: '20020ADJST', domainName: 'gl', callback });
	}

	closeBtnClick = () => {
		this.props.modal.close('adjust');
	}
	render() {
		let { table, search, modal } = this.props;
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { createModal } = modal;
		return (
			<div className="nc-bill-list">
				<HeaderArea
					title={this.state.json['20020ADJST-000007']}
					btnContent={
						this.props.button.createButtonApp({
							area: 'gl_voucher',
							buttonLimit: 3,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
				/>
				{/* <div className="nc-bill-header-area">
					<div className="header-title-search-area">
						{createPageIcon()}
						<h2 className="title-search-detail">{this.state.json['20020ADJST-000007']}</h2>
					</div>
					<div className="header-button-area">
						{this.props.button.createButtonApp({
							area: 'gl_voucher',
							buttonLimit: 3,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>  */}
				<div className="nc-bill-search-area">
					{NCCreateSearch(this.searchId, {
						clickSearchBtn: searchBtnClick.bind(this, this.state),
						showAdvBtn: false,
						defaultConditionsNum: 2,
						onAfterEvent: this.onAfterEvent.bind(this)
					})}
				</div>
				<div className="nc-bill-table-area">
					{createSimpleTable(this.tableId, {
						handlePageInfoChange: pageInfoClick,
						tableModelConfirm: tableModelConfirm,
						showIndex: true
					})}
				</div>
				<div>
					{this.state.json['20020ADJST-000003'] ? (
						createModal('adjust', {
							title: this.state.json['20020ADJST-000003'], // 弹框表头信息/* 国际化处理： 整理*/
							content: adjustItem(this), //弹框内容，可以是字符串或dom
							beSureBtnClick: adjustComfirm.bind(this), //点击确定按钮事件
							cancelBtnClick: this.closeBtnClick.bind(this), //取消按钮事件回调
							userControl: true, // 点确定按钮后，是否自动取消弹出框.true:手动关。false:自动关
							className: 'senior', //  模态框大小 sm/lg/xlg
							rightBtnName: this.state.json['20020ADJST-000004'], //左侧按钮名称,默认取消/* 国际化处理： 取消*/
							leftBtnName: this.state.json['20020ADJST-000005'] //右侧按钮名称， 默认确认/* 国际化处理： 确定*/
						})) : null}
				</div>
				<div>
					{this.state.json['20020ADJST-000006'] ? (
						createModal('adjustuser', {
							title: this.state.json['20020ADJST-000006'], // 弹框表头信息/* 国际化处理： 凭证操作员参照更新*/
							content: adjustuserItem(this), //弹框内容，可以是字符串或dom
							beSureBtnClick: adjustuserComfirm.bind(this), //点击确定按钮事件
							userControl: false, // 点确定按钮后，是否自动取消弹出框.true:手动关。false:自动关
							className: 'adjustuser-modal', //  模态框大小 sm/lg/xlg
							rightBtnName: this.state.json['20020ADJST-000004'], //左侧按钮名称,默认取消/* 国际化处理： 取消*/
							leftBtnName: this.state.json['20020ADJST-000005'] //右侧按钮名称， 默认确认/* 国际化处理： 确定*/
						})) : null}
				</div>
			</div>
		);
	}

	/* 编辑后事件 */
	onAfterEvent(field, val) {
		if (field == 'pk_accountingbook') {
			if (val.refpk && val.refpk != '') {
				ajax({
					url: '/nccloud/gl/voucher/queryBookCombineInfo.do',
					data: {
						pk_accountingbook: val.refpk
					},
					success: (res) => {
						let { success, data } = res;
						if (success) {
							let period = data.bizPeriod;
							this.props.search.setSearchValByField(this.searchId, 'period', {
								display: period,
								value: period
							});

							let meta = this.props.meta.getMeta();
							let items = meta[this.searchId].items;
							for (let i = 0; i < items.length; i++) {
								if (items[i].attrcode == 'period') {
									items[i].queryCondition = () => {
										return {
											pk_accperiodscheme: data.pk_accperiodscheme,
											GridRefActionExt: 'nccloud.web.gl.ref.FilterAdjustPeriodRefSqlBuilder'
										};
									};
								}
								if (items[i].attrcode == 'pk_vouchertype') {
									items[i].queryCondition = () => {
										return {
											GridRefActionExt: 'nccloud.web.gl.ref.VouTypeRefSqlBuilder',
											pk_org: val.refpk,
											isDataPowerEnable: 'Y',
											DataPowerOperationCode: 'fi'
										};
									};
								}
							}
							this.props.meta.setMeta(meta);
						}
					}
				});
			}
		}
	}
}

VoucherAdjustList = createPage({
	//initTemplate: initTemplate,
	//mutiLangCode: '2002'
})(VoucherAdjustList);

ReactDOM.render(<VoucherAdjustList />, document.querySelector('#app'));
