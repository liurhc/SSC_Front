import { high, base, ajax, toast } from 'nc-lightapp-front';
const { NCRow: Row, NCCol: Col, NCInput, NCRadio, NCTextArea,NCNumber } = base;

/**
 * 整理
 */
function adjustItem(_this) {
	return (
		<div className="nc-theme-form-label-c">
		<div className="modal-form-item ">
			<Row>
				<Col xs={3} md={3} sm={3}>
					<span className="item-title">{_this.state.json['20020ADJST-000010']/* 国际化处理： 凭证号生成规则*/}</span>
				</Col>
				<Col xs={4} md={4} sm={4}>
					{/* <div className="item-content"> */}
						<NCNumber
							scale={0}
							// min={1}
							value={_this.state.adjust_beginNo}
							onChange={(e) => {
								_this.setState({
									adjust_beginNo: e
								});
							}}
						/>
						</Col>
						<Col xs={1} md={1} sm={1}>
						<span className="to">~</span>
						</Col>
						<Col xs={4} md={4} sm={4}>
						<NCNumber
							scale={0}
							// min={1}
							value={_this.state.adjust_endNo}
							onChange={(e) => {
								_this.setState({
									adjust_endNo: e
								});
							}}
						/>
					{/* </div> */}
				</Col>
			</Row>
		</div>
		<div className="modal-form-item">
			<span className="item-title">{_this.state.json['20020ADJST-000011']/* 国际化处理： 编号方式*/}</span>
			<NCRadio.NCRadioGroup
				name="ordertype"
				selectedValue={_this.state.adjust_ordertype}
				onChange={(v) => {
					_this.setState({
						adjust_ordertype: v
					});
				}}
			>
				<NCRadio value="1">{_this.state.json['20020ADJST-000012']/* 国际化处理： 按现有顺序重新编号*/}</NCRadio>
				<NCRadio value="2">{_this.state.json['20020ADJST-000013']/* 国际化处理： 按日期顺序重新编号*/}</NCRadio>
			</NCRadio.NCRadioGroup>
		</div>
		<p className="tip-box">
			<i className="iconfont icon-tixing" />
			<div className="tip">
				{_this.state.json['20020ADJST-000014']}<br />{_this.state.json['20020ADJST-000018']/* 国际化处理： 说明,本功能用于清理凭证断号,错号,并将重新排列凭证号,注意,本功能是不可逆操作,调整后的数据将无法恢复,请慎用*/}
			</div>
		</p>
		</div>
	);
}

function adjustComfirm() {
	if (
		this.state.adjust_accountingbook == '' ||
		this.state.adjust_year == '' ||
		this.state.adjust_period == '' ||
		this.state.adjust_vouchertype == ''
	) {
		return;
	}
	if((this.state.adjust_beginNo&&this.state.adjust_beginNo!=''&&this.state.adjust_beginNo<1)
		||(this.state.adjust_endNo&&this.state.adjust_endNo!=''&&this.state.adjust_endNo<1)){
		toast({content:this.state.json['20020ADJST-000008'],color:'warning'});/* 国际化处理： 请输入大于0的正整数*/
		return false;
	}
	let data = {
		pk_accountingbook: this.state.adjust_accountingbook, //取查询条件上的财务核算账簿
		year: this.state.adjust_year, //查询条件上的年
		period: this.state.adjust_period, //查询条件上的月
		pk_vouchertype: this.state.adjust_vouchertype, //查询条件上的凭证类型
		ordertype: this.state.adjust_ordertype, //1=按现有顺序重新编号； 2=按日期顺序重新编号
		beginNo: this.state.adjust_beginNo,
		endNo: this.state.adjust_endNo
	};
	ajax({
		url: '/nccloud/gl/voucher/adjust.do',
		data: data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					this.props.table.setAllTableData('gl_voucher', data['gl_voucher']);
				} else {
					this.props.table.setAllTableData('gl_voucher', { rows: [] });
				}
				this.props.modal.close('adjust');
				toast({ content: this.state.json['20020ADJST-000009'], color: 'success', position: 'bottom' });/* 国际化处理： 整理成功！*/
			}
		}
	});
}

export { adjustItem, adjustComfirm };
