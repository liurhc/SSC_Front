import { ajax, toast ,promptBox} from 'nc-lightapp-front';
import './adjustuser.less';
import createScript from '../../../../public/components/uapRefer.js';
/**
 * 凭证操作员参照更新
 */
function adjustuserItem(_this) {
	let mybook;
	let referUrl = 'uapbd/refer/org/AccountBookTreeRef' + '/index.js';
	let appcode = _this.props.getUrlParam('c')||_this.props.getSearchParam('c');
	if (!_this.state['adjustuser']) {
		{
			createScript.call(_this, referUrl, 'adjustuser');
		}
	} else {
		mybook = _this.state['adjustuser'] ? (
			<div className="line-item">
				<div className="item-title">{_this.state.json['20020ADJST-000024']/* 国际化处理： 财务核算账簿*/}</div>
				<div className="item-content">
					{_this.state['adjustuser']({
						value: {
							refname: _this.state.user_accountingbook.refname,
							refpk: _this.state.user_accountingbook.refpk
						},
						isMultiSelectedEnabled: false,
						disabledDataShow : true,
						onChange: (v) => {
							_this.setState({
								user_accountingbook: v
							});
						},
						queryCondition: () => {
							return {
								TreeRefActionExt: 'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
								appcode: appcode
							};
						}
					})}
				</div>
			</div>
		) : (
			<div />
		);
	}
	return <div className="adjustuser">{mybook}</div>;
}

function adjustuserComfirm() {
	promptBox({
		color: "warning",
		content: this.state.json['20020ADJST-000022'],/* 国际化处理： 凭证操作员的整理，调整后凭证查询的制单人、审核人、出纳人、记账人参照将变为在系统出现过（即做过操作的操作员）的参照。*/
		beSureBtnClick: adjustuser.bind(this),
	})
	
}

function adjustuser(){
	ajax({
		url: '/nccloud/gl/voucher/adjustuser.do',
		data: { pk_accountingbook: this.state.user_accountingbook.refpk },
		success: (res) => {
			let { success } = res;
			if (success) {
				toast({ content: this.state.json['20020ADJST-000023'], color: 'success', position: 'bottom' });/* 国际化处理： 整理操作员成功！*/
			}
		}
	});
}

export { adjustuserItem, adjustuserComfirm };
