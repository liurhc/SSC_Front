import {ajax,toast} from 'nc-lightapp-front';

//点击查询，获取查询区数据
let tableid="gl_voucher";
export default function clickSearchBtn(state,props,searchVal) {
    if(searchVal&&searchVal.conditions&&searchVal.conditions.length>0){
        let searchValData=searchVal.conditions;
        let condtion={};
        for(let i=0;i<searchValData.length;i++){
            if("pk_accountingbook"==searchValData[i].field){
                condtion.pk_accountingbook=searchValData[i].value.firstvalue;
            }else if("pk_vouchertype"==searchValData[i].field){
                condtion.pk_vouchertype=searchValData[i].value.firstvalue;
            }else if("period"==searchValData[i].field){
                //会计年、会计月
                let yearmonth=searchValData[i].display;
                condtion.year=yearmonth.split('-')[0];
                condtion.period=yearmonth.split('-')[1];
            }
        }

        ajax({
            url: '/nccloud/gl/voucher/adjustquery.do',
            data: condtion,
            success: (res) => {
                let { success, data } = res;
                if (success) {
                    if(data){
                        let message=this.state.inlt&&this.state.inlt.get('20020ADJST-000000', {number: data[tableid].rows.length});
                        toast({ color: 'success', content: message});/* 国际化处理： 查询成功，共{number}条*/
                        this.props.table.setAllTableData(tableid, data[tableid]);
                        this.props.button.setButtonDisabled(['adjust','print'],false);
                    }else{
                        toast({ color: 'warning', content: this.state.json['20020ADJST-000002'] });/* 国际化处理： 未查询出符合条件的数据*/
                        this.props.table.setAllTableData(tableid, {rows:[]});
                        this.props.button.setButtonDisabled(['adjust','print'],true);
                    }
                    
                    this.setState({
                        adjust_accountingbook: condtion.pk_accountingbook,
                        adjust_year: condtion.year,
                        adjust_period: condtion.period,
                        adjust_vouchertype: condtion.pk_vouchertype
                    })                    
                }
            }
        });
    }
}