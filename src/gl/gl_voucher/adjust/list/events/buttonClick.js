import { print } from 'nc-lightapp-front';

let tableid = 'gl_voucher';
export default function buttonClick(props, id) {
    switch (id) {
        case 'adjust':
            //整理
            props.modal.show('adjust');
            break;
        case 'adjustuser':
            //凭证操作员参照更新
            props.modal.show('adjustuser');
            break;
        case 'print':
            let allrows=props.table.getAllTableData(tableid);
            let data=[];
            for(let i=0;i<allrows.rows.length;i++){
                data.push(allrows.rows[i].values);
            }
            print(
                'pdf',  //支持两类: 'html'为模板打印, 'pdf'为pdf打印
                '/nccloud/gl/voucher/adjustprint.do', //后台服务url
                {
                    billtype: 'C0',  //单据类型
                    appcode: '20020ADJST',      //功能节点编码，即模板编码
                    nodekey: '',     //模板节点标识
                    oids: [],    // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
                    userjson: JSON.stringify(data)
                },false
            );
            break;
    }
}