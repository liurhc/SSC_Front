import { createPage, ajax, base, toast,cacheTools } from 'nc-lightapp-front';
let { NCPopconfirm, NCIcon } = base;

let searchId = '2002adjustquery';
let tableId = 'gl_voucher';
let pageId = '2002voucheradjust_list';
export default function (props) {   
    let appcode = props.getUrlParam('c')||props.getSearchParam('c');
    
    props.createUIDom(
		{
			pagecode: pageId,//页面id
			appcode: appcode//小应用id
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta,data.context);
					props.meta.oid=meta[searchId].oid;
					props.meta.setMeta(meta);
					setDefaultValue(props,data.context);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					props.button.setButtonDisabled(['adjust','print'],true);
					//props.button.setButtonVisible('edit', false);
				}
			}
		}
	)
}

function modifierMeta(props, meta,context) {
	let appcode = props.getUrlParam('c')||props.getSearchParam('c');
    meta[searchId].items = meta[searchId].items.map((item, key) => {
		item.visible = true;
		item.col = '3';
		if (item.attrcode == 'pk_accountingbook') {
			//item.initialvalue={};
			item.disabledDataShow = false,
			item.queryCondition = () => {
				return {
					"TreeRefActionExt": 'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
					"appcode": appcode
				}
			}
		}
		if (item.attrcode == 'pk_vouchertype') {
			//设置凭证类别过滤
			item.queryCondition = () => {
				return {
					pk_org: context.defaultAccbookPk,
					isDataPowerEnable: 'Y',
					DataPowerOperationCode: 'fi'
				};
			};
		}
		return item;
	})

    return meta;
}

function setDefaultValue(props, context) {	
	let oldaccountingbook = props.search.getSearchValByField(searchId, 'pk_accountingbook');
	if (context.defaultAccbookPk) {
		if (!oldaccountingbook || !oldaccountingbook.value.firstvalue || oldaccountingbook.value.firstvalue == '') {
			let pk_accountingbook = {
				display: context.defaultAccbookName,
				value: context.defaultAccbookPk
			}
			props.search.setSearchValByField(searchId, 'pk_accountingbook', pk_accountingbook);

			//设置默认会计期间			
			ajax({
				url: '/nccloud/gl/voucher/queryBookCombineInfo.do',
				data: {
					pk_accountingbook:pk_accountingbook.value
				},
				success: (res) => {
					let { success, data } = res;
					if (success) {
						let period = data.bizPeriod
						props.search.setSearchValByField(searchId, 'period', { display: period, value: period });

						let meta = props.meta.getMeta();
						let items = meta[searchId].items;
						for (let i = 0; i < items.length; i++) {
							if (items[i].attrcode == 'period') {
								items[i].queryCondition = () => {
									return {
										"pk_accperiodscheme": data.pk_accperiodscheme,
										"GridRefActionExt": 'nccloud.web.gl.ref.FilterAdjustPeriodRefSqlBuilder'
									}
								}
							}
							if(items[i].attrcode == 'pk_vouchertype'){
								items[i].queryCondition = () => {                       
								   return {
									   "GridRefActionExt": 'nccloud.web.gl.ref.VouTypeRefSqlBuilder',
									   pk_org:pk_accountingbook.value,
									   isDataPowerEnable: 'Y',
									   DataPowerOperationCode : 'fi'
								   }
							   }
						   }
						}
					}
				}
			});
		}
	}
}
