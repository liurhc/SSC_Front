import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';
// import moment from 'moment';

import {high,base,ajax,deepClone, createPage, getMultiLang } from 'nc-lightapp-front';
// import './index.less';//加上后弹框就跑到右下角了


const { Refer } = high;

const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCModal: Modal,
} = base;

import { toast } from '../../../../public/components/utils';

const NCOption = Select.NCOption;
// const deepClone = require('../../../../public/components/deepClone');
const format = 'YYYY-MM-DD';
import createScript from '../../../../public/components/uapRefer.js';
import {
    businessUnit,
    createReferFn,
    getCheckContent, getReferAppCode,
    getReferDetault, returnMoneyType
} from "../../../../manageReport/common/modules/modules";
import DateGroup from "../../../../manageReport/common/modules/dateGroup";
import BusinessUnitTreeRef from "../../../../../uapbd/refer/orgv/BusinessUnitVersionDefaultAllTreeRef";
// import AccountBookByFinanceOrgRef from '../../../../../uapbd/refer/org/AccountBookByFinanceOrgRef';



class CentralConstructorModal extends Component {

	constructor(props) {
		super(props);
		this.state={
		    json: {},
            isqueryuntallyed: true,
            mainAttachedDisabled: false,//主附表的是否禁止
			accountingbook: {
				refname: '',
				refpk: '',
			},  //核算账簿
			selectedQueryValue: '0', //查询方式单选
			selectedCurrValue: '1', //返回币种单选
			isGlobal: false, //是否启用全局本币
			isGroup: false, //是否启用集团本币
			ismain: '0', // 0主表  1附表
			begindate: '', //开始时间
			enddate: '', //结束时间
			currtype: '',      //选择的币种
			currtypeName: '',  //选择的币种名称
			currtypeList: [],  //币种列表
			defaultCurrtype: {         //默认币种
				name: '',
				pk_currtype: '',
			},
			checkbox: {
				one: false,
				two: false,
				three: false,
			},
            searchStyle: true, //所有
            rangeDate: [],
			isShowUnit:false,//是否显示业务单元
		}
	
	}

	static defaultProps = {
		show: false,
		title: '',
		content: '',
		closeButton: false,
		icon: 'icon-tishianniuchenggong',
		isButtonWhite: true,
		isButtonShow: true,
		// accAssItems: [],
	};
    componentWillMount() {
        let callback= (json) =>{
            this.setState({
				json:json,
                currtype: json['20021CFQRY-000002'],/* 国际化处理： 本币*/
			},()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20021CFQRY', 'dategroup', 'childmodules'],domainName:'gl',currentLocale:'simpchn',callback});
        this.getCurrtypeList();
    }

	componentDidMount() {
        getReferAppCode(this, 'appcode');
        getReferDetault(this, true, {
            businessUnit,
            getBackData:this.getBackData
        }, 'CashQuery')


	}

	getBackData = () => {
        let urlParam = this.props.getUrlParam('paramObj');
        //('urlParam:::', urlParam)
        if(urlParam){
            let resultObj = JSON.parse(urlParam);
            let newObj = {
                checkbox:{},
            };
            newObj.accountingbook = resultObj.accountbookObj;
            newObj.rangeDate = resultObj.rangeDate;
            newObj.currtype = resultObj.pk_currtype;
            newObj.checkbox.one = resultObj.includetally === "Y" ? true : false;
            newObj.checkbox.two = resultObj.includesum === "Y" ? true : false;
            newObj.checkbox.three = resultObj.includestop === "Y" ? true : false;
            newObj.ismain = resultObj.ismain;
            newObj.selectedCurrValue = resultObj.returncurr;
            newObj.selectedQueryValue = resultObj.querytype;
            newObj.currtypeName = resultObj.currtypeName;
            newObj.isShowUnit = resultObj.isShowUnit;
            newObj.buSecond = resultObj.buSecond

            this.setState(Object.assign({}, this.state, newObj))
        }
	}

	componentWillReceiveProps(nextProps) {
		// this.getEndAccount(nextProps);
	}

	handleRadioChange = (value) => {
		//('value:::::', value);
		this.setState({selectedQueryValue: value});
		if(value === '1' || value === '2'){
			this.setState({
                mainAttachedDisabled: true,
                currtype: this.state.json['20021CFQRY-000002'],/* 国际化处理： 本币*/
                ismain: '0'
			})
		}else{
            this.setState({
                mainAttachedDisabled: false
            })
		}
	}

	handleRadioCurrChange(value) {
		this.setState({selectedCurrValue: value});
	}

	getInfoByBook() {

		if (!this.state.accountingbook || !this.state.accountingbook.refpk) {
			return;
		}
		
		let accountingbook = {
			refname: this.state.accountingbook.refname,
			refpk: this.state.accountingbook.refpk,
		};

		let self = this;
		let url = '/nccloud/gl/voucher/queryAccountingBook.do';
		let data = {
			pk_accountingbook: this.state.accountingbook.refpk,	
			year: '',		
		};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		    	const { data, error, success } = res;

		        if(success){

		        	let isGlobal = false;   //是否启用全局本币
		        	let isGroup = false;    //是否启用集团本币
		        	if (data.globalCurrType && data.globalCurrType.pk_currtype) {
		        		isGlobal = true;
		        	}

		        	if (data.groupCurrType && data.groupCurrType.pk_currtype) {
		        		isGroup = true;
		        	}

		        	self.setState({
		        		isGlobal,
		        		isGroup,
		        	}, )
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});

		// this.getYears()

	}




	getCurrtypeList = () => {
		let self = this;
		let url = '/nccloud/gl/voucher/queryAllCurrtype.do';
		// let data = '';
		let data = {
			"localType":"1",
		};


		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		        //(res.data);
		        const { data, error, success } = res;
		        if(success){
					self.setState({
						currtypeList: data,
						currtype: self.state.json['20021CFQRY-000002'],/* 国际化处理： 本币*/
					})

		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});

	}


	handleCurrtypeChange(value) {
		let self = this;
		// if (!this.state.accountingbook || !this.state.accountingbook.refpk) {
		// 	toast({ content: '请先选择核算账簿', color: 'warning' });
		// 	return;
		// }
		//('benbi:::', value);
		let currtypeList = this.state.currtypeList;
		currtypeList.forEach(function (item, index) {
			if (item.pk_currtype == value ) {
				self.setState({
					currtypeName: item.name,
				})
			}
		})
		//(currtypeList)

		this.setState({
			currtype: value,
		// }, this.getTables);
		}, this.getRates);
	}


	handleIsmainChange(value) {
		this.setState({
			ismain: value,
		})
	}


	changeCheck=()=> {
		// this.setState({checked:!this.state.checked});
	}

	onChange1(e) {
	    //(e);
	    let checkbox = deepClone(this.state.checkbox);
	    checkbox.one = e;
	    this.setState({checkbox});
	}

	onChange2(e) {
	    //(e);
	    let checkbox = deepClone(this.state.checkbox);
	    checkbox.two = e;
	    this.setState({checkbox});
	}

	onChange3(e) {
	    //(e);
	    let checkbox = deepClone(this.state.checkbox);
	    checkbox.three = e;
	    this.setState({checkbox});
	}
	
	//起始时间变化
	onStartChange(d) {
		//(d)
		this.setState({
			begindate:d
		})
	}
	
	//结束时间变化
	onEndChange(d) {
		this.setState({
			enddate:d
		})
	}
    handleDateChange = (value) => {//日期: 范围选择触发事件
        this.setState({
            begindate: value[0],
            enddate: value[1],
            rangeDate: value
        })
    }

	render() {
		let { show, title}= this.props;
		return  <Modal
			fieldid='query'
            show={ show }
            onHide={()=>this.props.onCancel(true)}
            className='cashQuery'
        >
            <Modal.Header closeButton>
                <Modal.Title>
                    {title}
                </Modal.Title>
            </Modal.Header >
			<Modal.Body id="modalOuter">
                <div className='right_query noserchmatter'>
                    <div className='query_body1'>
                        <div className='query_form'>
				<Row className="myrow">
					<Col md={2} sm={2}>
                        <span style={{color: 'red'}}>*</span>
						<span className="nc-theme-form-label-c">{this.state.json['20021CFQRY-000003']}：</span>{/* 国际化处理： 财务核算账簿*/}
					</Col>
					<Col md={10} sm={10}>
						<div className="book-ref">
                            {
                                createReferFn(
                                    this,
                                    {
                                        url: 'uapbd/refer/org/AccountBookTreeRef/index.js',
                                        value: this.state.accountingbook,
                                        referStateKey: 'checkAccountBook',
                                        referStateValue: this.state.checkAccountBook,
                                        stateValueKey: 'accountingbook',
                                        flag: true,
                                        showGroup: true,
                                        queryCondition: {
                                            pk_accountingbook: this.state.accountingbook && this.state.accountingbook.refpk,
                                            multiGroup: 'Y',
                                            "appcode": this.props.getSearchParam('c')
                                        }
                                    },
                                    {
                                        businessUnit: businessUnit
                                    },
									'CashQuery'
                                )
                            }
						</div>



					</Col>
				</Row>
                {
                    this.state.isShowUnit === 'Y' ?
                        <Row className="myrow">
                            <Col md={2} sm={2}>
                                <span className="nc-theme-form-label-c">{this.state.json['20021CFQRY-000004']}：</span>{/* 国际化处理： 业务单元*/}
                            </Col>
                            <Col md={10} sm={10}>
                                <div className="book-ref">
                                    <BusinessUnitTreeRef
										fieldid='buSecond'
                                        value={this.state.buSecond}
                                        isMultiSelectedEnabled= {true}
                                        isShowDisabledData = {true}
                                        queryCondition = {{
                                            "pk_accountingbook": this.state.accountingbook[0] && this.state.accountingbook[0].refpk,
                                            "isDataPowerEnable": 'Y',
                                            "DataPowerOperationCode" : 'fi',
                                            "TreeRefActionExt":'nccloud.web.gl.ref.GLBUVersionWithBookRefSqlBuilder'
                                        }}
                                        onChange={(v)=>{
                                            //(v);
                                            this.setState({
                                                buSecond: v,
                                            }, () => {
                                                if(this.state.accountingbook && this.state.accountingbook[0]) {
                                                }
                                            })
                                        }
                                        }
                                    />

                                </div>

                            </Col>
                        </Row> :
                        <div></div>
                }

                {/*会计期间*/}
                <DateGroup
                    selectionState = 'false'
                    start = {this.state.start}
                    end = {this.state.end}
                    enddate = {this.state.enddate}
                    pk_accperiodscheme = {this.state.pk_accperiodscheme}
                    pk_accountingbook = {this.state.accountingbook[0]}
                    rangeDate = {this.state.rangeDate}
                    handleValueChange = {this.handleValueChange}
                    handleDateChange = {this.handleDateChange}
                    self = {this}
                    showRadio = {this.state.showRadio}
                    showPeriod={true}
                    showDate={false}
                />

				<Row className="myrow">
					<Col md={2} sm={2}>
						<span className="nc-theme-form-label-c">{this.state.json['20021CFQRY-000005']}：</span>{/* 国际化处理： 查询方式*/}
					</Col>
					<Col md={9} sm={9}>

						<Radio.NCRadioGroup
						  name="fruit"
						  selectedValue={this.state.selectedQueryValue}
						 
						  onChange={this.handleRadioChange.bind(this)}
						>
						    <Radio value="0" >{this.state.json['20021CFQRY-000006']}</Radio>{/* 国际化处理： 所有现金流量*/}

						    <Radio value="1" disabled={this.state.searchStyle}>{this.state.json['20021CFQRY-000007']}</Radio>{/* 国际化处理： 外部现金流量*/}

						    <Radio value="2" disabled={this.state.searchStyle}>{this.state.json['20021CFQRY-000008']}</Radio>{/* 国际化处理： 内部现金流量*/}
						</Radio.NCRadioGroup>

					</Col>					
				</Row>

				<Row className="myrow">
					<Col md={2} sm={2}>
						<span className="nc-theme-form-label-c">{this.state.json['20021CFQRY-000009']}：</span>{/* 国际化处理： 主附表*/}
					</Col>
					<Col md={10} sm={10}>				
						{/*主附表下拉*/}
						<Col md={5} sm={5}>
							<div className="book-ref">
								<Select
									fieldid='ismain'
									showClear={false}
									value={this.state.ismain}
									disabled={this.state.mainAttachedDisabled}
									// style={{ width: 150, marginRight: 6 }}
									onChange={this.handleIsmainChange.bind(this)}							   	
								>
									<NCOption value={'0'} key={'0'} >{this.state.json['20021CFQRY-000010']}</NCOption>{/* 国际化处理： 主表*/}
									<NCOption value={'1'} key={'1'} >{this.state.json['20021CFQRY-000011']}</NCOption>{/* 国际化处理： 附表*/}

								</Select>

							</div>
						</Col>
						<Col md={2} sm={2}></Col>
						{/*币种下拉*/}
						<Col md={5} sm={5}>
							<div className="book-ref">
								<Select
									fieldid='currtype'
									showClear={false}
									value={this.state.currtype}
									disabled={this.state.mainAttachedDisabled}
									// style={{ width: 150, marginRight: 6 }}
									onChange={this.handleCurrtypeChange.bind(this)}
								>
									{this.state.currtypeList.map((item, index) => {
										return <NCOption value={item.pk_currtype} key={item.pk_currtype} >{item.name}</NCOption>
									} )}

								</Select>

							</div>
						</Col>
					</Col>					
				</Row>

				<Row className="myrow">
					<Col md={2} sm={2}><span className="nc-theme-form-label-c">{this.state.json['20021CFQRY-000012']}：</span></Col>{/* 国际化处理： 显示属性*/}
					<Col md={4} sm={4}>
						<Checkbox 
							colors="dark"
							className="mycheck"
							disabled={!this.state.isqueryuntallyed}
							checked={this.state.checkbox.one}  onChange={this.onChange1.bind(this)}
						>
							{this.state.json['20021CFQRY-000013']} {/* 国际化处理： 包含未记账凭证*/}
						</Checkbox>
					</Col>
					<Col md={3} sm={3}>
						<Checkbox colors="dark" className="mycheck" checked={this.state.checkbox.two}  onChange={this.onChange2.bind(this)}>
							{this.state.json['20021CFQRY-000014']} {/* 国际化处理： 显示累计*/}
						</Checkbox>
					</Col>
					<Col md={3} sm={3}>
						<Checkbox colors="dark" className="mycheck" checked={this.state.checkbox.three}  onChange={this.onChange3.bind(this)} >
							{this.state.json['20021CFQRY-000015']} {/* 国际化处理： 显示停用*/}
						</Checkbox>
					</Col>
						
				</Row>

                {/*返回币种：*/}
                {
                    returnMoneyType(
                        this,
                        {
                            key: 'selectedCurrValue',
                            value: this.state.selectedCurrValue,
                            groupEdit: this.state.groupCurrency,
                            gloableEdit: this.state.globalCurrency
                        },
						this.state.json
                    )
                }
						</div>.
					</div>
				</div>

			</Modal.Body>
			<Modal.Footer>

				<Button
					fieldid='query'
                    className= "button-primary"
					onClick = {() => {
						let pk_accountingbookArr = [];
						let pk_unit = [];
						if (Array.isArray(this.state.accountingbook)) {
							this.state.accountingbook.forEach(function (item) {
								//('itemmm:::', item)
								pk_accountingbookArr.push(item.refpk)
							})
						}
						if(pk_accountingbookArr.length===0){
							toast({
								content: this.state.json['20021CFQRY-000021'],// 国际化处理： 请选择核算账簿
								color: 'warning'
							})
							return;
						}
						if (Array.isArray(this.state.buSecond)) {
							this.state.buSecond.forEach(function (item) {
								pk_unit.push(item.refpk)
							})
						}
						let newdata = {
							"pk_accountingbook": pk_accountingbookArr,
							"begindate": this.state.rangeDate[0],
							"enddate": this.state.rangeDate[1],
							"pk_currtype": this.state.currtype,
							"includetally": (this.state.checkbox.one ? 'Y' : 'N'),
							"includesum": (this.state.checkbox.two ? 'Y' : 'N'),
							"includestop": (this.state.checkbox.three ? 'Y' : 'N'),
							"ismain": this.state.ismain,
							"returncurr": this.state.selectedCurrValue,
							"querytype": this.state.selectedQueryValue,
							'bookname': this.state.accountingbook.refname,
							currtypeName: this.state.currtypeName ? this.state.currtypeName : this.state.json['20021CFQRY-000002'],/* 国际化处理： 本币*/
							/*******业务单元****/
							pk_units: pk_unit,
							accountbookObj: this.state.accountingbook,
                            isShowUnit: this.state.isShowUnit,
                            buSecond: this.state.buSecond,
							rangeDate: this.state.rangeDate,
						}

						this.props.onConfirm(newdata)
					}}
				>
                    {this.state.json['20021CFQRY-000000']}{/* 国际化处理： 查询*/}
				</Button>


				<Button
					fieldid='cancel'
					className= 'btn-2 btn-cancel'
					onClick={() => {this.props.onCancel(false)}}
				>{this.state.json['20021CFQRY-000001']}</Button>{/* 国际化处理： 取消*/}
			</Modal.Footer>
			<Loading fullScreen showBackDrop={true} show={this.state.isLoading} />



		</Modal>;
	}
}

CentralConstructorModal = createPage({})(CentralConstructorModal)
export default CentralConstructorModal
