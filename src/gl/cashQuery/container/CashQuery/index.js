import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';

// import OpenTable from './OpenTable';

import {high,base,ajax,deepClone, createPage, getMultiLang,createPageIcon } from 'nc-lightapp-front';
const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCPopconfirm: Popconfirm,NCMenu: Menu,NCModal: Modal,
    NCAffix,
    NCTooltip:Tooltip,
} = base;

import './index.less';
// import 'bee-select/build/Select.css';
import { toast } from '../../../public/components/utils';
const { Refer } = high;
// import MainOrgRef from '../../../public/components/MainOrgRef.js';

// const deepClone = require('../../../public/components/deepClone');
const { Item, Divider, SubMenu, ItemGroup } = Menu;
import MainSelectModal from './MainSelectModal';
import { SimpleTable } from 'nc-report';
import reportPrint from '../../../public/components/reportPrint';

import {tableDefaultData} from '../../../manageReport/defaultTableData'
//react-demo 13.html
import {searchById} from "../../../manageReport/common/modules/createPureBtn";
import {getDetailPort} from "../../../manageReport/common/modules/modules";
import {
    cashFlowDetailSearch, cashFlowDetailSearchCode,
    cashFlowAnalyTable, cashFlowAnalyTableCode
} from "../../../manageReport/referUrl";
import {businessUnit, createReferFn} from "../../../manageReport/common/modules/modules";
import {whetherDetail} from "../../../manageReport/common/modules/simpleTableClick";
import {rowBackgroundColor} from '../../../manageReport/common/htRowBackground';
import HeaderArea from '../../../public/components/HeaderArea';
class OpenBalance extends Component {
	constructor(props) {
		super(props);
//每定义一个state 都要添加备注
		this.state = {
            json: {},
            inlt:null,
            paramObj: {}, //查询参数
			MainSelectModalShow: false, //查询模态框
			tableAll: {
				column: [],
				data:[],
			},              //表格全体数据
			initTableAll: null,  //初始查询出的表格全体数据
			tableStyle: {}, //表格样式
			querydata: {},
			queryCond: {
				begindate: '',
				enddate: '',
				bookname: '',
				currtypeName: '',

			},
			textAlginArr:[],//对其方式
            dataout: tableDefaultData,
			pk_cashflow: '', //已点击行的pk_cashflow
            showThreBtn: false
		}
		this.searchById = searchById.bind(this);
		this.whetherDetail = whetherDetail.bind(this);
		this.rowBackgroundColor = rowBackgroundColor.bind(this);
	}

    componentWillMount() {
        let callback= (json,status,inlt) =>{
            this.setState({json:json,inlt},()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20021CFQRY', 'childmodules'],domainName:'gl',currentLocale:'simpchn',callback});
    }
	componentDidMount() {
        this.setState({
            dataout: tableDefaultData
        })
	    let urlParam = this.props.getUrlParam('paramObj');
	    //('urlParam:::', urlParam)
	    if(urlParam){
            this.getDatas(JSON.parse(urlParam))
        };
        let appceod = this.props.getSearchParam('c');
        this.searchById('20021CFQRYPAGE',appceod);
        this.getParam()
        let queryData=this.props.getUrlParam('cashError')&&JSON.parse(this.props.getUrlParam('cashError'))
	}

    getParam = () => {
        //('getParam>>>', this.props.getUrlParam);
        let data = this.props.getUrlParam && this.props.getUrlParam('paramObj');
        //('datata:::',data);
        if(!data){//如果data存在，那就是返回到首页的
            this.props.button.setDisabled({ refresh: true, linkerror: true, linkdetail: true, linkcf: true, directprint: true});
        }
    }

	queryShow = () => {
		this.setState({
			MainSelectModalShow: true,
		})
	}

	handleCoords() {
		let info = this.refs.balanceTable.getReportInfo();
		//(info);
		//(info.coords.col);
		//(info.coords.row);
		return info.coords.row;

	}

    checkoutError = () => {
	    let data = {...this.state.queryCond};
        let url = '/nccloud/gl/cashflow/cfcasedetailquery.do';
        data.iserror = 'Y';
        ajax({
            loading: true,
            url,
            data,
            success: (res) => {
                const { data, message, success } = res;
                //('newSuccssss:::', res, this.state.json['20021CFQRY-000016'])
                if(success){
                    if(data){
                        this.goError()
                    }else{
                        toast({ content: this.state.json['20021CFQRY-000016'], color: 'warning' });/* 国际化处理： 没有错误分析的凭证!*/
                    }

                }else {
                    toast({ content: message.message, color: 'warning' });
                }
            },
            error: function (res) {
                toast({ content: res.message, color: 'danger' });

            }
        });
    }

	//错误分析页
	goError = () => {
		// window.location.href = "./welcome.html?id="+key;
		// ?id=1&name=location
		//(' queryCond >>',this.state.queryCond);
		let queryCond = this.state.queryCond;
		if (!queryCond) {
			toast({ content: this.state.json['20021CFQRY-000017'], color: 'warning' });/* 国际化处理： 请先进行查询*/
			return
		}
		let data = deepClone(queryCond);
		//('datatata::', data,this.state.paramObj);
        this.props.openTo('/gl/cashQuery/container/Error/index.html', {
            cashError: JSON.stringify(data),
            param: JSON.stringify(this.state.paramObj)
        })

	}

	//联查明细页
	goDetail = () => {

		let queryCond = this.state.queryCond;
		if (!queryCond) {
			toast({ content: this.state.json['20021CFQRY-000017'], color: 'warning' });/* 国际化处理： 请先进行查询*/
			return
		}
		let data = deepClone(queryCond);
		let index = this.handleCoords();
		//(index)
		if (index < 2) {
			toast({ content: this.state.json['20021CFQRY-000018'], color: 'warning' });/* 国际化处理： 请先选择一数据行*/
			return
		}
//('getDetailPort', this.state.paramObj)
        getDetailPort(this, {
            jumpTo: cashFlowDetailSearch,
            key: 'cashFlowDetailSearch',
            appcode: cashFlowDetailSearchCode,
            pagecode: `${cashFlowDetailSearchCode}PAGE`,
            searchParam: this.state.paramObj
        }, this.state.json)
	}

	//现金流量分析表联查
	goAnalysis = () => {

		let queryCond = this.state.queryCond;
		if (!queryCond) {
			toast({ content: this.state.json['20021CFQRY-000017'], color: 'warning' });/* 国际化处理： 请先进行查询*/
			return
		}
        getDetailPort(this, {
            jumpTo: cashFlowAnalyTable,
            key: 'cashFlowDetailSearch',
            appcode: cashFlowAnalyTableCode,
            searchParam: this.state.paramObj
        }, this.state.json)
		
	}


	getDatas = (datas) => {
		this.setState({
			paramObj: datas
		});
        this.props.button.setDisabled({ refresh: false, linkerror: false, directprint: false, linkdetail:true, linkcf: true});

		if(datas.pk_accountingbook.length >1){
			this.setState({
                showThreBtn: true
			})
		}else{
            this.setState({
                showThreBtn: false
            })
		}
		this.ajaxData(datas)
	}

	/**
     * flag: true 标记是点击刷新按钮
	* */
	ajaxData = (data, flag) => {
		let self = this;
        let queryCond = data;


        let url = '/nccloud/gl/cashflow/cfquery.do';
        ajax({
            loading: true,
            url,
            data,
            success: function (res) {
                //('suuuuu:::', self.state.showThreBtn)
                if(self.state.showThreBtn){
                    //('suuuuu:::1', self.state.showThreBtn)
                    self.props.button.setButtonVisible([ 'linkerror', 'linkdetail', 'linkcf'], false);
                }else{
                    //('suuuuu:::2', self.state.showThreBtn)
                    self.props.button.setButtonVisible([ 'linkerror', 'linkdetail', 'linkcf'], true);
                }
                const { data, message, success } = res;
                if(success){
                    if(flag){
                        toast({title: self.state.json['20021CFQRY-000024'], color: 'success'})
                    }else{
                        // toast({content: self.state.inlt && self.state.inlt.get('20021CFQRY-000025',{length : data.data.length}), color: 'success'})
                        toast({title: self.state.json['20021CFQRY-000028'], color: 'success'})
                    }
                    if (data) {
                        let num = 1;
                        let numArr = [];
                        let rows = [];   //表格显示的数据合集
                        let columHead = []; //第一层表头
                        let parent = []; //存放一级表头，二级表头需要补上
                        let parent1 = []; //存放二级表头，三级表头需要补上
                        let child1 = []; //第三层表头
                        let child2 = []; //第二层表头
                        let colKeys = [];  //列key值，用来匹配表体数据
                        let colAligns=[];  //每列对齐方式
                        let colWidths=[];  //每列宽度
                        let oldWidths=[];
                        let textAlginArr = self.state.textAlginArr.concat([]);      //对其方式
                        data.column.forEach(function (value) {//column:[]原始数据表头信息
                            //('>>>>////', value);
                            let valueCell = {};
                            valueCell.title = value.title;
                            valueCell.key = value.key;
                            valueCell.align = 'center';
                            valueCell.bodyAlign = 'left';
                            valueCell.style = 'head';
                            if(value.children){
                                self.setState({
                                    flow: true
                                })
                                if(parent.length>0){
                                    child1.push(...parent);
                                }
                                for(let i=0;i<value.children.length;i++){
                                    if(value.children[i].children){
                                        child2.push(...parent1);
                                        for(let k=0;k<value.children[i].children.length;k++){
                                            let child1Cell = {};
                                            child1Cell.title = value.children[i].title;
                                            child1Cell.key = value.children[i].key;
                                            child1Cell.align = 'center';
                                            child1Cell.bodyAlign = 'left';
                                            child1Cell.style = 'head';
                                            let child2Cell = {};
                                            child2Cell.title = value.children[i].children[k].title;
                                            child2Cell.key = value.children[i].children[k].key;
                                            child2Cell.align = 'center';
                                            child2Cell.bodyAlign = 'left';
                                            child2Cell.style = 'head';
                                            columHead.push(valueCell);
                                            child1.push(child1Cell);
                                            child2.push(child2Cell);
                                            colKeys.push(value.children[i].children[k].key);
                                            colAligns.push(value.children[i].children[k].align);
                                            colWidths.push(value.children[i].children[k].width);
                                        }
                                        parent1 = [];
                                        //('jjjjjjj:', colWidths)
                                    }else {
                                        let innerCell2 = {};
                                        innerCell2.title = value.children[i].title;
                                        innerCell2.key = value.children[i].key;
                                        innerCell2.align = 'center';
                                        innerCell2.bodyAlign = 'left';
                                        innerCell2.style = 'head';
                                        columHead.push(valueCell);
                                        child1.push(innerCell2);
                                        parent1.push(innerCell2);
                                        colKeys.push(value.children[i].key);
                                        colAligns.push(value.children[i].align);
                                        colWidths.push(value.children[i].width);
                                    }
                                }
                                parent = [];
                            }else{
                                let cellObj = {};
                                cellObj.title = value.title;
                                cellObj.key = value.key;
                                cellObj.align = 'center';
                                cellObj.bodyAlign = 'left';
                                cellObj.style = 'head';
                                columHead.push(valueCell);
                                parent1.push(cellObj);
                                parent.push(cellObj);
                                colKeys.push(value.key);
                                colAligns.push(value.align);
                                colWidths.push(value.width);
                                //('elses::', parent, parent1)
                            }
                        });
                        //('>>>>?????',parent1, parent, child1, child2)
                        let columheadrow = 0; //表头开始行
                        if (data.headtitle){
                            columheadrow = 1;
                            let headtitle = [];
                            data.headtitle.forEach(function (value) {
                                headtitle.push(value[0]);
                                headtitle.push(value[1]);
                            });
                            for(let i=headtitle.length;i<columHead.length;i++){
                                headtitle.push('');
                            }
                            rows.push(headtitle);
                        }

                        let mergeCells = [];
                        let row,col,rowspan,colspan;
                        let headcount = 1; //表头层数
                        if(child1.length>0){
                            headcount++;
                        }
                        if(child2.length>0){
                            headcount++;
                        }
                        let currentCol = 0;
                        //('length::',rows.length, data.data.length)
                        //计算表头合并格
                        for(let i=0;i<data.column.length;i++) {
                            let value = data.column[i];
                            //*********
                            if(value.children){//有子元素的
                                let childLeng = value.children.length;
                                mergeCells.push([columheadrow, currentCol, columheadrow, currentCol + childLeng -1 ]);
                                //('chuldLeng', childLeng, mergeCells)

                                let headCol = currentCol;
                                for(let i=0;i<value.children.length;i++){

                                    //('ccchildhdh', value.children[i].children);
                                    let childlen = 0;
                                    if(value.children[i].children){

                                        let childlen = value.children[i].children.length;
                                        currentCol = currentCol+childlen;
                                        for(let k=0;k<value.children[i].children.length;k++){
                                            textAlginArr.push(value.children[i].children[k].align);
                                        }
                                    }else if(childlen > 0){//子元素里面没有子元素的

                                        //('childlen<<', childlen)
                                        mergeCells.push([columheadrow+1, currentCol, columheadrow+2, currentCol]);//cell
                                        currentCol++;
                                        //('chuldLengif', childLeng, mergeCells)
                                    }else {
                                        currentCol++;
                                        textAlginArr.push(value.children[i].align);
                                    }
                                }
                            }else{//没有子元素对象
                                mergeCells.push([columheadrow, currentCol, headcount-1, currentCol]);//currentCol
                                currentCol++;
                                textAlginArr.push(value.align);
                                //('currentCol', currentCol, mergeCells)
                            }
                        }
                        //('mergeCells>>',this, mergeCells)
                        if(parent.length>0){
                            child1.push(...parent);
                        }
                        if(child2.length>0&&parent1.length>0){
                            child2.push(...parent1);
                        }

                        rows.push(columHead);
                        //('rrrrrrcolumHead:', rows, child1, child2,self.state.flow);
                        if(child1.length>0 && self.state.flow){
                            rows.push(child1);
                        }
                        if(child2.length>0 && self.state.flow){
                            rows.push(child2);
                        }
                        //('rows>>>', rows);
                        if(data!=null && data.data.length>0){
                            //('colKeys>>>', colKeys);
                            let rowdata = [];
                            let flag = 0;
                            for(let i=0;i<data.data.length;i++){
                                flag++;
                                for(let j=0;j<colKeys.length;j++){
                                    //('data.data[i][colKeys[j]>', data.data[i], data.data[i][colKeys[j]]);
                                    // {
                                    //     "title":"11163",
                                    //     "key":"pk_org",
                                    //     "align":"left",
                                    //     "style":"body"
                                    // }
                                    let itemObj = {
                                        align: 'left',
                                        style: 'body'
                                    };
                                    itemObj.title = data.data[i][colKeys[j]];
                                    itemObj.key = colKeys[j] + flag;
                                    // rowdata.push(data.data[i][colKeys[j]])
                                    itemObj.pk_cashflow = data.data[i].pk_cashflow;
                                    itemObj.drill = data.data[i].drill;
                                    itemObj.align = colAligns[j]
                                    rowdata.push(itemObj)
                                }
                                rows.push(rowdata);
                                rowdata = [];
                            }
                        }
                        //('rowdata>>>', rows);
                        let rowhighs = [];
                        for (let i=0;i<rows.length;i++){
                            rowhighs.push(23);
                        }
                        self.state.dataout.data.cells= rows;//存放表体数据
                        //('settings.cells', self.state.dataout.data.cells)
                        self.state.dataout.data.colWidths=colWidths;
                        oldWidths.push(...colWidths);
                        self.state.dataout.data.oldWidths = oldWidths;
                        self.state.dataout.data.rowHeights=rowhighs;

                        let frozenCol;
                        for(let i=0;i<colAligns.length;i++){
                            if(colAligns[i]=='center'){
                                frozenCol = i;
                                break;
                            }
                        }
                        self.state.dataout.data.fixedColumnsLeft = frozenCol; //冻结
                        self.state.dataout.data.fixedRowsTop = headcount+columheadrow; //冻结
                        //('>>>', self.state.dataout.data.fixedRowsTop)
                        let quatyIndex = [];
                        for (let i=0;i<colKeys.length;i++){
                            if(colKeys[i].indexOf('quantity')>0){
                                quatyIndex.push(i);
                            }
                        }

                        self.state.dataout.data.freezing={
                            "isFreeze":true,
                            "row":self.state.dataout.data.fixedRowsTop,
                            "col":0
                        }
                        if(mergeCells.length>0){
                            self.state.dataout.data.mergeInfo= mergeCells;//存放表头合并数据
                        }
                        //('settings.mergeInfo', self.state.dataout.data.mergeInfo);
                        let headAligns=[];

                        for(let i=0;i<headcount+columheadrow;i++){
                            for(let j=0;j<columHead.length;j++){
                                headAligns.push( {row: i, col: j, className: "htCenter htMiddle"});
                            }
                        }
                        self.state.dataout.data.cell= headAligns,
                            self.state.dataout.data.cellsFn=function(row, col, prop) {
                                let cellProperties = {};
                                if (row >= headcount+columheadrow||(columheadrow==1&&row==0)) {
                                    cellProperties.align = colAligns[col];
                                    cellProperties.renderer = negativeValueRenderer;//调用自定义渲染方法
                                }
                                return cellProperties;
                            }
                        //('>>>>result', self.state.dataout.data);
                        self.setState({
                            dataout:self.state.dataout,
                            showTable: true,
                            queryCond,
                            querydata: data.data,
                            textAlginArr: colAligns,
                            MainSelectModalShow: false,
                            selectRowIndex:0
                        });
                    }
                }else {
                    let newTableDefaultData = deepClone(tableDefaultData)
                    self.setState({
                        MainSelectModalShow: false,
                        dataout: {...newTableDefaultData},
                        selectRowIndex:0
                    })
                    toast({content: self.state.json['20021CFQRY-000022'], color: 'warning'})//请求数据为空
                }
            },
            error: function (res) {
                toast({ content: res.message, color: 'danger' });
            }
        });
	}

	//直接输出
    printExcel=()=>{
        let {textAlginArr,dataout} = this.state;
        let {cells,mergeInfo} = dataout.data;
        let dataArr = [];
        cells.map((item,index)=>{
            let emptyArr=[];
            item.map((list,_index)=>{
                if(list){
                    emptyArr.push(list.title);
                }
            })
            dataArr.push(emptyArr);
        })
        reportPrint(mergeInfo,dataArr,textAlginArr);
    }
    handleLoginBtn = (obj, btnName) => {
	    //('handleLoginBtn>', obj, btnName)

        if(btnName === 'query'){//1、查询
            this.queryShow()
        }else if(btnName === 'refresh'){//2、刷新 linkerror linkdetail linkcf directprint
            this.ajaxData(this.state.paramObj, true)
        }else if(btnName === 'linkerror'){//2、错误分析
            this.checkoutError()
        }else if(btnName === 'linkdetail'){//3、明细
            this.goDetail()
        }else if(btnName === 'linkcf'){//4、现金流量联查
            this.goAnalysis()
        }else if(btnName === 'directprint'){//5、直接输出
            this.printExcel()
        }
    }

	render() {

		return (
			<div id="cashQuery" className="manageReportContainer">
                <HeaderArea 
                    title = {this.state.json['20021CFQRY-000019']} /* 国际化处理： 现金流量查询*/
                    btnContent = {this.props.button.createButtonApp({
                        area: 'head',
                        buttonLimit: 3,
                        onButtonClick: (obj, tbnName) => this.handleLoginBtn(obj, tbnName),
                    })}
                />

				<div className="m-analysis-main-bottom nc-theme-gray-area-bgc">
                    <div className={"m-analysis-main-bottom-div"}>
						<span className="main-bottom-span nc-theme-form-label-c">{this.state.json["20021CFQRY-000020"]}{/*日期*/}</span>
                        <FormControl
                            value={this.state.queryCond.begindate + '--' + this.state.queryCond.enddate}
                            disabled
                        />
                    </div>
				</div>


				<MainSelectModal
					show={this.state.MainSelectModalShow}
					
					ref='MainSelectModal'
					title={this.state.json["20021CFQRY-000023"]}//查询条件
					icon=""
					onConfirm={(datas) => {
						this.getDatas(datas);
					}}
					
					onCancel={() => {
						let MainSelectModalShow = false;
						this.setState({ MainSelectModalShow });
					}}
				/>
                <div className='report-table-area'>
                    <SimpleTable
                         ref="balanceTable"
                         data = {this.state.dataout}
                         onCellMouseDown = {(e, coords, td) => {
                             this.whetherDetail('drill','CashQuery');
                             this.rowBackgroundColor(e, coords, td)
                         }}
                     />
                </div>
			</div>
		)
	}
}


OpenBalance = createPage({})(OpenBalance)

export default OpenBalance
