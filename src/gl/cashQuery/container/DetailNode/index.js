import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';
// import moment from 'moment';

import {high,base,ajax,deepClone, createPage, getMultiLang,createPageIcon,gzip } from 'nc-lightapp-front';
import './index.less';


const { Refer } = high;

const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCAffix
} = base;

import { toast } from '../../../public/components/utils';
import {searchById} from "../../../manageReport/common/modules/createPureBtn";
import {whetherDetail} from "../../../manageReport/common/modules/simpleTableClick";
import {rowBackgroundColor} from '../../../manageReport/common/htRowBackground';

// const deepClone = require('../../../public/components/deepClone');
import DetailModal from './DetailModal';
import { SimpleTable } from 'nc-report';
import reportPrint from '../../../public/components/reportPrint';
import {tableDefaultData} from "../../../manageReport/defaultTableData";
import {getDetailPort} from "../../../manageReport/common/modules/modules";
import {relevanceSearch} from "../../../manageReport/referUrl";
import HeaderArea from '../../../public/components/HeaderArea';
class Detail extends Component {
	constructor(props) {
		super(props);
		this.state = {
		    json: {},
			textAlginArr:[],//对其方式
			DetailModalShow: false, //明细节点模态框是否显示
			tableStyle: {},
			tableAll: {
				column: [],
				data:[],
			},              //表格全体数据
            dataout: tableDefaultData,

		}
		this.searchById = searchById.bind(this);
		this.whetherDetail = whetherDetail.bind(this);
		this.rowBackgroundColor = rowBackgroundColor.bind(this);
	}
    componentWillMount() {
        let callback= (json) =>{
            this.setState({json:json},()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20020CFDQRY', 'childmodules'],domainName:'gl',currentLocale:'simpchn',callback});
    }

	componentDidMount(){
        this.setState({
            dataout: tableDefaultData
        })
        let appceod = this.props.getSearchParam('c');
        this.searchById('20020CFDQRYPAGE','20020CFDQRY');
        //('Mount:::', appceod);
        this.getParam();
        this.props.button.setDisabled({
            directprint: true, linkvou: true
		})
    }

    getParam = () => {
		let gziptools = new gzip();
        let data = this.props.getUrlParam && gziptools.unzip(this.props.getUrlParam('status'));
        //('datata:::',data);
        if(data){
            this.props.button.setButtonVisible({query: false});
            let paramData = data
            this.getDatas(paramData, true)
        }
    }

	getDatas = (datas, flag) => {//flag:true 表示是从联查过来的，联查过来的不需要isquery参数
		let self = this;
		let url = '/nccloud/gl/cashflow/cfcasedetailquery.do';
		let data = deepClone(datas);
		this.setState({
			queryParam: datas
		})
        data.pk_accountingbook = Array.isArray(data.pk_accountingbook) ? data.pk_accountingbook : [data.pk_accountingbook];
        if(!flag){
            data.isquery = 'Y';
		}
		let queryCond = data;
		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		        //(res.data);
		        const { data, message, success } = res;
		        if(success){
		        	if (data) {
                        self.props.button.setDisabled({
                            directprint: false
                        })
		        		let num = 1;
		        		let numArr = [];
					    let rows = [];   //表格显示的数据合集
					    let columHead = []; //第一层表头
					    let parent = []; //存放一级表头，二级表头需要补上
					    let parent1 = []; //存放二级表头，三级表头需要补上
					    let child1 = []; //第三层表头
					    let child2 = []; //第二层表头
					    let colKeys = [];  //列key值，用来匹配表体数据
					    let colAligns=[];  //每列对齐方式
					    let colWidths=[];  //每列宽度
						let oldWidths=[];
						let textAlginArr = self.state.textAlginArr.concat([]);      //对其方式
					    data.column.forEach(function (value) {//column:[]原始数据表头信息
						    //('>>>>////', value);
						    let valueCell = {};
						    valueCell.title = value.title;
						    valueCell.key = value.key;
						    valueCell.align = 'center';
						    valueCell.bodyAlign = 'left';
						    valueCell.style = 'head';
						    if(value.children){
							    self.setState({
								    flow: true
							    })
							    if(parent.length>0){
								    child1.push(...parent);
							    }
							    for(let i=0;i<value.children.length;i++){
								    if(value.children[i].children){
									    child2.push(...parent1);
									    for(let k=0;k<value.children[i].children.length;k++){
										    let child1Cell = {};
										    child1Cell.title = value.children[i].title;
										    child1Cell.key = value.children[i].key;
										    child1Cell.align = 'center';
										    child1Cell.bodyAlign = 'left';
										    child1Cell.style = 'head';
										    let child2Cell = {};
										    child2Cell.title = value.children[i].children[k].title;
										    child2Cell.key = value.children[i].children[k].key;
										    child2Cell.align = 'center';
										    child2Cell.bodyAlign = 'left';
										    child2Cell.style = 'head';
										    columHead.push(valueCell);
										    child1.push(child1Cell);
										    child2.push(child2Cell);
										    colKeys.push(value.children[i].children[k].key);
										    colAligns.push(value.children[i].children[k].align);
										    colWidths.push(value.children[i].children[k].width);
									    }
									    parent1 = [];
									    //('jjjjjjj:', colWidths)
								    }else {
									    let innerCell2 = {};
									    innerCell2.title = value.children[i].title;
									    innerCell2.key = value.children[i].key;
									    innerCell2.align = 'center';
									    innerCell2.bodyAlign = 'left';
									    innerCell2.style = 'head';
									    columHead.push(valueCell);
									    child1.push(innerCell2);
									    parent1.push(innerCell2);
									    colKeys.push(value.children[i].key);
									    colAligns.push(value.children[i].align);
									    colWidths.push(value.children[i].width);
								    }
							    }
							    parent = [];
						    }else{
							    let cellObj = {};
							    cellObj.title = value.title;
							    cellObj.key = value.key;
							    cellObj.align = 'center';
							    cellObj.bodyAlign = 'left';
							    cellObj.style = 'head';
							    columHead.push(valueCell);
							    parent1.push(cellObj);
							    parent.push(cellObj);
							    colKeys.push(value.key);
							    colAligns.push(value.align);
							    colWidths.push(value.width);
							    //('elses::', parent, parent1)
						    }
					    });
					    //('>>>>?????',parent1, parent, child1, child2)
					    let columheadrow = 0; //表头开始行
					    if (data.headtitle){
						    columheadrow = 1;
						    let headtitle = [];
						    data.headtitle.forEach(function (value) {
							    headtitle.push(value[0]);
							    headtitle.push(value[1]);
						    });
						    for(let i=headtitle.length;i<columHead.length;i++){
							    headtitle.push('');
						    }
						    rows.push(headtitle);
					    }

					    let mergeCells = [];
					    let row,col,rowspan,colspan;
					    let headcount = 1; //表头层数
					    if(child1.length>0){
						    headcount++;
					    }
					    if(child2.length>0){
						    headcount++;
					    }
					    let currentCol = 0;
					    //('length::',rows.length, data.data.length)
					    //计算表头合并格
					    for(let i=0;i<data.column.length;i++) {
						    let value = data.column[i];
						    //*********
						    if(value.children){//有子元素的
							    let childLeng = value.children.length;
							    mergeCells.push([columheadrow, currentCol, columheadrow, currentCol + childLeng -1 ]);
							    //('chuldLeng', childLeng, mergeCells)

							    let headCol = currentCol;
							    for(let i=0;i<value.children.length;i++){

								    //('ccchildhdh', value.children[i].children);
								    let childlen = 0;
								    if(value.children[i].children){

									    let childlen = value.children[i].children.length;
										currentCol = currentCol+childlen;
										for(let k=0;k<value.children[i].children.length;k++){
											textAlginArr.push(value.children[i].children[k].align);
										}
								    }else if(childlen > 0){//子元素里面没有子元素的

									    //('childlen<<', childlen)
									    mergeCells.push([columheadrow+1, currentCol, columheadrow+2, currentCol]);//cell
									    currentCol++;
									    //('chuldLengif', childLeng, mergeCells)
								    }else {
										currentCol++;
										textAlginArr.push(value.children[i].align);
	                  }
							    }
						    }else{//没有子元素对象
							    mergeCells.push([columheadrow, currentCol, headcount-1, currentCol]);//currentCol
								currentCol++;
								textAlginArr.push(value.align);
							    //('currentCol', currentCol, mergeCells)
						    }
					    }
					    //('mergeCells>>',this, mergeCells)
					    if(parent.length>0){
						    child1.push(...parent);
					    }
					    if(child2.length>0&&parent1.length>0){
						    child2.push(...parent1);
					    }

					    rows.push(columHead);
					    //('rrrrrrcolumHead:', rows, child1, child2,self.state.flow);
					    if(child1.length>0 && self.state.flow){
						    rows.push(child1);
					    }
					    if(child2.length>0 && self.state.flow){
						    rows.push(child2);
					    }
					    //('rows>>>', rows);
					    if(data!=null && data.data.length>0){
						    //('colKeys>>>', colKeys);
						    let rowdata = [];
						    let flag = 0;
						    for(let i=0;i<data.data.length;i++){
							    flag++;
							    for(let j=0;j<colKeys.length;j++){
								    //('data.data[i][colKeys[j]>', data.data[i], data.data[i][colKeys[j]]);
								    // {
								    //     "title":"11163",
								    //     "key":"pk_org",
								    //     "align":"left",
								    //     "style":"body"
								    // }
								    let itemObj = {
									    align: 'left',
									    style: 'body'
								    };
								    itemObj.title = data.data[i][colKeys[j]];
								    itemObj.key = colKeys[j] + flag;
								    // rowdata.push(data.data[i][colKeys[j]])
                                    itemObj.pk_voucher = data.data[i].pk_voucher;
                                    itemObj.align = colAligns[j];
								    rowdata.push(itemObj)
							    }
							    rows.push(rowdata);
							    rowdata = [];
						    }
					    }
					    //('rowdata>>>', rows);
					    let rowhighs = [];
					    for (let i=0;i<rows.length;i++){
						    rowhighs.push(23);
					    }
					    let newDataSource = deepClone(self.state.dataout);
                        newDataSource.data.cells= rows;//存放表体数据
					    //('settings.cells', self.state.dataout.data.cells)
                        newDataSource.data.colWidths=colWidths;
					    oldWidths.push(...colWidths);
                        newDataSource.data.oldWidths = oldWidths;
                        newDataSource.data.rowHeights=rowhighs;
					    let frozenCol;
					    for(let i=0;i<colAligns.length;i++){
						    if(colAligns[i]=='center'){
							    frozenCol = i;
							    break;
						    }
					    }
                        newDataSource.data.fixedColumnsLeft = 0; //冻结
						newDataSource.data.fixedRowsTop = headcount+columheadrow; //冻结
						
						// self.state.dataout.data.fixedColumnsLeft = 0; //冻结
						// self.state.dataout.data.fixedRowsTop = headcount+columheadrow; //冻结
						newDataSource.data.freezing = {//固定表头
							"isFreeze" : true,
							"row": newDataSource.data.fixedRowsTop,
							"col" : newDataSource.data.fixedColumnsLeft
						}
					    let quatyIndex = [];
					    for (let i=0;i<colKeys.length;i++){
						    if(colKeys[i].indexOf('quantity')>0){
							    quatyIndex.push(i);
						    }
					    }


					    if(mergeCells.length>0){
                            newDataSource.data.mergeInfo= mergeCells;//存放表头合并数据
					    }
					    //('settings.mergeInfo', newDataSource.data.mergeInfo);
					    let headAligns=[];

					    for(let i=0;i<headcount+columheadrow;i++){
						    for(let j=0;j<columHead.length;j++){
							    headAligns.push( {row: i, col: j, className: "htCenter htMiddle"});
						    }
					    }
                        newDataSource.data.cell= headAligns,
                            newDataSource.data.cellsFn=function(row, col, prop) {
								    let cellProperties = {};
								    if (row >= headcount+columheadrow||(columheadrow==1&&row==0)) {
									    cellProperties.align = colAligns[col];
									    cellProperties.renderer = negativeValueRenderer;//调用自定义渲染方法
								    }
								    return cellProperties;
							    }
					    //('>>>>result', newDataSource.data);
					    self.setState({
						    dataout:newDataSource,
							showTable: true,
                            DetailModalShow: false,
							textAlginArr: colAligns,
							selectRowIndex:0
					    });
		        	}else{
                        let newTableDefaultData = deepClone(tableDefaultData)
                        self.setState({
                            DetailModalShow: false,
							dataout: {...newTableDefaultData},
							selectRowIndex:0
                        })
                        toast({content: self.state.json['20020CFDQRY-000013'], color: 'warning'})
					}



		        }else {
		            toast({ content: message.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'danger' });
		        
		    }
		});

	}

	queryShow() {
		this.setState({
			DetailModalShow: true,
		})
	}

	 //直接输出
	 printExcel=()=>{
        let {textAlginArr,dataout} = this.state;
        let {cells,mergeInfo} = dataout.data;
        let dataArr = [];
        cells.map((item,index)=>{
            let emptyArr=[];
            item.map((list,_index)=>{
                if(list){
                    emptyArr.push(list.title);
                }
            })
            dataArr.push(emptyArr);
        })
        reportPrint(mergeInfo,dataArr,textAlginArr);
    }
    handleLoginBtn = (obj, btnName) => {
        if(btnName === 'query'){//1、查询
            this.queryShow()
		}else if(btnName === 'refresh'){//2、刷新
			let isLink = this.props.getUrlParam && this.props.getUrlParam('status') ? true : false
            if(this.state.queryParam){
                this.getDatas(this.state.queryParam, isLink);
			}

        }else if(btnName === 'directprint'){//3、直接输出
            this.printExcel()
        }else if(btnName === 'linkvou'){//4、联查凭证
            getDetailPort(this, {
                url: '/nccloud/gl/accountrep/triaccquery.do',
                jumpTo: relevanceSearch,
                key: 'relevanceSearch',//relevanceSearch
                appcode: '20023030',
                flagParam: 'cashFlowDetailSearch'
            }, this.state.json)
        }
	}
	render() {
		//('render:::', this.state, base)
		return (

			<div id="error" className='manageReportContainer'>
				<HeaderArea 
                    title = {this.state.json['20020CFDQRY-000010']} /* 国际化处理： 现金流量明细查询*/
                    btnContent = {this.props.button.createButtonApp({
						area: 'head',
						buttonLimit: 3,
						onButtonClick: (obj, tbnName) => this.handleLoginBtn(obj, tbnName),
					})}
                />
                <div className="m-analysis-main-bottom nc-theme-gray-area-bgc">
					<div className={"m-analysis-main-bottom-div"}>
						<span className="main-bottom-span nc-theme-common-font-c">{this.state.json['20020CFDQRY-000011']}{/*日期*/}</span>
						<FormControl
							value={ this.state.queryParam &&  (this.state.queryParam.rangeDate || (this.state.queryParam.begindate+'--'+this.state.queryParam.enddate))}
							disabled
						/>
					</div>
                </div>
                <div className='report-table-area'>
					<SimpleTable
						 ref="balanceTable"
						 data = {this.state.dataout}
                         onCellMouseDown = {(e, coords, td) => {
                             //('recorddd:::', this.refs.balanceTable.getRowRecord());
                             this.whetherDetail('pk_voucher', 'DetailNode');
                             this.rowBackgroundColor(e, coords, td)

                         }}
					 />
				</div>

				<DetailModal
					show={this.state.DetailModalShow}
					
					ref='MainSelectModal'
					title={this.state.json['20020CFDQRY-000012']}
					icon=""
					onConfirm={(datas) => {
						this.getDatas(datas);
					}}
					
					onCancel={() => {
						let DetailModalShow = false;
						this.setState({ DetailModalShow });
					}}
				/>


			</div>

			)
									
	}
}
Detail = createPage({})(Detail)
export default Detail
