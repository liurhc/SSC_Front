import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';
// import moment from 'moment';

import {high,base,ajax,deepClone, createPage, getMultiLang } from 'nc-lightapp-front';
// import './index.less';
import  '../../../../public/reportcss/searchmodalpage.less'

const { Refer } = high;

const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCModal: Modal,
} = base;

import { toast } from '../../../../public/components/utils';

const NCOption = Select.NCOption;
// const deepClone = require('../../../../public/components/deepClone');
const format = 'YYYY-MM-DD';
// import AccountBookByFinanceOrgRef from '../../../../../uapbd/refer/org/AccountBookByFinanceOrgRef';
import CashflowTreeRef from '../../../../../uapbd/refer/fiacc/CashflowTreeRef';
import createScript from '../../../../public/components/uapRefer.js';
import {
    businessUnit,
    createReferFn,
    getCheckContent, getReferAppCode,
    getReferDetault, returnMoneyType
} from "../../../../manageReport/common/modules/modules";
import DateGroup from "../../../../manageReport/common/modules/dateGroup";
import BusinessUnitTreeRef from "../../../../../uapbd/refer/orgv/BusinessUnitVersionDefaultAllTreeRef";



class CentralConstructorModal extends Component {

	constructor(props) {
		super(props);
		this.state={
			json: {},
            isqueryuntallyed: true,
            rangeDate: [],
			accountingbook: {
				refname: '',
				refpk: '',
			},  //核算账簿
			cashflow: {
				refname: '',
				refpk: '',  //现金流量项目参照
			},
			selectedQueryValue: '0', //查询方式单选
			selectedCurrValue: '1', //返回币种单选
			ismain: '0', // 0主表  1附表
			begindate: '', //开始时间
			enddate: '', //结束时间
			currtype: '',      //选择的币种
			currtypeName: '',  //选择的币种名称
			currtypeList: [],  //币种列表
			defaultCurrtype: {         //默认币种
				name: '',
				pk_currtype: '',
			},
			checkbox: {
				one: false,
				two: false,
				three: false,
			}

		}
	
	}

	static defaultProps = {
		show: false,
		title: '',
		content: '',
		closeButton: false,
		icon: 'icon-tishianniuchenggong',
		isButtonWhite: true,
		isButtonShow: true,
		// accAssItems: [],
	};
    componentWillMount() {
        let callback= (json) =>{
            this.setState({
				json:json,
                currtype: json['20020CFDQRY-000000'],/* 国际化处理： 本币*/
			},()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20020CFDQRY', 'dategroup', 'childmodules'],domainName:'gl',currentLocale:'simpchn',callback});
        this.getCurrtypeList();
    }

	componentDidMount() {
        getReferAppCode(this, 'appcode');
        setTimeout(() => getReferDetault(this, false, {
            businessUnit
        }, 'CashQuery'),0)
	}

	handleRadioChange(value) {
		this.setState({selectedQueryValue: value});
	}

	handleRadioCurrChange(value) {
		this.setState({selectedCurrValue: value});
	}




	getCurrtypeList = () => {
		let self = this;
		let url = '/nccloud/gl/voucher/queryAllCurrtype.do';
		// let data = '';
		// let data = {};
		let data = {
			"localType":"1",
		};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		        const { data, error, success } = res;
		        if(success){
					self.setState({
						currtypeList: data,
						currtype: self.state.json['20020CFDQRY-000000'],/* 国际化处理： 本币*/
					})
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});

	}


	handleCurrtypeChange(value) {
		let self = this;
		// if (!this.state.accountingbook || !this.state.accountingbook.refpk) {
		// 	toast({ content: '请先选择核算账簿', color: 'warning' });
		// 	return;
		// }
		let currtypeList = this.state.currtypeList;
		currtypeList.forEach(function (item, index) {
			if (item.pk_currtype == value ) {
				self.setState({
					currtypeName: item.name,
				})
			}
		})

		this.setState({
			currtype: value,
		// }, this.getTables);
		}, this.getRates);
	}


	handleIsmainChange(value) {
		this.setState({
			ismain: value,
		})
	}


	changeCheck=()=> {
		// this.setState({checked:!this.state.checked});
	}

	onChange1(e) {
	    //(e);
	    let checkbox = deepClone(this.state.checkbox);
	    checkbox.one = e;
	    this.setState({checkbox});
	}

	onChange2(e) {
	    //(e);
	    let checkbox = deepClone(this.state.checkbox);
	    checkbox.two = e;
	    this.setState({checkbox});
	}

	onChange3(e) {
	    //(e);
	    let checkbox = deepClone(this.state.checkbox);
	    checkbox.three = e;
	    this.setState({checkbox});
	}
	
	//起始时间变化
	onStartChange(d) {
		//(d)
		this.setState({
			begindate:d
		})
	}
	
	//结束时间变化
	onEndChange(d) {
		this.setState({
			enddate:d
		})
	}
    handleDateChange = (value) => {//日期: 范围选择触发事件
        //('handleDateChange:', value);
        this.setState({
            begindate: value[0],
            enddate: value[1],
            rangeDate: value
        })
    }

	render() {
		let { show, title, content, closeButton, icon, isButtonWhite, isButtonShow}= this.props;
		return <Modal
			fieldid='query'
            show={ show }
            onHide={()=>this.props.onCancel(true)}
            className='cashQuery'
        >
            <Modal.Header closeButton>
                <Modal.Title>
                    {title}
                </Modal.Title>
            </Modal.Header >
			<Modal.Body id="modalOuter">
                <div className='right_query noserchmatter'>
                    <div className='query_body1'>
                        <div className='query_form'>
				<Row className="myrow">
					<Col md={2} sm={2}>
                        <span style={{color: 'red'}}>*</span>
						<span className='nc-theme-form-label-c'>{this.state.json['20020CFDQRY-000001']}：</span>{/* 国际化处理： 财务核算账簿*/}
					</Col>
					<Col md={10} sm={10}>
						<div className="book-ref">
                            {
                                createReferFn(
                                    this,
                                    {
                                        url: 'uapbd/refer/org/AccountBookTreeRef/index.js',
                                        value: this.state.accountingbook,
                                        referStateKey: 'checkAccountBook',
                                        referStateValue: this.state.checkAccountBook,
                                        stateValueKey: 'accountingbook',
                                        flag: false,
                                        showGroup: true,
                                        queryCondition: {
                                            pk_accountingbook: this.state.accountingbook && this.state.accountingbook.refpk
                                        }
                                    },
                                    {
                                        businessUnit: businessUnit
                                    },
									'DetailNode'
                                )
                            }
						</div>
					</Col>
				</Row>
                {
                    this.state.isShowUnit === 'Y' ?
                        <Row className="myrow">
                            <Col md={2} sm={2}>
                                <span className='nc-theme-form-label-c'>{this.state.json['20020CFDQRY-000002']}：</span>{/* 国际化处理： 业务单元*/}
                            </Col>
                            <Col md={10} sm={10}>
                                <div className="book-ref">
                                    <BusinessUnitTreeRef
										fieldid='buSecond'
                                        value={this.state.buSecond}
                                        isMultiSelectedEnabled= {true}
                                        isShowDisabledData = {true}
                                        queryCondition = {{
                                            "pk_accountingbook": this.state.accountingbook && this.state.accountingbook.refpk,
                                            "isDataPowerEnable": 'Y',
                                            "DataPowerOperationCode" : 'fi',
                                            "TreeRefActionExt":'nccloud.web.gl.ref.GLBUVersionWithBookRefSqlBuilder'
                                        }}
                                        onChange={(v)=>{
                                            //(v);
                                            this.setState({
                                                buSecond: v,
                                            }, () => {
                                                if(this.state.accountingbook && this.state.accountingbook[0]) {
                                                }
                                            })
                                        }
                                        }
                                    />

                                </div>

                            </Col>
                        </Row> :
                        <div></div>
                }

                {/*会计期间*/}
                <DateGroup
                    selectionState = 'false'
                    start = {this.state.start}
                    end = {this.state.end}
                    enddate = {this.state.enddate}
                    pk_accperiodscheme = {this.state.pk_accperiodscheme}
                    pk_accountingbook = {this.state.accountingbook}
                    rangeDate = {this.state.rangeDate}
                    handleValueChange = {this.handleValueChange}
                    handleDateChange = {this.handleDateChange}
                    self = {this}
                    showRadio = {this.state.showRadio}
                    showPeriod={true}
                    showDate={false}
                />

				<Row className="myrow">
					<Col md={2} sm={2}>
						<span className='nc-theme-form-label-c'>{this.state.json['20020CFDQRY-000003']}：</span>{/* 国际化处理： 主附表*/}
					</Col>
					<Col md={10} sm={10}>
					
						{/*主附表下拉*/}
						<Col md={5} sm={5}>
							<div className="book-ref">
								<Select
									fieldid='ismain'
									value={this.state.ismain}							   	
									// style={{ width: 150, marginRight: 6 }}
									onChange={this.handleIsmainChange.bind(this)}							   	
								>
									<NCOption value={'0'} key={'0'} >{this.state.json['20020CFDQRY-000004']}</NCOption>{/* 国际化处理： 主表*/}
									<NCOption value={'1'} key={'1'} >{this.state.json['20020CFDQRY-000005']}</NCOption>{/* 国际化处理： 附表*/}

								</Select>

							</div>
						</Col>
						<Col md={2} sm={2}></Col>
						{/*现金流量表项*/}
						<Col md={5} sm={5}>
							<div className="book-ref">
								<CashflowTreeRef
									value={this.state.cashflow}
									isMultiSelectedEnabled = {true}
									onlyLeafCanSelect = {true}
									queryCondition = {() => {
										return { 
											ismain: this.state.ismain == '0' ? 'Y' : 'N',
											TreeRefActionExt:'nccloud.web.gl.ref.CashFlowTypeRefSqlBuilder',
											"isDataPowerEnable": 'Y',
											"DataPowerOperationCode" : 'fi',
											'pk_org': this.state.unitValueParam
										}}
									}
									onChange={(v)=>{
										//(v);
									
										this.setState({
											cashflow: v,
										})

									}
									}

								/>

							</div>
						</Col>
					</Col>					
				</Row>


				<Row className="myrow">
					<Col md={2} sm={2}>
						<span className='nc-theme-form-label-c'>{this.state.json['20020CFDQRY-000006']}：</span>{/* 国际化处理： 币种*/}
					</Col>
					<Col md={10} sm={10}>
						<Col md={5} sm={5}>
							<div className="book-ref">
								<Select
									fieldid='currtype'
									value={this.state.currtype}
									
									// style={{ width: 150, marginRight: 6 }}
								
								
									onChange={this.handleCurrtypeChange.bind(this)}
									
								>
									{this.state.currtypeList.map((item, index) => {
										return <NCOption value={item.pk_currtype} key={item.pk_currtype} >{item.name}</NCOption>
									} )}

								</Select>

							</div>
						</Col>
						<Col md={2} sm={2}></Col>
						<Col md={5} sm={5}>
							<Checkbox colors="dark"
									className="mycheck"
									disabled={!this.state.isqueryuntallyed}
									checked={this.state.checkbox.one}
									onChange={this.onChange1.bind(this)}
							>
								{this.state.json['20020CFDQRY-000007']} {/* 国际化处理： 包含未记账凭证*/}
							</Checkbox>
						</Col>
					</Col>
				</Row>

                {/*返回币种：*/}
                {
                    returnMoneyType(
                        this,
                        {
                            key: 'selectedCurrValue',
                            value: this.state.selectedCurrValue,
                            groupEdit: this.state.groupCurrency,
                            gloableEdit: this.state.globalCurrency
                        },
						this.state.json
                    )
                }
						</div>
					</div>
				</div>

			</Modal.Body>
			{isButtonShow &&
				<Modal.Footer>

					<Button
						fieldid='query'
                        className= "button-primary"
						onClick = {() => {
							let pk_unit = [];
                            if (Array.isArray(this.state.buSecond)) {
                                this.state.buSecond.forEach(function (item) {
                                    pk_unit.push(item.refpk)
                                })
                            }
                            let cashFlowArr = [];
                            if(Array.isArray(this.state.cashflow)){
                                this.state.cashflow.map((item, index) => {
                                    cashFlowArr.push(item.refpk)
								})
							}
                            //('clickkkk::', this.state)
							let newdata = {
								"pk_accountingbook": this.state.accountingbook.refpk,
								"begindate": this.state.rangeDate[0],
								"enddate": this.state.rangeDate[1],
								"pk_currtype": this.state.currtype,
								"includetally": (this.state.checkbox.one ? 'Y' : 'N'),
								"includesum": (this.state.checkbox.two ? 'Y' : 'N'),
								"includestop": (this.state.checkbox.three ? 'Y' : 'N'),
								"ismain": this.state.ismain,
								"returncurr": this.state.selectedCurrValue,
								"querytype": this.state.selectedQueryValue,
								"pk_cashflow": cashFlowArr,
								'rangeDate': this.state.rangeDate,
								pk_units: pk_unit,
							}
							//(newdata)

							this.props.onConfirm(newdata)

						}}
					>
						{this.state.json['20020CFDQRY-000008']}{/* 国际化处理： 查询*/}
					</Button>


					<Button 
						fieldid='cancel'
						className= 'btn-2 btn-cancel'
						onClick={() => {this.props.onCancel(false)}}
					>{this.state.json['20020CFDQRY-000009']}</Button>{/* 国际化处理： 取消*/}
				</Modal.Footer>
			}
			<Loading fullScreen showBackDrop={true} show={this.state.isLoading} />



		</Modal>;
	}
}

CentralConstructorModal = createPage({})(CentralConstructorModal)

export default CentralConstructorModal;
