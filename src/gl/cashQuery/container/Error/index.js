import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';
// import moment from 'moment';

import {high,base,ajax,deepClone, createPage, getMultiLang ,createPageIcon, gzip} from 'nc-lightapp-front';
import './index.less';


const { Refer } = high;

const { NCTable:Table, NCDiv } = base;
const gziptools = new gzip();
import { toast } from '../../../public/components/utils';
import {getDetailPort} from "../../../manageReport/common/modules/modules";
import {relevanceSearch} from "../../../manageReport/referUrl";
import {searchById} from "../../../manageReport/common/modules/createPureBtn";
import HeaderArea from '../../../public/components/HeaderArea';
import { openToVoucher } from '../../../public/common/voucherUtils';


// const deepClone = require('../../../public/components/deepClone');

class Error extends Component {
	constructor(props) {
		super(props);
//每定义一个state 都要添加备注
		this.state = {
			json: {},
            selectIndex: 0,
			dataList: [],
			tableStyle: {},
			tableAll: {
				column: [],
				data:[],
			},              //表格全体数据
            selectRowData: {},
			searchDisabled: true,//联查凭证是否禁止
			selectedIndex: null,
			json:{}
		}
		this.searchById = searchById.bind(this);
		this.goBack = this.goBack.bind(this)
	}

    componentWillMount(){
		let callback= (json) =>{
            this.setState({
              json:json,
                columns_reportSum:this.columns_reportSum,
				},()=>{
				initTemplate.call(this, this.props);
            })
        }
		getMultiLang({moduleId:'20021CFQRY',domainName:'gl',currentLocale:'simpchn',callback});
    }

	goBack() {
        this.props.linkTo('/gl/cashQuery/pages/main/index.html',{
			paramObj: this.props.getUrlParam('param'),
			cashError: JSON.stringify(this.props.getUrlParam('cashError'))
		})//src/gl/cashQuery/pages/main/index.js
	}

    componentWillMount() {
        let callback= (json) =>{
            this.setState({json:json},()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20021CFQRY', 'childmodules'],domainName:'gl',currentLocale:'simpchn',callback});
    }

	componentDidMount() {
        let appceod = this.props.getSearchParam('c');
        this.searchById('20021CFQRYPAGE',appceod)
		let self = this;
		let url = '/nccloud/gl/cashflow/cfcasedetailquery.do';
        //('datata:::', this.props.getUrlParam('cashError'));
		let data = JSON.parse(this.props.getUrlParam('cashError')) //JSON.parse(sessionStorage.getItem('cashError'));
        this.props.button.setDisabled({
            linkvou: true,
        })
		data.iserror = 'Y';

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {

		       
		        const { data, message, success } = res;
		        //('newSuccssss:::', res)
		        if(success){
					if (data) {
						let tableAll = deepClone(data);
						let widthAll = 0;
						tableAll.data.forEach(function (item) {
							item.key = item.pk_voucher;
						});
						tableAll.column.forEach(function (item) {
							if (!item.children) {
								widthAll = widthAll + (+item.width);
								item.width = item.width + 'px';

							} else {
								item.children.forEach(function (item2) {
									widthAll = widthAll + (+item2.width);
									item2.width += 'px'
								})
							}
						});
						self.setState({
							tableAll,
							tableStyle: {width: widthAll,},
							
						})
					}


		        }else {
		            toast({ content: message.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'danger' });
		        
		    }
		});

		
	}

    handleRowClick = (data, index, event) => {
        let trs=document.getElementsByClassName("u-table-row-level-0")
        Array.from(trs).forEach((a,b,c)=>{
            trs[b].id=""
			trs[index].id="idya"
			if(trs[index].id!=''){
				this.props.button.setDisabled({
					linkvou: false,
				})
			}
		})
		this.setState({
            searchDisabled:false,
			selectRowData: data,
			selectedIndex: index
		})
        this.props.button.setDisabled({
            linkvou: false,
        })
	}
    jumpToEdit = () => {
		//('jumpToEdit>>>', this.state.tableAll.data);
		let pkArr = [];
        this.state.tableAll.data.map((item, index) => {
            pkArr.push(item.pk_voucher)
		});
        let searchParam = JSON.parse(this.props.getUrlParam('param'));
		searchParam.pk_voucher = pkArr
		//('searchParam>>', searchParam);
		let cashData = gziptools.zip(JSON.stringify({
			param: searchParam,
		}));
        this.props.openTo('/gl/gl_cashFlows/CashFlowAnalyse/main/index.html',
			{//gl/gl_cashFlows/pages/CashFlowAnalyse/index.html
				searchDataCash: cashData,
                c: '20020CFANY&',
                p: '20020CFANYPAGE',
                appcode: '20020CFANY',
                pagecode: '20020CFANYPAGE'
        })
	}

    handleLoginBtn = (obj, btnName) => {
        //('handleLoginBtn>', obj, btnName)

        if(btnName === 'edit'){//1、编辑
            this.jumpToEdit()
		}else if(btnName === 'linkvou'){//2、联查凭证
            getDetailPort(this, {
                url: '/nccloud/gl/accountrep/triaccquery.do',
                jumpTo: relevanceSearch,
                key: 'cashFlowSearch',//relevanceSearch
                appcode: '20023030',
                selectRowData: this.state.selectRowData}, this.state.json)
        }
        // else if(btnName === 'reback'){//3、返回
        //     this.goBack()
        // }
    }
    getTableHeight = () => {
        let tableHeight=document.getElementById('app').offsetHeight-110;
        return tableHeight;
    }
	render() {
		//('render:::',this.state)
		let { tableAll, selectedIndex } = this.state
		let columns = tableAll.column;
		let data = tableAll.data;
		return (
			<div id="error" className='nc-single-table'>
				<HeaderArea 
					title = {this.state.json['20021CFQRY-000026']} /* 国际化处理： 错误分析*/
					initShowBackBtn = {true}
					backBtnClick = {this.goBack}
                    btnContent = {this.props.button.createButtonApp({
                        area: 'error',
                        buttonLimit: 3,
                        onButtonClick: (obj, tbnName) => this.handleLoginBtn(obj, tbnName)
                    })}
                />
				<NCDiv fieldid="errorAnalysis" areaCode={NCDiv.config.TableCom} className="nc-singleTable-table-area">
					<Table
						columns={columns}
						rowClassName = {(record, index, indent)=>{
							// 平台提供选中行类名nctable-selected-row, 设置点击行背景色
							return selectedIndex === index ? 'nctable-selected-row' : ''
						}}
						onRowClick = {(data, index, event) => this.handleRowClick(data, index, event)}
						bordered
						data={data}
						//scroll={{x: true, y: 560,}}
						emptyText={() => {this.state.json['20021CFQRY-000027']}}
						scroll={{
							x:true,
							y:this.getTableHeight()
						}}
					/>
				</NCDiv>
			</div>
		)
	}
}

Error = createPage({})(Error)

ReactDOM.render(<Error />,document.querySelector('#app'));