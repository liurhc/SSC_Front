import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';
// import moment from 'moment';

import {high,base,ajax,deepClone,viewModel } from 'nc-lightapp-front';
let { setGlobalStorage, getGlobalStorage, removeGlobalStorage } = viewModel;
import './index.css';
import './index.less';


const { Refer } = high;

const { NCFormControl: FormControl,NCInput: Input,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown
} = base;

import { toast } from '../../../public/components/utils';


// const deepClone = require('../../../public/components/deepClone');
import { SimpleTable } from 'nc-report';

export default class Detail extends Component {
	constructor(props) {
		super(props);
//每定义一个state 都要添加备注
		this.state = {
			tableStyle: {},
			mydata: {

				currtypeName: '',
				begindate: '',
				enddate: '',
				bookname: '',

			},
            dataout: {data:{
                cells : [
	                [{title: "科目编码", key: "acccode", align: "center", bodyAlign: "left", style: "head"},
	                {title: "科目名称", key: "accname", align: "center", bodyAlign: "left", style: "head"},
	                {title: "币种", key: "currtype", align: "center", bodyAlign: "left", style: "head"},
	                {title: "方向", key: "initorint", align: "center", bodyAlign: "left", style: "head"},
	                {title: "期初余额", key: undefined, align: "center", bodyAlign: "left", style: "head"},
	                {title: "期初余额", key: undefined, align: "center", bodyAlign: "left", style: "head"},
	                {title: "期初余额", key: undefined, align: "center", bodyAlign: "left", style: "head"},
	                {title: "本期借方", key: undefined, align: "center", bodyAlign: "left", style: "head"},
	                {title: "本期借方", key: undefined, align: "center", bodyAlign: "left", style: "head"},
	                {title: "本期借方", key: undefined, align: "center", bodyAlign: "left", style: "head"},
	                {title: "本期贷方", key: undefined, align: "center", bodyAlign: "left", style: "head"},
	                {title: "本期贷方", key: undefined, align: "center", bodyAlign: "left", style: "head"},
	                {title: "本期贷方", key: undefined, align: "center", bodyAlign: "left", style: "head"},
	                {title: "借方累计", key: undefined, align: "center", bodyAlign: "left", style: "head"},
	                {title: "借方累计", key: undefined, align: "center", bodyAlign: "left", style: "head"},
	                {title: "借方累计", key: undefined, align: "center", bodyAlign: "left", style: "head"},
	                {title: "贷方累计", key: undefined, align: "center", bodyAlign: "left", style: "head"},
	                {title: "贷方累计", key: undefined, align: "center", bodyAlign: "left", style: "head"},
	                {title: "贷方累计", key: undefined, align: "center", bodyAlign: "left", style: "head"},
	                {title: "方向", key: "endorint", align: "center", bodyAlign: "left", style: "head"},
	                {title: "期末余额", key: undefined, align: "center", bodyAlign: "left", style: "head"},
	                {title: "期末余额", key: undefined, align: "center", bodyAlign: "left", style: "head"},
	                {title: "期末余额", key: undefined, align: "center", bodyAlign: "left", style: "head"}],

	                [
		                {title: "科目编码", key: "acccode", align: "center", bodyAlign: "left", style: "head"},
		                {title: "科目名称", key: "accname", align: "center", bodyAlign: "left", style: "head"},
		                {title: "币种", key: "currtype", align: "center", bodyAlign: "left", style: "head"},
		                {title: "方向", key: "initorint", align: "center", bodyAlign: "left", style: "head"},
		                {title: "数量", key: "initquantity", align: "center", bodyAlign: "left", style: "head"},
		                {title: "原币", key: "initamount", align: "center", bodyAlign: "left", style: "head"},
		                {title: "本币", key: "initlocamount", align: "center", bodyAlign: "left", style: "head"},
		                {title: "数量", key: "debitquantity", align: "center", bodyAlign: "left", style: "head"},
		                {title: "原币", key: "debitamount", align: "center", bodyAlign: "left", style: "head"},
		                {title: "本币", key: "debitlocamount", align: "center", bodyAlign: "left", style: "head"},
		                {title: "数量", key: "creditquantity", align: "center", bodyAlign: "left", style: "head"},
		                {title: "原币", key: "creditamount", align: "center", bodyAlign: "left", style: "head"},
		                {title: "本币", key: "creditlocamount", align: "center", bodyAlign: "left", style: "head"},
		                {title: "数量", key: "debitaccumquantity", align: "center", bodyAlign: "left", style: "head"},
		                {title: "原币", key: "debitaccumamount", align: "center", bodyAlign: "left", style: "head"},
		                {title: "本币", key: "debitaccumlocamount", align: "center", bodyAlign: "left", style: "head"},
		                {title: "数量", key: "creditaccumquantity", align: "center", bodyAlign: "left", style: "head"},
		                {title: "原币", key: "creditaccumamount", align: "center", bodyAlign: "left", style: "head"},
		                {title: "本币", key: "creditaccumlocamount", align: "center", bodyAlign: "left", style: "head"},
		                {title: "方向", key: "endorint", align: "center", bodyAlign: "left", style: "head"},
		                {title: "数量", key: "endquantity", align: "center", bodyAlign: "left", style: "head"},
		                {title: "原币", key: "endamount", align: "center", bodyAlign: "left", style: "head"},
		                {title: "本币", key: "endlocamount", align: "center", bodyAlign: "left", style: "head"}]
	                ],
                mergeInfo:[
	                [0, 0, 1, 0],
	                [0, 1, 1, 1],
	                [0, 2, 1, 2],
	                [0, 3, 1, 3],
	                [0, 4, 0, 6],
	                [0, 7, 0, 9],
	                [0, 10, 0, 12],
	                [0, 13, 0, 15],
	                [0, 16, 0, 18],
	                [0, 19, 1, 19],
	                [0, 20, 0, 22]
                ],
                cell : [],
                cellsFn:{},
                rowHeaders: true,
                manualColumnResize:true,
                readOnly:true,
                columnSorting: true, // 排序功能
                wordWrap:false,
                colHeaders:true,
                autoColumnSize:false,
                autoRowSize:false,
                afterSelection: this.selectRows
                // minCols:50,
            }},
			tableAll: {
				column: [],
				data:[],
			},              //表格全体数据


		}
	}



	goBack() {
		window.location.href = '../main/index.html';
	}

	componentDidMount() {
		let self = this;
		let url = '/nccloud/gl/cashflow/cflinkquery.do';
		let data = JSON.parse(getGlobalStorage('sessionStorage', 'cashAnalysis'));
		let mydata = {
			currtypeName: data.currtypeName,
			begindate: data.begindate,
			enddate: data.enddate,
			bookname: data.bookname,

		}

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {

		        const { data, message, success } = res;
		        if(success){
/*		        	if (data) {
		        		let tableAll = deepClone(data);
		        		let widthAll = 0;
		        		tableAll.data.forEach(function (item) {
		        			item.key = item.pk_voucher;
		        		});
		        		tableAll.column.forEach(function (item) {
		        			if (!item.children) {
		        				widthAll = widthAll + (+item.width);
		        				item.width = item.width + 'px';

		        			} else {
		        				item.children.forEach(function (item2) {
		        					widthAll = widthAll + (+item2.width);
		        					item2.width += 'px'
		        				})
		        			}
		        		});
		        		self.setState({
		        			tableAll,
		        			// mydata,
		        			tableStyle: {width: widthAll,},
		        			
		        		})
		        	}*/

		        	if (data) {
		        		let num = 1;
		        		let numArr = [];
					    let rows = [];   //表格显示的数据合集
					    let columHead = []; //第一层表头
					    let parent = []; //存放一级表头，二级表头需要补上
					    let parent1 = []; //存放二级表头，三级表头需要补上
					    let child1 = []; //第三层表头
					    let child2 = []; //第二层表头
					    let colKeys = [];  //列key值，用来匹配表体数据
					    let colAligns=[];  //每列对齐方式
					    let colWidths=[];  //每列宽度
					    let oldWidths=[];
					    data.column.forEach(function (value) {//column:[]原始数据表头信息
						    let valueCell = {};
						    valueCell.title = value.title;
						    valueCell.key = value.key;
						    valueCell.align = 'center';
						    valueCell.bodyAlign = 'left';
						    valueCell.style = 'head';
						    if(value.children){
							    self.setState({
								    flow: true
							    })
							    if(parent.length>0){
								    child1.push(...parent);
							    }
							    for(let i=0;i<value.children.length;i++){
								    if(value.children[i].children){
									    child2.push(...parent1);
									    for(let k=0;k<value.children[i].children.length;k++){
										    let child1Cell = {};
										    child1Cell.title = value.children[i].title;
										    child1Cell.key = value.children[i].key;
										    child1Cell.align = 'center';
										    child1Cell.bodyAlign = 'left';
										    child1Cell.style = 'head';
										    let child2Cell = {};
										    child2Cell.title = value.children[i].children[k].title;
										    child2Cell.key = value.children[i].children[k].key;
										    child2Cell.align = 'center';
										    child2Cell.bodyAlign = 'left';
										    child2Cell.style = 'head';
										    columHead.push(valueCell);
										    child1.push(child1Cell);
										    child2.push(child2Cell);
										    colKeys.push(value.children[i].children[k].key);
										    colAligns.push(value.children[i].children[k].align);
										    colWidths.push(value.children[i].children[k].width);
									    }
									    parent1 = [];

								    }else {
									    let innerCell2 = {};
									    innerCell2.title = value.children[i].title;
									    innerCell2.key = value.children[i].key;
									    innerCell2.align = 'center';
									    innerCell2.bodyAlign = 'left';
									    innerCell2.style = 'head';
									    columHead.push(valueCell);
									    child1.push(innerCell2);
									    parent1.push(innerCell2);
									    colKeys.push(value.children[i].key);
									    colAligns.push(value.children[i].align);
									    colWidths.push(value.children[i].width);
								    }
							    }
							    parent = [];
						    }else{
							    let cellObj = {};
							    cellObj.title = value.title;
							    cellObj.key = value.key;
							    cellObj.align = 'center';
							    cellObj.bodyAlign = 'left';
							    cellObj.style = 'head';
							    columHead.push(valueCell);
							    parent1.push(cellObj);
							    parent.push(cellObj);
							    colKeys.push(value.key);
							    colAligns.push(value.align);
							    colWidths.push(value.width);
						    }
					    });
					    let columheadrow = 0; //表头开始行
					    if (data.headtitle){
						    columheadrow = 1;
						    let headtitle = [];
						    data.headtitle.forEach(function (value) {
							    headtitle.push(value[0]);
							    headtitle.push(value[1]);
						    });
						    for(let i=headtitle.length;i<columHead.length;i++){
							    headtitle.push('');
						    }
						    rows.push(headtitle);
					    }

					    let mergeCells = [];
					    let row,col,rowspan,colspan;
					    let headcount = 1; //表头层数
					    if(child1.length>0){
						    headcount++;
					    }
					    if(child2.length>0){
						    headcount++;
					    }
					    let currentCol = 0;

					    //计算表头合并格
					    for(let i=0;i<data.column.length;i++) {
						    let value = data.column[i];
						    //*********
						    if(value.children){//有子元素的
							    let childLeng = value.children.length;
							    mergeCells.push([columheadrow, currentCol, columheadrow, currentCol + childLeng -1 ]);


							    let headCol = currentCol;
							    for(let i=0;i<value.children.length;i++){


								    let childlen = 0;
								    if(value.children[i].children){

									    let childlen = value.children[i].children.length;
									    currentCol = currentCol+childlen;
								    }else if(childlen > 0){//子元素里面没有子元素的


									    mergeCells.push([columheadrow+1, currentCol, columheadrow+2, currentCol]);//cell
									    currentCol++;

								    }else {
									    currentCol++;
	                  }
							    }
						    }else{//没有子元素对象
							    mergeCells.push([columheadrow, currentCol, headcount-1, currentCol]);//currentCol
							    currentCol++;

						    }
					    }

					    if(parent.length>0){
						    child1.push(...parent);
					    }
					    if(child2.length>0&&parent1.length>0){
						    child2.push(...parent1);
					    }

					    rows.push(columHead);

					    if(child1.length>0 && self.state.flow){
						    rows.push(child1);
					    }
					    if(child2.length>0 && self.state.flow){
						    rows.push(child2);
					    }

					    if(data!=null && data.data.length>0){

						    let rowdata = [];
						    let flag = 0;
						    for(let i=0;i<data.data.length;i++){
							    flag++;
							    for(let j=0;j<colKeys.length;j++){
								    // {
								    //     "title":"11163",
								    //     "key":"pk_org",
								    //     "align":"left",
								    //     "style":"body"
								    // }
								    let itemObj = {
									    align: 'left',
									    style: 'body'
								    };
								    itemObj.title = data.data[i][colKeys[j]];
								    itemObj.key = colKeys[j] + flag;
								    // rowdata.push(data.data[i][colKeys[j]])
								    rowdata.push(itemObj)
							    }
							    rows.push(rowdata);
							    rowdata = [];
						    }
					    }

					    let rowhighs = [];
					    for (let i=0;i<rows.length;i++){
						    rowhighs.push(23);
					    }
					    self.state.dataout.data.cells= rows;//存放表体数据

					    self.state.dataout.data.colWidths=colWidths;
					    oldWidths.push(...colWidths);
					    self.state.dataout.data.oldWidths = oldWidths;
					    self.state.dataout.data.rowHeights=rowhighs;
					    let frozenCol;
					    for(let i=0;i<colAligns.length;i++){
						    if(colAligns[i]=='center'){
							    frozenCol = i;
							    break;
						    }
					    }
					    self.state.dataout.data.fixedColumnsLeft = frozenCol; //冻结
					    self.state.dataout.data.fixedRowsTop = headcount+columheadrow; //冻结

					    let quatyIndex = [];
					    for (let i=0;i<colKeys.length;i++){
						    if(colKeys[i].indexOf('quantity')>0){
							    quatyIndex.push(i);
						    }
					    }


					    if(mergeCells.length>0){
						    self.state.dataout.data.mergeInfo= mergeCells;//存放表头合并数据
					    }

					    let headAligns=[];

					    for(let i=0;i<headcount+columheadrow;i++){
						    for(let j=0;j<columHead.length;j++){
							    headAligns.push( {row: i, col: j, className: "htCenter htMiddle"});
						    }
					    }
					    self.state.dataout.data.cell= headAligns,
							    self.state.dataout.data.cellsFn=function(row, col, prop) {
								    let cellProperties = {};
								    if (row >= headcount+columheadrow||(columheadrow==1&&row==0)) {
									    cellProperties.align = colAligns[col];
									    cellProperties.renderer = negativeValueRenderer;//调用自定义渲染方法
								    }
								    return cellProperties;
							    }

					    self.setState({
						    dataout:self.state.dataout,
						    showTable: true,
					    });
		        	}


		        	self.setState({
		        		mydata
		        	})



		        }else {
		            toast({ content: message.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'danger' });
		        
		    }
		});
		
	}

	
	render() {

		let columns = this.state.tableAll.column;
		let data = this.state.tableAll.data;



		return (

			<div id="error">

				<div className="main-top" >
					<span className="main-title">现金流量分析表</span> 
					<Button colors="success">打印</Button>					
					{/*试算平衡*/}
					<Button  onClick={ this.goBack.bind(this) }>返回</Button>					
				</div>

				<div className="main-bottom">
					<span className="main-bottom-span">日期</span>
					<FormControl
					    className="main-bottom-input"
					    value={this.state.mydata.begindate + '--' + this.state.mydata.enddate}
					    disabled
					/> 

					<span className="main-bottom-span">核算账簿</span> 
					<FormControl
					    className="main-bottom-input"
					    value={this.state.mydata.bookname}
					    disabled
					/>

					<span className="main-bottom-span">本币</span> 
					<FormControl
					    className="main-bottom-input"
					    value={this.state.mydata.currtypeName}
					    disabled
					/>  

					
				</div>

                <div className='handsontableContainer'>
					<SimpleTable
						 ref="balanceTable"
						 data = {this.state.dataout}
					 />
				</div>


{/*				<Table
				  columns={columns}
				  className='main-table'
				  bordered
				  data={data}
				  style = {this.state.tableStyle}
				  emptyText={() => '无数据'}
				/>*/}

			</div>

			)
									
	}
}
