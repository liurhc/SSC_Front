import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';
// import moment from 'moment';

import {high,base,ajax,deepClone, createPage, getMultiLang,createPageIcon,gzip } from 'nc-lightapp-front';

const { Refer } = high;

const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCAffix,NCTooltip:Tooltip
} = base;

import { toast } from '../../../public/components/utils';
import {searchById} from "../../../manageReport/common/modules/createPureBtn";
import {handleSimpleTableClick, whetherDetail} from "../../../manageReport/common/modules/simpleTableClick";
import {setData} from "../../../manageReport/common/withPageDataSimbletable";
import {rowBackgroundColor} from '../../../manageReport/common/htRowBackground';

// const deepClone = require('../../../public/components/deepClone');
import DetailModal from './DetailModal';
import { SimpleTable } from 'nc-report';
import reportPrint from '../../../public/components/reportPrint';
import {tableDefaultData} from "../../../manageReport/defaultTableData";
import {getDetailPort} from "../../../manageReport/common/modules/modules";
import {cashFlowDetailSearch, cashFlowDetailSearchCode, relevanceSearch} from "../../../manageReport/referUrl";
import HeaderArea from '../../../public/components/HeaderArea';
class DetailNode extends Component {
	constructor(props) {
		super(props);
		this.state = {
			json: {},
			DetailModalShow: false, //明细节点模态框是否显示
			tableStyle: {},
			datas: {
				time: '',
				bookname: '',
				currtypeName: '',
			},
			tableAll: {
				column: [],
				data:[],
			},              //表格全体数据
			textAlginArr:[],//对其方式
            dataout: tableDefaultData,

		}
		this.searchById = searchById.bind(this);
		this.handleSimpleTableClick = handleSimpleTableClick.bind(this);
		this.whetherDetail = whetherDetail.bind(this);
		this.rowBackgroundColor = rowBackgroundColor.bind(this);
	}
    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20021CFRPT', 'childmodules'],domainName:'gl',currentLocale:'simpchn',callback});
    }

	componentDidMount(){
		this.setState({
            dataout: tableDefaultData
		})
        let appceod = this.props.getSearchParam('c');
        this.searchById('20021CFRPTPAGE','20021CFRPT');
        this.getParam();
        this.props.button.setDisabled({ refresh: true, directprint: true, linkdetail: true});
    }
    getParam = () => {
        let gziptools = new gzip();
        let data = this.props.getUrlParam && gziptools.unzip(this.props.getUrlParam('status'));
        //('datata:::',data);
        if(data){
            this.props.button.setButtonVisible({query: false});
            this.statuData = data;
            let paramData = data;
            this.getDatas(paramData)
        }
    }

	getDatas = (datas) => {
		let stateDatas = {
			time: datas.time,
			bookname: datas.bookname,
			currtypeName: datas.currtypeName
        }
		this.setState({
			datas: stateDatas,
            paramObj: datas
		})
		let self = this;
		let url = '/nccloud/gl/cashflow/cfanaquery.do';

		//开发环境用用友股份-核算账簿  测试环境用自建7703
		let data = {
			assvos: datas.assvos,
			pk_accountingbook: datas.pk_accountingbook,
			pk_unit: datas.pk_unit,
			"ismain": datas.ismain,
			"includetally": datas.includetally,
			"includestop": datas.includestop,
			pk_cashflow: datas.pk_cashflow,
			pk_accasoa: datas.pk_accasoa,
			pk_innercorp: datas.pk_innercorp,
			"pk_currtype": datas.pk_currtype,
			"returncurr": datas.returncurr,
			"begindate": datas.begindate,
			"enddate": datas.enddate,
			selectedrow: datas.selectedrow,
			needsummary: datas.needsummary,
		};
		let queryCond = data;
		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
                self.state.dataout = deepClone(tableDefaultData)
		        //(res.data);
		        const { data, message, success } = res;
		        if(success){
                    self.props.button.setDisabled({refresh: false, directprint: false});
		        	if (data) {
		        		let tableAll = deepClone(data);
		        		let widthAll = 0;
		        		tableAll.data.forEach(function (item) {
		        			item.key = item.pk_cashflow;
		        		});
		        		tableAll.column.forEach(function (item) {
		        			if (!item.children) {
		        				widthAll = widthAll + (+item.width);
		        				item.width = item.width + 'px';

		        			} else {
		        				item.children.forEach(function (item2) {
		        					widthAll = widthAll + (+item2.width);
		        					item2.width += 'px'
		        				})
		        			}
		        		});
		        		// self.setState({
		        		// 	tableAll,
		        		// 	tableStyle: {width: widthAll,},
		        		// 	queryCond,
		        		// })
    			        	if (data) {
    						    let rows = [];   //表格显示的数据合集
    						    let columHead = []; //第一层表头
    						    let parent = []; //存放一级表头，二级表头需要补上
    						    let parent1 = []; //存放二级表头，三级表头需要补上
    						    let child1 = []; //第三层表头
    						    let child2 = []; //第二层表头
    						    let colKeys = [];  //列key值，用来匹配表体数据
    						    let colAligns=[];  //每列对齐方式
    						    let colWidths=[];  //每列宽度
								let oldWidths=[];
								let textAlginArr = self.state.textAlginArr.concat([]);      //对其方式
								let multiHead = false;
                                tableAll.column.forEach(function (value) {
                                	if(value.children){
                                        multiHead = true;
									}
								})
                                tableAll.column.forEach(function (value) {//column:[]原始数据表头信息
    							    //('>>>>////', value);
    							    let valueCell = {};
    							    valueCell.title = value.title;
    							    valueCell.key = value.key;
    							    valueCell.align = 'center';
    							    valueCell.bodyAlign = 'left';
    							    valueCell.style = 'head';
    							    if(value.children){
    								    self.setState({
    									    flow: true
    								    })
    								    if(parent.length>0){
    									    child1.push(...parent);
    								    }
    								    for(let i=0;i<value.children.length;i++){
    									    if(value.children[i].children){
    										    child2.push(...parent1);
    										    for(let k=0;k<value.children[i].children.length;k++){
    											    let child1Cell = {};
    											    child1Cell.title = value.children[i].title;
    											    child1Cell.key = value.children[i].key;
    											    child1Cell.align = 'center';
    											    child1Cell.bodyAlign = 'left';
    											    child1Cell.style = 'head';
    											    let child2Cell = {};
    											    child2Cell.title = value.children[i].children[k].title;
    											    child2Cell.key = value.children[i].children[k].key;
    											    child2Cell.align = 'center';
    											    child2Cell.bodyAlign = 'left';
    											    child2Cell.style = 'head';
    											    columHead.push(valueCell);
    											    child1.push(child1Cell);
    											    child2.push(child2Cell);
    											    colKeys.push(value.children[i].children[k].key);
    											    colAligns.push(value.children[i].children[k].align);
    											    colWidths.push(value.children[i].children[k].width);
    										    }
    										    parent1 = [];
    										    //('jjjjjjj:', colWidths)
    									    }else {
    										    let innerCell2 = {};
    										    innerCell2.title = value.children[i].title;
    										    innerCell2.key = value.children[i].key;
    										    innerCell2.align = 'center';
    										    innerCell2.bodyAlign = 'left';
    										    innerCell2.style = 'head';
    										    columHead.push(valueCell);
    										    child1.push(innerCell2);
    										    parent1.push(innerCell2);
    										    colKeys.push(value.children[i].key);
    										    colAligns.push(value.children[i].align);
    										    colWidths.push(value.children[i].width);
    									    }
    								    }
    								    parent = [];
    							    }else  if(multiHead){
    								    let cellObj = {};
    								    cellObj.title = value.title;
    								    cellObj.key = value.key;
    								    cellObj.align = 'center';
    								    cellObj.bodyAlign = 'left';
    								    cellObj.style = 'head';
    								    columHead.push(valueCell);
    								    parent1.push(cellObj);
    								    parent.push(cellObj);
    								    colKeys.push(value.key);
    								    colAligns.push(value.align);
    								    colWidths.push(value.width);
    								    //('elses::', parent, parent1)
    							    }else if(!multiHead){
                                        columHead.push(valueCell);
                                        colKeys.push(value.key);
                                        colAligns.push(value.align);
                                        colWidths.push(value.width);
									}

    						    });
    						    //('>>>>?????',parent1, parent, child1, child2)
    						    let columheadrow = 0; //表头开始行
    						    if (data.headtitle){
    							    columheadrow = 1;
    							    let headtitle = [];
    							    data.headtitle.forEach(function (value) {
    								    headtitle.push(value[0]);
    								    headtitle.push(value[1]);
    							    });
    							    for(let i=headtitle.length;i<columHead.length;i++){
    								    headtitle.push('');
    							    }
    							    rows.push(headtitle);
    						    }

    						    let mergeCells = [];
    						    let row,col,rowspan,colspan;
    						    let headcount = 1; //表头层数
    						    if(child1.length>0){
    							    headcount++;
    						    }
    						    if(child2.length>0){
    							    headcount++;
    						    }
    						    let currentCol = 0;
    						    //('length::',rows.length, tableAll.data.length)
    						    //计算表头合并格
    						    for(let i=0;i<tableAll.column.length;i++) {
    							    let value = tableAll.column[i];
    							    //*********
    							    if(value.children){//有子元素的
    								    let childLeng = value.children.length;
    								    mergeCells.push([columheadrow, currentCol, columheadrow, currentCol + childLeng -1 ]);
    								    //('chuldLeng', childLeng, mergeCells)

    								    let headCol = currentCol;
    								    for(let i=0;i<value.children.length;i++){

    									    //('ccchildhdh', value.children[i].children);
    									    let childlen = 0;
    									    if(value.children[i].children){

    										    let childlen = value.children[i].children.length;
												currentCol = currentCol+childlen;
												for(let k=0;k<value.children[i].children.length;k++){
													textAlginArr.push(value.children[i].children[k].align);
												}
    									    }else if(childlen > 0){//子元素里面没有子元素的

    										    //('childlen<<', childlen)
    										    mergeCells.push([columheadrow+1, currentCol, columheadrow+2, currentCol]);//cell
    										    currentCol++;
    										    //('chuldLengif', childLeng, mergeCells)
    									    }else {
												currentCol++;
												textAlginArr.push(value.children[i].align);
    		                  }
    								    }
    							    }else{//没有子元素对象
    								    mergeCells.push([columheadrow, currentCol, headcount-1, currentCol]);//currentCol
										currentCol++;
										textAlginArr.push(value.align);
    								    //('currentCol', currentCol, mergeCells)
    							    }
    						    }
    						    //('mergeCells>>',this, mergeCells)
    						    if(parent.length>0){
    							    child1.push(...parent);
    						    }
    						    if(child2.length>0&&parent1.length>0){
    							    child2.push(...parent1);
    						    }

    						    rows.push(columHead);
    						    //('rrrrrrcolumHead:', rows, child1, child2,self.state.flow);
    						    if(child1.length>0 && self.state.flow){
    							    rows.push(child1);
    						    }
    						    if(child2.length>0 && self.state.flow){
    							    rows.push(child2);
    						    }
    						    //('rows>>>', rows);
    						    if(tableAll!=null && tableAll.data.length>0){
    							    //('colKeys>>>', colKeys);
    							    let rowdata = [];
    							    let flag = 0;
    							    for(let i=0;i<tableAll.data.length;i++){
    								    flag++;
    								    for(let j=0;j<colKeys.length;j++){
    									    //('data.data[i][colKeys[j]>', tableAll.data[i], tableAll.data[i][colKeys[j]]);
    									    // {
    									    //     "title":"11163",
    									    //     "key":"pk_org",
    									    //     "align":"left",
    									    //     "style":"body"
    									    // }
    									    let itemObj = {
    										    align: 'left',
    										    style: 'body'
    									    };
    									    itemObj.title = tableAll.data[i][colKeys[j]];
    									    itemObj.key = colKeys[j] + flag;
    									    // rowdata.push(data.data[i][colKeys[j]])
                                            itemObj.innercorp = tableAll.data[i].innercorp;
                                            itemObj.pk_detail  = tableAll.data[i].pk_detail;
                                            itemObj.pk_cashflow = tableAll.data[i].pk_cashflow;
                                            itemObj.align = colAligns[j]
                                            rowdata.push(itemObj)
    								    }
    								    rows.push(rowdata);
    								    rowdata = [];
    							    }
    						    }
    						    //('rowdata>>>', rows);
    						    let rowhighs = [];
    						    for (let i=0;i<rows.length;i++){
    							    rowhighs.push(23);
    						    }
                                tableAll.data.cells= rows;//存放表体数据
    						    //('settings.cells', tableAll.data.cells)
                                tableAll.data.colWidths=colWidths;
    						    oldWidths.push(...colWidths);
                                tableAll.data.oldWidths = oldWidths;
                                tableAll.data.rowHeights=rowhighs;
    						    let frozenCol;
    						    for(let i=0;i<colAligns.length;i++){
    							    if(colAligns[i]=='center'){
    								    frozenCol = i;
    								    break;
    							    }
    						    }
                                tableAll.data.fixedColumnsLeft = frozenCol; //冻结
								tableAll.data.fixedRowsTop = headcount+columheadrow; //冻结
								tableAll.data.freezing = {//固定表头
									"isFreeze" : true,
									"row": tableAll.data.fixedRowsTop,
									"col" : tableAll.data.fixedColumnsLeft
								}
    						    //('>>>', tableAll.data.fixedRowsTop)
    						    let quatyIndex = [];
    						    for (let i=0;i<colKeys.length;i++){
    							    if(colKeys[i].indexOf('quantity')>0){
    								    quatyIndex.push(i);
    							    }
    						    }


    						    if(mergeCells.length>0){
                                    tableAll.data.mergeInfo= mergeCells;//存放表头合并数据
    						    }
    						    //('settings.mergeInfo', tableAll.data.mergeInfo);
    						    let headAligns=[];

    						    for(let i=0;i<headcount+columheadrow;i++){
    							    for(let j=0;j<columHead.length;j++){
    								    headAligns.push( {row: i, col: j, className: "htCenter htMiddle"});
    							    }
    						    }
                                tableAll.data.cell= headAligns,
                                    tableAll.data.cellsFn=function(row, col, prop) {
    									    let cellProperties = {};
    									    if (row >= headcount+columheadrow||(columheadrow==1&&row==0)) {
    										    cellProperties.align = colAligns[col];
    										    cellProperties.renderer = negativeValueRenderer;//调用自定义渲染方法
    									    }
    									    return cellProperties;
    								    }
    						    //('>>>>result', tableAll.data);
    						    self.setState({
    							    dataout:tableAll,
									showTable: true,
                                    DetailModalShow: false,
									textAlginArr: colAligns,
									selectRowIndex:0
    						    });
    			        	}

		        	}else{
		        		//('else:::::', tableDefaultData);
		        		let newTableDefaultData = deepClone(tableDefaultData)
		        		self.setState({
                            DetailModalShow: false,
							dataout: {...newTableDefaultData},
							selectRowIndex:0
						})
						toast({content: self.state.json['20021CFRPT-000036'], color: 'warning'})//请求数据为空！
					}



		        }else {
		            toast({ content: message.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'danger' });
		        
		    }
		});

	}

	queryShow = () => {
		this.setState({
			DetailModalShow: true,
		})
	}

	//直接输出
    printExcel=()=>{
        let {textAlginArr,dataout} = this.state;
        let {cells,mergeInfo} = dataout.data;
        let dataArr = [];
        cells.map((item,index)=>{
            let emptyArr=[];
            item.map((list,_index)=>{
                if(list){
                    emptyArr.push(list.title);
                }
            })
            dataArr.push(emptyArr);
        })
        reportPrint(mergeInfo,dataArr,textAlginArr);
    }

    //联查明细页
    goDetail = () => {
        getDetailPort(this, {
            jumpTo: cashFlowDetailSearch,
            key: 'cashFlowDetailSearch',
            appcode: cashFlowDetailSearchCode,
            searchParam: this.state.paramObj,
			nodeParam: 'analysisNode'
        }, this.state.json)
    }

    handleLoginBtn = (obj, btnName) => {
		//('btnName>', btnName)
        if(btnName === 'query'){//1、查询
            this.queryShow()
        }else if(btnName === 'linkdetail'){//2、明细
            this.goDetail()
        }else if(btnName === 'directprint'){//3、直接输出
            this.printExcel()
        }else if(btnName === 'refresh'){//刷新
			if(this.state.paramObj){
                this.getDatas(this.state.paramObj)
			}else{
				toast({content: this.state.json['20021CFRPT-000035'], color: 'warning'})//请先查询数据！
			}
		}
	}

	render() {
		//('render::', this.state, this.statuData, this.statuData && this.statuData.accountbookObj)
		return (
			<div id="error" className='manageReportContainer'>
				<HeaderArea 
                    title = {this.state.json['20021CFRPT-000031']} /* 国际化处理： 现金流量分析表*/
                    btnContent = {this.props.button.createButtonApp({
                        area: 'head',
						buttonLimit: 3,
						onButtonClick: (obj, tbnName) => this.handleLoginBtn(obj, tbnName)
                    })}
                />
                <div className="m-analysis-main-bottom nc-theme-gray-area-bgc">
                    <div className={"m-analysis-main-bottom-div"}>
						<span className="main-bottom-span nc-theme-common-font-c">{this.state.json['20021CFRPT-000032']}</span>{/*核算账簿*/}
						<Tooltip trigger="hover" placement="top" inverse={false} overlay={ (this.statuData && this.statuData.accountbookObj && this.statuData.accountbookObj[0].refname) || this.state.datas.bookname}>
							<span>
								<FormControl
									className="main-bottom-input m-main-bottom-input m-main-bottom-input-long "
									value={ (this.statuData && this.statuData.accountbookObj && this.statuData.accountbookObj[0].refname) || this.state.datas.bookname}
									disabled
								/>
							</span>
              			</Tooltip>
                    </div>
                    <div className={"m-analysis-main-bottom-div"}>
						<span className="main-bottom-span nc-theme-common-font-c">{this.state.json['20021CFRPT-000033']}</span>{/*日期*/}
						<Tooltip trigger="hover" placement="top" inverse={false} overlay={(this.statuData && `${this.statuData.begindate}-${this.statuData.enddate}`) || this.state.datas.time}>
							<span>
								<FormControl
									className="main-bottom-input m-main-bottom-input"
									value={(this.statuData && `${this.statuData.begindate}-${this.statuData.enddate}`) || this.state.datas.time}
									disabled
								/>
							</span>
              			</Tooltip>
                    </div>
                    <div className={"m-analysis-main-bottom-div"}>
						<span className="main-bottom-span nc-theme-common-font-c">{this.state.json['20021CFRPT-000024']}</span>{/*币种*/}
						<Tooltip trigger="hover" placement="top" inverse={false} overlay={this.state.paramObj && this.state.paramObj.currtypeName}>
							<span>
								<FormControl
									className="main-bottom-input m-main-bottom-input"
									value={this.state.paramObj && this.state.paramObj.currtypeName}
									disabled
								/>
							</span>
              			</Tooltip>
                    </div>
                </div>

                <div className='report-table-area'>
					<SimpleTable
						ref="balanceTable"
						data = {this.state.dataout}
						onCellMouseDown = {(e, coords, td) => {
							//('recorddd:::', this.refs.balanceTable.getRowRecord());
                            this.whetherDetail('pk_detail',);
                            this.rowBackgroundColor(e, coords, td)

						}}
					/>
				</div>

                <DetailModal
                    show={this.state.DetailModalShow}

                    ref='MainSelectModal'
                    title={this.state.json['20021CFRPT-000034']}//{/*查询条件*/}
                    icon=""
                    onConfirm={(datas) => {
                        this.getDatas(datas);
                    }}

                    onCancel={() => {
                        let DetailModalShow = false;
                        this.setState({ DetailModalShow });
                    }}
                />
            </div>
        )
	}
}

DetailNode = createPage({})(DetailNode)

export default DetailNode
