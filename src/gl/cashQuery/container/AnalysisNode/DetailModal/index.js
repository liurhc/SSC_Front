import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';
// import moment from 'moment';

import {high,base,ajax,deepClone, createPage, getMultiLang } from 'nc-lightapp-front';
import './index.less';
import  '../../../../public/reportcss/searchmodalpage.less'

const { Refer } = high;

const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCModal: Modal,NCInput: Input
} = base;

import { toast } from '../../../../public/components/utils';

const NCOption = Select.NCOption;
// const deepClone = require('../../../../public/components/deepClone');
import createScript from '../../../../public/components/uapRefer.js';
const format = 'YYYY-MM-DD';
const Timeformat = "YYYY-MM-DD HH:mm:ss";

import AssistModal from './AssistModal';
// import AccountBookByFinanceOrgRef from '../../../../../uapbd/refer/org/AccountBookByFinanceOrgRef';
import CashflowTreeRef from '../../../../../uapbd/refer/fiacc/CashflowTreeRef';
import AccountDefaultModelTreeRef from '../../../../../uapbd/refer/fiacc/AccountDefaultModelTreeRef';
import FinanceOrgAllGroupAllDataTreeRef from '../../../../../uapbd/refer/org/FinanceOrgAllGroupAllDataTreeRef';
import {
    businessUnit,
    createReferFn,
    getCheckContent, getReferAppCode,
    getReferDetault, renderMoneyType, renderRefer, returnMoneyType
} from "../../../../manageReport/common/modules/modules";
import '../../../../manageReport/css/searchModal/index.less';
import {subjectRefer} from "../../../../manageReport/referUrl";
import BusinessUnitTreeRef from "../../../../../uapbd/refer/orgv/BusinessUnitVersionDefaultAllTreeRef";
// import createScript from '../../../../public/components/uapRefer.js';
import {handleNumberInput} from "../../../../manageReport/common/modules/numberInputFn";
import DateGroup from "../../../../manageReport/common/modules/dateGroup";
import ReferWraper from '../../../../public/components/ReferWraper'

class CentralConstructorModal extends Component {

	constructor(props) {
		super(props);
		this.state={
		    json: {},
            accAssItems: [],
            stringRefValue: '',// 参照字符
            numberRefValue: '',//参照数值
            numberRefValueInteger: '', //参照整数
			assvos: [], //最终的辅助核算项
            buSecond: [],
			assDisplay: '', //选择辅助核算按钮/* 国际化处理： 选择辅助核算*/
			AssistModalShow: false,
			isGlobal: false, //是否启用全局本币
			isGroup: false, //是否启用集团本币
			accountingbook: {
				refname: '',
				refpk: '',
			},  //核算账簿
			cashflow:[],
			inner: [],
            rangeDate: [],
			selectedQueryValue: '0', //查询方式单选
			selectedCurrValue: '1', //返回币种单选
			ismain: '0', // 0主表  1附表
			begindate: '', //开始时间
			enddate: '', //结束时间
			currtype: '',      //选择的币种/* 国际化处理： 本币*/
			currtypeName: '',  //选择的币种名称
			currtypeList: [],  //币种列表
			defaultCurrtype: {         //默认币种
				name: '',
				pk_currtype: '',
			},
			checkbox: {
				one: false,
				two: false,
				three: false,
			},
			checkboxTable: {
				cash: true,    //现金流量项目
				cashSum: false,
				accasoa: false,  //科目
				accasoaSum: false,
				ass: false,    //辅助核算
				assSum: false,
				inner: false,  //内部单位
				innerSum: false
	
			},
			accasoa: [],
            secondData: [],//第二层表格的数据
		}

		this.renderRefer = renderRefer.bind(this);
		this.handleNumberInput = handleNumberInput.bind(this);
		this.showAss = this.showAss.bind(this);

	}

	static defaultProps = {
		show: false,
		title: '',
		content: '',
		closeButton: false,
		icon: 'icon-tishianniuchenggong',
		isButtonWhite: true,
		isButtonShow: true,
		// accAssItems: [],
	};

    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json,
                currtype: json['20021CFRPT-000002'],      //选择的币种/* 国际化处理： 本币*/
                currtypeName: json['20021CFRPT-000002'],  //选择的币种名称
                dateInputPlaceholder: json['20021CFRPT-000000'],/* 国际化处理： 选择日期*/
                assDisplay: json['20021CFRPT-000001'], //选择辅助核算按钮/* 国际化处理： 选择辅助核算*/
                secondColumn: [
                {
                    title: (<div fieldid='select'>{json['20021CFRPT-000003']}</div>),/* 国际化处理： 选择*/
                    dataIndex: "select",
					key: "select",
					width: 100,
                    render: (text, record, index) => {
                        //('secondColumn>', text, record)
                        return <div fieldid='select'>
							<Checkbox
								checked={record.select}
								onChange={() => {
									//('acceleChange:',record);
									record.index = index;
									this.secondModalSelect(record, index);
								}}
							/>
						</div>
                    }
                },
                {
                    title: (<div fieldid='name'>{json['20021CFRPT-000004']}</div>),/* 国际化处理： 辅助内容名称*/
                    dataIndex: "name",
					key: "name",
					render: (text, record, index) => <div fieldid='name'>{text ? text : <span>&nbsp;</span>}</div>
                },
                {
                    title: (<div fieldid='refnodename'>{json['20021CFRPT-000005']}</div>),/* 国际化处理： 辅助核算名称*/
                    dataIndex: "refnodename",
                    key: "refnodename",
                    render: (text, record) => {
                        //('refpath>>>>', text, record);
                        let renderSecondEle = this.renderNewRefPath(record, 'refnodename');
                        return renderSecondEle;
                    }
                }
            ]
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20021CFRPT', 'dategroup', 'childmodules'],domainName:'gl',currentLocale:'simpchn',callback});
        this.getCurrtypeList();
    }

    secondModalSelect(record, index){//第二层弹框的复选框
        //('secondModalSelect>', record);
        let {tableSourceData} = this.state;
        let newSelect = !record.select;
        record.select = newSelect;

        this.setState({tableSourceData},() => {
            //('repssss:::', this.state);
        })
    }

    dateRefChange = (key, value, record) => {
        let length = record.inputlength;
        let exeReg = new RegExp("^.{0," + length + "}$");
        let result = exeReg.exec(value);
        //('dateRefChange>',value, record, result);
        if(result && key === 'stringRefValue'){
            record.selectRange = result[0]
            this.setState({
                [key]: result[0]
            })
        }else if(key != 'stringRefValue') {
            record.selectRange = value
            this.setState({
                [key]: value
            })
        }
    }
    numberRefChange = (key, value, record, param) => {
        //('numberRefChange>>', value, record);
        this.handleNumberInput(key, value, param, record);
        setTimeout(() => record.selectRange = this.state[key], 0)

    }
    renderNewRefPath = (record, fieldid) => {
        //('renderNewRefPath>', record, record.queryname);
        let mybook;

        if(record.refpath){
            let objKey = record.pk_checktype;
            if(!this.state[objKey]){//undefined
                //('createScript:', this.state);
                {createScript.call(this,record.refpath+".js",objKey)}

            }else {
                let pkOrgParam = this.state.buSecond.length > 0 ? this.state.buSecond[0].refpk : this.state.unitValueParam;
                //('rendertableRight>:::', this.state, this.state[objKey]);
                let queryParam = {
                    "pk_accountingbook": this.state.accountingbook && this.state.accountingbook.refpk,
                    "pk_org": pkOrgParam,
                    "isDataPowerEnable": 'Y',
                    "DataPowerOperationCode" : 'fi'
                }
                if(record.classid.length == 20){
                    queryParam.pk_defdoclist = record.classid
				}
                let options = {
					fieldid: fieldid,
                    value: record.selectCells,
                    isMultiSelectedEnabled: true,
                    queryCondition: {...queryParam},
                    "isShowDisabledData": true,
                    onChange: (v) => {
                        //('vvvvvv:::', v)
                        let {tableSourceData} = this.state;
                        let refpkArr = [];
                        let selectName = [];
                        v.forEach((item) => {
                            //('vFormach::', item);
                            refpkArr.push(item.refpk);
                            selectName.push(item.refname)
                        })
                        record.selectCells = v
                        let pk_checkvalue = refpkArr.join(',')
                        record.pk_checkvalue = pk_checkvalue;
                        record.selectName = selectName;
                        record.refpk = refpkArr;
                        this.setState({
                            tableSourceData
                        })
                    }
                };
                let newOPtions = {}
                if(record.classid=='b26fa3cb-4087-4027-a3b6-c83ab2a086a9'||record.classid=='40d39c26-a2b6-4f16-a018-45664cac1a1f') {//部门，人员
                    //('recorddd>>???', record);
                    newOPtions = {
						...options,
						"unitValueIsNeeded":false,
						"isShowDimission":true,
                        queryCondition: {
                            ...queryParam,
							"busifuncode":"all",
							isShowDimission:true//显示离职人员
                        },
                        isShowUnit:true,
                        unitProps:{
                            refType: 'tree',
                            refName: this.state.json['20021CFRPT-000006'],/* 国际化处理： 业务单元*/
                            refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                            rootNode:{refname:this.state.json['20021CFRPT-000006'],refpk:'root'},/* 国际化处理： 业务单元*/
                            placeholder:this.state.json['20021CFRPT-000006'],/* 国际化处理： 业务单元*/
                            queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                            treeConfig:{name:[this.state.json['20021CFRPT-000007'], this.state.json['20021CFRPT-000008']],code: ['refcode', 'refname']},/* 国际化处理： 编码,名称*/
                            isMultiSelectedEnabled: false,
                            //unitProps:unitConf,
                            isShowUnit:false
                        },
                        unitCondition:{
                            pk_financeorg: pkOrgParam,
                            'TreeRefActionExt':'nccloud.web.gl.ref.OrgRelationFilterRefSqlBuilder'
                        },
                    }
                }else {
                    newOPtions = {
                        ...options
                    }
                }
                mybook = (
                    <Row>
                        <Col xs={12} md={12}>
                            {
                                this.state[objKey] ? (this.state[objKey])(newOPtions) : <div/>
                            }
                        </Col>
                    </Row>
                );
            }
        }else if(record.datatype === '1'){//字符
            return <Input
				fieldid = {fieldid}
                value={this.state.stringRefValue}
                onChange={(value) => this.dateRefChange('stringRefValue', value, record)}
            />;
        } else if(record.datatype === '4'){//整数
            return <Input
				fieldid = {fieldid}
				value={this.state.numberRefValueInteger}
                onChange={(value) => this.numberRefChange('numberRefValueInteger', value, record, 'assistBalanceInteger') }
            />;
        } else if(record.datatype === '31'){//数值(小数)
            return <Input
				fieldid = {fieldid}
				value={this.state.numberRefValue}
                onChange={(value) => this.numberRefChange('numberRefValue', value, record, 'assistBalance') }
            />;
        } else if(record.datatype === '32'){//布尔
            return <Select
				fieldid = {fieldid}
				value={this.state.booleanValue}
                className='search-boolean'
                onChange={(value)=>{
                    //('NCSelect>>>', value);
                    this.dateRefChange('booleanValue', value, record)
                }}
            >
                <NCOption value="Y">{this.state.json['20021CFRPT-000011']}</NCOption>{/* 国际化处理： 是*/}
                <NCOption value="N">{this.state.json['20021CFRPT-000012']}</NCOption>{/* 国际化处理： 否*/}
                <NCOption value="">{this.state.json['20021CFRPT-000013']}</NCOption>{/* 国际化处理： 空*/}
            </Select>;
        } else if(record.datatype === '33'){//日期
            return (
                <DatePicker
					format={format}
					fieldid = {fieldid}
                    value={this.state.assistDateValue}
                    placeholder={this.state.dateInputPlaceholder}
                    onChange={(value) => this.dateRefChange('assistDateValue', value, record)}
                />
            );
        } else if(record.datatype === '34'){//日期+时分秒时间
            return (
                <DatePicker
					fieldid = {fieldid}
                    showTime={true}
                    format={Timeformat}
                    value={this.state.assistDateTimeValue}
                    placeholder={this.state.dateInputPlaceholder}
                    onChange={(value) => this.dateRefChange('assistDateTimeValue', value, record)}
                />
            );
        }

        return mybook;
    }

    getAss = (pk_accasoa, prepareddate, pk_accountingbook) => {
        let self = this;
        let url = '/nccloud/gl/accountrep/assbalancequeryobject.do';
        let data = {
            pk_accountingbook: pk_accountingbook,
            "needaccount":false
        };

        ajax({
            loading: true,
            url,
            data,
            success: function (res) {
                //('success???',res.data);
                const { data, error, success } = res;
                if(success){
                    if (data) {
                        let newList = [];
                        for (var i = 0; i < res.data.length; i++) {
                            newList.push({
                                newValues: {
                                    refname: '',
                                    refpk: '',
                                }

                            })
                        }
                        self.setState({
                            accAssItems: res.data,
                            newList,
                            pk_accountingbook,
                        })
                    }
                }else {
                    toast({ content: error.message, color: 'warning' });
                }
            },
            error: function (res) {
                toast({ content: res.message, color: 'warning' });

            }
        });
    }

	componentDidMount() {
        getReferAppCode(this, 'appcode');
        setTimeout(() => getReferDetault(this, false, {
            businessUnit
        }, 'CashQuery'),0)
	}

	componentWillReceiveProps(nextProps) {
		// this.getEndAccount(nextProps);
	}

	handleRadioChange(value) {
		this.setState({selectedQueryValue: value});
	}

	handleRadioCurrChange(value) {
		this.setState({selectedCurrValue: value});
	}


	showAss = () => {
		//(this.state.json['20021CFRPT-000009'])/* 国际化处理： 显示辅助核算参照*/
		let prepareddate = '';
		let pk_accasoa = '';
		let pk_accountingbook = this.state.accountingbook.refpk;

		if(this.state.accAssItems.length === 0){
            this.getAss(pk_accasoa, prepareddate, pk_accountingbook);
        }

		this.setState({
			AssistModalShow: true,
		})
	}




	getCurrtypeList = () => {
		let self = this;
		let url = '/nccloud/gl/voucher/queryAllCurrtype.do';
		// let data = '';
		// let data = {};
		let data = {
			"localType":"1",
            "showAllCurr":"1"
		};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		        const { data, error, success } = res;
		        if(success){
					self.setState({
						currtypeList: data,
						currtype: self.state.json['20021CFRPT-000002'],/* 国际化处理： 本币*/
					})
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});

	}

	// 辅助核算模态框
	handleAssistModal = (MsgModalAll, assDatas, row) => {
		//('handleAssistModal>>>>', MsgModalAll, assDatas)
		//todo 增加按钮里显示文字

			let assvos = [];
			let jsonData = [];
			let assDisplay = this.state.json['20021CFRPT-000001'];/* 国际化处理： 选择辅助核算*/
			this.setState({
				AssistModalShow: false,
			});
        assDatas.map((item, index) => {
        	if(item.selectItem){
                assvos.push({
                    pk_checktype: item.selectItem.pk_Checktypes,
                    pk_checkvalue: item.selectItem.selectValues,
                })
			}
		})
			for (var i = 0; i < assDatas.length; i++) {
				if(assDatas[i].selectValues) {
					assvos.push({
						pk_checktype: assDatas[i].pk_Checktypes,
						pk_checkvalue: assDatas[i].selectValues,
					})

					jsonData.push({
						checktypecode: assDatas[i].codes,
						checktypename: assDatas[i].names,
						checkvaluecode: assDatas[i].selectCodes,
						checkvaluename: assDatas[i].selectNames,
						pk_Checktype: assDatas[i].pk_Checktypes,
						pk_Checkvalue: assDatas[i].selectValues,
						m_classid: assDatas[i].classids,

					})
				}

			}

			assDisplay =  (assDatas.map(function(item,index){
			        		//(assDatas)
			        		return `【${item.names}:${item.selectNames}】`
			        	})).toString(); 

			this.setState({
				assvos,
				assDisplay,
			})
	};


	handleCurrtypeChange(value) {
	    //('handleCurrtypeChange>', value);
		let self = this;
		// if (!this.state.accountingbook || !this.state.accountingbook.refpk) {
		// 	toast({ content: '请先选择核算账簿', color: 'warning' });
		// 	return;
		// }
		let currtypeList = this.state.currtypeList;
		currtypeList.forEach(function (item, index) {
			if (item.pk_currtype == value ) {
				self.setState({
					currtypeName: item.name,
				})
			}
		})
		this.setState({
			currtype: value,
		// }, this.getTables);
		}, this.getRates);
	}


	handleIsmainChange(value) {
		this.setState({
			ismain: value,
		})
	}


	changeCheck=()=> {
		// this.setState({checked:!this.state.checked});
	}

	onChange1(e) {
	    //(e);
	    let checkbox = deepClone(this.state.checkbox);
	    checkbox.one = e;
	    this.setState({checkbox});
	}

	onChange2(e) {
	    //(e);
	    let checkbox = deepClone(this.state.checkbox);
	    checkbox.two = e;
	    this.setState({checkbox});
	}

	onChange3(e) {
	    //(e);
	    let checkbox = deepClone(this.state.checkbox);
	    checkbox.three = e;
	    this.setState({checkbox});
	}



	onChangeCash(e) {
		let checkboxTable = deepClone(this.state.checkboxTable);
		checkboxTable.cash = e;
		this.setState({checkboxTable});
	}

	onChangeCashSum(e) {
		let checkboxTable = deepClone(this.state.checkboxTable);
		checkboxTable.cashSum = e;
		this.setState({checkboxTable});
	}

	onChangeAccasoa(e) {
		let checkboxTable = deepClone(this.state.checkboxTable);
		checkboxTable.accasoa = e;
		if (e == false) {
			checkboxTable.accasoaSum = e;
            checkboxTable.ass = e
		}
		this.setState({
			checkboxTable: {...checkboxTable}
		});
	}

	onChangeAccasoaSum(e) {
		let checkboxTable = deepClone(this.state.checkboxTable);
		checkboxTable.accasoaSum = e;
		this.setState({checkboxTable});
	}

	onChangeAss(e) {//accasoa:会计科目
		let checkboxTable = deepClone(this.state.checkboxTable);
		checkboxTable.ass = e;
		if (e == false) {
			checkboxTable.assSum = e;
		}
		if(e){
			checkboxTable.accasoa = e
		}
		this.setState({
			checkboxTable: {...checkboxTable}
		});
	}

	onChangeAssSum(e) {
		let checkboxTable = deepClone(this.state.checkboxTable);
		checkboxTable.assSum = e;
		this.setState({checkboxTable});
	}

	onChangeInner(e) {
		let checkboxTable = deepClone(this.state.checkboxTable);
		checkboxTable.inner = e;
		if (e == false) {
			checkboxTable.innerSum = e;
		}
		this.setState({checkboxTable});
	}

	onChangeInnerSum(e) {
		let checkboxTable = deepClone(this.state.checkboxTable);
		checkboxTable.innerSum = e;
		this.setState({checkboxTable});
	}

	
	//起始时间变化
	onStartChange(d) {
		//(d)
		this.setState({
			begindate:d
		})
	}
	
	//结束时间变化
	onEndChange(d) {
		this.setState({
			enddate:d
		})
	}

	getInfoByBook() {

		if (!this.state.accountingbook || !this.state.accountingbook.refpk) {
			return;
		}
		
		let accountingbook = {
			refname: this.state.accountingbook.refname,
			refpk: this.state.accountingbook.refpk,
		};

		let self = this;
		let url = '/nccloud/gl/voucher/queryAccountingBook.do';
		let data = {
			pk_accountingbook: this.state.accountingbook.refpk,	
			year: '',		
		};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		    	const { data, error, success } = res;

		        if(success){

		        	let isGlobal = false;   //是否启用全局本币
		        	let isGroup = false;    //是否启用集团本币
		        	if (data.globalCurrType && data.globalCurrType.pk_currtype) {
		        		isGlobal = true;
		        	}

		        	if (data.groupCurrType && data.groupCurrType.pk_currtype) {
		        		isGroup = true;
		        	}

		        	self.setState({
		        		isGlobal,
		        		isGroup,
		        	}, )
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});

		// this.getYears()

	}

    handleValueChange = (key, value) => {
		this.setState({
			[key]: value
		})
	}

    handleDateChange = (value) => {//日期: 范围选择触发事件
        //('handleDateChange:', value);
        this.setState({
            begindate: value[0],
            enddate: value[1],
            rangeDate: value
        })
    }

    setSelectCell = () => {
	    this.showSelectArr = [];
	    this.state.accAssItems.map((itemCell, index) => {
            if (itemCell.select) {
                let showitem = "【" + itemCell.name + ":";
                if (!itemCell.selectName || itemCell.selectName === '' || (Array.isArray(itemCell.selectName) && itemCell.selectName.length == 0)) { // 不选值，即为全选
                    showitem += "ALL";
                } else { // 参照的单选和多选
                    if (Array.isArray(itemCell.selectName)) {
                        showitem += itemCell.selectName.join(',');
                    } else {
                        showitem += itemCell.selectName;
                    }
                }
                showitem += "】";
                this.showSelectArr.push(showitem);
            }
        })

    }

	render() {
		let { show, title, isButtonShow}= this.props;
		//('s>>>', this.state, this.state.accAssItems)
		return <Modal
			fieldid='query'
            show={ show }
            onHide={()=>this.props.onCancel(true)}
            className='cashQuery'
        >
            <Modal.Header closeButton>
                <Modal.Title>
                    {title}
                </Modal.Title>
            </Modal.Header >
			<Modal.Body id="modalOuter">
				<div className='right_query noserchmatter'>
					<div className='query_body1'>
						<div className='query_form'>
							<Row className="myrow">
								<Col md={2} sm={2}>
									<span style={{ color: 'red' }}>*</span>
									<span className="nc-theme-form-label-c">{this.state.json['20021CFRPT-000014']}：</span>{/* 国际化处理： 财务核算账簿*/}
								</Col>
								<Col md={10} sm={10}>
									<div className="book-ref">
										{
											createReferFn(
												this,
												{
													url: 'uapbd/refer/org/AccountBookTreeRef/index.js',
													value: this.state.accountingbook,
													referStateKey: 'checkAccountBook',
													referStateValue: this.state.checkAccountBook,
													stateValueKey: 'accountingbook',
													flag: false,
													queryCondition: {
														pk_accountingbook: this.state.accountingbook && this.state.accountingbook.refpk,
														"isDataPowerEnable": 'Y',
														"DataPowerOperationCode": 'fi'
													}
												},
												{
													businessUnit: businessUnit
												},
												'AnalysisNode'
											)
										}
									</div>
								</Col>
							</Row>
							{
								this.state.isShowUnit === 'Y' ?
									<Row className="myrow">
										<Col md={2} sm={2}>
											<span className='nc-theme-form-label-c'>{this.state.json['20021CFRPT-000006']}：</span>{/* 国际化处理： 业务单元*/}
										</Col>
										<Col md={10} sm={10}>
											<div className="book-ref">
												<BusinessUnitTreeRef
													fieldid='buSecond'
													value={this.state.buSecond}
													isMultiSelectedEnabled={true}
													isShowDisabledData={true}
													queryCondition={{
														"pk_accountingbook": this.state.accountingbook && this.state.accountingbook.refpk,
														"isDataPowerEnable": 'Y',
														"DataPowerOperationCode": 'fi',
														"TreeRefActionExt": 'nccloud.web.gl.ref.GLBUVersionWithBookRefSqlBuilder'
													}}
													onChange={(v) => {
														//(v);
														this.setState({
															buSecond: v,
														}, () => {
															if (this.state.accountingbook && this.state.accountingbook[0]) {
															}
														})
													}
													}
												/>

											</div>

										</Col>
									</Row> :
									<div></div>
							}

							<Row className="myrow">
								<Col md={2} sm={2}>
									<span className='nc-theme-form-label-c'>{this.state.json['20021CFRPT-000015']}：</span>{/* 国际化处理： 主附表*/}
								</Col>

								{/*主附表下拉*/}
								<Col md={4} sm={4}>
									<div className="book-ref">
										<Select
											fieldid='ismain'
											value={this.state.ismain}
											showClear={false}
											// style={{ width: 150, marginRight: 6 }}
											onChange={this.handleIsmainChange.bind(this)}
										>
											<NCOption value={'0'} key={'0'} >{this.state.json['20021CFRPT-000016']}</NCOption>{/* 国际化处理： 主表*/}
											<NCOption value={'1'} key={'1'} >{this.state.json['20021CFRPT-000017']}</NCOption>{/* 国际化处理： 附表*/}

										</Select>

									</div>
								</Col>

							</Row>
							<div className="cashquerytable nc-theme-area-split-bc">
								<Row className="myrow-table">

									<Row>
										<Col md={2} sm={2}>
											<div className='nc-theme-common-font-c'>{this.state.json['20021CFRPT-000003']}</div>{/* 国际化处理： 选择*/}
										</Col>
										<Col md={3} sm={3}>
											<div className='nc-theme-common-font-c'>{this.state.json['20021CFRPT-000018']}</div>{/* 国际化处理： 查询对象*/}
										</Col>

										<Col md={4} sm={4}>
											<div className='nc-theme-common-font-c'>{this.state.json['20021CFRPT-000019']}</div>{/* 国际化处理： 查询范围*/}
										</Col>

										<Col md={3} sm={3}>
											<div className='nc-theme-common-font-c'>{this.state.json['20021CFRPT-000020']}</div>{/* 国际化处理： 计算小计*/}
										</Col>
									</Row>
									<Row >

										{/*现金流量项目*/}
										<Col md={2} sm={2}>
											<Checkbox colors="dark" className="mycheck" checked={this.state.checkboxTable.cash} disabled onChange={this.onChangeCash.bind(this)}></Checkbox>
										</Col>
										<Col md={3} sm={3}>
											<div className='nc-theme-common-font-c'>{this.state.json['20021CFRPT-000021']}</div>{/* 国际化处理： 现金流量项目*/}
										</Col>
										<Col md={4} sm={4}>
											<CashflowTreeRef
												value={this.state.cashflow}
												isMultiSelectedEnabled={true}
												//现金流量过滤条件后端是怎么定的？
												queryCondition={() => {
													return {
														"versiondate": '',
														"pk_accountingbook": this.state.accountingbook.refpk,
														"isDataPowerEnable": 'Y',
														"DataPowerOperationCode": 'fi',
														'pk_org': this.state.unitValueParam,
														ismain: this.state.ismain == '0' ? 'Y' : 'N',
														TreeRefActionExt: 'nccloud.web.gl.ref.CashFlowTypeRefSqlBuilder',
													}
												}}
												onChange={(v) => {
													//(v);

													this.setState({
														cashflow: v,
													})

												}
												}

											/>
										</Col>
										<Col md={3} sm={3}>
											<Checkbox colors="dark" className="mycheck" checked={this.state.checkboxTable.cashSum} onChange={this.onChangeCashSum.bind(this)}></Checkbox>
										</Col>

									</Row>
									<Row>
										{/*会计科目*/}
										<Col md={2} sm={2}>
											<Checkbox
												colors="dark"
												className="mycheck"
												checked={this.state.checkboxTable.accasoa}
												onChange={this.onChangeAccasoa.bind(this)}
											/>
										</Col>

										<Col md={3} sm={3}>
											<div className='nc-theme-common-font-c'>{this.state.json['20021CFRPT-000022']}</div>{/* 国际化处理： 会计科目*/}
										</Col>
										<Col md={4} sm={4}>
											{
												this.renderRefer(
													subjectRefer,
													this.state.accasoa,
													'subjectReferStateKey',
													this.state['subjectReferStateKey'],
													'accasoa',
													true,
													{
														"pk_accountingbook": this.state.accountingbook && this.state.accountingbook.refpk,
														"isDataPowerEnable": 'Y',
														"DataPowerOperationCode": 'fi'
													},
													this.handleValueChange
												)
											}
										</Col>


										<Col md={3} sm={3}>
											<Checkbox
												colors="dark"
												className="mycheck"
												checked={this.state.checkboxTable.accasoaSum}
												disabled={!this.state.checkboxTable.accasoa}
												onChange={this.onChangeAccasoaSum.bind(this)}
											/>
										</Col>

									</Row>


									<Row>

										{/*辅助核算*/}
										<Col md={2} sm={2}>
											<Checkbox colors="dark" className="mycheck" checked={this.state.checkboxTable.ass} onChange={this.onChangeAss.bind(this)}></Checkbox>
										</Col>

										<Col md={3} sm={3}>
											<div className='nc-theme-common-font-c'>{this.state.json['20021CFRPT-000010']}</div>{/* 国际化处理： 辅助核算*/}
										</Col>

										<Col md={4} sm={4}>
											<ReferWraper
												// outStyle={{width: '200px', marginLeft: '10px'}}
												placeholder = {this.state.assDisplay} 
												display={this.state.assDisplay}
												disabled={false}
												onClick={this.state.checkboxTable.ass ? this.showAss: '' }
											/>
											{/* <div style={{ display: 'flex' }}>
												<Input
													// style={{ border: 'none' }}
													placeholder={this.state.assDisplay}
													value={this.showSelectArr}
												/>
												<span
													className='ellipsisContainer'
													onClick={
														() => {
															(this.state.checkboxTable.ass) && this.showAss()
														}
													}
												>
													<span className='ellipsisCell'>.</span>
													<span className='ellipsisCell'>.</span>
													<span className='ellipsisCell'>.</span>
												</span>
											</div> */}
										</Col>

										<Col md={3} sm={3}>
											<Checkbox colors="dark" className="mycheck" checked={this.state.checkboxTable.assSum} disabled={!this.state.checkboxTable.ass} onChange={this.onChangeAssSum.bind(this)}></Checkbox>
										</Col>

									</Row>

									<Row>

										{/*内部单位*/}

										<Col md={2} sm={2}>
											<Checkbox colors="dark" className="mycheck" checked={this.state.checkboxTable.inner} onChange={this.onChangeInner.bind(this)}></Checkbox>
										</Col>
										<Col md={3} sm={3}>
											<div className='nc-theme-common-font-c'>{this.state.json['20021CFRPT-000023']}</div>{/* 国际化处理： 内部单位*/}
										</Col>
										<Col md={4} sm={4}>
											<FinanceOrgAllGroupAllDataTreeRef
												disabled={!this.state.checkboxTable.inner}
												value={this.state.inner}
												isMultiSelectedEnabled={true}
												queryCondition={() => {
													return {
														"isDataPowerEnable": 'Y',
														"DataPowerOperationCode": 'fi'
													}
												}}
												onChange={(v) => {
													//(v);

													this.setState({
														inner: v,
													})

												}
												}
											/>
										</Col>
										<Col md={3} sm={3}>
											<Checkbox colors="dark" className="mycheck" checked={this.state.checkboxTable.innerSum} disabled onChange={this.onChangeInnerSum.bind(this)}></Checkbox>
										</Col>

									</Row>
								</Row>
							</div>

							{/*会计期间*/}
							<DateGroup
								selectionState='false'
								start={this.state.start}
								end={this.state.end}
								enddate={this.state.enddate}
								pk_accperiodscheme={this.state.pk_accperiodscheme}
								pk_accountingbook={this.state.accountingbook}
								rangeDate={this.state.rangeDate}
								// handleValueChange = {this.handleValueChange}
								handleDateChange={this.handleDateChange}
								self={this}
								showRadio={this.state.showRadio}
								showPeriod={true}
								showDate={false}
							/>


							<Row className="myrow">
								<Col md={2} sm={2}>
									<span className='nc-theme-form-label-c'>{this.state.json['20021CFRPT-000024']}：</span>{/* 国际化处理： 币种*/}
								</Col>
								<Col md={4} sm={4}>
									<div className="book-ref">
										<Select
											fieldid='currtype'
											value={this.state.currtype}
											showClear={false} //不显示‘x’
											// style={{ width: 150, marginRight: 6 }}


											onChange={this.handleCurrtypeChange.bind(this)}

										>
											{this.state.currtypeList.map((item, index) => {
												return <NCOption value={item.pk_currtype} key={item.pk_currtype} >{item.name}</NCOption>
											})}

										</Select>

									</div>
								</Col>



							</Row>

							<Row className="myrow">
								<Col md={2} sm={2}>
									<span className='nc-theme-form-label-c'>{this.state.json['20021CFRPT-000025']}: </span>{/* 国际化处理： 显示属性*/}
								</Col> 
								<Col md={4} sm={4}>
									<Checkbox colors="dark" className="mycheck" id='includeuntally' checked={this.state.checkbox.one} onChange={this.onChange1.bind(this)}>
										{this.state.json['20021CFRPT-000026']} {/* 国际化处理： 包含未记账凭证*/}
									</Checkbox>
								</Col>

								<Col md={4} sm={4}>
									<Checkbox colors="dark" className="mycheck" id='showstop1' checked={this.state.checkbox.three} onChange={this.onChange3.bind(this)} >
										{this.state.json['20021CFRPT-000027']} {/* 国际化处理： 显示停用*/}
									</Checkbox>
								</Col>

							</Row>

							{/*返回币种：*/}
							{
								returnMoneyType(
									this,
									{
										key: 'selectedCurrValue',
										value: this.state.selectedCurrValue,
										groupEdit: this.state.groupCurrency,
										gloableEdit: this.state.globalCurrency
									},
									this.state.json
								)
							}
						</div>
					</div>
				</div>

			</Modal.Body>
			{isButtonShow &&
				<Modal.Footer>

					<Button
						fieldid='query'
                        className= "button-primary"
						onClick = {() => {
							let checks = this.state.checkboxTable;

							let selectedrow = ['Y', 'N', 'N', 'N'];
							if (checks.accasoa) {
								selectedrow[1] = 'Y';
							};
							if (checks.ass) {
								selectedrow[2] = 'Y';
							};
							if (checks.inner) {
								selectedrow[3] = 'Y';
							};

							let needsummary = ['N','N','N','N'];
							if (checks.cashSum) {
								needsummary[0] = 'Y';
							};
							if (checks.accasoaSum) {
								needsummary[1] = 'Y';
							};
							if (checks.assSum) {
								needsummary[2] = 'Y';
							};
							if (checks.innerSum) {
								needsummary[3] = 'Y';
							};
							let assvosResult = [];//辅助核算
                            this.state.checkboxTable.ass && this.state.accAssItems && this.state.accAssItems.map((item, index) => {
                            	let cellObj = {};
                            	if(item.select){
                                    cellObj.pk_checktype = item.pk_checktype;
                                    if(item.selectCells){
                                        cellObj.pk_checkvalue = item.refpk.join(',');
									}
                                    assvosResult.push(cellObj)
								}
							});
                            let pkAccasoa = [];//会计科目
                            this.state.checkboxTable.accasoa && this.state.accasoa.length>0 && this.state.accasoa.map((item, index) => {
                                pkAccasoa.push(item.refpk);
							})
							let pkCashflow = [];//现金流量项目
							this.state.cashflow.length>0 && this.state.cashflow.map((item, index) => {
                                pkCashflow.push(item.refpk)
							})
							let innerArr = [];//内部单位
                            this.state.checkboxTable.inner && this.state.inner.length>0 && this.state.inner.map((item, cell) => {
                                innerArr.push(item.refpk)
							})
                            let unitPk = [];
                            this.state.buSecond.length>0 && this.state.buSecond.map((item, index) => {
                                //('itemmm::', item);
                                unitPk.push(item.refpk);
                            })
                            //('uuuuu:::', unitPk);
								let newdata = {
									"pk_accountingbook": this.state.accountingbook.refpk,
									"begindate": this.state.rangeDate[0],
									"enddate": this.state.rangeDate[1],
									"pk_currtype": this.state.currtype,
									"includetally": (this.state.checkbox.one ? 'Y' : 'N'),
									"includesum": (this.state.checkbox.two ? 'Y' : 'N'),
									"includestop": (this.state.checkbox.three ? 'Y' : 'N'),
									"ismain": this.state.ismain,
									"returncurr": this.state.selectedCurrValue,
									"querytype": this.state.selectedQueryValue,
									"pk_cashflow": pkCashflow,//现金流量项目
									assvos: assvosResult,//辅助核算
									selectedrow,
									needsummary,
									pk_accasoa: pkAccasoa,//会计科目
									pk_innercorp: innerArr,//内部单位
									time: this.state.rangeDate[0] + '--' + this.state.rangeDate[1],
									bookname: this.state.accountingbook.refname,
									currtypeName: this.state.currtypeName,
                                    pk_unit: unitPk
								}
								//(newdata)

								this.props.onConfirm(newdata)
								
							}
						}
					>
                        {this.state.json['20021CFRPT-000028']}{/* 国际化处理： 查询*/}
					</Button>


					<Button 
						fieldid='cancel'
						className= 'btn-2 btn-cancel'
						onClick={() => {this.props.onCancel(false)}}
					>{this.state.json['20021CFRPT-000029']}</Button>{/* 国际化处理： 取消*/}
				</Modal.Footer>
			}
			<Loading fullScreen showBackDrop={true} show={this.state.isLoading} />

			<AssistModal
				// show={true}
				className = "AssistModala"
				accountBooking={this.state.accountingbook && this.state.accountingbook.refpk}
				show={this.state.AssistModalShow}
				columns={this.state.secondColumn}
				data = {this.state.accAssItems}
				ref='AssistModal'
				title={this.state.json['20021CFRPT-000010']}/* 国际化处理： 辅助核算*/
				icon=""
				// accAssItems = {this.state.info.accAssItems}
				onConfirm={(assDatas) => {
					this.handleAssistModal(this.state.AssistModalShow, assDatas);
                    this.setSelectCell()
				}}
			
				onCancel={() => {
					let AssistModalShow = false;
					this.setState({ AssistModalShow });
				}}
			/>



		</Modal>;
	}
}

CentralConstructorModal = createPage({})(CentralConstructorModal)
export default CentralConstructorModal
