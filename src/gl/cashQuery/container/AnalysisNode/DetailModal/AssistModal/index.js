import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';

import {high,base,ajax,deepClone, getMultiLang } from 'nc-lightapp-front';
const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCModal: Modal,NCInput: Input
} = base;
export default class MsgModal extends Component {

	constructor(props) {
		super(props);
		this.state={
			json: {},
			checkFormNow:false,//控制表单form回调
			startdate:{//制单日期
			},
			activeKey: '1',
			accountData:{
			},
			pk_accountingbook: '',
			fenluIndex: 1,      //点击辅助核算分录行的index
			rows: [],           //卡片界面的的rows
			// arrList: new Array(99),
			arrList: [],
			newList: [],  //各参照类型的参照值
			accAssItems: [],   //根据会计科目和制单日期获取的将要用来请求参照的数组值
			index: -1,
			value: '', //临时性参照v
		};
	}

    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:'20021CFRPT',domainName:'gl',currentLocale:'simpchn',callback});
    }

	static defaultProps = {
		show: false,
		title: '',
		content: '',
		closeButton: false,
		icon: 'icon-tishianniuchenggong',
		isButtonWhite: true,
		isButtonShow: true,
	};


	checkForm = (flag, obj) => {
		let edit = obj;
		this.props.onSubmit(edit, this.props.opre);
		this.setState({
			checkFormNow: false,
			open1:true,
			open2:true,
			open3:true
		});
	};


	handleGTypeChange =() =>{
		
	};



	render() {
		let { show, title, content, closeButton, icon, isButtonWhite, isButtonShow}= this.props;
		
		return <Modal
			id='tableModal'
			className={isButtonShow ? 'msg-modal' : 'msg-modal footer-hidden'}
			show={ show } 
			onHide={this.close}
			animation={false}
			backdrop="static"
		    >
			<Modal.Header closeButton= {closeButton}>
				<Modal.Title>
					<span className={`title-icon iconfont ${icon}`}></span>
					<span className='bd-title-1'>{title}</span>
					<span>
						<Icon 
							className='close-icon iconfont icon-guanbi'
							onClick={() => {this.props.onCancel(true);}}
						/>
					</span>
				</Modal.Title>
			</Modal.Header >
			<Modal.Body>
                <Table
                    columns={this.props.columns} data={this.props.data}
                />
			</Modal.Body>
			{isButtonShow &&
				<Modal.Footer>
					<Button
                        className= "button-primary"
						onClick={() => {
								this.props.onConfirm(this.state.accAssItems)
							}
						}
					>{this.state.json['20021CFRPT-000030']}</Button>{/* 国际化处理： 确定*/}
					<Button 
						className= 'btn-2 btn-cancel'
						onClick={() => {this.props.onCancel(false);}}
					>{this.state.json['20021CFRPT-000029']}</Button>{/* 国际化处理： 取消*/}
				</Modal.Footer>
			}
		</Modal>;
	}
}
