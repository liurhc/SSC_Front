import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base,toast,getMultiLang,createPageIcon} from 'nc-lightapp-front';
let { NCMessage} = base;
const { NCDropdown:Dropdown, NCIcon:Icon, NCMenu:Menu, NCButton:Button } = base;
import './index.less'

const tableid = 'reconcilemeg';
const pagecode = '2002130299_web_list';
const appcode = '2002130203';
const urls = {
	save : '/nccloud/gl/reconcile/reconcilerulsavemsg.do',
	query : '/nccloud/gl/reconcile/reconcilerulequerymsg.do',
	queryTemplet : '/nccloud/gl/reconcile/reconcileruledeletemsg.do'
};
const isShowOffEnable = false;			//是否启用“显示停用”功能
let allTableData = {}; 

//获取并初始化模板
let initTemplate = (props) => {
    props.createUIDom({
		pagecode : pagecode,
		appcode : appcode
	},
	(data)=>{
		let meta = data.template;
		meta = modifierMeta(props, meta)
		props.meta.setMeta(meta);
		data.button && props.button.setButtons(data.button);
		props.button.setButtonsVisible({
			add: true,
			edit: true,
			delete: true,
			save: false,
			cancel: false,
			refresh:true
		});
	});
}

//对表格模板进行加工操作
function modifierMeta(props,meta) {
	meta[tableid].showindex=true;  //表格显示序号
	
	meta[tableid].items = meta[tableid].items.map((item, key) => {
		if (item.attrcode == 'ismsgcenter') {
			item.initialvalue={value:true,display:null};
		}
		if (item.attrcode == 'pk_role') {
			item.isMultiSelectedEnabled = false;
		}
		return item;
	});
	return meta;
}

class Messageset extends Component {
	constructor(props) {
		super(props);
		this.props = props;
		this.props.button.setButtonsVisible({
			add: false,
			edit: false,
			save: false,
			cancel: false,
			delete: false
		});
		this.state={
			searchValue:'',
			orgvalue:{},
			searchDisable:false,				//简单搜索框是否禁用	true：禁用		false：可用
			moreButton:false,				//更多按钮状态
			showOffDisable:true,			//显示停用复选框是否禁用	true：禁用		false：可用
			isShowOff:false,				//列表是否显示停用数据
			json:{}
		};
		this.pk_reconcilerule={}
	}
	componentWillReceiveProps (nextProp) {
		let {loadData,showOrHide}=this.props;
		this.pk_reconcilerule=nextProp.pk_reconcilerule;
		
	}
	componentDidMount() {
		let callback= (json) =>{
			this.setState({json:json},()=>{
				initTemplate.call(this, this.props);
				// this.loadDept();
			})
		}
		getMultiLang({moduleId:'2002130203',domainName:'gl',currentLocale:'zh-CN',callback});
		this.pk_reconcilerule=this.props.pk_reconcilerule;
		this.getData(false);
		this.props.button.setDisabled({
			delete: true
		});
	}

	
	//请求列表数据
	getData = (showOff = false) => {
		let type=this.props.getUrlParam('type');
		let pk_reconcilerule=this.pk_reconcilerule;
		let	data={
				"pagecode": pagecode,
				"pk_reconcilerule":pk_reconcilerule
			}
		//如果showOff为true，则显示停用数据，在请求参数data中增加显示停用参数，根据业务前后端自行处理
		ajax({
			url: urls['query'],
			data,
            success: (res) => {
				let { success, data } = res;
				if (success) {
					if(res){
						if(res.data!=null){
							allTableData = data[tableid];
							this.props.editTable.setTableData(tableid, data[tableid]);
						}else{
							this.props.editTable.setTableData(tableid, { rows: [] });
						}
					}else{
						this.props.editTable.setTableData(tableid, { rows: [] });
					}
				}
			}
		});
	};
	//请求列表数据
	getDataFfresh = (showOff = false) => {
		let type=this.props.getUrlParam('type');
		let pk_reconcilerule=this.pk_reconcilerule;
		let	data={
				"pagecode": pagecode,
				"pk_reconcilerule":pk_reconcilerule
			}
		//如果showOff为true，则显示停用数据，在请求参数data中增加显示停用参数，根据业务前后端自行处理
		ajax({
			url: urls['query'],
			data,
            success: (res) => {
				let { success, data } = res;
				if (success) {
					if(res){
						if(res.data!=null){
							allTableData = data[tableid];
							this.props.editTable.setTableData(tableid, data[tableid]);
						}else{
							this.props.editTable.setTableData(tableid, { rows: [] });
						}
					}else{
						this.props.editTable.setTableData(tableid, { rows: [] });
					}
					toast({color:"success", content:this.state.json['2002130203-000002']})
				}
			}
		});
	};
	//表格编辑后事件
	onAfterEvent(props, moduleId , key,valuechange, changerows, index,value, data) {
		// return;
		//目前data一直为空，报错，此处先屏蔽
		//props, moduleId(区域id), key(操作的键), value（当前值）, changedrows（新旧值集合）, record（行数据）, index（当前index）
		//表格的编辑后事件，暂时没用到，处理表格字段编辑后业务及验证使用
		// let length = this.props.editTable.getNumberOfRows(moduleId);
		// if(((length-1)===index)&&data.status==='2'){
		// 	this.onButtonClick('add');
		// }
	//   	meta[moduleId].items.map((item) => {
	// 	 if (item.attrcode == 'pk_role') {
	// 		}
	// 	});
		if(key == 'pk_role'){
			let refcode=valuechange.refcode;
			let refname=valuechange.refname;
			this.props.editTable.setValByKeyAndIndex(moduleId,index,'pk_role',{display:refname});
			this.props.editTable.setValByKeyAndIndex(moduleId,index,'rolename',{value:refcode,display:refcode});
		}
		// if(key === 'isgrade' && data.values['doclevel'] === '0'){
		// 	let allRows = props.editTable.getAllRows(moduleId);
		// 	data.status = '1'
		// 	let changDdata = {
		// 		pageid:pagecode,
		// 		model: {
		// 			areaType: 'table',
		// 			pageinfo: null,
		// 			rows: data
		// 		}
		// 	};
		// 	ajax({
		// 		url: urls['save'],
		// 		data:changDdata,
		// 		success: (res) => {     //此处使用箭头函数，如果不使用箭头函数，一定要bind(this)
		// 			let { success, data } = res;
		// 			if (success) {
		// 				()=>{
		// 					//操作成功，更新页面当前行数据
		// 					// let retData = data[moduleId];
		// 					// allRows[index] = retData.rows[0];
		// 					// let allData = props.editTable.getAllData(moduleId);
		// 					// allData.rows = allRows;
        //                     // props.editTable.setTableData(moduleId, allData);
        //                     let allD = this.props.editTable.getAllData(tableid);
        //                     Utils.filterDelRows(allD.rows);//过滤清除删除状态的行
        //                     Utils.filterResult(allD,data[tableid].rows);//将保存后返回的数据重新放置到页面
        //                     this.props.editTable.setTableData(tableid,allD);
		// 				}
		// 			}
		// 		}
		// 	});
		// }
	}

	//更新按钮状态
    updateButtonStatus(){
		//此处控制按钮的隐藏显示及启用状态
		let tableData = this.props.editTable.getCheckedRows(tableid);
		let length = tableData.length;//获取列表页选择数据的行数
		if(this.props.editTable.getStatus(tableid) === 'edit'){//编辑状态
			this.props.button.setButtonsVisible({
				add: true,
				edit: false,
				save: true,
				cancel: true,
                delete: true,
                refresh: false
			});
			this.setState({
				searchDisable:true,
				showOffDisable:true
			});
		}else{//浏览态
			
			this.props.button.setButtonsVisible({
				add: true,
				edit: true,
				delete: true,
				save: false,
				cancel: false,
				refresh: true
			});
			this.setState({
				searchDisable:false,
				showOffDisable:false
			});
			if(length === 0){//未选择数据
				this.props.button.setDisabled({
					delete: false
				});
			}
		}
		if(length>0){
			this.props.button.setDisabled({
				delete: false
			});
		}else{
			this.props.button.setDisabled({
				delete: true
			});
		}
	}
	// daleteButtons(){
	// 	let tableData = this.props.editTable.getCheckedRows(tableid);
	// 	let type=this.props.getUrlParam("type");
	// 	if(type="org"){
	// 		let pk_org=tableData[0].data.values.pk_org;
	// 		let org=this.state.orgvalue.refpk;
	// 		if(pk_org!=org){
	// 			this.props.button.setDisabled({
	// 				delete: false
	// 			});
	// 		}
	// 	}
	// }

	changeTable(value){
		ajax({
			url: urls['query'],
			data:{
				"pagecode": pagecode,
				type:this.props.getUrlParam('type'),
				pk_org:value.refpk
				// pk_group:value.values.pk_group.value
			},
            success: (res) => {
				let { success, data } = res;
				if (success) {
					if(res.data!=null){
						allTableData = data[tableid];
						this.props.editTable.setTableData(tableid, allTableData);
					}
				}
			}
		});
	}

	//显示停用数据
	showOffChange(){
		this.setState({
			isShowOff : !this.state.isShowOff
		});
		this.getData(this.state.isShowOff);
	}

	//按钮点击事件
	onButtonClick(props,id) {
		let pk_reconcilerule=this.pk_reconcilerule;
		switch (id) {
			case 'add':
				this.props.editTable.addRow(tableid);
				let num = this.props.editTable.getNumberOfRows(tableid); //获取列表总行数
                // this.props.editTable.hideColByKey(tableid,'opr');//隐藏操作列
                this.props.editTable.setStatus(tableid,'edit');
                this.updateButtonStatus();
				break;
			case 'edit':
                this.props.editTable.setStatus(tableid, 'edit');
                this.updateButtonStatus();
				break;
			case 'cancel':
				this.props.editTable.cancelEdit(tableid);
                // this.props.editTable.showColByKey(tableid,'opr');//显示操作列
                this.updateButtonStatus();
                break;
            case 'refresh':
                this.getDataFfresh();
				break;
			case 'save':
				let tableData = this.props.editTable.getChangedRows(tableid);   //保存时，只获取改变的数据行而不是table的所有数据行，减少数据传输
				
				let data = {
					
					pageid:pagecode,
					userjson:pk_reconcilerule,//传到后台处理的公有协同pk
					model : {
						areaType: "table",
						pageinfo: null,
						rows: []
					}
                };
                //去除掉空行
                let tempArr = new Array();
                for(let i = 0; i < tableData.length; i++){
                    if(tableData[i].values.pk_role.value != null && tableData[i].values.pk_role.value.length > 0){
                        tempArr.push(tableData[i]);
                    }
                }
                if(tempArr.length < 1){
                    toast({content:this.state.json['2002130203-000025'],color:'warning'});/* 国际化处理： 请填写数据*/
                    return;
                }else{
                    tableData = tempArr;
                }
				data.model.rows = tableData;
				ajax({
					url: urls['save'],
					data,
                    success: function (res)  {     //此处使用箭头函数，如果不使用箭头函数，一定要bind(this)
                        let { success,data} = res;
						if (success) {
                            this.props.editTable.setStatus(tableid, 'browse');//设置表格状态为浏览态/设置表格状态为浏览态
							if(data){
                                let allD = this.props.editTable.getAllData(tableid);
                                let length = allD.length;
                                this.props.editTable.setTableData(tableid,data[tableid]);
							}
						toast({color:"success",content:this.state.json['2002130203-000009']});/* 国际化处理： 保存成功*/
                        }
					}.bind(this)
				});
				break;
			case "delete":
				let selectedData=this.props.editTable.getCheckedRows(tableid);
				if(this.props.editTable.getStatus(tableid) === 'edit'){
					let indexArr=[];
					selectedData.forEach((val) => {
						indexArr.push(val.index);
					});
					this.props.editTable.deleteTableRowsByIndex(tableid, indexArr);
				}else{
				if(selectedData.length==0){
					toast({color:"error",content:this.state.json['2002130203-000026']});/* 国际化处理： 请选择要删除的数据*/
					return 
				}
				let indexArr=[];
				let dataArr=[];
				selectedData.forEach((val) => {
					let delObj = {
						status: '3',
						values: {
							ts: {
								display: this.state.json['2002130203-000027'],/* 国际化处理： 时间戳*/
							},
							pk_reconcilemeg: {
								display: this.state.json['2002130203-000028'],/* 国际化处理： 主键*/
							}
						}
					};
					delObj.rowId=val.data.rowId;
					delObj.values.ts.value=val.data.values.ts.value;
					delObj.values.pk_reconcilemeg.value=val.data.values.pk_reconcilemeg.value;
					dataArr.push(delObj);
					indexArr.push(val.index);
				});
				data = {
					pageid:pagecode,
					userjson:pk_reconcilerule,
					model: {
						areaType: 'table',
						pageinfo: null,
						rows: dataArr
					}
				};
				ajax({
					url: urls['save'],
					data,
					success: (res) => {     //此处使用箭头函数，如果不使用箭头函数，一定要bind(this)
						let { success, data } = res;
						if (success) {
							if(data){
								// this.props.editTable.setTableData(tableid,data[tableid]);
								this.props.editTable.deleteTableRowsByIndex(tableid, indexArr);
							}else{
								this.props.editTable.setTableData(tableid,{ rows: [] });
							}
							toast({color:"success",content:this.state.json['2002130203-000023']});/* 国际化处理： 删除成功*/
							this.props.button.setDisabled({
								delete: true
							});
						}
					}
				});
				}
				break;
		}
	
	}

	onSelectMoreButton({ key }) {
		toast({color:"info",content:this.state.json['2002130203-000029']});/* 国际化处理： 努力开发中*/
	 
	}

	/* 非通用代码，与节点业务有关，不同业务自行修改，不需要的删除 */
    onChangeIsgrade(){

    }

    //表头简单筛选
	onSearch(value){
		this.setState({ searchValue:value });
		let allData =   Utils.clone(allTableData);
		if(value.trim()===''){
			
		}else{
			let rows = Array.of();
			for(var row of allData.rows){
				if(row.values['code'].value.indexOf(value)>-1 || row.values['name'].value.indexOf(value)>-1){
					rows.push(row);
				}
			}
			allData.rows = rows;
		}
		this.props.editTable.setTableData(tableid,allData);
	}

	render() {
		let {button,editTable } = this.props;
		let { createEditTable } = editTable;
        const { Item } = Menu;
		let { createButtonApp } = button;
		let moreButton = (
			<Menu
                  onSelect={this.onSelectMoreButton.bind(this)}>
                      <Item key="1">{this.state.json['2002130203-000012']}</Item>{/* 国际化处理： 消息设置*/}
                </Menu>
		);
		return (
			<div className="nc-single-table" id="nc-single-tablenc">
				{/* 标题 title */}
				<div className="nc-singleTable-header-area">
                    <div className="header-title-search-area">
					 {/*页面大图标*/}
					 {createPageIcon()} 
					    <h2 className="title-search-detail">{this.state.json['2002130203-000012']}</h2>
                    </div>
					{/* 按钮区  btn-group */}
					<div className="header-button-area">
                        {createButtonApp({
                            area: 'list',//按钮注册中的按钮区域
                            buttonLimit: 5, 
                            onButtonClick: this.onButtonClick.bind(this),
                            popContainer: document.querySelector('.header-button-area')
                        })}
					</div>
					</div>
				{/* 列表区 */}
				<div  className='nc-singleTable-table-area"'>
						{createEditTable(tableid, {//列表区
							//onCloseModel: onCloseModelFn,                    // 弹窗控件点击关闭事件 
							onAfterEvent: this.onAfterEvent.bind(this),                      // 控件的编辑后事件  
							useFixedHeader:true,    
							// onSelected: this.daleteButtons.bind(this),                        // 左侧选择列单个选择框回调
							//onSelectedAll: onSelectedAllFn,                  // 左侧选择列全选回调
							selectedChange: this.updateButtonStatus.bind(this),                // 选择框有变动的钩子函数
							statusChange: this.updateButtonStatus.bind(this),				//表格状态监听
							showIndex:true,				//显示序号
							showCheck:true			//显示复选框
							//params: 'test',                                  // 自定义传参

						})}
					</div>
			</div>



		);
	}
}

Messageset = createPage({
	// initTemplate: initTemplate
})(Messageset);

// ReactDOM.render(<Messageset />, document.querySelector('#app'));

export default Messageset;
