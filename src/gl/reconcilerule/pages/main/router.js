import { asyncComponent } from 'nc-lightapp-front';

const card = asyncComponent(() => import(/* webpackChunkName: "gl/reconcilerule/pages/card/card" */ /* webpackMode: "eager" */ '../card'));
const List = asyncComponent(() => import(/* webpackChunkName: "gl/reconcilerule/pages/list/list" */ /* webpackMode: "eager" */ '../list'));

const routes = [
	{
		path: '/',
		component: List,
		exact: true
	},
	{
		path: '/list',
		component: List
	},
	{
		path: '/card',
		component: card
	}
];

export default routes;
