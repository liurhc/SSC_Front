import buttonClick from './buttonClick';
import initTemplate from './initTemplate';
import afterEvent from './afterEvent';
import tableButtonClick from './tableButtonClick';
import tableBeforeEvents from './tableBeforeEvents';
export { buttonClick, afterEvent,initTemplate,tableButtonClick, tableBeforeEvents};
