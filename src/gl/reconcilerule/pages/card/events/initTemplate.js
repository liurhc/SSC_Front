import { base, ajax ,getBusinessInfo,getMultiLang} from 'nc-lightapp-front';
let { NCPopconfirm } = base;
import {ENABLESTATE,NODETYPE,dataSource,pkname} from '../consts.js';
import pubUtil from '../../../../public/common/pubUtil.js';
import tableButtonClick from './tableButtonClick'

const formId = 'reconcilerule';
const tableId = 'subvos';
const pageId = '2002130203_web_card';
let appcode='2002130203';

export default function(props) {
	let page = this;
	let nodetype=props.getUrlParam('nodetype');
	if(nodetype==NODETYPE.GROUP){
		appcode='2002130202';
	}
	appcode=this.props.getSearchParam('c');
	props.createUIDom(
		{
			pagecode: pageId,//页面id
			appcode: appcode//注册按钮的id
		}, 
		function (data){
			if(data){
				if(data.template){
					let meta = data.template;
					modifierMeta(props, meta,page)
					props.meta.setMeta(meta);
					let accasoa=['oppaccasoa','selfaccasoa'];
					props.cardTable.hideColByKey(tableId,accasoa);
					if(props.getUrlParam('status') != 'browse'){
						//props.cardTable.addRow(tableId);
					}
				}
				if(data.button){
					let button = data.button;
					props.button.setButtons(button);
				}
			}
		}
	)
}

function changeTable(props, meta){
	meta[tableId].items = meta[tableId].items.map((item, index) => {
		     });
			return meta;
}
function modifierMeta(props, meta,page) {
	
		let status = props.getUrlParam('status');
	meta[formId].status = status;
	meta[tableId].status = status;
	//form表单处理数据
	meta[formId].items = meta[formId].items.map((item, index) => {
		if(item.attrcode == 'enablestate'){
			if(page){
				item.initialvalue={value:'1',display:page.state.json['2002130203-000007']};/* 国际化处理： 未启用*/
			}
	     };
	let nodetype=props.getUrlParam('nodetype');
		let  type='global';
		if(nodetype=='group'){
			type='all';
		}
		if (item.attrcode == 'pk_contrastrule') {
			item.refcode = 'gl/refer/voucher/ContrastRuleGridRef/index';
			item.queryCondition = () => {
				return {startstatus:type,"isDataPowerEnable": 'Y',"DataPowerOperationCode" : 'fi'};
			}
		}
		return item;
	});
	let multiLang = props.MutiInit.getIntl('2052');
	//table处理数据
	meta[tableId].items = meta[tableId].items.map((item, index) => {
		return item;
	});
	if(page){
		meta[tableId].items.push({
			attrcode: 'opr',
			label: page.state.json['2002130203-000008'],/* 国际化处理： 操作*/
			fixed: 'right',
			itemtype: 'customer', 
			visible: true,
			className: 'table-opr',
			width:'120px',
			render: (text, record, index) => {
				let buttonAry =
					props.getUrlParam("status") === "browse"
						? [""]
						: ["deleteline"];
				return props.button.createOprationButton(buttonAry, {
					area: "table_body",
					buttonLimit: 3,
					onButtonClick: (props, key) => tableButtonClick(page, props, key, text, record, index,tableId)
				});
			}
		});
	}
	return meta;
}
function getAccountCondition(page){
	let pk_setofbook = page.pk_setofbook;
	const condition = {
		pk_accountingbook:'', 
		pk_setofbook:'',
		datestr:''
	}
	condition.pk_setofbook = pk_setofbook;
	let businessInfo = getBusinessInfo();
	let buziDate = businessInfo.businessDate;
	if(buziDate){
		condition.datestr = buziDate.split(' ')[0];
	}
	return condition;
}
function getAccountCondition1(page){
	let pk_setofbook = page.pk_setofbook;
	const condition = {
		setPk_org:'', 
		setPk_setofbook:'',
		setDatestr:''
	}
	condition.pk_setofbook = pk_setofbook;
	let businessInfo = getBusinessInfo();
	let buziDate = businessInfo.businessDate;
	if(buziDate){
		condition.datestr = buziDate.split(' ')[0];
	}
	return condition;
}
