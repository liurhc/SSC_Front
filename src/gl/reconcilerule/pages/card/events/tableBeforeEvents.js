import { toastWarning } from "../../../../public/common/toast"
import {AREA_CODE, ITEM_KEY} from "../consts"

export default function(props, moduleId, key, value, index, record, status){
    let page = this;
    let pk_contrastruleObj = page.props.form.getFormItemsValue(AREA_CODE.FORM, ITEM_KEY.PK_CONTRASTRULE);
    let fromRule = pk_contrastruleObj && pk_contrastruleObj.value;
    if(fromRule){
        toastWarning(page.state.json['2002130203-000033']); /* 已引用规则不允许修改数据 */
        return false;
    }
    return true;
}