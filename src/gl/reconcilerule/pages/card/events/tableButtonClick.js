import { ajax, base, toast } from 'nc-lightapp-front';
import {formId,tableId,pagecode, AREA_CODE, ITEM_KEY} from '../consts';
import {toastWarning} from '../../../../public/common/toast'

const tableButtonClick = (page, props, key, text, record, index,tableId) => {
    switch (key) {
        case 'deleteline':
            delLine(page, index);
            break;
        case 'switchview':
           let status = props.cardTable.getStatus(tableId);
            if(status === 'browse')
            {
                props.cardTable.toggleRowView(tableId, record);
            }
            else
            {
                props.cardTable.openModel(tableId, 'edit', record, index);            
            }
            break;
        default:
            break;
    }
};
function delLine(page, index){
    let pk_contrastruleObj = page.props.form.getFormItemsValue(AREA_CODE.FORM, ITEM_KEY.PK_CONTRASTRULE);
    let fromRule = pk_contrastruleObj && pk_contrastruleObj.value;
    if(fromRule){
        toastWarning("11111111");
    }else{
        page.props.cardTable.delRowsByIndex(AREA_CODE.FORM, index);
    }
}
export default tableButtonClick;