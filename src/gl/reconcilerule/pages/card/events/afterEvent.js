import { ajax ,getBusinessInfo,getMultiLang,deepClone} from 'nc-lightapp-front';

export default function afterEvent(props, moduleId, key,value, changedrows, i, s, g) {
	let accasoa=['oppaccasoa','selfaccasoa'];
	let code=['selfsubjcode','oppsubjcode'];
	if (key === 'pk_org_v') {
		let data = {
			nbcrcode:'HO',
			pk_group:'0001A1100000000005T5',
			pk_org:value.value
		}
	}
	if (key === 'cmaterialvid') {
		let materialsNew = value;
		if(materialsNew && materialsNew.length>1){
			props.cardTable.setValByKeyAndIndex(moduleId,i,key,{value:materialsNew[0].refpk,display:materialsNew[0].refname});
			for(let i=1;i<materialsNew.length;i++){
				props.cardTable.addRow(moduleId);
				let ll = props.cardTable.getNumberOfRows(moduleId);
				props.cardTable.setValByKeyAndIndex(moduleId, ll-1,key,{value:materialsNew[i].refpk,display:materialsNew[i].refname});
			}
		}
	}
	//表体编辑后事件
    if(key == 'fconfirmpoint'){
		let data = props.createBodyAfterEventData('20521030', this.formId, this.tableId, moduleId, key, changedrows);
	}
	if(key=='pk_setofbook'){
		let nodetype=props.getUrlParam('nodetype');
		let pk_reconcilerule=props.form.getFormItemsValue(this.formId, ['pk_reconcilerule'])[0];
		if(pk_reconcilerule.value){
			let delrows=props.cardTable.getAllRows(this.tableId, true);
			if(delrows.length>0){
				for(let i=0;i<delrows.length;i++){
					props.cardTable.delRowByRowId(this.tableId, delrows[i].rowid);
				}
			}
		}else{
			props.cardTable.setTableData(this.tableId, { rows: [] });
		}
		if(value.value==null || value.value==''){
			props.cardTable.hideColByKey(this.tableId,accasoa);
			props.cardTable.showColByKey(this.tableId,code);
		}else{
			props.cardTable.hideColByKey(this.tableId,code);
			props.cardTable.showColByKey(this.tableId,accasoa);
		}
		this.pk_setofbook=value.value;
		let meta=props.meta.getMeta();
		meta[this.tableId].items = meta[this.tableId].items.map((item, index) => {
			const condition = {
				pk_accountingbook:'', // 核算账簿（组织级必传）
				pk_setofbook:'',    // 账簿类型（政策性科目参照必传）
				pk_org:'',         //政策性科目集团级必传，全局传空
				dateStr:'',
				"isDataPowerEnable": 'Y',
				// "DataPowerOperationCode" : 'fi'
			}
			condition.pk_setofbook = value.value;
			let businessInfo = getBusinessInfo();
			let buziDate = businessInfo.businessDate;
			let groupId=businessInfo.groupId;
			if(buziDate){
				condition.dateStr = buziDate.split(' ')[0];
			}
			if(nodetype=='group'){
				condition.pk_org=groupId;
			}else{
				condition.pk_org='GLOBLE00000000000000';
			}
	        if(item.attrcode == 'selfaccasoa'||item.attrcode == 'oppaccasoa'){
					item.refcode = 'uapbd/refer/fiacc/AccountDefaultGridTreeRef/index';
					item.isAccountRefer = false; 
					item.queryCondition = () => {
						return condition;
					}
	        }
			return item;
		});
		props.meta.setMeta(meta);
	}
	if(key=='pk_contrastrule'){
		let delrows=props.cardTable.getAllRows(this.tableId, true);
		if(delrows.length>0){
			for(let i=0;i<delrows.length;i++){
				props.cardTable.delRowByRowId(this.tableId, delrows[i].rowid);
			}
		}
		let pk_reconcilerule=props.form.getFormItemsValue(this.formId, ['pk_reconcilerule'])[0];
		if(value.value==null || value.value==''){
			// if(delrows.length>0){
			// 	for(let i=0;i<delrows.length;i++){
			// 		props.cardTable.delRowByRowId(this.tableId, delrows[i].rowid);
			// 	}
			// }
			this.props.button.setButtonVisible([ 'deleteline','addline'], true);
			this.props.form.setFormItemsDisabled(this.formId,{'pk_setofbook':false})
				
			// })
		}else{
			// props.cardTable.setTableData(this.tableId, {rows:[]});
			//选择内部交易规则时，需要添加一行科目，并且进行赋值；
			props.cardTable.hideColByKey(this.tableId,code);
			props.cardTable.showColByKey(this.tableId,accasoa);
		
			let url='/nccloud/gl/reconcile/reconcilerulequerycontrastrule.do';
			let data={
				"pk_contrastrule":value.value
			}
			let pk_setofbook=null;//内部交易对账规则的账簿类型
			let selfaccasoa=[];//本方科目
			let oppaccasoa=[];//对方科目
			ajax({
				url:url,
				data:data,
				async:false, 
				success:(res)=>{
					if(res.data){
						//取出内部交易对账规则的账簿类型，并下面进行赋值
						pk_setofbook=res.data[0].pk_book;
						let subjvos=res.data[0].subjvos;
						//循环取出本方和对方科目
						for(let i=0;i<subjvos.length;i++){
							let subjvo=subjvos[i];
							if(subjvo.isself==true){
								selfaccasoa[0]=subjvo.pk_accasoa;
								selfaccasoa[1]=subjvo.code;
							}else{
								oppaccasoa[0]=subjvo.pk_accasoa;
								oppaccasoa[1]=subjvo.code;
							}
						}
					}
					// let delrows=props.cardTable.getAllRows(this.tableId, true);
					// if(delrows.length>0){
					// 	for(let i=0;i<delrows.length;i++){
					// 		props.cardTable.delRowByRowId(this.tableId, delrows[i].rowid);
					// 	}
					// }
					// let datas=[];
					// datas.push({values:clone});
					// clone.selfaccasoa={display: selfaccasoa[1],  value: selfaccasoa[0]};
					// props.cardTable.insertRowsAfterIndex(this.tableId,datas,0);
					// setTimeout(() => {
						props.cardTable.addRow(this.tableId, 1, {'selfaccasoa': {display: selfaccasoa[1],  value: selfaccasoa[0]},'selfsubjcode':{display:selfaccasoa[1],value:selfaccasoa[1]},'oppsubjcode':{display:oppaccasoa[1],value:oppaccasoa[1]},'oppaccasoa': {display: oppaccasoa[1],  value: oppaccasoa[0]}}, false);
						props.cardTable.setStatus(this.tableId,'browse');
					// }, '100');
					
				}
			})
			//选择内部交易规则时,不可以进行填行和增行；
			this.props.button.setButtonVisible([ 'deleteline','addline'], false);
			//对账簿类型和会计科目进行处理，并进行赋值
			let pk_setofbookstr=[];
			if(pk_setofbook){
				pk_setofbookstr=pk_setofbook.split(',');
				// props.cardTable.addRow(this.tableId, 1, {'selfaccasoa': {display: selfaccasoa[1],  value: selfaccasoa[0]},'oppaccasoa': {display: oppaccasoa[1],  value: oppaccasoa[0]}}, false);
				this.props.form.setFormItemsValue(this.formId,{'pk_setofbook':{value:pk_setofbookstr[0],display:pk_setofbookstr[1]}});
			}
			else{
				this.props.form.setFormItemsValue(this.formId,{'pk_setofbook':{value:null,display:null}});
			}
			//设置账簿类型不可变
			// this.props.form.setFormItemsValue(this.formId,{'crtelimvoucher':{value:true,diaplay:null}})
			this.props.form.setFormItemsDisabled(this.formId,{'pk_setofbook':true,"crtelimvoucher":true});
			
		}
	}
	if(key=='selfaccasoa'){
		if(value!=null){
			let code=value.code;
			if(value.refcode){
				code=value.refcode;
			}
			this.props.cardTable.setValByKeyAndIndex(this.tableId,i,'selfsubjcode',{display:code,value:code});
		}else{
			this.props.cardTable.setValByKeyAndIndex(this.tableId,i,'selfsubjcode',{display:null,value:null});
		}
	}
	if(key=='oppaccasoa'){
		if(value!=null){
			let code=value.code;
			if(value.refcode){
				code=value.refcode;
			}
			this.props.cardTable.setValByKeyAndIndex(this.tableId,i,'oppsubjcode',{display:code,value:code});
		}else{
			this.props.cardTable.setValByKeyAndIndex(this.tableId,i,'oppsubjcode',{display:null,value:null});
		}
	}
}
