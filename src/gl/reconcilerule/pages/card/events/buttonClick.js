import { ajax, base, toast,cardCache,promptBox,getMultiLang} from 'nc-lightapp-front';
import { pkname,dataSource} from '../consts';
let { getCacheById, updateCache,getNextId,deleteCacheById,getCurrentLastId } = cardCache;
let { NCMessage } = base;
export default function(props, id) {
  let nodetype=props.getUrlParam('nodetype');
  switch (id) {
    case 'add':
      props.form.EmptyAllFormValue(this.formId);
      props.cardTable.setTableData(this.tableId, { rows: [] })
      props.pushTo('/card', {
        status: 'add',
        nodetype:nodetype,
      });
      this.toggleShow()
      this.props.form.setFormItemsDisabled(this.formId,{'pk_setofbook':false});
    break;
    case 'save':
      this.saveBill();
      this.toggleShow()
      break
    case 'edit':
      props.pushTo('/card', {
        status: 'edit',
        nodetype:nodetype,
        id: props.getUrlParam('id')
      })
      this.componentDidMount();
    break;            
    case 'copy':
      props.pushTo('/card', {
        status: 'edit',
        nodetype:nodetype,
        id: props.getUrlParam('id'),
        copyFlag:'copy'
      })
      this.componentDidMount();
      break

    case 'delete':
      promptBox({
        color:"warning",
        title: this.state.json['2002130203-000000'],/* 国际化处理： 删除*/
        content: this.state.json['2002130203-000001'],  /* 国际化处理： 确定要删除吗？*/
        beSureBtnClick: deleteAction.bind(props,this)
      });
      break
    case 'refresh':
      props.pushTo('/card', {
        status: 'browse',
        nodetype:nodetype,
        id: props.getUrlParam('id')
      });
      this.componentDidMount();
      this.toggleShow();
      toast({color:"success",content:this.state.json['2002130203-000002']});/* 国际化处理： 刷新成功*/
      break
    case 'back':
      props.pushTo('/list',{nodetype:nodetype});
      break
    case 'cancel':
      promptBox({
        color:"warning",
        title: this.state.json['2002130203-000003'],/* 国际化处理： 取消*/
        content: this.state.json['2002130203-000004'], /* 国际化处理： 确定要取消吗？*/
        beSureBtnClick: cancelAction.bind(props,this)
      });
      break
    case 'enable':
      enableRule(props,this);
    break;
    case 'disable':
      disableRule(props,this);
    break;
    case 'addline':
      props.cardTable.addRow(this.tableId)
      break
    case 'deleteline':
      let selrow = props.cardTable.getCheckedRows(this.tableId);
      let index=[];
      if(selrow){
        for(let i=0;i<selrow.length;i++){
          index.push(selrow[i].index);
        }
        props.cardTable.delRowsByIndex(this.tableId,index);
      }
     
      break
      case 'messageset':
      this.state.pk_reconcilerule=props.getUrlParam('id');
      this.props.modal.show('messageset');
      break;
    default:
      break
  }
}

function enableRule(props,_this){
  let record = props.createMasterChildData(_this.pageid, _this.formId, _this.tableId);
  let nodetype=props.getUrlParam('nodetype');
  ajax({
    url: '/nccloud/gl/reconcile/reconcileruleenable.do',
    data: record,
    success: (res) => {
      let { success, data } = res;
      let id=record.head[_this.formId].rows[0].values.pk_reconcilerule.value;
      if (success) {
        if (res.data.head && res.data.head[_this.formId]) {
          _this.props.form.setAllFormValue({ [_this.formId]: res.data.head[_this.formId] });
        }
        if (res.data.body && res.data.body[_this.tableId]) {
          _this.props.cardTable.setTableData(_this.tableId, res.data.body[_this.tableId]);
        }
        updateCache(pkname,id,res.data,_this.formId,dataSource);
        _this.toggleShow()
        _this.updateButtons(res);
        toast({color:"success",content:_this.state.json['2002130203-000005']});/* 国际化处理： 启用成功*/
      }
    }
  });
}

function disableRule(props,_this){
  let record = props.createMasterChildData(_this.pageid, _this.formId, _this.tableId);
  let nodetype=props.getUrlParam('nodetype');
  ajax({
    url: '/nccloud/gl/reconcile/reconcileruledisable.do',
    data: record,
    success: (res) => {
      let id=record.head[_this.formId].rows[0].values.pk_reconcilerule.value;
      if (res.data.head && res.data.head[_this.formId]) {
        _this.props.form.setAllFormValue({ [_this.formId]: res.data.head[_this.formId] });
      }
      if (res.data.body && res.data.body[_this.tableId]) {
        _this.props.cardTable.setTableData(_this.tableId, res.data.body[_this.tableId]);
      }
      updateCache(pkname,id,res.data,_this.formId,dataSource);
      _this.toggleShow();
      _this.updateButtons(res);
      toast({color:"success",content:_this.state.json['2002130203-000006']});/* 国际化处理： 停用成功*/
    }
  });
}

function deleteAction(_this){
  let nodetype=_this.props.getUrlParam('nodetype');
  let id=_this.props.getUrlParam('id');
  let nextId = getNextId(id, dataSource);
  ajax({
    url: '/nccloud/gl/reconcile/reconcileruledelete.do',
    data: {
        pk_reconcilerule: _this.props.getUrlParam('id'),
        enablestate : _this.props.form.getFormItemsValue(_this.formId, 'enablestate').value,
        ts: _this.props.form.getFormItemsValue(_this.formId, 'ts').value
    },
    success: (res) => {
        if (res.success) {
            deleteCacheById(pkname,id,dataSource);
            let nextCache = getCacheById(nextId, dataSource);
            if(nextId){
              if(nextCache){
                _this.props.setUrlParam({"id":nextId});
                _this.props.form.setAllFormValue({ [_this.formId]: nextCache.head[_this.formId] });
                _this.props.cardTable.setTableData(_this.tableId, nextCache.body[_this.tableId]);
                // page.setButtonStatus(nextCache);
                }else{
                  _this.props.setUrlParam({"id":nextId});
                  _this.componentDidMount();
                }
            }else{
              _this.props.form.EmptyAllFormValue(_this.formId);
              _this.props.cardTable.setTableData(_this.tableId, { rows: [] });
              _this.props.button.setDisabled({
                delete: true,
                edit: true,
                enable: true,
                disable: true,
                refresh:true,
                copy:true,
                messageset:true
              });
            }
            
        }
        toast({color:"success",content:_this.state.json['2002130203-000023']});/* 国际化处理： 删除成功*/
    }
  });
}

const cancelAction = (page) => {
  let id = getCurrentLastId(dataSource);
  if(id!=null && id!='' && id!=undefined){
    let cardData = getCacheById(id, dataSource);
    page.props.setUrlParam({id, copyFlag:'', status:'browse'})
    if(cardData){
      page.props.form.setAllFormValue({[page.formId]:cardData.head[page.formId]});
      page.props.cardTable.setTableData(page.tableId,cardData.body[page.tableId]);
    }else{
      page.componentDidMount();
    }
  }else{
    page.props.setUrlParam({"status":'browse', copyFlag:''});
    page.props.form.EmptyAllFormValue(page.formId);
    page.props.cardTable.setTableData(page.tableId, { rows: [] });
  }
  page.forceUpdate()
  page.toggleShow();
}
