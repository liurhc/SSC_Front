
export const ENABLESTATE = {
    UNENABLE : "1",//未启用
    ENABLE : "2",//已启用   
    UNABLE : "3"//已停用
}

export const NODETYPE = {
    GROUP : "group",
    GLOBAL : "global"
}

export const dataSource = 'gl.reconcilerule.pages.2002130203';
export const dataSourceGlobal = 'gl.reconcilerule.pages.2002130203Global';
export const dataSourceGroup = 'gl.reconcilerule.pages.2002130203Group';
export const pkname = 'pk_reconcilerule';

const AREA_CODE = {
    FORM:'reconcilerule',
    TABLE:'subvos'
}

const ITEM_KEY = {
    PK_CONTRASTRULE : 'pk_contrastrule'
}

export {AREA_CODE, ITEM_KEY};
