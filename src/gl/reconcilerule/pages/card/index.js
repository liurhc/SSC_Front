//主子表卡片
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast,cardCache,getMultiLang,getBusinessInfo,createPageIcon} from 'nc-lightapp-front';
let { NCFormControl,NCBackBtn} = base;
import { buttonClick, initTemplate, afterEvent, tableBeforeEvents } from './events';
import './index.less';
import Messageset from '../msg/index';
import {ENABLESTATE,NODETYPE,dataSource,pkname} from './consts.js';
import pubUtil from '../../../public/common/pubUtil.js';
import {CardLayout, Form, Table, ModalArea,TopArea} from '../../../../fipub/public/components/layout/CardLayout';
import {Header, HeaderButtonArea} from '../../../../fipub/public/components/layout/Header';
let { getCacheById, updateCache,addCache,getCurrentLastId,getNextId,deleteCacheById} = cardCache;
let table_id= 'subvos';
class Card extends Component {
	constructor(props) {
		super(props);
		this.formId = 'reconcilerule';
		this.moduleId = '2002';
		this.tableId = 'subvos';
		this.pageid='2002130203_web_card';
		this.pk_setofbook='';
		this.state={
			querydata :'',
			pk_reconcilerule:'',
			json:{}
		}
	}
	componentDidMount() {
		//查询单据详情
		if (this.props.getUrlParam('status') != 'add') {
			let pk_reconcilerule = this.props.getUrlParam('id');
			let data = { pk_reconcilerule, pagecode: this.pageid };
			ajax({
				url: '/nccloud/gl/reconcile/reconcilerulecard.do',
				data: data,
				async:false, 
				success: (res) => {
					if (res.data) {
						updateCache(pkname,pk_reconcilerule,res.data,this.formId,dataSource);	
						if (res.data.head) {
							this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
						}
						if (res.data.body) {
							setTimeout(()=>{
								this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
						}, 1000)
						}
						let copyflag=this.props.getUrlParam('copyFlag');
					if(copyflag=='copy'){
						this.props.form.setFormItemsValue(this.formId,{'enablestate':{value:'1',display:this.state.json['2002130203-000007']}});/* 国际化处理： 未启用*/
						this.props.form.setFormItemsValue(this.formId, { pk_reconcilerule: null });
						this.props.form.setFormItemsValue(this.formId, { ts: null });
						this.props.form.setFormItemsValue(this.formId, { startstatus: null });
						this.props.form.setFormItemsValue(this.formId, { dr: null });
						this.props.form.setFormStatus(this.formId,"add");
						setTimeout(()=>{
							this.props.cardTable.setColValue(this.tableId,'pk_reconcilerulesub',{value: '', display: ''});
							this.props.cardTable.setColValue(this.tableId,'pk_reconcilerule',{value: '', display: ''});
							this.props.cardTable.setColValue(this.tableId,'ts',{value: '', display: ''});
							this.props.cardTable.setColValue(this.tableId,'startstatus',{value: '', display: ''});
							this.props.cardTable.setColValue(this.tableId,'dr',{value: '', display: ''});
						}, 1000)
					}	
						setTimeout(()=>{
							let meta = this.props.meta.getMeta();
							let pk_setofbook = this.props.form.getFormItemsValue(this.formId,'pk_setofbook');
							this.pk_setofbook=pk_setofbook.value;
							if(pk_setofbook.value){
		                         meta[table_id].items.map((item, index) => {
		                            if(item.attrcode == 'selfsubjcode'
									||item.attrcode == 'oppsubjcode'){
								item.visible = false;
		                        }else if(item.attrcode == 'selfaccasoa'
								||item.attrcode == 'oppaccasoa'){
										const condition = {
											pk_accountingbook:'', // 核算账簿（组织级必传）
											pk_setofbook:'',    // 账簿类型（政策性科目参照必传）
											pk_org:'',         //政策性科目集团级必传，全局传空
											dateStr:'',
											"isDataPowerEnable": 'Y',
											// "DataPowerOperationCode" : 'fi'
										}
										condition.pk_setofbook = pk_setofbook.value;
										let businessInfo = getBusinessInfo();
										let buziDate = businessInfo.businessDate;
										let groupId=businessInfo.groupId;
										if(buziDate){
											condition.dateStr = buziDate.split(' ')[0];
										}
										let nodetype=this.props.getUrlParam('nodetype');
										if(nodetype=='group'){
											condition.pk_org=groupId;
										}else{
											condition.pk_org='GLOBLE00000000000000';
										}
								        if(item.attrcode == 'selfaccasoa'||item.attrcode == 'oppaccasoa'){
												item.refcode = 'uapbd/refer/fiacc/AccountDefaultGridTreeRef/index';
												item.isAccountRefer = false; 
												item.queryCondition = () => {
													return condition;
												}
								        }
		                                	item.visible = true;
		                           		}
		                        });
							}else {
								meta[table_id].items.map((item, index) => {
		                            if(item.attrcode == 'selfaccasoa'
							||item.attrcode == 'oppaccasoa'){
		                                item.visible = false;
		                            }else if(item.attrcode == 'selfsubjcode'
							||item.attrcode == 'oppsubjcode'){
							item.visible = true;
		                        	}
		//                             return item;
		                    });
							}
							this.props.meta.setMeta(meta);
						}, 1000)
						this.updateButtons(res);
					} else {
						this.props.form.EmptyAllFormValue(this.formId);
						// this.props.form.setAllFormValue({ [this.formId]: { rows: [] } });
						this.props.cardTable.setTableData(this.tableId, { rows: [] });
					}
					
				}
			});
		} else {
			setTimeout(()=>{
				this.props.form.setFormItemsValue(this.formId,{'startdate':{value:pubUtil.getSysDateStr(),diaplay:pubUtil.getSysDateStr()}});
				
			}, 100)
		}
		this.props.button.setDisabled({
			deleteline: true
		});
		this.toggleShow();
	}

	//切换页面状态
	toggleShow = () => {
		let status = this.props.getUrlParam('status');
		let flag = status === 'browse' ? false : true;
		this.props.button.setButtonVisible([ 'save','cancel','addline','deleteline'], flag);
		this.props.button.setButtonVisible([ 'delete', 'edit', 'query','add','refresh','back','copy','enable','disable','messageset' ], !flag);
		this.props.form.setFormStatus(this.formId, status);
		this.props.cardTable.setStatus(this.tableId, status);
	};
	//删除单据
	delConfirm = () => {

		let nodetype=this.props.getUrlParam('nodetype');
		let id=this.props.getUrlParam('id');
		let nextId = getNextId(id, dataSource);
		ajax({
			url: '/nccloud/gl/reconcile/reconcileruledelete.do',
			data: {
				id: this.props.getUrlParam('id'),
				ts: this.props.form.getFormItemsValue(this.formId, 'ts').value
			},
			success: (res) => {
				if (res) {
					deleteCacheById(pkname,id,dataSource);
					let nextCache = getCacheById(nextId, dataSource);
					if(nextCache){
						page.props.form.setAllFormValue({ [formId]: nextCache.head[formId] });
						page.props.cardTable.setTableData(tableId, nextCache.body[tableId]);
						// page.setButtonStatus(nextCache);
					  }else{
						this.props.props.setUrlParam(id);
						componentDidMount();
					  }
				}
			}
		});
	};

	remove = (data) => {
		
	}

	updateButtons(res){
		let nodetype=this.props.getUrlParam('nodetype');
		if(nodetype==NODETYPE.GROUP){
			let pk_group=res.data.head[this.formId].rows[0].values.pk_group;
			if(pk_group.value){
				this.props.button.setDisabled({
					delete: false,
					enable: false,
					disable: false,
					edit: false,
					messageset:false
				});
				let enablestate=res.data.head[this.formId].rows[0].values.enablestate;
				if(enablestate.value==ENABLESTATE.ENABLE){
					this.props.button.setDisabled({
						delete: true,
						edit: true,
						enable: true,
						disable: false
					});
				}else{
					this.props.button.setDisabled({
						delete: false,
						edit: false,
						enable: false,
						disable: true
					});
				}
			}else{
				this.props.button.setDisabled({
					enable: true,
					disable: true,
					delete: true,
					edit: true,
					messageset:true
				});
			}
		}else{
			let enablestate=res.data.head[this.formId].rows[0].values.enablestate;
			if(enablestate.value==ENABLESTATE.ENABLE){
			this.props.button.setDisabled({
				delete: true,
				edit: true,
				enable: true,
				disable: false
			});
		}else{
			this.props.button.setDisabled({
				delete: false,
				edit: false,
				enable: false,
				disable: true
			});
		}
		}
		
		let pk_contrastrule=res.data.head[this.formId].rows[0].values.pk_contrastrule;
		if(pk_contrastrule.value){
			this.props.cardTable.setStatus(this.tableId,'browse');
			//选择内部交易规则时,不可以进行填行和增行；
			this.props.button.setButtonVisible([ 'deleteline','addline'], false);
			//设置账簿类型不可变
			// this.props.form.setFormItemsValue(this.formId,{'crtelimvoucher':{value:true,diaplay:null}})
			this.props.form.setFormItemsDisabled(this.formId,{'pk_setofbook':true,"crtelimvoucher":true});
		}
	}
	//保存单据
	saveBill = () => {
		let check=this.props.form.isCheckNow(this.formId);
		if(check){
		//过滤表格空行
		let tableCheck=this.props.cardTable.checkTableRequired(this.tableId);
		if(!tableCheck){
			return;
		}
		let nodetype=this.props.getUrlParam('nodetype');
		let CardData = this.props.createMasterChildData(this.pageid, this.formId, this.tableId);
		let rows=CardData.body[this.tableId].rows;
		
		if(rows.length>0){
			let flag=false;
			for(let i=0;i<rows.length;i++){
				if(rows[i].status!=3){
					flag=true;
				}
			}
			if(flag==false){
				toast({color:"danger",content:this.state.json['2002130203-000010']});/* 国际化处理： 表体协同科目为空*/
			}else{
				CardData.head[this.formId].rows[0].values.pk_group.value=nodetype;
				let id=CardData.head[this.formId].rows[0].values.pk_reconcilerule.value;
				let url = '/nccloud/gl/reconcile/reconcilerulesave.do'; //新增保存
				ajax({
					url: url,
					data:CardData,
					success: (res) => {
						let pk_reconcilerule = null;
						if (res.success) {
							if (res.data) {
								toast({color:'success',content:this.state.json['2002130203-000009']});/* 国际化处理： 保存成功*/
								if (res.data.head && res.data.head[this.formId]) {
									this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
									pk_reconcilerule = res.data.head[this.formId].rows[0].values.pk_reconcilerule.value;
									if(id!=null && id!=''){
										updateCache(pkname,pk_reconcilerule,res.data,this.formId,dataSource);
									}else{
										addCache(pk_reconcilerule,res.data,this.formId,dataSource);	
									}
								}
								if (res.data.body && res.data.body[this.tableId]) {
									this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
								}
							}
						}
						// this.props.pushTo('/card', {
						// 	status: 'browse',
						// 	id: pk_reconcilerule,
						// 	nodetype:nodetype
						// });
						this.props.setUrlParam({status:'browse',id:pk_reconcilerule,nodetype:nodetype});
						this.toggleShow();
						this.updateButtons(res);
						this.forceUpdate();
					}
				});
			}
		}else{
			toast({color:"danger",content:this.state.json['2002130203-000010']});/* 国际化处理： 表体协同科目为空*/
		}
				}
		
		
	};

	getButton(page){
		let meta = page.props.meta.getMeta();
		let pk_setofbook = page.props.form.getFormItemsValue(page.formId,'pk_setofbook');
		page.pk_setofbook=pk_setofbook.value;
		if(pk_setofbook.value){
                         meta[table_id].items = meta[table_id].items.map((item, index) => {
                            if(item.attrcode == 'selfsubjcode'
							||item.attrcode == 'oppsubjcode'){
						item.visible = false;
                        }else if(item.attrcode == 'selfaccasoa'
						||item.attrcode == 'oppaccasoa'){
                                	item.visible = true;
                           		}
                            return item;
                        });
		}else {
			meta[table_id].items = meta[table_id].items.map((item, index) => {
                            if(item.attrcode == 'selfaccasoa'
					||item.attrcode == 'oppaccasoa'){
                                item.visible = false;
                            }else if(item.attrcode == 'selfsubjcode'
					||item.attrcode == 'oppsubjcode'){
					item.visible = true;
                        	}
                            return item;
                    });
		}
		page.props.meta.setMeta(meta);
	};

	getButtonNames = (codeId) => {
		if (codeId === 'edit' || codeId === 'add' || codeId === 'save') {
			return 'main-button';
		} else {
			return 'secondary - button';
		}
	};
	back = (props) =>{
		let type=this.props.getUrlParam('nodetype');
		this.props.pushTo('/list',{nodetype:type});
	};
	//获取列表肩部信息
	getTableHead = (buttons) => {
		return (
			<div className="shoulder-definition-area">
			{this.props.getUrlParam("status") != "browse"?
				<div className="definition-icons">
				{this.props.button.createButtonApp({
						area: "recrule_child",
						buttonLimit: 3,
						onButtonClick: buttonClick.bind(this),
						popContainer: document.querySelector(".nc-bill-table-area")
					})}
			</div>:''}
			{/* <div className="definition-icons"></div> */}
			</div>
		);
	};
	selectedChange(){
		let selectRows=this.props.cardTable.getCheckedRows(this.tableId);
        if(selectRows && selectRows.length>0){
			this.props.button.setDisabled({
				deleteline: false
			});
        }else{
			this.props.button.setDisabled({
				deleteline: true
			});
        }    
	}
	componentWillMount() {
		let callback= (json) =>{
			this.setState({json:json},()=>{
				initTemplate.call(this, this.props);
			})
		}
		getMultiLang({moduleId:'2002130203',domainName:'gl',currentLocale:'zh-CN',callback});
           window.onbeforeunload = () => {
            let status = this.props.getUrlParam("status");
            if (status == 'edit' || status == 'add') {
                return this.state.json['2002130203-000011']/* 国际化处理： 确定要离开吗？*/
            }
        }
    }
	render() {
		let { cardTable, form, button, modal } = this.props;
		let buttons = this.props.button.getButtons();
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createModal } = modal;
		let status=this.props.getUrlParam('status');
		let nodetype=this.props.getUrlParam('nodetype');
		return (
			<CardLayout>
				<TopArea>
				<Header
					showGoback={status=='browse'}
					onBackClick={this.back}
				>
					<HeaderButtonArea>
						{this.props.button.createButtonApp({
							area: 'recrule_card',
							buttonLimit: 10, 
							onButtonClick: buttonClick.bind(this), 
							popContainer: document.querySelector('.header-button-area')
						})}
					</HeaderButtonArea>
				</Header>
				<Form>
					{createForm(this.formId, {
						onAfterEvent: afterEvent.bind(this)
					})}
				</Form>
				</TopArea>
				<Table>
					{createCardTable(this.tableId, {
                        adaptionHeight:true,
						tableHead: this.getTableHead.bind(this, buttons),
						modelSave: this.saveBill,
						onBeforeEvent: tableBeforeEvents.bind(this),
						onAfterEvent: afterEvent.bind(this),
						selectedChange: this.selectedChange.bind(this),
						showIndex:true,
                        hideSwitch:()=>{return false;},
						showCheck: true
					})}
				</Table>
				<ModalArea>
					{createModal('messageset', {
						title: this.state.json['2002130203-000012'],// 弹框表头信息/* 国际化处理： 消息设置*/
						content: <div><Messageset pk_reconcilerule={this.state.pk_reconcilerule}/></div>, //弹框内容，可以是字符串或dom
						size:'xlg', //  模态框大小 sm/lg/xlg
						noFooter : true, //是否需要底部按钮,默认true
						
						className:''
					})}
					{createModal('delete', {
						title: multiLang && multiLang.get('20521030-0020'),
						content: multiLang && multiLang.get('20521030-0006'),
						beSureBtnClick: this.delConfirm
					})}
				</ModalArea>
			</CardLayout>
		);
	}
}

Card = createPage({
	mutiLangCode: '2052'
})(Card);

export default Card;
