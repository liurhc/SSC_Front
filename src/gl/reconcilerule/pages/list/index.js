//主子表列表

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base,cardCache,getMultiLang,createPageIcon} from 'nc-lightapp-front';
const { NCTabsControl } = base;
import Messageset from '../msg/index';
import { buttonClick, initTemplate, searchBtnClick, tableModelConfirm,doubleClick} from './events';
import './index.less';
import {ENABLESTATE,NODETYPE,dataSource,pkname} from './consts.js';
import {Header, HeaderButtonArea} from '../../../../fipub/public/components/layout/Header'
let {setDefData, getDefData } = cardCache;

let pagecode="2002130203_web_list";
class List extends Component {
	constructor(props) {
		super(props);
		this.moduleId = '2002';
		this.searchId = '20021302query';
		this.tableId = 'reconcilerule';
		this.pagecode=pagecode;
		this.state={
			querydata :'',
			pk_reconcilerule:'',
			json:{},
			inlt:null
		}
		// initTemplate.call(this, props);
	}
	componentDidMount() {
		this.getData();
	}
	componentWillMount(){
		let callback= (json,status,inlt) =>{
			this.setState({json:json,inlt},()=>{
				initTemplate.call(this,this.props); 
				// this.loadDept();
			})
		}
		getMultiLang({moduleId:'2002130203',domainName:'gl',currentLocale:'zh-CN',callback});
	}
	getButtonNames = (codeId) => {
		if (codeId === 'edit' || codeId === 'add' || codeId === 'save') {
			return 'main-button';
		} else {
			return 'secondary - button';
		}
	};

	onRowDoubleClick = (record,index)=>{
		let nodetype=this.props.getUrlParam('nodetype');
		this.props.pushTo('/card',{
			status:'browse',
			id: record.pk_reconcilerule.value,
			nodetype:nodetype
		})
	};

	getData = (serval) => {
		let { hasCacheData } = this.props.table;
		this.props.button.setDisabled({
			delete: true,
			edit: true,
			enable:true,
			disable:true,
			copy:true
		});
		if(!hasCacheData(dataSource)){
			let data = {
				 pagecode: '2002130203_web_list',
				 nodetype : this.props.getUrlParam('nodetype'),
				 oid:'1002Z310000000001TL5',  //查询模板id，手工添加在界面模板json中，放在查询区，后期会修改
				 querytype:'tree',
			};
	
			ajax({
				url: '/nccloud/gl/reconcile/reconcilerulequery.do',
				data: data,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						if (data) {
							this.props.table.setAllTableData(this.tableId, data[this.tableId]);
							setDefData(this.tableId, dataSource, data);
						} else {
							this.props.table.setAllTableData(this.tableId, { rows: [] });
						}
					}
				}
			});
		}
	};
	render() {
		let { table, button, search,modal } = this.props;
		let buttons = this.props.button.getButtons();
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { createModal } = modal;
		let { createButton, getButtons } = button;
		let nodetype=this.props.getUrlParam('nodetype');
		return (
			<div className="nc-bill-list" id="billlista">
				<Header>
					<HeaderButtonArea>
						{this.props.button.createButtonApp({
						area: 'recrule_list',
						buttonLimit: 4, 
						onButtonClick: buttonClick.bind(this), 
						popContainer: document.querySelector('.header-button-area')
						})}
					</HeaderButtonArea>
				</Header>
				<div className="nc-bill-search-area">
					{NCCreateSearch(this.searchId, {
						clickSearchBtn: searchBtnClick.bind(this),
						defaultConditionsNum: 6 //默认显示几个查询条件
					})}
				</div>
				<div className="nc-bill-table-area">
					{createSimpleTable(this.tableId, {
	//					handlePageInfoChange: pageInfoClick,
						tableModelConfirm: tableModelConfirm,
						// onSelected: this.updateButtonStatus.bind(this),
						showCheck: false,
						showIndex: true,
						dataSource: dataSource,
						pkname: pkname,
						onRowDoubleClick: doubleClick.bind(this)
					})}
				</div>
				<div>
					{createModal('messageset', {
						title: this.state.json['2002130203-000012'],// 弹框表头信息/* 国际化处理： 消息设置*/
						content: <div><Messageset pk_reconcilerule={this.state.pk_reconcilerule}/></div>, //弹框内容，可以是字符串或dom
						size:'xlg', //  模态框大小 sm/lg/xlg
						noFooter : true, //是否需要底部按钮,默认true
						
						className:'messagesetaa',
						closeButton:true
					})}
				</div>

			</div>
		);
	}

	updateButtonStatus(){
		let tableData = this.props.table.getCheckedRows(this.tableId);
		let nodetype=this.props.getUrlParam('nodetype');
		let length = tableData.length;//获取列表页选择数据的行数
		if(length>0){
			let enablestate=tableData[0].data.values.enablestate.value;
			this.props.button.setDisabled({
				copy:false
			});
			
			if(nodetype==NODETYPE.GLOBAL){
				if(enablestate==ENABLESTATE.UNENABLE){
					this.props.button.setDisabled({
						delete: false,
						edit: false,
						enable:false,
						disable:true,
					});
				}else if(enablestate==ENABLESTATE.ENABLE){
					this.props.button.setDisabled({
						delete: true,
						edit: true,
						enable:true,
						disable:false
					});
				}else{
					this.props.button.setDisabled({
						delete: false,
						edit: false,
						enable:false,
						disable:true
					});
				}
			}else{
				let pk_group=tableData[0].data.values.pk_group;
				if(pk_group.value){
					this.props.button.setDisabled({
						delete: false,
						edit: false,
						enable:false,
						disable:false,
						messageset:false
					});
					if(enablestate==ENABLESTATE.UNENABLE){
						this.props.button.setDisabled({
							delete: false,
							edit: false,
							enable:false,
							disable:true,
						});
					}else if(enablestate==ENABLESTATE.ENABLE){
						this.props.button.setDisabled({
							delete: true,
							edit: true,
							enable:true,
							disable:false
						});
					}else{
						this.props.button.setDisabled({
							delete: false,
							edit: false,
							enable:false,
							disable:true
						});
					}
				}else{
					this.props.button.setDisabled({
						delete: true,
						edit: true,
						enable:true,
						disable:true,
						messageset:true
					});
				}
			}
			
		}else{
			this.props.button.setDisabled({
				delete: true,
				edit: true,
				enable:true,
				disable:true,
				copy:true
			});
		}
	}
}


List = createPage({
	mutiLangCode: '2002'
})(List);

// ReactDOM.render(<List />, document.querySelector('#app'));
export default List;
