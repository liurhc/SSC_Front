import searchBtnClick from './searchBtnClick';
import initTemplate from './initTemplate';
import buttonClick from './buttonClick';
import doubleClick from './doubleClick';
import tableButtonClick from './tableButtonClick';
export { searchBtnClick,initTemplate,buttonClick,doubleClick,tableButtonClick};
