import { ajax, toast ,base,cardCache,getMultiLang} from 'nc-lightapp-front';
let { NCMessage } = base;
let {setDefData, getDefData } = cardCache;
import {ENABLESTATE,pagecode,dataSource,searchId,} from '../consts.js';
const tableButtonClick = (props, key, text, record, index,tableId,page) => {
    let nodetype=props.getUrlParam('nodetype');
    let searchVal=props.search.getAllSearchData(searchId);
    let pk_reconcilerule=record.pk_reconcilerule.value;
    let data;
    switch (key) {
        // 表格操作按钮
        case 'edit':
            props.pushTo('/card',{
                status:'edit',
                nodetype:nodetype,
                id: record.pk_reconcilerule.value
            })
        break;
        case 'delete':
            let {deleteCacheId} = props.table;
            ajax({
                url: '/nccloud/gl/reconcile/reconcileruledelete.do',
                data: {
                    pk_reconcilerule: record.pk_reconcilerule.value,
                    enablestate : record.enablestate.value,
                    ts: record.ts.value
                },
                success: (res) => { 
                    if (res.success) {
                        toast({color:"success",content:page.state.json['2002130203-000023']});/* 国际化处理： 删除成功*/
                        deleteCacheId(tableId,record.pk_reconcilerule.value);
                        props.table.deleteTableRowsByIndex(tableId, index);
                    }
                }
            });
            break;
        case 'copy':
            props.pushTo('/card',{
                status: 'edit',
                id: record.pk_reconcilerule.value,
                copyFlag:'copy',
                nodetype:nodetype
            })
            break;
        case 'enable':
            let enableData ={
                nodetype:nodetype,
                pk_reconcilerule:pk_reconcilerule,
                pagecode:pagecode,
                querycondition:searchVal,
                querytype:'tree',
                oid:'1002Z310000000001TL5'
            }
            ajax({
            url: '/nccloud/gl/reconcile/reconcilerulelistenable.do',
            data: enableData,
            success: (res) => {
                let { success, data } = res;
                if (success) {
                    if (data) {
                        props.table.setAllTableData(tableId, data[tableId]);
                        setDefData(tableId, dataSource, data);
                    } else {
                        props.table.setAllTableData(tableId, { rows: [] });
                    }
                    toast({color:"success",content:page.state.json['2002130203-000005']});/* 国际化处理： 启用成功*/
                }
            }
            });
            break;
        case 'disable':
            let disableData ={
                nodetype:nodetype,
                pk_reconcilerule:pk_reconcilerule,
                pagecode:pagecode,
                querycondition:searchVal,
                querytype:'tree',
                oid:'1002Z310000000001TL5'
            }
            ajax({
            url: '/nccloud/gl/reconcile/reconcilerulelistdisable.do',
            data: disableData,
            success: (res) => {
                let { success, data } = res;
                if (success) {
                    if (data) {
                        props.table.setAllTableData(tableId, data[tableId]);
                        setDefData(tableId, dataSource, data);
                    } else {
                        props.table.setAllTableData(tableId, { rows: [] });
                    }
                    toast({color:"success",content:page.state.json['2002130203-000006']});/* 国际化处理： 停用成功*/
                }
            }
            });
            break;
        case 'messageset':
            page.state.pk_reconcilerule=record.pk_reconcilerule.value;
            page.props.modal.show('messageset');
            break;
        default:
            break;
    }
};
export default tableButtonClick;
