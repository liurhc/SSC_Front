import {ajax,cardCache,getMultiLang,toast} from 'nc-lightapp-front';
import {dataSource} from '../consts.js';
let {setDefData, getDefData } = cardCache;
//点击查询，获取查询区数据
export default function clickSearchBtn(props,searchVal) {
    // if(searchVal){
        let data={
            querycondition:searchVal,
            pagecode: '2002130203_web_list',
            //queryAreaCode:20521030,  //查询区编码
            oid:'1002Z310000000001TL5',  //查询模板id，手工添加在界面模板json中，放在查询区，后期会修改
            querytype:'tree',
            nodetype : props.getUrlParam('nodetype'),
        };
        
        this.state.querydata = data;
        ajax({
            url: '/nccloud/gl/reconcile/reconcilerulequery.do',
            data: data,
            success: (res) => {
                let { success, data } = res;
                if (success) {
                    if(data){
                        this.props.table.setAllTableData(this.tableId, data[this.tableId]);
                        setDefData(this.tableId, dataSource, data);
                        toast({color:"success",content:this.state.inlt && this.state.inlt.get('2002130203-000031',{count : data[this.tableId].rows.length})});
                    }else{
                        this.props.table.setAllTableData(this.tableId, {rows:[]});
                        toast({color:"warning",content:this.state.json['2002130203-000032']})
                    }
                    
                }
            }
        });
    // }
    
};
