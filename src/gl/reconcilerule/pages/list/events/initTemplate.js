import { createPage, ajax, base, toast,getMultiLang} from 'nc-lightapp-front';
let { NCPopconfirm, NCIcon } = base;
import {NODETYPE,ENABLESTATE} from '../consts.js';
import {NULL_STR, GLOBAL_ORG} from '../../../../public/common/consts.js'
import tableButtonClick  from './tableButtonClick';

let searchId = '20021302query';
let tableId = 'reconcilerule';
let pageId = '2002130203_web_list';
let appcode = '2002130203';
export default function (props) {
		let nodetype=props.getUrlParam('nodetype');
		if(nodetype==NODETYPE.GROUP){
			appcode='2002130202';
		}
		let page=this;
	props.createUIDom(
		{
			pagecode: pageId,//页面id
			appcode: appcode//注册按钮的id
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta,page);
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					props.button.setPopContent('delete',page.state.json['2002130203-000024']);/* 国际化处理： 确认要删除该信息吗？*/
					props.button.setPopContent('enable',page.state.json['2002130203-000019']);/* 国际化处理： 是否确认要启用？*/
					props.button.setPopContent('disable',page.state.json['2002130203-000020']);/* 国际化处理： 是否确认要停用？*/
				}
			}
		}
	)
}
function getListButtons(text, record, index,page){
	let nodetype=page.props.getUrlParam('nodetype');
	let buttons=[];
	if(record.enablestate.value != ENABLESTATE.ENABLE){
		if(nodetype=="group"){
			if(!isGlobal(record.pk_group.value)){
				buttons= ['edit','delete','copy','enable','messageset'];
			}else{
				buttons= ['copy','messageset'];
			}
		}else{
			buttons = ['edit','delete','copy','enable','messageset'];
		}
	}else{
		if(nodetype=="group"){
			if(!isGlobal(record.pk_group.value)){
				buttons= ['copy','disable','messageset'];
			}else{
				buttons= ['copy','messageset'];
			}
		}else{
			buttons= ['copy','disable','messageset'];
		}
	}
	return buttons;
}

/**
 * 是否全局数据
 * @param {所属组织主键} pk_org 
 */
function isGlobal(pk_org){
	if(pk_org && pk_org != NULL_STR && pk_org != GLOBAL_ORG){
		return false;
	}
	return true;
}

function seperateDate(date){
	if (typeof date !=='string') return ;
	let result = date.split(' ') && date.split(' ')[0];
	return result;
}

function modifierMeta(props, meta,page) {
	meta[searchId].items=meta[searchId].items.map((item, key) => {
		let nodetype=props.getUrlParam('nodetype');
		let  type='global';
		if(nodetype=='group'){
			type='all';
		}
		if (item.attrcode == 'pk_setofbook') {
			item.refcode='uapbd/refer/org/SetOfBookGridRef/index';
			item.isMultiSelectedEnabled = true;
		}
		if (item.attrcode == 'pk_contrastrule') {
			item.refcode = 'gl/refer/voucher/ContrastRuleGridRef/index';
			item.isMultiSelectedEnabled = true;
			item.queryCondition = () => {
				return {startstatus:type,"isDataPowerEnable": 'Y',"DataPowerOperationCode" : 'fi'};
			}
		}
		return item;
	})

	meta[tableId].items = meta[tableId].items.map((item, key) => {
		let nodetype=props.getUrlParam('nodetype');
		if (item.attrcode == 'code') {
			item.render = (text, record, index) => {
				return (
					<a
						style={{cursor: 'pointer'}}
						onClick={() => {
							props.pushTo('/card', {
								nodetype:props.getUrlParam('nodetype'),
								status: 'browse',
								id: record.pk_reconcilerule.value
							});
						}}
					>
						{record.code && record.code.value}
					</a>
				);
			};
		}
		if (item.attrcode == 'dbilldate'){
			item.render = (text, record, index) => {
				return (
					<span>
						{record.dbilldate && seperateDate(record.dbilldate.value)}
					</span>
				);
			};
		}
		if(nodetype=='global'){
			if(item.attrcode == 'pk_group'){
				item.visible=false;
			}
		}
		return item;
	});

	meta[tableId].items.push({
		attrcode: 'opr',
		label: page.state.json['2002130203-000008'],/* 国际化处理： 操作*/
		fixed: 'right',
		itemtype: 'customer', 
		visible: true,
		className: 'table-opr',
		width:'160px',
		render: (text, record, index) => {
            return props.button.createOprationButton(getListButtons(text, record, index,page), {
                area: "list_inner",
                buttonLimit: 3,
                onButtonClick: (props, key) => tableButtonClick(props, key, text, record, index,tableId,page)
            });
        }
	});
	let multiLang = props.MutiInit.getIntl('2052');
	return meta;
}
