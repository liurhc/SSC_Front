import { base, ajax, toast,promptBox,cardCache,getMultiLang} from 'nc-lightapp-front';
let { NCMessage } = base;
import {ENABLESTATE,NODETYPE,dataSource,pkname} from '../consts.js';
let {setDefData, getDefData } = cardCache;
export default function buttonClick(props, id) {
    let rows = props.table.getCheckedRows(this.tableId);
    let nodetype=props.getUrlParam('nodetype');
    switch (id) {
        case 'add':
            props.pushTo('/card',{
                status:'add',
                nodetype:nodetype
            })
        break;
        case 'edit':      
            if(rows){
                let record = rows[0].data.values;
                props.pushTo('/card',{
                    status:'edit',
                    nodetype:nodetype,
                    id: record.pk_reconcilerule.value
                })
            }
             break;
         case 'delete':
            promptBox({
                color:"info",
                title: this.state.json['2002130203-000018'],/* 国际化处理： 您确定要删除所选数据*/
                beSureBtnClick: deleteAction.bind(this,rows,props)
            });
         break;
         case 'refresh':
            let data=this.state.querydata;
            if(null==data || data ==''){
                data={"nodetype":nodetype, pagecode: '2002130203_web_list',};
            }
            ajax({
                url: '/nccloud/gl/reconcile/reconcilerulequery.do',
                data: data,
                success: (res) => {
                    let { success, data } = res;
                    if (success) {
                        if (data) {
                            this.props.table.setAllTableData(this.tableId, data[this.tableId]);
                        } else {
                            this.props.table.setAllTableData(this.tableId, { rows: [] });
                        }
                        toast({color:"success",title:this.state.json['2002130203-000030']})
                    }
                }
            });
             break;
        case 'enable':
            promptBox({
                color:"info",
                title: this.state.json['2002130203-000019'],/* 国际化处理： 是否确认要启用？*/
                beSureBtnClick:  enableRule.bind(this,props)
            });
            break;
        case 'disable':
            promptBox({
                color:"info",
                title: this.state.json['2002130203-000020'],/* 国际化处理： 是否确认要停用？*/
                beSureBtnClick:  disableRule.bind(this,props)
            });
            break;
        case 'copy':
        if(rows){
            let record = rows[0].data.values;
            props.pushTo('/card',{
                status: 'edit',
                id: record.pk_reconcilerule.value,
                copyFlag:'copy',
                nodetype:nodetype
            })
        }
        break;
        case 'messageset':
        if(rows.length>0){
            let record = rows[0].data.values;
            this.state.pk_reconcilerule=record.pk_reconcilerule.value;
            this.props.modal.show('messageset');
        }else{
            toast({color:"warning",content:this.state.json['2002130203-000021']});/* 国际化处理： 请选择公有协同设置*/
        }
        break;
    }
}

function enableRule(props){
    let rows = props.table.getCheckedRows(this.tableId);
    if(rows.length==1){
        let searchVal=props.search.getAllSearchData(this.searchId);
        let pk_reconcilerule=rows[0].data.values.pk_reconcilerule.value;
        let nodetype=props.getUrlParam('nodetype');
        let data ={
            nodetype:nodetype,
            pk_reconcilerule:pk_reconcilerule,
            pagecode:this.pagecode,
            querycondition:searchVal,
            querytype:'tree',
            oid:'1002Z310000000001TL5'
        }
        ajax({
        url: '/nccloud/gl/reconcile/reconcilerulelistenable.do',
        data: data,
        success: (res) => {
            let { success, data } = res;
            if (success) {
                //  let enablestate = { value: ENABLESTATE.ENABLE, display: ENABLESTATE.ENABLENAME, scale: 0 };
                //  props.table.setValByKeyAndIndex(this.tableId,rows[0].index,'enablestate',enablestate);
                if (data) {
                    props.table.setAllTableData(this.tableId, data[this.tableId]);
                    setDefData(this.tableId, dataSource, data);
                } else {
                    props.table.setAllTableData(this.tableId, { rows: [] });
                }
                toast({color:"success",content:this.state.json['2002130203-000005']});/* 国际化处理： 启用成功*/
            }
        }
        });
    }else{
        toast({color:"warning",content:this.state.json['2002130203-000022']});/* 国际化处理： 请选择一条数据*/
    }
  }
  
  function disableRule(props){
    let rows = props.table.getCheckedRows(this.tableId);
    if(rows.length==1){
        let searchVal=props.search.getAllSearchData(this.searchId);
    let pk_reconcilerule=rows[0].data.values.pk_reconcilerule.value;
    let nodetype=props.getUrlParam('nodetype');
    let data ={
        nodetype:nodetype,
        pk_reconcilerule:pk_reconcilerule,
        pagecode:this.pagecode,
        querycondition:searchVal,
        querytype:'tree',
        oid:'1002Z310000000001TL5'
    }
    ajax({
      url: '/nccloud/gl/reconcile/reconcilerulelistdisable.do',
      data: data,
      success: (res) => {
        let { success, data } = res;
        if (success) {
            // let enablestate = { value: ENABLESTATE.UNABLE, display: ENABLESTATE.UNABLENAME, scale: 0 };
            //  props.table.setValByKeyAndIndex(this.tableId,rows[0].index,'enablestate',enablestate);
            if (data) {
                props.table.setAllTableData(this.tableId, data[this.tableId]);
                setDefData(this.tableId, dataSource, data);
            } else {
                props.table.setAllTableData(this.tableId, { rows: [] });
            }
            toast({color:"success",content:this.state.json['2002130203-000006']});/* 国际化处理： 停用成功*/
        }
      }
    });
    }else{
        toast({color:"warning",content:this.state.json['2002130203-000022']}); /* 国际化处理： 请选择一条数据*/
    }
  }

  function deleteAction(rows,props){
    let {deleteCacheId} = this.props.table;
        if(rows){
            let record = rows[0].data.values;
            ajax({
                url: '/nccloud/gl/reconcile/reconcileruledelete.do',
                data: {
                    pk_reconcilerule: record.pk_reconcilerule.value,
                    enablestate : record.enablestate.value,
                    ts: record.ts.value
                },
                success: (res) => { 
                    if (res.success) {
                        toast({color:"success",content:this.state.json['2002130203-000023']});/* 国际化处理： 删除成功*/
                        deleteCacheId(this.tableId,record.pk_reconcilerule.value);
                        props.table.deleteTableRowsByIndex(this.tableId, rows[0].index);
                    }
                }
            });
        }
  }
