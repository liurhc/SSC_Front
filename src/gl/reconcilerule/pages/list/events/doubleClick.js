import { ajax, toast,cacheTools } from 'nc-lightapp-front';

export default function doubleClick(record,props, index, e) {
    this.props.pushTo('/card', {
        nodetype:this.props.getUrlParam('nodetype'),
        status: 'browse',
        id: record.pk_reconcilerule.value
    });
}
