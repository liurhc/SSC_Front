import { ajax, toast ,base,cardCache,getMultiLang} from 'nc-lightapp-front';

export const ENABLESTATE = {
    UNENABLE : "1",//未启用
    ENABLE : "2",//已启用   
    UNABLE : "3",//已停用
    // UNENABLENAME:'未启用',/* 国际化处理： 未启用*/
    // ENABLENAME : '已启用',   /* 国际化处理： 已启用*/
    // UNABLENAME : '已停用',/* 国际化处理： 已停用*/
}

export const NODETYPE = {
    GROUP : "group",
    GLOBAL : "global"
}
export const dataSource = 'gl.reconcilerule.pages.2002130203';
export const dataSourceGlobal = 'gl.reconcilerule.pages.2002130203Global';
export const dataSourceGroup = 'gl.reconcilerule.pages.2002130203Group';
export const pkname = 'pk_reconcilerule';

export const searchId = '20021302query';

export const  pagecode="2002130203_web_list";
