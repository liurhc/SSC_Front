import ajax from '../../../fipub/public/common/ajax'

let actions = {
    queryList:(data, callback) => {
        ajax({
            url:'/nccloud/gl/accountcontrast/queryList.do',
            data,
            successcb:(res) => {
                if(typeof callback === 'function'){
                    callback(res);
                }
            }
        });
    },
    queryByPk:(data, callback) =>{
        let url = '/nccloud/gl/accountcontrast/queryByPk.do';
        ajax({data, url, successcb:callback});
    },
    insert:(data, callback) =>{
        let url = '/nccloud/gl/accountcontrast/insert.do';
        ajax({data, url, successcb:callback});
    },
    update:(data, callback) =>{
        let url = '/nccloud/gl/accountcontrast/update.do';
        ajax({data, url, successcb:callback});
    },
    delete:(data, callback) => {
        let url = '/nccloud/gl/accountcontrast/del.do';
        ajax({data, url, successcb:callback});
    }
}

export {actions};
