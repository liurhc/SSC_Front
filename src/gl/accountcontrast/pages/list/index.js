import {BaseComponent, OPENTYPE} from '../../../../fipub/public/components/BaseComponent'
import { createPage, high, getMultiLang} from 'nc-lightapp-front';
import {Header, HeaderButtonArea, HeaderSearchArea} from '../../../../fipub/public/components/layout/Header'
import {ListLayout, Table, ModalArea} from '../../../../fipub/public/components/layout/ListLayout'
import {initTemplate, buttonClick, onTableSelectedChange} from './events'
import {LIST_CONSTS, dataSource, pkname, CARD_CONSTS} from '../consts'
import ReferLoader from '../../../../fipub/public/components/ReferLoader'
import {REFCODES} from '../../../../fipub/public/common/consts'
import AccountContrastCacheUtil from '../AccountContrastCacheUtil';
import {actions} from '../actions';
import getDefaultAccountBook from '../../../public/components/getDefaultAccountBook.js';
import {commonApi} from '../../../public/common/actions';

class AccountContrastList extends BaseComponent{
    constructor(props){
        super(props, LIST_CONSTS.TABLE_ID);
        this.state = {
            showGoback : false,
            selectedPks:[],
            accountingbooks:[]
        }
        this.cacheUtil = new AccountContrastCacheUtil(props.ViewModel);
        let accountingbooks = this.cacheUtil.getAccountingbookInfo();
        if(!accountingbooks || accountingbooks.length <= 0){
            getDefaultAccountBook(this.getAppcode()).then((defaultBookInfo) => {
                if(defaultBookInfo && defaultBookInfo.value){
                    commonApi.queryBookCombineInfo({data:defaultBookInfo.value, success:(result) => {
                        let {data,success} = result;
                        if(success && data.isParallel){
                            accountingbooks = [];
                            let accountingbook = {refpk:defaultBookInfo.value, refname: defaultBookInfo.display};
                            accountingbooks.push(accountingbook);
                            this.cacheUtil.setAccountingbookInfo(accountingbooks);
                            this.setState({accountingbooks});
                            this.refresh();
                        }
                    }});
                }
            })
        }
    }

    componentWillMount(){
        let accountingbooks = this.cacheUtil.getAccountingbookInfo();
        this.setState({accountingbooks});
		/* 加载多语资源 */
		let callback = (json, status, intl) =>{
			if(status){
				this.setState({json, intl}, () => {
					initTemplate.call(this, this.props);
				});
			}else{
			}
		}
		getMultiLang({moduleId:'20028201', domainName:'gl', callback}); 
    }

    /**
     * 更新账簿选中信息
     */
    updateAccountingbookInfo = (v) =>{
        let accountingbooks = [];
        if(v && v.length > 0){
            v.map((item) => {
                let info = {refpk:item.refpk, refname:item.refname};
                accountingbooks.push(info);
            });
        }
        this.cacheUtil.setAccountingbookInfo(accountingbooks);
        this.setState({accountingbooks})
    }

    /**
     * 刷新页面
     */
    refresh = (callback) => {
        let page = this;
        let pagecode = page.getPagecode();
        let pk_accountingbooks = [];
        let accountingbooksInfo = page.cacheUtil.getAccountingbookInfo();
        if(accountingbooksInfo && accountingbooksInfo.length>0){
            accountingbooksInfo.map((item) => {
                pk_accountingbooks.push(item.refpk);
            });
        }
        let params = {pagecode, pk_accountingbooks};
        actions.queryList(params, (data) => {
            page.loadTableData(data);
            if(typeof callback === 'function'){
                callback(data);
            }
        });
    }

    loadTableData = (data) =>{
        let page = this;
        let tableData = {rows:[]};
        if(data){
            tableData = data[LIST_CONSTS.TABLE_ID];
        }
        page.props.table.setAllTableData(LIST_CONSTS.TABLE_ID, tableData);
        page.updateDelBtnState();
    }

    getSelectedPks = () => {
        let page = this;
        let pks = [];
        let rows = page.getCheckedRows();
        if(rows && rows.length>0){
            rows.map((row) => {pks.push(row.data.values.pk_accountcontrast.value)});
        }
        return pks;
    }

    updateDelBtnState = () => {
        let page = this;
        let num = page.getCheckedRowsNum();
        let disabled = true;
        if(num) disabled = false;
        let btnState = {del:disabled};
        page.cacheUtil.setBtnState(btnState);
        page.props.button.setButtonDisabled(btnState);
    }

    render(){
        let page = this;
        let {showGoback, selectedPks} = this.state;
        let {table, button} = this.props;
        let tableId = LIST_CONSTS.TABLE_ID;
        let accountingbooksInfo = this.state.accountingbooks;
        let appcode = page.getAppcode();
		const {ExcelImport}=high;
        return (
            <ListLayout>
                <Header showGoback = {showGoback}>
                    <HeaderSearchArea>
                        <ReferLoader
                            tag = 'temp'
                            fieldid='accountbook_ref'
                            refcode = {REFCODES.ACCOUNTINGBOOK}
                            value = {accountingbooksInfo}
                            isMultiSelectedEnabled = {true}
                            queryCondition = {() => {
                                let condition = {
                                    TreeRefActionExt:'nccloud.web.gl.ref.AccntBookFilterUnParaSqlBuilder,nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                                    appcode:appcode
                                };
                                return condition;
                            }}
                            onChange = {(v) => {
                                this.updateAccountingbookInfo(v);
                                this.refresh();
                            }}
                        />
                    </HeaderSearchArea>
                    <HeaderButtonArea>
                        {button.createButtonApp({
                            area:'header',
                            buttonLimit:3,
                            onButtonClick:buttonClick.bind(page)
                        })}
                    </HeaderButtonArea>
                </Header>
                <Table>
                    {table.createSimpleTable(tableId, {
			fieldid:'accntctrst',
                        dataSource:dataSource,
                        pkname:pkname,
                        showCheck:true,
                        onRowDoubleClick:(record, index, props, e)=>{
                            let params = {};
                            params.pk = record.pk_accountcontrast.value;
                            params.pagecode = CARD_CONSTS.PAGE_CODE;
                            params.status = 'browse';
                            page.open(OPENTYPE.PUSH, '/card', params);
                        },
                        selectedChange:onTableSelectedChange.bind(this)
                    })}
                </Table>
                <ModalArea>
                    <ExcelImport
						{...Object.assign(page.props)}
						moduleName="gl" //模块名
						billType="ACCM" //单据类型
						// billType="C0" //单据类型
						selectedPKS={selectedPks}
						appcode={page.getAppcode()}
						pagecode={page.getPagecode()}
						referVO={{ignoreTemplate:true}}
                    />
                </ModalArea>
            </ListLayout>
        );
    }
}

AccountContrastList = createPage({})(AccountContrastList);
export default AccountContrastList;
