import {toastWarning, toastSuccess} from '../../../../../fipub/public/common/toast'
import {OPENTYPE} from '../../../../../fipub/public/components/BaseComponent'
import {CARD_CONSTS, LIST_CONSTS} from '../../consts'
import {actions} from '../../actions'
import { promptBox } from "nc-lightapp-front"

export default function(props, id){
    let page = this;
    switch(id){
        case 'add': add(page); break;
        case 'del': del(page); break;
        case 'export': exportExcel(page); break;
        case 'refresh': refresh(page); break;
    }
}

function add(page){
    let params = {pagecode:CARD_CONSTS.PAGE_CODE, status:'add'};
    page.open(OPENTYPE.PUSH, '/card', params);
}

function del(page){
    promptBox({
        color:'warning',
        content:page.state.json['20028201-000000'],/* 国际化处理： 该操作将删除选中核算账簿下的所有科目对照条目\n您确定要删除所选数据吗？*/
        beSureBtnClick:() => {
            let selectedRows = page.props.table.getCheckedRows(LIST_CONSTS.TABLE_ID);
            let datas = [];
            let indexs = [];
            for(var key in selectedRows){
                datas.push(selectedRows[key].data);
                indexs.push(key);
            }
            actions.delete(datas, (data) => {
                toastSuccess(page.state.json['20028201-000001']);/* 国际化处理： 删除成功*/
                page.delTableRowsByIndex(indexs);
                page.updateDelBtnState();
            }); 
        }
    });
    
}

function exportExcel(page){
    let selectedPks = page.getSelectedPks();
    page.setState({selectedPks}, () => {
        page.props.modal.show('exportFileModal');
    });
}

function refresh(page){
    page.refresh(() => {toastSuccess(page.state.json['20028201-000002'])});/* 国际化处理： 刷新成功*/
}
