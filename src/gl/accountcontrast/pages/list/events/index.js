import initTemplate from './initTemplate';
import buttonClick from './buttonClick';
import tableBtnClick from './tableBtnClick';
import onTableSelectedChange from './tableSelectedChange';

export {initTemplate, buttonClick, tableBtnClick, onTableSelectedChange};
