/**
 * 根据选中行数切换删除按钮状态
 * 
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} newNum 
 * @param {*} oldNum 
 */
export default function onTableSelectedChange(props, moduleId, newNum, oldNum){
    let page = this;
    // if((newNum && !oldNum) || (!newNum && oldNum)){
        page.updateDelBtnState();
    // }
}
