import {OPENTYPE} from '../../../../../fipub/public/components/BaseComponent'
import {toastSuccess} from '../../../../../fipub/public/common/toast'
import {CARD_CONSTS, LIST_CONSTS} from '../../consts'
import {actions} from '../../actions'

export default function(page, key, record, index){
    switch(key){
        case 'edit_line': editLine(page, record); break;
        case 'del_line': delLine(page, record, index); break;
    }
}

function editLine(page, record){
    let params = {status:'edit', pagecode:CARD_CONSTS.PAGE_CODE};
    params.pk = record.pk_accountcontrast && record.pk_accountcontrast.value;
    page.open(OPENTYPE.PUSH, '/card', params);
}

function delLine(page, record, index){
    delete record.key;
    let values = record
    let data = [{status:'0', values}];
    actions.delete(data, (data) => {
        toastSuccess(page.state.json['20028201-000001']);/* 国际化处理： 删除成功*/
        page.delTableRowsByIndex(index);
        page.props.table.deleteCacheId(LIST_CONSTS.TABLE_ID, record.pk_accountcontrast.value);
        page.updateDelBtnState();
    });
}
