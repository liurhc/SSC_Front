import {LIST_CONSTS} from '../../consts';
import {tableBtnClick} from './index'
import {excelImportconfig} from 'nc-lightapp-front'
export default function(props){
    this.createUIDom(
        {initTemplate, initBtn, doneCallback}
    );
}

function initTemplate(page, meta){
    let opr = {
        label:page.state.json['20028201-000003'],/* 国际化处理： 操作*/
        attrcode:'opr',
        itemtype:'customer',
        width:'150px',
        visible:true,
        fixed:'right',
        render:(text, record, index) => {
            let btnArr = ['edit_line','del_line'];
            return page.props.button.createOprationButton(btnArr, {
                area:'table_opr',
                buttonLimit:3,
                onButtonClick:(props, key) => tableBtnClick(page, key, record, index)
            });
        }
    }
    meta[LIST_CONSTS.TABLE_ID].items.push(opr);
}

function initBtn(page, button){
    let appcode = page.getAppcode();
    let pagecode = page.getPagecode();
    let excelConfig = excelImportconfig(page.props, 'gl', 'ACCM', true, '', {appcode, pagecode}, page.refresh);
    page.props.button.setUploadConfig('import', excelConfig);
    let btnState = page.cacheUtil.getBtnState();
    if(btnState){
        page.props.button.setButtonDisabled(btnState);
    }else{
        page.updateDelBtnState();
    }
}

function doneCallback(page){
    page.props.button.setPopContent('del_line', page.state.json['20028201-000004']);/* 国际化处理： 确认要删除该信息吗？*/
}
