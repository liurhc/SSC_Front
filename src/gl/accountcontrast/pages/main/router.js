import { asyncComponent } from 'nc-lightapp-front';
import AccountContrastList from '../list';

const Card = asyncComponent(() => import (/* webpackChunkName:gl/accountcontrast/pages/card/card */ /* webpackMode: "eager" */ '../card'));

const routes = [
    {path:'/', component:AccountContrastList, exact:true},
    {path:'/list', component:AccountContrastList},
    {path:'/card', component:Card}
];

export default routes;
