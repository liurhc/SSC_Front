import {CARD_CONSTS} from '../../consts'
import {commonApi} from '../../../../public/common/actions'
/**
 * 表单的编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} oldValue 
 */
export default function onAfterFormEvent(props, moduleId, key, value, oldValue){
    let page = this;
    if(moduleId === CARD_CONSTS.FORM_ID){
        if(key === 'accounting'){
            let pk_accountingbook;
            if(value && value.value){
                pk_accountingbook = value.value;
            }
            page.updatePk_orgInfo(pk_accountingbook);
            //TODO 清空子表数据
            page.props.cardTable.setTableData(CARD_CONSTS.TABLE_ID, {rows:[]});
            page.addRow(0, true);
            page.resetRowNO();
        }
    }
}
