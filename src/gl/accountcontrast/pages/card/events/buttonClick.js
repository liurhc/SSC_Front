import {CARD_CONSTS, dataSource, pkname} from '../../consts';
import {cardCache, promptBox, deepClone} from "nc-lightapp-front";
import {actions} from '../../actions'
import { toastSuccess } from '../../../../../fipub/public/common/toast';

export default function(page, id){
    switch(id){
        case 'add': add(page); break;
        case 'edit': edit(page); break;
        case 'del': del(page); break;
        case 'refresh': refresh(page); break;
        case 'save': save(page); break;
        case 'cancel': cancel(page); break;
        case 'add_line': add_line(page); break;
        case 'del_line': del_line(page); break;
        case 'copy': copy(page); break;
        case 'cancel_copy': cancelCopy(page); break;
        case 'paste': paste(page); break;
        case 'insert': insert(page); break;
        case 'cut': cut(page); break;
    }
}

function add(page){
    page.initAddPage();
    page.setUrlParam({pk:'', status:'add'});
    page.toggleShow();
}
function edit(page){
    page.setUrlParam({status:'edit'});
    page.toggleShow();
}
function del(page){
    promptBox({
        title:page.state.json['20028201-000006'],/* 国际化处理： 删除 */
        color:'warning',
        content:page.state.json['20028201-000000'],/* 国际化处理： 该操作将删除选中核算账簿下的所有科目对照条目\n您确定要删除所选数据吗？*/
        beSureBtnClick:() => {
            let formData = page.props.form.getAllFormValue(CARD_CONSTS.FORM_ID);
            let data = [{status:'0', values:formData.rows[0].values}];
            actions.delete(data, (data) =>{
                toastSuccess(page.state.json['20028201-000001']);/* 国际化处理： 删除成功*/
                let pk = page.getUrlParam('pk');
                let nextPk = cardCache.getNextId(pk, dataSource);
                cardCache.deleteCacheById(pkname, pk, dataSource);
                page.setUrlParam({pk:nextPk});
                page.loadCardData();
            });
        }
    });
 
}
function refresh(page){
    let pk = page.getUrlParam('pk');
    if(!pk) return;
    page.queryCardData(pk, (data) => {
        if(data){
            page.setCardData(data);
            toastSuccess(page.state.json['20028201-000002'])/* 国际化处理： 刷新成功！*/
        }else{
            cardCache.deleteCacheById(pkname, pk, dataSource);
            page.clearPageData();
        }
    });
}

function save(page){
    page.saveData();
}

function cancel(page){
    let pk = page.getUrlParam('pk');
    pk = pk ? pk : cardCache.getCurrentLastId(dataSource);
    page.setUrlParam({pk, status:'browse'});
    page.toggleShow();
    page.loadCardData();
}
function add_line(page){
    let {cardTable} = page.props;
    let index = cardTable.getNumberOfRows(CARD_CONSTS.TABLE_ID);
    cardTable.addRow(CARD_CONSTS.TABLE_ID, index, {}, true);
    page.resetRowNO();
}
function del_line(page){
    let selectedRows = page.getCheckedRows();
    let indexs = [];
    if(selectedRows){
        selectedRows.map((item) => {
            indexs.push(item.index);
        })
    }
    page.delTableRowsByIndex(indexs);
    page.resetRowNO();
}

/**
 * 表头复制事件
 * @param {*} page 
 */
function copy(page){
    let rows = page.props.cardTable.getCheckedRows(CARD_CONSTS.TABLE_ID);
    if(rows){
        rows = deepClone(rows);
        rows.map((one) => {
            one.data.values.pk_accountcontrast_b = {display:null, value:null};
        });
        page.cacheUtil.setCopyData(rows);
    }
    //TODO 更改表格按钮显示状态
    let show = true;
    
    page.updateCopyStateBtnState(show);
}

function paste(page){
    let rows = page.cacheUtil.getCopyData();
    let rowNum = page.props.cardTable.getNumberOfRows(CARD_CONSTS.TABLE_ID);
    if(rows && rows.length > 0){
        let datas = [];
        rows.map((item) => {
            let values = item.data.values;
            let data = {values};
            datas.push(data);
        });
        page.props.cardTable.insertRowsAfterIndex(CARD_CONSTS.TABLE_ID, datas, rowNum - 1);
        page.resetRowNO();
    }
}
function insert(page){}

function cancelCopy(page){
    page.cacheUtil.setCopyData([]);
    let show = false;
    // page.props.cardTable.resetTableData(CARD_CONSTS.TABLE_ID); /* 会取消卡片的编辑状态 */
    page.updateCopyStateBtnState(false);
}

function cut(page){
    page.updateTableData(page);
    let rows = page.getCheckedRows(CARD_CONSTS.TABLE_ID);
    if(!rows || rows.length <= 0) return;
    let indexs = [];
    rows = deepClone(rows);
    rows.map((row) => {
        row.data.values.pk_accountcontrast_b = {display:null, value:null};
        indexs.push(row.index);
    });
    page.cacheUtil.setCopyData(rows);
    page.delTableRowsByIndex(indexs);
    page.updateCopyStateBtnState(true);
}
