import {CARD_CONSTS} from '../../consts'
import {tableBtnClick} from './index'
export default function(props){
    let page = this;
    page.createUIDom({initTemplate, initBtn, doneCallback});
}

function initTemplate(page, meta){
    initFormMeta(page, meta);
    initTableMeta(page, meta);
}

function initTableMeta(page, meta){
    meta[CARD_CONSTS.TABLE_ID].items.map((item) => {
        item.isSort = false;
        if(item.attrcode == 'source'){
            item.isShowDisabledData = false;
            item.queryCondition = () =>{
                return getAccountCondition(page, 'source');
            }
        }else if(item.attrcode == 'pripose'){
            item.onlyLeafCanSelect = true;
            item.isShowDisabledData = false;
            item.queryCondition = () => {
                return getAccountCondition(page, 'pripose');
            }
        }else if(item.attrcode == 'pk_accountcontrast_b'){
            item.initialvalue = {display:null, value:null};
        }else if(item.attrcode == 'pk_accountcontrast'){
            let pk = page.getUrlParam('pk');
            item.initialvalue = {display:pk, value:pk};
        }
    });
    let opr = {
        label:page.state.json['20028201-000003'],/* 国际化处理： 操作*/
        attrcode:'opr',
        itemtype:'customer',
        className:'table_opr',
        fixed:'right',
        width:'200px',
        visible:true,
        render:(value, record, index)=>{
            let btnArr = ['insert', 'del_line', 'copy_line', 'paste_here', 'cut_line'];
            return (
                <div>
                    {page.props.button.createOprationButton(btnArr, {
                        area:'table_row',
                        buttonLimit:3,
                        onButtonClick:(props, key) => tableBtnClick(page, key, record, index)
                    })}
                </div>
            );
        }
    }
    meta[CARD_CONSTS.TABLE_ID].items.push(opr);
}

function initFormMeta(page, meta){
    let appcode = page.getAppcode();
    meta[CARD_CONSTS.FORM_ID].items.map((item) => {
        if(item.attrcode === 'approvestatus'){
            item.initialvalue = {value:'-1'};
        }
        if(item.attrcode === 'accounting'){
            let accountbooks = page.cacheUtil.getAccountingbookInfo();
            if(accountbooks && accountbooks.length > 0){
                item.initialvalue = {display:accountbooks[0].refname, value:accountbooks[0].refpk};
                page.updatePk_orgInfo(accountbooks[0].refpk);
            }
            item.queryCondition = () => {
                let condition = {
                    TreeRefActionExt:'nccloud.web.gl.ref.AccntBookFilterUnParaSqlBuilder,nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                    appcode:appcode
                };
                return condition;
            }
        }
    });
}

function getAccountCondition(page, key){
    let pk_accountingbook = page.getAccountingbookInfo();
    let dateStr = page.getBuziDate();
	let condition =  {
		isDataPowerEnable : 'Y',
		DataPowerOperationCode : 'fi',
		pk_accountingbook:pk_accountingbook,
        dateStr : dateStr,
        showFi : 'N',
        showBudget: 'N',
		TreeRefActionExt: "nccloud.web.gl.ref.AccntTypeSqlBuilder4Parallel"
    };
    if(key === 'source') condition.showFi = 'Y'; 
    if(key === 'pripose') condition.showBudget = 'Y';
    return condition;
}

function initBtn(page, buttons){
}

function doneCallback(page){
    page.toggleShow();
    page.loadCardData();
    page.disableTailForm(page.props.form);
}
