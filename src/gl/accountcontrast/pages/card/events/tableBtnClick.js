import {CARD_CONSTS} from '../../consts'
import {deepClone} from "nc-lightapp-front";

export default function(page, key, record, index){
    switch(key){
        case 'insert':
            insert(page, index + 1);
            break;
        case 'del_line':
            delline(page, index);
            break;
        case 'copy_line':
            copyline(page, index);
            break;
        case 'paste_here':
            pastehere(page, index);
            break;
        case 'cut_line':
            cutline(page, index);
            break;
    }    
}

function insert(page, index){
    page.addRow(index, true);
    page.resetRowNO();
}

function copyline(page, index){
    page.props.cardTable.pasteRow(CARD_CONSTS.TABLE_ID, index, ['pk_accountcontrast_b']);
    page.resetRowNO();
}

function cutline(page, index){
    page.updateTableData(page);
    let rows = page.getRowsByIndexs(index);
    let row = deepClone(rows[0]);
    row.values.pk_accountcontrast_b = {display:null, value:null};
    let cacheData = [{data:row}]
    page.cacheUtil.setCopyData(cacheData);
    page.delTableRowsByIndex(index);
    page.updateCopyStateBtnState(true);
}

function delline(page, index){
    page.delTableRowsByIndex(index);
    page.resetRowNO();
}

function pastehere(page, index){
    let rows = page.cacheUtil.getCopyData();
    if(rows && rows.length > 0){
        let datas = [];
        rows.map((item) => {
            let values = item.data.values;
            let data = {values};
            datas.push(data);
        });
        page.props.cardTable.insertRowsAfterIndex(CARD_CONSTS.TABLE_ID, datas, index);
        page.resetRowNO();
    }
}
