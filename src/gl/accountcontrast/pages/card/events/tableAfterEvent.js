import {CARD_CONSTS} from '../../consts'
/**
 * 表格编辑后事件
 * 
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedrows 
 * @param {*} index 
 * @param {*} record 
 * @param {*} type 
 * @param {*} method 
 */
export default function onAfterEvent(props, moduleId, key, value, changedrows, index, record, type, method){
    let page = this;
    if(moduleId == CARD_CONSTS.TABLE_ID){
        if(key === 'source' || key === 'pripose'){
            dealWithAccasoaChange(page, moduleId, index, key, value, record);
        }
    }
}

/**
 * 处理科目变更事件
 */
function dealWithAccasoaChange(page, moduleId, index, key, value, record){
    if(key === 'source'){
        record.values.srass = {};
        record.values.source = {display:value.dispname, value:value.refpk};
    }else if(key === 'pripose'){
        record.values.prass = {};
        record.values.pripose = {display:value.dispname, value:value.refpk};
    }
}
