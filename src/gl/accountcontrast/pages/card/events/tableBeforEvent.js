import {commonApi} from '../../../../public/common/actions'

export default function beforEvent(props, moduleId, key, value, index, record) {
    let page = this;
    if (key === 'srass') {
        let pk_accasoa = getPk_accasoa(record, key);
        if(pk_accasoa){
            showAssModal(page, moduleId, key, index, record);
        }
        return false;
    }else if(key === 'prass'){
        let pk_accasoa = getPk_accasoa(record, key);
        if(pk_accasoa){
            showAssModal(page, moduleId, key, index, record);
        }
        return false;
    }
    return true;
}

function showAssModal(page, moduleId, key, index, record) {
    let prepareddate = page.getBuziDate();
    let pk_accasoa = getPk_accasoa(record, key);
    commonApi.queryAssItem({prepareddate, pk_accasoa}, (data) => {
        if(data && data.length > 0){
            let pk_org = page.cacheUtil.getPk_org();
            let assid = getAssId(record, key);
            let pretentAssData = {
                pk_accountingbook: page.getAccountingbookInfo(),
                pk_accasoa:pk_accasoa,
                assid:assid,
                prepareddate:prepareddate,
                pk_org:pk_org,
                checkboxShow:false,
                linenum:index
            };
            let {assModalState} = page.state;
            assModalState = {
                show:true,
                defaultValue:pretentAssData,
                callback:(data)=>{
                    updateAssInfo(page, moduleId, key, index, data);
                    page.closeAssModal();
                }
            }
            page.setState({assModalState});
        }
    });
}

function getPk_accasoa(record, key){
    let accasoaKey = key === 'srass' ? 'source' : 'pripose';
    let item = record.values[accasoaKey];
    return item && item.value;
}

function getAssId(record, key){
    let item = record.values[key];
    return item&&item.value;
}

function updateAssInfo(page, moduleId, key, index, assInfo){
    assInfo  = assInfo ? assInfo : {};
    page.props.cardTable.setValByKeysAndIndex(moduleId, index, {[key]:{display: assInfo.assname, value:assInfo.assid}});
}
