import initTemplate from './initTemplate';
import buttonClick from './buttonClick';
import tableBtnClick from './tableBtnClick';
import onAfterEvent from './tableAfterEvent';
import beforEvent from './tableBeforEvent';
import onAfterFormEvent from './onAfterFormEvent';
import onTableSelectedChange from './tableSelectedChange';

export {initTemplate, buttonClick, tableBtnClick, onAfterEvent, beforEvent, onAfterFormEvent, onTableSelectedChange};
