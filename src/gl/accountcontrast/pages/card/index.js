import { BaseComponent, OPENTYPE } from '../../../../fipub/public/components/BaseComponent'
import { createPage, cardCache, getMultiLang } from 'nc-lightapp-front';
import { CardLayout, Form, Table, ModalArea, TopArea } from '../../../../fipub/public/components/layout/CardLayout'
import { Header, HeaderSearchArea, HeaderButtonArea, HeaderPaginationArea } from '../../../../fipub/public/components/layout/Header'
import { initTemplate, buttonClick, onAfterEvent, beforEvent, onAfterFormEvent, onTableSelectedChange } from './events'
import { CARD_CONSTS, LIST_CONSTS, dataSource, pkname } from '../consts'
import { actions } from '../actions'
import AccountContrastCacheUtil from '../AccountContrastCacheUtil'
import { TABLE_TYPE, VOSTATUS } from '../../../../fipub/public/common/consts'
import AssModal from '../../../public/components/modals/AssModal'
import {commonApi} from '../../../public/common/actions';
import { toastSuccess } from '../../../../fipub/public/common/toast';
class AccountContrastCard extends BaseComponent {
    constructor(props) {
        super(props, CARD_CONSTS.TABLE_ID, TABLE_TYPE.CARD);
        this.cacheUtil = new AccountContrastCacheUtil(props.ViewModel);
        this.state = {
            showGoback: false,
            assModalState: { show: false, callback: null, defaultValue: {} },
            cardTableShowCheck:false
        }
    }

    componentWillMount() {
        /* 加载多语资源 */
		let callback = (json, status, intl) =>{
			if(status){
				this.setState({json, intl}, () => {
					initTemplate.call(this, this.props);
				});
			}
		}
		getMultiLang({moduleId:'20028201', domainName:'gl', callback});
    }

    componentDidMount(){
        this.setBeforeUnload();
        this.updateBathTableBtnDisableState();
        this.props.cardTable.toggleCardTable(CARD_CONSTS.TABLE_ID, true);
    }

    disableTailForm(form){
        let disabled = false;
        let status = {
            creator:disabled,
            createtime:disabled,
            modifier:disabled,
            modifiedtime:disabled
        };
        form.setFormItemsDisabled(CARD_CONSTS.FORM_TAIL_ID, status);
    }

    /**
     * 切换页面状态
     */
    toggleShow = () => {
        let page = this;
        let status = this.getPageStatus();
        page.props.form.setFormStatus(CARD_CONSTS.FORM_ID, status);
        status = status === 'browse' ? 'browse' : 'edit';
        page.props.cardTable.setStatus(CARD_CONSTS.TABLE_ID, status);
        this.updateBtnShowState(status);
        this.updateTableOprState(status);

        //更改cardTable showCheck状态
        let cardTableShowCheck = status === 'browse' ? false : true;
        page.setState({cardTableShowCheck});
        
        page.updateAccntBookEditableState(page.getPageStatus());
        page.updateHeaderBtnState();

    }

    /**
     * 更新按钮显示状态
     * 只与页面状态有关
     */
    updateBtnShowState = (status) => {
        let page = this;
        let btnVisibleState = {};
        let browseKeys = ['add', 'edit', 'del', 'refresh'];
        let editKeys = ['save', 'cancel', 'add_line', 'del_line', 'copy', 'cut'];
        let disabledKeys = ['cancel_copy', 'paste'];
        let visible = true;
        if (status !== 'browse') visible = false;
        browseKeys.map((item) => { btnVisibleState[item] = visible });
        editKeys.map((item) => { btnVisibleState[item] = !visible });
        disabledKeys.map((item) => { btnVisibleState[item] = false });
        page.props.button.setButtonsVisible(btnVisibleState);
        page.setState({ showGoback: visible });
    }

    updateTableOprState = (state) => {
        let page = this;
        let visible = state === 'browse' ? false : true;
        let meta = page.props.meta.getMeta();
        if (meta && meta[CARD_CONSTS.TABLE_ID]) {
            meta[CARD_CONSTS.TABLE_ID].items.map((item) => {
                if (item.attrcode === 'opr') {
                    item.visible = visible;
                }
            });
            page.props.meta.setMeta(meta);
            if (visible) {
                page.updateCopyStateBtnState(false);
            }
        }
    }

    updateCopyStateBtnState = (isCopying) => {
        let page = this;
        let btnState = {};
        let copyingBtn = ['paste_here', 'paste', 'cancel_copy'];
        let normalBtn = ['insert', 'del_line', 'copy_line', 'add_line', 'copy', 'cut', 'cut_line'];
        copyingBtn.map((item) => { btnState[item] = isCopying });
        normalBtn.map((item) => { btnState[item] = !isCopying });
        page.props.button.setButtonsVisible(btnState);
    }

    /**
     * 更新按钮可用状态
     * 只与数据状态有关
     */
    updateBtnDisableState = () => {

    }

    /**
     * 更新核算账簿参照是否可编辑
     * 修改时不可编辑
     * 新增时可编辑
     */
    updateAccntBookEditableState = (pageStatus) => {
        let disabled = pageStatus === 'edit';
        if(pageStatus !== 'browse'){
            let page = this;
            page.props.form.setFormItemsDisabled(CARD_CONSTS.FORM_ID, {accounting:disabled});
        }
    }

    /**
     * 更新组织信息
     */
    updatePk_orgInfo = (pk_accountingbook) => {
        let page = this;
        let pk_org;
        if(pk_accountingbook){
            let data = {pk_accountingbook};
            commonApi.queryOrgByAccountBook(data, (data)=>{
                pk_org = data.pk_org;
                page.cacheUtil.setPk_org(pk_org);
            });
        }else{
            page.cacheUtil.setPk_org(pk_org);
        }
    }

    onButtonClick = (props, id) => {
        let page = this;
        buttonClick(page, id);
    }

    /**
     * 渲染列表肩部按钮
     */
    renderTableHeader = () => {
        let page = this;
        return (
            <div>
                {page.props.button.createButtonApp({
                    area: 'table_header',
                    onButtonClick: page.onButtonClick
                })}
            </div>
        )
    }

    //TODO 获取核算账簿信息，清空核算账簿后的处理逻辑
    getAccountingbookInfo = () => {
        let page = this;
        let bookInfo = page.props.form.getFormItemsValue(CARD_CONSTS.FORM_ID, 'accounting');
        return bookInfo && bookInfo.value;
    }

    /**
     * 请求卡片数据
     */
    queryCardData = (pk, callback) => {
        let pagecode = this.getPagecode();
        let params = { pagecode, pk };
        actions.queryByPk(params, (data) => {
            cardCache.updateCache(pkname, pk, data, CARD_CONSTS.FORM_ID, dataSource);
            if (typeof callback === 'function')
                callback(data);
        });

    }

    /**
     * 加载卡片数据
     */
    loadCardData = () => {
        let page = this;
        let pk = this.getUrlParam('pk');
        if (pk) {
            let data = cardCache.getCacheById(pk, dataSource);
            if (data) {
                page.setCardData(data);
            }
            page.queryCardData(pk, this.setCardData);
        }else{/* 清空页面数据 */
            page.clearPageData();
        }
    }

    clearPageData = () => {
        let page = this;
        page.setUrlParam({pk:''});
        page.props.form.EmptyAllFormValue(CARD_CONSTS.FORM_ID);
        page.props.cardTable.setTableData(CARD_CONSTS.TABLE_ID, {rows:[]});
        page.updateHeaderBtnState();
    }

    /**
     * 切换页面状态时更新表头按钮状态
     * 当前无数据时【修改、删除、刷新】按钮不可用
     */
    updateHeaderBtnState = () => {
        let page = this;
        let pk = page.getUrlParam('pk');
        let disbled = !pk;
        let btns = ['edit', 'del', 'refresh'];
        page.props.button.setButtonDisabled(btns, disbled);
    }

    /**
     * 将卡片数据加载到页面组件中
     */
    setCardData = (data) => {
        if(!data) return;
        let page = this;
        if (data.head) {
            let formData = data.head[CARD_CONSTS.FORM_ID];
            page.props.form.setAllFormValue({ [CARD_CONSTS.FORM_ID]: formData });
            //TODO 更新pk_org信息
            let pk_accountingbook = formData && formData.rows[0].values.accounting.value;
            page.updatePk_orgInfo(pk_accountingbook);
        }
        if (data.body) {
            let bodyData = data.body[CARD_CONSTS.TABLE_ID];
            page.props.cardTable.setTableData(CARD_CONSTS.TABLE_ID, bodyData);
        }
    }

    saveData = () => {
        let page = this;
        let status = page.getPageStatus();
        page.props.cardTable.filterEmptyRows(CARD_CONSTS.TABLE_ID, ['source', 'pripose'], 'include');
        if (!page.checkDataRequiredCard({ formids: CARD_CONSTS.FORM_ID, tableids: CARD_CONSTS.TABLE_ID })) return;
        let cardData = page.props.createMasterChildData(CARD_CONSTS.PAGE_CODE, CARD_CONSTS.FORM_ID, CARD_CONSTS.TABLE_ID);
        if (status === 'edit') {
            actions.update(cardData, (data) => {
                let pk = data.head[CARD_CONSTS.FORM_ID].rows[0].values.pk_accountcontrast.value;
                page.setUrlParam({ status: 'browse' });
                page.setCardData(data);
                cardCache.updateCache(pkname, pk, data, CARD_CONSTS.FORM_ID, dataSource);
                page.toggleShow();
                toastSuccess(page.state.json['20028201-000005']);/* 国际化处理：保存成功 */
            });
        } else if (status === 'add') {
            actions.insert(cardData, (data) => {
                let pk = data.head[CARD_CONSTS.FORM_ID].rows[0].values.pk_accountcontrast.value;
                page.setUrlParam({ pk, status: 'browse' });
                page.setCardData(data);
                cardCache.addCache(pk, data, CARD_CONSTS.FORM_ID, dataSource);
                page.toggleShow();
                toastSuccess(page.state.json['20028201-000005']);/* 国际化处理：保存成功 */
            });
        }
    }

    /**
     * 初始化新增页面
     */
    initAddPage = () => {
        let page = this;
        page.props.form.EmptyAllFormValue(CARD_CONSTS.FORM_ID);
        page.props.cardTable.setTableData(CARD_CONSTS.TABLE_ID, { rows: [] });
        page.props.cardTable.addRow(CARD_CONSTS.TABLE_ID, 0, {}, false);
    }

    /**
     * 重置表格数据行号
     */
    resetRowNO = () => {
        let page = this;
        let rows = page.getAllRows();
        let data = { rows: [] };
        if (rows) {
            rows.map((item, index) => {
                item.values.rowno = { value: index + 1 + '', display: item.index + 1 + '' };
                if (item.status && item.status == VOSTATUS.UNCHANGED)
                    item.status = VOSTATUS.UPDATED;
            })
            data.rows = rows;
            page.setTableData(data);
        }
    }

    closeAssModal = () => {
        let page = this;
        let {assModalState} = page.state;
        assModalState.show = false;
        page.setState({ assModalState });
    }

    /**
     * 更新表格数据，用于剪切取消时恢复表格状态
     */
    updateTableData(page){
        let allRows = page.getAllRows();

    }

    /**
     * 更新行批量操作按钮可用状态
     */
    updateBathTableBtnDisableState = () =>{
        let page = this;
        let num = page.getCheckedRowsNum();
        let disabled = true;
        if(num) disabled = false;
        page.props.button.setButtonDisabled(['del_line', 'copy', 'cut'], disabled);
    }

    render() {
        let page = this;
        let { button, form, cardTable } = page.props;
        let { showGoback, assModalState, cardTableShowCheck } = page.state;
        return (
            <CardLayout>
                <TopArea>
                    <Header
                        showGoback={showGoback}
                        onBackClick={() => {
                            page.open(OPENTYPE.PUSH, '/list', { pagecode: LIST_CONSTS.PAGE_CODE, status:'browse' });
                        }}
                        >
                        <HeaderButtonArea>
                            {button.createButtonApp({
                                area: 'header',
                                buttonLimit: 3,
                                onButtonClick: page.onButtonClick
                            })}
                        </HeaderButtonArea>
                    </Header>
                    <Form>
                        {form.createForm(CARD_CONSTS.FORM_ID, {
			    fieldid:'accntctrst',
                            onAfterEvent:onAfterFormEvent.bind(this),
                        })}
                    </Form>
                </TopArea>
                <Table>
                    {cardTable.createCardTable(CARD_CONSTS.TABLE_ID, {
                        adaptionHeight:true,
			fieldid:'accntctrst',
                        tableHead: page.renderTableHeader,
                        onAfterEvent: onAfterEvent.bind(this),
                        onBeforeEvent: beforEvent.bind(this),
                        showIndex: true,
                        showCheck: cardTableShowCheck,
                        selectedChange: onTableSelectedChange.bind(this),
                        hideSwitch:()=>{return false;}
                    })}
                </Table>
                <ModalArea>
                    {/* 辅助核算弹窗 */}
                    <AssModal
                        showOrHide={assModalState.show}
                        pretentAssData={assModalState.defaultValue}
                        onConfirm={assModalState.callback}
                        showDisableData='no'
                        handleClose={() => {
                            page.closeAssModal();
                        }}
                    />
                </ModalArea>
            </CardLayout>
        );
    };
}

AccountContrastCard = createPage({
    orderOfHotKey: [CARD_CONSTS.FORM_ID, CARD_CONSTS.TABLE_ID]
})(AccountContrastCard);
export default AccountContrastCard;
