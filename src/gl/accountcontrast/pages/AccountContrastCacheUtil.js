import CacheUtil from '../../public/common/CacheUtil';

const dataSource = 'fi.gl.accountcontrast.cache'

/**
 * 缓存工具类
 */
class AccountContrastCacheUtil extends CacheUtil{
    constructor(ViewModal){
        super(ViewModal, dataSource);
    }

    getAccountingbookInfo(){
        return this.getData('accountingbook');
    }

    setAccountingbookInfo(info){
        this.setData('accountingbook', info);
    }

    setPk_org(pk_org){
        this.setData('pk_org', pk_org);
    }

    getPk_org(){
        return this.getData('pk_org');
    }

    getCopyData(){
        return this.getData('copyData');
    }

    setCopyData(copyData){
        this.setData('copyData', copyData);
    }

    setBtnState(btnState){
        this.setData('btnState', btnState);
    }

    getBtnState(){
        return this.getData('btnState');
    }

}

export default AccountContrastCacheUtil;
