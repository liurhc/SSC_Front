const LIST_CONSTS = {
    TABLE_ID:'list_table',
    PAGE_CODE:'20028201_list'
}

const CARD_CONSTS = {
    PAGE_CODE:'20028201_card',
    FORM_ID: 'form',
    TABLE_ID: 'card_table',
    FORM_TAIL_ID: 'tail'
}

const dataSource = 'fi.gl.accountcontrast.dataSource';
const pkname = 'pk_accountcontrast';

export {LIST_CONSTS, CARD_CONSTS, dataSource, pkname}
