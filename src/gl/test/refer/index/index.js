/**
 * Created by mianh on 2018/4/13 0013.
 */
import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { base } from 'nc-lightapp-front';

const { NCCol: Col, NCRow: Row } = base;

import './index.less'

import Qinhaof from './qinhaof.js'


class RefDemoTest extends Component {
	render() {
		return (
			<Row>
				<Col md={12} xs={12} sm={12}>
					<h1>参照测试</h1>
				</Col>

				<Qinhaof/>

			</Row>
		)
	}
};

ReactDOM.render(<RefDemoTest />, document.querySelector('#app'));
