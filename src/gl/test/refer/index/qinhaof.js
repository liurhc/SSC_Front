import React, { Component } from 'react';

import { base } from 'nc-lightapp-front';

const { NCCol: Col, NCRow: Row } = base;


import TransSortPermTreeRef from '../../../refer/voucher/TransSortPermTreeRef/index';
import ContrastRuleTreeRef from '../../../refer/voucher/ContrastRuleGridRef/index';
import TransCurrGridRef from '../../../refer/voucher/TransCurrGridRef/index';
import TransSortTreeRef from '../../../refer/voucher/TransSortTreeRef/index';
import GLAccPeriodDefaultTreeGridRef from '../../../refer/voucher/GLAccPeriodDefaultTreeGridRef/index';
import TransProjGridRef from '../../../refer/transfer/TransProjGridRef/index';
import AmortizeGridRef from '../../../refer/amortize/AmortizeGridRef';

export default class Qinhaof extends Component {
    constructor(props) {
        super(props)
        this.state = {
            configs: {}
        }
    }

    createCfg(id, param) {
        var obj = {
            value: this.state.configs[id] ? this.state.configs[id].value : [],
            onChange: function (val) {
                var temp = Object.assign(this.state.configs[id], { value: val });
                this.setState(Object.assign(this.state.configs, temp));
            }.bind(this)
        }
        this.state.configs[id] = obj;
        var result_param = Object.assign(obj, param)
        return result_param;
    }

    render() {
        return (
            <Col md={12} xs={12} sm={12}>
                <Row>
                    <Col md={12} xs={12} sm={12}>
                        <h2>秦皓</h2>
                    </Col>
                </Row>
                <Row>
                   <Col md={4} xs={4} sm={4}>
                        <Row>
                            <Col md={4} xs={4} sm={4}>
                                角色和操作员：
                            </Col>
                            <Col md={8} xs={8} sm={8}>
                                {TransSortPermTreeRef({} = this.createCfg("TransSortPermTreeRef", {
                                    isMultiSelectedEnabled: true,
                                    queryCondition: function () {
                                        let condition = {
                                            pk_accountingbook : '1001Z310000000009DD2'
                                        }
                                        return condition;
                                    }
                                }))}
                            </Col>
                        </Row>
                    </Col>
                    <Col md={4} xs={4} sm={4}>
                        <Row>
                            <Col md={4} xs={4} sm={4}>
                                内部交易对账规则：
                            </Col>
                            <Col md={8} xs={8} sm={8}>
                                {ContrastRuleTreeRef({} = this.createCfg("ContrastRuleTreeRef", {
                                    isMultiSelectedEnabled: true,
                                    queryCondition: function () {
                                        let condition = {
                                            pk_accountingbook : '1001Z310000000009DD2'
                                        }
                                        return condition;
                                    }
                                }))}
                            </Col>
                        </Row>
                    </Col>
                    <Col md={4} xs={4} sm={4}>
                        <Row>
                            <Col md={4} xs={4} sm={4}>
                                所有币种档案：
                            </Col>
                            <Col md={8} xs={8} sm={8}>
                                {TransCurrGridRef({} = this.createCfg("TransCurrGridRef", {
                                    isMultiSelectedEnabled: true,
                                    queryCondition:function () {
                                        let condition = {
                                            showAllCurr : 'Y',
                                            showLocalCurr : "Y"
                                        }
                                        return condition;
                                    }
                                }))}
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row>
                    <Col md={4} xs={4} sm={4}>
                        <Row>
                            <Col md={4} xs={4} sm={4}>
                                结转分类档案：
                            </Col>
                            <Col md={8} xs={8} sm={8}>
                                {TransSortTreeRef({} = this.createCfg("TransSortTreeRef", {
                                    isMultiSelectedEnabled: true
                                }))}
                            </Col>
                        </Row>
                    </Col>
                    <Col md={4} xs={4} sm={4}>
                        <Row>
                            <Col md={4} xs={4} sm={4}>
                                自定义结转方案：
                            </Col>
                            <Col md={8} xs={8} sm={8}>
                                {TransProjGridRef({} = this.createCfg("TransSortTreeRef", {
                                    queryCondition:() => {
                                        return { pk_org:''};
                                    }
                                }))}
                            </Col>
                        </Row>
                    </Col>
                     <Col md={4} xs={4} sm={4}>
                        <Row>
                            <Col md={4} xs={4} sm={4}>
                                会计期间方案：
                            </Col>
                            <Col md={8} xs={8} sm={8}>
                                {GLAccPeriodDefaultTreeGridRef({} = this.createCfg("GLAccPeriodDefaultTreeGridRef", {
                                    isMultiSelectedEnabled: true,
                                    queryCondition : () => {
                                        let condition = {
                                            pk_accountingbook: '1001Z310000000009DD2',
                                            showPeriodInit: 'Y'
                                        }    
                                        return condition;
                                    }
                                }))}
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row>
                    <Col md={4} xs={4} sm={4}>
                        <Row>
                            <Col md={4} xs={4} sm={4}>
                                周期凭证定义：
                            </Col>
                            <Col md={8} xs={8} sm={8}>
                                {AmortizeGridRef({} = this.createCfg("AmortizeGridRef", {
                                    queryCondition:() => {
                                        return { pk_org:''}; /* 核算账簿 */
                                    }
                                }))}
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Col>
        )
    }
};