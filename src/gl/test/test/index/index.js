import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {StrUtils} from '../../../public/utils/StrUtils'

/**
 * 测试页面 add by qinhaof
 */
class TestPage extends Component{
    constructor(props){
        super(props);
        this.state = {
            inputValue : ''
        }
    }

    handleChange = (e) => {
        let maxLength = 10;
        let value = e.target.value;
        let inputValue = StrUtils.subStrByByteLength(value, maxLength);
        this.setState({inputValue});
    }

    render(){
        let value = this.state.inputValue;
        return (
        <div>
            <input type = 'text' value = {value} onChange = {this.handleChange}/>
        </div>);
    }
}

ReactDOM.render(<TestPage />, document.querySelector('#app'));