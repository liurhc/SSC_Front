import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';
// import moment from 'moment';

import {high,base,ajax,deepClone, createPage, getMultiLang } from 'nc-lightapp-front';
import './index.less';


const { Refer } = high;

const {
	NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
	NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
	NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCModal: Modal,NCInput: Input
} = base;

import { toast } from '../../../../public/components/utils';

const NCOption = Select.NCOption;
const format = 'YYYY-MM-DD';
const Timeformat = "YYYY-MM-DD HH:mm:ss";
import {handleNumberInput} from "../../../../manageReport/common/modules/numberInputFn";
import BusinessUnitTreeRef from '../../../../../uapbd/refer/orgv/BusinessUnitVersionDefaultAllTreeRef';

import createScript from '../../../../public/components/uapRefer.js';
import {
    businessUnit,
    createReferFn,
    getCheckContent,
    getReferDetault, renderRefer,returnMoneyType 
} from "../../../../manageReport/common/modules/modules";
import {subjectRefer} from "../../../../manageReport/referUrl";
import DateGroup from '../../../../manageReport/common/modules/dateGroup'
import CheckBoxCells from '../../../../manageReport/common/modules/checkBox';
import {handleValueChange} from '../../../../manageReport/common/modules/handleValueChange'



class CentralConstructorModal extends Component {

	constructor(props) {
		super(props);
		this.state={
            appcode: "20023025",
            groupCurrency: true,//是否禁止"集团本币"可选；true: 禁止
            globalCurrency: true, //是否禁止"全局本币"可选；true: 禁止
		    json: {},
            dateInputPlaceholder: '',/* 国际化处理： 选择日期*/
            assistDateValue: '', //辅助参照中的日期的state值
            assistDateTimeValue: '', //辅助参照中的日期+时分秒的state值
            stringRefValue: '', //辅助参照中的字符的state值
            numberRefValue: '',//辅助参照中的数值的state值
            numberRefValueInteger: '',//辅助参照中整数值
            booleanValue: '', //辅助参照中布尔值

            selectedKeys: [],
            targetKeys: [],
            changeParam: false, //false:点击条件时不触发接口，,
            rightSelectItem: [], //科目的"条件"按钮中存放右侧数据的pk值


            showRadio: false,//控制是否显示"会计期间"的但选框；false: 不显示
            start: {},  //开始期间
            end: {}, //结束期间
            /*****会计期间******/
            selectionState: 'true', //按期间查询true，按日期查询false
            startyear: '', //开始年
            startperiod: '',  //开始期间
            endyear: '',  //结束年
            endperiod: '',   //结束期间
            /*****日期******/
            begindate: '', //开始时间
            enddate: '', //结束时间
            rangeDate: [],//时间显示

			accountingbook: {
				refname: '',
				refpk: '',
			},  //核算账簿
			accasoa: {           //科目
				refname:'',
				refpk: '',
			},
			buSecond: { },//二级单元
			pk_accperiodscheme: '', //会计期间参照用的过滤
			vouchertype: {
				refname:'',
				refpk: '',
			},
			accAssItems: [],
			newList: [],
			buorg: '',
			prepareddate: '',
			configs: {},//人员参照
			versiondateList: [], //科目版本列表
			accountlvl: '1', //设置级次1到几
			selectedQueryValue: '0', //查询方式单选
			selectedCurrValue: '1', //返回币种单选
			ismain: '0', // 0主表  1附表
			currtype: '',      //选择的币种
			currtypeName: '',  //选择的币种名称
			currtypeList: [],  //币种列表
			defaultCurrtype: {         //默认币种
				name: '',
				pk_currtype: '',
			},
			checkbox: {
				one: false,
				two: false,
				three: false,
			},
			talliedscope: '0',
			balanceori: '-1', //余额方向
			startlvl: '1', //开始级次
			endlvl: '1',  //结束级次
			startInput: '', //凭证开始数字
			endInput: '', //凭证结束数字
			startRest: '', //余额范围开始
			endRest: '',   //余额范围结束
			startSummary: '', //摘要前
			endSummary: '', //摘要位
			makePerson:{  //制单人
			    refcode:"",
			    refname:"",
			    refpk:""
			},
			startcode: {}, //开始科目编码
			endcode: {
				refname:'',
				refpk: '',
				refcode: '',
			},    //结束科目编码
			versiondate: '', //制单日期，后续要用下拉选
			isleave: false, //是否所有末级
			isoutacc: false, //是否表外科目
			includeuntally: false, //包含凭证的4个复选框 未记账
			includeerror: false, //包含凭证的4个复选框 错误
			includetranfer: true, //包含凭证的4个复选框 损益结转
			includereclassify: false, //包含凭证的4个复选框 重分类
			mutibook: 'N', //多主体显示方式的选择
			disMutibook: true, //多主体显示是否禁用
			currplusacc: 'Y', //排序方式
			disCurrplusacc: true, //排序方式是否禁用
			showzerooccur: false, //无发生不显示
			showzerobalanceoccur: true, //无余额无发生不显示
			sumbysubjtype: false, //科目类型小计
			disSumbysubjtype: true, //科目类型小计是否禁用
			twowaybalance: false, //借贷方显示余额
			showSecond: false, //是否显示二级业务单元
			multbusi: false, //多业务单元复选框
			isversiondate: false,
			begintime: '', 			
			endtime: '', 
			timeStyle: '1',
			tableDataSource: []
		};

        this.renderRefer = renderRefer.bind(this);
		this.handleValueChange = handleValueChange.bind(this);
        this.handleNumberInput = handleNumberInput.bind(this);
	}

	static defaultProps = {
		show: false,
		title: '',
		content: '',
		closeButton: false,
		icon: 'icon-tishianniuchenggong',
		isButtonWhite: true,
		isButtonShow: true,
		// accAssItems: [],
	};


    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json,
                dateInputPlaceholder: this.state.json['20023025-000000'],/* 国际化处理： 选择日期*/
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20023025', 'dategroup', 'checkbox', 'subjectandlevel'],domainName:'gl',currentLocale:'simpchn',callback});
    }
    componentDidMount() {
        setTimeout(() => getReferDetault(this, false, {
            businessUnit
        }),0)
    }
    selectJournalTable(record, index){
        let {accAssItems} = this.state;
        let newObj = accAssItems;
        newObj[index].select = !accAssItems[index].select;
        this.setState({
            accAssItems: [...newObj]
        })
        //('selectJournalTable:', record, accAssItems);
    }
    renderNoRefer = (item) => {
        // 辅助用classid判断
        // "BS000010000100001001"; // 字符串
        // "BS000010000100001004"; // 整数
        // "BS000010000100001031"; // 数值
        // "BS000010000100001032"; // 布尔
        // "BS000010000100001033"; // 日期
        // "BS000010000100001034"; // 日期时间
        //('renderNoRefer>>', item)
        if(item.classid === 'BS000010000100001001'){// 字符串
            return <Input
                value={this.state.stringRefValue}
                onChange={(value) => this.dateRefChange('stringRefValue', value, item)}
            />;
        }else if(item.classid === 'BS000010000100001004'){// 整数
            return <Input
                value={this.state.numberRefValueInteger}
                onChange={(value) => this.numberRefChange('numberRefValueInteger', value, item, 'journalInteger') }
            />;
        }else if(item.classid === 'BS000010000100001031'){// 数值
            return <Input
                value={this.state.numberRefValue}
                onChange={(value) => this.numberRefChange('numberRefValue', value, item, 'journalNumber') }
            />;
        }else if(item.classid === 'BS000010000100001032'){// 布尔
            return <Select
                value={this.state.booleanValue}
                className='search-boolean'
                onChange={(value)=>{
                    //('NCSelect>>>', value);
                    this.dateRefChange('booleanValue', value, item)
                }}
            >
                <NCOption value="Y">{this.state.json['20023025-000017']}</NCOption>{/* 国际化处理： 是*/}
                <NCOption value="N">{this.state.json['20023025-000018']}</NCOption>{/* 国际化处理： 否*/}
                <NCOption value="">{this.state.json['20023025-000019']}</NCOption>{/* 国际化处理： 空*/}
            </Select>;
        }else if(item.classid === 'BS000010000100001033'){//日期
            return (
                <DatePicker
                    format={format}
                    value={this.state.assistDateValue}
                    placeholder={this.state.dateInputPlaceholder}
                    onChange={(value) => this.dateRefChange('assistDateValue', value, item)}
                />
            );
        }else if(item.classid === 'BS000010000100001034'){// 日期时间
            return (
                <DatePicker
                    showTime={true}
                    format={Timeformat}
                    value={this.state.assistDateTimeValue}
                    placeholder={this.state.dateInputPlaceholder}
                    onChange={(value) => this.dateRefChange('assistDateTimeValue', value, item)}
                />
            );
        }

    }
    renderJournalRefer(record, fieldid){//渲染时序表的表格参照内容
        //('record>>>', record)
        if(record.refnodename){
            return this.rednerPureRefer(record, fieldid);
        }else if(record.classid === 'BS000010000100001001'){//"字符"
            return <Input
                fieldid={fieldid}
                value={this.state.stringRefValue}
                onChange={(value) => this.dateRefChange('stringRefValue', value, record)}
            />;
        }else if(record.classid === 'BS000010000100001034'){//日期
            return (
                <DatePicker
                    fieldid={fieldid}
                    format={format}
                    value={this.state.assistDateValue}
                    placeholder={this.state.dateInputPlaceholder}
                    onChange={(value) => this.dateRefChange('assistDateValue', value, record)}
                />
            );
        }else if(record.classid === 'BS000010000100001031'){//数值(小数)
            return <Input
                fieldid={fieldid}
                value={this.state.numberRefValue}
                onChange={(value) => this.numberRefChange('numberRefValue', value, record, 'journalNumber') }
            />;
        }else if(record.classid === 'BS000010000100001004'){//整数
            return <Input
                fieldid={fieldid}
                value={this.state.numberRefValueInteger}
                onChange={(value) => this.numberRefChange('numberRefValueInteger', value, record, 'journalInteger') }
            />;
        }else if(record.classid === 'BS000010000100001032'){//布尔
            return <Select
                fieldid={fieldid}
                value={this.state.booleanValue}
                className='search-boolean'
                onChange={(value)=>{
                    //('NCSelect>>>', value);
                    this.dateRefChange('booleanValue', value, record)
                }}
            >
                <NCOption value="Y">{this.state.json['20023025-000017']}</NCOption>{/* 国际化处理： 是*/}
                <NCOption value="N">{this.state.json['20023025-000018']}</NCOption>{/* 国际化处理： 否*/}
                <NCOption value="">{this.state.json['20023025-000019']}</NCOption>{/* 国际化处理： 空*/}
            </Select>;
        }else if(record.classid === 'BS000010000100001034'){//日期+时分秒时间
            return (
                <DatePicker
                    fieldid={fieldid}
                    showTime={true}
                    format={Timeformat}
                    value={this.state.assistDateTimeValue}
                    placeholder={this.state.dateInputPlaceholder}
                    onChange={(value) => this.dateRefChange('assistDateTimeValue', value, record)}
                />
            );
        }
    }
    rednerPureRefer(record, fieldid){
        let param = record.classid
        //('renderReferEle>',record, param, this.state[param], this.state.param);
        let referUrl = record.refnodename + '.js' ;//
        if(!this.state[param]){//undefined
            {createScript.call(this,referUrl,param)}
        }else{
            //('myattrcodeFn', this.state,this.state.buSecond, this.state.accountingbook,this.state[param], this.state.accountingbook[0] && this.state.accountingbook[0].refname);
            //buSecond
            let pkOrgParam = Object.keys(this.state.buSecond).length>0 ? this.state.buSecond.refpk : this.state.unitValueParam;
            let options = {
                fieldid: fieldid,
                value:this.state.accAssItems[record.key] && this.state.accAssItems[record.key].selectRefer,
                isMultiSelectedEnabled:false,
                "isShowDisabledData": true,
                queryCondition:() => {
                    return {
                        "pk_accountingbook": this.state.accountingbook && this.state.accountingbook.refpk,
                        "pk_org":pkOrgParam,
                        "isDataPowerEnable": "Y",
                        "DataPowerOperationCode" : 'fi',
                        "dateStr": this.state.isversiondate ? this.state.versiondate : this.state.busiDate
                    }
                },
                onChange: (v)=>{
                    let self = this;
                    //('render:param',param, v, record);
                    let {accAssItems} = this.state;
                    let newDate = accAssItems;
                    newDate[record.key].selectRefer = v;
                    this.setState({
                        accAssItems: [...newDate]
                    })
                }

            };
            let newOptions = {};
            if(record.refnodename === "uapbd/refer/sminfo/BankaccSubGridTreeRef/index"){
                newOptions = {
                    ...options
                }
            }else if(record.classid=='b26fa3cb-4087-4027-a3b6-c83ab2a086a9'||record.classid=='40d39c26-a2b6-4f16-a018-45664cac1a1f'){
                newOptions = {
                    ...options,
                    isShowUnit:true,
                    "isShowDisabledData": true,
                    "unitValueIsNeeded":false,
                    "isShowDimission":true,
                    queryCondition:() => {
                        return {
                            "pk_accountingbook": this.state.accountingbook && this.state.accountingbook.refpk,
                            "pk_org": pkOrgParam,
                            "isDataPowerEnable": "Y",
                            "DataPowerOperationCode" : 'fi',
                            dateStr: this.state.isversiondate ? this.state.versiondate : this.state.busiDate,
                            "busifuncode":"all",
                            isShowDimission:true
                        }
                    },
                    unitProps:{
                        refType: 'tree',
                        refName: this.state.json['20023025-000009'],/* 国际化处理： 业务单元*/
                        refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                        rootNode:{refname:this.state.json['20023025-000009'],refpk:'root'},/* 国际化处理： 业务单元*/
                        placeholder:this.state.json['20023025-000009'],/* 国际化处理： 业务单元*/
                        queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                        treeConfig:{name:[this.state.json['20023025-000010'], this.state.json['20023025-000011']],code: ['refcode', 'refname']},/* 国际化处理： 编码,名称*/
                        isMultiSelectedEnabled: false,
                        //unitProps:unitConf,
                        isShowUnit:false
                    },
                    unitCondition:{
                        pk_financeorg: pkOrgParam,
                        'TreeRefActionExt':'nccloud.web.gl.ref.OrgRelationFilterRefSqlBuilder'
                    },
                }
            }else if(record.classid.length === 20){
                newOptions = {
                    ...options,
                    pk_defdoclist: record.classid
                }
            }else {
                newOptions = {
                    ...options,
                    "isShowDisabledData": true,
                }
            }

            return (
                <Row>
                    <Col xs={12} md={12}>
                        {
                            this.state[param]?(this.state[param])(newOptions):<div/>
                        }
                    </Col>
                </Row>
            )
        }
    }
    dateRefChange = (key, value, record) => {
        //('dateRefChange>',value, record);
        if(key === 'stringRefValue'){
            let length = record.inputlength;
            let exeReg = new RegExp("^.{0," + length + "}$");
            let result = exeReg.exec(value);
            if(result){
                record.selectObj = result[0]
                this.setState({
                    [key]: result[0]
                })
            }
        } else {
            record.selectObj = value
            this.setState({
                [key]: value
            })
        }
    }
    /**
     * param: 标记来之来之哪个节点
     * */
    numberRefChange = (key, value, record, param) => {
        //('numberRefChange>>', value, record);
        this.handleNumberInput(key, value, param, record);
        setTimeout(() => record.selectObj = this.state[key], 0)

    }
    dateRefChange = (key, value, record) => {
        //('dateRefChange>',value, record);
        if(key === 'stringRefValue'){
            let length = record.inputlength;
            let exeReg = new RegExp("^.{0," + length + "}$");
            let result = exeReg.exec(value);
            if(result){
                record.selectObj = result[0]
                this.setState({
                    [key]: result[0]
                })
            }
        } else {
            record.selectObj = value
            this.setState({
                [key]: value
            })
        }
    }

	componentWillReceiveProps(nextProps) {
		// this.getEndAccount(nextProps);
	}

	handleRadioChange(value) {
		this.setState({selectedQueryValue: value});
	}

	handleRadioCurrChange(value) {
		this.setState({selectedCurrValue: value});
	}

	getlvlver() {
		let self = this;
		let url = '/nccloud/gl/glpub/accountinfoquery.do';

		let data = {
			pk_accountingbook: this.state.accountingbook.refpk,
		};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		    	// 
		    	// 默认取科目版本列表最后一个
		        //(res.data);
		        const { data, error, success } = res;

		        if(success){
		        	
					self.setState({
						accountlvl: data.accountlvl,
						versiondateList: data.versiondate,
						versiondate: data.versiondate[data.versiondate.length - 1],
						pk_accperiodscheme: data.pk_accperiodscheme,
					})
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});


	}



	//记账范围变化
	handleRadioScopeChange(value) {
		this.setState({talliedscope: value});
	}
	

	handleBalanceoriChange(value) {
		this.setState({balanceori: value});
	}


	handleIsmainChange(value) {
		this.setState({
			ismain: value,
		})
	}

	onBegintimeChange(d) {
		this.setState({
			begintime:d
		})
	}

	onEndtimeChange(d) {
		this.setState({
			endtime:d
		})
	}
	
	//根据核算账簿获取相关信息
	getInfoByBook(type) {
		// //(this.state.accountingbook)


		if (!this.state.accountingbook || !this.state.accountingbook.refpk) {
			//如果是清空了核算账簿，应该把相关的东西都还原
			return;
		}

		this.getlvlver();
		
		let self = this;
		let url = '/nccloud/gl/voucher/queryAccountingBook.do';
		let data = {
			pk_accountingbook: this.state.accountingbook.refpk,	
			year: '',		
		};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		    	const { data, error, success } = res;
		    	//(res)
		    	// return
		        if(success){
		        	let isStartBUSecond = data.isStartBUSecond;
		        	let showSecond = false;
		        	let disMutibook = true;
		        	

		        

		        	if (isStartBUSecond) {
						showSecond = true;
					} else {
						showSecond = false;
					}
					
		        	self.setState({
		        		isStartBUSecond: data.isStartBUSecond,
		        		showSecond,
		        		disMutibook,
		        		buorg: data.defaultBUOrg.pk_org,
		        		prepareddate: data.versiondate,
		        	},)
		        } else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },

		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });		        
		    }
		});
	}


	getAss = (pk_accasoa) => {//选择科目触发的事件；
		//('getAss>>', pk_accasoa)
		let self = this;
		let url = '/nccloud/gl/accountrep/summarylistqueryassitem.do';
		let includeSub = 'true';// 包含下级科目的辅助
		let data =  {
            pk_accasoa: pk_accasoa,
			prepareddate: this.state.busiDate,
			includeSub
		}
		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		        //('success>>>',res.data);
		        const { data, error, success } = res;
		        if(success){
		        	if (data) {
    		        	let newList = [];
                        data.map((itemCell, i) => {
                            itemCell.key = i;
                            itemCell.value = {
                                display: '',
                                value: ''
                            }
                            itemCell.select = true
                        })
    		        	for (var i = 0; i < data.length; i++) {
    		        		newList.push({
    		        			newValues: {
    		        				refname: '',
    		        				refpk: '',
    		        			}
    		        		})
    		        	}
    					self.setState({
    						accAssItems: deepClone(data),
    						newList,
    			
    					})
		        	} else {
		        		self.setState({
		        			accAssItems: [],
		        			newList: [],
		        	
		        		})
		        	}
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		    }
		});
	}

	accountReferChangeGetAss = () => {
        this.setState({
            startcode: {}
        })
		this.getAss('');

	}

    dateRefChange = (key, value, record) => {
        //('dateRefChange>',value, record);
        let length = record.inputlength;
        let exeReg = new RegExp("^.{0," + length + "}$");
        let result = exeReg.exec(value);
        if(key === 'stringRefValue'){
            if(result){
                record.selectRefer = result[0]
                this.setState({
                    [key]: result[0]
                })
            }
		} else {
            record.selectRefer = value
            this.setState({
                [key]: value
            })
        }
    }
    numberRefChange = (key, value, record, param) => {
        //('numberRefChange>>', value, record);
        this.handleNumberInput(key, value, param, record);
        setTimeout(() => record.selectRefer = this.state[key], 0)

    }

	loadRows = (data) => {
		let self=this;
		let myref;
		let {accAssItems} = this.state;
		let mydata = this.state.accAssItems
		//('mydata>/>>>>',mydata)

		let assLists = mydata.map(function (item, index) {
			//('item>>>', item, mydata[index])
			if (item.refnodename) {
				let referUrl = item.refnodename + '.js';
				let mycode = 'mycode' + index;
				if(!self.state[referUrl]){
					{createScript.call(self,referUrl,referUrl)}
				}else{
					let options = {
                        value: item.selectRefer,
                        isMultiSelectedEnabled:false,
                        "isShowDisabledData": true,
                        onChange: (v) => {
                            //('onchange:::', v)
                            item.selectRefer = v;

                            self.setState({
                                accAssItems: deepClone(mydata)
                            })

                        },
                        "unitValueIsNeeded":false,
                        queryCondition: () => {
                            return {
                                // "pk_accountingbook": self.state.pk_accountingbook,
                                dateStr: self.state.isversiondate ? self.state.versiondate : self.state.busiDate,
                                "pk_org": self.state.unitValueParam,
                                "isDataPowerEnable": "Y",
                                "DataPowerOperationCode" : 'fi',
                            }
                        }

                    };
					let newOptions = {};
                    if(item.classid=='b26fa3cb-4087-4027-a3b6-c83ab2a086a9'||item.classid=='40d39c26-a2b6-4f16-a018-45664cac1a1f'){
                        newOptions = {
							...options,
                            isShowUnit:true,
                            "isShowDisabledData": true,
                            unitProps:{
                                refType: 'tree',
                                refName: this.state.json['20023025-000009'],/* 国际化处理： 业务单元*/
                                refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                                rootNode:{refname:this.state.json['20023025-000009'],refpk:'root'},/* 国际化处理： 业务单元*/
                                placeholder:this.state.json['20023025-000009'],/* 国际化处理： 业务单元*/
                                queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                                treeConfig:{name:[this.state.json['20023025-000010'], this.state.json['20023025-000011']],code: ['refcode', 'refname']},/* 国际化处理： 编码,名称*/
                                isMultiSelectedEnabled: false,
                                //unitProps:unitConf,
                                isShowUnit:false
                            },
                            unitCondition:{
                                pk_financeorg: self.state.unitValueParam,
                                'TreeRefActionExt':'nccloud.web.gl.ref.OrgRelationFilterRefSqlBuilder'
                            },
						}
					}else if(item.classid.length === 20){
                        newOptions = {
                            ...options,
                            pk_defdoclist: item.classid
                        }
					}else{
                    	newOptions = {
							...options
						}
					}


					myref =  (
						<Row>
							<Col xs={12} md={12}>
							{self.state[referUrl]?(self.state[referUrl])(newOptions):<div/>}
							</Col>
						</Row>
					);
				}
                return (
                    <Row className="assList">
                        <Col md={2} sm={2}>
                            <div>{index + 1}</div>
                        </Col>
                        <Col md={2} sm={2}>
                            <div>{item.name}</div>
                        </Col>

                        <Col md={4} sm={4} style={{marginLeft:-31}}>
                            {myref}

                        </Col>
                    </Row>
                )
			}else{
                myref = self.renderNoRefer(item);
                return (
                    <Row className="assList">
                        <Col md={2} sm={2}>
                            <div>{index + 1}</div>
                        </Col>
                        <Col md={2} sm={2}>
                            <div>{item.name}</div>
                        </Col>

                        <Col md={4} sm={4} style={{marginLeft:-31}}>
                            {myref}

                        </Col>
                    </Row>
                )

			}


		})
		//(assLists)

		return (
			<div className="ass-outer">
				<Row>
					<Col md={2} sm={2}>
						<span>{this.state.json['20023025-000020']}</span>{/* 国际化处理： 辅助核算*/}
					</Col>
					<Col md={10} sm={10}><div className="line"></div></Col>
				</Row>
				<Row>
					<Col md={2} sm={2}>
						<div>{this.state.json['20023025-000021']}</div>{/* 国际化处理： 序号*/}
					</Col>
					<Col md={2} sm={2}>
						 <div>{this.state.json['20023025-000003']}</div>{/* 国际化处理： 辅助内容名称*/}
					</Col>
					<Col md={4} sm={4}>
						 <div>{this.state.json['20023025-000005']}</div>{/* 国际化处理： 辅助核算名称*/}
					</Col>
				</Row>
                {assLists}
			</div>
		);
	};


	
	//所有末级复选框变化
	onChangeIsleave(e) {
	    //(e);
	    let isleave = deepClone(this.state.isleave);
	   
	    this.setState({isleave: e});
	}

	//表外科目复选框变化
	onChangeIsoutacc(e) {  
	    this.setState({isoutacc: e});
	}

	//启用科目版本复选框变化
	onChangeIsversiondate(e) {  
	    this.setState({isversiondate: e});
	}


	//未记账凭证复选框变化
	onChangeIncludeuntally(e) {  
	    this.setState({includeuntally: e,
	    	includeerror: e,
	    });
	}

	//错误凭证复选框变化
	onChangeIncludeerror(e) {
		this.setState({includeerror: e});
	}

	//损益结转凭证复选框变化
	onChangeincludetranfer(e) {
		this.setState({includetranfer: e});
	}

	//损益结转凭证复选框变化
	onChangeincludereclassify(e) {
		this.setState({includereclassify: e});
	}

	//多主体显示单选框变化
	handleMutibookChange(e) {
		this.setState({mutibook: e});
	}

	//无发生不显示复选框变化
	onChangeShowzerooccur(e) {
		this.setState({showzerooccur: e});
	}

	//无余额无发生不显示复选框变化
	onChangeShowzerobalanceoccur(e) {
		this.setState({showzerobalanceoccur: e});
	}

	//无发生不显示复选框变化
	onChangeSumbysubjtype(e) {
		this.setState({sumbysubjtype: e});
	}


	//借贷方显示余额复选框变化
	onChangeTwowaybalance(e) {
		this.setState({twowaybalance: e});
	}

	//合并业务单元复选框变化
	onChangeMultbusi(e) {
		let disCurrplusacc = true;
		if (e === true) {
			disCurrplusacc = false;
		} else {
			
		}

		this.setState({
			multbusi: e,
			disCurrplusacc,

		});
	}
	
	
	onInputChange(key, e) {
		//(key)
		//(e)

		switch(key) {
			case 'startRest':
				this.setState({
					startRest: e
				});
				break;
			case 'endRest':
				this.setState({
					endRest: e
				});
				break;
			case 'endSummary':
				this.setState({
					endSummary: e
				});
				break;
		}
	}

	onStartSummaryChange(value) {
		this.setState({
			startSummary: value
		});
	}

	changeCheck=()=> {
		// this.setState({checked:!this.state.checked});
	}

	
	//起始时间变化
	onStartChange(d) {
		//(d)
		this.setState({
			begindate:d
		})
	}
	
	//结束时间变化
	onEndChange(d) {
		this.setState({
			enddate:d
		})
	}

	getTime(period) {
		//(period)
		let self = this;
		let url = '/nccloud/gl/glpub/queryDateByPeriod.do';

		let data = {
			pk_accountingbook: this.state.accountingbook.refpk,
			period:period,
		};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		    	// 
		    	// 默认取科目版本列表最后一个
		        //(res.data);
		        const { data, error, success } = res;

		        if(success){

		        	
					self.setState({
						endtime: data.enddate,
					})
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});

	}

	handleGTypeChange =() =>{
		
	};
    // handleValueChange = (key, value, param) => {
    //     //('handleValueChange>>', key, value);
    //     this.setState({
    //         [key]: value
    //     });
    //     if(param === 'SubjectVersion' || param === 'accbalance') {
    //         this.setState({
    //             changeParam: true,
    //             rightSelectItem: []
    //         })
    //     }
    // }
    handleSelect = (value) => {//
        //('handleSelect', value);

        this.setState({
            rightSelectItem: [...value]
        })
    }

	render() {
        this.columns = [
            {
                title: (<div fieldid='select'>{this.state.json['20023025-000001']}</div>),/* 国际化处理： 选择*/
                dataIndex: "select",
                key: "select",
                render: (text, record, index) => {
                    //('secondColumn>', text, record)
                    return <div fieldid='select'><Checkbox
                        checked={record.select}
                        onChange={() => {
                            //(this.state.json['20023025-000002'],record);/* 国际化处理： 序时账的tableChange:*/
                            record.index = index;
                            this.selectJournalTable(record, index);
                        }}
                    /></div>
                }
            },
            {
                title: (<div fieldid='searchObj'>{this.state.json['20023025-000003']}</div>),/* 国际化处理： 辅助内容名称*/
                dataIndex: "searchObj",
                key: "searchObj",
                render: (text, record, index) => {
                    //(this.state.json['20023025-000004'], text, record);/* 国际化处理： 序时账的table:*/
                    return (
                        <div fieldid='searchObj'>{record.name ? record.name : <span>&nbsp;</span>}</div>
                    )
                }
            },
            {
                title: (<div fieldid='searchRange'>{this.state.json['20023025-000005']}</div>),/* 国际化处理： 辅助核算名称*/
                dataIndex: "searchRange",
                key: "searchRange",
                render: (text, record,index) => {
                    //(this.state.json['20023025-000006'],record, record.type);/* 国际化处理： 核算内容:::*/
                    let renderEle = this.renderJournalRefer(record, 'searchRange');
                    return renderEle;
                }
            }
        ];
		let self = this;
		//('renderrr::', this.state)
		let { show, title,isButtonShow}= this.props;
		return <Modal
            fieldid='query'
			show={ show } 
			onHide={()=>this.props.onCancel(true)}
            className='summaryall'
		    >
			<Modal.Header closeButton>
				<Modal.Title>
					{title}
				</Modal.Title>
			</Modal.Header >
			<Modal.Body id="modalOuter">
                <div className='right_query noserchmatter'>
                    <div className='query_body1'>
                        <div className='query_form summaryall'>
                            <Row className="myrow">
                                <Col md={2} sm={2}>
                                    <span style={{color: 'red'}}>*</span>
                                    <span className='nc-theme-form-label-c'>{this.state.json['20023025-000022']}：</span>{/* 国际化处理： 核算账簿*/}
                                </Col>
                                <Col md={10} sm={10}>
                                    <div className="book-ref">
                                        {
                                            createReferFn(
                                                this,
                                                {
                                                    url: 'uapbd/refer/org/AccountBookTreeRef/index.js',
                                                    value: this.state.accountingbook,
                                                    referStateKey: 'checkAccountBook',
                                                    referStateValue: this.state.checkAccountBook,
                                                    stateValueKey: 'accountingbook',
                                                    flag: false,
                                                    queryCondition: {
                                                        pk_accountingbook: this.state.accountingbook && this.state.accountingbook.refpk
                                                    }
                                                },
                                                {
                                                    businessUnit: businessUnit,
                                                    renderTableFirstData: this.accountReferChangeGetAss,
                                                },
                                                'summaryall'
                                            )
                                        }

                                    </div>

                                </Col>
                            </Row>
                            {
                                this.state.isShowUnit ?
                                    <Row className="myrow">
                                        <Col md={2} sm={2}>
                                            <span style={{color: 'red'}}>*</span>
                                            <span className='nc-theme-form-label-c'>{this.state.json['20023025-000009']}：</span>{/* 国际化处理： 业务单元*/}
                                        </Col>
                                        <Col md={10} sm={10}>
                                            <div className="book-ref">
                                                <BusinessUnitTreeRef
                                                    fieldid='buSecond'
                                                    value={this.state.buSecond}
                                                    // isMultiSelectedEnabled= {true}
                                                    isShowDisabledData = {true}
                                                    isHasDisabledData = {true}
                                                    queryCondition = {{
                                                        "pk_accountingbook": this.state.accountingbook && this.state.accountingbook.refpk,
                                                        "isDataPowerEnable": 'Y',
                                                        "DataPowerOperationCode" : 'fi',
                                                        "TreeRefActionExt":'nccloud.web.gl.ref.GLBUVersionWithBookRefSqlBuilder'
                                                    }}
                                                    onChange={(v) => {
                                                        //(v);
                                                        this.setState({
                                                            buSecond: v,
                                                        }, () => {
                                                            if (this.state.accountingbook && this.state.accountingbook.refpk) {
                                                            }
                                                        })
                                                    }
                                                    }
                                                />

                                            </div>

                                        </Col>

                                    </Row> : ''
                            }

                            {/*科目*/}
                            <Row className="myrow">
                                <Col md={2} sm={2}>
                                    <span style={{color: 'red'}}>*</span>
                                    <span className='nc-theme-form-label-c'>{this.state.json['20023025-000016']}：</span>{/* 国际化处理： 科目*/}
                                </Col>
                                <Col md={10} sm={10}>
                                    <div className="book-ref">
                                        {
                                            this.renderRefer(
                                                subjectRefer,
                                                this.state.startcode,
                                                'subjectReferStateKey',
                                                this.state['subjectReferStateKey'],
                                                'startcode',
                                                false,
                                                {
                                                    "pk_accountingbook": this.state.accountingbook && this.state.accountingbook.refpk,
                                                    "isDataPowerEnable": 'Y',
                                                    "DataPowerOperationCode" : 'fi',
                                                    "includeSub": "Y"
                                                },
                                                (stateValueKey, v, fileParam, pk_accasoa, prepareddate) => {
                                                    this.handleValueChange(stateValueKey, v, fileParam);
                                                    this.getAss(pk_accasoa, prepareddate)
                                                },
                                                'summaryall'
                                            )
                                        }
                                    </div>
                                </Col>

                            </Row>
                            <div className="modalTable">
                                <Table
                                    columns={this.columns}
                                    data={this.state.accAssItems}
                                />
                            </div>
                            {/*会计期间*/}
                            <DateGroup
                                selectionState = {this.state.selectionState}
                                start = {this.state.start}
                                end = {this.state.end}
                                enddate = {this.state.enddate}
                                pk_accperiodscheme = {this.state.pk_accperiodscheme}
                                pk_accountingbook = {this.state.accountingbook}
                                rangeDate = {this.state.rangeDate}
                                handleValueChange = {this.handleValueChange}
                                handleDateChange = {this.handleDateChange}
                                self = {this}
                                showRadio = {this.state.showRadio}
                            />

                            {/*余额范围：*/}
                            <Row className="myrow">
                                <Col md={2} sm={2}>
                                    <span className='nc-theme-form-label-c'>{this.state.json['20023025-000023']}：</span>{/* 国际化处理： 余额范围*/}
                                </Col>


                                <Col md={4} sm={4}>
                                    <NCNumber
                                        scale={4}
                                        fieldid='startNumber'
                                        // placeholder="请输入数量"
                                        value={this.state.startRest}
                                        onChange={this.onInputChange.bind(this, 'startRest')}
                                    />

                                </Col>

                                <Col md={1} sm={1}>
                                    <span className='nc-theme-form-label-c'>{this.state.json['20023025-000024']}</span>{/* 国际化处理： 至*/}
                                </Col>

                                <Col md={4} sm={4}>
                                    <NCNumber
                                        scale={4}
                                        fieldid='endNumber'
                                        // placeholder="请输入数量"
                                        value={this.state.endRest}
                                        onChange={this.onInputChange.bind(this, 'endRest')}
                                    />
                                </Col>

                            </Row>

                            {/*摘要：*/}
                            <Row className="myrow">
                                <Col md={2} sm={2}>
                                    <span className='nc-theme-form-label-c'>{this.state.json['20023025-000025']}：</span>{/* 国际化处理： 摘要*/}
                                </Col>


                                <Col md={4} sm={4}>

                                    <FormControl
                                        fieldid='startSummary'
                                        value={this.state.startSummary}
                                        ref="test"
                                        onChange={this.onStartSummaryChange.bind(this)}
                                    />

                                </Col>

                                <Col md={1} sm={1}>
                                    <span className='nc-theme-form-label-c'>{this.state.json['20023025-000026']}</span>{/* 国际化处理： 前*/}
                                </Col>

                                <Col md={4} sm={4}>
                                    <NCNumber
                                        scale={0}
                                        fieldid='endSummary'
                                        // placeholder="请输入数量"
                                        value={this.state.endSummary}
                                        onChange={this.onInputChange.bind(this, 'endSummary')}
                                    />
                                </Col>

                                <Col md={1} sm={1}>
                                    <span className='nc-theme-form-label-c'>{this.state.json['20023025-000027']}</span>{/* 国际化处理： 位*/}
                                </Col>

                            </Row>

                            {/*凭证范围*/}
                            <CheckBoxCells
                                title= {this.state.json['20023025-000012']} ///* 国际化处理： 凭证范围*/
                                paramObj = {{
                                    "noChargeVoucher": {//未记账凭证
                                        title: this.state.json['20023025-000013'],/* 国际化处理： 包含未记账凭证*/
                                        show: true,
                                        disabled: this.state.disabled,
                                        checked: this.state.includeuntally,
                                        onChange: (value) => this.handleValueChange('includeuntally', value, 'assistAnalyzSearch')
                                    },
                                    "errorVoucher": {//错误凭证
                                        show: true,
                                        checked: this.state.includeerror,
                                        onChange: (value) => this.handleValueChange('includeerror', value, 'assistAnalyzSearch')
                                    },
                                    "harmToBenefitVoucher": {//损益结转凭证
                                        show: false,
                                        checked: this.state.includeplcf,
                                        onChange: (value) => this.handleValueChange('includeplcf', value, 'assistAnalyzSearch')
                                    },
                                    "againClassifyVoucher": {//重分类凭证
                                        show: false,
                                        checked: this.state.includerc,
                                        onChange: (value) => this.handleValueChange('includerc', value, 'assistAnalyzSearch')
                                    },
                                }}
                            />

                            {/*返回币种*/}
                            {
                                returnMoneyType(
                                    this,
                                    {
                                        key: 'selectedCurrValue',
                                        value: this.state.selectedCurrValue,
                                        groupEdit: this.state.groupCurrency,
                                        gloableEdit: this.state.globalCurrency
                                    },
                                    this.state.json
                                )
                            }
						</div>
					</div>
				</div>

			</Modal.Body>
			{isButtonShow &&
				<Modal.Footer>
                    <Button 
                        fieldid='query'
						className= "button-primary"
						onClick = {() => {
							let arrRefer = [];
							this.state.accAssItems.map((item, indx) => {
								//('accAssItems>>>', item);
								if(item.select){
                                    let itemCellArr = {};
                                    itemCellArr.typecode = item.code;
                                    itemCellArr.typename = item.name;
                                    if(item.selectRefer){
                                        itemCellArr.valuecode  = (typeof item.selectRefer) === 'string' ? item.selectRefer : item.selectRefer.refcode;
                                        itemCellArr.valuename  = (typeof item.selectRefer) === 'string' ? item.selectRefer : item.selectRefer.refname;
                                        itemCellArr.pk_checkvalue = (typeof item.selectRefer) === 'string' ? item.selectRefer : item.selectRefer.refpk;
                                    }
                                    itemCellArr.pk_checktype = item.pk_accassitem;
                                    arrRefer.push(itemCellArr)

                                }
							})
                            let data = {
                                pk_accountingbook: this.state.accountingbook.refpk,
                                pk_unit: this.state.buSecond.refpk ? [this.state.buSecond.refpk] : [],
                                acccode: this.state.startcode.code ? [this.state.startcode.code] : [],

                                startyear: this.state.startyear,
                                endyear: this.state.endyear,
                                startperiod: this.state.startperiod,
                                endperiod:   this.state.endperiod,

                                // querybyperiod: true,

                                //是否包含未记账、错误
                                includeuntally: this.state.includeuntally ? 'Y' : 'N',
                                includeerror: this.state.includeerror ? 'Y' : 'N',

                                returncurr: this.state.selectedCurrValue, //返回币种  1,2,3 组织,集团,全局

                                startbalance: this.state.startRest,
                                endbalance: this.state.endRest,
                                explanation: this.state.startSummary,
                                bitcounter: this.state.endSummary,
                                pk_accasoa: this.state.rightSelectItem,//'条件'
                                m_assvos: arrRefer
                            }
                            let url = '/nccloud/gl/accountrep/checkparam.do'
                            let flagShowOrHide;
                            ajax({
                                url,
                                data,
                                async:false,
                                success:function(response){
                                    flagShowOrHide = true
                                },
                                error:function(error){
                                    flagShowOrHide = false
                                    toast({content: error.message, color: 'warning'});
                                }
                            })
                            if(flagShowOrHide == true){
                                this.props.onConfirm(data)
                            }else if(flagShowOrHide == false){
                                return true
                            }

							}
						}
					>
						{this.state.json['20023025-000007']}
					</Button>

					<Button
                        fieldid='cancel'
						onClick={() => {this.props.onCancel(false)}}
					>{this.state.json['20023025-000008']}</Button>
				</Modal.Footer>
			}
			<Loading fullScreen showBackDrop={true} show={this.state.isLoading} />
		</Modal>;
	}
}

CentralConstructorModal = createPage({})(CentralConstructorModal)
export default CentralConstructorModal;
