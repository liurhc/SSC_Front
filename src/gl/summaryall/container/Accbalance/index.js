/**
 * 摘要汇总表
 * */


import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';

// import OpenTable from './OpenTable';

import {high,base,ajax,deepClone,createPage, getMultiLang,createPageIcon } from 'nc-lightapp-front';
const { NCDiv } = base;

import './index.less';
import { toast } from '../../../public/components/utils';
import MainSelectModal from './MainSelectModal';
import { SimpleTable } from 'nc-report';
const { PrintOutput } = high;
import RepPrintModal from '../../../manageReport/common/printModal'
import { mouldOutput } from '../../../public/components/printModal/events'
import { printRequire } from '../../../manageReport/common/printModal/events'
import HeadCom from '../../../manageReport/common/headSearch/headCom';

import {tableDefaultData}  from '../../../manageReport/defaultTableData';
import reportPrint from '../../../public/components/reportPrint.js';
import reportSaveWidths from '../../../public/common/reportSaveWidths.js';

import {setData} from "../../../manageReport/common/simbleTableData";
import {searchById} from "../../../manageReport/common/modules/createBtn";
import HeaderArea from '../../../public/components/HeaderArea';
import Iframe from '../../../public/components/Iframe';
class Accbalance extends Component {
	constructor(props) {
		super(props);

		//每定义一个state 都要添加备注
		this.state = {
			json: {},
            accountType: 'quantityamountcolumn',//金额式
			flag:0,//切换表头用的
      		showTable: false,
      		textAlginArr:[],//对其方式
			disabled: true,
            visible: false,
			appcode: this.props.getUrlParam('c') || this.props.getSearchParam('c'),
			ctemplate: '',
			nodekey: '',
			printParams:{}, //查询框参数，打印使用
			queryDatas:{},
			data: {},
			headtitle: [['', ''], ['', ''],['', ''],['', ''],],
			MainSelectModalShow: false, //查询模态框
			tableAll: {
				column: [],
				data:[],
			},              //表格全体数据
			initTableAll: null,  //初始查询出的表格全体数据
			tableStyle: {}, //表格样式
			queryCond: null, //已选择的查询条件
			pk_cashflow: '', //已点击行的pk_cashflow
			datas: {
				time: '',
				bookname: '',
				currtypeName: '',
			},
			dataout: tableDefaultData,
            dataWithPage: {},
            'summaryall': []
		}
		this.searchById = searchById.bind(this);
	}

    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json,
                'summaryall': [
                    {
                        title: json['20023025-000036'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20023025-000037'],
                        styleClass:"m-brief"
                    },

                    {
                        title: json['20023025-000038'],
                        styleClass:"m-brief"
                    }
                ]
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20023025', 'publiccommon'],domainName:'gl',currentLocale:'simpchn',callback});
    }

    componentDidMount() {
        this.setState({
            data: {}
        });
        let appceod = this.props.getSearchParam('c');
        this.searchById('20023025PAGE',appceod);
        this.props.button.setDisabled({
            print: true, saveformat: true
        });
    }
	//直接输出
	print = () => {
		let {resourceDatas,dataout,textAlginArr}=this.state;
		let dataArr=[];
		let emptyArr=[];
		let mergeInfo=dataout.data.mergeInfo;
		dataout.data.cells.map((item,index)=>{
			emptyArr=[];
			item.map((list,_index)=>{
				//emptyArr=[];
				if(list){
					// if(list.title){
					//     list.title='';
					// }
					emptyArr.push(list.title);
				}
			})
			dataArr.push(emptyArr);
		})
		//(dataArr);
		reportPrint(mergeInfo,dataArr,textAlginArr);
	}
	//保存列宽
	handleSaveColwidth=()=>{
	    let {json} = this.state;
		let info=this.refs.balanceTable.getReportInfo();
		let callBack = this.refs.balanceTable.resetWidths
		reportSaveWidths(this.state.appcode,info.colWidths, json, callBack);
	}
	//打印
	showPrintModal = () => {
        this.setState({
            visible: true
        })
    }
    handlePrint(data, isPreview) {
		let printUrl = '/nccloud/gl/accountrep/summarylistprint.do'
		let { printParams, appcode } = this.state
		let { ctemplate, nodekey } = data
		printParams.queryvo = data
		this.setState({
			ctemplate: ctemplate,
			nodekey: nodekey
		})
        printRequire(this.props, printUrl, appcode, nodekey, ctemplate, printParams, isPreview)
        this.handleCancel()
    }
    handleCancel() {
        this.setState({
            visible: false
        });
	}
	//模板输出
	showOutputModal = () => {
		// this.refs.printOutput.open()
		let outputUrl = '/nccloud/gl/accountrep/summarylistoutput.do'
		let { appcode, nodekey, ctemplate, printParams } = this.state
        mouldOutput(outputUrl, appcode, nodekey, ctemplate, printParams)
        // let outputData = mouldOutput(appcode, nodekey, ctemplate, printParams)
        // this.setState({
        //     outputData: outputData
        // })
    }
    handleOutput() {
        //(this.state.outputData,this.state.json['20023025-000032'])/* 国际化处理： -----模板输出----成功？----*/
	}

	queryShow = () => {
		this.setState({
			MainSelectModalShow: true,
		})
	}

	test(datas) {
		let self = this;

      let url = '/nccloud/gl/accountrep/summarylistquery.do'; //摘要汇总表接口

     let data = datas;
     // data = {"pk_accountingbook":"1001A3100000000000PE","pk_unit":"","multbusi":false,"usesubjversion":false,"versiondate":"2017-09-13","startcode":"1001","endcode":"1001","startlvl":"1","endlvl":"5","isleave":false,"isoutacc":false,"startyear":"2017","endyear":"2018","startperiod":"01","endperiod":"06","querybyperiod":true,"startvoucherno":"","endvoucherno":"","operator":"","talliedscope":"0","includeerror":true,"includetranfer":true,"includereclassify":false,"returncurrtype":"1","currtypeplussubj":true};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		        const { data, error, success } = res;
		        if(success){
		        	if (data) {
						let columnInfo=[],headtitle=[],balanceVO=[];
						if(data.data.length>0){
							self.setState({
								disabled:false
							})
							self.props.button.setDisabled({
								print: false, saveformat: false
							});
						}
						self.setState({
                            dataWithPage: data
						})
						if(data.column){
							columnInfo=data.column;
						}
						if(data.headtitle){
							headtitle=data.headtitle;
						}
						if(data.data){
							balanceVO=data.data;
						}
                        setData(self, data)
						// changeToHandsonTableData(self, data, columnInfo, headtitle, balanceVO);
					}
				}
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});
	}



	getDatas = (datas) => {
		//this.state.queryDatas=datas;
		this.test(datas);
		this.setState({
			queryDatas: datas,
			printParams: datas,
			MainSelectModalShow: false,
		})
	}
    handleLoginBtn = (obj, btnName) => {
        //('handleLoginBtn>>>', obj, btnName)
        if(btnName === 'search'){//1、查询
            this.queryShow()
        }else if(btnName === 'print'){//2、打印
            this.showPrintModal()
        }else if(btnName === 'templateOutput'){//3、模板输出
            this.showOutputModal()
        }else if(btnName === 'directprint'){//4、直接输出
            this.print()
        }else if(btnName === 'saveformat'){//5、保存列宽
            this.handleSaveColwidth()
		} else if (btnName === 'refresh') {// 刷新
			if (Object.keys(this.state.queryDatas).length > 0) {
				this.getDatas(this.state.queryDatas);
			}
		}
    }

	render() {
		let { modal } = this.props;
		const { createModal } = modal;
		return (
			<div id="cashQuery" className='manageReportContainer'>
				<HeaderArea 
                    title = {this.state.json['20023025-000034']} /* 国际化处理： 摘要汇总表*/
                    btnContent = {this.props.button.createButtonApp({
						area: 'btnarea',
						buttonLimit: 3,
						onButtonClick: (obj, tbnName) => this.handleLoginBtn(obj, tbnName),
					})}
                />
				<NCDiv areaCode={NCDiv.config.SEARCH}>
					<div className="searchContainer nc-theme-gray-area-bgc">
						{
							this.state.summaryall.map((items) => {
								return(
									<HeadCom
										content={this.state.dataWithPage.data}
										lastThre = {this.state.dataWithPage && this.state.dataWithPage.headtitle}
										labels={items.title}
										key={items.title}
										changeSelectStyle={
											(key, value) => this.handleValueChange(key, value, 'assistAnalyzSearch', () => setData(this, this.state.dataWithPage))
										}
										accountType = {this.state.accountType}
										isLong = {items.styleClass}
									/>
								)
							})
						}
					</div>
				</NCDiv>
                <div className='report-table-area'>
					<SimpleTable
						 ref="balanceTable"
						 data = {this.state.dataout}
					 />
				</div>
				<MainSelectModal
					show={this.state.MainSelectModalShow}
					
					ref='MainSelectModal'
					title= {this.state.json['20023025-000033']} ///* 国际化处理： 查询条件*/
					icon=""
					onConfirm={(datas) => {
						this.getDatas(datas);
					}}
					
					onCancel={() => {
						let MainSelectModalShow = false;
						this.setState({ MainSelectModalShow });
					}}
				/>
				<RepPrintModal
					noRadio={true}
					noCheckBox={true}
                    appcode={this.state.appcode}
                    visible={this.state.visible}
                    handlePrint={this.handlePrint.bind(this)}
                    handleCancel={this.handleCancel.bind(this)}
                />
				<PrintOutput					
					ref='printOutput'
					url='/nccloud/gl/accountrep/summarylistoutput.do'
					data={this.state.outputData}
                    callback={this.handleOutput.bind(this)}
				/>
				{createModal('printService', {
					className: 'print-service'
				})}
				<Iframe />
			</div>
			)			
		
	}
}

Accbalance = createPage({})(Accbalance)
export default Accbalance;
