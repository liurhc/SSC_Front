import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';
// import moment from 'moment';

import { base,ajax,deepClone, createPage, getMultiLang } from 'nc-lightapp-front';
import './index.less';

const { NCRadio:Radio, NCRow:Row,NCCol:Col, NCSelect:Select, NCCheckbox:Checkbox, NCDiv } = base;

const NCOption = Select.NCOption;
import BusinessUnitTreeRef from '../../../../../uapbd/refer/orgv/BusinessUnitVersionDefaultAllTreeRef';
import {
    businessUnit,
    createReferFn, delKeyOfObj,
    getReferDetault, renderLevelOptions,
    renderRefer, returnMoneyType
} from "../../../../manageReport/common/modules/modules";
import SubjectVersion from "../../../../manageReport/common/modules/subjectVersion";
import DateGroup from '../../../../manageReport/common/modules/dateGroup';
import SubjectLevel from '../../../../manageReport/common/modules/subjectAndLevel'
import {clearTransterData, handleChange, handleSelectChange} from "../../../../manageReport/common/modules/transferFn";
import CheckBoxCells from '../../../../manageReport/common/modules/checkBox';
import initTemplate from '../../../../manageReport/modalFiles/initTemplate';
import { FICheckbox } from '../../../../public/components/base';
class CentralConstructorModal extends Component {

	constructor(props) {
		super(props);
		this.state=this.getAllState()
        this.renderRefer = renderRefer.bind(this);
		this.searchId = '20023030query';
		this.index = '' //账簿打印行号
	}

    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json,
                gData: [{key: '', name: json['20023030-000018']}],/* 国际化处理： 会计科目*/
                currtype: json['20023030-000002'],/* 国际化处理： 本币*/
                currtypeName: json['20023030-000002'],  //选择的币种名称
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20023030', 'dategroup', 'checkbox', 'subjectandlevel', 'subjectversion', 'childmodules'],domainName:'gl',currentLocale:'simpchn',callback});
		if(!this.props.hideBtnArea){
			initTemplate.call(this,this.props, this.state.appcode)
		}
		this.getCurrtypeList();
    }

	componentDidMount() {
        setTimeout(() => getReferDetault(this, true, {
            businessUnit
		},'',this.props.defaultAccouontBook),0);
		// setTimeout(() => initTemplate.call(this,this.props, this.state.appcode), 0)
		this.props.MainSelectRef && this.props.MainSelectRef(this)
	}

	componentWillReceiveProps(nextProps) {
		// this.getEndAccount(nextProps);
	}
	//获取初始化state
	getInitState = () => {
		let initState = {
			defaultAccouontBook:[this.props.defaultAccouontBook],
			appcode: "20023030",
			// json: {},
            isqueryuntallyed: true, //是否禁止设置查询包含未记账凭证;true: 禁止
            groupCurrency: true,//是否禁止"集团本币"可选；true: 禁止
            globalCurrency: true, //是否禁止"全局本币"可选；true: 禁止
            selectedKeys: [],
            targetKeys: [],

            changeParam: true, //false:点击条件时不触发接口，,
            rightSelectItem: [], //科目的"条件"按钮中存放右侧数据的pk值

            showRadio: true,//控制是否显示"会计期间"的但选框；false: 不显示
            start: {},  //开始期间
            end: {}, //结束期间
            /*****会计期间******/
            selectionState: 'true', //按期间查询true，按日期查询false
            startyear: '', //开始年
            startperiod: '',  //开始期间
            endyear: '',  //结束年
            endperiod: '',   //结束期间
            /*****日期******/
            begindate: '', //开始时间
            enddate: '', //结束时间
            rangeDate: [],//时间显示
            versiondate: '', //制单日期，后续要用下拉选
            versionDateArr: [],
			accountingbook: this.props.defaultAccouontBook&&this.props.defaultAccouontBook.refpk?[this.props.defaultAccouontBook]:[],  //核算账簿
			accasoa: {           //科目
				refname:'',
				refpk: '',
			},
			buSecond: {        //二级单元
				refname:'',
				refpk: '',
			},
			pk_accperiodscheme: '', //会计期间参照用的过滤
			versiondateList: [], //科目版本列表
			accountlvl: '1', //设置级次1到几
			selectedQueryValue: '0', //查询方式单选
			selectedCurrValue: '1', //返回币种单选
			selectedSortValue: '0', //排序方式
			ismain: '0', // 0主表  1附表
			currtype: '',      //选择的币种
			currtypeName: '',  //选择的币种名称
			currtypeList: [],  //币种列表
			defaultCurrtype: {         //默认币种
				name: '',
				pk_currtype: '',
			},
			checkbox: {
				one: false,
				two: false,
				three: false,
			},
			balanceori: '-1', //余额方向
			startlvl: '1', //开始级次
			endlvl: '1',  //结束级次
			startcode: {
				refname:'',
				refpk: '',
				refcode: '',
			}, //开始科目编码
			endcode: {
				refname:'',
				refpk: '',
				refcode: '',
			},    //结束科目编码

			isleave: false, //是否所有末级
			isoutacc: false, //是否表外科目
			includeuntally: false, //包含凭证的4个复选框 未记账
			includeerror: false, //包含凭证的4个复选框 错误
			includeplcf: true, //包含凭证的4个复选框 损益结转
			includerc: false, //包含凭证的4个复选框 重分类
			combinedetail: false, // 合并同凭证分录
			showaccum: false,  //期间无发生显示累计
			showbusdate: false,  //显示业务日期
			partionbyperiod: false, //按期间分页
			mutibookAlone: false, //多核算账簿的选择
			disMutibook: true, //多主体显示是否禁用
			currplusacc: 'Y', //排序方式
			disCurrplusacc: true, //排序方式是否禁用
			showzerooccur: false, //无发生不显示
			showzerobalanceoccur: true, //无余额无发生不显示
			sumbysubjtype: false, //科目类型小计
			disSumbysubjtype: true, //科目类型小计是否禁用
			twowaybalance: false, //借贷方显示余额
			showSecond: false, //是否显示二级业务单元
			multbusi: false, //多业务单元复选框
			isversiondate: false,		
			transferDataSource: []	//穿梭框原数据	
		}
		return initState
	}
	//state中添加json
	getAllState = () => {
		let initState = this.getInitState()
		initState.json={}
		return initState
	}
	//初始化state
	setInitState = (querystate) => {
		if(JSON.stringify(querystate) !=='{}') {
			let disabled = this.props.status === 'browse' ? true : false
			this.setState(querystate)
			this.TransferTerms.setTransferState(querystate.transferDataSource, disabled)
		} else {
			let initState = this.getInitState()
			delKeyOfObj(initState, ['currtype', 'currtypeName', 'currtypeList'])
			this.setState(initState);
			getReferDetault(this, true, {
				businessUnit
			},'',this.props.defaultAccouontBook)
		}
	}
	/**
	 * 条件 弹框‘确定’ 
	 * transferDataSource: 穿梭框数据
	 */
	transferSure = (transferDataSource) => {
		this.setState({
			transferDataSource
		})
	}
	//打开高级查询面板
	openAdvSearch = (index, querystate) => {
		initTemplate.call(this,this.props, this.state.appcode).then((data)=>{
			this.props.search.openAdvSearch(this.searchId, true)
			this.index = index;
			this.setInitState(querystate);
		})
	}
	//获取返回父组件的值
	getConfirmData = (tempState) => {
		let pk_accountingbook = [];
		let pk_unit = [];

		if (Array.isArray(tempState.accountingbook)) {
			tempState.accountingbook.forEach(function (item) {
				pk_accountingbook.push(item.refpk)
			})
		}

		if (Array.isArray(tempState.buSecond)) {
			tempState.buSecond.forEach(function (item) {
				pk_unit.push(item.refpk)
			})
		}

		let confirmData = {
			pk_accountingbook: pk_accountingbook,
			pk_unit: pk_unit,
			multbusi: tempState.multbusi,
			usesubjversion: tempState.isversiondate ? 'Y' : 'N',// 是否启用科目版本
			versiondate: tempState.isversiondate ? tempState.versiondate : tempState.busiDate,//科目版本
			// pk_accasoa://条件框选择科目
			startcode: tempState.startcode.code,  //开始科目编码
			endcode: tempState.endcode.code,    //结束科目编码
			startlvl: tempState.startlvl.toString(),
			endlvl: tempState.endlvl.toString(),
			isleave: tempState.isleave ? 'Y' : 'N',
			isoutacc: tempState.isoutacc ? 'Y' : 'N',
			/**会计期间：*/
			querybyperiod: tempState.selectionState,//
			startyear: tempState.startyear,
			endyear: tempState.endyear,
			startperiod: tempState.startperiod,
			endperiod: tempState.endperiod,
			/**日期：*/
			startdate: tempState.begindate,
			enddate: tempState.enddate,
			//未记账、错误、损益结转、重分类  Y/N  注意：‘包含凭证’是个标题，后面的才是选项
			includeuntally: tempState.includeuntally ? 'Y' : 'N',
			includeerror: tempState.includeerror ? 'Y' : 'N',
			includeplcf: tempState.includeplcf ? 'Y' : 'N',
			includerc: tempState.includerc ? 'Y' : 'N',
			pk_currtype: tempState.currtype,
			returncurr: tempState.selectedCurrValue, //返回币种  1,2,3 组织,集团,全局
			mutibook: tempState.mutibookAlone ? 'Y' : 'N',  //写死的'N'？ 多核算账簿？
			showzerobalanceoccur: tempState.showzerobalanceoccur ? 'Y' : 'N',
			sorttype: tempState.selectedSortValue, //排序方式
			combinedetail: tempState.combinedetail ? 'Y' : 'N',
			showaccum: tempState.showaccum ? 'Y' : 'N',
			showbusdate: tempState.showbusdate ? 'Y' : 'N',
			partionbyperiod: tempState.partionbyperiod ? 'Y' : 'N',
			pk_accasoa: tempState.rightSelectItem,//'条件'
			appcode: tempState.appcode,
			index: this.index
		}
		return confirmData
	}

	handleRadioChange(value) {
		this.setState({selectedQueryValue: value});
	}

	handleRadioCurrChange(value) {
		this.setState({selectedCurrValue: value});
	}

	//排序方式变化
	handleRadioSortChange(value) {
		this.setState({selectedSortValue: value});
	}
	
	//排序方式单选变化
	handleCurrplusacc(value) {
		this.setState({currplusacc: value});
	}
	

	getlvlver() {
		let self = this;
		let url = '/nccloud/gl/glpub/accountinfoquery.do';

		let data = {
			pk_accountingbook: this.state.accountingbook[0].refpk,
		};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		    	// 默认取科目版本列表最后一个
		        //(res.data);
		        const { data, error, success } = res;

		        if(success){
		        	
					self.setState({
						accountlvl: data.accountlvl,
						versiondateList: data.versiondate,
						versiondate: data.versiondate[data.versiondate.length - 1],
						pk_accperiodscheme: data.pk_accperiodscheme,
					})
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});


	}






	getCurrtypeList = () => {
		let self = this;
		let url = '/nccloud/gl/voucher/queryAllCurrtype.do';
		// let data = '';
		let data = {
			"localType":"1",
			"showAllCurr":"1"
		};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		        const { data, error, success } = res;
		        if(success){
					self.setState({
						currtypeList: data,
					})
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});

	}

	handleBalanceoriChange(value) {
		this.setState({balanceori: value});
	}


	handleCurrtypeChange(value) {
		let self = this;
		// if (!this.state.accountingbook || !this.state.accountingbook.refpk) {
		// 	toast({ content: '请先选择核算账簿', color: 'warning' });
		// 	return;
		// }
		let currtypeList = this.state.currtypeList;
		currtypeList.forEach(function (item, index) {
			if (item.pk_currtype == value ) {
				self.setState({
					currtypeName: item.name,
				})
			}
		})

		this.setState({
			currtype: value,
		// }, this.getTables);
		}, this.getRates);
	}


	handleIsmainChange(value) {
		this.setState({
			ismain: value,
		})
	}
	
	//根据核算账簿获取相关信息
	getInfoByBook(type) {
		// //(this.state.accountingbook)


		if (!this.state.accountingbook || !this.state.accountingbook[0]) {
			//如果是清空了核算账簿，应该把相关的东西都还原
			return;
		}

		this.getlvlver();
		
		let self = this;
		let url = '/nccloud/gl/voucher/queryAccountingBook.do';
		let data = {
			pk_accountingbook: this.state.accountingbook[0].refpk,	
			year: '',		
		};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		    	const { data, error, success } = res;
		    	//(res)
		    	// return
		        if(success){
		        	let isStartBUSecond = data.isStartBUSecond;
		        	let showSecond = false;
		        	let disMutibook = true;
		        	

		        	//单选判断
		        	if (self.state.accountingbook.length == 1) {

			        	if (isStartBUSecond) {
							showSecond = true;
						} else {
							showSecond = false;
						}
					} else {
						showSecond = false;
						disMutibook = false;

					}
		        	self.setState({
		        		isStartBUSecond: data.isStartBUSecond,
		        		showSecond,
		        		disMutibook,
		        	},)
		        } else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },

		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });		        
		    }
		});
	}

	//开始级次变化
	handleStartlvlChange(value) {
		//(value)
		this.setState({
			startlvl: value,
		})
	}
	
	//结束级次变化
	handleEndlvlChange(value) {
		//(value)
		this.setState({
			endlvl: value,
		})
	}
	
	//科目版本下拉变化
	handleVersiondateChange(value) {
		this.setState({
			versiondate: value,
		})
	}
	
	//所有末级复选框变化
	onChangeIsleave(e) {
	    //(e);
	    let isleave = deepClone(this.state.isleave);
	   
	    this.setState({isleave: e});
	}

	//表外科目复选框变化
	onChangeIsoutacc(e) {  
	    this.setState({isoutacc: e});
	}

	//启用科目版本复选框变化
	onChangeIsversiondate(e) {  
	    this.setState({isversiondate: e});
	}


	//未记账凭证复选框变化
	onChangeIncludeuntally(e) {  
	    this.setState({
			includeuntally: e,
            includeerror: e
	    });
	}

	//错误凭证复选框变化
	onChangeIncludeerror(e) {
		this.setState({includeerror: e});
	}

	//损益结转凭证复选框变化
	onChangeIncludeplcf(e) {
		this.setState({includeplcf: e});
	}

	//损益结转凭证复选框变化
	onChangeIncluderc(e) {
		this.setState({includerc: e});
	}

	//合并同凭证分录复选框变化
	onChangeCombinedetail(e) {
		this.setState({combinedetail: e});
	}

	
	//期间无发生显示累计复选框变化
	onChangeShowaccum(e) {
		this.setState({showaccum: e});
	}

	

	//显示业务日期复选框变化
	onChangeShowbusdate(e) {
		this.setState({showbusdate: e});
	}

	//按期间分页复选框变化
	onChangePartionbyperiod(e) {
		this.setState({partionbyperiod: e});
	}

	//多核算账簿复选框变化
	handleMutibookChange(e) {
		this.setState({mutibookAlone: e});
	}

	//无发生不显示复选框变化
	onChangeShowzerooccur(e) {
		this.setState({showzerooccur: e});
	}

	//无余额无发生不显示复选框变化
	onChangeShowzerobalanceoccur(e) {
		this.setState({showzerobalanceoccur: e});
	}

	//无发生不显示复选框变化
	onChangeSumbysubjtype(e) {
		this.setState({sumbysubjtype: e});
	}


	//借贷方显示余额复选框变化
	onChangeTwowaybalance(e) {
		this.setState({twowaybalance: e});
	}

	//合并业务单元复选框变化
	onChangeMultbusi(e) {
		let disCurrplusacc = true;
		if (e === true) {
			disCurrplusacc = false;
		} else {
			
		}

		this.setState({
			multbusi: e,
			disCurrplusacc,

		});
	}

	changeCheck=()=> {
		// this.setState({checked:!this.state.checked});
	}

	
	//起始时间变化
	onStartChange(d) {
		//(d)
		this.setState({
			begindate:d
		})
	}
	
	//结束时间变化
	onEndChange(d) {
		this.setState({
			enddate:d
		})
	}

	handleGTypeChange =() =>{
		
	};
    handleValueChange = (key, value, param) => {
        //('key<>', key, value)
        this.setState({
            [key]: value
        });
        if(param === 'SubjectVersion' || param === 'accbalance') {
            this.setState({
                changeParam: true,
                rightSelectItem: []
            })
        }
    }
    handleDateChange = (value) => {//日期: 范围选择触发事件
        //('handleDateChange:', value);
        this.setState({
            begindate: value[0],
            enddate: value[1],
            rangeDate: value
        })
    }
    handleSelect = (value) => {//
        //('handleSelect》》》', value);

        this.setState({
            rightSelectItem: [...value]
        })
    }
    renderModalList = () => {
		let { status, hideBtnArea } = this.props
		let disabled = status === 'browse' ? true : false
        return (
            <div className='right_query'>
                <div className='query_body1'>
                    <div className='query_form threedetail'>
					<NCDiv fieldid="accountingbook" areaCode={NCDiv.config.Area}>
						<Row className="myrow">
							<Col md={2} sm={2}>
								<span style={{color: 'red'}}>*</span>
								<span className='nc-theme-form-label-c'>{this.state.json['20023030-000005']}：</span>{/* 国际化处理： 核算账簿*/}
							</Col>
							
							<Col md={6} sm={6}>
								<div className="book-ref">
									{
										createReferFn(
											this,
											{
												url: 'uapbd/refer/org/AccountBookTreeRef/index.js',
												value: this.state.accountingbook,
												referStateKey: 'checkAccountBook',
												referStateValue: this.state.checkAccountBook,
												stateValueKey: 'accountingbook',
												flag: true,
												disabled: disabled,
												queryCondition: {
													pk_accountingbook: this.state.accountingbook[0] && this.state.accountingbook[0].refpk
												}
											},
											{
												businessUnit: businessUnit
											},
											'threedetail'
										)
									}
								</div>
							</Col>
							<Col md={4} sm={4}>
								<Checkbox
									colors="dark"
									disabled={disabled ? disabled : this.state.disMutibook}
									checked={this.state.mutibookAlone}
									onChange={this.handleMutibookChange.bind(this)}
								>
									{this.state.json['20023030-000006']}  {/* 国际化处理： 多核算账簿合并*/}
								</Checkbox>
							</Col>
							
						</Row>
					</NCDiv>
                {
                    this.state.isShowUnit ?
                        <Row className="myrow">
                            <Col md={2} sm={2}>
                                <span className='nc-theme-form-label-c'>{this.state.json['20023030-000007']}：</span>{/* 国际化处理： 业务单元*/}
                            </Col>
                            <Col md={6} sm={6}>
                                <div className="book-ref">
                                    <BusinessUnitTreeRef
										fieldid='buSecond'
										value={this.state.buSecond}
										disabled={disabled}
                                        isMultiSelectedEnabled={true}
                                        isShowDisabledData = {true}
                                        isHasDisabledData = {true}
                                        queryCondition = {{
                                            "pk_accountingbook": this.state.accountingbook[0] && this.state.accountingbook[0].refpk,
                                            "isDataPowerEnable": 'Y',
                                            "DataPowerOperationCode" : 'fi',
                                            "TreeRefActionExt":'nccloud.web.gl.ref.GLBUVersionWithBookRefSqlBuilder'
                                        }}
                                        onChange={(v) => {
                                            //(v);
                                            this.setState({
                                                buSecond: v,
                                            }, () => {
                                                if (this.state.accountingbook && this.state.accountingbook[0]) {
                                                }
                                            })
                                        }
                                        }
                                    />

                                </div>

                            </Col>
                            <Col md={4} sm={4}>
                                <FICheckbox colors="dark" disabled={disabled} checked={this.state.multbusi}
                                          onChange={this.onChangeMultbusi.bind(this)}>
									{this.state.json['20023030-000008']}	{/* 国际化处理： 多业务单元合并*/}	
								</FICheckbox>
                            </Col>
                        </Row>: ''
                }

				{/*会计期间*/}
				{
					hideBtnArea ? '' 
					:
					<DateGroup
						selectionState = {this.state.selectionState}
						start = {this.state.start}
						end = {this.state.end}
						enddate = {this.state.enddate}
						pk_accperiodscheme = {this.state.pk_accperiodscheme}
						pk_accountingbook = {this.state.accountingbook[0]}
						rangeDate = {this.state.rangeDate}
						handleValueChange = {this.handleValueChange}
						handleDateChange = {this.handleDateChange}
						self = {this}
						showRadio = {this.state.showRadio}
					/>
				}
                

                {/*启用科目版本：*/}
                <SubjectVersion
                    checked = {this.state.isversiondate}//复选框是否选中
                    data={this.state.versionDateArr}//
					value={this.state.versiondate}
					CheckboxDisabled = {disabled}
                    disabled={disabled ? disabled : !this.state.isversiondate}
                    handleValueChange = {(key, value) => this.handleValueChange(key, value, 'SubjectVersion')}
                />

                {/*科目，级次：*/}
                <SubjectLevel
					parent = {this}
					disabled = {disabled}
                    accountingbook = {this.state.accountingbook}
                    isMultiSelectedEnabled = {false}//控制多选单选

                    selectedKeys = {this.state.selectedKeys}
                    targetKeys = {this.state.targetKeys}
                    changeParam = {this.state.changeParam}
                    handleSelect = {this.handleSelect}
                    handleValueChange = {this.handleValueChange}
					transferSure = {this.transferSure}
					TransferRef = {(init) => {this.TransferTerms = init}}
                    handleSelectChange = {(sourceSelectedKeys, targetSelectedKeys) => handleSelectChange(this, sourceSelectedKeys, targetSelectedKeys)}
                    handleChange = {(nextTargetKeys, direction, moveKeys) => handleChange(this,nextTargetKeys, direction, moveKeys) }
                    clearTransterData = {() => clearTransterData(this)}
                />

                {/*包含凭证：*/}
                <CheckBoxCells
                    paramObj = {{
						'disabled': disabled,
                        "noChargeVoucher": {//未记账凭证
                            show: true,
                            checked: this.state.includeuntally,
                            onChange: this.onChangeIncludeuntally.bind(this)
                        },
                        "errorVoucher": {//错误凭证
                            show: true,
                            checked: this.state.includeerror,
                            disabled: !this.state.includeuntally,
                            onChange: this.onChangeIncludeerror.bind(this)
                        },
                        "harmToBenefitVoucher": {//损益结转凭证
                            show: true,
                            checked: this.state.includeplcf,
                            onChange: this.onChangeIncludeplcf.bind(this)
                        },
                        "againClassifyVoucher": {//重分类凭证
                            show: true,
                            checked: this.state.includerc,
                            onChange: this.onChangeIncluderc.bind(this)
                        },
                    }}
                />

                {/*排序方式*/}
                <Row className="myrow">
                    <Col md={2} sm={2}>
                        <span className='nc-theme-form-label-c'>{this.state.json['20023030-000009']}：</span>{/* 国际化处理： 排序方式*/}
                    </Col>
                    <Col md={9} sm={9}>
                        <Radio.NCRadioGroup
                            selectedValue={this.state.selectedSortValue}
                            onChange={(value) => this.handleValueChange('selectedSortValue',value)}
                        >
                            <Radio value="0" id='madeDate' disabled={disabled}>
								{this.state.json['20023030-000010']} {/* 国际化处理： 制单日期*/}
							</Radio>
                            <Radio value="1"  id='voucherId' disabled={disabled}>
								{this.state.json['20023030-000011']} {/* 国际化处理： 凭证号*/}
							</Radio>
                        </Radio.NCRadioGroup>

                    </Col>
                </Row>

                {/*币种*/}
                <Row className="myrow">
                    <Col md={2} sm={2}>
                        <span className='nc-theme-form-label-c'>{this.state.json['20023030-000012']}：</span>{/* 国际化处理： 币种*/}
                    </Col>

                    {/*币种下拉*/}
                    <Col md={3} sm={3}>
                        <div className="book-ref">
                            <Select
								showClear={false}
								fieldid ='pk_currtype'
                                value={this.state.currtype}

                                style={{ width: 150}}
								disabled={disabled}

                                onChange={this.handleCurrtypeChange.bind(this)}

                            >
                                {this.state.currtypeList.map((item, index) => {
                                    return <NCOption value={item.pk_currtype} key={item.pk_currtype} >{item.name}</NCOption>
                                } )}

                            </Select>

                        </div>
                    </Col>
                </Row>

                {/*返回币种：*/}
                {
                    returnMoneyType(
                        this,
                        {
                            key: 'selectedCurrValue',
                            value: this.state.selectedCurrValue,
                            groupEdit: this.state.groupCurrency,
                            gloableEdit: this.state.globalCurrency
                        },
						this.state.json,
						disabled
                    )
                }

				{/* 显示属性*/}
                <Row className="">
					<Col md={2} sm={2}><span className='nc-theme-form-label-c'>{this.state.json['20023030-000013']}：</span></Col>{/* 国际化处理： 显示属性*/}
                    <Col md={10} sm={10}>
						<Col md={3} sm={3}>
							<FICheckbox colors="dark" disabled={disabled} checked={this.state.showzerobalanceoccur}  onChange={this.onChangeShowzerobalanceoccur.bind(this)}>
								{this.state.json['20023030-000014']}  {/* 国际化处理： 无余额无发生不显示*/}
							</FICheckbox>
						</Col>
						<Col md={3} sm={3}>
							<FICheckbox colors="dark" disabled={disabled} checked={this.state.combinedetail}   onChange={this.onChangeCombinedetail.bind(this)} >
								{this.state.json['20023030-000015']}  {/* 国际化处理： 合并同凭证分录*/}
							</FICheckbox>
						</Col>
						<Col md={3} sm={3}>
							<FICheckbox colors="dark" disabled={disabled} checked={this.state.showaccum}   onChange={this.onChangeShowaccum.bind(this)} >
								{this.state.json['20023030-000016']} {/* 国际化处理： 期间无发生显示累计*/}
							</FICheckbox>
						</Col>

						<Col md={3} sm={3}>
							<FICheckbox colors="dark" disabled={disabled} checked={this.state.showbusdate}  onChange={this.onChangeShowbusdate.bind(this)} >
								{this.state.json['20023030-000017']} {/* 国际化处理： 显示业务日期*/}
							</FICheckbox>
						</Col>
					</Col>
                </Row>
			</div>
			</div>
            </div>
		)
    }
    clickPlanEve = (value) => {
		let { status } = this.props;
		if (status !== 'browse') {
			this.state = deepClone(value.conditionobj4web.nonpublic);
			this.setState(this.state)
		}
    }
    saveSearchPlan = () => {//点击《保存方案》按钮触发事件
        //('saveSearchPlan>>', this.state)
        return {...this.state}
    }
    clickSearchBtn = () => {//点击查询框的"查询"按钮事件
		
    }

	render() {
		//('render:::', this.state);
        let { search, hideBtnArea, status } = this.props;
		let { NCCreateSearch } = search;
		let isHideBtnArea = typeof(hideBtnArea) === 'boolean' && hideBtnArea ? hideBtnArea : false
        return <div>
            {
                NCCreateSearch(this.searchId, {
					onlyShowSuperBtn:true,                       // 只显示高级按钮
                    replaceSuperBtn:this.state.json['20023030-000004'],                      // 自定义替换高级按钮名称/* 国际化处理： 查询*/
                    replaceRightBody:this.renderModalList,
                    hideSearchCondition:true,//隐藏《候选条件》只剩下《查询方案》
                    clickPlanEve:this.clickPlanEve ,            // 点击高级面板中的查询方案事件 ,用于业务组为自定义查询区赋值
                    saveSearchPlan:this.saveSearchPlan ,        // 保存查询方案确定按钮事件，用于业务组返回自定义的查询条件
					oid:this.props.meta.oid,
					hideBtnArea: isHideBtnArea,
					isSynInitAdvSearch: true,//渲染右边面板自定义区域
					// showAdvSearchPlanBtn: status==='browse' ? false : true, // 高级面板中是否显示保存方案按钮
					searchBtnName: isHideBtnArea ? this.state.json['20023030-000000'] : this.state.json['20023030-000004'], /* 国际化处理： 确定*//* 国际化处理： 查询*/
                    clickSearchBtn: () => {
                        // let pk_accountingbook = [];
						// let pk_unit = [];

						// if (Array.isArray(this.state.accountingbook)) {
						// 	this.state.accountingbook.forEach(function (item) {
						// 		pk_accountingbook.push(item.refpk)
						// 	})
						// }

						// if (Array.isArray(this.state.buSecond)) {
						// 	this.state.buSecond.forEach(function (item) {
						// 		pk_unit.push(item.refpk)
						// 	})
						// }

						// let data = {
						// 	pk_accountingbook: pk_accountingbook,
						// 	pk_unit,
						// 	multbusi: this.state.multbusi,
						// 	usesubjversion: this.state.isversiondate ? 'Y' : 'N',// 是否启用科目版本
						// 	versiondate: this.state.isversiondate ? this.state.versiondate : this.state.busiDate,//科目版本
						// 	// pk_accasoa://条件框选择科目
						// 	startcode: this.state.startcode.code,  //开始科目编码
						// 	endcode: this.state.endcode.code,    //结束科目编码
						// 	startlvl: this.state.startlvl.toString(),
						// 	endlvl: this.state.endlvl.toString(),
						// 	isleave: this.state.isleave ? 'Y' : 'N',
						// 	isoutacc: this.state.isoutacc ? 'Y' : 'N',
						// 	/**会计期间：*/
						// 	querybyperiod: this.state.selectionState,//
						// 	startyear: this.state.startyear,
						// 	endyear: this.state.endyear,
						// 	startperiod: this.state.startperiod,
						// 	endperiod: this.state.endperiod,
						// 	/**日期：*/
						// 	startdate: this.state.begindate,
						// 	enddate: this.state.enddate,
						// 	//未记账、错误、损益结转、重分类  Y/N  注意：‘包含凭证’是个标题，后面的才是选项
						// 	includeuntally: this.state.includeuntally ? 'Y' : 'N',
						// 	includeerror: this.state.includeerror ? 'Y' : 'N',
						// 	includeplcf: this.state.includeplcf ? 'Y' : 'N',
						// 	includerc: this.state.includerc ? 'Y' : 'N',
						// 	pk_currtype: this.state.currtype,
						// 	returncurr: this.state.selectedCurrValue, //返回币种  1,2,3 组织,集团,全局
						// 	mutibook: this.state.mutibookAlone ? 'Y' : 'N',  //写死的'N'？ 多核算账簿？
						// 	showzerobalanceoccur: this.state.showzerobalanceoccur ? 'Y' : 'N',
						// 	sorttype: this.state.selectedSortValue, //排序方式
						// 	combinedetail: this.state.combinedetail ? 'Y' : 'N',
						// 	showaccum: this.state.showaccum ? 'Y' : 'N',
						// 	showbusdate: this.state.showbusdate ? 'Y' : 'N',
						// 	partionbyperiod: this.state.partionbyperiod ? 'Y' : 'N',
						// 	pk_accasoa: this.state.rightSelectItem,//'条件'
						// 	appcode: this.state.appcode,
						// 	index: this.index
						// }
						if(status !== 'browse'){
							let data = this.getConfirmData(this.state)
							let url = '/nccloud/gl/accountrep/checkparam.do'
							let flagShowOrHide;
							ajax({
								url,
								data,
								async: false,
								success: function (response) {
									flagShowOrHide = true
								},
								error: function (error) {
									flagShowOrHide = false
									toast({ content: error.message, color: 'warning' });
								}
							})
							if (flagShowOrHide == true) {
								if(hideBtnArea){
									this.props.onConfirm(data, this.state)
								} else {
									this.props.onConfirm(data)
								}
							} else {
								return true;
							}
						} else {
							return
						}
						
					},  // 点击按钮事件
                })}
        </div>
	}
}

CentralConstructorModal = createPage({})(CentralConstructorModal)
export default CentralConstructorModal;
