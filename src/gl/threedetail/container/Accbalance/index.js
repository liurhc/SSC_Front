import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';

// import OpenTable from './OpenTable';

import {high,base,ajax, deepClone, createPage, getMultiLang,gzip,createPageIcon } from 'nc-lightapp-front';
const { NCTree:Tree, NCDiv } = base;

const TreeNode = Tree.NCTreeNode;
const keys = ['0-0-0', '0-0-1'];

import './index.less';
import { toast } from '../../../public/components/utils';
import MainSelectModal from './MainSelectModal';
import { SimpleTable } from 'nc-report';
import {tableDefaultData} from '../../../manageReport/defaultTableData';
import reportPrint from '../../../public/components/reportPrint.js';
import reportSaveWidths from '../../../public/common/reportSaveWidths.js';
import RepPrintModal from '../../../manageReport/common/printModal'
import { getDetailPort } from '../../../manageReport/common/modules/modules'
import { relevanceSearch, journalUrl} from '../../../manageReport/referUrl'
const { PrintOutput } = high;
import { mouldOutput } from '../../../public/components/printModal/events'
import { printRequire } from '../../../manageReport/common/printModal/events'
import HeadCom from '../../../manageReport/common/headSearch/headCom';
import {handleValueChange} from '../../../manageReport/common/modules/handleValueChange'
import {searchById} from "../../../manageReport/common/modules/createBtn";
import {handleSimpleTableClick} from "../../../manageReport/common/modules/simpleTableClick";
import {setData} from "../../../manageReport/common/simbleTableData";
import PageButtonGroup from '../../../manageReport/common/modules/pageButtonGroup';
import {whetherDetail} from "../../../manageReport/common/modules/simpleTableClick";
import {rowBackgroundColor} from "../../../manageReport/common/htRowBackground";
import HeaderArea from '../../../public/components/HeaderArea';
import Iframe from '../../../public/components/Iframe';
import { queryRelatedAppcode, originAppcode } from '../../../public/common/queryRelatedAppcode.js';
class Accbalance extends Component {
	constructor(props) {
		super(props);
		//每定义一个state 都要添加备注
		this.state = {
            expandedKeys:"",//更新左侧树的默认展开key
            treeindex:[],//后台返回的下一个查询数据的下标值
            selectNum:"",
            json: {},
            typeDisabled: true,//账簿格式 默认是禁用的
            showTree:false,
            showRotate: true,//控制加载层
            codes: [],
            firstPageDisable: true,
            lastPageDisable: true,
            accountType: 'amountcolumn',//金额式
			flag:0,//切换表头用的
      		showTable: false,
      		textAlginArr:[],//对其方式
			disabled: true,
            visible: false,
			appcode: this.props.getUrlParam('c') || this.props.getSearchParam('c'),
			currpage:'',
			outputData: {},
			ctemplate: '', //模板输出-模板
			nodekey: '',
			printParams:{}, //查询框参数，打印使用
			data: {},
			headtitle: {},
			gData: [],/* 国际化处理： 会计科目*/
			defaultExpandedKeys: keys,
			defaultSelectedKeys: keys,
			defaultCheckedKeys:keys,
         	myhandsontableData: {},
         	queryDatas: {},
         	accountcode: '',
         	settings:{  
	         data : [],
	         mergeCells:[],
	         cell : [],
	         cells:{},
	         rowHeaders: true,
	         manualColumnResize:true, 
	         readOnly:true,
	         columnSorting: true, // 排序功能
	         wordWrap:false,
	         colHeaders:true,
	         autoColumnSize:false,  
	         autoRowSize:false,
	         // afterSelection: this.selectRows,

	         // minCols:50,
           },
			pager: {},
			MainSelectModalShow: false, //查询模态框
			tableAll: {
				column: [],
				data:[],
			},              //表格全体数据
			initTableAll: null,  //初始查询出的表格全体数据
			tableStyle: {}, //表格样式
			queryCond: null, //已选择的查询条件
			pk_cashflow: '', //已点击行的pk_cashflow
			datas: {
				time: '',
				bookname: '',
				currtypeName: '',
			},
            dataout: deepClone(tableDefaultData),
            'threeDetail': [],
            hideBtnArea : false, // 是否隐藏高级查询按钮
            defaultAccouontBook: {} //默认账簿
        }
		this.columnInfo = [];
		this.headtitle = [];
		this.balanceVO = [];
        this.pageType = 'nextPage';
        this.dataPage = 0; //存放请求回来的数据椰树信息
        this.handleValueChange = handleValueChange.bind(this);
        this.searchById = searchById.bind(this);
        this.handleSimpleTableClick = handleSimpleTableClick.bind(this);
        this.whetherDetail = whetherDetail.bind(this);
        this.rowBackgroundColor = rowBackgroundColor.bind(this);
	}

    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json,
                gData: [{key: '', name: this.state.json['20023030-000018']}],/* 国际化处理： 会计科目*/
                'threeDetail': [
                    {
                        title: json['20023030-000047'],
                        styleClass:"m-long"
                    },
                    {
                        title: json['20023030-000048'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20023030-000049'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20023030-000050'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20023030-000051'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20023030-000052'],
                        styleClass:"m-brief"
                    }

                ],
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20023030', 'publiccommon', 'childmodules'],domainName:'gl',currentLocale:'simpchn',callback});
    }

    componentDidMount() {
        this.setState({
            data: {}
        })
        this.getParam();
        let appceod = this.props.getSearchParam('c');
        this.searchById('20023030PAGE', appceod).then((res) => {
            // searchById返回当前登录环境的数据源信息。
            // 如果联查的数据源与当前数据源不一致，则隐藏页面的所有按钮。
            let param = this.props.getUrlParam && this.props.getUrlParam('status');
            if (param && res && res.data.context) {
                let { context } = res.data
                let unzipParam = new gzip().unzip(param)
                if (unzipParam.env) {
                    let envs = unzipParam.env.replace(/\[/g, "").replace(/\]/g, "").replace(/\"/g, "").split(",");
                    if (envs[0] && envs[0] != res.data.context.dataSource) {
                        this.props.button.setButtons([]);
                        this.setState({ hideBtnArea: true });
                    }
                }
                this.state.defaultAccouontBook = {refname:context.defaultAccbookName,refpk:context.defaultAccbookPk, getflag: true}
            }
        });
        this.props.button.setDisabled({
            print: true, filter: true,
            linkvoucher: true, templateOutput: true,
            directprint: true, saveformat: true
        });
        this.props.button.setButtonVisible(['first', 'pre', 'next', 'last'], false)
    }
	//直接输出
	print(){
		let {resourceDatas,dataout,textAlginArr}=this.state;
		let dataArr=[];
		let emptyArr=[];
		let mergeInfo=dataout.data.mergeInfo;
		dataout.data.cells.map((item,index)=>{
			emptyArr=[];
			item.map((list,_index)=>{
				//emptyArr=[];
				if(list){
					// if(list.title){
					//     list.title='';
					// }
					emptyArr.push(list.title);
				}
			})
			dataArr.push(emptyArr);
		})
		//(dataArr);
		reportPrint(mergeInfo,dataArr,textAlginArr);
	}
	//保存列宽
	handleSaveColwidth=()=>{
	    let {json} = this.state;
        let info=this.refs.balanceTable.getReportInfo();
        let callBack = this.refs.balanceTable.resetWidths
		reportSaveWidths(this.state.appcode,info.colWidths, json, callBack);
	}
	showPrintModal() {
        this.setState({
            visible: true
        })
    }
    handlePrint(data, isPreview) {
		let printUrl = '/nccloud/gl/accountrep/triaccdetailprint.do'
		let { printParams, appcode ,currpage } = this.state
		let { ctemplate, nodekey } = data
		printParams.currpage = currpage
		printParams.queryvo = data
		this.setState({
			ctemplate: ctemplate,
			nodekey: nodekey
        })
		//(printParams,'--------printParams')
        printRequire(this.props,printUrl, appcode, nodekey, ctemplate, printParams, isPreview)
        this.handleCancel()
    }
    handleCancel() {
        this.setState({
            visible: false
        });
	}
	showOutputModal() {
		// this.refs.printOutput.open()
		let outputUrl = '/nccloud/gl/accountrep/triaccdetailoutput.do'
		let { appcode, nodekey, ctemplate, printParams } = this.state
        mouldOutput(outputUrl, appcode, nodekey, ctemplate, printParams)
        // let outputData = mouldOutput(appcode, nodekey, ctemplate, printParams)
        // this.setState({
        //     outputData: outputData
        // })
    }
    handleOutput() {
        //(this.state.outputData,this.state.json['20023030-000019'])/* 国际化处理： -----模板输出----成功？----*/
    }

    getParam = () => {
        let urlParam;
        // 联查参数，需解压
        let data = this.props.getUrlParam && this.props.getUrlParam('status');
        if (data) {
            urlParam = new gzip().unzip(data);
        }
        // 小友智能查账，不需解压
        let param = this.props.getSearchParam('param');
        if (param) {
            urlParam = JSON.parse(param);
        }
        // 查询
        if (urlParam) {
            this.setState({
                printParams: urlParam,
            })
            this.getDatas(urlParam);
        }
    }

	queryShow() {
		this.setState({
			MainSelectModalShow: true,
		})
	}

	test(datas, index, param) {
		let self = this;
        this.props.button.setDisabled({
            print: false, templateOutput: false,
            directprint: false, saveformat: false
        });
      let url = '/nccloud/gl/accountrep/detailbookquery.do';
		ajax({
		    loading: true,
		    url,
		    data: datas,
		    success: function (res) {
		    	const { data, error, success } = res;
		    	//('datatata:::', data);
		    	if(success){
                    if(self.dataPage === 0){
                        toast({
                            color: 'success'
                        })
                    }
		        	if (data) {
                        if (index != 1) {
                            self.setState({
                                gData: data.treeData ? data.treeData : [{key: '', name: self.state.json['20023030-000018']}]/* 国际化处理： 会计科目*/
                            })
                        }
                        if(self.dataPage === 0 && self.state.codes.length === 0){
                            self.setState({
                                lastPageDisable: false,
                                codes: [...data.accounts]
                            })
                            self.props.button.setDisabled({
                                next: false, last: false
                            });
                        }
                        let TempPage = data.index;
                        // 设置应选中树节点的下标
                        if (self.state.codes) {
                            for (let i = 0; i < self.state.codes.length; i++) {
                                if (self.state.codes[i].split(' ')[0] == TempPage) {
                                    self.dataPage = i;
                                    break;
                                }
                            }
                        }
                        // 控制翻页按钮是否可用
                        self.setState({
                            firstPageDisable: self.dataPage == 0,
                            lastPageDisable: self.dataPage == self.state.codes.length - 1
                        })
						if(data.data.length>0 ){
							self.setState({
								disabled:false
							})
						}
                        self.setState({
                            dataWithPage: data,
                            typeDisabled: false,
                            currpage: TempPage
                        })
						if(data.column){
							self.columnInfo=data[self.state.accountType];
						}
						if(data.headtitle){
							self.headtitle=data.headtitle;
						}
						if(data.data){
							self.balanceVO=data.data;
						}
						self.setState({
                            dataWithPage: data,
                            selectRowIndex:0
                        })
                        let treeindex=data.treeindex;
                        if(self.state.selectNum!='1'){
                            let expandedKeys;
                            let gData = self.state.gData;
                            if(gData){
                                if (treeindex.length == 1) {
                                    expandedKeys = '';
                                } else {
                                    let treeNode = gData[treeindex[0]];
                                    for (let i = 1; i < treeindex.length - 1; i++) {
                                        treeNode = treeNode.children[treeindex[i]];
                                    }
                                    expandedKeys = treeNode.key;
                                }
                            }
                            self.setState({
                                expandedKeys                                              
                            })
                        }
                        self.setState({
                            showTree: true,
                            treeindex: treeindex
                        }, () => {
                            if (treeindex && self.state.selectNum != 1) {
                                let treeLen = treeindex.length;
                                let childEle = document.getElementsByClassName('myTree');
                                let innerEle = childEle[0].childNodes;//li元素(标签)的个数
                                let childAele = innerEle[self.state.treeindex[0]];
                                for (let i = 1; i < self.state.treeindex.length; i++) {
                                    let chileNodeIndex = self.state.treeindex[i];
                                    if (childAele.childNodes[2]) {
                                        childAele = childAele.childNodes[2].childNodes[chileNodeIndex];
                                    }
                                }
                                let selectTwo = document.getElementsByClassName('u-tree-node-selected');
                                if (selectTwo.length > 0) {
                                    selectTwo[0] && selectTwo[0].classList.remove('u-tree-node-selected');
                                }
                                childAele = childAele.getElementsByClassName('u-tree-node-content-wrapper');
                                childAele[0] && childAele[0].classList.add('u-tree-node-selected');
                                let docPosition = childAele[0].offsetTop - 158 //选中文本位置
                                childEle[0].scrollTop = docPosition  // 翻页时滚动条自动滑动
                            }
                        })
                        setData(self, data, true);
		        	}
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});
	}
    onExpand = (info) =>{
        if(info.length>0){
            this.setState({
                expandedKeys:info[info.length-1]
            })
        }else{
            this.setState({
                expandedKeys:""
            })
        }

    }
	onSelect = (info, param) =>  {
        this.setState({
            selectNum:"1"
        })
        if (info.length == 0 || info[0] == "0-0") {
            return
        }
        let childAele = document.getElementsByClassName('u-tree-node-selected');
        if(childAele.length>0){//清除掉之前的选中状态
            for(let i = 0;i < childAele.length; i++){
                let infoFlag = childAele[i].childNodes[0].childNodes[0].data
                            && childAele[i].childNodes[0].childNodes[0].data.split(" ")[0];
                if(info != infoFlag){
                    childAele[i].classList.remove("u-tree-node-selected")
                }
            }          
        }
        for(let index=0; index<this.state.gData.length; index++){
        	if(this.state.gData[index].key === info[0]){
        		if(index > 0 && index < this.state.gData.length){
        			this.setState({
						firstPageDisable: false,
                        selectRowIndex: 0
					})
                    this.props.button.setDisabled({
                        first: false, pre:false,
                        next: false, last: false
                    });
				}else{
                    this.setState({
                        firstPageDisable: true,
                        selectRowIndex: 0
                    })
                    this.props.button.setDisabled({
                        first: true, pre:true
                    });
				}
				this.dataPage = index;
				break;
			}
			//('onselecttt:::', this.state.gData[index], )
		}

		
		let datas = this.state.queryDatas;
        datas.accountcode = info;
        // 使用accountcode查询，清空页码和操作
        delete datas.pageindex;
        delete datas.operation;
        this.props.button.setDisabled({
            linkvoucher: true
        });
		this.test(datas, 1, param);
		this.setState({
			currpage:info[0]
		})
	}


	onCheck = (checkedKeys) => {
		let self = this;
		//('onCheck', checkedKeys);
		const cks = {
			checked: checkedKeys.checked || checkedKeys,
		};
		// this.setState({checkedKeys:cks});
	}

	/**
     * param: true标示是点击刷新
     * */
	getDatas = (datas, param) => {
        // 清空页码和操作
        delete datas.pageindex;
        datas.operation = "nextPage";

        this.dataPage = 0;
        if(param){
            this.state.queryDatas.accountcode = []
        }
        this.pageType = 'nextPage';
        this.state.codes = [];
        this.props.button.setDisabled({
            first: true, pre:true,
        });
		this.setState({
			queryDatas:datas,
			printParams:datas,
			firstPageDisable: true,
            showTree: false,
            expandedKeys: '',
            selectNum: ''
		}, () => this.test(datas))
	}
    /**
     * param: 'firstPage, nextPage, prePage, lastPage;
     * dataWithPage: 总数据
     * paramObj
     * */
    handlePage = (param, dataWithPage) => {//页面控制事件
        let {queryDatas, codes} = this.state;
        let dataLength = codes.length;

        let childEle = document.getElementsByClassName('myTree');
        if(childEle.length>0){
            let innerEle = childEle[0].childNodes;//li元素(标签)的个数
            let childAele = document.getElementsByClassName('u-tree-node-selected');

            childAele[0] && childAele[0].classList.remove('u-tree-node-selected');
        }
            //('handlePage>>',codes,dataLength, param, dataWithPage, childEle[0],innerEle);
            //('innerEle.length:',innerEle.length)
            let renderData = [];
            let obj = {};
            let stringPage = '1';
            let newCode = [];
            queryDatas.operation = param;// 翻页操作
            switch(param){
                case 'firstPage':
                    this.dataPage = 0;
                    queryDatas.pageindex = (this.dataPage).toString();
                    this.setState({
                        queryDatas,
                        firstPageDisable: true,
                        lastPageDisable: false,
                        selectRowIndex: 0,
                        selectNum: ''
                    })
                    this.props.button.setDisabled({
                        first: true, pre:true,
                        next: false, last: false
                    });
                    //('firstPage:::', this.dataPage, obj, this.state.paramObj);
                    break;
                case 'nextPage':
                    this.dataPage += 1;
                    queryDatas.pageindex = (this.dataPage).toString();
                    this.setState({
                        queryDatas,
                        firstPageDisable: false,//可以点击
                        selectRowIndex: 0,
                        selectNum:"2"
                    })
                    this.props.button.setDisabled({
                        first: false, pre:false,
                    });
                    if(this.dataPage === dataLength-1){
                        this.setState({
                            lastPageDisable: true
                        })
                        this.props.button.setDisabled({
                            next: true, last: true
                        });
                    }
                    break;
                case 'prePage':
                    if (this.dataPage === 0) {
                        return;
                    }
                    this.dataPage -=1;
                    queryDatas.pageindex = (this.dataPage).toString();
                    if(this.dataPage <= dataLength-2){
                        this.setState({
                            lastPageDisable: false
                        })
                        this.props.button.setDisabled({
                            next: false, last: false
                        });
                    }
                    if(this.dataPage === 0){
                        this.setState({
                            firstPageDisable: true
                        })
                        this.props.button.setDisabled({
                            first: true, pre:true,
                        });
                    }
                    this.setState({
                        queryDatas,
                        selectRowIndex: 0,
                        selectNum:"3"
                    })
                    break;
                case 'lastPage':
                    this.dataPage = dataLength-1;
                    queryDatas.pageindex = (this.dataPage).toString();
                    this.setState({
                        queryDatas,
                        lastPageDisable: true,
                        firstPageDisable: false,
                        selectRowIndex: 0,
                        selectNum: ''
                    });
                    this.props.button.setDisabled({
                        first: false, pre:false,
                        next: true, last: true
                    });
                    break;
            }
            this.test(this.state.queryDatas, 1);
            return;
       // }
        
    }
    handleLoginBtn = (obj, btnName) => {
        //('handleLoginBtn>>>', obj, btnName)
        if(btnName === 'print'){//1、打印
            this.showPrintModal()
        }else if(btnName === 'filter'){//6、过滤
            let queryCond = this.getOrigParam();// 查询条件
            let islink = this.state.queryDatas.link != null;// 联查
            let isFromMultiOrg = this.isFromMultiOrg();// 来自科目余额表按核算账簿列式的联查
            // 过滤提示：1、不支持多主体过滤 2、不支持按版本过滤
            if (queryCond.pk_accountingbook && queryCond.pk_accountingbook.length > 1
                    && !isFromMultiOrg && (!islink || (islink && queryCond.mutibook == 'Y')/* 联查来的，多主体合并则不支持过滤，按主体列式支持。 */)) {
                toast({ content: this.state.json['20023030-000053']/* 国际化处理： 不支持多主体过滤*/, color: 'warning' });
                return;
            }
            if (queryCond.usesubjversion == 'Y') {
                toast({ content: this.state.json['20023030-000054']/* 国际化处理： 不支持按版本过滤*/, color: 'warning' });
                return;
            }
            // 过滤
            getDetailPort(this, {
                url: '/nccloud/gl/accountrep/triaccquery.do',
                jumpTo: journalUrl,
                key: 'threeDetailToJournal',
                appcode: queryRelatedAppcode(originAppcode.journal),
                filterParam: 'filter'
            }, this.state.json)
        }else if(btnName === 'linkvoucher'){//5、联查凭证
            getDetailPort(this, {
                url: '/nccloud/gl/accountrep/triaccquery.do',
                jumpTo: relevanceSearch,
                key: 'relevanceSearch',
                appcode: '20023030'
            }, this.state.json)
        }else if(btnName === 'templateOutput'){//2、模板输出
            this.showOutputModal()
        }else if(btnName === 'directprint'){//3、直接输出
            this.print()
        }else if(btnName === 'saveformat'){//4、保存列宽
            this.handleSaveColwidth()
        }else if(btnName === 'refresh'){//刷新
            if (Object.keys(this.state.queryDatas).length > 0) {
                this.getDatas(this.state.queryDatas, true);
            }
        }else if(btnName === 'first'){//首页
            this.pageType = 'nextPage';
            this.handlePage('firstPage', this.state.dataWithPage)
        }else if(btnName === 'pre'){//上一页
            this.pageType = 'prePage';
            this.handlePage('prePage', this.state.dataWithPage)
        }else if(btnName === 'next'){//下一页
            this.pageType = 'nextPage';
            this.handlePage('nextPage', this.state.dataWithPage)
        }else if(btnName === 'last'){//末页
            this.pageType = 'prePage';
            this.handlePage('lastPage', this.state.dataWithPage)
        }
    }
    firstCallback = () => {//首页
        this.pageType = 'nextPage';
        this.handlePage('firstPage', this.state.dataWithPage)
    }
    preCallBack = () => {//上一页
        this.pageType = 'prePage';
        this.handlePage('prePage', this.state.dataWithPage)
    }
    nextCallBack = () => {//下一页
        this.pageType = 'nextPage';
        this.handlePage('nextPage', this.state.dataWithPage)
    }
    lastCallBack = () => {//末页
        this.pageType = 'prePage';
        this.handlePage('lastPage', this.state.dataWithPage)
    }

    getOrigParam = () => {// 取查询条件。联查来的，取原始查询条件。
        let queryDatas = this.state.queryDatas;
        while (queryDatas && queryDatas.origparam) {
            queryDatas = queryDatas.origparam;
        }
        return queryDatas;
    }

    isFromMultiOrg = () => {// 是否来自科目余额表按核算账簿列式的联查
        let isFromMultiOrg = false;
        let queryDatas = this.state.queryDatas;
        while (queryDatas && queryDatas.origparam) {
            if (queryDatas.from == "multicorp") {
                isFromMultiOrg = true;
            }
            queryDatas = queryDatas.origparam;
        }
        return isFromMultiOrg;
    }
	render() {
        //('render::', this.state);
        let { modal, DragWidthCom } = this.props;
        const { createModal } = modal;
        let {expandedKeys} = this.state
		const loop = data => data.map((item) => {
			if (item.children && item.children.length) {
			  return <TreeNode liAttr={{"fieldid": `${item.key}_node`}} key={item.key} title={item.name}>{loop(item.children)}</TreeNode>;
			}
			return <TreeNode liAttr={{"fieldid": `${item.key}_node`}} key={item.key} title={item.name} />;

      	});
        let leftDom = <div className="tree-area">{/* 左树区域 tree-area*/}
            {
                this.state.showTree?
                    <NCDiv fieldid="threedetail" areaCode={NCDiv.config.TreeCom} className='left-area'>
                        <Tree
                            ref='treeNodeRef'
                            className="myTree"
                            showLine
                            checkStrictly
                            onSelect={ (info) => this.onSelect(info, 'onSelect')}
                            expandedKeys={expandedKeys}
                            onExpand={(info) => this.onExpand(info)}

                        >
                            {loop(this.state.gData)}
                        </Tree>
                    </NCDiv>
                    :
                    <div className='myTreenone nc-theme-l-area-bgc'>
                        <div style={{color: '#bfbfbf'}}>
                            <i style={{fontSize: '50px'}} className='uf icon-tip uf-exc-c-o'></i>
                            <p style={{fontSize: '13px'}}>{this.state.json['20023030-000055']}</p>
                        </div>
                    </div>
            }
        </div>
        let rightDom =  <div className="table-area">
            <SimpleTable
                ref="balanceTable"
                data = {this.state.dataout}
                // onCellMouseDown = {(e, coords, td) => this.handleSimpleTableClick(e, coords, td, 'threedetail')}
                onCellMouseDown = {(e, coords, td) => {
                    //('recorddd:::', this.refs.balanceTable.getRowRecord());
                    this.whetherDetail('link', 'multiAccountSearch');
                    this.rowBackgroundColor(e, coords, td)
                }}
            />
        </div>

		return (
			<div id="cashQuery" className='nc-bill-tree-table three-report'>
                <HeaderArea 
                    title = {this.state.json['20023030-000020']} /* 国际化处理： 三栏式明细账*/
                    btnContent = {
                        <div>
                            <div className='account-query-btn'>
                                <MainSelectModal
                                    hideBtnArea={this.state.hideBtnArea}
                                    defaultAccouontBook={this.state.defaultAccouontBook}
                                    onConfirm={(datas) => {
                                        this.getDatas(datas);
                                    }}
                                />
                            </div>
                            {this.props.button.createButtonApp({
                                area: 'btnarea',
                                buttonLimit: 3,
                                onButtonClick: (obj, tbnName) => this.handleLoginBtn(obj, tbnName),
                            })}
                        </div>
                    }
                    pageBtnContent = {
                        <PageButtonGroup
                            first={{disabled: this.state.firstPageDisable, callBack: this.firstCallback}}
                            pre={{disabled: this.state.firstPageDisable, callBack: this.preCallBack}}
                            next={{disabled: this.state.lastPageDisable, callBack: this.nextCallBack}}
                            last={{disabled: this.state.lastPageDisable, callBack: this.lastCallBack}}
                        />
                    }
                />
                <NCDiv areaCode={NCDiv.config.SEARCH} className="searchContainer nc-theme-gray-area-bgc">
                    {
                        this.state.threeDetail.map((items) => {
                            return(
                                <HeadCom
                                    labels={items.title}
                                    key={items.title}
                                    // content={this.state.dataWithPage.data}
                                    // disabled={this.state.disabled}
                                    lastThre = {this.headtitle && this.headtitle}
                                    changeSelectStyle={
                                        (key, value) => this.handleValueChange(key, value, 'assistAnalyzSearch', () => setData(this, this.state.dataWithPage))
                                    }
                                    accountType = {this.state.accountType}
                                    isLong = {items.styleClass}
                                    typeDisabled = {this.state.typeDisabled}
                                />
                            )
                        })
                    }
                </NCDiv>
                <div className='tree-table-container'>
                    <div className="tree-table" style={{height: '100%'}}>
                        <DragWidthCom
                            onDragStop={()=>{
                                this.refs.balanceTable.updateSettings()
                            }}
                            leftDom={leftDom}     //左侧区域dom
                            rightDom={rightDom}     //右侧区域dom
                            defLeftWid='280px'      // 默认左侧区域宽度，px/百分百
                        />
                    </div>
                </div>
                
				<RepPrintModal
                    appcode={this.state.appcode}
                    visible={this.state.visible}
                    showScopeall={true}
                    handlePrint={this.handlePrint.bind(this)}
                    handleCancel={this.handleCancel.bind(this)}
                />
				<PrintOutput                    
                    ref='printOutput'
                    url='/nccloud/gl/accountrep/triaccdetailoutput.do'
                    data={this.state.outputData}
                    callback={this.handleOutput.bind(this)}
                />
                {createModal('printService', {
                    className: 'print-service'
                })}
                <Iframe />
			</div>
		)
	}
}
Accbalance = createPage({})(Accbalance)
export default Accbalance
