import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';
// import moment from 'moment';

import {high,base,ajax,deepClone, createPage, getMultiLang } from 'nc-lightapp-front';
import './index.less';
import  '../../../../public/reportcss/searchmodalpage.less'


const { Refer } = high;

const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect,
NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCModal: Modal,
} = base;

import { toast } from '../../../../public/components/utils';

const NCOption = NCSelect.NCOption;
// const deepClone = require('../../../../public/components/deepClone');
const format = 'YYYY-MM-DD';
// import AccountBookByFinanceOrgRef from '../../../../../uapbd/refer/org/AccountBookByFinanceOrgRef';

// import AccperiodMonthTreeGridRef from '../../../../../uapbd/refer/pubinfo/AccperiodMonthTreeGridRef';
import AccPeriodDefaultTreeGridRef from '../../../../../uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef';
import BusinessUnitTreeRef from '../../../../../uapbd/refer/orgv/BusinessUnitVersionDefaultAllTreeRef';

import AccountDefaultModelTreeRef from '../../../../../uapbd/refer/fiacc/AccountDefaultModelTreeRef';
import createScript from '../../../../public/components/uapRefer.js';
import {
    businessUnit,
    createReferFn, getCheckContent, delKeyOfObj,
    getReferDetault, renderLevelOptions, renderMoneyType,
    renderRefer, returnMoneyType
} from "../../../../manageReport/common/modules/modules";
import {subjectRefer} from "../../../../manageReport/referUrl";
import SubjectVersion from "../../../../manageReport/common/modules/subjectVersion";
import DateGroup from '../../../../manageReport/common/modules/dateGroup'
import SubjectLevel from '../../../../manageReport/common/modules/subjectAndLevel'
import {clearTransterData, handleChange, handleSelectChange} from "../../../../manageReport/common/modules/transferFn";
import CheckBoxCells from '../../../../manageReport/common/modules/checkBox';
import initTemplate from '../../../../manageReport/modalFiles/initTemplate';
import { FICheckbox } from '../../../../public/components/base';

class CentralConstructorModal extends Component {

	constructor(props) {
		super(props);
		this.state = this.getAllState()
        this.renderRefer = renderRefer.bind(this);
		this.searchId = '20023010query';
		this.index = '' //账簿打印行号
	}

    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json,
                gData: [{key: '', name: this.state.json['20023010-000012']}],/* 国际化处理： 会计科目*/
                currtype: json['20023010-000000'],      //选择的币种/* 国际化处理： 本币*/
                currtypeName: json['20023010-000000'],  //选择的币种名称/* 国际化处理： 本币*/
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20023010', 'publiccommon', 'dategroup', 'checkbox', 'subjectandlevel', 'subjectversion', 'childmodules'],domainName:'gl',currentLocale:'simpchn',callback});
	   if(!this.props.hideBtnArea){
			initTemplate.call(this,this.props, this.state.appcode)
		}
		this.getCurrtypeList();
		
    }

	componentDidMount() {
        setTimeout(() => getReferDetault(this, true, {
            businessUnit
		},'',this.props.defaultAccouontBook),0);
		// setTimeout(() => initTemplate.call(this,this.props, this.state.appcode), 0)
		this.props.MainSelectRef && this.props.MainSelectRef(this)
	}

	componentWillReceiveProps(nextProps) {
		// this.getEndAccount(nextProps);
	}
	//获取初始化state
	getInitState = () => {
		let initState = {
			defaultAccouontBook:[this.props.defaultAccouontBook],
			appcode: "20023010",
			// json: {},
            isqueryuntallyed: true, //是否禁止设置查询包含未记账凭证;true: 禁止
            groupCurrency: true,//是否禁止"集团本币"可选；true: 禁止
            globalCurrency: true, //是否禁止"全局本币"可选；true: 禁止

            showRadio: false,//控制是否显示"会计期间"的但选框；false: 不显示
            selectedKeys: [],
            targetKeys: [],
            changeParam: true, //false:点击条件时不触发接口，,
            rightSelectItem: [], //科目的"条件"按钮中存放右侧数据的pk值

            start: {},  //开始期间
            end: {}, //结束期间
            /*****会计期间******/
            selectionState: 'true', //按期间查询true，按日期查询false
            startyear: '', //开始年
            startperiod: '',  //开始期间
            endyear: '',  //结束年
            endperiod: '',   //结束期间
            /*****日期******/
            begindate: '', //开始时间
            enddate: '', //结束时间
            rangeDate: [],//时间显示
            versionDateArr: [],
			accountingbook: this.props.defaultAccouontBook&&this.props.defaultAccouontBook.refpk?[this.props.defaultAccouontBook]:[],  //核算账簿
			accasoa: {           //科目
				refname:'',
				refpk: '',
			},
			buSecond: {        //二级单元
				refname:'',
				refpk: '',
			},
			versiondateList: [], //科目版本列表
			accountlvl: '1', //设置级次1到几
			selectedQueryValue: '0', //查询方式单选
			selectedCurrValue: '1', //返回币种单选
			ismain: '0', // 0主表  1附表

			currtype: '',      //选择的币种/* 国际化处理： 本币*/
			currtypeName: '',  //选择的币种名称/* 国际化处理： 本币*/
			currtypeList: [],  //币种列表
			defaultCurrtype: {         //默认币种
				name: '',
				pk_currtype: '',
			},
			checkbox: {
				one: false,
				two: false,
				three: false,
			},
			balanceori: '-1', //余额方向
			startlvl: '1', //开始级次
			endlvl: '1',  //结束级次
			startcode: {
				refname:'',
				refpk: '',
				refcode: '',
			}, //开始科目编码
			endcode: {
				refname:'',
				refpk: '',
				refcode: '',
			},    //结束科目编码
			versiondate: '', //制单日期，后续要用下拉选
			isleave: false, //是否所有末级
			isoutacc: false, //是否表外科目
			includeuntally: false, //包含凭证的4个复选框 未记账
			includeerror: false, //包含凭证的4个复选框 错误
			includeplcf: true, //包含凭证的4个复选框 损益结转
			includerc: false, //包含凭证的4个复选框 重分类
			mutibook: 'N', //多主体显示方式的选择
			disMutibook: true, //多主体显示是否禁用
			currplusacc: 'Y', //排序方式
			disCurrplusacc: true, //排序方式是否禁用
			showzerooccur: false, //无发生不显示
			showzerobalanceoccur: true, //无余额无发生不显示
			sumbysubjtype: false, //科目类型小计
			disSumbysubjtype: true, //科目类型小计是否禁用
			twowaybalance: false, //借贷方显示余额
			showSecond: false, //是否显示二级业务单元
			multbusi: false, //多业务单元复选框

			isversiondate: false,
			pk_accperiodscheme: '', //会计期间参照用的过滤	
			transferDataSource: []	//穿梭框原数据
		}
		return initState
	}
	//state中添加json
	getAllState = () => {
		let initState = this.getInitState()
		initState.json={}
		return initState
	}
	//初始化state
	setInitState = (querystate) => {
		if(JSON.stringify(querystate) !=='{}') {
			let disabled = this.props.status === 'browse' ? true : false
			this.setState(querystate,()=>{
				this.TransferTerms.setTransferState(querystate.transferDataSource, disabled)
			})
			
		} else {
			let initState = this.getInitState()
			delKeyOfObj(initState, ['currtype', 'currtypeName', 'currtypeList'])
			this.setState(initState);
			getReferDetault(this, true, {
				businessUnit
			},'',this.props.defaultAccouontBook);
		}
	}
	/**
	 * 条件 弹框‘确定’ 
	 * transferDataSource: 穿梭框数据
	 */
	transferSure = (transferDataSource) => {
		this.setState({
			transferDataSource
		})
	}
	//打开高级查询面板
	openAdvSearch = (index, querystate) => {
		initTemplate.call(this,this.props, this.state.appcode).then((data)=>{
			this.props.search.openAdvSearch(this.searchId, true)
			this.index = index;
			this.setInitState(querystate);
		})
	}
	//获取返回父组件的值
	getConfirmData = (tempState) => {
		let pk_accountingbook = [];
		let pk_unit = [];
		if (Array.isArray(tempState.accountingbook)) {
			tempState.accountingbook.forEach(function (item) {
				pk_accountingbook.push(item.refpk)
			})
		}
		if (Array.isArray(tempState.buSecond)) {
			tempState.buSecond.forEach(function (item) {
				pk_unit.push(item.refpk)
			})
		}
		let confirmData = {
			pk_accountingbook: pk_accountingbook,
			pk_unit: pk_unit,
			multbusi: tempState.multbusi,
			usesubjversion: tempState.isversiondate ? 'Y' : 'N',// 是否启用科目版本 
			versiondate: tempState.isversiondate ? tempState.versiondate : tempState.busiDate,//科目版本

			startcode: tempState.startcode.code,  //开始科目编码
			endcode: tempState.endcode.code,    //结束科目编码
			startlvl: String(tempState.startlvl),
			endlvl: String(tempState.endlvl),
			isleave: tempState.isleave ? 'Y' : 'N',
			isoutacc: tempState.isoutacc ? 'Y' : 'N',
			startyear: tempState.startyear,
			endyear: tempState.endyear,
			startperiod: tempState.startperiod,
			endperiod: tempState.endperiod,

			//未记账、错误、损益结转、重分类  Y/N  注意：‘包含凭证’是个标题，后面的才是选项
			includeuntally: tempState.includeuntally ? 'Y' : 'N',
			includeerror: tempState.includeerror ? 'Y' : 'N',
			includeplcf: tempState.includeplcf ? 'Y' : 'N',
			includerc: tempState.includerc ? 'Y' : 'N',
			pk_currtype: tempState.currtype,
			returncurr: tempState.selectedCurrValue, //返回币种  1,2,3 组织,集团,全局
			mutibook: tempState.mutibook,

			showzerobalanceoccur: tempState.showzerobalanceoccur ? 'Y' : 'N',
			pk_accasoa: tempState.rightSelectItem,//'条件'
			appcode: tempState.appcode,
			index: this.index
		}
		return confirmData 
	}
	
	handleRadioChange(value) {
		this.setState({selectedQueryValue: value});
	}

	handleRadioCurrChange(value) {
		this.setState({selectedCurrValue: value});
	}
	
	//排序方式单选变化
	handleCurrplusacc(value) {
		this.setState({currplusacc: value});
	}
	

	getlvlver() {
		let self = this;
		let url = '/nccloud/gl/glpub/accountinfoquery.do';

		let data = {
			pk_accountingbook: this.state.accountingbook[0].refpk,
		};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		    	// 默认取科目版本列表最后一个
		        //(res.data);
		        const { data, error, success } = res;

		        if(success){
		        	
					self.setState({
						accountlvl: data.accountlvl,
						versiondateList: data.versiondate,
						versiondate: data.versiondate[data.versiondate.length - 1],
						pk_accperiodscheme: data.pk_accperiodscheme,
					})
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});


	}


    handleValueChange = (key, value, param) => {
		//'includeuntally', value, 'assistAnalyzSearch' / 'includeerror', value, 'assistAnalyzSearch'
		//('key<>', key, value, param)
		this.setState({
			[key]: value
		})
		if(param === 'SubjectVersion' || param === 'accbalance') {
            this.setState({
                changeParam: true,
                rightSelectItem: []
            })
        };
		if(key === 'includeuntally'){
			this.setState({
                includeerror: value
			})
		}
	}

	getCurrtypeList = () => {
		let self = this;
		let url = '/nccloud/gl/voucher/queryAllCurrtype.do';
		// let data = '';
		let data = {
			"localType":"1",
			showAllCurr: '1',
		};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		        const { data, error, success } = res;
		        if(success){
					self.setState({
						currtypeList: data,
					})
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});

	}

	handleBalanceoriChange(value) {
		this.setState({balanceori: value});
	}


    handleCurrtypeChange = (keyParam, value) => {
        //('handleCurrtypeChange>', keyParam, value)
        this.setState({
            [keyParam]: value,
        });
    }


	handleIsmainChange(value) {
		this.setState({
			ismain: value,
		})
	}
	
	//根据核算账簿获取相关信息
	getInfoByBook(type) {
		// //(this.state.accountingbook)


		if (!this.state.accountingbook || !this.state.accountingbook[0]) {
			//如果是清空了核算账簿，应该把相关的东西都还原
			return;
		}

		this.getlvlver();
		
		let self = this;
		let url = '/nccloud/gl/voucher/queryAccountingBook.do';
		let data = {
			pk_accountingbook: this.state.accountingbook[0].refpk,	
			year: '',		
		};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		    	const { data, error, success } = res;
		    	//(res)
		    	// return
		        if(success){
		        	let isStartBUSecond = data.isStartBUSecond;
		        	let showSecond = false;
		        	let disMutibook = true;
		        	

		        	//单选判断
		        	if (self.state.accountingbook.length == 1) {

			        	if (isStartBUSecond) {
							showSecond = true;
						} else {
							showSecond = false;
						}
					} else {
						showSecond = false;
						disMutibook = false;

					}
		        	self.setState({
		        		isStartBUSecond: data.isStartBUSecond,
		        		showSecond,
		        		disMutibook,
		        	},)
		        } else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },

		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });		        
		    }
		});
	}

	//开始级次变化
	handleStartlvlChange(value) {
		//(value)
		this.setState({
			startlvl: value,
		})
	}
	
	//结束级次变化
	handleEndlvlChange(value) {
		//(value)
		this.setState({
			endlvl: value,
		})
	}
	
	//科目版本下拉变化
	handleVersiondateChange(value) {
		this.setState({
			versiondate: value,
		})
	}
	
	//所有末级复选框变化
	onChangeIsleave(e) {
	    //(e);
	    let isleave = deepClone(this.state.isleave);
	   
	    this.setState({isleave: e});
	}

	//表外科目复选框变化
	onChangeIsoutacc(e) {  
	    this.setState({isoutacc: e});
	}

	//启用科目版本复选框变化
	onChangeIsversiondate(e) {  
	    this.setState({isversiondate: e});
	}


	//未记账凭证复选框变化
	onChangeIncludeuntally(e) {  
	    this.setState({
			includeuntally: e,
            includeerror: e
	    });

	}

	//错误凭证复选框变化
	onChangeIncludeerror(e) {
		this.setState({includeerror: e});
	}

	//损益结转凭证复选框变化
	onChangeIncludeplcf(e) {
		this.setState({includeplcf: e});
	}

	//损益结转凭证复选框变化
	onChangeIncluderc(e) {
		this.setState({includerc: e});
	}

	//多主体显示单选框变化
	handleMutibookChange(e) {
		this.setState({mutibook: e});
	}

	//无发生不显示复选框变化
	onChangeShowzerooccur(e) {
		this.setState({showzerooccur: e});
	}

	//无余额无发生不显示复选框变化
	onChangeShowzerobalanceoccur(e) {
		this.setState({showzerobalanceoccur: e});
	}

	//无发生不显示复选框变化
	onChangeSumbysubjtype(e) {
		this.setState({sumbysubjtype: e});
	}


	//借贷方显示余额复选框变化
	onChangeTwowaybalance(e) {
		this.setState({twowaybalance: e});
	}

	//合并业务单元复选框变化
	onChangeMultbusi(e) {
		let disCurrplusacc = true;
		if (e === true) {
			disCurrplusacc = false;
		} else {
			
		}

		this.setState({
			multbusi: e,
			disCurrplusacc,

		});
	}

	changeCheck=()=> {
		// this.setState({checked:!this.state.checked});
	}

	
	//起始时间变化
	onStartChange(d) {
		//(d)
		this.setState({
			begindate:d
		})
	}
	
	//结束时间变化
	onEndChange(d) {
		this.setState({
			enddate:d
		})
	}

    handleSelect = (value) => {//
        //('handleSelect', value);

        this.setState({
            rightSelectItem: [...value]
        })
    }
    clearTransterData = () => {
        this.setState({
            selectedKeys: [],
            targetKeys: []
        })
    }
    handleChange = (nextTargetKeys, direction, moveKeys) => {
        //('handleChange>>', nextTargetKeys, direction, moveKeys);
        this.setState({ targetKeys: nextTargetKeys });
        this.handleSelect(nextTargetKeys)
    }

    handleSelectChange = (sourceSelectedKeys, targetSelectedKeys) => {
        this.setState({ selectedKeys: [...sourceSelectedKeys, ...targetSelectedKeys] });
        //('sourceSelectedKeys: ', sourceSelectedKeys);
        //('targetSelectedKeys: ', targetSelectedKeys);
    }
    renderModalList = () => {
		let { status, hideBtnArea } = this.props
		let disabled = status === 'browse' ? true : false
        return (
            <div className='right_query'>
                <div className='query_body1'>
                    <div className='query_form threeall'>
                <Row className="myrow">
                    <Col md={2} sm={2}>
                        <span style={{color: 'red'}}>*</span>
                        <span className='nc-theme-form-label-c'>{this.state.json['20023010-000006']}：</span>{/* 国际化处理： 核算账簿*/}
                    </Col>
                    <Col md={7} sm={7}>
                        <div className="book-ref">
                            {/*{mybook}*/}
                            {
                                createReferFn(
                                    this,
                                    {
                                        url: 'uapbd/refer/org/AccountBookTreeRef/index.js',
                                        value: this.state.accountingbook,
                                        referStateKey: 'checkAccountBook',
                                        referStateValue: this.state.checkAccountBook,
                                        stateValueKey: 'accountingbook',
										flag: true,
										disabled: disabled,
                                        queryCondition: {
                                            pk_accountingbook: this.state.accountingbook[0] && this.state.accountingbook[0].refpk
                                        }
                                    },
                                    {
                                        businessUnit: businessUnit
                                    },
                                    'threeAll'
                                )
                            }
                        </div>

                    </Col>
                </Row>
                {
                    this.state.isShowUnit ?
                        <Row className="myrow">
                            <Col md={2} sm={2}>
                                <span className='nc-theme-form-label-c'>{this.state.json['20023010-000007']}：</span>{/* 国际化处理： 业务单元*/}
                            </Col>
                            <Col md={7} sm={7}>
                                <div className="book-ref">
                                    <BusinessUnitTreeRef
										fieldid = 'buSecond'
                                        value={this.state.buSecond}
                                        isMultiSelectedEnabled={true}
                                        isShowDisabledData = {true}
										isHasDisabledData = {true}
										disabled = {disabled}
                                        queryCondition = {{
                                            "pk_accountingbook": this.state.accountingbook[0] && this.state.accountingbook[0].refpk,
                                            "isDataPowerEnable": 'Y',
                                            "DataPowerOperationCode" : 'fi',
                                            "TreeRefActionExt":'nccloud.web.gl.ref.GLBUVersionWithBookRefSqlBuilder'
                                        }}
                                        onChange={(v) => {
                                            //(v);
                                            this.setState({
                                                buSecond: v,
                                            }, () => {
                                                if (this.state.accountingbook && this.state.accountingbook[0]) {
                                                }
                                            })
                                        }
                                        }
                                    />

                                </div>

                            </Col>
                            <Col md={3} sm={3}>
                                <FICheckbox colors="dark" className="mycheck" disabled={disabled} checked={this.state.multbusi}
                                          onChange={this.onChangeMultbusi.bind(this)}>
									{this.state.json['20023010-000008']}	 {/* 国际化处理： 多业务单元合并*/} 
								</FICheckbox>
                            </Col>
                        </Row>: ''
                }

				{/*会计期间*/}
				{
					hideBtnArea ? '' 
					: 
					<DateGroup
						selectionState = {this.state.selectionState}
						// disabled = {disabled}
						start = {this.state.start}
						end = {this.state.end}
						enddate = {this.state.enddate}
						pk_accperiodscheme = {this.state.pk_accperiodscheme}
						pk_accountingbook = {this.state.accountingbook[0]}
						rangeDate = {this.state.rangeDate}
						handleValueChange = {this.handleValueChange}
						handleDateChange = {this.handleDateChange}
						self = {this}
						showRadio = {this.state.showRadio}
					/>
				}
                

                {/*启用科目版本：*/}
                <SubjectVersion
                    checked = {this.state.isversiondate}//复选框是否选中
                    data={this.state.versionDateArr}//
					value={this.state.versiondate}
					CheckboxDisabled = {disabled}
                    disabled={disabled ? disabled : !this.state.isversiondate}
                    handleValueChange = {(key, value) => this.handleValueChange(key, value, 'SubjectVersion')}
                />

                {/*科目，级次：*/}
                <SubjectLevel
					parent = {this}
					disabled = {disabled}
                    accountingbook = {this.state.accountingbook}
                    isMultiSelectedEnabled = {false}//控制多选单选
                    selectedKeys = {this.state.selectedKeys}
                    targetKeys = {this.state.targetKeys}
                    changeParam = {this.state.changeParam}
                    handleSelect = {this.handleSelect}
					handleValueChange = {this.handleValueChange}
					transferSure = {this.transferSure}
					TransferRef = {(init) => {this.TransferTerms = init}}
                    handleSelectChange = {(sourceSelectedKeys, targetSelectedKeys) => handleSelectChange(this, sourceSelectedKeys, targetSelectedKeys)}
                    handleChange = {(nextTargetKeys, direction, moveKeys) => handleChange(this,nextTargetKeys, direction, moveKeys) }
                    clearTransterData = {() => clearTransterData(this)}
                />

                {/*包含凭证*/}
                <CheckBoxCells
                    paramObj = {{
						'disabled': disabled,
                        "noChargeVoucher": {//未记账凭证
                            show: true,
                            checked: this.state.includeuntally,
                            onChange: (value) => this.handleValueChange('includeuntally', value, 'assistAnalyzSearch')
                        },
                        "errorVoucher": {//错误凭证
                            show: true,
                            checked: this.state.includeerror,
                            onChange: (value) => this.handleValueChange('includeerror', value, 'assistAnalyzSearch')
                        },
                        "harmToBenefitVoucher": {//损益结转凭证
                            show: true,
                            checked: this.state.includeplcf,
                            onChange: (value) => this.handleValueChange('includeplcf', value, 'assistAnalyzSearch')
                        },
                        "againClassifyVoucher": {//重分类凭证
                            show: true,
                            checked: this.state.includerc,
                            onChange: (value) => this.handleValueChange('includerc', value, 'assistAnalyzSearch')
                        },
                    }}
                />

                {/*多主体显示方式*/}
                <Row className="myrow">
                    <Col md={2} sm={2}>
						<span className='nc-theme-form-label-c'>{this.state.json['20023010-000003']}</span>{/* 国际化处理： 多主体显示方式*/}
                    </Col>
                    <Col md={9} sm={9}>
                        <Radio.NCRadioGroup
                            name="fruit"
                            selectedValue={this.state.mutibook}
                            onChange={this.handleMutibookChange.bind(this)}
                        >
                            <Radio value="Y" disabled={disabled ? disabled : this.state.disMutibook}>
								{this.state.json['20023010-000009']} {/* 国际化处理： 多主体合并*/}
							</Radio>
                            <Radio value="N" disabled={disabled ? disabled : this.state.disMutibook}>
								{this.state.json['20023010-000010']} {/* 国际化处理： 按主体列示*/}
							</Radio>
                        </Radio.NCRadioGroup>

                    </Col>
                </Row>

                {/*币种*/}
                {renderMoneyType(this.state.currtypeList, this.state.currtypeName, this.handleCurrtypeChange, this.state.json, disabled)}

                {/*返回币种：*/}
                {
                    returnMoneyType(
                        this,
                        {
                            key: 'selectedCurrValue',
                            value: this.state.selectedCurrValue,
                            groupEdit: this.state.groupCurrency,
                            gloableEdit: this.state.globalCurrency
                        },
						this.state.json,
						disabled
                    )
                }
                <Row className="myrow">
					<Col md={2} sm={2}><span className='nc-theme-form-label-c'>{this.state.json['20023010-000011']}：</span></Col>{/* 国际化处理： 显示属性*/}
                    <Col md={10} sm={10} >
                        <Checkbox colors="dark" className="mycheck"    id='showzerobalanceoccur' disabled={disabled} checked={this.state.showzerobalanceoccur}  onChange={this.onChangeShowzerobalanceoccur.bind(this)}>
							{ this.state.json['20023010-000004']} {/* 国际化处理： 无余额无发生不显示*/}
						</Checkbox>
                    </Col>
                </Row>
			</div>
			</div>
			</div>
		)
    }
    clickPlanEve = (value) => {
		let { status } = this.props;
		if (status !== 'browse') {
			this.state = deepClone(value.conditionobj4web.nonpublic);
			this.setState(this.state)
		}
    }
    saveSearchPlan = () => {//点击《保存方案》按钮触发事件
		//('saveSearchPlan>>', this.state)
        return {...this.state}
	}
	render() {
		//('rendeeerr:', this.state)
		let { search, hideBtnArea, status } = this.props;
		let { NCCreateSearch } = search;
		let isHideBtnArea = typeof(hideBtnArea) === 'boolean' && hideBtnArea ? hideBtnArea : false;
		let meta=this.props.meta.getMeta();
		return (
			<div>
                {
					JSON.stringify(meta)!='{}'?
					NCCreateSearch(this.searchId, {
						onlyShowSuperBtn:true,                       // 只显示高级按钮
                        replaceSuperBtn:this.state.json['20023010-000001'],                      // 自定义替换高级按钮名称/* 国际化处理： 查询*/
                        replaceRightBody:this.renderModalList,
                        hideSearchCondition:true,//隐藏《候选条件》只剩下《查询方案》
                        clickPlanEve: this.clickPlanEve ,            // 点击高级面板中的查询方案事件 ,用于业务组为自定义查询区赋值
                        saveSearchPlan:this.saveSearchPlan ,        // 保存查询方案确定按钮事件，用于业务组返回自定义的查询条件
						oid:this.props.meta.oid,
						hideBtnArea: isHideBtnArea,
						isSynInitAdvSearch: true,//渲染右边面板自定义区域
						// showAdvSearchPlanBtn: status==='browse' ? false : true, // 高级面板中是否显示保存方案按钮
						searchBtnName: isHideBtnArea ? this.state.json['20023010-000053'] : this.state.json['20023010-000001'], /* 国际化处理： 确定*//* 国际化处理： 查询*/
						showSearchBtn: status==='browse' ? false : true, // 是否显示查询按钮 ,默认显示
                        clickSearchBtn: () => {
							
							if(status !== 'browse'){
								let data = this.getConfirmData(this.state)
								let url = '/nccloud/gl/accountrep/checkparam.do'
								let flagShowOrHide;
								ajax({
									url,
									data,
									async: false,
									success: function (response) {
										flagShowOrHide = true
									},
									error: function (error) {
										flagShowOrHide = false
										toast({ content: error.message, color: 'warning' });
									}
								})
								if (flagShowOrHide == true) {
									if(hideBtnArea){
										this.props.onConfirm(data, this.state)
									} else {
										this.props.onConfirm(data)
									}
								} else {
									return true;
								}
							} else {
								return 
							}
							
							
						},  // 点击按钮事件
					})
				:<div/>}
			</div>
		)
	}
}

CentralConstructorModal = createPage({})(CentralConstructorModal)

export default CentralConstructorModal
