import React, { Component } from 'react';
import { high,base,ajax,createPage, getMultiLang, gzip } from 'nc-lightapp-front';
const { NCTree:Tree, NCDiv } = base;

const TreeNode = Tree.NCTreeNode;
const keys = ['0-0-0', '0-0-1'];

import './index.less';
import { toast } from '../../../public/components/utils';
import MainSelectModal from './MainSelectModal';
import { SimpleTable } from 'nc-report';

import {tableDefaultData} from '../../../manageReport/defaultTableData';
import reportPrint from '../../../public/components/reportPrint.js';
import reportSaveWidths from '../../../public/common/reportSaveWidths.js';
import {gl_pkreport_threeall} from '../../../public/common/reportPkConst.js';
const { PrintOutput } = high;
import RepPrintModal from '../../../manageReport/common/printModal'
import { mouldOutput } from '../../../public/components/printModal/events'
import { printRequire } from '../../../manageReport/common/printModal/events'
import HeadCom from '../../../manageReport/common/headSearch/headCom';
import {handleValueChange} from '../../../manageReport/common/modules/handleValueChange'

import {searchById} from "../../../manageReport/common/modules/createBtn";
import {setData} from "../../../manageReport/common/simbleTableData";
import {whetherDetail} from "../../../manageReport/common/modules/simpleTableClick";
import PageButtonGroup from '../../../manageReport/common/modules/pageButtonGroup'
import {rowBackgroundColor} from "../../../manageReport/common/htRowBackground";
import HeaderArea from '../../../public/components/HeaderArea';
import Iframe from '../../../public/components/Iframe';
import { queryRelatedAppcode, originAppcode } from '../../../public/common/queryRelatedAppcode.js';
//react-demo 13.html
class Accbalance extends Component {
	constructor(props) {
		super(props);

		//每定义一个state 都要添加备注
		this.state = {
            expandedKeys:"",//更新左侧树的默认展开key
            treeindex:[],//后台返回的下一个查询数据的下标值
            selectNum:"",//1是直接鼠标点击左侧树，2是下一页，3是上一页
		    json: {},
            typeDisabled: true,//账簿格式 默认是禁用的
            showTree: false,//显示左边的树
            showRotate: true,
            accountType: 'amountcolumn',//金额式
			flag:0,//切换表头用的
      		showTable: false,
      		textAlginArr:[],//对其方式
			disabled: true,
            visible: false,
			appcode: this.props.getUrlParam('c') || this.props.getSearchParam('c'),
			currpage:'',
			outputData: {},
			ctemplate: '', //模板输出-模板
			nodekey: '',
			printParams:{}, //查询框参数，打印使用
            showAddBtn: true,//控制联查按钮；true：禁止
			data: {},
			headtitle: [['', ''], ['', ''],['', ''],['', ''],],
			gData: [],/* 国际化处理： 会计科目*/

            firstPageDisable: true,
            lastPageDisable: true,
            codes: [],
            queryDatas: {},//存放回调的查询参数，用于翻页

         	myhandsontableData: {},

         	accountcode: '',
         	settings:{  
	         data : [],
	         mergeCells:[],
	         cell : [],
	         cells:{},
	         rowHeaders: true,
	         manualColumnResize:true, 
	         readOnly:true,
	         columnSorting: true, // 排序功能
	         wordWrap:false,
	         colHeaders:true,
	         autoColumnSize:false,  
	         autoRowSize:false,

	         // minCols:50,
           },
			pager: {},
			MainSelectModalShow: false, //查询模态框
			tableAll: {
				column: [],
				data:[],
			},              //表格全体数据
			initTableAll: null,  //初始查询出的表格全体数据
			tableStyle: {}, //表格样式
			queryCond: null, //已选择的查询条件
			pk_cashflow: '', //已点击行的pk_cashflow
			datas: {
				time: '',
				bookname: '',
				currtypeName: '',
			},
            dataout: tableDefaultData,
            'threeAll': [],
			defaultAccouontBook: {} //默认账簿	
		}
		this.columnInfo = [];
		this.headtitle = [];
		this.balanceVO = [];
		this.pageType = 'nextPage'
        this.dataPage = 0; //存放请求回来的数据椰树信息
        this.handleValueChange = handleValueChange.bind(this);
        this.searchById = searchById.bind(this);
        this.whetherDetail = whetherDetail.bind(this);
        this.rowBackgroundColor = rowBackgroundColor.bind(this);
	}
    componentDidUpdate(){
        // let {selectNum} = this.state;
        // let offSetHei = document.getElementsByClassName("u-tree-node-selected")
        // let all = document.getElementsByClassName("myTree u-tree u-tree-show-line")
        // let allHeight;
        // let bodyheight;
        // let allScrollTop;
        // if(all.length > 0){
        //     allHeight = all[0].offsetHeight
        //     allScrollTop = all[0].scrollTop
        // }
        // if(offSetHei.length > 0){
        //     bodyheight = offSetHei[0].offsetTop-120
        // }
        // if(selectNum == '2'){
        //     if(bodyheight > allHeight){
        //         let distance = bodyheight - allHeight
        //         all[0].scrollTop = distance
        //     }

        // }
        // if(selectNum == '3'){
        //     if(bodyheight < allScrollTop){
        //         all[0].scrollTop = allScrollTop - bodyheight
        //     } 
        // }
        // let {treeindex} = this.state;
        // if(treeindex){
        //     let treeLen = treeindex.length;
        //     let childEle = document.getElementsByClassName('myTree');
        //     if(childEle[0]){
        //         let innerEle = childEle[0].childNodes;//li元素(标签)的个数
        //         if(this.state.selectNum!=1){
        //             let childAele = innerEle[this.state.treeindex[0]];
        //             for (let i = 1; i < this.state.treeindex.length; i++) {
        //                 let chileNodeIndex = this.state.treeindex[i];
        //                 if(childAele.childNodes[2]){
        //                     childAele = childAele.childNodes[2].childNodes[chileNodeIndex];
        //                 }
                       
        //             }
        //             childAele = childAele.getElementsByClassName('u-tree-node-content-wrapper');
        //             childAele[0] && childAele[0].classList.add('u-tree-node-selected');
        //         }
        //     }
        // }
    }
    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json,
                gData: [{key: '', name: this.state.json['20023010-000012']}],/* 国际化处理： 会计科目*/
                'threeAll': [
                    {
                        title: json['20023010-000047'],
                        styleClass:"m-long"
                    },
                    {
                        title: json['20023010-000048'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20023010-000049'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20023010-000050'],
                        styleClass:"m-brief"
                    },

                    {
                        title: json['20023010-000051'],
                        styleClass:"m-brief"
                    }
                ]
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20023010', 'publiccommon'],domainName:'gl',currentLocale:'simpchn',callback});
    }
    componentDidMount() {
        this.setState({
            data: {}
        })
        this.getParam();
        let appceod = this.props.getSearchParam('c');
        this.searchById('20023010PAGE', appceod).then((res) => {
            if (param && res && res.data.context) {
                let { context } =res.data
                this.state.defaultAccouontBook = {refname:context.defaultAccbookName,refpk:context.defaultAccbookPk, getflag: true}
            }
        });
        this.props.button.setDisabled({
            print: true, templateOutput: true,
            linkdetail: true, directprint: true, saveformat: true,
        })
        this.props.button.setButtonVisible(['first', 'pre', 'next', 'last'], false)
    }
    getParam = () => {
        let urlParam;
        // 联查参数，需解压
        let data = this.props.getUrlParam && this.props.getUrlParam('status');
        if (data) {
            urlParam = new gzip().unzip(data)
        }
        // 小友智能查账参数，不需解压
        let param = this.props.getSearchParam('param');
        if (param) {
            urlParam = JSON.parse(param);
        }
        // 查询
        if (urlParam) {
            this.getDatas(urlParam);
        }
    }

	//直接输出
	print = () => {
		let {resourceDatas,dataout,textAlginArr}=this.state;
		let dataArr=[];
		let emptyArr=[];
		let mergeInfo=dataout.data.mergeInfo;
		dataout.data.cells.map((item,index)=>{
			emptyArr=[];
			item.map((list,_index)=>{
				//emptyArr=[];
				if(list){
					// if(list.title){
					//     list.title='';
					// }
					emptyArr.push(list.title);
				}
			})
			dataArr.push(emptyArr);
		})
		//(dataArr);
		reportPrint(mergeInfo,dataArr,textAlginArr);
	}
	//保存列宽
	handleSaveColwidth=()=>{
	    let {json} = this.state;
		let info=this.refs.balanceTable.getReportInfo();
        //('widtsss:', info)
        let callBack = this.refs.balanceTable.resetWidths
		reportSaveWidths(gl_pkreport_threeall,info.colWidths, json, callBack);
	}
	showPrintModal = () => {
        this.setState({
            visible: true
		})
    }
    handlePrint(data, isPreview) {
		let printUrl = '/nccloud/gl/accountrep/triaccbookprint.do'
		let { printParams, appcode,currpage } = this.state
		let { ctemplate, nodekey } = data
		printParams.currpage = currpage
        printParams.queryvo = data
		this.setState({
			ctemplate: ctemplate,
			nodekey: nodekey
        })
        printRequire(this.props,printUrl, appcode, nodekey, ctemplate, printParams, isPreview)
        this.handleCancel()
    }
    handleCancel() {
        this.setState({
            visible: false
        });
	}
	showOutputModal = () => {
		// this.refs.printOutput.open()
		let outputUrl = '/nccloud/gl/accountrep/triaccbookoutput.do'
		let { appcode, nodekey, ctemplate, printParams } = this.state
        mouldOutput(outputUrl, appcode, nodekey, ctemplate, printParams)
    }
    handleOutput() {
        //(this.state.outputData,this.state.json['20023010-000013'])/* 国际化处理： -----模板输出----成功？----*/
    }

	queryShow() {
		this.setState({
			MainSelectModalShow: true,
		})
	}
 //index:用来标记是否获取左边树的数据
//param： 用来标记点击事件的来源
	test(datas, index, param) {
        let { showAddBtn, typeDisabled, firstPageDisable, lastPageDisable,
            codes, gData, disabled, currpage, selectRowIndex, expandedKeys, 
            dataWithPage, subjectTitle, showTree, treeindex } = this.state;
		let self = this;
        let url = '/nccloud/gl/accountrep/triaccquery.do';
		ajax({
		    loading: true,
		    url,
		    data: datas,
		    success: function (res) {
		    	const { data, error, success } = res;
		    	if(success){
		    	    if(self.dataPage === 0){
                        toast({
                            color: 'success'
                        })
                    }
                    self.props.button.setDisabled({
                        print: false, templateOutput: false,
                        directprint: false, saveformat: false,
                    })
                    showAddBtn = false;//解除联查按钮的禁止
                    typeDisabled = false;
		        	if (data) {
                        if (self.dataPage === 0 && self.state.codes.length === 0) {
                            lastPageDisable= false,
                            codes= [...data.accounts];
                            self.props.button.setDisabled({
                                next: false, last: false,
                            })
                        }
                        // 设置科目树数据
                        let TempPage = data.index;
                        if (index != 1) {
                            gData = data.treeData ? data.treeData : [{key: '', name: slef.state.json['20023010-000012']}]/* 国际化处理： 会计科目*/
                        }
                        // 设置应选中树节点的下标
                        if (codes) {
                            for (let i = 0; i < codes.length; i++) {
                                if (codes[i].split(' ')[0] == TempPage) {
                                    self.dataPage = i;
                                    break;
                                }
                            }
                        }
                        // 控制翻页按钮是否可用
                        firstPageDisable = self.dataPage == 0;
                        lastPageDisable = (self.dataPage == codes.length - 1);
                        if(data.data.length>0){
                            disabled=false;
                            currpage= TempPage;
                            selectRowIndex=0;
                        }
                        treeindex=data.treeindex;
                        if(self.state.selectNum!='1' ){
                            if(gData){
                                if (treeindex.length == 1) {
                                    expandedKeys = '';
                                } else {
                                    let treeNode = gData[treeindex[0]];
                                    for (let i = 1; i < treeindex.length - 1; i++) {
                                        treeNode = treeNode.children[treeindex[i]];
                                    }
                                    expandedKeys = treeNode.key;
                                }
                            }
                            self.setState({
                                expandedKeys                                              
                            })
                        }

                        //expandedKeys=data.index

                        if(data.column){
                            self.columnInfo=data[self.state.accountType];
                        }
                        if(data.headtitle){
                            self.headtitle=data.headtitle;
                        }
                        if(data.data){
                            self.balanceVO=data.data;
                        }
                        dataWithPage= data;
                        subjectTitle= data.accountnames;
                        showTree= true;
                        
                        self.setState({
                            showTree, treeindex,subjectTitle,
                            disabled, currpage, selectRowIndex,
                            showAddBtn, typeDisabled, firstPageDisable, lastPageDisable,
                            codes, gData, dataWithPage
                        }, () => {
                            if (treeindex && self.state.selectNum != 1) {
                                let treeLen = treeindex.length;
                                let childEle = document.getElementsByClassName('myTree');
                                let innerEle = childEle[0].childNodes;//li元素(标签)的个数
                                let childAele = innerEle[self.state.treeindex[0]];
                                for (let i = 1; i < self.state.treeindex.length; i++) {
                                    let chileNodeIndex = self.state.treeindex[i];
                                    if (childAele.childNodes[2]) {
                                        childAele = childAele.childNodes[2].childNodes[chileNodeIndex];
                                    }
                                }
                                let selectTwo = document.getElementsByClassName('u-tree-node-selected');
                                if (selectTwo.length > 0) {
                                    selectTwo[0] && selectTwo[0].classList.remove('u-tree-node-selected');
                                }
                                childAele = childAele.getElementsByClassName('u-tree-node-content-wrapper');
                                childAele[0] && childAele[0].classList.add('u-tree-node-selected');
                                let docPosition = childAele[0].offsetTop - 158 //选中文本位置
                                childEle[0].scrollTop = docPosition  // 翻页时滚动条自动滑动
                            }
                        })
                        
                        setData(self, data, true);
		        	}
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});
	}
    onExpand = (info) =>{
        if(info.length>0){
            this.setState({
                expandedKeys:info[info.length-1]
            })
        }else{
            this.setState({
                expandedKeys:""
            })
        }

    }
	onSelect = (info, param) =>  {
        this.setState({
            selectNum:"1"
        })
        let childAele = document.getElementsByClassName('u-tree-node-selected');
        if(childAele.length>0){//清除掉之前的选中状态
            for(let i = 0;i < childAele.length; i++){
                let infoFlag = childAele[i].childNodes[0].childNodes[0].data
                            && childAele[i].childNodes[0].childNodes[0].data.split(" ")[0];
                if(info != infoFlag){
                    childAele[i].classList.remove("u-tree-node-selected")
                }
            }          
        }       
        if (info.length == 0 || info[0] == "0-0") {
            return
        }
        for(let index=0; index<this.state.gData.length; index++){
            if(this.state.gData[index].key === info[0]){
                if(index > 0){
                    this.setState({
                        firstPageDisable: false,
                        selectRowIndex: 0
                    })
                    this.props.button.setDisabled({
                        first: false, pre: false,
                    })
                }else{
                    this.setState({
                        firstPageDisable: true,
                        selectRowIndex: 0
                    })
                    this.props.button.setDisabled({
                        first: true, pre: true
                    })
                }
                this.dataPage = index;
                break;
            }
        }

		
		let datas = this.state.queryDatas;
        datas.accountcode = info;
        // 使用accountcode查询，清空页码和操作
        delete datas.pageindex;
        delete datas.operation;
        this.props.button.setDisabled({
            linkdetail: true
        });
		this.test(datas, 1, param);
		this.setState({
			currpage:info[0]
		})
	}


	onCheck = (checkedKeys) => {
		let self = this;
		const cks = {
			checked: checkedKeys.checked || checkedKeys,
		};
	}

	/**
     * param: true 点击刷新
     * */
	getDatas = (datas, param) => {
        // 清空页码和操作
        delete datas.pageindex;
        datas.operation="nextPage";
        this.dataPage = 0;
        if(param){
            this.state.queryDatas.accountcode = []
        }

        this.pageType = 'nextPage';
        this.state.codes = [];
        this.props.button.setDisabled({
            first: true, pre: true
        })
        this.setState({
            queryDatas:datas,
            printParams:datas,
            firstPageDisable: true,
            showTree: false,
            expandedKeys: '',
            selectNum: ''
        }, () => this.test(datas))


	}

    getDetailPort(paramObj){//联查"明细"
        let url = paramObj.url; //'/nccloud/gl/accountrep/triaccquery.do';
        let data = this.getDataType(paramObj);
        if(Object.keys(data.link).length > 0){
            this.jumpToDetail(data, paramObj);
        }else{
            toast({content: this.state.json['20023010-000014'], color: 'warning'})/* 国际化处理： 此行不支持联查明细！*/
        }
    }
    getDataType = (param) => {
        let selectRow = this.getSelectRowData()[0].link;

        let result = '';
        if(param.key === 'detail'){
            result = {
                origparam: {...this.state.queryDatas},
                link: {...selectRow},
                "class": "nccloud.pubimpl.gl.account.TriaccbooksLinkTridetailParamTransfer"
            }
        }else if(param.key === 'totalAccount'){
            result = {
                ...this.state.queryDatas,
                link: {...selectRow},
                "class": "nccloud.pubimpl.gl.account.AccbalLinkTriaccbookParamTransfer"
            }
        }else{//联查"辅助"
            selectRow['key'] = 'relevanceAssist';
            result = {
                ...this.state.queryDatas,
                link: {...selectRow},
                "pk_accassitems": [...this.state.selectRow],
                "class": "nccloud.pubimpl.gl.account.AccbalLinkAssbalParamTransfer",
                "from": "accbal" // 联查来源：(多主体)科目余额表accbal
            }
        }
        return result;
    }
    jumpToDetail = (data, paramObj) => {
        let gziptools = new gzip();
        this.props.openTo(
            paramObj.jumpTo,
            {appcode: paramObj.appcode, status: gziptools.zip(JSON.stringify(data)),id: '12wew24'}
        )
    }
    //获取handsonTable当前选中行数据
    getSelectRowData=()=>{
        let selectRowData=this.refs.balanceTable.getRowRecord();
        return selectRowData;
    }
    /**
     * param: 'firstPage, nextPage, prePage, lastPage;
     * dataWithPage: 总数据
     * paramObj
     * */
    handlePage = (param, dataWithPage) => {//页面控制事件
        let {queryDatas, codes, treeindex} = this.state;
        let dataLength = codes.length;

        let childEle = document.getElementsByClassName('myTree');
        if(childEle.length>0){
            let innerEle = childEle[0].childNodes;//li元素(标签)的个数
            let childAele = document.getElementsByClassName('u-tree-node-selected');
            if(childAele[0]){
                for(let i=0;i<childAele.length;i++){
                    childAele[i] && childAele[i].classList.remove('u-tree-node-selected');

                }
            }
            
            
        }
            let renderData = [];
            let obj = {};
            let stringPage = '1';
            queryDatas.operation = param;// 翻页操作
            switch(param){
                case 'firstPage':
                    this.dataPage = 0;
                    queryDatas.pageindex = this.dataPage.toString();

                    this.setState({
                        queryDatas,
                        firstPageDisable: true,
                        lastPageDisable: false,
                        selectRowIndex: 0,
                        selectNum: ''
                    });
                    this.props.button.setDisabled({
                        first: true, pre: true,
                        next: false, last: false,
                    })
                    //('firstPage:::', this.dataPage, obj, this.state.paramObj);
                    break;
                case 'nextPage':
                    this.dataPage += 1;
                    queryDatas.pageindex = (this.dataPage).toString();

                    this.setState({
                        queryDatas,
                        firstPageDisable: false,//可以点击
                        selectRowIndex: 0,
                        selectNum:"2"
                    })
                    this.props.button.setDisabled({
                        first: false, pre: false,
                    })
                    if(this.dataPage === dataLength-1){
                        this.setState({
                            lastPageDisable: true
                        })
                        this.props.button.setDisabled({
                            next: true, last: true,
                        })
                    }
                    break;
                case 'prePage':

                    if (this.dataPage === 0) {                      
                        let childEle = document.getElementsByClassName('myTree');
                        let innerEle = childEle[0].childNodes;//li元素(标签)的个数
                        let childAele = innerEle[this.dataPage].getElementsByClassName('u-tree-node-content-wrapper');
                        childAele[0] && childAele[0].classList.add('u-tree-node-selected');
                        return;
                    }
                    this.dataPage -=1;
                    queryDatas.pageindex = this.dataPage.toString();

                    if(this.dataPage === dataLength-2){
                        this.setState({
                            lastPageDisable: false
                        })
                        this.props.button.setDisabled({
                            next: false, last: false,
                        })
                    }
                    if(this.dataPage === 0){
                        this.setState({
                            firstPageDisable: true
                        })
                        this.props.button.setDisabled({
                            first: true, pre: true
                        })
                    }
                    this.setState({
                        queryDatas,
                        selectRowIndex: 0,
                        selectNum:"3"
                    })
                    break;
                case 'lastPage':
                    this.dataPage = dataLength-1;
                    queryDatas.pageindex = this.dataPage.toString();

                    this.setState({
                        queryDatas,
                        lastPageDisable: true,
                        firstPageDisable: false,
                        selectRowIndex: 0,
                        selectNum: ''
                    });
                    this.props.button.setDisabled({
                        first: false, pre: false,
                        next: true, last: true,
                    })
                    //('lastPage>>>>>', this.dataPage)
                    break;
            }
            this.test(this.state.queryDatas, 1);
            return;
        //}
    }
    handleLoginBtn = (obj, btnName) => {
        if(btnName === 'print'){//打印
            this.showPrintModal()
        }else if(btnName === 'templateOutput'){//模板输出
            this.showOutputModal()
        }else if(btnName === 'linkdetail'){//联查明细
            this.getDetailPort({
                url: '/nccloud/gl/accountrep/triaccquery.do',
                jumpTo: '/gl/threedetail/pages/main/index.html',
                key: 'detail',
                appcode: queryRelatedAppcode(originAppcode.detailbook)
            })
        }else if(btnName === 'first'){//首页
            this.pageType = 'nextPage';
            this.handlePage('firstPage', this.state.dataWithPage)
        }else if(btnName === 'pre'){//上一页
            this.pageType = 'prePage';
            this.handlePage('prePage', this.state.dataWithPage)
        }else if(btnName === 'next'){//下一页
            this.pageType = 'nextPage';
            this.handlePage('nextPage', this.state.dataWithPage)
        }else if(btnName === 'last'){//末页
            this.pageType = 'prePage';
            this.handlePage('lastPage', this.state.dataWithPage)
        }else if(btnName === 'directprint'){//直接输出
            this.print()
        }else if(btnName === 'saveformat'){//保存列宽
            this.handleSaveColwidth()
        }else if(btnName === 'refresh'){
            if(Object.keys(this.state.queryDatas).length>0){
                this.getDatas(this.state.queryDatas, true);
            }
        }
    }

    firstCallback = () => {//首页
        this.pageType = 'nextPage';
        this.handlePage('firstPage', this.state.dataWithPage)
    }
    preCallBack = () => {//上一页
        this.pageType = 'prePage';
        this.handlePage('prePage', this.state.dataWithPage)
    }
    nextCallBack = () => {//下一页
        this.pageType = 'nextPage';
        this.handlePage('nextPage', this.state.dataWithPage)
    }
    lastCallBack = () => {//末页
        this.pageType = 'prePage';
        this.handlePage('lastPage', this.state.dataWithPage)
    }

	render() {
        let {expandedKeys} = this.state
        let { modal, DragWidthCom } = this.props;
        const { createModal } = modal;
		const loop = data => data.map((item) => {
			if (item.children && item.children.length) {
			    return <TreeNode liAttr={{"fieldid": `${item.key}_node`}} key={item.key} title={item.name}>
                    {loop(item.children)}
                </TreeNode>;
			}
			return <TreeNode liAttr={{"fieldid":` ${item.key}_node`}} key={item.key} title={item.name} />;

		});
        let leftDom = <div className="tree-area">{/* 左树区域 tree-area*/}
            {
                this.state.showTree?
                        <NCDiv fieldid="threeall" areaCode={NCDiv.config.TreeCom} className='left-area'>
                        <Tree
                            ref='treeNodeRef'
                            className="myTree"
                            showLine
                            checkStrictly
                            onSelect={ (info) => this.onSelect(info, 'onSelect')}
                            style={{width: '100%'}}
                            expandedKeys={expandedKeys}
                            onExpand={(info) => this.onExpand(info)}
                        // defaultExpandAll={true}
                        >
                            {loop(this.state.gData)}
                        </Tree>
                    </NCDiv>
                    :
                <div className='myTreenone nc-theme-l-area-bgc'>
                    <div style={{color: '#bfbfbf'}}>
                        <i style={{fontSize: '50px'}} className='uf icon-tip uf-exc-c-o'></i>
                        <p style={{fontSize: '13px'}}>{this.state.json['20023010-000052']}</p>
                    </div>
                </div>
            }
        </div>
        let rightDom =  <div className="table-area">
            <SimpleTable
                ref="balanceTable"
                data = {this.state.dataout}
                onCellMouseDown = {(e, coords, td) => {
                    //('recorddd:::', this.refs.balanceTable.getRowRecord());
                    this.whetherDetail('link');
                    this.rowBackgroundColor(e, coords, td)
                }}
            />
        </div>
       
		return (
			<div id="cashQuery" className='nc-bill-tree-table three-report'>
                <HeaderArea 
                    title = {this.state.json['20023010-000015']} /* 国际化处理： 三栏式总账*/
                    btnContent = {
                        <div>
                            <div className='account-query-btn'>
                                <MainSelectModal
                                    defaultAccouontBook={this.state.defaultAccouontBook}
                                    onConfirm={(datas) => {
                                        this.getDatas(datas);
                                    }}
                                />
                            </div>
                            {//按钮集合
                                this.props.button.createButtonApp({
                                    area: 'btnarea',
                                    buttonLimit: 3,
                                    onButtonClick: (obj, tbnName) => this.handleLoginBtn(obj, tbnName),
                                })
                            }
                        </div>
                    }
                    pageBtnContent = {
                        <PageButtonGroup
                            first={{disabled: this.state.firstPageDisable, callBack: this.firstCallback}}
                            pre={{disabled: this.state.firstPageDisable, callBack: this.preCallBack}}
                            next={{disabled: this.state.lastPageDisable, callBack: this.nextCallBack}}
                            last={{disabled: this.state.lastPageDisable, callBack: this.lastCallBack}}
                        />
                    }
                />
                <NCDiv areaCode={NCDiv.config.SEARCH}>
                    <div className="searchContainer nc-theme-gray-area-bgc">
                        {
                            this.state.threeAll.map((items) => {
                                return(
                                    <HeadCom
                                        labels={items.title}
                                        key={items.title}
                                        threeallSubject={this.state.subjectTitle}
                                        lastThre = {this.headtitle && this.headtitle}
                                        changeSelectStyle={
                                            (key, value) => this.handleValueChange(key, value, 'assistAnalyzSearch', () => setData(this, this.state.dataWithPage))
                                        }
                                        accountType = {this.state.accountType}
                                        isLong = {items.styleClass}
                                        typeDisabled = {this.state.typeDisabled}
                                    />
                                )
                            })
                        }
                    </div>
                </NCDiv>
                <div className='tree-table-container'>
                    <div className="tree-table" style={{height: '100%'}}>
                        <DragWidthCom
                            onDragStop={()=>{
                                this.refs.balanceTable.updateSettings()
                            }}
                            leftDom={leftDom}     //左侧区域dom
                            rightDom={rightDom}     //右侧区域dom
                            defLeftWid='280px'      // 默认左侧区域宽度，px/百分百
                        />
                    </div>
                </div>
                
				<RepPrintModal
                    noCheckBox={true}
                    showScopeall={true}
                    appcode={this.state.appcode}
                    visible={this.state.visible}
                    handlePrint={this.handlePrint.bind(this)}
                    handleCancel={this.handleCancel.bind(this)}
                />
				<PrintOutput                    
                    ref='printOutput'
                    url='/nccloud/gl/accountrep/triaccbookoutput.do'
                    data={this.state.outputData}
                    callback={this.handleOutput.bind(this)}
                />
                {createModal('printService', {
                    className: 'print-service'
                })}
                <Iframe />
			</div>
		)
	}
}
Accbalance = createPage({})(Accbalance)
export default Accbalance
