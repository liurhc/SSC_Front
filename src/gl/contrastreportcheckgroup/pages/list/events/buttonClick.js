import { base, ajax, toast,cacheTools } from 'nc-lightapp-front';
import clickSearchBtn from './searchBtnClick';
export default function buttonClick(props, id) {
    switch (id) {
        case 'refresh':
            let searchVal = cacheTools.get(this.searchId);
            let self = this;
            clickSearchButton(props,searchVal,'refresh',self);
        break;
    }
}
function clickSearchButton(props,searchVal,flag,self) {
    if(searchVal){
        let pageId = '20022030_web_list';
        let searchId = '20022030_query';
        let tableId = '20022030_list';
        let data={
            querycondition: searchVal,
            pagecode:pageId,
            querytype:'tree',
        };
        cacheTools.set(searchId, searchVal);
        //this.state.querydata=data;
        ajax({
            url: '/nccloud/gl/contrast/contrastreportcheckquery.do',
            data: data,
            success: (res) => {
                let { success, data } = res;
                if (success) {
                    if(data){
                        if(flag=='refresh')
                        {
                            toast({ color: 'success', title:self.state.json['20022030-000007']});/* 国际化处理：刷新成功！*/
                        }
                        props.table.setAllTableData(tableId, data[tableId]);
                    }else{
                        if(flag=='refresh')
                        {
                            toast({ color: 'success', title:self.state.json['20022030-000007']});/* 国际化处理：刷新成功！*/
                        }
                        props.table.setAllTableData(tableId, {rows:[]});
                    }
                    
                }
            }
        });
    }
}
