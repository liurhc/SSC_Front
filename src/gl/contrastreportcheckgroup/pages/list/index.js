//主子表列表

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base ,getMultiLang,createPageIcon} from 'nc-lightapp-front';
const { NCTabsControl,NCAffix} = base;
import { buttonClick, initTemplate, searchBtnClick,onAfterEvent } from './events';
import HeaderArea from '../../../public/components/HeaderArea';
import './index.less';
import '../../../public/reportcss/firstpage.less';
class List extends Component {
	constructor(props) {
		super(props);
		this.moduleId = '2002';
		this.searchId = '20022030_query';
		this.tableId = '20022030_list';
		this.state = {
			querydata : {},
			json:{},
			inlt:null
		};
		this.nodetype = props.getUrlParam('nodetype');
	}

	componentDidMount() {
	}
	componentWillMount(){
		let callback= (json,status,inlt) =>{
			this.setState({json:json,inlt},()=>{
				initTemplate.call(this, this.props);
			})
		}
        getMultiLang({moduleId:'20022018',domainName:'gl',currentLocale:'zh-CN',callback});
	}
	//按钮控制
	onRowClick = (props,id,record,index)=>{
		if(record['checkstatus'].value==false)
		{
			props.button.setButtonDisabled(['audit'], false);
			props.button.setButtonDisabled(['unaudit'], true);
		}
		else
		{
			props.button.setButtonDisabled(['unaudit'], false);
			props.button.setButtonDisabled(['audit'], true);
		}
	};

	onRowDoubleClick = (record,index)=>{
		this.props.linkTo('/gl/contrastreportcheckgroup/pages/card/index.html', {
			pagecode: '20022030_web_card',
			status: 'browse',
			id: record.pk_contrastreportcreate.value
		})
	};

	render() {
		let { table, button, search } = this.props;
		let buttons = this.props.button.getButtons();
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { createButton, getButtons } = button;
		return (
			<div className="nc-bill-list" id="nc-bill">
				<HeaderArea
					title={this.props.getSearchParam('n')}
					btnContent={this.props.button.createButtonApp({
						area: 'contrastreportcheck_list',
						buttonLimit: 4,
						onButtonClick: buttonClick.bind(this),
						popContainer: document.querySelector('.header-button-area')
					})}
				/>
				<div className="nc-bill-search-area">
					{NCCreateSearch(this.searchId, {
						clickSearchBtn: searchBtnClick.bind(this),
						defaultConditionsNum: 6, //默认显示几个查询条件
						onAfterEvent: onAfterEvent.bind(this)
					})}
				</div>
				
				<div className="nc-bill-table-area">
					{createSimpleTable(this.tableId, {
						fieldid: 'reportcheckgroup',
						//showCheck: true,
						showIndex: true,
						onRowDoubleClick : this.onRowDoubleClick,
						onRowClick : this.onRowClick
					})}
				</div>
			</div>
		);
	}
}

List = createPage({
	//mutiLangCode: '2002'
})(List);

ReactDOM.render(<List />, document.querySelector('#app'));
