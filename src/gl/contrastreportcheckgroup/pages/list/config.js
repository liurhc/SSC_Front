import { ajax, toast ,base,cardCache,getMultiLang} from 'nc-lightapp-front';

export const ISAUDIT = {
    AUDIT : true,//未启用
    UNAUDIT : false,//已启用
}

export const moduleId = '2002';

export const searchId = '20022030_query';

export const tableId = '20022030_list';