//主子表卡片
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast,getMultiLang,createPageIcon} from 'nc-lightapp-front';
let { NCFormControl,NCAffix } = base;
import { buttonClick, initTemplate,setSelectedData} from './events';
import SeachModal from './searchModal';
import HeaderArea from '../../../public/components/HeaderArea';
import '../../../public/reportcss/firstpage.less';
import './index.less'

class Card extends Component {
	constructor(props) {
		super(props);
		this.state={
			isCardPaginationShow: false,  //控制分页是否显示
			seachModalShow:false,
			selectedData:{},
			json:{},
			inlt:null

		};
		this.defaultAccbook = {};
		this.formId = 'head';
		this.moduleId = '2002';
		this.tableId = 'body';
		this.pageId = '20022015_LIST';
	}
	
	componentWillReceiveProps(nextProps){
	}

	componentDidMount() {
		this.pageButtonVisible();
	}  
	componentWillMount() {
        let callback= (json,status,inlt) =>{
            this.setState({json:json,inlt},()=>{
				initTemplate.call(this,this.props);
            })
        }
        getMultiLang({moduleId:'20022015',domainName:'gl',currentLocale:'zh-CN',callback});
    }
	//设置按钮可用性
	pageButtonVisible = () => {
		this.props.button.setButtonDisabled(['refresh','make'], true);
	};

  setsearchModalShow=(data)=>{
	  this.setState({
		seachModalShow:data
	  })
  }
	
	render() {
		let { cardTable, form, button, modal, cardPagination } = this.props;
		let buttons = this.props.button.getButtons();
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		let { createForm } = form;
		let { createCardTable } = cardTable;
		const {createCardPagination} = cardPagination;
		let { createButton } = button;
		let { createModal } = modal;
		let { isCardPaginationShow,seachModalShow } = this.state;
		return (
			<div className="nc-bill-list nc-bill-card" id="billcard">
			<div className="nc-bill-top-area">
					{/* 因为isCardPaginationShow是false，不会显示翻页按钮。所以HeaderArea里没包含翻页按钮。 */}
					<HeaderArea
						title={this.state.json['20022015-000005']}/* 国际化处理： 内部交易汇总对账*/
						btnContent={this.props.button.createButtonApp({
							area: 'head',
							buttonLimit: 3,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					/>
				
				<div className="nc-bill-form-area">
					{createForm(this.formId, {
						fieldid: 'SumContrast'
					})}
				</div>
				</div>
				{
					<div className="nc-bill-table-area">
						{createCardTable(this.tableId, {
                        adaptionHeight:true,
							fieldid: 'SumContrast',
							showCheck: true,
							showIndex: true,
							onRowClick: setSelectedData.bind(this),
							hideSwitch:()=>{return false}
						})}
					</div>
				}
				
				<SeachModal
					defaultAccbook = {this.defaultAccbook}
					seachModalShow={seachModalShow}
					onClose={() => {
						this.setState({
							seachModalShow: false
						})
					}}
					props={this.props}
					setsearchModalShow={this.setsearchModalShow.bind(this)}
					selectedData={this.state.selectedData}
				/>
			</div>
		);
	}
}

Card = createPage({
     	//initTemplate: initTemplate,
     	//mutiLangCode: '2052'
     })(Card);

ReactDOM.render(<Card />, document.querySelector('#app'));
