import React, { Component } from 'react';
import {high,base,ajax,getBusinessInfo,cacheTools ,getMultiLang } from 'nc-lightapp-front';
const { NCModal:Modal, NCButton:Button,NCRow,NCCol ,NCDatePicker,NCCheckbox,NCTable:Table,
    NCSelect:Select,NCRadio,NCDiv} = base;
import createScript from '../../../../public/components/uapRefer.js';
import ContrastRuleGridRef from '../../../../refer/voucher/ContrastRuleGridRef/index.js';
import CurrtypeGridRef from '../../../../../uapbd/refer/pubinfo/CurrtypeGridRef/index';
import { toast } from '../../../../public/components/utils.js';
import Store from "../../../../public/components/GlobalStore.js";
import ReferLoader from "../../../../public/ReferLoader/index.js";
import './index.less';
export default class SearchModal extends Component {
    constructor(props) {
        super(props);
        let {defaultAccbook} = props;
        this.state = {
            selectedData:{},
            showModal: false,
            pk_rule:{
                refname:'',
                refpk:''
            },
            pk_currtype:{
                refname:'',
                refpk:''
            },
            pk_selfaccountingbook:{

                refname:defaultAccbook.refname,
                refpk:defaultAccbook.refpk,
            },
            pk_otheraccountingbook:[],
            searchData:{
                pk_rule:'',//对账规则主键
                rulename:'',//对账规则名
                ruledirection:true,//规则方向 正向true 反向false
                pk_currtype:'', //币种
                pk_selfaccountingbook:'',//（本方核算账簿：只能单选，有默认值）
                pk_otheraccountingbook:[],//（对方核算账簿：可以复选，无默认值）
                assvo:'',//辅助核算选择： 用户不选择就是’’ 
                amount:false,//对账种类：金额选择
                amountdisable:true,//金额控件的不可用性，true不可用，false可用
                quantity:false,//数量选择  
                quantitydisable:true,//数量控件的不可用性，true不可用，false可用
                begindate:'',//日期：开始日期
                enddate:'',//结束日期  
                range:'1',//对符范围：1未对符2已对符3全部
                contraststyle:'1', //对账方式：1借贷 2贷借3净发生
                date:"Y",//（默认是true）
            },
            json:{},
            inlt:null
        };
        this.close = this.close.bind(this);
        this.open = this.open.bind(this);
    }

    componentDidMount(){
    }

    componentWillMount() {
        let callback= (json,status,inlt) =>{
            this.setState({json:json,inlt},()=>{
            })
        }
        getMultiLang({moduleId:'20022015',domainName:'gl',currentLocale:'zh-CN',callback});
    }

    componentWillReceiveProps(nextProps){
        let { searchData,pk_rule,pk_currtype,pk_selfaccountingbook,pk_otheraccountingbook }=this.state
        let appUrlLocal = decodeURIComponent(window.location.href);
        let jumpflag =false;
        if(appUrlLocal.indexOf("jumpflag=true") != -1){
            jumpflag=true;
        }
        //跳转回来，给查询面板参照赋值
        if(jumpflag==true)
        {
            if(cacheTools.get('sumqueryvo')){
               
                searchData=cacheTools.get('sumqueryvo');
            }
            if(cacheTools.get('sumpk_rule')){
                pk_rule = cacheTools.get('sumpk_rule');
            }
            if(cacheTools.get('sumpk_currtype')){
                pk_currtype = cacheTools.get('sumpk_currtype');
            }
            if(cacheTools.get('sumaccountingbookpk')){
                pk_selfaccountingbook.refpk = cacheTools.get('sumaccountingbookpk');
                pk_selfaccountingbook.refname = cacheTools.get('sumaccountingbookname');
            }
            if(cacheTools.get('sumotheraccountingbook')){
                pk_otheraccountingbook = cacheTools.get('sumotheraccountingbook');
            }
        }
        let showModal=nextProps.seachModalShow;
        let selectedData=nextProps.selectedData
        this.setState({
            showModal,
            searchData,
            selectedData,
            pk_rule,
            pk_currtype,
            pk_selfaccountingbook,
            pk_otheraccountingbook
        })
    }

    close() {
        this.props.onClose();
       }

    handleConfirm=()=>{
        let localProps=this.props;
        let {searchData,selectedData} = this.state;
        if(searchData.pk_rule=='' ){
            toast({content:this.state.json['20022015-000032'] ,color:'warning'});   /* 国际化处理： 对账规则不能为空，请知悉*/

        }else if(searchData.pk_rule!='' && searchData.pk_selfaccountingbook==''){   /* 国际化处理:  本方核算账簿不能为空，请知悉*/
            toast({content:this.state.json['20022015-000033'] ,color:'warning'});
        }else if(searchData.pk_rule!='' && searchData.pk_selfaccountingbook!=''&&searchData.pk_otheraccountingbook.length==0){
            toast({content:this.state.json['20022015-000034'] ,color:'warning'});    /* 国际化处理:  对方核算账簿不能为空，请知悉*/
        }
        else{
            cacheTools.set('sumqueryvo',searchData);
            this.setState({
                showModal: false
            });
            this.props.setsearchModalShow(false);
            this.getInitData(searchData);
        }
       
    }

    getInitData=(searchData)=>{//获取全部数据
        let self=this;
        let localProps = this.props.props;
        //设置按钮的可用性
        let content = cacheTools.get('summain');
        if(content=='N')
        {
            localProps.button.setButtonDisabled(['refresh','make'], false);
        }
        else
        {
            localProps.button.setButtonDisabled('refresh', false);
            localProps.button.setButtonDisabled('make',true);
        }
        ajax({
            url: '/nccloud/gl/sumcontrast/show.do',
            data: searchData,
            success: (res) => { 
             if(res.data)
              {
              //设置表头数据
              localProps.form.setFormItemsValue('head',{'contrastRule':{value:res.data.contrastRule,display:null}});
              localProps.form.setFormItemsValue('head',{'ass':{value:res.data.ass,display:null}});
              localProps.form.setFormItemsValue('head',{'selfAccount':{value:res.data.selfAccount,display:null}});
              localProps.form.setFormItemsValue('head',{'otherAccount':{value:res.data.otherAccount,display:null}});
              localProps.form.setFormItemsValue('head',{'contrastWay':{value:res.data.contrastWay,display:null}});
              localProps.form.setFormItemsValue('head',{'dateArea':{value:res.data.dateArea,display:null}});
              localProps.form.setFormItemsValue('head',{'contrastArea':{value:res.data.contrastArea,display:null}});
            }
            }
        })
        ajax({
        url: '/nccloud/gl/sumcontrast/query.do',
        data: searchData,
        success: (res) => { 
            if(res.data)
            {
                if (res.data.body) {
                    localProps.cardTable.setTableData('body', res.data.body);
                    let len = res.data.body.rows.length;
                    toast({ color: 'success', content: self.state.inlt&&self.state.inlt.get('20022015-000006',{count:len}) });/* 国际化处理： 查询成功，共,条。*/
                    let content = cacheTools.get('sumcontent');
                    if(searchData.contraststyle==1){
                        //借贷                    
                        self.setDCContrast(content);
                    }else if(searchData.contraststyle==2){
                        //贷借
                        self.setCDContrast(content);
                    }else if(searchData.contraststyle==3){
                        //净发生
                        self.setBalanceContrast(content);
                    }
                }
            }
            else
            {
                toast({ color: 'warning', content: self.state.json['20022015-000008'] });/* 国际化处理： 未查询出符合条件的数据！*/
                localProps.cardTable.setTableData('body', {rows:[]});
            }
        }
        })
    }

  //借贷
  setDCContrast = (content) => {
    let index = new Array();
	let tableid = 'body';
	let localPropS=this.props.props
	if(content[0]==='N')
	{
		localPropS.cardTable.hideColByKey(tableid,['self_debitquantity','other_creditquantity','quantityequal','quantitybalance']);
    }
    else
    {
        localPropS.cardTable.showColByKey(tableid,['self_debitquantity','other_creditquantity','quantityequal','quantitybalance']);
        let col0 = localPropS.cardTable.getColValue('body','quantityequal');
        for (let i=0;i<col0.length;i++)
        {
            if(col0[i].value==true)
            {
                index.push(i)
            }
        }
    }
	if(content[1]==='N')
	{
		localPropS.cardTable.hideColByKey(tableid,['self_debitamount','other_creditamount','amountequal','amountbalance']);
    }
    else
    {
        localPropS.cardTable.showColByKey(tableid,['self_debitamount','other_creditamount','amountequal','amountbalance']);
        let col1 = localPropS.cardTable.getColValue('body','amountequal');
        for (let i=0;i<col1.length;i++)
        {
            if(col1[i].value==true)
            {
                index.push(i)
            }
        }
    }
	if(content[2]==='N')
	{
		localPropS.cardTable.hideColByKey(tableid,['self_debitorgamount','other_creditorgamount','orgamountequal','orgamountbalance']);
    }
    else
    {
        localPropS.cardTable.showColByKey(tableid,['self_debitorgamount','other_creditorgamount','orgamountequal','orgamountbalance']);
        let col2 = localPropS.cardTable.getColValue('body','orgamountequal');
        for (let i=0;i<col2.length;i++)
        {
            if(col2[i].value==true)
            {
                index.push(i)
            }
        }
    }
	if(content[3]==='N')
	{
		localPropS.cardTable.hideColByKey(tableid,['self_debitgroupamount','other_creditgroupamount','groupamountequal','groupamountbalance']);
    }
    else
    {
        localPropS.cardTable.showColByKey(tableid,['self_debitgroupamount','other_creditgroupamount','groupamountequal','groupamountbalance']);
        let col3 = localPropS.cardTable.getColValue('body','groupamountequal');
        for (let i=0;i<col3.length;i++)
        {
            if(col3[i].value==true)
            {
                index.push(i)
            }
        }
    }
	if(content[4]==='N')
	{
		localPropS.cardTable.hideColByKey(tableid,['self_debitglobalamount','other_creditglobalamount','globalamountequal','globalamountbalance']);
    }
    else
    {
        localPropS.cardTable.showColByKey(tableid,['self_debitglobalamount','other_creditglobalamount','globalamountequal','globalamountbalance']);
        let col4 = localPropS.cardTable.getColValue('body','globalamountequal');
        for (let i=0;i<col4.length;i++)
        {
            if(col4[i].value==true)
            {
                index.push(i)
            }
        }
    }
    localPropS.cardTable.hideColByKey(tableid,
    ['self_creditquantity','self_creditamount','self_creditorgamount','self_creditgroupamount','self_creditglobalamount',
    'other_debitquantity','other_debitamount','other_debitorgamount','other_debitgroupamount','other_debitglobalamount',
    'self_quantityoccur','self_amountoccur','self_orgamountoccur','self_groupamountoccur','self_globalamountoccur',
    'other_quantityoccur','other_amountoccur','other_orgamountoccur','other_groupamountoccur','other_globalamountoccur']);
    //设置选中行
    localPropS.cardTable.selectRowsByIndex('body', index);
  }

  //贷借
  setCDContrast = (content) => {
    let index = new Array();
	let tableid = 'body'
    let localPropS=this.props.props
	if(content[0]==='N')
	{
		localPropS.cardTable.hideColByKey(tableid,['self_creditquantity','other_debitquantity','quantityequal','quantitybalance']);
    }
    else
    {
        localPropS.cardTable.showColByKey(tableid,['self_creditquantity','other_debitquantity','quantityequal','quantitybalance']);
        let col0 = localPropS.cardTable.getColValue('body','quantityequal');
        for (let i=0;i<col0.length;i++)
        {
            if(col0[i].value==true)
            {
                index.push(i)
            }
        }
    }
	if(content[1]==='N')
	{
		localPropS.cardTable.hideColByKey(tableid,['self_creditamount','other_debitamount','amountequal','amountbalance']);
    }
    else
    {
        localPropS.cardTable.showColByKey(tableid,['self_creditamount','other_debitamount','amountequal','amountbalance']);
        let col1 = localPropS.cardTable.getColValue('body','amountequal');
        for (let i=0;i<col1.length;i++)
        {
            if(col1[i].value==true)
            {
                index.push(i)
            }
        } 
    }
	if(content[2]==='N')
	{
		localPropS.cardTable.hideColByKey(tableid,['self_creditorgamount','other_debitorgamount','orgamountequal','orgamountbalance']);
    }
    else
    {
        localPropS.cardTable.showColByKey(tableid,['self_creditorgamount','other_debitorgamount','orgamountequal','orgamountbalance']);
        let col2 = localPropS.cardTable.getColValue('body','orgamountequal');
        for (let i=0;i<col2.length;i++)
        {
            if(col2[i].value==true)
            {
                index.push(i)
            }
        }
    }
	if(content[3]==='N')
	{
		localPropS.cardTable.hideColByKey(tableid,['self_creditgroupamount','other_debitgroupamount','groupamountequal','groupamountbalance']);
    }
    else
    {
        localPropS.cardTable.showColByKey(tableid,['self_creditgroupamount','other_debitgroupamount','groupamountequal','groupamountbalance']);
        let col3 = localPropS.cardTable.getColValue('body','groupamountequal');
        for (let i=0;i<col3.length;i++)
        {
            if(col3[i].value==true)
            {
                index.push(i)
            }
        }
    }
	if(content[4]==='N')
	{
		localPropS.cardTable.hideColByKey(tableid,['self_creditglobalamount','other_debitglobalamount','globalamountequal','globalamountbalance']);
    }
    else
    {
        localPropS.cardTable.showColByKey(tableid,['self_creditglobalamount','other_debitglobalamount','globalamountequal','globalamountbalance']);
        let col4 = localPropS.cardTable.getColValue('body','globalamountequal');
        for (let i=0;i<col4.length;i++)
        {
            if(col4[i].value==true)
            {
                index.push(i)
            }
        }
    }

    localPropS.cardTable.hideColByKey(tableid,
    ['self_debitquantity','self_debitamount','self_debitorgamount','self_debitgroupamount','self_debitglobalamount',
    'other_creditquantity','other_creditamount','other_creditorgamount','other_creditgroupamount','other_creditglobalamount',
    'self_quantityoccur','self_amountoccur','self_orgamountoccur','self_groupamountoccur','self_globalamountoccur',
    'other_quantityoccur','other_amountoccur','other_orgamountoccur','other_groupamountoccur','other_globalamountoccur']);
    //设置选中行
    localPropS.cardTable.selectRowsByIndex('body', index);
  }
  
  //净发生
  setBalanceContrast = (content) => {
    let index = new Array();
    let localPropS = this.props.props
	let tableid = 'body'
	if(content[0]==='N')
	{
        localPropS.cardTable.hideColByKey(tableid,['self_quantityoccur','other_quantityoccur','quantityequal','quantitybalance']);
    }
    else
    {
        localPropS.cardTable.showColByKey(tableid,['self_quantityoccur','other_quantityoccur','quantityequal','quantitybalance']);
        let col0 = localPropS.cardTable.getColValue('body','quantityequal');
        for (let i=0;i<col0.length;i++)
        {
            if(col0[i].value==true)
            {
                index.push(i)
            }
        }
    }
	if(content[1]==='N')
	{
        localPropS.cardTable.hideColByKey(tableid,['self_amountoccur','other_amountoccur','amountequal','amountbalance']);
    }
    else
    {
        localPropS.cardTable.showColByKey(tableid,['self_amountoccur','other_amountoccur','amountequal','amountbalance']);
        let col1 = localPropS.cardTable.getColValue('body','amountequal')
        for (let i=0;i<col1.length;i++)
        {
            if(col1[i].value==true)
            {
                index.push(i)
            }
        }
    }
	if(content[2]==='N')
	{
        localPropS.cardTable.hideColByKey(tableid,['self_orgamountoccur','other_orgamountoccur','orgamountequal','orgamountbalance']);
    }
    else
    {
        localPropS.cardTable.showColByKey(tableid,['self_orgamountoccur','other_orgamountoccur','orgamountequal','orgamountbalance']);
        let col2 = localPropS.cardTable.getColValue('body','orgamountequal');
        for (let i=0;i<col2.length;i++)
        {
            if(col2[i].value==true)
            {
                index.push(i)
            }
        }
    }
	if(content[3]==='N')
	{
		localPropS.cardTable.hideColByKey(tableid,['self_groupamountoccur','other_groupamountoccur','groupamountequal','groupamountbalance']);
    }
    else
    {
        localPropS.cardTable.showColByKey(tableid,['self_groupamountoccur','other_groupamountoccur','groupamountequal','groupamountbalance']);
        let col3 = localPropS.cardTable.getColValue('body','groupamountequal');
        for (let i=0;i<col3.length;i++)
        {
            if(col3[i].value==true)
            {
                index.push(i)
            }
        }
    }
	if(content[4]==='N')
	{
		localPropS.cardTable.hideColByKey(tableid,['self_globalamountoccur','other_globalamountoccur','globalamountequal','globalamountbalance']);
    }
    else
    {
        localPropS.cardTable.showColByKey(tableid,['self_globalamountoccur','other_globalamountoccur','globalamountequal','globalamountbalance']);
        let col4 = localPropS.cardTable.getColValue('body','globalamountequal');
        for (let i=0;i<col4.length;i++)
        {
            if(col4[i].value==true)
            {
                index.push(i)
            }
        }
    }

    localPropS.cardTable.hideColByKey(tableid,
    ['self_debitquantity','self_debitamount','self_debitorgamount','self_debitgroupamount','self_debitglobalamount',
    'self_creditquantity','self_creditamount','self_creditorgamount','self_creditgroupamount','self_creditglobalamount',
    'other_debitamount','other_debitorgamount','other_debitgroupamount','other_debitglobalamount','other_debitquantity',
    'other_creditamount','other_creditorgamount','other_creditgroupamount','other_creditglobalamount','other_creditquantity']);
	//设置选中行
    localPropS.cardTable.selectRowsByIndex('body', index);
  }

    open() {
        this.setState({
            showModal: true
        });
    }

    render () {
        let {searchData,pk_rule,pk_currtype,pk_selfaccountingbook,pk_otheraccountingbook}=this.state;
        let {defaultAccbook} = this.props;
        //给查询条件赋初值
        pk_selfaccountingbook.refname = cacheTools.get('sumaccountingbookname') || '';
        pk_selfaccountingbook.refpk = cacheTools.get('sumaccountingbookpk') || '';
        searchData.pk_selfaccountingbook = cacheTools.get('sumaccountingbookpk') || '';
        pk_otheraccountingbook= cacheTools.get('sumotheraccountingbook');
        searchData.pk_otheraccountingbook = cacheTools.get('sumotheraccountingbookpk');
        pk_currtype = cacheTools.get('sumpk_currtype')||'';
        searchData.pk_currtype = cacheTools.get('sumpk_currtypepk')||'';
        searchData.begindate = cacheTools.get('sumbegindate');
        searchData.enddate = cacheTools.get('sumenddate');
        return (
        <div>
            <Modal
                className = "sumcontrast m-mxdzcx"
                show = { this.state.showModal }
                onHide = { this.close } 
                fieldid="search">
                <Modal.Header closeButton fieldid="header-area">
                    <Modal.Title>{this.state.json['20022015-000009']}</Modal.Title>{/* 国际化处理： 汇总对账查询*/}
                    {/* <i class="uf uf-close" style={{position:"absolute",right:"20px",fontSize:"14px"}} onClick={ this.close }></i> */}
                </Modal.Header>

                <Modal.Body>
                <NCDiv fieldid="query" areaCode={NCDiv.config.FORM}  className="nc-theme-form-label-c">
                    <NCRow>
                        <NCCol lg={3} sm={3} xs={3}>
                        <span style={{"color":"#f22c1d","margin-right":"2px"}}>*</span>
                            {this.state.json['20022015-000010']}{/* 国际化处理： 对账规则*/}
                        </NCCol>
                        <NCCol lg={9} sm={9} xs={9}>
                            <ContrastRuleGridRef 
                            fieldid="contrastrule"
                            queryCondition ={(v)=>{
                                return {startstatus:'All'};
                            }}
                                value={pk_rule}
                                onChange={(v)=>{
                                    pk_rule=v
                                    //发送请求确定数量金额控件的可用性
                                    if(pk_rule.refpk)
                                    {
                                        searchData.pk_rule=v.refpk;
                                        searchData.rulename=v.refname;
                                        ajax({
                                            url: '/nccloud/gl/sumcontrast/rulecontent.do',
                                            data: pk_rule.refpk,
                                            success: (res) => { 
                                            //返回规则中的内容
                                            //对规则的查询统一在切换规则的时候做
                                            if(res.data)
                                            {
                                                let main=res.data.main;
                                                cacheTools.set('summain',main); 
                                                let content=res.data.content;
                                                cacheTools.set('sumcontent',content);   
                                                
                                                cacheTools.set('sumShowGroup',res.data.showGroup);
                                                this.forceUpdate(); 
                                                if(content[0]=='Y')
                                                {
                                                    searchData.quantity=true;
                                                    searchData.quantitydisable=true;
                                                }
                                                else
                                                {
                                                    searchData.quantity=false;
                                                    searchData.quantitydisable=true;
                                                }
                                                if(content[1]=='Y'||content[2]=='Y'||content[3]=='Y'||content[4]=='Y')
                                                {
                                                    searchData.amount=true;
                                                    searchData.amountdisable=true;
                                                }
                                                else
                                                {
                                                    searchData.amount=false;
                                                    searchData.amountdisable=true;
                                                }
                                            }
                                            }
                                        })
                                    }
                                    else
                                    {
                                        searchData.pk_rule='';
                                        searchData.rulename='';
                                    }
                                    cacheTools.set('sumaccountingbookname', defaultAccbook.refname);
                                    cacheTools.set('sumaccountingbookpk', defaultAccbook.refpk);
                                    cacheTools.set('sumotheraccountingbook',new Array(0));
                                    cacheTools.set('sumotheraccountingbookpk',new Array(0));
                                    //缓存本地，跳转回来参照赋值
                                    cacheTools.set('sumpk_rule',pk_rule);
                                    this.setState({
                                        pk_rule,searchData
                                    })
                                }}
                            />
                        </NCCol>
                    </NCRow>
                    <NCRow>
                        <NCCol lg={3} sm={3} xs={3}>
                            {this.state.json['20022015-000011']}{/* 国际化处理： 规则方向*/}
                        </NCCol>
                        <NCCol lg={9} sm={9} xs={9}>
                            <NCRadio.NCRadioGroup
                                name="12"
                                selectedValue={searchData.ruledirection}
                                onChange={(v)=>{
                                    searchData.ruledirection=v;
                                    this.setState({
                                        searchData
                                    })
                                }}>
                                <NCRadio  value={true}>{this.state.json['20022015-000012']}</NCRadio>{/* 国际化处理： 正向*/}
                                <NCRadio  value={false}>{this.state.json['20022015-000013']}</NCRadio>{/* 国际化处理： 反向*/}
                            </NCRadio.NCRadioGroup>
                        </NCCol>
                    </NCRow>
                    <NCRow>
                        <NCCol lg={3} sm={3} xs={3}>
                            {this.state.json['20022015-000014']}{/* 国际化处理： 币种*/}
                        </NCCol>
                        <NCCol lg={9} sm={9} xs={9}>
                            <CurrtypeGridRef 
                                fieldid="currtype"
                                value={pk_currtype}
                                onChange={(v)=>{
                                    pk_currtype=v,
                                    searchData.pk_currtype=v.refpk;
                                    cacheTools.set('sumpk_currtype',pk_currtype);
                                    cacheTools.set('sumpk_currtypepk',v.refpk);
                                    this.setState({
                                        pk_currtype,searchData
                                    })
                                }}
                            />
                        </NCCol>
                    </NCRow>
                    <NCRow>
                        <NCCol lg={3} sm={3} xs={3}>
                        <span style={{"color":"#f22c1d","margin-right":"2px"}}>*</span>
                            {this.state.json['20022015-000015']}{/* 国际化处理： 本方核算账簿*/}
                        </NCCol>
                        <NCCol lg={9} sm={9} xs={9}>
                            <ReferLoader
                                fieldid="accountbook"
                                tag = 'selfbook'
                                refcode = 'uapbd/refer/org/AccountBookTreeRef/index.js'
                                value = {{refname:pk_selfaccountingbook.refname,refpk:pk_selfaccountingbook.refpk}}
                                isMultiSelectedEnabled = {false}
                                showGroup = {false}
                                showInCludeChildren = {false}
                                queryCondition = {()=>{
                                    let pk_contrastrule = cacheTools.get('sumpk_rule');
                                    if(pk_contrastrule)
                                    {                            
                                        let condition = {
                                            "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilderForContarstRule',
                                            pk_contrastrule:pk_contrastrule.refpk,
                                            appcode: cacheTools.get('sumappcode'),
                                            isSelf:'Y'
                                        }
                                        return condition;
                                    }
                                    else
                                    {
                                        let condition = {
                                            "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilderForContarstRule'
                                        }
                                        return condition;
                                    }
                                }
                                }
                                onChange ={(v)=>{
                                                pk_selfaccountingbook.refname=v.refname;
                                                pk_selfaccountingbook.refpk=v.refpk;
                                                searchData.pk_selfaccountingbook = v.refpk;
                                                //缓存本地，跳转回来参照赋值
                                                cacheTools.set('sumaccountingbookpk',v.refpk);
                                                cacheTools.set('sumaccountingbookname',v.refname);
                                                this.setState({
                                                    pk_selfaccountingbook,
                                                    searchData
                                                })
                                            }}
                            />
 
                        </NCCol>
                    </NCRow>
                    <NCRow>
                        <NCCol lg={3} sm={3} xs={3}>
                        <span style={{"color":"#f22c1d","margin-right":"2px"}}>*</span>
                            {this.state.json['20022015-000016']}{/* 国际化处理： 对方核算账簿*/}
                        </NCCol>
                        <NCCol lg={9} sm={9} xs={9}>
                        <ReferLoader
                            fieldid="accountbook1"
                            tag = 'test'
                            refcode = 'uapbd/refer/org/AccountBookTreeRef/index.js'
                            value = {pk_otheraccountingbook}
                            isMultiSelectedEnabled = {true}
                            showGroup = {cacheTools.get('sumShowGroup')}
                            showInCludeChildren = {true}
                            disabledDataShow = {true}
                            queryCondition = {()=>{
                                let pk_contrastrule = cacheTools.get('sumpk_rule');
                                if(pk_contrastrule)
                                {
                                    let condition = {
                                        "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilderForContarstRule',
                                        pk_contrastrule:pk_contrastrule.refpk,
                                        appcode: cacheTools.get('sumappcode'),
                                        isSelf:'N',
                                        showGroup:cacheTools.get('sumShowGroup'),
                                        multiGroup:'Y'
                                        }
                                        return condition;
                                }
                                else
                                {
                                    let condition = {
                                        "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilderForContarstRule'
                                        }
                                        return condition;
                                }
                            }    
                            }
                            onChange ={(v)=>{
                                           pk_otheraccountingbook = [];
                                            let modalArr=[]
                                            if(v&&v.length!=0){
                                                v.map((item,index)=>{
                                                    modalArr.push(item.refpk);
                                                    pk_otheraccountingbook.push({refname:item.refname,refpk:item.refpk});
                                                })
                                            }
                                            searchData.pk_otheraccountingbook = modalArr;
                                            cacheTools.set('sumotheraccountingbookpk',modalArr);
                                            //缓存本地，跳转回来参照赋值
                                            cacheTools.set('sumotheraccountingbook',pk_otheraccountingbook);
                                            this.setState({
                                                pk_otheraccountingbook,
                                                searchData
                                            })
                                        }}
                        />
 
                        </NCCol>
                    </NCRow>
                    <NCRow>
                        <NCCol lg={3} sm={3} xs={3}>
                            {this.state.json['20022015-000017']}{/* 国际化处理： 对账种类*/}
                        </NCCol>
                        <NCCol  className={"dzzl"}  lg={4} sm={4} xs={4}>
                            <NCCheckbox colors="info" name="agree"
                                checked={searchData.amount}
                                disabled={searchData.amountdisable}
                                onChange={(v)=>{
                                    searchData.amount=v;
                                    this.setState({
                                        searchData
                                    })
                                }}
                                >
                                    {this.state.json['20022015-000018']}{/* 国际化处理： 金额*/}
                            </NCCheckbox>
                        </NCCol>
                        <NCCol   className={"dzzl"}  lg={4} sm={4} xs={4}>
                            <NCCheckbox colors="info" name="agree"
                                checked={searchData.quantity}
                                disabled={searchData.quantitydisable}
                                onChange={(v)=>{
                                    searchData.quantity=v;
                                    this.setState({
                                        searchData
                                    })
                                }}
                                >
                                    {this.state.json['20022015-000019']}{/* 国际化处理： 数量*/}
                            </NCCheckbox>
                        </NCCol>
                    </NCRow>
                    <NCRow>
                         <NCCol lg={3} sm={3} xs={3}>
                            <NCCheckbox colors="info" name="agree"
                            checked={searchData.date == 'Y'}
                            onChange={(v)=>{
                                searchData.date=v?'Y':'N';
                                this.setState({
                                    searchData
                                })
                            }}
                                        className = {"hzdz-rq"}
                            >
                                {this.state.json['20022015-000020']}{/* 国际化处理： 日期*/}
                        </NCCheckbox>
                       </NCCol> 
                        <NCCol className={"ri-wid"}  lg={4} sm={4} xs={4}>
                            <NCDatePicker
                                fieldid="sumbegindate"
                                value={searchData.begindate}
                                disabled = {!(searchData.date == 'Y')}
                                onChange={(v)=>{
                                    searchData.begindate=v;
                                    cacheTools.set('sumbegindate',v);
                                    this.setState({
                                        searchData
                                    })
                                }}
                            />
                        </NCCol>
                        <NCCol lg={1} sm={1} xs={1}>
                        <span className={"zhi_"}>{this.state.json['20022015-000021']}</span>{/* 国际化处理： 至*/}
                        </NCCol>
                        <NCCol className={"ri-wid"}  lg={4} sm={4} xs={4}>
                            <NCDatePicker
                                fieldid="sumenddate"
                                value={searchData.enddate}
                                disabled = {!(searchData.date == 'Y')}
                                onChange={(v)=>{
                                    searchData.enddate=v;
                                    cacheTools.set('sumenddate',v);
                                    this.setState({
                                        searchData
                                    })
                                }}
                            />
                        </NCCol>
                    </NCRow>
                    <NCRow>
                        <NCCol lg={3} sm={3} xs={3}>
                            {this.state.json['20022015-000022']}{/* 国际化处理： 对符范围*/}
                        </NCCol>
                        <NCCol lg={9} sm={9} xs={9}>
                            <NCRadio.NCRadioGroup
                                name="12"
                                selectedValue={searchData.range}
                                onChange={(v)=>{
                                    searchData.range=v;
                                    this.setState({
                                        searchData
                                    })
                                }}>
                                <NCRadio  value='1'>{this.state.json['20022015-000023']}</NCRadio>{/* 国际化处理： 未对符*/}
                                <NCRadio  value='2'>{this.state.json['20022015-000024']}</NCRadio>{/* 国际化处理： 已对符*/}
                                <NCRadio  value='3'>{this.state.json['20022015-000025']}</NCRadio>{/* 国际化处理： 全部*/}
                            </NCRadio.NCRadioGroup>
                        </NCCol>
                    </NCRow>
                    <NCRow>
                        <NCCol lg={3} sm={3} xs={3}>
                        {this.state.json['20022015-000026']}{/* 国际化处理： 对账方式*/}
                        </NCCol>
                        <NCCol lg={9} sm={9} xs={9}>
                            <NCRadio.NCRadioGroup
                                name="12"
                                selectedValue={searchData.contraststyle}
                                onChange={(v)=>{
                                    searchData.contraststyle=v;
                                    this.setState({
                                        searchData
                                    })
                                }}>
                                <NCRadio  value='1'>{this.state.json['20022015-000027']}</NCRadio>{/* 国际化处理： 借贷对账*/}
                                <NCRadio  value='2'>{this.state.json['20022015-000028']}</NCRadio>{/* 国际化处理： 贷借对账*/}
                                <NCRadio  value='3'>{this.state.json['20022015-000029']}</NCRadio>{/* 国际化处理： 净发生*/}
                            </NCRadio.NCRadioGroup>
                        </NCCol>
                    </NCRow>
                    </NCDiv>
                </Modal.Body>
                <Modal.Footer>
                    <Button fieldid="confirm" onClick={ this.handleConfirm } colors="primary">{this.state.json['20022015-000030']}</Button>{/* 国际化处理： 查询*/}
                    <Button fieldid="cancel" onClick={ this.close } shape="border" className="cancel">{this.state.json['20022015-000031']}</Button>{/* 国际化处理： 取消*/}
                   
                </Modal.Footer>
           </Modal>
        </div>
        )
    }
}

