import { ajax, base, toast,cacheTools } from 'nc-lightapp-front';
export default function(props, id) {

  let queryvo = cacheTools.get('sumqueryvo');
  let content = cacheTools.get('sumcontent');
  switch (id) {
      case 'query'://查询
      this.setState({
        seachModalShow:true
      })
      
      break;
    case 'refresh'://刷新
    ajax({
      url: '/nccloud/gl/sumcontrast/query.do',
      data: queryvo,
      success: (res) => { 
        if(res.data)
        {
          if (res.data.body) {
						this.props.cardTable.setTableData(this.tableId, res.data.body);
						toast({ color: 'success', title: this.state.json['20022015-000007']});/* 国际化处理：刷新成功！*/
            if(queryvo.contraststyle==1){
              //借贷
              setDCContrast(content,this.props);
             }else if(queryvo.contraststyle==2){
              //贷借
              setCDContrast(content,this.props);
             }else if(queryvo.contraststyle==3){
              //净发生
              setBalanceContrast(content,this.props);
             } 
          }
        }
        else
        {
					toast({ color: 'success', title: this.state.json['20022015-000007']});/* 国际化处理：刷新成功！*/
          this.props.cardTable.setTableData(this.tableId, {rows:[]});
        }
      }
    })
    break;
		case 'contrast'://勾对
		let checkdata = this.props.cardTable.getCheckedRows(this.tableId);
    if(checkdata.length==0){
      toast({color: 'warning', content: this.state.json['20022015-000001']});/* 国际化处理： 请选择要勾兑的数据*/
      return 
    }
    
    let checkdataArray=[];
    checkdata.forEach((val) => {
			let currentArray =new Array(3)
			currentArray[0] = val.index+'',
      currentArray[1] = val.data.values.pk_currtype.value,
      currentArray[2] = val.data.values.pk_selfaccountingbook.value,
      currentArray[3] = val.data.values.pk_otheraccountingbook.value
      checkdataArray.push(currentArray);
    });
    let contrastdatavo = {
      data:checkdataArray,
      queryvo:queryvo
    };

      ajax({
        url: '/nccloud/gl/sumcontrast/contrast.do',
        data:contrastdatavo,
        success: (res) => {
          if (res.success) {
            if(res.data)
            {
              toast({color: 'success', content: this.state.json['20022015-000002']});/* 国际化处理： 勾对成功*/
              //刷前台数据
              ajax({
                url: '/nccloud/gl/sumcontrast/query.do',
                data: queryvo,
                success: (res) => { 
                  if(res.data)
                  {
                    if (res.data.body) {
                      this.props.cardTable.setTableData(this.tableId, res.data.body);
                    }
                  }
                  else
                  {
                    this.props.cardTable.setTableData(this.tableId, {rows:[]});
                  }
                }
              })
            }
            else
            {
              toast({color: 'warning', content: this.state.json['20022015-000003']});/* 国际化处理： 勾对失败*/
            }
          }
        }
      });
    break;

    case 'detail'://明细
        if(JSON.stringify(this.state.selectedData) == "{}"){
            toast({ color: 'warning', content: this.state.json['20022015-000004'] });/* 国际化处理： 没有选中项*/
            return;
        }
        let sendUrl = '/gl/gl_balancecontrast/detailcontrast/list/index.html';
        props.linkTo(
          sendUrl,
          { appcode:'20022010',pagecode:'20022010_LIST',jumpflag:true}
        );
        break;
    case 'make'://生成对账报告
        ajax({
          url: '/nccloud/gl/sumcontrast/make.do',
          data : queryvo,
          success: (res) => {
            if (res.success) {
              toast({ color: 'success', content: res.data });
            }
          }
        });
        break;
    default:
      break
  }
}

//借贷
function setDCContrast  (content,props) {
	let index = new Array();
	let tableid = 'body';
	if(content[0]==='N')
	{
	  props.cardTable.hideColByKey(tableid,['self_debitquantity','other_creditquantity','quantityequal','quantitybalance']);
	}
	else
	{
      props.cardTable.showColByKey(tableid,['self_debitquantity','other_creditquantity','quantityequal','quantitybalance']);
			let col0 = props.cardTable.getColValue('body','quantityequal');
			for (let i=0;i<col0.length;i++)
			{
					if(col0[i].value==true)
					{
							index.push(i)
					}
			}
	}
	if(content[1]==='N')
	{
	  props.cardTable.hideColByKey(tableid,['self_debitamount','other_creditamount','amountequal','amountbalance']);
	}
	else
	{
      props.cardTable.showColByKey(tableid,['self_debitamount','other_creditamount','amountequal','amountbalance']);
			let col1 = props.cardTable.getColValue('body','amountequal');
			for (let i=0;i<col1.length;i++)
			{
					if(col1[i].value==true)
					{
							index.push(i)
					}
			}
	}
	if(content[2]==='N')
	{
	  props.cardTable.hideColByKey(tableid,['self_debitorgamount','other_creditorgamount','orgamountequal','orgamountbalance']);
	}
	else
	{
      props.cardTable.showColByKey(tableid,['self_debitorgamount','other_creditorgamount','orgamountequal','orgamountbalance']);
			let col2 = props.cardTable.getColValue('body','orgamountequal');
			for (let i=0;i<col2.length;i++)
			{
					if(col2[i].value==true)
					{
							index.push(i)
					}
			}
	}
	if(content[3]==='N')
	{
	  props.cardTable.hideColByKey(tableid,['self_debitgroupamount','other_creditgroupamount','groupamountequal','groupamountbalance']);
	}
	else
	{
      props.cardTable.showColByKey(tableid,['self_debitgroupamount','other_creditgroupamount','groupamountequal','groupamountbalance']);
			let col3 = props.cardTable.getColValue('body','groupamountequal');
			for (let i=0;i<col3.length;i++)
			{
					if(col3[i].value==true)
					{
							index.push(i)
					}
			}
	}
	if(content[4]==='N')
	{
	  props.cardTable.hideColByKey(tableid,['self_debitglobalamount','other_creditglobalamount','globalamountequal','globalamountbalance']);
	}
	else
	{
      props.cardTable.showColByKey(tableid,['self_debitglobalamount','other_creditglobalamount','globalamountequal','globalamountbalance']);
			let col4 = props.cardTable.getColValue('body','globalamountequal');
			for (let i=0;i<col4.length;i++)
			{
					if(col4[i].value==true)
					{
							index.push(i)
					}
			}
	}
  
    props.cardTable.hideColByKey(tableid,
    ['self_creditquantity','self_creditamount','self_creditorgamount','self_creditgroupamount','self_creditglobalamount',
    'other_debitquantity','other_debitamount','other_debitorgamount','other_debitgroupamount','other_debitglobalamount',
    'self_quantityoccur','self_amountoccur','self_orgamountoccur','self_groupamountoccur','self_globalamountoccur',
    'other_quantityoccur','other_amountoccur','other_orgamountoccur','other_groupamountoccur','other_globalamountoccur']);
    //设置选中行
    props.cardTable.selectRowsByIndex('body', index);
  }
//贷借
function setCDContrast(content,props) {
	let index = new Array();
	let tableid = 'body'
	if(content[0]==='N')
	{
	  props.cardTable.hideColByKey(tableid,['self_creditquantity','other_debitquantity','quantityequal','quantitybalance']);
	}
	else
	{
      props.cardTable.showColByKey(tableid,['self_creditquantity','other_debitquantity','quantityequal','quantitybalance']);
			let col0 = props.cardTable.getColValue('body','quantityequal');
			for (let i=0;i<col0.length;i++)
			{
					if(col0[i].value==true)
					{
							index.push(i)
					}
			}
	}
	if(content[1]==='N')
	{
	  props.cardTable.hideColByKey(tableid,['self_creditamount','other_debitamount','amountequal','amountbalance']);
	}
	else
	{
      props.cardTable.showColByKey(tableid,['self_creditamount','other_debitamount','amountequal','amountbalance']);
			let col1 = props.cardTable.getColValue('body','amountequal');
			for (let i=0;i<col1.length;i++)
			{
					if(col1[i].value==true)
					{
							index.push(i)
					}
			} 
	}
	if(content[2]==='N')
	{
	  props.cardTable.hideColByKey(tableid,['self_creditorgamount','other_debitorgamount','orgamountequal','orgamountbalance']);
	}
	else
	{
      props.cardTable.showColByKey(tableid,['self_creditorgamount','other_debitorgamount','orgamountequal','orgamountbalance']);
			let col2 = props.cardTable.getColValue('body','orgamountequal');
			for (let i=0;i<col2.length;i++)
			{
					if(col2[i].value==true)
					{
							index.push(i)
					}
			}
	}
	if(content[3]==='N')
	{
	  props.cardTable.hideColByKey(tableid,['self_creditgroupamount','other_debitgroupamount','groupamountequal','groupamountbalance']);
	}
	else
	{
      props.cardTable.showColByKey(tableid,['self_creditgroupamount','other_debitgroupamount','groupamountequal','groupamountbalance']);
			let col3 = props.cardTable.getColValue('body','groupamountequal');
			for (let i=0;i<col3.length;i++)
			{
					if(col3[i].value==true)
					{
							index.push(i)
					}
			}
	}
	if(content[4]==='N')
	{
	  props.cardTable.hideColByKey(tableid,['self_creditglobalamount','other_debitglobalamount','globalamountequal','globalamountbalance']);
	}
	else
	{
      props.cardTable.showColByKey(tableid,['self_creditglobalamount','other_debitglobalamount','globalamountequal','globalamountbalance']);
			let col4 = props.cardTable.getColValue('body','globalamountequal');
			for (let i=0;i<col4.length;i++)
			{
					if(col4[i].value==true)
					{
							index.push(i)
					}
			}
	}
  
    props.cardTable.hideColByKey(tableid,
    ['self_debitquantity','self_debitamount','self_debitorgamount','self_debitgroupamount','self_debitglobalamount',
    'other_creditquantity','other_creditamount','other_creditorgamount','other_creditgroupamount','other_creditglobalamount',
    'self_quantityoccur','self_amountoccur','self_orgamountoccur','self_groupamountoccur','self_globalamountoccur',
    'other_quantityoccur','other_amountoccur','other_orgamountoccur','other_groupamountoccur','other_globalamountoccur']);
    //设置选中行
    props.cardTable.selectRowsByIndex('body', index);
  
  }
  //净发生
  function setBalanceContrast(content,props) {
	let index = new Array();
	let tableid = 'body'
	if(content[0]==='N')
	{
	  props.cardTable.hideColByKey(tableid,['self_quantityoccur','other_quantityoccur','quantityequal','quantitybalance']);
	}
	else
	{
      props.cardTable.showColByKey(tableid,['self_quantityoccur','other_quantityoccur','quantityequal','quantitybalance']);
			let col0 = props.cardTable.getColValue('body','quantityequal');
			for (let i=0;i<col0.length;i++)
			{
					if(col0[i].value==true)
					{
							index.push(i)
					}
			}
	}
	if(content[1]==='N')
	{
	  props.cardTable.hideColByKey(tableid,['self_amountoccur','other_amountoccur','amountequal','amountbalance']);
	}
	else
	{
      props.cardTable.showColByKey(tableid,['self_amountoccur','other_amountoccur','amountequal','amountbalance']);
			let col1 = props.cardTable.getColValue('body','amountequal')
			for (let i=0;i<col1.length;i++)
			{
					if(col1[i].value==true)
					{
							index.push(i)
					}
			}
	}
	if(content[2]==='N')
	{
	  props.cardTable.hideColByKey(tableid,['self_orgamountoccur','other_orgamountoccur','orgamountequal','orgamountbalance']);
	}
	else
	{
      props.cardTable.showColByKey(tableid,['self_orgamountoccur','other_orgamountoccur','orgamountequal','orgamountbalance']);
			let col2 = props.cardTable.getColValue('body','orgamountequal');
			for (let i=0;i<col2.length;i++)
			{
					if(col2[i].value==true)
					{
							index.push(i)
					}
			}
	}
	if(content[3]==='N')
	{
	  props.cardTable.hideColByKey(tableid,['self_groupamountoccur','other_groupamountoccur','groupamountequal','groupamountbalance']);
	}
	else
	{
      props.cardTable.showColByKey(tableid,['self_groupamountoccur','other_groupamountoccur','groupamountequal','groupamountbalance']);
			let col3 = props.cardTable.getColValue('body','groupamountequal');
			for (let i=0;i<col3.length;i++)
			{
					if(col3[i].value==true)
					{
							index.push(i)
					}
			}
	}
	if(content[4]==='N')
	{
	  props.cardTable.hideColByKey(tableid,['self_globalamountoccur','other_globalamountoccur','globalamountequal','globalamountbalance']);
	}
	else
	{
      props.cardTable.showColByKey(tableid,['self_globalamountoccur','other_globalamountoccur','globalamountequal','globalamountbalance']);
			let col4 = props.cardTable.getColValue('body','globalamountequal');
			for (let i=0;i<col4.length;i++)
			{
					if(col4[i].value==true)
					{
							index.push(i)
					}
			}
	}
  
    props.cardTable.hideColByKey(tableid,
    ['self_debitquantity','self_debitamount','self_debitorgamount','self_debitgroupamount','self_debitglobalamount',
    'self_creditquantity','self_creditamount','self_creditorgamount','self_creditgroupamount','self_creditglobalamount',
    'other_debitamount','other_debitorgamount','other_debitgroupamount','other_debitglobalamount','other_debitquantity',
    'other_creditamount','other_creditorgamount','other_creditgroupamount','other_creditglobalamount','other_creditquantity']);
		//设置选中行
    props.cardTable.selectRowsByIndex('body', index);
  
  }
