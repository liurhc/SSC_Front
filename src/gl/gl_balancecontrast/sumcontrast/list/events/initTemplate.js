import { base, ajax,getBusinessInfo,cacheTools  } from 'nc-lightapp-front';
let { NCPopconfirm } = base;
const formId = 'head';
const tableId = 'body';
const pageId = '20022015_LIST';

export default function(props) {
	let page = this;
	let appcode = props.getUrlParam('c')||props.getSearchParam('c');
	cacheTools.set('sumappcode',appcode); 
	props.createUIDom(
		{
			pagecode: pageId,
			appcode:appcode
		}, 
		function (data){
			if(data){	
				if(data.template){
					let meta = data.template;
					modifierMeta(props, meta)
					props.meta.setMeta(meta);
				}
				if(data.button){
					let button = data.button;
					props.button.setButtons(button);
				}
				if(data.context){ 
				//页面跳转，不需要执行
				if(!props.getUrlParam('jumpflag'))
				{	
						//查询面板的开始日期和截至日期
						let businessInfo = getBusinessInfo();
						let senddata = 
						{
							pk_org:null,
							busiDate:businessInfo.businessDate 
						}
						if(data.context.defaultAccbookPk)
						{
							cacheTools.set('sumaccountingbookpk',data.context.defaultAccbookPk); 
							cacheTools.set('sumaccountingbookname',data.context.defaultAccbookName);
							page.defaultAccbook.refname = data.context.defaultAccbookName;
							page.defaultAccbook.refpk = data.context.defaultAccbookPk;
							senddata.pk_org = data.context.defaultAccbookPk;
							
						}
						ajax({
							url: '/nccloud/gl/sumcontrast/date.do',
							data: senddata,
							success: (res) => { 
							//返回规则中的内容
							//对规则的查询统一在切换规则的时候做
							if(res.data)
							{
									let begindate=res.data.begindate;
									cacheTools.set('sumbegindate',begindate); 
									let enddate=res.data.enddate;
									cacheTools.set('sumenddate',enddate);   
							}}
					})
				}
			}
			if(props.getUrlParam('jumpflag'))
			{
				let queryvo = cacheTools.get('sumqueryvo');
				getInitData(queryvo,props);
			}	
			}   
		}
	)
}

function modifierMeta(props, meta) {
	let status = props.getUrlParam('status');
	meta[formId].status = status;
	meta[tableId].status = status;
	//let multiLang = props.MutiInit.getIntl('2052');
	return meta;
}

function getInitData(searchData,props){//获取全部数据
	let content = cacheTools.get('summain');
	if(content=='N')
	{
		props.button.setButtonDisabled(['refresh','make'], false);
	}
	else
	{
		props.button.setButtonDisabled('refresh', false);
		props.button.setButtonDisabled('make',true);
	}
	ajax({
		url: '/nccloud/gl/sumcontrast/show.do',
		data: searchData,
		success: (res) => { 
		 if(res.data)
		  {
		  //设置表头数据          
		  props.form.setFormItemsValue('head',{'contrastRule':{value:res.data.contrastRule,display:null}});
		  props.form.setFormItemsValue('head',{'ass':{value:res.data.ass,display:null}});
		  props.form.setFormItemsValue('head',{'selfAccount':{value:res.data.selfAccount,display:null}});
		  props.form.setFormItemsValue('head',{'otherAccount':{value:res.data.otherAccount,display:null}});
		  props.form.setFormItemsValue('head',{'contrastWay':{value:res.data.contrastWay,display:null}});
		  props.form.setFormItemsValue('head',{'dateArea':{value:res.data.dateArea,display:null}});
		  props.form.setFormItemsValue('head',{'contrastArea':{value:res.data.contrastArea,display:null}});
		}
		}
	})
	ajax({
	url: '/nccloud/gl/sumcontrast/query.do',
	data: searchData,
	success: (res) => { 
		if(res.data)
		{
			if (res.data.body) {
				props.cardTable.setTableData('body', res.data.body);
				let content = cacheTools.get('sumcontent');
				if(searchData.contraststyle==1){
					//借贷
					setDCContrast(content,props);
					}else if(searchData.contraststyle==2){
					//贷借
					setCDContrast(content,props);
					}else if(searchData.contraststyle==3){
					//净发生
					setBalanceContrast(content,props);
					}
			}
		}
		else
		{
			props.cardTable.setTableData('body', {rows:[]});
		}
	}
	})
}
//借贷
function setDCContrast  (content,props) {
	let index = new Array();
	let tableid = 'body';
	if(content[0]==='N')
	{
	  props.cardTable.hideColByKey(tableid,['self_debitquantity','other_creditquantity','quantityequal','quantitybalance']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_debitquantity','other_creditquantity','quantityequal','quantitybalance']);
		let col0 = props.cardTable.getColValue('body','quantityequal');
		for (let i=0;i<col0.length;i++)
		{
				if(col0[i].value==true)
				{
						index.push(i)
				}
		}
	}
	if(content[1]==='N')
	{
	  props.cardTable.hideColByKey(tableid,['self_debitamount','other_creditamount','amountequal','amountbalance']);
	}
	else
	{
			props.cardTable.showColByKey(tableid,['self_debitamount','other_creditamount','amountequal','amountbalance']);
			let col1 = props.cardTable.getColValue('body','amountequal');
			for (let i=0;i<col1.length;i++)
			{
					if(col1[i].value==true)
					{
							index.push(i)
					}
			}
	}
	if(content[2]==='N')
	{
	  props.cardTable.hideColByKey(tableid,['self_debitorgamount','other_creditorgamount','orgamountequal','orgamountbalance']);
	}
	else
	{
			props.cardTable.showColByKey(tableid,['self_debitorgamount','other_creditorgamount','orgamountequal','orgamountbalance']);
			let col2 = props.cardTable.getColValue('body','orgamountequal');
			for (let i=0;i<col2.length;i++)
			{
					if(col2[i].value==true)
					{
							index.push(i)
					}
			}
	}
	if(content[3]==='N')
	{
	  props.cardTable.hideColByKey(tableid,['self_debitgroupamount','other_creditgroupamount','groupamountequal','groupamountbalance']);
	}
	else
	{
			props.cardTable.showColByKey(tableid,['self_debitgroupamount','other_creditgroupamount','groupamountequal','groupamountbalance']);
			let col3 = props.cardTable.getColValue('body','groupamountequal');
			for (let i=0;i<col3.length;i++)
			{
					if(col3[i].value==true)
					{
							index.push(i)
					}
			}
	}
	if(content[4]==='N')
	{
	  props.cardTable.hideColByKey(tableid,['self_debitglobalamount','other_creditglobalamount','globalamountequal','globalamountbalance']);
	}
	else
	{
			props.cardTable.showColByKey(tableid,['self_debitglobalamount','other_creditglobalamount','globalamountequal','globalamountbalance']);
			let col4 = props.cardTable.getColValue('body','globalamountequal');
			for (let i=0;i<col4.length;i++)
			{
					if(col4[i].value==true)
					{
							index.push(i)
					}
			}
	}
	  props.cardTable.hideColByKey(tableid,
    ['self_creditquantity','self_creditamount','self_creditorgamount','self_creditgroupamount','self_creditglobalamount',
    'other_debitquantity','other_debitamount','other_debitorgamount','other_debitgroupamount','other_debitglobalamount',
    'self_quantityoccur','self_amountoccur','self_orgamountoccur','self_groupamountoccur','self_globalamountoccur',
    'other_quantityoccur','other_amountoccur','other_orgamountoccur','other_groupamountoccur','other_globalamountoccur']);
    //设置选中行
    props.cardTable.selectRowsByIndex('body', index);
  }
//贷借
function setCDContrast(content,props) {
	let index = new Array();
	let tableid = 'body'
	if(content[0]==='N')
	{
	  props.cardTable.hideColByKey(tableid,['self_creditquantity','other_debitquantity','quantityequal','quantitybalance']);
	}
	else
	{
			props.cardTable.showColByKey(tableid,['self_creditquantity','other_debitquantity','quantityequal','quantitybalance']);
			let col0 = props.cardTable.getColValue('body','quantityequal');
			for (let i=0;i<col0.length;i++)
			{
					if(col0[i].value==true)
					{
							index.push(i)
					}
			}
	}
	if(content[1]==='N')
	{
	  props.cardTable.hideColByKey(tableid,['self_creditamount','other_debitamount','amountequal','amountbalance']);
	}
	else
	{
			props.cardTable.showColByKey(tableid,['self_creditamount','other_debitamount','amountequal','amountbalance']);
			let col1 = props.cardTable.getColValue('body','amountequal');
			for (let i=0;i<col1.length;i++)
			{
					if(col1[i].value==true)
					{
							index.push(i)
					}
			} 
	}
	if(content[2]==='N')
	{
	  props.cardTable.hideColByKey(tableid,['self_creditorgamount','other_debitorgamount','orgamountequal','orgamountbalance']);
	}
	else
	{
			props.cardTable.showColByKey(tableid,['self_creditorgamount','other_debitorgamount','orgamountequal','orgamountbalance']);
			let col2 = props.cardTable.getColValue('body','orgamountequal');
			for (let i=0;i<col2.length;i++)
			{
					if(col2[i].value==true)
					{
							index.push(i)
					}
			}
	}
	if(content[3]==='N')
	{
	  props.cardTable.hideColByKey(tableid,['self_creditgroupamount','other_debitgroupamount','groupamountequal','groupamountbalance']);
	}
	else
	{
			props.cardTable.showColByKey(tableid,['self_creditgroupamount','other_debitgroupamount','groupamountequal','groupamountbalance']);
			let col3 = props.cardTable.getColValue('body','groupamountequal');
			for (let i=0;i<col3.length;i++)
			{
					if(col3[i].value==true)
					{
							index.push(i)
					}
			}
	}
	if(content[4]==='N')
	{
	  props.cardTable.hideColByKey(tableid,['self_creditglobalamount','other_debitglobalamount','globalamountequal','globalamountbalance']);
	}
	else
	{
			props.cardTable.showColByKey(tableid,['self_creditglobalamount','other_debitglobalamount','globalamountequal','globalamountbalance']);
			let col4 = props.cardTable.getColValue('body','globalamountequal');
			for (let i=0;i<col4.length;i++)
			{
					if(col4[i].value==true)
					{
							index.push(i)
					}
			}
	}
  
		props.cardTable.hideColByKey(tableid,
    ['self_debitquantity','self_debitamount','self_debitorgamount','self_debitgroupamount','self_debitglobalamount',
    'other_creditquantity','other_creditamount','other_creditorgamount','other_creditgroupamount','other_creditglobalamount',
    'self_quantityoccur','self_amountoccur','self_orgamountoccur','self_groupamountoccur','self_globalamountoccur',
    'other_quantityoccur','other_amountoccur','other_orgamountoccur','other_groupamountoccur','other_globalamountoccur']);
    //设置选中行
    props.cardTable.selectRowsByIndex('body', index);
  
  }
  //净发生
  function setBalanceContrast(content,props) {
	let index = new Array();
	let tableid = 'body'
	if(content[0]==='N')
	{
	  props.cardTable.hideColByKey(tableid,['self_quantityoccur','other_quantityoccur','quantityequal','quantitybalance']);
	}
	else
	{
			props.cardTable.showColByKey(tableid,['self_quantityoccur','other_quantityoccur','quantityequal','quantitybalance']);
			let col0 = props.cardTable.getColValue('body','quantityequal');
			for (let i=0;i<col0.length;i++)
			{
					if(col0[i].value==true)
					{
							index.push(i)
					}
			}
	}
	if(content[1]==='N')
	{
	  props.cardTable.hideColByKey(tableid,['self_amountoccur','other_amountoccur','amountequal','amountbalance']);
	}
	else
	{
			props.cardTable.showColByKey(tableid,['self_amountoccur','other_amountoccur','amountequal','amountbalance']);
			let col1 = props.cardTable.getColValue('body','amountequal')
			for (let i=0;i<col1.length;i++)
			{
					if(col1[i].value==true)
					{
							index.push(i)
					}
			}
	}
	if(content[2]==='N')
	{
	  //差额
	  props.cardTable.hideColByKey(tableid,['self_orgamountoccur','other_orgamountoccur','orgamountequal','orgamountbalance']);
	}
	else
	{
			props.cardTable.showColByKey(tableid,['self_debitorgamount','other_creditorgamount','orgamountequal','orgamountbalance']);
			let col2 = props.cardTable.getColValue('body','orgamountequal');
			for (let i=0;i<col2.length;i++)
			{
					if(col2[i].value==true)
					{
							index.push(i)
					}
			}
	}
	if(content[3]==='N')
	{
	  props.cardTable.hideColByKey(tableid,['self_groupamountoccur','other_groupamountoccur','groupamountequal','groupamountbalance']);
	}
	else
	{
			props.cardTable.showColByKey(tableid,['self_groupamountoccur','other_groupamountoccur','groupamountequal','groupamountbalance']);
			let col3 = props.cardTable.getColValue('body','groupamountequal');
			for (let i=0;i<col3.length;i++)
			{
					if(col3[i].value==true)
					{
							index.push(i)
					}
			}
	}
	if(content[4]==='N')
	{
	  props.cardTable.hideColByKey(tableid,['self_globalamountoccur','other_globalamountoccur','globalamountequal','globalamountbalance']);
	}
	else
	{
			props.cardTable.showColByKey(tableid,['self_globalamountoccur','other_globalamountoccur','globalamountequal','globalamountbalance']);
			let col4 = props.cardTable.getColValue('body','globalamountequal');
			for (let i=0;i<col4.length;i++)
			{
					if(col4[i].value==true)
					{
							index.push(i)
					}
			}
	}
  
		props.cardTable.hideColByKey(tableid,
    ['self_debitquantity','self_debitamount','self_debitorgamount','self_debitgroupamount','self_debitglobalamount',
    'self_creditquantity','self_creditamount','self_creditorgamount','self_creditgroupamount','self_creditglobalamount',
    'other_debitamount','other_debitorgamount','other_debitgroupamount','other_debitglobalamount','other_debitquantity',
    'other_creditamount','other_creditorgamount','other_creditgroupamount','other_creditglobalamount','other_creditquantity']);
		//设置选中行
    props.cardTable.selectRowsByIndex('body', index);
  
  }


