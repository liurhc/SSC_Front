import { ajax,toast,cacheTools,deepClone } from 'nc-lightapp-front';

export default function setSelectedData(props, moduleId, record, index) {
    let selectedData = record.values;
    let sumsearchData;
    let detailsearchData;
    if(cacheTools.get('sumqueryvo')){
        sumsearchData = cacheTools.get('sumqueryvo');
        detailsearchData = deepClone(sumsearchData);
    }
    //明细没有规则，直接把汇总的规则传递，用于查询使用
    if(cacheTools.get('sumcontent')){
        cacheTools.set('detailcontent',cacheTools.get('sumcontent'));
        cacheTools.set('detailmain',cacheTools.get('summain')); 
    }
    detailsearchData.pk_currtype=selectedData.pk_currtype.value;
    detailsearchData.assvo=selectedData.assid.value;
    detailsearchData.pk_otheraccountingbook=selectedData.pk_otheraccountingbook.value;
    cacheTools.set('detailqueryvo',detailsearchData);
    this.setState(
        {selectedData}
    )
}
