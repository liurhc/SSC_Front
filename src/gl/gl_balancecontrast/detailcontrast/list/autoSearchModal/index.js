import React, { Component } from 'react';
import {high,base,ajax,cacheTools,getMultiLang,toast} from 'nc-lightapp-front';
const { NCModal:Modal, NCButton:Button,NCRow,NCCol ,NCDatePicker,NCCheckbox,NCTable:Table,
    NCSelect:Select,NCRadio,NCNumber,NCDiv
} = base;
import createScript from '../../../../public/components/uapRefer.js';
import ContrastRuleGridRef from '../../../../refer/voucher/ContrastRuleGridRef/index.js';
import CurrtypeGridRef from '../../../../../uapbd/refer/pubinfo/CurrtypeGridRef/index';
import {InputItem} from '../../../../public/components/FormItems';
import RulesModal from './RulesModal'
import './index.less';
export default class SearchModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            pk_rule:{
                refname:'',
                refpk:''
            },
            pk_currtype:{
                refname:'',
                refpk:''
            },
            pk_selfaccountingbook:{
                refname:'',
                refpk:''
            },
            pk_otheraccountingbook:{
                refname:'',
                refpk:''
            },
            sendData:{},
            searchData:{
                bcontrast :true ,//1对账方式：勾选 ，false不勾选
                //btotalcontrast前端看不到
                amount:true,//2对账种类：金额选择数量选择  
                quantity:true,
                begindate:'',//3制单日期：开始日期
                enddate: '',//截止日期
                isday:false,//4相差天：
                day:'',//勾选，天数: （数字输入框）
                ismoney:true,//金额相差范围,默认勾选
                money:0.01,//5相差金额
                reverse:true,//6方向相反：勾选
                iscurrency:true,//7币种相同：勾选
                isCurrencyDisable:true,//币种可勾选性
                ischeckstyle:false,//8结算方式相同：不勾选
                ischeckno:false,//9结算号相同：不勾选
                isexplanatin:false,//10摘要相同：不勾选
                self_accountingbook:'',//11本方账簿：
                other_accountingbook:'',//12对方账簿：
                pk_rule:'', //13对账规则主键：（内部交易对账规则参照）
                normal:false //14反向选择框：不勾选
            },
            json:{}
        };
        this.close = this.close.bind(this);
        this.open = this.open.bind(this);
    }
    componentWillReceiveProps(nextProps){
        let showModal=nextProps.autoSeachModalShow;
        let searchData=nextProps.searchData;
        this.setState({
            showModal,
            sendData:searchData
        })
    }
    componentWillMount() {
        let callback= (json) =>{
            this.setState({json:json},()=>{
            })
        }
        getMultiLang({moduleId:'20022010',domainName:'gl',currentLocale:'zh-CN',callback});
    }
    close() {
        this.props.onClose();
        // this.setState({
        //     showModal: false
        // });
    }
    handleConfirm=()=>{
        let searchData = this.state.searchData;
        if(!searchData)
		{
		 toast({content:this.state.json['20022010-000052'] ,color:'warning'});   /* 国际化处理： 对账规则不能为空，请知悉*/
		}
		else if(searchData.pk_rule=='' ){
		 toast({content:this.state.json['20022010-000052'] ,color:'warning'});   /* 国际化处理： 对账规则不能为空，请知悉*/
 
		 }else if(searchData.pk_rule!='' && searchData.self_accountingbook==''){   /* 国际化处理:  本方核算账簿不能为空，请知悉*/
			 toast({content:this.state.json['20022010-000053'] ,color:'warning'});
		 }else if(searchData.pk_rule!='' && searchData.self_accountingbook!=''&&searchData.other_accountingbook==''){
			 toast({content:this.state.json['20022010-000054'] ,color:'warning'});    /* 国际化处理:  对方核算账簿不能为空，请知悉*/
         }
         else
         {
            this.setState({
                showModal: false
            });
            this.props.getAutoData(searchData);
            this.props.setautoSeachModalShow(false);
         }
    }
    //获取规则
    getRule=(data)=>{
        let searchData=this.state.searchData;
        searchData.self_accountingbook=data.self_accountingbook.refpk;
        searchData.other_accountingbook=data.other_accountingbook.refpk;
        searchData.pk_rule=data.refpk;
        this.setState({searchData});
    }
   
    open() {
        this.setState({
            showModal: true
        });
    }
    render () {
        let {searchData,pk_rule,pk_currtype,pk_selfaccountingbook,pk_otheraccountingbook}=this.state;
        searchData.begindate = cacheTools.get('detailbegindate');
        searchData.enddate = cacheTools.get('detailenddate');
        searchData.isCurrencyDisable = cacheTools.get('autoCurrencyCheck')==false?false:true;
        return (
        <div>
            <Modal
            fieldid="autobox"
            show = { this.state.showModal }
            size={'lg'}
            onHide = { this.close } 
            className={'allBooksSearch m-mxdzcx'}
            id="autobox"
            >
                <Modal.Header>
                    <Modal.Title>{this.state.json['20022010-000000']}</Modal.Title>{/* 国际化处理： 自动勾对*/}
                </Modal.Header>

                <Modal.Body>
                <NCDiv fieldid="query" areaCode={NCDiv.config.FORM}  className="nc-theme-form-label-c">
                    <NCRow>
                        <NCCol lg={3} sm={3} xs={3}>
                            {this.state.json['20022010-000001']}{/* 国际化处理： 对账方式*/}
                        </NCCol>
                        <NCCol lg={4} sm={4} xs={4}>
                            <NCCheckbox colors="info" name="agree"
                                checked={searchData.bcontrast }
                                disabled
                                onChange={(v)=>{
                                    searchData.bcontrast =v;
                                    this.setState({
                                        searchData
                                    })
                                }}
                                >
                                    {this.state.json['20022010-000002']}{/* 国际化处理： 逐笔勾对*/}
                            </NCCheckbox>
                        </NCCol>
                    </NCRow>
                    <NCRow>
                        <NCCol lg={3} sm={3} xs={3}>
                            {this.state.json['20022010-000003']}{/* 国际化处理： 对账种类*/}
                        </NCCol>
                        <NCCol  className={"dzzl"}  lg={4} sm={4} xs={4}>
                            <NCCheckbox colors="info" name="agree"
                                checked={searchData.quantity }
                                onChange={(v)=>{
                                    searchData.quantity =v;
                                    this.setState({
                                        searchData
                                    })
                                }}
                                >
                                    {this.state.json['20022010-000004']}{/* 国际化处理： 数量*/}
                            </NCCheckbox>
                        </NCCol>
                        <NCCol  className={"dzzl"}  lg={4} sm={4} xs={4}>
                            <NCCheckbox colors="info" name="agree"
                                checked={searchData.amount }
                                onChange={(v)=>{
                                    searchData.amount =v;
                                    this.setState({
                                        searchData
                                    })
                                }}
                                >
                                    {this.state.json['20022010-000005']}{/* 国际化处理： 金额*/}
                            </NCCheckbox>
                        </NCCol>
                    </NCRow>
                    <div className={"m-zdrq-z"}>
                    <NCRow>
                        <NCCol lg={3} sm={3} xs={3}>
                            {this.state.json['20022010-000006']}{/* 国际化处理： 制单日期*/}
                        </NCCol>
                        <NCCol  className={"ri-wid-1"}  lg={4} sm={4} xs={4}>
                            <NCDatePicker
                                fieldid="detailbegindate"
                                value={searchData.begindate}
                                onChange={(v)=>{
                                    searchData.begindate=v;
                                    cacheTools.set('detailbegindate',v);
                                    this.setState({
                                        searchData
                                    })
                                }}
                            />
                        </NCCol>
                        <span className={"zhi_"}>{this.state.json['20022010-000007']}</span>{/* 国际化处理： 至*/}
                        <NCCol  className={"ri-wid-2"}  lg={4} sm={4} xs={4}>
                            <NCDatePicker
                                fieldid="detailenddate"
                                value={searchData.enddate}
                                onChange={(v)=>{
                                    searchData.enddate=v;
                                    cacheTools.set('detailenddate',v);
                                    this.setState({
                                        searchData
                                    })
                                }}
                            />
                        </NCCol>
                    </NCRow>
                    <NCRow>
                        <NCCol className={"hefrs"} lg={4} sm={4} xs={4}>
                            <NCCheckbox colors="info" name="agree"
                                    checked={searchData.isday }
                                    onChange={(v)=>{
                                        searchData.isday =v;
                                        this.setState({
                                            searchData
                                        })
                                    }}
                                    >
                                        {this.state.json['20022010-000008']}{/* 国际化处理： 相差天数*/}
                            </NCCheckbox>
                        </NCCol>
                       
                        <NCCol className={"ri-wid"} lg={3} sm={3} xs={3}>
                            <NCNumber
                                fieldid="datescale"
                                type="customer"
                                name="scale"
                                disabled = {!searchData.isday}
                                value={searchData.day}
                                className={"hzdz-rq"}
                                onChange={(v) => {
                                    searchData.day=v
                                    this.setState({
                                        searchData
                                    })
                                }}
                            />
                        </NCCol>
                    </NCRow>
                    <NCRow>
                         <NCCol  className={"hefrs"}  lg={4} sm={4} xs={4}>
                            <NCCheckbox colors="info" name="agree"
                                    disabled
                                    checked={searchData.ismoney }
                                    className={"hzdz-rq"}
                                    onChange={(v)=>{
                                        searchData.ismoney =v;
                                        this.setState({
                                            searchData
                                        })
                                    }}
                                    >
                                        {this.state.json['20022010-000009']}{/* 国际化处理： 金额差额范围*/}
                            </NCCheckbox>
                        </NCCol>
                        <NCCol lg={1} sm={1} xs={1}>
                            &lt;
                        </NCCol>
                        <NCCol className={"ri-wid"}  lg={3} sm={3} xs={3}>
                            <InputItem
                                fieldid="amountscale"
                                type="customer"
                                className={"hzdz-rq"}
                                name="scale"
                                disabled
                                defaultValue={searchData.money}
                            />
                        </NCCol>
                    </NCRow>                                        

                    <NCRow>
                        <NCCol  className={"hefrs"}  lg={4} sm={4} xs={4}>
                            <NCCheckbox colors="info" name="agree"
                                    checked={searchData.reverse }
                                    className={"hzdz-rq"}
                                    disabled
                                    onChange={(v)=>{
                                        searchData.reverse =v;
                                        this.setState({
                                            searchData
                                        })
                                    }}
                                    >
                                        {this.state.json['20022010-000010']}{/* 国际化处理： 方向相反*/}
                            </NCCheckbox>
                        </NCCol>
                        <NCCol lg={4} sm={4} xs={4}>
                            <NCCheckbox colors="info" name="agree"
                                    checked={searchData.iscurrency }
                                    disabled = {searchData.isCurrencyDisable}
                                    onChange={(v)=>{
                                        searchData.iscurrency =v;
                                        this.setState({
                                            searchData
                                        })
                                    }}
                                    >
                                        {this.state.json['20022010-000011']}{/* 国际化处理： 币种相同*/}
                            </NCCheckbox>
                        </NCCol>
                    </NCRow>
                    <NCRow>
                        <NCCol  className={"hefrs"}  lg={4} sm={4} xs={4}>
                            <NCCheckbox colors="info" name="agree"
                            className={"hzdz-rq"}
                                    checked={searchData.ischeckstyle }
                                    onChange={(v)=>{
                                        searchData.ischeckstyle =v;
                                        this.setState({
                                            searchData
                                        })
                                    }}
                                    >
                                        {this.state.json['20022010-000012']}{/* 国际化处理： 结算方式相同*/}
                            </NCCheckbox>
                        </NCCol>
                        <NCCol lg={4} sm={4} xs={4}>
                            <NCCheckbox colors="info" name="agree"
                                    checked={searchData.ischeckno }
                                    onChange={(v)=>{
                                        searchData.ischeckno =v;
                                        this.setState({
                                            searchData
                                        })
                                    }}
                                    >   
                                        {this.state.json['20022010-000013']}{/* 国际化处理： 结算号相同*/}
                            </NCCheckbox>
                        </NCCol>
                        <NCCol lg={4} sm={4} xs={4}>
                            <NCCheckbox colors="info" name="agree"
                                    checked={searchData.isexplanatin }
                                    onChange={(v)=>{
                                        searchData.isexplanatin =v;
                                        this.setState({
                                            searchData
                                        })
                                    }}
                                    >   
                                        {this.state.json['20022010-000014']}{/* 国际化处理： 摘要相同*/}
                            </NCCheckbox>
                        </NCCol>
                    </NCRow>
                    </div>
                    </NCDiv>
                </Modal.Body>

                <Modal.Footer>
                <NCRow className="rowbott">
                        <NCCol lg={6} sm={6} xs={6}>
                            <RulesModal
                                
                                sendSearchData={searchData}
                                getRule={this.getRule.bind(this)}
                                defaultAutoData={this.props.defaultAutoData}
                            />
                        </NCCol>
                        <NCCol lg={6} sm={6} className="second">
                            <NCCheckbox colors="info" name="agree"
                                checked={searchData.normal }
                                onChange={(v)=>{
                                    searchData.normal =v;
                                    this.setState({
                                        searchData
                                    })
                                }}
                                >
                                    {this.state.json['20022010-000015']}{/* 国际化处理： 反向*/}
                            </NCCheckbox>
                        </NCCol>
                    </NCRow>
                    <Button fieldid="confirm" onClick={ this.handleConfirm } colors="primary" className="besure">{this.state.json['20022010-000016']}</Button>{/* 国际化处理： 确定*/}
                    <Button fieldid="cancel" onClick={ this.close } shape="border" className="becancel">{this.state.json['20022010-000017']}</Button>{/* 国际化处理： 取消*/}
                </Modal.Footer>
           </Modal>
        </div>
        )
    }
}
