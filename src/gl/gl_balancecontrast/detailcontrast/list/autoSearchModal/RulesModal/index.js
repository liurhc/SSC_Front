import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {high,base,ajax,cacheTools,getMultiLang,toast} from 'nc-lightapp-front';
import createScript from '../../../../../public/components/uapRefer.js';
const { NCModal,NCButton,NCRadio,NCTable,NCRow,NCCol } = base;
const { Refer } = high;
import './index.less';
import ReferLoader from "../../../../../public/ReferLoader/index.js";
export default class RulesModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            selectData:null,
            tableData:[],
            defaultAutoData:null,
            json:{},
            correctIndex:null,
            selectedRow: null // 当前选中行
        };
        this.close = this.close.bind(this);
        this.columns = [];
    }
    componentWillMount() {
        let callback= (json) =>{
            this.setState({json:json},()=>{
                this.columns =[
                    {
                      title: (<div fieldid="refcode">{this.state.json['20022010-000018']}</div>),/* 国际化处理： 规则编码*/
                      dataIndex: "refcode",
                      key: "refcode",
                      width: 130,
                      render: (text, record, index) => (
                        <div fieldid="refcode">{record.refcode}</div>
                      )
                    },
                    {
                      title: (<div fieldid="pk_creditaccasoa">{this.state.json['20022010-000019']}</div>),/* 国际化处理： 规则名称*/
                      dataIndex: "pk_creditaccasoa",
                      key: "pk_creditaccasoa",
                      width: 130,
                      render: (text, record, index) => (
                        <div fieldid="pk_creditaccasoa">{record.refname}</div>
                      )
                    },
                    {
                      title: (<div fieldid="self_accountingbook">{this.state.json['20022010-000020']}</div>),/* 国际化处理： 本方账簿*/
                      dataIndex: "self_accountingbook",
                      key: "self_accountingbook",
                      width:300,
                      render: (text, record, index) => (
                        this.getBooks(record,'self',index)
                      )
                    },
                    {
                      title: (<div fieldid="other_accountingbook">{this.state.json['20022010-000021']}</div>),/* 国际化处理： 对方账簿*/
                      dataIndex: "other_accountingbook",
                      key: "other_accountingbook",
                      width:300,
                      render: (text, record, index) => (
                        this.getBooks(record,'other',index)
                      )
                    }
                  ];
            })
        }
        getMultiLang({moduleId:'20022010',domainName:'gl',currentLocale:'zh-CN',callback});
    }
    componentDidUpdate(){
    }


    componentDidMount(){
        let url= '/nccloud/gl/contrast/contrastruleref.do';
        let self = this;
        let sendData = self.props.defaultAutoData;
        ajax({
            url,
            data: {"pid":"","keyword":"","queryCondition":{startstatus:'All'},"pageInfo":{"pageSize":50,"pageIndex":0}},
            success: (res) => { 
                if (res.data) {
                    let localData = res.data.rows;
                    let queryvo = self.props.sendSearchData;                    ;
                    if(sendData==null)
                    {
                        
                        for(let i=0;i<localData.length;i++){
                            localData[i].self_accountingbook={
                                refname:'',
                                refpk:''
                            }
                            localData[i].other_accountingbook={
                                refname:'',
                                refpk:''
                            } 
                        } 
                    }
                    else
                    {
                        cacheTools.set("autopk",sendData.pk_rule.refpk);
                        queryvo.pk_rule=sendData.pk_rule.refpk;
                        queryvo.self_accountingbook = sendData.pk_selfaccountingbook.refpk;                
                        queryvo.other_accountingbook = sendData.pk_otheraccountingbook.refpk;
                        for(let i=0;i<localData.length;i++){
                            if(localData[i].refpk==sendData.pk_rule.refpk)
                            {
                                self.setState({
                                    correctIndex:i,
                                    selectedRow: i,
                                    selectData:localData[i]
                                },()=>{
                                })
                                localData[i].self_accountingbook={
                                    refname:sendData.pk_selfaccountingbook.refname,
                                    refpk:sendData.pk_selfaccountingbook.refpk
                                }
                                localData[i].other_accountingbook={
                                    refname:sendData.pk_otheraccountingbook.refname,
                                    refpk:sendData.pk_otheraccountingbook.refpk,
                                } 
                            }
                            else
                            {
                                localData[i].self_accountingbook={
                                    refname:'',
                                    refpk:''
                                }
                                localData[i].other_accountingbook={
                                    refname:'',
                                    refpk:''
                                }
                            }
                        }
                    }
                    self.setState({
                        tableData:localData
                    })
                }
            }
        })
    }
    componentWillReceiveProps(nextProps){
     
    }
    getBooks=(data,dir,index)=>{
        let referUrl = 'uapbd/refer/org/AccountBookTreeRef' + '/index.js';
		 if(!this.state[data.record+dir]){
		 	{createScript.call(this,referUrl,data.record+dir)}
		 }else{
            if(dir=='self'){
                return (
                    <ReferLoader
                    fieldid="self_accountingbook"
                    tag = 'test'
                    refcode = 'uapbd/refer/org/AccountBookTreeRef/index.js'
                    value = {data.self_accountingbook}
                    queryCondition = {()=>{
                      if(cacheTools.get("autopk"))
                      {
                          let condition = {
                              "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilderForContarstRule',
                              pk_contrastrule:cacheTools.get("autopk"),
                              appcode: cacheTools.get('detailappcode'),
                              isSelf:'Y'
                              }
                              return condition;
                      }
                      else
                      {
                          let condition = {
                              "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilderForContarstRule',
                              appcode: cacheTools.get('detailappcode')
                              }
                              return condition;
                      }
                       
                    return condition;
                    
                    }}
                    onChange ={(v)=>{
                                     let tableData=this.state.tableData;
                                     tableData[index].self_accountingbook.refname=v.refname;
                                     tableData[index].self_accountingbook.refpk=v.refpk;
                                     this.setState({
                                          tableData
                                     })
                                }}
                    
                />
                )
            }
			else if(dir=='other'){
                return (
                    <ReferLoader
                    fieldid="other_accountingbook"
                    tag = 'test'
                    refcode = 'uapbd/refer/org/AccountBookTreeRef/index.js'
                    value = {data.other_accountingbook}
                    isMultiSelectedEnabled = {false}
                    showGroup = {cacheTools.get('autoShowGroup')}
                    showInCludeChildren = {cacheTools.get('autoShowGroup')}
                    queryCondition = {()=>{
                        if(cacheTools.get("autopk"))
                        {
                            let condition = {
                                "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilderForContarstRule',
                                pk_contrastrule:cacheTools.get("autopk"),
                                appcode: cacheTools.get('detailappcode'),
                                isSelf:'N',
                                showGroup:cacheTools.get('autoShowGroup'),
                                refpkGroup:-1
                                }
                                return condition;
                        }
                        else
                        {
                            let condition = {
                                "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilderForContarstRule',
                                appcode: cacheTools.get('detailappcode'),
                                }
                                return condition;
                        }
                       
                    return condition;
                    
                    }}
                    onChange ={(v)=>{
                                     let tableData=this.state.tableData;
                                     tableData[index].other_accountingbook.refname=v.refname;
                                     tableData[index].other_accountingbook.refpk=v.refpk;
                                     this.setState({
                                          tableData
                                     })
                                }}
                />
                )
            }	
		}
    }
    close() {
        this.setState({
            showModal:false
        })
    }
    open() {
        this.setState({
            showModal:true
        })
    }
    clickRow=(data,rowClassName)=>{
        this.setState(
            {
                selectData:data
            }
        )
    }
    handleConfirm=()=>{
       let selectData = this.state.selectData;
       if(!selectData)
       {
        toast({content:this.state.json['20022010-000052'] ,color:'warning'});   /* 国际化处理： 对账规则不能为空，请知悉*/
       }
       else if(selectData.refpk=='' ){
        toast({content:this.state.json['20022010-000052'] ,color:'warning'});   /* 国际化处理： 对账规则不能为空，请知悉*/

        }else if(selectData.refpk!='' && selectData.self_accountingbook.refpk==''){   /* 国际化处理:  本方核算账簿不能为空，请知悉*/
            toast({content:this.state.json['20022010-000053'] ,color:'warning'});
        }else if(selectData.refpk!='' && selectData.self_accountingbook.refpk!=''&&selectData.other_accountingbook.refpk==''){
            toast({content:this.state.json['20022010-000054'] ,color:'warning'});    /* 国际化处理:  对方核算账簿不能为空，请知悉*/
        }
        else
        {
            this.setState({
                showModal:false
           })
           this.props.getRule(selectData)
        }
    }
    getRow = (record,index) => {

        ajax({
            url: '/nccloud/gl/sumcontrast/rulecontent.do',
            data: record.refpk,
            success: (res) => {
                if(res.data)
                {
                    cacheTools.set('autoShowGroup',res.data.showGroup);
                }
            }
        })
        cacheTools.set("autopk",record.refpk); 
        this.setState(
            {
                selectData:record
            }
        )
        this.setState({
            selectedRow: index
        })
	};
    render () {
        let {tableData,selectData,selectedRow}=this.state;
        let columns = this.columns;
        return (
            <div >
              <NCButton
                fieldid="contrastrule"
                className="demo-margin"
                onClick = { this.open.bind(this) }>
                    {this.state.json['20022010-000022']}{/* 国际化处理： 内部交易对账规则*/}
              </NCButton>​
              <NCModal show = {
                  this.state.showModal
              }
              fieldid="contrastrule"
              className='explanModal'
              backdrop={ false }
              size={'lg'}
			  >
                <NCModal.Header>
                  <NCModal.Title>{this.state.json['20022010-000022']}</NCModal.Title>{/* 国际化处理： 内部交易对账规则*/}
                </NCModal.Header>

                <NCModal.Body >
                    <div fieldid="contrastrule">
                        <NCTable
                                columns={columns}
                                data={tableData}
                                //onRowClick={this.clickRow}
                                scroll={{ x:false, y: 200 }}
                                //TODO 有时会不显示最后的数据
                                style={{'height':'250px'}}
                                onRowClick={this.getRow}
                                className="table_tr_body"
                                rowClassName = {(record, index, indent)=>{
                                    // 平台提供选中行类名nctable-selected-row
                                    return selectedRow === index ? 'nctable-selected-row' : ''
                                }}
                        />
                   </div>
                </NCModal.Body>

                <NCModal.Footer id="sureorcan">
                  <NCButton fieldid="confirm" onClick={this.handleConfirm} color="primary" className="confirma">{this.state.json['20022010-000023']}</NCButton>{/* 国际化处理： 确定*/}
                  <NCButton fieldid="cancel" onClick={this.close}>{this.state.json['20022010-000024']}</NCButton>{/* 国际化处理： 取消*/}
                </NCModal.Footer>

              </NCModal>
            </div>
        )
    }
}

