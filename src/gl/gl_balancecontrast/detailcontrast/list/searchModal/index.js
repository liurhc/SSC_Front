import React, { Component } from 'react';
import {high,base,toast,ajax,cacheTools,getMultiLang} from 'nc-lightapp-front';
const { NCModal:Modal, NCButton:Button,NCRow,NCCol ,NCDatePicker,NCCheckbox,NCTable:Table,
    NCSelect:Select,NCRadio,NCDiv
} = base;
import createScript from '../../../../public/components/uapRefer.js';
import ContrastRuleGridRef from '../../../../refer/voucher/ContrastRuleGridRef/index.js';
import CurrtypeGridRef from '../../../../../uapbd/refer/pubinfo/CurrtypeGridRef/index';
import {InputItem,} from '../../../../public/components/FormItems';
import Store from "../../../../public/components/GlobalStore.js";
import ReferLoader from "../../../../public/ReferLoader/index.js";
import './index.less';
export default class SearchModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            content:'',
            showModal: false,
            pk_rule:{
                refname:'',
                refpk:'',
            },
            pk_currtype:{
                refname:'',
                refpk:''
            },
            pk_selfaccountingbook:{
                refname:'',
                refpk:'',
            },
            pk_otheraccountingbook:{
                refname:'',
                refpk:'',
            },
            searchData:{
                pk_rule:'',//对账规则主键
                rulename:'',//对账规则名称
                ruledirection:true,//规则方向 正向true 反向false 
                pk_currtype:'', //币种
                pk_selfaccountingbook:'',//（本方核算账簿：只能单选，有默认值）
                pk_otheraccountingbook:'',//（对方核算账簿：只能单选无默认值）
                assvo:'',//辅助核算选择： 用户不选择就是’’ 
                amount:false,//对账种类：金额选择
                amountdisable:true,//金额控件的不可用性，true不可用，false可用
                quantity:false,//数量选择  
                quantitydisable:true,//数量控件的不可用性，true不可用，false可用
                begindate:'',//日期：开始日期
                enddate:'',//结束日期  
                range :'1',//对符范围：1为对符2已对符3全部
                //contraststyle :null, //对账方式：1借贷 2贷借3净发生 //明细查询没有这个字段
                date:'Y',//默认是Y 
                day:'',//天数
                reacheddate:'',//截止日期
            },
            json:{}
        };
        this.close = this.close.bind(this);
        this.open = this.open.bind(this);
    }
    componentDidMount(){
    
    }
    componentWillMount() {
        let callback= (json) =>{
            this.setState({json:json},()=>{
            })
        }
        getMultiLang({moduleId:'20022010',domainName:'gl',currentLocale:'zh-CN',callback});
    }
    componentWillReceiveProps(nextProps){
        let showModal=nextProps.seachModalShow;
        this.setState({
            showModal
        })
    }
    close() {
        this.props.setsearchModalShow(false);
    }
    handleConfirm=()=>{
        let localProps=this.props.props;
        let searchData = this.state.searchData;
        cacheTools.set('detailqueryvo',searchData);
        if(searchData.pk_rule=='' ){
            toast({content:this.state.json['20022010-000052'] ,color:'warning'});   /* 国际化处理： 对账规则不能为空，请知悉*/

        }else if(searchData.pk_rule!='' && searchData.pk_selfaccountingbook==''){   /* 国际化处理:  本方核算账簿不能为空，请知悉*/
            toast({content:this.state.json['20022010-000053'] ,color:'warning'});
        }else if(searchData.pk_rule!='' && searchData.pk_selfaccountingbook!=''&&searchData.pk_otheraccountingbook==''){
            toast({content:this.state.json['20022010-000054'] ,color:'warning'});    /* 国际化处理:  对方核算账簿不能为空，请知悉*/
        }else if(searchData.begindate == null || searchData.enddate == null ){
            toast({content:this.state.json['20022010-000056'] ,color:'warning'});    /* 国际化处理:  日期不能为空，请知悉*/
        } else{
            this.props.setsearchModalShow(false);
            this.props.getSearchData(searchData);
            this.getInitData(searchData);
            let {pk_rule,pk_otheraccountingbook,pk_selfaccountingbook}=this.state;
            this.props.sendToAuto({
                pk_rule,
                pk_otheraccountingbook,
                pk_selfaccountingbook
            });
        }


        
    }
    //获取查询值
    getInitData=(searchData)=>{
        let self=this;
        let localProps = this.props.props;
        //设置按钮的可用性
        let content = cacheTools.get('detailmain');
        if(content=='N')
        {
            localProps.button.setButtonDisabled('make',false);
        }
        else
        {
            localProps.button.setButtonDisabled('make',true);
        }
        ajax({
            url: '/nccloud/gl/detailcontrast/show.do',
            data: searchData,
            success: (res) => { 
                if(res.data){
                    self.props.setContentValue(res.data.content)
                    let head = {
                        'contrastRule':{value:res.data.contrastRule,display:null},
                        'dateArea':{value:res.data.dateArea,display:null},
                        'ass':{value:res.data.ass,display:null},
                        'corpKind':{value:res.data.corpKind,display:null}
                    }
                    let subhead1 = {
                        'selfAccBook':{value:res.data.selfAccBook,display:res.data.selfAccBook},
                        'selfaccounts':{value:res.data.selfaccounts,display:null}
                    }
                    let subhead2 = {
                        'otherAccBook':{value:res.data.otherAccBook,display:null},
                        'otheraccounts':{value:res.data.otheraccounts,display:null}
                    }
                    //设置表头数据
                    localProps.form.setFormItemsValue('head',head);
                    //本方表头
                    localProps.form.setFormItemsValue('subhead1', subhead1);
                    //对方表头
                    localProps.form.setFormItemsValue('subhead2', subhead2);               
                }
            }
        })
        ajax({
        url: '/nccloud/gl/detailcontrast/query.do',
        data: searchData,
        success: (res) => { 
            if (res.data) {
                localProps.button.setButtonDisabled(['refresh','hand','compare'],false);
                if(res.data['subbody1'])
                {  
                    localProps.cardTable.setTableData('subbody1', res.data['subbody1'].subbody1);
                }
                else
                {
                    localProps.cardTable.setTableData('subbody1', {rows:[]});
                }
                if(res.data['subbody2'])
                {
                    localProps.cardTable.setTableData('subbody2', res.data['subbody2'].subbody2);
                }
                else
                {
                    localProps.cardTable.setTableData('subbody2', {rows:[]});
                }
                //隐藏字段
                this.setQueryContent(searchData.amount,searchData.quantity,cacheTools.get('detailcontent'));
            }
            else
            {
                //设置按钮可用性
                localProps.button.setButtonDisabled('refresh',false);
                localProps.button.setButtonDisabled(['hand','compare'],true);
                toast({ color: 'warning', content: this.state.json['20022010-000036'] });/* 国际化处理： 未查询出符合条件的数据！*/
                localProps.cardTable.setTableData('subbody1', {rows:[]});
                localProps.cardTable.setTableData('subbody2', {rows:[]});
            }
        }
        })
    }
   //设置隐藏列
   setQueryContent = (amount,quantity,content) => {
        let tableid1 = 'subbody1';
        let tableid2 = 'subbody2'
        let localProps=this.props.props;
        if(amount==true&&quantity==true)
        {
            localProps.cardTable.showColByKey(tableid1,['quantityequal','amountequal']);
            localProps.cardTable.showColByKey(tableid2,['quantityequal','amountequal']);
        }
        if(amount==true&&quantity==false)
        {
            localProps.cardTable.hideColByKey(tableid1,'quantityequal');
            localProps.cardTable.hideColByKey(tableid2,'quantityequal');
            localProps.cardTable.showColByKey(tableid1,'amountequal');
            localProps.cardTable.showColByKey(tableid2,'amountequal');
        }
        else if (amount==false&&quantity==true)
        {
            localProps.cardTable.hideColByKey(tableid1,'amountequal');
            localProps.cardTable.hideColByKey(tableid2,'amountequal');
            localProps.cardTable.showColByKey(tableid1,'quantityequal');
            localProps.cardTable.showColByKey(tableid2,'quantityequal');
        }

        if(content[0]==='N')
        {
            localProps.cardTable.hideColByKey(tableid1,['debitquantity','creditquantity']);
            localProps.cardTable.hideColByKey(tableid2,['debitquantity','creditquantity']);
        }
        else
        {
            localProps.cardTable.showColByKey(tableid1,['debitquantity','creditquantity']);
            localProps.cardTable.showColByKey(tableid2,['debitquantity','creditquantity']);
        }
        if(content[1]==='N')
        {
            localProps.cardTable.hideColByKey(tableid1,['debitamount','creditamount']);
            localProps.cardTable.hideColByKey(tableid2,['debitamount','creditamount']);
        }
        else
        {
            localProps.cardTable.showColByKey(tableid1,['debitamount','creditamount']);
            localProps.cardTable.showColByKey(tableid2,['debitamount','creditamount']);
        }
        if(content[2]==='N')
        {
            localProps.cardTable.hideColByKey(tableid1,['debitorgamount','creditorgamount']);
            localProps.cardTable.hideColByKey(tableid2,['debitorgamount','creditorgamount']);
        }
        else
        {
            localProps.cardTable.showColByKey(tableid1,['debitorgamount','creditorgamount']);
            localProps.cardTable.showColByKey(tableid2,['debitorgamount','creditorgamount']);
        }
        if(content[3]==='N')
        {
            localProps.cardTable.hideColByKey(tableid1,['debitgroupamount','creditgroupamount']);
            localProps.cardTable.hideColByKey(tableid2,['debitgroupamount','creditgroupamount']);
        }
        else
        {
            localProps.cardTable.showColByKey(tableid1,['debitgroupamount','creditgroupamount']);
            localProps.cardTable.showColByKey(tableid2,['debitgroupamount','creditgroupamount']);
        }
        if(content[4]==='N')
        {
            localProps.cardTable.hideColByKey(tableid1,['debitglobalamount','creditglobalamount']);
            localProps.cardTable.hideColByKey(tableid2,['debitglobalamount','creditglobalamount']);
        }
        else
        {
            localProps.cardTable.showColByKey(tableid1,['debitglobalamount','creditglobalamount']);
            localProps.cardTable.showColByKey(tableid2,['debitglobalamount','creditglobalamount']);
        }

    }
    open() {
        this.setState({
            showModal: true
        });
    }
    render () {
        let {searchData,pk_rule,pk_currtype,pk_selfaccountingbook,pk_otheraccountingbook}=this.state;
        let {defaultAccbook} = this.props;
        pk_selfaccountingbook.refname = cacheTools.get('detailaccountingbookname') || '';
        pk_selfaccountingbook.refpk = cacheTools.get('detailaccountingbookpk')|| '';
        searchData.pk_selfaccountingbook = cacheTools.get('detailaccountingbookpk') || '';
        pk_otheraccountingbook.refname = cacheTools.get('detailotheraccountingbookname')||'';
        pk_otheraccountingbook.refpk = cacheTools.get('detailotheraccountingbookpk')||'';
        searchData.pk_otheraccountingbook = cacheTools.get('detailotheraccountingbookpk')||'';
        pk_currtype = cacheTools.get('detailpk_currtype')||'';
        searchData.pk_currtype = cacheTools.get('detailpk_currtypepk')||'';
        searchData.begindate = cacheTools.get('detailbegindate');
        searchData.enddate = cacheTools.get('detailenddate');
        searchData.reacheddate = cacheTools.get('detailreacheddate');
        return (
        <div>
            <Modal 
                fieldid="search"
                onHide={this.close}
                animation={true}   
                className = {"detailcontrast m-mxdzcx"}
                show = { this.state.showModal }
            >
                <Modal.Header  closeButton fieldid="header-area">
                    <Modal.Title>{this.state.json['20022010-000037']}</Modal.Title>{/* 国际化处理： 明细对账查询*/}
                    {/* <i class="uf uf-close" style={{position:"absolute",right:"20px",fontSize:"14px"}} onClick={ this.close }></i> */}
                </Modal.Header>

                <Modal.Body>
                <NCDiv fieldid="query" areaCode={NCDiv.config.FORM}  className="nc-theme-form-label-c">
                    <NCRow>
                        
                        <NCCol lg={3} sm={3} xs={3}>
                        <span style={{"color":"#f22c1d","margin-right":"2px"}}>*</span>
                            {this.state.json['20022010-000038']}{/* 国际化处理： 对账规则*/}
                        </NCCol>
                        <NCCol lg={9} sm={9} xs={9}>
                            <ContrastRuleGridRef
                                fieldid="contrastrule"
                                queryCondition ={(v)=>{
                                    return {startstatus:'All'};
                                }}
                                value={pk_rule}
                                onChange={(v)=>{
                                    pk_rule=v
                                    if(pk_rule.refpk)
                                    {
                                        searchData.pk_rule=v.refpk;
                                        searchData.rulename=v.refname;
                                        ajax({
                                            url: '/nccloud/gl/sumcontrast/rulecontent.do',
                                            data: pk_rule.refpk,
                                            success: (res) => {
                                            //返回规则中的内容
                                            //对规则的查询统一在切换规则的时候做
                                            if(res.data)
                                            {
                                                let main=res.data.main;
                                                cacheTools.set('detailmain',main);
                                                let content=res.data.content;
                                                cacheTools.set('detailcontent',content);
                                                cacheTools.set('detailShowGroup',res.data.showGroup);
                                                cacheTools.set('autoCurrencyCheck',res.data.currencyCheck);
                                                this.forceUpdate();
                                                if(content[0]=='Y')
                                                {
                                                    searchData.quantity=true;
                                                    searchData.quantitydisable=false;
                                                }
                                                else
                                                {
                                                    searchData.quantity=false;
                                                    searchData.quantitydisable=true;
                                                }
                                                if(content[1]=='Y'||content[2]=='Y'||content[3]=='Y'||content[4]=='Y')
                                                {
                                                    searchData.amount=true;
                                                    searchData.amountdisable=false;
                                                }
                                                else
                                                {
                                                    searchData.amount=false;
                                                    searchData.amountdisable=true;
                                                }
                                                this.setState({
                                                    searchData
                                                })
                                            }
                                            }
                                        })
                                    }
                                    else
                                    {
                                        searchData.pk_rule='';
                                        searchData.rulename='';
                                    }
                                    cacheTools.set('detailaccountingbookname',defaultAccbook.refname);
                                    cacheTools.set('detailaccountingbookpk',defaultAccbook.refpk);
                                    cacheTools.set('detailotheraccountingbookname',null);
                                    cacheTools.set('detailotheraccountingbookpk',null);
                                    cacheTools.set('detailpk_rule',pk_rule);
                                    this.setState({
                                        pk_rule,searchData
                                    })
                                }
                            }
                            />
                        </NCCol>
                    </NCRow>
                    <NCRow>
                        <NCCol lg={3} sm={3} xs={3}>
                            {this.state.json['20022010-000039']}{/* 国际化处理： 规则方向*/}
                        </NCCol>
                        <NCCol lg={9} sm={9} xs={9}>
                            <NCRadio.NCRadioGroup
                                name="12"
                                selectedValue={searchData.ruledirection}
                                onChange={(v)=>{
                                    searchData.ruledirection=v;
                                    this.setState({
                                        searchData
                                    })
                                }}>
                                <NCRadio  value={true}>{this.state.json['20022010-000040']}</NCRadio>{/* 国际化处理： 正向*/}
                                <NCRadio  value={false}>{this.state.json['20022010-000015']}</NCRadio>{/* 国际化处理： 反向*/}
                            </NCRadio.NCRadioGroup>
                        </NCCol>
                    </NCRow>
                    <NCRow>
                        <NCCol lg={3} sm={3} xs={3}>
                            {this.state.json['20022010-000041']}{/* 国际化处理： 币种*/}
                        </NCCol>
                        <NCCol lg={9} sm={9} xs={9}>
                            <CurrtypeGridRef
                                fieldid="currtype"
                                value={pk_currtype}
                                onChange={(v)=>{
                                    pk_currtype=v,
                                    searchData.pk_currtype=v.refpk;
                                    cacheTools.set('detailpk_currtype',pk_currtype);
                                    cacheTools.set('detailpk_currtypepk',v.refpk);
                                    this.setState({
                                        pk_currtype,searchData
                                    })
                                }}
                            />
                        </NCCol>
                    </NCRow>
                    <NCRow>
                    
                        <NCCol lg={3} sm={3} xs={3}>
                        <span style={{"color":"#f22c1d","margin-right":"2px"}}>*</span>
                            {this.state.json['20022010-000042']}{/* 国际化处理： 本方核算账簿*/}
                        </NCCol>
                        <NCCol lg={9} sm={9} xs={9}>
                            <ReferLoader
                                fieldid="accountbook"
                                tag = 'selfbook'
                                refcode = 'uapbd/refer/org/AccountBookTreeRef/index.js'
                                value = {{refname:pk_selfaccountingbook.refname,refpk:pk_selfaccountingbook.refpk}}
                                isMultiSelectedEnabled = {false}
                                showGroup = {false}
                                showInCludeChildren = {false}
                                queryCondition = {()=>{
                                     if(pk_rule)
                                     {
                                         let condition = {
                                             "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilderForContarstRule',
                                             pk_contrastrule:pk_rule.refpk,
                                             appcode: cacheTools.get('detailappcode'),
                                             isSelf:'Y'
                                             }
                                             return condition;
                                     }
                                     else
                                     {
                                         let condition = {
                                             "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilderForContarstRule',
                                             appcode: cacheTools.get('detailappcode')
                                             }
                                             return condition;
                                     }
                                }}
                                onChange ={(v)=>{
                                                pk_selfaccountingbook.refname=v.refname;
                                                pk_selfaccountingbook.refpk=v.refpk;
                                                searchData.pk_selfaccountingbook = v.refpk;
                                                cacheTools.set('detailaccountingbookname',v.refname);
                                                cacheTools.set('detailaccountingbookpk',v.refpk);
                                                this.setState({
                                                    pk_selfaccountingbook,
                                                    searchData
                                                })
                                            }}
                                        
                            />
                        </NCCol>
                    </NCRow>
                    <NCRow>
                        
                        <NCCol lg={3} sm={3} xs={3}>
                        <span style={{"color":"#f22c1d","margin-right":"2px"}}>*</span>
                            {this.state.json['20022010-000043']}{/* 国际化处理： 对方核算账簿*/}
                        </NCCol>
                        <NCCol lg={9} sm={9} xs={9}>
                            <ReferLoader
                                fieldid="accountbook1"
                                tag = 'othersbook'
                                refcode = 'uapbd/refer/org/AccountBookTreeRef/index.js'
                                value = {{refname:pk_otheraccountingbook.refname,refpk:pk_otheraccountingbook.refpk}}
                                isMultiSelectedEnabled = {false}
                                showGroup = {cacheTools.get('detailShowGroup')}
                                showInCludeChildren = {cacheTools.get('detailShowGroup')}
                                queryCondition = {()=>{
                                if(pk_rule)
                                {
                                    let condition = {
                                        "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilderForContarstRule',
                                        pk_contrastrule:pk_rule.refpk,
                                        appcode: cacheTools.get('detailappcode'),
                                        isSelf:'N',
                                        showGroup :cacheTools.get('detailShowGroup'),
                                        multiGroup:'Y'
                                        }
                                        return condition;
                                }
                                else
                                {
                                    let condition = {
                                        "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilderForContarstRule',
                                        appcode: cacheTools.get('detailappcode')
                                        }
                                        return condition;
                                }

                                }}
                                onChange ={(v)=>{
                                                pk_otheraccountingbook.refname=v.refname;
                                                pk_otheraccountingbook.refpk=v.refpk;
                                                searchData.pk_otheraccountingbook = v.refpk;
                                                cacheTools.set('detailotheraccountingbookname',v.refname);
                                                cacheTools.set('detailotheraccountingbookpk',v.refpk);
                                                this.setState({
                                                    pk_otheraccountingbook,
                                                    searchData
                                                })
                                            }}
                                        
                            />
                        </NCCol>
                    </NCRow>
                    <NCRow>
                        <NCCol lg={3} sm={3} xs={3}>
                            {this.state.json['20022010-000003']}{/* 国际化处理： 对账种类*/}
                        </NCCol>
                        <NCCol className={"dzzl"} lg={4} sm={4} xs={4}>
                            <NCCheckbox colors="info" name="agree"
                                checked={searchData.amount}
                                disabled={searchData.amountdisable}
                                onChange={(v)=>{
                                    searchData.amount=v;
                                    this.setState({
                                        searchData
                                    })
                                }}
                                >
                                    {this.state.json['20022010-000005']}{/* 国际化处理： 金额*/}
                            </NCCheckbox>
                        </NCCol>
                        <NCCol  className={"dzzl"}  lg={4} sm={4} xs={4}>
                            <NCCheckbox colors="info" name="agree"
                            checked={searchData.quantity}
                                checked={searchData.quantity}
                                disabled={searchData.quantitydisable}
                                onChange={(v)=>{
                                    searchData.quantity=v;
                                    this.setState({
                                        searchData
                                    })
                                }}
                                >
                                    {this.state.json['20022010-000004']}{/* 国际化处理： 数量*/}
                            </NCCheckbox>
                        </NCCol>
                    </NCRow>
                    <NCRow>
                        <NCRadio.NCRadioGroup
                            name="fruit"
                            selectedValue={searchData.date}
                            className='dateSearchModel'
                            onChange={(v)=>{
                                searchData.date=v;
                                this.setState({
                                    searchData
                                })
                            }}>
                        <NCRow>
                            <NCCol lg={1} sm={1} xs={1}>
                                <NCRadio value={'Y'} ></NCRadio>
                            </NCCol>
                            <NCCol lg={2} sm={2} xs={2}>
                                {this.state.json['20022010-000044']}{/* 国际化处理： 日期*/}
                            </NCCol>
                            <NCCol className={"ri-wid"} lg={4} sm={4} xs={4}>
                                <NCDatePicker
                                    fieldid="detailbegindate"
                                    value={searchData.begindate}
                                    disabled={!searchData.date}
                                    onChange={(v)=>{
                                        searchData.begindate=v;
                                        cacheTools.set('detailbegindate',v);
                                        this.setState({
                                            searchData
                                        })
                                    }}
                                />
                            </NCCol>
                            <span className={"zhi_"}>{this.state.json['20022010-000007']}</span>{/* 国际化处理： 至*/}
                            <NCCol  className={"ri-wid"}  lg={4} sm={4} xs={4}>
                                <NCDatePicker
                                    fieldid="detailenddate"
                                    value={searchData.enddate}
                                    disabled={!searchData.date}
                                    onChange={(v)=>{
                                        searchData.enddate=v;
                                        cacheTools.set('detailenddate',v);
                                        this.setState({
                                            searchData
                                        })
                                    }}
                                />
                            </NCCol>
                        </NCRow>
                        <NCRow>
                            <NCCol lg={1} sm={1} xs={1}>
                                <NCRadio value={'N'} ></NCRadio>
                            </NCCol>
                            <NCCol lg={2} sm={2} xs={2}>
                                {this.state.json['20022010-000045']}{/* 国际化处理： 到*/}
                            </NCCol>
                            <NCCol lg={3} sm={3} xs={3}>
                                <NCDatePicker
                                    fieldid="detailreacheddate"
                                    value={searchData.reacheddate}
                                    disabled={searchData.date}
                                    onChange={(v)=>{
                                        searchData.reacheddate=v;
                                        cacheTools.set('detailreacheddate',v);
                                        this.setState({
                                            searchData
                                        })
                                    }}
                                />
                            </NCCol>
                            <NCCol className = {"wfdts"} lg={2} sm={2} xs={2}>
                                {this.state.json['20022010-000046']}{/* 国际化处理： 未对符天数*/}
                            </NCCol>
                            <NCCol lg={3} sm={3} xs={3}>
                                <InputItem
                                    fieldid="scale"
                                    type="customer"
                                    name="scale"
                                    defaultValue={searchData.day}
                                    disabled={searchData.date}
                                    onChange={(v) => {
                                        searchData.day=v
                                        this.setState({
                                            searchData
                                        })
                                    }}
                                />
                            </NCCol>
                        </NCRow>
                    </NCRadio.NCRadioGroup>
                    </NCRow>
                    <NCRow>
                        <NCCol lg={3} sm={3} xs={3}>
                            {this.state.json['20022010-000047']}{/* 国际化处理： 对符范围*/}
                        </NCCol>
                        <NCCol lg={9} sm={9} xs={9}>
                            <NCRadio.NCRadioGroup
                                name="12"
                                selectedValue={searchData.range}
                                onChange={(v)=>{
                                    searchData.range=v;
                                    this.setState({
                                        searchData
                                    })
                                }}>
                                <NCRadio  value='1'>{this.state.json['20022010-000048']}</NCRadio>{/* 国际化处理： 未对符*/}
                                <NCRadio  value='2'>{this.state.json['20022010-000049']}</NCRadio>{/* 国际化处理： 已对符*/}
                                <NCRadio  value='3'>{this.state.json['20022010-000050']}</NCRadio>{/* 国际化处理： 全部*/}
                            </NCRadio.NCRadioGroup>
                        </NCCol>
                    </NCRow>
                    </NCDiv>
                </Modal.Body>

                <Modal.Footer id="fotter">
                    
                    <Button fieldid="confirm" onClick={ this.handleConfirm } colors="primary">{this.state.json['20022010-000051']}</Button>{/* 国际化处理： 查询*/}
                    <Button fieldid="cancel" onClick={ this.close } shape="border" >{this.state.json['20022010-000017']}</Button>{/* 国际化处理： 取消*/}
                </Modal.Footer>
           </Modal>
        </div>
        )
    }
}
