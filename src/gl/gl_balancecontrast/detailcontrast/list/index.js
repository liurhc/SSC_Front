//主子表卡片
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast, cacheTools,getMultiLang,createPageIcon} from 'nc-lightapp-front';
let { NCFormControl ,NCAffix} = base;
import { buttonClick, initTemplate, rowEvent, afterEvent } from './events';
import ContrastRuleGridRef from '../../../refer/voucher/ContrastRuleGridRef/index.js';
import SeachModal from './searchModal';
import AutoSearchModal from './autoSearchModal';
import HeaderArea from '../../../public/components/HeaderArea';
import '../../../public/reportcss/firstpage.less';
import './index.less'
class Card extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isCardPaginationShow: false,  //控制分页是否显示
			seachModalShow: false,
			autoSeachModalShow: false,
			rule: {
				name: '',
				value: ''
			},
			batchno: '',
			searchData: {},
			autoSearchData: {},
			compareStatus: false,//对照显示
			content: null,//“YYYNN”
			tableData2: [],//2表格数据
			SearchDataSum: null,//汇总查询数据
			defaultAutoData: null,
			initData: {//序号初始化参数
				selfMaxNo: 1,
				otherMaxNo: 1,
				selfNoMap: new Map(),
				otherNoMap: new Map(),
				selfRowNoMap: new Map(),
				otherRowNoMap: new Map()
			},
			batchnoArray: [],
			json:{}
		};
		cacheTools.set('selfMaxNo', 1);
		cacheTools.set('otherMaxNo', 1);
		this.formId = 'head';
		this.subformId1 = 'subhead1';
		this.subformId2 = 'subhead2';
		this.moduleId = '2002';
		this.tableId1 = 'subbody1';
		this.tableId2 = 'subbody2';
		this.pageId = '20022010_LIST';
		this.back=this.back.bind(this,props)
		this.defaultAccbook = {};
	}

	componentWillReceiveProps() {

	}
	componentDidMount() {
		//设置按钮可见性
		this.pageButtonVisible();
		let queryvo = cacheTools.get('detailqueryvo');
		setTimeout(()=>{
			let upScroll = document.getElementsByClassName("u-table-body")[0];
			let downScroll = document.getElementsByClassName("u-table-body")[1];
			upScroll.addEventListener("scroll", function () {
				downScroll.scrollLeft = upScroll.scrollLeft
			});
			downScroll.addEventListener("scroll", function () {
				upScroll.scrollLeft = downScroll.scrollLeft
			})   
		}, 1000);

	}

	componentWillMount() {
        let callback= (json) =>{
            this.setState({json:json},()=>{
                initTemplate.call(this,this.props);
            })
        }
        getMultiLang({moduleId:'20022010',domainName:'gl',currentLocale:'zh-CN',callback});
    }
	//设置按钮可见性
	pageButtonVisible = () => {
		let jumpflag = this.props.getUrlParam('jumpflag');
		this.setState({ isCardPaginationShow: false });
		let buttonGroup1 = ['query', 'contrastdown', 'auto', 'hand', 'discorp', 'comparedown', 'compare', 'cancelcompare', 'follow'];
		let buttonGroup2 = ['back', 'contrast', 'discontrast'];
		let buttonGroup3 = ['save', 'cancel']
		let buttonGroup4 = ['nofollow']
		if (jumpflag) {
			this.props.button.setButtonVisible(buttonGroup1, false);
			this.props.button.setButtonVisible(buttonGroup2, true);
			this.props.button.setButtonVisible(buttonGroup3, false);
			this.props.button.setButtonVisible(buttonGroup4, false);

		}
		else {
			this.props.button.setButtonVisible(buttonGroup1, true);
			this.props.button.setButtonVisible(buttonGroup2, false);
			this.props.button.setButtonVisible(buttonGroup3, false);
			this.props.button.setButtonVisible(buttonGroup4, false);
			this.props.button.setButtonDisabled(['refresh', 'hand', 'compare', 'cancelcompare', 'make'], true);

		}
		this.props.form.setFormStatus(this.formId, status);
		this.props.cardTable.setStatus(this.tableId, status);
	};

	//隐藏字段
	setQueryContent = (amount, quantity, content) => {
		let tableid1 = 'subbody1';
		let tableid2 = 'subbody2'

		if (amount == true && quantity == true) {
			this.props.cardTable.showColByKey(tableid1, ['quantityequal', 'amountequal']);
			this.props.cardTable.showColByKey(tableid2, ['quantityequal', 'amountequal']);
		}
		else if (amount == true && quantity == false) {
			this.props.cardTable.hideColByKey(tableid1, 'quantityequal');
			this.props.cardTable.hideColByKey(tableid2, 'quantityequal');
			this.props.cardTable.showColByKey(tableid1, 'amountequal');
			this.props.cardTable.showColByKey(tableid2, 'amountequal');
		}
		else if (amount == false && quantity == true) {
			this.props.cardTable.hideColByKey(tableid1, 'amountequal');
			this.props.cardTable.hideColByKey(tableid2, 'amountequal');
			this.props.cardTable.showColByKey(tableid1, 'quantityequal');
			this.props.cardTable.showColByKey(tableid2, 'quantityequal');
		}

		if (content[0] === 'N') {
			this.props.cardTable.hideColByKey(tableid1, ['debitquantity', 'creditquantity']);
			this.props.cardTable.hideColByKey(tableid2, ['debitquantity', 'creditquantity']);
		}
		else {
			this.props.cardTable.showColByKey(tableid1, ['debitquantity', 'creditquantity']);
			this.props.cardTable.showColByKey(tableid2, ['debitquantity', 'creditquantity']);
		}
		if (content[1] === 'N') {
			this.props.cardTable.hideColByKey(tableid1, ['debitamount', 'creditamount']);
			this.props.cardTable.hideColByKey(tableid2, ['debitamount', 'creditamount']);
		}
		else {
			this.props.cardTable.showColByKey(tableid1, ['debitamount', 'creditamount']);
			this.props.cardTable.showColByKey(tableid2, ['debitamount', 'creditamount']);
		}
		if (content[2] === 'N') {
			this.props.cardTable.hideColByKey(tableid1, ['debitorgamount', 'creditorgamount']);
			this.props.cardTable.hideColByKey(tableid2, ['debitorgamount', 'creditorgamount']);
		}
		else {
			this.props.cardTable.showColByKey(tableid1, ['debitorgamount', 'creditorgamount']);
			this.props.cardTable.showColByKey(tableid2, ['debitorgamount', 'creditorgamount']);
		}
		if (content[3] === 'N') {
			this.props.cardTable.hideColByKey(tableid1, ['debitgroupamount', 'creditgroupamount']);
			this.props.cardTable.hideColByKey(tableid2, ['debitgroupamount', 'creditgroupamount']);
		}
		else {
			this.props.cardTable.showColByKey(tableid1, ['debitgroupamount', 'creditgroupamount']);
			this.props.cardTable.showColByKey(tableid2, ['debitgroupamount', 'creditgroupamount']);
		}

		if (content[4] === 'N') {
			this.props.cardTable.hideColByKey(tableid1, ['debitglobalamount', 'creditglobalamount']);
			this.props.cardTable.hideColByKey(tableid2, ['debitglobalamount', 'creditglobalamount']);
		}
		else {
			this.props.cardTable.showColByKey(tableid1, ['debitglobalamount', 'creditglobalamount']);
			this.props.cardTable.showColByKey(tableid2, ['debitglobalamount', 'creditglobalamount']);
		}

	}
	//反勾对				  
	discontrast = () => {
		let { rule, batchno } = this.state;
		let datas = {
			pk_contrastrule: rule.value,
			batchno: batchno
		};
		ajax({
			url: '/nccloud/gl/sumcontrast/discontrast.do',
			data: datas,
			success: (res) => {
				if (res.success) {
					if (res.data) {
						toast({ color: 'success', content: this.state.json['20022010-000031'] });/* 国际化处理： 操作成功*/
						let queryvo = cacheTools.get('detailqueryvo');
						//反勾对成功，重新查询数据
						ajax({
							url: '/nccloud/gl/detailcontrast/query.do',
							data: queryvo,
							success: (res) => {
								if (res.data) {
									if(res.data['subbody1'])
									{
										this.props.cardTable.setTableData(this.tableId1, res.data['subbody1'].subbody1);
									}
									else
									{
										this.props.cardTable.setTableData(this.tableId1, { rows: [] });
									}
									if(res.data['subbody2'])
									{
										this.props.cardTable.setTableData(this.tableId2, res.data['subbody2'].subbody2);
									}
									else
									{
										this.props.cardTable.setTableData(this.tableId2, { rows: [] });
									}
									//设置隐藏字段
									this.setQueryContent(queryvo.amount, queryvo.quantity, cacheTools.get('detailcontent'));
								}
								else {
									this.props.cardTable.setTableData(this.tableId1, { rows: [] });
									this.props.cardTable.setTableData(this.tableId2, { rows: [] });
								}
							}
						})
					}
					else {
						toast({ color: 'warning', content: this.state.json['20022010-000032'] });/* 国际化处理： 操作失败*/
					}
				}
			}
		});
	}

	setsearchModalShow = (data) => {
		this.setState({
			seachModalShow: data,
			compareStatus:false
		})
	}

	setautoSeachModalShow = (data) => {
		this.setState({
			autoSeachModalShow: data
		})
	}

	handContrast = (props, moduleId, record, index) => {
	}

	getSearchData = (data) => {
		this.setState({
			searchData: data
		})
	}

	getAutoData = (data) => {
		this.setState({
			autoSearchData: data
		})
		//点击弹出对话框的确定之后才执行
		let self = this
		ajax({
			url: '/nccloud/gl/detailcontrast/auto.do',
			data,
			success: (res) => {
				toast({ color: 'success', content: this.state.json['20022010-000025'] });/* 国际化处理： 对账成功*/
				//勾对成功重新查询
				//需要做判断，有查询条件做查询，没有查询条件不做查询
				if (self.state.searchData.pk_rule) {
					ajax({
						url: '/nccloud/gl/detailcontrast/query.do',
						data: self.state.searchData,
						success: (res) => {
							if (res.data) {
								if(res.data['subbody1'])
								{
									self.props.cardTable.setTableData(this.tableId1, res.data['subbody1'].subbody1);
								}
								else
								{
									self.props.cardTable.setTableData(this.tableId1, { rows: [] });
								}
								if(res.data['subbody2'])
								{
									self.props.cardTable.setTableData(this.tableId2, res.data['subbody2'].subbody2);
								}
								else
								{
									self.props.cardTable.setTableData(this.tableId2, { rows: [] });
								}
								//隐藏字段
								self.setQueryContent(self.state.searchData.amount, self.state.searchData.quantity, cacheTools.get('detailcontent'));
							}
							else {
								self.props.cardTable.setTableData(this.tableId1, { rows: [] });
								self.props.cardTable.setTableData(this.tableId2, { rows: [] });
							}
						}
					})
				}

			}
		})
	}

	getTableData2 = (data) => {
		        this.setState({
			            tableData2: data
		        })
	}

	setContentValue = (data) => {
		        this.setState({
			            content: data
		        })
	}

	getSearchDataSum = (data) => {
		this.setState({
			SearchDataSum: data
		})
	}

	sendToAuto = (data) => {
		this.setState({
			defaultAutoData: data
		})
	}

	back=(props)=>{
		props.linkTo('/gl/gl_balancecontrast/sumcontrast/list/index.html',{appcode:'20022015',pagecode:'20022015_LIST',jumpflag: true})
	}
	singleCheck(props, moduleId, record, index,status)
	{
		let moduleId1;
		let moduleId2
		if(moduleId=='subbody1')
		{
			moduleId1 = 'subbody1';
			moduleId2 = 'subbody2';
		}
		else
		{
			moduleId1 = 'subbody2';
			moduleId2 = 'subbody1';
		}
		let { batchno } = this.state;
		if(status==true)
		{
			let checkdata = props.cardTable.getCheckedRows(moduleId1);
			if(checkdata.length>1)
			{
				let indexBefore;
				checkdata.forEach((item, key) => {
					if(item.index !=index)
					{
						indexBefore=item.index
					}
				})
				props.cardTable.unSelectRowsByIndex(moduleId,indexBefore);
			}
			batchno = record.values.batchno.value==null?'':record.values.batchno.value;
			this.setState({
				batchno
			})  
			
			let checkdataother = props.cardTable.getCheckedRows(moduleId2);
			if(checkdataother.length>0)
			{
				let otherIndexArray = new Array();
				checkdataother.forEach((item, key) => {
					otherIndexArray.push(item.index);
				})
				props.cardTable.unSelectRowsByIndex(moduleId2,otherIndexArray);
			}
			
		}
		else
		{
		
			if(batchno == record.values.batchno.value)
			{
				batchno='';
			}
			this.setState({
				batchno
			})  
		}
	}
	getTableHeightPosi = () => {
        let tableHeight=(document.getElementById('app').offsetHeight-310)/2;
        return tableHeight;
        }
	render() {
		let { cardTable, form, button, modal, cardPagination } = this.props;
		let buttons = this.props.button.getButtons();
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		let { createForm } = form;
		let { createCardTable } = cardTable;
		const { createCardPagination } = cardPagination;
		let { createButton } = button;
		let { createModal } = modal;
		let { isCardPaginationShow, seachModalShow, autoSeachModalShow, defaultAutoData } = this.state;
		let jumpflag=this.props.getUrlParam('jumpflag');
		return (
			<div className="nc-bill-list" id="detail_bill_card">
				<SeachModal
					defaultAccbook={this.defaultAccbook}
					seachModalShow={seachModalShow}
					props={this.props}
					setsearchModalShow={this.setsearchModalShow.bind(this)}
					getSearchData={this.getSearchData.bind(this)}
					setContentValue={this.setContentValue.bind(this)}
					getTableData2={this.getTableData2.bind(this)}
					getSearchDataSum={this.getSearchDataSum.bind(this)}
					sendToAuto={this.sendToAuto.bind(this)}
				/>
				<AutoSearchModal
					autoSeachModalShow={autoSeachModalShow}
					onClose={() => {
						this.setState({
							autoSeachModalShow: false
						})
					}}
					props={this.props}
					searchData={this.state.searchData}
					setautoSeachModalShow={this.setautoSeachModalShow.bind(this)}
					getAutoData={this.getAutoData.bind(this)}
					defaultAutoData={defaultAutoData}
				/>
				<div className="nc-bill-top-area">
					{/* 因为isCardPaginationShow是false，不会显示翻页按钮。所以HeaderArea里没包含翻页按钮。 */}
					<HeaderArea
						initShowBackBtn={jumpflag}
						backBtnClick={this.back}
						title={this.state.json['20022010-000034']}/* 国际化处理： 内部交易明细对账*/
						btnContent={this.props.button.createButtonApp({
							area: 'head',
							buttonLimit: 3,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					/>

					<div className="nc-bill-form-area">
						{createForm(this.formId, {
							fieldid: 'detailhead',
							onAfterEvent: afterEvent.bind(this)
						})}
					</div>
				</div>
				{/* <div style={{ height: 8 }}></div> */}
				{
					<div className="nc-bill-table-area1 nc-bill-table-area">
						<div className="nc-bill-form-area1 table_from">
							{createForm(this.subformId1, {
								fieldid: 'detailSubhead1',
								onAfterEvent: afterEvent.bind(this)
							})}
						</div>
						{createCardTable(this.tableId1, {
							fieldid: 'detailSubbody1',
							onAfterEvent: afterEvent.bind(this),
							showIndex: true,
							onRowClick: rowEvent.bind(this),
                        	hideSwitch:()=>{return false;},
							height:'200px'//this.getTableHeightPosi() 
							//showCheck: true,
							//onSelected:this.singleCheck.bind(this), 
						})}
					</div>
				}

				{
					<div className="nc-bill-table-area2  nc-bill-table-area nc-theme-area-split-bc">
						<div className="nc-bill-form-area2 table_from" >
							{createForm(this.subformId2, {
								fieldid: 'detailSubhead2',
								onAfterEvent: afterEvent.bind(this)
							})}
						</div>
						{createCardTable(this.tableId2, {
                        	adaptionHeight:true,
							fieldid: 'detailSubbody2',
							onAfterEvent: afterEvent.bind(this),
							showIndex: true,
                        	hideSwitch:()=>{return false;},
							// height:this.getTableHeightPosi() 
							//showCheck: true,
							//onSelected:this.singleCheck.bind(this), 
						})}
					</div>
				}

				{
					createModal(
						'discontrastModel',
						{
							title: this.state.json['20022010-000033'],/* 国际化处理： 取消勾对*/
							className: "m-mxdzcx",
							content:
							<div className="m-nbjydz">
								<div className={"u-row"}>
									<div class='u-col-md-4 u-col-sm-4 u-col-xs-4 ' >
										<span>{this.state.json['20022010-000022']}</span>{/* 国际化处理： 内部交易对账规则*/}
									</div>
									<div class='u-col-md-8 u-col-sm-8 u-col-xs-8 ' >
										<ContrastRuleGridRef
											fieldid="contrastrule"
											value={{ refname: this.state.rule.name, refpk: this.state.rule.value }}
											queryCondition ={(v)=>{
                                    			return {startstatus:'All'};
                                			}}
											onChange={(v) => {
												let { rule } = this.state
												rule.name = v.refname;
												rule.value = v.refpk;
												this.setState({
													rule
												})
											}}
										/>
									</div>
								</div>
								<div className={"u-row"}>
									<div class='u-col-md-4 u-col-sm-4 u-col-xs-4 ' >
										<span>{this.state.json['20022010-000035']}</span>{/* 国际化处理： 批次号*/}
									</div>
									<div class='u-col-md-8 u-col-sm-8 u-col-xs-8 ' >
										<NCFormControl
											fieldid="batchno"
											value={this.state.batchno}
											onChange={(v) => {
												let { batchno } = this.state;
												batchno = v;
												this.setState({
													batchno
												})

											}}
										/>
									</div>
								</div>
							</div>,
							size: 'lg',
							beSureBtnClick: this.discontrast.bind(this)
						}
					)
				}
			</div>
		);
	}
}

Card = createPage({
	//initTemplate: initTemplate,
	//mutiLangCode: '2052'
})(Card);

ReactDOM.render(<Card />, document.querySelector('#app'));
