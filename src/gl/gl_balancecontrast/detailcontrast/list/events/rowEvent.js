import { ajax,toast,cacheTools } from 'nc-lightapp-front';

export default function afterEvent(props, moduleId, record, index) 
{
	this.setState({
		batchno:record.values.batchno?record.values.batchno.value:''
	})
	let {compareStatus}=this.state;
	if(compareStatus==true)
	{
		let content = cacheTools.get('detailcontent');
		let {searchData,tableData2}=this.state;
		let tableData2Rows = tableData2.rows;
	    let newTableData2=[];
	    let table1RowData=record.values;
		//处理已对符数据
		if(table1RowData.pk_contrast.value)
		{
           for(let i=0;i<tableData2Rows.length;i++)
			{
               if(tableData2Rows[i].values.pk_contrast.value&&tableData2Rows[i].values.pk_contrast.value==table1RowData.pk_contrast.value)
				{
                     newTableData2.push(tableData2Rows[i])
                }
            }
       	}  
		//处理未对符数据
		else
		{
			let contrasttype;
			if(searchData.quantity==true)
			{
				if(searchData.amount==true)
				{
					contrasttype = 3;
				}
				else
				{
					contrasttype = 1;
				}
			}
			else
			{
				if(searchData.amount==true)
				{
					contrasttype = 2;
				}
			}
			let elements = getDetailContrastElementsByContrastTypeAndContent(contrasttype, content);
			for(let j=0;j<tableData2Rows.length;j++)
			{
				if(!tableData2Rows[j].values.pk_contrast.value)
				{
					if(checkEqual(record, tableData2Rows[j], elements,props))
					{
						newTableData2.push(tableData2Rows[j]);
					}
				}
			}
		}
		props.cardTable.setTableData('subbody2', { rows: [] } );
		props.cardTable.setTableData('subbody2', { rows: newTableData2 } );
	}
}

function getDetailContrastElementsByContrastTypeAndContent(contrasttype,contrastcontent) 
{
	let list = new Array();
	//如果是数量
	if(contrasttype==1){
		if(contrastcontent[0]=='Y')
		{
			list.push('creditquantity');
			list.push('debitquantity');
		}
		//如果是金额
	}else if(contrasttype==2){
		if(contrastcontent[1]=='Y')
		{
			list.push('creditamount');
			list.push('debitamount');
		}
		if(contrastcontent[2]=='Y')
		{
			list.push('creditorgamount');
			list.push('debitorgamount');
		}
		if(contrastcontent[3]=='Y')
		{
			list.push('creditgroupamount');
			list.push('debitgroupamount');
		}
		if(contrastcontent[4]=='Y')
		{
			list.push('creditglobalamount');
			list.push('debitglobalamount');
		}
		//如果是金额和数量
	} else if (contrasttype==3)
	{
		if(contrastcontent[0]=='Y')
		{
			list.push('creditquantity');
			list.push('debitquantity');
		}
		if(contrastcontent[1]=='Y')
		{
			list.push('creditamount');
			list.push('debitamount');
		}
		if(contrastcontent[2]=='Y')
		{
			list.push('creditorgamount');
			list.push('debitorgamount');
		}
		if(contrastcontent[3]=='Y')
		{
			list.push('creditgroupamount');
			list.push('debitgroupamount');
		}
		if(contrastcontent[4]=='Y')
		{
			list.push('creditglobalamount');
			list.push('debitglobalamount');
		}
	}
	return list;
}
function getContrastMap(props)
{
	let map = new Map();
	map.set('creditquantity', 'debitquantity');
	map.set('creditamount', 'debitamount');
	map.set('creditorgamount', 'debitorgamount');
	map.set('creditgroupamount', 'debitgroupamount');
	map.set('creditglobalamount', 'debitglobalamount');
	map.set( 'debitquantity','creditquantity');
	map.set( 'debitamount','creditamount');
	map.set( 'debitorgamount' ,'creditorgamount');
	map.set('debitgroupamount' ,'creditgroupamount');
	map.set( 'debitglobalamount' ,'creditglobalamount');
	return map;
}

function checkEqual(selfvo,othervo,elements,props) {
	// 首先判断辅助核算
	if (selfvo.values.typevalue.value&&!othervo.values.typevalue.value) 
	{
		return false;
	} 
	else if(selfvo.values.typevalue.value&&!othervo.values.typevalue.value)
	{
		return false;
	}
	else if(selfvo.values.typevalue.value&&othervo.values.typevalue.value&&selfvo.values.typevalue.value!=othervo.values.typevalue.value)
	{
		return false;
	}
	// 然后判断金额
	let map = getContrastMap(props);
	for (let i=0;i<elements.length;i++) 
	{
		let selfdata = selfvo.values[elements[i]].value;
		let otherdata = othervo.values[map.get(elements[i])].value;

		if(selfdata&&!otherdata)
		{
			return false;
		}
		else if(!selfdata&&otherdata)
		{
			return false;
		}
		if (selfdata&&otherdata&&selfdata!=otherdata) 
		{
			return false;
		}

	}
	return true;
}

