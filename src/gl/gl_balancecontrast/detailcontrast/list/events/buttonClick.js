import { ajax, base, toast,cacheTools } from 'nc-lightapp-front';
export default function(props, id) {
  let changedata1;
  let changedata2;
  let buttonGroup1 = ['save','cancel'];
  let buttonGroup2 = ['comparedown','compare','cancelcompare','follow'];
  let buttonGroup3 = ['query','refresh','contrastdown','auto','hand','discorp','make'];
  let buttonGroup4 = ['nofollow'];

  let queryvo = cacheTools.get('detailqueryvo');
  let jumpflag = this.props.getUrlParam('jumpflag');
  let {batchnoArray}=this.state;
  switch (id) {
    case 'query'://查询
      this.setState({
        seachModalShow:true
      })
    break;

    case 'auto'://自动勾对
      this.setState({
        autoSeachModalShow:true
      })
    break;
    case 'hand'://手工勾对

    //按钮显示控制
    this.props.button.setButtonVisible(buttonGroup1, true);
    this.props.button.setButtonVisible(buttonGroup2, true);
    this.props.button.setButtonVisible(buttonGroup3, false);
    this.props.button.setButtonVisible(buttonGroup4, false);
    //控件可编辑性控制
    this.props.cardTable.setStatus(this.tableId1,'edit',null);
    this.props.cardTable.setStatus(this.tableId2,'edit',null);
    this.props.cardTable.setColEditableByKey(this.tableId1, ['no','amountequal','quantityequal'], false);
    this.props.cardTable.setColEditableByKey(this.tableId2, ['no','amountequal','quantityequal'], false);
    //设置对照显示变量初始值
    let initData = {
      selfMaxNo : 1,
      otherMaxNo : 1,
      selfNoMap : new Map(),
      otherNoMap : new Map(),
      selfRowNoMap : new Map(),
      otherRowNoMap : new Map()
    }
    this.setState({
      initData:initData
    })
    cacheTools.set('selfMaxNo',1);
    cacheTools.set('otherMaxNo',1);
    break;
    case 'save'://保存

    changedata1 = this.props.cardTable.getChangedRows(this.tableId1);
    changedata2 = this.props.cardTable.getChangedRows(this.tableId2);

    let changedata1PK = [];
    changedata1.forEach((val) => {
      let currentArray =new Array(4);
      currentArray[0] = val.values.pk_contrastdetail.value;
      currentArray[1] = val.values.no.value;
      currentArray[2] = val.values.amountequal.value;
      currentArray[3] = val.values.quantityequal.value;
      changedata1PK.push(currentArray);

    });
    let changedata2PK = [];
    changedata2.forEach((val) => {
      let currentArray =new Array(4)
      currentArray[0] = val.values.pk_contrastdetail.value;
      currentArray[1] = val.values.no.value;
      currentArray[2] = val.values.amountequal.value;
      currentArray[3] = val.values.quantityequal.value;
      changedata2PK.push(currentArray);
    });

    let savedata = {
      batchnoArray:batchnoArray,
      changedata1:changedata1PK,
      changedata2:changedata2PK,
      queryvo:queryvo
    };

    ajax({
      url: '/nccloud/gl/detailcontrast/hand.do',
      data : savedata,
      success: (res) => { 
        if (res.data) {
          toast({ color: 'success', content: this.state.json['20022010-000025'] });/* 国际化处理： 对账成功*/
          //勾对成功重新查询
          ajax({
            url: '/nccloud/gl/detailcontrast/query.do',
            data: queryvo,
            success: (res) => {
              //设置按钮和界面状态
              this.props.button.setButtonVisible(buttonGroup1, false);
              this.props.button.setButtonVisible(buttonGroup2, true);
              this.props.button.setButtonVisible(buttonGroup3, true);
              this.props.cardTable.setStatus(this.tableId1,'browse',null);
              this.props.cardTable.setStatus(this.tableId2,'browse',null);
              if (res.data) {
                if(res.data['subbody1'])
                {
                  this.props.cardTable.setTableData(this.tableId1, res.data['subbody1'].subbody1);
                }
                else
                {
                  this.props.cardTable.setTableData(this.tableId1, {rows:[]});
                }
                if(res.data['subbody2'])
                {
                  this.props.cardTable.setTableData(this.tableId2, res.data['subbody2'].subbody2);
                }
                else
                {
                  this.props.cardTable.setTableData(this.tableId2, {rows:[]});
                }
                //隐藏字段
                this.setQueryContent(queryvo.amount,queryvo.quantity,cacheTools.get('detailcontent'));
              }
              else
              {
                this.props.cardTable.setTableData(this.tableId1, {rows:[]});
                this.props.cardTable.setTableData(this.tableId2, {rows:[]});
              }
              let {compareStatus} = this.state;
              if(compareStatus)
              {
                if(compareStatus==true)
                {
                  this.state.tableData2 = this.props.cardTable.getAllData('subbody2');
                  this.props.button.setButtonDisabled(['compare'],true);
                  this.props.button.setButtonDisabled(['cancelcompare'],false);
                }
                else
                {
                  this.props.button.setButtonDisabled(['compare'],false);
                  this.props.button.setButtonDisabled(['cancelcompare'],true);
                }
              }
          }
        })
        }
        else
        {
          toast({ color: 'warning', content: this.state.json['20022010-000026'] });/* 国际化处理： 对账失败*/
        }
      }

    })

    break;
    case 'cancel'://取消手动
    //按钮显示控制
    this.props.button.setButtonVisible(buttonGroup1, false);
    this.props.button.setButtonVisible(buttonGroup2, true);
    this.props.button.setButtonVisible(buttonGroup3, true);
    //设置序号状态清空
    setTimeout(()=>{
    this.props.cardTable.resetTableData(this.tableId1);
    this.props.cardTable.resetTableData(this.tableId2);
      }
    )
    //控件可编辑性控制
    this.props.cardTable.setStatus(this.tableId1,'browse',null);
    this.props.cardTable.setStatus(this.tableId2,'browse',null);
    //清空反勾对的序号
    batchnoArray.splice(0,batchnoArray.length);
    break;
    case 'discorp'://取消勾对
    let rulediscorp =  this.state.rule;
    rulediscorp.name = queryvo.rulename;
    rulediscorp.value= queryvo.pk_rule;
    let batchnodiscorp = this.state.batchno;
    this.setState({
      batchnodiscorp,rulediscorp
    },()=>{
      this .props.modal.show('discontrastModel');
    })

    break;
    case 'make'://生成对账报告
    ajax({
      url: '/nccloud/gl/detailcontrast/make.do',
      data:queryvo,
      success: (res) => {
        if (res.success) {
          toast({ color: 'success', content: res.data });
        }
      }
    });
    break;
    //跳转页面的按钮
    case 'back'://返回
        let sendUrl = '/gl/gl_balancecontrast/sumcontrast/list/index.html';
        this.props.linkTo(
          sendUrl,
          { appcode:'20022015',pagecode:'20022015_LIST',jumpflag: true}
            );
    break;
    case 'refresh'://刷新
      ajax({
        url: '/nccloud/gl/detailcontrast/query.do',
       //设置不同的数据源
        data:queryvo,
        success: (res) => { 
          if (res.data) {
            toast({ color: 'success', title: this.state.json['20022010-000055']});/* 国际化处理：刷新成功！*/
            if(res.data['subbody1'])
            {
              this.props.cardTable.setTableData(this.tableId1, res.data['subbody1'].subbody1);
            }
            else
            {
              this.props.cardTable.setTableData(this.tableId1, {rows:[]});
            }
            if(res.data['subbody2'])
            {
              this.props.cardTable.setTableData(this.tableId2, res.data['subbody2'].subbody2);
            }
            else
            {
              this.props.cardTable.setTableData(this.tableId2, {rows:[]});
            }
            //隐藏字段
            this.setQueryContent(queryvo.amount,queryvo.quantity,cacheTools.get('detailcontent'));
            //跳转过来的页面，都是可以编辑的
            if(jumpflag=='true')
            {
              this.props.cardTable.setStatus(this.tableId1,'edit',null);
              this.props.cardTable.setStatus(this.tableId2,'edit',null);
            }
          }
          else
          {
            toast({ color: 'success', title: this.state.json['20022010-000055']});/* 国际化处理：刷新成功！*/
            this.props.cardTable.setTableData(this.tableId1, {rows:[]});
            this.props.cardTable.setTableData(this.tableId2, {rows:[]});
          }
      }
    })
    break;
    case 'contrast'://勾对
      let detailContrastDatas1 = this.props.cardTable.getChangedRows(this.tableId1);
      let detailContrastDatas2 = this.props.cardTable.getChangedRows(this.tableId2);
      if (detailContrastDatas1.length == 0) {
        toast({ color: 'warning', content: this.state.json['20022010-000028'] });/* 国际化处理： 请勾对数据!*/
        return;
      }
      if (detailContrastDatas2.length == 0) {
        toast({ color: 'warning', content: this.state.json['20022010-000028'] });/* 国际化处理： 请勾对数据!*/
        return;
      }
      let detailContrastSelf = [];
      let detailContrastOther = [];
      detailContrastDatas1.forEach((val) => {
        if(val.values.no.value)
        {
          detailContrastSelf.push(val.values.pk_contrastdetail.value);
        }
      });
      detailContrastDatas2.forEach((val) => {
        if(val.values.no.value)
        {
          detailContrastOther.push(val.values.pk_contrastdetail.value);
        }
      });
      
      let contrastdata = {
        
        detailContrastSelf,
        detailContrastOther,
        //需要在小应用跳转的时候传递汇总查询条件
        queryvo

      };
        ajax({
          url: '/nccloud/gl/sumcontrast/detailcontrast.do',
          data: contrastdata,
          success: (res) => {
            if (res.success) {
              if(res.data)
              {
               toast({color: 'success', content: this.state.json['20022010-000029']});/* 国际化处理： 勾对成功*/
               ajax({
                url: '/nccloud/gl/detailcontrast/query.do',
                data: queryvo,
                success: (res) => { 
                  if (res.data) {
                    if(res.data['subbody1'])
                    {
                      this.props.cardTable.setTableData(this.tableId1, res.data['subbody1'].subbody1);
                    }
                    else
                    {
                      this.props.cardTable.setTableData(this.tableId1, {rows:[]});
                    }
                    if(res.data['subbody2'])
                    {
                      this.props.cardTable.setTableData(this.tableId2, res.data['subbody2'].subbody2);
                    }
                    else
                    {
                      this.props.cardTable.setTableData(this.tableId2, {rows:[]});
                    }
                    //隐藏字段
                    this.setQueryContent(queryvo.amount,queryvo.quantity,cacheTools.get('detailcontent'));
                  }
                  else
                  {
                    this.props.cardTable.setTableData(this.tableId1, {rows:[]});
                    this.props.cardTable.setTableData(this.tableId2, {rows:[]});
                  }
                }
              })
              }
              else
              {
                toast({color: 'warning', content: this.state.json['20022010-000030']});/* 国际化处理： 勾对失败*/
              }
              }
          }
        });
     break;
     case 'discontrast'://反勾对
        let rulediscontrast =  this.state.rule;
        rulediscontrast.name =  queryvo.rulename;
        rulediscontrast.value=  queryvo.pk_rule;
        let batchnodiscontrast = this.state.batchno;
        this.setState({
          batchnodiscontrast,rulediscontrast
        },()=>{
          this .props.modal.show('discontrastModel');
        })
        
    break;
    case 'compare'://对照显示
      this.setState({
        compareStatus:true
      })
      //this.state.tableData2 = this.props.cardTable.getAllRows('subbody2');
      this.state.tableData2 = this.props.cardTable.getAllData('subbody2');
      this.props.button.setButtonDisabled(['compare'],true);
      this.props.button.setButtonDisabled(['cancelcompare'],false);
    break;
    case 'cancelcompare'://取消对照
      this.setState({
        compareStatus:false
      })
      this.props.button.setButtonDisabled(['compare'],false);
      this.props.button.setButtonDisabled(['cancelcompare'],true);
    //显示数据
    let tableData2Current =this.state.tableData2;
    this.props.cardTable.setTableData(this.tableId2,tableData2Current);
    break;
    // case 'follow'://同步显示
    // this.props.button.setButtonVisible('follow',false);
    // this.props.button.setButtonVisible('nofollow',true);
   // roll(true,upScroll,downScroll,one,two)
    // upScroll.addEventListener("scroll",one); 
    // downScroll.addEventListener("scroll",two); 
    // upScroll.removeEventListener("scroll",one) 
    // downScroll.removeEventListener("scroll",two) 
   // break;
    //case 'nofollow'://取消同步
  //  roll(false,upScroll,downScroll,one,two);
//     alert(1)
//     this.props.button.setButtonVisible('nofollow',false);
//     this.props.button.setButtonVisible('follow',true);
//     upScroll.removeEventListener("scroll",one) 
//     downScroll.removeEventListener("scroll",two) 
   // break;
    default:
      break;
  }
}





