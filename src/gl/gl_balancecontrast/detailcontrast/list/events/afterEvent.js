import { ajax,toast,cacheTools } from 'nc-lightapp-front';

let selfMaxNo;
let otherMaxNo;
let selfNoMap;
let otherNoMap;
let selfRowNoMap;
let otherRowNoMap;

export default function afterEvent(props, moduleId, key,value, changedrows, i, s, g) {
	//表体编辑后事件
	if (moduleId == this.tableId1||moduleId == this.tableId2)
    {
		if(key=='amountequal'||key=='quantityequal')
		{
			let {initData,batchnoArray}=this.state;
			let sendselfMaxNo = cacheTools.get('selfMaxNo');
			// if(sendselfMaxNo){
			// 	sendselfMaxNo = JSON.parse(sendselfMaxNo);
			// }
			let sendotherMaxNo = cacheTools.get('otherMaxNo');
			// if(sendotherMaxNo){
			// 	sendotherMaxNo = JSON.parse(sendotherMaxNo);
			// }
			//selfMaxNo = initData.selfMaxNo;
			//otherMaxNo =initData.otherMaxNo;
			selfMaxNo = sendselfMaxNo;
			otherMaxNo =sendotherMaxNo;
			selfNoMap = initData.selfNoMap;
			otherNoMap = initData.otherNoMap;
			selfRowNoMap = initData.selfRowNoMap;
			otherRowNoMap = initData.otherRowNoMap;

			let isSelf;
			let tableid;
			let tableidother; 

			if(moduleId == this.tableId1)
			{
				isSelf = true;
				tableid = 'subbody1';
				tableidother = 'subbody2'; 
			}
			else
			{
				isSelf = false;
				tableid = 'subbody2';
				tableidother = 'subbody1';
			}
			if(value==true)
			{
				pushRow(isSelf,tableid,tableidother,i,s,props);
			}
			else
			{
				deleteRow(isSelf,tableid,tableidother,i,s,props,batchnoArray);
			}
		}
	}
	
}

function reset()
{
	selfMaxNo = 1;
	otherNaxNo = 1;
	selfNoMap.clear();
	otherNoMap.clear();
	selfRowNoMap.clear();
	otherRowNoMap.clear();
}

function pushRow(isSelf,tableid,tableidother,row,rowcontent,props)
{
	if(rowcontent.values.no.value)
	{return;}
	let maxNoKey = getSelfMaxNo(isSelf);
	
	if(!getSelfNoMap(isSelf).has(maxNoKey))
	{
		//本方没有号对方没有号
		if(!getOtherNoMap(isSelf).has(maxNoKey))
		{
			dealWithSelfNullNoOtherNullNo(isSelf,tableid,row,maxNoKey,props);
		}
		//本方没有号对方有号
		else
		{
			dealWithSelfNullNo(isSelf,tableid,tableidother,row,rowcontent,props);
		}
	}
	else
	{
		//本方有号对方没有号
		if(!getOtherNoMap(isSelf).has(maxNoKey))
		{
			dealWithOtherNullNo(isSelf,tableid,row,maxNoKey,rowcontent,props);
		}
		//本方有号对方有号
		else
		{
			dealWithBothNo(isSelf,tableid,tableidother,row,rowcontent,props);
		}
	}

}
function getSelfNoMap(isSelf)
{
	if(isSelf)
	{
		return selfNoMap;
	}
	else
	{
		return otherNoMap;
	}
}
function getOtherNoMap(isSelf)
{
	if(isSelf)
	{
		return otherNoMap;
	}
	else
	{
		return selfNoMap;
	}
}
function getSelfRowNoMap(isSelf)
{
	if(isSelf)
	{
		return selfRowNoMap;
	}
	else
	{
		return otherRowNoMap;
	}
}
function getSelfMaxNo(isSelf)
{
	if(isSelf) 
	{
		return selfMaxNo;
	}
	else 
	{
		return otherMaxNo;
	}
}
function getOtherMaxNo(isSelf) {
	if(isSelf) 
	{
		return otherMaxNo;
	}
	else
	{
		return selfMaxNo;
	}
}
function setSelfMaxNo(isSelf,newValue) {
	if(isSelf) 
	{
		selfMaxNo = newValue;
		cacheTools.set('selfMaxNo',newValue);
	}
	else 
	{
		otherMaxNo = newValue;
		cacheTools.set('otherMaxNo',newValue);
	}
}

//本方没有号对方没有号
function dealWithSelfNullNoOtherNullNo(isSelf,tableid,row,maxNoKey,props)
{
	let rowList = new Array();
	rowList.push(row);
	getSelfNoMap(isSelf).set(maxNoKey, rowList);
	getSelfRowNoMap(isSelf).set(row, maxNoKey);
	props.cardTable.setValByKeyAndIndex(tableid,row,'no',{value: maxNoKey+"" });

}

//本方没有号对方有号
function dealWithSelfNullNo(isSelf,tableid,tableidother,row,rowcontent,props)
{
	let selfNo = getSelfMaxNo(isSelf);
	//辅助核算相同最大对方序号
	while(selfNo <= getOtherMaxNo(isSelf)) {
		//对方有号
		if(getOtherNoMap(isSelf).has(selfNo)) {
			let otherRowList = getOtherNoMap(isSelf).get(selfNo);
			let otherRow = otherRowList[0];
			//辅助核算相同
	        let otherRowContent = props.cardTable.getValByKeyAndIndex(tableidother,otherRow,'typevalue');
			if(rowcontent.values.typevalue.value==otherRowContent.value) {
				break;
			}
		}
		selfNo++;
	}
	//给最大号设值
	setSelfMaxNo(isSelf,selfNo);
	let rowList = new Array();
	rowList.push(row);
	getSelfNoMap(isSelf).set(getSelfMaxNo(isSelf),rowList);
	getSelfRowNoMap(isSelf).set(row,getSelfMaxNo(isSelf));
	props.cardTable.setValByKeyAndIndex(tableid,row,'no',{value: getSelfMaxNo(isSelf)+"" });
}
//本方有号对方没有号
function dealWithOtherNullNo(isSelf,tableid,row,maxNoKey,rowcontent,props)
{
	let rowList = getSelfNoMap(isSelf).get(maxNoKey);
	let oldRow = rowList[0];
	// 辅助核算不相同则，需要新增一个序号
	let oldRowContent = props.cardTable.getValByKeyAndIndex(tableid,oldRow,'typevalue');
	if(!rowcontent.values.typevalue.value==oldRowContent.value) {
		let selfMaxNo2 = getSelfMaxNo(isSelf);
		selfMaxNo2++;
		let list = new Array();
		list.push(row);
		getSelfNoMap(isSelf).set(getSelfMaxNo(isSelf), list);
	}
	 else 
	 {
		rowList.push(row);
	 }
	
	getSelfRowNoMap(isSelf).set(row,getSelfMaxNo(isSelf));
	props.cardTable.setValByKeyAndIndex(tableid,row,'no',{value: getSelfMaxNo(isSelf)+"" });

}
//本方有号对方有号
function dealWithBothNo(isSelf,tableid,tableidother,row,rowcontent,props)
{
	let selfNo = getSelfMaxNo(isSelf);
	let selfRowList = getSelfNoMap(isSelf).get(getSelfMaxNo(isSelf));
	//对方有号
	if(getOtherNoMap(isSelf).has(selfNo)) {
		let otherRowList = getOtherNoMap(isSelf).get(selfNo);
		let otherRow = otherRowList[0];
		//辅助核算相同
		let otherRowContent = props.cardTable.getValByKeyAndIndex(tableidother,otherRow,'typevalue');
		if(rowcontent.values.typevalue.value==otherRowContent.value) {
			//之前的借贷数据对上,启用新号
			if(checkValue(tableid,tableidother,selfRowList,otherRowList,props)) {
				selfNo++;
			}
		}
	}
	
	setSelfMaxNo(isSelf, selfNo);
	
	if(getSelfNoMap(isSelf).has(getSelfMaxNo(isSelf))) {
		let list = getSelfNoMap(isSelf).get(getSelfMaxNo(isSelf));
		list.push(row);
	}
	else 
	{
		let rowList = new Array();
		rowList.push(row);
		getSelfNoMap(isSelf).set(getSelfMaxNo(isSelf), rowList);
	}
	
	getSelfRowNoMap(isSelf).set(row, getSelfMaxNo(isSelf));
	props.cardTable.setValByKeyAndIndex(tableid,row,'no',{value: getSelfMaxNo(isSelf)+"" });
}

function checkValue(tableid,tableidother,selfRowList,otherRowList,props) {
	
	let contrastcontent = cacheTools.get('detailcontent');
	let queryvo = cacheTools.get('detailqueryvo');
	// if(queryvo){
	//   queryvo = JSON.parse(queryvo);
	// }
	let contrasttype;
	if(queryvo.quantity==true){
		if(queryvo.amount==true){
			contrasttype=3;
		}else{
			contrasttype =1;
		}
	}else{
		if(queryvo.amount==true){
			contrasttype =2;
		}
	}
	let elements = getDetailContrastElementsByContrastTypeAndContent(contrasttype,contrastcontent);
	//然后判断金额 
	let map = getContrastMap();
	for(let i=0;i<elements.length;i++){
		let selfvalue = getSumValue(tableid,tableidother,selfRowList,otherRowList, elements[i],props);
		let oppValue = getSumValue(tableid,tableidother,selfRowList,otherRowList, map.get(elements[i]),props);
		if(selfvalue!=oppValue) {
			return false;
		}
	}
	return true;
}
function getDetailContrastElementsByContrastTypeAndContent(contrasttype,contrastcontent) {

	let list = new Array();
	//如果是数量
	if(contrasttype==1){
		if(contrastcontent[0]=='Y'){
			list.push('creditquantity');
			list.push('debitquantity');
		}
		//如果是金额
	}else if(contrasttype==2){
		if(contrastcontent[1]=='Y'){
			list.push('creditamount');
			list.push('debitamount');
		}
		if(contrastcontent[2]=='Y'){
			list.push('creditorgamount');
			list.push('debitorgamount');
		}
		if(contrastcontent[3]=='Y'){
			list.push('creditgroupamount');
			list.push('debitgroupamount');
		}
		if(contrastcontent[4]=='Y'){
			list.push('creditglobalamount');
			list.push('debitglobalamount');
		}
		//如果是金额和数量
	} else if (contrasttype==3){
		if(contrastcontent[0]=='Y'){
			list.push('creditquantity');
			list.push('debitquantity');
		}
		if(contrastcontent[1]=='Y'){
			list.push('creditamount');
			list.push('debitamount');
		}
		if(contrastcontent[2]=='Y'){
			list.push('creditorgamount');
			list.push('debitorgamount');
		}
		if(contrastcontent[3]=='Y'){
			list.push('creditgroupamount');
			list.push('debitgroupamount');
		}
		if(contrastcontent[4]=='Y'){
			list.push('creditglobalamount');
			list.push('debitglobalamount');
		}
	}
	return list;
}
function getContrastMap()
{
	let map = new Map();
	map.set('creditquantity', 'debitquantity');
	map.set('creditamount', 'debitamount');
	map.set('creditorgamount', 'debitorgamount');
	map.set('creditgroupamount', 'debitgroupamount');
	map.set('creditglobalamount', 'debitglobalamount');
	map.set( 'debitquantity','creditquantity');
	map.set( 'debitamount','creditamount');
	map.set( 'debitorgamount' ,'creditorgamount');
	map.set('debitgroupamount' ,'creditgroupamount');
	map.set( 'debitglobalamount' ,'creditglobalamount');
	return map;
}
function getSumValue(tableid,tableidother,selfRowList,otherRowList,str,props){
	//直接在表格上取数据
	let tmp=0;
	if(selfRowList && selfRowList.length>0){
		for(let i=0; i< selfRowList.length;i++){
			//tmp += selfvos[i].values.str.value ;
			let selfRowContent = props.cardTable.getValByKeyAndIndex(tableid,selfRowList[i],str);
			if(selfRowContent.value)
			{
				tmp += selfRowContent.value*1 ;
			}
		}
	}
	
	 if(otherRowList && otherRowList.length>0){
	 	for(let i=0;i<otherRowList.length;i++){
			let otherRowContent = props.cardTable.getValByKeyAndIndex(tableidother,otherRowList[i],str);
			if(otherRowContent.value)
			{
				tmp += otherRowContent.value*1 ;
			}
	    }
	
	return tmp;
    }

}


function deleteRow(isSelf,tableid,tableidother,row,rowcontent,props,batchnoArray)
{

	//单独处理一下已对符数据的取消勾选
	if(rowcontent.values.batchno.value)
	{
		let batchno = rowcontent.values.batchno.value;
		//实现批量手工取消勾对
		batchnoArray.push(batchno);
		let selfdata = props.cardTable.getColValue(tableid,'batchno');
		let otherdata = props.cardTable.getColValue(tableidother,'batchno');
		for(let i=0;i<selfdata.length;i++)
		{
			if(selfdata[i].value&&selfdata[i].value==batchno)
			{
				props.cardTable.setValByKeyAndIndex(tableid,i,'batchno',{value: null});
				props.cardTable.setValByKeyAndIndex(tableid,i,'amountequal',{value: false});
				props.cardTable.setValByKeyAndIndex(tableid,i,'quantityequal',{value: false});
			}
		}
		for(let j=0;j<otherdata.length;j++)
		{
			if(otherdata[j].value&&otherdata[j].value==batchno)
			{
				props.cardTable.setValByKeyAndIndex(tableidother,j,'batchno',{value: null});
				props.cardTable.setValByKeyAndIndex(tableidother,j,'amountequal',{value: false});
				props.cardTable.setValByKeyAndIndex(tableidother,j,'quantityequal',{value: false});
			}
		}

	}
	else
	{
		let no = getSelfRowNoMap(isSelf).get(row);
		if(no) {
			let selfNoMap = getSelfNoMap(isSelf);
			let selfRowList = selfNoMap.get(no);
			props.cardTable.setValByKeyAndIndex(tableid,row,'no',{value: null});
			getSelfRowNoMap(isSelf).delete(row);
			if(selfRowList && selfRowList.length>0) {
				selfRowList.splice(selfRowList.indexOf(row), 1 );
				if(selfRowList.length==0) {
					selfNoMap.delete(no);
					if(getSelfMaxNo(isSelf)==no && getSelfMaxNo(isSelf) >1) {
						setSelfMaxNo(isSelf,no-1);
					}
				}
			}	
		}	
	}
}



