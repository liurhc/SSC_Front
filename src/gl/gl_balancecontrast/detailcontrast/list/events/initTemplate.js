import { base, ajax,toast,getBusinessInfo,cacheTools } from 'nc-lightapp-front';
let { NCPopconfirm } = base;
const formId = 'head';
const subformId1 = 'subhead1';
const subformId2 = 'subhead1';
const tableId1 = 'subbody1';
const tableId2 = 'subbody2';
const pageId = '20022010_LIST';

export default function(props) {
	let page=this;
	let appcode = props.getUrlParam('c')||props.getSearchParam('c');
	cacheTools.set('detailappcode',appcode); 
	props.createUIDom(
		{
			pagecode: pageId,
			appcode:appcode
		}, 
		function (data){
			if(data){
				if(data.template){
					let meta = data.template;
					modifierMeta(props, meta,page)
					props.meta.setMeta(meta);
				}
				if(data.button){
					let button = data.button;
					props.button.setButtons(button);
				}
				if(data.context){
					if(!props.getUrlParam('jumpflag'))
					{	
						//查询面板的开始日期和截至日期
						let businessInfo = getBusinessInfo();
						let senddata =
							{
								pk_org:null,
								busiDate:businessInfo.businessDate 
							}
						if(data.context.defaultAccbookPk)
						{
							cacheTools.set('detailaccountingbookpk',data.context.defaultAccbookPk); 
							cacheTools.set('detailaccountingbookname',data.context.defaultAccbookName); 
							page.defaultAccbook.refname=data.context.defaultAccbookName;
							page.defaultAccbook.refpk=data.context.defaultAccbookPk;
							senddata.pk_org = data.context.defaultAccbookPk;
						}
						ajax({
							url: '/nccloud/gl/sumcontrast/date.do',
							data: senddata,
							success: (res) => { 
							//返回规则中的内容
							//对规则的查询统一在切换规则的时候做
							if(res.data)
							{
									let begindate=res.data.begindate;
									cacheTools.set('detailbegindate',begindate); 
									let enddate=res.data.enddate;
									cacheTools.set('detailenddate',enddate);  
									cacheTools.set('detailreacheddate',enddate); 
							}}
						})
					}
				}
				//页面跳转，初始化页面数据
				if(props.getUrlParam('jumpflag')){
					let queryvo = cacheTools.get('detailqueryvo');
					getInitData(queryvo,props);
				}
			}
		}
	)
}

function modifierMeta(props, meta,page) {
	let status = props.getUrlParam('status');
	meta[formId].status = status;
	meta[subformId1].status = status;
	meta[tableId1].status = status;
	meta[subformId2].status = status;
	meta[tableId2].status = status;
	//let multiLang = props.MutiInit.getIntl('2052');
	return meta;
}

function getInitData(searchData,props){
	//按钮控制
	let content = cacheTools.get('detailmain');
	if(content=='N')
	{
		props.button.setButtonDisabled('make',false);
	}
	else
	{
		props.button.setButtonDisabled('make',true);
	}
	ajax({
		url: '/nccloud/gl/detailcontrast/show.do',
		data: searchData,
		success: (res) => { 
		if(res.data){
			//设置表头数据
			props.form.setFormItemsValue('head',{'contrastRule':{value:res.data.contrastRule,display:null}});
			props.form.setFormItemsValue('head',{'dateArea':{value:res.data.dateArea,display:null}});
			props.form.setFormItemsValue('head',{'ass':{value:res.data.ass,display:null}});
			props.form.setFormItemsValue('head',{'corpKind':{value:res.data.corpKind,display:null}});
		
			//本方表头
			props.form.setFormItemsValue('subhead1',{'selfAccBook':{value:res.data.selfAccBook,display:null}});
			props.form.setFormItemsValue('subhead1',{'selfaccounts':{value:res.data.selfaccounts,display:null}});
			
			//对方表头
			props.form.setFormItemsValue('subhead2',{'otherAccBook':{value:res.data.otherAccBook,display:null}});
			props.form.setFormItemsValue('subhead2',{'otheraccounts':{value:res.data.otheraccounts,display:null}});
			}
		}
	})
	ajax({
	url: '/nccloud/gl/detailcontrast/query.do',
	data: searchData,
	success: (res) => { 
		if (res.data) {
			if(res.data['subbody1'])
			{
				props.cardTable.setTableData('subbody1', res.data['subbody1'].subbody1);
			}
			else
			{
				props.cardTable.setTableData('subbody1', {rows:[]});
			}
			if(res.data['subbody2'])
			{
				props.cardTable.setTableData('subbody2', res.data['subbody2'].subbody2);
			}
			else
			{
				props.cardTable.setTableData('subbody2', {rows:[]});
			}
			//隐藏字段
			setQueryContent(searchData.amount,searchData.quantity,cacheTools.get('detailcontent'),props);
			props.cardTable.setStatus('subbody1','edit',null);
			props.cardTable.setStatus('subbody2','edit',null);

		}
		else
		{
			toast({ color: 'success', content: page.state.json['20022010-000027'] });/* 国际化处理： 未查询到数据*/
			props.cardTable.setTableData('subbody1', {rows:[]});
			props.cardTable.setTableData('subbody2', {rows:[]});
		}
	}
	})
}
//设置隐藏列
function setQueryContent(amount,quantity,content,props){
	let tableid1 = 'subbody1';
	let tableid2 = 'subbody2'

	if(amount==true&&quantity==true)
	{
		props.cardTable.showColByKey(tableid1,['quantityequal','amountequal']);
		props.cardTable.showColByKey(tableid2,['quantityequal','amountequal']);
	}
	if(amount==true&&quantity==false)
	{
		props.cardTable.hideColByKey(tableid1,'quantityequal');
		props.cardTable.hideColByKey(tableid2,'quantityequal');
		props.cardTable.showColByKey(tableid1,'amountequal');
		props.cardTable.showColByKey(tableid2,'amountequal');
	}
	else if (amount==false&&quantity==true)
	{
		props.cardTable.hideColByKey(tableid1,'amountequal');
		props.cardTable.hideColByKey(tableid2,'amountequal');
		props.cardTable.showColByKey(tableid1,'quantityequal');
		props.cardTable.showColByKey(tableid2,'quantityequal');
	}

	if(content[0]==='N')
	{
		props.cardTable.hideColByKey(tableid1,['debitquantity','creditquantity']);
		props.cardTable.hideColByKey(tableid2,['debitquantity','creditquantity']);
	}
	else
	{
		props.cardTable.showColByKey(tableid1,['debitquantity','creditquantity']);
		props.cardTable.showColByKey(tableid2,['debitquantity','creditquantity']);
	}
	if(content[1]==='N')
	{
		props.cardTable.hideColByKey(tableid1,['debitamount','creditamount']);
		props.cardTable.hideColByKey(tableid2,['debitamount','creditamount']);
	}
	else
	{
		props.cardTable.showColByKey(tableid1,['debitamount','creditamount']);
		props.cardTable.showColByKey(tableid2,['debitamount','creditamount']);
	}
	if(content[2]==='N')
	{
		props.cardTable.hideColByKey(tableid1,['debitorgamount','creditorgamount']);
		props.cardTable.hideColByKey(tableid2,['debitorgamount','creditorgamount']);
	}
	else
	{
		props.cardTable.showColByKey(tableid1,['debitorgamount','creditorgamount']);
		props.cardTable.showColByKey(tableid2,['debitorgamount','creditorgamount']);
	}
	if(content[3]==='N')
	{
		props.cardTable.hideColByKey(tableid1,['debitgroupamount','creditgroupamount']);
		props.cardTable.hideColByKey(tableid2,['debitgroupamount','creditgroupamount']);
	}
	else
	{
		props.cardTable.showColByKey(tableid1,['debitgroupamount','creditgroupamount']);
		props.cardTable.showColByKey(tableid2,['debitgroupamount','creditgroupamount']);
	}
	if(content[4]==='N')
	{
		props.cardTable.hideColByKey(tableid1,['debitglobalamount','creditglobalamount']);
		props.cardTable.hideColByKey(tableid2,['debitglobalamount','creditglobalamount']);
	}
	else
	{
		props.cardTable.showColByKey(tableid1,['debitglobalamount','creditglobalamount']);
		props.cardTable.showColByKey(tableid2,['debitglobalamount','creditglobalamount']);
	}

}
