import {ajax } from 'nc-lightapp-front';

let requestDomain =  '';

let requestApiOverwrite = {
    // 查询接口
    query: (opt) => {
        ajax({
            url: '/nccloud/gl/contrastrule/query.do',
            data: opt.data,
            success: opt.success
        });
    },
    // 删除
    delete: (opt) => {
        ajax({
            url: '/nccloud/gl/contrastrule/delete.do',
            data: opt.data,
            success: opt.success
        });
    },
    // 启用停用
    startstop: (opt) => {
        ajax({
            url: '/nccloud/gl/contrastrule/start.do',
            data: opt.data,
            success: opt.success,
            error:opt.error
        });
    },
    // 查询接口
    editCheck: (opt) => {
        ajax({
            url: '/nccloud/gl/contrastrule/contrastruleeditcheck.do',
            data: {pk_contrastrule: opt.data},
            success: opt.success
        });
    }
}

export default  requestApiOverwrite;
