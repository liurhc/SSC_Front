/**页面全局变量 */
let currentVar = {
    pagecode: 'yxysdy',
    list: 'contrastRulelist',
    headButtonArea: 'list_head',
    listButtonArea: 'list_inner',
    // cardURL: '/gl/gl_balancecontrast/contrastrule/cardGlobal/index.html',
    cardURL: '/cardGlobal',
    cardPageCode: '200017GLOBA_CARD',
    createOrg:'GLOBLE00000000000000',
    dataSource: 'gl.gl_balancecontrast.contrastrule',
    pkname: 'pk_contrastrule'
}
window.presetVar = {
    ...currentVar
};
export default currentVar
