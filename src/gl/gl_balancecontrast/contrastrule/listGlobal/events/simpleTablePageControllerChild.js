import {base, promptBox,toast,cardCache} from 'nc-lightapp-front';
const {NCMessage} = base;
import simpleTablePageController from '../../../../public/common/simpleTablePageController'
import pubUtils from '../../../../public/common/pubUtil'
import presetVar from '../presetVar'
import requestApi from '../requestApi'
import pubUtil from '../../../../public/common/pubUtil';
let {setDefData, getDefData } = cardCache;
class simpleTablePageControllerChild extends simpleTablePageController{
    constructor(main, props, meta, initFlag) {
        let newMeta = {
            ...meta, 
            dataSource: (meta || {}).dataSource || presetVar.dataSource,
            listPkField: (meta || {}).listPkField || 'pk_contrastrule',
            listId: (meta || {}).listId || presetVar.list,
            listButtonArea: (meta || {}).listButtonAreas || presetVar.listButtonArea,
            // listButtonAreas: (meta || {}).listButtonAreas || {[presetVar.list]:presetVar.listButtonArea},
            cardUrl: (meta || {}).cardUrl || presetVar.cardURL,
            cardPageCode: (meta || {}).cardPageCode || presetVar.cardPageCode,
        };
        super(main, props, newMeta, initFlag);
    }

    initMeta(meta){
        this.props.button.setPopContent('Enable',this.main.state.json['200017-000004']);/* 国际化处理： 确定要启用吗？*/
        this.props.button.setPopContent('Disable',this.main.state.json['200017-000005']);/* 国际化处理： 确定要停用吗？*/
        meta[ presetVar.list].items = meta[ presetVar.list].items.map((item, key) => {
            if(item.attrcode=="startstatus"){
                item.itemtype="select";
            }
            //单据号添加下划线超链接
            if (item.attrcode == 'code') {
                item.render = (text, record, index) => {
                    return (
                    <span
                        style={{  cursor: 'pointer',color: '#007ace'}}
                        onClick={() => {
                            this.setAllPks();
                                let newCacheData = this.getCacheData();
                                if(newCacheData){
                                    this.setCacheData(newCacheData);
                                }
                                this.setDataSource(record, index);
                            pubUtil.pushTo(this.props, this.cardUrl,{
                                appcode: this.props.getSearchParam('c'),
                                pagecode: this.cardPageCode,
                                status: 'browse',
                                open_status: 'browse',
                                id: record.pk_contrastrule.value,
                                ...this.AddListRowDoubleClickParam(record, index)
                            });
                        }}
                    >
                        {record.code && record.code.value}
                    </span>
                );
            };
        }
            return item;
        });
        
    }

    Edit(AreaId, data, index){
        let id=data[this.listPkField].value;
        requestApi.editCheck({
            data: id,
            success: (data) => {
                this.setAllPks();
                    let newCacheData = this.getCacheData();
                    if(newCacheData){
                        this.setCacheData(newCacheData);
                    }
                    this.setDataSource(data, index);
                    pubUtil.pushTo(this.props, this.cardUrl,{
                    appcode: this.props.getSearchParam('c'),
                    pagecode: this.cardPageCode,
                    status: 'edit',
                    open_status: 'edit',
                    id: id
                }); 
            }
        })
        
    }
    /**
     * 取得表格行按钮
     * @param {*} text 
     * @param {*} record 
     * @param {*} index 
     */
    getListButtons(text, record, index){
        if(record.startstatus.value == '1'){
            return ['Edit', 'Disable'];
        }else{
            return ['Edit', 'DelLine', 'Enable'];
        }
    }
    /**
     * 查询数据
     * @param {*} key 
     */
    queryData(key){
        let { hasCacheData } = this.props.table;
	    // if(!hasCacheData(presetVar.dataSource)){
            requestApi.query({
                data: pubUtils.getSysCode(this.props),
                success: (data) => {
                    this.setTableData(data);
                    setDefData(presetVar.list, presetVar.dataSource, data);
                }
            })
        // }
    }
    /**
     * 删除数据
     * @param {*} data 
     * @param {*} index 
     */
    doDelLine(data, index){
        let _this = this;
        requestApi.delete({
            data: {pk_contrastrule: data['pk_contrastrule'].value, ...pubUtils.getSysCode(_this.props)},
            success: (data) => {
                _this.props.table.deleteTableRowsByIndex(_this.listId, index);
                pubUtils.showMeggage(_this.props, pubUtils.MESSAGETYPE.DELETE,_this);
            }
        })
    }
    /**
     * 启用
     */
    Enable(AreaId, data, index){
        this.cfStartStop(data.pk_contrastrule.value, data.startstatus.value, index);
        // promptBox({
        //     color:"warning",
        //     title: "取消？",
        //     content: '确认要取消吗',
        //     beSureBtnName: "确认",
        //     cancelBtnName: "取消" ,
        //     beSureBtnClick: this.doEnable.bind(this.props,this,AreaId, data, index)
        //   });
    }
    doEnable(){
        this.cfStartStop(data.pk_contrastrule.value, data.startstatus.value, index);
    }
    /**
     * 停用
     */
    Disable(AreaId, data, index){
        this.cfStartStop(data.pk_contrastrule.value, data.startstatus.value, index);
    }
    // 自定义方法-------------------------------------------------------------
    /**
     * 启用停用处理函数
     * @param {*} flag 
     */
    cfStartStop(pk_contrastrule,startstatus,index){
        requestApi.startstop({
            data: {
                pk_contrastrule: pk_contrastrule,
                startstatus: startstatus
            },
            success: (data) => {
                let date = '';
                if (data.data.enddate) {
                    date = data.data.enddate;
                } else {
                    date = data.data.startdate;
                }
                this.cfCallBackStartStop(startstatus, index, date);
            }
        })
    }
    /**
     * 启用停用处理回掉
     * @param {*} index 
     */
    cfCallBackStartStop(startstatus, index, date){
        // TODO：多语
        if(startstatus == '1'){
            this.props.table.setValByKeyAndIndex(presetVar.list, index, 'startstatus', {value: '-1', display:this.main.state.json['200017-000006']});/* 国际化处理： 停用*/
            this.props.table.setValByKeyAndIndex(presetVar.list, index, 'enddate', {value: date, display:date});
            toast({color:"success",content: this.main.state.json['200017-000007']});/* 国际化处理： 停用成功*/
        }else if(startstatus == '0'){
            this.props.table.setValByKeyAndIndex(presetVar.list, index, 'startstatus', {value: '1', display:this.main.state.json['200017-000008']});/* 国际化处理： 启用*/
            this.props.table.setValByKeyAndIndex(presetVar.list, index, 'startdate', {value: date, display:date});
            toast({color:"success",content: this.main.state.json['200017-000009']});/* 国际化处理： 启用成功*/
        }else if(startstatus == '-1'){
            this.props.table.setValByKeyAndIndex(presetVar.list, index, 'startstatus', {value: '1', display:this.main.state.json['200017-000008']});/* 国际化处理： 启用*/
            this.props.table.setValByKeyAndIndex(presetVar.list, index, 'enddate', {value: null, display:null});
            toast({color:"success",content: this.main.state.json['200017-000009']});/* 国际化处理： 启用成功*/
        }
        
    }
}
export default simpleTablePageControllerChild;
