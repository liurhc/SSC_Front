import {Component} from 'react';
import {createPage,getMultiLang,createPageIcon} from 'nc-lightapp-front';
import BusinessUnitTreeRef from '../../../../uapbd/refer/org/BusinessUnitTreeRef';
import simpleTablePageControllerChild from './events/simpleTablePageControllerChild'
import presetVar from './presetVar'
import HeaderArea from '../../../public/components/HeaderArea';
import './index.less';
//内部交易对账规则列表页
class ContrastruleList extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.simpleTablePageControllerChild = new simpleTablePageControllerChild(this, props);
        this.state={
            json:{}
        }
    }   
    componentDidMount(){    

    }
    componentWillMount(){
		let callback= (json) =>{
			this.setState({json:json},()=>{
                
			})
		}
        getMultiLang({moduleId:'200017',domainName:'gl',currentLocale:'zh-CN',callback});
	}
    componentDidUpdate(){
        // let table=document.getElementsByClassName("u-table-body")
        // table[0].style.maxHeight="0"
        // table[0].style.height=(window.innerHeight-120)+"px"
        // table[0].style.minHeight=(window.innerHeight-120)+"px"
    }
    render() {
        const {table, search, button} = this.props;
        const {createSimpleTable} = table;
        const {createButtonApp, createOprationButton} = button;

        return (
            <div id="dfkmdzgxsz" className="nc-bill-list">
                <HeaderArea
                    title={this.state.json['200017-000010']}
                    btnContent={createButtonApp({
                        area: presetVar.headButtonArea,
                        buttonLimit: 3,
                        onButtonClick: (props, actionId) => { this.simpleTablePageControllerChild.headButtonClick(actionId) }
                    })}
                />
                <div className="nc-bill-table-area">
                    {createSimpleTable(presetVar.list, {
                        dataSource: presetVar.dataSource,
                        pkname: presetVar.pkname,
                        onRowDoubleClick: (record, index)=>{this.simpleTablePageControllerChild.listRowDoubleClick(record, index)},
                        showCheck: false,
                        showIndex: true,                       
                    })}
                </div>
            </div>
        )
    }
}
ContrastruleList = createPage({})(ContrastruleList);
export default ContrastruleList;
