import cardPageController from '../../cardGlobal/events/cardPageControllerChild'
import presetVar from '../../listGlobal/presetVar'
import cardPresetVar from '../../cardGlobal/presetVar'
import {getBusinessInfo} from 'nc-lightapp-front';
class cardPageControllerChild extends cardPageController{
    constructor(main, props, meta) {
        super(main, props, {
            // listUrl: '/gl/gl_balancecontrast/contrastrule/listGroup/index.html',
            listUrl: '/listGroup',
            listPageCode: '200018GROUP_LIST',
            cardURL: '/cardGroup',
        });
    }
    
    /**
     * ҳ��״̬�������
     * @param {*} state 
     */
    setPageState(state){
        super.setPageState(state);
        switch(state){
            case 'browse':
                this.props.button.setButtonVisible(['Enable'], true);
                this.props.button.setButtonVisible(['Disable'], true);
                break;
            case 'add':
                let meta = this.props.meta.getMeta();
                // // 更新本方、对方科目显示项目
                // meta[presetVar.list].items.map((one)=>{
                //     if(one.attrcode == 'code' || one.attrcode == 'name'){
                //         one.visible = true;
                //     }
                // })
                // meta[presetVar.list2].items.map((one)=>{
                //     if(one.attrcode == 'code' || one.attrcode == 'name'){
                //         one.visible = true;
                //     }
                // })
                // props.meta.setMeta(meta);
                this.listIds.map((one1)=>{
                    meta[one1].items.map((one)=>{
                        if(one.attrcode == 'code' || one.attrcode == 'name'){
                            one.disabled = false;
                        }
                    })
                    this.props.meta.setMeta(meta);
                    // this.props.editTable.setStatus(one1, 'edit');
                })
                this.props.button.setButtonVisible(['Enable'], false);
                this.props.button.setButtonVisible(['Disable'], false);
                let businessInfo = getBusinessInfo();
                this.props.form.setFormItemsValue(cardPresetVar.formId, {'createorg':{display:businessInfo.groupName,value:businessInfo.groupId}});
                break;
            case 'edit':
                this.props.button.setButtonVisible(['Enable'], false);
                this.props.button.setButtonVisible(['Disable'], false);
                break;
        }
    }
    /**
     * 集团数据不能操作
     * @param {*} data 
     */
    setPageData(data){
        let props = this.props;
        let isGlobal = data.data.head.head.rows[0].values.createorg.value == presetVar.createOrg;
        props.form.setFormItemsDisabled(this.formId, {startstatus:isGlobal});
        props.button.setButtonDisabled(['Edit', 'Delete', 'Enable', 'Disable'], isGlobal);
        super.setPageData(data);
    }

    initTemplateSetPageState(data){
        super.initTemplateSetPageState(data);
        let pageStatus = this.props.getUrlParam('status');
        this.setPageState(pageStatus);
        if(pageStatus == 'browse'){
            this.queryData(this.id);
        }else if(pageStatus == 'edit'){
            this.queryData(this.id);
        }else if(pageStatus == 'add'){
            let businessInfo = getBusinessInfo();
            this.props.form.setFormItemsValue(cardPresetVar.formId, {'createorg':{display:businessInfo.groupName,value:businessInfo.groupId}});
        }
    }
}export default cardPageControllerChild;
