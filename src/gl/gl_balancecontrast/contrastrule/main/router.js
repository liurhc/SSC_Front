import { asyncComponent } from 'nc-lightapp-front';

const ContrastinitListGlobal = asyncComponent(() => import(/* webpackChunkName: "gl/gl_balancecontrast/contrastrule/listGlobal/listGlobal" */ /* webpackMode: "eager" */ '../listGlobal/contrastruleList'));
const ContrastinitCardGlobal = asyncComponent(() => import(/* webpackChunkName: "gl/gl_balancecontrast/contrastrule/cardGlobal/cardGlobal" */ /* webpackMode: "eager" */ '../cardGlobal/contrastruleCard'));
const ContrastinitListGroup = asyncComponent(() => import(/* webpackChunkName: "gl/gl_balancecontrast/contrastrule/listGroup/listGroup" */ /* webpackMode: "eager" */ '../listGroup/contrastruleList'));
const ContrastinitCardGroup = asyncComponent(() => import(/* webpackChunkName: "gl/gl_balancecontrast/contrastrule/cardGroup/cardGroup" */ /* webpackMode: "eager" */ '../cardGroup/contrastruleCard'));


const routes = [
	{
		path: '/listGlobal',
		component: ContrastinitListGlobal
	},
	{
		path: '/cardGlobal',
		component: ContrastinitCardGlobal
	},
	{
		path: '/listGroup',
		component: ContrastinitListGroup
	},
	{
		path: '/cardGroup',
		component: ContrastinitCardGroup
	}

];

export default routes;
