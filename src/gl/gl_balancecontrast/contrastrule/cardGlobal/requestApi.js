import {ajax } from 'nc-lightapp-front';
import pubUtils from '../../../public/common/pubUtil'

let requestDomain =  '';

let requestApiOverwrite = {
    // 查询接口
    query: (opt) => {
        ajax({
            url: '/nccloud/gl/contrastrule/cardquery.do',
            data: opt.data,
            success: opt.success
        });
    },
    // 保存
    save: (opt) => {
        ajax({
            url: '/nccloud/gl/contrastrule/manager.do',
            data: {...opt.data, ...{['userjson']: JSON.stringify(pubUtils.getSysCode(opt.props))}},
            success: opt.success
        });
    },
    // 删除
    delete: (opt) => {
        ajax({
            url: '/nccloud/gl/contrastrule/delete.do',
            data: {...opt.data, ...{['userjson']: JSON.stringify(pubUtils.getSysCode(opt.props))}},
            success: opt.success
        });
    },
    // 取得模板
    getTemplet: (opt) => {
        ajax({
            url: '/nccloud/gl/contrastrule/templet.do',
            data: opt.data,
            success: opt.success
        });
    },
    // 是否启用集团或全局本币
    queryCurr: (opt) => {
        ajax({
            url: '/nccloud/gl/contrastrule/queryGroupOrGlobalCurrInfoAction.do',
            data: opt.data,
            success: opt.success
        });
    },
    editCheck: (opt) => {
        ajax({
            url: '/nccloud/gl/contrastrule/contrastruleeditcheck.do',
            data: {pk_contrastrule: opt.data},
            success: opt.success
        });
    }
}

export default  requestApiOverwrite;
