/**页面全局变量 */
let currentVar = {
    pageCode: 'yxysdy',
    list: 'contrastrulelist1',
    list2: 'contrastrulelist2',
    list3: 'contrastRuleAsslist',
    headArea: 'head',
    bodyArea: 'bodys',
    formId: 'head',
    childFormId1: 'contrastrulelist',
    childFormId2: 'dateset',
    childFormId3: 'contrastcontent',
	headButtonArea: 'card_head',
    listButtonArea: 'card_body_inner',
    listShoulderButtonArea: 'card_body',
    // listURL: '/gl/gl_balancecontrast/contrastrule/listGlobal/index.html',
    listURL: '/listGlobal',
    cardURL: '/cardGroup',
    
    listPageCode: '200017GLOBA_LIST',
    pkname:'pk_contrastrule',
    /**
     * 表单项目
     */
    formField:{
        /**
         * 按主薄对账
         */
        ismainorgcontrast: 'ismainorgcontrast'
    }
    
}
window.presetVar = {
    ...currentVar
};
export default currentVar
