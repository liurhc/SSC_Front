import {Component} from 'react';
import {createPage, base, cacheTools,getMultiLang,createPageIcon} from 'nc-lightapp-front';
const {NCTabsControl, NCBackBtn} = base;
import cardPageControllerChild from './events/cardPageControllerChild'
import presetVar from './presetVar'
import listPresetVar from '../listGlobal/presetVar'
import HeaderArea from '../../../public/components/HeaderArea';
import './index.less';
//内部交易对账规则-全局 列表页
class ContrastruleCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            paginationShowClassName: '',
            selectTab: 0,
            json:{}
        };
        // 页面初始化
        this.cardPageControllerChild = new cardPageControllerChild(this, props);
    }

    componentDidMount(){
        // let icon=document.getElementsByClassName("back-btn")   
        // icon[0].innertext=""
        // icon[0].innerHTML=`<i class="uf uf-minus"></i>`
        // icon[0].fontSize="15px"
        // let title=document.getElementsByClassName("tab_title")
        
          
    }
    componentWillMount() {
        let callback= (json) =>{
			this.setState({json:json},()=>{
			})
		}
        getMultiLang({moduleId:'200017',domainName:'gl',currentLocale:'zh-CN',callback});
           window.onbeforeunload = () => {
            let status = this.props.getUrlParam("status");
            if (status == 'edit' || status == 'add') {
                return this.state.json['200017-000000']/* 国际化处理： 确定要离开吗？*/
            }
        }
    }
    render() {
        const {editTable, search, button, cardPagination, modal, form} = this.props;
         const {createModal} = modal;
        const {createForm} = form;
        
        const { createCardPagination } = cardPagination;
        const {createEditTable} = editTable;
        const {createButtonApp} = button;
        const {NCCreateSearch} = search;
        let status=this.props.getUrlParam('status');
        let getCreateCardPagination=()=>{
            
            if(this.props.getUrlParam('status') == 'browse' || this.props.getUrlParam('status') == 'edit'){
                return(
                    <div className={'header-cardPagination-area ' + this.state.paginationShowClassName} style={{float:'right'}}>
                    {createCardPagination({
                        dataSource: listPresetVar.dataSource,
                        handlePageInfoChange:(props,data)=>{this.cardPageControllerChild.pageInfoClick(data)}
                    })}
                </div>
                )
            }
        }

        let getShowList = (tab) =>{
            if(this.state.selectTab == tab){
                return 'show';
            }else{
                return 'hide';
            }
        }

        return (
            <div id="qccshcard_card" className="nc-bill-card">
                <div className="nc-bill-top-area">
                    <HeaderArea
                        initShowBackBtn={status == 'browse'}
                        backBtnClick={this.cardPageControllerChild.backButtonClick.bind(this.cardPageControllerChild)}
                        title={this.state.json['200017-000010']}
                        btnContent={createButtonApp({
                            area: presetVar.headButtonArea,
                            buttonLimit: 3,
                            onButtonClick: (props, actionId) => { this.cardPageControllerChild.headButtonClick(actionId) }
                        })}
                        pageBtnContent={getCreateCardPagination()}
                    />
                    <div className="nc-bill-form-area">
                        {createForm(presetVar.formId, {
                            fieldid: 'contrastrule',
                            expandArr: [ presetVar.childFormId1, presetVar.childFormId2, presetVar.childFormId3],
                            onAfterEvent: (props,moduleId, key, value, oldValue)=>{this.cardPageControllerChild.cfAfterEventForm(moduleId, key, value, oldValue)}
                        })}
                    </div>
                </div>
                <div className="">
                    <div className="tab-definInfo-area" style={{position:'relative'}}>
                        <NCTabsControl defaultKey={this.state.selectTab}>
                            <div key={0} clickFun={()=>this.cardPageControllerChild.cfNavChangeFun(0)}>
                                {this.state.json['200017-000012']}
                            </div>
                            <div key={1} clickFun={()=>this.cardPageControllerChild.cfNavChangeFun(1)}>
                                {this.state.json['200017-000013']}
                            </div>
                            {/* <div key={2} clickFun={()=>this.cardPageControllerChild.cfNavChangeFun(2)}>
                                {multiLang && multiLang.get('2002CONTRASTRULE-0004')}
                            </div> */}
                        </NCTabsControl>
                        <div className="shoulder-definition-area">
                        <div className="definition-icons">
                            {createButtonApp({
                                area: presetVar.listShoulderButtonArea, 
                                buttonLimit: 3,
                                onButtonClick: (props, actionId)=>{this.cardPageControllerChild.headButtonClick(actionId)}
                            })}
                        </div>
                    </div>
                </div>
                    <div className={getShowList(0)}>
                        {createEditTable(presetVar.list, {
                            onAfterEvent:(props, moduleId, key, value, changedrows, record, index)=>this.cardPageControllerChild.cfAfterEventList(moduleId, key, value, changedrows, index, record),
                            showIndex: true
                        })}
                    </div>
                    <div className={getShowList(1)}>
                        {createEditTable(presetVar.list2, {
                            onAfterEvent:(props, moduleId, key, value, changedrows, record, index)=>this.cardPageControllerChild.cfAfterEventList(moduleId, key, value, changedrows, index, record),
                            showIndex: true
                        })}
                    </div>
                    {/* <div className={getShowList(2)}>
                        {createEditTable(presetVar.list3, {
                            onAfterEvent:(props, moduleId, key, value, changedrows, record, index)=>this.cardPageControllerChild.cfAfterEventList(moduleId, key, value, changedrows, index, record),
                            onBeforeEvent:(props, moduleId, item, index, value, record)=>this.cardPageControllerChild.cfBeforeEvent(moduleId, item, index, value, record),
                            showIndex: true
                        })}
                    </div> */}
                </div>
                {createModal('delete', {
                    className:"dielate",
                    title: this.state.json['2002-0003'],
                    content: this.state.json['2002-0004'],
                    beSureBtnClick: ()=>{this.cardPageControllerChild.doDelete()}
                })}
            </div>

        )
    }
}
ContrastruleCard = createPage({
    orderOfHotKey:[presetVar.formId, presetVar.list, presetVar.list2]
})(ContrastruleCard);
export default ContrastruleCard;
