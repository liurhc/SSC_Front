let changeCodeNameTemplate = (one, value) => {
    if(value.value == true){
        if(one.attrcode == 'code' || one.attrcode == 'name'){
            one.visible = false;
        }else if(one.attrcode == 'codeInput'){
            one.visible = true;
        }
    }else{
        if(one.attrcode == 'code' || one.attrcode == 'name'){
            one.visible = true;
        }else if(one.attrcode == 'codeInput'){
            one.visible = false;
        }

    }
}
export default {changeCodeNameTemplate}
