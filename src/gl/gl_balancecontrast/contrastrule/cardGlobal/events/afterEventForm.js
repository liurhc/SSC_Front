import presetVar from '../presetVar'
import utils from './utils'
import cardPageControllerChild from './cardPageControllerChild'
/**
 * 表单值变更事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} oldValue 
 */
export default function(page,props, moduleId, key, value, oldValue){
    switch(key){
        // 按主账簿对账
        case 'ismainorgcontrast':
            let meta = props.meta.getMeta();
            // 设置帐簿类型是否可用
            meta[presetVar.formId].items.map((one)=>{
                if(one.attrcode == 'pk_book'){
                    if(value.value == false){
                        one.disabled = false;
                        props.form.setFormItemsDisabled(presetVar.formId,{pk_book:false});
                    }else{
                        one.disabled = true;
                        props.form.setFormItemsDisabled(presetVar.formId,{pk_book:true});
                        props.form.setFormItemsValue(presetVar.formId,{pk_book: {value: '' , display: ''}});
                    }
                }
            })
            // 按主账簿对账取消勾选时，清空本方、对方科目表体行
            if(value.value == false){
                let allRows1 = props.editTable.getAllRows(presetVar.list, true);
                if(allRows1!=null){
                    allRows1.map((one)=>{
                        // TODO: 平台问题，同步情况下无法连续删除行
                        window.setTimeout(() => {
                            props.editTable.deleteTableRowsByRowId(presetVar.list, one.rowid);        
                        },10);
                    })
                }
                let allRows2 = props.editTable.getAllRows(presetVar.list2, true);
                if(allRows2!=null){
                    allRows2.map((one)=>{
                        // TODO: 平台问题，同步情况下无法连续删除行
                        window.setTimeout(() => {
                            props.editTable.deleteTableRowsByRowId(presetVar.list2, one.rowid);        
                        },10);
                    })
                }
            }
            // 更新本方、对方科目显示项目
            meta[presetVar.list].items.map((one)=>{
                utils.changeCodeNameTemplate(one ,value);
            })
            meta[presetVar.list2].items.map((one)=>{
                utils.changeCodeNameTemplate(one ,value);
            })
            props.meta.setMeta(meta);

        break;
        case 'pk_book':
            if(value.value!=oldValue.value){
                let allRows1 = props.editTable.getAllRows(presetVar.list, true);
                if(allRows1!=null){
                    allRows1.map((one)=>{
                        // TODO: 平台问题，同步情况下无法连续删除行
                        window.setTimeout(() => {
                            props.editTable.deleteTableRowsByRowId(presetVar.list, one.rowid);        
                        },10);
                    })
                }
                let allRows2 = props.editTable.getAllRows(presetVar.list2, true);
                if(allRows2!=null){
                    allRows2.map((one)=>{
                        // TODO: 平台问题，同步情况下无法连续删除行
                        window.setTimeout(() => {
                            props.editTable.deleteTableRowsByRowId(presetVar.list2, one.rowid);        
                        },10);
                    })
                }
            }
        break;
        case 'startstatus':
            let startstatus = (props.form.getFormItemsValue(presetVar.formId, 'startstatus') || {}).value;
            if(startstatus==true){
                page.main.cardPageControllerChild.Enable();
                // props.button.setButtonDisabled("Delete",true);
            }else{
                page.main.cardPageControllerChild.Disable();
                // props.button.setButtonDisabled("Delete",false);
            }
        break;
    }
}
