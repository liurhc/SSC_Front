import presetVar from '../presetVar'
import requestApi from '../requestApi'
import {getBusinessInfo} from 'nc-lightapp-front';
/**
 * 表体内容变更后处理
 * @param {*} that 
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedrows 
 * @param {*} record 
 * @param {*} index 
 */
export default function(that, props, moduleId, key, value, changedrows, record, index){
    if(moduleId == presetVar.list || moduleId == presetVar.list2){
        // 本方、对方科页签目内容变更
        if(key == 'code'){
            let code=value.refcode;
            if(!value.hasOwnProperty('refcode')){
                code=value.code;
            }
            // 编码带出PK名称
            props.editTable.setValByKeyAndIndex(moduleId, index, 'pk_accasoa', {
                value:value.refpk
            });
            props.editTable.setValByKeyAndIndex(moduleId, index, 'code', {
                value:code,
                display:code
            });
            if(props.form.getFormItemsValue(presetVar.formId, presetVar.formField.ismainorgcontrast).value != true){
                props.editTable.setValByKeyAndIndex(moduleId, index, 'name', {
                    value:value.refname,
                    display:value.refname
                });
            }
            props.editTable.setValByKeyAndIndex(moduleId, index, 'codeInput', {
                value:code
            });
        }else if(key == 'codeInput'){
            // 手输编码联动（保存用）
            props.editTable.setValByKeyAndIndex(moduleId, index, 'pk_accasoa', {
                value:value
            });
            props.editTable.setValByKeyAndIndex(moduleId, index, 'code', {
                value:value
            });
        }
    }else if(moduleId == presetVar.list3){
        // 辅助核算页签内容变更
        if(key == 'pk_checktype'){
            // 辅助核算类型
            if(!that.doccodeRefCode[value.refpk] && value.refpk){
                requestApi.getTemplet({
                    data: {pk_checktype: [value.refpk]},
                    success: (data) => {
                        (((data.data || {}).contrastRuleAsslist || {}).items || []).map((one)=>{
                            if(one.attrcode == 'doccode'){
                                that.doccodeRefCode[value.refpk] = one.refcode;
                            }
                        })
                        let meta = that.props.meta.getMeta();
                        meta[presetVar.list3].items.map((item) => {
                            if(item.attrcode=='doccode'){
                                // if(one.itemtype=="refer"){
                                    let appcode=that.props.getSearchParam("c");
                                    if(appcode=="200017GLOBA"){
                                        item.queryCondition = () => {
                                            return {
                                                "pk_org":"GLOBLE00000000000000"
                                            };
                                        }
                                    }else{
                                        let businessInfo = getBusinessInfo();
                                        item.queryCondition = () => {
                                            return {
                                                "pk_org":businessInfo.groupId
                                            };
                                        }
                                    }
                                // }
                            }

                        })
                        that.props.meta.setMeta(meta);
                    }
                })
            }
        }
        if(key == 'doccode'){
            // 编码带出名称
            props.editTable.setValByKeyAndIndex(moduleId, index, 'docname', {
                value:value.refname,
                display:value.refname

            });
            props.editTable.setValByKeyAndIndex(moduleId, index, 'doccode', {
                value:value.refcode,
                display:value.refcode
            });

        }


    }
}
