import {base,getBusinessInfo,toast,promptBox,cardCache} from 'nc-lightapp-front';
const {NCMessage} = base;
import cardPageController from '../../../../public/common/cardPageController'
import simpleTablePageControllerChild from '../../listGlobal/events/simpleTablePageControllerChild'
import pubUtil from '../../../../public/common/pubUtil'
import afterEventForm from './afterEventForm'
import afterEventList from './afterEventList'
import utils from './utils'
import presetVar from '../presetVar'
import listPresetVar from '../../listGlobal/presetVar'
import requestApi from '../requestApi'
import listRequestApi from '../../listGlobal/requestApi'
let { getCacheById, updateCache,addCache,getCurrentLastId,getNextId,deleteCacheById} = cardCache;
class cardPageControllerChild extends cardPageController{
    constructor(main, props, meta) {
        let newMeta = {
            ...meta,
            dataSource: (meta || {}).dataSource || listPresetVar.dataSource,
            paginationShowClassName: (meta || {}).paginationShowClassName || 'paginationShowClassName',
            bodyId: (meta || {}).bodyId || presetVar.bodyArea,
            formId: (meta || {}).formId || presetVar.formId,
            listIds: (meta || {}).listIds || [presetVar.list, presetVar.list2],
            // listIds: (meta || {}).listIds || [presetVar.list, presetVar.list2, presetVar.list3],  有辅助核算模块
            listButtonAreas: (meta || {}).listButtonAreas || {
                [presetVar.list]:presetVar.listButtonArea,
                [presetVar.list2]:presetVar.listButtonArea,
                // [presetVar.list3]:presetVar.listButtonArea
            },
            listUrl: (meta || {}).listUrl || presetVar.listURL,
            listPageCode: (meta || {}).listPageCode || presetVar.listPageCode,
            browseButtons: (meta || {}).listPageCode || ['Enable', 'Disable'],
            pkField: 'pk_contrastrule',
            cardURL:(meta || {}).cardURL || presetVar.cardURL,
        };
        super(main, props, newMeta);
        this.doccodeRefCode = {};
        this.buttonStatus=true;
    }

    /**
     * 初始化模板
     * @param {*} meta
     */
    initMeta(meta){
        meta[presetVar.formId].items.map((one)=>{
            if(one.attrcode == 'startstatus'){
                one.itemtype="switch_browse"
            }
        })
        let listNewItems1=[];
        meta[presetVar.list].items.map((one)=>{
            listNewItems1.push(one);
            if(one.attrcode == 'code'){
                listNewItems1.push({
                    attrcode: one.attrcode + 'Input',
                    label: one.label,
                    itemtype: 'input'
                })
            }
            if(one.attrcode == 'name'){
                one.disabled=true;
            }
        })
        meta[presetVar.list].items = listNewItems1;
        let listNewItems2=[];
        meta[presetVar.list2].items.map((one)=>{
            listNewItems2.push(one);
            if(one.attrcode == 'code'){
                listNewItems2.push({
                    attrcode: one.attrcode + 'Input',
                    label: one.label,
                    itemtype: 'input'
                })
            }
            if(one.attrcode == 'name'){
                one.disabled=true;
            }
        })
        meta[presetVar.list2].items = listNewItems2;
        this.props.form.setFormItemsDisabled(presetVar.formId, {'startdate':true,'enddate':true});
        requestApi.queryCurr({
            data: {date:pubUtil.getSysDateStr()},
            success: (data) => {
                if(data){
                    if(data.data.NC001 != false){
                       this.props.form.setFormItemsVisible(presetVar.formId,{'groupamount':true});
                    }else{
                        this.props.form.setFormItemsVisible(presetVar.formId,{'groupamount':false});
                    }
                    if(data.data.NC002 != false){
                        this.props.form.setFormItemsVisible(presetVar.formId,{'globalamount':true});
                    }else{
                        this.props.form.setFormItemsVisible(presetVar.formId,{'globalamount':false});
                    }
                }
            }
        })
    }
    /**
     * 初始化参照过滤条件
     * @param {*} meta
     */
    initRefParam(meta){
        let businessInfo = getBusinessInfo();
        let groupId=businessInfo.groupId;
        if(this.listUrl.indexOf('listGlobal')  != -1){
            groupId='GLOBLE00000000000000';
        }
        meta[presetVar.list].items.map((one)=>{
            if(one.attrcode == 'code'){
                one.isAccountRefer = false;
                one.queryCondition = () => {
                    let pk_accountingbook = this.props.form.getFormItemsValue(presetVar.formId, 'pk_book').value;
                    return {
                        "pk_setofbook": pk_accountingbook,
                        datestr: pubUtil.getSysDateStr(),
                        pk_org:groupId,
                        "isDataPowerEnable": 'Y',
                        // "DataPowerOperationCode" : 'fi'
                    };
                }
            }
        })
        meta[presetVar.list2].items.map((one)=>{
            if(one.attrcode == 'code'){
                one.isAccountRefer = false;
                one.queryCondition = () => {
                    return {
                        "pk_setofbook": this.props.form.getFormItemsValue(presetVar.formId, 'pk_book').value,
                        datestr: pubUtil.getSysDateStr(),
                        pk_org:groupId,
                        "isDataPowerEnable": 'Y',
                        // "DataPowerOperationCode" : 'fi'
                    };
                }
            }
        })
    }

    initTemplateSetPageState(data){
        this.addOpr(presetVar.list);
        this.addOpr(presetVar.list2);
        super.initTemplateSetPageState(data);
    }

    Edit(){
        let props = this.props;
        let id= props.form.getFormItemsValue(presetVar.formId, 'pk_contrastrule').value;
        requestApi.editCheck({
            data: id,
            success: (data) => {
                this.setPageState('edit');
                props.form.setFormItemsDisabled(presetVar.formId, {startstatus:true});
            }
        })
        
    }

    getListButtons(listAreaId, text, record, index){
        if(this.buttonStatus){
            return ['DelLine'];
        }
    }
    /**
     * 取消按钮事件
     */
    Cancel(){
        let status=this.props.getUrlParam('status');
      if(status=='edit'){
            let currId=this.id; 
          let cardData = getCacheById(currId, this.dataSource);
          if(currId){
            this.props.setUrlParam(currId);
            if(cardData){
                this.loadPageData(this, cardData);
            }else{
                this.queryData(currId);
            }
          }else{
            this.props.form.EmptyAllFormValue(presetVar.formId);
            this.props.editTable.setTableData(presetVar.list, { rows: [] });
          }
      }else{
          let id = getCurrentLastId(this.dataSource);
          let cardData = getCacheById(id, this.dataSource);
          if(id){
            this.props.setUrlParam(id);
            if(cardData){
                this.loadPageData(this, cardData);
            }else{
                this.queryData(id);
            }
          }else{
            this.props.form.EmptyAllFormValue(presetVar.formId);
            this.props.editTable.setTableData(presetVar.list, { rows: [] });
          }
      }
      this.setPageState('browse');
    }
    /**
     * 查询数据
     * @param {*} id
     */
    queryData(id){
        let cardData = getCacheById(id, this.dataSource);
        if(cardData){
            this.loadPageData(this, cardData);
        }else{
            requestApi.query({
                data: {pk_contrastrule: id, ...pubUtil.getSysCode(this.props)},
                success: (data) => {
                    if(data.data != null){
                        updateCache(presetVar.pkname,id, data, this.formId, this.dataSource);
                        this.loadPageData(this, data);
                    }
                }
            })
        }
    }

    /**
     * 加载页面数据
     * @param {*} data 
     */
    loadPageData(page, data){
        let startstatus=data.data[presetVar.headArea][presetVar.formId].rows[0].values.startstatus;
        startstatus = startstatus&&startstatus.value;
        if(startstatus==1){
            data.data[presetVar.headArea][presetVar.formId].rows[0].values.startstatus.value=true;
            page.props.button.setButtonDisabled("Delete",true);
        }else{
            data.data[presetVar.headArea][presetVar.formId].rows[0].values.startstatus.value=false;
            page.props.button.setButtonDisabled("Delete",false);
        }
        if( data.data[presetVar.headArea][presetVar.formId].rows[0].values.ismainorgcontrast.value==true){
            page.props.form.setFormItemsDisabled(presetVar.formId,{pk_book:true});
        }
        page.setPageData(data);
        let createorg=data.data[presetVar.headArea][presetVar.formId].rows[0].values.createorg;
        if(createorg){
            if(createorg.value=='GLOBLE00000000000000'){
                page.props.form.setFormItemsValue(presetVar.formId, {createorg: {value: null, display:null}});
            }
        }
        let neverStarted = startstatus == '0';
        if(!neverStarted){
            // 曾经启用过的规则只能修改对账内容和是否包含未记账，其他都不能修改。
            let meta = page.props.meta.getMeta();
            page.buttonStatus=false;
            meta[presetVar.formId].items.map((one)=>{
                if(one.attrcode != 'crtelimvoucher'){
                        one.disabled=true;
                }
                if(one.attrcode == 'startstatus'){
                    one.disabled=false;
                }
            })
            meta[presetVar.childFormId1].items.map((one)=>{
                if(one.attrcode != 'untallied'){
                    one.disabled=true;
                }
            })
            meta[presetVar.childFormId2].items.map((one)=>{
                one.disabled=true;
            })
            meta[presetVar.list].items.map((one)=>{
                one.disabled=true;
            })
            meta[presetVar.list2].items.map((one)=>{
                one.disabled=true;
            })
            // meta[presetVar.list3].items.map((one)=>{
            //     one.disabled=true;
            // })
            page.props.meta.setMeta(meta);
        }else{
            let meta = page.props.meta.getMeta();
            meta[presetVar.list].items.map((one) =>{
                if(one.attrcode == 'code' || one.attrcode == 'codeInput'){
                    one.disabled = false;
                }
            })
            meta[presetVar.list2].items.map((one) => {
                if(one.attrcode == 'code' || one.attrcode == 'codeInput'){
                    one.disabled = false;
                }
            })
            page.props.meta.setMeta(meta);
        }
        let itemDisabled = !neverStarted;
        page.props.button.setButtonDisabled(['AddLine','DelLine'], itemDisabled); 
        page.props.form.setFormItemsDisabled(presetVar.formId,{'code':itemDisabled,'name':itemDisabled,'ismainorgcontrast':itemDisabled,
            'pk_book':itemDisabled,'contrastmoney':itemDisabled
        });
    }
    /**
     * 重写设置页面数据、适配特殊数据结构
     * @param {*} data
     */
  setPageData(data){
    // 适配后端返回数据结构
    let contrastrulelist1 = {areacode: presetVar.list, rows:[]};
    let contrastrulelist2 = {areacode: presetVar.list2, rows:[]};
    // let contrastRuleAsslist = {areacode: presetVar.list3, rows:[]};
    if((data.data[this.bodyId] || {}).body != null ){
        data.data[this.bodyId].body.rows.map((one)=>{
            one.values.codeInput = one.values.code;
            if(one.values.isself.value == true){
                contrastrulelist1.rows.push(one);
            }else{
                contrastrulelist2.rows.push(one);
            }
        })
    }
    // if((data.data[this.bodyId] || {}).contrastRuleAsslist != null ){
    //     data.data[this.bodyId].contrastRuleAsslist.rows.map((one)=>{
    //         contrastRuleAsslist.rows.push(one);
    //     })
    // }
    let newData = {data:{
        [this.headId]: data.data.head,
        [this.bodyId]: {
            [presetVar.list]: contrastrulelist1,
            [presetVar.list2]: contrastrulelist2,
            //  [presetVar.list3]: contrastRuleAsslist
        }
    }};
    super.setPageData(newData);
    // 模板变更
    if(newData.data!=null){
        if(newData.data.head.head.rows[0].values.startstatus.value == '1'){
            this.props.button.setButtonVisible(['Enable'], false);
        }else{
            this.props.button.setButtonVisible(['Disable'], false);
        }
        // 模板变更
        let meta = this.props.meta.getMeta();
        let ismainorgcontrast = newData.data.head.head.rows[0].values.ismainorgcontrast;
        meta[presetVar.formId].items.map((one)=>{
            if(one.attrcode == 'pk_book'){
                if(ismainorgcontrast.value == false){
                    one.disabled = false;
                }else{
                    one.disabled = true;
                }
            }
        })
        meta[presetVar.list].items.map((one)=>{
            utils.changeCodeNameTemplate(one ,ismainorgcontrast);
        })
        meta[presetVar.list2].items.map((one)=>{
            utils.changeCodeNameTemplate(one ,ismainorgcontrast);
        })
        this.props.meta.setMeta(meta);
    }
}

    /**
     * 增行按钮事件
     */
    AddLine(){
        if(this.main.state.selectTab == 0){
            this.props.editTable.addRow(presetVar.list);
        } else if(this.main.state.selectTab == 1){
            this.props.editTable.addRow(presetVar.list2);
        } else if(this.main.state.selectTab == 2){
            this.props.editTable.addRow(presetVar.list3);
        }
    }
    /**
     * 保存动作
     * @param {*data} 页面数据
     */
    doSave(data){

        data.contrastrulelist1.rows.map((one)=>{
            if(one.values.isself){
              one.values.isself.value=true
            }

        })
        if(data[presetVar.formId].rows.length>0){
            let value=data[presetVar.formId].rows[0].values.startstatus.value;
            let startdate=data[presetVar.formId].rows[0].values.startdate;
            if(value==true){
                data[presetVar.formId].rows[0].values.startstatus.value='1';
            }else{
                if(startdate){
                    if(startdate.value){
                        data[presetVar.formId].rows[0].values.startstatus.value='-1';
                    }else{
                        data[presetVar.formId].rows[0].values.startstatus.value='0';
                    }
                }else{
                    data[presetVar.formId].rows[0].values.startstatus.value='0';
                }
            }
        }
        requestApi.save({
            props: this.props,
            data: {
                [presetVar.formId] : {[presetVar.headArea]:data[presetVar.formId]},
                [this.bodyId] : {
                    [presetVar.list]:{...data[presetVar.list], areacode: presetVar.list},
                    [presetVar.list2]:{...data[presetVar.list2], areacode: presetVar.list2},
                    // [presetVar.list3]:{...data[presetVar.list3], areacode: presetVar.list3},
                },
                pageid: this.props.getSearchParam('p')
            },
            success: (data) => {
                this.callBackSave(data);
            }
        })
    }

   /**
     * 保存回掉方法
     * @param {*data} 返回数据
     */
    callBackSave(data){
        if(this.pkField && data.data){
            let id=data.data[this.headId][this.formId].rows[0].values[this.pkField].value
            this.setId(id);
            if (this.props.getUrlParam('status') === 'edit') {
                updateCache(presetVar.pkname,id,data,this.formId,this.dataSource);
              }else{
                addCache(id,data,this.formId,this.dataSource);
              }
            this.props.setUrlParam({
                id:id,
                status: 'browse'
            });
        }
        this.setPageState('browse');
        this.loadPageData(this, data);
        pubUtil.showMeggage(this.props, pubUtil.MESSAGETYPE.SAVE,this);
    }
    /**
     * 删除事件
     */
    doDelete(){
        requestApi.delete({
            props: this.props,
            data: {pk_contrastrule: this.id},
            success: (data) => {
                this.callBackDelete(data);
                
            }
        })
    }

    callBackDelete(){
        let id = this.props.getUrlParam("id");
        deleteCacheById("pk_contrastrule",id,listPresetVar.dataSource);
        let nextId = getNextId(id, listPresetVar.dataSource);
        if(nextId){
            this.props.setUrlParam({id:nextId});
            this.setId(nextId);
            this.queryData(nextId);
        }else{
            this.props.setUrlParam({id:null});
            this.setId(null);
            this.props.form.EmptyAllFormValue(this.formId);
            this.listIds.map((one)=>{
                this.props.editTable.setTableData(one, {rows:[]});
            })
            this.updateBtnState(this, 'browse', null);
        }

        toast({color:"success",content:this.main.state.json['200017-000001']})/* 国际化处理： 删除成功*/
        
    }
    /**
     * 启用
     */
    Enable(){
        let startstatus = (this.props.form.getFormItemsValue(presetVar.formId, 'startstatus') || {}).value;
        if(startstatus==true){
            startstatus="-1";
        }
        this.cfStartStop(this.id,startstatus);
        this.props.button.setButtonDisabled(['AddLine','DelLine'],true); 
        this.props.form.setFormItemsDisabled(presetVar.formId,{'code':true,'name':true,'ismainorgcontrast':true,
        'pk_book':true,'contrastmoney':true
        });
        this.buttonStatus=false;
        let meta = this.props.meta.getMeta();
        meta[presetVar.list].items.map((one)=>{
            if(one.attrcode=='code'){
                one.disabled=true;
            }
        })
        meta[presetVar.list2].items.map((one)=>{
            if(one.attrcode=='code'){
                one.disabled=true;
            }
        })
        this.props.meta.setMeta(meta);
    }
    /**
     * 停用                                                           z
     */
    Disable(){
        let startstatus = (this.props.form.getFormItemsValue(presetVar.formId, 'startstatus') || {}).value;
        if(startstatus==false || startstatus==null){
            startstatus="1";
        }
        this.cfStartStop(this.id,startstatus);
        this.props.button.setButtonDisabled(['AddLine','DelLine'],false); 
        this.props.form.setFormItemsDisabled(presetVar.formId,{'code':false,'name':false,'ismainorgcontrast':false,
        'pk_book':false,'contrastmoney':false
        });
        this.buttonStatus=true;
        let meta = this.props.meta.getMeta();
        meta[presetVar.list].items.map((one)=>{
            if(one.attrcode=='code'){
                one.disabled=false;
            }
        })
        meta[presetVar.list2].items.map((one)=>{
            if(one.attrcode=='code'){
                one.disabled=false;
            }
        })
        this.props.meta.setMeta(meta);
    }
    // 自定义方法-------------------------------------------------------------
    /**
     * 启用停用处理函数
     * @param {*} flag
     */
    cfStartStop(pk_contrastrule,startstatus,index){
        listRequestApi.startstop({
            data: {
                pk_contrastrule: pk_contrastrule,
                startstatus: startstatus
            },
            success: (data) => {
                this.cfCallBackStartStop(startstatus, index,data);
                if(startstatus==true){
                    this.props.button.setButtonDisabled("Delete",false);
                }else{
                    this.props.button.setButtonDisabled("Delete",true);
                }
            },
            error:(res) => {
                this.props.form.setFormItemsValue(presetVar.formId, {startstatus: {value: false, display:null}});
                toast({color:"danger",content:""+res.message})
            }
        })
    }
    /**
     * 启用停用处理回掉
     * @param {*} index
     */
    cfCallBackStartStop(startstatus, index,data){
        if(startstatus == '1'){
            this.props.button.setButtonVisible(['Enable'], true);
            this.props.button.setButtonVisible(['Disable'], false);
            // "2002CONTRASTRULE-0007": "停用", "2002CONTRASTRULE-0009": "停用成功",
            this.props.form.setFormItemsValue(presetVar.formId, {startstatus: {value: false, display:this.main.state.json['200017-000016']}});
            this.props.form.setFormItemsValue(presetVar.formId, {'enddate':{value: data.data.enddate, display:data.data.enddate}});
            toast({color:"success",content: this.main.state.json['200017-000018']});
         }else{
            this.props.button.setButtonVisible(['Enable'], false);
            // "2002CONTRASTRULE-0006": "启用", "2002CONTRASTRULE-0008": "启用成功",
            this.props.button.setButtonVisible(['Disable'], true);
            this.props.form.setFormItemsValue(presetVar.formId, {startstatus: {value: true, display:this.main.state.json['200017-000015']}});
            this.props.form.setFormItemsValue(presetVar.formId, {'startdate':{value: data.data.startdate, display:data.data.startdate}});
            this.props.form.setFormItemsValue(presetVar.formId, {'enddate':{value: null, display:null}});
            toast({color:"success",content: this.main.state.json['200017-000017']});
        }

    }
    cfNavChangeFun(tab){
        this.main.setState({selectTab:tab});
    }
    cfAfterEventForm(moduleId, key, value, oldValue){
        afterEventForm(this,this.props, moduleId, key, value, oldValue);
    }
    /**
     * 列表编辑后事件
     * @param {*} props
     * @param {*} moduleId
     * @param {*} key
     * @param {*} value
     * @param {*} changedrows
     * @param {*} record
     * @param {*} index
     */
    cfAfterEventList(moduleId, key, value, changedrows, record, index){
        afterEventList(this, this.props, moduleId, key, value, changedrows, record, index);
    }

    /**
     * 列表编辑前事件
     * @param {*} moduleId
     * @param {*} item
     * @param {*} index
     * @param {*} value
     * @param {*} record
     */
    cfBeforeEvent(moduleId, item, index, value, record){
        if(item.attrcode == 'doccode'){
            let pk_checktype = this.props.editTable.getValByKeyAndIndex(presetVar.list3, index, 'pk_checktype').value;
            if(this.doccodeRefCode[pk_checktype]){
                item.itemtype = 'refer';
                item.refcode = this.doccodeRefCode[pk_checktype];
            }else{
                item.itemtype = 'input';
                item.refcode = undefined;
            }
            let meta = this.props.meta.getMeta();
            meta.contrastRuleAsslist.items.map((one)=>{
                if(one.attrcode == 'doccode'){
                    one.refcode=item.refcode;
                    one.itemtype=item.itemtype;
                }
            })
            this.props.renderItem('table', 'contrastRuleAsslist', 'doccode', null);
            this.props.meta.setMeta(meta);
        }
        return true;
    }
    /**
     * 页面状态变更处理
     * @param {*} state
     */
    setPageState(state){
        switch(state){
            case 'browse':
                this.main.setState({[this.paginationShowClassName]: 'show'});
                this.props.button.setButtonVisible(this.editButtons, false);
                this.props.button.setButtonVisible(this.browseButtons, true);
                this.props.form.setFormStatus(this.formId, 'browse');
                this.listIds.map((one)=>{
                    this.props.editTable.setStatus(one, 'browse');
                    // this.removeOpr(one);
                })
                this.hideOpr(this.listIds);
                //根据需求处理
                this.props.form.setFormItemsDisabled(this.formId,{'startstatus':false});
                this.props.setUrlParam({status:'browse'});
            break;
            case 'add':
                this.main.setState({[this.paginationShowClassName]: 'hide'});
                this.props.button.setButtonVisible(this.browseButtons, false);
                this.props.button.setButtonVisible(this.editButtons, true);
                this.props.form.setFormStatus(this.formId, 'add');
                
                this.listIds.map((one)=>{
                    // this.removeOpr(one);
                    this.props.editTable.setStatus(one, 'edit');
                    // this.addOpr(one);
                })
                this.showOpr(this.listIds);
                //新增的功能
                this.props.form.setFormItemsValue(this.formId,{'startstatus':{display:this.main.state.json['200017-000002'],value:'0'},'crtelimvoucher':{display:this.main.state.json['200017-000003'],value:'Y'}});/* 国际化处理： 未启用,生成抵销分录*/
                this.props.form.setFormItemsDisabled(this.formId,{'startstatus':true});
                this.props.form.setFormItemsValue(this.formId,{'startstatus':{display:'',value:false}});
                this.props.form.setFormItemsDisabled(this.formId,{'pk_book':false});
                this.props.setUrlParam({status:'add'});
                // 更新本方、对方科目显示项目
                let meta = this.props.meta.getMeta();
                meta[presetVar.list].items.map((one)=>{
                    utils.changeCodeNameTemplate(one ,{value:false});
                    if(one.attrcode=='code' || one.attrcode == 'codeInput'){
                        one.disabled=false;
                    }
                })
                meta[presetVar.list2].items.map((one)=>{
                    utils.changeCodeNameTemplate(one ,{value:false});
                    if(one.attrcode=='code' || one.attrcode == 'codeInput'){
                        one.disabled=false;
                    }
                })
                this.props.meta.setMeta(meta);       
                setTimeout(() => {
                    this.props.editTable.addRow(presetVar.list);
                    this.props.editTable.addRow(presetVar.list2);
                }, 10);

                this.props.button.setButtonDisabled(['AddLine','DelLine'],false); 
                this.props.form.setFormItemsDisabled(presetVar.formId,{'code':false,'name':false,'ismainorgcontrast':false,
                'pk_book':false,'contrastmoney':false, startstatus:true
                });
                this.buttonStatus=true;
            break;
            case 'edit':
                this.main.setState({[this.paginationShowClassName]: 'hide'});
                this.props.button.setButtonVisible(this.browseButtons, false);
                this.props.button.setButtonVisible(this.editButtons, true);
                this.props.form.setFormStatus(this.formId, 'edit');
                this.listIds.map((one)=>{
                    this.props.editTable.setStatus(one, 'edit');
                    // this.addOpr(one);
                })
                this.showOpr(this.listIds);
                this.props.form.setFormItemsDisabled(this.formId,{'startstatus':true});
                this.setPageEditState(this);
                this.props.setUrlParam({status:'edit'});
            break;
        }
    }
    /**
     *新增按钮事件
     */
    Add(){
        // super.Add();
        this.props.form.setFormItemsDisabled(this.formId,{'startstatus':true})
        this.props.form.setFormItemsValue(this.formId,{'startstatus':{display:'',value:false}});
        this.props.form.EmptyAllFormValue(this.formId);
        setTimeout(() => {
            this.listIds.map((one)=>{
                this.props.editTable.setTableData(one, {rows:[]});
            })
        }, 100);
        this.setPageState('add');
    }
     /**
     * 保存新增动作
     * @param {*data} 页面数据
     */
    doSaveAdd(data){

        data.contrastrulelist1.rows.map((one)=>{
            if(one.values.isself){
              one.values.isself.value=true
            }

        })
        if(data[presetVar.formId].rows.length>0){
            let value=data[presetVar.formId].rows[0].values.startstatus.value;
            let startdate=data[presetVar.formId].rows[0].values.startdate;
            if(value==true){
                data[presetVar.formId].rows[0].values.startstatus.value='1';
            }else{
                if(startdate){
                    if(startdate.value){
                        data[presetVar.formId].rows[0].values.startstatus.value='-1';
                    }else{
                        data[presetVar.formId].rows[0].values.startstatus.value='0';
                    }
                }else{
                    data[presetVar.formId].rows[0].values.startstatus.value='0';
                }
            }
        }
        requestApi.save({
            props: this.props,
            data: {
                [presetVar.formId] : {[presetVar.headArea]:data[presetVar.formId]},
                [this.bodyId] : {
                    [presetVar.list]:{...data[presetVar.list], areacode: presetVar.list},
                    [presetVar.list2]:{...data[presetVar.list2], areacode: presetVar.list2},
                    // [presetVar.list3]:{...data[presetVar.list3], areacode: presetVar.list3},
                },
                pageid: this.props.getSearchParam('p')
            },
            success: (data) => {
                this.callBackSave(data);
                this.Add();
            }
        })
    }

    setPageEditState(page){
        let mainOrgContrast = page.props.form.getFormItemsValue(presetVar.formId, 'ismainorgcontrast');
        let pk_book_disable = mainOrgContrast && mainOrgContrast.value;
        page.props.form.setFormItemsDisabled(presetVar.formId, {pk_book:pk_book_disable});
    }

    updateBtnState(page, pageState, pageData){
        if(pageState == 'browse'){
            if(!pageData){
                page.props.button.setButtonDisabled(['Edit', 'Delete', ''], true);
            }
        }
    }

    setOprVisibility(listAreaIds, visible){
        let meta = this.props.meta.getMeta();
        listAreaIds.map((listAreaId) => {
            meta[listAreaId].items.map((item) =>{
                if(item.attrcode == 'opr') item.visible = visible;
            });
        })
        this.props.meta.setMeta(meta);
    }

    showOpr(listAreaIds){
        this.setOprVisibility(listAreaIds, true);
    }

    hideOpr(listAreaIds){
        this.setOprVisibility(listAreaIds, false)
    }

        /**
     * 添加处理列
     * @param {*} listAreaId 
     */
    addOpr(listAreaId){
        let meta = this.props.meta.getMeta();
        let event = {
            label: this.main.state.json['2002-0002'],
            attrcode: 'opr',
            itemtype: 'customer',
            visible: false,
            width: '180px',
            fixed: 'right',
            render: (text, record, index) =>{
                return this.props.button.createOprationButton(this.getListButtons(), {
                    area: this.listButtonAreas[listAreaId],
                    buttonLimit: 3,
                    onButtonClick: (props, btnKey) => {
                        this.listButtonClick(listAreaId, btnKey, record, index);
                    }
                });
            }
        };
        meta[listAreaId].items.push(event);
        this.props.meta.setMeta(meta);
    }
}export default cardPageControllerChild;
// this.props.form.setFormItemsDisabled(presetVar.formId,{"startstatus":false});
