import simpleTablePageController from '../../listGlobal/events/simpleTablePageControllerChild'
import presetVar from '../../listGlobal/presetVar'
class simpleTablePageControllerChild extends simpleTablePageController{
    constructor(main, props, meta, initFlag) {
        super(main, props, {
            cardUrl: '/cardGroup',
            cardPageCode: '200018GROUP_CARD'
        }, initFlag);
    }
        /**
     * 取得表格行按钮
     * @param {*} text 
     * @param {*} record 
     * @param {*} index 
     */
    getListButtons(text, record, index){
        //集团节点只能修改集团数据
        if(record.createorg.value==null || record.createorg.value == presetVar.createOrg){
            return [];
        }else{
            if(record.startstatus.value == '1'){
                return ['Edit', 'Disable'];
            }else{
                return ['Edit', 'DelLine', 'Enable'];
            }
        }
        
    }
}
export default simpleTablePageControllerChild;
