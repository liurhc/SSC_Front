import {Component} from 'react';
import {createPage,getMultiLang,createPageIcon} from 'nc-lightapp-front';

import BusinessUnitTreeRef from '../../../../uapbd/refer/org/BusinessUnitTreeRef';
import simpleTablePageControllerChild from './events/simpleTablePageControllerChild'
import presetVar from '../listGlobal/presetVar'
import HeaderArea from '../../../public/components/HeaderArea';
import './index.less';



class ContrastruleList extends Component {
    constructor(props) {
        super(props);
        this.state = {json:{}};
        this.simpleTablePageControllerChild = new simpleTablePageControllerChild(this, props);
    }
    componentDidMount() {
        
    }
    componentWillMount(){
		let callback= (json) =>{
			this.setState({json:json},()=>{
			})
		}
        getMultiLang({moduleId:'200017',domainName:'gl',currentLocale:'zh-CN',callback});
	}
    componentDidUpdate(){
    }
    render() {
        const {table, search, button} = this.props;
        const {createSimpleTable} = table;
        const {createButtonApp, createOprationButton} = button;

        return (
            <div id="dfkmdzgxsz" className="nc-bill-list head_title">
                <HeaderArea
                    title = {this.state.json['200017-000011']}
                    btnContent = {createButtonApp({
                        area: presetVar.headButtonArea,
                        buttonLimit: 3,
                        onButtonClick: (props, actionId) => { this.simpleTablePageControllerChild.headButtonClick(actionId) }
                    })}
                />
                <div className="nc-bill-table-area">
                    {createSimpleTable(presetVar.list, {
                        dataSource: presetVar.dataSource,
                        pkname: presetVar.pkname,
                        onRowDoubleClick: (record, index)=>{this.simpleTablePageControllerChild.listRowDoubleClick(record, index)},
                        showCheck: false,
                        showIndex: true
                    })}
                </div>
            </div>

        )
    }
}
ContrastruleList = createPage({})(ContrastruleList);
export default ContrastruleList;
