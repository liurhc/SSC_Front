import React, {Component} from 'react';
import {ajax,createPage, base,toast,promptBox,getMultiLang,cardCache} from 'nc-lightapp-front';
const { NCModal, NCButton, NCMessage} = base;
import pubUtil from '../../../../public/common/pubUtil'
import requestApi from '../../card/requestApi'
let {getDefData} = cardCache;
import './index.less'
class Jsqcye extends Component {
    constructor(props) {
        super();
        this.state = {
            json:{}
        }
        this.appcode=props.appcode;
        this.flag=props.flag;
        this.refpk='';
        this.status='';
        
        window.setTimeout(()=>{
            props.form.setFormStatus('form', 'edit');
        },10)
    }
    componentWillMount(){
		let callback= (json) =>{
			this.setState({json:json},()=>{
                this.setMeta(this);
			})
		}
        getMultiLang({moduleId:'200017',domainName:'gl',currentLocale:'zh-CN',callback});
	}
    setMeta(_this){
        _this.props.meta.setMeta({
            form:{
                code:'form',
                moduletype: 'form',
                items: [
                    {
                        label: _this.state.json['20022002-000010'],/* 国际化处理： 核算账簿*/
                        attrcode: 'pk_accountingbook',
                        itemtype: 'refer',
                        refcode: 'uapbd/refer/org/AccountBookTreeRef/index',
                        visible: true,
                        required: true,
                        queryCondition: () => {
                            return {
                                "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                                "appcode": _this.appcode,
                                "isDataPowerEnable": 'Y',
                                "DataPowerOperationCode" : 'fi'
                            };
                        }
                    },
                    {
                        label: _this.state.json['20022002-000011'],/* 国际化处理： 内部交易对账规则*/
                        attrcode: 'pk_contrastrule',
                        itemtype: 'refer',
                        refcode: 'gl/refer/voucher/ContrastRuleGridRef/index.js',
                        visible: true,
                        required: true,
                        queryCondition: () => {
                            return {
                                "pk_accountingbook": _this.props.form.getFormItemsValue("form", 'pk_accountingbook').value || '',
                                "accountingbook":"accountingbook",
                                "isDataPowerEnable": 'Y',
                                "DataPowerOperationCode" : 'fi'
                            };
                        }
                    },
                    {
                        label: _this.state.json['20022002-000012'],/* 国际化处理： 科目*/
                        attrcode: 'pk_accasoa',
                        itemtype: 'refer',
                        refcode: 'uapbd/refer/fiacc/AccountDefaultGridTreeRef/index',
                        visible: true,
                        isAccountRefer: true,
                        queryCondition: () => {
                                return {
                                    "pk_accountingbook": _this.props.form.getFormItemsValue('form', 'pk_accountingbook').value,
                                    "pk_contrastrule": _this.props.form.getFormItemsValue('form', 'pk_contrastrule').value,
                                    "TreeRefActionExt":"nccloud.web.gl.ref.AccasoaRefSqlBuilder",
                                    "isDataPowerEnable":true,
                                    "versiondate": pubUtil.getVersionDateStr(),
                                    dateStr: pubUtil.getSysDateStr(),
                                    "isDataPowerEnable": 'Y',
                                    "DataPowerOperationCode" : 'fi'
                                }
                        }
                    },
                    {
                        label: _this.state.json['20022002-000013'],/* 国际化处理： 年度*/
                        attrcode: 'syear',
                        itemtype: 'select',
                        visible: true
                    }
                ]
            }
        });
    }
    componentWillReceiveProps (nextProp) {
        if(nextProp.headRef && (JSON.stringify(nextProp.headRef))!="{}"){
            if(this.flag=="list"){
                // if(nextProp.headRef.refpk!=this.refpk){
                    let data={value:nextProp.headRef.refpk,display:nextProp.headRef.refname};
                    this.props.form.setFormItemsValue("form", {"pk_accountingbook":data});
                    this.refpk=nextProp.headRef.refpk;
                // } 
            }else if(this.flag=="card"){
                if(nextProp.headRef.value!=this.refpk){
                    let value='';
                    let display='';
                    let headRef=getDefData("headRef",'gl.gl_balancecontrast.contrastinit');
                    if(nextProp.headRef.value){
                        value= nextProp.headRef.value;
                        display= nextProp.headRef.display;
                    }else{
                        value= headRef.refpk;
                        display= headRef.refname;
                    }
                    let bookValue={
                        "display": display,
                        "value":value
                    };
                    this.props.form.setFormItemsValue("form", {"pk_accountingbook":bookValue});
                    this.refpk=value;
                } 
            }
        }

            if (nextProp.status && (JSON.stringify(nextProp.status)) != "{}") {
                if(nextProp.status!=this.status){
                    this.props.form.setFormStatus("form", nextProp.status);
                }
            }
    }
    /**
     * 保存按钮点击
     */
    saveFormButtonClick() {
        let pk_accountingbook=this.props.form.getFormItemsValue('form', 'pk_accountingbook');
        let pk_contrastrule=this.props.form.getFormItemsValue('form', 'pk_contrastrule');
        let pk_accasoa=this.props.form.getFormItemsValue('form', 'pk_accasoa');
        if(!pk_accountingbook || !pk_accountingbook.value){
            toast({color:"warning",content:this.state.json['20022002-000014']});/* 国际化处理： 请选择财务核算账簿*/
        }else{
            if(!pk_contrastrule || !pk_contrastrule.value){
                toast({color:"warning",content:this.state.json['20022002-000015']});/* 国际化处理： 请选择内部交易对账规则*/
            }else{
                if(!pk_accasoa || !pk_accasoa.value){
                    toast({color:"warning",content:this.state.json['20022002-000016']});/* 国际化处理： 请选择会计科目*/
                }else{
                    promptBox({
                        color:"info",
                        title: this.state.json['20022002-000017'],/* 国际化处理： 提示：计算期初余额将覆盖您本年度的期初已对符数，请确认是否继续执行？*/
                        beSureBtnClick: this.beSureBtnClick.bind(this)
                      });
                }
            }
        }
    }

    beSureBtnClick(){
        ajax({
            url: '/nccloud/gl/contrast/contrastinitcompute.do',
            data: {
                pk_accountingbook: this.props.form.getFormItemsValue('form', 'pk_accountingbook').value,
                pk_contrastrule:　this.props.form.getFormItemsValue('form', 'pk_contrastrule').value,
                year:　this.props.form.getFormItemsValue('form', 'syear').value,
                pk_accasoa:　[this.props.form.getFormItemsValue('form', 'pk_accasoa').value]
            },
            success: (data)=>{
                this.props.doConfirm && this.props.doConfirm(data);
                toast({color:"success",content:this.state.json['20022002-000026']});
                this.cancelFormButtonClick();
                if(this.props.parent.simpleTablePageControllerChild){
                    this.props.parent.simpleTablePageControllerChild.refresh();
                }
            }
        });
    }
    /**
     * 取消按钮点击
     */
    cancelFormButtonClick() {
        this.status='';
        this.refpk='';
        this.props.parent.setState({[this.props.showFormModalState]: false});
    };
    render(){
        const {form} = this.props;
        const {createForm} = form;
        let doOpen = () => {
            if(this.props.showFormModal){
                return true;
            }else{
                return false;
            }
        }
        return(
            <div id="jsqcye">
                <NCModal show={doOpen()} size="lg">
                    <NCModal.Header>
                        {/* "2002QCCSH-0005": "计算期初余额" */}
                        <NCModal.Title>{this.state.json['20022002-000027']}</NCModal.Title>
                    </NCModal.Header>
                    <NCModal.Body>
                        {createForm('form', {
                            fieldid: 'Jsqcye',
                            onAfterEvent: (props,moduleId, key, value, oldValue)=>{
                                let pk_accountingbook= props.form.getFormItemsValue('form', 'pk_accountingbook').value;
                                let pk_contrastrule= props.form.getFormItemsValue('form', 'pk_contrastrule').value;
                                if(key=="pk_accountingbook" || key=="pk_contrastrule"){
                                    if(key=="pk_accountingbook"){
                                        props.form.setFormItemsValue('form', {'pk_contrastrule':{value:null,display:null}});
                                    }
                                    if(pk_accountingbook && pk_contrastrule){
                                        requestApi.contrastinitlink({
                                            data:{
                                                pk_accountingbook: pk_accountingbook,
                                                pk_contrastrule: pk_contrastrule
                                            },
                                            success: (data)=>{
                                                let meta = props.meta.getMeta();
                                                meta['form'].items.map((one)=>{
                                                    if(one.attrcode == 'syear'){
                                                        one.options = data.data.syear_options
                                                    }
                                                })
                                                let syear="";
                                                if(data.data.startdate.value){
                                                    let value=data.data.startdate.value.split("-");
                                                    syear=value[0];
                                                }
                                                props.form.setFormItemsValue('form', {"syear":
                                                    {display:syear,
                                                    value:syear}
                                                })
                                                props.meta.setMeta(meta);
                                            }
                                        })   
                                    }
                                }
                                if(key=="pk_accasoa"){
                                    if(!pk_accountingbook){
                                        toast({ content: this.state.json['20022002-000018'], color: 'warning' });/* 国际化处理： 请先选择核算账簿*/
                                    }else{
                                        if(!pk_contrastrule){
                                            toast({ content: this.state.json['20022002-000019'], color: 'warning' });/* 国际化处理： 请先选择内部交易对账规则*/
                                        }
                                    }
                                }
                            }
                        })}
                    </NCModal.Body>
                    <NCModal.Footer>
                        <NCButton
                            fieldid="confirm"
                            colors="primary"
                            onClick={this.saveFormButtonClick.bind(this)}
                        >
                            {/* "2002-0010": "确定" */}
                            {this.state.json['2002-0010']}
                        </NCButton>
                        <NCButton fieldid="cancel" onClick={this.cancelFormButtonClick.bind(this)}>
                            {/* "2002-0011": "取消" */}
                            {this.state.json['2002-0011']}
                        </NCButton>
                    </NCModal.Footer>
                </NCModal>
            </div>
        )
    }
}
Jsqcye = createPage({})(Jsqcye);
export default Jsqcye;
