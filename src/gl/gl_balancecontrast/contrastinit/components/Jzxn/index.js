import React, {Component} from 'react';
import {ajax, createPage, base, promptBox,cardCache,toast,getMultiLang} from 'nc-lightapp-front';
const { NCModal, NCButton, NCMessage} = base;
import pubUtil from '../../../../public/common/pubUtil'
import './index.less'
let {setDefData, getDefData } = cardCache;
class Jzxn extends Component {
    constructor(props) {
        super();
        this.state = {
            json:{},
            inlt:null
        }
        this.appcode=props.appcode;
        this.flag=props.flag;
        this.refpk='';
        this.status='';
        
        window.setTimeout(()=>{
            props.form.setFormStatus('form', 'edit');
        },10)
    }

    componentWillMount(){
		let callback= (json,status,inlt) =>{
			this.setState({json: json,inlt},()=>{
                this.setMeta(this);
			})
		}
        getMultiLang({moduleId:'200017',domainName:'gl',currentLocale:'zh-CN',callback});
	}
    setMeta(_this){
        _this.props.meta.setMeta({
            form:{
                code:'form',
                moduletype: 'form',
                items: [
                    {
                        label: _this.state.json['20022002-000010'],/* 国际化处理： 核算账簿*/
                        attrcode: 'pk_accountingbook',
                        itemtype: 'refer',
                        refcode: 'uapbd/refer/org/AccountBookTreeRef/index',
                        visible: true,
                        required: true,
                        queryCondition: () => {
                            return {
                                "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                                "appcode": _this.appcode,
                                "isDataPowerEnable": 'Y',
                                "DataPowerOperationCode" : 'fi'
                            };
                        }
                    },
                    {
                        label: _this.state.json['20022002-000011'],/* 国际化处理： 内部交易对账规则*/
                        attrcode: 'pk_contrastrule',
                        itemtype: 'refer',
                        refcode: 'gl/refer/voucher/ContrastRuleGridRef/index.js',
                        visible: true,
                        required: true,
                        queryCondition: () => {
                            return {
                                "pk_accountingbook": _this.props.form.getFormItemsValue("form", 'pk_accountingbook').value || '',
                                "accountingbook":"accountingbook",
                                "isDataPowerEnable": 'Y',
                                "DataPowerOperationCode" : 'fi'
                            };
                        }
                    },
                ]
            }
        });
    }
    componentWillReceiveProps (nextProp) {
        if (nextProp.status && (JSON.stringify(nextProp.status)) != "{}") {
            if(nextProp.status!=this.status){
                this.props.form.setFormStatus("form", nextProp.status);
            }
        }
        if(nextProp.headRef && (JSON.stringify(nextProp.headRef))!="{}"){
            if(this.flag=="list"){
                // if(nextProp.headRef.refpk!=this.refpk){
                    let data={value:nextProp.headRef.refpk,display:nextProp.headRef.refname};
                    this.props.form.setFormItemsValue("form", {"pk_accountingbook":data});
                    this.refpk=nextProp.headRef.refpk;
                // } 
            }else if(this.flag=="card"){
                // if(nextProp.headRef.value!=this.refpk){
                    this.props.form.setFormItemsValue("form", {"pk_accountingbook":nextProp.headRef});
                    this.refpk=nextProp.headRef.value;
                // } 
            }
        }
    }
        
	// }
    /**
     * 保存按钮点击
     */
    saveFormButtonClick() {
        let pk_accountingbook=this.props.form.getFormItemsValue('form', 'pk_accountingbook').value;
        let pk_contrastrule=this.props.form.getFormItemsValue('form', 'pk_contrastrule').value;
        if(!pk_contrastrule){
            toast({ content: this.state.json['20022002-000034'], color: 'danger' });
        }else{
            let url = '';
            let msg = '';
            let tips = '';
            
            //保存前增加提示
            ajax({
                url: '/nccloud/gl/contrastinit/contrastinitgetmaxyear.do',
                data: {
                    pk_accountingbook: pk_accountingbook,
                    pk_contrastrule:　pk_contrastrule
                },
                success: (data)=>{
                    let tipsYear = parseInt(data.data.maxYear);
                    if(this.props.JzxnFlag == true){
                        url = '/nccloud/gl/contrast/contrastinitnextyear.do';
                        msg =　this.state.json['20022002-000024'];
                        // tips = (this.state.json['20022002-000030']);
                        tips = this.state.inlt && this.state.inlt.get('20022002-000030',{before : tipsYear , after : tipsYear+1});
                    }else{
                        url = '/nccloud/gl/contrast/contrastinitcancelnextyear.do';
                        msg = this.state.json['20022002-000025'];
                        tips = this.state.inlt && this.state.inlt.get('20022002-000033',{now : tipsYear});
                        // tips = (this.state.json['20022002-000033']) + (tipsYear) + (this.state.json['20022002-000034']);
                    }
                    promptBox({color:"info",
                        content: tips,
                        beSureBtnClick: this.beSureBtnClick.bind(this,msg,url),
                        cancelBtnClick: function() {
                            return;
                        }
                    });
                }
            });
        }
        
        
    }

    beSureBtnClick(msg,url){
        ajax({
            url: url,
            data: {
                pk_accountingbook: this.props.form.getFormItemsValue('form', 'pk_accountingbook').value,
                pk_contrastrule:　this.props.form.getFormItemsValue('form', 'pk_contrastrule').value
            },
            success: (data)=>{
                this.props.doConfirm && this.props.doConfirm(data);
                toast({ content: msg, color: 'success' });
                this.cancelFormButtonClick();
                if(this.props.parent.simpleTablePageControllerChild){
                    this.props.parent.simpleTablePageControllerChild.refresh();
                }
            }
        });
    }
    afterEvent(props,formId,key,c,d){
        if(key=='pk_accountingbook'){
            props.form.setFormItemsValue(formId,{'pk_contrastrule':{value:null,display:null}});
        }
    }
    /**
     * 取消按钮点击
     */
    cancelFormButtonClick() {
        this.props.parent.setState({[this.props.showFormModalState]: false});
        this.status='';
        this.refpk='';
    };
    render(){
        const {form} = this.props;
        const {createForm} = form;
        let doOpen = () => {
            if(this.props.showFormModal){
                return true;
            }else{
                return false;
            }
        }
        return(
            <div id="jsqcye">
                <NCModal show={doOpen()} size="lg" className="next m-qccsh-mode">
                    <NCModal.Header>
                        {/* TODO：多语 */}
                        <NCModal.Title>
                        {
                            // "2002QCCSH-0006": "结转下年"
                            // "2002QCCSH-0007": "取消结转"
                            this.props.JzxnFlag == true ? (this.state.json['20022002-000028']) : (this.state.json['20022002-000029'])
                        }
                        </NCModal.Title>
                    </NCModal.Header>
                    <NCModal.Body>
                        {createForm('form', {
                            fieldid: 'Jzxn',
                             onAfterEvent: this.afterEvent.bind(this), 
                        })}
                    </NCModal.Body>
                    <NCModal.Footer>
                        <NCButton
                            fieldid="confirm"
                            colors="primary"
                            onClick={this.saveFormButtonClick.bind(this)}
                        >
                            {/* "2002-0010": "确定" */}
                            {this.state.json['2002-0010']}
                        </NCButton>
                        <NCButton fieldid="cancel" onClick={this.cancelFormButtonClick.bind(this)}>
                            {/* "2002-0011": "取消" */}
                            {this.state.json['2002-0011']}
                        </NCButton>
                    </NCModal.Footer>
                </NCModal>
            </div>
        )
    }
}
Jzxn = createPage({})(Jzxn);
export default Jzxn;
