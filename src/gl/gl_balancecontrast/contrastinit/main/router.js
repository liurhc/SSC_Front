import { asyncComponent } from 'nc-lightapp-front';
// import ContrastinitList from '../list/contrastinitList';

// const ContrastinitCard = asyncComponent(() => import('../card/contrastinitCard'));

const ContrastinitCard = asyncComponent(() => import(/* webpackChunkName: "gl/gl_balancecontrast/contrastinit/card/card" */ /* webpackMode: "eager" */ '../card/contrastinitCard'));
const ContrastinitList = asyncComponent(() => import(/* webpackChunkName: "gl/gl_balancecontrast/contrastinit/list/list" */ /* webpackMode: "eager" */ '../list/contrastinitList'));


const routes = [
	{
		path: '/',
		component: ContrastinitList,
		exact: true
	},
	{
		path: '/list',
		component: ContrastinitList
	},
	{
		path: '/card',
		component: ContrastinitCard
	}
];

export default routes;
