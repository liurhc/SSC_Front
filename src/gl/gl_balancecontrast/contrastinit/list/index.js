import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import ContrastinitList from './contrastinitList';

ReactDOM.render(<ContrastinitList />
    , document.querySelector('#app')); 
