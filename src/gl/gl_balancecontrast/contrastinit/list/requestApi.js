import {ajax } from 'nc-lightapp-front';
let requestApiOverwrite = {
    // 查询账簿属性
    queryBookCombineInfo: (opt) => {
        ajax({
            url: '/nccloud/gl/voucher/queryBookCombineInfo.do',
            data: {pk_accountingbook: opt.data},
            success: opt.success
        });
    },
    // 查询接口
    query: (opt) => {
        ajax({
            url: '/nccloud/gl/contrastinit/contrastinitlistquery.do',
            data: opt.data || {},
            success: opt.success
        });
    },
    // 删除
    delete: (opt) => {
        ajax({
            url: '/nccloud/gl/contrastinit/contrastinitdelete.do',
           //n url: '/nccloud/gl/nbjydz/qccsh/DeleteListAction.do',
            data: opt.data,
            success: opt.success
        });
    }
}
export default  requestApiOverwrite;
