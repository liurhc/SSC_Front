import { cardCache,toast} from "nc-lightapp-front";
let { setDefData, getDefData } = cardCache;
import simpleTablePageController from '../../../../public/common/simpleTablePageController'
import pubUtils from '../../../../public/common/pubUtil'
import presetVar from '../presetVar'
import requestApi from '../requestApi'
class simpleTablePageControllerChild extends simpleTablePageController {
    constructor(main, props, meta) {
        let newMeta = {
            ...meta,
            dataSource: (meta || {}).dataSource || presetVar.dataSource,
            listPkField: (meta || {}).listPkField || 'pk_contrastinit',
            listId: (meta || {}).listId || presetVar.list,
            listButtonArea: (meta || {}).listButtonAreas || presetVar.listButtonArea,
            cardUrl: (meta || {}).cardUrl || presetVar.cardURL,
            cardPageCode: (meta || {}).cardPageCode || presetVar.cardPageCode,
        };
        super(main, props, newMeta);
        // 自定义属性
        this.cfQueryKey = { logic: 'and', conditions: [] };
    }


    initTemplate() {
        this.props.createUIDom(
            {},
            (data) => {
                let meta = data.template;
                this.initMeta(meta);
                this.initRefParam(meta);
                window.setTimeout(() => {
                    this.addListOpr(meta, this.listId, this.listButtonArea);
                    meta[presetVar.list].items.map((one) => {
                        if(one.attrcode=='pk_contrastrule.name'){
                            one.width=160;
                        }
                        switch (one.attrcode) {
                            case 'groupamount':
                                    one.visible = false;
                                break;
                            case 'globalamount':
                                    one.visible = false;
                                break;
                        }
                    })
                    this.props.meta.setMeta(meta);
                    this.props.button.setButtons(data.button);
                    //添加按钮
                    this.props.button.setPopContent('DelLine', this.main.state.json['20022002-000021']);/* 国际化处理： 确定要删除吗？*/
                    // TODO：缓存
                    // if(!this.props.table.hasCacheData(this.dataSource)){
                    this.callBackInitTemplate(data);
                    // }
                }, 1);
            }
        )
    }
    /**
     * 根据缓存设置默认值
     */
    callBackInitTemplate(data) {
        let cfQueryKey = getDefData('cfQueryKey', presetVar.dataSource);
        if (cfQueryKey) {
            this.main.setState({ headRef: cfQueryKey.pk_accountingbook,ruleRef:cfQueryKey.pk_contrastrule });
            this.cfQueryKey = cfQueryKey;
            let dataVlue = {
                "pk_accountingbook": cfQueryKey.pk_accountingbook.refpk,//核算账簿
                "pk_contrastrule": cfQueryKey.pk_contrastrule.refpk//内部交易对账规则
            }
            this.queryData(dataVlue);
        }else{
            this.query(data);
        }

    }
    /**
     * 根据缓存设置默认值
     */
    query(data) {
        if (data.context.defaultAccbookPk) {
            let dataPk = {
                "refname": data.context.defaultAccbookName,
                "refpk": data.context.defaultAccbookPk,
                "pk_accountingbook": data.context.defaultAccbookPk
            }
            let dataVlue = {
                "pk_accountingbook": data.context.defaultAccbookPk,//核算账簿
                "pk_contrastrule": ''//内部交易对账规则
            }
            this.main.setState({ headRef: dataPk });
            setDefData("headRef", this.dataSource, dataPk);
            this.queryData(dataVlue);
        }
    }

    componentWillReceiveProps(){

    }
    /**
     * 查询数据
     * @param {*} key 
     */
    queryData(key) {
        let pk_accountingbook = key.pk_accountingbook;
        requestApi.query({
            data: key,
            success: (data) => {
                // this.props.table.setAllTableData(this.listId, { rows: [] });
                this.setTableData(data);
                setDefData(this.listId, presetVar.dataSource, data.data);
                if (pk_accountingbook) {
                    // 判断是否启用全局本币、集团本币
                    requestApi.queryBookCombineInfo({
                        data: pk_accountingbook,
                        success: (data) => {
                            let meta = this.props.meta.getMeta();
                            meta[presetVar.list].items.map((one) => {
                                switch (one.attrcode) {
                                    case 'groupamount':
                                        if (((data || {}).data || {}).NC001) {
                                            one.visible = true;
                                        } else {
                                            one.visible = false;
                                        }
                                        break;
                                    case 'globalamount':
                                        if (((data || {}).data || {}).NC002) {
                                            one.visible = true;
                                        } else {
                                            one.visible = false;
                                        }
                                        break;
                                }
                            })
                            this.props.meta.setMeta(meta);
                        }
                    })
                }
            }
        })
    }
    /**
     * 删除数据
     * @param {*} data 
     * @param {*} index 
     */
    doDelLine(alldata, index) {
        requestApi.delete({
            data: { pk_contrastinit: alldata['pk_contrastinit'].value },
            success: (data) => {
                this.callBackDelLine(data, index, alldata['pk_contrastinit'].value);
            }
        })
    }
    /**
     * 计算期初余额 按钮事件
     */
    Jsqcye() {
        this.main.setState({ JsqcyeModalState: true });
        this.main.status="edit";
    }
    /**
     * 结转下年
     */
    Jzxn() {
        this.main.setState({ JzxnFlag: true, JzxnModalState: true});
        this.main.status="edit";
    }
    /**
     * 取消结转
     */
    Qxjz() {
        this.main.setState({ JzxnFlag: false, JzxnModalState: true });
        this.main.status="edit";
    }
    /**
     * 保存缓存数据
     */
    setDataSource() {
        let data={
            'pk_accountingbook':this.main.state.headRef,
            'pk_contrastrule':this.main.state.ruleRef
        }
        setDefData('cfQueryKey', presetVar.dataSource, data);
        
    }
    // 自定义方法---------------------------------------------------------------------
    /**
     * 查询按钮事件
     * @param {*} data 
     */
    cfSearchBtnClick(data) {
        if (data == false) {
            return;
        }
        this.cfQueryKey = data;
        this.queryData(data);
    }
    /**
     * 结转取消结转确定后事件
     */
    cgJzxnModalDoConfirm() {
        // this.queryData(this.cfQueryKey);
    }

    cfHeadRefChange(data) {
        this.main.setState({ headRef: data });
        let dataVlue = {
            "pk_accountingbook": data.refpk,//核算账簿
            "pk_contrastrule": ''//内部交易对账规则
        }
        this.main.setState({ ruleRef:{}});
        setDefData("headRef", this.dataSource, data);
        this.queryData(dataVlue);
    }
    changeRule(data) {
        this.main.setState({ ruleRef: data });
        let dataVlue = {
            "pk_accountingbook": '',//核算账簿
            "pk_contrastrule": data.refpk//内部交易对账规则
        }
        if (this.main.state.headRef.refpk) {
            dataVlue.pk_accountingbook = this.main.state.headRef.refpk
        }
        this.queryData(dataVlue);
    }
    Refresh(){
        let dataVlue = {
            "pk_accountingbook": '',//核算账簿
            "pk_contrastrule": ''//内部交易对账规则
        }
        if (this.main.state.headRef.pk_accountingbook) {
            dataVlue.pk_accountingbook = this.main.state.headRef.refpk
        }
        if (this.main.state.ruleRef.refpk) {
            dataVlue.pk_contrastrule = this.main.state.ruleRef.refpk
        }
        this.queryData(dataVlue);
        if (this.main.state.headRef.refpk) {
            let pk_accountingbook=this.main.state.headRef.refpk;
            // 判断是否启用全局本币、集团本币
            requestApi.queryBookCombineInfo({
                data: pk_accountingbook,
                success: (data) => {
                    let meta = this.props.meta.getMeta();
                    meta[presetVar.list].items.map((one) => {
                        switch (one.attrcode) {
                            case 'groupamount':
                                if (((data || {}).data || {}).NC001) {
                                    one.visible = true;
                                } else {
                                    one.visible = false;
                                }
                                break;
                            case 'globalamount':
                                if (((data || {}).data || {}).NC002) {
                                    one.visible = true;
                                } else {
                                    one.visible = false;
                                }
                                break;
                        }
                    })
                }
            })
        }
        toast({color:"success",title:this.main.state.json['20022002-000022']})/* 国际化处理： 刷新成功*/
    }
    refresh(){
        let dataVlue = {
            "pk_accountingbook": '',//核算账簿
            "pk_contrastrule": ''//内部交易对账规则
        }
        // if(){

        // }
        if (this.main.state.headRef.pk_accountingbook) {
            dataVlue.pk_accountingbook = this.main.state.headRef.refpk
        }
        if (this.main.state.ruleRef.refpk) {
            dataVlue.pk_contrastrule = this.main.state.ruleRef.refpk
        }
        this.queryData(dataVlue);
        if (this.main.state.headRef.refpk) {
            let pk_accountingbook=this.main.state.headRef.refpk;
            // 判断是否启用全局本币、集团本币
            requestApi.queryBookCombineInfo({
                data: pk_accountingbook,
                success: (data) => {
                    let meta = this.props.meta.getMeta();
                    meta[presetVar.list].items.map((one) => {
                        switch (one.attrcode) {
                            case 'groupamount':
                                if (((data || {}).data || {}).NC001) {
                                    one.visible = true;
                                } else {
                                    one.visible = false;
                                }
                                break;
                            case 'globalamount':
                                if (((data || {}).data || {}).NC002) {
                                    one.visible = true;
                                } else {
                                    one.visible = false;
                                }
                                break;
                        }
                    })
                }
            })
        }
    }
}
export default simpleTablePageControllerChild;
