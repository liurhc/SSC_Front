import {Component} from 'react';
import {createPage,base,getMultiLang,createPageIcon} from 'nc-lightapp-front';
let {NCAffix} = base;
import JsqcyeModal from '../components/Jsqcye'
import JzxnModal from '../components/Jzxn'

import simpleTablePageControllerChild from './events/simpleTablePageControllerChild'
import ReferLoader from '../../../public/ReferLoader/index.js';
import presetVar from './presetVar'
import HeaderArea from '../../../public/components/HeaderArea';
import './index.less';
import '../../../public/reportcss/firstpage.less';
class ContrastinitList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            JsqcyeModalState: false,
            JzxnModalState: false,
            JzxnFlag: true,
            headRef:{},
            ruleRef:{},
            flag:false,
            json:{}
        };
        this.status='';
        // 页面初始化
        this.simpleTablePageControllerChild = new simpleTablePageControllerChild(this, props);
    }
    componentWillMount(){
		let callback= (json) =>{
			this.setState({json:json},()=>{
                
			})
		}
        getMultiLang({moduleId:'200017',domainName:'gl',currentLocale:'zh-CN',callback});
    }
    renderSearchContent = () => {
        return (
            <div className='search-con'>
            <div className="title-search-detail">
                <ReferLoader
                    fieldid={'accountbook'}
                    tag={'test'}
                    refcode={'uapbd/refer/org/AccountBookTreeRef/index.js'}
                    queryCondition={() => {
                        return {
                            "TreeRefActionExt": 'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                            "appcode": this.props.getSearchParam('c'),
                            "isDataPowerEnable": 'Y',
                            "DataPowerOperationCode": 'fi'
                        }
                    }}
                    // disabled={this.state.headRef_disabled}
                    value={this.state.headRef}
                    onChange={(data) => { this.simpleTablePageControllerChild.cfHeadRefChange(data) }}
                    isMultiSelectedEnabled={false}
                />
            </div>
            <div className="title-search-detail accout">
                <ReferLoader
                    fieldid={'contrastrule'}
                    tag={'test'}
                    refcode={'gl/refer/voucher/ContrastRuleGridRef/index.js'}
                    queryCondition={() => {
                        if (this.state.headRef.pk_accountingbook) {
                            return {
                                "pk_accountingbook": this.state.headRef.pk_accountingbook, "isDataPowerEnable": 'Y',
                                "DataPowerOperationCode": 'fi'
                            }
                        } else {
                            return { startstatus: "all" };
                        }
                    }}
                    // disabled={this.state.headRef_disabled}
                    value={this.state.ruleRef}
                    onChange={(data) => { this.simpleTablePageControllerChild.changeRule(data) }}
                    isMultiSelectedEnabled={false}
                />
            </div>
            </div> 
        )
    }
    render() {
        const {table, search, button} = this.props;
        const {createSimpleTable} = table;
        const {createButtonApp, createOprationButton} = button;
        const {NCCreateSearch} = search;
        return (
            <div id="dfkmdzgxsz" className="nc-bill-list m-nbjycsh">
                <HeaderArea
                    title={this.state.json['20022002-000020']}/* 国际化处理： 内部交易期初*/
                    searchContent={this.renderSearchContent()}
                    btnContent={createButtonApp({
                        area: presetVar.headButtonArea,
                        buttonLimit: 3,
                        onButtonClick: (props, actionId) => { this.simpleTablePageControllerChild.headButtonClick(actionId) }
                    })}
                />

               {/* <div className="nc-bill-search-area">
                    {NCCreateSearch(presetVar.searchArea, {
                        clickSearchBtn: (props, data)=>{this.simpleTablePageControllerChild.cfSearchBtnClick(data)},
                        defaultConditionsNum: 4,
                        showAdvBtn: true

                    })}
                </div> */}
                <div className="nc-bill-table-area">
                    {createSimpleTable(presetVar.list, {
                        dataSource: presetVar.dataSource,
                        pkname:presetVar.pkname,
                        onRowDoubleClick: (record, index)=>{this.simpleTablePageControllerChild.listRowDoubleClick(record, index)},
                        showCheck: false,
                        showIndex: true
                    })}
                </div>
                <JsqcyeModal
                    showFormModalState="JsqcyeModalState" 
                    parent={this}
                    showFormModal={this.state.JsqcyeModalState}
                    appcode={this.props.getSearchParam('c')}
                    headRef={this.state.headRef}
                    flag={"list"}
                    status={this.status}
                />
                <JzxnModal
                    JzxnFlag={this.state.JzxnFlag}
                    showFormModalState="JzxnModalState" 
                    parent={this}
                    showFormModal={this.state.JzxnModalState}
                    doConfirm={()=>this.simpleTablePageControllerChild.cgJzxnModalDoConfirm()}
                    appcode={this.props.getSearchParam('c')}
                    headRef={this.state.headRef}
                    flag={"list"}
                    status={this.status}
                />
            </div>
        )
    }
}
ContrastinitList = createPage({})(ContrastinitList);
export default ContrastinitList;
