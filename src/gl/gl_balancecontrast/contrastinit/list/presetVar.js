/**页面全局变量 */
let currentVar = {
    list: 'initializationlist',
    searchArea: 'initializationform',
    headButtonArea: 'list_head',
    listButtonArea: 'list_inner',
    // cardURL:'/gl/gl_balancecontrast/contrastinit/card/index.html',
    cardURL:'/card',
    cardPageCode: '20022002_CARD',
    dataSource: 'gl.gl_balancecontrast.contrastinit',
    pkname:'pk_contrastinit'
}
export default currentVar
