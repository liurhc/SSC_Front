import {Component} from 'react';
import {createPage, base,getMultiLang,createPageIcon} from 'nc-lightapp-front';
const {NCBackBtn,NCAffix}=base;
import cardPageControllerChild from './events/cardPageControllerChild'
import JsqcyeModal from '../components/Jsqcye'
import JzxnModal from '../components/Jzxn'
import presetVar from './presetVar'
import listPresetVar from '../list/presetVar'
import HeaderArea from '../../../public/components/HeaderArea';
import './index.less';
import listAfterEvent from './events/listAfterEvent';
import listBeforeEvent from './events/listBeforeEvent';
class ContrastinitCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            paginationShowClassName: '',
            JsqcyeModalState: false,
            JzxnModalState: false,
            JzxnFlag: true,
            headRef:{},
            json:{}
        };
        // 页面初始化
        this.cardPageControllerChild = new cardPageControllerChild(this, props);
        this.listAfterEvent = new listAfterEvent(this, props);
        this.status="";
    }
    componentDidMount(){
        // let icon=document.getElementsByClassName("back-btn")   
        // icon[0].innertext="";
        // icon[0].innerHTML=`<i class="uf uf-minus"></i>`
        // icon[0].fontSize="15px"
        let title=document.getElementsByClassName("tab_title")
    }
    componentWillMount() {
        let callback= (json) =>{
            this.setState({json:json},()=>{
                
            })
        }
        getMultiLang({moduleId:'200017',domainName:'gl',currentLocale:'zh-CN',callback});
           window.onbeforeunload = () => {
            let status = this.props.getUrlParam("status");
            if (status == 'edit' || status == 'add') {
                return this.state.json['20022002-000000']/* 国际化处理： 确定要离开吗？*/
            }
        }
    }
    getTableHeightPosi = () => {
        let tableHeight=document.getElementById('app').offsetHeight-260;
        return tableHeight;
    }
    render() {
        const {editTable, button, cardPagination, modal, form} = this.props;
        const {createModal} = modal;
        const {createForm} = form;
        const { createCardPagination } = cardPagination;
        const {createEditTable} = editTable;
        const {createButtonApp} = button;
        let status=this.props.getUrlParam('status');
        let getCreateCardPagination=()=>{
            if(this.props.getUrlParam('status') == 'browse' || this.props.getUrlParam('status') == 'edit'){
                return(
                    <div className={'header-cardPagination-area ' + this.state.paginationShowClassName} style={{float:'right'}}>
                        {createCardPagination({
                            dataSource: listPresetVar.dataSource,
                            handlePageInfoChange:(props,data)=>{this.cardPageControllerChild.pageInfoClick(data)}
                        })}
                    </div>
                )
            }
        }
        return (
            <div id="qccshcard_card" className="nc-bill-card">
                <div className="nc-bill-top-area">
                    <HeaderArea
                        initShowBackBtn={status == 'browse'}
                        backBtnClick={this.cardPageControllerChild.backButtonClick.bind(this.cardPageControllerChild)}
                        title={this.state.json['20022002-000023']}
                        btnContent={createButtonApp({
                            area: presetVar.headButtonArea,
                            buttonLimit: 3,
                            onButtonClick: (props, actionId) => { this.cardPageControllerChild.headButtonClick(actionId) }
                        })}
                        pageBtnContent={getCreateCardPagination()}
                    />
                    <div className="nc-bill-form-area">
                        {createForm(presetVar.formId, {
                            onAfterEvent: (props,moduleId, key, value, oldValue)=>{this.cardPageControllerChild.cfAfterEventForm(moduleId, key, value, oldValue)}
                        })}
                    </div>
                </div>
                {
                    <div className="nc-bill-table-area">
                        <div className="shoulder-definition-area">
                            <div className="definition-icons">
                                {createButtonApp({
                                    area: presetVar.listShoulderButtonArea,
                                    buttonLimit: 3,
                                    onButtonClick: (props, actionId)=>{this.cardPageControllerChild.headButtonClick(actionId)}
                                })}
                            </div>
                        </div>
                        {createEditTable(presetVar.list, {
                            showCheck: true,
                            showIndex: true,
                            onBeforeEvent:listBeforeEvent.bind(this),
                            onAfterEvent: listAfterEvent.bind(this),
                            selectedChange:(data)=>{this.cardPageControllerChild.selectedChange(data)},
                            height: this.getTableHeightPosi()
                        })}
                    </div>
                }

                {createModal('delete', {
                    title: this.state.json['2002-0003'],
                    content: this.state.json['2002-0004'],
                    beSureBtnClick: this.delConfirm
                })}
                <JsqcyeModal
                    showFormModalState="JsqcyeModalState"
                    parent={this}
                    showFormModal={this.state.JsqcyeModalState}
                    appcode={this.props.getSearchParam('c')}
                    headRef={this.state.headRef}
                    flag={"card"}
                    status={this.status}
                />
                <JzxnModal
                    JzxnFlag={this.state.JzxnFlag}
                    showFormModalState="JzxnModalState"
                    parent={this}
                    showFormModal={this.state.JzxnModalState}
                    appcode={this.props.getSearchParam('c')}
                    headRef={this.state.headRef}
                    flag={"card"}
                    status={this.status}
                />
            </div>

        )
    }
}
ContrastinitCard = createPage({})(ContrastinitCard);
export default ContrastinitCard;
