/**页面全局变量 */
let currentVar = {
    pageId: 'yxysdy',
    list: 'initializationcardlist',
    formId: 'initializationcardform',
    headArea: 'head',
    bodyArea: 'body',
    headButtonArea: 'card_head',
    listButtonArea: 'card_body_inner',
    listShoulderButtonArea: 'card_body',
    listPageCode: '20022002_LIST',
    // listURL:'/gl/gl_balancecontrast/contrastinit/list/index.html'
    listURL:'/list'
    
}
window.presetVar = {
    ...currentVar
};
export default currentVar
