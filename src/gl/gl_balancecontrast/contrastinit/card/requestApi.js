import {ajax } from 'nc-lightapp-front';
import pubUtils from '../../../public/common/pubUtil'
let requestApiOverwrite = {

// 查询账簿属性
    queryBookCombineInfo: (opt) => {
        ajax({
            url: '/nccloud/gl/voucher/queryBookCombineInfo.do',
            data: {pk_accountingbook: opt.data},
            success: opt.success
        });
    },
    // 查询接口
    query: (opt) => {
        ajax({
            url: '/nccloud/gl/contrastinit/contrastinitcardquery.do',
            data: opt.data,
            success: opt.success
        });
    },
    // 保存
    save: (opt) => {
        ajax({
            url: '/nccloud/gl/contrastinit/contrastinitsave.do',
            data: {...opt.data, ...{['userjson']: JSON.stringify(pubUtils.getSysCode(opt.props))}},
            success: opt.success
        });
    },
    // 删除
    delete: (opt) => {
        ajax({
            url: '/nccloud/gl/contrastinit/contrastinitdelete.do',
            data: opt.data,
            success: opt.success
        });
    },
    // 查询年度等
    contrastinitlink: (opt) => {
        ajax({
            url: '/nccloud/gl/contrastinit/contrastinitlink.do',
            data: opt.data,
            success: opt.success
        });
    },
    // 校验接口
    check: (opt) => {
        ajax({
            url: '/nccloud/gl/contrastinit/contrastinitcardcheck.do',
            data: opt.data,
            success: opt.success
        });
    },
    // 校验接口
    checkYear: (opt) => {
        ajax({
            url: '/nccloud/gl/contrastinit/contrastinitcardcheckYear.do',
            data: opt.data,
            success: opt.success
        });
    },
    queryscale: (opt) => {
        ajax({
            url: '/nccloud/gl/contrastinit/contrastinitgetscale.do',
            data: {currPk: opt.data},
            success: opt.success
        });
    },
}

export default  requestApiOverwrite;
