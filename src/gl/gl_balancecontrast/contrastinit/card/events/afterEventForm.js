import changeListTemplate from './changeListTemplate'
import presetVar from '../presetVar'
import requestApi from '../requestApi';
import cardPageControllerChild from './cardPageControllerChild'
import {promptBox} from "nc-lightapp-front";
export default function(props, moduleId, key, value, oldValue, controll){
    switch(key){
        // 是否已对副
        case 'pk_accasoa':
            if(value!=oldValue){
                props.editTable.setTableData(presetVar.list,{rows: []});
            }
        break;
        case 'iscontrasted':
            changeListTemplate(props, value.value);
            break;
        case 'pk_accountingbook':
            if(!oldValue.value){
                if(value.value){
                    props.form.setFormItemsDisabled(presetVar.formId,{
                        'pk_contrastrule':false,
                        'syear':false,
                        'pk_accasoa':false,
                        'iscontrasted':false});
                        props.button.setButtonDisabled(['AddLine'],false);
                    }else{
                        props.form.setFormItemsDisabled(presetVar.formId,{
                            'pk_contrastrule':true,
                            'syear':true,
                            'pk_accasoa':true,
                            'iscontrasted':true});
                        props.button.setButtonDisabled(['AddLine'],true);
                    }
                    beSureBtnClick.bind(this,props,value,oldValue)
            }else{
                promptBox({
                    color:"info",
                    title: controll.main.state.json['20022002-000001'],/* 国际化处理： 确认修改*/
                    content: controll.main.state.json['20022002-000002'],/* 国际化处理： 确定修改核算账簿，这样会清空您录入的信息？*/
                    beSureBtnClick: beSureBtnClick.bind(this,props,value,oldValue),
                    cancelBtnClick: cancelBtnClick.bind(this,props,oldValue)
                });
            }
           break
        case 'pk_contrastrule':
            let pk_accountingbook= props.form.getFormItemsValue(presetVar.formId, 'pk_accountingbook').value;
            let pk_contrastrule= props.form.getFormItemsValue(presetVar.formId, 'pk_contrastrule').value;
            if(value!=oldValue){
                props.form.setFormItemsValue(presetVar.formId, {'syear':{value:null,display:null}});
                props.form.setFormItemsValue(presetVar.formId, {'pk_accasoa':{value:null,display:null}});
                props.editTable.setTableData(presetVar.list,{rows: []});
            }
            if(pk_accountingbook && pk_contrastrule){
                requestApi.contrastinitlink({
                    data:{
                        pk_accountingbook: props.form.getFormItemsValue(presetVar.formId, 'pk_accountingbook').value,
                        pk_contrastrule: props.form.getFormItemsValue(presetVar.formId, 'pk_contrastrule').value
                    },
                    success: (data)=>{
                        // 设置启用日期和科目默认值
                        props.form.setFormItemsValue(presetVar.formId, {
                            startdate: data.data.startdate,
                            pk_accasoa: data.data.pk_accasoa
                        })
                        let meta = props.meta.getMeta();
                        meta[presetVar.formId].items.map((one)=>{
                            if(one.attrcode == 'syear'){
                                // 更新年度项目枚举项
                                one.options = data.data.syear_options
                            }
                        })
                        meta[presetVar.list].items.map((one)=>{
                            if(data.data.showList && data.data.showList.indexOf(one.attrcode) > -1){
                                // 设置显示项目
                                one.visible = true;
                                // one.required =
                            }else if(data.data.hideList && data.data.hideList.indexOf(one.attrcode) > -1){
                                // 设置隐藏项目
                                one.visible = false;
                            }
                            if(one.attrcode == 'pk_currtype'){
                                one.initialvalue = data.data.pk_currtype;
                            }
                            if(one.attrcode == 'amount'){
                                one.scale = data.data.pk_currtype.amount_scale;
                            }
                            if(one.attrcode == 'locamount'){
                                one.scale = data.data.pk_currtype.amount_scale;
                            }
                            if(one.attrcode == 'groupamount'){
                                if(data.data.groupCurrinfo){
                                    one.scale = data.data.groupCurrinfo.scale;
                                }
                            }
                            if(one.attrcode == 'globalamount'){
                                if(data.data.globalCurrinfo){
                                    one.scale = data.data.globalCurrinfo.scale;
                                }
                            }
                        })
                        props.meta.setMeta(meta);
                        // 保存pk_org
                        controll.pk_org = pk_accountingbook;
                        controll.org = data.data.org;
                        controll.props.button.setButtonDisabled(['AddLine'],false);
                    }
                })
            }else{
                controll.pk_org = '';
                controll.org = '';
                controll.props.button.setButtonDisabled(['AddLine'],true);
            }
            break;
        case 'amount':
            let pk_accountingbook1= props.form.getFormItemsValue(presetVar.formId, 'pk_accountingbook').value;

            if(pk_accountingbook1){
                // 判断是否启用全局本币、集团本币
                requestApi.queryBookCombineInfo({
                    data:pk_accountingbook1,
                    success: (data) => {
                        let monref=data.data.currinfo;
                        let  money=props.editTable.getAllData(presetVar.list).rows[0].values.pk_currtype;
                        if(monref.value==money.value){
                            let amount=props.editTable.getAllData(presetVar.list).rows[0].values.amount;
                            props.editTable.setValByKeyAndIndex(presetVar.list,0,'locamount',{value:amount.value, display:amount.display, scale:0,isEdit:false })
                        }

                    }
                })
            }
        case 'locamount':
            let pk_accountingbook2= props.form.getFormItemsValue(presetVar.formId, 'pk_accountingbook').value;

            if(pk_accountingbook2){
                // 判断是否启用全局本币、集团本币
                requestApi.queryBookCombineInfo({
                    data:pk_accountingbook2,
                    success: (data) => {
                        let monref=data.data.currinfo;
                        let  money=props.editTable.getAllData(presetVar.list).rows[0].values.pk_currtype;
                        if(monref.value==money.value){
                            let locamount=props.editTable.getAllData(presetVar.list).rows[0].values.locamount;
                            props.editTable.setValByKeyAndIndex(presetVar.list,0,'amount',{value:locamount.value, display:locamount.display, scale:0,isEdit:false })
                        }

                    }
                })
            }
            case 'syear':
            let year=value.value;
            // let pk_accountingbook2= props.form.getFormItemsValue(presetVar.formId, 'pk_accountingbook').value;
            let pk_accountbookCheck=props.form.getFormItemsValue(presetVar.formId, 'pk_accountingbook').value;
            let pk_contrastruleCheck=props.form.getFormItemsValue(presetVar.formId, 'pk_contrastrule').value;
            let data={
                "year":year,
                "pk_accountbook":pk_accountbookCheck,
                "pk_contrastrule":pk_contrastruleCheck
            }
            requestApi.checkYear({
                data: data,
                success: (data) => {
                },
                error: (data) => {
                }
            })
    }
}
function beSureBtnClick(props,value,oldValue){
   if(value.value){
    props.form.setFormItemsDisabled(presetVar.formId,{
        'pk_contrastrule':false,
        'syear':false,
        'pk_accasoa':false,
        'iscontrasted':false});
       if(value!=oldValue){
           props.form.EmptyAllFormValue(presetVar.formId);
           props.editTable.setTableData(presetVar.list,{rows: []});
           props.form.setFormItemsValue(presetVar.formId,{"pk_accountingbook":{display:value.refname,value:value.refpk}});
       }
       let pk_accountingbook3 = props.form.getFormItemsValue(presetVar.formId, 'pk_accountingbook').value;
       requestApi.queryBookCombineInfo({
           data:pk_accountingbook3,
           success: (data) => {
               let meta = props.meta.getMeta();
               meta[presetVar.list].items.map((one)=>{
                   // 组织
                   if(one.attrcode == 'locamount'){
                       one.scale = data.data.orgscale;
                   }
                   // 全局
                   if(one.attrcode == 'globalamount'){
                       one.scale = data.data.globalscale;
                   }
                   // 集团
                   if(one.attrcode == 'groupamount'){
                       one.scale = data.data.groupscale;
                   }
               })
               props.meta.setMeta(meta);
           }
       })
       props.button.setButtonDisabled(['AddLine','DelLine'],false);
       
   }else{
       props.form.EmptyAllFormValue(presetVar.formId);
       props.editTable.setTableData(presetVar.list,{rows: []});
       props.button.setButtonDisabled(['AddLine','DelLine'],true);
       props.form.setFormItemsDisabled(presetVar.formId,{
           'pk_contrastrule':true,
           'syear':true,
           'pk_accasoa':true,
           'iscontrasted':true});
   }
}

function cancelBtnClick(props,value){
    props.form.setFormItemsValue(presetVar.formId,{"pk_accountingbook":{display:value.display,value:value.value}});
}
