import cardPageController from '../../../../public/common/cardPageController'
import pubUtil from '../../../../public/common/pubUtil'
import afterEventForm from './afterEventForm'
import changeListTemplate from './changeListTemplate'
import presetVar from '../presetVar'
import listPresetVar from '../../list/presetVar'
import requestApi from '../requestApi'
import {cardCache,toast,promptBox} from "nc-lightapp-front";
let { getCacheById, updateCache,addCache,getCurrentLastId,getNextId,deleteCacheById,getDefData} = cardCache;

class cardPageControllerChild extends cardPageController{
    constructor(main, props, meta) {
        let newMeta = {
            ...meta, 
            dataSource: (meta || {}).dataSource || listPresetVar.dataSource,
            paginationShowClassName: (meta || {}).paginationShowClassName || 'paginationShowClassName',
            formId: (meta || {}).formId || presetVar.formId,
            listIds: (meta || {}).listIds || [presetVar.list],
            listButtonAreas: (meta || {}).listButtonAreas || {[presetVar.list]:presetVar.listButtonArea},
            listUrl: (meta || {}).listUrl || presetVar.listURL,  
            listPageCode: (meta || {}).listPageCode || presetVar.listPageCode,
            pkField: 'pk_contrastinit'
        };
        super(main, props, newMeta);
        this.pk_org = '';
        this.org='';//辅助核算需要传核算账簿的默认组织
    }
    /**
     * 初始化模板
     * @param {*} meta 
     */
    initMeta(meta){
        meta[presetVar.list].items.map((one, index)=>{
            // 辅助核算
            if(one.attrcode == 'pk_customer'){
                    one.width=210;
                 meta[presetVar.list].items[index] = this.getAssidModalRedner(one);
            }
        })
    }
    onRef(ref){
        this.assidModal=ref;
    }
    /**
     * 取得辅助核算弹窗数据
     * @param {*} key 
     * @param {*} record 
     * @param {*} index 
     */
    getPretentAssData(key, record, index){
        let ref = {};
        ref.pk_accountingbook=this.props.form.getFormItemsValue(presetVar.formId, 'pk_accountingbook').value;
        ref.pk_accasoa=this.props.form.getFormItemsValue(presetVar.formId, 'pk_accasoa').value;
        ref.checkboxShow=false;
        ref.pk_org=this.org;
        ref.assid=record.values.pk_customer.value;
        // ref.checkboxShow=true;
        return ref;
    }
    /**
     * 辅助核算点击后事件
     * @param {*} assid 
     * @param {*} data 
     * @param {*} one 
     * @param {*} attrcode 
     * @param {*} index 
     * @param {*} display 
     */
    assidModalRednerDoConfirm(assid, data, attrcode, index, display){
        // if((data || {}).data){
        //     data.data.map((one)=>{
        //         if(one){
        //             // assid.push(one)
        //             if(!one.checkvaluename){
        //                 one.checkvaluename="~";
        //             }
        //             value.push(one.pk);
        //             checkvaluename.push('【'+one.checktypename + ':' + one.checkvaluename+'】');
        //         }
        //     })
        // }
        //选中辅助核算以后会发请求，将assid和名称统一返回回来，和会计科目一样，有显示模式的，不能直接进行拼接；
        //现由于同步异步的问题，导致显示不出来，但是可以保存成功
        window.setTimeout(()=>{
            // 设置表格值
            this.props.editTable.setValByKeyAndIndex(presetVar.list, index, attrcode, {value: assid, display: display, isEdit: true});
            // 设置行编辑状态
            if(this.props.editTable.getRowStatus(presetVar.list, index) == '0'){
                this.props.editTable.setRowStatus(presetVar.list, index, '1');
            }
        },10);
    }
    /**
     * 初始化参照过滤条件
     * @param {*} meta 
     */
    initRefParam(meta){
        meta[presetVar.formId].items.map((one)=>{
            if(one.attrcode == 'pk_accasoa'){
                one.isAccountRefer = true;
                one.queryCondition = () => {
                    return {
                        "pk_accountingbook": this.props.form.getFormItemsValue(presetVar.formId, 'pk_accountingbook').value,
                        "pk_contrastrule": this.props.form.getFormItemsValue(presetVar.formId, 'pk_contrastrule').value,
                        "TreeRefActionExt":"nccloud.web.gl.ref.AccasoaRefSqlBuilder",
                        "isDataPowerEnable":true,
                        "versiondate": pubUtil.getVersionDateStr(),
                        dateStr: pubUtil.getSysDateStr(),
                        "isDataPowerEnable": 'Y',
                        "DataPowerOperationCode" : 'fi'
                    };
                }
            }else if(one.attrcode == 'pk_contrastrule'){
                one.queryCondition = () => {
                    return {
                        "pk_accountingbook": this.props.form.getFormItemsValue(presetVar.formId, 'pk_accountingbook').value,
                        "isDataPowerEnable": 'Y',
                        "DataPowerOperationCode" : 'fi'
                    };
                }
            }else if(one.attrcode == 'pk_accountingbook'){
                one.isMultiSelectedEnabled=false;
                one.showGroup=false;
                one.showInCludeChildren=false;
                one.queryCondition = () => {
				return {appcode:this.props.getSearchParam('c'),
                    TreeRefActionExt:'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                    "isDataPowerEnable": 'Y',
                    "DataPowerOperationCode" : 'fi'
				};
			}
            }
        })
        meta[presetVar.list].items.map((one)=>{
            if(one.attrcode == 'pk_vouchertype' || one.attrcode == 'explanation'){
                one.queryCondition = () => {
                    return {
                        "pk_org": this.pk_org,
                        "isDataPowerEnable": 'Y',
                        "DataPowerOperationCode" : 'fi'
                    };
                }
            }
        })
    }
    /**
     * 查询数据
     * @param {*} id 
     */
    queryData(id){
        requestApi.query({
            data: {pk_contrastinit: id, ...pubUtil.getSysCode(this.props)},
            success: (data) => {
                this.setPageData(data);
                if(data.data){
                    this.pk_org=data.data.head[presetVar.formId].rows[0].values.pk_accountingbook.value;
                    this.org=data.data.head[presetVar.formId].rows[0].values.pk_org.value;
                    if(data.data.head[presetVar.formId].rows[0].values.syearOption){
                        if(data.data.head[presetVar.formId].rows[0].values.syearOption.value){
                            let meta = this.props.meta.getMeta();
                            meta[presetVar.formId].items.map((one)=>{
                                if(one.attrcode == 'syear'){
                                    // 更新年度项目枚举项
                                    let syearOptions=data.data.head[presetVar.formId].rows[0].values.syearOption.value;
                                    let values=[];
                                    if(syearOptions.length>0){
                                        for(let i=0;i<syearOptions.length;i++){
                                            values.push(syearOptions[i]);
                                        }
                                        one.options=values;
                                    }
                                }
                            })
                            this.props.meta.setMeta(meta); 
                        }
                    }
                }
                changeListTemplate(this.props, (this.props.form.getFormItemsValue(presetVar.formId, 'iscontrasted') || {}).value);
            }
        })
    }
/**
     * 行删除事件
     * @param {*} AreaId 
     * @param {*} data 
     * @param {*} index 
     */
    DelTableLine(){
        let iscontrasted=(this.props.form.getFormItemsValue(presetVar.formId, 'iscontrasted') || {}).value;
        if(iscontrasted==true){
            toast({color:"danger",content:this.main.state.json['20022002-000003']})/* 国际化处理： 已对帐数据不允许删除！*/
        }else{
            let data = {};
            let AreaId='';
            this.listIds.map((one)=>{
                data[one] = this.props.editTable.getCheckedRows(one);
                AreaId=one;
            })
            let allIndex=[];
            if(data[AreaId].length>0){
                for(let i=0;i<data[AreaId].length;i++){
                    allIndex.push(data[AreaId][i].index);
                }
                this.props.editTable.deleteTableRowsByIndex(AreaId, allIndex);
             } else{
                pubUtil.showMeggage(this.props, pubUtil.MESSAGETYPE.WARNING,this);
            }
        }
    }

    /**
     * 行删除事件
     * @param {*} AreaId 
     * @param {*} data 
     * @param {*} index 
     */
    DelLine(AreaId, data, index){
        let iscontrasted=(this.props.form.getFormItemsValue(presetVar.formId, 'iscontrasted') || {}).value;
        if(iscontrasted==true){
            toast({color:"danger",content:this.main.state.json['20022002-000003']})/* 国际化处理： 已对帐数据不允许删除！*/
        }else{
            this.props.editTable.delRow(AreaId, index);
        }
    }

    /**
     * 保存动作
     * @param {*data} 页面数据
     */
    doSave(data){
        if(data){
            let dataList=data[presetVar.list].rows;
            let datas=[];
            let allFlag=false;
            let assFlag=false;
            if(dataList.length>0){
                for(let i=0;i<dataList.length;i++){
                    datas.push(dataList[i]);
                    if(!dataList[i].values.amount.value && !dataList[i].values.locamount.value 
                        && !dataList[i].values.quantity.value && !dataList[i].values.groupamount.value 
                        && !dataList[i].values.globalamount.value){
                        allFlag=true;
                    }
                    if(!dataList[i].values.pk_customer.value){
                        assFlag=true;
                    }
                }
                let flag=this.props.editTable.checkRequired(presetVar.list,datas);
                if(flag){
                    if(assFlag){
                        toast({color:"danger",content:this.main.state.json['20022002-000004']});/* 国际化处理： 辅助核算不能为空！*/
                        return;
                    }
                    if(allFlag){
                        toast({color:"danger",content:this.main.state.json['20022002-000005']});/* 国际化处理： 数量、原币、组织本币、集团本币、全局本币不能全部为空！*/
                        return;
                    }
                    requestApi.save({
                        props: this.props,
                        data: {
                            [presetVar.headArea] : {[presetVar.formId]:data[presetVar.formId]},
                            [presetVar.bodyArea] : {[presetVar.list]:data[presetVar.list]},
                            pageid: this.props.getSearchParam('p')
                        },
                        success: (data) => {
                            this.callBackSave(data);
                        }
                    })
                }
            }else{
                toast({color:"danger",content:this.main.state.json['20022002-000006']});/* 国际化处理： 表体数据不能为空！*/
            }
        }
    }
    /**
     * 删除事件
     */
    Delete(){
        promptBox({
            color:"info",
            title: this.main.state.json['20022002-000007'],/* 国际化处理： 删除*/
            content: this.main.state.json['20022002-000008'],/* 国际化处理： 确定要删除？*/
            beSureBtnClick: this.doDelete.bind(this)
        });
    }
    doDelete(){
        requestApi.delete({
            props: this.props,
            data: {pk_contrastinit: this.props.getUrlParam("id")},
            success: (data) => {
                this.callBackDelete(data);
            }
        })
    }
    callBackDelete(){
        let id = this.props.getUrlParam("id");
        let nextId = getNextId(id, listPresetVar.dataSource);
        if(nextId){
            this.props.button.setButtonDisabled(["Edit","Delete","Assist"],false);
        }else{
            this.props.button.setButtonDisabled(["Edit","Delete","Assist"],true);
        }
        this.props.setUrlParam({id:nextId});
        deleteCacheById("pk_contrastinit",id,listPresetVar.dataSource);
        this.setId(nextId);
        this.queryData(nextId);
        toast({color:"success",content:this.main.state.json['20022002-000009']});/* 国际化处理： 删除成功*/
    }
    /**
     * 计算期初余额 按钮事件
     */
    Jsqcye(){
        let aa=this.props.form.getFormItemsValue(presetVar.formId, 'pk_accountingbook');
        this.main.setState({JsqcyeModalState:true,headRef:aa});
        this.main.status="edit";
    }
    /**
     * 结转下年
     */
    Jzxn(){
        this.main.setState({JzxnFlag: true, JzxnModalState:true,headRef:{}});
        this.main.status="edit";
    }
    /**
     * 取消结转
     */
    Qxjz(){
        this.main.setState({JzxnFlag: false, JzxnModalState:true,headRef:{}});
        this.main.status="edit";
    }
    // 自定义方法-------------------------------------------------------------
    cfNavChangeFun(tab){
        this.main.setState({selectTab:tab});
    }
    /**
     * 表单数据变更后事件
     * @param {*} moduleId 
     * @param {*} key 
     * @param {*} value 
     * @param {*} oldValue 
     */
    cfAfterEventForm(moduleId, key, value, oldValue){
        afterEventForm(this.props, moduleId, key, value, oldValue, this);
    }

    setPageState(state){
        super.setPageState(state);
        switch(state){
            case 'browse':
            this.setButtonStatusVisible(['Assist'],true); 
            break;
            case 'add':
                // this.setButtonStatusVisible("Dropdown1",false);
                let headRef=getDefData("headRef",this.dataSource);
                this.setButtonStatus(['AddLine','DelLine'],true);
                this.setButtonStatusVisible(['Assist'],false);
                this.props.form.setFormItemsDisabled(presetVar.formId,{
                    'pk_contrastrule':true,
                    'syear':true,
                    'pk_accasoa':true,
                    'iscontrasted':true});
                if(headRef){
                    if(headRef.refpk){
                        let data={
                            display:headRef.refname,
                            value:headRef.refpk
                        }
                        this.props.form.setFormItemsValue(presetVar.formId, {"pk_accountingbook":data});
                        this.props.form.setFormItemsDisabled(presetVar.formId,{
                            'pk_contrastrule':false,
                            'syear':false,
                            'pk_accasoa':false,
                            'iscontrasted':false});
                    }
                }
            break;
            case 'edit':
            this.setButtonStatusVisible(['Assist'],false);
            this.props.form.setFormItemsDisabled(presetVar.formId,{
                "pk_accountingbook":true,
                'pk_contrastrule':true,
                'pk_accasoa':true,
                'iscontrasted':true});
            break;
        }
    }

    selectedChange(){
        let selectRows=this.props.editTable.getCheckedRows(presetVar.list);
        if(selectRows && selectRows.length>0){
            this.setButtonStatus(['DelLine'],false);
        }else{
            this.setButtonStatus(['DelLine'],true);
        }     
    }

    Cancel(){
        let status=this.props.getUrlParam('status');
        if(status=='edit'){
            let currId=this.props.getUrlParam('id');
            let cardData = getCacheById(currId, listPresetVar.dataSource);
            if(currId){
                this.props.setUrlParam({id:currId});
                if(cardData){
                    if(cardData != null && cardData.data != null){
                        this.props.form.setAllFormValue({[presetVar.formId]: cardData.data.head[presetVar.formId] || {rows:[]}});
                        if(nextCache.data.body != null){
                            this.props.editTable.setTableData(presetVar.list, cardData.data.body[presetVar.list] || {rows:[]});
                        }else{
                            this.props.editTable.setTableData(presetVar.list, {rows:[]});
                        }
                    }else{
                        this.props.form.EmptyAllFormValue(presetVar.formId); 
                    }
                }else{
                    this.queryData(currId);
                }
            }else{
                this.props.form.EmptyAllFormValue(presetVar.formId); 
                this.props.editTable.setTableData(presetVar.list, {rows:[]});
            }
        }else{
            let id = getCurrentLastId(listPresetVar.dataSource);
            let cardData = getCacheById(id, listPresetVar.dataSource);
            if(id){
                this.id=id;
                this.props.setUrlParam({id:id});
                if(cardData){
                    if(cardData != null && cardData.data != null){
                        this.props.form.setAllFormValue({[presetVar.formId]: cardData.data.head[presetVar.formId] || {rows:[]}});
                        if(nextCache.data.body != null){
                            this.props.editTable.setTableData(presetVar.list, cardData.data.body[presetVar.list] || {rows:[]});
                        }else{
                            this.props.editTable.setTableData(presetVar.list, {rows:[]});
                        }
                    }else{
                        this.props.form.EmptyAllFormValue(presetVar.formId); 
                    }
                }else{
                    this.queryData(id);
                }
            }else{
                this.props.form.EmptyAllFormValue(presetVar.formId); 
                this.props.editTable.setTableData(presetVar.list, {rows:[]});
            }
        }
        this.setPageState("browse");
        this.props.setUrlParam({status:'browse'})
    }
}export default cardPageControllerChild;
