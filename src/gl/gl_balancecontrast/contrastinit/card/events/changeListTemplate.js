import presetVar from '../presetVar'
import requestApi from '../requestApi';
export default function (props, value) {
    let meta = props.meta.getMeta();
    let data=props.form.getAllFormValue(presetVar.formId);
    let pk_contrastinit=data.rows[0].values.pk_contrastinit;
    if(pk_contrastinit){
        if(pk_contrastinit.value){
            if(value == true){
                props.form.setFormItemsDisabled(presetVar.formId, {
                    'pk_accountingbook': true,
                    'pk_contrastrule': true, 
                    'startdate': true,
                    'pk_accasoa': true,
                    'iscontrasted': true,
                    'istransfered': true
                });
            }
        }
    }
    let pk_accountingbook= props.form.getFormItemsValue(presetVar.formId, 'pk_accountingbook').value;
            let pk_contrastrule= props.form.getFormItemsValue(presetVar.formId, 'pk_contrastrule').value;
            if(pk_accountingbook && pk_contrastrule){
                requestApi.contrastinitlink({
                    data:{
                        pk_accountingbook: props.form.getFormItemsValue(presetVar.formId, 'pk_accountingbook').value,
                        pk_contrastrule: props.form.getFormItemsValue(presetVar.formId, 'pk_contrastrule').value
                    },
                    success: (data)=>{
                        meta[presetVar.list].items.map((one)=>{
                            if(data.data.showList && data.data.showList.indexOf(one.attrcode) > -1){
                                // 设置显示项目
                                one.visible = true;
                                // one.required =
                            }else if(data.data.hideList && data.data.hideList.indexOf(one.attrcode) > -1){
                                // 设置隐藏项目
                                one.visible = false;
                            }
                            if(one.attrcode == 'pk_currtype'){
                                one.initialvalue = data.data.pk_currtype;
                            }
                            if(one.attrcode == 'amount'){
                                one.scale = data.data.pk_currtype.amount_scale;
                            }
                        })
                        props.meta.setMeta(meta);
                    }
                })
            }
    if (value == true) {
        // 凭证类别、凭证号、分录序号、原币 不显示
        meta[presetVar.list].items.map((one) => {
            if (one.attrcode == 'pk_vouchertype' || one.attrcode == 'voucherno' || one.attrcode == 'detailno' || one.attrcode == 'amount') {
                one.visible = false;
                // one.required = false;
            }
        })
    } else {
        // 凭证类别、凭证号、分录序号、原币 显示
        meta[presetVar.list].items.map((one) => {
            if (one.attrcode == 'pk_vouchertype' || one.attrcode == 'voucherno' || one.attrcode == 'detailno' || one.attrcode == 'amount') {
                one.visible = true;
                // one.required = true;
            }
        })
    }
    props.meta.setMeta(meta);
}
