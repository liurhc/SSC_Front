import {base} from 'nc-lightapp-front';
const {NCMessage} = base;
import requestApi from '../requestApi'
import presetVar from '../presetVar'
export default function listAfterEvent(props, moduleId, key, value, changedrows, index, record) {

    if(key == 'amount'){
        if(value.length>1){
            let pk_accountingbook=this.props.form.getFormItemsValue(presetVar.formId, 'pk_accountingbook').value;
            if(pk_accountingbook){
                // 判断是否启用全局本币、集团本币
                requestApi.queryBookCombineInfo({
                    data:pk_accountingbook,
                    success: (data) => {
                        let scale = data.data.scale;
                        //组织本币
                        let rmb=data.data.currinfo;
                        //集团本币
                        let groupCurrinfo=data.data.groupCurrinfo;
                        //全局本币
                        let globalCurrinfo=data.data.globalCurrinfo;
                        let currrmb=record.values.pk_currtype;
                        if(rmb.value==currrmb.value){
                            let amount=this.props.editTable.getAllData(presetVar.list).rows[index].values.amount;
                            this.props.editTable.setValByKeyAndIndex(presetVar.list,index,'locamount',{value:amount.value, display:amount.display, scale:scale,isEdit:false })
                        }
                        if(groupCurrinfo.value==currrmb.value){
                            let amount=this.props.editTable.getAllData(presetVar.list).rows[index].values.amount;
                            this.props.editTable.setValByKeyAndIndex(presetVar.list,index,'groupamount',{value:amount.value, display:amount.display, scale:scale,isEdit:false })
                        }
                        if(globalCurrinfo.value==currrmb.value){
                            let amount=this.props.editTable.getAllData(presetVar.list).rows[index].values.amount;
                            this.props.editTable.setValByKeyAndIndex(presetVar.list,index,'globalamount',{value:amount.value, display:amount.display, scale:scale,isEdit:false })
                        }
                    }
                })
            }

        }
    }

    if(key=='pk_currtype'){
        requestApi.queryscale({
            data:value.refpk,
            success: (data) => {
                this.props.editTable.setValByKeyAndIndex(presetVar.list,index,'amount',{ scale:data.data[0] * 1})
            }
        })
    }
    if(key == 'voucherdate'){
        if(value){
            if(value.value){
                let enableDate=this.props.form.getFormItemsValue(presetVar.formId, 'startdate').value;
                let data={
                    "voucherdate":value,
                    "enableDate":enableDate
                }
                requestApi.check({
                    data: data,
                    success: (data) => {}
                })
            }
        }
    }
    if(key == 'locamount'){
        if(value.length>1){
            let pk_accountingbook1=this.props.form.getFormItemsValue(presetVar.formId, 'pk_accountingbook').value;
            if(pk_accountingbook1){
                // 判断是否启用全局本币、集团本币
                requestApi.queryBookCombineInfo({
                    data:pk_accountingbook1,
                    success: (data) => {
                        let currrmb=record.values.pk_currtype;
                        let scale = data.data.scale;
                         //组织本币
                         let rmb=data.data.currinfo;
                         //集团本币
                         let groupCurrinfo=data.data.groupCurrinfo;
                         //全局本币
                         let globalCurrinfo=data.data.globalCurrinfo;
                        if(rmb.value==currrmb.value){
                            let locamount=this.props.editTable.getAllData(presetVar.list).rows[index].values.locamount;
                            this.props.editTable.setValByKeyAndIndex(presetVar.list,index,'amount',{value:locamount.value, display:locamount.display, scale:scale,isEdit:false })
                        }
                        if(groupCurrinfo.value==currrmb.value){
                            let amount=this.props.editTable.getAllData(presetVar.list).rows[index].values.locamount;
                            this.props.editTable.setValByKeyAndIndex(presetVar.list,index,'groupamount',{value:amount.value, display:amount.display, scale:scale,isEdit:false })
                        }
                        if(globalCurrinfo.value==currrmb.value){
                            let amount=this.props.editTable.getAllData(presetVar.list).rows[index].values.locamount;
                            this.props.editTable.setValByKeyAndIndex(presetVar.list,index,'globalamount',{value:amount.value, display:amount.display, scale:scale,isEdit:false })
                        }
                    }
                })
            }

        }
    }

}
