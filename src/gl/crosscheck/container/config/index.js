const ORG_LIST = {
	pagecode: '20000CRVRLO_LIST',
	appcode: '20000CRVRLO'
};
const ORG_CARD = {
	pagecode: '20000CRVRLO_CARD',
	appcode: '20000CRVRLO'
};

const GROUP_LIST = {
	pagecode: '20000CRVRLG_LIST',
	appcode: '20000CRVRLG'
};
const GROUP_CARD = {
	pagecode: '20000CRVRLG_CARD',
	appcode: '20000CRVRLG'
};

export { ORG_LIST, ORG_CARD, GROUP_LIST, GROUP_CARD };
