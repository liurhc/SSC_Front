import React, { Component } from 'react';
import { ajax,toast,getMultiLang } from 'nc-lightapp-front';
import createScript from '../../../../public/components/uapRefer';
import ReferLoader from '../../../../public/ReferLoader/index';
import drawingRef from '../../../../doc_comparison/util/drawingRef'

const emptyCell = <span>&nbsp;</span>
export default class EditableCell extends Component {
	constructor(props) {
		super(props);
		this.state = {
			editable: false,
			refCode: '',
			json: {}
		};
	}
	componentWillMount() {
		let callback= (json) =>{
			this.setState({
				json:json,
			})
		}
		getMultiLang({moduleId:'20000CRVRL',domainName:'gl',currentLocale:'simpchn',callback});
	}
	bindAccDataHandle = (type, val, index) => {
		this.props.bindAccDataHandle(type, val, index);
	};
	//查询refCode
	queryRefer = (record, col, tabType) => {
		//let beanId = null;
		let beanId =
			record[`${col}beanid`].indexOf('#') == -1 ? record[`${col}beanid`] : record[`${col}beanid`].split('#')[1];
		ajax({
			url: '/nccloud/fipub/ref/queryRefCode.do',
			data: { beanId },
			success: (res) => {
				if (res.success) {
					if (res.data.refCode) {
						this.setState({ editable: true, refCode: res.data.refCode });
					}else{
						toast({content:this.state.json['20000CRVRL-000018'], color:'warning'});/* 国际化处理： 辅助核算和属性只支持档案类型！*/
					}
				}
			}
		});
	};

	// 手动录入支持 ALL 和 null
	handleVal = (val) => {
		if(val && val.refpk){
			if(val.refpk.toUpperCase() === 'ALL'){
				val.refpk = 'ALL'
				val.refname = 'ALL'
			} else if(val.refpk.toLowerCase() === 'null') {
				val.refpk = 'null'
				val.refname = 'null'
			} else {
				val.refpk = ''
				val.refname = ''
			}
		}else{
			val = {refpk:'', refname:''}
		}
		return val
	}
	//加载refer
	renderRefer = (index, record, col, orgInfo, type) => {
		let { showModal, businessInfo } = this.props;
		let { refCode, json } = this.state;
		if (refCode.indexOf('.js') == -1) {
			refCode += '.js';
		}
		let refKey = `${col}_${record.key}_${index}`;
		let pk_defdoclist = record[`${col}beanid`];
		let page = this;
		let condition ={
			pk_org:orgInfo.refpk,
			pk_defdoclist,
			refcode:refCode,
			isShowUnit:true,
			classid:pk_defdoclist
		}
		let fieldid = col.replace('col','item')
		let defaultValue;
		if(record) defaultValue = this.generateDocMapDefaultValue(record, col);
		let props = {
			fieldid: type,
			onChange: (val) => {
				if(val instanceof Array && val.length>0) {
					if(!val[0].refcode){
						val[0] = this.handleVal(val[0])
					}
				} else {
					if(!val.refcode){
						val = this.handleVal(val)
					}
				}
				this.props.matchupTableHandle('edit', val, index, col, showModal);
			},
			onBlur: () => {
				this.setState({ editable: false });
			},
			autoFocus:true,
			checkStrictly:false, //false时，会选中输入的值
			isMultiSelectedEnabled:showModal			
		}

		return drawingRef(page, json, refKey, condition, defaultValue, props);
	};

	generateDocMapDefaultValue(record, col){
		console.log(record);
		console.log(col);
		let defaultValue;
		if(record[`new${col}`]){/* 批生成弹框 多选 */
			defaultValue = [];
			let items = record[`new${col}`].split('#$#');
			items.map((item) => {
				let temp = item.split('#');
				if(temp.length >= 2){
					let tempValue = {refname:temp[0], refpk:temp[1]};
					defaultValue.push(tempValue);
				}
			});
		}else{
			defaultValue = {refname:record[col], refpk:record[`${col}pk_bd`]};
		}
		return defaultValue;
	}

	//操作档案类型表格
	docTypeTbaleHandle = (type, val, index, colname) => {
		this.props.docTypeTbaleHandle(type, val, index, colname);
	};
	buildRenderDom = () => {
		let {
			status,
			text,
			record,
			type,
			index,
			tabType,
			Refer,
			docTypeData,
			col,
			renderRefer,
			refcode,
			tag,
			orgInfo,
			queryCondition
		} = this.props;
		let { editable } = this.state;
		let refname = '',
			refpk = '';
		if (type == 'propname') {
			refname = 'propname';
			refpk = 'proppk';
		} else if (type == 'voname') {
			refname = 'voname';
			refpk = 'vopk';
		}
		if (status == 'browse') {
			return <div fieldid={type}>{text ? text : emptyCell}</div>;
		} else {
			if (editable) {
				switch (tabType) {
					case 1: /* 档案类型 */
						let referProps = {
							isMultiSelectedEnabled: false,
							autoFocus: true,
							fieldid: type,
							onChange: (val) => {
								this.docTypeTbaleHandle('edit', val, index, type);
							},
							onBlur: () => {
								this.setState({ editable: false });
							}
						};
						if (record.newLine) {
							if (type == 'propname') {
								return (
									<div fieldid={type}>
										<Refer
											{...referProps}
											value={{
												refname: record.propname,
												refpk: record.proppk
											}}
											queryCondition={{ 
												beanId: record.vopk,
												"isDataPowerEnable": 'Y',
												"DataPowerOperationCode" : 'fi'
											}}
										/>
									</div>
								);
							} else {
								let defaultValue = [{refname:record['voname'], refpk:record['vopk']}]
								return (
									// <Refer
									// 	{...referProps}
									// 	isMultiSelectedEnabled={true}
									// 	queryCondition = {{"isDataPowerEnable": 'Y',"DataPowerOperationCode" : 'fi'}}
									// 	value={{
									// 		refname: record[refname],
									// 		refpk: record[refpk]
									// 	}}
									// />
									<div fieldid={type}>
										<ReferLoader
											{...referProps}
											tag={tag}
											refcode={refcode}
											isMultiSelectedEnabled = {true}
											// value={{refname:record[refname], refpk:record[refpk]}}
											value={defaultValue}
										/>
									</div>
								);
							}
						} else {
							if (type == 'propname') {
								return (
									<div fieldid={type}>
										<Refer
											{...referProps}
											value={{
												refname: record.propname,
												refpk: record.proppk
											}}
											queryCondition={{ beanId: record.vopk, "isDataPowerEnable": 'Y',"DataPowerOperationCode" : 'fi' }}
										/>
									</div>
								);
							} else {
								return <div fieldid={type}>{text ? text : emptyCell}</div>;
							}
						}
						break;
					case 2: /* 档案对应关系 */
						return this.renderRefer(index, record, col, orgInfo, type);
						break;
					case 3:	/* 绑定科目 */
						if (type == 'itemcode') {
							let defaultValue = [];
							if(record) defaultValue.push({refname:record.code, refpk:record.pk_bindmeta});
							return (
								<div fieldid={type}>
									<ReferLoader
										tag = {tag}
										fieldid={type}
										refcode = {refcode}
										isMultiSelectedEnabled={true}
										queryCondition={queryCondition} //todo辅助核算过滤
										value={defaultValue}
										onChange={(val) => {
											this.bindAccDataHandle('edit', val, index);
										}}
										onFocus={()=>{
											if(queryCondition.hasOwnProperty('pk_accountingbook')&&!queryCondition.pk_accountingbook){
												toast({ content: this.state.json['20000CRVRL-000019'], color: 'warning' });/* 国际化处理： 请选择财务核算账簿！*/
											}
				
											if(queryCondition.hasOwnProperty('pk_setofbook') && !queryCondition.pk_setofbook) {
												toast({ content: this.state.json['20000CRVRL-000020'], color: 'warning' });/* 国际化处理： 请选择财务核算账簿类型！*/
											}
										}}
										onBlur={() => {
											this.setState({ editable: false });
										}}
									/>
								</div>
							);
						} else {
							return <div fieldid={type}>{text ? text : emptyCell}</div>;
						}
						break;
					default:
						break;
				}
			} else {
				return (
					<div 
						fieldid={type || col }
						style={text ? {} : { height: '100%' }}
						onClick={() => {
							if (tabType == 1 || tabType == 3) {
								this.setState({ editable: true });
							} else {
								this.queryRefer(record, col, tabType);
							}
						}}
					>
						{text ? text : emptyCell}
					</div>
				);
			}
		}
	};
	render() {
		let { status, text, record, type, index, tabType, orgInfo } = this.props;
		let { editable } = this.state;
		return <div style={text ? {} : { height: '100%' }}>{this.buildRenderDom()}</div>;
	}
}
