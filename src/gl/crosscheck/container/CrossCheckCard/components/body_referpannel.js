import React, { Component } from 'react';
import { ajax, base, high } from 'nc-lightapp-front';
import BusinessUnitTreeRef from '../../../../../uapbd/refer/org/BusinessUnitTreeRef';

const { NCButton: Button, NCTree: Tree, NCIcon: Icon, NCTable: Table, NCFormControl: FormControl, NCDiv } = base;

export default class ReferPannel extends Component {
	constructor(props) {
		super(props);
	}
	render() {
		let { pageType, status, ruleHead, orgInfo, json } = this.props;
		return (
			<NCDiv fieldid="check" areaCode={NCDiv.config.FORM}>
				<div className="nc-bill-search-area cc-card-referpannel">
					{pageType == 'org' ? (
						<div className="referpannel-item">
							<span className="title nc-theme-common-font-c">{json['20000CRVRL-000016']}：</span>{/* 国际化处理： 创建组织*/}
							<span className="refer-content">
								<BusinessUnitTreeRef
									fieldid='creatunit'
									disabled={true}
									value={{ refname: orgInfo.refname, refpk: orgInfo.refpk }}
								/>
							</span>
						</div>
					) : (
						''
					)}

					<div className="referpannel-item">
						<span className="title nc-theme-common-font-c"><i className='require-true'>*</i>{json['20000CRVRL-000017']}：</span>{/* 国际化处理： 规则名称*/}
						<span className="refer-content">
							<FormControl
								disabled={status == 'browse' ? true : false}
								fieldid='rulename'
								value={ruleHead.name}
								onChange={(val) => {
									this.props.handleRuleHead(val);
								}}
							/>
						</span>
					</div>
				</div>
			</NCDiv>
		);
	}
}
