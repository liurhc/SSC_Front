import React, { Component } from 'react';
import { base, createPage, createPageIcon } from 'nc-lightapp-front';
const { NCButton: Button, NCButtonGroup: ButtonGroup, NCBackBtn } = base;
import HeaderArea from '../../../../public/components/HeaderArea';
class Header extends Component {
	constructor(props) {
		super(props);
	}
	headerBtnClick = (props, type) => {
		this.props.headerBtnClick(props, type);
	};
	browseItem = (type) => {
		this.props.browseItem(type);
	};
	goBack = () => {
		this.props.switchPage()
	}
	render() {
		let { NCProps, status, pageBtnStatus } = this.props;
		return (
			<HeaderArea 
				title = {this.props.getSearchParam('n')}
				initShowBackBtn = {status == 'browse' ? true : false}
				backBtnClick = {this.goBack}
				btnContent = {NCProps.button.createButtonApp({
					area: 'page_header',
					onButtonClick: this.headerBtnClick
				})}
				pageBtnContent = {status == 'browse' ?  
					<ButtonGroup className='btn-group'>
						<Button shape="border" disabled={pageBtnStatus.prevBtn} onClick={this.browseItem.bind(this, 'first')}>
							<i className="iconfont icon-shangyiye" />
						</Button>
						<Button shape="border" disabled={pageBtnStatus.prevBtn} onClick={this.browseItem.bind(this, 'prev')}>
							<i className="iconfont icon-jiantouzuo" />
						</Button>
						<Button shape="border" disabled={pageBtnStatus.nextBtn} onClick={this.browseItem.bind(this, 'next')}>
							<i className="iconfont icon-jiantouyou" />
						</Button>
						<Button shape="border" disabled={pageBtnStatus.nextBtn} onClick={this.browseItem.bind(this, 'last')}>
							<i className="iconfont icon-xiayiye" />
						</Button>
					</ButtonGroup> 
					: 
					''
				}
			/>		
		);
	}
}

Header = createPage({})(Header)
export default Header
