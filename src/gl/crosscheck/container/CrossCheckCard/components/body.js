import React, { Component } from 'react';
import { base, getBusinessInfo, toast, createPage } from 'nc-lightapp-front';
import ReferPannel from './body_referpannel';
import EditTableCell from './editTableCell';
import MdTreeByIdRef from '../../../../../uap/refer/riart/mdTreeByIdRef/index';
import SetOfBookGridRef from '../../../../../uapbd/refer/org/SetOfBookGridRef/index';
import ReferLoader from '../../../../public/ReferLoader/index';
import {accbookRefcode, accountRefCode, accountGlobalRefCode} from '../../../../public/ReferLoader/constants';
import {getTableHeight } from '../../../../public/common/method.js';
import initTemplate from '../event/initTemplate';
const {
	NCModal: Modal,
	NCButton: Button,
	NCTable: Table,
	NCTabs: Tabs,
	NCCheckbox: Checkbox,
	NCDiv
} = base;
const { NCTabPane: TabPane } = Tabs;
const emptyCell = <span>&nbsp;</span>

class Body extends Component {
	constructor(props) {
		super(props);
		initTemplate.call(this, props);
		this.state = {
			//	showModal: false
		};
	}
	//操作档案类型表格
	docTypeTbaleHandle = (type, val, index, colname) => {
		this.props.docTypeTbaleHandle(type, val, index, colname);
	};

	//操作档案对应关系表格
	matchupTableHandle = (type) => {
		this.props.matchupTableHandle(type);
	};
	bindAccDataHandle = (type, val, index) => {
		this.props.bindAccDataHandle(type, val, index);
	};
	//全选操作
	onAllCheckChange = (tab, status) => {
		this.props.onAllCheckChange(tab, status);
	};
	//单选操作
	onCheckboxChange = (text, record, index, tab) => {
		this.props.onCheckboxChange(text, record, index, tab);
	};
	//核算账薄
	handleAccountingBook = (key, val) => {
		this.props.handleAccountingBook(key, val);
	};
	//适用所有科目
	handleAllItems = () => {
		this.props.handleAllItems();
	};
	//规则名称
	handleRuleHead = (val) => {
		this.props.handleRuleHead(val);
	};
	//批生成
	batchCreate = () => {
		//this.setState({ showModal: true });
		this.props.handleModal(true);
	};
	//覆盖生成
	overCreate = () => {
		this.props.overCreate();
	};
	//增量生成
	addCreate = () => {
		this.props.addCreate();
	};
	//取消
	cancelCreate = () => {
		//this.props.addCreate();
		this.closeModal();
	};
	//关闭模态框
	closeModal = () => {
		this.props.handleModal(false);
	};
	//判断2、3页签按钮的可用性
	checkBtnAbled = (docTypeData) => {
		//let {docTypeData} = this.props;
		let btnDisabled = true;
		if (docTypeData.length) {
			for (let i = 0; i < docTypeData.length; i++) {
				if (docTypeData[i].voname) {
					btnDisabled = false;
					break;
				}
			}
		}
		return btnDisabled;
	};
	// 
	onButtonClick = (props, id) => {
		switch(id){
			case 'doc_add_line':// Tab1增行
				this.docTypeTbaleHandle('addLine');
				break;
			case 'doc_del_line'://Tab1删行
				this.docTypeTbaleHandle('delLine');
				break;
			case 'batch_gen'://Tab2批生成
				this.batchCreate();
				break;
			case 'item_add_line'://Tab2增行
				this.matchupTableHandle('addLine');
				break;
			case 'item_del_line'://Tab2删行
				this.matchupTableHandle('delLine');
				break; 
			case 'item_mv_up'://Tab2上移
				this.matchupTableHandle('top');
				break; 
			case 'item_mv_down'://Tab2下移
				this.matchupTableHandle('bottom');
				break;
			case 'bind_add_line'://Tab3增行
				this.bindAccDataHandle('addLine');
				break;
			case 'bind_del_line'://Tab3删行
				this.bindAccDataHandle('delLine');
				break;
			default:
			break;
		}
	}
	componentDidMount(){
		this.docTypeTbaleHandle('addLine');
		this.props.button.setButtonDisabled(['doc_del_line','item_del_line','bind_del_line'],true)
		this.props.BodyRef(this)
	}
	updataBtnStatus = (flag1, flag2, flag3) => {
		this.props.button.setButtonDisabled(['doc_del_line'],flag1)
		this.props.button.setButtonDisabled(['item_del_line'],flag2)
		this.props.button.setButtonDisabled(['bind_del_line'],flag3)
	}
	//tab肩部按钮
	tabBtns = (btnDisabled) => {
		let { pageType, status, orgInfo, accountingBook, accountingBookType, isAllItems, tabkey, appcode,json, bindAccontDisabled } = this.props;
		let btns = '';
		if (status != 'browse') {
			switch (tabkey) {
				case '1':
					btns = (
						<div className="doctype-btns">
							{/* doc_add_line doc_del_line */}
							{this.props.button.createButtonApp({
								area: 'table_header_doc',
								onButtonClick: this.onButtonClick, 
								popContainer: document.querySelector('.doctype-btns')
							})}
						</div>
					);
					break;
				case '2':
					btns = (
						<div className="matchup-table-btns">
							{/* batch_gen item_add_line item_del_line item_mv_up item_mv_down */}
							{this.props.button.createButtonApp({
								area: 'table_header_item',
								onButtonClick: this.onButtonClick, 
								popContainer: document.querySelector('.matchup-table-btns')
							})}
						</div>
					);
					break;
				case '3':
					btns = (
						<div className="bind-item-btns">
							<Checkbox disabled={btnDisabled} checked={isAllItems} onChange={this.handleAllItems}>
								{json['20000CRVRL-000008']}{/* 国际化处理： 适用所有科目*/}
							</Checkbox>
							{pageType == 'org' ? (
								<div className="accounting-book">
									<span className="title nc-theme-common-font-c">{json['20000CRVRL-000009']}：</span>{/* 国际化处理： 核算帐薄*/}
									<span className="refer-content" style={{ display: 'inline-block', width: '200px' }}>
										<ReferLoader
											tag = {'accountbook'}
											refcode = {accbookRefcode}
											value={{
												refname: accountingBook.refname,
												refpk: accountingBook.refpk
											}}
											fieldid='accountbook'
											disabled={bindAccontDisabled}
											// queryContion={orgInfo}
											queryCondition = {() => {
												return {
													"TreeRefActionExt": 'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
													"pk_org": orgInfo.refpk,
												}
											}}
											onChange={(val) => {
												this.handleAccountingBook('accountingBook', val);
											}}
										/>
									</span>
								</div>
							) : (
								<div className='accounting-book'>
									<span className="title nc-theme-common-font-c">{json['20000CRVRL-000010']}：</span>{/* 国际化处理： 核算帐薄类型*/}
									<span className="refer-content" style={{ display: 'inline-block', width: '200px' }}>
										<SetOfBookGridRef
											value={{
												refname: accountingBookType.refname,
												refpk: accountingBookType.refpk
											}}
											fieldid='accountingBookType'
											disabled={bindAccontDisabled}
											onChange={(val) => {
												this.handleAccountingBook('accountingBookType', val);
											}}
										/>
									</span>
								</div>
							)}
							{/* bind_add_line bind_del_line */}
							{this.props.button.createButtonApp({
								area: 'table_header_bind',
								onButtonClick: this.onButtonClick, 
								popContainer: document.querySelector('.bind-item-btns')
							})}
						</div>
					);
					break;
				default:
					break;
			}
		}
		return btns;
	};
	//tab改变回调
	tabChange = (key) => {
		let {docTypeData, isAllItems,json} =this.props
		let btnDisabled = this.checkBtnAbled(docTypeData);
		let tab3BtnStatus = btnDisabled || isAllItems
		if(key!=='1' && docTypeData.length===0){
			toast({ content: json['20000CRVRL-000000'], color: 'warning' });/* 国际化处理： 请选择档案类型！*/
		}
		if(key==='2'){
			// this.matchupTableHandle('addLine');
			// this.props.button.setButtonDisabled(['item_del_line'],true)
			// this.props.button.setButtonDisabled(['batch_gen', 'item_add_line', 'item_del_line', 'item_mv_up', 'item_mv_down'], btnDisabled);
		}
		if(key==='3'){
			// this.bindAccDataHandle('addLine');
			// this.props.button.setButtonDisabled(['bind_del_line'],true)
			// this.props.button.setButtonDisabled(['bind_add_line', 'bind_del_line'], tab3BtnStatus)
		}
		this.props.handleTabkey(key);
	};
	
	/**
	 * 生成科目参照过滤条件
	 */
	getAccountRefCondition = () =>{
		let page = this;
		let condition = {};
		let businessInfo = getBusinessInfo();
		if(page.props.pageType == 'org'){
			condition.pk_accountingbook = page.props.accountingBook.refpk;
			condition.pk_org = page.props.orgInfo.refpk;
			condition.TreeRefActionExt = 'nccloud.web.fipub.ref.AccountAssFilterSqlBuilder';
		}else{
			condition.pk_setofbook = page.props.accountingBookType.refpk;
			condition.pk_org = businessInfo.groupId;
		}
		let buziDate = businessInfo.businessDate;
		condition.dateStr = buziDate.split(' ')[0];
		condition.assTypes = this.getAssTypes(page.props.docTypeData);
		return condition;
	}

	/**
	 * 获取辅助核算类别
	 */
	getAssTypes(docTypeData){
		let assTypes = '';
		if(docTypeData){
			docTypeData.map((item, index) => {
				if(index !== docTypeData.length - 1){
					assTypes += item.vopk + ',';
				}else{
					assTypes += item.vopk;
				}
			});
		}
		return assTypes;
	}

	render() {
		let {
			pageType,
			status,
			matchupCols,
			orgInfo,
			matchupTableData,
			docTypeData,
			bindAccData,
			checkedAll,
			checkedArray,
			accountingBook,
			accountingBookType,
			isAllItems,
			ruleHead,
			showModal,
			modalTableData,
			tabkey,
			json
		} = this.props;
		// let BindItemRefer = pageType == 'org' ? AccountDefaultGridTreeRef : AccountGridTreeRefInGroupOrGlobal;
		let refcode = pageType == 'org' ? accountRefCode : accountGlobalRefCode;
		// let BindItemRefer = <ReferLoader tag = {'account'} refcode/>;
		//档案类型
		const docTypeColumns = [
			{
				title: (
					<div fieldid='firstcol'>
						<Checkbox
							className="table-checkbox"
							checked={checkedAll['tab1'].checked}
							indeterminate={checkedAll['tab1'].indeterminate}
							onChange={this.onAllCheckChange.bind(this, 'tab1')}
						/>
					</div>
				),
				key: 'checkbox',
				attrcode: 'checkbox',
                dataIndex: 'checkbox',
				className: 'table-checkbox-class',
				visible: true,
				itemtype: 'customer',
				width: 60,
				render: (text, record, index) => {
					let rowChecked =
						checkedArray['tab1'][record.idx] && checkedArray['tab1'][record.idx].checked ? true : false;
					return (
						<div fieldid='firstcol'>
							<Checkbox
								className="table-checkbox"
								checked={rowChecked}
								onChange={this.onCheckboxChange.bind(this, text, record, index, 'tab1')}
							/>
						</div>
					);
				}
			},
			{ 
				title: (<div fieldid='idx'>{json['20000CRVRL-000001']}</div>),
				className: 'table-xh-class', 
				dataIndex: 'idx', 
				key: 'idx',
				render: (text, record, index) => <div fieldid='idx'>{text ? text : emptyCell}</div>
			},/* 国际化处理： 序号*/

			{
				title: <div fieldid='voname'><i style={{color: '#E14C46'}}>*</i>{json['20000CRVRL-000011']}</div>,/* 国际化处理： 元数据实体名称*/
				dataIndex: 'voname',
				key: 'voname',
				render: (text, record, index) => {
					return (
						<EditTableCell
							status={status}
							text={text}
							record={record}
							index={index}
							tabType={1}
							type={'voname'}
							docTypeTbaleHandle={this.docTypeTbaleHandle}
							tag={`voname${index}`}
							refcode={'uap/refer/riart/bdmdMainentityAndEnumRef/index.js'}
						/>
					);
				}
			},
			{
				title: (<div fieldid='propname'>{json['20000CRVRL-000002']}</div>),/* 国际化处理： 属性（可选）*/
				dataIndex: 'propname',
				key: 'propname',
				render: (text, record, index) => {
					return (
						<EditTableCell
							status={status}
							text={text}
							record={record}
							index={index}
							tabType={1}
							type={'propname'}
							docTypeTbaleHandle={this.docTypeTbaleHandle}
							Refer={MdTreeByIdRef}
						/>
					);
				}
			}
		];
		//绑定科目
		const bindItemColumns = [
			{
				title: (
					<div fieldid='firstcol'>
						<Checkbox
							className="table-checkbox"
							checked={checkedAll['tab3'].checked}
							indeterminate={checkedAll['tab3'].indeterminate}
							onChange={this.onAllCheckChange.bind(this, 'tab3')}
						/>
					</div>
				),
				key: 'checkbox',
				attrcode: 'checkbox',
                dataIndex: 'checkbox',
				className: 'table-checkbox-class',
				visible: true,
				itemtype: 'customer',
				width: 60,
				render: (text, record, index) => {
					let rowChecked =
						checkedArray['tab3'][record.idx] && checkedArray['tab3'][record.idx].checked ? true : false;
					return (
						<div fieldid='firstcol'>
							<Checkbox
								className="table-checkbox"
								checked={rowChecked}
								onChange={this.onCheckboxChange.bind(this, text, record, index, 'tab3')}
							/>
						</div>
					);
				}
			},
			{ 
				title: (<div fieldid='idx'>{json['20000CRVRL-000001']}</div>),
				className: 'table-xh-class', 
				dataIndex: 'idx', 
				key: 'idx',
				render: (text, record, index) => <div fieldid='idx'>{text ? text : emptyCell}</div>
			 },/* 国际化处理： 序号*/
			{
				title: (<div fieldid='code'>{json['20000CRVRL-000003']}</div>),/* 国际化处理： 科目编码*/
				dataIndex: 'code',
				key: 'code',
				render: (text, record, index) => {
					return (
						<EditTableCell
							tag = {'account'}
							refcode = {refcode}
							status={status}
							text={text}
							record={record}
							index={index}
							tabType={3}
							// Refer={BindItemRefer}
							type={'itemcode'}
							bindAccDataHandle={this.bindAccDataHandle}
							orgInfo = {orgInfo}
							queryCondition={this.getAccountRefCondition()}
						/>
					);
				}
			},
			{
				title: (<div fieldid='name'>{json['20000CRVRL-000004']}</div>),/* 国际化处理： 科目名称*/
				dataIndex: 'name',
				key: 'name',
				render: (text, record, index) => {
					return <EditTableCell type='itemname' status={status} text={text} record={record} index={index} tabType={3} />;
				}
			}
		];
		let btnDisabled = this.checkBtnAbled(docTypeData);
		let btns = this.tabBtns(btnDisabled);
		let tab3Status = btnDisabled || isAllItems
		// let tip = <div>选择档案类型</div>;
		let tableHeight = getTableHeight(185)
		return (
			<div className="cc-card-body">
				<ReferPannel
					json={json}
					pageType={pageType}
					status={status}
					ruleHead={ruleHead}
					orgInfo={orgInfo}
					handleRuleHead={this.handleRuleHead}
				/>
				<div className="cc-card-tabs">
					<Tabs
						defaultActiveKey="1"
						// tabBarStyle="upborder"
						// className="demo1-tabs"
						extraContent={btns}
						onChange={this.tabChange}
						animated={false}
					>
						{/* <Tooltip trigger="hover" placement={'top'} inverse overlay={tip}>
						
						</Tooltip> */}
						<TabPane tab={json['20000CRVRL-000005']} key="1">{/* 国际化处理： 选择档案类型*/}
							<NCDiv fieldid='doctype' areaCode={NCDiv.config.TableCom}>
								<Table 
									bodyStyle={{height: tableHeight}}
									scroll={{ x: true, y: tableHeight}}									
									columns={docTypeColumns} 
									data={docTypeData} 
								/>
							</NCDiv>
						</TabPane>
						<TabPane tab={json['20000CRVRL-000006']} key="2" disabled={btnDisabled}>{/* 国际化处理： 设置档案对应关系*/}
							<NCDiv fieldid='matchup' areaCode={NCDiv.config.TableCom}>
								<Table 
									bodyStyle={{height: tableHeight}}
									scroll={{ x: true, y: tableHeight}}	
									columns={matchupCols} 
									data={matchupTableData} 
									// addBlankCol={false} //是否自动添加空白列
								/>
							</NCDiv>
						</TabPane>
						<TabPane tab={json['20000CRVRL-000007']} key="3" disabled={tab3Status}>{/* 国际化处理： 绑定科目*/}
							<NCDiv fieldid='bindacc' areaCode={NCDiv.config.TableCom}>
								<Table 
									bodyStyle={{height: tableHeight}}
									scroll={{ x: true, y: tableHeight}}	
									columns={bindItemColumns} 
									data={bindAccData} 
								/>
							</NCDiv>
						</TabPane>
					</Tabs>
				</div>

				<Modal className="batch-create-modal m-modal-bsc" show={showModal} onHide={this.closeModal}>
					<Modal.Header closeButton className="text-center">
						<Modal.Title>{json['20000CRVRL-000012']}</Modal.Title>{/* 国际化处理： 批生成*/}
					</Modal.Header>
					<Modal.Body>
						<Table columns={matchupCols} data={modalTableData} />
					</Modal.Body>
					<Modal.Footer className="text-center">
						<Button fieldid='cover-generate' colors="primary" onClick={this.overCreate} bordered>
							{json['20000CRVRL-000013']}{/* 国际化处理： 覆盖生成*/}
						</Button>
						<Button fieldid='incre-generate' colors="primary" onClick={this.addCreate}>
							{json['20000CRVRL-000014']}{/* 国际化处理： 增量生成*/}
						</Button>
						<Button fieldid='cancel' onClick={this.cancelCreate}>
							{json['20000CRVRL-000015']}{/* 国际化处理： 取消*/}
						</Button>
					</Modal.Footer>
				</Modal>
			</div>
		);
	}
}

Body = createPage({})(Body)
export default Body
