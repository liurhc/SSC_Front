import { ajax, viewModel } from 'nc-lightapp-front';
import { ORG_CARD, GROUP_CARD } from '../../config/index';
const { setGlobalStorage, getGlobalStorage, removeGlobalStorage } = viewModel;

export default function initTemplate(props) {
	let check_rule_card = JSON.parse(getGlobalStorage('sessionStorage', 'check_rule_card'))
	let { pageType } = check_rule_card;
	ajax({
		url: '/nccloud/platform/appregister/queryallbtns.do',
		data: pageType == 'org' ? ORG_CARD : GROUP_CARD,
		success: (res) => {
			if (res.success) {
				if (res.data) {
					props.button.setButtons(res.data);
					let { status } = this.state;
					if (status != 'browse') {
						props.button.setButtonVisible([ 'update', 'delete', 'add', 'back' ], false);
					} else {
						props.button.setButtonVisible([ 'save', 'cancel' ], false);
					}
				}
			}
		}
	});
}
