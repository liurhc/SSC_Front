import React, { Component } from 'react';
import { ajax, base, createPage, toast, promptBox, getBusinessInfo, getMultiLang, viewModel } from 'nc-lightapp-front';
import initTemplate from './event/initTemplate';
import Header from './components/header';
import Body from './components/body';
import { linkTo, cartesianProductOf } from '../../util';
import EditTableCell from './components/editTableCell';
import { ORG_LIST, GROUP_LIST } from '../config/index';
const { NCCheckbox: Checkbox } = base;
const { setGlobalStorage, getGlobalStorage, removeGlobalStorage } = viewModel;

import './index.less';
const emptyCell = <span>&nbsp;</span>
class Card extends Component {
	constructor(props) {
		super(props);
		// initTemplate.call(this, props);
		this.state = {
			appcode: this.props.getUrlParam('c') || this.props.getSearchParam('c'),
			showModal: false,
			status: 'browse',
			tabkey: '1',
			pageType: '',
			orgInfo: {},
			ruleHead: {}, //规则本身信息（包含规则名称）
			matchupCols: [],
			matchupTableData: [],
			docTypeData: [],
			bindAccData: [],
			modalTableData: [], //模态框表格数据
			isAllItems: false, //所有科目
			checkedAll: { tab1: {}, tab2: {}, tab3: {} },
			checkedArray: { tab1: {}, tab2: {}, tab3: {} },
			accountingBook: {}, //核算账薄
			accountingBookType: {}, //核算账薄类型  集团
			bindAccontDisabled: false, //绑定科目--核算账簿/类型 是否禁用
			pageBtnStatus: {
				prevBtn: true,
				nextBtn: false
			},
			json: {}
		};
	}
	updatePageBtnStatus = (currIndex, rulePks) =>{

		let {pageBtnStatus} =this.state
		let tempStatus = pageBtnStatus
		if(rulePks.length===0){
			tempStatus = {prevBtn: true, nextBtn: true}
		} else {
			if(currIndex == 0){
				// tempStatus.prevBtn = true
				tempStatus = {prevBtn: true, nextBtn: false}
			}else if(currIndex == rulePks.length - 1){
				// tempStatus.nextBtn = true
				tempStatus = {prevBtn: false, nextBtn: true}
			} else {
				tempStatus = {prevBtn: false, nextBtn: false}
			}
		}
		
		this.setState({
			pageBtnStatus: tempStatus
		})
	}

	componentWillMount() {
		let callback= (json) =>{
			this.setState({
				json:json,
			},()=>{		  
				initTemplate.call(this, this.props);
			})
		}
		getMultiLang({moduleId:'20000CRVRL',domainName:'gl',currentLocale:'simpchn',callback});
	}
	componentDidMount() {
		let check_rule_card = JSON.parse(getGlobalStorage('sessionStorage', 'check_rule_card'))
		let { status, pageType, orgInfo, currPkRule, rulePks } = check_rule_card;
		let currIndex = rulePks.indexOf(currPkRule);
		this.updatePageBtnStatus(currIndex,rulePks)
		let { checkedAll, checkedArray, bindAccontDisabled } = this.state;
		if (status == 'add') {
			bindAccontDisabled = false
			let matchupCols = [
				{
					title: (
						<div fieldid='firstcol'>
							<Checkbox
								className="table-checkbox"
								checked={checkedAll['tab2'].checked}
								indeterminate={checkedAll['tab2'].indeterminate}
								onChange={this.onAllCheckChange.bind(this, 'tab2')}
							/>
						</div>	
					),
					key: 'checkbox',
					attrcode: 'checkbox',
                	dataIndex: 'checkbox',
					className: 'table-checkbox-class',
					visible: true,
					itemtype: 'customer',
					width: 60,
					render: (text, record, index) => {
						let rowChecked =
							checkedArray['tab2'][record.idx] && checkedArray['tab2'][record.idx].checked ? true : false;
						return (
							<div fieldid='firstcol'>
								<Checkbox
									className="table-checkbox"
									checked={rowChecked}
									onChange={this.onCheckboxChange.bind(this, text, record, index, 'tab2')}
								/>
							</div>
						);
					}
				},
				{ 
					title: (<div fieldid='idx'>{this.state.json['20000CRVRL-000001']}</div>),
					className: 'table-xh-class', 
					dataIndex: 'idx', 
					key: 'idx',
					render: (text, record, index) => <div fieldid='idx'>{text ? text : emptyCell}</div>
				 }/* 国际化处理： 序号*/
			];
			this.setState({ matchupCols });
		} else {
			bindAccontDisabled = true
			this.queryRule(currPkRule, pageType);
		}
		this.setState({ status, pageType, orgInfo, bindAccontDisabled });
	}
	//核算账薄
	handleAccountingBook = (key, val) => {
		this.setState({ [key]: val });
	};
	//所有科目
	handleAllItems = () => {
		let bindAccData = [];
		if (!this.state.isAllItems) {
			bindAccData.push({ name: this.state.json['20000CRVRL-000021'], code: 'ALL', idx: 1, key: 1, pk_bindmeta: '' });/* 国际化处理： 所有科目*/
		}
		this.setState({ isAllItems: !this.state.isAllItems, bindAccData });
	};
	//规则名称
	handleRuleHead = (val) => {
		let maxLength = 50
		if(val.length <= maxLength){
			this.state.ruleHead.name = val;	
		} else {
			this.state.ruleHead.name = val.slice(0, maxLength)
		}
		this.setState({ ruleHead: this.state.ruleHead });	
	};
	//查询规则
	queryRule = (pk_rule, pageType='') => {
		let { accountingBook, accountingBookType } = this.state
		ajax({
			url: '/nccloud/fipub/rulectrl/queryRuleByPk.do',
			data: { pk_rule },
			success: (res) => {
				let {success, data} = res
				if (success) {
					if (data) {
						if(pageType==='org'){
							accountingBook.refpk = data.head.pk_accountingbook
							accountingBook.refname = data.head.accountingbookName
						} else {
							accountingBookType.refpk = data.head.pk_accountingbook
							accountingBookType.refname = data.head.accountingbookName
						}
						let resTableData = data.items;
						let bindItems = data.bindItems;
						this.setState({ ruleHead: data.head, accountingBook, accountingBookType });
						this.buildTableColumnsAndData(resTableData, bindItems);
					}
				}
			}
		});
	};
	//处理档案对应关系数据
	buildMatchupCols = (newCols) => {
		let { checkedAll, checkedArray } = this.state
		let columns = [
			{
				title: (
					<div fieldid='firstcol'>
						<Checkbox
							className="table-checkbox"
							checked={checkedAll['tab2'].checked}
							indeterminate={checkedAll['tab2'].indeterminate}
							onChange={this.onAllCheckChange.bind(this, 'tab2')}
						/>
					</div>
				),
				key: 'checkbox',
				attrcode: 'checkbox',
				dataIndex: 'checkbox',
				className: 'table-checkbox-class',
				visible: true,
				itemtype: 'customer',
				width: 60,
				render: (text, record, index) => {
					let rowChecked =
						checkedArray['tab2'][record.idx] && checkedArray['tab2'][record.idx].checked ? true : false;
					return (
						<div fieldid='firstcol'>
							<Checkbox
								className="table-checkbox"
								checked={rowChecked}
								onChange={this.onCheckboxChange.bind(this, text, record, index, 'tab2')}
							/>
						</div>
					);
				}
			},
			{ 
				title: (<div fieldid='idx'>{this.state.json['20000CRVRL-000001']}</div>), 
				dataIndex: 'idx', 
				key: 'idx',
				render: (text, record, index) => <div fieldid='idx'>{text ? text : emptyCell}</div>
			}/* 国际化处理： 序号*/
		]
		return [...columns, ...newCols]
	}
	//构建columns和data
	buildTableColumnsAndData = (resTableData, bindItems) => {
		let { status, checkedAll, checkedArray } = this.state;
		let modalTableData = [];/* 批生成模态框表格数据 */
		if (!resTableData) {
			this.setState({ columns: [], tableData: [] });
			return;
		}
		let columnsData = {},
			oriTableData = [],
			columns = [
				{
					title: (
						<div fieldid='firstcol'>
							<Checkbox
								className="table-checkbox"
								checked={checkedAll['tab2'].checked}
								indeterminate={checkedAll['tab2'].indeterminate}
								onChange={this.onAllCheckChange.bind(this, 'tab2')}
							/>
						</div>
					),
					key: 'checkbox',
					attrcode: 'checkbox',
                    dataIndex: 'checkbox',
					className: 'table-checkbox-class',
					visible: true,
					itemtype: 'customer',
					width: 60,
					render: (text, record, index) => {
						let rowChecked =
							checkedArray['tab2'][record.idx] && checkedArray['tab2'][record.idx].checked ? true : false;
						return (
							<div fieldid='firstcol'>
								<Checkbox
									className="table-checkbox"
									checked={rowChecked}
									onChange={this.onCheckboxChange.bind(this, text, record, index, 'tab2')}
								/>
							</div>
						);
					}
				},
				{ 
					title: (<div fieldid='idx'>{this.state.json['20000CRVRL-000001']}</div>), 
					dataIndex: 'idx', 
					key: 'idx',
					render: (text, record, index) => <div fieldid='idx'>{text ? text : emptyCell}</div>
				}/* 国际化处理： 序号*/
			],
			tableData = [],
			docTypeData = [],
			bindAccData = [];
		for (let i = 0; i < resTableData.length; i++) {
			if (resTableData[i].styleflag == 0) {
				columnsData = resTableData[i];
			} else {
				oriTableData.push(resTableData[i]);
			}
		}
		//构建档案对应关系columns   档案类型数据
		for (let i = 1; i < 10; i++) {
			let item = columnsData[`item${i}`];
			if (item) {
				let titleTxt = item.propName ? `${item.bdName}\\${item.propName}` : item.bdName;
				let title = <div fieldid={`item${i}`}><i style={{color: '#E14C46'}}>*</i>{titleTxt}</div>
				let col = {
					title,
					dataIndex: `col${i}`,
					key: `col${i}`,
					width: 160,
					render: (text, record, index) => {
						return this.newEditTableCell(text, record, index, 2, `col${i}`);
					}
				};
				let docTypeDataItem = {
					voname: item.bdName,
					vopk: item.pk_bd,
					propname: item.propName || '',
					proppk: item.prop || '',
					refClass: item.refClass,
					key: i,
					idx: i
				};
				docTypeData.push(docTypeDataItem);
				columns.push(col);
			}
		}
		//构建档案对应关系tableData
		for (let i = 0; i < oriTableData.length; i++) {
			let row = { key: oriTableData[i].pk_item, idx: i + 1 };
			for (let j = 1; j < 10; j++) {
				if (oriTableData[i][`item${j}`]) {
					row[`col${j}`] =
						oriTableData[i][`item${j}`].bdCode + '\\' + (oriTableData[i][`item${j}`].bdName || '');
					row[`col${j}pk_bd`] = oriTableData[i][`item${j}`].pk_bd;
					//row[`col${j}prop`] = oriTableData[i][`item${j}`].prop;
				}
			}
			tableData.push(row);
		}
		for (let i = 0; i < tableData.length; i++) {
			for (let j = 0; j < docTypeData.length; j++) {
				tableData[i][`col${j + 1}beanid`] =
					docTypeData[j].proppk == '' ? docTypeData[j].vopk : docTypeData[j].refClass;
			}
		}
		for (let i = 0; i < bindItems.length; i++) {
			bindAccData.push({
				pk_bindmeta: bindItems[i].pk_bindmeta,
				name: bindItems[i].name,
				code: bindItems[i].code,
				idx: i + 1,
				key: i + 1
			});
		}	
		//模态框表格数据
		let line = {};
		line.idx = 1;
		line.key = 1;
		if (docTypeData.length) {
			for (let i = 0; i < docTypeData.length; i++) {
				line[`col${i + 1}`] = '';
				line[`col${i + 1}beanid`] = docTypeData[i].proppk ? docTypeData[i].refClass : docTypeData[i].vopk;
			}
			line.cols = docTypeData.length;
			modalTableData = [ line ];
		}
	
		this.setState({ matchupCols: columns, matchupTableData: tableData, docTypeData, bindAccData, modalTableData});
	};
	//更新规则保存
	updateRule = () => {
		// 档案类型itemid为0，档案对应关系itemid为1
		let { bindAccData, docTypeData, matchupTableData, ruleHead, accountingBook, status, orgInfo, accountingBookType, pageType } = this.state;
		if (!ruleHead.name || !ruleHead.name.replace(/(^\s*)|(\s*$)/g, '')) {
			toast({ color: 'warning', content: this.state.json['20000CRVRL-000022'] });/* 国际化处理： 请填写规则名称！*/
			return;
		}
		if (!docTypeData.length || !matchupTableData.length || !bindAccData.length) {
			toast({ color: 'warning', content: this.state.json['20000CRVRL-000023'] });/* 国际化处理： 请选择档案类型、档案对应关系、绑定科目！*/
			return;
		}
		let bindItems = [],
			items = [];
		if(pageType === 'org'){
			accountingBook ? (ruleHead.pk_accountingbook = accountingBook.refpk) : null;
		}else{
			accountingBookType ? (ruleHead.pk_accountingbook = accountingBookType.refpk) : null;
		}
		ruleHead.pk_org = orgInfo.refpk;
		for (let i = 0; i < bindAccData.length; i++) {
			bindItems.push({
				name: bindAccData[i].name,
				code: bindAccData[i].code,
				pk_bindmeta: bindAccData[i].pk_bindmeta ? bindAccData[i].pk_bindmeta : null
			});
		}
		let tableHead = { styleflag: '0', itemid: '0' };
		for (let i = 0; i < 9; i++) {
			if (docTypeData[i]) {
				tableHead[`item${i + 1}`] = {
					pk_bd: docTypeData[i].vopk,
					prop: docTypeData[i].proppk ? docTypeData[i].proppk.split('#')[0] : null
				};
			} else {
				tableHead[`item${i + 1}`] = null;
			}
		}
		items.push(tableHead);
		for (let i = 0; i < matchupTableData.length; i++) {
			let row = { styleflag: '1', itemid: '1' };
			// let row = { styleflag: '1', itemid: matchupTableData[i].idx };
			for (let j = 1; j < 10; j++) {
				if (matchupTableData[i][`col${j}pk_bd`]) {
					row[`item${j}`] = {
						pk_bd: matchupTableData[i][`col${j}pk_bd`]
						//prop: matchupTableData[i].proppk == '' ? null : matchupTableData[i].proppk
					};
				} else {
					row[`item${j}`] = null;
				}
			}
			items.push(row);
		}
		let url = '';
		if (status == 'add') {
			url = '/nccloud/fipub/rulectrl/insertRule.do';
		} else if (status == 'edit') {
			url = '/nccloud/fipub/rulectrl/updateRule.do';
		}

		ajax({
			url,
			data: { head: ruleHead, items, bindItems },
			success: (res) => {
				if (res.success) {
					toast({ color: 'success', content: this.state.json['20000CRVRL-000024'] });/* 国际化处理： 保存成功！*/
					this.queryRule(res.data);
					let check_rule_card = JSON.parse(getGlobalStorage('sessionStorage', 'check_rule_card'))
					let { status, pageType, currPkRule, rulePks } = check_rule_card;
					rulePks.splice(rulePks.indexOf(currPkRule), 1, res.data);
					check_rule_card.currPkRule = res.data;
					setGlobalStorage('sessionStorage', 'check_rule_card', JSON.stringify(check_rule_card));
					this.props.button.setButtonVisible([ 'save', 'cancel' ], false);
					this.props.button.setButtonVisible([ 'update', 'delete', 'add', 'back' ], true);
					this.setState({ status: 'browse' });
				} else {
					toast({ color: 'warning', content: this.state.json['20000CRVRL-000025'] });/* 国际化处理： 保存失败！*/
				}
			}
		});
	};
	//删除规则
	deleteRule = (rule) => {
		if(!rule){
			toast({ color: 'warning', content: this.state.json['20000CRVRL-000044'] });/* 国际化处理： 没有需要删除的规则！*/
		} else {
			promptBox({
				color: 'warning',
				title: this.state.json['20000CRVRL-000026'],  /* 国际化处理： 删除*/
				content: this.state.json['20000CRVRL-000027'],/* 国际化处理： 确定要删除吗？*/
				beSureBtnClick: () => {
					ajax({
						url: '/nccloud/fipub/rulectrl/deleteRule.do',
						data: { pk_rule: rule },
						success: (res) => {
							if (res.success) {
								toast({ color: 'success', content: this.state.json['20000CRVRL-000028'] });/* 国际化处理： 删除成功！*/
								setTimeout(() => {
									this.switchPage();
								}, 1000);
							}
						}
					});
				},
				cancelBtnClick: () => {}
			});
		}
	};
	switchPage = () => {
		let { orgInfo, pageType } = this.state;
		let check_rule_card = { orgInfo };
		let url = `/gl/crosscheck/pages/list_${pageType}/index.html`;
		let params = pageType == 'org' ? ORG_LIST : GROUP_LIST;
		linkTo(this.props, url, params, check_rule_card);
	};
	//按钮操作
	headerBtnClick = (props, type) => {
		let check_rule_card = JSON.parse(getGlobalStorage('sessionStorage', 'check_rule_card'))
		let { status, pageType, currPkRule } = check_rule_card;
		switch (type) {
			case 'delete':
				this.deleteRule(currPkRule);
				break;
			case 'save':
				this.updateRule();
				break;
			case 'cancel':
				promptBox({
					color: 'warning',
					title: this.state.json['20000CRVRL-000015'],  /* 国际化处理： 取消*/
					content: this.state.json['20000CRVRL-000029'],/* 国际化处理： 确定要取消吗？*/
					beSureBtnClick: () => {
						this.queryRule(currPkRule);
						if(status == 'add'){
							this.setState({
								tabkey: '1',
								ruleHead: {},
								matchupTableData: [],
								docTypeData: [],
								bindAccData: []
							})
						}
						this.setState({ status: 'browse' });
						this.props.button.setButtonVisible([ 'save', 'cancel' ], false);
						this.props.button.setButtonVisible([ 'update', 'delete', 'add', 'back' ], true);
					},
					cancelBtnClick: () => {}
				});
				
				break;
			case 'update':
				this.setState({ status: 'edit', bindAccontDisabled: true });
				this.props.button.setButtonVisible([ 'save', 'cancel' ], true);
				this.props.button.setButtonVisible([ 'update', 'delete', 'add', 'back' ], false);
				this.resetCheckbox();
				break;
			case 'add':
				let { checkedAll, checkedArray } = this.state;
				this.setState({
					status: 'add',
					bindAccontDisabled: false,
					ruleHead: {},
					matchupCols: [
						{
							title: (
								<div fieldid='firstcol'>
									<Checkbox
										className="table-checkbox"
										checked={checkedAll['tab2'].checked}
										indeterminate={checkedAll['tab2'].indeterminate}
										onChange={this.onAllCheckChange.bind(this, 'tab2')}
									/>
								</div>
							),
							key: 'checkbox',
							attrcode: 'checkbox',
                			dataIndex: 'checkbox',
							className: 'table-checkbox-class',
							visible: true,
							itemtype: 'customer',
							width: 60,
							render: (text, record, index) => {
								let rowChecked =
									checkedArray['tab2'][record.idx] && checkedArray['tab2'][record.idx].checked
										? true
										: false;
								return (
									<div fieldid='firstcol'>
										<Checkbox
											className="table-checkbox"
											checked={rowChecked}
											onChange={this.onCheckboxChange.bind(this, text, record, index, 'tab2')}
										/>
									</div>
								);
							}
						},
						{ 
							title: (<div fieldid='idx'>{this.state.json['20000CRVRL-000001']}</div>), 
							dataIndex: 'idx', 
							key: 'idx',
							render: (text, record, index) => <div fieldid='idx'>{text ? text : emptyCell}</div>
						}/* 国际化处理： 序号*/
					],
					matchupTableData: [],
					docTypeData: [],
					bindAccData: [],
					isAllItems: false,
					checkedAll: { tab1: {}, tab2: {}, tab3: {} },
					checkedArray: { tab1: {}, tab2: {}, tab3: {} }
				});
				this.props.button.setButtonVisible([ 'save', 'cancel' ], true);
				this.props.button.setButtonVisible([ 'update', 'delete', 'add', 'back' ], false);
				break;
			// case 'back':
			// 	this.switchPage();
			// 	break;
			default:
				break;
		}
	};
	//首条、上一条、下一条、末条
	browseItem = (type) => {
		let check_rule_card = JSON.parse(getGlobalStorage('sessionStorage', 'check_rule_card'))
		let { status, pageType, orgInfo, currPkRule, rulePks } = check_rule_card;
		let currIndex = rulePks.indexOf(currPkRule);
		// this.updatePageBtnStatus(currIndex,rulePks)
		let queryRulePk = '';
		switch (type) {
			case 'first':
			case 'prev':
				if (currIndex == 0) {
					toast({ color: 'warning', content: this.state.json['20000CRVRL-000030'] });/* 国际化处理： 已经是第一条了！*/
				} else {
					queryRulePk = type == 'first' ? rulePks[0] : rulePks[currIndex - 1];
					let pageIndex = rulePks.indexOf(queryRulePk);
					this.updatePageBtnStatus(pageIndex,rulePks)
					this.queryRule(queryRulePk);
					check_rule_card.currPkRule = queryRulePk;
					setGlobalStorage('sessionStorage', 'check_rule_card', JSON.stringify(check_rule_card));
				}
				break;
			case 'last':
			case 'next':
				if (currIndex == rulePks.length - 1) {
					toast({ color: 'warning', content: this.state.json['20000CRVRL-000031'] });/* 国际化处理： 已经是最后一条了！*/
				} else {
					queryRulePk = type == 'last' ? rulePks[rulePks.length - 1] : rulePks[currIndex + 1];
					let pageIndex = rulePks.indexOf(queryRulePk);
					this.updatePageBtnStatus(pageIndex,rulePks)
					this.queryRule(queryRulePk);
					check_rule_card.currPkRule = queryRulePk;
					setGlobalStorage('sessionStorage', 'check_rule_card', JSON.stringify(check_rule_card));
				}
				break;
			default:
				break;
		}
	};
	//操作档案类型表格
	docTypeTbaleHandle = (type, val, index, colname) => {
		let docTypeData = this.state.docTypeData.concat([]);
		let matchupCols = this.state.matchupCols.concat([]);
		let matchupTableData = this.state.matchupTableData.concat([]);
		let modalTableData = this.state.modalTableData.concat([]);
		switch (type) {
			case 'edit':
				let refname = '',
					refpk = '';
				if (colname == 'propname') {
					refname = 'propname';
					refpk = 'proppk';
					val = [ val ];
					for (let i = 0; i < val.length; i++) {
						docTypeData[index + i][refname] = val[i].refname || '';
						docTypeData[index + i][refpk] = val[i].refpk || '';
					}
					//修改级联table表头
					let edittitle = docTypeData[index].propname
						? `${docTypeData[index].voname}\\${docTypeData[index].propname}`
						: docTypeData[index].voname;
					let title = <div fieldid={`item${index + 1}`}><i style={{color: '#E14C46'}}>*</i>{edittitle}</div>
					if (!matchupCols[index + 2]) {
						matchupCols[index + 2] = {
							// fieldid: `col${index + 1}`,
							dataIndex: `col${index + 1}`,
							key: `col${index + 1}`,
							width: 160,
							render: (text, record, index) => {
								return this.newEditTableCell(text, record, index, 2, `col${index + 1}`);
							}
						};
					}
					matchupCols[index + 2].title = title;
				} else if (colname == 'voname') {
					refname = 'voname';
					refpk = 'vopk';
					docTypeData.splice(index, 1);
					for (let i = 0; i < val.length; i++) {
						let newdocData = {
							[refname]: val[i].refname || '',
							[refpk]: val[i].refpk || '',
							idx: index + i + 1,
							key: index + i + 1,
							newLine: true
						};
						docTypeData.splice(index + i, 0, newdocData);
					}
					for (let i = 0; i < docTypeData.length; i++) {
						docTypeData[i].key = docTypeData[i].idx = i + 1;
					}
					//修改级联table表头
					matchupCols.splice(index + 2, 1);
					for (let i = 0; i < val.length; i++) {
						let edittitle = docTypeData[index + i].propname
							? `${docTypeData[index + i].voname}\\${docTypeData[index + i].propname}`
							: docTypeData[index + i].voname;
						let title = <div fieldid={`item${index + i + 1}`}><i style={{color: '#E14C46'}}>*</i>{edittitle}</div>
						let newmatCol = {
							title: title,
							// fieldid: `col${index + i + 1}`,
							dataIndex: `col${index + i + 1}`,
							key: `col${index + i + 1}`,
							width: 160,
							render: (text, record, index) => {
								return this.newEditTableCell(text, record, index, 2, `col${index + i + 1}`);
							}
						};
						matchupCols.splice(index + i + 2, 0, newmatCol);
					}
					for (let i = 2; i < matchupCols.length; i++) {
						matchupCols[i].key = `col${i - 1}`;
						// matchupCols[i].fieldid = `col${i - 1}`;
						matchupCols[i].dataIndex = `col${i - 1}`;
						matchupCols[i].width = 160;
						matchupCols[i].render = (text, record, index) => {
							return this.newEditTableCell(text, record, index, 2, `col${i - 1}`);
						};
					}
				}
				//修改级联table数据
				if (matchupTableData.length) {
					for (let j = 0; j < val.length; j++) {
						for (let i = 0; i < matchupTableData.length; i++) {
							matchupTableData[i][`col${index + j + 1}`] = '';
							matchupTableData[i][`col${index + j + 1}beanid`] = docTypeData[index + j].proppk
								? docTypeData[index + j].proppk
								: docTypeData[index + j].vopk;
						}
					}
				}
				//模态框表格数据
				let line = {};
				line.idx = 1;
				line.key = 1;
				if (docTypeData.length) {
					for (let i = 0; i < docTypeData.length; i++) {
						line[`col${i + 1}`] = '';
						line[`col${i + 1}beanid`] = docTypeData[i].proppk ? docTypeData[i].proppk : docTypeData[i].vopk;
					}
					line.cols = docTypeData.length;
					modalTableData = [ line ];
				}
				this.setState({ docTypeData, matchupCols, matchupTableData, modalTableData });
				break;
			case 'addLine':
				let lastLine = docTypeData[docTypeData.length - 1];
				let newLine = {};
				for (let key in lastLine) {
					newLine[key] = '';
				}
				newLine.idx = docTypeData.length + 1;
				newLine.key = docTypeData.length + 1;
				newLine.newLine = true;
				docTypeData.push(newLine);
				//修改级联table表头
				let newtitle = newLine.propname ? `${newLine.voname}\\${newLine.propname}` : newLine.voname;
				let title = <div fieldid={`item${docTypeData.length}`}><i style={{color: '#E14C46'}}>*</i>{newtitle}</div>
				let newcol = {
					title: title,
					// fieldid: `col${docTypeData.length}`,
					dataIndex: `col${docTypeData.length}`,
					key: `col${docTypeData.length}`,
					width: 160,
					render: (text, record, index) => {
						return this.newEditTableCell(text, record, index, 2, `col${docTypeData.length}`);
					}
				};
				matchupCols.push(newcol);
				// this.setState({ docTypeData, matchupCols, modalTableData });
				this.setState({ docTypeData, matchupCols });
				break;
			case 'delLine':
				let checkedRows = this.state.checkedArray.tab1;
				debugger
				let checkedLen = Object.keys(checkedRows).length;
				if (checkedLen) {
					for (const key in checkedRows) {
						//处理档案类型数据
						for (let i = 0; i < docTypeData.length; i++) {
							if (docTypeData[i].idx == key) {
								docTypeData.splice(i, 1);
							}
						}
						//处理档案对应关系数据
						// for (let j = 0; j < matchupCols.length; j++) {
						// 	if (matchupCols[j].dataIndex == `col${key}`) {
						// 		matchupCols.splice(j, 1);
						// 	}
						// }
						// if (docTypeData.length) {
						// 	for (let n = 0; n < matchupTableData.length; n++) {
						// 		delete matchupTableData[n][`col${key}`];
						// 	}
						// } else {
						// 	matchupTableData = [];
						// }
					}
					let newCols = []
					for (let i = 0; i < docTypeData.length; i++) {
						docTypeData[i].idx = i + 1;
						docTypeData[i].key = i + 1;
						//处理档案对应关系数据
						debugger
						let titleTxt = docTypeData[i].propname ? `${docTypeData[i].voname}\\${docTypeData[i].propname}` : docTypeData[i].voname;
						let title = <div fieldid={`item${i}`}><i style={{color: '#E14C46'}}>*</i>{titleTxt}</div>
						let col = {
							title,
							dataIndex: `col${i+1}`,
							key: `col${i+1}`,
							width: 160,
							render: (text, record, index) => {
								return this.newEditTableCell(text, record, index, 2, `col${i+1}`);
							}
						};
						newCols.push(col)
					}
					let tempMatchupCols = this.buildMatchupCols(newCols)
					this.state.checkedArray.tab1 = {};
					this.state.checkedAll.tab1 = { checked: false, indeterminate: false };
					this.setState({
						checkedArray: this.state.checkedArray,
						checkedAll: this.state.checkedAll,
						docTypeData,
						matchupCols: tempMatchupCols,
						matchupTableData: []
					});
				} else {
					toast({ color: 'warning', content: this.state.json['20000CRVRL-000032'] });/* 国际化处理： 请选中删除行！*/
				}
				break;
			default:
				break;
		}
	};
	//上移、下移操作
	move = (type) => {
		let matchupTableData = this.state.matchupTableData.concat([]);
		let checkedArray = Object.assign({}, this.state.checkedArray.tab2);
		let keys = Object.keys(checkedArray);
		if (keys.length == 1) {
			if (type == 'top' && keys[0] == '1') {
				toast({ color: 'warning', content: this.state.json['20000CRVRL-000033'] });/* 国际化处理： 第一行无法上移！*/
				return;
			}
			if (type == 'bottom' && keys[0] == matchupTableData.length) {
				toast({ color: 'warning', content: this.state.json['20000CRVRL-000034'] });/* 国际化处理： 最后一行无法下移！*/
				return;
			}
			let oldIndex = keys[0] - 1;

			let exchangeIndex = type == 'top' ? oldIndex - 1 : oldIndex + 1;
			let exchangeItem = matchupTableData[exchangeIndex];
			let moveItem = matchupTableData.splice(oldIndex, 1)[0];
			exchangeItem.idx = type == 'top' ? exchangeItem.idx + 1 : exchangeItem.idx - 1;
			moveItem.idx = type == 'top' ? moveItem.idx - 1 : moveItem.idx + 1;
			matchupTableData.splice(type == 'top' ? oldIndex - 1 : oldIndex + 1, 0, moveItem);
			checkedArray = { [moveItem.idx]: { checked: true, record: moveItem } };
			this.state.checkedArray.tab2 = checkedArray;
			this.setState({ matchupTableData, checkedArray: this.state.checkedArray });
		} else {
			toast({ color: 'warning', content: this.state.json['20000CRVRL-000035'] });/* 国际化处理： 请选择一条要移动的数据！*/
		}
	};
	//操作档案对应关系表格
	matchupTableHandle = (type, val, index, col, isModal) => {
		let matchupTableData = this.state.matchupTableData.concat([]);
		let docTypeData = this.state.docTypeData.concat([]);
		let modalTableData = this.state.modalTableData.concat([]);
		switch (type) {
			case 'edit':
				if (isModal) {
					let text = '';
					let display = '';
					for (let i = 0; i < val.length; i++) {
						if (i == val.length - 1) {
							text += `${val[i].refname}#${val[i].refpk}#${modalTableData[0][`${col}beanid`]}`;
							display += val[i].refname;
							continue;
						}
						text += `${val[i].refname}#${val[i].refpk}#${modalTableData[0][`${col}beanid`]}#$#`;
						display += val[i].refname + ' ';
					}
					modalTableData[0][col] = display;
					modalTableData[0][`new${col}`] = text;
					this.setState({ modalTableData });
				} else {
					matchupTableData[index][col] = val.refname;
					matchupTableData[index][`${col}pk_bd`] = val.refpk;
					this.setState({ matchupTableData });
				}
				break;
			case 'addLine':
				let newLine = {};
				if (matchupTableData.length) {
					let lastLine = matchupTableData[matchupTableData.length - 1];
					for (let key in lastLine) {
						if (key.indexOf('beanid') != -1) {
							newLine[key] = lastLine[key];
							continue;
						}
						newLine[key] = '';
					}
					newLine.idx = matchupTableData.length + 1;
					newLine.key = matchupTableData.length + 1;
					newLine.newLine = true;
					matchupTableData.push(newLine);
				} else {
					newLine.idx = 1;
					newLine.key = 1;
					newLine.newLine = true;
					if (docTypeData.length) {
						for (let i = 0; i < docTypeData.length; i++) {
							newLine[`col${i + 1}`] = '';
							newLine[`col${i + 1}beanid`] = docTypeData[i].proppk
								? docTypeData[i].proppk
								: docTypeData[i].vopk;
						}
						matchupTableData.push(newLine);
					} else {
						toast({ color: 'warning', content: this.state.json['20000CRVRL-000000'] });/* 国际化处理： 请选择档案类型！*/
					}
				}

				this.setState({ matchupTableData });
				break;
			case 'delLine':
				let checkedRows = this.state.checkedArray.tab2;
				let checkedLen = Object.keys(checkedRows).length;
				if (checkedLen) {
					for (const key in checkedRows) {
						for (let i = 0; i < matchupTableData.length; i++) {
							if (matchupTableData[i].idx == key) {
								matchupTableData.splice(i, 1);
							}
						}
					}
					this.state.checkedArray.tab2 = {};
					this.state.checkedAll.tab2 = { checked: false, indeterminate: false };
					this.setState({
						matchupTableData,
						checkedArray: this.state.checkedArray,
						checkedAll: this.state.checkedAll
					});
				} else {
					toast({ color: 'warning', content: this.state.json['20000CRVRL-000036'] });/* 国际化处理： 请选择删除数据！*/
				}
				break;
			case 'top':
			case 'bottom':
				this.move(type);
				break;
			default:
				break;
		}
	};
	//绑定科目表格
	bindAccDataHandle = (type, val, index) => {
		let bindAccData = this.state.bindAccData.concat([]);
		let { isAllItems } = this.state;
		if (isAllItems) {
			return;
		}
		switch (type) {
			case 'addLine':
				let newLine = { code: '', name: '', pk_bindmeta: '' };
				newLine.idx = bindAccData.length + 1;
				newLine.key = bindAccData.length + 1;
				bindAccData.push(newLine);
				this.setState({ bindAccData });
				break;
			case 'delLine':
				let checkedRows = this.state.checkedArray.tab3;
				let checkedLen = Object.keys(checkedRows).length;
				if (checkedLen) {
					for (const key in checkedRows) {
						for (let i = 0; i < bindAccData.length; i++) {
							if (bindAccData[i].idx == key) {
								bindAccData.splice(i, 1);
							}
						}
					}
					this.state.checkedArray.tab3 = {};
					this.state.checkedAll.tab3 = { checked: false, indeterminate: false };
					this.setState({
						bindAccData,
						checkedArray: this.state.checkedArray,
						checkedAll: this.state.checkedAll
					});
				} else {
					toast({ color: 'warning', content: this.state.json['20000CRVRL-000036'] });/* 国际化处理： 请选择删除数据！*/
				}
				break;
			case 'edit':
				if (val.length == 1) {
					bindAccData[index].name = val[0].refname;
					bindAccData[index].code = val[0].refcode;
					bindAccData[index].pk_bindmeta = val[0].refpk;
				} else if (val.length > 1) {
					bindAccData.splice(index, 1);
					for (let i = 0; i < val.length; i++) {
						let item = {
							name: val[i].refname,
							code: val[i].refcode,
							pk_bindmeta: val[i].refpk,
							idx: index + i + 1,
							key: index + i + 1
						};
						bindAccData.splice(index + i, 0, item);
					}
					for (let i = 0; i < bindAccData.length; i++) {
						bindAccData[i].idx = i + 1;
						bindAccData[i].key = i + 1;
					}
				}

				this.setState({ bindAccData });
				break;
			default:
				break;
		}
	};
	//单选操作
	onCheckboxChange = (text, record, index, tab) => {
		let { checkedArray, docTypeData, matchupTableData, bindAccData, checkedAll } = this.state;
		let currentTableData = [];
		if (tab == 'tab1') {
			currentTableData = docTypeData.concat([]);
		} else if (tab == 'tab2') {
			currentTableData = matchupTableData.concat([]);
		} else if ((tab = 'tab3')) {
			currentTableData = bindAccData.concat([]);
		}

		let currCheckedArray = checkedArray[tab];
		if (currCheckedArray[record.idx] && currCheckedArray[record.idx].checked) {
			delete currCheckedArray[record.idx];
		} else {
			currCheckedArray[record.idx] = { checked: true, record };
		}
		let currCheckedAll = { checked: false, indeterminate: false };
		let checkedLen = Object.keys(currCheckedArray).length;
		if (checkedLen == currentTableData.length) {
			currCheckedAll.checked = true;
			currCheckedAll.indeterminate = false;
		} else if (checkedLen > 0 && checkedLen < currentTableData.length) {
			currCheckedAll.checked = false;
			currCheckedAll.indeterminate = true;
		}
		checkedAll[tab] = currCheckedAll;
		this.setState({ 
			checkedArray, 
			checkedAll 
		}, ()=>{
			let {checkedArray}=this.state
			let {tab1, tab2, tab3} = checkedArray
			let isCheckedTab1 = JSON.stringify(tab1) == "{}"
			let isCheckedTab2 = JSON.stringify(tab2) == "{}"
			let isCheckedTab3 = JSON.stringify(tab3) == "{}"
			this.Body.updataBtnStatus(isCheckedTab1,isCheckedTab2,isCheckedTab3)			
		});
	};
	//全选操作
	onAllCheckChange = (tab, status) => {
		let checkedArray = this.state.checkedArray;
		let checkedAll = this.state.checkedAll;
		if (status) {
			let tableData = [];
			if (tab == 'tab1') {
				tableData = this.state.docTypeData;
			} else if (tab == 'tab2') {
				tableData = this.state.matchupTableData;
			} else {
				tableData = this.state.bindAccData;
			}
			if (tableData && tableData.length) {
				for (let i = 0; i < tableData.length; i++) {
					checkedArray[tab][tableData[i].idx] = { checked: true, record: tableData[i] };
				}
			}
		} else {
			checkedArray[tab] = {};
		}
		checkedAll[tab] = { checked: status, indeterminate: false };
		if (tab == 'tab2') {
			this.resetCheckbox();
		}
		this.setState({ 
			checkedArray, 
			checkedAll 
		},()=>{
			let {checkedArray}=this.state
			let {tab1, tab2, tab3} = checkedArray
			let isCheckedTab1 = JSON.stringify(tab1) == "{}"
			let isCheckedTab2 = JSON.stringify(tab2) == "{}"
			let isCheckedTab3 = JSON.stringify(tab3) == "{}"
			this.Body.updataBtnStatus(isCheckedTab1,isCheckedTab2,isCheckedTab3)	
		});
	};
	resetCheckbox = () => {
		let { checkedAll, checkedArray } = this.state;
		this.state.matchupCols[0] = {
			title: (
				<div fieldid='firstcol'>
					<Checkbox
						className="table-checkbox"
						checked={checkedAll['tab2'].checked}
						indeterminate={checkedAll['tab2'].indeterminate}
						onChange={this.onAllCheckChange.bind(this, 'tab2')}
					/>
				</div>
			),
			key: 'checkbox',
			attrcode: 'checkbox',
            dataIndex: 'checkbox',
			className: 'table-checkbox-class',
			visible: true,
			itemtype: 'customer',
			width: 60,
			render: (text, record, index) => {
				let rowChecked =
					checkedArray['tab2'][record.idx] && checkedArray['tab2'][record.idx].checked ? true : false;
				return (
					<div fieldid='firstcol'>
						<Checkbox
							className="table-checkbox"
							checked={rowChecked}
							onChange={this.onCheckboxChange.bind(this, text, record, index, 'tab2')}
						/>
					</div>
				);
			}
		};
		this.setState({ 
			matchupCols: this.state.matchupCols 
		});
	};
	//获取最新status并传递到档案对应关系
	newEditTableCell = (text, record, index, tabType, col) => {
		let { status, docTypeData, orgInfo, showModal } = this.state;
		let businessInfo = getBusinessInfo();
		let fieldid = col.replace('col','item')
		return (
			<EditTableCell
				businessInfo={businessInfo}
				showModal={showModal}
				status={status}
				text={text}
				record={record}
				index={index}
				tabType={tabType}
				type={fieldid}
				docTypeData={docTypeData}
				matchupTableHandle={this.matchupTableHandle}
				col={col}
				orgInfo={orgInfo}
			/>
		);
	};
	//生成模态框(默认)表格数据
	generateModalData = () => {
		let {docTypeData} = this.state
		let line = {};
		line.idx = 1;
		line.key = 1;
		if (docTypeData.length) {
			for (let i = 0; i < docTypeData.length; i++) {
				line[`col${i + 1}`] = '';
				line[`col${i + 1}beanid`] = docTypeData[i].proppk ? docTypeData[i].refClass : docTypeData[i].vopk;
			}
			line.cols = docTypeData.length;
		}
		return [line]
	}
	//显示/隐藏模态框
	handleModal = (val) => {
		let modalData = this.generateModalData()
		this.setState({ showModal: val, modalTableData: modalData });
	};
	//生成新数据
	createNewData = (startIndex) => {
		if(!startIndex) startIndex = 1;
		let modalTableData = this.state.modalTableData.concat([])[0];
		let cols = modalTableData.cols;
		let obj = [];
		for (let i = 1; i <= cols; i++) {
			for (let key in modalTableData) {
				if (key == `newcol${i}`) {
					obj.push(modalTableData[key].split('#$#'));
				}
			}
		}
		let res = cartesianProductOf(...obj);
		let newMatchupData = [];
		for (let i = 0; i < res.length; i++) {
			let newline = { key: i + startIndex, idx: i + startIndex };

			for(let j = 0; j < cols; j++){
				newline[`col${j + 1}beanid`] = modalTableData[`col${j+1}beanid`]; //用作查询参照是的beanid
			}

			for (let j = 0; j < res[i].length; j++) {
				let col = res[i][j].split('#');
				newline[`col${j + 1}`] = col[0];
				newline[`col${j + 1}pk_bd`] = col[1]; //档案对照关系参照本身的refpk 用作保存
			}
			newMatchupData.push(newline);
		}
		return newMatchupData;
	};
	//覆盖生成
	overCreate = () => {
		let newData = this.createNewData();
		this.setState({ matchupTableData: newData, showModal: false });
	};
	// 取pk_bd
	createPKbdArr = (tableData) => {
		let PKbdArr = []
		tableData.map((item)=>{
			let tempKey = ''
			for (const key in item) {
				if(key.endsWith('pk_bd')){
					tempKey = tempKey + item[key]
				}
			}
			PKbdArr.push(tempKey)
		})
		return PKbdArr
	}
	//增量生成
	addCreate = () => {
		let matchupTableData = this.state.matchupTableData.concat([]);
		let startIndex = matchupTableData.length + 1;
		let newData = this.createNewData(startIndex);
		// 去重
		let needAddData = []
		let oldPkbdArr = this.createPKbdArr(matchupTableData)
		newData.map((item)=>{
			let tempKey = ''
			for (const key in item) {
				if(key.endsWith('pk_bd')){
					tempKey = tempKey + item[key]
				}
			}
			if(!oldPkbdArr.includes(tempKey)){
				needAddData.push(item);
			}
		})
		let newMatchupTableData = matchupTableData.concat(needAddData);
		
		this.setState({ matchupTableData: newMatchupTableData, showModal: false });
	};
	//操作tabkey
	handleTabkey = (key) => {
		this.setState({ tabkey: key });
	};
	render() {
		let {
			status,
			pageType,
			orgInfo,
			matchupCols,
			matchupTableData,
			docTypeData,
			bindAccData,
			checkedAll,
			checkedArray,
			accountingBook,
			accountingBookType,
			isAllItems,
			ruleHead,
			showModal,
			modalTableData,
			tabkey,
			appcode,
			pageBtnStatus,
			json,
			bindAccontDisabled 
		} = this.state;
		return (
			<div className="nc-bill-list ccheck-card-page">
				<Header
					json={json}
					NCProps={this.props}
					status={status}
					headerBtnClick={this.headerBtnClick}
					switchPage={this.switchPage}
					browseItem={this.browseItem}
					pageBtnStatus={pageBtnStatus}
				/>
				<Body
					json={json}
					appcode={appcode}
					status={status}
					tabkey={tabkey}
					showModal={showModal}
					pageType={pageType}
					orgInfo={orgInfo}
					matchupCols={matchupCols}
					matchupTableData={matchupTableData}
					modalTableData={modalTableData}
					docTypeData={docTypeData}
					bindAccData={bindAccData}
					checkedAll={checkedAll}
					checkedArray={checkedArray}
					isAllItems={isAllItems}
					ruleHead={ruleHead}
					accountingBook={accountingBook}
					accountingBookType={accountingBookType}
					bindAccontDisabled={bindAccontDisabled}
					docTypeTbaleHandle={this.docTypeTbaleHandle}
					matchupTableHandle={this.matchupTableHandle}
					bindAccDataHandle={this.bindAccDataHandle}
					onAllCheckChange={this.onAllCheckChange}
					onCheckboxChange={this.onCheckboxChange}
					handleAccountingBook={this.handleAccountingBook}
					handleAllItems={this.handleAllItems}
					handleRuleHead={this.handleRuleHead}
					handleModal={this.handleModal}
					overCreate={this.overCreate}
					addCreate={this.addCreate}
					handleTabkey={this.handleTabkey}
					BodyRef={(init) => { this.Body = init }}
				/>
			</div>
		);
	}
}
export default (Card = createPage({})(Card));
