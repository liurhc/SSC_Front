import { ajax } from 'nc-lightapp-front';
import { ORG_LIST, GROUP_LIST } from '../../config/index';
export default function initTemplate(props) {
	ajax({
		url: '/nccloud/platform/appregister/queryallbtns.do',
		data: this.props.pageType == 'org' ? ORG_LIST : GROUP_LIST,
		success: (res) => {
			if (res.success) {
				if (res.data) {
					props.button.setButtons(res.data);
					props.button.setButtonVisible([ 'add', 'update', 'delete', 'switch', 'save', 'cancel' ], false);
				}
			}
		}
	});
}
