import { ajax } from 'nc-lightapp-front';

export const xhr = (url, type) => {
    let promise = new Promise((resolve, reject) => {
        ajax({
            url: url,
            method: type,
            success: function(data) {
                resolve(data);
            },
            error: function(err) {
                reject(err);
            }
        })
    });
    return promise;
}
