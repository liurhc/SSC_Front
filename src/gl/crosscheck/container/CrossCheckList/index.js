import React, { Component } from 'react';
import { ajax, createPage, toast, getBusinessInfo, promptBox, getMultiLang, viewModel } from 'nc-lightapp-front';
import initTemplate from './event/initTemplate';
import Header from './components/header';
import Body from './components/body';
import { linkTo } from '../../util/index';
import { ORG_CARD, GROUP_CARD } from '../config/index';
import {commonApi} from '../../../public/common/actions'

import './index.less';
const { setGlobalStorage, getGlobalStorage, removeGlobalStorage } = viewModel;

class List extends Component {
	constructor(props) {
		super(props);
		// initTemplate.call(this, props);
		this.appcode = this.props.getSearchParam('c');
		this.state = {
			orgInfo: {}, //头部组织信息
			treeSwitch: 1, //1  规则-科目   2  科目-规则
			treeData: [],
			columns: [],
			rules: [],
			currPkRule: '', //当前pk_rule
			rulePks: [], //规则列表
			json: {}
		};
	}

	componentWillMount() {
		let callback= (json) =>{
			this.setState({
				json:json,
			},()=>{		  
				initTemplate.call(this, this.props);
			})
		}
		getMultiLang({moduleId:'20000CRVRL',domainName:'gl',currentLocale:'simpchn',callback});
	}

	componentDidMount() {
		let page = this;
		let check_rule_card = JSON.parse(getGlobalStorage('sessionStorage', 'check_rule_card'))
		let { pageType } = this.props;
		if (check_rule_card) {
			let { orgInfo } = check_rule_card;
			if (orgInfo) {
				this.setState({ orgInfo });
				this.queryTree(orgInfo);
			}
			return;
		}
		if (pageType == 'group') {
			let info = getBusinessInfo();
			if(info){
				let orgInfo = {
					refname: info.groupName,
					refpk: info.groupId
				};
				this.setState({ orgInfo });
				this.queryTree(orgInfo);
			}
		}else{
			//TODO 获取默认组织信息
			commonApi.getContext({appcode:this.appcode}, (res) => {
				if(res.success){
					let data = res.data;
					let orgInfo = {
						refpk:data.pk_org,
						refname:data.org_Name
					}
					this.setState({orgInfo});
					this.queryTree(orgInfo);
				}
			});
		}
	}

	getOrgInfo = () => {
		let {pageType} = this.props;
		if(pageType === 'org'){
			let {orgInfo} = this.state;
			return orgInfo;
		}else{
			let info = getBusinessInfo();
			if(info){
				let orgInfo = {
					refname: info.groupName,
					refpk: info.groupId
				};
				return orgInfo;
			}
		}
	}
	
	buildRulePks = (data) => {
		let rulePks = [];
		for (let i = 0; i < data.length; i++) {
			rulePks.push(data[i].refpk);
		}
		return rulePks;
	};
	//查询树
	queryTree = (orgInfo,callback) => {
		let res1 = false;
		ajax({
			url: '/nccloud/fipub/rulectrl/queryTree.do',
			data: { pk_org: orgInfo.refpk }, //业务单元主键,
			success: (res) => {
				let {success, data} = res
				if (success) {

					let {rulePks, treeData, currPkRule} = this.state
					if (data) {
						rulePks = this.buildRulePks(data.children);
						treeData = data.children
						currPkRule = data.children[0].refpk
						this.queryRule(data.children[0].refpk);
						// this.setState({ rulePks, treeData: data.children, currPkRule: data.children[0].refpk });
						
					} else {
						rulePks = []
						treeData = []
						currPkRule = []
						this.setState({
							columns: [], 
							tableData: []
						})
					}
					if(callback){
						callback()
					}
					this.setState({ rulePks, treeData: treeData, currPkRule: currPkRule });
				}
			}
		});
	};
	//查询规则
	queryRule = (pk_rule) => {
		ajax({
			url: '/nccloud/fipub/rulectrl/queryRuleByPk.do',
			data: { pk_rule },
			success: (res) => {
				if (res.success) {
					if (res.data) {
						let resTableData = res.data.items;
						this.buildTableColumnsAndData(resTableData);
					}
				}
			}
		});
	};
	//构建columns和data
	buildTableColumnsAndData = (resTableData) => {
		if (!resTableData) {
			this.setState({ columns: [], tableData: [] });
			return;
		}
		let columnsData = {},
			oriTableData = [],
			columns = [],
			tableData = [];
		for (let i = 0; i < resTableData.length; i++) {
			if (resTableData[i].styleflag == 0) {
				columnsData = resTableData[i];
			} else {
				oriTableData.push(resTableData[i]);
			}
		}
		//构建columns
		for (let i = 1; i < 10; i++) {
			let item = columnsData[`item${i}`];
			if (item) {
				let title = (<div fieldid={`item${i}`}>{item.propName ? `${item.bdName}\\${item.propName}` : item.bdName}</div>);
				let col = {
					title,
					dataIndex: `col${i}`,
					key: `col${i}`,
					width: 120,
					render: (text, record, index) => <div fieldid={`item${i}`}>{text ? text : <span>&nbsp;</span>}</div>
				};
				columns.push(col);
			}
		}
		//构建tableData
		for (let i = 0; i < oriTableData.length; i++) {
			let row = { key: oriTableData[i].pk_item };
			for (let j = 1; j < 10; j++) {
				if (oriTableData[i][`item${j}`]) {
					row[`col${j}`] =
						oriTableData[i][`item${j}`].bdCode + '\\' + (oriTableData[i][`item${j}`].bdName || '');
				}
			}
			tableData.push(row);
		}
		this.setState({ columns, tableData });
	};
	//组织改变时回调
	handleOrg = (orgInfo) => {
		this.setState({ orgInfo }, () => {
			this.queryTree(orgInfo);
		});
	};
	handleTreeData = () => {
		let { treeData } = this.state;
		let newTreeData = [];
		for (let i = 0; i < treeData.length; i++) {
			if (treeData[i].children) {
				let children = treeData[i].children;
				for (let j = 0; j < children.length; j++) {
					let newChild = {
						isleaf: true,
						children: null,
						parentRefPk: children[j].refpk,
						refpk: treeData[i].refpk,
						refname: treeData[i].refname
					};
					children[j].children = [ newChild ];
					children[j].isleaf = false;
					newTreeData.push(children[j]);
				}
			}
		}
		return newTreeData;
	};
	//切换左树形态
	switchTree = () => {
		let { treeSwitch, treeData } = this.state;
		let newTreeData = [];
		if (treeSwitch == 2) {
			newTreeData = this.handleTreeData();
		} else if (treeSwitch == 1) {
			let tempData = this.handleTreeData();
			let emptObj = {};
			for (let i = 0; i < tempData.length; i++) {
				if (emptObj[tempData[i].refpk]) {
					emptObj[tempData[i].refpk].children.push(...tempData[i].children);
				} else {
					emptObj[tempData[i].refpk] = tempData[i];
				}
			}
			newTreeData = Object.values(emptObj);
		}
		this.setState({ treeData: newTreeData });
	};
	//跳转页面
	switchPage = (status, item) => {
		let { pageType } = this.props;
		let { rulePks } = this.state;
		let orgInfo = this.getOrgInfo();
		if (!Object.keys(orgInfo).length) {
			if(pageType === 'org'){
				toast({ color: 'warning', content: this.state.json['20000CRVRL-000043'] });/* 国际化处理： 请选择组织！*/
			}
			return;
		}
		let currPkRule = '';
		if (status == 'edit') {
			currPkRule = item.refpk;
		}
		let check_rule_card = {
			status,
			pageType,
			rulePks,
			orgInfo,
			currPkRule
		};
		let url = '/gl/crosscheck/pages/card/index.html';
		let params = pageType == 'org' ? ORG_CARD : GROUP_CARD;
		linkTo(this.props, url, params, check_rule_card);
	};
	//头部按钮点击回调
	headerBtnClick = (props, type, item) => {
		switch (type) {
			case 'refresh':
				let orgInfo = this.getOrgInfo();
				let callback = () => {
					toast({ title: this.state.json['20000CRVRL-000045'], color: 'success' });/* 国际化处理： 刷新成功！*/
				}
				this.queryTree(orgInfo,callback);
				break;
			case 'add':
				this.switchPage('add');
				break;
			case 'switch':
				let { treeSwitch } = this.state;
				this.setState({ treeSwitch: treeSwitch == 1 ? 2 : 1 }, () => {
					this.switchTree();
				});
				break;
			case 'update':
				this.switchPage('edit', item);
				break;
			case 'delete':
				this.deleteRule(item.refpk);
				break;
			default:
				break;
		}
	};
	//删除规则
	deleteRule = (pk_rule) => {
		promptBox({
			color:'warning',
			title: this.state.json['20000CRVRL-000026'],/* 国际化处理： 删除*/
			content: this.state.json['20000CRVRL-000027'],  /* 国际化处理： 确定要删除吗？*/
			beSureBtnClick: () => {
				let orgInfo  = this.getOrgInfo();
				ajax({
					url: '/nccloud/fipub/rulectrl/deleteRule.do',
					data: { pk_rule },
					success: (res) => {
						if (res.success) {
							//this.queryRule();
							this.queryTree(orgInfo);
							toast({ color: 'success', content: this.state.json['20000CRVRL-000028'] });/* 国际化处理： 删除成功！*/
						}
					}
				});
			},
			cancelBtnClick: () => {}
		});		
	};
	//点击treeNode
	onSelectTree = (selectedKeys, e) => {
		let { treeSwitch, currPkRule } = this.state;
		let pk_rule = '';
		if (treeSwitch == 1) {
			pk_rule = e.node.props.isLeaf ? e.node.props.parentKey : e.node.props.eventKey;
		} else if (treeSwitch == 2) {
			pk_rule = e.node.props.isLeaf ? e.node.props.eventKey : e.node.props.parentKey;
		}
		if (currPkRule != pk_rule) {
			this.setState({ currPkRule: pk_rule });
			this.queryRule(pk_rule);
		}
	};
	render() {
		let { pageType } = this.props;
		let { treeSwitch, treeData, columns, tableData, currPkRule, rulePks, json } = this.state;
		let orgInfo = this.getOrgInfo();
		return (
			<div className="nc-bill-tree-table ccheck-list-page m-ccheck-card-page">
				<Header
					json={json}
					NCProps={this.props}
					pageType={pageType}
					orgInfo={orgInfo}
					headerBtnClick={this.headerBtnClick}
					handleOrg={this.handleOrg}
					appcode={this.appcode}
				/>
				<Body
					json={json}
					NCProps={this.props}
					treeData={treeData}
					treeSwitch={treeSwitch}
					headerBtnClick={this.headerBtnClick}
					onSelectTree={this.onSelectTree}
					columns={columns}
					tableData={tableData}
					currPkRule={currPkRule}
					rulePks={rulePks}
					orgInfo={orgInfo}
					pageType={pageType}
				/>
			</div>
		);
	}
}

export default (List = createPage({})(List));
