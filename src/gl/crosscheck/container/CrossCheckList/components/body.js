import React, { Component } from 'react';
import { base, createPage } from 'nc-lightapp-front';
import { linkTo } from '../../../util';
const { NCButton: Button, NCTree: Tree, NCIcon: Icon, NCTable: Table, NCDiv } = base;
const TreeNode = Tree.NCTreeNode;

class ListBody extends Component {
	constructor(props) {
		super(props);
		// this.props = props;
		// let { syncTree } = this.props;
		// let { setSyncTreeData } = syncTree;
		// this.setSyncTreeData = setSyncTreeData; //设置树根节点数据方法
		this.state = {
			isHover: ''
		};
	}
	//
	headerBtnClick = (props, type, item) => {
		let {treeData} = this.props	
		if(type==='switch' && treeData.length === 0){
			return false
		} 
		this.props.headerBtnClick(props, type, item);

	};
	//构建树节点
	buildTreeNodes = (data) => {
		if (data && data.length) {
			return data.map((item) => {
				if (item.children) {
					return (
						<TreeNode liAttr={{"fieldid": `${item.refname}_node`}} title={this.renderTreeTitle(item)} key={item.refpk} parentKey={item.parentRefPk}>
							{this.buildTreeNodes(item.children)}
						</TreeNode>
					);
				}
				return (
					<TreeNode
						liAttr={{"fieldid": `${item.refname}_node`}}
						title={this.renderTreeTitle(item)}
						key={item.refpk}
						parentKey={item.parentRefPk}
						isLeaf={item.isleaf}
					/>
				);
			});
		} else {
			return [];
		}
	};
	renderTreeTitle = (item) => {
		let titleIcon = '',
			titleInfo;
		let { treeSwitch } = this.props;

		titleInfo = <span className="title-middle">{item.refname}</span>;

		//编辑图标
		if (this.state.isHover == item.refpk) {
			let treeIcon = (
				<span className='tree-icon'>
					<i
						className="iconfont icon-bianji"
						onClick={(e) => {this.headerBtnClick('', 'update', item)}}
					/>
					<i
						className="iconfont icon-shanchu"
						onClick={(e) => {this.headerBtnClick('', 'delete', item)}}
					/>
					<i
						className="iconfont icon-xx-chakanxiangqing"
						onClick={this.onRowDoubleClick}
					/>
				</span>
			);
			if (treeSwitch == 1 && !item.isleaf) {
				titleIcon = treeIcon;
			} else if (treeSwitch == 2 && item.isleaf) {
				titleIcon = treeIcon;
			}
		}
		return (
			<div className="title-con">
				{titleInfo}
				{titleIcon}
			</div>
		);
	};
	//鼠标进入节点
	onMouseEnter = (e) => {
		this.setState({ isHover: e.node.props.eventKey });
	};
	//鼠标离开节点
	onMouseLeave = (e) => {
		this.setState({ isHover: '' });
	};
	//点击treeNode
	onSelectTree = (selectedKeys, e) => {
		this.props.onSelectTree(selectedKeys, e);
	};
	//双击表格行
	onRowDoubleClick = (record, index, event) => {
		let { currPkRule, orgInfo, pageType, NCProps, rulePks } = this.props;
		let check_rule_card = {
			status: 'browse',
			currPkRule,
			pageType,
			rulePks
		};
		let pagecode = this.props.getSearchParam('p');
		let appcode = this.props.getSearchParam('c');
		
		// pageType == 'org' && (check_rule_card.orgInfo = orgInfo);
		check_rule_card.orgInfo = orgInfo
		let url = '/gl/crosscheck/pages/card/index.html';
		let params = { pagecode: pagecode, appcode: appcode };
		linkTo(NCProps, url, params, check_rule_card);
	};
	clickEditIconEve(data) {

	}
	clickDelIconEve(data) {

	}
	onSelectEve(data, item, isChange) {
		
	}

	// componentWillReceiveProps(nextProps) {
	// 	if (this.props.treeData !== nextProps.treeData){
	// 		this.setSyncTreeData('tree1', nextProps.treeData);
	// 	}
	// }
	render() {
		let { NCProps, treeSwitch, treeData, columns, tableData, pageType, orgInfo,syncTree } = this.props;
		let { createSyncTree } = syncTree;
		let addBtnStatus = pageType==='org' && !orgInfo.refpk ? true : false
		let treeNodes = this.buildTreeNodes(treeData);
		let { DragWidthCom } = NCProps;
		let iWidth = window.innerWidth
		let iHeight = window.innerHeight
		let iconStyle = {}
		let aStyle = {}
		// let aDisabled = ''
		if(treeData.length===0){
			// aDisabled = 'javascript:return false;'
			aStyle = {cursor: 'not-allowed'}
			iconStyle = {opacity: 0.2}
		}
		return (
			// <div className="ccheck-page-body">
				<div className="tree-table">
					<DragWidthCom
						leftDom={
							<NCDiv fieldid="crosscheck" areaCode={NCDiv.config.TREE} className="tree-area body-left-tree">
								<div className="btns-area">
									<Button
										fieldid='add'
										className="add-rule-btn button-primary"
										disabled={addBtnStatus}
										onClick={this.headerBtnClick.bind(this, '', 'add')}
									>
										<i className='iconfont icon-tianjiayingyong' />
										{this.props.json['20000CRVRL-000037']}{/* 国际化处理： 新增*/}
									</Button>
									{treeSwitch == 1 ? (
										<a href='#' style={aStyle} className='a-icon'>
											<i
												style={iconStyle}
												className="tree-list-menu iconfont icon-guize"
												onClick={this.headerBtnClick.bind(this, '', 'switch')}
											/>
										</a>
									) : (
										<a href='#' style={aStyle} className='a-icon'>
											<i
												style={iconStyle}
												className="tree-list-menu iconfont icon-kemu"
												onClick={this.headerBtnClick.bind(this, '', 'switch')}
											/>
										</a>
									)}
								</div>
								{!treeNodes || !treeNodes.length ? (
										<div className="mark nc-theme-area-bgc nc-theme-common-font-c nc-theme-area-split-bc">
											<p>{this.props.json['20000CRVRL-000038']}、{this.props.json['20000CRVRL-000039']}，{this.props.json['20000CRVRL-000040']}！</p>
											{/* 国际化处理： 输入来源组织,目的组织信息后,可新增类型*/}
										</div>
									) : (
										''
									)}
								{/* {createSyncTree({
									treeId: 'tree1',
									showLine: true,
									showModal: false, //是否使用弹出式编辑
									needSearch: false, //是否需要查询框，默认为true,显示。false: 不显示
									clickEditIconEve: this.clickEditIconEve.bind(this),
									onSelectEve: this.onSelectEve.bind(this), //选择节点回调方法
									// clickAddIconEve: this.clickAddIconEve.bind(this), //增加节点
									clickDelIconEve: this.clickDelIconEve.bind(this)
								})} */}
								<NCDiv fieldid="crosscheck" areaCode={NCDiv.config.TreeCom}>
									<Tree
										showLine={true}
										onSelect={this.onSelectTree}
										onMouseLeave={this.onMouseLeave}
										onMouseEnter={this.onMouseEnter}
									>
										{this.buildTreeNodes(treeData)}
									</Tree>
								</NCDiv>
							</NCDiv>
						} //左侧区域dom
						rightDom={
							<NCDiv fieldid='crosscheck' areaCode={NCDiv.config.TableCom} className="table-area">
								<Table 
									columns={columns} 
									data={tableData} 
									onRowDoubleClick={this.onRowDoubleClick}
									adaptionHeight={true}
									// scroll={{ x: iWidth-160, y: iHeight-100 }} 
								/>
							</NCDiv>
						} //右侧区域dom
						defLeftWid="20%" // 默认左侧区域宽度，px/百分百
					/>
				</div>
			// </div>
		);
	}
}


ListBody = createPage({
	// initTemplate: initTemplate,
	// mutiLangCode: '2002'
})(ListBody)
export default ListBody
