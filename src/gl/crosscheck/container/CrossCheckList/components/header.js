import React, { Component } from 'react';
import { ajax, base, high, createPage, createPageIcon } from 'nc-lightapp-front';
import BusinessUnitTreeRef from '../../../../../uapbd/refer/org/BusinessUnitTreeRef';
import HeaderArea from '../../../../public/components/HeaderArea';
class ListHeader extends Component {
	constructor(props) {
		super(props);
	}
	headerBtnClick = (props, type) => {
		this.props.headerBtnClick(props, type);
	};
	render() {
		let { pageType, orgInfo, NCProps, appcode } = this.props;
		let pageTitle = '';
		if (pageType == 'org') {
			pageTitle = this.props.json['20000CRVRL-000041'];/* 国际化处理： 科目交叉校验规则-组织*/
		} else if (pageType == 'group') {
			pageTitle = this.props.json['20000CRVRL-000042'];/* 国际化处理： 科目交叉校验规则-集团*/
		}
		return (
			<div className="ccheck-page-header">
				<HeaderArea 
					title = {pageTitle}
					searchContent = {
						<div className="title">
							{pageType == 'org' ? (
								<div className='title-search'>
									<i className='require-true'>*</i>
									<div className="org-refer">
										<BusinessUnitTreeRef
											fieldid='unit'
											value={{ refpk: orgInfo.refpk, refname: orgInfo.refname }}
											queryCondition={{
												funcode: appcode,
												TreeRefActionExt: 'nccloud.web.fipub.ref.BusinessUnitSqlBuilder'
											}}
											onChange={(val) => {
												this.props.handleOrg(val);
											}}
										/>
									</div>
								</div>
							) : (
								''
							)}
						</div>
					}
					btnContent = {NCProps.button.createButtonApp({
						area: 'page_header',
						onButtonClick: this.headerBtnClick
					})}
				/>
			</div>
		);
	}
}

ListHeader = createPage({})(ListHeader)
export default ListHeader
