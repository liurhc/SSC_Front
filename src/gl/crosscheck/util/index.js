import { viewModel } from 'nc-lightapp-front';
const { setGlobalStorage} = viewModel;

export function linkTo(NCProps, url, params, sessionData) {
	setGlobalStorage('sessionStorage', 'check_rule_card', JSON.stringify(sessionData), ()=>{});
	NCProps.linkTo(url, params);
}
export function cartesianProductOf() {
	return Array.prototype.reduce.call(
		arguments,
		(total, currItem) => {
			let res = [];
			total.map((tol) => {
				currItem.map((curr) => {
					res.push(tol.concat([ curr ]));
				});
			});
			return res;
		},
		[ [] ]
	);
}
