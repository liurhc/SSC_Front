//主子表卡片
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast } from 'nc-lightapp-front';
let { NCFormControl } = base;
import { initTemplate } from './events';
import './index.less';
class Card extends Component {
	constructor(props) {
		super(props);
		this.moduleId = '2002';
		this.tableId = 'table';
		this.pageid = '20022022_linksubjdataoccur_list'
	}
	componentDidMount() {
		this.toggleShow();
	}

	//切换页面状态
	toggleShow = () => {
		let status = this.props.getUrlParam('status');
		let flag = status === 'browse' ? false : true;
		this.props.button.setButtonVisible([ 'return','refresh'], flag);
		this.props.button.setButtonVisible([ 'return', 'refresh' ], !flag);
		this.props.form.setFormStatus(this.formId, status);
		this.props.cardTable.setStatus(this.tableId, status);
	};

	getButtonNames = (codeId) => {
		if (codeId === 'edit' || codeId === 'add' || codeId === 'save') {
			return 'main-button';
		} else {
			return 'secondary - button';
		}
	};

	//获取列表肩部信息
	getTableHead = (buttons) => {
		let { createButton } = this.props.button;
		return (
			<div className="shoulder-definition-area">
				<div className="definition-search">
				</div>
				<div className="definition-icons">
					{this.props.cardTable.createBrowseIcons(this.tableId, {
						iconArr: [ 'close', 'open', 'max' ],
						maxDestAreaId: 'finance-reva-revecontract-card'
					})}
					{buttons.map((v) => {
						if (v.btncode == 'add') {
							return createButton(v.btncode, {
								name: v.btnname,
								buttonColor: this.getButtonNames(v.btncode)
							});
						}
					})}
				</div>
			</div>
		);
	};
	render() {
		let { cardTable, form, button, modal } = this.props;
		let buttons = this.props.button.getButtons();
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createButton } = button;
		let { createModal } = modal;
		return (
			<div className="nc-bill-card">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area"><h2 className='title-search-detail'></h2></div>
					<div className="header-button-area">
						{this.props.button.createButtonApp({
						area: 'linksubjdata',
						buttonLimit: 10, 
						popContainer: document.querySelector('.header-button-area')
						})}
                    </div>
				</div>
				<div className="nc-bill-form-area">
					{
					}
				</div>
				{
					<div className="nc-bill-table-area">
						{this.getTableHead(buttons)}
						{createCardTable(this.tableId, {
                        adaptionHeight:true,
							fieldid: 'LinkSubjdataOccur',
							tableHead: this.getTableHead.bind(this, buttons),
							//showCheck: true,
							showIndex: true,
							hideSwitch:()=>{return false;}
						})}
					</div>
				}

				{createModal('delete', {
					//title: multiLang && multiLang.get('20521030-0020'),
					//content: multiLang && multiLang.get('20521030-0006'),
					beSureBtnClick: this.delConfirm
				})}
			</div>
		);
	}
}

Card = createPage({
	initTemplate: initTemplate,
	//mutiLangCode: '2052'
})(Card);

ReactDOM.render(<Card />, document.querySelector('#app'));
