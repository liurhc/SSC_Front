import { base, ajax,cacheTools } from 'nc-lightapp-front';
let { NCPopconfirm } = base;
let tableId = 'table';
let pageId = '20022022_linksubjdataoccur_list';
export default function(props) {
		let appcode = props.getUrlParam('c')||props.getSearchParam('c');
		props.createUIDom(
		{
			pagecode: pageId,
			appcode: appcode
		}, 
		function (data){
			if(data){
				if(data.template){
					let meta = data.template;
					modifierMeta(props, meta)
					props.meta.setMeta(meta);
				}
				if(data.button){
					let button = data.button;
					props.button.setButtons(button);
				}
				if (props.getUrlParam('status') == 'browse') {
					let data = {
						pk_contrastrule: props.getUrlParam('pk_contrastrule'),
						year: props.getUrlParam('year'),
						period: props.getUrlParam('period'),
						pk_accountingbook: props.getUrlParam('pk_accountingbook'),
						pk_oppaccountingbook: props.getUrlParam('pk_oppaccountingbook'),
						pk_currtype: props.getUrlParam('pk_currtype'),
						pk_periodschema: props.getUrlParam('pk_periodschema'),
						contrastmoneytype: props.getUrlParam('contrastmoneytype'),
						allsubj: props.getUrlParam('allsubj'),
						pagecode: pageId 
					};
					ajax({
						url: '/nccloud/gl/corpcontrastresult/sumcontrastquerylinksubjdata.do',
						data: data,
						success: (res) => {
							if (res.data) {
								props.cardTable.setTableData(tableId, res.data[tableId]);
								//字段控制
								setQueryContent(props,cacheTools.get('corpcontrastcontent'));
							} else {
								props.cardTable.setTableData(tableId, { rows: [] });
							}
						}
					});
				}
			}   
		}
	)
}

function modifierMeta(props, meta) {
	let status = props.getUrlParam('status');
	meta[tableId].status = status;
	
	meta[tableId].items.forEach((item, key) => {
		return item;
		});
	return meta;
}
//设置隐藏列
function setQueryContent(props,content){
	let tableid = 'table';
	if(content[0]==='N')
	{
		props.cardTable.hideColByKey(tableid,['debit_occur_quantity','debit_accum_quantity','credit_occur_quantity','credit_accum_quantity']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['debit_occur_quantity','debit_accum_quantity','credit_occur_quantity','credit_accum_quantity']);
	}
	if(content[1]==='N')
	{
		props.cardTable.hideColByKey(tableid,['debit_occur_cur','debit_accum_cur','credit_occur_cur','credit_accum_cur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['debit_occur_cur','debit_accum_cur','credit_occur_cur','credit_accum_cur']);
	}
	if(content[2]==='N')
	{
		props.cardTable.hideColByKey(tableid,['debit_occur_orgcur','debit_accum_orgcur','credit_occur_orgcur','credit_accum_orgcur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['debit_occur_orgcur','debit_accum_orgcur','credit_occur_orgcur','credit_accum_orgcur']);
	}
	if(content[3]==='N')
	{
		props.cardTable.hideColByKey(tableid,['debit_occur_groupcur','debit_accum_groupcur','credit_occur_groupcur','credit_accum_groupcur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['debit_occur_groupcur','debit_accum_groupcur','credit_occur_groupcur','credit_accum_groupcur']);
	}
	if(content[4]==='N')
	{
		props.cardTable.hideColByKey(tableid,['debit_occur_globalcur','debit_accum_globalcur','credit_occur_globalcur','credit_accum_globalcur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['debit_occur_globalcur','debit_accum_globalcur','credit_occur_globalcur','credit_accum_globalcur']);
	}
}
