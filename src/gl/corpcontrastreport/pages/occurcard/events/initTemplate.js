import { base, ajax,cacheTools } from 'nc-lightapp-front';
let { NCPopconfirm } = base;
const formId = 'form';
const tableId = 'table';
const pageId = '20022022_web_occur_card';
import tableButtonClick from './tableButtonClick';
export default function(props) {
	let page = this;
	let appcode = props.getUrlParam('c')||props.getSearchParam('c');
	props.createUIDom(
		{
			pagecode: pageId,
			appcode:appcode
		}, 
		function (data){
			if(data){
				if(data.template){
					let meta = data.template;
					modifierMeta(props, meta, page)
					props.meta.setMeta(meta);
				}
				if(data.button){
					let button = data.button;
					props.button.setButtons(button);
				}
				if (props.getUrlParam('status') == 'browse') {
				let data = {
					pk_contrastrule: props.getUrlParam('pk_contrastrule'),
					year: props.getUrlParam('year'),
					month: props.getUrlParam('month'),
					reportstatus: props.getUrlParam('reportstatus'),
					pk_periodschema: props.getUrlParam('pk_periodschema'),
					pagecode: pageId
				};
				ajax({
					url: '/nccloud/gl/corpcontrastresult/sumcontrastoccurdetailcard.do',
					data: data,
					success: (res) => {
						if (res.data) {
							if (res.data.head) {
								props.form.setAllFormValue({ [formId]: res.data.head[formId] });
								//按钮控制
								if(props.form.getFormItemsValue(formId,'booktype.name').display==null)
								{
									props.button.setButtonVisible(['linkreport'], false);
									props.button.setButtonVisible(['linkresult'], true);
								}
								else
								{
									props.button.setButtonVisible(['linkreport'], true);
									props.button.setButtonVisible(['linkresult'], false);
								}
							}
							if (res.data.body) {
								props.cardTable.setTableData(tableId, res.data.body[tableId]);
								//字段控制
								setQueryContent(props,cacheTools.get('corpcontrastcontent'));
							}
						} else {
							props.form.setAllFormValue({ [formId]: { rows: [] } });
							props.cardTable.setTableData(tableId, { rows: [] });
						}
					}
				});
			}

			}   
		}
	)
}

function getListButtons (text, record, index, page, props) {
	let buttonArray = ['linksubjrowdata', 'linkallsubjdata'];
	if(props.form.getFormItemsValue(formId,'booktype.name').display==null) {
		buttonArray.push('linkresult');
	} else {
		buttonArray.push('linkreport');
	}
	return buttonArray;

}

function modifierMeta(props, meta, page) {
	let status = props.getUrlParam('status');
	meta[formId].status = status;
	meta[tableId].status = status;

	meta[tableId].items.forEach((item, key) => {
		return item;
		});
	meta[tableId].items.push({
		attrcode: 'opr',
		label: page.state.json['20022022-000006'],/* 国际化处理： 操作*/
		fixed: 'right',
		itemtype: 'customer', 
		visible: true,
		className: 'table-opr',
		width:'255px',
		render: (text, record, index) => {
            return props.button.createOprationButton(getListButtons(text, record, index, page, props), {
                area: "corpcontrastreport_occur_col",
                buttonLimit: 3,
                onButtonClick: (props, key) => tableButtonClick(props, key, text, record, index,tableId, page)
            });
        }
	});
	let multiLang = props.MutiInit.getIntl('2052');
	return meta;
}
//设置隐藏列
function setQueryContent(props,content){
	let tableid = 'table';
	if(content[0]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_occur_quantity','self_accum_quantity','opp_occur_quantity','opp_accum_quantity','bal_occur_quantity','bal_accum_quantity']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_occur_quantity','self_accum_quantity','opp_occur_quantity','opp_accum_quantity','bal_occur_quantity','bal_accum_quantity']);
	}
	if(content[1]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_occur_cur','self_accum_cur','opp_occur_cur','opp_accum_cur','bal_occur_cur','bal_accum_cur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_occur_cur','self_accum_cur','opp_occur_cur','opp_accum_cur','bal_occur_cur','bal_accum_cur']);
	}
	if(content[2]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_occur_orgcur','self_accum_orgcur','opp_occur_orgcur','opp_accum_orgcur','bal_occur_orgcur','bal_accum_orgcur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_occur_orgcur','self_accum_orgcur','opp_occur_orgcur','opp_accum_orgcur','bal_occur_orgcur','bal_accum_orgcur']);
	}
	if(content[3]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_occur_groupcur','self_accum_groupcur','opp_occur_groupcur','opp_accum_groupcur','bal_occur_groupcur','bal_accum_groupcur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_occur_groupcur','self_accum_groupcur','opp_occur_groupcur','opp_accum_groupcur','bal_occur_groupcur','bal_accum_groupcur']);
	}
	if(content[4]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_occur_globalcur','self_accum_globalcur','opp_occur_globalcur','opp_accum_globalcur','bal_occur_globalcur','bal_accum_globalcur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_occur_globalcur','self_accum_globalcur','opp_occur_globalcur','opp_accum_globalcur','bal_occur_globalcur','bal_accum_globalcur']);
	}
}
