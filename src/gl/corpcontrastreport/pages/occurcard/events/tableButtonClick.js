import { ajax, toast, base, cardCache, getMultiLang, cacheTools } from 'nc-lightapp-front';
let { NCMessage } = base;
let { setDefData, getDefData } = cardCache;
import { tableId } from '../config.js';
const tableButtonClick = (props, key, text, record, index, tableId, page) => {
    let data;
    switch (key) {
        // 表格操作按钮
    case 'linkreport':
      if (record) {
        let data = {
          pagecode:'20022022_linkreportcard',
          status: 'browse',
          pk_contrastrule: page.props.getUrlParam('pk_contrastrule'),
          year: page.props.getUrlParam('year'),
          period: page.props.getUrlParam('month'),
          reportstatus: page.props.getUrlParam('reportstatus'),
          pk_periodschema: page.props.getUrlParam('pk_periodschema'),
          pk_accountingbook: record.values.pk_self.display,
          pk_oppaccountingbook: record.values.pk_opp.display,
          pk_currtype: record.values.pk_currtype.value,
          contrastmoneytype: 'N'
        }
        props.openTo('/gl/corpcontrastreport/pages/linkreportcard/index.html', data);
      }
      else
      {
        toast({color: 'warning', content: page.state.json['20022022-000000']});/* 国际化处理： 请选择一行数据*/
      }
      break
    case 'linksubjrowdata':
      if (record) {
        let data = {
          pagecode:'20022022_linksubjdataoccur_list',
          status: 'browse',
          pk_contrastrule: page.props.getUrlParam('pk_contrastrule'),
          year: page.props.getUrlParam('year'),
          period: page.props.getUrlParam('month'),
          reportstatus: page.props.getUrlParam('reportstatus'),
          pk_periodschema: page.props.getUrlParam('pk_periodschema'),
          pk_accountingbook: record.values.pk_self.display,
          pk_oppaccountingbook: record.values.pk_opp.display,
          //用传递主键
          pk_currtype: record.values.pk_currtype.value,
          contrastmoneytype: 'N',
          allsubj: 'N'
        }
        props.openTo('/gl/corpcontrastreport/pages/linksubjdataoccur/index.html', data);
      }
      else
      {
        toast({color: 'warning', content: page.state.json['20022022-000000']});/* 国际化处理： 请选择一行数据*/
      }
      break
    case 'linkallsubjdata':
      if (record) {
        let data = {
          pagecode:'20022022_linksubjdataoccur_list',
          status: 'browse',
          pk_contrastrule: page.props.getUrlParam('pk_contrastrule'),
          year: page.props.getUrlParam('year'),
          period: page.props.getUrlParam('month'),
          reportstatus: page.props.getUrlParam('reportstatus'),
          pk_periodschema: page.props.getUrlParam('pk_periodschema'),
          pk_accountingbook: record.values.pk_self.display,
          pk_oppaccountingbook: record.values.pk_opp.display,
          pk_currtype: record.values.pk_currtype.value,
          contrastmoneytype: 'N',
          allsubj: 'Y'
        }
        props.openTo('/gl/corpcontrastreport/pages/linksubjdataoccur/index.html', data);
      }
      else
      {
        toast({color: 'warning', content: page.state.json['20022022-000000']});/* 国际化处理： 请选择一行数据*/
      }
      break
    case 'linkresult':
      if (record) {
        let data = {
          appcode:'20022035',
          pagecode:'20022035_web_list',
          jumpflag:'false',
          status: 'browse',
          pk_contrastrule: page.props.getUrlParam('pk_contrastrule'),
          year: page.props.getUrlParam('year'),
          month: page.props.getUrlParam('month'),
          pk_periodschema: page.props.getUrlParam('pk_periodschema'),
          pk_self: record.values.pk_self.display,
          pk_opp: record.values.pk_opp.display,
          //把对账规则传过去做字段过滤
          content:cacheTools.get('corpcontrastcontent')
        }
        props.openTo('/gl/contrastreportresult/pages/list/index.html', data);
      }
      else
      {
        toast({color: 'warning', content: page.state.json['20022022-000000']});/* 国际化处理： 请选择一行数据*/
      }
      break
        default:
            break;
    }
};
export default tableButtonClick;
