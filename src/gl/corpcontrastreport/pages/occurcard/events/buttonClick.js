import { ajax, base, toast,cacheTools } from 'nc-lightapp-front';
export default function(props, id) {
  let rows = props.cardTable.getClickRowIndex(this.tableId);
  switch (id) {
    case 'back':
      props.linkTo('/gl/corpcontrastreport/pages/list/index.html',
    {
      pagecode:'20022022_web_list',
      jumpflag:true
    }
    )
      break
    case 'refresh':
      let data = {
				pk_contrastrule: this.props.getUrlParam('pk_contrastrule'),
				year: this.props.getUrlParam('year'),
				month: this.props.getUrlParam('month'),
				reportstatus: this.props.getUrlParam('reportstatus'),
				pk_periodschema: this.props.getUrlParam('pk_periodschema'),
				pagecode: this.pageId
			};
      ajax({
        url: '/nccloud/gl/corpcontrastresult/sumcontrastoccurdetailcard.do',
        data: data,
        success: (res) => {
          if (res.data) {
            toast({ color: 'success', title:this.state.json['20022022-000004']});/* 国际化处理：刷新成功！*/
            if (res.data.head) {
              this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
            }
            if (res.data.body) {
              this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
              //字段控制
							setQueryContent(this.props,cacheTools.get('corpcontrastcontent'));
            }
          } else {
            this.props.form.setAllFormValue({ [this.formId]: { rows: [] } });
            this.props.cardTable.setTableData(this.tableId, { rows: [] });
          }
        }
      });
      break
    case 'linkreport':
      if (rows) {
        let record = rows['record'].values;
        let data = {
          pagecode:'20022022_linkreportcard',
          status: 'browse',
          pk_contrastrule: this.props.getUrlParam('pk_contrastrule'),
          year: this.props.getUrlParam('year'),
          period: this.props.getUrlParam('month'),
          reportstatus: this.props.getUrlParam('reportstatus'),
          pk_periodschema: this.props.getUrlParam('pk_periodschema'),
          pk_accountingbook: record.pk_self.display,
          pk_oppaccountingbook: record.pk_opp.display,
          pk_currtype: record.pk_currtype.value,
          contrastmoneytype: 'N'
        }
        props.openTo('/gl/corpcontrastreport/pages/linkreportcard/index.html', data);
      }
      else
      {
        toast({color: 'warning', content: this.state.json['20022022-000000']});/* 国际化处理： 请选择一行数据*/
      }
      break
    case 'linksubjrowdata':
      if (rows) {
        let record = rows['record'].values;
        let data = {
          pagecode:'20022022_linksubjdataoccur_list',
          status: 'browse',
          pk_contrastrule: this.props.getUrlParam('pk_contrastrule'),
          year: this.props.getUrlParam('year'),
          period: this.props.getUrlParam('month'),
          reportstatus: this.props.getUrlParam('reportstatus'),
          pk_periodschema: this.props.getUrlParam('pk_periodschema'),
          pk_accountingbook: record.pk_self.display,
          pk_oppaccountingbook: record.pk_opp.display,
          //用传递主键
          pk_currtype: record.pk_currtype.value,
          contrastmoneytype: 'N',
          allsubj: 'N'
        }
        props.openTo('/gl/corpcontrastreport/pages/linksubjdataoccur/index.html', data);
      }
      else
      {
        toast({color: 'warning', content: this.state.json['20022022-000000']});/* 国际化处理： 请选择一行数据*/
      }
      break
    case 'linkallsubjdata':
      if (rows) {
        let record = rows['record'].values;
        let data = {
          pagecode:'20022022_linksubjdataoccur_list',
          status: 'browse',
          pk_contrastrule: this.props.getUrlParam('pk_contrastrule'),
          year: this.props.getUrlParam('year'),
          period: this.props.getUrlParam('month'),
          reportstatus: this.props.getUrlParam('reportstatus'),
          pk_periodschema: this.props.getUrlParam('pk_periodschema'),
          pk_accountingbook: record.pk_self.display,
          pk_oppaccountingbook: record.pk_opp.display,
          pk_currtype: record.pk_currtype.value,
          contrastmoneytype: 'N',
          allsubj: 'Y'
        }
        props.openTo('/gl/corpcontrastreport/pages/linksubjdataoccur/index.html', data);
      }
      else
      {
        toast({color: 'warning', content: this.state.json['20022022-000000']});/* 国际化处理： 请选择一行数据*/
      }
      break
    case 'linkresult':
      if (rows) {
        let record = rows['record'].values;
        let data = {
          appcode:'20022035',
          pagecode:'20022035_web_list',
          jumpflag:'false',
          status: 'browse',
          pk_contrastrule: this.props.getUrlParam('pk_contrastrule'),
          year: this.props.getUrlParam('year'),
          month: this.props.getUrlParam('month'),
          pk_periodschema: this.props.getUrlParam('pk_periodschema'),
          pk_self: record.pk_self.display,
          pk_opp: record.pk_opp.display,
          //把对账规则传过去做字段过滤
          content:cacheTools.get('corpcontrastcontent')
        }
        props.openTo('/gl/contrastreportresult/pages/list/index.html', data);
      }
      else
      {
        toast({color: 'warning', content: this.state.json['20022022-000000']});/* 国际化处理： 请选择一行数据*/
      }
      break
    default:
      break
  }
}
//设置隐藏列
function setQueryContent(props,content){
	let tableid = 'table';
	if(content[0]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_occur_quantity','self_accum_quantity','opp_occur_quantity','opp_accum_quantity','bal_occur_quantity','bal_accum_quantity']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_occur_quantity','self_accum_quantity','opp_occur_quantity','opp_accum_quantity','bal_occur_quantity','bal_accum_quantity']);
	}
	if(content[1]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_occur_cur','self_accum_cur','opp_occur_cur','opp_accum_cur','bal_occur_cur','bal_accum_cur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_occur_cur','self_accum_cur','opp_occur_cur','opp_accum_cur','bal_occur_cur','bal_accum_cur']);
	}
	if(content[2]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_occur_orgcur','self_accum_orgcur','opp_occur_orgcur','opp_accum_orgcur','bal_occur_orgcur','bal_accum_orgcur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_occur_orgcur','self_accum_orgcur','opp_occur_orgcur','opp_accum_orgcur','bal_occur_orgcur','bal_accum_orgcur']);
	}
	if(content[3]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_occur_groupcur','self_accum_groupcur','opp_occur_groupcur','opp_accum_groupcur','bal_occur_groupcur','bal_accum_groupcur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_occur_groupcur','self_accum_groupcur','opp_occur_groupcur','opp_accum_groupcur','bal_occur_groupcur','bal_accum_groupcur']);
	}
	if(content[4]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_occur_globalcur','self_accum_globalcur','opp_occur_globalcur','opp_accum_globalcur','bal_occur_globalcur','bal_accum_globalcur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_occur_globalcur','self_accum_globalcur','opp_occur_globalcur','opp_accum_globalcur','bal_occur_globalcur','bal_accum_globalcur']);
	}
}
