//主子表卡片
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast ,getMultiLang,createPageIcon} from 'nc-lightapp-front';
let { NCFormControl } = base;
import { buttonClick, initTemplate } from './events';
import './index.less';
class Card extends Component {
	constructor(props) {
		super(props);
		this.formId = 'form';
		this.moduleId = '2002';
		this.tableId = 'table';
		this.pageId='20022022_web_occur_card';
		this.back=this.back.bind(this,props);
		this.state={
			json:{}
		}
	}
	componentDidMount() {
		this.toggleShow();
	}
	componentWillMount(){
		let callback= (json) =>{
			this.setState({json:json},()=>{
				initTemplate.call(this, this.props);
			})
		}
        getMultiLang({moduleId:'20022018',domainName:'gl',currentLocale:'zh-CN',callback});
	}
	//切换页面状态
	toggleShow = () => {
		let status = this.props.getUrlParam('status');
		let flag = status === 'browse' ? false : true;
		this.props.button.setButtonVisible([ 'return','refresh'], flag);
		this.props.button.setButtonVisible([ 'return', 'refresh' ], !flag);
		this.props.form.setFormStatus(this.formId, status);
		this.props.cardTable.setStatus(this.tableId, status);
	};

	getButtonNames = (codeId) => {
		if (codeId === 'edit' || codeId === 'add' || codeId === 'save') {
			return 'main-button';
		} else {
			return 'secondary - button';
		}
	};

	//获取列表肩部信息
	getTableHead = (buttons) => {
		let { createButton } = this.props.button;
		return (
			<div className="shoulder-definition-area">
				<div className="definition-search">
				</div>
				<div className="definition-icons">
					{this.props.cardTable.createBrowseIcons(this.tableId, {
						iconArr: [ 'close', 'open', 'max' ],
						maxDestAreaId: 'finance-reva-revecontract-card'
					})}
					{buttons.map((v) => {
						if (v.btncode == 'add') {
							return createButton(v.btncode, {
								name: v.btnname,
								onButtonClick: buttonClick.bind(this),
								buttonColor: this.getButtonNames(v.btncode)
							});
						}
					})}
				</div>
			</div>
		);
	};

	back=(props)=>{
		props.linkTo('/gl/corpcontrastreport/pages/list/index.html',{pagecode:'20022022_web_list',jumpflag: true})
	}

	render() {
		let { cardTable, form, button, modal } = this.props;
		let buttons = this.props.button.getButtons();
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createButton } = button;
		let { createModal } = modal;
		return (
			<div className="nc-bill-card">
				<div className="nc-bill-header-area">
					<i className="back-btn iconfont icon-fanhuishangyiji" onClick={this.back} />
					<div className="header-title-search-area">{createPageIcon()}<h2 className='title-search-detail'>{this.state.json['20022022-000001']}</h2></div>{/* 国际化处理： 总额对账查询*/}
					<div className="header-button-area">
						{this.props.button.createButtonApp({
						area: 'corpcontrastreport_occur_card',
						buttonLimit: 10, 
						onButtonClick: buttonClick.bind(this), 
						popContainer: document.querySelector('.header-button-area')
						})}
                    </div>
				</div>
				<div className="nc-bill-form-area">
					{createForm(this.formId, {
						fieldid:'OccurCard'
					})}
				</div>
				{
					<div className="nc-bill-table-area">
						{this.getTableHead(buttons)}
						{createCardTable(this.tableId, {
                        adaptionHeight:true,
							fieldid:'OccurCard',
							tableHead: this.getTableHead.bind(this, buttons),
							//showCheck: true,
							showIndex: true,
							hideSwitch:()=>{return false;}
						})}
					</div>
				}

				{createModal('delete', {
					//title: multiLang && multiLang.get('20521030-0020'),
					//content: multiLang && multiLang.get('20521030-0006'),
					beSureBtnClick: this.delConfirm
				})}
			</div>
		);
	}
}

Card = createPage({
	//mutiLangCode: '2052'
})(Card);

ReactDOM.render(<Card />, document.querySelector('#app'));
