//主子表列表

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base ,getMultiLang,createPageIcon} from 'nc-lightapp-front';
const { NCTabsControl,NCAffix } = base;
import { buttonClick, initTemplate, searchBtnClick, onAfterEvent } from './events';
import HeaderArea from '../../../public/components/HeaderArea';
import './index.less';
import '../../../public/reportcss/firstpage.less';
class List extends Component {
	constructor(props) {
		super(props);
		this.moduleId = '2002';
		this.searchId = '20022022_query';
		this.tableId = '20022022_list';
		this.state = {
			querydata : {},
			json:{},
			inlt:null
		};
	}

	componentDidMount() {
	}
	componentWillMount(){
		let callback= (json,status,inlt) =>{
			this.setState({json:json,inlt},()=>{
			})
		}
        getMultiLang({moduleId:'20022018',domainName:'gl',currentLocale:'zh-CN',callback});
	}
	onRowDoubleClick = (record,index)=>{
		let url = '';
		let pagecode ='';
		if (record.occurDetailVOs.value && record.occurDetailVOs.value.length > 0) {
			pagecode = '20022022_web_occur_card';
			url = '/gl/corpcontrastreport/pages/occurcard/index.html'
		}
		if (record.balanceDetailVOs.value && record.balanceDetailVOs.value.length > 0) {
			pagecode = '20022022_web_balance_card';
			url = '/gl/corpcontrastreport/pages/balancecard/index.html'
		}
		this.props.linkTo(url, {
			pagecode:pagecode,
			status: 'browse',
			pk_contrastrule: record.pk_contrastrule.value,
			year: record.year.value,
			month: record.month.value,
			reportstatus: record.reportstatus.value,
			pk_periodschema: record.pk_resultlisttabvo.value
		})
	};

	render() {
		let { table, button, search } = this.props;
		let buttons = this.props.button.getButtons();
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { createButton, getButtons } = button;
		return (
			<div className="nc-bill-list" id="nc-bill-list-nc">
				<HeaderArea
					title={this.state.json['20022022-000001']}/* 国际化处理： 总额对账查询*/
					btnContent={
						this.props.button.createButtonApp({
							area: 'corpcontrastreport_list',
							buttonLimit: 4,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})
					}
				/>
				<div className="nc-bill-search-area">
					{NCCreateSearch(this.searchId, {
						clickSearchBtn: searchBtnClick.bind(this),
						defaultConditionsNum: 6, //默认显示几个查询条件
						onAfterEvent: onAfterEvent.bind(this)
					})}
				</div>
				<div className="nc-bill-table-area">
					{createSimpleTable(this.tableId, {
						fieldid:'ReportList',
						//showCheck: true,
						showIndex: true,
						onRowDoubleClick : this.onRowDoubleClick
					})}
				</div>
			</div>
		);
	}
}

List = createPage({
	initTemplate: initTemplate,
	//mutiLangCode: '2002'
})(List);

ReactDOM.render(<List />, document.querySelector('#app'));
