import { createPage, ajax, base, toast,cacheTools } from 'nc-lightapp-front';
import clickSearchBtn from './searchBtnClick';
let { NCPopconfirm, NCIcon } = base;
let pageId = '20022022_web_list';
let searchId = '20022022_query';
let tableId = '20022022_list';

export default function (props) {	
	let appcode = props.getUrlParam('c')||props.getSearchParam('c');
	cacheTools.set('corpreportappcode',appcode); 
	props.createUIDom(
		{
			pagecode: pageId,
			appcode: appcode
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta)
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if(props.getUrlParam('jumpflag'))
				{
					let searchVal = cacheTools.get(searchId);
					clickSearchBtn(props,searchVal,'update');
				}
			}
		}
	)
}

function modifierMeta(props, meta) {

	//查询区
	meta[searchId].items = meta[searchId].items.map((item, key) => {
		if (item.attrcode == 'pk_contrastrule') 
		{
			item.isMultiSelectedEnabled = true;
			item.queryCondition = ()=> 
			{
				return {'GridRefActionExt':'nccloud.web.gl.ref.ContrastRuleRefSqlBuilderForCorpContarstReport'}
			}
		}
		else if (item.attrcode == 'yearperiod') 
		{
	
			item.queryCondition = ()=> {
				return {
					"pk_accperiodscheme":cacheTools.get('corpcontrastpk_accperiodscheme'),
					"isadj":"N"
				}
			}
		}
		return item;
	})

	meta[tableId].items = meta[tableId].items.map((item, key) => {
		if (item.attrcode == 'month') {
			item.render = (text, record, index) => {
				return (
					<a
						style={{cursor: 'pointer' }}
						onClick={() => {
							let url = '';
							let pagecode ='';
							if (record.occurDetailVOs.value && record.occurDetailVOs.value.length > 0) {
								pagecode = '20022022_web_occur_card';
								url = '/gl/corpcontrastreport/pages/occurcard/index.html'
							}
							if (record.balanceDetailVOs.value && record.balanceDetailVOs.value.length > 0) {
								pagecode = '20022022_web_balance_card';
								url = '/gl/corpcontrastreport/pages/balancecard/index.html'
							}
							props.linkTo(url, {
								pagecode:pagecode,
								status: 'browse',
								pk_contrastrule: record.pk_contrastrule.value,
								year: record.year.value,
								month: record.month.value,
								reportstatus: record.reportstatus.value,
								pk_periodschema: record.pk_resultlisttabvo.value
							})
						}}
					>
						{record.month && record.month.value}
					</a>
				);
			};
		}
		return item;
	});
	return meta;
}
