import { ajax,toast,cacheTools } from 'nc-lightapp-front';
let searchId =  '20022022_query'
let field ='year'
 export default function onAfterEvent(field, val) {
	let self = this; 
    if(field =='pk_contrastrule')
	 {
		 if(val.length==0)
		 {
			 return;
		 }
		//添加到缓存
        cacheTools.set('corpcontrastpk',val[0].refpk);
        //获取日期的默认值
        let pk_contrastrule = val[0].refpk;
        let sendData = {pk_contrastrule:pk_contrastrule};
		let url = '/nccloud/gl/contrast/contrastreportmakeinit.do';
		ajax({
			url:url,
			data:sendData,
			success: function(response){
				const { success } = response;
				if (success) {
					if(response.data){
						cacheTools.set('corpcontrastcontent',response.data.content);
					}	
				}   
			}
		});
	}
	else if(field =='pk_accperiodscheme')
	{
		if(val.refpk)
		{
			cacheTools.set('corpcontrastpk_accperiodscheme',val.refpk);
		}
		else
		{
 			setInitData(self.props,'yearperiod',null,null);
		}
	}
 }

function setInitData(props,field,display,value) {
  let data = {
            display:display,value:value
        };
    props.search.setSearchValByField(searchId,field,data)

}
