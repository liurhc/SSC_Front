import { base, ajax,cacheTools } from 'nc-lightapp-front';
let { NCPopconfirm } = base;
let tableId = 'table';
let pageId = '20022022_linksubjdatabalance_list';
export default function(props) {
		let appcode = props.getUrlParam('c')||props.getSearchParam('c');
		props.createUIDom(
		{
			pagecode: pageId,
			appcode: appcode
		}, 
		function (data){
			if(data){
				if(data.template){
					let meta = data.template;
					modifierMeta(props, meta)
					props.meta.setMeta(meta);
				}
				if(data.button){
					let button = data.button;
					props.button.setButtons(button);
				}
				if (props.getUrlParam('status') == 'browse') {
					let data = {
						pk_contrastrule: props.getUrlParam('pk_contrastrule'),
						year: props.getUrlParam('year'),
						period: props.getUrlParam('period'),
						pk_accountingbook: props.getUrlParam('pk_accountingbook'),
						pk_oppaccountingbook: props.getUrlParam('pk_oppaccountingbook'),
						pk_currtype: props.getUrlParam('pk_currtype'),
						pk_periodschema: props.getUrlParam('pk_periodschema'),
						contrastmoneytype: props.getUrlParam('contrastmoneytype'),
						allsubj: props.getUrlParam('allsubj'),
						pagecode: pageId 
					};
					ajax({
						url: '/nccloud/gl/corpcontrastresult/sumcontrastquerylinksubjdata.do',
						data: data,
						success: (res) => {
							if (res.data) {
								props.cardTable.setTableData(tableId, res.data[tableId]);
								//字段控制
								setQueryContent(props,cacheTools.get('corpcontrastcontent'));
							} else {
								props.cardTable.setTableData(tableId, { rows: [] });
							}
						}
					});
				}
			}   
		}
	)
}

function modifierMeta(props, meta) {
	let status = props.getUrlParam('status');
	meta[tableId].status = status;
	
	meta[tableId].items.forEach((item, key) => {
		return item;
		});
	return meta;
}
//设置隐藏列
function setQueryContent(props,content){
	let tableid = 'table';
	if(content[0]==='N')
	{
		props.cardTable.hideColByKey(tableid,['d_yearinit_quantity','d_init_quantity','d_end_quantity','c_yearinit_quantity','c_init_quantity','c_end_quantity']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['d_yearinit_quantity','d_init_quantity','d_end_quantity','c_yearinit_quantity','c_init_quantity','c_end_quantity']);
	}
	if(content[1]==='N')
	{
		props.cardTable.hideColByKey(tableid,['d_yearinit_cur','d_init_cur','d_end_cur','c_yearinit_cur','c_init_cur','c_end_cur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['d_yearinit_cur','d_init_cur','d_end_cur','c_yearinit_cur','c_init_cur','c_end_cur']);
	}
	if(content[2]==='N')
	{
		props.cardTable.hideColByKey(tableid,['d_yearinit_orgcur','d_init_orgcur','d_end_orgcur','c_yearinit_orgcur','c_init_orgcur','c_end_orgcur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['d_yearinit_orgcur','d_init_orgcur','d_end_orgcur','c_yearinit_orgcur','c_init_orgcur','c_end_orgcur']);
	}
	if(content[3]==='N')
	{
		props.cardTable.hideColByKey(tableid,['d_yearinit_groupcur','d_init_groupcur','d_end_groupcur','c_yearinit_groupcur','c_init_groupcur','c_end_groupcur','balance_yearinit_groupcur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['d_yearinit_groupcur','d_init_groupcur','d_end_groupcur','c_yearinit_groupcur','c_init_groupcur','c_end_groupcur','balance_yearinit_groupcur']);
	}
	if(content[4]==='N')
	{
		props.cardTable.hideColByKey(tableid,['d_yearinit_globalcur','d_init_globalcur','d_end_globalcur','c_yearinit_globalcur','c_init_globalcur','c_end_globalcur','balance_yearinit_globalcur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['d_yearinit_globalcur','d_init_globalcur','d_end_globalcur','c_yearinit_globalcur','c_init_globalcur','c_end_globalcur','balance_yearinit_globalcur']);
	}
}
