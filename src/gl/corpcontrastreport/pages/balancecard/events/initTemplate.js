import { base, ajax,cacheTools } from 'nc-lightapp-front';
let { NCPopconfirm } = base;

const formId = 'form';
const tableId = 'table';
const pageId = '20022022_web_balance_card';
import tableButtonClick from './tableButtonClick';
export default function(props) {
	let page = this;
	let appcode = props.getUrlParam('c')||props.getSearchParam('c');
	props.createUIDom(
		{
			pagecode: pageId,
			appcode: appcode
		}, 
		function (data){
			if(data){
				if(data.template){
					let meta = data.template;
					modifierMeta(props, meta, page)
					props.meta.setMeta(meta);
				}
				if(data.button){
					let button = data.button;
					props.button.setButtons(button);
				}
				if (props.getUrlParam('status') == 'browse') {

				let data = {
					pk_contrastrule: props.getUrlParam('pk_contrastrule'),
					year: props.getUrlParam('year'),
					month: props.getUrlParam('month'),
					reportstatus: props.getUrlParam('reportstatus'),
					pk_periodschema: props.getUrlParam('pk_periodschema'),
					pagecode: pageId
				};
				ajax({
					url: '/nccloud/gl/corpcontrastresult/sumcontrastbalancedetailcard.do',
					data: data,
					success: (res) => {
						if (res.data) {
							if (res.data.head) {
								props.form.setAllFormValue({ [formId]: res.data.head[formId] });
								//按钮控制
								if(props.form.getFormItemsValue(formId,'booktype.name').display==null)
								{
									props.button.setButtonVisible(['linkreport'], false);
									props.button.setButtonVisible(['linkresult'], true);
								}
								else
								{
									props.button.setButtonVisible(['linkreport'], true);
									props.button.setButtonVisible(['linkresult'], false);
								}
							}
							if (res.data.body) {
								props.cardTable.setTableData(tableId, res.data.body[tableId]);
								//字段控制
								setQueryContent(props,cacheTools.get('corpcontrastcontent'));
							}
						} else {
							props.form.setAllFormValue({ [formId]: { rows: [] } });
							props.cardTable.setTableData(tableId, { rows: [] });
						}
					}
				});
			}
			}   
		}
	)
}

function getListButtons (text, record, index, page, props) {
	let buttonArray = ['linksubjrowdata', 'linkallsubjdata'];
	if(props.form.getFormItemsValue(formId,'booktype.name').display==null) {
		buttonArray.push('linkresult');
	} else {
		buttonArray.push('linkreport');
	}
	return buttonArray;
}

function modifierMeta(props, meta, page) {
	let status = props.getUrlParam('status');
	meta[formId].status = status;
	meta[tableId].status = status;
	
	meta[tableId].items.forEach((item, key) => {
		return item;
		});
	meta[tableId].items.push({
		attrcode: 'opr',
		label: page.state.json['20022022-000006'],/* 国际化处理： 操作*/
		fixed: 'right',
		itemtype: 'customer', 
		visible: true,
		className: 'table-opr',
		width:'320px',
		render: (text, record, index) => {
            return props.button.createOprationButton(getListButtons(text, record, index, page, props), {
                area: "corpcontrastreport_balance_col",
                buttonLimit: 3,
                onButtonClick: (props, key) => tableButtonClick(props, key, text, record, index,tableId, page)
            });
        }
	});
	let multiLang = props.MutiInit.getIntl('2052');
	return meta;
}
//设置隐藏列
function setQueryContent(props,content){
	let tableid = 'table';
	if(content[0]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_yearinit_quantity','self_init_quantity','self_end_quantity','other_yearinit_quantity','other_init_quantity','other_end_quantity','balance_yearinit_quantity','balance_init_quantity','balance_end_quantity']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_yearinit_quantity','self_init_quantity','self_end_quantity','other_yearinit_quantity','other_init_quantity','other_end_quantity','balance_yearinit_quantity','balance_init_quantity','balance_end_quantity']);
	}
	if(content[1]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_yearinit_cur','self_init_cur','self_end_cur','other_yearinit_cur','other_init_cur','other_end_cur','balance_yearinit_cur','balance_init_cur','balance_end_cur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_yearinit_cur','self_init_cur','self_end_cur','other_yearinit_cur','other_init_cur','other_end_cur','balance_yearinit_cur','balance_init_cur','balance_end_cur']);
	}
	if(content[2]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_yearinit_orgcur','self_init_orgcur','self_end_orgcur','other_yearinit_orgcur','other_init_orgcur','other_end_orgcur','balance_yearinit_orgcur','balance_init_orgcur','balance_end_orgcur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_yearinit_orgcur','self_init_orgcur','self_end_orgcur','other_yearinit_orgcur','other_init_orgcur','other_end_orgcur','balance_yearinit_orgcur','balance_init_orgcur','balance_end_orgcur']);
	}
	if(content[3]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_yearinit_groupcur','self_init_groupcur','self_end_groupcur','other_yearinit_groupcur','other_init_groupcur','other_end_groupcur','balance_yearinit_groupcur','balance_init_groupcur','balance_end_groupcur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_yearinit_groupcur','self_init_groupcur','self_end_groupcur','other_yearinit_groupcur','other_init_groupcur','other_end_groupcur','balance_yearinit_groupcur','balance_init_groupcur','balance_end_groupcur']);
	}
	if(content[4]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_yearinit_globalcur','self_init_globalcur','self_end_globalcur','other_yearinit_globalcur','other_init_globalcur','other_end_globalcur','balance_yearinit_globalcur','balance_init_globalcur','balance_end_globalcur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_yearinit_globalcur','self_init_globalcur','self_end_globalcur','other_yearinit_globalcur','other_init_globalcur','other_end_globalcur','balance_yearinit_globalcur','balance_init_globalcur','balance_end_globalcur']);
	}
}
