import { ajax, base, toast,cacheTools } from 'nc-lightapp-front';
export default function(props, id) {
  let rows = props.cardTable.getClickRowIndex(this.tableId);
  switch (id) {
    case 'back':
    props.linkTo('/gl/corpcontrastreport/pages/list/index.html',
    {
      pagecode:'20022022_web_list',
      jumpflag:true
    }
    )
      break
    case 'refresh':
      let data = {
        pk_contrastrule: this.props.getUrlParam('pk_contrastrule'),
        year: this.props.getUrlParam('year'),
        month: this.props.getUrlParam('month'),
        reportstatus: this.props.getUrlParam('reportstatus'),
        pk_periodschema: this.props.getUrlParam('pk_periodschema'),
        pagecode: this.pageid
      };
      ajax({
        url: '/nccloud/gl/corpcontrastresult/sumcontrastbalancedetailcard.do',
        data: data,
        success: (res) => {
          if (res.data) {
            toast({ color: 'success', title:this.state.json['20022022-000004']});/* 国际化处理：刷新成功！*/
            if (res.data.head) {
              this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
            }
            if (res.data.body) {
              this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
              //字段控制
							setQueryContent(this.props,cacheTools.get('corpcontrastcontent'));
            }
          } else {
            this.props.form.setAllFormValue({ [this.formId]: { rows: [] } });
            this.props.cardTable.setTableData(this.tableId, { rows: [] });
          }
        }
      });
      break
    case 'linkreport':
      if (rows) {
        let record = rows['record'].values;
        let data = {
          pagecode:'20022022_linkreportcard',
          status: 'browse',
          pk_contrastrule: this.props.getUrlParam('pk_contrastrule'),
          year: this.props.getUrlParam('year'),
          period: this.props.getUrlParam('month'),
          reportstatus: this.props.getUrlParam('reportstatus'),
          pk_periodschema: this.props.getUrlParam('pk_periodschema'),
          pk_accountingbook: record.self_accountingbook.display,
          pk_oppaccountingbook: record.other_accountingbook.display,
          pk_currtype: record.pk_currtype.value,
          contrastmoneytype: 'Y'
        }
        props.openTo('/gl/corpcontrastreport/pages/linkreportcard/index.html', data);
      }
      else
      {
        toast({color: 'warning', content: this.state.json['20022022-000000']});/* 国际化处理： 请选择一行数据*/
      }
      break
    case 'linksubjrowdata':
      if (rows) {
        let record = rows['record'].values;
        let data = {
          status: 'browse',
          pagecode:'20022022_linksubjdatabalance_list',
          pk_contrastrule: this.props.getUrlParam('pk_contrastrule'),
          year: this.props.getUrlParam('year'),
          period: this.props.getUrlParam('month'),
          reportstatus: this.props.getUrlParam('reportstatus'),
          pk_periodschema: this.props.getUrlParam('pk_periodschema'),
          pk_accountingbook: record.self_accountingbook.display,
          pk_oppaccountingbook: record.other_accountingbook.display,
          pk_currtype: record.pk_currtype.value,
          contrastmoneytype: 'Y',
          allsubj: 'N'
        }
        props.openTo('/gl/corpcontrastreport/pages/linksubjdatabalance/index.html', data);
      }
      else
      {
        toast({color: 'warning', content: this.state.json['20022022-000000']});/* 国际化处理： 请选择一行数据*/
      }
      break
    case 'linkallsubjdata':
      if (rows) {
        let record = rows['record'].values;
        let data = {
          status: 'browse',
          pagecode :'20022022_linksubjdatabalance_list',
          pk_contrastrule: this.props.getUrlParam('pk_contrastrule'),
          year: this.props.getUrlParam('year'),
          period: this.props.getUrlParam('month'),
          reportstatus: this.props.getUrlParam('reportstatus'),
          pk_periodschema: this.props.getUrlParam('pk_periodschema'),
          pk_accountingbook: record.self_accountingbook.display,
          pk_oppaccountingbook: record.other_accountingbook.display,
          pk_currtype: record.pk_currtype.value,
          contrastmoneytype: 'Y',
          allsubj: 'Y'
        }
        props.openTo('/gl/corpcontrastreport/pages/linksubjdatabalance/index.html', data);
      }
      else
      {
        toast({color: 'warning', content: this.state.json['20022022-000000']});/* 国际化处理： 请选择一行数据*/
      }
      break
    case 'linkresult':
      if (rows) {
        let record = rows['record'].values;
        let data = {
          appcode:'20022035',
          pagecode:'20022035_web_list',
          jumpflag:'false',
          status: 'browse',
          pk_contrastrule: this.props.getUrlParam('pk_contrastrule'),
          year: this.props.getUrlParam('year'),
          month: this.props.getUrlParam('month'),
          pk_periodschema: this.props.getUrlParam('pk_periodschema'),
          pk_self: record.self_accountingbook.display,
          pk_opp: record.other_accountingbook.display,
          //把对账规则传过去做字段过滤
          content:cacheTools.get('corpcontrastcontent')
        }
        props.openTo('/gl/contrastreportresult/pages/list/index.html', data);
      }
      else
      {
        toast({color: 'warning', content: this.state.json['20022022-000000']});/* 国际化处理： 请选择一行数据*/
      }
      break
    default:
      break
  }
}
//设置隐藏列
function setQueryContent(props,content){
	let tableid = 'table';
	if(content[0]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_yearinit_quantity','self_init_quantity','self_end_quantity','other_yearinit_quantity','other_init_quantity','other_end_quantity','balance_yearinit_quantity','balance_init_quantity','balance_end_quantity']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_yearinit_quantity','self_init_quantity','self_end_quantity','other_yearinit_quantity','other_init_quantity','other_end_quantity','balance_yearinit_quantity','balance_init_quantity','balance_end_quantity']);
	}
	if(content[1]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_yearinit_cur','self_init_cur','self_end_cur','other_yearinit_cur','other_init_cur','other_end_cur','balance_yearinit_cur','balance_init_cur','balance_end_cur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_yearinit_cur','self_init_cur','self_end_cur','other_yearinit_cur','other_init_cur','other_end_cur','balance_yearinit_cur','balance_init_cur','balance_end_cur']);
	}
	if(content[2]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_yearinit_orgcur','self_init_orgcur','self_end_orgcur','other_yearinit_orgcur','other_init_orgcur','other_end_orgcur','balance_yearinit_orgcur','balance_init_orgcur','balance_end_orgcur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_yearinit_orgcur','self_init_orgcur','self_end_orgcur','other_yearinit_orgcur','other_init_orgcur','other_end_orgcur','balance_yearinit_orgcur','balance_init_orgcur','balance_end_orgcur']);
	}
	if(content[3]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_yearinit_groupcur','self_init_groupcur','self_end_groupcur','other_yearinit_groupcur','other_init_groupcur','other_end_groupcur','balance_yearinit_groupcur','balance_init_groupcur','balance_end_groupcur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_yearinit_groupcur','self_init_groupcur','self_end_groupcur','other_yearinit_groupcur','other_init_groupcur','other_end_groupcur','balance_yearinit_groupcur','balance_init_groupcur','balance_end_groupcur']);
	}
	if(content[4]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_yearinit_globalcur','self_init_globalcur','self_end_globalcur','other_yearinit_globalcur','other_init_globalcur','other_end_globalcur','balance_yearinit_globalcur','balance_init_globalcur','balance_end_globalcur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_yearinit_globalcur','self_init_globalcur','self_end_globalcur','other_yearinit_globalcur','other_init_globalcur','other_end_globalcur','balance_yearinit_globalcur','balance_init_globalcur','balance_end_globalcur']);
	}
}
