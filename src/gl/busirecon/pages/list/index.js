//主子表列表

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base,getMultiLang,cacheTools,cardCache,createPageIcon } from 'nc-lightapp-front';
let { getDefData,setDefData } = cardCache;
import createScript from '../../../public/components/uapRefer.js';
let { NCTabsControl ,NCButton,NCTable,NCRow:Row,NCCol:Col,NCCheckbox: Checkbox} = base;
import { buttonClick, initTemplate,searchBtnClick,afterEvent} from './events';
import GlobalStore from '../../../public/components/GlobalStore';
import getLangCode from '../../../public/common/getLangCode';
import './index.less';
import HeaderArea from '../../../public/components/HeaderArea';
import { dataSource,pkname} from './constants';
const moduleId='200260';
class List extends Component {
	constructor(props) {
        super(props);
		// this.pageCode = '200260SETG_brbase';
		this.tableId = 'gl_brsetting';
		this.searchId='busirecon_query';
		this.state = {
			json:{},
			showSealDataFlag : false,
			pk_accountingbook : '',
			pageData : [],
			context:{},//保存context 数据
		}
		this.moduleId=moduleId;
	}
	componentWillMount(){
		let callback= (json) =>{
            this.setState({
              json:json,
                columns_reportSum:this.columns_reportSum,
				},()=>{
				initTemplate.call(this, this.props);
            })
        }
		getMultiLang({moduleId:'200260',domainName:'gl',currentLocale:'simpchn',callback});
    }
	//双击行跳转卡片页
	tableRowDoubleClick=(record,index,props,e)=>{//props,record
		//{pageCode:200260SETG_brbase_card,"pk_brsetting":"对账规则主键"}
		let pageCode_card;
		let nodetype=this.props.getSearchParam('nodetype')?this.props.getSearchParam('nodetype'):this.props.getUrlParam('nodetype');
		if(nodetype=='group'){
			pageCode_card='200260SETG_brbase_card';
		}else if(nodetype=='org'){
			pageCode_card='200260SETO_brbase_card';
		}
		this.props.pushTo('/card', {
			c:this.props.getSearchParam('c')?this.props.getSearchParam('c'):this.props.getUrlParam('c'),
			nodetype:this.props.getSearchParam('nodetype')?this.props.getSearchParam('nodetype'):this.props.getUrlParam('nodetype'),
			pageCode:pageCode_card,
			p:pageCode_card,
			pk_brsetting:record.pk_brsetting.value,
			ts:record.ts.value,
			status: 'browse'
		});
		//列表跳卡片缓存查询条件
		let searchVal = props.search.getAllSearchData(this.searchId)
        if (searchVal) {
            setDefData('gl_brsetting', dataSource, searchVal);
        }
	}
	statusChangeEvent=()=>{
		let{context}=this.state;
		let searchId = 'busirecon_query';
		if(this.props.getUrlParam('nodetype')=='group'){

		}else if(this.props.getUrlParam('nodetype')=='org'){
			let data={display:context.defaultAccbookName,value:context.defaultAccbookPk};
			
			this.props.search.setSearchValByField(searchId,'pk_accountingbook',data);

		}
	}
	selectedChange=(props,newVal,oldVal)=>{//选中行发生变化
		if(oldVal != 0){
			props.button.setButtonDisabled(['delete'], false)
		}else{
			props.button.setButtonDisabled(['delete'], true)
		}
	}
	renderCompleteEvent = () => {// 查询区渲染完成回调函数，用于渲染卡片跳回列表保存查询条件
        let cachesearch = getDefData('gl_brsetting', dataSource);
        if (cachesearch && cachesearch.conditions) {
          // this.props.search.setSearchValue(this.searchId, cachesearch);
          for(let item of cachesearch.conditions){
            // if (item.field == 'busi_billdate') {
            //    // 时间类型特殊处理
            //    let time = [];
            //    time.push(item.value.firstvalue);
            //    time.push(item.value.secondvalue);
            //    this.props.search.setSearchValByField(this.searchId,item.field,
            //             {display:item.display,value:time});
            // }else{
                this.props.search.setSearchValByField(this.searchId,item.field,
                {display:item.display,value:item.value.firstvalue});
             //}
          }
        }
    }

    render() {
        let { table, button, search } = this.props;
		let buttons = this.props.button.getButtons();
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { createButton, getButtons } = button;  	   
		return (
			<div className="nc-bill-list" id="oragization">
				<HeaderArea 
                    title = {this.props.getUrlParam('nodetype')=='group'?this.state.json['200260-0002']:this.state.json['200260-0001']}/* 国际化处理： 往来核销处理*/
                    
                    btnContent = {
						this.props.button.createButtonApp({
							area: 'busirecon',
							buttonLimit: 4, 
							onButtonClick: buttonClick.bind(this), 
							popContainer: document.querySelector('.header-button-area')
						})
                    }
                />
				{/* <div className="nc-bill-header-area" fieldid="header_area">
					<div className="header-title-search-area">
						{createPageIcon()}
						<h2 className="title-search-detail" fieldid={`${this.props.getUrlParam('nodetype')=='group'?this.state.json['200260-0002']:this.state.json['200260-0001']}_title`}>
							{this.props.getUrlParam('nodetype')=='group'?this.state.json['200260-0002']:this.state.json['200260-0001']}
						</h2>
					</div>
					<div className="header-button-area">
                        {this.props.button.createButtonApp({
                            area: 'busirecon',
                            buttonLimit: 4, 
                            onButtonClick: buttonClick.bind(this), 
                            popContainer: document.querySelector('.header-button-area')
                        })}
					</div>
				</div> */}
				<div className="nc-bill-search-area">
					{NCCreateSearch(this.searchId, {
						onAfterEvent:afterEvent.bind(this),
                        clickSearchBtn: searchBtnClick.bind(this,this.state),
						showAdvBtn: true,
						statusChangeEvent:this.statusChangeEvent.bind(this),
						defaultConditionsNum:1,
						renderCompleteEvent:this.renderCompleteEvent 
					})}
				</div>
				{/* <div style={{ height: '10px' }} /> */}
				<div className="nc-bill-table-area">
					{createSimpleTable(this.tableId, {
                       	onRowDoubleClick:this.tableRowDoubleClick.bind(this),
						showCheck: true,
						showIndex: true,
						selectedChange: this.selectedChange.bind(this),
						dataSource: dataSource,
						pkname: pkname
					})}
				</div>
            </div>
		);
    }
	getUrlParam=(pop)=>{
		if (!pop) return;
		let result;
		let queryString = window.location.search || window.location.hash;
		queryString = queryString.substring(1);
		if (queryString.includes('?')) {
			queryString = queryString.replace(/\?/g, '&');
		}
		let params = queryString;
		if (params) {
			params = params.split('&');
			let item = params.find((e) => e.indexOf(pop) !== -1);
			item && (result = item.split('=')[1]);
		}
		return result;
	}
}
List = createPage({
})(List);
export default List;
