import searchBtnClick from './searchBtnClick';
import initTemplate from './initTemplate';
import pageInfoClick from './pageInfoClick';
import buttonClick from './buttonClick';
import getLangCode from './getLangCode';
import refresh from './refresh';
import afterEvent from './afterEvent';
export { refresh, searchBtnClick,initTemplate,pageInfoClick,buttonClick,getLangCode,afterEvent };