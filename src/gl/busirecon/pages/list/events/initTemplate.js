import { createPage, ajax, base, toast,cacheTools,print } from 'nc-lightapp-front';
import clickSearchBtn from './searchBtnClick';
import refresh  from './refresh';
let { NCPopconfirm, NCIcon ,NCTooltip:Tooltip} = base;
let searchId = 'busirecon_query';
// const pageCode = '200260SETO_brbase';
const tableId = 'gl_brsetting';
const config={
    "isDataPowerEnable":'Y',
    "DataPowerOperationCode":'fi'
};
export default function (props) {
	let appcode = props.getSearchParam('c') || '';
	let pageCode;
	cacheTools.set("brsettingNodeType",props.getUrlParam('nodetype'));
	if(props.getUrlParam('nodetype')=='group'){
		pageCode = '200260SETG_brbase';
	}else if(props.getUrlParam('nodetype')=='org'){
		pageCode = '200260SETO_brbase';
	}
	let _this = this;
	props.createUIDom(
		{
			pagecode: pageCode,//页面id
			appcode: appcode//小应用id
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					let context=data.context;
					_this.setState({context:context});
					meta = modifierMeta(props, meta,_this);
					props.meta.oid=meta[searchId].oid;
					props.meta.setMeta(meta);
					setDefaultVaule(props,meta,context);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					props.button.setButtonVisible('edit', false);
					props.button.setPopContent('deleterow', _this.state.json['200260-0005']);
				}
				
				if(props.getUrlParam('jumpflag')){
					let searchVal = cacheTools.get('brsettingSearchVal');
					clickSearchBtn(null,props,searchVal);
				}
				refresh(props);
			}
		}
	)
	props.button.setButtonDisabled(['delete'], true)
}
//设置默认值
function setDefaultVaule(props,meta,context){
	if(props.getUrlParam('nodetype')=='group'){

	}else if(props.getUrlParam('nodetype')=='org'){
		let data={display:context.defaultAccbookName,value:context.defaultAccbookPk};
		
		props.search.setSearchValByField(searchId,'pk_accountingbook',data);
	}
}
function modifierMeta(props, meta,self) {
	let appcode = props.getSearchParam('c') || '';
	meta[searchId].items = meta[searchId].items.map((item, key) => {
		// item.visible = true;
		item.col = '3';
		if(item.attrcode=='pk_currtype'){
			item.refcode='uapbd/refer/pubinfo/CurrtypeGridRef/index';
			//item.isMultiSelectedEnabled = true;单选多选
		}
		return item;
	})

	meta[tableId].items = meta[tableId].items.map((item, key) => {
		if (item.attrcode == 'code') {
            item.render = (text, record, index) => {
				let tip=(<div>{record.code.value}</div>);
                if(record)
                return (
					<Tooltip trigger='hover' placement='top' inverse={false} overlay={tip}>
						<a
							style={{ textDecoration: 'underline', cursor: 'pointer' }}
							onClick={() => {
								oprButtonClick(self, props, 'code', record, index)
							}}
						>
							{record.code.value}
						</a>
					</Tooltip>
                );
            };
        }
		return item;
	});
	if(props.getUrlParam('nodetype')=='group'){
	}else if(props.getUrlParam('nodetype')=='org'){
		meta[searchId].items.map((item,index)=>{
			if(item.attrcode=='pk_accountingbook'){
				item.required=true;
				item.disabledDataShow=true;
				item.queryCondition=() => {
					return Object.assign({
						"TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
						"appcode":appcode
					},config)
				}
			}
		})
	}
	meta[tableId].items.push({
		attrcode: 'opr',
		label: self.state.json['200260-0061'],
		fixed: 'right',
		itemtype: 'customer', 
		visible: true,
		className: 'table-opr',
		width:'160px',
		render: (text, record, index) => {
            return props.button.createOprationButton(['edit','deleterow'], {
                area: "row_area",
                buttonLimit: 3,
				onButtonClick: (props, key) => oprButtonClick(self, props, key, record, index)
				// (props, key) => self.tableRowDoubleClick.bind(self,record,index,props)
            });
        }
	});
	return meta;
}
function oprButtonClick(self, props, key, record, index) {
	switch (key) {
		case 'edit'://修改
		operate(self, props, key, record, index,'edit')
		break;
		case 'deleterow'://删除
			operate(self, props, key, record, index,'browse')
		break;
		case 'code'://code编码
			// operate(self, props, key, record, index,'browse')
			self.tableRowDoubleClick(record,index,props);
		break;

		default:
		break;
	}
}
function operate(self, props, key, record, index,status){
	let {deleteCacheId} = props.table;
	if(key=='deleterow'){
		let pk_brsetting=[record.pk_brsetting.value];
		let opurl='/nccloud/gl/busirecon/brsettingDel.do';
		ajax({
			url: opurl,
			data: pk_brsetting,
			async:true,
			success: (res) => {
				let { success, data } = res;
				props.table.deleteTableRowsByIndex(tableId, index);
				if (res.data && res.data.warn) {
					toast({ content: res.data.warn, color: 'warning' });
				} else{
					toast({ content: self.state.json['200260-0008'], color: 'success' });
					deleteCacheId(tableId,pk_brsetting);
				}
			},
			error: (res) => {
				toast({ content: res.message, color: 'danger'})
			}
		})
	}else{
		let pageCode_card;
		let nodetype=self.props.getSearchParam('nodetype')?self.props.getSearchParam('nodetype'):self.props.getUrlParam('nodetype');
		if(nodetype=='group'){
			pageCode_card='200260SETG_brbase_card';
		}else if(nodetype=='org'){
			pageCode_card='200260SETO_brbase_card';
		}
		props.pushTo('/card', {
			c:self.props.getUrlParam('c'),
			nodetype:props.getUrlParam('nodetype'),
			pageCode:pageCode_card,	
			p:pageCode_card,
			id: record.pk_brsetting.value,
			ts:record.ts.value,
			status: 'browse'
		});

	}
}