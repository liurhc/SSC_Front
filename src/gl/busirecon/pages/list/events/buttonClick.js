import { promptBox,ajax, base, toast,cacheTools,print,withNav } from 'nc-lightapp-front';
import GlobalStore from '../../../../public/components/GlobalStore';
let tableid = 'gl_brsetting';
export default function buttonClick(props, id) {
    switch (id) {
        case 'add':
            let c = this.props.getSearchParam('c');
            let pageCode_card;
            let nodetype=this.props.getSearchParam('nodetype')?this.props.getSearchParam('nodetype'):this.props.getUrlParam('nodetype');
            if(nodetype=='group'){
                pageCode_card='200260SETG_brbase_card';
            }else if(nodetype=='org'){
                pageCode_card='200260SETO_brbase_card';
            }
            let self=this
			props.pushTo('/card', {
				status : 'add',
                nodetype:nodetype,
                c:c,
                pageCode: pageCode_card,
                p:pageCode_card
			})
            break;
        case 'delete':
            
            let delUrl = '/nccloud/gl/busirecon/brsettingDel.do';
            const selectedData = props.table.getCheckedRows(tableid);
            if(selectedData.length==0){//selectedData.length>1||
                toast({ content: this.state.json['200260-0003'], color: 'warning'})
                return false;
            }
            promptBox({
                color:'success',
                title:this.state.json['200260-0004'],
                content:this.state.json['200260-0005'],
                noFooter: false,                // 是否显示底部按钮(确定、取消),默认显示(false),非必输
                noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
                beSureBtnName:this.state.json['200260-0006'],          // 确定按钮名称, 默认为"确定",非必输
                cancelBtnName:this.state.json['200260-0007'],           // 取消按钮名称, 默认为"取消",非必输
                beSureBtnClick: dealOperate.bind(this,props, delUrl, id),   // 确定按钮点击调用函数,非必输
                cancelBtnClick: cancelBtnClick.bind(this)  // 取消按钮点击调用函数,非必输
            })
            //dealOperate(props, delUrl, id);
            break;
        case 'query':
            this.setState({
                showModal:true
            })
            break;
        case 'refresh':
            //刷新
            let data=cacheTools.get("brsettingSearchData");
            if(data){
                ajax({
                    url: '/nccloud/gl/busirecon/brsettingquery.do',
                    data: data,
                    success: (res) => {
                        let { success, data } = res;
                        if (success) {
                            if(data){
                                this.props.table.setAllTableData(tableid, data[tableid]);
                            }else{
                                this.props.table.setAllTableData(tableid, {rows:[]});
                            }
                            
                        }
                    }
                });
            }
            break;

    }
}

export function cancelBtnClick(){
    return false;
}
export function dealOperate(props, opurl, id) {
    let {deleteCacheId} = props.table;
    const selectedData = props.table.getCheckedRows(tableid);
    if (selectedData.length == 0) return false;
    let indexArr = [];
    let pk_brsetting=[];
    let dataArr = [];
    let indexObj = {};
    selectedData.forEach((val) => {
        indexArr.push(val.index);
        pk_brsetting.push(val.data.values.pk_brsetting.value);
    });
    ajax({
        url: opurl,
        data: pk_brsetting,
        async:true,
        success: (res) => {
            let { success, data } = res;
            if (data&&data.error&&data.error.length > 0) {
                toast({ content: data.error, color: 'danger', position: 'bottom' })
            }
            else if (success) {
                if (id == 'delete') {
                    props.table.deleteTableRowsByIndex(tableid, indexArr);
                    toast({ content: this.state.json['200260-0008'], color: 'success', position: 'bottom' })
                    deleteCacheId(tableid,pk_brsetting);
                } else {                                      
                }

            }
        },
        error: (res) => {
            toast({ content: res.message, color: 'danger'})
        }
    })
}

export function dealSuccess(props, data, indexObj, id) {
    let succdatas = data.success['gl_voucher'].rows;
    switch (id) {
        case 'delete':
            succdatas.forEach((val) => {
                props.table.deleteTableRowsByIndex(tableid, indexObj[val.values.pk_voucher.value]);
            });
            toast({ content: this.state.json['200260-0008'], color: 'success', position: 'bottom' })
            break;
    }
    
}