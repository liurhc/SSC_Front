import { ajax, toast,cardCache,cacheTools} from 'nc-lightapp-front';
import { tableId, searchId, pagecode, oid,dataSource,pkname} from '../constants';
let {setDefData, getDefData } = cardCache;
let tableid = 'gl_brsetting';
//点击查询，获取查询区数据
export default function (props) {
	let { hasCacheData } = props.table;
	if(!hasCacheData(dataSource)){
        let data=cacheTools.get("brsettingSearchData");
        if(data){
            ajax({
                url: '/nccloud/gl/busirecon/brsettingquery.do',
                data: data,
                success: (res) => {
                    let { success, data } = res;
                    if (success) {
                        if(data){
                            props.table.setAllTableData(tableid, data[tableid]);
                        }else{
                            props.table.setAllTableData(tableid, {rows:[]});
                        }
                        
                    }
                }
            });
        }
    }
};
