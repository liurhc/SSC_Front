import {ajax,cacheTools,deepClone} from 'nc-lightapp-front';
// const deepClone = require('../../../../public/components/deepClone');

//点击查询，获取查询区数据
let tableid="gl_brsetting";
export default function clickSearchBtn(state,props,searchVal) {
    //let self=this;
    if(searchVal&&searchVal.conditions&&searchVal.conditions.length>0){
        let data={
            querycondition:searchVal,
            pagecode: '200260SETG_brbase',
            queryAreaCode:'busirecon_query',  //查询区编码
            oid:props.meta.oid,//'1001Z31000000003US6P',  //查询模板id，手工添加在界面模板json中，放在查询区，后期会修改
            querytype:'tree',
            //nodetype:this.getUrlParam('nodetype')
            nodetype:cacheTools.get('brsettingNodeType')
        };
        cacheTools.set("brsettingSearchData",data);
        cacheTools.set("brsettingSearchVal",searchVal);
        ajax({
            url: '/nccloud/gl/busirecon/brsettingquery.do',
            data: data,
            success: (res) => {
                let { success, data } = res;
                if (success) {
                    if(data){
                        props.table.setAllTableData(tableid, data[tableid]);
                    }else{
                        props.table.setAllTableData(tableid, {rows:[]});
                    }
                    
                }
            }
        });
    }
};


