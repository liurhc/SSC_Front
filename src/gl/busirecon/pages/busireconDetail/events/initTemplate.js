import { createPage, ajax, base, toast,cacheTools,print } from 'nc-lightapp-front';
let { NCPopconfirm, NCIcon } = base;

let searchId = 'busirecon_query';
const pageCode = '200260EXEVOUCHER';
export default function (props) {

	let appcode =props.getSearchParam('c');
	ajax({
		url: '/nccloud/platform/appregister/queryallbtns.do', 
		data: {
			pagecode: pageCode,
			appcode: appcode//小应用id
		},
		async:false,
		success: function(res) {
			if (res.data) {
				let button = res.data;
				props.button.setButtons(button);
			}
		}
	});
}
