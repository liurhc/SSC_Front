import { ajax,toast,gzip } from 'nc-lightapp-front';
import { setData } from '../../../../manageReport/common/simbleTableData.js';
// import { voucher_gen } from '../../../../public/components/constJSON';
// import { voucherRelatedApp } from '../../../../public/components/oftenApi';
import { openToVoucher } from '../../../../public/common/voucherUtils';
// let ampub = require('ampub');
// // import ampub from 'ampub';
// const { components } = ampub.default;
// const { faQueryAboutUtils } = components;
// const { busireconLinkToFa } = faQueryAboutUtils;
export default function buttonClick(props, id) {
    let self=this;
    let filterData=[];
    let begin=false;
    let{querycondition,resourceDatas}=self.state;
    let rowDatas,link;
    let renderFirstData;
    let gziptools = new gzip();
    switch (id) {
        case 'linkvoucher'://联查凭证
            rowDatas=self.getSelectRowData();
            if(!rowDatas||rowDatas[0]==null||rowDatas.length==0||rowDatas[0].style=='head'){
                toast({ content:this.state.json['200260-0028'], color: 'warning' });//请先选中一行数据再进行联查
                return false;
            }
            link=rowDatas[0].link;
            if(link.glPk_voucher){
                let param={
                    pk_voucher: link.glPk_voucher,
                    titlename:self.state.json['200260-0050'],/* 国际化处理： 凭证联查*/
                }
                openToVoucher(self,param)
                // let voucherapp=voucherRelatedApp(voucher_gen);
                // self.props.openTo('/gl/gl_voucher/pages/main/index.html#/Welcome',
                //     {
                        
                //         link:gziptools.zip(JSON.stringify(link)),
                //         appcode:voucherapp.appcode,//'20020PREPA',
                //         c:voucherapp.appcode,//'20020PREPA',
                //         id:link.glPk_voucher,
                //         pagekey:'link',
                //         n:self.state.json['200260-0050'],//凭证联查
                //         status:'browse'

                //     }
                // )
            }else{
                toast({ content:self.state.json['200260-0046'], color: 'danger' });//请选择行有总账数据的进行联查凭证
                return false;
            }

        break;    
        case 'linkbill'://联查单据
            rowDatas=self.getSelectRowData();
            if(!rowDatas||rowDatas[0]==null||rowDatas.length==0||rowDatas[0].style=='head'){
                toast({ content:this.state.json['200260-0028'], color: 'warning' });//请先选中一行数据再进行联查
                return false;
            }
            link=rowDatas[0].link;
            let linkbillUrl='/nccloud/fipub/pub/linkbill.do';
            if(link.brPk_bill){
                let brBillTyep=link.brBillType;
                ajax({
                    url:linkbillUrl,
                    data:{"billType":brBillTyep},
                    success:function(response){
                        let { data, success } = response;
                        if(success){
                            let appcode=data.appcode;
                            let pagecode=data.appPageVO?data.appPageVO.pagecode:'';
                            let url=data.appPageVO?data.appPageVO.pageurl:'';
                            if(querycondition.busisystem=='FA'){//固定资产
                                let ampub = require('ampub');
                                const { components } = ampub.default;
                                const { faQueryAboutUtils } = components;
                                const { busireconLinkToFa } = faQueryAboutUtils;
                                let pkAndNode = link.brPk_bill.split('#');
                                let id = pkAndNode[0];
                                let params = {
                                    appcode,
                                    pagecode,
                                    url,
                                    id,
                                    pk_accbook:querycondition.linkaccountingbook,
                                    endYear: querycondition.endYear, //结束年
                                    endPeriod: querycondition.endPeriod //结束月
                                };
                                busireconLinkToFa(self.props, params);
                                // let pkAndNode = link.brPk_bill.split('#');
                                // let pk_card = pkAndNode[0];
                                // openAssetCardByPk(self,props, pk_card, appcode, pagecode, url);
                            }else{
                                
                                self.props.openTo(url,
                                {
                                    link:gziptools.zip(JSON.stringify(link)),
                                    appcode:appcode,
                                    pagecode:pagecode,
                                    id:link.brPk_bill,
                                    scene:'linksce',
                                    status:'browse'
            
                                })
                            }
                        }
                    },
                    error:function(error){
                        toast({ content: error, color: 'warning' });
                         
                     }
                })
                
            }else{
                toast({ content:self.state.json['200260-0051'], color: 'warning' });//请选择行有业务数据的进行联查单据
                return false;
            }
            break;
        case 'filterall'://过滤 全部
            filterData=resourceDatas.busivoucher;
            renderFirstData = {};
            renderFirstData.columnInfo = resourceDatas.columnInfo;
            renderFirstData.data = filterData;
            setData(self,renderFirstData);
            // self.changeToHandsonTableData(self,resourceDatas,resourceDatas.columnInfo,resourceDatas.headtitle,filterData);
            break;
        case 'filterrecon'://过滤 已对符
            // let self=this;
            // let filterData=[];
            // let begin=false;
            resourceDatas.busivoucher.map((item,index)=>{
                if(item.symbolType==this.state.json['200260-0047']&&item.symbolType!=null){//已对符
                    filterData.push(item);
                    begin=true;
                }else if(item.symbolType==null&&begin){
                    filterData.push(item);
                }else if(item.symbolType!=null){
                    begin=false;
                }
            })
            renderFirstData = {};
            renderFirstData.columnInfo = resourceDatas.columnInfo;
            renderFirstData.data = filterData;
            setData(self,renderFirstData);
            // self.changeToHandsonTableData(self,resourceDatas,resourceDatas.columnInfo,resourceDatas.headtitle,filterData);
            break;
        case 'filtercluenorecon'://过滤 有线索未对符
          // let self=this;
            // let filterData=[];
            // let begin=false;
            resourceDatas.busivoucher.map((item,index)=>{
                if(item.symbolType==this.state.json['200260-0048']&&item.symbolType!=null){//有线索未对符
                    filterData.push(item);
                    begin=true;
                }else if(item.symbolType==null&&begin){
                    filterData.push(item);
                }else if(item.symbolType!=null){
                    begin=false;
                }
            })
            renderFirstData = {};
            renderFirstData.columnInfo = resourceDatas.columnInfo;
            renderFirstData.data = filterData;
            setData(self,renderFirstData);
            // self.changeToHandsonTableData(self,resourceDatas,resourceDatas.columnInfo,resourceDatas.headtitle,filterData);
            break;
        case 'filternoclue'://过滤 无线索
            // let self=this;
            // let filterData=[];
            // let begin=false;
            resourceDatas.busivoucher.map((item,index)=>{
                if(item.symbolType==this.state.json['200260-0049']&&item.symbolType!=null){//无线索
                    filterData.push(item);
                    begin=true;
                }else if(item.symbolType==null&&begin){
                    filterData.push(item);
                }else if(item.symbolType!=null){
                    begin=false;
                }
            })
            renderFirstData = {};
            renderFirstData.columnInfo = resourceDatas.columnInfo;
            renderFirstData.data = filterData;
            setData(self,renderFirstData);
            // self.changeToHandsonTableData(self,resourceDatas,resourceDatas.columnInfo,resourceDatas.headtitle,filterData);
            break;

        case 'print'://打印
        self.print();
            break;
        default:
        break;
   
    }
}
/**
 * 联查固定资产卡片，通过超链接主键
 * @param {*} props 
 * @param {*} pk_card 
 * @param {*} status: 'browse' / 'edit'
 * @param {*} target: blank 打开新页签
 */
export function openAssetCardByPk(self,props, pk_card, appcode, pagecode, url, status = 'browse') {
	let cardcode = '#201201504A##201201508A##201201512A##201201516A#'; //和资产卡片有关的appcode
	if (appcode) {
		if (cardcode.includes(`#${appcode}#`)) {
			//说明时联查卡片
			ajax({
				url: '/nccloud/fa/facard/linkQueryFaCard.do',
				data: { id: pk_card },
				success: (res) => {
					if (res.data) {
						let linkData = res.data;
						linkData['status'] = status;
						linkData['scene'] = 'linksce'; // 设置联查场景
						props.openTo(linkData['url'], linkData);
					} else {
						toast({ content: self.state.json['200260-0052'], color: 'warning' });//无权限
					}
				},
				error: (err) => {

					toast({ content: err.message, color: 'danger' });
				}
			});
		} else {
			//联查单据
			props.openTo(url, {
				appcode: appcode,
				pagecode: pagecode,
				id: pk_card,
				status
			});
		}
	}
}
