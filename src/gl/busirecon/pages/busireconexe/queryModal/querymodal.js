import React, { Component } from 'react';
import {high,base,ajax,getMultiLang} from 'nc-lightapp-front';
// import moment from 'moment';
// import zhCN from 'rc-calendar/lib/locale/zh_CN';
import { CheckboxItem,RadioItem,TextAreaItem,ReferItem,SelectItem,InputItem,DateTimePickerItem} from '../../../../public/components/FormItems';
const { Refer} = high;
import { toast } from '../../../../public/components/utils.js';
import createScript from '../../../../public/components/uapRefer.js';

// const format = 'YYYY-MM-DD';
const { NCFormControl: FormControl, NCDatePicker: DatePicker, NCButton: Button, NCRadio: Radio, NCBreadcrumb: Breadcrumb,
    NCRow: Row, NCCol: Col, NCTree: Tree, NCIcon: Icon, NCLoading: Loading, NCTable: Table, NCSelect: Select,
    NCCheckbox: Checkbox, NCNumber, AutoComplete, NCDropdown: Dropdown, NCPanel: Panel,NCModal:Modal,NCForm,NCDiv
} = base;
import '../index.less';
import getAssDatas from "../../../../public/components/getAssDatas/index.js";
import checkMustItem from "../../../../public/common/checkMustItem.js";
const {  NCFormItem:FormItem } = NCForm;
const config={
    "isDataPowerEnable":'Y',
    "DataPowerOperationCode":'fi'
};
// const dateInputPlaceholder = '选择日期';


  const defaultProps12 = {
    prefixCls: "bee-table",
    multiSelect: {
      type: "checkbox",
      param: "key"
    }
  };


export default class SearchModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json:{},
            moduleid:'',//业务系统过滤
            pk_accountingbookArr:[],//多选账簿
            showModal:false,
            isShowUnit:false,//是否显示业务单元
            pk_accperiodscheme:'',//会计期间方案
            selectionStateStatus:true,//是否显示按期间查询还是按日期查询单选框
            year_month_Disabled:false,//开始期间结束期间是否可用
            beginEndDateDisabled:true,//日期范围是否可用\\
            modalDefaultValue:{},//默认数据
            assData: [//辅助核算信息
            ],
            loadData:[],//查询模板加载数据
            listItem:{},//模板数据对应值
            checkedAll:false,
            checkedArray: [
                false,
                false,
                false,
            ],
           // columns10:this.columns10,
            columns10:[]
        };
        this.close = this.close.bind(this);
    }
    componentWillMount(){
		let callback= (json) =>{
            this.columns10=[
                {
                  title:json['200260-0031'],//核算类型
                  dataIndex: "checktypename",
                  key: "checktypename",
                  width: "30%",
                  render: (text, record, index) => {
                    return <span>{record.checktypename}</span>;
                    }
                },
                {
                  title: json['200260-0032'],//核算内容
                  dataIndex: "checkvaluename",
                  key: "checkvaluename",
                    render: (text, record, index) => {
                        let defaultValue = { refname: record.checkvaluename, refpk: record.pk_Checkvalue };
                        let referUrl= record.refnodename+'.js'; 
                        if(!this.state[record.pk_accassitem]){
                            {createScript.call(this,referUrl,record.pk_accassitem)}
                            return <div />
                        }else{
                            return (
                            <FormItem
                                inline={true}
                                showMast={true}
                                labelXs={2} labelSm={2} labelMd={2}
                                xs={10} md={10} sm={10}
                                //labelName={record.itemName}
                                isRequire={true}
                                method="change"
                               >
                               {this.state[record.pk_accassitem]?(this.state[record.pk_accassitem])(
                                   {
                                       value:defaultValue,
                                       //isMultiSelectedEnabled:true,
                                       queryCondition:() => {
                                           return Object.assign({
                                            "pk_accountingbook": record.pk_accountingbook
                                           },config)
                                        },
                                        onChange:(v)=>{
                                            let { assData } =this.state;
                                            let originData = this.findByKey(record.key, assData);
                                            if (originData) {
                                            originData.checkvaluename = v.refname;
                                            originData.pk_Checkvalue = v.refpk;
                                            originData.checkvaluecode=v.refcode;
                                            }
                                            this.setState({
                                            assData
                                            })
                                        }
                                    }
                                ):<div />}
                            </FormItem>);
                        }
                  }
                }
            ];
            this.setState({
              json:json,
              columns10:this.columns10
				},()=>{
				//initTemplate.call(this, this.props);
            })
        }
		getMultiLang({moduleId:['200260','publiccommon'],domainName:'gl',currentLocale:'simpchn',callback});
    }
    // componentDidMount(){
    componentWillReceiveProps (nextProp) {
        let {loadData,showOrHide,modalDefaultValue}=nextProp;//this.props;
        let self=this;
        let { listItem,showModal,isShowUnit,pk_accountingbookArr,moduleid,pk_accperiodscheme }=self.state;
        if (showOrHide&&self.state.loadData.length==0) {
            moduleid=modalDefaultValue.moduleid;
            pk_accperiodscheme=modalDefaultValue.pk_accperiodscheme;
            loadData.forEach((item,i)=>{
                let  key;
                if(item.itemType=='refer'){
                    if(item.itemKey=='pk_accasoa'){
                        key=[{
                            display:'',
                            value:'',
                            isMustItem:item.isMustItem,
                            itemName:item.itemName
                        }]
                    }else if(item.itemKey=='pk_accountingbook'){
                        key=[{
                            display:modalDefaultValue.pk_accountingbook.display,
                            value:modalDefaultValue.pk_accountingbook.value,
                            isMustItem:item.isMustItem,
                            itemName:item.itemName
                        }]
                    }else if(item.itemKey=='year_month_begin'||item.itemKey=='year_month_end'){
                        key={
                            display:modalDefaultValue.bizPeriod,
                            value:modalDefaultValue.bizPeriod,
                            isMustItem:item.isMustItem,
                            itemName:item.itemName
                        }
                    }else{
                        key={
                            display:'',
                            value:'',
                            isMustItem:item.isMustItem,
                            itemName:item.itemName
                        }
                    } 
                }else if(item.itemType=='select'||item.itemType=='Dbselect'||item.itemType=='radio'){//下拉框赋初始值
                    key={
                        value:item.itemChild[0].value
                    }
                }else if(item.itemType=='checkbox'){
                    if(item.itemKey=='isOpenUpBusiUnit'){
                        key={
                            value:modalDefaultValue.isOpenUpBusiUnit
                        }
                    }else{
                        key={
                            value:''
                        }
                    }
                }else{
                    key={
                        value:''
                    }
                    if(item.itemType=='date'||item.itemType=='Dbdate'){
                        key={
                        //    value:moment().format("YYYY-MM-DD")
                        value:''
                        }
                    }
                }
                if(item.itemType=='Dbdate'||item.itemType=='DbtextInput'){
                    item.itemKey.map((k,index)=>{
                        let name= k;
                        listItem[name]=key
                    });
                }else{
                    let name= item.itemKey;
                    listItem[name]=key
                }
            
            })
            listItem.beginDate={value:modalDefaultValue.begindate};
            listItem.endDate={value:modalDefaultValue.enddate};
            pk_accountingbookArr.push(modalDefaultValue.pk_accountingbook.value);
            listItem.acccode=[];
            listItem.accname=[];
            self.setState({
                modalDefaultValue:modalDefaultValue,
                isShowUnit:modalDefaultValue.isShowUnit,
                loadData:loadData,
                showModal:showOrHide,
                listItem,pk_accountingbookArr,moduleid,pk_accperiodscheme
            })
        }else{
            self.setState({
                showModal:showOrHide
            })
        }
    }

    //表格操作根据key寻找所对应行
	findByKey(key, rows) {
		let rt = null;
		let self = this;
		rows.forEach(function(v, i, a) {
			if (v.key == key) {
				rt = v;
			}
		});
		return rt;
	}

    close() {
        this.props.handleClose();
    }

    confirm=()=>{
        let { listItem,assData } =this.state;
        let checkStatus=checkMustItem(listItem);//必输项校验
        if(!checkStatus.flag){
            toast({content:checkStatus.info+this.state.json['publiccommon-000001'],color:'warning'});
            return false;
        }
        if(listItem['year_month_begin'].value.split('-')[0]!=listItem['year_month_end'].value.split('-')[0]){
            toast({content:this.state.json['publiccommon-000003'],color:'warning'});//不支持跨年会计期间查询
            return false;
        }
        listItem.ass=assData;
        this.props.onConfirm(listItem);
    }

    queryList=(data)=>{
        let self=this;
        let dateInputPlaceholder = this.state.json['200260-0039'];//选择日期
        function getNowFormatDate() {
            let date = new Date();
            let seperator1 = "-";
            let year = date.getFullYear();
            let month = date.getMonth() + 1;
            let strDate = date.getDate();
            if (month >= 1 && month <= 9) {
                month = "0" + month;
            }
            if (strDate >= 0 && strDate <= 9) {
                strDate = "0" + strDate;
            }
            let currentdate = year + seperator1 + month + seperator1 + strDate;
            return currentdate;
        }
        let currrentDate = getNowFormatDate();
        let { listItem,isShowUnit,pk_accperiodscheme,assData,selectionStateStatus,year_month_Disabled,
            beginEndDateDisabled,modalDefaultValue,moduleid,pk_accountingbookArr } =self.state;
			return data.length!=0?(
				data.map((item, i) => {                   
                   switch (item.itemType) {
                        case 'refer':
                            let referUrl= item.config.refCode+'/index.js';
                            let DBValue=[];
                            let defaultValue={}
                            if(listItem[item.itemKey].length){                           
                                listItem[item.itemKey].map((item,index)=>{
                                    DBValue[index]={ refname: item.display, refpk:item.value };
                                })
                            }else{
                                defaultValue = { refname: listItem[item.itemKey].display, refpk: listItem[item.itemKey].value };  
                            }
                            if(!self.state[item.itemKey]){
                                {createScript.call(self,referUrl,item.itemKey)}
                            return <div />
                            }else{
                                if(item.itemKey=='pk_accountingbook'){
                                    return (
                                        <FormItem
                                            inline={true}
                                            showMast={item.isMustItem}
                                            labelXs={2} labelSm={2} labelMd={2}
                                            xs={10} md={10} sm={10}
                                            labelName={item.itemName}
                                            isRequire={true}
                                            method="change"
                                            >
                                            {self.state[item.itemKey]?(self.state[item.itemKey])(
                                                {
                                                    fieldid:item.itemKey,
                                                    value:DBValue,
                                                    isMultiSelectedEnabled:true,
                                                    disabledDataShow:true,
                                                    queryCondition:() => {
                                                        return Object.assign({
                                                            "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                                                            "appcode":modalDefaultValue.appcode
                                                        },config)
                                                    },
                                                    onChange:(v)=>{
                                                        if(v&&v.length>0){
                                                            //判断是否起用业务单元
                                                            let url = '/nccloud/gl/voucher/queryBookCombineInfo.do';
                                                            let pk_accpont = {"pk_accountingbook":v[0].refpk}
                                                            ajax({
                                                                url:url,
                                                                data:pk_accpont,
                                                                async:false,
                                                                success: function(response){
                                                                    const { success } = response;
                                                                    //渲染已有账表数据遮罩
                                                                    if (success) {
                                                                        if(response.data){
                                                                            isShowUnit=response.data.isShowUnit;
                                                                            listItem['isOpenUpBusiUnit'].value=response.data.isShowUnit?'Y':'N';
                                                                            listItem['year_month_begin'].value = response.data.bizPeriod;
                                                                            listItem['year_month_begin'].display = response.data.bizPeriod;
                                                                            listItem['year_month_end'].display = response.data.bizPeriod;
                                                                            listItem['year_month_end'].value = response.data.bizPeriod;
                                                                            listItem['beginDate'].value=response.data.begindate;
                                                                            listItem['endDate'].value=response.data.enddate;
                                                                            pk_accperiodscheme=response.data.pk_accperiodscheme;
                                                                        }
                                                                        self.setState({
                                                                            isShowUnit,listItem,pk_accperiodscheme
                                                                        })
                                                                    }   
                                                                }
                                                            });
                                                        }
                                                        if(v.length>1){//当选取值多于一个，按业务单元展开和业务单元参照不显示。
                                                            isShowUnit=false;
                                                        }
                                                        listItem[item.itemKey]=[];
                                                        pk_accountingbookArr=[];
                                                        v.map((arr,index)=>{
                                                            let accountingbook={
                                                                display:arr.refname,
                                                                value:arr.refpk
                                                            }
                                                            pk_accountingbookArr.push(arr.refpk);
                                                            listItem[item.itemKey].push(accountingbook);
                                                        })
                                                        if(!v||v.length==0){//清空账簿
                                                            listItem[item.itemKey]=[{display:'',value:'',isMustItem:true,itemName:this.state.json['200260-0014']}];
                                                        }
                                                        //情况对账规则
                                                        listItem['busirule']=[{display:'',value:'',isMustItem: false,itemName: this.state.json['200260-0017']}];
                                                        // listItem[item.itemKey].value = v.refpk
                                                        // listItem[item.itemKey].display = v.refname
                                                        this.setState({
                                                            listItem,isShowUnit,pk_accountingbookArr
                                                        })
                                                    }
                                                }
                                            ):<div />}
                                    </FormItem>);
                                }else if(item.itemKey=='pk_accasoa'){
                                    return (
                                        <FormItem
                                            inline={true}
                                            showMast={item.isMustItem}
                                            labelXs={2} labelSm={2} labelMd={2}
                                            xs={10} md={10} sm={10}
                                            labelName={item.itemName}
                                            isRequire={true}
                                            method="change"
                                        >
                                        {self.state[item.itemKey]?(self.state[item.itemKey])(
                                            {
                                                fieldid:item.itemKey,
                                                value:DBValue,
                                                isMultiSelectedEnabled:true,
                                                queryCondition:() => {
                                                    if(item.itemKey=='pk_accasoa'){
                                                        return Object.assign({
                                                            "refName":item.itemName,
                                                            "pk_accountingbook": listItem.pk_accountingbook[0].value? listItem.pk_accountingbook[0].value:'',
                                                            "versiondate":currrentDate,
                                                            "TreeRefActionExt":'nccloud.web.gl.verify.action.VerifyObjectRefSqlBuilder'
                                                            
                                                        },config)
                                                    }
                                                },
                                                onChange:(v)=>{
                                                    
                                                    listItem[item.itemKey]=[];
                                                    listItem.acccode=[];
                                                    listItem.accname=[];
                                                    //listItem[item.itemKey].shift();
                                                    v.map((arr,index)=>{
                                                        listItem.acccode.push(arr.refcode);
                                                        listItem.accname.push(arr.refname);
                                                        let accasoa={
                                                            display:arr.refname,
                                                            value:arr.refpk
                                                        }
                                                        listItem[item.itemKey].push(accasoa);
                                                    })
                                                    //根据选定的pk 实现过滤
                                                    if(item.itemKey!='pk_accountingbook'&&!listItem.pk_accountingbook[0].value){
                                                        toast({ content: this.state.json['200260-0033'], color: 'warning' });//请先选择核算账簿
                                                        return false
                                                    }
                                                    if(v&&v.length>0){
                                                        //请求辅助核算数据
                                                        let url = '/nccloud/gl/voucher/queryAssItem.do';
                                                        let queryData = {
                                                            pk_accasoa: v[0].refpk,
                                                            prepareddate: currrentDate,
                                                        };
                                                        assData=[];
                                                        ajax({
                                                            url:url,
                                                            data:queryData,
                                                            success: function(response){
                                                                const { success } = response;
                                                                //渲染已有账表数据遮罩
                                                                if (success) {
                                                                    if(response.data){
                                                                        if(response.data.length>0){
                                                                            response.data.map((item,index)=>{
                                                                                let obj={
                                                                                    key:index,
                                                                                    "checktypecode":item.code,
                                                                                    "checktypename" :item.name,
                                                                                    "pk_Checktype": item.pk_accassitem,
                                                                                    "refCode":item.refCode?item.refCode:item.code,
                                                                                    "refnodename":item.refnodename,
                                                                                    "pk_accassitem":item.pk_accassitem,
                                                                                    "pk_accountingbook":listItem.pk_accountingbook[0].value
                                                                                }
                                                                                assData.push(obj);
                                                                            })
                                                                        }
                                                                    }
                                                                    self.setState({
                                                                        assData
                                                                    })
                                                                }   
                                                            }
                                                        });
                                                    }
                                                    // listItem[item.itemKey].value = v.refpk
                                                    // listItem[item.itemKey].display = v.refname
                                                    this.setState({
                                                        listItem
                                                    })
                                                }
                                            }
                                        ):<div />}
                                    </FormItem>);
                                }else if(item.itemKey=='pk_units'){
                                    if(isShowUnit){
                                        return (
                                            <FormItem
                                                inline={true}
                                                showMast={item.isMustItem}
                                                labelXs={2} labelSm={2} labelMd={2}
                                                xs={10} md={10} sm={10}
                                                labelName={item.itemName}
                                                isRequire={true}
                                                method="change"
                                            >
                                                {self.state[item.itemKey]?(self.state[item.itemKey])(
                                                    {
                                                        fieldid:item.itemKey,
                                                        value:DBValue,
                                                        isMultiSelectedEnabled:true,
                                                        queryCondition:() => {
                                                            return Object.assign({
                                                                "pk_accountingbook": listItem.pk_accountingbook[0].value//self.state.pk_accountingbook.value
                                                            },config)
                                                        },
                                                        onChange:(v)=>{
                                                            //根据选定的pk 实现过滤
                                                            if(item.itemKey!='pk_accountingbook'&&!listItem.pk_accountingbook[0].value){
                                                                toast({ content: this.state.json['200260-0033'], color: 'warning' });//请先选择核算账簿
                                                                return false
                                                            }
                                                            // if(v.length<=0){return false;}
                                                            listItem[item.itemKey]=[];
                                                            v.map((arr,index)=>{
                                                                let accasoa={
                                                                    display:arr.refname,
                                                                    value:arr.refpk
                                                                }
                                                                listItem[item.itemKey].push(accasoa);
                                                            })
                                                            // listItem[item.itemKey].value = v.refpk
                                                            // listItem[item.itemKey].display = v.refname
                                                            this.setState({
                                                                listItem
                                                            })
                                                        }
                                                    }
                                                ):<div />}
                                        </FormItem>
                                        );
                                    }else{
                                        return(<div/>)
                                    }
                                    
                                }else if(item.itemKey=='year_month_begin'){
                                    return (
                                        <FormItem
                                            inline={true}
                                            showMast={item.isMustItem}
                                            labelXs={2} labelSm={2} labelMd={2}
                                            xs={4} md={4} sm={4}
                                            labelName={item.itemName}
                                            isRequire={true}
                                            // disabled={year_month_Disabled}
                                            method="change"
                                            >
                                            {self.state[item.itemKey]?(self.state[item.itemKey])(
                                                {
                                                    fieldid:item.itemKey,
                                                    value:defaultValue,
                                                    disabled:year_month_Disabled,
                                                    queryCondition:() => {
                                                        return {
                                                            "pk_accperiodscheme":pk_accperiodscheme
                                                        }
                                                    },    
                                                    onChange:(v)=>{
                                                        listItem[item.itemKey].value = v.refname?v.refname:'';
                                                        listItem[item.itemKey].display = v.refname?v.refname:'';
                                                        if(v&&v.values&&v.values.begindate){
                                                            listItem.beginDate={value:v.values.begindate.value};
                                                        }else{
                                                            listItem.beginDate={value:''}
                                                        }
                                                        this.setState({
                                                            listItem
                                                        })
                                                    }
                                                }
                                            ):<div />}
                                        </FormItem>
                                    );
                                }else if(item.itemKey=='year_month_end'){
                                    return (
                                        <FormItem
                                            inline={true}
                                            showMast={item.isMustItem}
                                            labelXs={1} labelSm={1} labelMd={1}
                                            xs={5} md={5} sm={5}
                                            // labelName={item.itemName}
                                            isRequire={true}
                                            method="change"
                                            className="year_month_endLable"
                                            >
                                            {self.state[item.itemKey]?(self.state[item.itemKey])(
                                                {
                                                    fieldid:item.itemKey,
                                                    value:defaultValue,
                                                    disabled:year_month_Disabled,  
                                                    queryCondition:() => {
                                                        return {
                                                            "includeAdj":"Y",
                                                            "pk_accperiodscheme":pk_accperiodscheme
                                                        }
                                                    }, 
                                                    onChange:(v)=>{
                                                        listItem[item.itemKey].value = v.refname?v.refname:'';
                                                        listItem[item.itemKey].display = v.refname?v.refname:'';
                                                        listItem.pk_accperiod=v.values&&v.values.pk_accperiod?v.values.pk_accperiod.value:null;
                                                        listItem.pk_accperiodmonth=v.values&&v.values.pk_accperiodmonth?v.values.pk_accperiodmonth.value:null;
                                                        listItem.yearmth=v.values&&v.values.yearmth?v.values.yearmth.value:null;
                                                        if(v&&v.values&&v.values.enddate&&v.values.enddate.value){
                                                            listItem.endDate={value:v.values.enddate.value};
                                                        }else{//调整期
                                                            if(v&&v.refname){
                                                                let url = '/nccloud/gl/glpub/queryDateByPeriod.do';
                                                                let data = {
                                                                    pk_accountingbook:listItem['pk_accountingbook'][0].value?listItem['pk_accountingbook'][0].value:listItem['pk_accountingbook'][0].refpk,
                                                                    period: v.refname
                                                                }
                                                                ajax({
                                                                    url,
                                                                    data,
                                                                    success: (response) => {
                                                                        let {success, data} = response;
                                                                        if(success){
                                                                            listItem['endDate'].value =data.enddate;
                                                                            self.setState({
                                                                                listItem
                                                                            })
                                                                        }
                                                                    },
                                                                    error: (error) => {
                                                                        toast({content: error.message, color: 'warning'})
                                                                    }
                                                                })
                                                            }else{
                                                                listItem['endDate'] = {value:''};
                                                            }
                                                        }
                                                        this.setState({
                                                            listItem
                                                        })
                                                    }
                                                }
                                            ):<div />}
                                        </FormItem>
                                    );
                                }else if(item.itemKey=="busirule"){
                                    return (
                                        <FormItem
                                            inline={true}
                                            showMast={item.isMustItem}
                                            labelXs={2} labelSm={2} labelMd={2}
                                            xs={10} md={10} sm={10}
                                            labelName={item.itemName}
                                            isRequire={true}
                                            method="change"
                                            >
                                            {self.state[item.itemKey]?(self.state[item.itemKey])(
                                                {
                                                    fieldid:item.itemKey,
                                                    value:DBValue,
                                                    isMultiSelectedEnabled:true,
                                                    queryCondition:() => {
                                                        return Object.assign({
                                                            "pk_accountingbook": pk_accountingbookArr.join(),//listItem['pk_accountingbook'][0].value,
                                                            "busisystem": listItem['busisystem'].value,
                                                            "GridRefActionExt":'nccloud.web.gl.ref.BusiRuleRefSqlBuilder'
                                                        },config)                                                     
                                                    },    
                                                    onChange:(v)=>{
                                                        listItem[item.itemKey]=[];
                                                        listItem.curName=[];
                                                        v.map((arr,index)=>{
                                                            listItem.curName.push(arr.refname);
                                                            let accasoa={
                                                                display:arr.refname,
                                                                value:arr.refpk
                                                            }
                                                            listItem[item.itemKey].push(accasoa);
                                                        })
                                                        this.setState({
                                                            listItem
                                                        })
                                                    }
                                                }
                                            ):<div />}
                                    </FormItem>);
                                }else if(item.itemKey=='busisystem'){//业务系统
                                    return (
                                        <FormItem
                                            inline={true}
                                            showMast={item.isMustItem}
                                            labelXs={2} labelSm={2} labelMd={2}
                                            xs={10} md={10} sm={10}
                                            labelName={item.itemName}
                                            isRequire={true}
                                            method="change"
                                            >
                                            {self.state[item.itemKey]?(self.state[item.itemKey])(
                                                {
                                                    fieldid:item.itemKey,
                                                    value:defaultValue,
                                                    //isMultiSelectedEnabled:true,
                                                    queryCondition:() => {
                                                        if(moduleid&&moduleid!=''){
                                                            return Object.assign({
                                                                "moduleid":moduleid,
                                                                "appcode": '200260SETO',//按照组织级过滤
                                                                "GridRefActionExt":'nccloud.web.gl.ref.BRSystemRefSqlBuilder'
                                                            },config)
                                                        }else{
                                                            return Object.assign({
                                                                "appcode": '200260SETO',//按照组织级过滤
                                                                "GridRefActionExt":'nccloud.web.gl.ref.BRSystemRefSqlBuilder'
                                                                //"pk_accountingbook": self.state.pk_accountingbook.value
                                                            },config)
                                                        }
                                                        
                                                    },    
                                                    onChange:(v)=>{
                                                        
                                                        if(v.values&&(v.values['dap_dapsystem.systypecode'].value=='FTS'||v.values['dap_dapsystem.systypecode'].value=='CMP')){
                                                            selectionStateStatus=true;
                                                            year_month_Disabled=false;
                                                            beginEndDateDisabled=true;
                                                        }else{
                                                            selectionStateStatus=false;
                                                            year_month_Disabled=false;
                                                            beginEndDateDisabled=true;
                                                        }
                                                        if(v.values){
                                                            listItem.appcode=v.values.appcode.value?v.values.appcode.value:'';
                                                            listItem.pagecode=v.values.pagecode.value?v.values.pagecode.value:'';
                                                            listItem.url=v.values.url.value?v.values.url.value:'';
                                                            listItem[item.itemKey].value = v.values['dap_dapsystem.systypecode'].value;
                                                            listItem[item.itemKey].display = v.refname;
                                                        }else{
                                                            listItem.appcode='';
                                                            listItem.pagecode='';
                                                            listItem.url='';
                                                            listItem[item.itemKey].value ='';
                                                            listItem[item.itemKey].display ='';
                                                        }
                                                        listItem['busirule']=[
                                                            {
                                                            display:'',
                                                            value:'',
                                                            isMustItem:false,
                                                            itemName:this.state.json['200260-0017']
                                                        }
                                                        ];
                                                        this.setState({
                                                            listItem,selectionStateStatus,year_month_Disabled,
                                                            beginEndDateDisabled
                                                        })
                                                    }
                                                }
                                            ):<div />}
                                        </FormItem>
                                    );
                                }else{
                                    return (
                                        <FormItem
                                            inline={true}
                                            showMast={item.isMustItem}
                                            labelXs={2} labelSm={2} labelMd={2}
                                            xs={10} md={10} sm={10}
                                            labelName={item.itemName}
                                            isRequire={true}
                                            method="change"
                                            >
                                            {self.state[item.itemKey]?(self.state[item.itemKey])(
                                                {
                                                    fieldid:item.itemKey,
                                                    value:defaultValue,
                                                    //isMultiSelectedEnabled:true,
                                                    queryCondition:() => {
                                                        return Object.assign({
                                                            //"pk_accountingbook": self.state.pk_accountingbook.value
                                                        },config)
                                                    },    
                                                    onChange:(v)=>{
                                                        listItem[item.itemKey].value = v.refpk
                                                        listItem[item.itemKey].display = v.refname
                                                        this.setState({
                                                            listItem
                                                        })
                                                    }
                                                }
                                            ):<div />}
                                        </FormItem>
                                    );
                                }  
                            }
                            break;
                        case 'date':
                            return (
                                    <FormItem
                                        inline={true}
                                        showMast={item.isMustItem}
                                        labelXs={2}
                                        labelSm={2}
                                        labelMd={2}
                                        xs={10}
                                        md={10}
                                        sm={10}
                                        labelName={item.itemName}
                                        method="blur"
                                        inputAlfer="%"
                                        errorMessage={this.state.json['200260-0034']}//输入格式错误
                                    >
                                        <DatePicker
                                            fieldid={item.itemKey}
                                            name={item.itemKey}
                                            type="customer"
                                            isRequire={true}
                                            //format={format}
                                            //disabled={isChange}
                                            //value={moment(listItem[item.itemKey].value)}
                                            //locale={zhCN}
                                            value={listItem[item.itemKey].value}
                                            onChange={(v) => {
                                                listItem[item.itemKey].value = v;
                                                this.setState({
                                                    listItem
                                                })
                                            }}
                                            placeholder={dateInputPlaceholder}
                                            />
                                        </FormItem>
                            );
                        case'Dbdate':
                            return(
                                // <Row>
                                // <Col xs={12} md={12} sm={12} className="dateMargin">
                                <FormItem
                                    inline={true}
                                    showMast={item.isMustItem}
                                    labelXs={2} labelSm={2} labelMd={2}
                                    xs={10} md={10} sm={10}
                                    labelName={item.itemName}
                                    method="blur"
                                    inputAlfer="%"
                                    errorMessage={this.state.json['200260-0034']}//输入格式错误
                                    inputAfter={
                                        // <Col>
                                        // <span className="online">&nbsp;--&nbsp;</span>
                                        <DatePicker
                                        fieldid="endDate"
                                        name={item.itemKey}
                                        type="customer"
                                        isRequire={true}
                                        disabled={beginEndDateDisabled}
                                        value={listItem.endDate.value}
                                        onChange={(v) => {
                                            listItem.endDate={value: v}
                                            this.setState({
                                                listItem
                                            })
                                        }}
                                        placeholder={dateInputPlaceholder}
                                        />
                                        // </Col>
                                    }
                                    >
                                    <DatePicker
                                        fieldid="beginDate"
                                        name={item.itemKey}
                                        type="customer"
                                        isRequire={true}
                                        disabled={beginEndDateDisabled}
                                        value={listItem.beginDate.value}
                                        onChange={(v) => {
                                            listItem.beginDate={value: v}
                                            this.setState({
                                                listItem
                                            })
                                        }}
                                        placeholder={dateInputPlaceholder}
                                    />
                                </FormItem>
                                // </Col>
                                // </Row>
                            );
                        case 'textInput':
                            return (
                                <FormItem
                                    inline={true}
                                    showMast={item.isMustItem}
                                    labelXs={2}
                                    labelSm={2}
                                    labelMd={2}
                                    xs={10}
                                    md={10}
                                    sm={10}
                                    labelName={item.itemName}
                                    isRequire={true}
                                    method="change"
                                >
                                    <FormControl
                                        fieldid={item.itemKey}
                                        value={listItem[item.itemKey].value}
                                        onChange={(v) => {
                                            listItem[item.itemKey].value = v
                                            this.setState({
                                                listItem
                                            })
                                        }}
                                    />
                                </FormItem>
                            );
                        case 'DbtextInput':
                            return(
                            // <Row>
                            //     <Col xs={8}  md={8} sm={8} className="dateMargin labelMargin">
                                <FormItem
                                    inline={true}
                                    showMast={item.isMustItem}
                                   labelXs={2}  labelSm={2} labelMd={2}
                                    xs={10}  md={10} sm={10}
                                    labelName={item.itemName}
                                    isRequire={true}
                                    method="change"
                                    inputAfter={
                                    <Col xs={12} md={12} sm={12}>
                                        <span className="online">&nbsp;--&nbsp;</span>                                                                          
                                        <FormControl
                                            fieldid={item.itemKey[1]}
                                            value={listItem[item.itemKey[1]].value}
                                            //disabled={true}
                                        // onFocus={this.focus}
                                            onChange={(v) => {
                                                let startkey=item.itemKey[0];
                                                let endkey=item.itemKey[1];
                                                // listItem[item.itemKey].value = v
                                                listItem[endkey]={value : v}
                                                this.setState({
                                                    listItem
                                                })
                                            }}
                                        />
                                    </Col>}
                                >
                                    <FormControl
                                        fieldid={item.itemKey[0]}
                                        value={listItem[item.itemKey[0]].value}
                                        //disabled={true}
                                    // onFocus={this.focus}
                                        onChange={(v) => {
                                            listItem[item.itemKey[0]]={value : v}
                                            this.setState({
                                                listItem
                                            })
                                        }}
                                    />
                                </FormItem>
                            //     </Col>                                
                            // </Row>
                            );
                        case 'radio':
                            if(item.itemKey=="selectionState"){
                                if(selectionStateStatus){
                                    return(
                                        <FormItem
                                        inline={true}
                                        // showMast={item.isMustItem}
                                        labelXs={2}
                                        labelSm={2}
                                        labelMd={2}
                                        xs={10}
                                        md={10}
                                        sm={10}
                                        labelName={item.itemName}
                                        isRequire={true}
                                        method="change"
                                    //  change={self.handleGTypeChange.bind(this, 'contracttype')}
                                    >
                                        <RadioItem
                                            fieldid={item.itemKey}
                                            name={item.itemKey}
                                            type="customer"
                                            defaultValue={listItem[item.itemKey].value}
                                            items={() => {
                                                return (item.itemChild) 
                                            }}
                                            onChange={(v)=>{
                                                if(v=='1'){
                                                    year_month_Disabled=true;
                                                    beginEndDateDisabled=false;
                                                }else if(v=='0'){
                                                    year_month_Disabled=false;
                                                    beginEndDateDisabled=true;
                                                }
                                                listItem[item.itemKey].value = v
                                                    this.setState({
                                                        listItem,
                                                        year_month_Disabled,
                                                        beginEndDateDisabled
                                                    })
                                            }}
                                        />
                                    </FormItem>
                                    )
                                }else{
                                    return(<div/>)
                                }
                            }else{
                                return(
                                    <FormItem
                                    inline={true}
                                    // showMast={item.isMustItem}
                                    labelXs={2}
                                    labelSm={2}
                                    labelMd={2}
                                    xs={10}
                                    md={10}
                                    sm={10}
                                    labelName={item.itemName}
                                    isRequire={true}
                                    method="change"
                                //  change={self.handleGTypeChange.bind(this, 'contracttype')}
                                >
                                    <RadioItem
                                        fieldid={item.itemKey}
                                        name={item.itemKey}
                                        type="customer"
                                        defaultValue={listItem[item.itemKey].value}
                                        items={() => {
                                            return (item.itemChild) 
                                        }}
                                        onChange={(v)=>{
                                            listItem[item.itemKey].value = v
                                                this.setState({
                                                    listItem
                                                })
                                        }}
                                    />
                                </FormItem>
                                )  
                            }
                            break;
                        case 'select':
                            return(
                                <FormItem
                                inline={true}
                                // showMast={item.isMustItem}
                                labelXs={2}
                                labelSm={2}
                                labelMd={2}
                                xs={10}
                                md={10}
                                sm={10}
                                labelName={item.itemName}
                                isRequire={true}
                                method="change"
                            >
                                <SelectItem 
                                    name={item.itemKey}
                                    fieldid={item.itemKey}
                                    defaultValue={listItem[item.itemKey].value} 
                                        items = {
                                            () => {
                                                return (item.itemChild) 
                                            }
                                        }
                                        onChange={(v)=>{

                                            listItem[item.itemKey].value = v
                                                this.setState({
                                                    listItem
                                                })
                                        }}
                            />
                            </FormItem>)
                            case 'Dbselect':
                            return(
                                <FormItem
                                inline={true}
                                // showMast={item.isMustItem}
                                labelXs={2}
                                labelSm={2}
                                labelMd={2}
                                xs={2}
                                md={2}
                                sm={2}
                                labelName={item.itemName}
                                isRequire={true}
                                method="change"
                            >
                                <SelectItem 
                                    name={item.itemKey}
                                    fieldid={item.itemKey}
                                    defaultValue={listItem[item.itemKey].value} 
                                        items = {
                                            () => {
                                                return (item.itemChild) 
                                            }
                                        }
                                        onChange={(v)=>{
                                            listItem[item.itemKey].value = v
                                            this.setState({
                                                listItem
                                            })
                                        }}
                            />
                            </FormItem>)
                        case 'checkbox':
                            if(item.itemKey=="isOpenUpBusiUnit"){
                                if(isShowUnit){
                                    return(<FormItem
                                        inline={true}
                                        showMast={item.isMustItem}
                                        labelXs={2} labelSm={2} labelMd={2}
                                        xs={10}  md={10} sm={10}
                                        labelName={item.itemName}
                                        // isRequire={true}
                                        className='checkboxStyle'
                                        method="change"
                                    >
                                        <CheckboxItem
                                            fieldid={item.itemKey} 
                                            name={item.itemKey} 
                                            defaultValue={listItem[item.itemKey].value} 
                                            boxs = {
                                                () => {
                                                    if(listItem[item.itemKey].value=='Y'){
                                                        item.itemChild[0].checked=true;
                                                    }
                                                    return (item.itemChild) 
                                                }
                                            }
                                            onChange={(v)=>{

                                                listItem[item.itemKey].value = (v[0].checked==true)?'Y':'N';
                                                this.setState({
                                                    listItem
                                                })
                                            }}
                                        />
                                    </FormItem>)
                                }else{
                                    return(<div/>)
                                }
                            }else{
                                return(
                                    <FormItem
                                    inline={true}
                                    showMast={item.isMustItem}
                                    labelXs={2} labelSm={2} labelMd={2}
                                    xs={10}  md={10} sm={10}
                                    labelName={item.itemName}
                                    // isRequire={true}
                                    className='checkboxStyle'
                                    method="change"
                                >
                                    <CheckboxItem 
                                        fieldid={item.itemKey}
                                        name={item.itemKey} 
                                    //defaultValue={this.state.periodloan.value} 
                                        boxs = {
                                            () => {
                                                return (item.itemChild) 
                                            }
                                        }
                                        onChange={(v)=>{

                                            listItem[item.itemKey].value = (v[0].checked==true)?'Y':'N';
                                            this.setState({
                                                listItem
                                            })
                                        }}
                                    />
                                </FormItem>
                                )
                            }
                            break;
                        default:
                        break;
                   }
               })
			):<div />;
           
    }


    onAllCheckChange = () => {
        let self = this;
        let checkedArray = [];
        let listData = self.state.data.concat();
        let selIds = [];
        for (var i = 0; i < self.state.checkedArray.length; i++) {
          checkedArray[i] = !self.state.checkedAll;
        }
        self.setState({
          checkedAll: !self.state.checkedAll,
          checkedArray: checkedArray,
        });
      };
      onCheckboxChange = (text, record, index) => {
        let self = this;
        let allFlag = false;
        let checkedArray = self.state.checkedArray.concat();
        checkedArray[index] = !self.state.checkedArray[index];
        for (var i = 0; i < self.state.checkedArray.length; i++) {
          if (!checkedArray[i]) {
            allFlag = false;
            break;
          } else {
            allFlag = true;
          }
        }
        self.setState({
          checkedAll: allFlag,
          checkedArray: checkedArray,
          // selIds: selIds
        });
        // self.props.onSelIds(selIds);
      };
      
      renderColumnsMultiSelect(columns) {
        const {checkedArray } = this.state;
        const { multiSelect } = this.props;
        let select_column = {};
        let indeterminate_bool = false;
        // let indeterminate_bool1 = true;
        if (multiSelect && multiSelect.type === "checkbox") {
          let i = checkedArray.length;
          while(i--){
              if(checkedArray[i]){
                indeterminate_bool = true;
                break;
              }
          }
          let defaultColumns = [
            {
              title: (
                <Checkbox
                  className="table-checkbox"
                  checked={this.state.checkedAll}
                  indeterminate={indeterminate_bool&&!this.state.checkedAll}
                  onChange={this.onAllCheckChange}
                />
              ),
              key: "checkbox",
              dataIndex: "checkbox",
              width: "5%",
              render: (text, record, index) => {
                return (
                  <Checkbox
                    className="table-checkbox"
                    checked={this.state.checkedArray[index]}
                    onChange={this.onCheckboxChange.bind(this, text, record, index)}
                  />
                );
              }
            }
          ];
          columns = defaultColumns.concat(columns);
        }
        return columns;
      }

    render() {
        let{showOrHide}=this.props;
        let { loadData,assData,columns10} =this.state;
        let columnsldad = this.renderColumnsMultiSelect(columns10);
        const emptyFunc = () => <span>{this.state.json['200260-0035']}</span>//这里没有数据!
        return (
            <div className="fl">
                <Modal
                    fieldid="query"
                    className={'senior'}
                    id="busirveiry"
                    show={showOrHide }
                    // backdrop={ this.state.modalDropup }
                    onHide={ this.close }
                    animation={true}
                    >
                    <Modal.Header closeButton fieldid="header-area">
                        <Modal.Title>{this.state.json['200260-0036']}</Modal.Title>
                    </Modal.Header >
                    <Modal.Body >
                        <NCDiv fieldid="query" areaCode={NCDiv.config.FORM} className="nc-theme-form-label-c">
                            <NCForm useRow={true}
                                submitAreaClassName='classArea'
                                showSubmit={false}　>
                                {this.queryList(loadData)}
                            </NCForm>
                        </NCDiv>
                        {/* <Table
                            columns={columnsldad} data={assData}
                            emptyText={emptyFunc}
                        /> */}
                        {/* {GetAssidData(this,assData)} */}
                    </Modal.Body>
                    <Modal.Footer fieldid="bottom_area">
                        {/* 确定 */}
                        <Button colors="primary" onClick={ this.confirm } fieldid="confirm">{this.state.json['200260-0006']}</Button>
                        {/* 取消 */}
                        <Button onClick={ this.close } fieldid="close">{this.state.json['200260-0038']}</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}
SearchModal.defaultProps = defaultProps12;