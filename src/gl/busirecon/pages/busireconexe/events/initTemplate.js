import { createPage, ajax, base, toast,cacheTools,print } from 'nc-lightapp-front';
let { NCPopconfirm, NCIcon } = base;

let searchId = 'busirecon_query';
// const pageCode = '200260EXEPAGE';
export default function (props) {

	let appcode =props.getSearchParam('c');
	let pageCode=props.getSearchParam('p');
	ajax({
		url: '/nccloud/platform/appregister/queryallbtns.do',
		data: {
			pagecode: pageCode,
			appcode: appcode//小应用id
		},
		async:false,
		success: function(res) {
			if (res.data) {
				let button = res.data;
					props.button.setButtons(button, () => {
						props.button.setButtonDisabled(['print', 'link','refresh','switch'], true);
						// props.button.setButtonVisible(['print', 'link','refresh','switch'], false);
					});
			}
		}
	});
}
