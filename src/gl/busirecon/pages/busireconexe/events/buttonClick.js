import { ajax, base, toast, cacheTools, print, withNav, gzip,viewModel } from 'nc-lightapp-front';
let { setGlobalStorage, getGlobalStorage, removeGlobalStorage } = viewModel;
import { appcode } from '../../card/constants';
import { setData } from '../../../../manageReport/common/simbleTableData.js';
import { queryRelatedAppcode, originAppcode } from '../../../../public/common/queryRelatedAppcode.js';
let tableid = 'gl_brsetting';
// @withNav
export default function buttonClick(props, id) {
    let { querycondition, accountType } = this.state;
    let rowDatas, pk_brsetting, pk_unit, pk_currtype, link;
    let gziptools = new gzip();
    switch (id) {
        case 'refresh'://刷新
            //刷新
            this.handleRefresh();
            break;
        case 'exerecon'://执行对账
            this.setState({
                showModal: true
            })
            break;
        case 'linkgl'://联查总账
            rowDatas = this.getSelectRowData();
            if (!rowDatas || rowDatas[0] == null || rowDatas.length == 0 || rowDatas[0].style == 'head') {
                toast({ content: this.state.json['200260-0028'], color: 'warning' });
                return false;
            }
            link = rowDatas[0].link;
            let querydata = {
                "condition": { ...querycondition },
                "link": link,
                "linkData": link,
                "class": "nc.vo.gl.busirecon.link.GlBusiReconTransferGlQueryVO"
            }
            this.props.openTo('/gl/threedetail/pages/main/index.html',
                {
                    "status": gziptools.zip(JSON.stringify(querydata)),
                    "appcode": queryRelatedAppcode('20023030'),
                    "pagecode": '20023030PAGE'
                }
            )
            break;
        case 'linkbusi'://联查业务账
            rowDatas = this.getSelectRowData();
            if (querycondition.busisystem == 'FTS' || querycondition.busisystem == 'CMP') {
                toast({ content: this.state.json['200260-0027'], color: 'warning' });
                return false;
            }
            if (!rowDatas || rowDatas[0] == null || rowDatas.length == 0 || rowDatas[0].style == 'head') {
                toast({ content: this.state.json['200260-0028'], color: 'warning' });
                return false;
            }
            link = rowDatas[0].link;
            let conditon = {};
            if (querycondition.busisystem == 'CMP' || querycondition.busisystem == 'IA') {
                conditon = {
                    "logic": "and",
                    "conditions": []
                };
                setGlobalStorage('sessionStorage','LinkReport', JSON.stringify(conditon));
                setGlobalStorage('sessionStorage','LinkReportUserDefObj', JSON.stringify(link));
            } else if (querycondition.busisystem == 'FA') {
                conditon = {
                    logic: 'and',
                    conditions: []
                };
                let userObj = {
                    condition: querycondition,
                    linkData: link,
                    class: 'nc.vo.gl.busirecon.link.GlBusiReconTransferBridgeQueryVO',
                    linkType: 'gl',
                    appcode: querycondition.appcode,
                    pagecode: querycondition.pagecode //应付
                };
                setGlobalStorage('sessionStorage','LinkReport', JSON.stringify(conditon));
                setGlobalStorage('sessionStorage','LinkReportUserDefObj', JSON.stringify(userObj));
            }
            this.props.openTo(querycondition.url,
                {
                    condition: gziptools.zip(JSON.stringify(querycondition)),
                    linkData: gziptools.zip(JSON.stringify(link)),
                    class: 'nc.vo.gl.busirecon.link.GlBusiReconTransferBridgeQueryVO',
                    linkType: 'gl',
                    appcode: querycondition.appcode,
                    pagecode: querycondition.pagecode//应付
                }
            )
            break;
        case 'linkrecondim'://联查对账维度

            rowDatas = this.getSelectRowData();
            if (!rowDatas || rowDatas[0] == null || rowDatas.length == 0 || rowDatas[0].style == 'head') {
                toast({ content: this.state.json['200260-0028'], color: 'warning' });
                return false;
            }
            link = rowDatas[0].link;
            this.props.openTo('/gl/busirecon/pages/busireconDim/index.html',
                {
                    condition: gziptools.zip(JSON.stringify(querycondition)),
                    link: gziptools.zip(JSON.stringify(link)),
                    appcode: '200260EXE',
                    pagecode: '200260EXEDIM'
                }
            )
            break;
        case 'recondetail'://联查对账明细
            rowDatas = this.getSelectRowData();
            if (querycondition.busisystem == 'FTS' || querycondition.busisystem == 'CMP') {
                toast({ content: this.state.json['200260-0027'], color: 'warning' });
                return false;
            }
            if (!rowDatas || rowDatas[0] == null || rowDatas.length == 0 || rowDatas[0].style == 'head') {
                toast({ content: this.state.json['200260-0028'], color: 'warning' });//请先选中一行数据再进行联查
                return false;
            }
            link = rowDatas[0].link;
            querycondition.linkaccountingbook=link?link.linkaccountingbook:'';
            this.props.openTo('/gl/busirecon/pages/busireconDetail/index.html',
                {
                    condition: gziptools.zip(JSON.stringify(querycondition)),
                    link: gziptools.zip(JSON.stringify(link)),
                    appcode: '200260EXE',
                    pagecode: '200260EXEVOUCHER'
                }
            )
            break;
        case 'switch'://切换
            let self = this;
            let { resourceDatas, flag } = self.state;
            let data = resourceDatas;
            let ChangecolumnInfo = data.columnInfo;
            if (flag > 0) {
                ChangecolumnInfo = data.columnInfo2;
            } else {
                ChangecolumnInfo = data.columnInfo;
            }
            if (flag >= 1) {
                flag = 0;
            } else {
                flag++;
            }
            if (flag == 0) {
                accountType = 'columnInfo';
                // ChangecolumnInfo=data.columnInfo;
            } else if (flag == 1) {
                accountType = 'columnInfo2';
                // ChangecolumnInfo=data.columnInfo1;
            }
            let renderFirstData = {};
            renderFirstData.columnInfo = data.columnInfo;
            renderFirstData.data = data.busirecon;
            renderFirstData.columnInfo2 = data.columnInfo2;
            self.setState({
                accountType, flag
            }, () => {
                setData(self, renderFirstData);
            })
            break;
        case 'print'://打印
            this.print();
            break;
        default:
            break;

    }
}
