import React, { Component } from 'react';
import { createPage, high, base, ajax, formDownload, deepClone,getMultiLang,createPageIcon } from 'nc-lightapp-front';
import { SimpleTable } from 'nc-report';
const { NCFormControl: FormControl, NCDatePicker: DatePicker, NCButton: Button, NCRadio: Radio, NCBreadcrumb: Breadcrumb,
    NCRow: Row, NCCol: Col, NCTree: Tree, NCMessage: Message, NCIcon: Icon, NCLoading: Loading, NCTable: Table, NCSelect: Select,
    NCCheckbox: Checkbox, NCNumber, AutoComplete, NCDropdown: Dropdown, NCPanel: Panel, NCForm, NCButtonGroup: ButtonGroup, NCAffix,NCTooltip:Tooltip,
} = base;
const { NCFormItem: FormItem } = NCForm;
import { toast } from '../../../public/components/utils.js';
import { CheckboxItem, RadioItem, TextAreaItem, ReferItem, SelectItem, InputItem, DateTimePickerItem } from '../../../public/components/FormItems';
import QueryModal from './queryModal/querymodal.js';
import { buttonClick, initTemplate } from './events';
import reportPrint from '../../../public/components/reportPrint.js';
import getDefaultAccountBook from '../../../public/components/getDefaultAccountBook.js';
import {rowBackgroundColor} from "../../../manageReport/common/htRowBackground";
import { tableDefaultData } from '../../../manageReport/defaultTableData';
import { setData } from '../../../manageReport/common/simbleTableData.js';
import HeaderArea from '../../../public/components/HeaderArea';
import getLangCode from '../../../public/common/getLangCode';
import '../../../public/reportcss/firstpage.less'
//import '../../../public/reachercss/reacher.less'
import './index.less'
import { businessUnit, createReferFn } from "../../../manageReport/common/modules/modules";
const moduleId = '200260';
class Busireconexe extends Component {
    constructor(props) {
        super(props);

        this.state = {
            json:{},
            accountType:'columnInfo',
            flag: 0,//切换表头用的
            showTable: false,
            flow: false,
            resourceDatas: [],//报表数据
            textAlginArr: [],//对其方式
            dataout: tableDefaultData,
            resourveQuery: {},//原始查询条件
            querycondition: {},//查询条件
            queryShow: true,//查询按钮默认显示
            listItem: {
                accountingbookValue: { display: '', value: '' },
                period: { display: '', value: '' }
            },
            moduleid: '',//业务系统过滤
            modalDefaultValue: {
                begindate: '',
                enddate: '',
                bizPeriod: '',//账簿对应的会计期间
                isShowUnit: false,//是否显示业务单元
                isOpenUpBusiUnit: false,//
                pk_accperiodscheme: '',//会计期间方案
                pk_accountingbook: { display: '', value: '' },
                appcode: this.props.getSearchParam("c")
            },//模态框里默认数据
            showModal: false,//查询条件对话框
            verifyBalancesData: [],
            conditionData: [],
            verBalanceVOData: {}
        }
        this.moduleId = moduleId;
        this.rowBackgroundColor = rowBackgroundColor.bind(this);
        this.handlesonTableClick=this.handlesonTableClick.bind(this)
    }
    componentWillMount(){
		let callback= (json) =>{
            this.setState({
              json:json,
                columns_reportSum:this.columns_reportSum,
				},()=>{
				initTemplate.call(this, this.props);
            })
        }
		getMultiLang({moduleId:['200260','publiccommon'],domainName:'gl',currentLocale:'simpchn',callback});
    }
    //打印
    print() {
        let { resourceDatas, dataout, textAlginArr } = this.state;
        let dataArr = [];
        let emptyArr = [];
        let mergeInfo = dataout.data.mergeInfo;
        dataout.data.cells.map((item, index) => {
            emptyArr = [];
            item.map((list, _index) => {
                //emptyArr=[];
                if (list) {
                    // if(list.title){
                    //     list.title='';
                    // }
                    emptyArr.push(list.title);
                }
            })
            dataArr.push(emptyArr);
        })

        reportPrint(mergeInfo, dataArr, textAlginArr);
    }
    // //查询
    handleQuery = () => {
        this.setState({
            showModal: true,
        })
    }
    handleClose = () => {
        this.setState({
            showModal: false
        })
    }
    //查询确定按钮 
    handleQueryClick = (data) => {
        let self = this;
        let { listItem, verifyBalancesData, resourveQuery, querycondition, resourceDatas, textAlginArr } = self.state;
        let url = '/nccloud/gl/busirecon/busireconExecute.do';
        let childData = deepClone(data);
        resourveQuery = deepClone(data);
        for (let k in childData) {
            if (k == 'appcode' || k == 'url' || k == 'pagecode'||k == 'pk_accperiod' || k == 'pk_accperiodmonth' || k == 'yearmth') {
            } else if (k == 'pk_currency') {
                childData.curName = childData[k].display ? childData[k].display : null;
                childData[k] = childData[k].value ? childData[k].value : null;
            } else if (k == 'pk_accountingbook'||k == 'pk_accasoa' || k == 'busirule') {
                if (childData[k].length > 0) {
                    if (childData[k][0].value == "") {
                        childData[k] = null;
                    } else {
                        childData[k].map((item, index) => {
                            //childData[k].push(item.value);
                            childData[k][index] = item.value;
                        })
                    }
                } else {
                    childData[k] = null;
                }
            } else if (k == 'pk_currtype') {
                childData[k] = childData[k].value ? [childData[k].value] : null;
            } else if (k == 'year_month_begin') {
                //listItem.yearMonth=childData[k];
                childData.beginYear = childData[k].value.split('-')[0];
                childData.beginPeriod = childData[k].value.split('-')[1];
            } else if (k == 'year_month_end') {
                //listItem.yearMonth=childData[k];
                childData.endYear = childData[k].value.split('-')[0];
                childData.endPeriod = childData[k].value.split('-')[1];
            } else {
                childData[k] = childData[k].value ? childData[k].value : null;
            }
        }
        querycondition = childData;
        let columnInfo = [], headtitle = [], busirecon = [];
        ajax({
            url: url,
            data: childData,
            success: function (response) {

                let { data, success } = response;
                if (success) {
                    listItem.accountingbookValue.value = data.accountingbookValue;
                    listItem.period.value = data.period;
                    if (data.columnInfo) {
                        columnInfo = data.columnInfo
                    }
                    if (data.headtitle) {
                        headtitle = data.headtitle;
                    }
                    if (data.busirecon) {
                        busirecon = data.busirecon;
                    }
                    resourceDatas = data;
                    self.setState({
                        resourveQuery, querycondition, resourceDatas, listItem,
                        selectRowIndex:0
                    })
                    let renderFirstData = {};
                    renderFirstData.columnInfo = data.columnInfo;
                    renderFirstData.data = data.busirecon?data.busirecon:[];
                    // renderFirstData.columnInfo1= data.columnInfo1;
                    renderFirstData.columnInfo2 =data.columnInfo2;
                    setData(self,renderFirstData);
                    self.props.button.setButtonDisabled(['print', 'link', 'refresh', 'switch'], false);
                }
            },
            error: function (res) {
                toast({ content: res.message, color: 'warning' });
                self.setState({
                    showTable: true,
                    showModal: false,
                })
            }
        })
    }
    //转换成handsontable需要的数据格式
    changeToHandsonTableData = (self, data, columnInfo, headtitle, busirecon) => {
        // let self=this;
        let { verifyBalancesData, resourveQuery, querycondition, resourceDatas, textAlginArr, flag } = self.state;
        if (flag > 0) {
            flag = 0;
        } else {
            flag++;
        }
        // resourceDatas=data;
        let rows = [];   //表格显示的数据合集
        let columHead = []; //第一层表头
        let parent = []; //存放一级表头，二级表头需要补上
        let parent1 = []; //存放二级表头，三级表头需要补上
        let child1 = []; //第三层表头
        let child2 = []; //第二层表头
        let colKeys = [];  //列key值，用来匹配表体数据
        let colAligns = [];  //每列对齐方式
        let colWidths = [];  //每列宽度
        let oldWidths = [];
        columnInfo.forEach(function (value) {//column:[]原始数据表头信息
            let valueCell = {};
            valueCell.title = value.title;
            valueCell.key = value.key;
            valueCell.align = 'center';
            valueCell.bodyAlign = 'left';
            valueCell.style = 'head';
            if (value.children) {
                self.setState({
                    flow: true,
                });
                if (parent.length > 0) {
                    child1.push(...parent);
                }
                for (let i = 0; i < value.children.length; i++) {
                    if (value.children[i].children) {
                        child2.push(...parent1);
                        for (let k = 0; k < value.children[i].children.length; k++) {
                            let child1Cell = {};
                            child1Cell.title = value.children[i].title;
                            child1Cell.key = value.children[i].key;
                            child1Cell.align = 'center';
                            child1Cell.bodyAlign = 'left';
                            child1Cell.style = 'head';
                            let child2Cell = {};
                            child2Cell.title = value.children[i].children[k].title;
                            child2Cell.key = value.children[i].children[k].key;
                            child2Cell.align = 'center';
                            child2Cell.bodyAlign = 'left';
                            child2Cell.style = 'head';
                            columHead.push(valueCell);
                            child1.push(child1Cell);
                            child2.push(child2Cell);
                            colKeys.push(value.children[i].children[k].key);
                            colAligns.push(value.children[i].children[k].align);
                            colWidths.push(value.children[i].children[k].width);
                        }
                        parent1 = [];
                    } else {
                        let innerCell2 = {};
                        innerCell2.title = value.children[i].title;
                        innerCell2.key = value.children[i].key;
                        innerCell2.align = 'center';
                        innerCell2.bodyAlign = 'left';
                        innerCell2.style = 'head';
                        columHead.push(valueCell);
                        child1.push(innerCell2);
                        parent1.push(innerCell2);
                        colKeys.push(value.children[i].key);
                        colAligns.push(value.children[i].align);
                        colWidths.push(value.children[i].width);
                    }
                }
                parent = [];
            } else {
                let cellObj = {};
                cellObj.title = value.title;
                cellObj.key = value.key;
                cellObj.align = 'center';
                cellObj.bodyAlign = 'left';
                cellObj.style = 'head';
                columHead.push(valueCell);
                parent1.push(cellObj);
                parent.push(cellObj);
                colKeys.push(value.key);
                colAligns.push(value.align);
                colWidths.push(value.width);
            }
        });
        let columheadrow = 0; //表头开始行
        if (headtitle) {
            columheadrow = 1;
            let headtitle = [];
            data.headtitle.forEach(function (value) {
                headtitle.push(value[0]);
                headtitle.push(value[1]);
            });
            for (let i = headtitle.length; i < columHead.length; i++) {
                headtitle.push('');
            }
            rows.push(headtitle);
        }

        let mergeCells = [];
        let row, col, rowspan, colspan;
        let headcount = 1; //表头层数
        if (child1.length > 0) {
            headcount++;
        }
        if (child2.length > 0) {
            headcount++;
        }
        let currentCol = 0;
        //计算表头合并格
        for (let i = 0; i < columnInfo.length; i++) {
            let value = columnInfo[i];
            //*********
            if (value.children) {//有子元素的
                let childLeng = value.children.length;
                let headCol = currentCol;
                for (let j = 0; j < value.children.length; j++) {
                    if (value.children[j].children) {
                        let childlen = value.children[j].children.length;
                        mergeCells.push([1, currentCol, 1, currentCol + childlen - 1]);
                        currentCol = currentCol + childlen;
                        for (let k = 0; k < value.children[j].children.length; k++) {
                            textAlginArr.push(value.children[j].children[k].align);
                        }
                    }
                    // else if(childlen > 0){//子元素里面没有子元素的

                    else {
                        mergeCells.push([columheadrow + 1, currentCol, headcount - 1, currentCol]);
                        currentCol++;
                        textAlginArr.push(value.children[j].align);
                    }
                }
                mergeCells.push([columheadrow, headCol, columheadrow, currentCol - 1]);
            } else {//没有子元素对象
                mergeCells.push([columheadrow, currentCol, headcount - 1, currentCol]);//currentCol
                currentCol++;
                textAlginArr.push(value.align);
            }
        }
        if (parent.length > 0) {
            child1.push(...parent);
        }
        if (child2.length > 0 && parent1.length > 0) {
            child2.push(...parent1);
        }

        rows.push(columHead);
        if (child1.length > 0 && self.state.flow) {
            rows.push(child1);
        }
        if (child2.length > 0 && self.state.flow) {
            rows.push(child2);
        }
        if (data != null && busirecon.length > 0) {
            let rowdata = [];
            let flag = 0;
            for (let i = 0; i < busirecon.length; i++) {
                flag++;
                for (let j = 0; j < colKeys.length; j++) {
                    // {
                    //     "title":"11163",
                    //     "key":"pk_org",
                    //     "align":"left",
                    //     "style":"body"
                    // }
                    let itemObj = {
                        align: 'left',
                        style: 'body'
                    };
                    itemObj.title = busirecon[i][colKeys[j]];
                    itemObj.key = colKeys[j] + flag;
                    //增加几个属性值供联查使用
                    itemObj.link = busirecon[i].link;
                    rowdata.push(itemObj)
                }
                rows.push(rowdata);
                rowdata = [];
            }
        }
        let rowhighs = [];
        for (let i = 0; i < rows.length; i++) {
            rowhighs.push(23);
        }
        self.state.dataout.data.cells = rows;//存放表体数据
        self.state.dataout.data.colWidths = colWidths;
        oldWidths.push(...colWidths);
        self.state.dataout.data.oldWidths = oldWidths;
        self.state.dataout.data.rowHeights = rowhighs;
        let frozenCol;
        for (let i = 0; i < colAligns.length; i++) {
            if (colAligns[i] == 'center') {
                frozenCol = i;
                break;
            }
        }
        self.state.dataout.data.fixedColumnsLeft = 0; //冻结  frozenCol
        self.state.dataout.data.fixedRowsTop = headcount + columheadrow; //冻结
        self.state.dataout.data.freezing = {//固定表头
            "isFreeze" : true,
            "row": self.state.dataout.data.fixedRowsTop,
            "col" : self.state.dataout.data.fixedColumnsLeft
        }
        let quatyIndex = [];
        for (let i = 0; i < colKeys.length; i++) {
            if (colKeys[i].indexOf('quantity') > 0) {
                quatyIndex.push(i);
            }
        }


        if (mergeCells.length > 0) {
            self.state.dataout.data.mergeInfo = mergeCells;//存放表头合并数据
        }
        let headAligns = [];

        for (let i = 0; i < headcount + columheadrow; i++) {
            for (let j = 0; j < columHead.length; j++) {
                headAligns.push({ row: i, col: j, className: "htCenter htMiddle" });
            }
        }
        self.state.dataout.data.cell = headAligns,
            self.state.dataout.data.cellsFn = function (row, col, prop) {
                let cellProperties = {};
                if (row >= headcount + columheadrow || (columheadrow == 1 && row == 0)) {
                    cellProperties.align = colAligns[col];
                    cellProperties.renderer = negativeValueRenderer;//调用自定义渲染方法
                }
                return cellProperties;
            }
        self.setState({
            dataout: self.state.dataout,
            showTable: true,
            showModal: false,
            textAlginArr,
            flag
        }, () => {
            self.props.button.setButtonDisabled(['print', 'link', 'refresh', 'switch'], false);
        });
    }
    //处理width
    changeWidth(arr) {
        arr.map((item, index) => {
            if (item.children) {
                this.changeWidth(item.children);
            } else {
                item['width'] = 100;
            }
        })
        return arr;
    }
    getHeadData = (listItem) => {
        // let {listItem}=this.state;   
        let headData = [
            { itemName: this.state.json['200260-0014'], itemType: 'textInput', itemKey: 'accountingbookValue' },//核算账簿
            { itemName: this.state.json['200260-0015'], itemType: 'textInput', itemKey: 'period' }
        ];
        return (
            headData.map((item, i) => {
                let defValue = listItem[item.itemKey].value;
                switch (item.itemType) {
                    case 'textInput':
                        return (
                            <FormItem
                                inline={true}
                                //showMast={true}
                                labelXs={1} labelSm={1} labelMd={1}
                                xs={2} md={2} sm={2}
                                labelName={item.itemName}
                                isRequire={true}
                                className={item.itemKey==="accountingbookValue"?"m-form-item":""}
                                method="change"
                            >
                                {/* <FormControl
                                value={defValue}
                            /> */}
                                <Tooltip trigger="hover" placement="top" inverse={false} overlay={defValue}>
                                    <span>
                                        <InputItem
                                            //isViewMode
                                            disabled={true}
                                            name={item.itemKey}
                                            type="customer"
                                            defaultValue={defValue}
                                        />
                                    </span>
                                </Tooltip>
                                
                            </FormItem>
                        )
                    default:
                        break;
                }
            })

        )
    }
    //刷新
    handleRefresh = () => {
        let { resourveQuery } = this.state;
        this.handleQueryClick(resourveQuery);
    }
    //转换
    handleChange = () => {

    }
    //获取当前选中行数据
    getSelectRowData = () => {
        let selectRowData = this.refs.balanceTable.getRowRecord();
        return selectRowData;
    }
    // GetRequest=(urlstr)=> {   
    // 	var url = decodeURIComponent(urlstr)//(window.parent.location.href); //获取url中"?"符后的字串   
    // 	var theRequest = new Object();   
    // 	if (url.indexOf("?") != -1) {   
    // 	   var str = url.substr(url.indexOf("?")+1);   
    // 	   var strs = str.split("&");   
    // 	   for(var i = 0; i < strs.length; i ++) {   
    // 		  theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);   
    // 	   }   
    // 	}   
    // 	return theRequest;   
    // }
    //获取默认会计期间 会计期间方案
    getDefaultYearmouth = (pk_accountingbook,modalDefaultValue,moduleid) => {
        let self = this;
        let url = '/nccloud/gl/voucher/queryBookCombineInfo.do';
        let pk_accpont = { "pk_accountingbook": pk_accountingbook }
        ajax({
            url: url,
            data: pk_accpont,
            success: function (response) {
                const { success } = response;
                //渲染已有账表数据遮罩
                if (success) {
                    if (response.data) {
                        modalDefaultValue.bizPeriod = response.data.bizPeriod;
                        modalDefaultValue.isShowUnit = response.data.isShowUnit;
                        modalDefaultValue.isOpenUpBusiUnit = response.data.isShowUnit ? 'Y' : 'N';
                        modalDefaultValue.pk_accperiodscheme = response.data.pk_accperiodscheme;
                        modalDefaultValue.begindate = response.data.begindate;
                        modalDefaultValue.enddate = response.data.enddate;
                    }
                    self.setState({
                        modalDefaultValue,moduleid
                    })
                }
            }
        });
    }
    componentDidMount() {
        let { modalDefaultValue, moduleid } = this.state;
        let appcode = this.props.getSearchParam("c");
        // moduleid = this.props.getSearchParam("moduleid");
        // let defaultAccouontBook = getDefaultAccountBook(appcode);
        getDefaultAccountBook(appcode).then((defaultAccouontBook)=>{
            modalDefaultValue.pk_accountingbook = defaultAccouontBook;
            modalDefaultValue.moduleid=defaultAccouontBook.moduleid;
            moduleid=defaultAccouontBook.moduleid;
            if (defaultAccouontBook.value) {
                this.getDefaultYearmouth(defaultAccouontBook.value,modalDefaultValue,moduleid);
            }
            // this.setState({
            //     modalDefaultValue, moduleid
            // })
        })
    }
    //handlesonTable 单元格点击事件
    handlesonTableClick=(e,coords,td)=>{
        let rowDatas=this.getSelectRowData();//获取当前行数据
        if(rowDatas){//数据无对账维度时，对账维度按钮不可用
            rowDatas.map((item,index)=>{
                if(item){
                    // if(item.key.indexOf("busidim")!=-1){//对账维度列
                    //     if(item.title){
                    //         this.props.button.setButtonDisabled(['linkrecondim'], false);
                    //     }else{
                    //         this.props.button.setButtonDisabled(['linkrecondim'], true);
                    //     }
                    // }
                    // let keyStr=item.key;
                    if(item.key.indexOf("busidim")!=-1){//对账维度列
                        if(item.title==this.state.json['200260-0053']){//总计
                            this.props.button.setButtonDisabled(['linkgl','linkbusi','linkrecondim','recondetail'], true);
                        }else{
                            if(item.title){
                                this.props.button.setButtonDisabled(['linkgl','linkbusi','linkrecondim','recondetail'], false);
                            }else{
                                this.props.button.setButtonDisabled(['linkrecondim'], true);
                                this.props.button.setButtonDisabled(['linkgl','linkbusi','recondetail'], false);
                            }
                        }
                    }

                }
            })
        }
    }
    render() {
        let { verifyBalancesData, listItem, queryShow, showTable, modalDefaultValue } = this.state;
        let loadQuery = [
            {
                itemName: this.state.json['200260-0014'], itemType: 'refer', itemKey: 'pk_accountingbook', isMustItem: true,
                config: { refCode: "uapbd/refer/org/AccountBookTreeRef" }
            },//核算账簿
            {
                itemName: this.state.json['200260-0016'], itemType: 'refer', itemKey: 'busisystem', isMustItem: true,
                config: { refCode: "gl/refer/voucher/BrSystemGridRef" }
            },//业务系统
            {
                itemName: this.state.json['200260-0017'], itemType: 'refer', itemKey: 'busirule', isMustItem: false,
                config: { refCode: "gl/refer/voucher/BusiRuleGridRef" }
            },//对账规则
            {
                itemName: this.state.json['200260-0018'], itemType: 'refer', itemKey: 'pk_currtype', isMustItem: false,
                config: { refCode: "uapbd/refer/pubinfo/CurrtypeGridRef" }
            },//对账币种
            {
                itemName: '', itemType: 'radio', itemKey: 'selectionState', isMustItem: false,
                itemChild: [
                    {
                        label: this.state.json['200260-0019'],//期间查询
                        value: '0',
                    },
                    {
                        label: this.state.json['200260-0020'],//日期查询
                        value: '1',
                    }
                ]
            },
            {
                itemName: this.state.json['200260-0021'], itemType: 'refer', itemKey: 'year_month_begin', isMustItem: false,
                config: { refCode: "uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef" }
            },//会计期间
            {
                itemName: this.state.json['200260-0021'], itemType: 'refer', itemKey: 'year_month_end', isMustItem: false,
                config: { refCode: "uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef" }
            },//会计期间
            { itemName: this.state.json['200260-0022'], itemType: 'Dbdate', isMustItem: true, itemKey: ['beginDate', 'endDate'] },//日期范围begin_date,end_date（必传）
            {
                itemName: this.state.json['200260-0023'], itemType: 'checkbox', isMustItem: false, itemKey: 'isIncludeUnTally',//凭证范围
                itemChild: [{
                    label: this.state.json['200260-0024'],//未记账凭证
                    checked: false
                }]
            },
            {
                itemName: this.state.json['200260-0025'], itemType: 'checkbox', itemKey: 'isOpenUpBusiUnit', isMustItem: false,//展现方式
                itemChild: [{
                    label: this.state.json['200260-0026'],//按业务单元展开
                    checked: false
                }]
            }
        ]
        return (
            <div className="nc-bill-list manageReportContainer" id="verDetail">
                <HeaderArea 
                    title = {this.state.json['200260-0057']}
                    btnContent = {
						this.props.button.createButtonApp({
                            area: 'reconexe',
                            buttonLimit: 4,
                            onButtonClick: buttonClick.bind(this),
                            popContainer: document.querySelector('.header-button-area')
                        })
                        
                    }
                />
                <QueryModal
                    loadData={loadQuery}
                    //lang={this.lang}
                    modalDefaultValue={modalDefaultValue}
                    showOrHide={this.state.showModal}
                    onConfirm={this.handleQueryClick.bind(this)}
                    handleClose={this.handleClose.bind(this)}
                />
                {/* //表头 */}
                <div className="nc-bill-search-area Mende-ncfrom-box-min nc-theme-form-label-c">
                    <div className="Mende-ncfrom-box" fieldid="busirecon_form-area">
                        <NCForm useRow={true}
                            submitAreaClassName='classArea'
                            showSubmit={false}　>
                            {this.getHeadData(listItem)}
                        </NCForm>
                    </div>
                </div>
                <div className='report-table-area'>
                    <SimpleTable
                        ref="balanceTable"
                        data={this.state.dataout}
                        // onCellMouseDown={this.handlesonTableClick.bind(this)}
                        onCellMouseDown={(e,coords,td)=>{
                            this.handlesonTableClick(e,coords,td);
                            this.rowBackgroundColor(e, coords, td)
                        }}
                    />
                </div>
            </div>
        )

    }
}
Busireconexe = createPage({
    initTemplate: initTemplate,
    mutiLangCode: moduleId
})(Busireconexe);
ReactDOM.render(<Busireconexe />, document.querySelector('#app'));