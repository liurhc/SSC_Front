// import {asyncComponent} from 'nc-lightapp-front';
// import GlobalStore from '../../../public/components/GlobalStore';
// import BusireconList from '../list/index.js';
// //const Verify = asyncComponent(() => import(/* webpackChunkName: "gl/busirecon/pages/list/listhome" */'./exportIndex.js'));
// const BusireconCard = asyncComponent(() => import(/* webpackChunkName: "gl/busirecon/pages/card/cardhome" */'../card/index.js'));

// const routes = [
//   {
//     path: '/',
//     component: BusireconList,
//     exact: true,
//   },
//   {
//     path: '/BusireconCard',
//     component: BusireconCard,
//   }
// ];

// export default routes;



import { asyncComponent } from 'nc-lightapp-front';

const card = asyncComponent(() => import(/* webpackChunkName: "gl/busirecon/pages/card/card" */ /* webpackMode: "eager" */ '../card'));
const List = asyncComponent(() => import(/* webpackChunkName: "gl/busirecon/pages/list/list" */ /* webpackMode: "eager" */ '../list'));

const routes = [
	{
		path: '/',
		component: List,
		exact: true
	},
	{
		path: '/list',
		component: List
	},
	{
		path: '/card',
		component: card
	}
];

export default routes;