import { ajax, base, toast,gzip,viewModel } from 'nc-lightapp-front';
let { setGlobalStorage, getGlobalStorage, removeGlobalStorage } = viewModel;
import GlobalStore from '../../../../public/components/GlobalStore';
import { setData } from '../../../../manageReport/common/simbleTableData.js';
import { queryRelatedAppcode, originAppcode } from '../../../../public/common/queryRelatedAppcode.js';
let tableid = 'gl_brsetting';
// @withNav
export default function buttonClick(props, id) {
    let {querycondition,accountType}=this.state;
    let rowDatas,pk_brsetting,pk_unit,pk_currtype,link;
    let gziptools = new gzip();
    switch (id) {
        case 'refresh':
            //刷新
            this.handleRefresh();
            break;
            break;
        case 'exerecon'://执行对账
            this.setState({
                isQueryShow:true
            })
            break;
        case 'linkgl'://联查总账
            rowDatas=this.getSelectRowData();
            if(!rowDatas||rowDatas[0]==null||rowDatas.length==0||rowDatas[0].style=='head'){
                toast({ content:this.state.json['200260-0028'], color: 'warning' });//请先选中一行数据再进行联查
                return false;
            }
            // rowDatas.map((item,index)=>{
            //     if(item){
            //         if(item.key=="busidim1"&&item.title==this.state.json['200260-0053']){//总计
            //             toast({ content:this.state.json['200260-0029'], color: 'warning' });//选中行带有“总计”的数据不能做联查
            //             return false;
            //         }
            //     }
            // })
            link=rowDatas[0].link;        
            let querydata={  
                "condition":{...querycondition},
                "link":link,
                "linkData":link,
                "class":"nc.vo.gl.busirecon.link.GlBusiReconTransferGlQueryVO",
                "linkType":"dimlink" 
            }   
            this.props.openTo('/gl/threedetail/pages/main/index.html',
                {
                    "status":gziptools.zip(JSON.stringify(querydata)),
                    "appcode":queryRelatedAppcode('20023030'),
                    "pagecode":'20023030PAGE'
                }
            )
            break;    
        case 'linkbusi'://联查业务账
            rowDatas=this.getSelectRowData();
            if(!rowDatas||rowDatas[0]==null||rowDatas.length==0||rowDatas[0].style=='head'){
                toast({ content:this.state.json['200260-0028'], color: 'warning' });
                return false;
            }
            // rowDatas.map((item,index)=>{
            //     if(item){
            //         if(item.key=="busidim1"&&item.title==this.state.json['200260-0053']){
            //             toast({ content:this.state.json['200260-0029'], color: 'warning' });
            //             return false;
            //         }
            //     }
            // })
            link=rowDatas[0].link;    
            let conditon={};
            let userObj ={};
            if(querycondition.busisystem=='CMP'||querycondition.busisystem=='IA'){
                conditon={
                    "logic":"and",
                    "conditions":[]
                    // "conditions":{
                    //     condition:querycondition,
                    //     linkData:link,
                    //     class:'nc.vo.gl.busirecon.link.GlBusiReconTransferBridgeQueryVO',
                    //     linkType:'gl',
                    //     appcode:querycondition.appcode,
                    //     pagecode:querycondition.pagecode//应付
                    // }
                };
                userObj={
                    condition:querycondition,
                    linkData:link,
                    class:'nc.vo.gl.busirecon.link.GlBusiReconTransferBridgeQueryVO',
                    linkType:'gl',
                    appcode:querycondition.appcode,
                    pagecode:querycondition.pagecode
                }
                link.class="nc.vo.gl.busirecon.link.GlBusiReconTransferGlQueryVO";
                setGlobalStorage('sessionStorage','LinkReport',JSON.stringify(conditon));
                setGlobalStorage('sessionStorage','LinkReportUserDefObj',JSON.stringify(userObj));
            }else if(querycondition.busisystem=='FA'){//固定资产
                conditon = {
                    logic: 'and',
                    conditions: []
                };
                userObj = {
                    condition: querycondition,
                    linkData: link,
                    class: 'nc.vo.gl.busirecon.link.GlBusiReconTransferBridgeQueryVO',
                    linkType: 'gl',
                    appcode: querycondition.appcode,
                    pagecode: querycondition.pagecode
                };
                setGlobalStorage('sessionStorage','LinkReport', JSON.stringify(conditon));
                setGlobalStorage('sessionStorage','LinkReportUserDefObj', JSON.stringify(userObj));
            }   
                if(querycondition.busisystem=='CMP'){
                    queryBrSetting(querycondition).then((queryconditionData)=>{
                        this.props.openTo(queryconditionData.FCurl,
                            {
                                condition:gziptools.zip(JSON.stringify(queryconditionData)),
                                linkData:gziptools.zip(JSON.stringify(link)),
                                class:'nc.vo.gl.busirecon.link.GlBusiReconTransferBridgeQueryVO',
                                linkType:'gl',
                                appcode:queryconditionData.FCappcode,
                                pagecode:queryconditionData.FCpagecode
                            }
                        )
                    });
                    
                }else{
                    this.props.openTo(querycondition.url,
                        {
                            condition:gziptools.zip(JSON.stringify(querycondition)),
                            linkData:gziptools.zip(JSON.stringify(link)),
                            class:'nc.vo.gl.busirecon.link.GlBusiReconTransferBridgeQueryVO',
                            linkType:'gl',
                            appcode:querycondition.appcode,
                            pagecode:querycondition.pagecode//应付
                        }
                    )
                }   
                
            break;
        case 'recondetail'://联查对账明细
        rowDatas=this.getSelectRowData();
        if(!rowDatas||rowDatas[0]==null||rowDatas.length==0||rowDatas[0].style=='head'){
            toast({ content:this.state.json['200260-0028'], color: 'warning' });//请先选中一行数据再进行联查
            return false;
        }
        // rowDatas.map((item,index)=>{
        //     if(item){
        //         if(item.key=="busidim1"&&item.title==this.state.json['200260-0053']){//总计
        //             toast({ content:this.state.json['200260-0029'], color: 'warning' });//选中行带有“总计”的数据不能做联查
        //             return false;
        //         }
        //     }
        // })
        link=rowDatas[0].link;
        this.props.openTo('/gl/busirecon/pages/busireconDetail/index.html',
            {
                condition:gziptools.zip(JSON.stringify(querycondition)),
                link:gziptools.zip(JSON.stringify(link)),
                appcode:'200260EXE',
                pagecode:'200260EXEVOUCHER'
            }
        )
            break;
        case 'switch'://切换
        let self=this;
        let {resourceDatas,flag}=self.state;
        let data=resourceDatas;
        let ChangecolumnInfo=data.columnInfo;
        if(flag>0){
            ChangecolumnInfo=data.columnInfo2;
        }else{
            ChangecolumnInfo=data.columnInfo;
        }
        if(flag>=1){
            flag=0;
        }else{
            flag++;
        }
        if(flag==0){
          accountType='columnInfo';
          // ChangecolumnInfo=data.columnInfo;
      }else if(flag==1){
          accountType='columnInfo2';
          // ChangecolumnInfo=data.columnInfo1;
      }
      let renderFirstData = {};
        renderFirstData.columnInfo = data.columnInfo;
        renderFirstData.data = data.busidim;
        renderFirstData.columnInfo2 =data.columnInfo2;
        self.setState({
          accountType,flag
        },()=>{
            setData(self,renderFirstData);
        })
        // self.changeToHandsonTableData(self,data,ChangecolumnInfo,data.headtitle,data.busidim);
            break;
        case 'print'://打印
        this.print();
            break;
        default:
        break;

    }
}
function queryBrSetting(querycondition){
    // let {querycondition}=this.state;
    // let self=this;
    let url='/nccloud/gl/busirecon/qryBrSettingAppInfo.do';
    let pagecode,appcode,param;
    if(querycondition.busirule!=null){
        param={pk_brsetting:querycondition.busirule[0]};
        return  new Promise((resolve, reject) => {    
            ajax({
                url:url,
                data:param,
                // async:false,
                success:function(response){
                    let { data, success } = response;
                    if(success){
                        querycondition.FCurl=data.pageurl;
                        querycondition.FCappcode=data.parentcode;
                        querycondition.FCpagecode=data.pagecode;
                        // self.setState({
                        //     querycondition
                        // })
                    }
                    resolve(querycondition)
                }
            })
        })
    }else{
        param={
            pk_accountingbook:querycondition.pk_accountingbook,
            busisystem:querycondition.busisystem
        };
        return  new Promise((resolve, reject) => {    
            ajax({
                url:url,
                data:param,
                // async:false,
                success:function(response){
                    let { data, success } = response;
                    if(success){
                        querycondition.FCurl=data.pageurl;
                        querycondition.FCappcode=data.parentcode;
                        querycondition.FCpagecode=data.pagecode;
                        // self.setState({
                        //     querycondition
                        // })
                    }
                    resolve(querycondition)
                }
            })
        })
    }
}