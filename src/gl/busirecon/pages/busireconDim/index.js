import React, { Component } from 'react';
import {createPage,high,base,ajax,deepClone,toast ,getMultiLang,gzip,createPageIcon} from 'nc-lightapp-front';
import { SimpleTable } from 'nc-report';
const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCPanel:Panel,NCForm,NCButtonGroup:ButtonGroup,NCAffix,NCTooltip:Tooltip
} = base;
const {  NCFormItem:FormItem } = NCForm;
import { CheckboxItem,RadioItem,TextAreaItem,ReferItem,SelectItem,InputItem,DateTimePickerItem} from '../../../public/components/FormItems';
import { buttonClick, initTemplate} from './events';
import {rowBackgroundColor} from "../../../manageReport/common/htRowBackground";
import {tableDefaultData} from '../../../manageReport/defaultTableData';
import { setData } from '../../../manageReport/common/simbleTableData.js';
import HeaderArea from '../../../public/components/HeaderArea';
import getLangCode from '../../../public/common/getLangCode';
import './index.less'
// import dimExport from './events/dimExport';
import reportPrint from '../../../public/components/reportPrint.js';
import '../../../public/reportcss/firstpage.less'
const moduleId='200260';

class Busireconexe extends Component{
    constructor(props){
        super(props);
        this.state={
            json:{},
            accountType:'columnInfo',
            flag:0,//切换表头用的
            showTable: false,
            flow: false,
            resourceDatas:[],//报表数据
            textAlginArr:[],//对其方式
            dataout: tableDefaultData,
            querycondition:{},//查询条件
            resourceQuerycondition:{},
            queryShow:true,//查询按钮默认显示
            listItem:{
                bookname:{display:'',value:''},
                period:{display:'',value:''},
                rulename:{display:'',value:''},
                rulecode:{display:'',value:''},
                accountname:{display:'',value:''},
                busisystem:{display:'',value:''},
                currencytype:{display:'',value:''},
                // balanorient:{display:'',value:''},

            },
            isQueryShow:false,//查询条件对话框
            verifyBalancesData:[],
            conditionData:[],
            verBalanceVOData:{}
        } 
        this.moduleId=moduleId;
        this.rowBackgroundColor = rowBackgroundColor.bind(this);
        this.handlesonTableClick=this.handlesonTableClick.bind(this)   
    }
    print(){  
        let {resourceDatas,dataout,textAlginArr}=this.state;
        let dataArr=[];
        let emptyArr=[];
        let mergeInfo=dataout.data.mergeInfo;
        dataout.data.cells.map((item,index)=>{
            emptyArr=[];
            item.map((list,_index)=>{
                //emptyArr=[];
                if(list){
                    // if(list.title){
                    //     list.title='';
                    // }
                    emptyArr.push(list.title);
                }
            })
            dataArr.push(emptyArr);
        })
        reportPrint(mergeInfo,dataArr,textAlginArr);
    }
    // //查询
    handleQuery=()=>{
        this.setState({
            isQueryShow:!this.state.isQueryShow
        })
    }
    //查询确定按钮 
    handleQueryClick=(param)=>{
        let self=this;
        let {listItem,verifyBalancesData,resourceDatas,textAlginArr}=self.state;
        // let url='/nccloud/gl/busirecon/busireconExecute.do';
        let url='/nccloud/gl/busirecon/busireconDim.do';
        let columnInfo=[],headtitle=[],busidim=[];
        ajax({
            url:url,
            data:param,
            success:function(response){
                let { data, success } = response;
                if(success){
                    listItem.bookname.value=data.bookname;
                    listItem.period.value=data.period;
                    // listItem.balanorient.value=data.balanorient;
                    listItem.rulename.value=data.rulename;
                    listItem.rulecode.value=data.rulecode;
                    listItem.busisystem.value=data.busisystem;
                    listItem.currencytype.value=data.currencytype;
                    listItem.accountname.value=data.accountname;
                    if(data.columnInfo){
                        columnInfo=data.columnInfo
                    }
                    if(data.headtitle){
                        headtitle=data.headtitle;
                    }
                    if(data.busidim){
                        busidim=data.busidim;
                    }
                    resourceDatas=data;
                    self.setState({
                        resourceDatas,listItem,
                        selectRowIndex:0
                    })
                    let renderFirstData = {};
                    renderFirstData.columnInfo = data.columnInfo;
                    renderFirstData.data = data.busidim;
                    // renderFirstData.columnInfo1= data.columnInfo1;
                    renderFirstData.columnInfo2 =data.columnInfo2;
                    setData(self,renderFirstData);
                    // self.changeToHandsonTableData(self,data,data.columnInfo,data.headtitle,data.busidim);
                }else{
                    toast({ content: response.error.message, color: 'warning' });
                    return false;
                }
            },
            error:function(error){
               toast({ content: error.message, color: 'warning' });
                self.setState({
                    isQueryShow: !self.state.isQueryShow,
                })
            }
        })
    }
    //转换成handsontable需要的数据格式
    changeToHandsonTableData=(self,data,columnInfo,headtitle,busirecon)=>{
        // let self=this;
        let {verifyBalancesData,querycondition,resourceDatas,textAlginArr,flag}=self.state;
       if(flag>0){
           flag=0;
       }else{
           flag++;
       }
        // resourceDatas=data;
        let rows = [];   //表格显示的数据合集
        let columHead = []; //第一层表头
        let parent = []; //存放一级表头，二级表头需要补上
        let parent1 = []; //存放二级表头，三级表头需要补上
        let child1 = []; //第三层表头
        let child2 = []; //第二层表头
        let colKeys = [];  //列key值，用来匹配表体数据
        let colAligns=[];  //每列对齐方式
        let colWidths=[];  //每列宽度
        let oldWidths=[];
        columnInfo.forEach(function (value) {//column:[]原始数据表头信息
            let valueCell = {};
            valueCell.title = value.title;
            valueCell.key = value.key;
            valueCell.align = 'center';
            valueCell.bodyAlign = 'left';
            valueCell.style = 'head';
            if(value.children){
                self.setState({
                    flow: true,
                });
                if(parent.length>0){
                    child1.push(...parent);
                }
                for(let i=0;i<value.children.length;i++){
                    if(value.children[i].children){
                        child2.push(...parent1);
                        for(let k=0;k<value.children[i].children.length;k++){
                            let child1Cell = {};
                            child1Cell.title = value.children[i].title;
                            child1Cell.key = value.children[i].key;
                            child1Cell.align = 'center';
                            child1Cell.bodyAlign = 'left';
                            child1Cell.style = 'head';
                            let child2Cell = {};
                            child2Cell.title = value.children[i].children[k].title;
                            child2Cell.key = value.children[i].children[k].key;
                            child2Cell.align = 'center';
                            child2Cell.bodyAlign = 'left';
                            child2Cell.style = 'head';
                            columHead.push(valueCell);
                            child1.push(child1Cell);
                            child2.push(child2Cell);
                            colKeys.push(value.children[i].children[k].key);
                            colAligns.push(value.children[i].children[k].align);
                            colWidths.push(value.children[i].children[k].width);
                        }
                        parent1 = [];
                    }else {
                        let innerCell2 = {};
                        innerCell2.title = value.children[i].title;
                        innerCell2.key = value.children[i].key;
                        innerCell2.align = 'center';
                        innerCell2.bodyAlign = 'left';
                        innerCell2.style = 'head';
                        columHead.push(valueCell);
                        child1.push(innerCell2);
                        parent1.push(innerCell2);
                        colKeys.push(value.children[i].key);
                        colAligns.push(value.children[i].align);
                        colWidths.push(value.children[i].width);
                    }
                }
                parent = [];
            }else{
                let cellObj = {};
                cellObj.title = value.title;
                cellObj.key = value.key;
                cellObj.align = 'center';
                cellObj.bodyAlign = 'left';
                cellObj.style = 'head';
                columHead.push(valueCell);
                parent1.push(cellObj);
                parent.push(cellObj);
                colKeys.push(value.key);
                colAligns.push(value.align);
                colWidths.push(value.width);
            }
        });
        let columheadrow = 0; //表头开始行
        if (headtitle){
            columheadrow = 1;
            let headtitle = [];
            data.headtitle.forEach(function (value) {
                headtitle.push(value[0]);
                headtitle.push(value[1]);
            });
            for(let i=headtitle.length;i<columHead.length;i++){
                headtitle.push('');
            }
            rows.push(headtitle);
        }

        let mergeCells = [];
        let row,col,rowspan,colspan;
        let headcount = 1; //表头层数
        if(child1.length>0){
            headcount++;
        }
        if(child2.length>0){
            headcount++;
        }
        let currentCol = 0;
        //计算表头合并格
        for(let i=0;i<columnInfo.length;i++) {
            let value =columnInfo[i];
            //*********
            if(value.children){//有子元素的
                let childLeng = value.children.length;
                let headCol = currentCol;
                for(let j=0;j<value.children.length;j++){
                    if(value.children[j].children){
                        let childlen = value.children[j].children.length;
                        mergeCells.push([1,currentCol,1,currentCol+childlen-1]);
                        currentCol = currentCol+childlen;
                        for(let k=0;k<value.children[j].children.length;k++){
                            textAlginArr.push(value.children[j].children[k].align);
                        }
                    }
                    // else if(childlen > 0){//子元素里面没有子元素的

                    else {
                        mergeCells.push([columheadrow+1,currentCol,headcount-1,currentCol]);
                        currentCol++;
                        textAlginArr.push(value.children[j].align);
                    }
                }
                mergeCells.push([columheadrow,headCol,columheadrow,currentCol-1]);
            }else{//没有子元素对象
                mergeCells.push([columheadrow, currentCol, headcount-1, currentCol]);//currentCol
                currentCol++;
                textAlginArr.push(value.align);
            }
        }
        if(parent.length>0){
            child1.push(...parent);
        }
        if(child2.length>0&&parent1.length>0){
            child2.push(...parent1);
        }

        rows.push(columHead);
        if(child1.length>0 && self.state.flow){
            rows.push(child1);
        }
        if(child2.length>0 && self.state.flow){
            rows.push(child2);
        }
        if(data!=null && busirecon.length>0){
            let rowdata = [];
            let flag = 0;
            for(let i=0;i<busirecon.length;i++){
                flag++;
                for(let j=0;j<colKeys.length;j++){
                    // {
                    //     "title":"11163",
                    //     "key":"pk_org",
                    //     "align":"left",
                    //     "style":"body"
                    // }
                    let itemObj = {
                        align: 'left',
                        style: 'body'
                    };
                    itemObj.title = busirecon[i][colKeys[j]];
                    itemObj.key = colKeys[j] + flag;
                    //增加几个属性值供联查使用
                    itemObj.link=busirecon[i].link;
                    rowdata.push(itemObj)
                }
                rows.push(rowdata);
                rowdata = [];
            }
        }
        let rowhighs = [];
        for (let i=0;i<rows.length;i++){
            rowhighs.push(23);
        }
        self.state.dataout.data.cells= rows;//存放表体数据
        self.state.dataout.data.colWidths=colWidths;
        oldWidths.push(...colWidths);
        self.state.dataout.data.oldWidths = oldWidths;
        self.state.dataout.data.rowHeights=rowhighs;
        let frozenCol;
        for(let i=0;i<colAligns.length;i++){
            if(colAligns[i]=='center'){
                frozenCol = i;
                break;
            }
        }
        self.state.dataout.data.fixedColumnsLeft = 0; //冻结  frozenCol
        self.state.dataout.data.fixedRowsTop = headcount+columheadrow; //冻结
        self.state.dataout.data.freezing = {//固定表头
            "isFreeze" : true,
            "row": self.state.dataout.data.fixedRowsTop,
            "col" : self.state.dataout.data.fixedColumnsLeft
        }
        let quatyIndex = [];
        for (let i=0;i<colKeys.length;i++){
            if(colKeys[i].indexOf('quantity')>0){
                quatyIndex.push(i);
            }
        }


        if(mergeCells.length>0){
            self.state.dataout.data.mergeInfo= mergeCells;//存放表头合并数据
        }
        let headAligns=[];

        for(let i=0;i<headcount+columheadrow;i++){
            for(let j=0;j<columHead.length;j++){
                headAligns.push( {row: i, col: j, className: "htCenter htMiddle"});
            }
        }
        self.state.dataout.data.cell= headAligns,
            self.state.dataout.data.cellsFn=function(row, col, prop) {
                let cellProperties = {};
                if (row >= headcount+columheadrow||(columheadrow==1&&row==0)) {
                    cellProperties.align = colAligns[col];
                    cellProperties.renderer = negativeValueRenderer;//调用自定义渲染方法
                }
                return cellProperties;
            }
        self.setState({
            dataout:self.state.dataout,
            showTable: true,
            isQueryShow:false,
            textAlginArr,
            flag
        },()=>{
            self.props.button.setButtonVisible(['print', 'link','refresh','switch'], true);
        });
        // self.handleQuery()
    }
    //处理width
    changeWidth(arr){
        arr.map((item,index)=>{
            if(item.children){
                this.changeWidth(item.children);
            }else{
               item['width']=100; 
            }            
        })
        return arr;
    }
    headData=()=>{
        return (
        [
            {itemName:this.state.json['200260-0014'],itemType:'textInput',itemKey:'bookname'},//核算账簿
            {itemName:this.state.json['200260-0015'],itemType:'textInput',itemKey:'period'},//对账期间
            {itemName:this.state.json['200260-0041'],itemType:'textInput',itemKey:'rulecode'},//规则编码
            {itemName:this.state.json['200260-0042'],itemType:'textInput',itemKey:'rulename'},//规则名称
            {itemName:this.state.json['200260-0043'],itemType:'textInput',itemKey:'accountname'},//会计科目
            {itemName:this.state.json['200260-0016'],itemType:'textInput',itemKey:'busisystem'},//业务系统
            {itemName:this.state.json['200260-0044'],itemType:'textInput',itemKey:'currencytype'}//币种
        ])
    }
    getHeadData=(listItem)=>{
       // let {listItem}=this.state;     
       let headDatas= this.headData();
        return (
            headDatas.map((item,i)=>{
                let defValue=listItem[item.itemKey].value;
                switch (item.itemType) {
                    case 'textInput':
                        return(
                        <FormItem
                            inline={true}
                            //showMast={true}
                            labelXs={1} labelSm={1} labelMd={1}
                            xs={2} md={2} sm={2}
                            labelName={item.itemName}
                            isRequire={true}
                            className={item.itemKey==="bookname"?"m-form-item":""}
                            method="change"
                            >
                            {/* <FormControl
                                value={defValue}
                            /> */}
                            <Tooltip trigger="hover" placement="top" inverse={false} overlay={defValue}>
                                <span>
                                    <InputItem
                                        //isViewMode
                                        disabled={true}
                                        name={item.itemKey}
                                        type="customer"
                                        defaultValue={defValue}
                                    />
                                </span>
                            </Tooltip>
                        </FormItem>
                    )
                    default:
                    break;
                }
            })
            
        )  
    }
    //查询业务规则
    queryBrSetting=()=>{
        let {querycondition}=this.state;
        let self=this;
        let url='/nccloud/gl/busirecon/qryBrSettingAppInfo.do';
        let pagecode,appcode,param;
        if(querycondition.busirule!=null)
            param={pk_brsetting:querycondition.busirule[0]};
        else
            param={
                pk_accountingbook:querycondition.pk_accountingbook,
                busisystem:querycondition.busisystem
            };
        ajax({
            url:url,
            data:param,
            success:function(response){
                let { data, success } = response;
                if(success){
                    querycondition.FCurl=data.pageurl;
                    querycondition.FCappcode=data.parentcode;
                    querycondition.FCpagecode=data.pagecode;
                    self.setState({
                        querycondition
                    })
                }
            }
        })
    }
    //刷新
    handleRefresh=()=>{
        let {querycondition,resourceQuerycondition}=this.state;
        this.handleQueryClick(resourceQuerycondition);
    }
    //获取当前选中行数据
    getSelectRowData=()=>{
        let selectRowData=this.refs.balanceTable.getRowRecord();
        return selectRowData;
    }
    //得到url地址的参数
    GetRequest=(urlstr)=> {   
		var url = decodeURIComponent(urlstr)//(window.parent.location.href); //获取url中"?"符后的字串   
		var theRequest = new Object();   
		if (url.indexOf("?") != -1) {   
		   var str = url.substr(url.indexOf("?")+1);   
		   var strs = str.split("&");   
		   for(var i = 0; i < strs.length; i ++) {   
			  theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);   
		   }   
		}   
		return theRequest;   
    }
    componentDidMount(){
    //     let pk_unit=this.props.getUrlParam('pk_unit')=='null'?null:this.props.getUrlParam('pk_unit')
    //     let param=JSON.parse(this.props.getUrlParam('link'));
    //     param.condition=JSON.parse(this.props.getUrlParam('condition'))
    //    this.handleQueryClick(param);
    }
    componentWillMount(){
        let pk_unit=this.props.getUrlParam('pk_unit')=='null'?null:this.props.getUrlParam('pk_unit')
        let gziptools = new gzip();
        let param=gziptools.unzip(this.props.getUrlParam('link'));
        param.condition=gziptools.unzip(this.props.getUrlParam('condition'));
        this.setState({
            querycondition:param.condition,
            resourceQuerycondition:param
        },()=>{
            this.handleQueryClick(param);
        })
        let callback= (json) =>{
            this.setState({
              json:json,
                columns_reportSum:this.columns_reportSum,
				},()=>{
				initTemplate.call(this, this.props);
            })
        }
		getMultiLang({moduleId:'200260',domainName:'gl',currentLocale:'simpchn',callback});
       
    }
    //handlesonTable 单元格点击事件
    handlesonTableClick=(e,coords,td)=>{
        let rowDatas=this.getSelectRowData();//获取当前行数据
        if(rowDatas){//数据无对账维度时，对账维度按钮不可用
            rowDatas.map((item,index)=>{
                if(item){
                    if(item.key.indexOf("busidim")!=-1){//对账维度列
                        if(item.title==this.state.json['200260-0053']){//总计
                            this.props.button.setButtonDisabled(['linkgl','linkbusi','linkrecondim','recondetail'], true);
                        }else{
                            if(item.title){
                                this.props.button.setButtonDisabled(['linkgl','linkbusi','linkrecondim','recondetail'], false);
                            }else{
                                this.props.button.setButtonDisabled(['linkrecondim'], true);
                                this.props.button.setButtonDisabled(['linkgl','linkbusi','recondetail'], false);
                            }
                        }
                    }

                }
            })
        }
    }
    render() {
        let { verifyBalancesData, listItem, queryShow } = this.state;
        return (
            <div className="nc-bill-list manageReportContainer" id="detail">
                <HeaderArea 
                    title = {this.state.json['200260-0058']}/*对账执行*/
                    btnContent = {
						this.props.button.createButtonApp({
                            area: 'busidim',
                            buttonLimit: 4,
                            onButtonClick: buttonClick.bind(this),
                            popContainer: document.querySelector('.header-button-area')
                        })
                        
                    }
                />


                {/* //表头 */}
                <div className="nc-bill-search-area nc-theme-form-label-c">
                    <div className="nc-theme-area-bgc" fieldid="busirecon_form-area">
                        <NCForm useRow={true}
                            submitAreaClassName='classArea'
                            showSubmit={false}　>
                            {this.getHeadData(listItem)}
                        </NCForm>
                    </div>
                </div>
                <div className='report-table-area'>
                    <SimpleTable
                        ref="balanceTable"
                        data={this.state.dataout}
                        // onCellMouseDown={this.handlesonTableClick.bind(this)}
                        onCellMouseDown={(e, coords, td) => {
                            this.handlesonTableClick(e, coords, td);
                            this.rowBackgroundColor(e, coords, td)
                        }}
                    />
                </div>
            </div>
        )

    }
}
Busireconexe = createPage({
	//initTemplate: initTemplate,
	mutiLangCode: moduleId
})(Busireconexe);
ReactDOM.render(<Busireconexe />,document.querySelector('#app'));