//主子表卡片
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast,getMultiLang ,cardCache,createPageIcon} from 'nc-lightapp-front';
import { buttonClick, initTemplate, afterEvent,beforeEvent, pageInfoClick,tableButtonClick,tableButtonClick1 } from './events';
import {  gl_brsettingFormId, br_datatypeFormId, br_contentFormId,
	 br_filtercondFormId, brmapDetailId,braccasoaDetailId, urls,ObjStatus,dataSource,pkname} from './constants';
import { isNullOrUndefined } from 'util';
let { NCAnchor, NCScrollLink, NCScrollElement, NCAffix,NCTabs,NCRow,NCCol,NCIcon:Icon } = base;
const { NCTabPane } = NCTabs;
import GetRequest from '../../../public/components/GetRequestUrl.js';
import getLangCode from '../../../public/common/getLangCode';
import './index.less';
import HeaderArea from '../../../public/components/HeaderArea';
const config={
    "isDataPowerEnable":'Y',
    "DataPowerOperationCode":'fi'
};
const moduleId='200260';
class Card extends Component {
	constructor(props) {
		super(props);
		this.formId = 'head';
		this.moduleId = '2002';
        this.systemInfoModuleid='';
        super(props);
		this.gl_brsettingFormId = gl_brsettingFormId;
		this.br_datatypeFormId = br_datatypeFormId;
		this.br_contentFormId = br_contentFormId;
		this.br_filtercondFormId = br_filtercondFormId;
		this.brmapDetailId = brmapDetailId;
		this.braccasoaDetailId=braccasoaDetailId;
		this.urls = urls;
		this.ObjStatus=ObjStatus;
		this.state = {
			json:{},
			status : props.getUrlParam('status'),
			pk_brsetting : props.getUrlParam('pk_brsetting'),
			ts:props.getUrlParam('ts'),
			qrytemplateData:{},
			isShowUnit:false,//是否显示业务单元
			RightPanelParam:{//业务系统档案模块需要的参数
				pk_accountingbook:'',
				pk_setofbook:'',
				pk_org:'',
				accountingbookpk_org:'',//选完账簿对应的组织
				pk_defdoc:''//业务系统
			},
			curComp: undefined,
			curCompFlag:false,//是否请求到了第三方模块
			comps: {},
			braccasoaDetaiDatas:{},
			systemInfoModuleid:'',//业务系统对应得moduleid
			//braccasoaRows:[]
		};
		this.moduleId=moduleId;
	}
	componentDidMount() {
		this.toggleShow(this.state.status);
		if(this.state.status&&this.state.status != 'add'){
				this.getData();		
		}
	}
	componentWillMount() {
		
		let callback= (json) =>{
			window.onbeforeunload = () => {
				let status = this.props.getUrlParam("status");
				if (status == 'edit' || status == 'add') {
					return ''
				}
			}
            this.setState({
              json:json,
                columns_reportSum:this.columns_reportSum,
				},()=>{
				initTemplate.call(this, this.props);
            })
        }
		getMultiLang({moduleId:'200260',domainName:'gl',currentLocale:'simpchn',callback});
	}
    getData = () => {
		let {status, pk_brsetting} = this.state;
		if(status == 'add')
			return;
		let data = {
			pk_brsetting : pk_brsetting,
			pageCode : this.props.getUrlParam('pageCode')?this.props.getUrlParam('pageCode'):this.props.getSearchParam('p')
		};
		let url = this.urls.queryCard;
		ajax({
			url: url,
			data : data,
			success : (res) => {
				if(res.success){
					this.loadData(res.data);
				}
			}
		});
    }
	/**
	 * 将请求到的数据加载到页面
	 */
	loadData = (data) => {
		let self=this;
		let pk_accasoa_display=[];
		let pk_accasoa_value=[];
		let {braccasoaDetaiDatas,qrytemplateData,curCompFlag,RightPanelParam,pk_brsetting,ts}=self.state;
        let pk_defdoc,pk_accountingbookvalue;
        if(data&&data.head.gl_brsetting&&data.head.gl_brsetting.rows[0]&&data.head.gl_brsetting.rows[0].values){
            pk_brsetting=data.head.gl_brsetting.rows[0].values.pk_brsetting.value;
            ts=data.head.gl_brsetting.rows[0].values.ts.value;
        }
		if(data){
			if(data.head){
				if(data.bodys[braccasoaDetailId]){
					let pk_braccasoaArr=data.bodys[braccasoaDetailId].rows;
					pk_braccasoaArr.map((item,index)=>{
						pk_accasoa_display.push(item.values.pk_accasoa.display);
						pk_accasoa_value.push(item.values.pk_accasoa.value);
					})
				}
				
				qrytemplateData=data.head[self.gl_brsettingFormId].rows[0].values.qrytemplate;
				pk_defdoc=data.head[self.gl_brsettingFormId].rows[0].values.pk_defdoc.value;
				pk_accountingbookvalue=data.head[self.gl_brsettingFormId].rows[0].values.pk_accountingbook.value;
				let pk_setingbookvalue=data.head[self.gl_brsettingFormId].rows[0].values.pk_setofbook.value;
				if(pk_setingbookvalue){
					RightPanelParam.pk_setofbook=pk_setingbookvalue;
				}
				RightPanelParam.pk_defdoc=pk_defdoc;
				if(pk_accountingbookvalue){
				    self.getAccountingbookOrg(pk_accountingbookvalue);	
				}
				if(qrytemplateData){
					curCompFlag=true;
					
				}
				data.head[self.gl_brsettingFormId].rows[0].values.brmap=null;
				data.head[self.gl_brsettingFormId].rows[0].values.braccasoas=null;
				//限制科目
				data.head[self.gl_brsettingFormId].rows[0].values.pk_accasoa={
					display:pk_accasoa_display.join(','),
					value:pk_accasoa_value.join(',')
				}
				self.props.form.setAllFormValue({
					[self.gl_brsettingFormId]:data.head[self.gl_brsettingFormId],
				});
			}
			if(data.bodys){
				if(data.bodys[braccasoaDetailId]){		
					braccasoaDetaiDatas=data.bodys[braccasoaDetailId];		
					self.props.cardTable.setTableData(self.brmapDetailId, data.bodys[self.brmapDetailId]);
					// this.props.cardTable.setTableData(this.cycleDetailId, data.bodys[this.cycleDetailId]);
					// this.props.form.setAllFormValue({[this.tailFormId]:data.bodys[this.tailFormId]});
				}
			}
		}
		self.setState({
			braccasoaDetaiDatas,curCompFlag,qrytemplateData,RightPanelParam,pk_brsetting,ts
		},()=>{
			self.QueryDefdocData(pk_defdoc);
		})
	}
//根据账簿请求pk_org
getAccountingbookOrg=(pk_accountingbookvalue)=>{
	let self=this;
	let {RightPanelParam,isShowUnit}=self.state;
	RightPanelParam.pk_accountingbook=pk_accountingbookvalue;
	let pk_accpont = {"pk_accountingbook":pk_accountingbookvalue};
	let url = '/nccloud/gl/voucher/queryBookCombineInfo.do';
	ajax({
		url:url,
		data:pk_accpont,
		async:false,
		success: function(response){
			const { success } = response;
			//渲染已有账表数据遮罩
			if (success) {
				if(response.data){
					isShowUnit=response.data.isShowUnit;
					RightPanelParam.pk_org=response.data.unit.value;
					RightPanelParam.accountingbookpk_org=response.data.unit.value
                    self.props.form.setFormItemsVisible(self.gl_brsettingFormId,{'pk_unit_v':isShowUnit});
				}
				self.setState({
					isShowUnit,RightPanelParam
				})
			}   
		}
	});
}
	//切换页面状态
	toggleShow = (status) => {
		this.createRightPanel(status);
		let flag = (status === 'browse') ? false : true;
		let status1 = (status === 'browse') ? 'browse' : "edit";
		this.props.button.setButtonVisible(['save','cancel','addline','delline'],flag);
		this.props.button.setButtonVisible([ 'add', 'edit','delete'], !flag);
		this.props.form.setFormStatus(this.gl_brsettingFormId, status1);
		this.props.form.setFormStatus(this.br_datatypeFormId, status1);
		this.props.form.setFormStatus(this.br_contentFormId, status1);
		this.props.form.setFormStatus(this.br_filtercondFormId, status1);
		this.props.cardTable.setStatus(this.brmapDetailId, status1);
		if(status=='add'){
			this.setState({
				status:status,
				RightPanelParam:{
					pk_accountingbook:'',
					pk_setofbook:'',
					pk_org:'',
					accountingbookpk_org:'',
					pk_defdoc:''
				},
				qrytemplateData:{}
			})
		}else{
			this.setState({status:status})
		}		
	};
	//校验外部模块组件必输项
	checkCondition=()=>{
		let flag=this.curCompInstance.checkCondition();
		return flag;
	}
	//获取外部模块组件的数据
	getAllFiledAndValues=()=>{
		let data={};
		if(this.curCompInstance){
			if(!this.curCompInstance.checkCondition()){
				// toast({content:'有必输项校验未通过',color:'warning'});
				return false;
			}
			data=this.curCompInstance.getAllFiledAndValues();
		}
		return data;
	}

	//保存单据
	saveBill = () => {
		let {braccasoaDetaiDatas,pk_brsetting}=this.state;
		let nodetype=this.props.getUrlParam('nodetype');
		let { addCache ,updateCache} = cardCache;
		
		let qrytemplateData=this.getAllFiledAndValues();
		if(!qrytemplateData){ return false;}
		let cardData = this.getDataNeedSave();
		let status=this.props.getUrlParam('status');
		// isCheckNow() 必输项校验
		if(!this.props.form.isCheckNow([this.gl_brsettingFormId,this.br_datatypeFormId ,this.br_contentFormId,this.br_filtercondFormId])){
			return false;
		}
		//对账内容（数量、原币、本币，三者必有其一不能一个都不选择）、
		//对账数据类型（借方发生、贷方发生、余额，三者必有其一不能一个都不选择），限制科目不可为空
		let checkData=cardData.head.gl_brsetting.rows[0].values;
		if(!checkData.type_end_ocr.value&&!checkData.type_endbal.value&&!checkData.type_balance.value){
			toast({content: this.state.json['200260-0009'],color:'danger'});
				return false;
		}
		if(!checkData.con_amount.value&&!checkData.con_locamount.value&&!checkData.con_quantity.value){
			toast({content: this.state.json['200260-0010'],color:'danger'});
			return false;
		}
		if(!checkData.pk_accasoa.value){
			toast({content: this.state.json['200260-0011'],color:'danger'});
			return false;
		}
		cardData.head.gl_brsetting.rows[0].values.qrytemplate.value=(qrytemplateData?qrytemplateData:null);
		cardData.head.gl_brsetting.rows[0].status=this.getStatus(status);		
		cardData.bodys.braccasoa=braccasoaDetaiDatas;
		if(cardData.bodys.brmap.rows.length==0){
			delete cardData.bodys.brmap.rows;
		}
		let url = this.urls.updateCard;
		ajax({
			url: url,
			data : cardData,
			success : (res) => {
				if(res.success){
					toast({ content:this.state.json['200260-0012'], color: 'success', });					
					if (this.props.getUrlParam('status') === 'edit') {
						updateCache(pkname,pk_brsetting,res.data,gl_brsettingFormId,dataSource);
					  }else{
						addCache(pk_brsetting,res.data,gl_brsettingFormId,dataSource);
					  }
					this.updatePageStatus({status:'browse', id:res.data});
					this.loadData(res.data);				
				}
			}
		});
	};
	updatePageStatus = (params) => {
        let pk_brsetting='',ts='';
		let c = this.props.getUrlParam('c');
		let nodetype=this.props.getUrlParam('nodetype');
  		let pageCode=this.props.getUrlParam('pageCode')?this.props.getUrlParam('pageCode'):this.props.getSearchParam('p');
        if(params.id&&params.id.head.gl_brsetting&&params.id.head.gl_brsetting.rows[0]&&params.id.head.gl_brsetting.rows[0].values){
            pk_brsetting=params.id.head.gl_brsetting.rows[0].values.pk_brsetting.value;
            ts=params.id.head.gl_brsetting.rows[0].values.ts.value;
        }else{
            pk_brsetting=this.props.getSearchParam('pk_brsetting');
        }
		this.props.pushTo('/card', {
			c:c,
			nodetype:nodetype,
			pageCode:pageCode,
			p:pageCode,
			pk_brsetting:pk_brsetting,
			ts:ts,
			status: 'browse' 
		})
		this.toggleShow('browse');
	}
	getStatus=(status)=>{
		for(let k in ObjStatus){
			if(status==k){
				return ObjStatus[k];
			}
		}
	}
	getDataNeedSave = () => {
		this.props.cardTable.filterEmptyRows(brmapDetailId, ['pk_brrelation', 'accitemvalue'], 'include');
		let pageId=this.props.getUrlParam('pageCode')?this.props.getUrlParam('pageCode'):this.props.getSearchParam('p');
		let cardData = this.props.createExtCardData(pageId, this.gl_brsettingFormId, [this.brmapDetailId]);
		return cardData;
	}
	createRightPanel(status){
		let {RightPanelParam,json}=this.state;
		let panel = this.state.curComp;
		let nodetype=this.props.getUrlParam('nodetype');
		// let status=this.props.getUrlParam('status');
        let paramData = {
			busireconData : this.state.qrytemplateData,
			pk_accountingbook:RightPanelParam.pk_accountingbook,
			pk_setofbook:RightPanelParam.pk_setofbook,
			pk_org:RightPanelParam.pk_org,
            status:status,
			json:json,
			nodetype:nodetype,//org为组织，group为集团
            onRef: (item)=> {
                this.curCompInstance = item;
			}
        };
        return !panel ?  '' :  (panel(paramData));
	}
changeModuledId=(moduledid)=>{
	let meta = this.props.meta.getMeta();
	// if(JSON.stringify(meta)!='{}'){
		if(meta[br_datatypeFormId]){
			meta[br_datatypeFormId].items = meta[br_datatypeFormId].items.map((item, key) =>{
				if(moduledid=='IA'){//存货核算
					item.visible=true;
					//this.props.form.setItemsVisible(this.gl_brsettingFormId,{'type_end_ocr':true,'type_endbal':true,'type_balance':true});
				}else if(moduledid=='AR'||moduledid=='AP'){//AR(应收)、AP(应付) 内容 数量、本币隐藏
					if(item.attrcode == 'type_end_ocr'||item.attrcode == 'type_endbal'){
						item.visible=false;
						//this.props.form.setItemsVisible(this.gl_brsettingFormId,{'type_end_ocr':false,'type_endbal':false});
					}else{
						item.visible=true;
						//this.props.form.setItemsVisible(this.gl_brsettingFormId,{'type_balance':true});
					}
				}else if(moduledid=='FTS'){//FTS(资金结算)
					item.visible=true;
					//this.props.form.setItemsVisible(this.gl_brsettingFormId,{'type_end_ocr':true,'type_endbal':true,'type_balance':true});
				}else if(moduledid=='CMP'){//CMP（资金管理）
					item.visible=true;
					//this.props.form.setItemsVisible(this.gl_brsettingFormId,{'type_end_ocr':true,'type_endbal':true,'type_balance':true});
				}else if(moduledid=='FA'){//FA（固定资产）借方发生、贷方发生 隐藏
					if(item.attrcode == 'type_end_ocr'||item.attrcode == 'type_endbal'){
						//this.props.form.setItemsVisible(this.gl_brsettingFormId,{'type_end_ocr':false,'type_endbal':false});
						item.visible=false;
					}else{
						item.visible=true;
						//this.props.form.setItemsVisible(this.gl_brsettingFormId,{'type_balance':true});
					}
				}
				return item;
			});
		}
		if(meta[br_contentFormId]){
			meta[br_contentFormId].items = meta[br_contentFormId].items.map((item, key) =>{
				if(moduledid=='IA'){//存货核算 对账内容 原币隐藏 其余都显示 
					if(item.attrcode == 'con_amount'){
						item.visible=false;
						//this.props.form.setItemsVisible(this.gl_brsettingFormId,{'con_amount':false});
					}else{
					item.visible=true;
					//	this.props.form.setItemsVisible(this.gl_brsettingFormId,{'con_quantity':true,'con_locamount':true});
					}
				}else if(moduledid=='AR'||moduledid=='AP'){//AR(应收)、AP(应付)
					item.visible=true;
					// this.props.form.setItemsVisible(this.gl_brsettingFormId,{'con_quantity':true,'con_locamount':true,'con_amount':true});
				}else if(moduledid=='FTS'){//FTS(资金结算)内容 数量、本币隐藏
					if(item.attrcode == 'con_quantity'||item.attrcode == 'con_locamount'){
						item.visible=false;
						// this.props.form.setItemsVisible(this.gl_brsettingFormId,{'con_quantity':false,'con_locamount':false});
					}else{
						item.visible=true;
						// this.props.form.setItemsVisible(this.gl_brsettingFormId,{'con_amount':true});
					}
				}else if(moduledid=='CMP'){//CMP（资金管理）数量隐藏 
					if(item.attrcode == 'con_quantity'){
						item.visible=false;
						// this.props.form.setItemsVisible(this.gl_brsettingFormId,{'con_quantity':false});
					}else{
						item.visible=true;
						// this.props.form.setItemsVisible(this.gl_brsettingFormId,{'con_amount':true,'con_locamount':true});
					}
				}else if(moduledid=='FA'){//FA（固定资产） 原币数量隐藏
					if(item.attrcode == 'con_quantity'||item.attrcode == 'con_amount'){
						item.visible=false;
						// this.props.form.setItemsVisible(this.gl_brsettingFormId,{'con_quantity':false,'con_amount':false});
					}else{
						item.visible=true;
						// this.props.form.setItemsVisible(this.gl_brsettingFormId,{'con_locamount':true});
					}
				}
				return item;
			});
		}
		if(meta[brmapDetailId]){
			meta[brmapDetailId].items = meta[brmapDetailId].items.map((item, key) =>{
				if(item.attrcode == 'pk_brrelation'){
					item.queryCondition = () => {
						return Object.assign({
							"moduleid":moduledid,
							"GridRefActionExt":"nccloud.web.gl.ref.BRInfoRefSqlBuilder"
						},config)
					}
				}
				return item;
			});
		}
		if(meta[br_datatypeFormId]||meta[br_contentFormId]||meta[brmapDetailId]){
            this.props.meta.setMeta(meta);
		}
		
	// }
}
//获取业务系统对应的模块数据
QueryDefdocData=(value,type,flag)=>{
	let self=this;
	let url='/nccloud/gl/busirecon/qryBRSystemInfo.do';
		let param={pk_defdoc: value};
		ajax({
			url  :url ,
			data :param ,
			async:false,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if(data){
						let systemInfoModuleid=data.moduleid;
						self.systemInfoModuleid=systemInfoModuleid;
						self.changeModuledId(systemInfoModuleid);
						if(data['webcomponent'] && data['webcomponent'].length>12){
							//url = '../../../../gl/public/components/reckoningReport/index'+'.js';
							let url = data['webcomponent'].trim()+'/index.js';
							// let url = 'gl/public/components/reckoningReport/index'+'.js';
							let moduleName = url.substring(0,url.length-3);
							
							let script =  document.createElement('script');
							script.src =  '../../../../' +url;
							script.type= 'text/javascript';    
							script.onload = () => {
								let moduleClass =  window[moduleName];
								if(!moduleClass) {
									toast({content: 'load panel class error!',color:'danger'});
									return;
								}
								self.state.comps[moduleName] = moduleClass.default;
								self.state.curComp = moduleClass.default;
								self.state.curCompFlag=true;
								self.state.systemInfoModuleid=systemInfoModuleid;
								self.setState(self.state);
							};
							script.onerror= () => {
								toast({content: self.state.json['200260-0062']+script.src,color:'danger'});    
							};
							document.body.appendChild(script);
						}else{
							self.state.curCompFlag=false;
							self.state.systemInfoModuleid=systemInfoModuleid;
							self.setState(self.state);
						}
						if(type){
							self.autoFocus(type,flag);
						}
					}else{
						let moduleName = url.substring(0,url.length-3);
						self.state.comps[moduleName] ='';
						self.state.curComp = '';
						self.state.curCompFlag=false;
						self.state.systemInfoModuleid='';
						self.setState(self.state);
					}
				}
			}
        });
	}

	//获取createCardTable肩部信息
	getTableHead = (buttons) => {
		let { createButton } = this.props.button;
		return (
			<div className="shoulder-definition-area" style={{right:100}}>
				<div className="definition-search">
					<span>
						{this.props.button.createButtonApp({
							area: 'busireconsub',
							buttonLimit: 3,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.definition-search')
						})}
					</span>
				</div>
				<div className="definition-icons">
					{this.props.cardTable.createBrowseIcons(this.tableId, {
						iconArr: ['close', 'open', 'max'],
						maxDestAreaId: 'finance-reva-revecontract-card'
					})}
				</div>
			</div>
		);
	};
	handleReturnBack=()=>{
		let nodetype=this.props.getUrlParam('nodetype');
		this.props.pushTo('/list', {nodetype:nodetype, status: "browse"})
		this.toggleShow()
	}
	selectedChange=(props,newVal,oldVal)=>{//选中行发生变化
        if(oldVal != 0){
            props.button.setButtonDisabled(['delline'], false)
        }else{
            props.button.setButtonDisabled(['delline'], true)
        }
	}
	//获取焦点
	autoFocus=(type,flag)=>{
		let self=this;
		setTimeout(() => {
			let modelNode= ReactDOM.findDOMNode(self);
		// 	let inputNode=modelNode&&modelNode.querySelector('.pk_accountingbook input.refer-input:first-child');
		// 	inputNode&&inputNode.focus()
		// }, 500);
		if(type=='pk_accountingbook'){
			let inputNode1=modelNode&&modelNode.querySelector('.pk_accountingbook input.refer-input');
			inputNode1&&inputNode1.focus()
		}else if(type=='pk_defdoc'){
			let inputNode2=modelNode&&modelNode.querySelector('.pk_defdoc input.refer-input');
			inputNode2&&inputNode2.focus()
		}else if(type=='pk_unit_v'){
			// if(flag=='1'){
			// 	let inputNode3=modelNode&&modelNode.querySelector('.pk_unit_v input.refer-input');
			// 	inputNode3&&inputNode3.focus();
			// }else{
			// 	let inputNode5=modelNode&&modelNode.querySelector('.closeaccctrltype .u-select-selection-rendered');
			// 	inputNode5&&inputNode5.focus();
			// }
		}else if(type=='pk_setofbook'){
			let inputNode4=modelNode&&modelNode.querySelector('.pk_setofbook input.refer-input');
			inputNode4&&inputNode4.focus()
		}
		}, 500);
	}
	render() {
		let { cardTable, form, button, modal, cardPagination } = this.props;
		let buttons = this.props.button.getButtons();
		//let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createCardPagination } = cardPagination;
		let { createButton, createButtonApp,createOprationButton } = button;
		let { createModal } = modal;
		let {status}=this.state;
		return (
			<div className="nc-bill-extCard nc-bill-list nc-bill-card" id="extcard">
				<HeaderArea 
                    title = {this.props.getUrlParam('nodetype')=='group'?this.state.json['200260-0002']:this.state.json['200260-0001']}/* 国际化处理： 往来核销处理*/
					initShowBackBtn = {status=='browse'?true:false} //是否显示返回按钮，默认false，不不显示返回按钮
					backBtnClick = {this.handleReturnBack} //返回按钮点击事件
                    btnContent = {
						createButtonApp({
								area:"busirecon",
								buttonLimit:3,
								onButtonClick: buttonClick.bind(this),
								popContainer:document.querySelector('.header-button-area')
						})
                        
                    }
                />
			<div className="nc-bill-top-area">				
				<NCScrollElement name='forminfo'>
					<div className="nc-bill-form-area">
						{createForm(this.gl_brsettingFormId, {
							expandArr: [this.br_datatypeFormId ,this.br_contentFormId,this.br_filtercondFormId],
							onAfterEvent: afterEvent.bind(this)
						})}
					</div>				
				</NCScrollElement>
				</div>
				<div className='gl_brsetting nc-bill-top-area'>
					<NCTabs 
						navtype="turn" 
						contenttype="moveleft" 
						defaultActiveKey= '1'
						onChange = {(v) => {
							this.setState({
								tabKeys: v
							})
						}}
						>
						<NCTabPane tab={this.state.json['200260-0013']} key='1'>
							{this.state.curCompFlag ?
								<div className="card-area">{this.createRightPanel(status)}</div>
								:<div className="no_data">
									<Icon type='uf-i-c-2' className="mark_inco_i" />
									<span>{this.state.json['200260-0054']}</span>
								</div>}
						</NCTabPane>
					</NCTabs>
				</div>
				<div className="nc-bill-table-area nc-theme-area-split-bc" id="nc-border">
					{/* {this.props.button.createButtonApp({
							area: 'busireconsub',
							buttonLimit: 4, 
							onButtonClick: buttonClick.bind(this), 
							popContainer: document.querySelector('.header-button-area')
						})} */}
					{createCardTable(brmapDetailId, {
                        adaptionHeight:true,
						tableHead: this.getTableHead.bind(this, buttons),
						hideSwitch:()=>{return false},
						onAfterEvent: afterEvent.bind(this),
						// onBeforeEvent:beforeEvent.bind(this),
						showCheck: true,
						showIndex: false,
						selectedChange: this.selectedChange.bind(this)
					})}
				</div> 
			</div>
		);
	}
}

Card = createPage({
	// initTemplate: initTemplate,
	//mutiLangCode: moduleId
})(Card);

export default Card;
//ReactDOM.render(<Card />, document.querySelector('#app'));
