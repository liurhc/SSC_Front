/**
 * 小应用主键
 */
// export const appcode = '200260SETG';


/**
 * 页面编码
 */
// export const pageId = '200260SETG_brbase_card';

/**
 * 查询模板主键
 */
//export const oid = '1002Z3100000000091MF';
/**
 * 表单区域
 */
// export const headFormId = 'amortize';
// export const tailFormId = 'amorcycle';
export const gl_brsettingFormId = 'gl_brsetting';
export const br_datatypeFormId = 'br_datatype';
export const br_contentFormId = 'br_content';
export const br_filtercondFormId = 'br_filtercond';

/**
 * 表体区域
 */
export const brmapDetailId = 'brmap';
export const braccasoaDetailId='braccasoa';
// export const cycleDetailId = 'cycledetail';

export const urls = {
    queryCard : '/nccloud/gl/busirecon/qryBrsettingCard.do',
    updateCard : '/nccloud/gl/busirecon/brsettingSave.do',
    insertCard : '/nccloud/gl/busirecon/insertBrSetting.do',
    delAmortizes : '',
    stopAmortizes : ''
}
export const ObjStatus = {
    unchanged : 0,
    edit : 1,//修改
    // update : 1,
    add : 2,//新增
    delete :3//删除
}


export const dataSource = 'gl.busirecon.pages.200260';

export const pkname = 'pk_brsetting';