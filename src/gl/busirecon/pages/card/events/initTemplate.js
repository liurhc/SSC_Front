import { base, ajax,getBusinessInfo } from 'nc-lightapp-front';
import { gl_brsettingFormId, br_datatypeFormId, br_contentFormId, br_filtercondFormId,
	brmapDetailId } from '../constants';
	const config={
		"isDataPowerEnable":'Y',
		"DataPowerOperationCode":'fi'
	};
	const disabledButtonsArr = ["delline"];
export default function(props) {
	let self=this;
	let {RightPanelParam}=self.state;
	let pageCode;
	let appcode = props.getSearchParam('c') || '';
	if(props.getUrlParam('nodetype')=='group'){
		pageCode = '200260SETG_brbase_card';
	}else if(props.getUrlParam('nodetype')=='org'){
		pageCode = '200260SETO_brbase_card';
	}
	props.createUIDom(
		{
			pagecode: pageCode,//页面id
			appcode: appcode//注册按钮的id
		}, 
		function (data){
			if(data){
				if(data.template){
					let meta = data.template;
					let context=data.context;
					if(props.getUrlParam('nodetype')=='group'){
						meta[gl_brsettingFormId].items.map((item,index)=>{
							if(item.attrcode=='pk_setofbook'){
								// item.visible=true;
							}else if(item.attrcode=='pk_defdoc'){//业务系统
								item.queryCondition = () =>{
									return Object.assign({
										"appcode":'200260SETG',//按照组织级过滤
										"GridRefActionExt":'nccloud.web.gl.ref.BRSystemRefSqlBuilder'
									},config)
								}
							}
						})
						meta[br_filtercondFormId].items.map((item,index)=>{
							if(item.attrcode == 'pk_accasoa'){
								item.isMultiSelectedEnabled=true;
								item.isAccountRefer=false;// false：政策性科目参照 （集团或全局）
								item.queryCondition = () =>{
									return getCondition(props,'group','pk_accasoa');
								}
							}
						})
						meta[brmapDetailId].items.map((item,index)=>{
							if(item.attrcode=='accitemvalue'){
								item.pageInfo={pageIndex:-1};
							}else if(item.attrcode=='pk_brrelation'){//业务系统档案
								// item.queryCondition = () =>{
								// 	return {
								// 		"pk_defdoc":props.form.getFormItemsValue(gl_brsettingFormId, 'pk_defdoc').value,
								// 		"GridRefActionExt":"nccloud.web.gl.ref.BRInfoRefSqlBuilder"
								// 	}
								// }
							}
						})
					}else if(props.getUrlParam('nodetype')=='org'){
						let isShowUnit=false;
						if(context.defaultAccbookPk&&props.getUrlParam('status')=='add'){
							let responseData=getDefaultYearmouth(context.defaultAccbookPk);
							isShowUnit=responseData.isShowUnit;
							RightPanelParam.pk_accountingbook=context.defaultAccbookPk;
							RightPanelParam.pk_org=responseData.pk_org;
							RightPanelParam.accountingbookpk_org=responseData.pk_org;
						}else{
                            isShowUnit=self.state.isShowUnit;
                        }
						meta[gl_brsettingFormId].items.map((item,index)=>{
							if(item.attrcode=='pk_setofbook'){
								// item.visible=false;
							}else if(item.attrcode=='pk_accountingbook'){
								// item.visible=true;
								if(props.getUrlParam('status')=='add'){
									let defaultAccountBookData={"pk_accountingbook":{display:context.defaultAccbookName?context.defaultAccbookName:'',value:context.defaultAccbookPk?context.defaultAccbookPk:''}};
									props.form.setFormItemsValue(gl_brsettingFormId,defaultAccountBookData);
								}						
								item.queryCondition=()=>{
									return getCondition(props,'org','pk_accountingbook');									
								}
							}else if(item.attrcode=='pk_unit_v'){
								item.visible=isShowUnit;
								item.queryCondition=()=>{
									return getCondition(props,'org','pk_unit_v');									
								}
							}else if(item.attrcode=='pk_defdoc'){//业务系统
								item.queryCondition = () =>{
									return Object.assign({
										"appcode":'200260SETO',//按照组织级过滤
										"GridRefActionExt":'nccloud.web.gl.ref.BRSystemRefSqlBuilder'
									},config)
								}
							}
						})
						meta[br_filtercondFormId].items.map((item,index)=>{
							if(item.attrcode == 'pk_accasoa'){
								item.isMultiSelectedEnabled=true;
								item.queryCondition = () =>{
									return getCondition(props,'org','pk_accasoa');
								}
							}
						})
						meta[brmapDetailId].items.map((item,index)=>{
							if(item.attrcode=='accitemvalue'){//辅助核算
								item.pageInfo={pageIndex:-1};
							}else if(item.attrcode=='pk_brrelation'){//业务系统档案
								// item.queryCondition = () =>{
								// 	return {
								// 		"pk_defdoc":props.form.getFormItemsValue(gl_brsettingFormId, 'pk_defdoc').value,
								// 		"GridRefActionExt":"nccloud.web.gl.ref.BRInfoRefSqlBuilder"
								// 	}
								// }
							}
						})
					}
					modifierMeta(props, meta,context,self);
					props.meta.setMeta(meta);
					if(self.state.systemInfoModuleid||self.systemInfoModuleid){
						self.changeModuledId(self.state.systemInfoModuleid?self.state.systemInfoModuleid:self.systemInfoModuleid);
					}
					self.setState({
						RightPanelParam
					})
				}
				if(data.button){
					let button = data.button;
					props.button.setButtons(button, () => {
						let status = props.getUrlParam('status');
						let flag = status === 'browse' ? false : true;
						props.button.setButtonVisible(['save', 'cancel'], flag);
						props.button.setButtonVisible([ 'add', 'edit','delete'], !flag);
					});
				}

			}   
		}
	)
	ajax({
		url: "/nccloud/platform/appregister/queryallbtns.do", 
		data: {
		  pagecode: pageCode,
		  appcode: appcode //小应用id
		},
		async: false,
		success: function(res) {
		  if (res.data) {
			let button = res.data;
			props.button.setButtons(button)
			props.button.setButtonDisabled(disabledButtonsArr, true);
		  }
		}
	  });
}
 //获取默认会计期间 会计期间方案

function getDefaultYearmouth(pk_accountingbook){
//   let self=this;
    let isShowUnit={};
  // let {modalDefaultValue}=self.state;
  let url = '/nccloud/gl/voucher/queryBookCombineInfo.do';
  let pk_accpont = {"pk_accountingbook":pk_accountingbook}
  ajax({
	  url:url,
	  data:pk_accpont,
	  async:false,
	  success: function(response){
		  const { success } = response;
		  //渲染已有账表数据遮罩
		  if (success) {
			  if(response.data){
				isShowUnit.isShowUnit=response.data.isShowUnit;
				isShowUnit.pk_org=response.data.unit.value;
			  }
			  
		  }   
	  }
  });
  return isShowUnit;
}
function modifierMeta(props, meta,context,self) {
	let status = props.getUrlParam('status') =='browse' ? 'browse':"edit";	
	// let status = 'browse';
	if(props.getUrlParam('status')=='add'){//新增的时候设置默认值
		let ctrltypedata={'closeaccctrltype':{display:self.state.json['200260-0059'],value:'1'}};	/*国际化处理  不检查*/	
		props.form.setFormItemsValue(gl_brsettingFormId,ctrltypedata);
		let balanceoridata={'balanceori':{display:self.state.json['200260-0060'],value:'0'}};		/*国际化处理  借*/	
		props.form.setFormItemsValue(gl_brsettingFormId,balanceoridata);
		
	}
	if(meta[gl_brsettingFormId]){
		meta[gl_brsettingFormId].status = status;
	}
	if(meta[br_datatypeFormId]){
		meta[br_datatypeFormId].status = status;
	}
	if(meta[br_contentFormId]){
		meta[br_contentFormId].status = status;
	}
	if(meta[br_filtercondFormId]){
		meta[br_filtercondFormId].status = status;
	}
	
	return meta;
}
function getCondition(props,nodetype,attrcode){
	let config={
		"isDataPowerEnable":'Y',
		"DataPowerOperationCode":'fi'
	};
	let businessInfo = getBusinessInfo();
    let buziDate = businessInfo.businessDate;
	if(nodetype=='group'){
		let pk_setofbook = props.form.getFormItemsValue(gl_brsettingFormId, 'pk_setofbook').value;
		// let pk_accountingbook = props.form.getFormItemsValue(gl_brsettingFormId, 'pk_accountingbook').value;
		return Object.assign({
			pk_setofbook:pk_setofbook,
			pk_org : businessInfo.groupId,
			dateStr:buziDate.split(' ')[0]
		},config)
	}else if(nodetype=='org'){
		let pk_accountingbook = props.form.getFormItemsValue(gl_brsettingFormId, 'pk_accountingbook').value;
		if(attrcode=='pk_accasoa'){
			return Object.assign({
				pk_accountingbook:pk_accountingbook,
				dateStr:buziDate.split(' ')[0]
			},config)
		}else if(attrcode=='pk_unit_v'){
			return Object.assign({										
				"pk_accountingbook": pk_accountingbook,
				"TreeRefActionExt":'nccloud.web.gl.ref.GLBUVersionWithBookRefSqlBuilder'
			},config)
		}else if(attrcode=='pk_accountingbook'){
			return Object.assign({										
				"TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
				"appcode":"200260SETO"
			},config)
		}
		
	}
	
}