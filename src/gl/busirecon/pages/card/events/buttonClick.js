import { ajax, base, toast, promptBox ,cardCache} from 'nc-lightapp-front';
import GlobalStore from '../../../../public/components/GlobalStore';
import GetRequest from '../../../../public/components/GetRequestUrl.js';
import {dataSource,pkname,urls} from '../constants';
let { getCacheById, updateCache,addCache,getCurrentLastId,getNextId,deleteCacheById} = cardCache;
export default function (props, id) {
  // let searchUrl = this.props.location.search;
  // let urlObj = GetRequest(searchUrl);
  let c=this.props.getSearchParam('c');
  let status=this.props.getUrlParam('status');
   let nodetype=this.props.getUrlParam('nodetype');
  let pageCode=this.props.getUrlParam('pageCode')?this.props.getUrlParam('pageCode'):this.props.getUrlParam('p');
  let pk_brsettingValue=this.props.getUrlParam('pk_brsetting');
  switch (id) {
    case 'save':
      this.saveBill();
      break;
    case 'edit':
    let nodetype1=props.getUrlParam('nodetype');
      // props.linkTo(
      //   '/gl/busirecon/pages/card/index.html',
      //   {
      //     c:c,
      //     nodetype: nodetype,
      //     pageCode: pageCode,
      //     pk_brsetting:pk_brsettingValue,//'1001Z3100000000152EC',// pk_brsetting,
      //     status: 'edit'
      //   },
      // );
      props.pushTo('/card', {
          c:c,
          nodetype: nodetype1,
          pageCode: pageCode,
          pk_brsetting:pk_brsettingValue,//'1001Z3100000000152EC',// pk_brsetting,
          status: 'edit'
      })
      props.setUrlParam({status:'edit'});

      this.toggleShow('edit');
      break;
    case 'return': //返回
      //props.linkTo('/gl/busirecon/pages/list/index.html?nodetype='+nodetype);
      props.pushTo("/list", {
        nodetype:nodetype, 
        status: "browse",
        appcode:'200260SETG'
      });


      break

    case 'add':

      let nodetype=props.getUrlParam('nodetype');
      props.form.EmptyAllFormValue(this.gl_brsettingFormId);
      let ctrltypedata={'closeaccctrltype':{display:this.state.json['200260-0059'],value:'1'}};	/*国际化处理  不检查*/	
      props.form.setFormItemsValue(this.gl_brsettingFormId,ctrltypedata);
      let balanceoridata={'balanceori':{display:this.state.json['200260-0060'],value:'0'}};		/*国际化处理  借*/	
      props.form.setFormItemsValue(this.gl_brsettingFormId,balanceoridata);
      props.cardTable.setTableData(this.brmapDetailId, { rows: [] })
      this.QueryDefdocData('');
      props.pushTo('/card', {
        c: c,
        pageCode: pageCode,
        nodetype: nodetype,
        status: 'add'
      })
      props.setUrlParam({status:'add'});
      // setTimeout(()=>{
      //   this.props.cardTable.addRow(this.brmapDetailId);
      //     this.props.cardTable.setValByKeyAndIndex(this.brmapDetailId, 0, 'propertyid', { value:'1', display:'1' })
      // },100)


       this.toggleShow('add');
      break;
    case 'delete':
      let {pk_brsetting,ts}=this.state;
      let delUrl = '/nccloud/gl/busirecon/brsettingDel.do';
      promptBox({
        color:'warning',
        title:this.state.json['200260-0004'],
        content:this.state.json['200260-0005'],
        noFooter: false,                // 是否显示底部按钮(确定、取消),默认显示(false),非必输
        noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
        beSureBtnName:this.state.json['200260-0056'],          // 确定按钮名称, 默认为"确定",非必输
        cancelBtnName:this.state.json['200260-0007'],           // 取消按钮名称, 默认为"取消",非必输
        beSureBtnClick: dealOperate.bind(this,props, delUrl, id,pk_brsetting,ts,c,nodetype,pageCode),   // 确定按钮点击调用函数,非必输
        cancelBtnClick: cancelBtnClick.bind(this)  // 取消按钮点击调用函数,非必输
      })
      // dealOperate(this,props, delUrl, id,pk_brsetting,ts,c,nodetype,pageCode);
      break;
    //table肩部按钮操作
    case 'delline'://删行
      let rows = this.props.cardTable.getCheckedRows(this.brmapDetailId);
      if(rows.length==0){
        toast({ content: this.state.json['200260-0014'], color: 'warning' });
        return false;
      }else{
      let indexs = [];
      rows.map((record) => {
        indexs.push(record.index);
      });
      this.props.cardTable.delRowsByIndex(this.brmapDetailId, indexs);
    }
    
    break;  
    case 'addline'://增行
      this.props.cardTable.addRow(this.brmapDetailId);
    break;
    case 'cancel'://取消
    promptBox({
      color:"warning",
      title: this.state.json['200260-0007'],
      content: this.state.json['200260-0055'],
      beSureBtnName: this.state.json['200260-0056'],
      cancelBtnName: this.state.json['200260-0007'] ,
      beSureBtnClick: cancela.bind(this,props,status,c,nodetype,pageCode,pk_brsetting)
      });
    break
    default:
    break
  }
}
export function cancelBtnClick(){
  return false;
}
export function cancela(props,status,c,nodetype,pageCode,pk_brsetting){
  const tableIds = this.brmapDetailId;
  
  if (status === 'edit') {
    let currId=props.getUrlParam('pk_brsetting'); 
    let cardData = getCacheById(currId, dataSource);
    if(currId){
      props.setUrlParam({"status":'browse'});
      props.setUrlParam(currId);
      if(cardData){
        props.form.setAllFormValue({[this.gl_brsettingFormId]:cardData.head[this.gl_brsettingFormId]});
        props.cardTable.setTableData(tableIds,cardData.bodys[tableIds]);
      }else{
        let data = {
          pk_brsetting : currId,
          pageCode : this.props.getUrlParam('pageCode')?this.props.getUrlParam('pageCode'):this.props.getSearchParam('p')
        };
        let url = urls.queryCard;
        ajax({
          url: url,
          data : data,
          success : (res) => {
            if(res.success){
              this.loadData(res.data);
            }
          }
        });
      }
    }else{
      props.setUrlParam({"status":'browse'});
      props.form.EmptyAllFormValue(this.gl_brsettingFormId);
      props.cardTable.setTableData(tableIds, { rows: [] });
    }
    this.toggleShow('browse')
  }else if(status === 'add'){
    let id = getCurrentLastId(dataSource);
    let cardData = getCacheById(id, dataSource);
    if(id!=null && id!='' && id!=undefined){
      props.setUrlParam({"status":'browse'});
      props.setUrlParam({"pk_brsetting":id});
      if(cardData){
        props.form.setAllFormValue({[this.gl_brsettingFormId]:cardData.head[this.gl_brsettingFormId]});
        props.cardTable.setTableData(tableIds,cardData.body[tableIds]);
      }else{
        let data = {
          pk_brsetting : id,
          pageCode : this.props.getUrlParam('pageCode')?this.props.getUrlParam('pageCode'):this.props.getSearchParam('p')
        };
        let url = urls.queryCard;
        ajax({
          url: url,
          data : data,
          success : (res) => {
            if(res.success){
              this.loadData(res.data);
            }
          }
        });
        
      }
    }else{
      props.setUrlParam({"status":'browse'});
      props.form.EmptyAllFormValue(this.gl_brsettingFormId);
      props.cardTable.setTableData(tableIds, { rows: [] });
    }
    this.toggleShow('browse')
  }
}


export function dealOperate(props, opurl, id,pk_brsetting,ts,c,nodetype,pageCode) {
  let self = this;
  let nextId = getNextId(pk_brsetting, dataSource);

  const tableIds = self.brmapDetailId;
    ajax({
      url: opurl,
      data: {
          pk_brsetting: pk_brsetting,
          ts:ts
      },
      async:true,
      success: (res) => {
          let { success, data } = res;
          if (data&&data.error&&data.error.length > 0) {
              toast({ content: data.error, color: 'danger'});
              return false;
          }
          else if (success) {          
          }
          deleteCacheById(pkname,pk_brsetting,dataSource);
          if(nextId!=null){
            let nextCache = getCacheById(nextId, dataSource);
            if(nextCache){
              self.props.form.setAllFormValue({ [self.gl_brsettingFormId]: nextCache.head[self.gl_brsettingFormId] });
              self.props.cardTable.setTableData(tableIds, nextCache.body[tableIds]);
              self.setButtonStatus(nextCache);
              self.props.setUrlParam({id:nextId});
            }else{
              let data = {
                pk_brsetting : nextId,
                pageCode : self.props.getUrlParam('pageCode')?self.props.getUrlParam('pageCode'):self.props.getSearchParam('p')
              };
              let url = urls.queryCard;
              ajax({
                url: url,
                data : data,
                success : (res) => {
                  if(res.success){
                    self.loadData(res.data);
                  }
                }
                
              });
              self.props.setUrlParam({id:nextId});
            }
          }else{
            self.props.form.EmptyAllFormValue(self.gl_brsettingFormId);
            self.props.cardTable.setTableData(tableIds, { rows: [] });
            self.setState({
              status:'browse',qrytemplateData:{}
            },()=>{
              self.QueryDefdocData('');
            })
          }
          
      },
      error: (res) => {
          toast({ content: res.message, color: 'danger'});
          return false;
      }
  })

    // self.props.form.EmptyAllFormValue(self.gl_brsettingFormId);
    // self.props.cardTable.setTableData(tableIds, { rows: [] });
}
