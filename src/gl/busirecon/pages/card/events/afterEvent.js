import { ajax,toast } from 'nc-lightapp-front';
import { gl_brsettingFormId, br_datatypeFormId, br_contentFormId, br_filtercondFormId,
	brmapDetailId } from '../constants';
export default function afterEvent(props, moduleId, key,value, changedrows, valueSource, s, g) {
	let self=this;
	let {RightPanelParam,isShowUnit}=self.state;
	let status=self.props.getUrlParam('status');
	let meta = props.meta.getMeta();
	if (key === 'pk_accountingbook') {//切换账簿清空限制科目
		//切换账簿清空业务单元，科目
		let pk_accasoaData={"pk_accasoa":{display:"",value:""}};
		self.props.form.setFormItemsValue(gl_brsettingFormId,pk_accasoaData);
		self.props.form.setFormItemsValue(gl_brsettingFormId,{'pk_unit_v':{display:'',value:''}});
		// self.props.form.setFormItemsValue(gl_brsettingFormId,{'pk_defdoc':{display:'',value:''}});
		let url = '/nccloud/gl/voucher/queryBookCombineInfo.do';
		if(value.value){
			RightPanelParam.pk_accountingbook=value.value;
			let pk_accpont = {"pk_accountingbook":value.value}
			ajax({
				url:url,
				data:pk_accpont,
				success: function(response){
					const { success } = response;
					//渲染已有账表数据遮罩
					if (success) {
						if(response.data){
							isShowUnit=response.data.isShowUnit;
							RightPanelParam.pk_org=response.data.unit.value;
							RightPanelParam.accountingbookpk_org=response.data.unit.value;
							meta[gl_brsettingFormId].items.map((item,index)=>{
								if(item.attrcode=='pk_unit_v'){
									item.visible=isShowUnit;
								}
							})
							props.meta.setMeta(meta);
						}
						self.setState({
							isShowUnit,RightPanelParam,status:'add',
							qrytemplateData:{}
						},()=>{
							self.QueryDefdocData(RightPanelParam.pk_defdoc,'pk_accountingbook');
						})
					}   
				}
			});
		}else{
			isShowUnit=false;
			RightPanelParam.pk_accountingbook='';
			RightPanelParam.pk_org='';
			RightPanelParam.accountingbookpk_org='';
			// RightPanelParam.pk_defdoc='';
			meta[gl_brsettingFormId].items.map((item,index)=>{
				if(item.attrcode=='pk_unit_v'){
					item.visible=isShowUnit;
				}
			})
			props.meta.setMeta(meta);
			self.setState({
				isShowUnit,RightPanelParam,status:'add',qrytemplateData:{}
			},()=>{
				self.QueryDefdocData('','pk_accountingbook');
			})
		}
		// self.autoFocus('pk_accountingbook');
	}else if (key=='pk_setofbook') {//切换账簿清空限制科目
		let meta = props.meta.getMeta();
		let pk_accasoaData={"pk_accasoa":{display:"",value:""}};
		self.props.form.setFormItemsValue(gl_brsettingFormId,pk_accasoaData);
		self.props.form.setFormItemsValue(gl_brsettingFormId,{'pk_unit_v':{display:'',value:''}});
		self.props.form.setFormItemsValue(gl_brsettingFormId,{'pk_unit':{display:'',value:''}});
		// self.props.form.setFormItemsValue(gl_brsettingFormId,{'pk_defdoc':{display:'',value:''}});
		if(value.value){
			RightPanelParam.pk_setofbook=value.value;
			self.setState({
				RightPanelParam,status:'add',qrytemplateData:{}
			},()=>{
				self.QueryDefdocData(RightPanelParam.pk_defdoc,'pk_setofbook');
			})
		}else{
			RightPanelParam.pk_setofbook='';
			// RightPanelParam.pk_defdoc='';
			self.setState({
				RightPanelParam,status:'add',qrytemplateData:{}
			},()=>{
				self.QueryDefdocData('','pk_setofbook');
			})
		}
		
	}else if(key === 'pk_unit_v'){
		let pk_unitValue='1';//设置业务单元选中不同的值
		if(changedrows.refpk&&value.refpk==changedrows.refpk){
			pk_unitValue='0';//表示业务单元选中相同的值
		}	
		if(value.value){
			RightPanelParam.pk_org=value.value;
			self.props.form.setFormItemsValue(gl_brsettingFormId,{'pk_unit_v':{display:value.refname,value:value.values.pk_vid.value}});
			self.props.form.setFormItemsValue(gl_brsettingFormId,{'pk_unit':{display:value.refname,value:value.values.pk_org.value}});
			self.setState({
				RightPanelParam,status:'add',qrytemplateData:{}
			},()=>{
				self.QueryDefdocData(RightPanelParam.pk_defdoc,'pk_unit_v',pk_unitValue);
			})
		}else{
			RightPanelParam.pk_org=RightPanelParam.accountingbookpk_org;
			// RightPanelParam.pk_defdoc='';
			self.props.form.setFormItemsValue(gl_brsettingFormId,{'pk_unit_v':{display:'',value:''}});
			self.props.form.setFormItemsValue(gl_brsettingFormId,{'pk_unit':{display:'',value:''}});
			self.setState({
				RightPanelParam,status:'add',qrytemplateData:{}
			},()=>{
				self.QueryDefdocData(RightPanelParam.pk_defdoc,'pk_unit_v',pk_unitValue);
			})
		}
		// self.autoFocus('pk_unit_v');
	}else if(key === 'pk_defdoc'){//业务系统
		self.props.form.setFormItemsValue(gl_brsettingFormId,
			{'type_endbal':{value:false,display:'否'},
			'type_end_ocr':{display:'否',value:false},
			'type_balance':{display:'否',value:false},
			'con_amount':{value:false,display:'否'},
			'con_quantity':{display:'否',value:false},
			'con_locamount':{display:'否',value:false}
			});
		self.props.form.setFormItemsDisabled(gl_brsettingFormId,{'type_endbal':false,'type_end_ocr':false,'type_balance':false});
		if(value.value){
			RightPanelParam.pk_defdoc=value.value;
			self.setState({
				RightPanelParam,status:'add',qrytemplateData:{}
			},()=>{
				if(!RightPanelParam.pk_accountingbook&&!RightPanelParam.pk_setofbook){
					self.QueryDefdocData('','pk_defdoc');
				}else{
					self.QueryDefdocData(value.value,'pk_defdoc');
				}
				
			})
			
		}else{
			RightPanelParam.pk_defdoc='';
			self.setState({
				RightPanelParam,status:'add',qrytemplateData:{}
			},()=>{
				self.QueryDefdocData('','pk_defdoc');
			})
		}
		// self.autoFocus('pk_defdoc');
	}else if(key==='pk_accasoa'){//限制科目  同步更新表格braccasoa里的数据
		let statusValue=self.getStatus(status);
		let rowsArr=[];
		valueSource.map((item,index)=>{
			rowsArr.push(
				{
					rowid:null,
					status:statusValue,//1或2
					values:{
						braccasoas:null,//{display:'',value:''},
						dr:null,
						pk_accasoa:{display:item.refname,value:item.refpk},
						pk_braccasoa:null,
						status:{value:statusValue},
						ts:null
					}
				}
			)
		})
		let braccasoaDetaiRows={
			rows:rowsArr,
			"areacode":"braccasoa"
		}
		self.setState({
			braccasoaDetaiDatas:braccasoaDetaiRows
		})		
	}else if(key=='type_balance'){//余额  "type_endbal"贷方发生额 type_end_ocr "借方发生额"
		//当业务系统是IA（存货核算），勾选余额，自动勾选上借方发生额，贷方发生额，且借方发生额、贷方发生额不可编辑。当不勾选余额，借方发生额、贷方发生额可以可以编辑
		if(self.state.systemInfoModuleid){
			if(self.state.systemInfoModuleid=='IA'){
				if(value.value){
					self.props.form.setFormItemsValue(gl_brsettingFormId,{'type_endbal':{value:true,display:'是'},'type_end_ocr':{display:'是',value:true}});
					self.props.form.setFormItemsDisabled(gl_brsettingFormId,{'type_endbal':true,'type_end_ocr':true});
				}else{
					// self.props.form.setFormItemsValue(gl_brsettingFormId,{'type_endbal':{value:false,display:'否'},'type_end_ocr':{display:'否',value:false}});
					self.props.form.setFormItemsDisabled(gl_brsettingFormId,{'type_endbal':false,'type_end_ocr':false});
				}
			}
		}
	}else if(key=='pk_brrelation'){//业务系统档案
		if(value.refcode){
			s.values.pk_classid.value=value.values.pk_classid.value;
		}else{
			s.values.pk_classid.value='';
		}
	}
}
