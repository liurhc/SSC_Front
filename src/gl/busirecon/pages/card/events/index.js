import buttonClick from './buttonClick';
import initTemplate from './initTemplate';
import afterEvent from './afterEvent';
import beforeEvent from './beforeEvent';
export { buttonClick, afterEvent,beforeEvent,initTemplate};
