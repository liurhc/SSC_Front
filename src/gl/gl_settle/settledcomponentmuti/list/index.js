import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, pageTo, ajax, getBusinessInfo, base,getMultiLang} from 'nc-lightapp-front';
const { NCProgressBar } = base;
import ReferLoader from "../../../public/ReferLoader/index.js";
import './index.less';

// 自定义表格
class ProgressTable extends React.Component {
	constructor(props) {
		super(props);
		this.props = props;
		this.state={
			json:{}
		}
	}
	componentWillMount() {
        let callback= (json) =>{
            this.setState({json:json},()=>{
            })
        }
        getMultiLang({moduleId:'2002SETTLED',domainName:'gl',currentLocale:'zh-CN',callback});
    }
	render() {
		return (
			<div className="progress-table nc-theme-area-bgc">
				<table border="1">
					<thead className="nc-theme-Widgets-bgc">
						<tr>
							<th className="progress_head" style={{width:"68px"}}>{this.state.json['2002REPORTCOMSINGLE-000002']}</th>{/* 国际化处理： 会计期间*/}
							<th className="progress_head">{this.state.json['2002REPORTCOMSINGLE-000003']}/{this.state.json['2002REPORTCOMSINGLE-000004']}</th>{/* 国际化处理： 财务核算账簿编码,名称*/}
							<th className="progress_head" style={{width:"64px"}}>{this.state.json['2002REPORTCOMSINGLE-000005']}</th>{/* 国际化处理： 完成情况*/}
							<th className="progress_head" style={{width:"57px"}}>{this.state.json['2002REPORTCOMSINGLE-000006']}</th>{/* 国际化处理： 是否结账*/}
						</tr>
					</thead>
					<tbody>
						{
							this.props.data.map((item) => {
								if (item.key % 2 !== 0) {
									let processvalue = parseFloat(item.processvalue);
									return (
										<tr className="tr-progress">
											<td colspan="4" style={{paddingLeft:"6px"}}>
												<NCProgressBar colors={processvalue === 100 ? 'primary' : 'danger'} now={processvalue}></NCProgressBar>
											</td>
										</tr>
									)
								} else {
									return (
										<tr>
											<td className="fts12 fts1">{item.period}</td>
											<td className="fts12">
												<span style={{paddingTop:"11px",fontFamily:"MicrosoftYaHei"}}>{item.accountingbookcode}</span>
												<span style={{fontFamily:"MicrosoftYaHei"}} className="accounting">{item.accountingbookname}</span>
											</td>
											<td className="complateda nc-theme-title-font-c">{item.processvalue}</td>
											<td className={item.issettled === 'N' ? 'se-info' : 'se-success'} style={{paddingTop:"10px"}}>
											{
												//item.issettled === 'N' ? '未结账' : '已结账'
												item.issettled === 'Z'?"" :  item.issettled === 'N' ? this.state.json['2002REPORTCOMSINGLE-000000'] : this.state.json['2002REPORTCOMSINGLE-000001']/* 国际化处理： 未结账,已结账*/
											}
											</td>
										</tr>
									)
								}
							})
						}
					</tbody>
				</table>
			</div>
		);
	}
}
class SettledcomponentMuti extends Component {
	constructor(props) {
		super(props);
		this.props = props;
		this.state = {
			setRefVal:{},
			tableData: [],
			json:{}
		}
		// 获取默认值
		this.accountingbook = {};
		// 判断单组织还是多组织
		this.appcode === '2002REPORTCOMMULTI';
		this.pageType = 'Multi';
		
		ajax({
			url: '/nccloud/platform/appregister/queryappcontext.do',
			data: {
				appcode: this.appcode
			},
			success: (res) => {
				let { data } = res;
				this.accountingbook = {
					display: data.defaultAccbookName,
					value: data.defaultAccbookPk
				};
				let { setRefVal }=this.state;
					// 组织默认值
					setRefVal={
						refname: this.accountingbook.display || '--',
						refpk: this.accountingbook.value
					}
					
				this.setState({setRefVal});
				// 判断页面为单组织还是多组织
				if (this.pageType === 'single') {
					this.getSingleData();
				} else {
					this.getMultiData();
				}
			},
		})
		
		this.getSingleData = this.getSingleData.bind(this);
		this.getMultiData = this.getMultiData.bind(this);
	}
	/**
	 * @method 当前页面挂载完成时调用
	 */
	componentDidMount() {
	}
	componentWillMount() {
        let callback= (json) =>{
            this.setState({json:json},()=>{
            })
        }
        getMultiLang({moduleId:'2002SETTLED',domainName:'gl',currentLocale:'zh-CN',callback});
    }
	// 单组织请求方法
	getSingleData() {
		let { groupId, userId, businessDate } = getBusinessInfo();
		ajax({
			url: '/nccloud/gl/settled/singlereport.do',
			data: {
				pk_accountingbook: this.state.setRefVal.refpk,
				pk_group: groupId,
				userId: userId,
				businessDate: businessDate
			},
			success: (res) => {
				let { success, data } = res;
				if (success) {
					data.key = 0;
					this.setState({
						tableData: [data, { key: 1, processvalue: data.processvalue }]
					})
				} else {

				}
			},
			error: (res) => {

			}
		})
	}
	// 多组织请求方法
	getMultiData() {
		let { groupId, userId, businessDate } = getBusinessInfo();
		ajax({
			url: '/nccloud/gl/settled/multireport.do',
			data: {
				pk_group: groupId,
				userId: userId,
				businessDate: businessDate
			},
			success: (res) => {
				let { success, data } = res;
				if (success) {
					let newData = [];
					if(data.length<5)
					{
						data.forEach((item, index) => {
							item.key = index * 2;
							newData.push(item, {
								key: item.key + 1,
								processvalue: item.processvalue
							})
						})

						let count =5-data.length; 
						while(count>0)
						{
							newData.push(
								{
									accountingbookcode: '',
									accountingbookname: '',
									issettled: 'Z',
									key: -2*count + 10,
									period: '',
									processvalue: ''
								}
								,{
								key: -2*count + 11,
								processvalue: '0.00%'
							})
							count--;
						}

					}
					else
					{
						data.forEach((item, index) => {
							item.key = index * 2;
							newData.push(item, {
								key: item.key + 1,
								processvalue: item.processvalue
							})
						})
					}
					this.setState({
						tableData: newData
					})
				} else {

				}
			},
			error: (res) => {

			}
		})
	}
	// 打开小应用
	openTo() {
		pageTo.openTo('/gl/gl_settle/settledreport/list/index.html',
			{
				appcode: '2002REPORT',
				pagecode: '2002REPORT_LIST'
			}
		)
	}
	render() {
		let SetRefer = (() => {
			if (this.pageType === 'single') {
				return (
					<div className="se-refer">
						<span className="se-refer-text">
							{this.state.setRefVal.refname&&this.state.setRefVal.refname}
						</span>
						<div className="se-refer-container">
							<ReferLoader
								tag = 'test'
								refcode = 'uapbd/refer/org/AccountBookTreeRef/index.js'
								value = {this.state.setRefVal}
								isMultiSelectedEnabled = {false}
								showGroup = {false}
                				showInCludeChildren = {false}
								queryCondition = {()=>{
									let condition = {
										"TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
										appcode:'2002SETTLED'
									}
								return condition;
								
								}}
								container={window.top.document.body}
								onChange={(val) => {
									this.setState({
										setRefVal: val
									}, () => {
										this.getSingleData();
									})
								}}
								clickContainer={<span className="se-refer-switch">{this.state.json['2002REPORTCOMSINGLE-000007']}</span>}
                   			 />
						</div>
					</div>
				)
			} else {
				return <div className="mt10"></div>;
			}
		})();
		return (
			<div id="settledcomponent" className="nc-theme-Widgets-bgc">
				<div className="se-header nc-theme-Widgets-font-c">
					<span className="title nc-theme-Widgets-font-c">{this.state.json['2002REPORTCOMSINGLE-000008']}</span>{/* 国际化处理： 月结协作工作台*/}
					<i className="se-opento iconfont icon-gengduo" onClick={this.openTo}>

					</i>
					{SetRefer}
				</div>
				<div className="se-body nc-theme-Widgets-font-c">
					<ProgressTable
						data={this.state.tableData}
					/>
				</div>
			</div>
		);
	}
}

SettledcomponentMuti = createPage({
	// initTemplate: initTemplate
})(SettledcomponentMuti);

//ReactDOM.render(<Settledcomponent />, document.querySelector('#app'));
export default SettledcomponentMuti;
