import {toast, ajax} from 'nc-lightapp-front'


function cp2othersFun(self,settleDrules, pk_accountingbooks, callback){
    let url = '/nccloud/gl/settled/rulecopy.do'
    if(!settleDrules || settleDrules.length <= 0){
        toast({content:self.state.json['2002SETTLED-000013'],color:'warning'});/* 国际化处理： 请选择要复制的数据，系统预置检查项不需要复制*/
        return
    }
    if(!pk_accountingbooks || pk_accountingbooks.length <= 0){
        toast({content:self.state.json['2002SETTLED-000017'],color:'warning'});/* 国际化处理： 请选择核算账簿*/
        return
    }
    let data = {
        pk_settledrule: settleDrules,
        pk_accountingbook: pk_accountingbooks
    }
    ajax({
        url,
        data,
        success: (res) => {
            if(callback){
                callback(res);           
            }
        }
    })
}

/**
 * 移动到核算账簿之前校验是否存在重复编码的定义
 * @param {*} pk_accountingbooks 
 * @param {*} codes 
 * @param {*} callback 
 */
function checkMultiCode(pk_accountingbooks, codes, callback){
	let url = '/nccloud/gl/settled/rulequeryrepeat.do'
	let sendData = {
		"pk_accountingbook": pk_accountingbooks,
		"pk_settledrule": codes
	}
	ajax({
		url: url,
		data: sendData,
		success: (res) => {
			let { success, data } = res;
			if (success) {
                if(callback){
                    callback(data);
                }
			}
		},
		error: (res) => {
			toast({content:res.message,color:'warning'});
		}
	});
}

export {cp2othersFun, checkMultiCode}
