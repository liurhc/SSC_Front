import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base,high,toast,cacheTools,promptBox,getMultiLang,createPageIcon} from 'nc-lightapp-front';
import ReferLoader from "../../../public/ReferLoader/index.js";
import AccountingBookModal from '../../../gl_transfer/transferDef/containers/AccountingBookModal'
import CopySettledRuleCheckModal from '../../../public/components/CopySettledRuleCheckModal'
import {cp2othersFun,  checkMultiCode} from '../utils';
import {Header, HeaderButtonArea, HeaderSearchArea} from '../../../../fipub/public/components/layout/Header'
import './index.less'
let { NCMessage,NCPopconfirm} = base;
const { Refer } = high;
const { NCDropdown:Dropdown, NCIcon:Icon, NCMenu:Menu, NCButton:Button } = base;

const tableid = '2002SERULE_list';
const pagecode = '2002SERULE_LIST';
const urls = {
	save : '/nccloud/gl/settled/rulesave.do',
	query : '/nccloud/gl/settled/rulequery.do',
	delete : '/nccloud/gl/settled/ruledelete.do'
};
let allTableData = {};

//获取并初始化模板
 function initTemplate(self,props){
	//获取默认账簿
	let tableStatus = props.editTable.getStatus(tableid);
	let appcode = props.getUrlParam('c')||props.getSearchParam('c');
	cacheTools.set('settledruleappcode',appcode); 
    props.createUIDom({
		pagecode : pagecode,
		appcode : appcode
	},
	(data)=>{
		if(data.template){
			let meta = data.template;
			modifierMeta(props, meta,self)
			props.meta.setMeta(meta);
		}
		if(data.button){
			let button = data.button;
			props.button.setButtons(button);
			props.button.setButtonsVisible(['Add','Edit','Refresh','Copy'],true);
			props.button.setButtonsVisible(['Save','SaveNew','Cancel'],false);
			props.button.setButtonDisabled(['Copy','Del'],true);
		}
		if(data.context){
			cacheTools.set('settledruleaccountingbookpk',data.context.defaultAccbookPk); 
			cacheTools.set('settledruleaccountingbookname',data.context.defaultAccbookName);
			self.setState({pk_accountingbook:data.context.defaultAccbookPk, accountingbookname:data.context.defaultAccbookName});
			//默认账簿的数据
			getData(cacheTools.get('settledruleaccountingbookpk'),props,'add',self);
		}		
		props.button.setPopContent('TableDel',self.state.json['2002SETTLED-000000']);/* 国际化处理： 确定要删除吗？*/
	});
	
}

function modifierMeta(props,meta,self) {
	 meta[tableid].items.map((item, key) => {
	if(item.attrcode == 'handler')
	 {
		 item.isMultiSelectedEnabled=false;
	 }
	});
	meta[tableid].showindex=true; 
	//添加表格操作列
	let event = {
		label: self.state.json['2002SETTLED-000001'],/* 国际化处理： 操作*/
		attrcode: 'opr',
		key: 'opr',
		fixed:'right',
		itemtype: 'customer',
		visible:true,
		render(text, record, index) {
			let tableStatus = props.editTable.getStatus(tableid);
			return  props.button.createOprationButton(
                ['TableDel'],
                {
                    area: "table-opr-button",
                    buttonLimit: 3,
                    onButtonClick: (props, id) => tableButtonClick(props, id, text, record, index,self)
                }
			)
		}
	};
	meta[tableid].items.push(event);
	return meta;
}

function tableButtonClick(props, id, text, record, index,self) {
    switch (props,id) {
		case 'TableDel':
            let tableStatus = props.editTable.getStatus(tableid);
            if (tableStatus === 'browse') {
				
                let delObj = {
					rowId: index,
					status: '3',
					values: {
						pk_accountingbook: {
							display: self.state.json['2002SETTLED-000002'],/* 国际化处理： 账簿*/
							value: record.values.pk_accountingbook.value
						},
						pk_settledrule: {
							display: self.state.json['2002SETTLED-000003'],/* 国际化处理： 主键*/
							value: record.values.pk_settledrule.value
						}
					}
				};
				let data = {
					model: {
						areaType: 'table',
						pageinfo: null,
						rows: [ delObj ]
					}
				};
				ajax({
					url: urls['delete'],
					data,
					success: function(res) {
						let { success, data } = res;
						if (success) {
							//NCMessage.create({content: '删除成功', color: 'success', position: 'bottom'});
							toast({content:self.state.json['2002SETTLED-000004'],color:'success'});/* 国际化处理： 删除成功*/
							props.editTable.setTableData(tableid, data[tableid]);
						}
					}.bind(this)
				});
			
            } else if (tableStatus === 'edit') {
				props.editTable.deleteTableRowsByIndex(tableid, index);
            }
            break;
        default:
            break;
    }
}
class SingleTable extends Component {
	constructor(props) {
		super(props);
		this.props = props;
		this.state={
			status : props.getUrlParam('status'),
			accbookModalShow:false,
			showCopyCheckModal: false,
			noRepeatArr: [],
			searchValue:'',
			pk_accountingbook:cacheTools.get('settledruleaccountingbookpk'),
			accountingbookname:cacheTools.get('settledruleaccountingbookname'),
			json:{}
		}
	}
	
	componentWillMount() {
		let callback = (json) =>{
			this.setState({json:json},()=>{
				initTemplate(this,this.props);
			})
		}
		getMultiLang({moduleId:'2002SETTLED',currentLocale:'simpchn',domainName:'gl',callback})
	}
	
	componentDidUpdate(){
		let delet=document.getElementsByClassName("u-menu-item-title-wrapper")
		let len=delet.length
		let lenpiece=len/2
		if(delet[lenpiece]){
			for(let i=0;i<10;i++){
				delet[lenpiece+i].style.display="none"
			}
		}
		let deletedit=document.getElementsByClassName("row-edit-option")
		let lenedit=deletedit.length
		let lenpieceedit=lenedit/2
		if(deletedit[lenpieceedit]){
			for(let i=0;i<10;i++){
				deletedit[lenpieceedit+i].style.display="none"
			}
		}
	}

	//更新按钮状态
    updateButtonStatus(props){
		//此处控制按钮的隐藏显示及启用状态
		//编辑态
		if(this.props.editTable.getStatus(tableid) === 'edit'){
			this.props.button.setButtonsVisible(['Add','Edit','Copy','Refresh'],false);
			/* 后续需求可能会要求检查项逐条保存，这里先隐藏保存新增按钮，等需要逐条保存时再放出 */
			// this.props.button.setButtonsVisible(['Save','SaveNew','Cancel','Del','Add'],true);
			this.props.button.setButtonsVisible(['Save','Cancel','Del','Add'],true);
			this.props.button.setMainButton(['Add'], false);
			this.props.button.setMainButton(['Save'], true);
			this.setPageState()
		//浏览态
        }else{
			this.props.button.setButtonsVisible(['Add','Edit','Copy','Refresh'],true);
			this.props.button.setButtonsVisible(['Save','SaveNew','Cancel'],false);
			this.props.button.setMainButton(['Add'], true);
			this.props.button.setMainButton(['Save'], false);
			this.setPageState()
		}
		
	}
	setPageState(){
		let state=this.props.editTable.getStatus(tableid)
        switch(state){
            case 'browse':
                this.props.button.setPopContent('TableDel',this.state.json['2002SETTLED-000000']);/* 国际化处理： 确定要删除吗？*/
            break;
			case 'add':
			this.props.button.setPopContent('TableDel');
            break;
            case 'edit':
			this.props.button.setPopContent('TableDel');
            break;
		}
	}
	updateCopyButtonStatus(){
		let tableData = this.props.editTable.getCheckedRows(tableid);
		if(tableData.length==0){
			this.props.button.setButtonDisabled(['Copy','Del'],true);
		}
		else{
			for(let i=0;i<tableData.length;i++){
				if(tableData[i].index>9){
					this.props.button.setButtonDisabled(['Copy','Del'],false);
				}
			}
		}
	}
	beSureBtnClick(props,tableid){
		props.editTable.cancelEdit(tableid);
		props.editTable.showColByKey(tableid,'opr');
		 this.updateButtonStatus();
	}
	selectCheck(props, moduleId, record, index,status){
		if(status==true){
			let checkdata = props.editTable.getCheckedRows(tableid);
			let indexBefore=[]
			if(checkdata.length>=1){

				checkdata.forEach((item, key) => {
					if(item.index <10)
					{
						
						indexBefore.push(item.index)
					}
				})
				props.editTable.selectTableRows(tableid,indexBefore,false);
			}
		}
	}
	//按钮点击事件
	onButtonClick(props,id) {
		switch (id) {
			case 'Add':
				props.editTable.setStatus(tableid, 'edit');
				let number = props.editTable.getNumberOfRows(tableid);
				// let numberArray=[];
				// for(let i=0;i<number;i++)
				// {
				// 	numberArray.push(i);
				// }
				props.editTable.showColByKey(tableid,'opr');
				props.editTable.addRow(tableid, number, true, null);
				props.editTable.setValByKeyAndIndex(tableid, number, 'tasktype', { value:2, display:this.state.json['2002SETTLED-000005'], isEdit:false });/* 国际化处理： 人工检查项*/
				props.editTable.setValByKeyAndIndex(tableid, number, 'sendmessage', { value:true, display:this.state.json['2002SETTLED-000018'], isEdit:false });/* 国际化处理： 是*/
				props.editTable.setValByKeyAndIndex(tableid, number, 'enabled', { value:true, display:this.state.json['2002SETTLED-000018'], isEdit:false });/* 国际化处理： 是*/
				props.editTable.setEditableRowByIndex(tableid, [0,1,2,3,4,5,6,7,8,9], false);
				this.updateButtonStatus();
				break;
			case 'Edit':
				props.editTable.showColByKey(tableid,'opr');
				props.editTable.setStatus(tableid, 'edit');
				props.editTable.setEditableRowByIndex(tableid, [0,1,2,3,4,5,6,7,8,9], false);
				this.updateButtonStatus();
				break;
			case 'Cancel':
			promptBox({
				color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
				title: this.state.json['2002SETTLED-000006'],                // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输/* 国际化处理： 取消*/
				content: this.state.json['2002SETTLED-000007'],             // 提示内容,非必输/* 国际化处理： 确定要取消吗？*/
				noFooter: false,                // 是否显示底部按钮(确定、取消),默认显示(false),非必输
				noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
				beSureBtnName: this.state.json['2002SETTLED-000008'],          // 确定按钮名称, 默认为"确定",非必输/* 国际化处理： 确定*/
				cancelBtnName: this.state.json['2002SETTLED-000006'],         // 取消按钮名称, 默认为"取消",非必输/* 国际化处理： 取消*/
				hasCloseBtn:false,             //显示“X”按钮，默认不显示，不显示是false，显示是true
				beSureBtnClick: this.beSureBtnClick.bind(this,props,tableid),   // 确定按钮点击调用函数,非必输
			  // 取消按钮点击调用函数,非必输
				closeByClickBackDrop:true,//点击遮罩关闭提示框，默认是true点击关闭，阻止关闭是false
			})
		
                break;
            case 'Refresh':
                getData(cacheTools.get('settledruleaccountingbookpk'),props,'refresh',this);
				break;
			case 'Save':
				let saveData = props.editTable.getChangedRows(tableid);   //保存时，只获取改变的数据行而不是table的所有数据行，减少数据传输
				if(saveData.length==0)
				{
					toast({content:this.state.json['2002SETTLED-000009'],color:'success'});/* 国际化处理： 保存成功*/
					props.editTable.setStatus(tableid, 'browse');
					return;
				}
				let check = props.editTable.checkRequired(tableid,saveData);
				if(check)
				{
					saveData.forEach((val) => {
						val.values.pk_accountingbook.value=cacheTools.get('settledruleaccountingbookpk');
					})
					let saveSendData = {
						pageid:pagecode,
						model : {
							areaType: "table",
							pageinfo: null,
							rows: []
						}
					};
					saveSendData.model.rows = saveData;
					ajax({
						url: urls['save'],
						data:saveSendData,
						success: function (res)  {     //此处使用箭头函数，如果不使用箭头函数，一定要bind(this)
							let { success,data} = res;
							if (success) {
								toast({ color: 'success', content: this.state.json['2002SETTLED-000009'] });/* 国际化处理： 保存成功*/
								props.editTable.setStatus(tableid, 'browse');
								props.editTable.showColByKey(tableid,'opr');
								if(data){
									props.editTable.setTableData(tableid,data[tableid]);
								}
							}
						}.bind(this)
					});
				}
				
				break;
			case 'SaveNew':
				let saveNewData = props.editTable.getChangedRows(tableid);   //保存时，只获取改变的数据行而不是table的所有数据行，减少数据传输
				if(saveNewData.length==0)
				{
					toast({content:this.state.json['2002SETTLED-000009'],color:'success'});/* 国际化处理： 保存成功*/
					props.editTable.setStatus(tableid, 'edit', null);
					let saveNewNumber = props.editTable.getNumberOfRows(tableid);
					let saveNewNumberArray=[];
					for(let i=0;i<saveNewNumber;i++)
					{
						saveNewNumberArray.push(i);
					}
					props.editTable.showColByKey(tableid,'opr');
					props.editTable.addRow(tableid, number, true, null);
					props.editTable.setValByKeyAndIndex(tableid, saveNewNumber, 'tasktype', { value:2, display:this.state.json['2002SETTLED-000005'], isEdit:false });/* 国际化处理： 人工检查项*/
					props.editTable.setValByKeyAndIndex(tableid, number, 'sendmessage', { value:true, display:this.state.json['2002SETTLED-000018'], isEdit:false });/* 国际化处理： 是*/
					props.editTable.setValByKeyAndIndex(tableid, number, 'enabled', { value:true, display:this.state.json['2002SETTLED-000018'], isEdit:false });/* 国际化处理： 是*/
					props.editTable.setEditableRowByIndex(tableid, saveNewNumberArray, false);
					this.updateButtonStatus();
					return;
				}
				saveNewData.forEach((val) => {
					val.values.pk_accountingbook.value=cacheTools.get('settledruleaccountingbookpk');
				})
				let saveNewSendData = {
					pageid:pagecode,
					model : {
						areaType: "table",
						pageinfo: null,
						rows: []
					}
				};
				saveNewSendData.model.rows = saveNewData;
				ajax({
					url: urls['save'],
					data:saveNewSendData,
					success: function (res)  {     //此处使用箭头函数，如果不使用箭头函数，一定要bind(this)
						let { success,data} = res;
						if (success) {
							toast({ color: 'success', content: this.state.json['2002SETTLED-000009'] });/* 国际化处理： 保存成功*/
							if(data){
								props.editTable.setTableData(tableid,data[tableid]);

								props.editTable.setStatus(tableid, 'edit', null);
								let saveNewNumber = props.editTable.getNumberOfRows(tableid);
								let saveNewNumberArray=[];
								for(let i=0;i<saveNewNumber;i++)
								{
									saveNewNumberArray.push(i);
								}
								props.editTable.showColByKey(tableid,'opr');
								props.editTable.addRow(tableid, number, true, null);
								props.editTable.setValByKeyAndIndex(tableid, saveNewNumber, 'tasktype', { value:2, display:this.state.json['2002SETTLED-000005'], isEdit:false });/* 国际化处理： 人工检查项*/
								props.editTable.setValByKeyAndIndex(tableid, number, 'sendmessage', { value:true, display:this.state.json['2002SETTLED-000018'], isEdit:false });/* 国际化处理： 是*/
								props.editTable.setValByKeyAndIndex(tableid, number, 'enabled', { value:true, display:this.state.json['2002SETTLED-000018'], isEdit:false });/* 国际化处理： 是*/
								props.editTable.setEditableRowByIndex(tableid, saveNewNumberArray, false);
								this.updateButtonStatus();
							}
						}
					}.bind(this)
				});
				break;
			case "Del":
			promptBox({
				color:"warning",
				title: this.state.json['2002SETTLED-000010'],/* 国际化处理： 删除？*/
				content: this.state.json['2002SETTLED-000011'],/* 国际化处理： 确认要删除吗*/
				beSureBtnName: this.state.json['2002SETTLED-000012'],/* 国际化处理： 确认*/
				cancelBtnName: this.state.json['2002SETTLED-000006'] ,/* 国际化处理： 取消*/
				beSureBtnClick: this.alldelete.bind(this,props,tableid)
				});
				break;
			case 'Copy':
				let checkDatas = props.editTable.getCheckedRows(tableid);
				let settledrules =[];
		        for(let i=0;i<checkDatas.length;i++)
		        {
					if(checkDatas[i].index<10)
		            {
		            	continue;
		            }
		            settledrules.push(checkDatas[i].data.values.pk_settledrule.value);
		         }
		         if(settledrules.length==0)
		         {
		         	toast({content:this.state.json['2002SETTLED-000013'],color:'warning'});/* 国际化处理： 请选择要复制的数据，系统预置检查项不需要复制*/
		            return
				}
				this.setState({
					accbookModalShow: true
				})
				break;
		}
	
	}
	alldelete(props){
		let selectedData = props.editTable.getCheckedRows(tableid);
				if(selectedData.length==0){
					toast({content:this.state.json['2002SETTLED-000014'],color:'warning'});/* 国际化处理： 请选择要删除的数据*/
					//NCMessage.create({content: '请选择要删除的数据', color: 'error', position: 'bottom'})
					return; 
				}
				let delObjArray = [];
				let indexArray = [];
				selectedData.forEach((val) => {
					if(val.data.values.pk_settledrule)
					{
						let delObj = {
							values: {
								pk_accountingbook: {
									display: this.state.json['2002SETTLED-000002'],/* 国际化处理： 账簿*/
									value: val.data.values.pk_accountingbook.value
								},
								pk_settledrule: {
									display: this.state.json['2002SETTLED-000003'],/* 国际化处理： 主键*/
									value: val.data.values.pk_settledrule.value
								}
							}
						};
						delObjArray.push(delObj);
					}
					else
					{
						indexArray.push(val.index);
					}
				});	
				if(delObjArray.length>0)
				{
					let data = {
						model: {
							areaType: 'table',
							pageinfo: null,
							rows: delObjArray,
						}
					};
					ajax({
						url: urls['delete'],
						data,
						success: (res) => {     //此处使用箭头函数，如果不使用箭头函数，一定要bind(this)
							let { success, data } = res;
							if (success) {
								toast({content:this.state.json['2002SETTLED-000004'],color:'success'});/* 国际化处理： 删除成功*/
								props.editTable.setTableData(tableid, data[tableid]);
								//NCMessage.create({content: '删除成功', color: 'success', position: 'bottom'});
							}
						}
					});
				}
				else{
					toast({content:this.state.json['2002SETTLED-000004'],color:'success'});/* 国际化处理： 删除成功*/
					props.editTable.deleteTableRowsByIndex(tableid, indexArray);
				}		
			}

	handleCp2others = (v) => {
		let {accbookModalShow} = this.state;
		accbookModalShow = false;
		//this.setState({accbookModalShow});
		let pk_accountingbooks = [];
		if(v){
			v.map((item) => {
				pk_accountingbooks.push(item.refpk);
				if(pk_accountingbooks.length>0){
					this.setState({accbookModalShow});
				}
			})
		}
		this.checkMultiCodeBeforeAction(pk_accountingbooks)
	}
	checkMultiCodeBeforeAction = (pk_accountingbooks) =>{
		let self = this;
		let checkDatas = self.props.editTable.getCheckedRows(tableid);
		//let settledrules = checkDatas.map(item => item.data.values.pk_settledrule.value)
		let settledrules =[];
        for(let i=0;i<checkDatas.length;i++)
        {
			if(checkDatas[i].index<10)
            {
            	continue;
            }
            settledrules.push(checkDatas[i].data.values.pk_settledrule.value);
         }
		let needCp2Arr = []; /* 需要复制到的账簿pk */
		if(pk_accountingbooks.length==0)
		{
			return;
		}
		checkMultiCode(pk_accountingbooks, settledrules, (data) => {
			let self = this;
			if(data.length===0){
				cp2othersFun(self,settledrules, pk_accountingbooks, (res) =>{
					if(res.data.flag==true){
						toast({content:res.data.result,color:'success'});
					}
					else
					{
						toast({content:res.data.result,color:'warning'});
					}
				});
			} else{
				let repeat_pks = data.map(item => item.pk_accountingbook)
				pk_accountingbooks.map((item,index)=>{
					if(!repeat_pks.includes(item)){
						needCp2Arr.push(item);
					}
				})
				self.setState({
					repeatData: data,
					noRepeatArr: needCp2Arr,
					showCopyCheckModal : true
				})
			}
		
		});

	}
	handleCopyCheckOK = (data) => {
		let self = this;
		let checkDatas = self.props.editTable.getCheckedRows(tableid)
		//let settledrules = checkDatas.map(item => item.data.values.pk_settledrule.value);
		let settledrules =[];
        for(let i=0;i<checkDatas.length;i++)
        {
			if(checkDatas[i].index<10)
            {
            	continue;
            }
            settledrules.push(checkDatas[i].data.values.pk_settledrule.value);
         }
		let {noRepeatArr}=this.state
		let selfArr = []
		if(data && data.length>0){
			data.map((item,index)=>{
				selfArr.push(item.pk_accountingbook)
			})
		}
		let newPkrefs = noRepeatArr.concat(selfArr)

		cp2othersFun(self,settledrules, newPkrefs, (res) =>{
			if(res.data.flag==true){
				toast({content:res.data.result,color:'success'});
			}
			else
			{
				toast({content:res.data.result,color:'warning'});
			}
		});
		this.setState({
			showCopyCheckModal: false
		})
	}

	render() {
		let { table, button, search,editTable } = this.props;
		let { pk_accountingbook, accbookModalShow, accountingbookname }=this.state;
		let { createEditTable } = editTable;
		let { NCCreateSearch } = search;
		let { createButton } = button;
		let {NCFormControl,NCCheckbox} = base;
        const { Item } = Menu;
        let { createButtonApp } = button;
		pk_accountingbook = pk_accountingbook || '';
		accountingbookname = accountingbookname || '';
		return (
			<div className="nc-single-table" id="month_check">
				<Header showGoback={false}>
					<HeaderSearchArea>
						<ReferLoader
							tag = 'test'
							fieldid='accountbook_ref'
							refcode = 'uapbd/refer/org/AccountBookTreeRef/index.js'
							value = {{refname: accountingbookname,refpk: pk_accountingbook}}
							isMultiSelectedEnabled = {false}
							queryCondition = {()=>{
								let condition = {
								"TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
								appcode:''
								}
								let appcode = cacheTools.get('settledruleappcode');
								if(appcode)
								{
									condition.appcode = appcode;
								}
							
							return condition;
							
							}}
							onChange ={(v)=>{
											pk_accountingbook = v.refpk;
											accountingbookname = v.refname;
											cacheTools.set('settledruleaccountingbookname',v.refname);
											cacheTools.set('settledruleaccountingbookpk',v.refpk);
											this.setState({
												pk_accountingbook, accountingbookname
											})
											getData(pk_accountingbook,this.props,'change',this);
										}}
						/>
					</HeaderSearchArea>
					<HeaderButtonArea>
						{createButtonApp({
                            area: 'header-button-area',//按钮注册中的按钮区域
                            buttonLimit: 1, 
                            onButtonClick: this.onButtonClick.bind(this),
                            popContainer: document.querySelector('.header-button-area')
                        })}
					</HeaderButtonArea>
				</Header>
				{/* 列表区 */}
				<div className='nc-singleTable-table-area"'>
					{createEditTable(tableid, {//列表区
						useFixedHeader:true,    
						selectedChange: this.updateCopyButtonStatus.bind(this), // 选择框有变动的钩子函数
						statusChange: this.updateButtonStatus.bind(this),   //表格状态监听
						showIndex:true,				                        //显示序号
						showCheck:true,
						onSelected:this.selectCheck.bind(this),			                            //显示复选框
						adaptionHeight:true
					})}
				</div>
				<AccountingBookModal
					show = {accbookModalShow}
					context = {{appcode: this.props.getSearchParam('c')}}
					onConfirm = {this.handleCp2others}
					onClose = {() => {
						this.setState({
							accbookModalShow: false
						});
					}}
				/>
				<CopySettledRuleCheckModal
					visible={this.state.showCopyCheckModal}
					repeatData = {this.state.repeatData}
					handleCopyCheckOK={this.handleCopyCheckOK.bind(this)}
					handleCancel={() => {
						this.setState({
							showCopyCheckModal: false
						})
					}}
				/>
			</div>
		);
	}
}
//请求列表数据
function getData(pk_accountingbook,props,flag,self) {
	if(pk_accountingbook)
	{
		let tableid  ='2002SERULE_list';
			ajax({
				url: urls['query'],
				data: {	
					"pk_accountingbook": pk_accountingbook
				},
				success: (res) => 
				{
					let { success, data } = res;
					if (success) 
					{
						if(flag=='refresh')
						{
							toast({ color: 'success', content: self.state.json['2002SETTLED-000019']/* 国际化处理： 刷新成功！*/ });
						}
						allTableData = data[tableid];
						props.button.setButtonDisabled(['Add','Edit','Refresh'], false);
						props.editTable.setTableData(tableid, data[tableid]);
					}
				}
			});
		}
		else
		{
			if(flag=='refresh')
			{
				toast({ color: 'success', content: self.state.json['2002SETTLED-000019']/* 国际化处理： 刷新成功！*/ });
			}
			props.button.setButtonDisabled(['Add','Edit','Refresh','Copy'], true);
			props.editTable.setTableData(tableid,{rows:[]});
		}
	}
	
SingleTable = createPage({

})(SingleTable);

ReactDOM.render(<SingleTable />, document.querySelector('#app'));
