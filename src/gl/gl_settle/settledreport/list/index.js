
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base,cacheTools ,getMultiLang,createPageIcon} from 'nc-lightapp-front';
let { NCTabsControl } = base;
import { buttonClick, initTemplate, searchBtnClick,onAfterEvent} from './events';
import {Header, HeaderSearchArea, HeaderButtonArea} from '../../../../fipub/public/components/layout/Header'
import './index.less'
class List extends Component {
	constructor(props) {
		super(props);
		this.state={
			allButtonsKey :[],
			pk_accperiodscheme:'',
			json:{},
			inlt:null
		};
		this.moduleId = '2002';
		this.searchId = '2002REPORT_query';
		this.tableId = '2002REPORT_list';
		let {form, button, table, insertTable, search} = this.props;
        let {setSearchValue, setSearchValByField, getAllSearchData} = search;
        this.setSearchValByField = setSearchValByField;//设置查询区某个字段值
		this.getAllSearchData = getAllSearchData;//获取查询区所有字段数据
		//initTemplate.call(this,props);
	}

	componentWillReceiveProps(nextProps){
	}

	componentDidMount() {
	}
	componentWillMount() {
        let callback= (json,status,inlt) =>{
			this.setState({
				json:json,
				inlt
			},()=>{
            })
        }
        getMultiLang({moduleId:'2002SETTLED',domainName:'gl',currentLocale:'zh-CN',callback});
    }
	onRowDoubleClick = (record,index)=>{
		let pk_accperiodmonth = cacheTools.get('settledreportpk_accperiodmonth');
		this.props.linkTo('/gl/gl_settle/settledreport/card/index.html', {
			pagecode:'2002REPORT_CARD',
			status:'browse',
			pk_accountingbook : record.pk_accountingbook.value,
			pk_accperiodmonth : pk_accperiodmonth
		});
	}

	updateSettledButtonStatus = (props)=>{
		let tableData = props.table.getCheckedRows('2002REPORT_list');
		if(tableData.length>0)
		{
			props.button.setButtonDisabled(['Settled'],false);
		}
		else
		{
			props.button.setButtonDisabled(['Settled'],true);
		}
	}

	render() {
		let { table, button, search } = this.props;
		let buttons = this.props.button.getButtons();
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { createButton, getButtons } = button;
		return (
			<div className="nc-bill-list" id="monthreport">
				<Header
					showGoback={false}
				>
					<HeaderButtonArea>
						{this.props.button.createButtonApp({
							area: 'list_head',
							buttonLimit: 3, 
							onButtonClick:buttonClick.bind(this), 
							popContainer: document.querySelector('.header-button-area')
						})}
					</HeaderButtonArea>
				</Header>
				<div className="nc-bill-search-area">
					{NCCreateSearch(
						this.searchId,
						{
							clickSearchBtn: searchBtnClick.bind(this),//   点击按钮事件
							showAdvBtn: true,                           //  显示高级按钮
							//查询区编辑后事件
							onAfterEvent: onAfterEvent.bind(this)
						}
					)}
				</div>
				<div className="nc-bill-table-area">
					{createSimpleTable(this.tableId, {
						lazyload: false,
						showCheck: true,
						showIndex: true,
						onRowDoubleClick : this.onRowDoubleClick,
						selectedChange : this.updateSettledButtonStatus.bind(this), // 选择框有变动的钩子函数
						adaptionHeight:true
					})}
				</div>
			</div>
		);
	}
}

List = createPage({
	initTemplate: initTemplate,
	//mutiLangCode: '2052'
})(List);

ReactDOM.render(<List />, document.querySelector('#app'));
