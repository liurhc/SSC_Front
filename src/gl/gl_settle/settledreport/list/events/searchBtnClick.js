import {ajax,toast,cacheTools} from 'nc-lightapp-front';
//点击查询，获取查询区数据
export default function clickSearchBtn(props,searchVal,flag) {
    if(searchVal){
        let pageId = '2002REPORT_LIST';
        let searchId = '2002REPORT_query';
        let tableId = '2002REPORT_list';
        let pageInfo = props.table.getTablePageInfo(tableId);
        let data={ 
                querycondition:searchVal,
                pageInfo:pageInfo,
                queryAreaCode:searchId, 
                querytype:'tree',
                pageId: pageId,    
        };
        cacheTools.set(searchId, searchVal);
        searchVal.conditions.forEach((val)=>
        {
            if(val.field=='accperiod')
            {
                cacheTools.set('settledreportpk_accperiodmonth',val.value.firstvalue);
            }
        })   
        ajax({
            url: '/nccloud/gl/settled/reportlistquery.do',
            data: data,
            success: (res) => {
                let { success, data } = res;
                if (success) {
                    if(data)
                    {
                        if(flag=='update')
                        {

                        }
                        else if(flag=='simple')
                        {
                            let len=res.data[tableId].rows.length
                            toast({ color: 'success', content: this.state.inlt&&this.state.inlt.get('2002REPORT-000020',{count:len}) });/* 国际化处理： 查询成功，共,条。*/
                        }
                        props.table.setAllTableData(tableId, data[tableId]);
                    }
                    else
                    {
                        if(flag=='update')
                        {

                        }
                        else if(flag=='simple')
                        {
                            toast({ color: 'warning', content: this.state.json['2002REPORT-000022'] });/* 国际化处理： 未查询出符合条件的数据！*/
                        }
                        props.table.setAllTableData(tableId, {rows:[]});
                    }
                }
            }
        });
    }
};
