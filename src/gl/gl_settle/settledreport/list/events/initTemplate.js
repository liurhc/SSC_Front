import { createPage, ajax, base, toast,cacheTools } from 'nc-lightapp-front';
import clickSearchBtn from './searchBtnClick';
let { NCPopconfirm, NCIcon } = base;
let pageId = '2002REPORT_LIST';
let searchId = '2002REPORT_query';
let tableId = '2002REPORT_list';
let accountingbookField ='pk_accountingbook';
let monthField ='accperiod';

export default function (props) {
	let appcode = props.getUrlParam('c')||props.getSearchParam('c');
	cacheTools.set('settledreportappcode',appcode); 
	props.createUIDom(
		{
			pagecode: pageId,
			appcode: appcode
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta)
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					props.button.setButtonDisabled(['Settled'],true);
				}
				if(data.context){
					//设置查询框默认值
					if(data.context.defaultAccbookPk)
					{
						cacheTools.set('settledreportaccountingbookpk',data.context.defaultAccbookPk); 
						cacheTools.set('settledreportaccountingbookname',data.context.defaultAccbookName);
						ajax({
							url: '/nccloud/gl/settled/reportlistaccperiod.do',
							data: data.context.defaultAccbookPk,
							success: (res) => { 
								if(res.data)
								{
									cacheTools.set('settledreportpk_accperiodscheme',res.data['pk_accperiodscheme']);
									cacheTools.set('settledreportpk_accperiodmonth', res.data['pk_accperiodmonth']);
									let accountingbookData = {
										display:data.context.defaultAccbookName,value:data.context.defaultAccbookPk
									};
									let monthData = {
										display:res.data['month'],value:res.data['pk_accperiodmonth']
									};
									props.search.setSearchValByField(searchId,accountingbookField,accountingbookData)
									props.search.setSearchValByField(searchId,monthField,monthData)
				
								}
							}
						})
					}
				}
				//页面跳转，初始化页面数据
				if(props.getUrlParam('jumpflag'))
				{
					let searchVal = cacheTools.get(searchId);
					clickSearchBtn(props,searchVal,'update');
				}	
			}
		}
	)
}

function modifierMeta(props, meta) {
	meta[searchId].items = meta[searchId].items.map((item, key) => {
		if(item.attrcode == 'pk_accountingbook')
		{
			item.isMultiSelectedEnabled = true;
			item.showGroup = false;
			item.showInCludeChildren = true;
			item.queryCondition = ()=> {
				let condition = {
					"TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
					
					appcode:''
					}
					let appcode = cacheTools.get('settledreportappcode');
					if(appcode)
					{
						condition.appcode = appcode;
					}
				return condition;
			}
		}
		else if(item.attrcode == 'accperiod')
		{
			item.queryCondition = ()=> {
				return {
					"pk_accperiodscheme":cacheTools.get('settledreportpk_accperiodscheme'),
					"GridRefActionExt":'nccloud.web.gl.ref.FilterAdjustPeriodRefSqlBuilder'
				}
			}
		}
		item.visible = true;
		item.col = '3';
		return item;
	})

	meta[tableId].items = meta[tableId].items.map((item, key) => {
		if (item.attrcode == 'accountingbookcode') {
			item.render = (text, record, index) => {
				return (
					<a
						style={{cursor: 'pointer'}}
						onClick={() => {
						    let pk_accperiodmonth = cacheTools.get('settledreportpk_accperiodmonth');
							props.linkTo('/gl/gl_settle/settledreport/card/index.html', {
								pagecode:'2002REPORT_CARD',
								status:'browse',
								pk_accountingbook : record.pk_accountingbook.value,
								pk_accperiodmonth : pk_accperiodmonth
							});
						}}
					>
						{ record.accountingbookcode && record.accountingbookcode.value }
					</a>
				);
			};
		}
		return item;
	});
	//let multiLang = props.MutiInit.getIntl('2052');
	return meta;
}
