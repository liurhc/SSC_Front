import { ajax,toast,cacheTools } from 'nc-lightapp-front';
let searchId =  '2002REPORT_query'
let field ='accperiod'
 export default function onAfterEvent(field, val) {
	let self = this; 
	if(field =='pk_accountingbook')
	{
		if(val.length==0)
		{
			setInitData(self.props,null,null);
		}
		else
		{
			//切换账簿获取会计期间方案
			ajax({
				url: '/nccloud/gl/settled/reportlistaccperiod.do',
				data: val[0].refpk,
				success: (res) => { 
					if(JSON.stringify(res.data)!='{}')
					{
						cacheTools.set('settledreportpk_accperiodscheme',res.data['pk_accperiodscheme']);
						cacheTools.set('settledreportpk_accperiodmonth', res.data['pk_accperiodmonth']);
						setInitData(self.props,res.data['month'],res.data['pk_accperiodmonth']);
					}
				}
			})
		}
	}
	else if(field =='accperiod')
	{
		if(val.refpk)
		{
			cacheTools.set('settledreportpk_accperiodmonth', val.refpk);
		}
	}
 }
 function setInitData(props,month,pk_accperiodmonth) {
	let data = {
			  display:month,value:pk_accperiodmonth
		  };
	  props.search.setSearchValByField(searchId,field,data)
  
  }


