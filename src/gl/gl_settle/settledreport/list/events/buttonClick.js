import { ajax, base, toast,cacheTools,getBusinessInfo } from 'nc-lightapp-front';
import clickSearchBtn from './searchBtnClick';
export default function buttonClick(props, id) {
    let pageId = '2002REPORT_LIST';
    let searchId = '2002REPORT_query';
    let tableId = '2002REPORT_list';
    let searchVal =cacheTools.get(searchId);
    let businessInfo = getBusinessInfo();
    let userId = businessInfo.userId;
    switch (id) {
        case 'Settled':
            let selectedData = props.table.getCheckedRows(this.tableId);
            if(selectedData.length==0)
            {
              toast({color: 'warning', content: this.state.json['2002REPORT-000019']});/* 国际化处理： 请选择数据*/
              return;
            }
            let settledData=[];
            selectedData.forEach((val) => {
                settledData.push({
                    pk_accountingbook:val.data.values.pk_accountingbook.value,
                    pk_accperiodmonth :  cacheTools.get('settledreportpk_accperiodmonth'),
                    userid : userId
                     }
                );
            });
            ajax({
                url: '/nccloud/gl/settled/reckoning.do',
                data: settledData,
                success: (res) => { 
                    if (res.data) 
                    {
                      if(res.data.flag==true)
                      {
                        toast({color: 'success', content: res.data.result});
                        clickSearchBtn(props,searchVal,'update');
                      }
                      else
                      {
                        toast({color: 'error', content: res.data.result});
                        clickSearchBtn(props,searchVal,'update');
                      }
                    }
                    
                }
            });
            break;
        case 'Refresh':
            let self = this;
            clickSearchButton(props,searchVal,'refresh',self);
            break;
        default:
            break;
    }
}
function clickSearchButton(props,searchVal,flag,self) {
    if(searchVal){
        let pageId = '2002REPORT_LIST';
        let searchId = '2002REPORT_query';
        let tableId = '2002REPORT_list';
        let pageInfo = props.table.getTablePageInfo(tableId);
        let data={ 
                querycondition:searchVal,
                pageInfo:pageInfo,
                queryAreaCode:searchId, 
                querytype:'tree',
                pageId: pageId,    
        };
        cacheTools.set(searchId, searchVal);
        ajax({
            url: '/nccloud/gl/settled/reportlistquery.do',
            data: data,
            success: (res) => {
                let { success, data } = res;
                if (success) {
                    if(data)
                    {
                        if(flag=='refresh')
                        {
                            toast({ color: 'success', title:self.state.json['2002REPORT-000021']});/* 国际化处理：刷新成功！*/
                        }
                        props.table.setAllTableData(tableId, data[tableId]);
                    }
                    else
                    {
                        if(flag=='refresh')
                        {
                            toast({ color: 'success', title:self.state.json['2002REPORT-000021']});/* 国际化处理：刷新成功！*/
                        }
                        props.table.setAllTableData(tableId, {rows:[]});
                    }
                }
            }
        });
    }
}
