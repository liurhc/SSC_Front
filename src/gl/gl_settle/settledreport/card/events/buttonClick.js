import { ajax, base, toast,cacheTools,getBusinessInfo,promptBox } from 'nc-lightapp-front';
let tableId = '2002REPORT_bodys';
let formId = '2002REPORT_head';
let businessInfo = getBusinessInfo();
let userId = businessInfo.userId;

export default function(props, id) {
  switch (id) {
    case 'Back':
      props.linkTo('/gl/gl_settle/settledreport/list/index.html',{pagecode:'2002REPORT_LIST',jumpflag: true})
    break
    case 'Edit': dealEdit(this); break;
    case 'Refresh':
      let refreshData = { 
        pk_accountingbook:cacheTools.get('settledreportaccountingbookpk'),
        pk_accperiodmonth:cacheTools.get('settledreportpk_accperiodmonth')
      };
      ajax({                      
        url: '/nccloud/gl/settled/reportcardquery.do',
        data: refreshData,
        success: (res) => 
        {
          if (res.data) 
          {
            toast({ color: 'success', title:this.state.json['2002REPORT-000021']});/* 国际化处理：刷新成功！*/
            if (res.data.head) 
            {
              res.data.head[formId].rows[0].values.period.value=res.data.head[formId].rows[0].values.year.value+"-"+res.data.head[formId].rows[0].values.period.value;
              //按钮控制
              if(res.data.head[formId].rows[0].values.issettled.value==1)
              {
                props.button.setButtonVisible(['Back','Edit','Refresh','Load','Settled'], true);
                props.button.setButtonVisible(['Save','Cancel'], false);
              }
              else
              {
                props.button.setButtonVisible(['Back','Refresh'], true);
                props.button.setButtonVisible(['Edit','Settled','Load','Save','Cancel'], false);
              }
              props.form.setAllFormValue({[formId]: res.data.head[formId] });
            }
            if (res.data.body) 
            {
              props.editTable.setTableData(tableId, res.data.body[tableId]);
            }
          } 
          else
          {
            props.form.setAllFormValue({ [formId]: { rows: [] } });
            props.editTable.setTableData(tableId, { rows: [] });
          }
        }
      });
    break;
      case 'Load':
      let loadPk_settledreport = props.form.getFormItemsValue(formId,'pk_settledreport').value;
      let loadData = { 
        pk_accountingbook:cacheTools.get('settledreportaccountingbookpk'),
        pk_accperiodmonth:cacheTools.get('settledreportpk_accperiodmonth'),
        pk_settledreport:loadPk_settledreport?loadPk_settledreport:null
      };
      promptBox({
        color: 'warning',
        content: this.state.json['2002REPORT-000002'],/* 国际化处理： 该操作将删除已保存的数据，重新载入检查项，请谨慎操作*/
        beSureBtnClick: ()=>{
          ajax({
            url: '/nccloud/gl/settled/reportcardreload.do',
            data :loadData,
              success: (res) =>
              {
                if (res.data) 
                {
                  toast({color: 'success', content: this.state.json['2002REPORT-000003']});/* 国际化处理： 操作成功*/
                  if (res.data.head) 
                  {
                    res.data.head[formId].rows[0].values.period.value=res.data.head[formId].rows[0].values.year.value+"-"+res.data.head[formId].rows[0].values.period.value;
                    if(res.data.head[formId].rows[0].values.issettled.value==1)
                    {
                      props.button.setButtonVisible(['Back','Edit','Refresh','Load','Settled'], true);
                      props.button.setButtonVisible(['Save','Cancel'], false);
                    }
                    else
                    {
                      props.button.setButtonVisible(['Back','Refresh','Load'], true);
                      props.button.setButtonVisible(['Edit','Settled','Save','Cancel'], false);
                    }
                    props.form.setAllFormValue({ [formId]: res.data.head[formId] });
                  }
                  if (res.data.body) 
                  {
                    props.editTable.setTableData(tableId, res.data.body[tableId]);
                  }
                } 
                else
                {
                  props.form.setAllFormValue({ [formId]: { rows: [] } });
                  props.editTable.setTableData(tableId, { rows: [] });
                }
            }
          })
        }
      })
      break
    case 'Settled':
      let settledData=[];
      settledData.push(
        {
          pk_accountingbook : cacheTools.get('settledreportaccountingbookpk'),
          pk_accperiodmonth :  cacheTools.get('settledreportpk_accperiodmonth'),
          userid : userId
        }
      )
      ajax({
        url: '/nccloud/gl/settled/reckoning.do',
        data: settledData,
        success: (res) => { 
          if (res.data) 
          {
            if(res.data.flag==true)
            {
              toast({color: 'success', content: res.data.result});
              query(props);
            }
            else
            {
              toast({color: 'error', content: res.data.result});
              query(props);
            }
          }
        }
      });
    break
    case 'Save':
      dealSave(this);
    break
    case 'Cancel':
		promptBox({
      color: 'warning',             
      title: this.state.json['2002REPORT-000005'],               /* 国际化处理： 取消*/
      content: this.state.json['2002REPORT-000006'],            /* 国际化处理： 确定要取消吗？*/
      noFooter: false,                
      noCancelBtn: false,             
      beSureBtnName: this.state.json['2002REPORT-000007'],          /* 国际化处理： 确定*/
      cancelBtnName: this.state.json['2002REPORT-000005'],        /* 国际化处理： 取消*/
      hasCloseBtn:false,             
      beSureBtnClick: beSureBtnClick.bind(this,props,tableId),   
      closeByClickBackDrop:true,
    })
    break
    default:
      break
  }
  
}
function query(props)
{
  let refreshNewData = { 
    pk_accountingbook:cacheTools.get('settledreportaccountingbookpk'),
    pk_accperiodmonth:cacheTools.get('settledreportpk_accperiodmonth')
  };
  ajax({                      
    url: '/nccloud/gl/settled/reportcardquery.do',
    data: refreshNewData,
    success: (res) => 
    {
      if (res.data) 
      {
        if (res.data.head) 
        {
          res.data.head[formId].rows[0].values.period.value=res.data.head[formId].rows[0].values.year.value+"-"+res.data.head[formId].rows[0].values.period.value;
          //按钮控制
          if(res.data.head[formId].rows[0].values.issettled.value==1)
          {
            props.button.setButtonVisible(['Back','Edit','Refresh','Load','Settled'], true);
            props.button.setButtonVisible(['Save','Cancel'], false);
          }
          else
          {
            props.button.setButtonVisible(['Back','Refresh'], true);
            props.button.setButtonVisible(['Edit','Settled','Load','Save','Cancel'], false);
          }
          props.form.setAllFormValue({[formId]: res.data.head[formId] });
        }
        if (res.data.body) 
        {
          props.editTable.setTableData(tableId, res.data.body[tableId]);
        }
      } 
      else
      {
        props.form.setAllFormValue({ [formId]: { rows: [] } });
        props.editTable.setTableData(tableId, { rows: [] });
      }
    }
  });
}
//取消弹窗确定按钮事件
function beSureBtnClick(props,tableId)
{
    props.linkTo('/gl/gl_settle/settledreport/card/index.html',{
      pagecode:'2002REPORT_CARD', 
      status: 'browse',
      pk_accountingbook:cacheTools.get('settledreportaccountingbookpk'),
      pk_accperiodmonth:cacheTools.get('settledreportpk_accperiodmonth')
    })
    props.button.setButtonVisible(['Back','Edit','Refresh','Load','Settled'], true);
    props.button.setButtonVisible(['Save','Cancel'], false);
    props.editTable.cancelEdit(tableId);
    props.editTable.setStatus(tableId, 'browse');
}

function dealSave(page){
    let props = page.props;
      let tableData = props.editTable.getChangedRows(tableId); 
      //只保存人工检查项
      let dataObj = [];
      if(tableData.length==0)
      {
  
      }
      else
      {
          tableData.forEach((val) => {
            dataObj.push({
              pk_settledrule:val.values.pk_settledrule.value,
              isfinished:val.values.isfinished.value,
              finishtime:val.values.finishtime.value,
              checkresult:val.values.checkresult.value,
                  }
              );
          });
          
        }
        let savePk_settledreport = props.form.getFormItemsValue(formId,'pk_settledreport').value;
        let savedata = {
          pk_accountingbook : cacheTools.get('settledreportaccountingbookpk'),
          pk_accperiodmonth :  cacheTools.get('settledreportpk_accperiodmonth'),
          pk_settledreport:savePk_settledreport?savePk_settledreport:null,
          itemdata:dataObj
        };
          ajax({
            url: '/nccloud/gl/settled/reportcardsave.do',
            data:savedata,
            success: function (res) 
            {     //此处使用箭头函数，如果不使用箭头函数，一定要bind(this)
              let { success,data} = res;
              if (success) 
              {
                toast({ color: 'success', content: this.state.json['2002REPORT-000004'] });/* 国际化处理： 保存成功*/
                if (data.head) 
                {
                  data.head[formId].rows[0].values.period.value=data.head[formId].rows[0].values.year.value+"-"+data.head[formId].rows[0].values.period.value;
                  //按钮控制
                  if(data.head[formId].rows[0].values.issettled.value==1)
                  {
                    props.button.setButtonVisible(['Back','Edit','Refresh','Load','Settled'], true);
                    props.button.setButtonVisible(['Save','Cancel'], false);
                  }
                  else
                  {
                    props.button.setButtonVisible(['Back','Refresh','Load'], true);
                    props.button.setButtonVisible(['Edit','Settled','Save','Cancel'], false);
                  }
                  props.form.setAllFormValue({ [formId]: data.head[formId] });
                }
                if(data.body)
                {
                  props.editTable.setTableData(tableId,data.body[tableId]);
                }
                props.editTable.setStatus(tableId, 'browse');
                props.setUrlParam(
                    {
                        pagecode:'2002REPORT_CARD', 
                        status: 'browse',
                        pk_accountingbook:cacheTools.get('settledreportaccountingbookpk'),
                        pk_accperiodmonth:cacheTools.get('settledreportpk_accperiodmonth')
                    }
                );

                page.toggleShow();
              }
            }.bind(page)
          });
}

function dealEdit(page){
  let props = page.props;
  props.setUrlParam({
    pagecode:'2002REPORT_CARD', 
    status: 'edit',
    pk_accountingbook:cacheTools.get('settledreportaccountingbookpk'),
    pk_accperiodmonth:cacheTools.get('settledreportpk_accperiodmonth')
  })
  props.button.setButtonVisible(['Save','Cancel'], true);
  props.button.setButtonVisible(['Back','Refresh','Edit','Load','Settled'], false);
  props.editTable.setStatus(tableId, 'edit');
  props.editTable.setColEditableByKey(tableId, 'isfinished', false);
  props.editTable.setColEditableByKey(tableId, ['pk_settledrule.systypecod','pk_settledrule.code','pk_settledrule.name','pk_settledrule.handler.user_name','requiretime','finishtime','checkresult','finishstate','pk_settledrule.appname'],true);
  props.editTable.setEditableRowByIndex(tableId, [0,1,2,3,4,5,6,7,8,9], false);
  page.toggleShow();
}