import { base, ajax,cacheTools } from 'nc-lightapp-front';
let { NCPopconfirm } = base;
const formId = '2002REPORT_head';
const tableId = '2002REPORT_bodys';
const pageId = '2002REPORT_CARD';
export default function(props) {
	let self=this;
	let appcode = props.getUrlParam('c')||props.getSearchParam('c');
	props.createUIDom(
		{
			pagecode: pageId,
			appcode: appcode
		}, 
		function (data){
			if(data){
				if(data.template){
					let meta = data.template;
					meta = modifierMeta(self,props, meta)
					//modifierMeta(props, meta)
					props.meta.setMeta(meta);
				}
				if(data.button){
					let button = data.button;
					props.button.setButtons(button);
				}
			}   
		}
	)
}

function modifierMeta(self,props, meta) {
	let status = props.getUrlParam('status');
	meta[formId].status = status;
	meta[tableId].status = status;
	meta[tableId].items.forEach((item, key) => {
		if (item.attrcode == 'pk_settledrule.pk_settledrule'||item.attrcode == 'pk_settledrule.handler.cuserid'||item.attrcode == 'pk_settledreport.pk_settledreport') {
			item.visiable=false;
		}
		else if (item.attrcode == 'pk_settledrule.appname') {
			item.renderStatus='browse'
			item.render = (text, record, index) => {
				return (<a
						style={{ textDecoration: 'underline', cursor: 'pointer' }}
						onClick={() => {
						if(index<10){
							let url ='/nccloud/gl/reckoning/reckoningreportforsettledreport.do';
							let pk_accpont = {"pk_accountingbook":cacheTools.get('settledreportaccountingbookpk'),  pk_accperiodmonth :  cacheTools.get('settledreportpk_accperiodmonth')};
							let {reportDataSum,reportSumTitle,reportSumFont,reportData,accountingbook,reportTitle,reportFont}=self.state;
							ajax({
								url:url,
								data:pk_accpont,
								success: function(response){
									const { data,success } = response;
									//渲染已有账表数据遮罩
									if (success) {
										if(response.data){
											reportDataSum=data.reportSumMesg;
											reportSumTitle=data.sumDisMesg;
											reportSumFont=data.sumFont;
											reportData=data.reportMesg;
											reportTitle=data.disMesg;
											reportFont=data.font;
											self.setState({
												reportDataSum,reportSumTitle,reportSumFont,reportData,reportTitle,reportFont,
												ShowHideFlag:true,
											})
										}
									}   
								}
							});
						}else{
							let url = null;
							let appcode = null;
							let pagecode = null;
							let senddata = {pk_appregister: record.values['pk_settledrule.appname'].value};
							ajax({
									url: '/nccloud/gl/settled/openapp.do',
									data: senddata,
									success: (res) => { 
										if(res.data)
										{
											url = res.data.url;
											appcode = res.data.appcode;
											pagecode = res.data.pagecode;
	
											props.openTo(url, {
												appcode : appcode,
												pagecode : pagecode,
											});
										}
									}
								})
						}

						
						}}
					>
						{ record.values['pk_settledrule.appname'] &&  record.values['pk_settledrule.appname'].display}
					</a>
				);
			};
		}
		return item;
	});
	//let multiLang = props.MutiInit.getIntl('2052');
	return meta;
}
