import { toast,getBusinessInfo } from 'nc-lightapp-front';

export default function afterEvent(props, moduleId, key,value, changedrows, i, s, g) {
	let tableId = '2002REPORT_bodys';
	//表体编辑后事件
    if(moduleId == this.tableId){
		if(key=='isfinished')
		{
			let businessInfo = getBusinessInfo();
			let userId = businessInfo.userId
			//if(s.values['pk_settledrule.handler.cuserid'].value==null||userId==s.values['pk_settledrule.handler.cuserid'].value)
			if(s.values['pk_settledrule.handler.cuserid'].value==null||userId==s.values['pk_settledrule.handler.cuserid'].display)
			{
				if(value==true)
				{
					props.editTable.setValByKeyAndIndex(tableId,i,'finishtime',{value:businessInfo.businessDate});
					props.editTable.setValByKeyAndIndex(tableId,i,'checkresult',{value:this.state.json['2002REPORT-000000']});/* 国际化处理： 执行完成*/
				}
				else
				{
					props.editTable.setValByKeyAndIndex(tableId,i,'finishtime',{value:null});
					props.editTable.setValByKeyAndIndex(tableId,i,'checkresult',{value:null});
				}
			}
			else
			{
				 toast({ color: 'warning', content: this.state.json['2002REPORT-000001'] });/* 国际化处理： 当前用户不是指定责任人*/
				 props.editTable.setValByKeyAndIndex(tableId,i,'isfinished',{value:!value});
			}
		}
	}
	
}
