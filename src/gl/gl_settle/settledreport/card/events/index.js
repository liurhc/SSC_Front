import buttonClick from './buttonClick';
import initTemplate from './initTemplate';
import afterEvent from './afterEvent';
export { buttonClick, afterEvent, initTemplate};
