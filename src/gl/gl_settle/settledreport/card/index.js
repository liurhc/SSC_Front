//主子表卡片
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { high,createPage, ajax, base, toast,cacheTools,pageTo ,getMultiLang,createPageIcon} from 'nc-lightapp-front';
let { NCFormControl } = base;
import { buttonClick, initTemplate, afterEvent } from './events';
import ReportModal from '../../../finalTreatment/reckoning/list/reportModal/reportModal.js'
import PrintModal from '../../../public/components/printModal';
const { PrintOutput } = high;
import { printRequire, mouldOutput } from '../../../public/components/printModal/events';
import {Header, HeaderButtonArea, HeaderPaginationArea} from '../../../../fipub/public/components/layout/Header'
import './index.less'
class Card extends Component {
	constructor(props) {
		super(props);
		this.state={
			appcode:'20020RECON',
			listItem:{
				pk_accountingbook:{},
				reckedMonth:{},
			},
			status : props.getUrlParam('status'),
			ShowHideFlag:false,
			reportDataSum:[],//结账汇总数据
			reportData:[],//结账详情数据
			reportSumTitle:'',//汇总报告title
            reportSumFont:{},//汇总报告字体
            reportTitle:'',//报告title
            reportFont:{},
			isCardPaginationShow: false,  //控制分页是否显示
			queryDataObj:{},//保存查询条件
			json:{}
		};
		this.formId = '2002REPORT_head';
		this.searchId = '2002REPORT_query';
		this.moduleId = '2002';
		this.tableId = '2002REPORT_bodys';
		this.pageId = '2002REPORT_CARD';
		initTemplate.call(this, this.props);
		this.back=this.back.bind(this,props)
	}

	componentWillReceiveProps(nextProps){
	}

	componentWillMount() {
        let callback= (json) =>{
            this.setState({json:json},()=>{
            })
        }
        getMultiLang({moduleId:'2002SETTLED',domainName:'gl',currentLocale:'zh-CN',callback});
    }
	componentDidMount() {
		//查询单据详情
		this.initShow();
	}

	toggleShow = () => {
		let page = this;
		let status = page.props.getUrlParam('status');
		page.setState({status});
	}

	//页面数据初始化
	initShow = () =>{
		if (this.props.getUrlParam('status') == 'browse') {
			let pk_accountingbook =  this.props.getUrlParam('pk_accountingbook');
			let pk_accperiodmonth = this.props.getUrlParam('pk_accperiodmonth');
			cacheTools.set('settledreportaccountingbookpk', pk_accountingbook);
			cacheTools.set('settledreportpk_accperiodmonth', pk_accperiodmonth);
			let data = { 
				pk_accountingbook:pk_accountingbook,
				pk_accperiodmonth:pk_accperiodmonth
			};
			
			ajax({                      
				url: '/nccloud/gl/settled/reportcardquery.do',
				data: data,
				success: (res) => {
					if (res.data) {
						if (res.data.head) {
							let {listItem} = this.state;
							let yearmonth = res.data.head[this.formId].rows[0].values.year.value+"/"+res.data.head[this.formId].rows[0].values.period.value; 
							let display=res.data.head[this.formId].rows[0].values.accountingbookname.value
							let value=res.data.head[this.formId].rows[0].values.pk_accountingbook.value
							let accountingbook = {display: display,value:value }
							listItem.pk_accountingbook=accountingbook;
							listItem.reckedMonth={value:yearmonth}
							this.setState({
								listItem
							})
							res.data.head[this.formId].rows[0].values.period.value=res.data.head[this.formId].rows[0].values.year.value+"-"+res.data.head[this.formId].rows[0].values.period.value;
							//按钮控制
							if(res.data.head[this.formId].rows[0].values.issettled.value==1)
							{
								this.props.button.setButtonVisible(['Back','Edit','Refresh','Load','Settled'], true);
								this.props.button.setButtonVisible(['Save','Cancel'], false);
							}
							else
							{
								this.props.button.setButtonVisible(['Back','Refresh','Load'], true);
								this.props.button.setButtonVisible(['Edit','Load','Settled','Save','Cancel'], false);
							}
							this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
						}
						if (res.data.body) {
							this.props.editTable.setTableData(this.tableId, res.data.body[this.tableId]);
						}
					} else {
						this.props.form.setAllFormValue({ [this.formId]: { rows: [] } });
						this.props.editTable.setTableData(this.tableId, { rows: [] });
					}
				}
			});
		} 
	}
  //打印
  handlePrintReport=()=> {
	this.setState({
        showPrintModal: true
        })
    }
    handlePrint(data) {
        let printUrl = '/nccloud/gl/accountrep/reckoningreportprint.do'
        let { queryDataObj, appcode,sumOrDetail,listItem} = this.state
        let { ctemplate, nodekey } = data
        queryDataObj.queryvo = data
        queryDataObj.pk_accountingbook=listItem['pk_accountingbook'].value;
        queryDataObj.year=listItem['reckedMonth'].value.split('/')[0];
        queryDataObj.month=listItem['reckedMonth'].value.split('/')[1];
        queryDataObj.issum='Y';
        this.setState({
            ctemplate: ctemplate,
            nodekey: nodekey
        })
        printRequire(printUrl, appcode, nodekey, ctemplate, queryDataObj)
        this.setState({
            showPrintModal: false
        });
    }
  //模板输出
  showOutputModal=()=> {
    let outputUrl = '/nccloud/gl/accountrep/reckoningreportoutput.do'
    let { appcode, nodekey, ctemplate, queryDataObj ,listItem} = this.state;
        queryDataObj.pk_accountingbook=listItem['pk_accountingbook'].value;
        queryDataObj.year=listItem['reckedMonth'].value.split('/')[0];
        queryDataObj.month=listItem['reckedMonth'].value.split('/')[1];
    mouldOutput(outputUrl, appcode, nodekey, ctemplate, queryDataObj)
  }
  handleOutput() {}
  //关闭
  handleClose=()=>{
	this.setState({
		ShowHideFlag: !this.state.ShowHideFlag,
		
	})
	}
	back=(props)=>{
		props.linkTo('/gl/gl_settle/settledreport/list/index.html',{pagecode:'2002REPORT_LIST',jumpflag: true})
	}
	render() {
		let { editTable, form, button, modal, cardPagination } = this.props;
		let buttons = this.props.button.getButtons();
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		let { createForm } = form;
		let { createEditTable } = editTable;
		const {createCardPagination} = cardPagination;
		let { createButton } = button;
		let { createModal } = modal;
		let { isCardPaginationShow } = this.state;
		let{accountingbook,columnsVerify,reckonintData,listItem,ShowHideFlag,
            reportDataSum,reportSumTitle,reportSumFont,reportData,reportTitle,reportFont}=this.state;
        let reportSum={
            reportSumData:reportDataSum,
            reportSumTitle:reportSumTitle,
            reportSumFont:reportSumFont,           
        }
        let reportDetail={
            reportDetailData:reportData,
            reportDetailTitle:reportTitle,
            reportDetailFont:reportFont
		}
		let status=this.props.getUrlParam('status');
		return (
			<div className="nc-bill-card" id="monthdetail">
				<div className="nc-bill-top-area">
					<Header showGoback={status=='browse'} onBackClick={this.back}>
						{ isCardPaginationShow ? 
							<HeaderPaginationArea>
								{createCardPagination({})}
							</HeaderPaginationArea>
							:null
						}
						<HeaderButtonArea>
							{this.props.button.createButtonApp({
								area: 'card_head',
								buttonLimit: 3, 
								onButtonClick:buttonClick.bind(this), 
								popContainer: document.querySelector('.header-button-area')
							})}	
						</HeaderButtonArea>
					</Header>
					<div className="nc-bill-form-area">
						{createForm(this.formId, {
							onAfterEvent: afterEvent.bind(this)
						})}
					</div>
				</div>
				<div className="nc-bill-bottom-area">
					<div className="nc-bill-table-area">
						{createEditTable(this.tableId, {
							adaptionHeight:true,
							onAfterEvent: afterEvent.bind(this),
							//showCheck: true,
							lazyload: false,//禁用懒加载
							showIndex: true
						})}
					</div>
				</div>
				<ReportModal 
                    // config={this.props}
                    reportSum={reportSum}
                    reportDetail={reportDetail}
                    showOrHide = {this.state.ShowHideFlag}  
                    // onConfirm = {this.handleQueryClick.bind(this)} 
                    handlePrintReport={this.handlePrintReport.bind(this)}//打印
                    showOutputModal={this.showOutputModal.bind(this)}//模板输出
                    handleClose={this.handleClose.bind(this)}
                />
				<PrintModal
                    noRadio={true}
                    noCheckBox={true}
                    appcode={this.state.appcode}
                    visible={this.state.showPrintModal}
                    handlePrint={this.handlePrint.bind(this)}
                    handleCancel={() => {
                    this.setState({
                        showPrintModal: false
                    })
                    }}
                />
                <PrintOutput                    
                    ref='printOutput'
                    url='/nccloud/gl/verify/verbalanceoutput.do'
                    data={this.state.outputData}
                    callback={this.handleOutput.bind(this)}
                />
			</div>
		);
	}
}

Card = createPage({
	// initTemplate: initTemplate,
	//mutiLangCode: '2052'
})(Card);

ReactDOM.render(<Card />, document.querySelector('#app'));
