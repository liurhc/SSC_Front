import initTemplate from './initTemplate'
import afterEvents from './afterEvents'
import beforeEvents from './beforeEvents'

export {initTemplate, afterEvents, beforeEvents}
