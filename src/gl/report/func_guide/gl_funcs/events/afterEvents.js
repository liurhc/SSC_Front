import { getBusinessInfo } from 'nc-lightapp-front';
import {KEYS, FORMID} from '../const';
import {commonApi} from '../../../../public/common/actions'

export default function afterEvents(props, moduleId, key, value, oldValue){
    let page = this;
    switch(key){
        case KEYS.PK_ORG: dealWithPK_ORGchange(page, value, oldValue); break;
        case KEYS.PK_SETOFBOOK: dealWithPK_SETOFBOOKchange(page, value, oldValue); break;
        case KEYS.YEAR: dealWithYEARchange(page, value, oldValue); break;
        case KEYS.PERIOD: dealWithPERIODchange(props, value, oldValue); break;
        case KEYS.PK_CURRTYPE: dealWithPK_CURRTYPEchange(page, value, oldValue); break;
        case KEYS.PK_RETCURR: dealWithPK_RETCURRchange(props, value, oldValue); break;
        case KEYS.PK_VOUCHERKIND: dealWithPK_VOUCHERKINDchange(props, value, oldValue); break;
        case KEYS.PK_ACCASOA: dealWithPK_ACCASOAchange(page, value, oldValue); break;
        case KEYS.INCLUDEUNTALLY: dealWithINCLUDEUNTALLYchange(props, value, oldValue); break;
    }
}

function dealWithPK_ORGchange(page, value, oldValue){
    let props = page.props;
    let meta = props.meta.getMeta();
    let pk_org = value && value.refpk;
    let orgCode = value && value.refcode || '';
    let tmpInfo = page.tmpInfo;
    tmpInfo.pk_org = pk_org;
    tmpInfo.orgCode = orgCode;
    meta[FORMID].items.map((item)=>{
        if(item.attrcode == KEYS.PK_SETOFBOOK){
            item.queryCondition = () =>{
                return {
					GridRefActionExt:'nccloud.web.gl.ref.FilterSefOfBookRefSqlBuilder',
                    pk_org
                } 
            }
        }
    });
    props.meta.setMeta(meta);
    clearFormInfo(page, KEYS.PK_SETOFBOOK, KEYS.YEAR, KEYS.PERIOD, KEYS.PK_ACCASOA);
}

function dealWithPK_SETOFBOOKchange(page, value, oldValue){
    let props = page.props;
    let pk_setofbook = value && value.refpk;
    let setOfBookCode = value && value.refcode || '';
    let tmpInfo = page.tmpInfo;
    tmpInfo.pk_setofbook = pk_setofbook;
    tmpInfo.setOfBookCode = setOfBookCode;
    let pk_org = tmpInfo.pk_org;
    if(pk_org && pk_setofbook){
        //TODO 更新pk_accountingbook 信息
        refreshAccountbookInfo(page, pk_org, pk_setofbook);
    }
    
    clearFormInfo(page, KEYS.YEAR, KEYS.PERIOD, KEYS.PK_ACCASOA);
}

function dealWithYEARchange(page, value, oldValue){
    //TODO refresh period info
    let year = value && value.value;
    let pk_accountingbook = page.tmpInfo.pk_accountingbook;
    if(year){
    }else{
        clearFormInfo(page, KEYS.PERIOD);
    }
}

function dealWithPERIODchange(props, value, oldValue){

}

function dealWithPK_CURRTYPEchange(page, value, oldValue){
    let currTypeCode = value && value.refcode || '';
    let tmpInfo = page.tmpInfo;
    tmpInfo.currTypeCode = currTypeCode;
}

function dealWithPK_RETCURRchange(props, value, oldValue){

}

function dealWithPK_VOUCHERKINDchange(props, value, oldValue){

}

function dealWithPK_ACCASOAchange(page, value, oldValue){
    //TODO refresh ass info
    let pk_accasoa = value && value.refpk;
    let accasoaCode = value && value.refcode || '';
    let tmpInfo = page.tmpInfo;
    tmpInfo.accasoaCode = accasoaCode;
    let buziInfo = getBusinessInfo();
    let pretentAssData = {
        pk_accountingbook: tmpInfo.pk_accountingbook,
		pk_accasoa: pk_accasoa,
		prepareddate: buziInfo.businessDate,
		pk_org: tmpInfo.pk_org,
		checkboxShow: true,
        checkedAll: true,
        checkedArray:[]
    }
    page.setState({pretentAssData});
}

function dealWithINCLUDEUNTALLYchange(props, value, oldValue){

}

function refreshAccountbookInfo(page, pk_org, pk_setofbook){
    let props = page.props;
    let meta = props.meta.getMeta();
    commonApi.qryAccBookByOrgAndSetOfBook({pk_org, pk_setofbook}, (res) => {
        let pk_accountingbook = res.data.pk_accountingbook;
        page.tmpInfo.pk_accountingbook = pk_accountingbook;
        meta[FORMID].items.map((item) => {
            if(item.attrcode == KEYS.PK_ACCASOA){
                item.queryCondition = () => {
                    return {pk_accountingbook};
                }
            }
        });
        props.meta.setMeta(meta);
    });
 
}


/**
 * 清空指定item中的内容
 * @param {this} page 
 * @param  {...any} keys item.attrcode
 */
function clearFormInfo(page, ...keys){
    let props = page.props;
    let meta = props.meta.getMeta();
    let data = {};
    keys.map((item) => {
        data[item] = {display:'', value:''}
    });
    props.form.setFormItemsValue(FORMID, data);
    meta[FORMID].items.map((item) => {
        if(item.itemtype == 'select' && keys.indexOf(item.attrcode) > -1){
            item.options = [{display:'', value:''}]
        }    
    });
    props.meta.setMeta(meta);
    keys.map((item) => {
        clearTmpInfo(page, item);
    });
}

function clearAssInfo(page){
    let pretentAssData = {
        pk_accountingbook: '',
		pk_accasoa: '',
		prepareddate: '',
		pk_org: '',
		checkboxShow: true,
		checkedAll: true
    }
    page.setState({pretentAssData});
}

function clearTmpInfo(page, key){
    let tmpInfo = page.tmpInfo;
    switch(key){
        case KEYS.PK_ORG:           tmpInfo.orgCode = ''; break;
        case KEYS.PK_SETOFBOOK:     tmpInfo.setOfBookCode = ''; break;
        case KEYS.PK_CURRTYPE:      tmpInfo.currTypeCode = ''; break;
        case KEYS.PK_ACCASOA:       
            tmpInfo.accasoaCode = '';
            clearAssInfo(page); 
            break;
    }
}
