import {KEYS, FORMID} from '../const'
import {commonApi} from '../../../../public/common/actions'
export default function(props, moduleId, key, value, data){
    switch(key){
        case KEYS.YEAR:
            dealYearClick(this);
            break;
        case KEYS.PERIOD:
            dealPeriodClick(this);
            break;
    }
    return true;
}

function dealYearClick(page){
    let props = page.props;
    let pk_accountingbook = page.tmpInfo.pk_accountingbook;
    if(pk_accountingbook){
        let meta = props.meta.getMeta();
        commonApi.qryYearByAccountingbook({pk_accountingbook}, (res) => {
            let options = [{display:'', value:''}];
            if(res.data && res.data.length > 0){
                res.data.map((item) => {
                    let option = {display:item, value:item};
                    options.push(option);
                });
            }
            meta[FORMID].items.map((item) => {
                if(item.attrcode == KEYS.YEAR){
                    item.options = options;
                }
            });
            props.meta.setMeta(meta);
        });       
    }
}

function dealPeriodClick(page){
    let props = page.props;
    let pk_accountingbook = page.tmpInfo.pk_accountingbook;
    let yearItem = props.form.getFormItemsValue(FORMID, KEYS.YEAR);
    let year = yearItem&&yearItem.value;
    if(pk_accountingbook && year){
        refreshPeriodInfo(props, pk_accountingbook, year);
    }
}

function refreshPeriodInfo(props, pk_accountingbook, year){
    let meta = props.meta.getMeta();
    commonApi.queryPeriodByBookAndYear({pk_accountingbook, year}, (res) => {
        let options = [{display:'', value:''}];
        if(res.data && res.data.length>0){
            res.data.map((item) => {
                let option = {display:item, value:item};
                options.push(option);
            });
        }
        meta[FORMID].items.map((item) => {
            if(item.attrcode == KEYS.PERIOD){
                item.options = options;
            }
        });
        props.meta.setMeta(meta);
    });
}
