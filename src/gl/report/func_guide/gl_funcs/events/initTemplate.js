import {FORMID, KEYS} from '../const'
export default function(props){
    let page = this;
    props.createUIDom(
		{
			appcode: '20028101',
			pagecode: '20028101_gl'
		}, 
		function (data){
			if(data){
				let meta = data.template;
				initMeta(page, meta);
				props.meta.setMeta(meta);
			}   
		}
	)
}

function initMeta(page, meta){
	let tmpInfo = page.tmpInfo;
	meta[FORMID].items.map((item) => {
		if(item.attrcode == KEYS.PK_SETOFBOOK){
			item.queryCondition = () => {
				return {
					GridRefActionExt:'nccloud.web.gl.ref.FilterSefOfBookRefSqlBuilder',
					pk_org:tmpInfo.pk_org
				};
			}
		}else if(item.attrcode == KEYS.PK_CURRTYPE){
			item.refcode = 'gl/refer/voucher/TransCurrGridRef/index';
			item.initialvalue={display:page.state.json['20028101-000000'], value:page.state.json['20028101-000000']};/* 国际化处理： 本币,本币*/
			item.queryCondition = {
				"isDataPowerEnable": 'Y',
				"DataPowerOperationCode" : 'fi',
				"showAllCurr" : 'N'
			};
		}else if(item.attrcode == KEYS.PK_ACCASOA){
			item.queryCondition = () => {
				return {pk_accountingbook:tmpInfo.pk_accountingbook}
			}
		}
	});	
}
