/**
 * 报表总账函数向导
 */
import {Component} from 'react';
import {createPage, base, getMultiLang} from 'nc-lightapp-front';
import {initTemplate, afterEvents, beforeEvents} from './events';
import {FORMID, KEYS} from './const'
import AssidModal from '../../../public/components/getAssDatas2'
import {appendItem, extractDispFromForm, extractValueFromForm, extractAssInfo} from '../utils'
import GuideCacheUtil from '../GuideCacheUtil';
const {NCButton, NCModal}=base;

class GLFuncGuide extends Component{
    constructor(props){
        super(props);
        this.state = {
            pretentAssData:{checkboxShow:true, checkedAll:true},
            title:''
        };
        this.cacheUtil = new GuideCacheUtil(props.ViewModel);
        this.assCompRef;
    }

    initEnv = () =>{
        this.dataSource = this.cacheUtil.getGLEnv() || {};
        this.tmpInfo = this.dataSource.tmpInfo || this.getInitTmpInfo();
    }

    updateEnv = () =>{
        let page = this;
        let dataSource = page.dataSource;
        if(!dataSource) dataSource = {};
        dataSource.formData = page.props.form.getAllFormValue(FORMID);
        dataSource.pretentAssData = page.state.pretentAssData;
        dataSource.tmpInfo = page.tmpInfo;
        page.cacheUtil.setGLEnv(dataSource);
    }

     componentWillMount(){
         		/* 加载多语资源 */
		let callback = (json, status, intl) =>{
			if(status){
				this.setState({json, intl}, () => {
                    this.initEnv();
					initTemplate.call(this, this.props);
				});
			}else{
			}
		}
		getMultiLang({moduleId:'20028101', domainName:'gl', callback});
     }

    componentDidMount(){
        this.props.form.setFormStatus(FORMID, 'add');
    }

    exportData = () =>{
        let page = this;
        let tmpInfo = page.tmpInfo;
        let formData = page.props.form.getAllFormValue(FORMID);
        let result = '';
        
        result = appendItem(result, tmpInfo.accasoaCode);
        
        let year = extractValueFromForm(formData, KEYS.YEAR);
        result = appendItem(result, year);

        let period = extractValueFromForm(formData, KEYS.PERIOD);
        result = appendItem(result, period);

        let assCompRef = page.assCompRef;
        let assData = assCompRef.exportAssData();
        let ass = extractAssInfo(assData);
        if(ass){
            result = appendItem(result, ass);
        }else{
            result += ","
        }

        let direction = extractValueFromForm(formData, KEYS.DIRECTION);
        if(!direction || direction == '2'){
            result += ","
        }else{
            let directionDisp = extractDispFromForm(formData, KEYS.DIRECTION); 
            result = appendItem(result, directionDisp);
        }

        let currTypeCode = tmpInfo.currTypeCode;
        result = appendItem(result, currTypeCode);

        let includeUntally = extractValueFromForm(formData, KEYS.INCLUDEUNTALLY);
        if(includeUntally){
            includeUntally = 'Y';
        }else{
            includeUntally = 'N';
        }
        result = appendItem(result, includeUntally);

        result = appendItem(result, tmpInfo.orgCode);

        let retCurr = extractDispFromForm(formData, KEYS.PK_RETCURR);
        result = appendItem(result, retCurr);
        
        let setOfBookCode = tmpInfo.setOfBookCode;
        let isDiff = extractValueFromForm(formData, KEYS.PK_VOUCHERKIND);
        if(isDiff == '1'){
            setOfBookCode += '#1';
        }else if(isDiff == '2'){
            setOfBookCode += '#0';
        }
        result = appendItem(result, setOfBookCode);

        while(result.endsWith(',')){
            result = result.substr(0, result.length - 1);
        }
        return result;
    }

    onConfirm = () => {
        let page = this;
        let result = page.exportData();
        result = page.props.funcType + "(" + result + ")";
        page.props.beSureBtnClickCallBack(result);
        page.hidePage();
    }

    onCancel = () => {
        let page = this;
        page.props.beCancelBtnClickCallBack();
        page.hidePage();
    }

    onClose = () => {
        let page = this;
        page.hidePage();
    }

    hidePage = () => {
        let page = this;
        page.setState({
            show:false,
        });
        page.resetPageData();
        page.props.modal.close('modal');
        page.updateEnv();
    }

    showPage = (nextProps) => {
        let page = this;
        let dataSource = page.dataSource;
        if(!dataSource) dataSource = {};
        let formData = dataSource.formData;
        if(formData){
            page.props.form.setAllFormValue({[FORMID]:formData});
        }else{
            page.props.form.EmptyAllFormValue(FORMID);
        }
        let pretentAssData = dataSource.pretentAssData || {checkboxShow:true, checkedAll:true};
        this.tmpInfo = dataSource.tmpInfo || this.getInitTmpInfo();
        let title = nextProps.funcType;
        page.setState({pretentAssData, show:true, title});
        page.props.modal.show('modal');
    }

    resetPageData = () => {
        let page = this;
        // page.props.form.EmptyAllFormValue(FORMID);
        // page.tmpInfo = page.getInitTmpInfo();
    }

    getInitTmpInfo = () =>{
        let page = this;
        let info = 
            {
                accountingbook:{},
                accasoaCode:'',
                orgCode:'',
                setOfBookCode:'',
                currTypeCode:page.state.json['20028101-000000']/* 国际化处理： 本币*/
            };
        return info;
    }

    componentWillReceiveProps(newProps){
        let page = this;
        let {show} = page.state;
        if(show == newProps.isShow)
            return;
        if(newProps.isShow){
            page.showPage(newProps);
            page.props.form.setFormStatus(FORMID, 'add');
        }else{
            page.hidePage();
        }
    }



    generateModalContent = () => {
        let page = this;
        let {form} = page.props;
        let {pretentAssData, show} = page.state;
        return (                        
        <div>
            {form.createForm(FORMID, {
                onAfterEvent:afterEvents.bind(this),
                onBeforeEvent:beforeEvents.bind(this)
            })}
            <AssidModal
                componentRef = {(childComp) => {page.assCompRef = childComp}}
                pretentAssData={pretentAssData}
                showDisableData={'no'}
                checkboxShow = {true}
                checkedAll = {true}
                handleClose={() => {
                    assidModalState.show = false;
                    this.setState({
                        assidModalState
                    });
                }}
                />
        </div>);
    }

    render(){
        let page = this;
        let {title} = page.state;
        let {modal} = page.props;
        return (
            <div>
                {modal.createModal('modal', {
                    title:title,
                    content:page.generateModalContent(),
                    beSureBtnClick:page.onConfirm,
                    cancelBtnClick:page.onCancel,
                    closeModalEve:page.onClose,
                    userControl:true,
                    onHide:page.onClose,
                    zIndex:290,
                    className:'combine'
                })}
            </div>
        );
    }

}

GLFuncGuide= createPage({})(GLFuncGuide);
export default {GuidComponents:GLFuncGuide};
