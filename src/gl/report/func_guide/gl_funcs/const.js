const FORMID = 'gl_funcs_form';
const APPCODE = '';
const PAGECODE = '';
const FUNC_NAMES = {

}

const KEYS = {
    PK_ORG:'pk_org_1',
    PK_SETOFBOOK:'pk_setofbook',
    YEAR:'year',
    PERIOD:'period',
    PK_CURRTYPE:'pk_currtype',
    PK_RETCURR:'pk_retcurrtype',
    PK_VOUCHERKIND:'pk_voucherkind',
    PK_ACCASOA:'pk_accasoa',
    INCLUDEUNTALLY:'includeUntally',
    DIRECTION:'direction'
}


const ACTIONS = {

};

export {FORMID, APPCODE, PAGECODE, FUNC_NAMES, ACTIONS, KEYS}
