const FORMID = 'cf_funcs_form';

const KEYS = {
    PK_ORG : "pk_org_1",
    PK_SEFOFBOOK : "pk_sefofbook",
    START_DATE : "start_date",
    END_DATE : "end_date",
    CURRTYPE : "currtype",
    RET_CURR : "ret_curr",
    PK_ACCASOA : "pk_accasoa",
    PK_CASHFLOW : "pk_cashflow",
    PK_INNERORG : "pk_innerorg",
    PK_UNIT : "pk_unit",
    INCLUDEUNTALLY : "includeUntally"
};

export{FORMID, KEYS};
