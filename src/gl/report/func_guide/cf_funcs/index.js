/**
 * 报表现金流量函数向导
 */
import {Component} from 'react';
import {initTemplate, afterEvents} from './events';
import {FORMID, KEYS} from './const'
import {createPage, toast, deepClone, getMultiLang} from 'nc-lightapp-front';
import AssidModal from '../../../public/components/getAssDatas2'
import {base} from 'nc-lightapp-front';
import {appendItem, extractDispFromForm, extractValueFromForm, extractAssInfo, removeSeparator} from '../utils'
import GuideCacheUtil from '../GuideCacheUtil'
const {NCButton}=base;

class CFFuncGuide extends Component{
     constructor(props){
        super(props);

        this.state = {
            pretentAssData:{checkboxShow:true, checkedAll:true},
            title:''
        }
        this.cacheUtil = new GuideCacheUtil(props.ViewModel);
        this.assCompRef;
     }

     initEnv = () => {
         this.dataSource = this.cacheUtil.getCFEnv();
         this.dataCache = this.dataSource.dataCache || this.getInitDataCache();
     }

     updateEnv = () => {
        let page = this;
        let dataSource = page.dataSource;
        if(!dataSource) dataSource = {};
        dataSource.formData = page.props.form.getAllFormValue(FORMID);
        dataSource.pretentAssData = page.state.pretentAssData;
        dataSource.dataCache = page.dataCache;
        page.cacheUtil.setCFEnv(dataSource);
     }
     componentWillMount(){
         		/* 加载多语资源 */
		let callback = (json, status, intl) =>{
			if(status){
				this.setState({json, intl}, () => {
                    this.initEnv();
					initTemplate.call(this, this.props);
				});
			}else{
			}
		}
		getMultiLang({moduleId:'20028101', domainName:'gl', callback});
     }
     componentDidMount(){
		this.props.form.setFormStatus(FORMID, 'add');
     }

     exportData = () => {
        let page = this;
        let {org, setOfBook, accasoaCode, currTypeCode, innerOrgCode, unitCode, cashFlowCode} = page.dataCache;
        let formData = page.props.form.getAllFormValue(FORMID);
        let result = '';

        result = appendItem(result, org && org.code || '');
        
        let startDate = extractValueFromForm(formData, KEYS.START_DATE);
        if(startDate){
            startDate = startDate.split(" ")[0];
        }
        result = appendItem(result, startDate);
        
        let endDate = extractValueFromForm(formData, KEYS.END_DATE);
        if(endDate){
            endDate = endDate.split(" ")[0];
        }
        result = appendItem(result, endDate);
        
        result = appendItem(result, cashFlowCode);

        let includeTally = extractValueFromForm(formData, KEYS.INCLUDEUNTALLY);
        if(includeTally == true){
            includeTally = '1';
        }else{
            includeTally = '0';
        }
        result = appendItem(result, includeTally, true);
        
        result = appendItem(result, currTypeCode || '');
        result = appendItem(result, extractDispFromForm(formData, KEYS.RET_CURR));
        result = appendItem(result, setOfBook && setOfBook.code || '');
        result = appendItem(result, accasoaCode || '');

        let assCompRef = page.assCompRef;
        let assData = assCompRef.exportAssData();
        let ass = extractAssInfo(assData);
        result = appendItem(result, ass);

        //TODO 内部单位
        result = appendItem(result, innerOrgCode || '');
        //TODO 二级核算
        result = appendItem(result, unitCode || '');
        //TODO 去掉结尾逗号
        result = removeSeparator(result);
        //TODO 校验币种信息
        if(page.state.json['20028101-000000'] == extractValueFromForm(formData, KEYS.CURRTYPE)/* 国际化处理： 本币*/
            && page.state.json['20028101-000001'] == extractValueFromForm(formData, KEYS.RET_CURR)){/* 国际化处理： 原币*/
            toast({color:'warning', content:page.state.json['20028101-000002']});/* 国际化处理： 发生币种为本币，则返回币种必须为本币！*/
            return '';
        }
        return result;
     }

     generateModalContent = () => {
         let page = this;
         let {form} = page.props;
         let {pretentAssData} = page.state; 
         return (
             <div>
                {form.createForm(FORMID, {onAfterEvent:afterEvents.bind(page)})};
                <AssidModal
                    componentRef = {(childComp) => {page.assCompRef = childComp}}
                    pretentAssData={pretentAssData}
                    showDisableData={'no'}
                    checkboxShow = {true}
                    checkedAll = {true}
                    handleClose={() => {
                        assidModalState.show = false;
                        this.setState({
                            assidModalState
                        });
                    }}
                />
             </div>
         );
     }

     onConfirm = () => {
        let page = this;
        let result = page.exportData();
        result = page.props.funcType + "(" + result + ")";
        page.props.beSureBtnClickCallBack(result);
        page.hidePage();
    }

    onCancel = () => {
        let page = this;
        page.props.beCancelBtnClickCallBack();
        page.hidePage();
    }

    onClose = () => {
        let page = this;
        page.hidePage();
    }

    hidePage = () => {
        let page = this;
        page.setState({
            show:false
        });
        page.props.modal.close('modal');
        this.updateEnv();
    }

    getInitDataCache = () => {
        let page = this;
        let cache = {
            org:{},
            setOfBook:{},
            accountingbook:{},
            accasoaCode:'',
            currTypeCode:page.state.json['20028101-000000'],/* 国际化处理： 本币*/
            innerOrgCode:'',
            unitCode:'',
            cashFlowCode:''
        }

        return cache;
    }

    showPage = (nextProps) => {
        let page = this;
        let dataSource = page.dataSource;
        if(!dataSource) dataSource = {};
        let formData = dataSource.formData;
        if(formData){
            page.props.form.setAllFormValue({[FORMID]:formData});
        }else{
            page.props.form.EmptyAllFormValue(FORMID);
        }
        let pretentAssData = dataSource.pretentAssData || {checkboxShow:true, checkedAll:true};
        this.dataCache = dataSource.dataCache || this.getInitDataCache();
        let title = nextProps.funcType;
        page.setState({pretentAssData, show:true, title});
        page.props.modal.show('modal');
    }

    componentWillReceiveProps(newProps){
        let page = this;
        let {show} = page.state;
        if(show == newProps.isShow)
            return;
        if(newProps.isShow){
            page.showPage(newProps);
            page.props.form.setFormStatus(FORMID, 'add');
        }else{
            page.hidePage();
        }
    }

     render(){
        let page = this;
        let {title} = page.state;
        let {modal} = page.props;
        return (
            <div>
                {modal.createModal('modal', {
                    title:title,
                    content:page.generateModalContent(),
                    beSureBtnClick:page.onConfirm,
                    cancelBtnClick:page.onCancel,
                    closeModalEve:page.onClose,
                    userControl:true,
                    onHide:page.onClose,
                    zIndex:290,
                    size:'xxlg',
                    className:'combine'
                })}
            </div>
        );
     }
}

CFFuncGuide = createPage({})(CFFuncGuide);
export default {GuidComponents:CFFuncGuide};
