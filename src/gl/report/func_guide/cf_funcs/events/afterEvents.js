import { getBusinessInfo } from 'nc-lightapp-front';
import {KEYS, FORMID} from '../const'
import {commonApi} from '../../../../public/common/actions'

export default function(props, moduleId, key, newValue, oldValue){
    let page = this;
    switch(key){
        case KEYS.PK_ORG:        dealPK_ORGChange(page, newValue); break;
        case KEYS.PK_SEFOFBOOK:  dealPK_SEFOFBOOKChange(page, newValue); break;
        case KEYS.START_DATE:    dealSTART_DATEChange(page, newValue); break;
        case KEYS.END_DATE:      dealEND_DATEChange(page, newValue); break;
        case KEYS.CURRTYPE:      dealCURRTYPEChange(page, newValue); break;
        case KEYS.RET_CURR:      dealRET_CURRChange(page, newValue); break;
        case KEYS.PK_ACCASOA:    dealPK_ACCASOAChange(page, newValue); break;
        case KEYS.PK_CASHFLOW:   dealPK_CASHFLOWChange(page, newValue); break;
        case KEYS.PK_INNERORG:         dealPK_INNERORGChange(page, newValue); break;
        case KEYS.PK_UNIT:       dealPK_UNITChange(page, newValue); break;
        case KEYS.INCLUDEUNTALLY:        dealINCLUDEUNTALLYChange(page, newValue); break;
    }
}

function dealPK_ORGChange(page, value){
    let props = page.props;
    let meta = props.meta.getMeta();
    let pk_org = value && value.refpk || '';
    let orgCode = value && value.refcode || '';
    //TODO 更新 dataCache 信息
    let {dataCache} = page;
    dataCache.org = {pk:pk_org, code:orgCode}
    //TODO 更新 meta 中SetOfBook参照过滤信息
    meta[FORMID].items.map((item) => {
        if(item.attrcode == KEYS.PK_SEFOFBOOK){
            item.queryCondition = () => {
                return {
                    GridRefActionExt:'nccloud.web.gl.ref.FilterSefOfBookRefSqlBuilder',
                    pk_org
                };
            }
        }
    });
    props.meta.setMeta(meta);
    //TODO 清空 SetOfBook信息，清空依赖pk_accountingbook 的字段
    clearFormInfo(page, KEYS.PK_SEFOFBOOK, KEYS.PK_ACCASOA, KEYS.PK_UNIT);
}

function dealPK_SEFOFBOOKChange(page, value){
    let pk_setofbook = value && value.refpk || '';
    let code = value && value.refcode || '';
    let dataCache = page.dataCache;
    dataCache.setOfBook = {pk:pk_setofbook, code:code};
    let pk_org = dataCache.org.pk;
    if(pk_org && pk_setofbook){
        //TODO 更新pk_accountingbook 信息
        refreshAccountbookInfo(page, pk_org, pk_setofbook);
    }
    clearFormInfo(page, KEYS.PK_ACCASOA, KEYS.PK_UNIT);
}

function dealSTART_DATEChange(page){

}

function dealEND_DATEChange(page){}

function dealCURRTYPEChange(page, value){
    updateDataCache(page, KEYS.CURRTYPE, value);
}

function dealRET_CURRChange(page, value){}

function dealPK_ACCASOAChange(page, value){
    //TODO refresh ass info
    let pk_accasoa = value && value.refpk;
    let accasoaCode = value && value.refcode || '';
    let dataCache = page.dataCache;
    dataCache.accasoaCode = accasoaCode;
    let buziInfo = getBusinessInfo();
    let pretentAssData = {
        pk_accountingbook: dataCache.accountingbook.pk,
        pk_accasoa: pk_accasoa,
        prepareddate: buziInfo.businessDate,
        pk_org: dataCache.org.pk,
        checkboxShow: true,
        checkedAll: true,
        checkedArray:[]
    }
    page.setState({pretentAssData});
}

function dealPK_CASHFLOWChange(page, value){
    updateDataCache(page, KEYS.PK_CASHFLOW, value);
}
function dealPK_INNERORGChange(page, value){
    updateDataCache(page, KEYS.PK_INNERORG, value);
}
function dealPK_UNITChange(page, value){
    updateDataCache(page, KEYS.PK_UNIT, value);
}
function dealINCLUDEUNTALLYChange(page){}
 

/**
 * 清空指定item中的内容
 * @param {this} page 
 * @param  {...any} keys item.attrcode
 */
function clearFormInfo(page, ...keys){
    let props = page.props;
    let data = {};
    keys.map((item) => {
        data[item] = {display:'', value:''}
    });
    props.form.setFormItemsValue(FORMID, data);
    keys.map((item) => {
        clearDataCache(page, item);
    });
}

/**
 * 清空缓存中的数据
 * @param {*} page 
 * @param {*} item 
 */
function clearDataCache(page, item){
    let dataCache = page.dataCache;
    switch(item){
        case KEYS.PK_ORG: dataCache.org = {}; break;
        case KEYS.PK_SEFOFBOOK: dataCache.setOfBook = {}; break;
        case KEYS.PK_ACCASOA: 
            dataCache.accasoaCode = ''; 
            clearAssInfo(page);
            break;
        case KEYS.CURRTYPE: data.currTypeCode = ''; break;
    }
}

function clearAssInfo(page){
    let pretentAssData = {
        pk_accountingbook: '',
		pk_accasoa: '',
		prepareddate: '',
		pk_org: '',
		checkboxShow: true,
        checkedAll: true,
        checkedArray:[]
    }
    page.setState({pretentAssData});
}

function refreshAccountbookInfo(page, pk_org, pk_setofbook){
    let props = page.props;
    let meta = props.meta.getMeta();
    commonApi.qryAccBookByOrgAndSetOfBook({pk_org, pk_setofbook}, (res) => {
        let pk_accountingbook = res.data.pk_accountingbook;
        page.dataCache.accountingbook.pk = pk_accountingbook;
        meta[FORMID].items.map((item) => {
            if(item.attrcode == KEYS.PK_ACCASOA){
                item.queryCondition = () => {
                    return {pk_accountingbook};
                }
            }
            if(item.attrcode == KEYS.PK_UNIT){
                if(res.data.isStartBUSecond == true){
                    item.visible = true;
                    item.queryCondition = () => {
                        return {pk_accountingbook};
                    }
                }else{
                    item.visible = false;
                }
            }
        });
        props.meta.setMeta(meta);
    });
}

function updateDataCache(page, key, value){
    let dataCache = page.dataCache;
    let refCode = value && value.refcode || '';
    let refpk = value && value.refpk || '';
    switch(key){
        case KEYS.PK_UNIT: 
            dataCache.unitCode = refCode; 
            break;
        case KEYS.PK_CASHFLOW:
            dataCache.cashFlowCode = refCode;
            break;
        case KEYS.PK_INNERORG:
            dataCache.innerOrgCode = refCode;
            break;
        case KEYS.PK_ACCASOA:
            dataCache.accasoaCode = refCode;
            break;
        case KEYS.CURRTYPE:
            dataCache.currTypeCode = refCode;
            break;
        case KEYS.PK_ORG:
            dataCache.org = {pk:refpk, code:refCode};
            break;
        case KEYS.PK_SEFOFBOOK:
            dataCache.setOfBook = {pk:refpk, code:refCode};
            break;
    }
}
