import afterEvents from './afterEvents';
import initTemplate from './initTemplate';

export {afterEvents, initTemplate};
