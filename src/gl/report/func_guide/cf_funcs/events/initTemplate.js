/**
 * TODO 默认信息设置
 * TODO 参照过滤条件
 * @param {*} props 
 */
import {getBusinessInfo} from 'nc-lightapp-front'
import {FORMID, KEYS} from '../const'

export default function(props){
    let page = this;
    props.createUIDom(
        {
            appcode:'20028101',
            pagecode:'20028101_cf'
        },
        function (data){
            if(data){
                let meta = data.template;
                initMeta(page, meta);
                props.meta.setMeta(meta);
            }
        }
    );
}

function initMeta(page, meta){
    // 设置默认日期信息
    let buziDate = getBusinessInfo().businessDate;
    let dataCache = page.dataCache;
    let pk_accountingbook = dataCache&&dataCache.accountingbook&&dataCache.accountingbook.pk;
    let pk_org = dataCache&&dataCache.org&&dataCache.org.pk;
    meta[FORMID].items.map((item) => {
        if(item.attrcode == KEYS.START_DATE || item.attrcode == KEYS.END_DATE){
            item.initialvalue={display:buziDate, value:buziDate};
        }else if(item.attrcode == KEYS.CURRTYPE){
            item.refcode = 'gl/refer/voucher/TransCurrGridRef/index';
            item.initialvalue={display:page.state.json['20028101-000000'], value:page.state.json['20028101-000000']};/* 国际化处理： 本币,本币*/
			item.queryCondition = {
				"isDataPowerEnable": 'Y',
				"DataPowerOperationCode" : 'fi',
				"showAllCurr" : 'N'
			};
		}else if(item.attrcode == KEYS.PK_SEFOFBOOK){
            item.queryCondition = () => {
                return {
                    GridRefActionExt:'nccloud.web.gl.ref.FilterSefOfBookRefSqlBuilder',
                    pk_org
                }
            }
        }else if(item.attrcode == KEYS.PK_ACCASOA){
            item.queryCondition = () => {
                return {pk_accountingbook}
            }
        }else if(item.attrcode == KEYS.PK_UNIT){
            item.queryCondition = ()=>{
                return {pk_accountingbook};
            }
        }
    });
}
