import {SEPARATOR} from './const'
let appendItem = (result, item, isNum = false) =>{
    if(isNum)
        return result + getNumItem(item);
    return result + getStrItem(item);
};


let getStrItem = (item) => {
    return `'${item}'${SEPARATOR}`;
};

let getNumItem = (item) => {
    return `${item}${SEPARATOR}`;
}

let extractValueFromForm = (formData, key) =>{
    let value = formData.rows[0].values[key];
    return value && value.value || '';
};

let extractDispFromForm = (formData, key) => {
    let value = formData.rows[0].values[key];
    return value && value.display || '';
};

let extractAssInfo = (assData) => {
    let assInfo = '';
    if(assData && assData.length > 0){
        assData.map((item) => {
            assInfo += '[' + item.checktypename;
            if(item.checkvaluecode){
                assInfo += '=' + item.checkvaluecode;
            }
            assInfo += ']';
        });
    }
    return assInfo;
};

let removeSeparator = (result) => {
    if(!result){
        return '';
    }
    while(result.endsWith(SEPARATOR)){
        result = result.substr(0, result.length - SEPARATOR.length);
    }
    return result;
}

export {appendItem, getStrItem, extractValueFromForm, extractDispFromForm, extractAssInfo, removeSeparator};
