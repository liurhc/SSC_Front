import CacheUtils from "../../public/common/CacheUtil";
const dataSource = 'gl.report.fun_guide.';
const GLENV = 'gl_env';
const CFENV = 'cf_env';

class GuideCacheUtil extends CacheUtils{
    constructor(ViewModel){
        super(ViewModel, dataSource);
    }

    setGLEnv(data){
        let env = this.getGLEnv()|| {};
        Object.assign(env, data);
        this.setData(GLENV, env);
    }

    getGLEnv(data){
        return this.getData(GLENV) || {};
    }

    setCFEnv(data){
        let env = this.getCFEnv();
        Object.assign(env, data);
        this.setData(CFENV, env);
    }

    getCFEnv(){
        return this.getData(CFENV) || {};
    }
}

export default GuideCacheUtil;
