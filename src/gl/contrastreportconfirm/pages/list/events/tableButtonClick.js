import { ajax, toast, base, cardCache, getMultiLang, cacheTools } from 'nc-lightapp-front';
let { NCMessage } = base;
let { setDefData, getDefData } = cardCache;
import { searchId, tableId } from '../config.js';
import clickSearchBtn from './searchBtnClick';
const tableButtonClick = (props, key, text, record, index, tableId,page) => {
    let data;
    let searchVal = cacheTools.get(searchId);
    switch (key) {
        // 表格操作按钮
        case 'confirm' :
        if(record){
            ajax({
                url: '/nccloud/gl/contrast/contrastreportmakeconfirm.do',
                data: {pk_contrastreportcreate: record.pk_contrastreportcreate.value},
                success: (res) => {
                let { success, data } = res;
                    if (success) {
                        if(data==true)
                        {
                            toast({color: 'success', content: page.state.json['20022020-000001']});/* 国际化处理： 确认成功*/
                            clickSearchBtn(props,searchVal,'update');
                        }
                        else
                        {
                            toast({color: 'warning', content: page.state.json['20022020-000002']});/* 国际化处理： 确认失败*/
                            clickSearchBtn(props,searchVal,'update');
                        }
                    }
                }
            });
        }
        break;
    case 'disconfirm' :
        if(record){
            ajax({
                url: '/nccloud/gl/contrast/contrastreportmakedisconfirm.do',
                data: {pk_contrastreportcreate: record.pk_contrastreportcreate.value},
                success: (res) => {
                let { success, data } = res;
                    if (success) {
                        if(data==true)
                        {
                            toast({color: 'success', content: page.state.json['20022020-000003']});/* 国际化处理： 取消确认成功*/
                            clickSearchBtn(props,searchVal,'update');
                        }
                        else
                        {
                            toast({color: 'warning', content: page.state.json['20022020-000004']});/* 国际化处理： 取消确认异常*/
                            clickSearchBtn(props,searchVal,'update');
                        }
                    }
                }
            });
        }
        break;
        default:
        break;
    }
};
export default tableButtonClick;
