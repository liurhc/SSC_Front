import { base, ajax,cacheTools } from 'nc-lightapp-front';
let { NCPopconfirm } = base;

const formId = 'form';
const tableId = 'table';
const pageId = '20022020_web_card';
export default function(props) {
	let appcode = props.getUrlParam('c')||props.getSearchParam('c');
	props.createUIDom(
		{
			pagecode: pageId,
			appcode: appcode
		}, 
		function (data){
			if(data){
				if(data.template){
					let meta = data.template;
					modifierMeta(props, meta)
					props.meta.setMeta(meta);
				}
				if(data.button){
					let button = data.button;
					props.button.setButtons(button);
				}

				//查询单据详情
				if (props.getUrlParam('status') == 'browse') {
					let data = { pk_contrastreportcreate: props.getUrlParam('id'), pagecode: pageId };
					ajax({
						url: '/nccloud/gl/contrast/contrastreportmakeconfirmcard.do',
						data: data,
						success: (res) => {
							if (res.data) {
								if (res.data.head) {
									props.form.setAllFormValue({ [formId]: res.data.head[formId] });
								}
								if (res.data.body) {
									props.cardTable.setTableData(tableId, res.data.body[tableId]);
									//字段控制
									setQueryContent(props,cacheTools.get('reportconfirmcontent'));
								}
							} else {
								props.form.setAllFormValue({ [formId]: { rows: [] } });
								props.cardTable.setTableData(tableId, { rows: [] });
							}
						}
					});
				} 
			}   
		}
	)
}

function modifierMeta(props, meta) {
	let status = props.getUrlParam('status');
	meta[formId].status = status;
	meta[tableId].status = status;
	
	meta[tableId].items.forEach((item, key) => {
		return item;
		});
	return meta;
}
//设置隐藏列
function setQueryContent(props,content){
	let tableid = 'table';
	if(content[0]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_un_quantity','selfcontrastedquantity','selfsumquantity','opp_un_quantity','oppcontrastedquantity','opp_sum_quantity','sumbalance_quantity']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_un_quantity','selfcontrastedquantity','selfsumquantity','opp_un_quantity','oppcontrastedquantity','opp_sum_quantity','sumbalance_quantity']);
	}
	if(content[1]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_un_cur','selfcontrastedcur','selfsumcur','opp_un_cur','oppcontrastedcur','opp_sum_cur','sumbalance_cur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_un_cur','selfcontrastedcur','selfsumcur','opp_un_cur','oppcontrastedcur','opp_sum_cur','sumbalance_cur']);
	}
	if(content[2]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_un_orgcur','selfcontrastedorgcur','selfsumorgcur','opp_un_orgcur','oppcontrastedorgcur','opp_sum_orgcur','sumbalance_orgcur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_un_orgcur','selfcontrastedorgcur','selfsumorgcur','opp_un_orgcur','oppcontrastedorgcur','opp_sum_orgcur','sumbalance_orgcur']);
	}
	if(content[3]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_un_groupcur','self_contrastedgroupcur','selfsumgroupcur','opp_un_groupcur','oppcontrastedgroupcur','opp_sum_groupcur','sumbalance_groupcur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_un_groupcur','self_contrastedgroupcur','selfsumgroupcur','opp_un_groupcur','oppcontrastedgroupcur','opp_sum_groupcur','sumbalance_groupcur']);
	}
	if(content[4]==='N')
	{
		props.cardTable.hideColByKey(tableid,['self_un_globalcur','selfcontrastedglobalcur','selfsumglobalcur','opp_un_globalcur','oppcontrastedglobalcur','opp_sum_globalcur','sumbalance_globalcur']);
	}
	else
	{
		props.cardTable.showColByKey(tableid,['self_un_globalcur','selfcontrastedglobalcur','selfsumglobalcur','opp_un_globalcur','oppcontrastedglobalcur','opp_sum_globalcur','sumbalance_globalcur']);
	}
}
