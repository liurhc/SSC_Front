import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {high,base,ajax,deepClone,createPage,toast,getMultiLang } from 'nc-lightapp-front';
import getDefaultAccountBook from '../../../public/components/getDefaultAccountBook.js';
import Closeaccbook from 'uapbd/exportArea/closeaccbook';//'../../../../uapbd/orgcloseacc/closeaccbook/main';
// import Closeaccbook from '../../../../gl/orgcloseacc/closeaccbook/main';
import './index.less';
/**
* 财务组织关账
*/
export default class Closeaccbook_uapbd extends Component{
    constructor(props){
        super(props);
        this.state={
            json:{},
            modalDefaultValue:{
                bizDate:'',//业务日期
                yearbegindate:'',//年初日期
                begindate:'',
                enddate:'',
                bizPeriod:'',//账簿对应的会计期间
                isShowUnit:false,//是否显示业务单元
                isOpenUpBusiUnit:'',//
                pk_accperiodscheme:'',//会计期间方案
                pk_accountingbook:{display:'',value:''},
                appCode:'2002CLACC'
            },//模态框里默认数据
        }
    }
    componentWillMount(){
		let callback= (json) =>{
            this.setState({
              json:json,
                columns_reportSum:this.columns_reportSum,
				},()=>{
				//initTemplate.call(this, this.props);
            })
        }
		getMultiLang({moduleId:'2002CLACC',domainName:'gl',currentLocale:'simpchn',callback});
    }
        //获取默认会计期间 会计期间方案
  getDefaultYearmouth=(pk_accountingbook)=>{
    let self=this;
    let {modalDefaultValue}=self.state;
    let url = '/nccloud/gl/voucher/queryBookCombineInfo.do';
    let pk_accpont = {"pk_accountingbook":pk_accountingbook}
    ajax({
        url:url,
        data:pk_accpont,
        async:false,
        success: function(response){
            const { success } = response;
            //渲染已有账表数据遮罩
            if (success) {
                if(response.data){
                    modalDefaultValue.bizPeriod=response.data.bizPeriod;
                    modalDefaultValue.isShowUnit=response.data.isShowUnit;
                    modalDefaultValue.isOpenUpBusiUnit='Y';
                    modalDefaultValue.pk_accperiodscheme=response.data.pk_accperiodscheme;
                    modalDefaultValue.begindate=response.data.begindate;
                    modalDefaultValue.enddate=response.data.enddate;
                    modalDefaultValue.bizDate=response.data.bizDate;
                    modalDefaultValue.yearbegindate=response.data.yearbegindate;
                }
                self.setState({
                    modalDefaultValue
                })
            }   
        }
    });
}
    componentDidMount(){
        let {modalDefaultValue}=this.state;
        let appcode=this.props.getSearchParam("c");
        // let defaultAccouontBook=getDefaultAccountBook(appcode);
        getDefaultAccountBook(appcode).then((defaultAccouontBook)=>{
            modalDefaultValue.pk_accountingbook=defaultAccouontBook;
            if(defaultAccouontBook.value){
                this.getDefaultYearmouth(defaultAccouontBook.value,modalDefaultValue);
            }       
            // this.setState({
            // modalDefaultValue
            // });
        })
    }
    render(){
        let {modalDefaultValue}=this.state;
        /**
         *  后面还要考虑 多语 的情况
         * @type {{title: string, showMoudles: Object}}
         */
        let config = {
            isGl:true,
            modalDefaultValue:modalDefaultValue ,
            appCode:'2002CLACC',
            title : this.state.json['2002CLACC-000000'],//显示节点名称/* 国际化处理： 财务组织关账*/
            showMoudles : { //传入需要显示的模块 key-value 只需传入本领域对应的可以即可，value为true
                '2002'  : true,//2002总账
                // '2006'  : true,//2006应收管理
                // '2008'  : true,//2008应付管理
                // '2011'  : true,//2011费用管理
                // '3607'  : true,//3607现金管理
                // '2014'  : true,//2014存货核算
                // '3830'  : true,//3830产品成本
                // '3840'  : true,//3840项目成本会计
                // '4008'  : true,//4008库存
                // 'STOREDOC' : true//STOREDOC仓库
            }
        }
        return (
            <Closeaccbook {...{config:config}}/>
        )
    }
}
Closeaccbook_uapbd = createPage({})(Closeaccbook_uapbd)
ReactDOM.render(<Closeaccbook_uapbd />, document.querySelector('#app'));
