import { Component } from 'react';
import {high,base,ajax,toast } from 'nc-lightapp-front';
const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCPanel:Panel,NCModal:Modal,NCForm,NCButtonGroup:ButtonGroup,
} = base;
const {  NCFormItem:FormItem } = NCForm;
import PrintModal from '../../../public/components/printModal';
const { PrintOutput } = high;
import { printRequire, mouldOutput } from '../../../public/components/printModal/events';
import './index.less'
class ExportReportModal extends Component{
    constructor(props){
        super(props)

        props.fthis.state['print'] = this.print;
        props.fthis.state['output'] = this.output;
        props.fthis.setState(props.fthis.state);
        

        this.columns_report = [
            {
                title: "",
                dataIndex: "strRowOne",
                key: "strRowOne",
                width: 100,
                render:(text, record, index)=>{
                    let {fontStr}=this.state;
                    let redFlag=false;
                    for(let k in fontStr){
                        if(k==text){
                            redFlag=true;
                        }
                    }
                    if(redFlag){
                        return(
                            <span className="fontColor">{text}</span>
                        )
                    }else{
                        return(
                            <span>{text}</span>
                        )
                    }
                    
                }
            },
            {
                title: "",
                dataIndex: "strRowTwo",
                key: "strRowTwo",
                width: 100,
                render:(text, record, index)=>{
                    let {fontStr}=this.state;
                    let redFlag=false;
                    for(let k in fontStr){
                        if(k==text){
                            redFlag=true;
                        }
                    }
                    if(redFlag){
                        return(
                            <span className="fontColor">{text}</span>
                        )
                    }else{
                        return(
                            <span>{text}</span>
                        )
                    }
                    
                }
            }
        ]
        this.state={
            appcode:'2002CLACC',
            reportTableData:[],
            reportTableTitle:'',
            fontStr:{},
            accountingbook:'',
            queryDataObj:{},
            pk_period:'',
            columns_report:this.columns_report
        }
    }
    componentWillMount(){
        let self=this;
        let {reportTableData,reportTableTitle,fontStr,accountingbook,pk_period}=self.state;
        let {pk_accountingbook,period}=this.props;
        // if(pk_accountingbook==""||pk_accountingbook==undefined){
        //     toast({ content: "核算账簿不能传空", color: 'warning' });
        //     return false;
        // }
        if (pk_accountingbook !== this.state.accountingbook ) {
            self.setState({
                accountingbook:pk_accountingbook,
                pk_period:period,
            })
            let url ='/nccloud/gl/reckoning/closereport.do';
            let pk_accpont = {"pk_accountingbook":pk_accountingbook,"period":period};
            ajax({
                url:url,
                data:pk_accpont,
                success: function(response){
                    const { data,success } = response;
                    //渲染已有账表数据遮罩
                    if (success) {
                        if(response.data){
                            reportTableData=data.reportMesg;
                            reportTableTitle=data.disMesg;
                            fontStr=data.font;
                            self.setState({
                                reportTableData,reportTableTitle,fontStr
                                
                            })
                        }
                    }   
                }
            });
        }
    }
    componentWillReceiveProps(nextProp){
    }
    componentDidMount(){
        
    }
    printPreVIew(cfg={}){//预览
 
    }
    

    //打印
    print=()=> {
        this.setState({
        showPrintModal: true
        })
    }
    handlePrint=(data) =>{
        let printUrl = '/nccloud/gl/settled/closereportprint.do'
        let { queryDataObj, appcode,sumOrDetail,pk_period,accountingbook} = this.state
        let { ctemplate, nodekey } = data
        queryDataObj.queryvo = data
        queryDataObj.pk_accountingbook=accountingbook;
        queryDataObj.pk_period=pk_period;
        // queryDataObj.year=pk_period.split('/')[0];
        // queryDataObj.month=pk_period.split('/')[1];
        queryDataObj.issum='Y';
        this.setState({
            ctemplate: ctemplate,
            nodekey: nodekey,
            showPrintModal: false
        },()=>{
            printRequire(printUrl, appcode, nodekey, ctemplate, queryDataObj)
        })
    }
  //模板输出
  output=()=>{
    let outputUrl = '/nccloud/gl/accountrep/closereportoutput.do'
    let { appcode, nodekey, ctemplate, queryDataObj ,listItem,pk_period,accountingbook} = this.state;
    queryDataObj.pk_accountingbook=accountingbook;
    queryDataObj.pk_period=pk_period;
    // queryDataObj.year=pk_period.split('/')[0];
    // queryDataObj.month=pk_period.split('/')[1];
    mouldOutput(outputUrl, appcode, nodekey, ctemplate, queryDataObj)
  }
  handleOutput() {
      
  }
    render(){
        let {reportTableData,reportTableTitle,columns_report}=this.state;
        return(
            <div id='finalTreatment_report'>

                <div class='reportTitle'>{reportTableTitle} </div>
                <Table 
                    columns={columns_report} 
                    bordered 
                    data={reportTableData}
                    scroll={{ x: columns_report.length > 8 ? (100+(columns_report.length-8)*15)+"%": '100%', y: 320 }}
                />
                <PrintModal
                    noRadio={true}
                    noCheckBox={true}
                    appcode={this.state.appcode}
                    visible={this.state.showPrintModal}
                    handlePrint={this.handlePrint.bind(this)}
                    handleCancel={() => {
                    this.setState({
                        showPrintModal: false
                    })
                    }}
                />
                <PrintOutput                    
                    ref='printOutput'
                    url='/nccloud/gl/verify/verbalanceoutput.do'
                    data={this.state.outputData}
                    callback={this.handleOutput.bind(this)}
                />
            </div>
        )
    }
}
export default function (props = {}) {
    var conf = {
    };

    return <ExportReportModal {...Object.assign(conf, props)} />
}
