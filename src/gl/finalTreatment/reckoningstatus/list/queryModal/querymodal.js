import React, { Component } from 'react';
import {high,base,ajax,getMultiLang} from 'nc-lightapp-front';
import { CheckboxItem,RadioItem,TextAreaItem,ReferItem,SelectItem,InputItem,DateTimePickerItem} from '../../../../public/components/FormItems';
const { Refer} = high;
import { toast } from '../../../../public/components/utils.js';
import createScript from '../../../../public/components/uapRefer.js';
import checkMustItem from "../../../../public/common/checkMustItem.js";
import '../index.less'
const { NCFormControl: FormControl, NCDatePicker: DatePicker, NCButton: Button, NCRadio: Radio, NCBreadcrumb: Breadcrumb,
    NCRow: Row, NCCol: Col, NCTree: Tree, NCIcon: Icon, NCLoading: Loading, NCTable: Table, NCSelect: Select,
    NCCheckbox: Checkbox, NCNumber, AutoComplete, NCDropdown: Dropdown, NCPanel: Panel,NCModal:Modal,NCForm,NCDiv
} = base;
import '../index.less'
const {  NCFormItem:FormItem } = NCForm;
const config={
    "isDataPowerEnable":'Y',
    "DataPowerOperationCode":'fi'
};

  const defaultProps12 = {
    prefixCls: "bee-table",
    multiSelect: {
      type: "checkbox",
      param: "key"
    }
  };


export default class SearchModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json:{},
            showModal:false,
            isShowUnit:false,//是否显示业务单元
            pk_accperiodscheme:'',
            assData: [//辅助核算信息
            ],
            modalDefaultValue:{},//默认数据
            loadData:[],//查询模板加载数据
            listItem:{},//模板数据对应值
            SelectedProductsData:[],//选中的数据
            checkedAll:false,
            checkedArray: [
            ],
          //  columns10:this.columns10
        };
        this.close = this.close.bind(this);
    }
    componentWillMount(){
		let callback= (json) =>{
            this.setState({
              json:json,
                columns_reportSum:this.columns_reportSum,
				},()=>{
            })
        }
		getMultiLang({moduleId:'20020RECOQ',domainName:'gl',currentLocale:'simpchn',callback});
    }
    componentDidMount(){
        let self=this;
        let {assData,checkedArray}=self.state;
        let url='/nccloud/gl/glpub/initInUseProduct.do';       
        let querydata={}
        ajax({
            url:url,
            data:querydata,
            success:function(response){
                let { data, success } = response;
				if(success){
                    if(data.length>1){
                        data.map((item,i)=>{//渲染是否有动态新增列，有待优化
                            checkedArray.push(false);
                            item.key=++i;
                        })
                    }
                    assData=data;
                    self.setState({
                        assData                       
                    })
                }
            }
        })
    }
    componentWillReceiveProps (nextProp) {
        let {loadData,showOrHide,modalDefaultValue}=nextProp;
        let self=this;
        let { listItem,showModal }=self.state
        if (showOrHide&&nextProp.loadData !== self.state.loadData&&self.state.loadData.length==0) {
            self.queryBizDate(modalDefaultValue.pk_accountingbook.value);
            loadData.forEach((item,i)=>{
                let  key;
                if(item.itemType=='refer'){
                    if(item.itemKey=='pk_accasoa'||item.itemKey=='pk_units'){
                        key=[{
                            display:'',
                            value:''
                        }]
                    }else if(item.itemKey=='pk_accountingbook'){
                        key=[{
                            display:modalDefaultValue.pk_accountingbook.display,
                            value:modalDefaultValue.pk_accountingbook.value
                        }]
                    }else{
                        key={
                            display:'',
                            value:''
                        }
                    }                   
                }else if(item.itemType=='select'||item.itemType=='Dbselect'||item.itemType=='radio'){//下拉框赋初始值
                    key={
                        value:item.itemChild[0].value
                    }
                }else{
                    key={
                        value:''
                    }
                    if(item.itemType=='date'||item.itemType=='Dbdate'){
                       
                        key={
                        //    value:moment().format("YYYY-MM-DD")
                        value:''
                        }
                    }
                }
                if(item.itemType=='Dbdate'||item.itemType=='DbtextInput'){
                    item.itemKey.map((k,index)=>{
                        let name= k;
                        listItem[name]=key
                    });
                }
                else{
                    let name= item.itemKey;
                    listItem[name]=key
                }
            
            })
            self.setState({
                loadData:loadData,
                modalDefaultValue:modalDefaultValue,
                showModal:showOrHide,
                listItem
            })
        }else{
            self.setState({
                showModal:showOrHide,
            })
        }
    }

    //表格操作根据key寻找所对应行
	findByKey(key, rows) {
		let rt = null;
		let self = this;
		rows.forEach(function(v, i, a) {
			if (v.key == key) {
				rt = v;
			}
		});
		return rt;
	}

    close() {
        this.props.handleClose();
    }

    confirm=()=>{
        let { listItem,assData,checkedArray,SelectedProductsData } =this.state;
        SelectedProductsData=[];


        if(listItem.pk_accountingbook[0]&&listItem.pk_accountingbook[0].value=="" && listItem.year_month&&listItem.year_month.value==""){
            toast({content:this.state.json['20020RECOQ-000018'],color:'warning'});/* 国际化处理： 核算账簿、会计期间为必输项，请知悉*/
            return false;
        }
        if(!listItem.pk_accountingbook[0]||listItem.pk_accountingbook[0].value==""){
            toast({content:this.state.json['20020RECOQ-000019'],color:'warning'});/* 国际化处理： 核算账簿为必输项，请知悉*/
            return false;
        }
        if(!listItem.year_month||listItem.year_month.value==""){
            toast({content:this.state.json['20020RECOQ-000020'],color:'warning'});/* 国际化处理： 会计期间为必输项，请知悉*/
            return false;
        }
        
        
        for(var i = 0; i < checkedArray.length; i++){
            if(checkedArray[i]==true){
                SelectedProductsData.push(assData[i].Code);
            }
        }
        listItem.products=SelectedProductsData;   
        if(listItem.products.length==0){
            toast({content:this.state.json['20020RECOQ-000021'],color:'warning'});/* 国际化处理： 请选择产品*/
            return false;

        }      
        this.props.onConfirm(listItem);    
    }
    queryBizDate=(pk_accountingbook)=>{
        let self=this;
        let{pk_accperiodscheme,listItem}=this.state;
        if(pk_accountingbook){
            let url = '/nccloud/gl/glpub/queryBizDate.do';
            let pk_accpont = {"pk_accountingbook":pk_accountingbook}
            ajax({
                url:url,
                data:pk_accpont,
                success: function(response){
                    const { success } = response;
                    //渲染已有账表数据遮罩
                    if (success) {
                        if(response.data){
                            pk_accperiodscheme=response.data.pk_accperiodscheme;
                            listItem['year_month'].display=response.data.bizPeriod;
                            listItem['year_month'].value=response.data.bizPeriod;
                        }
                        self.setState({
                            pk_accperiodscheme
                        })
                    }   
                }
            });
        }
    }
    queryList=(data)=>{
        let self=this;
        let columns10=[
            {
              title: (<div fieldid="Code" className="mergecells">{self.state.json['20020RECOQ-000015']}</div>),/* 国际化处理： 产品编码*/
              dataIndex: "Code",
              key: "Code",
              width: "30%",
              render: (text, record, index) => {
                return <div fieldid="Code">{!text?<span>&nbsp;</span>:text}</div>
            }
            },
            {
              title: (<div fieldid="Name" className="mergecells">{self.state.json['20020RECOQ-000016']}</div>),/* 国际化处理： 产品名称*/
              dataIndex: "Name",
              key: "Name",
              render: (text, record, index) => {
                return <div fieldid="Name">{!text?<span>&nbsp;</span>:text}</div>
            }
            }
        ];
        const dateInputPlaceholder = self.state.json['20020RECOQ-000014'];/* 国际化处理： 选择日期*/
        const emptyFunc = () => <span>{self.state.json['20020RECOQ-000024']}！</span>/* 国际化处理： 这里没有数据*/
        function getNowFormatDate() {
            let date = new Date();
            let seperator1 = "-";
            let year = date.getFullYear();
            let month = date.getMonth() + 1;
            let strDate = date.getDate();
            if (month >= 1 && month <= 9) {
                month = "0" + month;
            }
            if (strDate >= 0 && strDate <= 9) {
                strDate = "0" + strDate;
            }
            let currentdate = year + seperator1 + month + seperator1 + strDate;
            return currentdate;
        }
        let currrentDate = getNowFormatDate();
        let { listItem,isShowUnit,assData,pk_accperiodscheme,modalDefaultValue } =self.state;
			return data.length!=0?(
				data.map((item, i) => {                   
                    if(item.itemType=="refer") {
                       //case 'refer':
                       let referUrl= item.config.refCode+'/index.js';
                       let DBValue=[];
                       let defaultValue={}
                       if(listItem[item.itemKey].length){                           
                            listItem[item.itemKey].map((item,index)=>{
                                DBValue[index]={ refname: item.display, refpk:item.value };
                            })
                        }else{
                            defaultValue = { refname: listItem[item.itemKey].display, refpk: listItem[item.itemKey].value };  
                        }
                       //let defaultValue = { refname: listItem[item.itemKey].display, refpk: listItem[item.itemKey].value };  
					   if(!self.state[item.itemKey]){
                           {createScript.call(self,referUrl,item.itemKey)}
                           return <div />
					    }else{
                            if(item.itemKey=='pk_accountingbook'){
                                return (
                                    <FormItem
                                         inline={true}
                                         showMast={true}
                                         labelXs={2} labelSm={2} labelMd={2}
                                         xs={10} md={10} sm={10}
                                         labelName={item.itemName}
                                         isRequire={true}
                                         method="change"
                                         className="account"
                                        >
                                        {self.state[item.itemKey]?(self.state[item.itemKey])(
                                            {
                                                fieldid:item.itemKey,
                                                value:DBValue,
                                                isMultiSelectedEnabled:true,
                                                queryCondition:() => {
                                                    return Object.assign({
                                                        "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                                                        "appcode":modalDefaultValue.appcode
                                                    },config)
                                                 },
                                                onChange:(v)=>{
                                                    // if(v.length<=0){return false;}
                                                    if(v&&v.length>0&&v[0].refpk){
                                                        self.queryBizDate(v[0].refpk);
                                                    }
                                                    listItem[item.itemKey]=[];
                                                    v.map((arr,index)=>{
                                                        let accountingbook={
                                                            display:arr.refname,
                                                            value:arr.refpk
                                                        }
                                                        listItem[item.itemKey].push(accountingbook);
                                                    })
                                                    this.setState({
                                                        listItem
                                                    })
                                                }
                                            }
                                        ):<div />}
                                </FormItem>);
                            }else{
                                return (
                                    <FormItem
                                         inline={true}
                                         showMast={true}
                                         labelXs={2} labelSm={2} labelMd={2}
                                         xs={10} md={10} sm={10}
                                         labelName={item.itemName}
                                         isRequire={true}
                                         method="change"
                                         className="yeard"
                                        >
                                        {self.state[item.itemKey]?(self.state[item.itemKey])(
                                            {
                                                fieldid:item.itemKey,
                                                value:defaultValue,
                                                //isMultiSelectedEnabled:true,
                                                queryCondition:() => {
                                                    if(item.itemKey=='year_month'){
                                                        return Object.assign({
                                                            "pk_accperiodscheme":pk_accperiodscheme,
                                                            // "GridRefActionExt":'nccloud.web.gl.ref.FilterAdjustPeriodRefSqlBuilder'
                                                        },config)
                                                    }else{
                                                        return Object.assign(config, {
                                                            //"pk_accountingbook": self.state.pk_accountingbook.value
                                                        })
                                                    }
                                                },    
                                                onChange:(v)=>{
                                                    listItem[item.itemKey].value = v.refpk
                                                    listItem[item.itemKey].display = v.refname
                                                    this.setState({
                                                        listItem
                                                    })
                                                }
                                            }
                                        ):<div />}
                                </FormItem>);
                            } 
						   
					    }
                    }else if(item.itemType=="date"){ 
                            return (
                                    <FormItem
                                        inline={true}
                                        showMast={true}
                                        labelXs={2}
                                        labelSm={2}
                                        labelMd={2}
                                        xs={10}
                                        md={10}
                                        sm={10}
                                        labelName={item.itemName}
                                        method="blur"
                                        inputAlfer="%"
                                        errorMessage={this.state.json['20020RECOQ-000022']}/* 国际化处理： 输入格式错误*/
                                    >
                                        <DatePicker
                                            fieldid={item.itemKey}
                                            name={item.itemKey}
                                            type="customer"
                                            isRequire={true}
                                            value={listItem[item.itemKey].value}
                                            onChange={(v) => {
                                                listItem[item.itemKey].value = v;
                                                this.setState({
                                                    listItem
                                                })
                                            }}
                                            placeholder={dateInputPlaceholder}
                                            />
                                        </FormItem>
                            );
                    }
                    
                    else if(item.itemType=="Dbdate"){     
                            return(
                                <FormItem
                                    inline={true}
                                    showMast={true}
                                    labelXs={2} labelSm={2} labelMd={2}
                                    xs={10} md={10} sm={10}
                                    labelName={item.itemName}
                                    method="blur"
                                    inputAlfer="%"
                                    errorMessage={this.state.json['20020RECOQ-000022']}/* 国际化处理： 输入格式错误*/
                                    inputAfter={
                                        <Col xs={12} md={12} sm={12}>
                                        <span className="online">&nbsp;--&nbsp;</span>
                                        <DatePicker
                                        fieldid="endDate"
                                        name={item.itemKey}
                                        type="customer"
                                        isRequire={true}
                                        value={listItem.endDate.value}
                                        onChange={(v) => {
                                            listItem.endDate={value: v}
                                            this.setState({
                                                listItem
                                            })
                                        }}
                                        placeholder={dateInputPlaceholder}
                                    /></Col>}
                                    >
                                    <DatePicker
                                        fieldid="beginDate"
                                        name={item.itemKey}
                                        type="customer"
                                        isRequire={true}
                                        value={listItem.beginDate.value}
                                        onChange={(v) => {
                                            listItem.beginDate={value: v}
                                            this.setState({
                                                listItem
                                            })
                                        }}
                                        placeholder={dateInputPlaceholder}
                                    />
                                </FormItem>
                            );
                    }else if(item.itemType=="textInput"){     
                            return (
                                <FormItem
                                    inline={true}
                                    showMast={true}
                                    labelXs={2}
                                    labelSm={2}
                                    labelMd={2}
                                    xs={10}
                                    md={10}
                                    sm={10}
                                    labelName={item.itemName}
                                    isRequire={true}
                                    method="change"
                                >
                                    <FormControl
                                        fieldid={item.itemKey}
                                        value={listItem[item.itemKey].value}
                                        onChange={(v) => {
                                            listItem[item.itemKey].value = v
                                            this.setState({
                                                listItem
                                            })
                                        }}
                                    />
                                </FormItem>
                            );
                    }else if(item.itemType=="DbtextInput"){    
                            return(
                                <FormItem
                                    inline={true}
                                    showMast={false}
                                   labelXs={2}  labelSm={2} labelMd={2}
                                    xs={10}  md={10} sm={10}
                                    labelName={item.itemName}
                                    isRequire={true}
                                    method="change"
                                    inputAfter={
                                    <Col xs={12} md={12} sm={12}>
                                        <span className="online">&nbsp;--&nbsp;</span>                                                                          
                                        <FormControl
                                            fieldid={item.itemKey[1]}
                                            value={listItem[item.itemKey[1]].value}
                                            onChange={(v) => {
                                                let startkey=item.itemKey[0];
                                                let endkey=item.itemKey[1];
                                                listItem[endkey]={value : v}
                                                this.setState({
                                                    listItem
                                                })
                                            }}
                                        />
                                    </Col>}
                                >
                                    <FormControl
                                        fieldid={item.itemKey[0]}
                                        value={listItem[item.itemKey[0]].value}
                                        className="DbtextInput"
                                        onChange={(v) => {
                                            listItem[item.itemKey[0]]={value : v}
                                            this.setState({
                                                listItem
                                            })
                                        }}
                                    />
                                </FormItem>
                            );
                    }
                    else if(item.itemType=="radio"){    
                            return(<div className="bottradio">
                                <span className="confirma">{this.state.json['20020RECOQ-000007']}</span>{/* 国际化处理： 结账状态*/}
                                <FormItem
                                className="bottoma"
                                inline={true}
                                showMast={false}
                                labelXs={2}
                                labelSm={2}
                                labelMd={2}
                                xs={10}
                                md={10}
                                sm={10}
                                labelName={item.itemName}
                                isRequire={true}
                                method="change"
                            >
                            
                                    <RadioItem
                                        fieldid={item.itemKey}
                                        className="radio"
                                        name={item.itemKey}
                                        type="customer"
                                        defaultValue={listItem[item.itemKey].value?listItem[item.itemKey].value:item.itemChild[0].value}
                                        items={() => {
                                            return (item.itemChild) 
                                        }}
                                        onChange={(v)=>{
                                            listItem[item.itemKey].value = v
                                                this.setState({
                                                    listItem
                                                })
                                        }}
                                    />
                            </FormItem></div>
                            )
                    }
                    else if(item.itemType=="select"){    
                            return(
                                <FormItem
                                inline={true}
                                showMast={false}
                                labelXs={2}
                                labelSm={2}
                                labelMd={2}
                                xs={10}
                                md={10}
                                sm={10}
                                labelName={item.itemName}
                                isRequire={true}
                                method="change"
                            >
                                <SelectItem
                                    name={item.itemKey}
                                    fieldid={item.itemKey}
                                    defaultValue={item.itemChild[0].value?item.itemChild[0].value:''} 
                                        items = {
                                            () => {
                                                return (item.itemChild) 
                                            }
                                        }
                                        onChange={(v)=>{
                                           
                                            listItem[item.itemKey].value = v
                                                this.setState({
                                                    listItem
                                                })
                                        }}
                            />
                            </FormItem>)
                    }else if(item.itemType=="Dbselect"){    
                            return(
                                <FormItem
                                    inline={true}
                                    showMast={false}
                                    labelXs={2}
                                    labelSm={2}
                                    labelMd={2}
                                    xs={2}
                                    md={2}
                                    sm={2}
                                    labelName={item.itemName}
                                    isRequire={true}
                                    method="change"
                                    >
                                    <SelectItem 
                                    fieldid={item.itemKey}
                                    name={item.itemKey}
                                        defaultValue={item.itemChild[0].value?item.itemChild[0].value:''} 
                                            items = {
                                                () => {
                                                    return (item.itemChild) 
                                                }
                                            }
                                            onChange={(v)=>{
                                                listItem[item.itemKey].value = v
                                                this.setState({
                                                    listItem
                                                })
                                            }}
                                    />
                                </FormItem>
                            )
                    }else if(item.itemType=="checkbox"){    
                        return(
                            <FormItem
                            inline={true}
                            showMast={false}
                            xs={12}  md={12} sm={12}
                            className='checkboxStyle'
                            method="change"
                        >
                            <CheckboxItem name={item.itemKey} 
                            //defaultValue={this.state.periodloan.value} 
                                boxs = {
                                    () => {
                                        return (item.itemChild) 
                                    }
                                }
                                onChange={(v)=>{
                                    
                                    listItem[item.itemKey].value = (v[0].checked==true)?'Y':'N';
                                    this.setState({
                                        listItem
                                    })
                                }}
                            />
                        </FormItem>
                        )
                   }else if(item.itemType=="table"){
                    let { loadData,assData} =this.state;
                    let columnsldad = this.renderColumnsMultiSelect(columns10);
                        return(<div filedid={`${item.itemKey}_table`}>
                             <Table
                                 columns={columnsldad} data={assData}
                                 emptyText={emptyFunc}
                             />
                            </div>
                        )
                   }
               })
			):<div/>;
           
    }


    onAllCheckChange = () => {
        let self = this;
        let checkedArray = [];
        let selIds = [];
        for (var i = 0; i < self.state.checkedArray.length; i++) {
          checkedArray[i] = !self.state.checkedAll;
        }
        self.setState({
          checkedAll: !self.state.checkedAll,
          checkedArray: checkedArray,
        });
    };
    onCheckboxChange = (text, record, index) => {
        let self = this;
        let allFlag = false;
        let checkedArray = self.state.checkedArray.concat();
        checkedArray[index] = !self.state.checkedArray[index];
        for (var i = 0; i < self.state.checkedArray.length; i++) {
          if (!checkedArray[i]) {
            allFlag = false;
            break;
          } else {
            allFlag = true;
          }
        }
        self.setState({
          checkedAll: allFlag,
          checkedArray: checkedArray,
        });
    };
      
    renderColumnsMultiSelect(columns) {
        const {checkedArray } = this.state;
        const { multiSelect } = this.props;
        let select_column = {};
        let indeterminate_bool = false;
        if (multiSelect && multiSelect.type === "checkbox") {
          let i = checkedArray.length;
          while(i--){
              if(checkedArray[i]){
                indeterminate_bool = true;
                break;
              }
          }
          let defaultColumns = [
            {
              title: (<div fieldid="firstcol" className='checkbox-mergecells'>{
                <Checkbox
                  className="table-checkbox"
                  checked={this.state.checkedAll}
                  indeterminate={indeterminate_bool&&!this.state.checkedAll}
                  onChange={this.onAllCheckChange}
                />
              }</div>),
              key: "checkbox",
              dataIndex: "checkbox",
              width: "5%",
              render: (text, record, index) => {
                  return (
                      <div fieldid="firstcol">
                          <Checkbox
                              className="table-checkbox"
                              checked={this.state.checkedArray[index]}
                              onChange={this.onCheckboxChange.bind(this, text, record, index)}
                          />
                      </div>
                );
              }
            }
          ];
          columns = defaultColumns.concat(columns);
        }
        return columns;
    }

    render() {
        let{showOrHide}=this.props;
        let { loadData} =this.state;
        return (
            <div className="fl">
                <Modal
                fieldid="query"
                    className={'msg-modal check'}
                    show={showOrHide }
                    id="queryone"
                    onHide={ this.close }
                    animation={true}
                    >
                    <Modal.Header closeButton fieldid="header-area">
                        <Modal.Title>{this.state.json['20020RECOQ-000025']}</Modal.Title>{/* 国际化处理： 查询条件*/}
                    </Modal.Header >
                    <Modal.Body >
                        <NCDiv fieldid="query" areaCode={NCDiv.config.FORM} className="nc-theme-form-label-c">
                            <NCForm useRow={true}
                                submitAreaClassName='classArea'
                                showSubmit={false}　>
                                {loadData.length>0?this.queryList(loadData):''}
                            </NCForm>
                        </NCDiv>
                    </Modal.Body>
                    <Modal.Footer fieldid="bottom_area">
                        <Button colors="primary" onClick={ this.confirm } fieldid="confirm"> {this.state.json['20020RECOQ-000017'] }</Button>{/* 国际化处理： 查询*/}
                        <Button onClick={ this.close } fieldid="close"> {this.state.json['20020RECOQ-000026']} </Button>{/* 国际化处理： 取消*/}
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}
SearchModal.defaultProps = defaultProps12;
