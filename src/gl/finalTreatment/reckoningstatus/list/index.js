import React, { Component } from 'react';
import {createPage,high,base,ajax,deepClone,getMultiLang,createPageIcon } from 'nc-lightapp-front';
import { buttonClick, initTemplate} from './events';
import './index.less'
const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCPanel:Panel,NCForm,NCButtonGroup:ButtonGroup,NCAffix,NCDiv
} = base;
const {  NCFormItem:FormItem } = NCForm;
import { toast } from '../../../public/components/utils.js';
import getDefaultAccountBook from '../../../public/components/getDefaultAccountBook.js';
import { InputItem} from '../../../public/components/FormItems';
import QueryModal from './queryModal/querymodal.js';
import ReportModal from '../../reckoning/list/reportModal/reportModal';
import PrintModal from '../../../public/components/printModal';
const { PrintOutput } = high;
import { printRequire, mouldOutput } from '../../../public/components/printModal/events';
import HeaderArea from '../../../public/components/HeaderArea';
import { getTableHeight } from '../../../public/common/method.js';
class VerifyBalances extends Component{
    constructor(props){
        super(props);
        this.state={
            json:{},
            appcode:this.props.getUrlParam('c') || this.props.getSearchParam('c'),
            selectedRecord:'',//复制的key
            getrowkey: '',//获取指定行的Key
            listItem:{
                yearMonth:{display:'',value:''},
            },
            modalDefaultValue:{
                pk_accountingbook:{display:'',value:''},
                appcode:this.props.getSearchParam("c")
            },//模态框里默认数据
            pk_accountingbook:{display:'',value:''},
            isQueryShow:false,//查询条件对话框
            ShowHideFlag:false,//结账报告对话框
            columnsVerify:[],
            verifyBalancesData:[],
            queryCondition:[],//查询条件
            selectedData:{},//选中行的数据
            reportDataSum:[],
            reportSumTitle:'',
            reportSumFont:{},
            reportData:[],
            reportTitle:'',
            reportFont:{},
            queryDataObj:{}
        }
    }
    componentWillMount(){
		let callback= (json) =>{
            this.Defaultcolumns = [
                {
                    title: (<div fieldid="m_modulename" className="mergecells">{json['20020RECOQ-000006']}</div>),/* 国际化处理： 财务产品*/
                    dataIndex: "m_modulename",
                    key: "m_modulename",
                    width: 100,
                    render: (text, record, index) => <div fieldid="m_modulename">{!text?<span>&nbsp;</span>:text}</div>
                },
                {
                    title: (<div fieldid="m_bookname" className="mergecells">{json['20020RECOQ-000000']}</div>),/* 国际化处理： 核算账簿*/
                    dataIndex: "m_bookname",
                    key: "m_bookname",
                    width: 400,
                    render: (text, record, index) => <div fieldid="m_bookname">{!text?<span>&nbsp;</span>:text}</div>
                },
                {
                    title: (<div fieldid="m_settle" className="mergecells">{json['20020RECOQ-000007']}</div>),/* 国际化处理： 结账状态*/
                    dataIndex: "m_settle",
                    key: "m_settle",
                    width: 100,
                    render:(text, record, index) => {
                    
                        return(
                            <div fieldid="m_settle">{text==false?json['20020RECOQ-000004']:json['20020RECOQ-000003']}</div>/* 国际化处理： 未结账,已结账*/
                        )
                    }
                },
            ];
            this.setState({
              json:json,
                columns_reportSum:this.columns_reportSum,
				},()=>{
				initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:'20020RECOQ',domainName:'gl',currentLocale:'simpchn',callback});
        let {modalDefaultValue}=this.state;
        let appcode=this.props.getSearchParam("c");
        // let defaultAccouontBook=getDefaultAccountBook(appcode);
        getDefaultAccountBook(appcode).then((defaultAccouontBook)=>{
            modalDefaultValue.pk_accountingbook=defaultAccouontBook;
            this.setState({
                modalDefaultValue
            })
        })
    }
    //查询
    handleQuery=()=>{
        this.setState({
            isQueryShow:true
        })
    }
    //查询确定按钮 
    handleQueryClick=(data)=>{
        let self=this;
        let {pk_accountingbook,listItem,columnsVerify,verifyBalancesData,queryCondition}=this.state;
        let url='/nccloud/gl/reckoning/reckoningstatus.do';
        let childData = deepClone(data);
        for(let k in childData){
			if(k=='ass'||k=='acccode'||k=='accname'){
			}else if(k=='products'){

            }else if(k=='pk_accountingbook'){
                if(childData[k].length>0){
                    if(childData[k][0].value==""){
                        childData[k]=null;
                    }else{
                        childData[k].map((item,index)=>{
                            childData[k][index]=item.value;
                        })
                    }
                }else{
                    childData[k]=null;
                }
            }else if(k=='year_month'){
                listItem.yearMonth=childData[k];
                childData.year=childData[k].display.split('-')[0];
                childData.period=childData[k].display.split('-')[1];
            }else{
				childData[k]=childData[k].value?childData[k].value:null;
			}			
        }
        pk_accountingbook=childData.pk_accountingbook;
        queryCondition=childData;
        let buttonFalse=[];
        ajax({
            url:url,
            data:childData,
            success:function(response){
                let { data, success } = response;
				if(success){
                    if(data&&data.settleState){
                        //columnsVerify=self.changeWidth(data.settleState);
                        verifyBalancesData=data.settleState;
                        if(verifyBalancesData&&verifyBalancesData.length>0){
                            verifyBalancesData.map((item,i)=>{
                                item.key=++i;
                            })
                            buttonFalse=['startend','print','refresh'];
                        }else{
                            verifyBalancesData=[];
                            buttonFalse=[];
                        }
                    }else{
                        verifyBalancesData=[];
                        buttonFalse=[];
                    }
                    self.setState({
                        listItem,queryCondition,pk_accountingbook,
                        verifyBalancesData,isQueryShow: false,            
                    })
                    self.props.button.setButtonDisabled(buttonFalse, false);
                }
            },
            error:function(error){
                self.setState({
                    isQueryShow: false,
                })
            }
        })
        
        
        let trs=document.getElementsByClassName("u-table-row-level-0")
        Array.from(trs).forEach((a,b,c)=>{
            trs[b].id=""
        })
    }
    //处理width
    changeWidth(arr){
        arr.map((item,index)=>{
            if(item.children){
                this.changeWidth(item.children);
            }else{
               item['width']=100; 
            }            
        })
        return arr;
    }
    getHeadData=(listItem)=>{   
       let appcode=this.props.getSearchParam("c");
  
       const headData=[
        {itemName:this.state.json['20020RECOQ-000005'],itemType:'textInput',itemKey:'yearMonth'},/* 国际化处理： 会计期间:*/
    ]
        return (
            headData.map((item,i)=>{
                let defValue=listItem[item.itemKey].display;
              
                switch (item.itemType) {
                    case 'textInput':
                        return(
                        <FormItem
                            inline={true}
                            //showMast={true}
                            labelXs={1} labelSm={1} labelMd={1}
                            xs={2} md={2} sm={2}
                            labelName={item.itemName}
                            isRequire={true}
                            method="change"
                            >
                            {/* <FormControl
                                value={defValue}
                            /> */}
                            <InputItem
                                fieldid={item.itemKey}
                                //isViewMode
                                disabled={true}
                                name={item.itemKey}
                                type="customer"
                                defaultValue={defValue}
                            />
                        </FormItem> 
                    )
                    default:
                    break;
                }
            })
            
        )  
    }
    //刷新
    handleRefresh=()=>{
        let self=this;
        let {queryCondition,verifyBalancesData}=this.state;
        let url='/nccloud/gl/reckoning/reckoningstatus.do';
        ajax({
            url:url,
            data:queryCondition,
            success:function(response){
                let { data, success } = response;
				if(success){
                    //columnsVerify=self.changeWidth(data.settleState);
                    verifyBalancesData=data.settleState;
                    if(verifyBalancesData.length>1){
                        verifyBalancesData.map((item,i)=>{//渲染是否有动态新增列，有待优化
                            item.key=++i;
                        })
                    }
                    self.setState({
                        verifyBalancesData,                       
                    })
                }
            }
        })
        let buttonFalse=['startend','print','refresh']
            this.props.button.setButtonDisabled(buttonFalse, false);
            let trs=document.getElementsByClassName("u-table-row-level-0")
        Array.from(trs).forEach((a,b,c)=>{
            trs[b].id=""
        })
            
    }
    componentDidMount(){}
    //开始结账
    handleChange=()=>{
        let self=this;
        let {pk_accountingbook,selectedData,reckonintData,listItem,
            ShowHideFlag,reportDataSum,reportSumTitle,reportSumFont,reportData,reportTitle,reportFont}=self.state;
      
        if(!selectedData.m_moduleid){
            toast({ content: self.state.json['20020RECOQ-000008'], color: 'warning' });/* 国际化处理： 请先选中一行数据*/
            return false;
        }
        if(pk_accountingbook==""||pk_accountingbook==undefined){
            toast({ content: self.state.json['20020RECOQ-000009'], color: 'warning' });/* 国际化处理： 核算账簿不能为空*/
            return false;
        }
        let url ='/nccloud/gl/reckoning/reckoning.do';
        let pk_accpont = {
            "moduleid": selectedData.m_moduleid,
            "pk_accountingbook":selectedData.m_pk_accountingbook,
        };
        ajax({
            url:url,
            data:pk_accpont,
            success: function(response){
                const { data,success } = response;
                //渲染已有账表数据遮罩
                if (success) {
                    if(response.data){
                        let isReckoningble=data.isReckoningble;
                        if(isReckoningble){
                            toast({ content: self.state.json['20020RECOQ-000010'], color: 'success' });/* 国际化处理： 结账成功*/
                            reckonintData=data.reckedYear;
                            reckonintData.map((item,index)=>{
                                item.key=index;
                            })
                            listItem['yearMonth'].value=data.reckedMonth;
                            self.setState({
                                reckonintData,listItem,
                            })
                        }else{
                            toast({ content: self.state.json['20020RECOQ-000011'], color: 'warning' });/* 国际化处理： 结账失败*/
                            reportDataSum=data.reportSumMesg;
                            reportSumTitle=data.sumDisMesg;
                            reportSumFont=data.sumFont;
                            reportData=data.reportMesg;
                            reportTitle=data.disMesg;
                            reportFont=data.font;
                            self.setState({
                                reportDataSum,reportSumTitle,reportSumFont,reportData,reportTitle,reportFont,
                                ShowHideFlag:true,
                            })
                        }    
                    }
                }   
            }
        });

    }
    //关闭
    handleClose=()=>{
        this.setState({
			ShowHideFlag:false,
			
		})
    }
    closeQuery =() => {
        this.setState({
			isQueryShow:false
			
		})
    }
      //打印
    handlePrintReport() {
        this.setState({
        showPrintModal: true
        })
    }
    handlePrint(data) {
        let printUrl = '/nccloud/gl/accountrep/reckoningreportprint.do'
        let { queryDataObj, appcode,sumOrDetail,listItem,queryCondition} = this.state
        let { ctemplate, nodekey } = data
        queryDataObj.queryvo = data
        queryDataObj.pk_accountingbook=queryCondition.pk_accountingbook[0];
        queryDataObj.year=queryCondition['year'];
        queryDataObj.month=queryCondition['period'];
        queryDataObj.issum='Y';
        this.setState({
            ctemplate: ctemplate,
            nodekey: nodekey
        })
        printRequire(printUrl, appcode, nodekey, ctemplate, queryDataObj)
        this.setState({
            showPrintModal: false
        });
    }
  //模板输出
  showOutputModal() {
    let outputUrl = '/nccloud/gl/accountrep/reckoningreportoutput.do'
    let { appcode, nodekey, ctemplate, queryDataObj ,listItem,queryCondition} = this.state;
        queryDataObj.pk_accountingbook=queryCondition['pk_accountingbook'][0];
        queryDataObj.year=listItem.yearMonth.display.split('-')[0];
        queryDataObj.month=listItem.yearMonth.display.split('-')[1];
    mouldOutput(outputUrl, appcode, nodekey, ctemplate, queryDataObj)
  }
  handleOutput() {
  }
    //选中某一行
    getRow = (expanded, record) => {
		let copyData=deepClone(expanded);
		//表体数据增删获取key
        let { getrowkey,selectedRecord,selectedData } = this.state;
        selectedData=copyData;
		let getrow = expanded.key;
		this.setState({
            getrowkey: getrow,
            selectedRecord:record,
            selectedData
		},()=>{
            let buttonFalse=['startend','print','refresh']
            this.props.button.setButtonDisabled(buttonFalse, false);
        });
       
        let trs=document.getElementsByClassName("u-table-row-level-0")
        Array.from(trs).forEach((a,b,c)=>{
            // trs[b].style.background=""
            // trs[record].style.background="rgb(228,228,228)"
            trs[b].id=""
            trs[record].id="idya"
        })
        
      
    };
    // getTableHeight = () => {
    //     let accountContentHeight = this.refs.accountContent && getComputedStyle(this.refs.accountContent, null).height || "400px";
    //     let tableHeight = accountContentHeight.replace('px', '') - 60;
    //     return tableHeight;
    //     }
    getTableHeight = () => {
        let tableHeight=document.getElementById('app').offsetHeight-110;
        return tableHeight;
    }
    render(){
        
        let{columnsVerify,verifyBalancesData,listItem,
            reportDataSum,reportSumTitle,reportSumFont,reportData,reportTitle,reportFont,modalDefaultValue}=this.state;
        if(columnsVerify.length==0){
            columnsVerify=this.Defaultcolumns?this.Defaultcolumns:[];
        }
        let reportSum={
            reportSumData:reportDataSum,
            reportSumTitle:reportSumTitle,
            reportSumFont:reportSumFont,           
        }
        let reportDetail={
            reportDetailData:reportData,
            reportDetailTitle:reportTitle,
            reportDetailFont:reportFont
        }
        const loadQuery=[
            {itemName:this.state.json['20020RECOQ-000000'],itemType:'refer',itemKey:'pk_accountingbook',/* 国际化处理： 核算账簿*/
            config:{refCode:"uapbd/refer/org/AccountBookTreeRef"},queryGridUrl:'/nccloud/gl/voucher/ref.do',refType:'grid'},
            {itemName:this.state.json['20020RECOQ-000001'],itemType:'refer',itemKey:'year_month',/* 国际化处理： 会计期间*/
            config:{refCode:"uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef"},queryGridUrl:'/nccloud/gl/voucher/ref.do',refType:'grid'},
            {itemName:'',itemType:'table',itemKey:'products'},
            {itemName:'',itemType:'radio',itemKey:'reckoningStatusToQuery',
                itemChild: [
                    {
                        label: this.state.json['20020RECOQ-000002'],/* 国际化处理： 全部*/
                        value: '0',
                    },
                    {
                        label: this.state.json['20020RECOQ-000003'],/* 国际化处理： 已结账*/
                        value: '3',
                    },
                    {
                        label: this.state.json['20020RECOQ-000004'],/* 国际化处理： 未结账*/
                        value: '4',
                    }
                ]
            }
        ]
        return(
            <div className="verifyBalances nc-bill-list" id="verifyBalances" ref="accountContent">
            <HeaderArea 
                    title = {this.state.json['20020RECOQ-000013']}/* 国际化处理： 财务系统结账状态查询*/
                    searchContent = {
                        <div className="headtitle nc-theme-form-label-c" fieldid="ncform_form">
                            <NCForm useRow={true}
                                submitAreaClassName='classArea'
                                showSubmit={false}　>
                                {this.getHeadData(listItem)}
                            </NCForm>
                        </div>
                    }
                    btnContent = {
                        this.props.button.createButtonApp({
                            area: 'button_area',
                            onButtonClick: buttonClick.bind(this),
                        })
                    }
                />
                <QueryModal 
                    loadData={loadQuery} 
                    modalDefaultValue={modalDefaultValue} 
                    showOrHide = {this.state.isQueryShow}  
                    onConfirm = {this.handleQueryClick.bind(this)} 
                    //handleClose={this.handleQuery.bind(this)}
                    handleClose={this.closeQuery.bind(this)}
                />
                <NCDiv fieldid="reckoningstatus" areaCode={NCDiv.config.TableCom}>
                    <Table
                        columns={columnsVerify}
                        onRowClick={this.getRow}
                        bordered
                        data={verifyBalancesData}
                        bodyStyle={{height:getTableHeight(80)}}
                        scroll={{
                            x: true,
                            y: getTableHeight(80)
                            //    y:"calc(100vh - 250px)"
                        }}
                        // scroll={{ x: columnsVerify.length > 8 ? (100+(columnsVerify.length-8)*15)+"%": '100%', y: this.getTableHeight() }}
                    />
                </NCDiv>
                <ReportModal 
                    reportSum={reportSum}
                    reportDetail={reportDetail}
                    showOrHide = {this.state.ShowHideFlag}  
                    // onConfirm = {this.handleQueryClick.bind(this)} 
                    handlePrintReport={this.handlePrintReport.bind(this)}//打印
                    showOutputModal={this.showOutputModal.bind(this)}//模板输出
                    handleClose={this.handleClose.bind(this)}
                />
                <PrintModal
                    noRadio={true}
                    noCheckBox={true}
                    appcode={this.state.appcode}
                    visible={this.state.showPrintModal}
                    handlePrint={this.handlePrint.bind(this)}
                    handleCancel={() => {
                    this.setState({
                        showPrintModal: false
                    })
                    }}
                />
                <PrintOutput                    
                    ref='printOutput'
                    url='/nccloud/gl/verify/verbalanceoutput.do'
                    data={this.state.outputData}
                    callback={this.handleOutput.bind(this)}
                />
            </div>
        )    
    }
}
VerifyBalances = createPage({})(VerifyBalances);
ReactDOM.render(<VerifyBalances />, document.querySelector("#app"));
