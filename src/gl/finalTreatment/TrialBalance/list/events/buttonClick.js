import { ajax, base, toast,cacheTools,print,withNav , promptBox} from 'nc-lightapp-front';
import GlobalStore from '../../../../public/components/GlobalStore';
let tableid = 'gl_brsetting';
// @withNav
export default function buttonClick(props, id) {
    let {querycondition}=this.state;
    let rowDatas,pk_brsetting,pk_unit,pk_currtype,link;
    let page = this;
    switch (id) {
        case 'refresha'://试算
            page.refresh();
            break;
        case 'print'://打印
            if(page.iscalculated){
                page.showPrintModal()
            }else{
                printAlert(page);
            }
            break;
        default:
        break;
    }
}

function printAlert(page){
    promptBox({
        color:'warning',
        content:page.state.json['20020TRYBL-000023']/*请先试算平衡*/,
        noCancelBtn:true,
    })    
}