import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';
import {createPage,high,base,ajax,deepClone,getBusinessInfo,getMultiLang,toast,createPageIcon } from 'nc-lightapp-front';
import PrintModal from '../../../public/components/printModal'
import { printRequire, mouldOutput } from '../../../public/components/printModal/events'
import './index.css';
import './index.less';

import { buttonClick, initTemplate} from './events';
const { Refer } = high;

const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButtonGroup:ButtonGroup,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCAffix,NCDiv,
NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCForm
} = base;
import { CheckboxItem,InputItem,} from '../../../public/components/FormItems';
const {  NCFormItem:FormItem } = NCForm;
import getDefaultAccountBook from '../../../public/components/getDefaultAccountBook.js';
import {FISelect} from '../../../public/components/base'
const Select = FISelect
const Option = Select.FIOption;
import TrialResultTable from '../../../public/components/TrialResultTable';
import createScript from '../../../public/components/uapRefer.js';
import HeaderArea from '../../../public/components/HeaderArea';
import {organizeCalculationResult} from '../../../openBalance/utils/utils';
const config={
    "isDataPowerEnable":'Y',
    "DataPowerOperationCode":'fi'
};
class TrialBalance extends Component {
	constructor(props) {
		super(props);
//每定义一个state 都要添加备注
		this.state = {
			dataList: [],
			resultData: {},
			listItem:{
			},
			reckonintData:[],
			currtypeList: [],
			currtype: '',      //选择的币种
			pk_accountingbook: '',
			book: '',
			yearPeriod: '', //会计期间
			versionDate: '',
			pk_units: [],
			year: '',
			month: '',
			accountingbook: '',
			isStartBUSecond: false, //是否启用二级核算单元
			isGlobal: false,
			isGroup: false,
			selectNum: -1,
			pk_accperiodscheme:'',
			selectedRowIndex: 0,
			showPrintModal: false,
			hasTally:"",
			json:{}
		}
		this.iscalculated = false; /*查询条件改变后是否试算平衡 */
	}

	//打印
	showPrintModal(){
		this.setState({
			showPrintModal: true
		})
	}
	refresh = () => {
		let {listItem}=this.state;
		let year=listItem.year_month.value.split('-')[0];
		let month=listItem.year_month.value.split('-')[1];
		let businessInfo = getBusinessInfo();
		let currrentDate = businessInfo.businessDate.split(' ')[0] ; 
		let trialData = {
			pk_accountingbook: listItem.pk_accountingbook.value,
			pk_currtype: listItem.pk_currency.value,
			year:year,
			month:month,
			hasTally:this.state.hasTally?this.state.hasTally:'Y',
			versionDate: currrentDate,
			iscalculate:'Y'
		};
		this.getTrial(trialData);
	}

	// 获取查询条件
	getPrintParams(){
		let {dataList,resultData,currtype,pk_accountingbook,pk_units,listItem,hasTally} = this.state;
		let printParams = {}
		
		let isResultData = JSON.stringify(resultData) == "{}"
		let tempArr = isResultData ? dataList[0] : resultData 
		let businessInfo = getBusinessInfo();
        let currrentDate = businessInfo.businessDate.split(' ')[0] ; 		
		if(tempArr){	
			let resultVal = tempArr.balanResult.value
			let resultTxt = (resultVal==='3') ? this.state.json['20020TRYBL-000004'] : this.state.json['20020TRYBL-000005']/* 国际化处理： 试算结果平衡,试算结果不平衡*/
			printParams.hasTally = this.state.hasTally?this.state.hasTally:'Y' //是否包含未记账
			printParams.pk_accountingbook = listItem.pk_accountingbook.value //核算账簿
			printParams.year = listItem.year_month.value.split('-')[0];  //会计年
			printParams.period = listItem.year_month.value.split('-')[1];  //会计期间
			printParams.month = listItem.year_month.value.split('-')[1];  //会计期间
			printParams.versionDate = currrentDate //版本日期
			printParams.pk_currtype = listItem.pk_currency.value //币种	
			printParams.pk_unit = tempArr.pk_unit // 二级账簿
			printParams.occurmargin = this.getDiffStr(tempArr.fi.incurDiff, tempArr.pri&&tempArr.pri.incurDiff)  //发生额差额
			printParams.balancemargin = this.getDiffStr(tempArr.fi.balanDiff, tempArr.pri&&tempArr.pri.balanDiff) //余额差额
			printParams.result = resultTxt // 试算结果
			printParams.currname = listItem.pk_currency.value, //币种		pk_currency
			printParams.iscalculate='Y'
		}
		return printParams	
	}

	/**
	 * 生成差额字符串
	 */
	getDiffStr(fiDiff, priDiff){
		if((!fiDiff || fiDiff == 0.0) && (!priDiff || priDiff == 0.0))
			return '';
		let fiLable = this.state.json['20020TRYBL-000021'] /*财务科目*/;
		let priLable = this.state.json['20020TRYBL-000022'] /*预算科目*/;
		let str = '';
		if(fiDiff && fiDiff != 0.0){
			str = `${fiLable}：${fiDiff}`;
		}
		if(priDiff && priDiff != 0.0){
			str = `${str}    ${priLable}：${priDiff}`;
		}
		return str;
	}

	//打印框 确认
	handlePrint(data) {
		let printUrl = '/nccloud/gl/voucher/checkbalanceprint.do'
		let printParams = this.getPrintParams()
		let appcode = '20020TRYBL'
		let { ctemplate, nodekey } = data
		printParams.queryvo = data
		
		printRequire(printUrl, appcode, nodekey, ctemplate, printParams)
		this.setState({
			showPrintModal: false
		});
	}
	getSelectedDataFunc = data => {
	 	
	}

	goOpenBalance() {
		window.location.href = '../openBalance/index.html';
	}
	componentWillMount(){
		
		let {listItem,pk_accperiodscheme,isStartBUSecond}=this.state;
		let appcode=this.props.getSearchParam("c");
		let defaultAccouontBook={};
		let callback= (json) =>{
			getDefaultAccountBook(appcode).then((defaultAccouontBook)=>{
				let startPeriod='';
				if(defaultAccouontBook.value){
					this.getCurrtypeList();//请求币种信息
					let url = '/nccloud/gl/voucher/queryBookCombineInfo.do';
					let pk_accpont = {"pk_accountingbook":defaultAccouontBook.value};
					
					ajax({
						url:url,
						data:pk_accpont,
						async:false,
						success: function(response){
							const { success } = response;
							//渲染已有账表数据遮罩
							if (success) {
								if(response.data){
									pk_accperiodscheme=response.data.pk_accperiodscheme;
									isStartBUSecond=response.data.isShowUnit,
									startPeriod= response.data.startPeriod;
								}
							}   
						}
					});
				} 
				const headData=[
					{itemName:json['20020TRYBL-000000'],itemType:'refer',itemKey:'pk_accountingbook',/* 国际化处理： 核算账簿*/
					config:{refCode:"uapbd/refer/org/AccountBookTreeRef"}},
					{itemName:json['20020TRYBL-000001'],itemType:'refer',itemKey:'year_month',/* 国际化处理： 会计期间*/
					config:{refCode:"uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef"}},
					{itemName:json['20020TRYBL-000002'],itemType:'select',itemKey:'pk_currency',/* 国际化处理： 币种*/
						itemChild:[]
					},
					{itemName:json['20020TRYBL-000003'],itemType:'checkbox',itemKey:'hasTally',/* 国际化处理： 包含未记账凭证*/
						itemChild:[{
							label: json['20020TRYBL-000003'],/* 国际化处理： 包含未记账凭证*/
							checked: false
						}]
					},
				]
				headData.forEach((item,i)=>{
					let  key;
					if(item.itemType=='refer'){
						if(item.itemKey=='pk_units'){
							key=[{
								display:'',
								value:''
							}]
						}else if(item.itemKey=='pk_accountingbook'){
							key={
								display:defaultAccouontBook.display,
								value:defaultAccouontBook.value
							}
						}else if(item.itemKey=='year_month'){
							key={
								display:startPeriod,
								value:startPeriod
							}
						}else{
							key={
								display:'',
								value:''
							}
						} 
					}else if(item.itemType=='select'){//下拉框赋初始值
						key={
							value:json['20020TRYBL-000006']/* 国际化处理： 组织本币*/
						}
					}else if(item.itemType=='Dbselect'||item.itemType=='radio'){//下拉框赋初始值
						key={
							value:item.itemChild[0].value
						}
					}else{
						key={
							value:''
						}
						if(item.itemType=='date'||item.itemType=='Dbdate'){
						
							key={
							//    value:moment().format("YYYY-MM-DD")
							value:''
							}
						}
					}
					if(item.itemType=='Dbdate'||item.itemType=='DbtextInput'){
					item.itemKey.map((k,index)=>{
						let name= k;
						listItem[name]=key
					});
					}else{
						let name= item.itemKey;
						listItem[name]=key
					}
				
				})
				this.setState({
					listItem,pk_accperiodscheme,isStartBUSecond
				})
			})
            this.setState({
              json:json,
                columns_reportSum:this.columns_reportSum,
				},()=>{
				initTemplate.call(this, this.props);
            })
        }
		getMultiLang({moduleId:'20020TRYBL',domainName:'gl',currentLocale:'simpchn',callback});
		


    }
	componentDidMount() {
	}

	getCurrtypeList = () => {
		let self = this;
		let url = '/nccloud/gl/voucher/queryAllCurrtype.do';
		let data = {
			showGlobalCurr: (this.state.isGlobal ? '1' : '0'),
			showGroupCurr: (this.state.isGroup ? '1' : '0'),
		};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		      
		        const { data, error, success } = res;
		        if(success){
					data.map((item,index)=>{
						item.lable=item.name;
						item.value=item.pk_currtype;
					})
		        	self.setState({
		        		currtypeList: data,
		        	})
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});
	}

	getTrial = (trialData) => {
		let self = this;
		let url = '/nccloud/gl/voucher/calculation.do';
		let data = trialData;
		let {resultData}=self.state;
		resultData={};
		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		        const { data, error, success } = res;
		        if(success){
		        	let dataList = deepClone(data);
		        	organizeCalculationResult(dataList);
		        	self.setState({
						dataList: dataList,
						resultData,
						selectedRowIndex: 0,
		        	})
		            self.updateIsCaculated(true);
		        }else {
		        	toast({ content: message.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});
	}


	//币种切换onchange事件
	handleCurrtypeChange(value) {
		let {listItem}=this.state;
		let year=listItem.year_month.value.split('-')[0];
		let month=listItem.year_month.value.split('-')[1];
		let trialData = {
			pk_accountingbook:listItem.pk_accountingbook.value,
			pk_currtype: value,
			year: year,
			month: month
		}

		this.getTrial(trialData);
	}
	getHeadData=()=>{
    	let self=this;
	   	let {
			listItem,reckonintData,isStartBUSecond,isGlobal,isGroup,currtype,currtypeList,
			accountingbook,pk_accperiodscheme}=self.state;    
		let appcode=self.props.getSearchParam("c");
		const headData=[
			{itemName:self.state.json['20020TRYBL-000000'],itemType:'refer',itemKey:'pk_accountingbook',/* 国际化处理： 核算账簿*/
			config:{refCode:"uapbd/refer/org/AccountBookTreeRef"}},
			{itemName:self.state.json['20020TRYBL-000001'],itemType:'refer',itemKey:'year_month',/* 国际化处理： 会计期间*/
			config:{refCode:"uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef"}},
			{itemName:self.state.json['20020TRYBL-000002'],itemType:'select',itemKey:'pk_currency',/* 国际化处理： 币种*/
				itemChild:[]
			},
			{itemName:self.state.json['20020TRYBL-000003'],itemType:'checkbox',itemKey:'hasTally',/* 国际化处理： 包含未记账凭证*/
				itemChild:[{
					label: self.state.json['20020TRYBL-000003'],/* 国际化处理： 包含未记账凭证*/
					checked: false
				}]
			},
		]
		if(JSON.stringify(listItem)!='{}'){
			return (
				headData.map((item,i)=>{
					let defValue=listItem[item.itemKey].value;
					switch (item.itemType) {
						case 'refer':
							let referUrl= item.config.refCode+'/index.js';
							let defaultValue = { refname: listItem[item.itemKey].display, refpk: listItem[item.itemKey].value };  
							if(!self.state[item.itemKey]){
								{createScript.call(self,referUrl,item.itemKey)}
								return <div />
							}else{
								if(item.itemKey=='pk_accountingbook'){
									return (
										<FormItem
											inline={true}
											showMast={true}
											labelXs={1} labelSm={1} labelMd={1}
											xs={2} md={2} sm={2}
											labelName={item.itemName}
											isRequire={true}
											className="accountingbook"
											method="change"
											>
											{self.state[item.itemKey]?(self.state[item.itemKey])(
												{
													fieldid:item.itemKey,
													value:defaultValue,
													isMultiSelectedEnabled:false,
													disabledDataShow:true,
													queryCondition:() => {
														return Object.assign({
															"TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
															"appcode":appcode
														},config)
													},
													onChange:(v)=>{
														if(v.refpk){
															if(item.itemKey=='pk_accountingbook'){
																let url = '/nccloud/gl/voucher/queryBookCombineInfo.do';
																let pk_accpont = {"pk_accountingbook":v.refpk}
																ajax({
																	url:url,
																	data:pk_accpont,
																	success: function(response){
																		const { success } = response;
																		//渲染已有账表数据遮罩
																		if (success) {
																			if(response.data){
																				if(response.data.NC001){// 不是false的话接可以选择'集团本币'，解除禁止
																					isGroup = true;
																				}else{
																					isGroup = false;
																				}
																				if(response.data.NC002){// 不是false的话接可以选择'全局本币'，解除禁止
																					isGlobal = true;
																				}else{
																					isGlobal = false;
																				}
																				pk_accperiodscheme=response.data.pk_accperiodscheme;
																				listItem['year_month'].value = response.data.startPeriod;
																				listItem['year_month'].display = response.data.startPeriod;
																			}
																			self.setState({
																				isStartBUSecond:response.data.isShowUnit,
																				isGlobal,isGroup,pk_accperiodscheme
																			},()=>{
																				self.getCurrtypeList()
																			})
																		}   
																	}
																});
															}
															listItem[item.itemKey].value = v.refpk;
															listItem[item.itemKey].display = v.refname;
															accountingbook=v.refpk;
															this.setState({
																listItem,
																accountingbook
															})
														}
														this.updateIsCaculated(false);
													}
												}
											):<div />}
									</FormItem>);
								}else{
									return(
										<FormItem
											inline={true}
											showMast={true}
											labelXs={1} labelSm={1} labelMd={1}
											xs={2} md={2} sm={2}
											labelName={item.itemName}
											isRequire={true}
											method="change"
											className="year_month"
										>
									{self.state[item.itemKey]?(self.state[item.itemKey])(
										{
											fieldid:item.itemKey,
											value:defaultValue,
											queryCondition:() => {
												if(item.itemKey=='year_month'){
													return {
														"pk_accperiodscheme":pk_accperiodscheme,
														// "GridRefActionExt":'nccloud.web.gl.ref.FilterAdjustPeriodRefSqlBuilder'
													}
												}else{
													return {
														//"pk_accountingbook": self.state.pk_accountingbook.value
													}
												}
												},
											onChange:(v)=>{                                           
													listItem[item.itemKey].value = v.refname
													listItem[item.itemKey].display = v.refname
													this.setState({
														listItem
													})
													this.updateIsCaculated(false);
											}
											}
										):<div/>}
									</FormItem>)
								}
							}
							break;
						case 'select':
							return(
								<FormItem
								inline={true}
								showMast={false}
								labelXs={1} labelSm={1} labelMd={1}
								xs={2} md={2} sm={2}
								labelName={item.itemName}
								isRequire={true}
								method="change"
								className="pk_currenpay"
								>
								<Select
									fieldid={item.itemKey}
									size="lg"
									value={listItem[item.itemKey].value}
									style={{ width: 150}}
									onChange={
										(v)=>{
											listItem[item.itemKey].value = v
											currtype=v;
											this.setState({
												listItem,currtype
											},()=>{
												this.handleCurrtypeChange.bind(this,v)
											})
											this.updateIsCaculated(false);
										}									
									}	
									>
									{this.state.currtypeList.map((item, index) => {
										return <Option value={item.pk_currtype} key={item.pk_currtype} >{item.name}</Option>
									} )}
								
								</Select>
							</FormItem>
							)
						case 'textInput':
							return(
							<FormItem
								inline={true}
								showMast={true}
								labelXs={2} labelSm={2} labelMd={2}
								xs={2} md={2} sm={2}
								labelName={item.itemName}
								isRequire={true}
								method="change"
								>
								<InputItem
									fieldid={item.itemKey}
									//isViewMode
									//disabled={true}
									name={item.itemKey}
									type="customer"
									defaultValue={defValue}
								/>
							</FormItem>
							)
						case 'checkbox':
								return(
									<FormItem
									inline={true}
									showMast={false}
									xs={6}  md={6} sm={6}
									// labelName={item.itemName}
									// isRequire={true}
									className='checkboxStyle'
									method="change"
								>
									<CheckboxItem name={item.itemKey} 
									//defaultValue={this.state.periodloan.value} 
										boxs = {
											() => {
												return (item.itemChild) 
											}
										}
										onChange={(v)=>{
											listItem[item.itemKey].value = (v[0].checked==true)?'Y':'N';
											this.setState({
												listItem,
												hasTally:(v[0].checked==true)?'N':'Y'
											})
											this.updateIsCaculated(false);
										}}
									/>
								</FormItem>
							)
						default:
						break;
					}
				})
				
			)  
		}else{
			return (<div/>)
		}
	}
	classItemClick(item, index){
		this.setState({
			resultData: item,
			selectedRowIndex: index
		});
	}

	updateIsCaculated = (iscalculated) =>{
		let page = this;
		page.iscalculated = iscalculated;
	}

	render() {
		


		let columns = [
			{ title: this.state.json['20020TRYBL-000007'], dataIndex: "name", key: "name", width: 100 },/* 国际化处理： 业务单元*/
			{ title: this.state.json['20020TRYBL-000008'], dataIndex: "balanResult.display", key: "balanResult.display", width: 100 },/* 国际化处理： 结果*/
		];
		let data = this.state.dataList;
		let {resultData,selectedRowIndex} = this.state
		let isResultData = JSON.stringify(resultData) == "{}"
		let defaultValue = isResultData ? data[0] : resultData 
	
		// let data = this.state.dataList;
		let tableShow;
		if (this.state.isStartBUSecond) {
			tableShow = 'table-marginleft-show';
		} else {
			tableShow = 'table-marginleft-hide';
		}


		let columns2 = [
			{
				title: (<div fieldid="accountName" className="mergecells">{this.state.json['20020TRYBL-000009']}</div>),
				dataIndex: "accountName",
				key: "accountName",
				width: 80,
				render: (text, record, index) => <div class='indexCode' fieldid="accountName">{!text?<span>&nbsp;</span>:text}</div>
			},/* 国际化处理： 科目类型*/
			{
				title: (<div fieldid="dblDebit" className="mergecells">{this.state.json['20020TRYBL-000010']}</div>),
				dataIndex: "incur.dblDebit",
				key: "incur.dblDebit",
				width: 60,
				render: (text, record, index) => <div class='indexCode' fieldid="dblDebit">{!text?<span>&nbsp;</span>:text}</div>
			},/* 国际化处理： 借方*/
			{
				title: (<div fieldid="dblCredit" className="mergecells">{this.state.json['20020TRYBL-000011']}</div>),
				dataIndex: "incur.dblCredit",
				key: "incur.dblCredit",
				width: 60,
				render: (text, record, index) => <div class='indexCode' fieldid="dblCredit">{!text?<span>&nbsp;</span>:text}</div>
			},/* 国际化处理： 贷方*/
			{
				title: (<div fieldid="balanOrient" className="mergecells">{this.state.json['20020TRYBL-000012']}</div>),
				dataIndex: "balanOrient.display",
				key: "balanOrient.display",
				width: 60,
				render: (text, record, index) => <div class='indexCode' fieldid="balanOrient">{!text?<span>&nbsp;</span>:text}</div>
			},/* 国际化处理： 方向*/
			{
				title: (<div fieldid="balancedisp" className="mergecells">{this.state.json['20020TRYBL-000013']}</div>),
				dataIndex: "balancedisp",
				key: "balancedisp",
				width: 100,
				render: (text, record, index) => <div class='indexCode' fieldid="balancedisp">{!text?<span>&nbsp;</span>:text}</div>
			},/* 国际化处理： 余额*/
		];

		let tableInner = this.state.dataList.map((item, index) => {
			//let that=this
			let triTrue = <div className="triTrue">{this.state.json['20020TRYBL-000004']}</div>;/* 国际化处理： 试算结果平衡*/
			let triFalse = <div className="triFalse">{this.state.json['20020TRYBL-000005']}</div>/* 国际化处理： 试算结果不平衡*/

			{/*右表*/ }
			return <div className="table-all-left">
				{item.balanResult.display == this.state.json['20020TRYBL-000014'] ? triTrue : triFalse}{/* 国际化处理： 平*/}
				<div className='balanceColor'>
					<span>{this.state.json['20020TRYBL-000015'] + item.name}</span>{/* 国际化处理： 业务单元：*/}
					<span className="incurDiff">{this.state.json['20020TRYBL-000016']}: {item.incurDiff} </span>{/* 国际化处理： 借贷发生额合计差额*/}
					<span className="balanDiff">{this.state.json['20020TRYBL-000017']}：{item.balanDiff}</span>{/* 国际化处理： 借贷余额合计差额*/}

				</div>
				<div className="table-inner">
					<div fieldid="leftVOs_table">
						<Table
							className="table-left"
							columns={columns2}
							bordered
							data={item.leftVOs}
							scroll={{ x: 400, }}
						/>
					</div>
					<div fieldid="rightVOs_table">
						<Table
							className="table-right"
							columns={columns2}
							bordered
							data={item.rightVOs}
							scroll={{ x: 400, }}
						/>
					</div>

				</div>
			</div>


		})

		let newTableInner = tableInner;
		if (this.state.selectNum >= 0) {
			newTableInner = tableInner[this.state.selectNum]
		}

		return (

			<div id="trialBalance" className="nc-bill-list nc-theme-area-bgc nc-theme-form-label-c">
				<HeaderArea
					title={this.state.json['20020TRYBL-000018']}/* 国际化处理： 试算平衡*/
					btnContent={
						this.props.button.createButtonApp({
							area: 'button_area',
							onButtonClick: buttonClick.bind(this),
							// popContainer: document.querySelector('.header-button-area')
						})
					}
				/> 
				{/* //表头 */}
				<div className="nc-bill-search-area">
				<NCDiv fieldid="trialBalance" areaCode={NCDiv.config.FORM}>
					<NCForm useRow={true}
						submitAreaClassName='classArea'
						showSubmit={false}　>
						{this.getHeadData()}
					</NCForm>
				</NCDiv>
				</div>
				<div className="table_all nc-theme-area-split-bc" style={{minHeight: window.innerHeight - 115 + 'px'}}>
					{/*左表*/}
					<div className={this.state.isStartBUSecond ? 'table-left' : 'none'}>
					
					<div className="left-header">
						{this.state.json['20020TRYBL-000019']}{/* 国际化处理： 业务单元结果*/}
					</div>
					<div className="left-body">
						{data.length != 0 &&
							data.map((item, index) => {
								return (
									<div
										key={item.name}
										class={
											selectedRowIndex === index ? (
												'class-item selected-item'
											) : (
												'class-item'
											)
										}
										onClick={this.classItemClick.bind(this, item, index)}
									>
										<div className="class-name">{item.name}</div>
										<div className={(item.balanResult.display!==3) ? 'light_red result_txt' : 'result_txt'}>{item.balanResult.display}</div>{/* 国际化处理： 平*/}
										
									</div>
								);
							})}
					</div>
				</div>

					{/*右表*/}
					{/* <div className="right_body"> */}
					<div className={this.state.isStartBUSecond ? 'right_body' : 'right_tablea'}>
						{defaultValue&&defaultValue.fi?(
							<TrialResultTable 
								typeName={this.state.json['20020TRYBL-000021'] /*财务科目*/}
								data={defaultValue.fi}
								showUnits = {this.state.isStartBUSecond}
							/>
						):<div/>}
						{defaultValue&&defaultValue.pri?(
							<TrialResultTable
								typeName={this.state.json['20020TRYBL-000022'] /*预算科目*/}
								data={defaultValue.pri}
								showUnits = {this.state.isStartBUSecond}
							/>
						):<div/>}
						{!defaultValue || (!defaultValue.fi&&!defaultValue.pri)?
							(<div className="no_data">
								<Icon type='uf-i-c-2' className="mark_inco_i"/>
								<span>{this.state.json['20020TRYBL-000020']}</span>{/* 国际化处理： 暂无数据*/}
							</div>):<div/>
						}
					</div>
	
					


				</div>
				<PrintModal
					noRadio={true}
					noCheckBox={true}
					appcode='20020TRYBL'
					visible={this.state.showPrintModal}
					handlePrint={this.handlePrint.bind(this)}
					handleCancel={() => {
						this.setState({
							showPrintModal: false
						})
					}}
				/>


			</div>

			)
									
	}
}
TrialBalance = createPage({
  //  initTemplate: initTemplate
    // mutiLangCode: '2002'
})(TrialBalance);
ReactDOM.render(<TrialBalance />, document.querySelector("#app"));
