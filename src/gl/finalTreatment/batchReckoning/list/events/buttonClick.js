import { ajax, base, toast,cacheTools,print,withNav } from 'nc-lightapp-front';
import GlobalStore from '../../../../public/components/GlobalStore';
let tableid = 'gl_brsetting';


// @withNav
export default function buttonClick(props, id) {
    let {querycondition}=this.state;
    let rowDatas,pk_brsetting,pk_unit,pk_currtype,link;
    switch (id) {
        case 'BatchPrecloseAcc':
        //结账
        this.handleEndAccount();
        break;

        // 取消结账
        case 'BatchCloseAcc':
            this.handleUnendAccount();
            break;
            
        // 结账检查报告
        case 'BatchAntiPrecloseAcc':
            this.handleEndAccountReport();
            break;

        // 刷新
        case 'Refresh':
            this.handleRefresh();
            break;
        default:
        break;
    }
}
