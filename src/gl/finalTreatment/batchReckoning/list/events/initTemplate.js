import {
    createPage,
    ajax,
    base,
    toast,
    cacheTools,
    print
  } from "nc-lightapp-front";
  let { NCPopconfirm, NCIcon } = base;
  
  let searchId = "busirecon_query";
  const pageCode = "2002BATCHRECONPAGE";
  export default function(props) {
    let appcode = props.getSearchParam("c");
    ajax({
      url: "/nccloud/platform/appregister/queryallbtns.do",
      data: {
        pagecode: pageCode,
        appcode: appcode //小应用id
      },
      async: false,
      success: function(res) {
        if (res.data) {
                  let button = res.data;
                  
          props.button.setButtons(button, () => {
            const disabledButtonsArr = [
              'BatchPrecloseAcc',
              'BatchCloseAcc',
              'BatchAntiPrecloseAcc',
              'Refresh'
              ];
              // 先注释掉，别影响功能
              props.button.setButtonDisabled(disabledButtonsArr, true);
          });
        }
      }
    });
  }
  
