import React, { Component } from 'react';
import {high,base,ajax ,getMultiLang} from 'nc-lightapp-front';
const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCPanel:Panel,NCModal:Modal,NCForm,NCButtonGroup:ButtonGroup,NCDiv
} = base;
const {  NCFormItem:FormItem } = NCForm;
import '../index.less'
export default class ReportModal extends Component{
    constructor(props){
        super(props)
        this.columns_report = [
            {
                title: "",
                dataIndex: "strRowOne",
                key: "strRowOne",
                width: 100,
                render:(text, record, index)=>{
                    let {reportTableData}=this.state;
                    let redFlag=false;
                    for(let k in reportTableData.reportDetailFont){
                        if(k==text){
                            redFlag=true;
                        }
                    }
                    if(redFlag){
                        return(
                            <span className="fontColor"><pre>{text}</pre></span>
                        )
                    }else{
                        return(
                            <span><pre>{text}</pre></span>
                        )
                    }
                    
                }
            },
            {
                title: "",
                dataIndex: "strRowTwo",
                key: "strRowTwo",
                width: 100,
                render:(text, record, index)=>{
                    let {reportTableData}=this.state;
                    let redFlag=false;
                    for(let k in reportTableData.reportDetailFont){
                        if(k==text){
                            redFlag=true;
                        }
                    }
                    if(redFlag){
                        return(
                            <span className="fontColor"><pre>{text}</pre></span>
                        )
                    }else{
                        return(
                            <span><pre>{text}</pre></span>
                        )
                    }
                    
                }
            }
        ]
        this.state={
            json:{},
            MessageData:[],//结账信息
            reportTableData:[],
            reportTableDataSum:[],
            reportTableTitle:'',
            fontStr:{},
            columns_message:[],
            columns_report:this.columns_report,
           // columns_reportSum:this.columns_reportSum,
            sumOrDetail:'sum'//默认像是汇总table数据
        }
    }
    componentWillMount(){
        let callback= (json) =>{
            this.columns_message=[
                {
                    title:json['2002BATCHRECON-000002'],/* 国际化处理： 编码*/
                    dataIndex: "bookcode",
                    key: "bookcode",
                    width: 100,
                },
                {
                    title:json['2002BATCHRECON-000012'],/* 国际化处理： 主体账簿*/
                    dataIndex: "bookname",
                    key: "bookname",
                    width: 100,
                },
                {
                    title:json['2002BATCHRECON-000013'],/* 国际化处理： 处理结果*/
                    dataIndex: "message",
                    key: "message",
                    width: 100,
                }
            ]
            this.setState({
              json:json,
              columns_message:this.columns_message,
            },()=>{
             // initTemplate.call(this, this.props);
            })
          }
          getMultiLang({moduleId:'2002BATCHRECON',domainName:'gl',currentLocale:'simpchn',callback});
        }
    componentWillReceiveProps(nextProp){
        let {MessageData,reportTableDataSum,reportTableData,reportTableTitle}=this.state;
        let{reckoningMessageData}=nextProp;
        MessageData=reckoningMessageData;
        this.setState({
            MessageData
        })
    }
    componentDidMount(){

    }
    //关闭
    close() {
        this.props.handleClose();
    }
    //返回
    comeBack=()=>{
        let {sumOrDetail}=this.state;
        sumOrDetail='sum';
        this.setState({
            sumOrDetail
        })
    }
    //查看详情
    queryReportDetail=()=>{
        let {sumOrDetail}=this.state;
        sumOrDetail='detail';
        this.setState({
            sumOrDetail
        })
    }
    //打印
    confirm=()=>{
        this.props.handlePrintReport();
    }
    render(){
        let {MessageData,sumOrDetail,reportTableDataSum,reportTableData,reportTableTitle,columns_message,columns_report}=this.state;
        let{showOrHide}=this.props;
        let columns_reportSum = [
            {
                title: this.state.json['2002BATCHRECON-000014'],/* 国际化处理： 检查项*/
                dataIndex: "strRowOne",
                key: "strRowOne",
                width: 100,
                render:(text, record, index)=>{
                    let {reportTableDataSum}=this.state;
                    let redFlag=false;
                    for(let k in reportTableDataSum.reportSumFont){
                        if(k==text){
                            redFlag=true;
                        }
                    }
                    if(redFlag){
                        return(
                            <span className="fontColor"><pre>{text}</pre></span>
                        )
                    }else{
                        return(
                            <span><pre>{text}</pre></span>
                        )
                    }
                    
                }
            },
            {
                title: this.state.json['2002BATCHRECON-000015'],/* 国际化处理： 是否通过*/
                dataIndex: "strRowTwo",
                key: "strRowTwo",
                width: 100,
                render:(text, record, index)=>{
                    let {reportTableDataSum}=this.state;
                    let redFlag=false;
                    for(let k in reportTableDataSum.reportSumFont){
                        if(k==text){
                            redFlag=true;
                        }
                    }
                    if(redFlag){
                        return(
                            <span className="fontColor"><pre>{text}</pre></span>
                        )
                    }else{
                        return(
                            <span><pre>{text}</pre></span>
                        )
                    }
                    
                }
            },
            {
                title: this.state.json['2002BATCHRECON-000016'],/* 国际化处理： 说明*/
                dataIndex: "strRowThree",
                key: "strRowThree",
                width: 100,
                render:(text, record, index)=>{
                    let {reportTableDataSum}=this.state;
                    let redFlag=false;
                    for(let k in reportTableDataSum.reportSumFont){
                        if(k==text){
                            redFlag=true;
                        }
                    }
                    if(redFlag){
                        return(
                            <span className="fontColor"><pre>{text}</pre></span>
                        )
                    }else{
                        return(
                            <span><pre>{text}</pre></span>
                        )
                    }
                    
                }
            }
        ]
        return(
            <div id='finalTreatment_report'>
            <Modal
                    className={'msg-modal mdalsecend'}
                    show={showOrHide}
                    // backdrop={ this.state.modalDropup }
                    onHide={ this.close.bind(this) }
                    animation={true}
                    >
                    <Modal.Header closeButton>
                        <Modal.Title >{this.state.json['2002BATCHRECON-000017']}</Modal.Title>{/* 国际化处理： 结账信息*/}
                    </Modal.Header >
                    <Modal.Body >
                    <NCDiv fieldid="reportTable" areaCode={NCDiv.config.TableCom}>
                    <Table 
                        columns={columns_message} 
                        bordered 
                        data={MessageData}
                        scroll={{ x: columns_message.length > 8 ? (100+(columns_message.length-8)*15)+"%": '100%', y: 320 }}
                    />
                    </NCDiv>
                    </Modal.Body>
                    <Modal.Footer>
                        {/* <Button colors="primary" onClick={ this.confirm }> 确定 </Button> */}
                        {/* <Button onClick={ this.close }> 关闭 </Button> */}
                    </Modal.Footer>
                </Modal>
                
            </div>
        )
    }
}
