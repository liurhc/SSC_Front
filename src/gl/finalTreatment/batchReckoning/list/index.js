import React, { Component } from 'react';
import { high, base, ajax, deepClone, createPage, getBusinessInfo, getMultiLang, createPageIcon } from 'nc-lightapp-front';
const { NCFormControl: FormControl, NCDatePicker: DatePicker, NCButton: Button, NCRadio: Radio, NCBreadcrumb: Breadcrumb,
    NCRow: Row, NCCol: Col, NCTree: Tree, NCMessage: Message, NCIcon: Icon, NCLoading: Loading, NCTable: Table, NCSelect: Select,
    NCCheckbox: Checkbox, NCNumber, AutoComplete, NCDropdown: Dropdown, NCPanel: Panel, NCForm, NCButtonGroup: ButtonGroup,NCDiv
} = base;
const { NCFormItem: FormItem } = NCForm;
import { toast } from '../../../public/components/utils.js';
import PrintModal from '../../../public/components/printModal';
import { printRequire, mouldOutput } from '../../../public/components/printModal/events';
import { SelectItem, InputItem } from '../../../public/components/FormItems';
import createScript from '../../../public/components/uapRefer.js';
import getDefaultAccountBook from '../../../public/components/getDefaultAccountBook.js';
import ReportModal from './reportModal/reportModal';
import ReckoningOrUnreckonigModal from './reckoningOrUnreckonigModal/index.js';
import { buttonClick, initTemplate } from './events';
import HeaderArea from '../../../public/components/HeaderArea';
import { getTableHeight } from '../../../public/common/method.js';
import './index.less';
const config = {
    "isDataPowerEnable": 'Y',
    "DataPowerOperationCode": 'fi'
};
const defaultProps12 = {
    prefixCls: "bee-table",
    multiSelect: {
        type: "checkbox",
        param: "key"
    }
};
class Reckon extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json: {},
            appcode: this.props.getUrlParam('c') || this.props.getSearchParam('c'),
            selectedRecord: '',//复制的key
            getrowkey: '',//获取指定行的Key
            selectedData: {},//选中的数据
            ShowHideFlag: false,//查询条件对话框
            ShowHideMessge: false,//结账报告
            queryCondition: {},//查询条件
            accountingbook: { display: '', value: '' },
            reportDataSum: [],//结账汇总数据
            reportData: [],//结账详情数据
            listItem: {},
            checkedAll: false,
            checkedArray: [],
            reportSumTitle: '',//汇总报告title
            reportSumFont: {},//汇总报告字体
            reportTitle: '',//报告title
            reportFont: {},
            refersh: false,//刷新
            columnsVerify: [],
            reckonintData: [],
            verBalanceVOData: {},
            queryDataObj: {},//保存查询条件
            resourceData_sum: [],//查询出来的历史数据
            resourceColumns_sum: [],//查询出来的历史数据列字段
        }
    }
    componentWillMount() {
        let { listItem } = this.state;
        let appcode = this.props.getSearchParam("c");
        let callback = (json) => {
            const headData = [
                {
                    itemName: '', itemType: 'refer', itemKey: 'pk_accperiodscheme',
                    config: { refCode: "uapbd/refer/pubinfo/AccPeriodSchemeDefaultTreeRef" }
                },
                {
                    itemName: '', itemType: 'refer', itemKey: 'period',
                    config: { refCode: "uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef" }
                },
                {
                    itemName: '', itemType: 'refer', itemKey: 'pk_accountingbook',
                    config: { refCode: "uapbd/refer/org/AccountBookTreeRef" }
                },
                {
                    itemName: '', itemType: 'select', itemKey: 'state',
                    itemChild: [{
                        label: json['2002BATCHRECON-000000'],/* 国际化处理： 可结账*/
                        value: '1'
                    }, {
                        label: json['2002BATCHRECON-000001'],/* 国际化处理： 可取消结账*/
                        value: '2'
                    }]
                }
            ]
            this.Defaultcolumns = [
                {
                    title: (<div fieldid="bookcode" className="mergecells">{json['2002BATCHRECON-000002']}</div>),/* 国际化处理： 编码*/
                    dataIndex: "bookcode",
                    key: "bookcode",
                    width: 100,
                    render: (text, record, index) => {
                        return <div fieldid="bookcode">{!text?<span>&nbsp;</span>:text}</div>
                    }
                },
                {
                    title: (<div fieldid="bookname" className="mergecells">{json['2002BATCHRECON-000003']}</div>),/* 国际化处理： 名称*/
                    dataIndex: "bookname",
                    key: "bookname",
                    width: 100,
                    render: (text, record, index) => {
                        return <div fieldid="bookname">{!text?<span>&nbsp;</span>:text}</div>
                    }
                },
                {
                    title: (<div fieldid="endaccuser" className="mergecells">{json['2002BATCHRECON-000004']}</div>),/* 国际化处理： 结账人*/
                    dataIndex: "endaccuser",
                    key: "endaccuser",
                    width: 200,
                    render: (text, record, index) => {
                        return <div fieldid="endaccuser">{!text?<span>&nbsp;</span>:text}</div>
                    }
                },
                {
                    title: (<div fieldid="endacctime" className="mergecells">{json['2002BATCHRECON-000005']}</div>),/* 国际化处理： 结账时间*/
                    dataIndex: "endacctime",
                    key: "endacctime",
                    width: 200,
                    render: (text, record, index) => {
                        return <div fieldid="endacctime">{!text?<span>&nbsp;</span>:text}</div>
                    }
                },
                {
                    title: (<div fieldid="unendaccuser" className="mergecells">{json['2002BATCHRECON-000006']}</div>),/* 国际化处理： 取消结账人*/
                    dataIndex: "unendaccuser",
                    key: "unendaccuser",
                    width: 200,
                    render: (text, record, index) => {
                        return <div fieldid="unendaccuser">{!text?<span>&nbsp;</span>:text}</div>
                    }
                },
                {
                    title: (<div fieldid="unendacctime" className="mergecells">{json['2002BATCHRECON-000007']}</div>),/* 国际化处理： 取消结账时间*/
                    dataIndex: "unendacctime",
                    key: "unendacctime",
                    width: 200,
                    render: (text, record, index) => {
                        return <div fieldid="unendacctime">{!text?<span>&nbsp;</span>:text}</div>
                    }
                }
    
            ];
            getDefaultAccountBook(appcode).then((defaultAccouontBook) => {
                headData.forEach((item, i) => {
                    let key;
                    if (item.itemType == 'refer') {
                        if (item.itemKey == 'pk_accountingbook') {
                            key = [{
                                display: defaultAccouontBook.display,
                                value: defaultAccouontBook.value
                            }]
                        } else {
                            key = {
                                display: '',
                                value: ''
                            }
                        }
                    } else if (item.itemType == 'select' || item.itemType == 'Dbselect' || item.itemType == 'radio') {//下拉框赋初始值
                        key = {
                            value: item.itemChild[0].value
                        }
                    } else {
                        key = {
                            value: ''
                        }
                        if (item.itemType == 'date' || item.itemType == 'Dbdate') {
                            key = {
                                value: ''
                            }
                        }
                    }
                    if (item.itemType == 'Dbdate' || item.itemType == 'DbtextInput') {
                        item.itemKey.map((k, index) => {
                            let name = k;
                            listItem[name] = key
                        });
                    } else {
                        let name = item.itemKey;
                        listItem[name] = key
                    }
                })
                this.setState({
                    listItem
                })
            })
            this.setState({
                json: json,
            }, () => {
                initTemplate.call(this, this.props);
            })
        }
        getMultiLang({ moduleId: '2002BATCHRECON', domainName: 'gl', currentLocale: 'simpchn', callback });
    }
    //处理width
    changeWidth(arr) {
        arr.map((item, index) => {
            if (item.children) {
                this.changeWidth(item.children);
            } else {
                item['width'] = 150;
            }
        })
        return arr;
    }
    //处理数据
    dealWithData = (data) => {
        let childData = deepClone(data);
        for (let k in childData) {
            if (k == 'ass' || k == 'acccode' || k == 'accname' || k == 'curName') {

            } else if (k == 'pk_accountingbook' || k == 'pk_accasoa' || k == 'pk_units' || k == 'pk_currency') {
                if (childData[k].length > 0) {
                    if (childData[k][0].value == "") {
                        childData[k] = null;
                    } else {
                        childData[k].map((item, index) => {
                            //childData[k].push(item.value);
                            childData[k][index] = item.value;
                        })
                    }
                } else {
                    childData[k] = null;
                }
            } else {
                childData[k] = childData[k].value ? childData[k].value : null;
            }
        }
        return childData;
    }
    //切换核算账簿，会计期间时ajax请求后台数据
    queryTableDatas = (listItem) => {
        let self = this;
        let { reckonintData, queryCondition, checkedArray, checkedAll } = self.state;
        let queryParam = self.dealWithData(listItem);
        if (!queryParam.period || !queryParam.pk_accountingbook) {
            return false;
        }
        let url = '/nccloud/gl/reckoning/batchreckoningquery.do';
        checkedArray = [];
        ajax({
            url: url,
            data: queryParam,
            success: function (response) {
                const { data, success } = response;
                //渲染已有账表数据遮罩
                if (success) {
                    if (response.data) {
                        reckonintData = data;
                        reckonintData.map((item, index) => {
                            item.key = index;
                            checkedArray.push(false);
                        })
                    }
                    self.setState({
                        reckonintData, checkedArray,
                        checkedAll: false,
                    })
                }
            }
        });
    }
    //会计期间方案切换  需同步会计期间
    onAccperiodschemeChange = (value) => {
        let self = this;
        let { listItem } = self.state;
        let businessInfo = getBusinessInfo();
        let currrentDate = businessInfo.businessDate.split(' ')[0];
        ajax({
            url: '/nccloud/uapbd/org/QueryAccperiodmonth.do',
            data: {
                'pk_accperiodscheme': value,
                'curYm': currrentDate
            },
            success: (res) => {
                let { success, data } = res;
                if (success) {
                    if (data && data['pk_accperiodmonth'] && data['yearmth']) {
                        listItem['period'].value = data['yearmth'];
                        listItem['period'].display = data['yearmth'];
                    } else {
                        listItem['period'].value = '';
                        listItem['period'].display = '';
                    }
                    self.setState({
                        listItem
                    });
                }
            }
        });
    }
    getHeadData = () => {
        let self = this;
        let { listItem, reckonintData } = self.state;
        const headData = [
            {
                itemName: '', itemType: 'refer', itemKey: 'pk_accperiodscheme',
                config: { refCode: "uapbd/refer/pubinfo/AccPeriodSchemeDefaultTreeRef" }
            },
            {
                itemName: '', itemType: 'refer', itemKey: 'period',
                config: { refCode: "uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef" }
            },
            {
                itemName: '', itemType: 'refer', itemKey: 'pk_accountingbook',
                config: { refCode: "uapbd/refer/org/AccountBookTreeRef" }
            },
            {
                itemName: '', itemType: 'select', itemKey: 'state',
                itemChild: [{
                    label: self.state.json['2002BATCHRECON-000000'],/* 国际化处理： 可结账*/
                    value: '1'
                }, {
                    label: self.state.json['2002BATCHRECON-000001'],/* 国际化处理： 可取消结账*/
                    value: '2'
                }]
            }
        ]
        return listItem.pk_accperiodscheme ? (
            headData.map((item, i) => {
                let defValue = listItem[item.itemKey].value;
                switch (item.itemType) {
                    case 'refer':
                        let referUrl = item.config.refCode + '/index.js';
                        let DBValue = [];
                        let defaultValue = {}
                        if (listItem[item.itemKey].length) {
                            listItem[item.itemKey].map((item, index) => {
                                DBValue[index] = { refname: item.display, refpk: item.value };
                            })
                        } else {
                            defaultValue = { refname: listItem[item.itemKey].display, refpk: listItem[item.itemKey].value };
                        }
                        if (!self.state[item.itemKey]) {
                            { createScript.call(self, referUrl, item.itemKey) }
                            return <div />
                        } else {
                            if (item.itemKey == 'pk_accperiodscheme') {//会计期间方案 清空核算账簿和 带出默认的会计期间
                                return (
                                    <FormItem
                                        inline={true}
                                        //showMast={true}
                                        labelXs={2} labelSm={2} labelMd={2}
                                        xs={2} md={2} sm={2}
                                        labelName={item.itemName}
                                        isRequire={true}
                                        method="change"
                                    >
                                        {self.state[item.itemKey] ? (self.state[item.itemKey])(
                                            {
                                                fieldid:item.itemKey,
                                                value: defaultValue,
                                                isMultiSelectedEnabled: false,
                                                queryCondition: () => {
                                                    return Object.assign(config, {
                                                        //"pk_accountingbook": self.state.pk_accountingbook.value
                                                    })
                                                },
                                                onChange: (v) => {
                                                    let { accountingbook } = self.state;
                                                    if (v.refpk) {
                                                        self.onAccperiodschemeChange(v.refpk);
                                                    } else {
                                                        listItem.period = { display: '', value: '' }
                                                    }
                                                    listItem.pk_accountingbook = [{ display: '', value: '' }];

                                                    listItem[item.itemKey].value = v.refpk;
                                                    listItem[item.itemKey].display = v.refname;
                                                    this.setState({
                                                        listItem, accountingbook
                                                    })
                                                }
                                            }
                                        ) : <div />
                                        }
                                    </FormItem>);
                            } else if (item.itemKey == 'pk_accountingbook') {
                                return (
                                    <FormItem
                                        inline={true}
                                        showMast={true}
                                        labelXs={1} labelSm={1} labelMd={1}
                                        xs={2} md={2} sm={2}
                                        labelName={item.itemName}
                                        isRequire={true}
                                        method="change"
                                    >
                                        {self.state[item.itemKey] ? (self.state[item.itemKey])(
                                            {
                                                fieldid:item.itemKey,
                                                value: DBValue,
                                                isMultiSelectedEnabled: true,
                                                queryCondition: () => {
                                                    return Object.assign({
                                                        dependAccPeriodScheme: 'Y',
                                                        pk_accPeriodScheme: listItem['pk_accperiodscheme'].value,
                                                        "TreeRefActionExt": 'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                                                        "appcode": this.props.getSearchParam('c')
                                                    }, config)
                                                },
                                                onChange: (v) => {
                                                    let { accountingbook } = self.state;
                                                    listItem[item.itemKey] = [];
                                                    v.map((arr, index) => {
                                                        let accountingbookArr = {
                                                            display: arr.refname,
                                                            value: arr.refpk
                                                        }
                                                        listItem[item.itemKey].push(accountingbookArr);
                                                    })
                                                    self.queryTableDatas(listItem);
                                                    self.setState({
                                                        listItem, accountingbook
                                                    })
                                                }
                                            }
                                        ) : <div />
                                        }
                                    </FormItem>);
                            } else if (item.itemKey == 'period') {
                                return (
                                    <FormItem
                                        inline={true}
                                        showMast={true}
                                        labelXs={1} labelSm={1} labelMd={1}
                                        xs={2} md={2} sm={2}
                                        labelName={item.itemName}
                                        isRequire={true}
                                        method="change"
                                    >
                                        {self.state[item.itemKey] ? (self.state[item.itemKey])(
                                            {
                                                fieldid:item.itemKey,
                                                value: defaultValue,
                                                isMultiSelectedEnabled: false,
                                                queryCondition: () => {
                                                    return {
                                                        pk_accperiodscheme: listItem['pk_accperiodscheme'].value,
                                                    }
                                                },
                                                onChange: (v) => {
                                                    let { accountingbook } = self.state;
                                                    listItem[item.itemKey].value = v.refname;
                                                    listItem[item.itemKey].display = v.refname;
                                                    self.queryTableDatas(listItem);
                                                    this.setState({
                                                        listItem, accountingbook
                                                    })
                                                }
                                            }
                                        ) : <div />
                                        }
                                    </FormItem>);
                            } else {
                                return (
                                    <FormItem
                                        inline={true}
                                        //showMast={true}
                                        labelXs={1} labelSm={1} labelMd={1}
                                        xs={2} md={2} sm={2}
                                        labelName={item.itemName}
                                        isRequire={true}
                                        method="change"
                                    >
                                        {self.state[item.itemKey] ? (self.state[item.itemKey])(
                                            {
                                                fieldid:item.itemKey,
                                                value: defaultValue,
                                                queryCondition: () => {
                                                    return Object.assign({
                                                    }, config)
                                                },
                                                onChange: (v) => {
                                                    listItem[item.itemKey].value = v.refpk;
                                                    listItem[item.itemKey].display = v.refname;
                                                    this.setState({
                                                        listItem, accountingbook
                                                    })
                                                }
                                            }
                                        ) : <div />}
                                    </FormItem>)
                            }
                        }
                        break;
                    case 'select':
                        return (
                            <FormItem
                                inline={true}
                                showMast={false}
                                labelXs={1} labelSm={1} labelMd={1}
                                xs={1} md={1} sm={1}
                                labelName={item.itemName}
                                isRequire={true}
                                method="change"
                            >
                                <SelectItem 
                                    fieldid={item.itemKey}
                                    name={item.itemKey}
                                    defaultValue={defValue}
                                    items={
                                        () => {
                                            return (item.itemChild)
                                        }
                                    }
                                    onChange={(v) => {

                                        listItem[item.itemKey].value = v;
                                        self.queryTableDatas(listItem);
                                        this.setState({
                                            listItem
                                        })
                                    }}
                                />
                            </FormItem>
                        )
                    case 'textInput':
                        return (
                            <FormItem
                                inline={true}
                                showMast={true}
                                labelXs={2} labelSm={2} labelMd={2}
                                xs={2} md={2} sm={2}
                                labelName={item.itemName}
                                isRequire={true}
                                method="change"
                            >
                                <InputItem
                                    fieldid={item.itemKey}
                                    //isViewMode
                                    disabled={true}
                                    name={item.itemKey}
                                    type="customer"
                                    defaultValue={defValue}
                                />
                            </FormItem>
                        )
                    default:
                        break;
                }
            })

        )
            : (<div />)
    }
    //结账检查报告
    handleEndAccountReport = () => {
        let self = this;
        let { reportDataSum, reportSumTitle, reportSumFont, reportData, accountingbook, reportTitle, reportFont } = self.state;
        let selectedData = this.getSelectData();
        if (selectedData.length == 0) {
            toast({ content: self.state.json['2002BATCHRECON-000008'], color: 'warning' });/* 国际化处理： 请选择一行数据查看结账检查报告*/
            return false;
        }
        if (selectedData.length > 1) {
            toast({ content: self.state.json['2002BATCHRECON-000009'], color: 'warning' });/* 国际化处理： 一次只能选择一行数据查看结账检查报告*/
            return false;
        }
        let pk_accountingbook_select = '';
        selectedData.map((item, index) => {
            pk_accountingbook_select = item.pk_accountingbook;
        })
        let url = '/nccloud/gl/reckoning/reckoningreport.do';
        let pk_accpont = {
            "pk_accountingbook": pk_accountingbook_select
        }
        ajax({
            url: url,
            data: pk_accpont,
            success: function (response) {
                const { data, success } = response;
                //渲染已有账表数据遮罩
                if (success) {
                    if (response.data) {
                        reportDataSum = data.reportSumMesg;
                        reportSumTitle = data.sumDisMesg;
                        reportSumFont = data.sumFont;
                        reportData = data.reportMesg;
                        reportTitle = data.disMesg;
                        reportFont = data.font;
                        self.setState({
                            reportDataSum, reportSumTitle, reportSumFont, reportData, reportTitle, reportFont,
                            ShowHideFlag: true,
                        })
                    }
                }
            }
        });
    }
    //结账
    handleEndAccount = () => {
        let self = this;
        let { listItem, reckoningMessageData, } = self.state;
        let url = '/nccloud/gl/reckoning/batchreckoning.do';
        let selectedData = this.getSelectData();
        if (selectedData.length == 0) {
            toast({ content: self.state.json['2002BATCHRECON-000010'], color: 'warning' });/* 国际化处理： 请选择一行数据再进行操作*/
            return false;
        }
        let pk_accountingbook_select = [];
        selectedData.map((item, index) => {
            pk_accountingbook_select.push(item.pk_accountingbook);
        })
        let period = listItem.period.value;
        let pk_accpont = {
            "pk_accountingbook": pk_accountingbook_select,
            "period": period
        }
        ajax({
            url: url,
            data: pk_accpont,
            success: function (response) {
                const { data, success } = response;
                //渲染已有账表数据遮罩
                if (success) {
                    if (response.data) {
                        reckoningMessageData = data;
                        self.setState({
                            reckoningMessageData,
                            ShowHideMessge: true

                        })
                    }
                    self.handleRefresh();
                }
            }
        });
    }
    //取消结账
    handleUnendAccount = () => {
        let self = this;
        let { listItem, reckoningMessageData } = self.state;
        let url = '/nccloud/gl/reckoning/batchunreckoning.do';
        let selectedData = this.getSelectData();
        if (selectedData.length == 0) {
            toast({ content: self.state.json['2002BATCHRECON-000010'], color: 'warning' });/* 国际化处理： 请选择一行数据再进行操作*/
            return false;
        }
        let pk_accountingbook_select = []

        selectedData.map((item, index) => {
            pk_accountingbook_select.push(item.pk_accountingbook);
        })
        let period = listItem.period.value;
        let pk_accpont = {
            "pk_accountingbook": pk_accountingbook_select,
            "period": period
        }

        ajax({
            url: url,
            data: pk_accpont,
            success: function (response) {
                const { data, success } = response;
                //渲染已有账表数据遮罩
                if (success) {
                    if (response.data) {
                        reckoningMessageData = data;
                        self.setState({
                            reckoningMessageData,
                            ShowHideMessge: true

                        })

                    }
                    self.handleRefresh();
                }
            },
            error: function (res) {
                toast({ content: res.message, color: 'warning' });
            }
        });
    }
    //获取选中行数据
    getSelectData = () => {
        let { reckonintData, selectedData, checkedArray } = this.state;
        selectedData = [];
        for (var i = 0; i < checkedArray.length; i++) {
            if (checkedArray[i] == true) {
                selectedData.push(reckonintData[i]);
            }
        }
        return selectedData;
    }
    //刷新
    handleRefresh = () => {
        let { listItem } = this.state;
        this.queryTableDatas(listItem);
    }
    //打印
    handlePrintReport = () => {
        this.setState({
            showPrintModal: true
        })
    }
    showOutputModal() {
        let outputUrl = '/nccloud/gl/accountrep/reckoningreportoutput.do'
        let { appcode, nodekey, ctemplate, queryDataObj, listItem, selectedData } = this.state;
        queryDataObj.pk_accountingbook = selectedData['pk_accountingbook'];
        queryDataObj.year = listItem['period'].value.split('-')[0];
        queryDataObj.month = listItem['period'].value.split('-')[1];
        mouldOutput(outputUrl, appcode, nodekey, ctemplate, queryDataObj)
    }
    handlePrint(data) {
        let printUrl = '/nccloud/gl/accountrep/reckoningreportprint.do'
        let { queryDataObj, appcode, sumOrDetail, listItem, selectedData } = this.state
        let { ctemplate, nodekey } = data
        queryDataObj.queryvo = data
        queryDataObj.pk_accountingbook = selectedData['pk_accountingbook'];
        queryDataObj.year = listItem.period.value.split('-')[0];
        queryDataObj.month = listItem.period.value.split('-')[1];
        queryDataObj.issum = 'Y';
        this.setState({
            ctemplate: ctemplate,
            nodekey: nodekey
        })
        printRequire(printUrl, appcode, nodekey, ctemplate, queryDataObj)
        this.setState({
            showPrintModal: false
        });
    }
    //选中某一行
    getRow = (expanded, record) => {
        let copyData = deepClone(expanded);
        //表体数据增删获取key
        let { selectedData } = this.state;
        selectedData = copyData;
        let getrow = expanded.key;

        this.setState({
            getrowkey: getrow,
            selectedRecord: record,
            selectedData
        });
    };
    //关闭
    handleClose = () => {
        this.setState({
            ShowHideFlag: !this.state.ShowHideFlag,

        })
    }
    //结账信息关闭
    reckoningMessageClose = () => {
        this.setState({
            ShowHideMessge: !this.state.ShowHideMessge,

        })
    }
    GetRequest = (urlstr) => {
        var url = decodeURIComponent(urlstr)//(window.parent.location.href); //获取url中"?"符后的字串   
        var theRequest = new Object();
        if (url.indexOf("?") != -1) {
            var str = url.substr(url.indexOf("?") + 1);
            var strs = str.split("&");
            for (var i = 0; i < strs.length; i++) {
                theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
            }
        }
        return theRequest;
    }
    //全选
    onAllCheckChange = () => {
        let self = this;
        let checkedArray = [];
        let selIds = [];
        for (var i = 0; i < self.state.checkedArray.length; i++) {
            checkedArray[i] = !self.state.checkedAll;
        }
        self.setState({
            checkedAll: !self.state.checkedAll,
            checkedArray: checkedArray,
        }, () => {
            for (let i = 0; i < checkedArray.length; i++) {
                if (checkedArray.indexOf(true) == -1) {
                    let buttonFalse = ['BatchPrecloseAcc', 'BatchCloseAcc', 'BatchAntiPrecloseAcc', 'Refresh']
                    this.props.button.setButtonDisabled(buttonFalse, true);
                } else {
                    let buttonFalse = ['BatchPrecloseAcc',
                        'BatchCloseAcc',
                        'BatchAntiPrecloseAcc',
                        'Refresh']
                    this.props.button.setButtonDisabled(buttonFalse, false);
                }
            }
        });
    };
    //单选
    onCheckboxChange = (text, record, index) => {
        let self = this;
        let allFlag = false;
        let checkedArray = self.state.checkedArray.concat();
        checkedArray[index] = !self.state.checkedArray[index];
        for (var i = 0; i < self.state.checkedArray.length; i++) {
            if (!checkedArray[i]) {
                allFlag = false;
                break;
            } else {
                allFlag = true;
            }
        }
        self.setState({
            checkedAll: allFlag,
            checkedArray: checkedArray,
        }, () => {
            for (let i = 0; i < checkedArray.length; i++) {
                if (checkedArray.indexOf(true) == -1) {
                    let buttonFalse = ['BatchPrecloseAcc', 'BatchCloseAcc', 'BatchAntiPrecloseAcc', 'Refresh']
                    this.props.button.setButtonDisabled(buttonFalse, true);
                } else {
                    let buttonFalse = ['BatchPrecloseAcc',
                        'BatchCloseAcc',
                        'BatchAntiPrecloseAcc',
                        'Refresh']
                    this.props.button.setButtonDisabled(buttonFalse, false);
                }
            }
        });
        self.getRow(record, index);
    };
    renderColumnsMultiSelect(columns) {
        const { checkedArray } = this.state;
        const { multiSelect } = this.props;
        let select_column = {};
        let indeterminate_bool = false;
        if (multiSelect && multiSelect.type === "checkbox") {
            let i = checkedArray.length;
            while (i--) {
                if (checkedArray[i]) {
                    indeterminate_bool = true;
                    break;
                }
            }
            let defaultColumns = [
                {
                    title: (<div fieldid="firstcol" className='checkbox-mergecells'>{
                        <Checkbox
                            className="table-checkbox"
                            checked={this.state.checkedAll}
                            indeterminate={indeterminate_bool && !this.state.checkedAll}
                            onChange={this.onAllCheckChange}
                        />
                        }</div>),
                    key: "checkbox",
                    dataIndex: "checkbox",
                    width: 50,
                    render: (text, record, index) => {
                        return (
                            <div fieldid="firstcol">
                                <Checkbox
                                    className="table-checkbox"
                                    checked={this.state.checkedArray[index]}
                                    onChange={this.onCheckboxChange.bind(this, text, record, index)}
                                />
                            </div>
                        );
                    }
                }
            ];
            columns = defaultColumns.concat(columns);
        }
        return columns;
    }
    render() {
        let { columnsVerify, reckonintData, reckoningMessageData,
            reportDataSum, reportSumTitle, reportSumFont, reportData, reportTitle, reportFont } = this.state;
        
        if (columnsVerify.length == 0) {
            columnsVerify = this.Defaultcolumns?this.Defaultcolumns:[];
        }
        let columnsldad = this.renderColumnsMultiSelect(columnsVerify);
        let reportSum = {
            reportSumData: reportDataSum,
            reportSumTitle: reportSumTitle,
            reportSumFont: reportSumFont,
        }
        let reportDetail = {
            reportDetailData: reportData,
            reportDetailTitle: reportTitle,
            reportDetailFont: reportFont
        }
        return (
            <div className="reckoning nc-bill-list">
            <HeaderArea
				title={this.state.json['2002BATCHRECON-000011']}/* 国际化处理： 总账批量结账*/

				btnContent={
					this.props.button.createButtonApp({
                        area: 'list-area',
                        onButtonClick: buttonClick.bind(this),
                    })
				}
			/> 

                {/* //表头 */}
                <NCDiv fieldid="query" areaCode={NCDiv.config.FORM}>
                    <NCForm useRow={true}
                        submitAreaClassName='classArea'
                        showSubmit={false}　>
                        {this.getHeadData()}
                    </NCForm>
                </NCDiv>
                {/* //表体 */}
                <NCDiv fieldid="batchReckoning" areaCode={NCDiv.config.TableCom}>
                    <Table
                        columns={columnsldad}
                        bordered
                        onRowClick={this.getRow}
                        data={reckonintData}
                        bodyStyle={{height:getTableHeight(130)}}
                        scroll={{
                            x: true,
                            y: getTableHeight(130)
                            //    y:"calc(100vh - 250px)"
                        }}
                        // scroll={{ x: columnsldad.length > 8 ? (100 + (columnsldad.length - 8) * 15) + "%" : '100%', y: 320 }}
                    />
                </NCDiv>
                <ReportModal
                    reportSum={reportSum}
                    reportDetail={reportDetail}
                    showOrHide={this.state.ShowHideFlag}
                    handlePrintReport={this.handlePrintReport.bind(this)}
                    handleClose={this.handleClose.bind(this)}
                    showOutputModal={this.showOutputModal.bind(this)}//模板输出
                />
                <PrintModal
                    noRadio={true}
                    noCheckBox={true}
                    appcode={this.state.appcode}
                    visible={this.state.showPrintModal}
                    handlePrint={this.handlePrint.bind(this)}
                    handleCancel={() => {
                        this.setState({
                            showPrintModal: false
                        })
                    }}
                />
                <ReckoningOrUnreckonigModal
                    reckoningMessageData={reckoningMessageData}
                    showOrHide={this.state.ShowHideMessge}
                    handleClose={this.reckoningMessageClose.bind(this)}
                />
            </div>
        )

    }
}
Reckon.defaultProps = defaultProps12;
Reckon = createPage({
    initTemplate: initTemplate
})(Reckon);

ReactDOM.render(<Reckon />, document.querySelector("#app"));
