import {high,base,ajax,toast,getBusinessInfo,promptBox,deepClone,getMultiLang } from 'nc-lightapp-front';
const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
    NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
    NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCPanel:Panel,NCForm,NCButtonGroup:ButtonGroup,
    } = base;
    const {  NCFormItem:FormItem } = NCForm;
import { CheckboxItem,RadioItem,TextAreaItem,ReferItem,SelectItem,InputItem,DateTimePickerItem} from '../../../../public/components/FormItems';
import createScript from '../../../../public/components/uapRefer.js';
const config={
    "isDataPowerEnable":'Y',
    "DataPowerOperationCode":'fi'
};
 function rebuildBalanceItem(self,loadQuery){
    // let self=this;

       let {rebuildBalanceItem,pk_accasoaDisabled}=self.state;    
       let currrentDate;
    //    let businessInfo = getBusinessInfo();
    //     let currrentDate = businessInfo.businessDate; 
       let appcode=self.props.getSearchParam("c");
        return rebuildBalanceItem.pk_accountingbook?(
            loadQuery.map((item,i)=>{
                // let defValue=rebuildBalanceItem[item.itemKey].value;
                switch (item.itemType) {
                    case 'refer':
                    let referUrl= item.config.refCode+'/index.js';
                    let DBValue=[];
                    let defaultValue={}
                    if(rebuildBalanceItem[item.itemKey].length){                           
                        rebuildBalanceItem[item.itemKey].map((item,index)=>{
                            DBValue[index]={ refname: item.display, refpk:item.value };
                        })
                    }else{
                        defaultValue = { refname: rebuildBalanceItem[item.itemKey].display, refpk: rebuildBalanceItem[item.itemKey].value };  
                    }
                    // let defaultValue = { refname: rebuildBalanceItem[item.itemKey].display, refpk: rebuildBalanceItem[item.itemKey].value };  
                    if(!self.state[item.itemKey]){
                        {createScript.call(self,referUrl,item.itemKey)}
                        return <div />
                    }else{
                         if(item.itemKey=='pk_accountingbook'){
                             return (
                                 <FormItem
                                      inline={true}
                                      showMast={item.showMast}
                                      labelXs={2} labelSm={2} labelMd={2}
                                      xs={2} md={2} sm={2}
                                      labelName={item.itemName}
                                      isRequire={true}
                                      method="change"
                                     >
                                     {self.state[item.itemKey]?(self.state[item.itemKey])(
                                         {
                                            fieldid:item.itemKey,
                                             value:DBValue,
                                             isMultiSelectedEnabled:true,
                                             queryCondition:() => {
                                                 return Object.assign({
                                                    "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                                                    "appcode":appcode
                                                 },config)
                                              },
                                             onChange:(v)=>{
                                                let {accountingbook}=self.state;
                                                // if(v.length>0){
                                                    if(v.length>1){
                                                        pk_accasoaDisabled=true;
                                                        rebuildBalanceItem['pk_accasoa']={display:'',value:''}
                                                    }else{
                                                        pk_accasoaDisabled=false;
                                                    }
                                                    if(v.length>0){
                                                        self.getYearList(v[0].refpk);
                                                    }
                                                     
                                                     rebuildBalanceItem[item.itemKey]=[];
                                                        v.map((arr,index)=>{
                                                            let accountingbook={
                                                                display:arr.refname,
                                                                value:arr.refpk
                                                            }
                                                            rebuildBalanceItem[item.itemKey].push(accountingbook);
                                                        })
                                                    // rebuildBalanceItem[item.itemKey].value = v.refpk;
                                                    // rebuildBalanceItem[item.itemKey].display = v.refname;
                                                    self.setState({
                                                        rebuildBalanceItem,pk_accasoaDisabled
                                                    })
                                                // }
                                            }
                                        }
                                     ):<div />}
                             </FormItem>);
                        }else if(item.itemKey=='pk_accasoa'){
                            return (
                                <FormItem
                                     inline={true}
                                     showMast={item.showMast}
                                     labelXs={2} labelSm={2} labelMd={2}
                                     xs={2} md={2} sm={2}
                                     labelName={item.itemName}
                                     isRequire={true}
                                     method="change"
                                    >
                                    {self.state[item.itemKey]?(self.state[item.itemKey])(
                                        {
                                            fieldid:item.itemKey,
                                            value:DBValue,
                                            isMultiSelectedEnabled:true,
                                            disabled:pk_accasoaDisabled,
                                            queryCondition:() => {
                                                return {
                                                    "pk_accountingbook": rebuildBalanceItem.pk_accountingbook[0].value,
                                                    "dateStr":currrentDate.split(' ')[0]
                                                }
                                             },
                                             onFocus:()=>{
                                                let businessInfo = getBusinessInfo();
                                                currrentDate = businessInfo.businessDate; 
                                             },
                                            onChange:(v)=>{
                                               let {accountingbook}=self.state;
                                            //    if(v.length>0){
                                                rebuildBalanceItem[item.itemKey]=[];
                                                v.map((arr,index)=>{
                                                    let accountingbook={
                                                        display:arr.refname,
                                                        value:arr.refpk
                                                    }
                                                    rebuildBalanceItem[item.itemKey].push(accountingbook);
                                                })
                                                //    rebuildBalanceItem[item.itemKey].value = v.refpk;
                                                //    rebuildBalanceItem[item.itemKey].display = v.refname;
                                                   self.setState({
                                                       rebuildBalanceItem,accountingbook
                                                   })
                                            //    }
                                           }
                                       }
                                    ):<div />}
                            </FormItem>);
                        }
                    }
                    break;
                    case 'Dbselect':
                
                        return(
                            <FormItem
                            inline={true}
                            // showMast={false}
                            labelXs={2} labelSm={2} labelMd={2}
                            xs={2} md={2} sm={2}
                            labelName={item.itemName}
                            isRequire={true}
                            method="change"
                            inputAfter={
                                <Col xs={12} md={12} sm={12}>
                                    <Select
                                        fieldid="period"
                                        size="lg"
                                        value={rebuildBalanceItem['period'].value}
                                        style={{ width: 150}}
                                        onChange={
                                            (v)=>{
                                                rebuildBalanceItem.period={value: v}
                                                self.setState({
                                                    rebuildBalanceItem
                                                })
                                            }									
                                        }	
                                        >
                                        {self.state.periodList.map((item, index) => {
                                            return <Option value={item} key={item} >{item}</Option>
                                        } )}
                                    
                                    </Select>
                                </Col>
                            }
                            >
                            <Select
                                fieldid="year"
                                size="lg"
                                value={rebuildBalanceItem['year'].value}
                                style={{ width: 150}}
                                onChange={
                                    (v)=>{
                                        if(v){
                                            if(rebuildBalanceItem['pk_accountingbook'].length>0){
                                                self.getPeriodList(rebuildBalanceItem['pk_accountingbook'][0].value,v);
                                            }
                                            rebuildBalanceItem.year={value: v};
                                            self.setState({
                                                rebuildBalanceItem
                                            })
                                        }
                                    }									
                                }	
                                >
                                {self.state.yearList.map((item, index) => {
                                    return <Option value={item} key={item} >{item}</Option>
                                } )}
                            
                            </Select>
                        </FormItem>
                        )
                    case 'textInput':
                        return(
                        <FormItem
                            inline={true}
                            // showMast={true}
                            labelXs={2} labelSm={2} labelMd={2}
                            xs={2} md={2} sm={2}
                            labelName={item.itemName}
                            isRequire={true}
                            method="change"
                            >
                            {/* <FormControl
                                value={defValue}
                            /> */}
                            <InputItem
                                fieldid={item.itemKey}
                                //isViewMode
                                disabled={true}
                                name={item.itemKey}
                                type="customer"
                                defaultValue={defValue}
                            />
                        </FormItem>
                    )
                    default:
                    break;
                }
            })
            
        )
        :(<div/>)
 }
 function rebuildBalanceComfirm(){
    let {rebuildBalanceItem,json}=this.state;
    if(!rebuildBalanceItem.pk_accountingbook[0].value||rebuildBalanceItem.pk_accountingbook[0].value==''){
        toast({ content: json['20020RECON-000010'], color: 'warning' });/* 国际化处理： 请先选择核算账簿*/
        return false;
    }
    promptBox({
        color:'success',
        title:json['20020RECON-000015'],/* 国际化处理： 提示信息*/
        content:json['20020RECON-000020'],/* 国际化处理： 此操作会删除所有余额表数据并重新构造，执行此操作会更正部分异常数据对账平衡的影响。是否执行？*/
        noFooter: false,                // 是否显示底部按钮(确定、取消),默认显示(false),非必输
        noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
        beSureBtnName:json['20020RECON-000021'],          // 确定按钮名称, 默认为"确定",非必输/* 国际化处理： 确定*/
        cancelBtnName:json['20020RECON-000022'],           // 取消按钮名称, 默认为"取消",非必输/* 国际化处理： 取消*/
        beSureBtnClick: dealOperate.bind(this,json),   // 确定按钮点击调用函数,非必输
        cancelBtnClick: cancelBtnClick.bind(this)  // 取消按钮点击调用函数,非必输
    })
 }
 function dealOperate(json){
    let self = this;
    let {rebuildBalanceItem}=self.state;
    let childData = deepClone(rebuildBalanceItem);
    for(let k in childData){
        if(k=='ass'||k=='acccode'||k=='accname'){
        }else if(k=='pk_accountingbook'){
            if(childData[k].length>0){
                if(childData[k][0].value==""){
                    childData[k]=null;
                }else{
                    childData[k].map((item,index)=>{
                        //childData[k].push(item.value);
                        childData[k][index]=item.value;
                    })
                }
            }else{
                childData[k]=null;
            }
        }else if(k=='pk_accasoa'){
            if(childData[k].length>0){
                if(childData[k][0].value==""){
                    childData[k]=null;
                }else{
                    childData[k].map((item,index)=>{
                        //childData[k].push(item.value);
                        childData[k][index]=item.value;
                    })
                }
            }else{
                childData[k]=null;
            }
        }else{
            childData[k]=childData[k].value?childData[k].value:null;
        }			
    }
    if(childData.pk_accountingbook.length==1){
        childData.pk_accountingbook=childData.pk_accountingbook[0];
        childData.pk_accasoa=null;
    }
    let rebuildbalanceUrl = '/nccloud/gl/glpub/rebuildbalance.do';
    // let data = childData;
    ajax({
        url:rebuildbalanceUrl,
        data:childData,
        success: function (res) {
            const { data, error, success } = res;
            if(success){
                toast({ content: json['20020RECON-000033'], color: 'success' });//重建余额表成功
            }else {
                toast({ content: error.message, color: 'warning' });
            }
        },
        error: function (res) {
            toast({ content: res.message, color: 'warning' });
            
        }
    });
}
function cancelBtnClick(){
    return false;
}
 export {rebuildBalanceItem,rebuildBalanceComfirm}
