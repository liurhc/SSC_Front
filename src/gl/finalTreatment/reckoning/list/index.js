import React, { Component } from 'react';
import {createPage,high,base,ajax,deepClone,toast ,getMultiLang,createPageIcon} from 'nc-lightapp-front';
const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCPanel:Panel,NCForm,NCButtonGroup:ButtonGroup,NCAffix,NCDiv
} = base;
const {  NCFormItem:FormItem } = NCForm;
import { CheckboxItem,RadioItem,TextAreaItem,ReferItem,SelectItem,InputItem,DateTimePickerItem} from '../../../public/components/FormItems';
import createScript from '../../../public/components/uapRefer.js';
import ReportModal from './reportModal/reportModal';
import getDefaultAccountBook from '../../../public/components/getDefaultAccountBook.js';
import {rebuildBalanceItem,rebuildBalanceComfirm} from './rebuildBalancemodal/index';
import { buttonClick, initTemplate} from './events';
import PrintModal from '../../../public/components/printModal';
const { PrintOutput } = high;
import { printRequire, mouldOutput } from '../../../public/components/printModal/events';
import HeaderArea from '../../../public/components/HeaderArea';
import { getTableHeight } from '../../../public/common/method.js';
import ExportReportModal from './reportModal/exportReportModal';
import './index.less';
import '../../../public/reachercss/reacher.less';
import UploadTax from './uploadTax'
const config={
    "isDataPowerEnable":'Y',
    "DataPowerOperationCode":'fi'
};
class Reckon extends Component{
    constructor(props){
        super(props);
        this.state={
            pk_periodscheme:"",//上传税务云参数
            up_pk_accountingbook:{},//上传税务云pk
            pk_display:"",//上传税务云账簿名字
            json:{},
            period:'',//上传税务云期间
            taxcloudurl:'',//上传税务云地址
            isQueryShow:false,//上传税务云弹框
            appcode:this.props.getUrlParam('c') || this.props.getSearchParam('c'),
            ShowHideFlag:false,//查询条件对话框
            pk_accasoaDisabled:false,//控制科目是否可以编辑
            accountingbook:{display:'',value:''},
            reportDataSum:[],//结账汇总数据
            reportData:[],//结账详情数据
            listItem:{
            },//表头字段
            rebuildBalanceItem:{},//重建余额表的查询字段
            yearList:[],
            periodList:[],
            reportSumTitle:'',//汇总报告title
            reportSumFont:{},//汇总报告字体
            reportTitle:'',//报告title
            reportFont:{},
            refersh:false,//刷新
            columnsVerify:[],
            reckonintData:[],
            verBalanceVOData:{},
            queryDataObj:{},//保存查询条件
            resourceData_sum:[],//查询出来的历史数据
            resourceColumns_sum:[],//查询出来的历史数据列字段
            selectedData:{},//选中的数据
        }       
    }
    handleCloseTax = () =>{
        this.setState({
            isQueryShow: false
        });
    }
    //根据账簿请求会计年 月
    getYearList = (pk_accountingbook) => {
        let self = this;
        let {rebuildBalanceItem,yearList}=self.state;
		let url = '/nccloud/gl/voucher/yearcombo.do';
		let data = {
			"pk_accountingbook":pk_accountingbook
		};       
		ajax({
		    url,
		    data,
		    success: function (res) {
		      
		        const { data, error, success } = res;
		        if(success){
		        	self.setState({
		        		yearList: data,
		        	})
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});
    }
    //根据账簿请求会计年 月
    getPeriodList = (pk_accountingbook,year) => {
        let self = this;
		let url = '/nccloud/gl/voucher/periodcombo.do';
		let data = {
            "pk_accountingbook":pk_accountingbook,
            "year":year,
		};

		ajax({
		    url,
		    data,
		    success: function (res) {
		      
		        const { data, error, success } = res;
		        if(success){
		        	self.setState({
		        		periodList: data,
		        	})
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});
	}
    //处理width
    changeWidth(arr){
        arr.map((item,index)=>{
            if(item.children){
                this.changeWidth(item.children);
            }else{
               item['width']=150; 
            }            
        })
        return arr;
    }

    getHeadData=()=>{
        let self=this;
       let {listItem,reckonintData}=self.state;    
       let appcode=self.props.getSearchParam("c");
       const headData=[
        {itemName:this.state.json['20020RECON-000000'],itemType:'refer',itemKey:'pk_accountingbook',/* 国际化处理： 核算账簿*/
        config:{refCode:"uapbd/refer/org/AccountBookTreeRef"},queryGridUrl:'/nccloud/gl/voucher/ref.do',refType:'grid'},
        {itemName:this.state.json['20020RECON-000001'],itemType:'textInput',itemKey:'reckedMonth'},/* 国际化处理： 待结账月份:*/
    ]
        return listItem.reckedMonth?(
            headData.map((item,i)=>{
                let defValue=listItem[item.itemKey].value;
                switch (item.itemType) {
                    case 'refer':
                    let referUrl= item.config.refCode+'/index.js';
                    let defaultValue = { refname: listItem[item.itemKey].display, refpk: listItem[item.itemKey].value };  
                    if(!self.state[item.itemKey]){
                        {createScript.call(self,referUrl,item.itemKey)}
                        return <div />
                    }else{
                         if(item.itemKey=='pk_accountingbook'){
                             return (
                                 <FormItem
                                      inline={true}
                                      showMast={true}
                                      labelXs={2} labelSm={2} labelMd={2}
                                      xs={2} md={2} sm={2}
                                      labelName=''
                                      isRequire={true}
                                      method="change"
                                      className="changesm"
                                     >
                                     {self.state[item.itemKey]?(self.state[item.itemKey])(
                                         {
                                            fieldid:item.itemKey,
                                             value:defaultValue,
                                             isMultiSelectedEnabled:false,
                                             disabledDataShow:true,
                                             queryCondition:() => {
                                                 return Object.assign({
                                                    "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                                                    "appcode":appcode
                                                 },config)
                                              },
                                             onChange:(v)=>{
                                                let {accountingbook}=self.state;
                                                
                                                    if(item.itemKey=='pk_accountingbook'){
                                                        if(v.refpk){
                                                            self.getReckoningData(v.refpk);
                                                        }else{
                                                            reckonintData=[];
                                                        }

                                                    accountingbook.value=v.refpk;
                                                    accountingbook.display=v.refname;
                                                    listItem[item.itemKey].value = v.refpk;
                                                    listItem[item.itemKey].display = v.refname;
                                                    this.setState({
                                                        listItem,accountingbook,reckonintData
                                                    })
                                                }
                                            }
                                        }
                                     ):<div />}
                             </FormItem>);
                        }
                    }
                    break;
                    case 'select':
                        return(
                            <FormItem
                            inline={true}
                            // showMast={false}
                            labelXs={2} labelSm={2} labelMd={2}
                            xs={2} md={2} sm={2}
                            labelName={item.itemName}
                            isRequire={true}
                            method="change"
                            className="changemd"
                            >
                            <SelectItem 
                                name={item.itemKey}
                                fieldid={item.itemKey}
                                defaultValue={item.itemChild[0].value?item.itemChild[0].value:''} 
                                items = {
                                    () => {
                                        return (item.itemChild) 
                                    }
                                }
                                onChange={(v)=>{                                 
                                    listItem[item.itemKey].value = v
                                        this.setState({
                                            listItem
                                        })
                                }}
                            />
                        </FormItem>
                        )
                    case 'textInput':
                        return(
                        <FormItem
                            inline={true}
                            // showMast={true}
                            labelXs={2} labelSm={2} labelMd={2}
                            xs={2} md={2} sm={2}
                            labelName={item.itemName}
                            isRequire={true}
                            method="change"
                            className="changesm"
                            >
                            {/* <FormControl
                                value={defValue}
                            /> */}
                            {/* <InputItem
                                //isViewMode
                               // disabled={true}
                                name={item.itemKey}
                                type="customer"
                                defaultValue={defValue}
                                className="changesmmd"
                            /> */}
                            {listItem['pk_accountingbook'].value?                           
                            <span className="changesmmd" style={{verticalAlign:'middle'}}>{defValue.split("/")[0]}{this.state.json['20020RECON-000031']}{defValue.split("/")[1]}{this.state.json['20020RECON-000032']}</span>
                            :
                            <span className="changesmmd" style={{verticalAlign:'middle'}}></span>  
                        }
                        </FormItem>
                    )
                    default:
                    break;
                }
            })
            
        )  
        :(<div/>)
    }
    //结账检查报告
    handleEndAccountReport=()=>{
        let self=this;
        let {reportDataSum,reportSumTitle,reportSumFont,reportData,accountingbook,reportTitle,reportFont}=self.state;
        if(accountingbook.value==""||accountingbook.value==undefined){
            toast({ content: self.state.json['20020RECON-000010'], color: 'warning' });/* 国际化处理： 请先选择核算账簿*/
            return false;
        }
        let url ='/nccloud/gl/reckoning/reckoningreport.do';
        let pk_accpont = {"pk_accountingbook":accountingbook.value};
        ajax({
            url:url,
            data:pk_accpont,
            success: function(response){
                const { data,success } = response;
                //渲染已有账表数据遮罩
                if (success) {
                    if(response.data){
                        reportDataSum=data.reportSumMesg;
                        reportSumTitle=data.sumDisMesg;
                        reportSumFont=data.sumFont;
                        reportData=data.reportMesg;
                        reportTitle=data.disMesg;
                        reportFont=data.font;
                        self.setState({
                            reportDataSum,reportSumTitle,reportSumFont,reportData,reportTitle,reportFont,
                            ShowHideFlag:true,
                        })
                    }
                }   
            }
        });
    }
    //结账
    handleEndAccount=()=>{
        // this.refs.getSwordButton.print();
        // return false;
        let self=this;
        let {reckonintData,listItem,reportDataSum,reportSumTitle,reportSumFont,reportData,accountingbook,reportTitle,reportFont}=self.state;
        if(accountingbook.value==""||accountingbook.value==undefined){
            toast({ content: self.state.json['20020RECON-000010'], color: 'warning' });/* 国际化处理： 请先选择核算账簿*/
            return false;
        }
        let url ='/nccloud/gl/reckoning/reckoning.do';
        let pk_accpont = {"pk_accountingbook":accountingbook.value};
        ajax({
            url:url,
            data:pk_accpont,
            success: function(response){
                const { data,success } = response;
                //渲染已有账表数据遮罩
                if (success) {
                    if(response.data){
                        let isReckoningble=data.isReckoningble;
                        if(isReckoningble){
                            toast({ content: self.state.json['20020RECON-000011'], color: 'success' });/* 国际化处理： 结账成功*/
                            reckonintData=data.reckedYear;
                            reckonintData.map((item,index)=>{
                                item.key=index;
                            })
                            listItem['reckedMonth'].value=data.reckedMonth;
                            self.setState({
                                reckonintData,listItem
                            })
                        }else{
                            toast({ content: self.state.json['20020RECON-000012'], color: 'warning' });/* 国际化处理： 结账失败*/
                            reportDataSum=data.reportSumMesg;
                            reportSumTitle=data.sumDisMesg;
                            reportSumFont=data.sumFont;
                            reportData=data.reportMesg;
                            reportTitle=data.disMesg;
                            reportFont=data.font;
                            self.setState({
                                reportDataSum,reportSumTitle,reportSumFont,reportData,reportTitle,reportFont,
                                ShowHideFlag:true,
                            })
                        }    
                    }
                }   
            }
        });
    }
    //取消结账
    handleUnendAccount=()=>{
        let self=this;
        let {reckonintData,listItem,reportDataSum,reportData,accountingbook,reportTitle,reportFont}=self.state;
        if(accountingbook.value==""||accountingbook.value==undefined){
            toast({ content: self.state.json['20020RECON-000010'], color: 'warning' });/* 国际化处理： 请先选择核算账簿*/
            return false;
        }
        let url ='/nccloud/gl/reckoning/unreckoning.do';
        let pk_accpont = {"pk_accountingbook":accountingbook.value};
        ajax({
            url:url,
            data:pk_accpont,
            success: function(response){
                const { data,success } = response;
                //渲染已有账表数据遮罩
                if (success) {
                    if(response.data){
                        let isReckoningble=data.isReckoningble;
                        // if(isReckoningble){
                            toast({ content: self.state.json['20020RECON-000013'], color: 'success' });/* 国际化处理： 反结账成功*/
                            reckonintData=data.reckedYear;
                            reckonintData.map((item,index)=>{
                                item.key=index;
                            })
                            listItem['reckedMonth'].value=data.reckedMonth;
                            self.setState({
                                reckonintData,listItem
                            })
                        // }else{
                        //     reportData=data.reportMesg;
                        //     reportDataSum=data.reportSumMesg;
                        //     reportTitle=data.disMesg;
                        //     reportFont=data.font;
                        //     self.setState({
                        //         reportDataSum,reportData,reportTitle,reportFont,
                        //         ShowHideFlag:true,
                        //     })
                        // }    
                    }
                }   
            },
            error:function(res){
                toast({ content: res.message, color: 'danger' });
            }
        });
    }
    //重建余额表
    handleRebuildbalance=()=>{
        this.props.modal.show('rebuildbalance');
    }
    //上传税务云
    handleUploud=()=>{
        let self = this;
        let {listItem, up_pk_accountingbook, taxcloudurl, period, pk_periodscheme, pk_display} = this.state;
        up_pk_accountingbook = {'pk_accountingbook':listItem.pk_accountingbook.value}
        pk_display = listItem.pk_accountingbook.display
        let url = '/nccloud/gl/pfxx/taxcloudconfig.do';
        ajax({
		    url,
		    data:up_pk_accountingbook,
		    success: function (res) {
                taxcloudurl = res.data.url
                period = res.data.period
                pk_periodscheme = res.data.pk_periodscheme
                self.setState({
                    up_pk_accountingbook,
                    taxcloudurl,
                    pk_periodscheme,
                    period,
                    pk_display,
                    isQueryShow:true
                })
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });		        
		    }
		});

    }
      //打印
    handlePrintReport() {
        this.setState({
        showPrintModal: true
        })
    }
    handlePrint(data) {
        let printUrl = '/nccloud/gl/accountrep/reckoningreportprint.do'
        let { queryDataObj, appcode,sumOrDetail,listItem} = this.state
        let { ctemplate, nodekey } = data
        queryDataObj.queryvo = data
        queryDataObj.pk_accountingbook=listItem['pk_accountingbook'].value;
        queryDataObj.year=listItem['reckedMonth'].value.split('/')[0];
        queryDataObj.month=listItem['reckedMonth'].value.split('/')[1];
        queryDataObj.issum='Y';
        this.setState({
            ctemplate: ctemplate,
            nodekey: nodekey
        })
        printRequire(printUrl, appcode, nodekey, ctemplate, queryDataObj)
        this.setState({
            showPrintModal: false
        });
    }
  //模板输出
  showOutputModal() {
    let outputUrl = '/nccloud/gl/accountrep/reckoningreportoutput.do'
    let { appcode, nodekey, ctemplate, queryDataObj ,listItem} = this.state;
        queryDataObj.pk_accountingbook=listItem['pk_accountingbook'].value;
        queryDataObj.year=listItem['reckedMonth'].value.split('/')[0];
        queryDataObj.month=listItem['reckedMonth'].value.split('/')[1];
    mouldOutput(outputUrl, appcode, nodekey, ctemplate, queryDataObj)
  }
  handleOutput() {
  }
    //关闭
    handleClose=()=>{
        this.setState({
			ShowHideFlag: !this.state.ShowHideFlag,
			
		})
    }
    GetRequest=(urlstr)=> {   
		var url = decodeURIComponent(urlstr)//(window.parent.location.href); //获取url中"?"符后的字串   
		var theRequest = new Object();   
		if (url.indexOf("?") != -1) {   
		   var str = url.substr(url.indexOf("?")+1);   
		   var strs = str.split("&");   
		   for(var i = 0; i < strs.length; i ++) {   
			  theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);   
		   }   
		}   
		return theRequest;   
    }
    //字段格式转换
    keyValueChange=(defaultAccouontBook,data,listItem)=>{
        data.forEach((item,i)=>{
            let  key;
            if(item.itemType=='refer'){
                if(item.itemKey=='pk_accountingbook'){
                    key=[{
                        display:defaultAccouontBook.display,
                        value:defaultAccouontBook.value
                    }]
                }else if(item.itemKey=='pk_accasoa'){
                    key=[{
                        display:'',
                        value:''
                    }]
                }else{
                    key={
                        display:'',
                        value:''
                    }
                }
            }else if(item.itemType=='select'||item.itemType=='Dbselect'||item.itemType=='radio'){//下拉框赋初始值
                key={
                    value:''
                }
            }else{
                key={
                    value:''
                }
                if(item.itemType=='date'||item.itemType=='Dbdate'){
                    key={
                        value:''
                    }
                }
            }
            if(item.itemType=='Dbdate'||item.itemType=='DbtextInput'||item.itemType=='Dbselect'){
            item.itemKey.map((k,index)=>{
                let name= k;
                listItem[name]=key
            });
            }else{
                let name= item.itemKey;
                listItem[name]=key
            }        
        })
        this.setState({
            listItem
        })
    }
    //切换账簿请求数据和结账月份
    getReckoningData=(pk_accountingbook)=>{
        let self=this;        
        let {reckonintData,listItem}=this.state;
        let url ='/nccloud/gl/reckoning/refreshperiod.do';
        let pk_accpont = {"pk_accountingbook":pk_accountingbook}
        //let pk_accpont = {"pk_accountingbook": "1001A3100000000008MT"}
        ajax({
            url:url,
            data:pk_accpont,
            success: function(response){
                const { data,success } = response;
                //渲染已有账表数据遮罩
                if (success) {
                    if(response.data){
                        reckonintData=data.reckedYear;
                        reckonintData.map((item,index)=>{
                            item.key=index;
                        })
                        listItem['reckedMonth'].value=data.reckedMonth;
                    }
                    self.setState({
                        reckonintData
                    })
                }   
            }
        });
    }
    componentDidMount(){
        let self=this; 
        let url = '/nccloud/gl/pfxx/taxcloudconfig.do';
        ajax({
            url:url,
            success: function(response){
                self.props.button.setButtonDisabled(['uploadtaxcloud'], false);
            },
            error : function(response){
                 self.props.button.setButtonDisabled(['uploadtaxcloud'], true);
                
            }
        });
    }
    componentWillMount(){
        let {listItem,rebuildBalanceItem,accountingbook}=this.state;
        let appcode=this.props.getSearchParam("c");
        // let defaultAccouontBook=getDefaultAccountBook(appcode);        
        getDefaultAccountBook(appcode).then((defaultAccouontBook)=>{  
            accountingbook=defaultAccouontBook;
            if(defaultAccouontBook.value){
                this.getReckoningData(defaultAccouontBook.value); 
                this.getYearList(defaultAccouontBook.value);
            }    
        const headData=[
            {itemName:this.state.json['20020RECON-000000'],itemType:'refer',itemKey:'pk_accountingbook',/* 国际化处理： 核算账簿*/
            config:{refCode:"uapbd/refer/org/AccountBookTreeRef"},queryGridUrl:'/nccloud/gl/voucher/ref.do',refType:'grid'},
            {itemName:this.state.json['20020RECON-000001'],itemType:'textInput',itemKey:'reckedMonth'},/* 国际化处理： 待结账月份:*/
        ]
        const loadQuery=[
            {
                itemName:this.state.json['20020RECON-000000'],/* 国际化处理： 核算账簿*/
                itemType:'refer',
                itemKey:'pk_accountingbook',
                config:{refCode:"uapbd/refer/org/AccountBookTreeRef"},
                showMast:true
            },
            {
                itemName: this.state.json['20020RECON-000002'],/* 国际化处理： 会计科目*/
                itemType: "refer",
                itemKey: "pk_accasoa",
                config: { refCode: "uapbd/refer/fiacc/AccountDefaultGridTreeRef" },
                showMast:false
              },
            {itemName:this.state.json['20020RECON-000003'],itemType:'Dbselect',itemKey:['year','period'],/* 国际化处理： 会计期间*/
                itemChild:[]
            }
            // {itemName:'会计期间',itemType:'select',itemKey:'period',
            // 	itemChild:[]
            // }
        ]
        this.keyValueChange(defaultAccouontBook,loadQuery,rebuildBalanceItem);
        headData.forEach((item,i)=>{
            let  key;
            if(item.itemType=='refer'){
                if(item.itemKey=='pk_accountingbook'){
                    key={
                        display:defaultAccouontBook.display,
                        value:defaultAccouontBook.value
                    }
                }else{

                    key={
                        display:'',
                        value:''
                    }
                }
            }else if(item.itemType=='select'||item.itemType=='Dbselect'||item.itemType=='radio'){//下拉框赋初始值
                key={
                    value:item.itemChild[0].value
                }
            }else{
                key={
                    value:''
                }
                if(item.itemType=='date'||item.itemType=='Dbdate'){
                    key={
                        value:''
                    }
                }
            }
            if(item.itemType=='Dbdate'||item.itemType=='DbtextInput'){
            item.itemKey.map((k,index)=>{
                let name= k;
                listItem[name]=key
            });
            }else{
                let name= item.itemKey;
                listItem[name]=key
            }        
        })
        this.setState({
            listItem,accountingbook
        })
    })
    let callback= (json) =>{
        this.Defaultcolumns = [
            {
                title: (<div fieldid="dispReckedMonth" className="mergecells">{json['20020RECON-000004']}</div>),/* 国际化处理： 会计期间 */
                dataIndex: "dispReckedMonth",
                key: "dispReckedMonth",
                width: 100,
                render: (text, record, index) => {
                    return <div fieldid="dispReckedMonth">{!text?<span>&nbsp;</span>:text}</div>
                }
            },
            {
                title: (<div fieldid="dispReckedOrNot" className="mergecells">{json['20020RECON-000005']}</div>),/* 国际化处理： 是否结账*/
                dataIndex: "dispReckedOrNot",
                key: "dispReckedOrNot",
                width: 100,
                render: (text, record, index) => {
                    return <div fieldid="dispReckedOrNot">{!text?<span>&nbsp;</span>:text}</div>
                }
            },
            {
                title: (<div fieldid="endaccuser" className="mergecells">{json['20020RECON-000006']}</div>),/* 国际化处理： 结账人*/
                dataIndex: "endaccuser",
                key: "endaccuser",
                width: 100,
                render: (text, record, index) => {
                    return <div fieldid="endaccuser">{!text?<span>&nbsp;</span>:text}</div>
                }
            },
            {
                title: (<div fieldid="endacctime" className="mergecells">{json['20020RECON-000007']}</div>),/* 国际化处理： 结账时间*/
                dataIndex: "endacctime",
                key: "endacctime",
                width: 100,
                render: (text, record, index) => {
                    return <div fieldid="endacctime">{!text?<span>&nbsp;</span>:text}</div>
                }
            },
            {
                title: (<div fieldid="unendaccuser" className="mergecells">{json['20020RECON-000008']}</div>),/* 国际化处理： 取消结账*/
                dataIndex: "unendaccuser",
                key: "unendaccuser",
                width: 100,
                render: (text, record, index) => {
                    return <div fieldid="unendaccuser">{!text?<span>&nbsp;</span>:text}</div>
                }
            },
            {
                title: (<div fieldid="unendacctime">{json['20020RECON-000009']}</div>),/* 国际化处理： 取消结账时间*/
                dataIndex: "unendacctime",
                key: "unendacctime",
                width: 100,
                render: (text, record, index) => {
                    return <div fieldid="unendacctime">{!text?<span>&nbsp;</span>:text}</div>
                }
            }
        
        ];
        this.setState({
          json:json,
        },()=>{
          initTemplate.call(this, this.props);
        })
      }
      getMultiLang({moduleId:'20020RECON',domainName:'gl',currentLocale:'simpchn',callback});
    }

    render(){
        let {taxcloudurl, pk_periodscheme, up_pk_accountingbook, pk_display, period} = this.state
        
        let{accountingbook,columnsVerify,reckonintData,listItem,ShowHideFlag,
            reportDataSum,reportSumTitle,reportSumFont,reportData,reportTitle,reportFont}=this.state;
            let {modal} = this.props;
            let { createModal } = modal;
            if(columnsVerify.length==0){
            columnsVerify=this.Defaultcolumns?this.Defaultcolumns:[];
        }
        let reportSum={
            reportSumData:reportDataSum,
            reportSumTitle:reportSumTitle,
            reportSumFont:reportSumFont,           
        }
        let reportDetail={
            reportDetailData:reportData,
            reportDetailTitle:reportTitle,
            reportDetailFont:reportFont
        }
        const loadQuery=[
            {
                itemName:this.state.json['20020RECON-000000'],/* 国际化处理： 核算账簿*/
                itemType:'refer',
                itemKey:'pk_accountingbook',
                config:{refCode:"uapbd/refer/org/AccountBookTreeRef"},
                showMast:true
            },
            {
                itemName: this.state.json['20020RECON-000002'],/* 国际化处理： 会计科目*/
                itemType: "refer",
                itemKey: "pk_accasoa",
                config: { refCode: "uapbd/refer/fiacc/AccountDefaultGridTreeRef" },
                showMast:false
              },
            {itemName:this.state.json['20020RECON-000003'],itemType:'Dbselect',itemKey:['year','period'],/* 国际化处理： 会计期间*/
                itemChild:[]
            }
            // {itemName:'会计期间',itemType:'select',itemKey:'period',
            // 	itemChild:[]
            // }
        ]
        return(
            <div className="reckoning nc-bill-list" id="finalTreatment_rekoning">
            <HeaderArea
				title={this.state.json['20020RECON-000018']}/* 国际化处理： 总账结账*/
                searchContent = {
                    <div className="headtitle nc-theme-form-label-c" fieldid="reckoning_form-area">
                        <NCForm useRow={true}
                            submitAreaClassName='classArea'
                            showSubmit={false}　>
                            {this.getHeadData()}
                        </NCForm>
                    </div>
                }
				btnContent={
					this.props.button.createButtonApp({
                        area: 'button_area',
                        buttonLimit: 4,
                        onButtonClick: buttonClick.bind(this),
                        popContainer: document.querySelector('.header-button-area')
                    })
				}
			/> 
                {/* //表体 */}
                <NCDiv fieldid="reckoning" areaCode={NCDiv.config.TableCom}>
                    <Table 
                        columns={columnsVerify?columnsVerify:[]} 
                        bordered 
                        className="nc-table"
                        onRowClick={this.getRow}
                        data={reckonintData}
                        // scroll={{ x: columnsVerify.length > 8 ? (100+(columnsVerify.length-8)*15)+"%": '100%', y: 320 }}
                        bodyStyle={{height:getTableHeight(80)}}
                        scroll={{
                            x: true,
                            y: getTableHeight(80)
                            //    y:"calc(100vh - 250px)"
                        }}
                    />
                </NCDiv>
                <ReportModal 
                    // config={this.props}
                    reportSum={reportSum}
                    reportDetail={reportDetail}
                    showOrHide = {this.state.ShowHideFlag}  
                    // onConfirm = {this.handleQueryClick.bind(this)} 
                    handlePrintReport={this.handlePrintReport.bind(this)}//打印
                    showOutputModal={this.showOutputModal.bind(this)}//模板输出
                    handleClose={this.handleClose.bind(this)}
                />
                {/* <ExportReportModal 
                    data={this.state.data}
                    ref="getSwordButton"
                /> */}
                <div>
					{this.state.json['20020RECON-000015']&&createModal('rebuildbalance', {
                        className:"repeatexl",
						title: this.state.json['20020RECON-000015'],// 弹框表头信息/* 国际化处理： 重建余额表*/
						content: rebuildBalanceItem(this,loadQuery), //弹框内容，可以是字符串或dom
						beSureBtnClick: rebuildBalanceComfirm.bind(this), //点击确定按钮事件
						userControl: false,  // 点确定按钮后，是否自动关闭弹出框.true:手动关。false:自动关
						size: 'lg', //  模态框大小 sm/lg/xlg
						rightBtnName: this.state.json['20020RECON-000016'], //左侧按钮名称,默认关闭/* 国际化处理： 关闭*/
						leftBtnName: this.state.json['20020RECON-000017'], //右侧按钮名称， 默认确认/* 国际化处理： 确认*/
					})}
				</div>
                <PrintModal
                    noRadio={true}
                    noCheckBox={true}
                    appcode={this.state.appcode}
                    visible={this.state.showPrintModal}
                    handlePrint={this.handlePrint.bind(this)}
                    handleCancel={() => {
                    this.setState({
                        showPrintModal: false
                    })
                    }}
                />
                <PrintOutput                    
                    ref='printOutput'
                    url='/nccloud/gl/verify/verbalanceoutput.do'
                    data={this.state.outputData}
                    callback={this.handleOutput.bind(this)}
                />
                <UploadTax
                    showOrHide={this.state.isQueryShow}
                    handleClose={this.handleCloseTax.bind(this)}
                    taxcloudurl={taxcloudurl}
                    pk_periodscheme={pk_periodscheme}
                    up_pk_accountingbook={up_pk_accountingbook}
                    pk_display={pk_display}
                    //handleUploud={this.handleUploud.bind(this)}
                    period={period}
                    appcode={this.state.appcode}
                />
            </div>
        )
        
    }
}
Reckon = createPage({
   // initTemplate:initTemplate
})(Reckon);
ReactDOM.render(<Reckon />,document.querySelector('#app'));
