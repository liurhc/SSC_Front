import React, { Component } from "react";
import { toast, base, ajax, getMultiLang } from "nc-lightapp-front";
// import Immutable from 'immutable'; 
const { NCFormControl: FormControl, NCDatePicker: DatePicker, NCButton: Button, NCRadio: Radio, NCBreadcrumb: Breadcrumb,
    NCRow: Row, NCCol: Col, NCTree: Tree, NCIcon: Icon, NCLoading: Loading, NCTable: Table, NCSelect: Select,
    NCCheckbox: Checkbox, NCNumber, AutoComplete, NCDropdown: Dropdown, NCPanel: Panel,NCModal:Modal,NCForm,NCTooltip:Tooltip
} = base;
const {  NCFormItem:FormItem } = NCForm;
import ReferLoader from '../../../../public/ReferLoader/index.js';
import {InputItem} from '../../../../public/components/FormItems';
import AccPeriodDefaultTreeGridRef from '../../../../../uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef';
export default class UploadTax extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listItem:{},//模板数据对应值
            pk_periodscheme:"",//上传税务云参数
            up_pk_accountingbook:{},//上传税务云pk
            pk_display:"",//上传税务云账簿名字
            json:{},
            period:'',//上传税务云期间
            taxcloudurl:'',//上传税务云地址
            headRef:{},
            headPeriod:{},
            json:{}
        };
        this.close = this.close.bind(this);
    }
    componentWillMount() {
        let callback= (json) =>{
            this.setState({
                json:json
            },()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['uptoload'],domainName:'gl',currentLocale:'simpchn',callback});
    }
    componentWillReceiveProps(nextProp){
        this.setState({
            headRef:{
                'refpk':nextProp.up_pk_accountingbook.pk_accountingbook,
                'refname':nextProp.pk_display
            },
            headPeriod:{
                'refname':nextProp.period,
                'refpk':nextProp.pk_periodscheme
            },
            taxcloudurl:nextProp.taxcloudurl
        })

    }
    handleUploud=()=>{
        let self = this;
        let {listItem, up_pk_accountingbook, taxcloudurl, pk_display, headPeriod} = this.state;
        up_pk_accountingbook = {'pk_accountingbook':listItem.pk_accountingbook}
        let url = '/nccloud/gl/pfxx/taxcloudconfig.do';
        ajax({
		    url,
		    data:up_pk_accountingbook,
		    success: function (res) {
                taxcloudurl = res.data.url
                self.setState({
                    taxcloudurl,
                    pk_display,
                    headPeriod:{
                        'refname':res.data.period,
                        'refpk':res.data.pk_periodscheme
                    }
                })
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });		        
		    }
        });
    }
    close() {
        this.setState({
            listItem:{}
        })
        this.props.handleClose();
    }
    confirm=()=>{
        let self = this;
        let {listItem,headPeriod} = this.state;
        let {taxcloudurl} = this.props
        let upLound = {}
        if(listItem.pk_accountingbook == undefined){
            upLound.pk_accountingbook = this.props.up_pk_accountingbook.pk_accountingbook
        }else if(listItem.pk_accountingbook == ''){
            upLound.pk_accountingbook = ''
        }else if(listItem.pk_accountingbook != ''){
            upLound.pk_accountingbook = listItem.pk_accountingbook
        }
        if(headPeriod.refname){
            upLound.month = headPeriod.refname.split('-')[1]
            upLound.year = headPeriod.refname.split('-')[0]
        }else{
            upLound.year = ''
        }
        if(taxcloudurl == ''){
            toast({ content: self.state.json['uptoload-000000'], color: 'warning' });/* 国际化处理： 未设置上传地址，请先设置。*/
        }else if(upLound.pk_accountingbook==''){
            toast({ content: self.state.json['uptoload-000001'], color: 'warning' });/* 国际化处理： 核算账簿为必填项*/
        }else if (upLound.year==""){
            toast({ content: self.state.json['uptoload-000002'], color: 'warning' });/* 国际化处理： 会计期间为必填项*/
        }
        else if(taxcloudurl !='' && upLound.pk_accountingbook != 'no' && upLound.year!="no"){            
            let reg=/(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&:/~\+#]*[\w\-\@?^=%&/~\+#])?/;
            if(!reg.test(taxcloudurl)){
                toast({ content: self.state.json['uptoload-000003'], color: 'warning' });/* 国际化处理： 上传地址设置不正确，请重新设置*/
            }else{
                let url = "/nccloud/gl/pfxx/taxcloudupload.do"
                //先不发接口，直接下载出来以便测试查看生成的文件是否正确
                ajax({
                    url,
                    data:upLound,
                    success: function (response){
                        let {result} = response
                        toast({ color: 'success' });
                        self.props.handleClose();
                    },
                    error:function(error){
                        toast({ content: error.message, color: 'warning' });
                    }
                })
                // formDownload({
                //     params: {param : JSON.stringify(upLound)},
                //     url: url,
                //     enctype: 1
                // })
            }
             
        }
    }
    render(){
        let{showOrHide}=this.props;
        return (
            <div className="fl">
                <Modal
                    className={'msg-modal'}
                    show={showOrHide }
                    id="uploadTax"
                    onHide={ this.close }
                    animation={true}
                    >
                    <Modal.Header closeButton>
                        <Modal.Title >{this.state.json['uptoload-000004'] }</Modal.Title>{/* 国际化处理： 上传税务云*/}
                    </Modal.Header >
                    <Modal.Body >
                        <div className="line">
                            <span className="title t_before">{this.state.json['uptoload-000005']}：</span>{/* 国际化处理： 财务核算账簿*/}
                             <ReferLoader
                                tag={'test'}

                                refcode={'uapbd/refer/org/AccountBookTreeRef/index.js'}
                                queryCondition={{
                                     
                                       "TreeRefActionExt":'nccloud.web.gl.ref.AccountBookRefSqlBuilder',
                                       "appcode":this.props.appcode,
                                       "isShowUnit": false,
                                       "isDataPowerEnable": "Y",
                                       "DataPowerOperationCode": "fi",
                                       "treetype": "type",
                                       "isshow": true,
                                       "disabledDataShow": false
                                 }}
                                value={this.state.headRef}
                                onChange={(value)=>{
                                    let {listItem}=this.state
                                    if(value.pk_accountingbook){
                                        listItem.pk_accountingbook=value.pk_accountingbook
                                        this.setState({
                                            headPeriod:{},
                                            headRef:{
                                                'refpk':value.refpk,
                                                'refname':value.refname
                                            },
                                            listItem
                                        })
                                        this.handleUploud()
                                    }else{
                                        listItem.pk_accountingbook=''
                                        this.setState({
                                            headRef:{},
                                            headPeriod:{},
                                            listItem
                                        })
                                    }                             
                                }}                               
                                isMultiSelectedEnabled={false}                               
                            /> 
                        </div>
                        <div className="line">
                            <span className="title t_before">{this.state.json['uptoload-000006']}：</span>{/* 国际化处理： 会计期间*/}
                        <AccPeriodDefaultTreeGridRef
                           // disabled = {selectionState === 'true' ? false : true}
                            value={this.state.headPeriod}
                            queryCondition = {{
                                "pk_accperiodscheme": this.state.headPeriod.refpk?this.state.headPeriod.refpk:this.props.pk_periodscheme,
                                "GridRefActionExt": "nccloud.web.gl.ref.SettledAccperiodGridRefSqlBuilder,nccloud.web.gl.ref.AccperiodGridRefSqlBuilder",
                                "TreeRefActionExt": "nccloud.web.gl.ref.SettledAccperiodTreeRefSqlBuilder,nccloud.web.gl.ref.AccperiodTreeRefSqlBuilder",
                                "pk_accountingbook": this.state.listItem.pk_accountingbook?this.state.listItem.pk_accountingbook:this.props.up_pk_accountingbook.pk_accountingbook
                            }}
                            onChange={(value)=>{
                                let {listItem} = this.state;
                                if(value.refname){
                                    let year = value.refname.split('-')[0];
                                    let month = value.refname.split('-')[1];
                                    listItem.year = year;
                                    listItem.month = month;
                                    this.setState({
                                        listItem,
                                        headPeriod:{
                                            'refname':value.refname,
                                            'refpk':value.refpk
                                        }
                                    })
                                }else{
                                    listItem.year=''
                                    this.setState({
                                        listItem,
                                        headPeriod:{}
                                    })
                                }

                            }}
                        />
                        </div>
                        <div className="line">
                            <span className="title">{this.state.json['uptoload-000007']}：</span>{/* 国际化处理： 上传路径*/}
                            <Tooltip
                                inverse
                                placement='top'
                                overlay={this.state.taxcloudurl}>
                                <InputItem
                                    disabled={true}
                                    //name={item.itemKey}
                                    type="customer"
                                    defaultValue={this.state.taxcloudurl}
                                />
                            </Tooltip>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button colors="primary" onClick={ this.confirm }>{this.state.json['uptoload-000008']}</Button>{/* 国际化处理： 上传*/}
                        <Button onClick={ this.close }>{this.state.json['uptoload-000009']}</Button>{/* 国际化处理： 取消*/}
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}
