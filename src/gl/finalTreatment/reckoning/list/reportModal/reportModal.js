import React, { Component } from 'react';
import {high,base,ajax,getMultiLang } from 'nc-lightapp-front';
const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCPanel:Panel,NCModal:Modal,NCForm,NCButtonGroup:ButtonGroup,NCDiv
} = base;
const {  NCFormItem:FormItem } = NCForm;
import '../index.less';
export default class ReportModal extends Component{
    constructor(props){
        super(props)
        this.columns_report = [
            {
                title: "",
                dataIndex: "strRowOne",
                key: "strRowOne",
                width: 350,
                render:(text, record, index)=>{
                    let {reportTableData}=this.state;
                    let redFlag=false;
                    for(let k in reportTableData.reportDetailFont){
                        if(k==text){
                            redFlag=true;
                        }
                    }
                    if(redFlag){
                        return(
                            <div className="fontColor" fieldid="strRowOne"><pre>{!text?<span>&nbsp;</span>:text}</pre></div>
                        )
                    }else{
                        return(
                            <div fieldid="strRowOne"><pre>{!text?<span>&nbsp;</span>:text}</pre></div>
                        )
                    }
                    
                }
            },
            {
                title: "",
                dataIndex: "strRowTwo",
                key: "strRowTwo",
                width: 350,
                render:(text, record, index)=>{
                    let {reportTableData}=this.state;
                    let redFlag=false;
                    for(let k in reportTableData.reportDetailFont){
                        if(k==text){
                            redFlag=true;
                        }
                    }
                    if(redFlag){
                        return(
                            <div className="fontColor" fieldid="strRowTwo"><pre>{!text?<span>&nbsp;</span>:text}</pre></div>
                        )
                    }else{
                        return(
                            <div fieldid="strRowTwo"><pre>{!text?<span>&nbsp;</span>:text}</pre></div>
                        )
                    }
                    
                }
            }
        ]
        this.state={
            json:{},
            reportTableData:[],
            reportTableDataSum:[],
            reportTableTitle:'',
            fontStr:{},
            columns_report:this.columns_report,
            columns_reportSum:[],
            sumOrDetail:'sum'//默认像是汇总table数据
        }
    }
    componentWillMount() {
        let callback= (json) =>{
            this.columns_reportSum = [
                {
                    title: (<div fieldid="strRowOne" className='checkbox-mergecells'>{json['20020RECON-000023']}</div>),/* 国际化处理： 检查项*/
                    dataIndex: "strRowOne",
                    key: "strRowOne",
                    width: 300,
                    render:(text, record, index)=>{
                        let {reportTableDataSum}=this.state;
                        let redFlag=false;
                        for(let k in reportTableDataSum.reportSumFont){
                            if(k==text){
                                redFlag=true;
                            }
                        }
                        if(redFlag){
                            return(
                                <div className="fontColor" fieldid="strRowOne"><pre>{!text?<span>&nbsp;</span>:text}</pre></div>
                            )
                        }else{
                            return(
                                <div fieldid="strRowOne"><pre>{!text?<span>&nbsp;</span>:text}</pre></div>
                            )
                        }
                        
                    }
                },
                {
                title: (<div fieldid="strRowTwo" className='checkbox-mergecells'>{json['20020RECON-000024']}</div>),/* 国际化处理： 是否通过*/
                    dataIndex: "strRowTwo",
                    key: "strRowTwo",
                    width: 80,
                    render:(text, record, index)=>{
                        let {reportTableDataSum}=this.state;
                        let redFlag=false;
                        for(let k in reportTableDataSum.reportSumFont){
                            if(k==text){
                                redFlag=true;
                            }
                        }
                        if(redFlag){
                            return(
                                <div className="fontColor" fieldid="strRowTwo"><pre>{!text?<span>&nbsp;</span>:text}</pre></div>
                            )
                        }else{
                            return(
                                <div fieldid="strRowTwo"><pre>{!text?<span>&nbsp;</span>:text}</pre></div>
                            )
                        }
                        
                    }
                },
                {
                title: (<div fieldid="strRowThree" className='checkbox-mergecells'>{json['20020RECON-000025']}</div>),/* 国际化处理： 说明*/
                    dataIndex: "strRowThree",
                    key: "strRowThree",
                    width: 310,
                    render:(text, record, index)=>{
                        let {reportTableDataSum}=this.state;
                        let redFlag=false;
                        for(let k in reportTableDataSum.reportSumFont){
                            if(k==text){
                                redFlag=true;
                            }
                        }
                        if(redFlag){
                            return(
                                <div className="fontColor" fieldid="strRowThree"><pre>{!text?<span>&nbsp;</span>:text}</pre></div>
                            )
                        }else{
                            return(
                                <div fieldid="strRowThree"><pre>{!text?<span>&nbsp;</span>:text}</pre></div>
                            )
                        }
                        
                    }
                }
            ]
          this.setState({
            columns_reportSum:this.columns_reportSum,
            json:json,
          },()=>{
          })
        }
        getMultiLang({moduleId:'20020RECON',domainName:'gl',currentLocale:'simpchn',callback});
      }
    componentWillReceiveProps(nextProp){
        let {reportTableDataSum,reportTableData,reportTableTitle}=this.state;
        //let {reportDataSum,reportData,reportTitle,reportFont}=nextProp;
        let{reportSum,reportDetail,showOrHide}=nextProp;
        reportTableDataSum=reportSum;
        reportTableData=reportDetail;
        // reportTableData=reportData;
        // reportTableDataSum=reportDataSum;
        // reportTableTitle=reportTitle;
        // fontStr=reportFont
        this.setState({
            reportTableDataSum,reportTableData,
          //  sumOrDetail:'detail'
            //,reportTableTitle,fontStr
        })
    }
    componentDidMount(){

    }
    //关闭
    close() {
        this.props.handleClose();
    }
    //返回
    comeBack=()=>{
        let {sumOrDetail}=this.state;
        sumOrDetail='sum';
        this.setState({
            sumOrDetail
        })
    }
    //查看详情
    queryReportDetail=()=>{
        let {sumOrDetail}=this.state;
        sumOrDetail='detail';
        this.setState({
            sumOrDetail
        })
    }
    //打印
    confirm=()=>{
        this.props.handlePrintReport();
        // let {sumOrDetail}=this.state;
        // sumOrDetail='detail';
        // setTimeout(() => {
        //     this.setState({
        //         sumOrDetail
        //     })
        // }, 0);
        
    }
    //模板输出
    outPutModule=()=>{
        this.props.showOutputModal();
    }
    render(){
        let {sumOrDetail,reportTableDataSum,reportTableData,reportTableTitle,columns_report,columns_reportSum}=this.state;
        let{showOrHide}=this.props;
        return(
            <div id='finalTreatment_report'>
                <Modal
                    fieldid="query"
                    className={'msg-modal tryto'}
                    show={showOrHide}
                    // backdrop={ this.state.modalDropup }
                    onHide={this.close.bind(this)}
                    animation={true}
                >
                    <Modal.Header closeButton fieldid="header-area">
                        <Modal.Title>{this.state.json['20020RECON-000026']}</Modal.Title>{/* 国际化处理： 结账报告*/}
                    </Modal.Header >
                    <Modal.Body >

                        <ButtonGroup style={{ margin: 10 }}>
                            {sumOrDetail == 'detail' ?
                                <Button colors="primary" onClick={this.comeBack.bind(this)} fieldid="comeback">{this.state.json['20020RECON-000027']}</Button>/* 国际化处理： 返回*/
                                : null}
                            {sumOrDetail == 'sum' ?
                                <Button colors="primary" onClick={this.queryReportDetail.bind(this)} className="detail" fieldid="detailea">{this.state.json['20020RECON-000028']}</Button>/* 国际化处理： 查看详情*/
                                : null}
                            {sumOrDetail == 'detail' ?
                                <Button colors="" onClick={this.confirm.bind(this)} fieldid="print">{this.state.json['20020RECON-000029']}</Button>/* 国际化处理： 打印*/
                                : null}
                            {sumOrDetail == 'detail' ?
                                <Button colors="" onClick={this.outPutModule.bind(this)} fieldid="output">{this.state.json['20020RECON-000030']}</Button>/* 国际化处理： 模板输出*/
                                : null}
                            {/* <Button colors="primary" onClick={this.close.bind(this)}>关闭</Button> */}
                        </ButtonGroup>
                        <div class='reportTitle'>{reportTableData.reportDetailTitle} </div>
                        {(sumOrDetail == 'sum') ?
                            <div class='reportSumTitle'>{reportTableDataSum.reportSumTitle} </div>
                            : null}
                        {(sumOrDetail == 'sum') ?
                        <NCDiv fieldid="reportTable_sum" areaCode={NCDiv.config.TableCom}>
                            <Table
                                columns={columns_reportSum}
                                bordered
                                data={reportTableDataSum.reportSumData}
                                scroll={{ x: columns_reportSum.length > 8 ? (100 + (columns_reportSum.length - 8) * 15) + "%" : '100%', y: 320 }}
                            />
                            </NCDiv> : null}
                        {(sumOrDetail == 'detail') ?
                        <NCDiv fieldid="reportTable_detail" areaCode={NCDiv.config.TableCom}>
                            <Table
                                columns={columns_report}
                                bordered
                                data={reportTableData.reportDetailData}
                                scroll={{ x: columns_report.length > 8 ? (100 + (columns_report.length - 8) * 15) + "%" : '100%', y: 320 }}
                            />
                            </NCDiv> : null}
                    </Modal.Body>
                    <Modal.Footer>
                        {/* <Button colors="primary" onClick={ this.confirm }> 确定 </Button> */}
                        {/* <Button onClick={ this.close }> 关闭 </Button> */}
                    </Modal.Footer>
                </Modal>

            </div>
        )
    }
}
