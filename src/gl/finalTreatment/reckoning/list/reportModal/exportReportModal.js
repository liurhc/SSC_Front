import React, { Component } from 'react';
import {high,base,ajax,getMultiLang } from 'nc-lightapp-front';
const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCPanel:Panel,NCModal:Modal,NCForm,NCButtonGroup:ButtonGroup,NCDiv
} = base;
const {  NCFormItem:FormItem } = NCForm;
import { toast } from '../../../../public/components/utils.js';
import '../index.less'
export default class ExportReportModal extends Component{
    constructor(props){
        super(props)
        this.columns_report = [
            {
                title: "",
                dataIndex: "strRowOne",
                key: "strRowOne",
                width: 100,
                render:(text, record, index)=>{
                    let {fontStr}=this.state;
                    let redFlag=false;
                    for(let k in fontStr){
                        if(k==text){
                            redFlag=true;
                        }
                    }
                    if(redFlag){
                        return(
                            <div className="fontColor" fieldid="strRowOne">{!text?<span>&nbsp;</span>:text}</div>
                        )
                    }else{
                        return(
                            <div fieldid="strRowOne">{!text?<span>&nbsp;</span>:text}</div>
                        )
                    }
                    
                }
            },
            {
                title: "",
                dataIndex: "strRowTwo",
                key: "strRowTwo",
                width: 100,
                render:(text, record, index)=>{
                    let {fontStr}=this.state;
                    let redFlag=false;
                    for(let k in fontStr){
                        if(k==text){
                            redFlag=true;
                        }
                    }
                    if(redFlag){
                        return(
                            <div className="fontColor" fieldid="strRowTwo">{!text?<span>&nbsp;</span>:text}</div>
                        )
                    }else{
                        return(
                            <div fieldid="strRowTwo">{!text?<span>&nbsp;</span>:text}</div>
                        )
                    }
                    
                }
            }
        ]
        this.state={
            json:{},
            reportTableData:[],
            reportTableTitle:'',
            fontStr:{},
            accountingbook:'',
            columns_report:this.columns_report
        }
    }
	componentWillMount() {
		let callback= (json) =>{
		  this.setState({
			  json:json,
		},()=>{			  
			//initTemplate.call(this, this.props);
		  })
		}
		getMultiLang({moduleId:'20020RECON',domainName:'gl',currentLocale:'simpchn',callback});
		if(this.state.ref1.refpk!=""){
			this.props.button.setButtonDisabled('add',false);
		}
	}
    componentWillReceiveProps(nextProp){
        let self=this;
        let {reportTableData,reportTableTitle,fontStr,accountingbook}=self.state;
        let {pk_accountingbook,period}=nextProp.data;
        if(pk_accountingbook==""||pk_accountingbook==undefined){
            toast({ content: self.state.json['20020RECON-000010'], color: 'warning' });/* 国际化处理： 请先选择核算账簿*/
            return false;
        }
        if (pk_accountingbook !== this.state.accountingbook ) {
         
            let url ='/nccloud/gl/reckoning/closereport.do';
            let pk_accpont = {"pk_accountingbook":pk_accountingbook,"period":period};
            ajax({
                url:url,
                data:pk_accpont,
                success: function(response){
                    const { data,success } = response;
                    //渲染已有账表数据遮罩
                    if (success) {
                        if(response.data){
                            reportTableData=data.reportMesg;
                            reportTableTitle=data.disMesg;
                            fontStr=data.font;
                            self.setState({
                                reportTableData,reportTableTitle,fontStr,
                                accountingbook:pk_accountingbook
                            })
                        }
                    }   
                }
            });
        }
    }
    componentDidMount(){
        
    }
    print(cfg={}){//打印
       

    }
    
    printPreVIew(cfg={}){//预览
        

    }
    
    output(cfg={}){//输出
       
    }
    // //关闭
    // close() {
    //     this.props.handleClose();
    // }
    // //打印
    // confirm=()=>{
    //     this.props.handlePrintReport();
    // }
    render(){
        let {reportTableData,reportTableTitle,columns_report}=this.state;
        return(
            <div id='finalTreatment_report'>

                <div class='reportTitle'>{reportTableTitle} </div>
                {/* <div class='btn'>
                    <ButtonGroup style={{ margin: 10 }}>
                        <Button colors="primary" onClick={this.confirm.bind(this)}>打印</Button>
                        <Button colors="primary" onClick={this.close.bind(this)}>关闭</Button>
                    </ButtonGroup>
                </div> */}
                <NCDiv fieldid="reportTable" areaCode={NCDiv.config.TableCom}>
                <Table 
                    columns={columns_report} 
                    bordered 
                    data={reportTableData}
                    scroll={{ x: columns_report.length > 8 ? (100+(columns_report.length-8)*15)+"%": '100%', y: 320 }}
                />
                </NCDiv>
            </div>
        )
    }
}
// export default function (props = {}) {
//     var conf = {
//     };

//     return <ExportReportModal {...Object.assign(conf, props)} />
// }
