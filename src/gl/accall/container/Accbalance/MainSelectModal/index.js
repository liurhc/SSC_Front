import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';
// import moment from 'moment';

import {high,base,ajax,deepClone, createPage, getMultiLang } from 'nc-lightapp-front';
import './index.less';


const { Refer } = high;

const { NCFormControl: FormControl,NCDatePicker:DatePicker,NCButton: Button,NCRadio:Radio,NCBreadcrumb:Breadcrumb,
NCRow:Row,NCCol:Col,NCTree:Tree,NCMessage:Message,NCIcon:Icon,NCLoading:Loading,NCTable:Table,NCSelect:Select,
NCCheckbox:Checkbox,NCNumber,AutoComplete,NCDropdown:Dropdown,NCModal: Modal,
} = base;

import { toast } from '../../../../public/components/utils';


// const deepClone = require('../../../../public/components/deepClone');
const format = 'YYYY-MM-DD';
// import AccountBookByFinanceOrgRef from '../../../../../uapbd/refer/org/AccountBookByFinanceOrgRef';

// import AccperiodMonthTreeGridRef from '../../../../../uapbd/refer/pubinfo/AccperiodMonthTreeGridRef';
import AccPeriodDefaultTreeGridRef from '../../../../../uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef';
import BusinessUnitTreeRef from '../../../../../uapbd/refer/orgv/BusinessUnitVersionDefaultAllTreeRef';

import AccountDefaultModelTreeRef from '../../../../../uapbd/refer/fiacc/AccountDefaultModelTreeRef';
import VoucherTypeDefaultGridRef from '../../../../../uapbd/refer/fiacc/VoucherTypeDefaultGridRef';
import OperatorDefaultRef from '../../../../refer/voucher/operatorDefaultRef';
import initTemplate from '../../../../manageReport/modalFiles/initTemplate';
import createScript from '../../../../public/components/uapRefer.js';
import {
    businessUnit,
    createReferFn,
    getCheckContent,
    getReferDetault, renderLevelOptions,
    renderRefer, returnMoneyType
} from "../../../../manageReport/common/modules/modules";
import {subjectRefer} from "../../../../manageReport/referUrl";
import SubjectVersion from "../../../../manageReport/common/modules/subjectVersion";
import DateGroup from '../../../../manageReport/common/modules/dateGroup';
import SubjectLevel from '../../../../manageReport/common/modules/subjectAndLevel';
import {clearTransterData, handleChange, handleSelectChange} from "../../../../manageReport/common/modules/transferFn";
class CentralConstructorModal extends Component {

	constructor(props) {
		super(props);
		this.state={
			appcode: "20023020",
            json: {},
            selectedKeys: [],
            targetKeys: [],

            changeParam: true, //false:点击条件时不触发接口，,
            rightSelectItem: [], //科目的"条件"按钮中存放右侧数据的pk值

            showRadio: true,//控制是否显示"会计期间"的但选框；false: 不显示
            start: {},  //开始期间
            end: {}, //结束期间
            /*****会计期间******/
            selectionState: 'true', //按期间查询true，按日期查询false
            startyear: '', //开始年
            startperiod: '',  //开始期间
            endyear: '',  //结束年
            endperiod: '',   //结束期间
            /*****日期******/
            begindate: '', //开始时间
            enddate: '', //结束时间
            rangeDate: [],//时间显示
			accountingbook: {
				refname: '',
				refpk: '',
			},  //核算账簿
			accasoa: {           //科目
				refname:'',
				refpk: '',
			},
			buSecond: {},
			vouchertype: {
				refname:'',
				refpk: '',
			},
			configs: {},//人员参照
			versiondateList: [], //科目版本列表
			accountlvl: '1', //设置级次1到几
			selectedQueryValue: '0', //查询方式单选
			selectedCurrValue: '1', //返回币种单选
			ismain: '0', // 0主表  1附表
			currtype: '',      //选择的币种
			currtypeName: '',  //选择的币种名称
			currtypeList: [],  //币种列表
			defaultCurrtype: {         //默认币种
				name: '',
				pk_currtype: '',
			},
			checkbox: {
				one: false,
				two: false,
				three: false,
			},
			talliedscope: '1',
			balanceori: '-1', //余额方向
			startlvl: '1', //开始级次
			endlvl: '1',  //结束级次
			startInput: '', //凭证开始数字
			endInput: '', //凭证结束数字
			makePerson:{  //制单人
			    refcode:"",
			    refname:"",
			    refpk:""
			},
			startcode: {
				refname:'',
				refpk: '',
				refcode: '',
			}, //开始科目编码
			endcode: {
				refname:'',
				refpk: '',
				refcode: '',
			},    //结束科目编码
			versiondate: '', //制单日期，后续要用下拉选
			isleave: false, //是否所有末级
			isoutacc: false, //是否表外科目
			includeuntally: false, //包含凭证的4个复选框 未记账
			includeerror: false, //包含凭证的4个复选框 错误
            includeerrorEdit: true,

			includetranfer: true, //包含凭证的4个复选框 损益结转
			includereclassify: false, //包含凭证的4个复选框 重分类
			mutibook: 'N', //多主体显示方式的选择
			disMutibook: true, //多主体显示是否禁用
			currplusacc: 'Y', //排序方式
			disCurrplusacc: true, //排序方式是否禁用
			showzerooccur: false, //无发生不显示
			showzerobalanceoccur: true, //无余额无发生不显示
			sumbysubjtype: false, //科目类型小计
			disSumbysubjtype: true, //科目类型小计是否禁用
			twowaybalance: false, //借贷方显示余额
			showSecond: false, //是否显示二级业务单元
			multbusi: false, //多业务单元复选框
			isversiondate: false,
			pk_accperiodscheme: '', //会计期间参照用的过滤
			begintime: '', 			
			endtime: '', 
			timeStyle: '1', 		
		}
		this.renderRefer = renderRefer.bind(this);
        this.searchId = '20023020query';
	}

    componentWillMount() {
        let callback= (json) =>{
            this.setState({json:json},()=>{
                // initTemplate.call(this, this.props);
                // this.loadDept();
                //this.tableDefaultData=deepClone(tableDefaultData);
            })
        }
        getMultiLang({moduleId:['20023020', 'dategroup', 'subjectandlevel', 'subjectversion', 'childmodules'],domainName:'gl',currentLocale:'simpchn',callback});
    }


	componentDidMount() {
        setTimeout(() => getReferDetault(this, false, {
            businessUnit
        }),0);
        setTimeout(() => initTemplate.call(this,this.props), 0)
	}

	componentWillReceiveProps(nextProps) {
		// this.getEndAccount(nextProps);
	}

	handleRadioChange(value) {
		this.setState({selectedQueryValue: value});
	}

	handleRadioCurrChange(value) {
		this.setState({selectedCurrValue: value});
	}

	//记账范围变化
	handleRadioScopeChange(value) {
		//(value)
		let includeerror = false;
		if (value == '1') {
			this.setState({
                includeerror: false,
                includeerrorEdit: true,
			})

		} else {
			this.setState({
                includeerror: true,
                includeerrorEdit:false
			})
		}
		this.setState({
			talliedscope: value,
		});
	}
	
	//排序方式单选变化
	handleCurrplusacc(value) {
		this.setState({currplusacc: value});
	}
	

	getlvlver() {
		let self = this;
		let url = '/nccloud/gl/glpub/accountinfoquery.do';

		let data = {
			pk_accountingbook: this.state.accountingbook.refpk,
		};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		    	// 默认取科目版本列表最后一个
		        const { data, error, success } = res;

		        if(success){
		        	
					self.setState({
						accountlvl: data.accountlvl,
						versiondateList: data.versiondate,
						versiondate: data.versiondate[data.versiondate.length - 1],
						pk_accperiodscheme: data.pk_accperiodscheme,
					})
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});


	}






	getCurrtypeList = () => {
		let self = this;
		let url = '/nccloud/gl/voucher/queryAllCurrtype.do';
		// let data = '';
		let data = {
			"localType":"1",
		};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		        const { data, error, success } = res;
		        if(success){
					self.setState({
						currtypeList: data,
						currtype: this.state.json['20023020-000002'],/* 国际化处理： 本币*/
					})
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});

	}

	handleBalanceoriChange(value) {
		this.setState({balanceori: value});
	}

	createCfg = (id, param) => {//
	    var obj = {
	        value: this.state.configs[id] ? this.state.configs[id].value : [],
	        onChange: function (val) {
	            var temp = Object.assign(this.state.configs[id], { value: val });
	            this.setState(Object.assign(this.state.configs, temp));
	        }.bind(this)
	    }
	    this.state.configs[id] = obj;
	    var result_param = Object.assign(obj, param)
	    return result_param;
	}


	handleCurrtypeChange(value) {
		let self = this;
		// if (!this.state.accountingbook || !this.state.accountingbook.refpk) {
		// 	toast({ content: '请先选择核算账簿', color: 'warning' });
		// 	return;
		// }
		let currtypeList = this.state.currtypeList;
		currtypeList.forEach(function (item, index) {
			if (item.pk_currtype == value ) {
				self.setState({
					currtypeName: item.name,
				})
			}
		})

		this.setState({
			currtype: value,
		// }, this.getTables);
		}, this.getRates);
	}


	handleIsmainChange(value) {
		this.setState({
			ismain: value,
		})
	}
	
	//根据核算账簿获取相关信息
	getInfoByBook(type) {
		// //(this.state.accountingbook)


		if (!this.state.accountingbook || !this.state.accountingbook.refpk) {
			//如果是清空了核算账簿，应该把相关的东西都还原
			return;
		}

		this.getlvlver();
		
		let self = this;
		let url = '/nccloud/gl/voucher/queryAccountingBook.do';
		let data = {
			pk_accountingbook: this.state.accountingbook.refpk,	
			year: '',		
		};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		    	const { data, error, success } = res;
		    	//(res)
		    	// return
		        if(success){
		        	let isStartBUSecond = data.isStartBUSecond;
		        	let showSecond = false;
		        	let disMutibook = true;
		        	



			        	if (isStartBUSecond) {
							showSecond = true;
						} else {
							showSecond = false;
						}

		        	self.setState({
		        		isStartBUSecond: data.isStartBUSecond,
		        		showSecond,
		        		disMutibook,
		        	},)
		        } else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },

		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });		        
		    }
		});
	}

	//开始级次变化
	handleStartlvlChange(value) {
		//(value)
		this.setState({
			startlvl: value,
		})
	}
	
	//结束级次变化
	handleEndlvlChange(value) {
		//(value)
		this.setState({
			endlvl: value,
		})
	}
	
	//科目版本下拉变化
	handleVersiondateChange(value) {
		this.setState({
			versiondate: value,
		})
	}

	getTime(period) {
		//(period)
		let self = this;
		let url = '/nccloud/gl/glpub/queryDateByPeriod.do';

		let data = {
			pk_accountingbook: this.state.accountingbook.refpk,
			period:period,
		};

		ajax({
		    loading: true,
		    url,
		    data,
		    success: function (res) {
		    	// 默认取科目版本列表最后一个
		        const { data, error, success } = res;

		        if(success){

		        	
					self.setState({
						endtime: data.enddate,
					})
		            
		        }else {
		            toast({ content: error.message, color: 'warning' });
		        }
		    },
		    error: function (res) {
		    	toast({ content: res.message, color: 'warning' });
		        
		    }
		});

	}

	onBegintimeChange(d) {
		this.setState({
			begintime:d
		})
	}

	onEndtimeChange(d) {
		this.setState({
			endtime:d
		})
	}
	
	//所有末级复选框变化
	onChangeIsleave(e) {
	    //(e);
	    let isleave = deepClone(this.state.isleave);
	   
	    this.setState({isleave: e});
	}

	//表外科目复选框变化
	onChangeIsoutacc(e) {  
	    this.setState({isoutacc: e});
	}

	//启用科目版本复选框变化
	onChangeIsversiondate(e) {  
	    this.setState({isversiondate: e});
	}


	//未记账凭证复选框变化
	onChangeIncludeuntally(e) {  
	    this.setState({includeuntally: e});
	}

	//错误凭证复选框变化
	onChangeIncludeerror(e) {
		this.setState({includeerror: e});
	}

	//损益结转凭证复选框变化
	onChangeincludetranfer(e) {
		this.setState({includetranfer: e});
	}

	//损益结转凭证复选框变化
	onChangeincludereclassify(e) {
		this.setState({includereclassify: e});
	}

	//多主体显示单选框变化
	handleMutibookChange(e) {
		this.setState({mutibook: e});
	}

	//无发生不显示复选框变化
	onChangeShowzerooccur(e) {
		this.setState({showzerooccur: e});
	}

	//无余额无发生不显示复选框变化
	onChangeShowzerobalanceoccur(e) {
		this.setState({showzerobalanceoccur: e});
	}

	//无发生不显示复选框变化
	onChangeSumbysubjtype(e) {
		this.setState({sumbysubjtype: e});
	}


	//借贷方显示余额复选框变化
	onChangeTwowaybalance(e) {
		this.setState({twowaybalance: e});
	}

	//合并业务单元复选框变化
	onChangeMultbusi(e) {
		let disCurrplusacc = true;
		if (e === true) {
			disCurrplusacc = false;
		} else {
			
		}

		this.setState({
			multbusi: e,
			disCurrplusacc,

		});
	}
	
	//凭证号后数字变化
	onInputChange(key, e) {
		//(key)
		//(e)
		if (key == 'start') {
			this.state.startInput = e;
		} else {
			this.state.endInput = e;

		}
	}

	changeCheck=()=> {
		// this.setState({checked:!this.state.checked});
	}

	
	//起始时间变化
	onStartChange(d) {
		//(d)
		this.setState({
			begindate:d
		})
	}
	
	//结束时间变化
	onEndChange(d) {
		this.setState({
			enddate:d
		})
	}

	handleGTypeChange =() =>{
		
	};

	handleRadioTimestyleChange(value) {
		//(value)
		this.setState({
			timeStyle: value,
		})
	}
    handleValueChange = (key, value, param) => {
        //('handleValueChange>>', key, value);
        this.setState({
            [key]: value
        })
        if(param === 'SubjectVersion' || param === 'accbalance') {
            this.setState({
                changeParam: true,
                rightSelectItem: []
            })
        }
    }
    handleDateChange = (value) => {//日期: 范围选择触发事件
        //('handleDateChange:', value);
        this.setState({
            begindate: value[0],
            enddate: value[1],
            rangeDate: value
        })
    }

    handleSelect = (value) => {//
        //('handleSelect', value);

        this.setState({
            rightSelectItem: [...value]
        })
    }
    renderModalList = () => {
		return (
            <div className='right_query'>
                <div className='query_body1'>
                    <div className='query_form'>
                <Row className="myrow">
                    <Col md={2} sm={2}>
						<span style={{color: 'red'}}>*</span>
                        <span className='nc-theme-form-label-c'>{this.state.json['20023020-000006']}：</span>{/* 国际化处理： 核算账簿*/}
                    </Col>
                    <Col md={10} sm={10}>
                        <div className="book-ref">
                            {
                                createReferFn(
                                    this,
                                    {
                                        url: 'uapbd/refer/org/AccountBookTreeRef/index.js',
                                        value: this.state.accountingbook,
                                        referStateKey: 'checkAccountBook',
                                        referStateValue: this.state.checkAccountBook,
                                        stateValueKey: 'accountingbook',
                                        flag: false,
                                        queryCondition: {
                                            pk_accountingbook: this.state.accountingbook && this.state.accountingbook.refpk
                                        }
                                    },
                                    {
                                        businessUnit: businessUnit
                                    },
                                    'accall'
                                )
                            }
                        </div>

                    </Col>
                </Row>
                {
                    this.state.isShowUnit ?
                        <Row className="myrow">
                            <Col md={2} sm={2}>
                                <span className='nc-theme-form-label-c'>{this.state.json['20023020-000005']}：</span>
                            </Col>
                            <Col md={10} sm={10}>
                                <div className="book-ref">
                                    <BusinessUnitTreeRef
										fieldid='buSecond'
                                        value={this.state.buSecond}
                                        isShowDisabledData = {true}
                                        isHasDisabledData = {true}
                                        queryCondition = {{
                                            "pk_accountingbook": this.state.accountingbook && this.state.accountingbook.refpk,
                                            "isDataPowerEnable": 'Y',
                                            "DataPowerOperationCode" : 'fi',
                                            "TreeRefActionExt":'nccloud.web.gl.ref.GLBUVersionWithBookRefSqlBuilder'
                                        }}
                                        onChange={(v) => {
                                            //(v);
                                            this.setState({
                                                buSecond: v,
                                            }, () => {
                                                if (this.state.accountingbook && this.state.accountingbook.refpk) {
                                                }
                                            })
                                        }
                                        }
                                    />

                                </div>

                            </Col>
                        </Row> : ''
                }



                {/*会计期间*/}
                <DateGroup
                    selectionState = {this.state.selectionState}
                    start = {this.state.start}
                    end = {this.state.end}
                    enddate = {this.state.enddate}
                    pk_accperiodscheme = {this.state.pk_accperiodscheme}
                    pk_accountingbook = {this.state.accountingbook}
                    rangeDate = {this.state.rangeDate}
                    handleValueChange = {this.handleValueChange}
                    handleDateChange = {this.handleDateChange}
                    self = {this}
                    showRadio = {this.state.showRadio}
                />

                {/*科目，级次：*/}
                <SubjectLevel
                    parent = {this}
                    accountingbook = {this.state.accountingbook}
                    isMultiSelectedEnabled = {false}//控制多选单选

                    selectedKeys = {this.state.selectedKeys}
                    targetKeys = {this.state.targetKeys}
                    changeParam = {this.state.changeParam}
                    handleSelect = {this.handleSelect}
                    handleValueChange = {this.handleValueChange}

                    handleSelectChange = {(sourceSelectedKeys, targetSelectedKeys) => handleSelectChange(this, sourceSelectedKeys, targetSelectedKeys)}
                    handleChange = {(nextTargetKeys, direction, moveKeys) => handleChange(this,nextTargetKeys, direction, moveKeys) }
                    clearTransterData = {() => clearTransterData(this)}
                />

                {/*启用科目版本：*/}
                <SubjectVersion
                    checked = {this.state.isversiondate}//复选框是否选中
                    data={this.state.versionDateArr}//
                    value={this.state.versiondate}
                    disabled={!this.state.isversiondate}
                    handleValueChange = {(key, value) => this.handleValueChange(key, value, 'SubjectVersion')}
                />

                <Row className="myrow">
                    <Col md={2} sm={2}>
                        <span className='nc-theme-form-label-c'>{this.state.json['20023020-000007']}：</span>{/* 国际化处理： 凭证号*/}
                    </Col>

                    {/*凭证类别参照*/}
                    <Col md={4} sm={4}>
                        <div className="book-ref">
                            <VoucherTypeDefaultGridRef
								fieldid='vouchertype'
                                value={this.state.vouchertype}
                                onChange={(v)=>{
                                    //(v);
                                    this.setState({
                                        vouchertype: v,
                                    }, )
                                }
                                }
                                queryCondition = {() => {
                                    //查询条件不写成数组，而是取核算账簿数组的第一个
                                    return {
                                        "pk_accountingbook": this.state.accountingbook.refpk,  //单选时
                                        "isDataPowerEnable": 'Y',
                                        "DataPowerOperationCode" : 'fi',
										"pk_org": this.state.accountingbook && this.state.accountingbook.refpk
                                    }
                                }}
                            />

                        </div>
                    </Col>

                    <Col md={2}  style={{marginLeft:20}} >
                        <NCNumber
							scale={0}
							fieldid='startNum'
							// placeholder="请输入数量"
                            value={this.state.startInput}
                            onChange={this.onInputChange.bind(this, 'start')}
                        />

                    </Col>

                    <Col md={1} sm={1}>
						<p className='zhi nc-theme-form-label-c'>{this.state.json['20023020-000008']}</p>{/* 国际化处理： 至*/}
                    </Col>

                    <Col md={2} >
                        <NCNumber
							scale={0}
							fieldid='endNum'
							// placeholder="请输入数量"
                            value={this.state.endInput}
                            onChange={this.onInputChange.bind(this, 'end')}
                        />
                    </Col>

                </Row>

                <Row >
                    <Col md={2} sm={2}>
                        <span className='nc-theme-form-label-c'>{this.state.json['20023020-000009']}：</span>{/* 国际化处理： 制单人*/}
                    </Col>
                    <Col md={4} sm={4}>
                        <div className="book-ref">

                            {OperatorDefaultRef({} = this.createCfg("OperatorDefaultRef", {
                                // isMultiSelectedEnabled: true,
                                queryCondition: () => {
                                    let condition = {
                                        pk_accountingbook : this.state.accountingbook.refpk,
                                        user_Type : '0',
                                        GridRefActionExt:'nccloud.web.gl.ref.OperatorRefSqlBuilder',
                                    }
                                    return condition;
								},
								fieldid: 'makePerson',
                                value:{refname:this.state.makePerson.refname,
                                    refpk:this.state.makePerson.refpk},
                                onChange: (v)=>{
                                    let makePerson = v;
                                    this.setState({
                                        makePerson,
                                    })
                                }
                            }))}

                        </div>

                    </Col>

                </Row>


                <Row className="myrow sort-area">
                    <Col md={2} sm={2}>
                        <span className='nc-theme-form-label-c'>{this.state.json['20023020-000010']}：</span>{/* 国际化处理： 记账范围*/}
                    </Col>
                    <Col md={6} sm={6}>
                        <Radio.NCRadioGroup
                            selectedValue={this.state.talliedscope}
                            onChange={this.handleRadioScopeChange.bind(this)}
                        >
                            <Radio value="1">
								{this.state.json['20023020-000011']} {/* 国际化处理： 已记账*/}
							</Radio>
                            <Radio value="2">
								{this.state.json['20023020-000012']} {/* 国际化处理： 未记账*/}
							</Radio>
                            <Radio value="0">
								{this.state.json['20023020-000013']} {/* 国际化处理： 全部*/}
							</Radio>
                        </Radio.NCRadioGroup>
                    </Col>
					<Col md={4} sm={4} className='sort-checkbox'>
						<Checkbox
							colors="dark"
							checked={this.state.includeerror}
							disabled={this.state.includeerrorEdit}
							onChange={this.onChangeIncludeerror.bind(this)}
						>
							{this.state.json['20023020-000014']} {/* 国际化处理： 错误凭证*/}
						</Checkbox>
					</Col>

                </Row>

                <Row className="myrow">

                    <Col md={2} sm={2}>
                        <span className='nc-theme-form-label-c'>{this.state.json['20023020-000003']}</span>{/* 国际化处理： 包含凭证*/}
                    </Col>

                    <Col md={4} sm={4}>
                        <Checkbox colors="dark"  checked={this.state.includetranfer}  onChange={this.onChangeincludetranfer.bind(this)} >
							{this.state.json['20023020-000015']} {/* 国际化处理： 损益结转凭证*/}
						</Checkbox>
                    </Col>

                    <Col md={4} sm={4}>
                        <Checkbox colors="dark"  checked={this.state.includereclassify}  onChange={this.onChangeincludereclassify.bind(this)} >
							{this.state.json['20023020-000016']} {/* 国际化处理： 重分类凭证*/}
						</Checkbox>
                    </Col>

                </Row>

                {/*返回币种：*/}
                {
                    returnMoneyType(
                        this,
                        {
                            key: 'selectedCurrValue',
                            value: this.state.selectedCurrValue,
                            groupEdit: this.state.groupCurrency,
                            gloableEdit: this.state.globalCurrency
                        },
						this.state.json
                    )
                }

                <Row className="myrow">
                    <Col md={2} sm={2}>
                        <span className='nc-theme-form-label-c'>{this.state.json['20023020-000017']}：</span>{/* 国际化处理： 排序方式*/}
                    </Col>
                    <Col md={9} sm={9}>
                        <Radio.NCRadioGroup
                            name="fruit"
                            selectedValue={this.state.currplusacc}
                            onChange={this.handleCurrplusacc.bind(this)}
                        >
                            <Radio value="Y" id='moneySubject'>
								{`${this.state.json['20023020-000018']}+${this.state.json['20023020-000019']}`}  {/* 国际化处理： 币种,科目*/}
							</Radio>
                            <Radio value="N" id='subjectMoney'>
								{`${this.state.json['20023020-000019']}+${this.state.json['20023020-000018']}`} {/* 国际化处理： 科目,币种*/}
							</Radio>
                        </Radio.NCRadioGroup>

                    </Col>
                </Row>
			</div></div>
			</div>
		)
	}
    clickPlanEve = (value) => {
        //('clickPlanEve:???', 'value>',this, value, '>')
        this.state = deepClone(value.conditionobj4web.nonpublic);
        //('clickPlanEve:???12', this.state)
        this.setState(this.state)
    }
    saveSearchPlan = () => {//点击《保存方案》按钮触发事件
        //('saveSearchPlan>>', this.state)
        return {...this.state}
    }
    clickSearchBtn = () => {
		//('clickSearchBtn>>>')
		let data = {
			pk_accountingbook: this.state.accountingbook.refpk,
			pk_unit: this.state.buSecond.refpk,
			unitname: this.state.buSecond.refname,
			multbusi: this.state.multbusi,
			usesubjversion: this.state.isversiondate,
			versiondate: this.state.isversiondate ? this.state.versiondate : this.state.busiDate,//科目版本
			// pk_accasoa://条件框选择科目
			startcode: this.state.startcode.code,  //开始科目编码
			endcode: this.state.endcode.code,    //结束科目编码
			startlvl: String(this.state.startlvl),
			endlvl: String(this.state.endlvl),
			isleave: this.state.isleave,
			isoutacc: this.state.isoutacc,
			querybyperiod: this.state.selectionState,
			pk_vouchertype: this.state.vouchertype.refpk,
			vouchertypename: this.state.vouchertype.refname,
			startvoucherno: this.state.startInput,
			endvoucherno: this.state.endInput,
			operator: this.state.makePerson.refpk,
			talliedscope: this.state.talliedscope,

			/**会计期间，日期*/
			startyear: this.state.startyear,
			endyear: this.state.endyear,
			startperiod: this.state.startperiod,
			endperiod:   this.state.endperiod,
			startdate: this.state.begindate,
			enddate: this.state.enddate,


			//错误、损益结转、重分类

			includeerror: this.state.includeerror,
			includetranfer: this.state.includetranfer,
			includereclassify: this.state.includereclassify,

			returncurrtype: this.state.selectedCurrValue, //返回币种  1,2,3 组织,集团,全局
			currtypeplussubj: this.state.currplusacc == 'Y' ? true : false,
			pk_accasoa: this.state.rightSelectItem,//'条件'
		}
		
		let url = '/nccloud/gl/accountrep/checkparam.do'
		let flagShowOrHide;
		ajax({
			url,
			data,
			async: false,
			success: function (response) {
				flagShowOrHide = true
			},
			error: function (error) {
				flagShowOrHide = false
				toast({ content: error.message, color: 'warning' });
			}
		})
		if (flagShowOrHide == true) {
			this.props.onConfirm(data)
		} else {
			return true;
		}
	}

	render() {
		//('rererennder;;', this.state, this.props)
        let { search } = this.props;
        let { NCCreateSearch } = search;
		return (
			<div>
				{this.state.json['20023020-000000'] &&
					NCCreateSearch(this.searchId, {
						onlyShowSuperBtn:true,                       // 只显示高级按钮
						replaceSuperBtn:this.state.json['20023020-000000'],                      // 自定义替换高级按钮名称{/* 国际化处理： 查询*/
						replaceRightBody:this.renderModalList,
						hideSearchCondition:true,//隐藏《候选条件》只剩下《查询方案》
						clickPlanEve:this.clickPlanEve ,            // 点击高级面板中的查询方案事件 ,用于业务组为自定义查询区赋值
						saveSearchPlan:this.saveSearchPlan ,        // 保存查询方案确定按钮事件，用于业务组返回自定义的查询条件
						oid:this.props.meta.oid,
						clickSearchBtn: this.clickSearchBtn,  // 点击按钮事件
						isSynInitAdvSearch: true,//渲染右边面板自定义区域
					})}
			</div>
		);
	}
}

CentralConstructorModal = createPage({})(CentralConstructorModal)
export default CentralConstructorModal
