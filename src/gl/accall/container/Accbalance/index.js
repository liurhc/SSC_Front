import React, { Component } from 'react';
import { hashHistory, Redirect, Link } from 'react-router';

// import OpenTable from './OpenTable';

import { high, base, ajax, print, deepClone, createPage, getMultiLang, createPageIcon } from 'nc-lightapp-front';
const { NCDiv } = base;
const { PrintOutput } = high;

import './index.less';

import { toast } from '../../../public/components/utils';
const { Refer } = high;


import MainSelectModal from './MainSelectModal';

import { SimpleTable } from 'nc-report';
import {tableDefaultData} from '../../../manageReport/defaultTableData';
import reportPrint from '../../../public/components/reportPrint.js';
import reportSaveWidths from '../../../public/common/reportSaveWidths.js';
import {gl_pkreport_accall} from '../../../public/common/reportPkConst.js';
import RepPrintModal from '../../../manageReport/common/printModal'
import { mouldOutput } from '../../../public/components/printModal/events'
import { printRequire } from '../../../manageReport/common/printModal/events'
import HeadCom from '../../../manageReport/common/headSearch/headCom';
import {handleValueChange} from '../../../manageReport/common/modules/handleValueChange'
import {setData} from "../../../manageReport/common/simbleTableData";
import {searchById} from "../../../manageReport/common/modules/createBtn";
import HeaderArea from '../../../public/components/HeaderArea';
import Iframe from '../../../public/components/Iframe';
class Accbalance extends Component {
	constructor(props) {
		super(props);

		//每定义一个state 都要添加备注
		this.state = {
			json: {},
            typeDisabled: true,//账簿格式 默认是禁用的
            accountType: 'amountcolumn',//金额式
			flag:0,//切换表头用的
      		showTable: false,
      		textAlginArr:[],//对其方式
			disabled: true,
			visible: false,
			appcode: this.props.getUrlParam('c') || this.props.getSearchParam('c'),
			outputData: {},
			ctemplate: '', //模板输出-模板
			nodekey: '',
			printParams:{}, //查询框参数，打印使用
			queryDatas: {},
			data: {},
			MainSelectModalShow: false, //查询模态框
			tableAll: {
				column: [],
				data: [],
			},              //表格全体数据
			initTableAll: null,  //初始查询出的表格全体数据
			tableStyle: {}, //表格样式
			queryCond: null, //已选择的查询条件
			pk_cashflow: '', //已点击行的pk_cashflow
			datas: {
				time: '',
				bookname: '',
				currtypeName: '',
			},
			dataout:tableDefaultData,
            "accall": [],//表格头部标题
			
		}
		this.columnInfo = [];
		this.headtitle = [];
		this.balanceVO = [];
        this.handleValueChange = handleValueChange.bind(this);
        this.searchById = searchById.bind(this);
	}
	//直接输出
	print(){
		let {resourceDatas,dataout,textAlginArr}=this.state;
		let dataArr=[];
		let emptyArr=[];
		let mergeInfo=dataout.data.mergeInfo;
		dataout.data.cells.map((item,index)=>{
			emptyArr=[];
			item.map((list,_index)=>{
				//emptyArr=[];
				if(list){
					// if(list.title){
					//     list.title='';
					// }
					emptyArr.push(list.title);
				}
			})
			dataArr.push(emptyArr);
		})
		//(dataArr);
		reportPrint(mergeInfo,dataArr,textAlginArr);
	}
	//保存列宽
	handleSaveColwidth=()=>{
		let {json}=this.state
		let info=this.refs.balanceTable.getReportInfo();
		let callBack = this.refs.balanceTable.resetWidths
		reportSaveWidths(gl_pkreport_accall,info.colWidths,json, callBack);
	}
	showPrintModal = () => {
		this.setState({
			visible: true
		})
	}
	handlePrint(data, isPreview) {
		let printUrl = '/nccloud/gl/accountrep/subjassembleprint.do'
		let { printParams, appcode } = this.state
		let {ctemplate, nodekey}=data
		printParams.queryvo = data
		this.setState({
			ctemplate: ctemplate,
			nodekey: nodekey
		})
		//(printParams,'------printParams')
		printRequire(this.props, printUrl, appcode, nodekey, ctemplate, printParams, isPreview)
		this.handleCancel()
	}
	handleCancel() {
		this.setState({
			visible: false
		});
	}
	showOutputModal() {
		// this.refs.printOutput.open()
		let outputUrl = '/nccloud/gl/accountrep/subjassembleoutput.do'
		let { appcode, nodekey, ctemplate, printParams } = this.state
		mouldOutput(outputUrl, appcode, nodekey, ctemplate, printParams)	

        // let outputData = mouldOutput(outputUrl, appcode, nodekey, ctemplate, printParams)	
        // this.setState({
        //     outputData: outputData
        // })
    }
    handleOutput() {
        //(this.state.outputData,this.state.json['20023020-000020'])/* 国际化处理： -----模板输出----成功？----*/
	}

    componentWillMount() {
        let callback= (json) =>{
            this.setState({
				json:json,
                "accall": [//
                    {
                        title: json['20023020-000027'],
                        styleClass:"m-brief"
                    },
                    {
                        title: json['20023020-000028'],
                        styleClass:"m-brief"
                    },
                    {
                        title:"",
                        styleClass:""
                    },
                    {
                        title: json['20023020-000029'],
                        styleClass:"m-brief"
                    }
                ]
		},()=>{
                // initTemplate.call(this, this.props);
            })
        }
        getMultiLang({moduleId:['20023020','publiccommon'],domainName:'gl',currentLocale:'simpchn',callback});
    }


    componentDidMount() {
		this.setState({
			data: {}
		});
        let appceod = this.props.getSearchParam('c');
		this.searchById('20023020PAGE',appceod);
		this.props.button.setDisabled({
            print: true, templateOutput: true,
			directprint: true, saveformat: true,refresh: true
        })
	}

	queryShow() {
		this.setState({
			MainSelectModalShow: true,
		})
	}

	test(datas, param) {
		let self = this;
		let url = '/nccloud/gl/accountrep/subjassemblequery.do';
		let data = datas;
		ajax({
			loading: true,
			url,
			data,
			success: function (res) {
				const { data, error, success } = res;
				if (success) {
					if(param){
						toast({
							title: self.state.json['20023020-000030'] /* 国际化处理： 刷新成功 */
						})
					}
					if (data) {
						//('dddatatat::', data)

						if(data.data.length>0){
							self.setState({
								disabled:false,
							})
							self.props.button.setDisabled({
								print: false, templateOutput: false,
								directprint: false, saveformat: false,
							})
						}
						self.setState({
                            headtitle: data.headtitle,
                            typeDisabled: false,
						})
						if(data.column){
							self.columnInfo=data[self.state.accountType];
						}
						if(data.headtitle){
                            self.headtitle=data.headtitle;
						}
						if(data.data){
                            self.balanceVO=data.data;
						}
						self.setState({
                            dataWithPage: data
						})
                        setData(self, data, param)

						// changeToHandsonTableData(self, data, self.columnInfo, self.headtitle, self.balanceVO);
					}
				} else {
					toast({ content: error.message, color: 'warning' });
				}
			},
			error: function (res) {
				toast({ content: res.message, color: 'warning' });

			}
		});
	}

	getDatas = (datas, param) => {
		//this.state.queryDatas=datas;
        this.props.button.setDisabled({
           refresh: false
        })
		this.test(datas, param);
		this.setState({
			queryDatas: datas,
			printParams: datas
		})
	}
    handleLoginBtn = (obj, btnName) => {
        //('handleLoginBtn>>>', obj, btnName)
        if(btnName === 'print'){//1、打印
            this.showPrintModal()
        }else if(btnName === 'templateOutput'){//2、模板输出
            this.showOutputModal()
        }else if(btnName === 'directprint'){//3、直接输出
            this.print()
        }else if(btnName === 'saveformat'){//4、保存列宽
            this.handleSaveColwidth()
        } else if(btnName==='refresh'){
			if (Object.keys(this.state.queryDatas).length > 0) {
				this.getDatas(this.state.queryDatas, true);
			}
		}
    }

	render() {
		//(">>", this.state.myhandsontableData)
		let { modal } = this.props;
		const { createModal } = modal;
		return (
			<div id="cashQuery" className='manageReportContainer'>
				<HeaderArea 
                    title = {this.state.json['20023020-000021']} /* 国际化处理： 科目汇总表*/
                    btnContent = {
                        <div>
                            <div className='account-query-btn'>
								<MainSelectModal
									onConfirm={(datas) => {
										this.getDatas(datas);
									}}
								/>
                            </div>
                            {this.props.button.createButtonApp({
                                area: 'btnarea',
								buttonLimit: 3,
								onButtonClick: (obj, tbnName) => this.handleLoginBtn(obj, tbnName),
                            })}
                        </div>
                    }
                />
				<NCDiv areaCode={NCDiv.config.SEARCH}>
					<div className="searchContainer nc-theme-gray-area-bgc">
						{
							this.state.accall.map((items) => {
								return(
									<HeadCom
										lastThre = {this.state.headtitle && this.state.headtitle}
										labels={items.title}
										key={items.title}
										// disabled={this.state.disabled}
										changeSelectStyle={
											(key, value) => this.handleValueChange(key, value, 'assistAnalyzSearch', () => setData(this, this.state.dataWithPage))
										}
										accountType = {this.state.accountType}
										isLong = {items.styleClass}
									/>
								)
							})
						}
					</div>
				</NCDiv>
                <div className='report-table-area'>
					<SimpleTable
						ref="balanceTable"
						data={this.state.dataout}
					/>
				</div>

				<RepPrintModal
					// scopeGray={true}
					noRadio={true}
					// showNum={true}
					noCheckBox={true}
					appcode={this.state.appcode}
					visible={this.state.visible}
					// printModalRef={(inst) => this.printModal = inst}
					handlePrint={this.handlePrint.bind(this)}
					handleCancel={this.handleCancel.bind(this)}
				/>
				<PrintOutput					
					ref='printOutput'
					url='/nccloud/gl/accountrep/subjassembleoutput.do'
					data={this.state.outputData}
                    callback={this.handleOutput.bind(this)}
				/>
				{createModal('printService', {
					className: 'print-service'
				})}
				<Iframe />
			</div>
		)
	}
}

Accbalance = createPage({})(Accbalance)
export default Accbalance
