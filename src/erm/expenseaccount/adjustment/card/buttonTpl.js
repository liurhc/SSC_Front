/**
 *  前端写死的按钮模板。
 *  正常的按钮模板获取应该通过 props.createUIDom 获得
 */
let buttonTpl = {
    button: [
        {
            "id": "0001A41000000006J981",
            "type": "button_main",
            "key": "billSubmit",
            "title": "提交",
            "area": "head",
            "children": []
        },
        {
            "id": "0001A4100ss0000006J981",
            "type": "button_main",
            "key": "billSaveSubmit",
            "title": "保存提交",
            "area": "head",
            "children": []
        },
        {
            "id": "0001A4100000324app0006J982",
            "type": "button_main",
            "key": "billApprove",
            "title": "审批",
            "area": "head",
            "children": []
        },
        {
            "id": "0001A41000000006J982",
            "type": "button_main",
            "key": "billRecall",
            "title": "收回",
            "area": "head",
            "children": []
        },
        {
            "id": "00452A410000054d0006J5B2",
            "type": "button",
            "key": "pageCopy",
            "title": "复制",
            "area": "head",
            "children": []
        },
        {
            "id": "00452A435312094d0006J5B2",
            "type": "button",
            "key": "pageEdit",
            "title": "修改",
            "area": "head",
            "children": []
        },
        {
            "id": "0001A410000054d0006J5B2",
            "type": "button",
            "key": "pageDel",
            "title": "删除",
            "area": "head",
            "children": []
        },
        {
            "id": "00452A435312094d0006J5B2",
            "type": "button",
            "key": "pagePrint",
            "title": "打印",
            "area": "head",
            "children": []
        },
        {
            "id": "00452A43531zdbc094d0006J5B2",
            "type": "button_main",
            "key": "pageSave",
            "title": "保存",
            "area": "head",
            "children": []
        },
        {
            "id": "00452A43531zdbc094d0006J998",
            "type": "button",
            "key": "fileManager",
            "title": "附件管理",
            "area": "head",
            "children": []
        },
        {
            "id": "00452A43531zdbc094d0006J999",
            "type": "button",
            "key": "billView",
            "title": "联查单据",
            "area": "head",
            "children": []
        },
        {
            "id": "00452A4yxsm35312094d0006J5B2",
            "type": "button",
            "key": "imageUpload",
            "title": "影像扫描",
            "area": "head",
            "children": []
        },
        {
            "id": "00452A4yxck35312094d0006J5B2",
            "type": "button",
            "key": "imageShow",
            "title": "影像查看",
            "area": "head",
            "children": []
        },
        {
            "id": "0001A41000000006J5B1",
            "type": "button",
            "key": "billClose",
            "title": "关闭",
            "area": "head",
            "children": []
        },
        {
            "id": "0001A41000000006J5B166",
            "type": "button",
            "key": "billEnable",
            "title": "重启",
            "area": "head",
            "children": []
        },
        {
            "id": "0001A41000000006J5B2",
            "type": "button",
            "key": "billInvalid",
            "title": "作废",
            "area": "head",
            "children": []
        },
        {
            "id": "0001A41000000006234J5B1",
            "type": "button_secondary",
            "key": "pageClose",
            "title": "取消",
            "area": "head",
            "children": []
        },
        {
            "id": "0001A4100000000622J5B2",
            "type": "button_main",
            "key": "er_cshare_detail-add",
            "title": "新增",
            "area": "body-shoulder",
            "children": []
        },
        {
            "id": "0001A410000014f000622J5B2",
            "type": "button_main",
            "key": "er_cshare_detail-edit",
            "title": "编辑",
            "area": "er_cshare_detail",
            "children": []
        },
        {
            "id": "0001A4112300000000622J5B2",
            "type": "button_main",
            "key": "er_cshare_detail-del",
            "title": "删除",
            "area": "er_cshare_detail",
            "children": []
        },
        {
            "id": "0001Abj4100000000622J5B2",
            "type": "button_secondary",
            "key": "LinkVoucher",
            "title": "联查凭证",
            "area": "head",
            "children": []
        },
        {
            "id": "0001Abj4100000000622J5B2",
            "type": "button_secondary",
            "key": "LinkBudget",
            "title": "联查预算",
            "area": "head",
            "children": []
        }
    ]
}


export default buttonTpl;