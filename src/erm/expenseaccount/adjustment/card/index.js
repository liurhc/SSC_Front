import React, {Component} from 'react';
import ReactDOM from 'react-dom';

import Adjustment from './Adjustment';

ReactDOM.render((<Adjustment />)
    , document.querySelector('#app'));