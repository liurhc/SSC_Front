import presetVar from '../presetVar';

const { body1} = presetVar.body;
const {head1} = presetVar.head;

/**
 * 表头合计处理
 */
export function headSumHandle() {
    ///需要汇总的金额字段
    let bodyFieldArr = ["assume_amount"];
    let changeHeadItem = {};
    bodyFieldArr.forEach((bodyField) => {//循环字段
        let totalMny = 0;
        let colVal = this.props.cardTable.getColValue(body1, bodyField);//取列值
        let rows = this.props.cardTable.getVisibleRows(body1);
        if (rows) {
            rows.forEach((row) => {
                totalMny += parseFloat(+row.values[bodyField].value);//汇总
            });
        }
        let headField = bodyField == "assume_amount" ? "total" : bodyField;//total字段特殊处理
        let scale = this.props.form.getFormItemsValue(head1, headField).scale;//取精度
        changeHeadItem[headField] = { value: totalMny, display: null };
        if (bodyField == "assume_amount") {
            changeHeadItem['ybje'] = { value: totalMny, display: null };
        }
    });
    this.props.form.setFormItemsValue(head1, changeHeadItem) 
}
