import afterEvent from './afterEvent';
import beforeEvent from './beforeEvent';
import {pageButtonClick, setVar} from './pageButtonClick';

import tableButtonClick from './tableButtonClick';
import tableExtendButtonClick from './tableExtendButtonClick';
import refreshButtonClick from './refreshButtonClick';

export { tableButtonClick, tableExtendButtonClick, pageButtonClick, afterEvent,beforeEvent, setVar,refreshButtonClick};
