import requestApi from '../requestApi';
import 'ssccommon/components/globalPresetVar';
import presetVar from '../presetVar';
import {shoulderBtnVisibleHandle} from '../initTemplate';

export default function () {
   
//调整单
    let props = this.props;
    let billdata = {};
    billdata.openbillid =presetVar.openBillId;
    billdata.tradetype = presetVar.tradeType;
    //billdata.uid = "1001Z3100000000000AE";//这里以后从session中获取
    billdata.pagecode = presetVar.nccPageCode;
    requestApi.viewBill({
        data: billdata,
        success: (data) => {
            props.form.setAllFormValue({ [presetVar.head.head1]: data.head[presetVar.head.head1] });
            //设置表体数据
            Object.values(presetVar.body).forEach((body) => {
                if (data.bodys[body]) {
                    props.cardTable.setTableDataWithResetInitValue(body, data.bodys[body]);
                }
            });
            shoulderBtnVisibleHandle(props, data.head[presetVar.head.head1]);
            this.pubMessage.refreshSuccess();
        }
    });
}
