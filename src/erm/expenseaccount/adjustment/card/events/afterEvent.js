
import requestApi from '../requestApi';
import presetVar from '../presetVar';
import {updataFormData, updatacardTableData} from 'ssccommon/utils/changePageData';

const body1 = presetVar.body.body1;
const head1 = presetVar.head.head1;

export default function afterEvent(props, moduleId, key, value, changedrows, index, record) {

    console.log('编辑后事件', props, moduleId, key, value, changedrows, index, record);

    let paramdata = {};
    moduleId = moduleId == head1 ? "headform" : moduleId;

    paramdata = {
        "value": value,
        "tableid": moduleId,
        "field": key,
        "uistate": presetVar.pageStatus,
        "tradetype": presetVar.tradeType,
        "oldvalue": '',
        "pagecode": presetVar.nccPageCode,
        checkrule: 'true'
    }
    if (moduleId != "headform") {
        paramdata.changeRowId = changedrows[0].rowid;
    } else {
        paramdata.value = value.value;
    }

    switch (moduleId) {
        case 'headform':
            switch (key) {
                case 'pk_org'://表头财务组织
                case 'djrq'://表头单据日期
                case 'bzbm'://表头币种
                    if (!value.value) {
                        break;
                    }
                case 'bbhl'://表头本币汇率
                case 'dwbm_v'://表头借款报销人单位版本
                case 'dwbm'://表头借款报销人单位
                case 'fydwbm_v'://表头费用承担单位版本
                case 'fydwbm'://表头费用承担单位
                case 'pk_payorg_v'://表头支付单位版本
                case 'pk_payorg'://表头支付单位
                case 'deptid_v':// 表头报销人部门版本
                case 'deptid':// 表头报销人部门
                case 'fydeptid_v':// 表头费用承担部门版本
                case 'fydeptid':// 表头费用承担部门
                case "jkbxr"://借款报销人
                case "receiver"://表头收款人
                case "szxmid"://表头收支项目
                case "skyhzh"://表头个人银行账户
                case "jobid"://表头项目
                case "freecust"://散户
                case "custaccount"://表头客商银行账户
                case "paytarget"://表头收款对象
                    let billdata = props.createHeadAfterEventData('', head1, [body1], moduleId, key, value);
                    console.log('表头字段change后表头标题数据结构：', billdata)
                    let send_headdata = { billdata, paramdata };
                    // TOP NCCLOUD-91597 校验报销标准取模板时需要使用模板ID chengwbc ADD
	                send_headdata.templateid = props.meta.getMeta().pageid;
	                // BTM NCCLOUD-91597 校验报销标准取模板时需要使用模板ID
                    requestApi.valueChange({
                        data: send_headdata,
                        success: (data) => {
                            // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                            // //设置表头数据
                            // props.form.setAllFormValue({ [head1]: data.head[head1] });
                            // //设置表体数据
                            // props.cardTable.setTableDataWithResetInitValue(body1, data.bodys[body1]);
                            //设置表头数据
                            updataFormData(props, head1, data.head[head1]);
                            //设置表体数据
                            updatacardTableData(props, body1, data.bodys[body1]);
                            // BTM 性能优化，变更页面数据更新方法
                        }
                    })
                    break;
            }
            break;
        case body1:
            switch (key) {
                case 'assume_org': //费用承担单位
                case 'assume_amount': //承担金额
                case 'assume_dept': //费用承担部门
                    //表头修改后数据后台需要的结构， 一主一子  一主多子
                    if (changedrows[0].newvalue.value == changedrows[0].oldvalue.value) {
                        return;
                    }
                    let billdata = props.createHeadAfterEventData(presetVar.pageCode, head1, Object.values(presetVar.body), moduleId, key, value);

                    for (let bodyArea in billdata.card.bodys) {
                        //只发送未删除的行
                        billdata.card.bodys[bodyArea].rows = props.cardTable.getVisibleRows(bodyArea);
                    }

                    console.log('表头字段change后表头标题数据结构：', billdata);
                    if (value.value == undefined || value.value == null) {
                        paramdata.value = changedrows[0].newvalue.value;
                    }

                    let send_headdata = { billdata, paramdata };
                    // TOP NCCLOUD-91597 校验报销标准取模板时需要使用模板ID chengwbc ADD
	                send_headdata.templateid = props.meta.getMeta().pageid;
	                // BTM NCCLOUD-91597 校验报销标准取模板时需要使用模板ID
                    requestApi.valueChange({
                        data: send_headdata,
                        success: (data) => {
                            // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                            // //设置表头数据
                            // props.form.setAllFormValue({ [head1]: data.head[head1] });
                            // //设置表体数据
                            // props.cardTable.setTableData(moduleId, data.bodys[moduleId]);
                            //设置表头数据
                            updataFormData(props, head1, data.head[head1]);
                            //设置表体数据
                            updatacardTableData(props, moduleId, data.bodys[moduleId]);
                            // BTM 性能优化，变更页面数据更新方法
                        }
                    })

                    break;
            }
            break;
    }
};