/**
 * 表格肩部按钮click事件
 */
import presetVar from '../presetVar';

const { body1, body2 } = presetVar.body;
const head1 = presetVar.head.head1;


function tableButtonClick() {
    return {
        [body1 + '_Add']: {
            afterClick: () => {
                //headSumHandle.call(this);
            }
        }
    }
}

export default tableButtonClick;