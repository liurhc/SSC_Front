/**
 * 表格扩展按钮click事件
 */

import presetVar from '../presetVar';
import {headSumHandle} from "./commonEvent";


const tableInnerBtnAction = presetVar.tableInnerBtnAction;
const body1 = presetVar.body.body1;
const head1 = presetVar.head.head1;

function tableExtendButtonClick() {
    let returnObj = {};
    let areaId = body1;
    returnObj[areaId + '_Delete'] = {
        afterClick: () => {
            headSumHandle.call(this);
        }
    };
    returnObj[areaId + '_Copy'] = {
        afterClick: () => {
            headSumHandle.call(this);
        }
    };
    return returnObj;
}

export default tableExtendButtonClick;