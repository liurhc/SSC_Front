import { pageTo } from 'nc-lightapp-front';

/**
 * 全局变量设置文件
 */
import globalPresetVar from 'ssccommon/components/globalPresetVar';


const presetVar = {
    ...globalPresetVar,
    bodyChangeValuesAll:'',//缓存表体数据
    head: {
        head1: 'head'
    },
    //表体（子表）变量配置
    body: {
        body1: 'er_cshare_detail'
    },
    tradeType: '', //交易类型
    pageCode: '20110ADJUST_264a_IWEB', //老pageCode
    nccPageCode: null,
    nccAppCode: null,
    openBillId: null,
    pageStatus: null,
    pageButton: {
        ...globalPresetVar.pageButton
    },
    currentRowRecord: null
}

//以下变量可能会根据单据页面状态不同而发生变化，如在hashCHange的时候会发生改变。所以不要赋值给原始数据类型变量。
presetVar.nccPageCode = pageTo.getUrlParam('pagecode')|| pageTo.getSearchParam('p') || pageTo.getSearchParam('pagecode');
presetVar.nccAppCode = pageTo.getUrlParam('appcode') || pageTo.getSearchParam('c') || pageTo.getSearchParam('appcode');
presetVar.openBillId = pageTo.getUrlParam('id');
presetVar.pageStatus = pageTo.getUrlParam('status');
presetVar.tradeType = pageTo.getUrlParam("tradetype");
presetVar.scene = pageTo.getUrlParam('scene');


export default presetVar;

