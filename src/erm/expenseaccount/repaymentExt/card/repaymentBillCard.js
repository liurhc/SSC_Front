import './presetVar'
import React, { Component } from 'react';
import { createPage, base, high, getAttachments } from 'nc-lightapp-front';
import { refreshButtonClick, tableButtonClick, pageButtonClick, beforeEvent,afterEvent ,invoiceEvent} from './events';
import initTemplate from './initTemplate';
import JointBill from '../../../../sscrp/refer/jointbill/JointBillTableRef/JointBillModal';
import {
    BillStyle,
    BillHead,
    BillBody,
    Form,
    CardTable,
    CardTableGroup,
    ButtonGroup,
    MultiButtonGroup
} from 'ssccommon/components/bill';

import './index.less';
import LinkQueryJkbxModel from 'ssccommon/linkquery/LinkQueryJkbxModel';
import requestApi from "../../expenseaccount/card/requestApi";
import pubMessage from 'ssccommon/utils/pubMessage';
import getAttachmentLength from '../../../public/components/linkAttachment/getAttachmentLength'
import AttachmentOther from '../../../expenseaccount/expenseaccount/card/AttachmentOther';


const { NCUploader , Inspection, ApproveDetail,ApprovalTrans} = high;

const pageButton = window.presetVar.pageButton;
window.assingUsers = [];
window.sureAssign = false;//确定指派
window.assignBillId = '';//有指派的情况下，第一次点保存提交实际保存成功的单据pk

class repaymentBillCard extends Component {
    constructor(props) {
        super();
        this.state = {
            billId : '',
            showLinkQueryJkbxModel: false,
            target: null,
            showUploader: false,
            uploadPk : '',
            sourceData: null,
            showLinkBudget: false,
            hideTables:[],
            ShowApproveDetail: false,
            billtype: '',
            linkData:'',
			transtype_name:'',
            compositedata : '',
            compositedisplay : '',
            tableData:{},//附件除发票
        }

        initTemplate.call(this, props);
        this.pubMessage = new pubMessage(props);

    }
    billInvalid(){
        let param = {};
        param.openbillid=this.props.getUrlParam("id")||this.props.getUrlParam("billid");
        param.tradetype=window.presetVar.tradetype;
        param.pagecode=this.props.getSearchParam("p");
        requestApi.billInvalid({
            data: param,
            success: (data) => {
                if (data.success) {
                    location.reload();
                }
            }
        });
    }
    componentWillMount() {
        window.onbeforeunload = () => {
            let status = this.props.getUrlParam("status");
            if (status == 'edit'||status == 'add') {
                return ''/* 国际化处理： 确定要离开吗？*/
            }
        }
    }
    cancel(){
        this.setState({
            showLinkBudget: false
        })
    }
    affirm(info){
        this.setState({
            showLinkBudget: false
        })
    }
    closeApprove = () => {
        this.setState({
            ShowApproveDetail: false
        })
    }   
    getAssginUser = (value) => {
        window.assingUsers = value;
        window.sureAssign = true;
        pageButtonClick.call(this)[pageButton.billSubmit]();
        window.assignBillId = '';
        window.sureAssign = false;
    }

    turnOff(){
        this.setState({
            compositedisplay:false
        }) ;
        window.sureAssign = false;
        let props = this.props;
        props.go({
            status: 'browse',
            id: window.assignBillId,
            tradetype:window.presetVar.tradetype,
            scene:this.props.getUrlParam('scene')
        });
    }
    onHideUploader = () => {
        this.setState({
            showUploader: false,
        });
        let id = this.props.getUrlParam('id') ? this.props.getUrlParam('id') : this.state.billId;
        getAttachmentLength(id).then(lists => {
            this.props.button.setButtonTitle('fileManager', json['201102HKD-0009'] + ' ' + lists + ' ');//'附件'
        })
    }
    
    render() {
        let {modal} = this.props;
        let {createModal} = modal;
        let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId
        let btnModalConfig = {
            [pageButton.pageDel]: {
                // "2011-0004": "删除",
                // "2011-0005": "确认删除该单据吗？"
                title: multiLang && multiLang.get('2011-0004'),
                content: multiLang && multiLang.get('2011-0005'),
            },
            [pageButton.billInvalid]: {
                // "2011-0009": "作废",
                // "2011-0010": "确认作废该单据吗？"
                title: multiLang && multiLang.get('2011-0009'),
                content: multiLang && multiLang.get('2011-0010'),
                beSureBtnClick: this.billInvalid.bind(this)
            }
        }
        let { showUploader,target } = this.state;
        return (
            <div >
            <BillStyle
                {...this.props}
            >
            <BillHead
                refreshButtonEvent={refreshButtonClick.bind(this)}
				title={this.state.transtype_name}
            >            
                <ButtonGroup
                    areaId="head"
                    buttonEvent={pageButtonClick.bind(this)}
                    modalConfig={btnModalConfig}
                />
                <Form
                    areaId="head"
                    onAfterEvent={afterEvent.bind(this)}/>

            </BillHead>

            <BillBody>

                    <CardTableGroup
                        invisibleTables={[window.presetVar.body.body2]}
                        excludeTableAreaId={null}
                        totalItemCode="vat_amount"
                        modelSave={pageButtonClick.bind(this, this.props, 'pageSave')}
                        onBeforeEvent={beforeEvent.bind(this)}
                        onAfterEvent={afterEvent.bind(this)}
                    >
                    <MultiButtonGroup buttonEvent={tableButtonClick.bind(this)}   />
                </CardTableGroup>

                {
                    this.state.showUploader && < NCUploader
                    billId={ this.state.billId }
                    // target={target}
                    placement={'bottom'} />
                }

                <JointBill 
                    show={this.state.jointBillShow} 
                    close={() => this.setState({jointBillShow: false})} 
                    billCode={this.state.billId}
                    {...this.props} />

            </BillBody>

            </BillStyle>
   
        {/* 联查借款单 */}
        <LinkQueryJkbxModel
            show={this.state.showLinkQueryJkbxModel}
            linkData={this.state.linkData}
            close={() => this.setState({showLinkQueryJkbxModel: false})}
            tradetype={window.presetVar.tradetype}
            openBillId={this.state.billId}
            {...this.props}
        />

                <div>
                    {showUploader && < NCUploader
                        billId={ this.state.billId }
                        targrt={target}
                        onHide={ this.onHideUploader }
						customInterface={
							{
							queryLeftTree:"/nccloud/erm/pub/ErmAttachmentTreeNodeQueryAction.do",
							queryAttachments: "/nccloud/erm/pub/ErmAttachmentQueryAction.do"
							}
						}
                        placement={'bottom'}
                    />
                    }
                </div>
                <div>
                <ApproveDetail
                    show={this.state.ShowApproveDetail}
                    close={this.closeApprove}
                    billtype={this.state.billtype}
                    billid={this.state.billId}
                />
                  <AttachmentOther tableData={this.state.tableData}/>
            </div>
                <div>
                    <Inspection
                        show={ this.state.showLinkBudget }
                        sourceData = { this.state.sourceData }
                        cancel = { this.cancel.bind(this) }
                        affirm = { this.affirm.bind(this) }
                    />
                </div>
                <div>
                {/*"201102FYYT-0003": "指派",*/}
                {this.state.compositedisplay ? <ApprovalTrans
                    title= {multiLang && multiLang.get('201102FYYT-0003')}
                    data={this.state.compositedata}
                    display={this.state.compositedisplay}
                    getResult={this.getAssginUser}
                    cancel={this.turnOff.bind(this)}
                    /> : ""}
                </div>
            </div>
        )
    }
}

repaymentBillCard = createPage({
    mutiLangCode: '2011',
    billinfo:{
        billtype: 'card',  //虽然后台是一主多子数据结构，但是配置成extCard，编辑后请求公式验证会报错。因为会根据实际展现的表体数量来判断是一主多子还是一主一子。
        pagecode: '201102HKD_card_002', 
        headcode: 'head',
        bodycode: 'er_bxcontrast'
    },
    orderOfHotKey: ['er_bxcontrast']
})(repaymentBillCard);

export default repaymentBillCard;