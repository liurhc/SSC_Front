import 'ssccommon/components/globalPresetVar';
import { pageTo } from 'nc-lightapp-front';
//全局变量尽量配置在presetVar中，防止全局变量污染。也方便统一管理。
window.presetVar = {
    ...window.presetVar,
    pageId: '',
    pageCode: '',
    bodyChangeValuesAll:'',//缓存表体数据
    //表头（主表）变量配置
    head: {
        head1: 'head',
        head2: ''
    },
    //表体（子表）变量配置
    body: {
        body1:'er_bxcontrast',
        body2:'arap_bxbusitem'

    },
    tradetype:"2647",
    //搜索模板变量配置
    search: {}
};
//以下变量可能会根据单据页面状态不同而发生变化，如在hashCHange的时候会发生改变。所以不要赋值给原始数据类型变量。
const presetVar = {
    nccPageCode : pageTo.getUrlParam('pagecode')|| pageTo.getSearchParam('p') || pageTo.getSearchParam('pagecode'),
    nccAppCode : pageTo.getUrlParam('appcode') || pageTo.getSearchParam('c') || pageTo.getSearchParam('appcode'),
    openBillId : pageTo.getUrlParam('id'),
    pageStatus : pageTo.getUrlParam('status')
}
export default presetVar;