import { ajax, print, toast, promptBox, getMultiLang } from 'nc-lightapp-front';
import requestApi from '../requestApi';
import { dataHandle } from '../initTemplate';
import { imageScan, imageView } from "sscrp/rppub/components/image";
import presetVar from '../presetVar';
import linkVoucherApi from '../../../../public/components/linkvoucher/linkvoucher'
import linkBudgetApi from '../../../../public/components/linkbudget/linkbudget'
import { updataFormData, updatacardTableData } from 'ssccommon/utils/changePageData';
let pageButton = window.presetVar.pageButton;
let areaBtnAction = window.presetVar.areaBtnAction;
let statusVar = window.presetVar.status;
const head1 = window.presetVar.head.head1;
const body1 = window.presetVar.body.body1;
const body2 = window.presetVar.body.body2;
function getRequest1(url) {
    if (!url)
        url = document.location.search;
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substring(url.indexOf("?") + 1);
        var strs = str.split("&");
        for (var i = 0; i < strs.length; i++) {
            theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
        }
    }
    return theRequest;
}
function pageButtonClick() {
    // getMultiLang({moduleId: 2011, domainName: 'erm',currentLocale: 'zh-CN', callback: (json) => {
    let multiLang = this.props.MutiInit.getIntl(2011);
    let json = this.props.MutiInit.getIntl(2011);
    let billtype = this.props.getUrlParam("tradetype") || window.presetVar.tradetype;
    let props = this.props;
    let bodys = Object.values(window.presetVar.body);
    let bodyarr = [];
    let hideTable = this.state.hideTables;
    bodys.map((one) => {
        if (hideTable.indexOf(one) < 0) {
            bodyarr[bodyarr.length] = one;
        }
    })

    props.go = function (param) {
        if (location.pathname.indexOf('repaymentForPull') > -1) {
            props.pushTo('/card', param);
        } else {
            props.linkTo(location.pathname, param);
        }
    }
    return {

        //保存
        [pageButton.pageSave]: () => {
            let billdataForFormula = props.createMasterChildData(presetVar.nccPageCode, head1, body1);
            props.validateToSave(billdataForFormula, () => {
                let checkResult = props.form.isCheckNow('head') && props.cardTable.checkTableRequired();
                if (checkResult) {
                    let billdata = props.createExtCardData(props.meta.getMeta().code, "head", Object.values(window.presetVar.body));
                    let paramdata = {};
                    console.log(billdata);
                    paramdata.fromssc = "ssc",
                        paramdata.jkCheck = "true",
                        paramdata.ntbCheck = "true",
                        paramdata.msg_pk_bill = "",
                        paramdata.accessorybillid = this.accessorybillid;
                    paramdata.pk_tradetype = billtype;
                    paramdata.pagecode = this.props.getSearchParam("p");
                    paramdata.appcode = this.props.getSearchParam("c");
                    paramdata.openbillid = !props.getUrlParam("id") ? "" : props.getUrlParam("id");
                    let status = this.props.getUrlParam("status");
                    if (status == "edit") {
                        paramdata.status = "edit";
                    } else {
                        paramdata.status = "save";
                    }
                    billdata.head.userjson = JSON.stringify(paramdata);
                    // TOP NCCLOUD-91597 校验报销标准取模板时需要使用模板ID chengwbc ADD
                    billdata.templetid = this.props.meta.getMeta().pageid;
                    // BTM NCCLOUD-91597 校验报销标准取模板时需要使用模板ID                    console.log(billdata);
                    requestApi.savejkbx({
                        data: billdata,
                        success: (data) => {
                            console.log(data);
                            let billid = data.head.head.rows[0].values.pk_jkbx.value;

                            props.go({
                                status: statusVar.browse,
                                id: billid,
                                tradetype: window.presetVar.tradetype,
                                scene: this.props.getUrlParam('scene')
                            });
                            props.button.setButtonsVisible({
                                [pageButton.pageSave]: false,
                                [pageButton.pageEdit]: true
                            });

                            props.form.setFormStatus('head', 'browse');
                            props.billBodys && props.billBodys.forEach((body) => {
                                props.editTable.setStatus(body, 'browse');
                            });
                            // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                            // props.form.setAllFormValue({[head1]: data.head[head1]});
                            // props.cardTable.setTableData(body1, data.bodys[body1]);
                            // props.cardTable.setTableData(body2, data.bodys[body2]);
                            updataFormData(props, head1, data.head[head1]);
                            updatacardTableData(props, body1, data.bodys[body1]);
                            updatacardTableData(props, body2, data.bodys[body2]);
                            // BTM 性能优化，变更页面数据更新方法
                        },
                        error: (data) => {
                            toast({ content: data.message, color: 'danger' });
                        }
                    });
                }
            }, body1, null)
        },

        //取消
        [pageButton.pageCancel]: () => {

            let openbillid = props.getUrlParam("id");
            if (location.hash.indexOf(openbillid) > -1) {
                props.go({
                    status: statusVar.browse,
                    id: openbillid,
                    tradetype: window.presetVar.tradetype,
                    scene: props.getUrlParam('scene')
                });
                props.form.cancel(window.presetVar.head);
                props.cardTable.resetTableData(window.presetVar.body.body1);
                props.cardTable.resetTableData(window.presetVar.body.body2);
                props.form.setFormStatus('head', 'browse');
                props.billBodys && props.billBodys.forEach((body) => {
                    props.editTable.setStatus(body, 'browse');
                });
                props.button.setButtonsVisible({
                    [pageButton.pageSave]: false,
                    [pageButton.pageEdit]: true,
                    [pageButton.pageCancel]: true
                });
            } else {
                this.props.pushTo('/pull', {
                    'p': '201102HKD_card_001',
                    vbillno: props.getUrlParam('dztz_billno'),
                    pk: props.getUrlParam('dztz_billid'),
                    transerial: props.getUrlParam('dztz_batch'),
                    memo: props.getUrlParam('dztz_summary'),
                    moneyy: props.getUrlParam('dztz_money')
                });
                //window.top.close();
            }

            /* let props = this.props;
            location.hash = `#status=${statusVar.browse}&id=${props.getUrlParam('id')}&tradetype=${window.presetVar.tradetype}`;
            props.button.setButtonsVisible({
                [pageButton.pageSave]: false,
                [pageButton.pageCancel]: false,
                [pageButton.pageEdit]: true
            }); */
        },

        //提交
        [pageButton.billSubmit]: () => {
            let checkResult = props.form.isCheckNow('head') && props.cardTable.checkTableRequired();
            if (checkResult) {
                let billdataForFormula = props.createMasterChildData(presetVar.nccPageCode, head1, body1);
                props.validateToSave(billdataForFormula, () => {
                    let send_data = {};
                    let billdata = props.createExtCardData(props.meta.getMeta().code, "head", bodyarr);
                    let paramurl = getRequest1(parent.window.location.hash);
                    let paramdata = {};
                    console.log(billdata);
                    paramdata.openbillid = !(props.getUrlParam("billid") || props.getUrlParam("id")) ? "" : props.getUrlParam("billid") || props.getUrlParam("id");
                    paramdata.fromssc = "ssc",
                        paramdata.jkCheck = "",
                        paramdata.ntbCheck = "",
                        paramdata.msg_pk_bill = "",
                        paramdata.pk_tradetype = billtype;
                    paramdata.pagecode = paramurl.p;
                    paramdata.appcode = paramurl.c;
                    paramdata.assingUsers = window.assingUsers;
                    let status = this.props.getUrlParam("status");
                    if (status == "edit") {
                        paramdata.status = "edit";
                    } else {
                        paramdata.status = "save";
                    }
                    //如果指派点了确定  由于第一次点击保存提交实际上已经保存成功了，指派后再次提交应该走浏览提交逻辑
                    if (window.sureAssign) {
                        paramdata.status = "browse";
                        paramdata.openbillid = window.assignBillId;
                    }
                    billdata.head.userjson = JSON.stringify(paramdata);
                    console.log(billdata);
                    requestApi.submitBill({
                        data: billdata,
                        success: (data) => {//设置了指派
                            if (data.workflow && data.billid && data.bill && (data.workflow == 'approveflow' || data.workflow == 'workflow')) {
                                this.setState({
                                    compositedata: data,
                                    compositedisplay: true
                                });
                                window.assignBillId = data.billid;
                                updataFormData(props, head1, data.bill.head[head1]);
                                updatacardTableData(props, body1, data.bill.bodys[body1]);
                                updatacardTableData(props, body2, data.bill.bodys[body2]);
                            } else {
                                this.setState({
                                    compositedisplay: false
                                });
                                if (data.Alarm && data.Alarm == "Y") {
                                    promptBox({
                                        color: 'danger',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                                        title: multiLang && multiLang['201102HKD-0007'],//"预算控制提醒",                // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输
                                        content: data.message,             // 提示内容,非必输
                                        noFooter: false,                // 是否显示底部按钮(确定、取消),默认显示(false),非必输
                                        noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
                                        beSureBtnName: multiLang && multiLang['2011-0003'],//"确定",          // 确定按钮名称, 默认为"确定",非必输
                                        cancelBtnName: multiLang && multiLang['2011-0002'],//"取消",,         // 取消按钮名称, 默认为"取消",非必输
                                        beSureBtnClick: () => {
                                            let bodys = Object.values(window.presetVar.body);
                                            let bodyarr = [];
                                            let hideTable = this.state.hideTables;
                                            bodys.map((one) => {
                                                if (hideTable.indexOf(one) < 0) {
                                                    bodyarr[bodyarr.length] = one;
                                                }
                                            })
                                            let billdata = this.props.createExtCardData(this.props.meta.getMeta().code, "head", bodyarr);
                                            let appcode = (this.props.getUrlParam("appcode") == undefined || this.props.getUrlParam("appcode") == '') ? this.props.getSearchParam("c") : this.props.getUrlParam("appcode");
                                            let pagecode = (this.props.getUrlParam("pagecode") == undefined || this.props.getUrlParam("pagecode") == '') ? this.props.getSearchParam("p") : this.props.getUrlParam("pagecode");
                                            let paramdata = {};
                                            console.log(billdata);
                                            paramdata.openbillid = !(this.props.getUrlParam("billid") || this.props.getUrlParam("id")) ? "" : this.props.getUrlParam("billid") || this.props.getUrlParam("id");
                                            paramdata.fromssc = "ssc",
                                                paramdata.jkCheck = "",
                                                paramdata.ntbCheck = "true",
                                                paramdata.msg_pk_bill = "",
                                                paramdata.pk_tradetype = window.presetVar.tradetype;
                                            paramdata.pagecode = pagecode;
                                            paramdata.appcode = appcode;
                                            let status = this.props.getUrlParam("status");
                                            if (status == "edit") {
                                                paramdata.status = "edit";
                                            } else {
                                                paramdata.status = "save";
                                            }
                                            billdata.head.userjson = JSON.stringify(paramdata);
                                            console.log(billdata);
                                            requestApi.submitBill({
                                                data: billdata,
                                                success: (data) => {
                                                    console.log(data);
                                                    let billid = data.head.head.rows[0].values.pk_jkbx.value;
                                                    props.go({
                                                        status: window.presetVar.status.browse,
                                                        id: billid,
                                                        teadetype: window.presetVar.tradetype,
                                                        scene: this.props.getUrlParam('scene')
                                                    });
                                                    this.props.button.setButtonsVisible({
                                                        [pageButton.pageSave]: false,
                                                        [pageButton.pageEdit]: true,
                                                        [pageButton.pageDel]: false,
                                                        [pageButton.pageCancel]: true
                                                    });
                                                    //处理按钮相关状态
                                                    dataHandle(data.head.head, this.props);
                                                    this.props.form.setFormStatus('head', 'browse');
                                                    this.props.billBodys && this.props.billBodys.forEach((body) => {
                                                        this.props.editTable.setStatus(body, 'browse');
                                                    });
                                                },
                                                error: (data) => {
                                                    toast({ content: data.message, color: 'danger' });
                                                }
                                            });
                                        },
                                    })

                                } else {
                                    console.log(data);
                                    let billid = data.head.head.rows[0].values.pk_jkbx.value;
                                    props.go({
                                        status: statusVar.browse,
                                        id: billid,
                                        tradetype: billtype,
                                        scene: this.props.getUrlParam('scene')
                                    });
                                    props.button.setButtonsVisible({
                                        [pageButton.pageSave]: false,
                                        [pageButton.pageEdit]: true,
                                        [pageButton.pageCancel]: true
                                    });
                                    //处理按钮相关状态
                                    dataHandle(data.head.head, props);
                                    props.form.setFormStatus('head', 'browse');
                                    props.billBodys && props.billBodys.forEach((body) => {
                                        props.editTable.setStatus(body, 'browse');
                                    });
                                }
                            }
                            window.assingUsers = [];
                        },
                        error: (data) => {
                            toast({ content: data.message, color: 'danger' });
                        }
                    });
                }, body1, null)
            }
        },

        //删除
        [pageButton.pageDel]: () => {
            let props = this.props;
            let allData = {
                openbillid: props.getUrlParam("id"),
            };
            requestApi.billRemove({
                data: allData,
                success: (data) => {
                    if (data.success) {
                        window.top.close();
                    }
                }
            });
        },

        //作废
        [pageButton.billInvalid]: () => {
            //作废的单据不设置表体还款信息
            props.button.setButtonsVisible({
                'billSubmit': false,
                'pageEdit': false,
                'pageDel': false,
                'pagePrint': false,
                'fileManager': false,
                'imageUpload': false,
                'imageShow': false,
                'billInvalid': false,
                'linkQueryJkbx': false,
            });
        },

        //打印
        [pageButton.pagePrint]: () => {
            let vdata = {};
            vdata.billId = props.getUrlParam('id') || props.getUrlParam('id');
            vdata.billType = '2647';
            ajax({
                url: '/nccloud/erm/expenseaccount/ErmPrintValidaAction.do',
                data: vdata,
                success: (data) => {
                    if (!data.data.canPrint) {
                        NCMessage.create({ content: data.data.errMesg, color: 'error', position: 'bottom' });
                    } else {
                        print(
                            'pdf',  //支持两类: 'html'为模板打印, 'pdf'为pdf打印
                            '/nccloud/erm/expenseaccount/ErmJkbxPrintAction.do', //后台服务url
                            {
                                billtype: billtype,  //单据类型
                                funcode: (props.getUrlParam("appcode") == undefined || props.getUrlParam("appcode") == '') ? props.getSearchParam("c") : props.getUrlParam("appcode"),   //功能节点编码，即模板编码
                                nodekey: '2647_IWEB',     //模板节点标识
                                oids: [props.getUrlParam('id')],    // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
                            }
                        )
                    }
                }
            })
        },

        //影响扫描
        [pageButton.imageUpload]: () => {
            let billdata = props.createExtCardData(props.meta.getMeta().code, "head", Object.values(window.presetVar.body));
            let openbillid = props.getUrlParam('id');
            var billInfoMap = {};

            //基础字段 单据pk,单据类型，交易类型，单据的组织
            billInfoMap.pk_billid = openbillid;
            billInfoMap.pk_billtype = billdata.head.head.rows[0].values.pk_billtype.value;
            billInfoMap.pk_tradetype = billdata.head.head.rows[0].values.djlxbm.value;
            billInfoMap.pk_org = billdata.head.head.rows[0].values.pk_org.value;

            //影像所需 FieldMap
            billInfoMap.BillType = billdata.head.head.rows[0].values.djlxbm.value;
            billInfoMap.BillDate = billdata.head.head.rows[0].values.djrq.value;
            billInfoMap.Busi_Serial_No = billdata.head.head.rows[0].values.pk_jkbx.value;
            billInfoMap.pk_billtype = billdata.head.head.rows[0].values.pk_billtype.value;
            billInfoMap.OrgNo = billdata.head.head.rows[0].values.pk_org.value;
            billInfoMap.BillCode = billdata.head.head.rows[0].values.djbh.value;
            billInfoMap.OrgName = billdata.head.head.rows[0].values.pk_org.display;
            billInfoMap.Cash = billdata.head.head.rows[0].values.vat_amount.value;

            imageScan(billInfoMap, 'iweb');
        },


        //影像查看
        [pageButton.imageShow]: () => {
            let billdata = props.createExtCardData(props.meta.getMeta().code, "head", Object.values(window.presetVar.body));
            let pk_billid = billdata.head.head.rows[0].values.pk_jkbx.value;
            let pk_billtype = billdata.head.head.rows[0].values.pk_billtype.value;
            let pk_tradetype = billdata.head.head.rows[0].values.djlxbm.value;
            let pk_org = billdata.head.head.rows[0].values.pk_org.value;
            imageView({ pk_billid, pk_billtype, pk_tradetype, pk_org }, 'iweb');
        },

        //联查借款单
        [pageButton.linkQueryJkbx]: () => {
            let billId = this.state.billId || props.getUrlParam("id");
            ajax({
                url: '/nccloud/erm/expenseaccount/expenseaccountLinkQueryAction.do',
                data: {
                    queryType: 'jkbx',
                    tradetype: window.presetVar.tradetype,//交易类型
                    openBillId: billId//单据主键ok
                },
                success: result => {
                    if (result.data) {
                        this.setState({
                            linkData: result,
                            showLinkQueryJkbxModel: true,
                            billId: billId
                        })
                    } else {
                        let multiLang = this.props.MutiInit.getIntl(2011); //"您联查的借款单没有数据
                        toast({ content: multiLang && multiLang.get('201102JCLF_C-0007'), color: 'warning' });
                    }
                }
            });
        },

        //附件管理
        [pageButton.fileManager]: () => {
            let props = this.props;
            let billId = props.getUrlParam('id') || this.state.billId || "";
            let billtype = window.presetVar.tradetype || "";
            if (billId == "" && !this.state.showUploader) {
                //去后台生成单据主键
                requestApi.generatBillId({
                    data: { billtype: billtype },
                    success: (data) => {
                        console.log(data);
                        let accessorypkfildname = data["pkfieldName"];
                        billId = data[accessorypkfildname];
                        this.setState({
                            billId: billId,
                            showUploader: !this.state.showUploader
                        });
                        this.accessorybillid = billId;
                        window.assignBillId = billId;
                    },
                    error: (data) => {
                        toast({ content: data.message, color: 'danger' });
                    }
                });
            } else {
                this.setState({
                    billId: billId,
                    showUploader: !this.state.showUploader
                });
            }
        },

        //修改
        [pageButton.pageEdit]: () => {
            let props = this.props;

            let multiLang = this.props.MutiInit.getIntl(2011);
            let scene = props.getUrlParam('scene');
            //获取单据编号
            let djbh = props.form.getFormItemsValue("head", ['djbh'])[0].value;
            let canEdit = true;
            let isMaker = true;
            //来源于审批中心
            if (scene == 'approve' || scene == 'approvesce') {
                isMaker = false;
                //判断单据是否是当前用户待审批
                ajax({
                    url: '/nccloud/riart/message/list.do',
                    async: false,
                    data: {
                        billno: djbh,
                        isread: 'N'
                    },
                    success: result => {
                        if (result.data) {
                            if (result.data.total < 1) {
                                toast({ content: multiLang && multiLang.get("201102BCLF-0026"), color: 'warning' });
                                canEdit = false;
                            }
                        }
                    }
                });
            }
            //来源于我的作业
            if (scene == 'zycl') {
                isMaker = false;
                //判断单据是否是当前用户待处理
                ajax({
                    url: '/nccloud/ssctp/sscbd/SSCTaskHandlePendingNumAction.do',
                    async: false,
                    data: {
                        billno: djbh
                    },
                    success: result => {
                        if (result.data) {
                            if (result.data.total < 1) {
                                toast({ content: multiLang && multiLang.get("201102BCLF-0027"), color: 'warning' });
                                canEdit = false;
                            }
                        }
                    }
                });
            }
            if (!canEdit) {
                return;
            }

            if (isMaker) {
                //驳回不重走流程单据，设置不可编辑字段
                requestApi.getUnEditAttributes({
                    data: {
                        'pk_org': props.form.getFormItemsValue("head", ['pk_org'])[0].value,
                        'billid': props.form.getFormItemsValue("head", ['pk_jkbx'])[0].value,
                        'billType': props.form.getFormItemsValue("head", ['djlxbm'])[0].value
                    },
                    success: (data) => {
                        if (!(data == undefined || data.length == 0)) {
                            let formobj = {};
                            let arap_bxbusitemobj = [];
                            let er_bxcontrastobj = [];

                            data.forEach((item) => {
                                if (item.indexOf('er_busitem.') > -1) {
                                    arap_bxbusitemobj.push(item.split('.')[1]);
                                } else if (item.indexOf('er_bxcontrast.') > -1) {
                                    er_bxcontrastobj.push(item.split('.')[1]);
                                } else {
                                    formobj[item] = true;
                                }
                            })
                            props.form.setFormItemsDisabled('head', formobj)
                            props.cardTable.setColEditableByKey('arap_bxbusitem', arap_bxbusitemobj)
                            props.cardTable.setColEditableByKey('er_bxcontrast', er_bxcontrastobj)
                        }
                    }
                })
            }


            props.go({
                status: statusVar.edit,
                id: props.getUrlParam('id'),
                tradetype: window.presetVar.tradetype,
                scene: props.getUrlParam('scene')
            });
            props.button.setButtonsVisible({
                [pageButton.pageSave]: true,
                [pageButton.pageCancel]: true,
                [pageButton.pageEdit]: false,
                [pageButton.billSubmit]: false
            });
        },

        //收回
        [pageButton.billRecall]: () => {
            let param = {};
            param.openbillid = this.props.getUrlParam('id') || this.props.getUrlParam('billid');
            param.pagecode = this.props.getSearchParam("p");
            requestApi.billRecall({
                data: param,
                success: (data) => {
                    let billid = data.head.head.rows[0].values.pk_jkbx.value;
                    //location.hash = `#status=${statusVar.browse}&id=${billid}&tradetype=${window.presetVar.tradetype}&flow=recall`;
                    props.go({
                        status: statusVar.browse,
                        id: billid,
                        teadetype: window.presetVar.tradetype,
                        flow: 'recall',
                        scene: this.props.getUrlParam('scene')
                    });
                    props.button.setButtonsVisible({
                        [pageButton.pageSave]: false,
                        [pageButton.pageEdit]: true
                    });
                }
            });
        },

        //联查预算
        [pageButton.linkBudget]: () => {
            linkBudgetApi.link({
                data: {
                    "tradetype": this.props.getUrlParam('tradetype') || window.presetVar.tradetype,
                    "openbillid": this.props.getUrlParam('id')
                },
                success: (data) => {
                    this.setState({
                        sourceData: data,
                        showLinkBudget: true
                    })
                }
            })
        },

        //联查凭证
        [pageButton.linkVoucher]: () => {
            let paramurl = getRequest1(parent.window.location.hash);
            //如果没取到应用主键再取一下外层,主要适配作业任务打开单据时联查凭证
            if (!paramurl.ar) {
                paramurl = getRequest1(parent.parent.window.location.hash);
            }
            if (paramurl.ar) {
                let billdata = props.createExtCardData(props.meta.getMeta().code, "head", Object.values(window.presetVar.body));
                if (billdata.head.head.rows.length) {
                    linkVoucherApi.link({
                        data: {
                            props: props,
                            record: {
                                pk_billtype: this.props.getUrlParam('tradetype') || window.presetVar.tradetype,
                                pk_group: billdata.head.head.rows[0].values.pk_group.value,
                                pk_org: billdata.head.head.rows[0].values.pk_org.value,
                                relationID: this.props.getUrlParam('id')
                            },
                            appid: paramurl.ar
                        }
                    })
                }
            } else {
                toast({ content: multiLang && multiLang['201102HKD-0008'], color: 'danger' });//'联查凭证失败，缺少应用主键',
            }
        },

        //审批
        [pageButton.billApprove]: () => {
            let billdata = props.createExtCardData(props.meta.getMeta().code, "head", Object.values(window.presetVar.body));
            this.setState({
                ShowApproveDetail: true,
                billId: this.props.getUrlParam('id'),
                billtype: this.props.getUrlParam('tradetype') || window.presetVar.tradetype
            });
        }
    }
}

export default pageButtonClick;
