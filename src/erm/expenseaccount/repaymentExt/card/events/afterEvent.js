import {ajax,base,promptBox,getMultiLang} from 'nc-lightapp-front';
import requestApi from '../requestApi';
import {updataFormData, updatacardTableData} from 'ssccommon/utils/changePageData';

const {NCMessage} = base;

export default function afterEvent(props,moduleId,key,value,changedrows,index,attr) {

    getMultiLang({moduleId: 2011, domainName: 'erm',currentLocale: 'zh-CN', callback: (json) => {
    let paramdata = {};
    let moduleid =moduleId =="head"?"headform":moduleId;
    let pagecode = props.getSearchParam("p");
    let bodys = Object.values(window.presetVar.body);
    let bodyarr = [];
    let hideTable = this.state.hideTables;
    let _this = this;
    bodys.map((one)=>{
        if(hideTable.indexOf(one)<0){
            bodyarr[bodyarr.length] = one;
        }
    })
    if(moduleid=="headform"){
        // let billdata = props.createExtCardData(window.presetVar.pageId, window.presetVar.head.head1,bodyarr);
        // requestApi.handleLoadDataResDada(props.meta.getMeta(), {data:billdata});
    }
    paramdata = {
        "value":value,
        "tableid":moduleid,
        "field":key,
        "uistate":props.getUrlParam("status"),
        "tradetype":props.getUrlParam("tradetype")||window.presetVar.tradetype,
        "oldvalue":'',
        "pagecode":pagecode,
        checkrule:'true'
    }
    if(moduleid!="headform"){
        paramdata.changeRowId=changedrows[0].rowid;
    }else{
        paramdata.value = value.value;
    }

    //let send_data={billdata,paramdata};
    //编辑后事件
    // switch (key) {
    //     case 'vat_amount':
    //         console.log(billdata);
    //         requestApi.valueChange({
    //             data: send_data,
    //             success: (data)=> {
    //                 console.log(data);
    //                 props.form.setAllFormValue({[presetVar.head.head1]:data.head[presetVar.head.head1]});
    //                 props.cardTable.setTableData(moduleId, data.bodys[moduleId]);
    //             }
    //         });
    //         break;
    //     case 'tni_amount':
    //         console.log(billdata);
    //         requestApi.valueChange({
    //             data: send_data,
    //             success: (data)=> {
    //                 console.log(data);
    //                 props.form.setAllFormValue({[presetVar.head.head1]:data.head[presetVar.head.head1]});
    //                 props.cardTable.setTableData(moduleId, data.bodys[moduleId]);
    //             }
    //         });
    //         break;
    //     default:
    //         break;
    // }



    switch(moduleId) {
        case 'head' :
            switch(key) {
                case 'pk_org'://表头财务组织
                case 'pk_org_v'://表头财务组织
                if(value==undefined||value.value==null){
                    break;
                }
                case 'djrq'://表头单据日期
                case 'bzbm'://表头币种
                case 'bbhl'://表头本币汇率
                case 'dwbm_v'://表头借款报销人单位版本
                case 'dwbm'://表头借款报销人单位
                case 'fydwbm_v'://表头费用承担单位版本
                case 'fydwbm'://表头费用承担单位
                case 'pk_payorg_v'://表头支付单位版本
                case 'pk_payorg'://表头支付单位
                case 'deptid_v':// 表头报销人部门版本
                case 'deptid':// 表头报销人部门
                case 'fydeptid_v':// 表头费用承担部门版本
                case 'fydeptid':// 表头费用承担部门
                case "jkbxr"://借款报销人
                case "receiver"://表头收款人
                case "szxmid"://表头收支项目
                case "skyhzh"://表头个人银行账户
                case "jobid"://表头项目
                case "freecust"://散户
                case "custaccount"://表头客商银行账户
                case "paytarget"://表头收款对象
                    if(key=='paytarget'){
                        if(value.value=="0"){
                            props.form.setFormItemsDisabled("head",{'hbbm':true,'customer':true,"custaccount":true,'receiver':false,'custaccount':true,'freecust':true});
                        }else if(value.value=="1"){
                            props.form.setFormItemsDisabled("head",{'receiver':true,'customer':true,'hbbm':false,'custaccount':false,'freecust':false});
                        }else if(value.value=="2"){
                            props.form.setFormItemsDisabled("head",{'hbbm':true,'receiver':true,'customer':false,'custaccount':false,'freecust':true});
                        }
                    }
                case "assume_amount"://分摊页签表体承担金额编辑后事件
                case "assume_org"://分摊页签表体承担单位编辑后事件
                case "hbbm"://表头供应商
                case "customer"://表头客户
                    // console.log('head数据', props.form, props.form.getAllFormValue('head'));
                    // let headdata = props.form.getAllFormValue('head');
                    // let send_headdata = {headdata,paramdata};
                    // requestApi.valueChange({
                    //     data: send_data,
                    //     success: (data)=> {
                    //         console.log(data);
                    //         props.form.setAllFormValue({[presetVar.head.head1]:data.head[presetVar.head.head1]});
                    //         Object.keys(presetVar.body).forEach((item) =>{
                    //             props.cardTable.setTableData(item,data.data.body[item]);
                    //         })
                    //     }
                    // });
                    //   let onlyHeadData = props.createFormAfterEventData('20110ETEA_2641_IWEB', "head", moduleId, key, value);
                    //  console.log('表头字段change后仅有表头数据结构：', onlyHeadData)
                    //表头修改后数据后台需要的结构， 一主一子  一主多子
                    let billdata = props.createHeadAfterEventData('20110RB_2647_IWEB', "head", Object.values(window.presetVar.body), moduleId, key, value);
                    console.log('表头字段change后表头标题数据结构：', billdata)
                     let send_headdata = {billdata,paramdata};
                    // TOP NCCLOUD-91597 校验报销标准取模板时需要使用模板ID chengwbc ADD
	                send_headdata.templateid = props.meta.getMeta().pageid;
	                // BTM NCCLOUD-91597 校验报销标准取模板时需要使用模板ID
                    requestApi.valueChange({
                        data: send_headdata,
                        success: (data)=> {
                            // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                            //设置表头数据
                            // props.form.setAllFormValue({[presetVar.head.head1]: data.head[presetVar.head.head1]});
                            updataFormData(props, presetVar.head.head1, data.head[presetVar.head.head1]);
                            // BTM 性能优化，变更页面数据更新方法
                            //设置表体数据
                            // props.cardTable.setTableData(presetVar.body.body1, data.data.body[presetVar.body.body1]);
                            //'pk_org','assume_org','assume_dept','pk_iobsclass'
                            // let bmetas  = props.meta.getMeta();
                            // for(let body in data.bodys){
                            //     if(data.bodys[body] && data.bodys[body].rows){
                            //         data.bodys[body].rows.forEach((row) => {
                            //             for (let attrCode in row.values){
                            //                 let initialvalue = {
                            //                     value: row.values[attrCode].value,
                            //                     display: row.values[attrCode].display
                            //                 };
                            //                 for (let table in bmetas.gridrelation) {
                            //                     bmetas[table].items.forEach((item) => {
                            //                         if(item.itemtype == 'refer'){
                            //                             item.visible = true;//zheli fangkai
                            //                             item.disabled = false;
                            //                         }
                            //                         if (item.attrcode == attrCode){
                            //                             item.initialvalue = initialvalue;
                            //                         }
                            //                     });
                            //                 }
                            //             }
                            //         });
                            //     }
                            // }
                            // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                            // bodyarr.forEach((item) =>{
                            //     setTimeout(()=>{
                            //         props.cardTable.setTableData(item, data.bodys[item]);
                            //     },0);
                            // })
                            bodyarr.forEach((item) =>{
                                setTimeout(()=>{
                                    updatacardTableData(props, item, data.bodys[item]);
                                },0);
                            })
                            // BTM 性能优化，变更页面数据更新方法
                            //表头值变化后为表体元数据赋值
                            let headrows = data.head[presetVar.head.head1].rows[0].values;
                            let headtrr = ['pk_org','szxmid','pk_pcorg','pk_pcorg_v','pk_checkele','jobid','projecttask','pk_resacostcenter','pk_proline','pk_brand','fctno','bzbm','tax_rate','bbhl','groupbbhl','globalbbhl','receiver','freecust','jkbxr','dwbm','skyhzh','custaccount','customer','deptid','paytarget','hbbm'];
                            let cshare_detail = ['assume_org','assume_dept','pk_iobsclass'];//'fydwbm','fydeptid',
                            let metas  = props.meta.getMeta();
                            //Object.keys(presetVar.body).forEach((bodyindex) =>{
                            for (let bodyname in metas.gridrelation) {
                               // let bodyname = presetVar.body[bodyindex];
                                if(metas[bodyname].items!=undefined&&metas[bodyname].items!=null&&bodyname!='er_bxcontrast'&&bodyname!='accrued_verify'&&bodyname!='head'){
                                    metas[bodyname].items.forEach((item) => {
                                        if (headtrr.indexOf(item.attrcode) > -1 || cshare_detail.indexOf(item.attrcode) > -1) {
                                            let keycode = '';
                                            if(item.attrcode=='assume_org'){
                                                keycode = 'fydwbm';
                                            }else if(item.attrcode=='assume_dept'){
                                                keycode = 'fydwbm';
                                            }else if(item.attrcode=='pk_iobsclass'){
                                                keycode = 'szxmid';
                                            }else{
                                                keycode = item.attrcode;
                                            }
                                            if(headrows[keycode]!=undefined && headrows[keycode].value!=undefined){
                                                let valuecode = headrows[keycode].value==undefined?'':headrows[keycode].value;
                                                let displaycode = valuecode==''?'':(headrows[keycode].display==undefined||headrows[keycode].display==''?valuecode:headrows[keycode].display)
                                                let initialvalue={
                                                    value: valuecode,
                                                    display: displaycode
                                                }
                                                item.initialvalue =initialvalue;
                                            }
                                        }
                                    });
                                   // props.editTable.setStatus(bodyname, 'edit');
                                }
                            };
                            requestApi.handleLoadDataResDada(props.meta.getMeta(), {data},props);
                        }
                    })
                    break;
                default:
                    break;
            }
            break;
        default:
            //表体字段处理
            switch (key){
                case 'pk_org'://表体财务组织
                case 'djrq'://表体单据日期
                case 'bzbm'://表体币种
                case 'bbhl'://表体本币汇率
                case 'dwbm_v'://表体借款报销人单位版本
                case 'dwbm'://表体借款报销人单位
                case 'fydwbm_v'://表体费用承担单位版本
                case 'fydwbm'://表体费用承担单位
                case 'pk_payorg_v'://表体支付单位版本
                case 'pk_payorg'://表体支付单位
                case 'deptid_v':// 表体报销人部门版本
                case 'deptid':// 表体报销人部门
                case 'fydeptid_v':// 表体费用承担部门版本
                case 'fydeptid':// 表体费用承担部门
                case "jkbxr"://借款报销人
                case "receiver"://表体收款人
                case "szxmid"://表体收支项目
                case "skyhzh"://表体个人银行账户
                case "jobid"://表体项目
                case "freecust"://散户
                case "custaccount"://表体客商银行账户
                case "paytarget"://表体收款对象
                case "hbbm"://表体供应商
                case "customer"://表体客户
                case "vat_amount"://表体含税金额
                case 'tni_amount'://表体不含税金额
                case 'tax_amount'://表体税金
                case 'amount'://表体报销金额
                case 'ybje'://原币金额
                case 'tax_rate'://表体税率
                    //let billdata = props.createHeadAfterEventData('20110ETEA_2641_IWEB', "head",[moduleid], moduleId, key, value);
                   // let data = props.createGridAfterEventData('20521030', moduleid, moduleId, key, changedrows);
                   // let billdata = props.createBodyAfterEventData('20110ETEA_2641_IWEB', 'head', moduleid, moduleId, key, changedrows);
                    if(changedrows[0].newvalue.value==changedrows[0].oldvalue.value){
                        break;
                    }
                    let billdata = props.createHeadAfterEventData('20110RB_2647_IWEB', "head", bodyarr, moduleId, key, value);
                    console.log('表头字段change后表头标题数据结构：', billdata);
                    if(value.value==undefined||value.value==null){
                        paramdata.value = changedrows[0].newvalue.value;
                    }

                    let send_headdata = {billdata,paramdata};
                    // TOP NCCLOUD-91597 校验报销标准取模板时需要使用模板ID chengwbc ADD
	                send_headdata.templateid = props.meta.getMeta().pageid;
	                // BTM NCCLOUD-91597 校验报销标准取模板时需要使用模板ID
                    requestApi.valueChange({
                        data: send_headdata,
                        success: (data)=> {
                            //设置表头数据
                            // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                            // props.form.setAllFormValue({[presetVar.head.head1]: requestApi.filterDatas(data.head[presetVar.head.head1])});
                            updataFormData(props, presetVar.head.head1, requestApi.filterDatas(data.head[presetVar.head.head1]));
                            // BTM 性能优化，变更页面数据更新方法
                            //设置表体数据
                            // props.cardTable.setTableData(presetVar.body.body1, data.data.body[presetVar.body.body1]);
                            // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                            // setTimeout(()=>{
                            //     props.cardTable.setTableData(moduleid, data.bodys[moduleid]);
                            // },0);
                            setTimeout(()=>{
                                updatacardTableData(props, moduleid, data.bodys[moduleid]);
                            },0);
                            // BTM 性能优化，变更页面数据更新方法
                            let billdatas = props.createExtCardData(window.presetVar.pageId, window.presetVar.head.head1,bodyarr);
                            requestApi.handleLoadDataResDada(props.meta.getMeta(), {data:billdatas},props);
                        }
                    })
                // let onlyHeadData = props.createFormAfterEventData('20521030', presetVar.head.head1, moduleId, key, value);
                // console.log('表头字段change后仅有表头数据结构：', onlyHeadData)
                //表头修改后数据后台需要的结构， 一主一子  一主多子
                //     let headdata = props.createHeadAfterEventData('20110ETEA_2641_IWEB', presetVar.head.head1, moduleId, moduleId, key, value);
                //     console.log('表头字段change后表头标题数据结构：', headBodyData)
                //     let send_headdata = {headdata,paramdata};
                //
                //     requestApi.changeByHead({
                //     data: send_headdata,
                //     success: (data)=> {
                //         //设置表头数据
                //         props.form.setAllFormValue({[presetVar.head.head1]: data.data.head[presetVar.head.head1]});
                //         //设置表体数据
                //         props.cardTable.setTableData(presetVar.body.body1, data.data.body[presetVar.body.body1]);
                //
                //     }
                // })
            }
            break;
    }
}})
};