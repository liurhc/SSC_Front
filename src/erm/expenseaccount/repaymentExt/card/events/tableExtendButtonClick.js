/**
 * 表格扩展按钮click事件
 */
import {ajax} from 'nc-lightapp-front';

const areaBtnAction = window.presetVar.areaBtnAction;
const body1 = window.presetVar.body.body1;
const statusVar = window.presetVar.status;

function tableExtendButtonClick(props, btnKey, record, index) {
    switch (btnKey) {
        case body1 + areaBtnAction.edit :
            this.sscButton.setOperationButtonsVisible(body1, {
                [body1 + areaBtnAction.del] : false
            });
            props.cardTable.openModel(body1, statusVar.edit, record, index);
            break;
        case  body1 +  areaBtnAction.del :
            props.cardTable.delRowsByIndex(body1, index);
            break;
        default:
            break;

    }
}

export default tableExtendButtonClick;