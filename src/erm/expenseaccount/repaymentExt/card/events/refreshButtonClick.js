import {render, getCookie, dataHandle} from '../initTemplate'
import requestApi from '../requestApi';
import 'ssccommon/components/globalPresetVar';
import presetVar from '../presetVar';
import { cardCache } from 'nc-lightapp-front';
let { updateCache } = cardCache;

export default function () {
    let props = this.props;
    let pageStatus = props.getUrlParam('status');
    
    let pagecode = (props.getUrlParam("pagecode")==undefined||props.getUrlParam("pagecode")=='')?props.getSearchParam("p"):props.getUrlParam("pagecode");
    let appcode = (props.getUrlParam("appcode")==undefined||props.getUrlParam("appcode")=='')?props.getSearchParam("c"):props.getUrlParam("appcode");  
        let billdata = {};
        billdata.openbillid = props.getUrlParam("id")==undefined ? props.getUrlParam("billid"):props.getUrlParam("id");
        billdata.tradetype = props.getUrlParam("tradetype")||window.presetVar.tradetype;
        billdata.pagecode = pagecode;
        requestApi.viewBill({
            data:billdata,
            success:(data) =>{
                props.form.setAllFormValue({[window.presetVar.head.head1]:data.head[window.presetVar.head.head1]});
                //设置表体数据
                Object.values(window.presetVar.body).forEach((body) =>{
                    if(data.bodys[body]){
                        props.cardTable.setTableData(body, data.bodys[body]);
                    }
                });
                dataHandle(data.head["head"],props);
                props.form.setFormStatus('head', 'browse');
                Object.values(window.presetVar.body).forEach((body)=>{
                    props.editTable.setStatus(body, 'browse');
                });
                
                this.pubMessage.refreshSuccess();
            }
        });
}
