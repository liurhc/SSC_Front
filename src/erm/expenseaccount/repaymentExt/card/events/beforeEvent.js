import {ajax,base} from 'nc-lightapp-front';
import requestApi from '../requestApi';
// let referResultFilterCondition='';
const {NCMessage} = base;
//只对表体做编辑前事件处理
export default function beforeEvent(props,moduleId,key,value,index,changedrows) {
    let billdata = props.createExtCardData(window.presetVar.pageId, window.presetVar.head.head1,Object.values(window.presetVar.body));
    let moduleid =moduleId =="head"?"headform":moduleId;
    let referResultFilterCondition='';
    if(moduleid=="head"){
        return true;
    }
    let meta = props.meta.getMeta();
    let bodyValues = changedrows.values;
    let headValues = billdata.head[presetVar.head.head1].rows[0].values;
    let attrcode  = key;
        meta[moduleId].items.forEach((item) => {
            if (item.attrcode == attrcode&&item.itemtype == 'refer') {
                item.initialvalue = {
                    value: bodyValues[attrcode].value,
                    display: bodyValues[attrcode].display = null ? bodyValues[attrcode].value : bodyValues[attrcode].display
                };
                // let filterCondition = referResultFilterCondition[moduleId][item.attrcode];
                // if (filterCondition) {
                //     item.referFilterAttr = {[item.attrcode]: filterCondition};
                // }
                //处理交叉校验规则和参照过滤
                requestApi.filterAllRefParm(item, bodyValues, headValues,props,moduleId);
            }

        });

        window.presetVar.bodyChangeValuesAll = changedrows;
        return true;
};