/**
 * 表格肩部按钮click事件
 */
import {ajax} from 'nc-lightapp-front';

const areaBtnAction = window.presetVar.areaBtnAction;
const statusVar = window.presetVar.status;
const bodys = window.presetVar.body;

function tableButtonClick() {
    return {
        ['arap_bxbusitem-add'] : () => {
            let data = this.props.createExtCardData('20110ETEA_2641_IWEB', 'head', 'arap_bxbusitem');
            this.props.cardTable.addRow('arap_bxbusitem', undefined, null, false);
        }
    }
}

export default tableButtonClick;