import requestApi from "ssccommon/components/requestApi";
import {ajax,toast } from 'nc-lightapp-front';
let requestDomain =  '';
let referResultFilterCondition='';
let pageCode = window.presetVar.pageCode;
let requestApiOverwrite = {
    ...requestApi,
    //单据模板接口
    tpl: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/expenseaccount/HkTempletAction.do`,
            data: opt.data,
            success: opt.success
        });
    },
    loaddata:(opt) => {
        let props = opt.props;
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/expenseaccount/HkDefDataAction.do?tradetype=2647&uistatus=add`,
            data: opt.data,
            success: (data) => {
                if(data.data.userjson && JSON.parse(data.data.userjson)){
                    let djlx = JSON.parse(data.data.userjson);
                    if(djlx && djlx.ISCONTRAST && djlx.jkyjye && djlx.jkyjye >0){//是否冲借款
                        window.presetVar["ISCONTRAST"]=djlx.ISCONTRAST;
                    }
                    if(djlx && djlx.IS_MACTRL){//是否冲借款
                        window.presetVar["IS_MACTRL"]=djlx.IS_MACTRL;
                    }
                }

                let headValues = data.data.head[presetVar.head.head1].rows[0].values;
                data.data.head['head'].rows.forEach((row) => {
                    for (let attrCode in row.values) {
                        let initialvalue = {
                            value : row.values[attrCode].value,
                            display: row.values[attrCode].display,
                            scale: row.values[attrCode].scale
                        };
                        opt.meta['head'].items.forEach((item) => {
                            if (item.attrcode == attrCode && attrCode.indexOf('.')<0) {
                                item.initialvalue = initialvalue;
                                if(item.itemtype == 'refer'){
                  
                                    //处理交叉校验规则和参照过滤
                                    filterAllRefParm(item,props, 'head');
                                }
                            }
                        });
                    }
                });
                for(let body in data.data.bodys){
                    if(data.data.bodys[body] && data.data.bodys[body].rows){
                        data.data.bodys[body].rows.forEach((row) => {
                            for (let attrCode in row.values){
                                let initialvalue = {
                                    value: row.values[attrCode].value,
                                    display: row.values[attrCode].display,
                                    scale: row.values[attrCode].scale
                                };
                                for (let table in opt.meta.gridrelation) {
                                    opt.meta[table].items.forEach((item) => {
                                        if(item.attrcode=='tablecode'){
                                            let itvalue = {
                                                value:table,
                                                display:table
                                            }
                                            item.initialvalue = itvalue;
                                        }
                                    //    if(item.itemtype == 'refer'){
                                    //        item.visible = true;//zheli fangkai
                                   //         item.disabled = false;
                                    //    }
                                        if (item.attrcode == attrCode && attrCode.indexOf('.')<0){
                                            item.initialvalue = initialvalue;
                                    
                                            //处理交叉校验规则和参照过滤
                                            let headValues = data.data.head[presetVar.head.head1].rows[0].values;
                                            filterAllRefParm(item,props, table);
                                        }
                                    });
                                }
                            }
                        });
                    }

                }
                opt.success(data.data);
            }
        })
    },
    savejkbx:(opt) => {

        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/expenseaccount/HkSaveBillAction.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                if(JSON.parse(data.userjson) && JSON.parse(data.userjson).reimmsg && JSON.parse(data.userjson).reimmsg.length>0)
                    toast({content:JSON.parse(data.userjson).reimmsg,color:'warning'});
                opt.success(data);
            },
            error:(data)=>{
                toast({content:data.message,color:'warning'});
            }
        })
    },
    valueChange:(opt) => {

        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/expenseaccount/HkValueChangedAction.do`,
            data: opt.data,
            async:false,
            success: (data) => {
               // referResultFilterCondition = JSON.parse(JSON.parse(data.data.userjson).crossrule);//交叉校验规则设置
                data = data.data;
                opt.success(data);
            }
        })
    },
    viewBill:(opt) =>{
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/expenseaccount/HkViewBillAction.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        });
    },
    submitBill:(opt) =>{
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/expenseaccount/HkSubmitBillAction.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                if(data.success!=undefined&&data.success=='false'){
                    toast({content:data.message,color:'warning'});
                    return;
                }
                if(data.userjson && JSON.parse(data.userjson) && JSON.parse(data.userjson).reimmsg && JSON.parse(data.userjson).reimmsg.length>0)
                    toast({content:JSON.parse(data.userjson).reimmsg,color:'warning'});
                opt.success(data);
            },
            error:(data) =>{
                toast({content:data.message,color:'warning'});
            }
        });
    },
    generatBillId:(opt) =>{
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/action/generatBillId.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            },
            error:(data) =>{
                toast({content:data.message,color:'warning'});
            }
        });
    },
    billRemove: (opt) => {
        //数据适配开始
        let openbillid="";
        //数据适配结束
        opt.data = {
            ...opt.data,
            openbillid:opt.data.openbillid
        }
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/expenseaccount/HkDeleteBillAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            },
            error:(data) =>{
                toast({content:data.message,color:'warning'});
            }
        });

    },
    billInvalid: (opt) => {
        let openbillid="";

        opt.data.pageid=pageCode;
        opt.data = {
            ...opt.data,
            userjson:"{'pagecode':"+pageCode+
            ",'openbillid':"+opt.data.openbillid+
            ",'tradetype':'2647'"+
            ",'ntbCheck':'N'"+
            ",'accessorybillid':"+null+
            ",'fromssc':'N'"+
            ",'uistate':"+opt.data.status+
            "}"
        }
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/expenseaccount/HkInvalidBillAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            },
            error:(data) =>{
                toast({content:data.message,color:'warning'});
            }
        });

    },
    billRecall:(opt) =>{
        let openbillid=opt.data.openbillid;
        opt.data.pageid=pageCode;
        opt.data = {
            ...opt.data,
            userjson:"{'pagecode':"+pageCode+
            ",'openbillid':"+openbillid+
            "}"
        }
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/expenseaccount/HkRecallBillAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data.data);
            },
            error:(data) =>{
                toast({content:data.message,color:'warning'});
            }
        });
    },
    getUnEditAttributes:(opt) =>{
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/pub/ErmAttributeEditableAction.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            },
            error:(data) =>{
                toast({content:data.message,color:'warning'});
            }
        });
    },
    handleLoadDataResDada:(meta,data,props) =>{
       handleLoadDataResDada(meta, data,props)
    },
    filterAllRefParm:(item,props, table) =>{
        filterAllRefParm(item,props, table)
    },
    filterDatas:(data) =>{
        return filterDatas(data)
    }
}
function handleLoadDataResDada(meta, data,props) {
    if(data.data.head[presetVar.head.head1].rows[0])
    {
        let headValues = data.data.head[presetVar.head.head1].rows[0].values;
        for (let attrcode in headValues) {
            meta[presetVar.head.head1].items.forEach((item) => {
                if (item.attrcode == attrcode) {
                    item.initialvalue = {
                        value: headValues[attrcode].value,
                        display: headValues[attrcode].display=null?headValues[attrcode].value:headValues[attrcode].display
                    };
                    //处理交叉校验规则和参照过滤
                    filterAllRefParm(item,props,presetVar.head.head1);
                }

            });
        }
    }
    Object.keys(presetVar.body).forEach((bodyindex) =>{
        let bodyname = presetVar.body[bodyindex];
        if(data.data.bodys[bodyname] && data.data.bodys[bodyname].rows){
            data.data.bodys[bodyname].rows.forEach((row) => {
                let bodyValues = row.values;
                for (let attrcode in bodyValues) {
                    meta[bodyname].items.forEach((item) => {
                        if (item.attrcode == attrcode) {
                            item.initialvalue = {
                                value: bodyValues[attrcode].value,
                                display: bodyValues[attrcode].display = null ? bodyValues[attrcode].value : bodyValues[attrcode].display
                            };
                        }
                    });
                }
            });
        }
    });
}
function filterDatas(data){
    if(data==null){
        return {rows:[]}
    }
    let rows = data.rows;
    for(let i=0;i<rows.length;i++){
        if(rows[i].values==undefined){
            continue;
        }
        let values = rows[i].values;
        for(let item in values){
            if(item.indexOf('.')>-1&&values[item]!=undefined&&values[item]!=null){
               let itemp = item.substring(0,item.indexOf('.'));
                values[item] = {value:null,display:null,scale:null};
            }
        }
    }
    return data
}

function filterAllRefParm(item,props,moudleid){
    let PublicDefaultRefFilterPath = 'nccloud.web.action.erm.ref.sqlbuilder.PublicDefaultRefSqlBuilder';//公共 
    let BusinessUnitTreeRefFilterPath ='nccloud.web.action.erm.ref.sqlbuilder.BusinessUnitTreeRefSqlBuilder';//组织
    let DeptClassTreeGridRef ='nccloud.web.action.erm.ref.sqlbuilder.DeptClassTreeGridRef';//部门
    let DeptTreeGridRef ='nccloud.web.action.erm.ref.sqlbuilder.DeptTreeGridRef';//部门
    let CurrtypeRefSqlFilterPath ='nccloud.web.action.erm.ref.sqlbuilder.CurrtypeRefSqlBuilder';//币种
    let PsndocClassFilterPath ='nccloud.web.action.erm.ref.sqlbuilder.PsndocClassTreeGridRef';//人员
    let PsndocTreeFilterPath ='nccloud.web.action.erm.ref.sqlbuilder.PsndocTreeGridRef';//人员
    let PsnbankaccRefSqlBuilder ='nccloud.web.action.erm.ref.sqlbuilder.PsnbankaccRefSqlBuilder';//个人银行账户
    let PsnbankaccTreeGridRef ='nccloud.web.action.erm.ref.sqlbuilder.PsnbankaccTreeGridRef';//个人银行账户
    let BankacctreeGridRef ='nccloud.web.action.erm.ref.sqlbuilder.BankacctreeGridRef';//个人银行账户
    let SupplierTreeGridRef ='nccloud.web.action.erm.ref.sqlbuilder.SupplierTreeGridRef';//供应商
    let ProjecTreetGridRef ='nccloud.web.action.erm.ref.sqlbuilder.ProjecTreetGridRef' ;//项目
    let CustomerTreeGridRef='nccloud.web.action.erm.ref.sqlbuilder.CustomerTreeGridRef';//客户
    let PublicDefaultGridRef='nccloud.web.action.erm.ref.sqlbuilder.PublicDefaultRefSqlBuilder';//公共
    let OrgTreeRefFilterPath ='nccloud.web.action.erm.ref.sqlbuilder.OrgTreeRefSqlBuilder';//组织

    let filterold = item.referFilterAttr;
    let lengthArr = 1;
    if(filterold)
        lengthArr = filterold.length;
    let filter = new Array(lengthArr);
    if(filter){
        if(filter[item.attrcode]=="null"){
            filter[item.attrcode] ='';
        }else {

            if(filter[item.attrcode] && filter[item.attrcode].length>0)
            {filter[item.attrcode] = filter[item.attrcode].join(",");}
            else{filter[item.attrcode] ='';}
        }
    }else{filter ={};filter[item.attrcode] ='';}


    //所属组织，待确定是否需要特殊处理
    if (item.attrcode == 'pk_org'|| item.attrcode == 'pk_org_v') {
        item.queryCondition = () => {
            let paramurl = getRequest1(parent.window.location.hash);
            //如果没取到应用编码再取一下外层
            if(!paramurl.c){
                paramurl = getRequest1(parent.parent.window.location.hash);
            }
            let data = (moudleid == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['pk_group'].value) : (props.cardTable.getClickRowIndex(moudleid).record.values['pk_group'].value))
            return {pk_group: data, TreeRefActionExt:OrgTreeRefFilterPath,DataPowerOperationCode:'fi',/**使用权组**/isDataPowerEnable:'Y', appcode : paramurl.c}; // 根据pk_group过滤
        };
    }

    //部门
    if(item.attrcode=='deptid'){        
        item.queryCondition = ()=>{
            let data = (moudleid == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['dwbm'].value) : (props.cardTable.getClickRowIndex(moudleid).record.values['dwbm'].value))
            
            return {pk_org: data,TreeRefActionExt:DeptTreeGridRef,DataPowerOperationCode:'fi',/**使用权组**/isDataPowerEnable:'Y'}; // 根据apply_org过滤
        }
     }

     //部门多版本
     if(item.attrcode=='deptid_v'){        
        item.queryCondition = ()=>{
            let data = (moudleid == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['dwbm'].value) : (props.cardTable.getClickRowIndex(moudleid).record.values['dwbm'].value))
            
            return {pk_org: data,DataPowerOperationCode:'fi',/**使用权组**/isDataPowerEnable:'Y'}; // 根据apply_org过滤
        }
     }

    //费用承担部门
    if(item.attrcode=='fydeptid'){        
        item.queryCondition = ()=>{
            let data = (moudleid == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['fydwbm'].value) : (props.cardTable.getClickRowIndex(moudleid).record.values['fydwbm'].value))
            return {pk_org: data,TreeRefActionExt:DeptTreeGridRef,DataPowerOperationCode:'fi',/**使用权组**/isDataPowerEnable:'Y'}; // 根据apply_org过滤
        }
    }

    //费用承担部门版本
    if(item.attrcode=='fydeptid_v'){        
        item.queryCondition = ()=>{
            let data = (moudleid == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['fydwbm'].value) : (props.cardTable.getClickRowIndex(moudleid).record.values['fydwbm'].value))
            return {pk_org: data,DataPowerOperationCode:'fi',/**使用权组**/isDataPowerEnable:'Y'}; // 根据apply_org过滤
        }
    }

    //费用承担部门
    if(item.attrcode=='assume_dept'){        
        item.queryCondition = ()=>{
            let data = (moudleid == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['assume_org'].value) : (props.cardTable.getClickRowIndex(moudleid).record.values['assume_org'].value))
            return {pk_org: data,DataPowerOperationCode:'fi',/**使用权组**/isDataPowerEnable:'Y'}; // 根据apply_org过滤
        }
    }

    //收支项目
    if (item.attrcode == 'szxmid') {
        item.queryCondition = () => {            
           let data  = props.form.getAllFormValue(presetVar.head.head1).rows[0].values['fydwbm'].value;
            return {pk_org: data, TreeRefActionExt:PublicDefaultRefFilterPath,DataPowerOperationCode:'fi',/**使用权组**/isDataPowerEnable:'Y'}; // 根据assume_org过滤
        };
    }

    // 供应商
    if (item.attrcode == 'hbbm') {
        item.queryCondition = () => {
            let paytarget = (moudleid == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['paytarget'].value) : (props.cardTable.getClickRowIndex(moudleid).record.values['paytarget'].value))
            if(paytarget!="1"){
                //收款对象为供应商
                return {pk_org: ''};
            }
            let data = props.form.getAllFormValue(presetVar.head.head1).rows[0].values['fydwbm'].value            
            return {pk_org:data, GridRefActionExt:SupplierTreeGridRef,DataPowerOperationCode:'fi',/**使用权组**/isDataPowerEnable:'Y'}; // 根据pk_org过滤
        };
    }

    //客户
    if (item.attrcode == 'customer') {
        item.queryCondition = () => {
            let paytarget = (moudleid == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['paytarget'].value) : (props.cardTable.getClickRowIndex(moudleid).record.values['paytarget'].value))
            if(paytarget!="2"){
                //收款对象为客户
                return {pk_org: ''};
            }
            let data = props.form.getAllFormValue(presetVar.head.head1).rows[0].values['fydwbm'].value
            return {pk_org: data, GridRefActionExt:CustomerTreeGridRef,DataPowerOperationCode:'fi',/**使用权组**/isDataPowerEnable:'Y'}; // 根据pk_org过滤
        };
    }

    //收款人
    if(item.attrcode=='receiver'){
        
        item.queryCondition = ()=>{
            let paytarget = (moudleid == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['paytarget'].value) : (props.cardTable.getClickRowIndex(moudleid).record.values['paytarget'].value))
            if(paytarget!="0"){
                return {tradetype:window.presetVar.tradetype,pk_dept: '',GridRefActionExt:PsndocTreeFilterPath};
            }
            let dwbm = (moudleid == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['dwbm'].value) : (props.cardTable.getClickRowIndex(moudleid).record.values['dwbm'].value))
            return {pk_org:dwbm,DataPowerOperationCode:'fi',/**使用权组**/isDataPowerEnable:'Y'}; // 根据assume_org过滤
        }
    }
    //个人银行账户
    if(item.attrcode=='skyhzh'){       
        item.queryCondition = ()=>{
            let receiver = (moudleid == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['receiver'].value) : (props.cardTable.getClickRowIndex(moudleid).record.values['receiver'].value))
            //银行账户参照代码没接受人员信息？先返回空
            return {pk_psndoc:receiver,GridRefActionExt:PsnbankaccTreeGridRef,DataPowerOperationCode:'fi',/**使用权组**/isDataPowerEnable:'Y'};
        }
    }

    //单位银行账户
    if(item.attrcode=='fkyhzh'){        
        item.queryCondition = ()=>{
            let pk_org = (moudleid == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['pk_payorg'].value) : (props.cardTable.getClickRowIndex(moudleid).record.values['pk_payorg'].value))
            let bzbm = (moudleid == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['bzbm'].value) : (props.cardTable.getClickRowIndex(moudleid).record.values['bzbm'].value))            
            //银行账户参照代码没接受人员信息？先返回空
            return {pk_org:pk_org,pk_currtype:bzbm,GridRefActionExt:BankacctreeGridRef,DataPowerOperationCode:'fi',/**使用权组**/isDataPowerEnable:'Y'};
        }
    }

    //资金计划项目
    if(item.attrcode=='cashproj'){        
        item.queryCondition = ()=>{
            let pk_payorg = (moudleid == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['pk_payorg'].value) : (props.cardTable.getClickRowIndex(moudleid).record.values['pk_payorg'].value))
            return {pk_org:pk_payorg,isDisableDataShow:'N',DataPowerOperationCode:'fi',/**使用权组**/isDataPowerEnable:'Y'}; // 根据pk_org过滤
        }
    }

    //现金账户
    if(item.attrcode=='pk_cashaccount'){        
        item.queryCondition = ()=>{
            let pk_payorg = (moudleid == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['pk_payorg'].value) : (props.cardTable.getClickRowIndex(moudleid).record.values['pk_payorg'].value))            
            let pk_group = props.form.getAllFormValue(presetVar.head.head1).rows[0].values['pk_group'].value;             
            return {pk_org: pk_payorg,pk_group:pk_group,TreeRefActionExt:'',DataPowerOperationCode:'fi',/**使用权组**/isDataPowerEnable:'Y'}; // 根据pk_org过滤
        }
    }

    //现金流量项目
    if(item.attrcode=='cashitem'){
        
        item.queryCondition = ()=>{
            let pk_payorg = (moudleid == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['pk_payorg'].value) : (props.cardTable.getClickRowIndex(moudleid).record.values['pk_payorg'].value))
            let pk_group = props.form.getAllFormValue(presetVar.head.head1).rows[0].values['pk_group'].value;                         
            return {pk_org: pk_payorg,pk_group:pk_group,TreeRefActionExt:'',DataPowerOperationCode:'fi',/**使用权组**/isDataPowerEnable:'Y'}; // 根据pk_org过滤
        }
    }

    //散户
    if(item.attrcode=='freecust'){
        
        item.queryCondition = ()=>{
            let paytarget = (moudleid == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['paytarget'].value) : (props.cardTable.getClickRowIndex(moudleid).record.values['paytarget'].value))
            let customSupplier = '';
            if(paytarget=="0"){
                return {customSupplier:''};
            }else if(paytarget=="1"){
                customSupplier = dataValues['hbbm']==null?"":dataValues['hbbm'].value;
                customSupplier = (moudleid == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['hbbm'].value) : (props.cardTable.getClickRowIndex(moudleid).record.values['hbbm'].value))
                
            }else if(paytarget=="2") {
                customSupplier = (moudleid == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['customer'].value) : (props.cardTable.getClickRowIndex(moudleid).record.values['customer'].value))
            }
           return {'pk_customsupplier':customSupplier,'customSupplier': customSupplier,DataPowerOperationCode:'fi',/**使用权组**/isDataPowerEnable:'Y'}; // 根据pk_org过滤
        }
    }

    //项目
    if(item.attrcode=='jobid'){        
        item.queryCondition = ()=>{
            let pk_org = (moudleid == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['pk_payorg'].value) : (props.cardTable.getClickRowIndex(moudleid).record.values['pk_payorg'].value))            
            //银行账户参照代码没接受人员信息？先返回空
            return {org_id:pk_org,GridRefActionExt:ProjecTreetGridRef,DataPowerOperationCode:'fi',/**使用权组**/isDataPowerEnable:'Y'};
        }
    }

    //币种
    if (item.attrcode == 'bzbm') {
        item.queryCondition = () => {
            return {pk_currtype:filter[item.attrcode],TreeRefActionExt:CurrtypeRefSqlFilterPath,DataPowerOperationCode:'fi',/**使用权组**/isDataPowerEnable:'Y'};
        };
    }

    //人员
    if (item.attrcode == 'jkbxr') {
        item.queryCondition = () => {
            let data = (moudleid == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['dwbm'].value) : (props.cardTable.getClickRowIndex(moudleid).record.values['dwbm'].value))
            if(moudleid == presetVar.head.head1){
                return {tradetype:window.presetVar.tradetype,pk_org:data,org_id:data,GridRefActionExt:PsndocTreeFilterPath,DataPowerOperationCode:'fi',/**使用权组**/isDataPowerEnable:'Y'}; // 根据assume_org过滤                
            }else{
                //新增业务报销单表体借款报销单人能参选到报销人单位下所有部门和人员
                return {pk_org:data,DataPowerOperationCode:'fi',/**使用权组**/isDataPowerEnable:'Y'}; // 根据assume_org过滤                
            }
        };
    }

    //报销事由
    if (item.attrcode == 'zy') {
        item.queryCondition = () => {
            let data = (moudleid == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['dwbm'].value) : (props.cardTable.getClickRowIndex(moudleid).record.values['dwbm'].value))
            return {pk_org: data,TreeRefActionExt:PsndocTreeFilterPath,DataPowerOperationCode:'fi',/**使用权组**/isDataPowerEnable:'Y'}; // 根据assume_org过滤
        };
    }

    //客商银行账号
    if(item.attrcode=='custaccount'){        
        item.queryCondition = ()=>{
            let accclass = "";
            let pk_cust = "";
            let paytarget = (moudleid == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['paytarget'].value) : (props.cardTable.getClickRowIndex(moudleid).record.values['paytarget'].value))            
            if(paytarget=="1"){
                //收款对象为供应商
                accclass = "3";
                pk_cust = (moudleid == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['hbbm'].value) : (props.cardTable.getClickRowIndex(moudleid).record.values['hbbm'].value))                            
            } else if(paytarget=="2"){
                //收款对象为客户
                accclass = "2";
                pk_cust = (moudleid == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['customer'].value) : (props.cardTable.getClickRowIndex(moudleid).record.values['customer'].value))                            
            }
            let bzbm = (moudleid == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['bzbm'].value) : (props.cardTable.getClickRowIndex(moudleid).record.values['bzbm'].value))                       
            return { pk_cust: pk_cust,accclass:accclass,pk_currtype:bzbm,TreeRefActionExt:'',DataPowerOperationCode:'fi',/**使用权组**/isDataPowerEnable:'Y'};
        }
    }

    //核算要素
    if(item.attrcode=='pk_checkele'){
        item.queryCondition = ()=>{
            let pk_org = (moudleid == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['pk_org'].value) : (props.cardTable.getClickRowIndex(moudleid).record.values['pk_org'].value))                        
            return {pk_factorasoa:filter[item.attrcode], pk_org: pk_org,TreeRefActionExt:'',DataPowerOperationCode:'fi',/**使用权组**/isDataPowerEnable:'Y'};
        }
    }

    //成本中心
    if(item.attrcode=='pk_resacostcenter'){
        item.queryCondition = ()=>{
            let pk_org = (moudleid == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['pk_org'].value) : (props.cardTable.getClickRowIndex(moudleid).record.values['pk_org'].value))            
            return {pk_ccgroup:filter[item.attrcode], pk_org: pk_org,TreeRefActionExt:'',DataPowerOperationCode:'fi',/**使用权组**/isDataPowerEnable:'Y'};
        }
    }

    //合同号
    if (item.attrcode == 'fctno') {
        item.queryCondition = () => {
            let pk_org = props.form.getAllFormValue(presetVar.head.head1).rows[0].values['pk_org'].value          
            let bzbm =  props.cardTable.getClickRowIndex(moudleid).record.values['bzbm'].value;            
            let hbbm = props.cardTable.getClickRowIndex(moudleid).record.values['hbbm'].value;
            return {GridRefActionExt:PublicDefaultGridRef,pk_org:pk_org,cvendorid:hbbm,DataPowerOperationCode:'fi',/**使用权组**/isDataPowerEnable:'Y'};
        };
    }

    let bodys = Object.values(window.presetVar.body);
    let bodyarr = [];
    bodys.map((one)=>{
            bodyarr[bodyarr.length] = one;
    })
    //处理交叉校验规则
    if (item.itemtype == 'refer') {
        let oldQueryCondition = item.queryCondition;
        let crossrule_itemkey = item.attrcode;
        item.queryCondition = (obj) => {
            let crossrule_datasout = props.createExtCardData(props.meta.getMeta().code, "head", bodyarr);
            let crossrule_tradetype = props.form.getFormItemsValue(presetVar.head.head1,'djlxbm').value;
            let crossrule_org = props.form.getFormItemsValue(presetVar.head.head1,'pk_org').value;
            let crossrule_datatypes = 'extbillcard';
          
            let crossrule_datas = crossrule_datasout;
            if(moudleid!='head' && window.presetVar.bodyChangeValuesAll!=null) {
                crossrule_datas.bodys[moudleid].rows = [window.presetVar.bodyChangeValuesAll];
            }
            let conditionFlag = true;
            if(oldQueryCondition==null || oldQueryCondition==undefined || oldQueryCondition=='undefined')
            conditionFlag = false;
      
            let oldData='';
            if(conditionFlag)
            oldData = oldQueryCondition();
            if(oldData==null || oldData==undefined || oldData=='undefined')
            oldData='';

            let config = { 
                crossrule_datas:JSON.stringify(crossrule_datas),
                crossrule_tradetype:crossrule_tradetype,
                crossrule_org:crossrule_org, 
                crossrule_datatypes:crossrule_datatypes,
                crossrule_area:moudleid,
                crossrule_itemkey:crossrule_itemkey,
                ...oldData
                };

            //参照类型的自定义项字段增加组织过滤
            if(!oldData.pk_org){
                config.pk_org = crossrule_org;
            }
                
                if(obj==undefined || obj==null || obj.refType==undefined || obj.refType==null ){
                    if(config.TreeRefActionExt==null || config.TreeRefActionExt==undefined || config.TreeRefActionExt.length==0)
                    config.TreeRefActionExt = PublicDefaultGridRef; 
                    return config;
                }
                
                if (obj.refType == 'grid') {
                    if(config.GridRefActionExt==null || config.GridRefActionExt==undefined || config.GridRefActionExt.length==0)
                    config.GridRefActionExt = PublicDefaultGridRef;
                    } else if (obj.refType == 'tree') {
                    if(config.TreeRefActionExt==null || config.TreeRefActionExt==undefined || config.TreeRefActionExt.length==0)
                    config.TreeRefActionExt = PublicDefaultGridRef;
                    } else if (obj.refType == 'gridTree') {
                    if(config.GridRefActionExt==null || config.GridRefActionExt==undefined || config.GridRefActionExt.length==0)
                    config.GridRefActionExt = PublicDefaultGridRef;
                 }                
                return config;
        }
    }
}
export default  requestApiOverwrite;

function getRequest1(url) {
    if (!url)
        url = document.location.search;
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substring(url.indexOf("?") + 1);
        var strs = str.split("&");
        for (var i = 0; i < strs.length; i++) {
            theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
        }
    }
    return theRequest;
}