import React, {Component} from 'react';
import {createPage, base} from 'nc-lightapp-front';
import {buttonClick, initTemplate, afterEvent} from './events';
import {Nav} from 'ssccommon/components/global-nav'
import FinanceOrg from '../../../../uapbd/refer/org/FinanceOrgTreeRef';
import AccperiodDefaultTreeRef from '../../../../uapbd/refer/pubinfo/AccPeriodDefaultTreeGridRef';
import {
    ProfileStyle,
    ProfileHead,
    ProfileBody,
    BodyRight,
    HeadCenterCustom,
    ButtonGroup
} from 'ssccommon/components/profile';
import {EditTable} from 'ssccommon/components/table';

import pubMessage from 'ssccommon/utils/pubMessage'

import './index.less';

const {NCRadio,NCButton} = base;

class ClientageGrid extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            selectedValue: 'wjz',  // 结账状态
            FinanceOrgValue:'',      // 财务组织
            FinanceOrgPk:'',      // 财务组织
            isEdit: true
        }       
        this.pubMessage = new pubMessage(props);
        initTemplate.call(this, props);
    }
    componentDidMount() {
    }

    render() {
        let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId
        return (
            <div className="nc-single-table" id='expenseaccount-checkout'>

                <ProfileStyle
                    layout="singleTable"
                    {...this.props}
                >
                    {/*页面头*/}
                    {/*"201103FYJZ-0001": "费用结账",*/}
                    <ProfileHead title={multiLang && multiLang.get('201103FYJZ-0001')}>
                        <HeadCenterCustom>

                            <div style={{position: 'relative'}}>
                                <span class="ssc-must">*</span>

                                {FinanceOrg({
                                    onChange:buttonClick.pkorgChange.bind(this),
                                    value:this.state.FinanceOrgValue,
                                    isMultiSelectedEnabled:true
                                })}
                            </div>
                           

                            <NCRadio.NCRadioGroup
                                name="fruit" selectedValue={this.state.selectedValue}
                                 onChange={buttonClick.radioChange.bind(this)}
                                >

                                <NCRadio value="wjz" >{multiLang && multiLang.get('201103FYJZ-0002')}</NCRadio>

                                <NCRadio value="yjz" >{multiLang && multiLang.get('201103FYJZ-0003')}</NCRadio>

                            </NCRadio.NCRadioGroup>

                            <NCButton className='checkout-search--btn' style={{marginLeft:'-20px'}}  colors="primary" onClick={ buttonClick.onSearch.bind(this) }>{multiLang && multiLang.get('201103DTFY-0005')}</NCButton>
                        </HeadCenterCustom>
                        <ButtonGroup
                            area={window.presetVar.btnArea}
                            buttonEvent={
                                {
                                    Closing: {click:buttonClick.Closing.bind(this)},
                                    CancelClosing:{click:buttonClick.CancelClosing.bind(this)}

                                }
                            }
                        />
                    </ProfileHead>
                    {/*页面体*/}
                    <ProfileBody>
                        <BodyRight> 
                            <EditTable
                                showCheck={this.state.isEdit}
                                areaId={window.presetVar.listArea}
                                onAfterEvent={afterEvent.bind(this)}
								// 选中列表数据后,取消置灰"结账"和"取消结账"两个按钮
                                selectedChange={(props, moduleId,newVal,oldVal)=>{
                                    if(newVal > 0){
                                         props.button.setButtonDisabled(['Closing','CancelClosing'],false);
                                    }else if(newVal == 0){
                                         props.button.setButtonDisabled(['Closing','CancelClosing'],true);
                                    }
                                }}
                            />
                        </BodyRight>
                    </ProfileBody>
                </ProfileStyle>
            </div>
        )
    }
}

let ClientageDom = createPage({
    mutiLangCode: '2011'
})(ClientageGrid);
export default ClientageDom;