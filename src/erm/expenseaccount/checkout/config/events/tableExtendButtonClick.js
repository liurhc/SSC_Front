/**
 * 表格扩展按钮click事件
 */
import buttonClick from './buttonClick.js';
import { ajax,base,toast } from 'nc-lightapp-front';
const {NCMessage} = base;
function tableExtendButtonClick(that) {
    let multiLang = that.props.MutiInit.getIntl(2011);
    return {
        ['Closing'] : (record, index) => {
            let vdata = {};
            vdata[window.presetVar.listArea] = {};
            vdata[window.presetVar.listArea].areaType = 'table';
            vdata[window.presetVar.listArea].areacode = null;
            vdata[window.presetVar.listArea].rows = [];
            vdata[window.presetVar.listArea].rows.push(record);
            
            ajax({
                url: '/nccloud/erm/expenseaccount/CloseAccClose.do',
                data: vdata,
                success: (data) => {
                    if (data.data.ret){
                        let msg_content = multiLang && multiLang.get('201103FYJZ-0006');
                        NCMessage.create({content: msg_content, color: 'success', position: 'bottom'});
                        buttonClick.onSearch.call(that);
                    }else {
                        let msg_content = multiLang && multiLang.get('201103FYJZ-0007');
                        toast({ content: msg_content+data.data.retMesg, color: 'warning' });
                        //NCMessage.create({content: msg_content+data.data.retMesg, color: 'error', position: 'bottom'});
                        //onSearch.call(that);
                    }

                }
            })
        },
        ['CancelClosing'] : (record, index) => {
            let vdata = {};
            vdata[window.presetVar.listArea] = {};
            vdata[window.presetVar.listArea].areaType = 'table';
            vdata[window.presetVar.listArea].areacode = null;
            vdata[window.presetVar.listArea].rows = [];
            vdata[window.presetVar.listArea].rows.push(record);
            ajax({
                url: '/nccloud/erm/expenseaccount/CloseAccUnClose.do',
                data: vdata,
                success: (data) => {
                    if (data.data.ret){
                        let msg_content = multiLang && multiLang.get('201103FYJZ-0006');
                        NCMessage.create({content: msg_content, color: 'success', position: 'bottom'});
                        buttonClick.onSearch.call(that);
                    }else {
                        let msg_content = multiLang && multiLang.get('201103FYJZ-0007');
                        toast({ content: msg_content+data.data.retMesg, color: 'warning' });
                        //NCMessage.create({content: msg_content+data.data.retMesg, color: 'error', position: 'bottom'});
                    }

                }
            })
        }
    }
}

export default tableExtendButtonClick;