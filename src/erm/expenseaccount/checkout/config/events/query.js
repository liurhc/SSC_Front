import requestApi from '../requestApi'
import dataModelChange from './dataModelChange'

//共享中心改变事件
let onSSCCenterChange =(that, e)=>{
    that.setState({sscCenterPk:e.refpk, SSCCenter: e});
    if (e.refpk!=undefined){
        let req = {'pk':e.refpk,'showDisabled':that.state.showDisabled};
        queryTableData(that, req);
    }else{
        that.props.editTable.setTableData(window.presetVar.listArea, { rows: [] });
    }

}
/**
 * 显示停用复选框勾选事件
 * @param {*} that 
 * @param {*} e 
 */
let showDisabledChange = (that, e) =>{
    // 更改状态
    that.setState({showDisabled:e});
    let req = {'pk':that.state.sscCenterPk,'showDisabled':e};
    queryTableData(that, req);
}

// 查询表数据
let queryTableData = (that, req) =>{
    requestApi.queryTableData({
        data: req,
        success: (res) => {
            if(res){
                let resNew = dataModelChange.voToJson(res);
                that.props.editTable.setTableData(window.presetVar.listArea, resNew[window.presetVar.listArea]);
            }else{
                that.props.editTable.setTableData(window.presetVar.listArea, { rows: [] });
            }
        },
        error:(res) =>{
            console.log(res.message);
        }
    })
}
export default {onSSCCenterChange, showDisabledChange, queryTableData}