let jsonToVo = (data) =>{
    data[window.presetVar.listArea].rows.map((one)=>{
        if(one.values.enablestate.value == true){
            one.values.enablestate.value=2;
        }else{
            one.values.enablestate.value=1;
        }
    })
    return data;
}

let voToJson = (data) =>{
    data[window.presetVar.listArea].rows.map((one)=>{
        if(one.values.isendacc.value == false){
            one.values.isendacc={value:window.presetVar.multiLangJson['201103FYJZ-0009']/*'未结账'*/};
        }else{
            one.values.isendacc={value:window.presetVar.multiLangJson['201103FYJZ-0010']/*'已结账'*/};
        }
    })
    return data;
}

export default {jsonToVo, voToJson}