export default {
    button:[
        {
            "id": "0001A41000000006J5B1",
            "type": "button_main",
            "key": "Closing",
            "title": "结账",
            "area": 'btnArea',
            "children": []
        },
        {
            "id": "0001A41000000006J5B1",
            "type": "button_main",
            "key": "CancelClosing",
            "title": "取消结账",
            "area": 'btnArea',
            "children": []
        }
    ],
    addModel:{
        moduletype: 'form',
        items: [
            {
                "itemtype": "label",
                "label": "委托关系主键",
                "maxlength": "20",
                "disabled": true,
                "attrcode": "pk_ssclientage"
            },
            {
                "itemtype": "label",
                "label": "共享服务中心主键",
                "maxlength": "20",
                "disabled": true,
                "attrcode": "pk_sscunit"
            },
            {
                "itemtype": "input",
                "visible": true,
                "label": "组织编码",
                "col":6,
                "maxlength": "20",
                "disabled": true,
                "attrcode": "pk_org.code"
            },
            {
                "itemtype": "refer",
                "visible": true,
                "label": "组织名称",
                "col":6,
                "maxlength": "20",
                "refcode": "uapbd/refer/org/BusinessUnitTreeRef/index.js",
                "required": true,
                "attrcode": "pk_org"
            },
            {
                "itemtype": "input",
                "visible": true,
                "label": "会计期间",
                "col":6,
                "maxlength": "20",
                "disabled": true,
                "attrcode": "pk_org.pk_group.name"
            },
            {
                "itemtype": "checkbox_switch",
                "visible": true,
                "label": "开始日期",
                "maxlength": "1",
                "attrcode": "busiunittype1",
                "initialvalue": {//控件默认值
                    "value": false,
                    "display": ""
                }
            },
            {
                "itemtype": "checkbox_switch",
                "visible": true,
                "label": "结束日期",
                "maxlength": "1",
                "attrcode": "busiunittype3",
                "initialvalue": {//控件默认值
                    "value": false,
                    "display": ""
                },
            },
            {
                "itemtype": "checkbox_switch",
                "visible": true,
                "label": "结账状态",
                "maxlength": "1",
                "attrcode": "busiunittype2",
                "initialvalue": {//控件默认值
                    "value": false,
                    "display": ""
                }
            },
            {
                "itemtype": "checkbox_switch",
                "visible": true,
                "label": "结账人",
                "maxlength": "1",
                "attrcode": "busiunittype4",
                "initialvalue": {//控件默认值
                    "value": false,
                    "display": ""
                }
            },
            {
                "itemtype": "checkbox_switch",
                "visible": true,
                "label": "结账时间",
                "maxlength": "1",
                "attrcode": "busiunittype5",
                "initialvalue": {//控件默认值
                    "value": false,
                    "display": ""
                }
            },
            {
                "itemtype": "checkbox_switch",
                "visible": true,
                "label": "取消结账人",
                "maxlength": "1",
                "attrcode": "busiunittype6",
                "initialvalue": {//控件默认值
                    "value": false,
                    "display": ""
                }
            },
            {
                "itemtype": "checkbox_switch",
                "label": "取消结账时间",
                "visible": true,
                "maxlength": "1",
                "attrcode": "busiunittype10",
                "initialvalue": {//控件默认值
                    "value": false,
                    "display": ""
                }
            },
            {
                "itemtype": "checkbox_switch",
                "visible": false,
                "label": "操作",
                "maxlength": "1",
                "attrcode": "busiunittype7",
                "initialvalue": {//控件默认值
                    "value": false,
                    "display": ""
                }
            },
            {
                "itemtype": "label",
                "label": "时间戳",
                "maxlength": "19",
                "disabled": true,
                "attrcode": "ts"
            }
        ],
        status: 'edit',
    }
}