import { createPage, ajax, base, getMultiLang } from 'nc-lightapp-front';
import 'ssccommon/components/globalPresetVar';
import {setTableExtendCol} from 'ssccommon/components/profile'
import fixedTemplet from './fixedTemplet'
import tableExtendButtonClick from "./tableExtendButtonClick";


export default function (props) {
    let that = this;

    //变量设置规范
    window.presetVar = {
        ...window.presetVar,
        pageId: '201103FYJZ_list',
        addFormCode: 'addModel',
        listArea: '201103FYJZ_list',
        btnArea: 'btnArea',
        listBtnArea: 'listbtnarea'
    };

    props.createUIDom(
        {
        pagecode: window.presetVar.pageId,//页面id
        appid: ' '//注册按钮的id
        },
        function (data) {
            getMultiLang({moduleId: 2011, domainName: 'erm',currentLocale: 'simpchn',callback: (json) => {
                window.presetVar.multiLangJson = json;
            let meta = data.template;
            // // 设值新增弹出窗体模板
            // meta[window.presetVar.addFormCode] = fixedTemplet[window.presetVar.addFormCode];
            //设置区域模板
            props.meta.setMeta(data.template);
            //设置按钮模板
            props.button.setButtons(data.button);
			// 页面加载的时候置灰"结账"和"取消结账"两个按钮
            props.button.setButtonDisabled(['Closing','CancelClosing'],true);
            setTableExtendCol(props, props.meta.getMeta(), [{
                areaId: window.presetVar.listArea,
                btnAreaId: window.presetVar.btnArea,
                buttonVisible: (record, index) => {
                    let multiLang = props.MutiInit.getIntl(2011);
                    if(record.values.isendacc.value == (multiLang && multiLang.get('201103FYJZ-0009'))){
                        return ['Closing']
                    }else{
                        return ['CancelClosing']
                    }

                },
                onButtonClick: tableExtendButtonClick.bind(this,that)
            }]);
        }})
        }
    )
}