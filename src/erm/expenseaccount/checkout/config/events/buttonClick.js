import { ajax, base,toast } from 'nc-lightapp-front';
import {setTableExtendCol} from 'ssccommon/components/profile'
import dataModelChange from './dataModelChange'
const {NCMessage} = base;

/**
 * 状态选择
 * @param value
 */
let radioChange = function (value){
    this.setState({selectedValue: value});
}

/**
 * 会计期间变化
 * @param value
 */
/*let accperiodChange = function (value){
    this.setState({Accperiodrefpk: value.refpk});
    this.setState({AccperiodValue: value});

}*/

/**
 * 财务组织变化
 * @param value
 */
let pkorgChange = function (value){
    this.setState({FinanceOrgValue: value});
    let orgs =[];
    for(var i=0;i<value.length;i++){

        orgs.push(value[i].refpk);
    }
    this.setState({FinanceOrgPk: orgs});

}



/**
 * 结账
 * @param {*} that 
 */
let Closing = function() {
    let vdata = {};
    vdata[window.presetVar.listArea] = {};
    vdata[window.presetVar.listArea].areaType = 'table';
    vdata[window.presetVar.listArea].areacode = null;
    vdata[window.presetVar.listArea].rows = [];
    let that = this;
    let selectData = this.props.editTable.getCheckedRows(window.presetVar.listArea);
    let multiLang = this.props.MutiInit.getIntl(2011);
    selectData.forEach((sdata) => {
        vdata[window.presetVar.listArea].rows.push(sdata.data);
    })
    ajax({
        url: '/nccloud/erm/expenseaccount/CloseAccClose.do',
        data: vdata,
        success: (data) => {
            if (data.data.ret){
                toast({content: multiLang && multiLang.get('201103FYJZ-0006'), color: 'success'});
                // onSearch.call(that);
            }else {
                let msg_content = multiLang && multiLang.get('201103FYJZ-0007');
                toast({ content: msg_content+data.data.retMesg, color: 'warning' });
            }

        }
    })
}

/**
 * 取消结账
 * @param {*} that 
 */
let CancelClosing = function(){
    let vdata = {};
    vdata[window.presetVar.listArea] = {};
    vdata[window.presetVar.listArea].areaType = 'table';
    vdata[window.presetVar.listArea].areacode = null;
    vdata[window.presetVar.listArea].rows = [];

    let selectData = this.props.editTable.getCheckedRows(window.presetVar.listArea);
    let multiLang = this.props.MutiInit.getIntl(2011);
    selectData.forEach((sdata) => {
        vdata[window.presetVar.listArea].rows.push(sdata.data);
    })
    let that = this;
    ajax({
        url: '/nccloud/erm/expenseaccount/CloseAccUnClose.do',
        data: vdata,
        success: (data) => {
            if (data.data.ret){
                let msg_content = multiLang && multiLang.get('201103FYJZ-0011');
                toast({content: msg_content, color: 'success'});
                onSearch.call(that);
            }else {
                let msg_content = multiLang && multiLang.get('201103FYJZ-0012');
                toast({ content: msg_content+data.data.retMesg, color: 'warning' });
            }

        }
    })

}
/**
 * 查询
 * @param {*} that 
 */
let onSearch = function() {
    let vdata  = {};
    vdata.selectdItemValue = this.state.selectedValue;
    vdata.pkorg = this.state.FinanceOrgPk;
    let multiLang = this.props.MutiInit.getIntl(2011);
    if(!vdata.pkorg){
        let msg_content = multiLang && multiLang.get('201103FYJZ-0008');
        toast({ content: msg_content, color: 'warning' });
        return;
    }
    // vdata.Accperiodrefpk = this.state.Accperiodrefpk;
    ajax({
        url: '/nccloud/erm/expenseaccount/CloseAccDataQuery.do',
        data: vdata,
        success: (data) => {
            let totalCount = 0;
            if (data.data==undefined){
                this.props.editTable.setTableData(window.presetVar.listArea, {rows:[]});
            }else {
                let newData = dataModelChange.voToJson(data.data);
                this.props.editTable.setTableData(window.presetVar.listArea, newData[window.presetVar.listArea]);
                totalCount = newData[window.presetVar.listArea].rows.length;
            }
            this.pubMessage.querySuccess(data ? totalCount : 0);
        }
    })
}

export default {pkorgChange,radioChange,Closing, CancelClosing,onSearch}
