import { createPage, ajax, base,toast } from 'nc-lightapp-front';
import requestApi from '../requestApi'
import query from './query';
let {NCMessage} = base;
export default function afterEvent(props, moduleId, key,changedrows, value,index,data) {
    let multiLang = this.props.MutiInit.getIntl(2011);
    switch(moduleId){
        case window.presetVar.listArea:
            switch (key) {
                case 'enablestate':
                    let tableData = props.editTable.getAllData(moduleId);
                    // 此处转换启用前端switch(true,false)和后端枚举不一致,暂时这么写
                    tableData.rows = tableData.rows.filter((item)=>{
                        if (item.rowid===data.rowid){
                            if(value){
                                item.values.enablestate.value = 1;
                            }else {
                                item.values.enablestate.value = 2;
                            }
                            return [item]
                        }
                    })
                    // 此处转换启用前端switch(true,false)和后端枚举不一致,暂时这么写
                    let req = {'pk':this.state.sscCenterPk,'showDisabled':this.state.showDisabled};
                    
                    if (changedrows){
                        // 启用
                        requestApi.enable({
                            data: {
                                head: tableData
                            },
                            success: (res) => {
                                NCMessage.create({content: multiLang && multiLang.get('201103FYJZ-0004'), color: 'success', position: 'bottomRight'});
                                query.queryTableData(this, req);
                            }
                        })
                    }else {
                        // 停用
                        requestApi.disabled({
                            data: {
                                head: tableData
                            },
                            success: (res) => {
                                NCMessage.create({content: multiLang && multiLang.get('201103FYJZ-0005'), color: 'success', position: 'bottomRight'});
                                query.queryTableData(this, req);
                            }
                        })
                    }
                    break;
                case 'pk_org':
                    ;
                    let rowid  = data.rowid;
                    // 平台参照有问题后期删除undefined校验
                    if(changedrows.refcode){
                        props.editTable.setValByKeyAndRowId(moduleId,rowid,'pk_org.code',{value:changedrows.refcode,display:changedrows.refcode});
                        props.editTable.setValByKeyAndRowId(moduleId,rowid,'pk_org.pk_group.name',{value:changedrows.values.gname.value,display:changedrows.values.gname.value});
                    }
                    // 平台参照有问题后期删除undefined校验
                    // props.editTable.setValByKeyAndRowId(moduleId,rowid,'pk_sscorg',{value:this.state.sscCenterPk});
                    break;
                default:
                    break;
            }
            break;
        case window.presetVar.addFormCode:
            switch (key){
                case 'pk_org':
                    if(index.refcode!==undefined){
                        props.form.setFormItemsValue(window.presetVar.addFormCode,{'pk_org.code':{value:'',display:index.refcode}});
                        props.form.setFormItemsValue(window.presetVar.addFormCode,{'pk_org.pk_group.name':{value:'',display:index.values.gname.value}});
                    }
                    break;
                default:
                    break;
            }
        default:
            break;
    }

}