import React, {Component} from 'react';
import {connect} from 'react-redux';
import {createPage,cacheTools} from 'nc-lightapp-front';
import {buttonClick, initTemplate, afterEvent, onRowDoubleClick, refreshButtonClick} from './events';
import requestApi from './requestApi';

import pubMessage from 'ssccommon/utils/pubMessage';

import {
    ProfileStyle,
    ProfileHead,
    ProfileBody,
    HeadCenterCustom,
    ButtonGroup
} from 'ssccommon/components/profile';
import {EditTable} from 'ssccommon/components/table';

import BusinessUnitTreeRef from 'uapbd/refer/org/BusinessUnitTreeRef';

import './index.less';

let body = window.presetVar.body;

class LoanControl_org extends Component {

    constructor(props) {
        super();
        this.state = {
            businessUnitTreeRefValue: {},
            status: 'browse'
        }
        this.pubMessage = new pubMessage(props);
        initTemplate.call(this, props);
    }

    componentDidMount() {
        if('back' == this.props.getUrlParam('status')){
            this.setState({
                businessUnitTreeRefValue: {refpk: cacheTools.get('ref').refpk, refname: cacheTools.get('ref').refname}
            })
        }else {
            businessUnitTreeRefValue: {}
        }
    }

    render() {
        let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId
        buttonClick.addBtnEventConfig.click = buttonClick.addBtnEventConfig.click.bind(this);
        let props = this.props;

        return (
            /*档案风格布局*/
            <ProfileStyle
                layout="singleTable"
                {...this.props}
            >
                {/*页面头*/}
                <ProfileHead
                    // "201101JKKZ-0001": "借款控制规则-组织"
                    title={multiLang && multiLang.get('201101JKKZ-0001')}
                    refreshButtonEvent={refreshButtonClick.bind(this)}
                    status = { this.state.status }
                >
                    <HeadCenterCustom>
                        <span class="ssc-ref-must">*</span>
                            <BusinessUnitTreeRef
                            value={this.state.businessUnitTreeRefValue}
                            onChange={(data) => {
                                this.setState({
                                    businessUnitTreeRefValue : data
                                }),

                                    requestApi.getTableData({
                                        data: { pk_org : data.refpk,
                                            isGroup : false},
                                        success: (data) => {
                                            this.props.editTable.setTableData(body.body1, data.data[body.body1]);
                                        }
                                    })

                            }}
                            queryCondition={() => {
                                return {
                                    TreeRefActionExt:'nccloud.web.action.erm.ref.sqlbuilder.BusinessUnitTreeRefBuilder',
                                    appcode : props.getSearchParam('c')
                                }
                            }}
                        />
                    </HeadCenterCustom>

                    <ButtonGroup
                        area="page_header"
                        buttonLimit="3"
                        buttonEvent={
                            {
                                Add: buttonClick.addBtnEventConfig
                            }

                        }
                    />
                </ProfileHead>

                {/*页面体*/}
                <ProfileBody>
                    <EditTable
                        areaId="LoanControl"
                        showCheck= {false}
                        onAfterEvent={afterEvent.bind(this)}
                        onRowDoubleClick={onRowDoubleClick.bind(this)}
                    >
                    </EditTable>

                </ProfileBody>
            </ProfileStyle>
        )
    }
}

LoanControl_org = createPage({
    mutiLangCode: '2011'
})(LoanControl_org);

export default LoanControl_org;
