import {ajax, cacheTools, toast} from 'nc-lightapp-front';
import requestApi from '../requestApi';


//增加
let addBtnEventConfig = {
    click(props){
        if((this.state.businessUnitTreeRefValue || {}).refpk == null){
            let multiLang = this.props.MutiInit.getIntl(2011);
            // "201102BCLF-0045": "请选择组织!",
            toast({
                color: 'warning',
                content: (multiLang && multiLang.get('201102BCLF-0045')) || '请选择组织!'
            });
            return;
        }
        let ref = {
            refname: this.state.businessUnitTreeRefValue.refname,
            refpk: this.state.businessUnitTreeRefValue.refpk
        }
        cacheTools.set('ref',ref);
        props.linkTo('/erm/expenseaccount/loancontrol_org/card/index.html', {
            status: 'add',
            refname: this.state.businessUnitTreeRefValue.refname,
            refpk: this.state.businessUnitTreeRefValue.refpk,
            pagecode:'201101JKKZ_CARD'
        });
    }
}


export default { addBtnEventConfig}


