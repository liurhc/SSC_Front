import {ajax,cacheTools} from 'nc-lightapp-front';
import 'ssccommon/components/globalPresetVar';
import {setTableExtendCol} from 'ssccommon/components/profile'
import requestApi from '../requestApi';


//变量设置规范，如需要
window.presetVar = {
    ...window.presetVar,
    pageId: '',
    head: {
        head1: '',
        head2: ''
    },
    body: {
        body1: 'LoanControl',
        body2: ''
    },
    search: {}
};

let body = window.presetVar.body;


export default function (props) {

    props.createUIDom(
        {
            pagecode: '201101JKKZ_LIST',//页面id
            appid: '0001Z310000000006H15'//注册按钮的id
        },
        (data) => {
            let meta = data.template;
            let buttonTpl = data.button;

            meta[body.body1].items = meta[body.body1].items.map((item, key) => {
                if (item.attrcode == 'paracode') {
                    item.renderStatus= 'browse';
                    item.render = (text, record, index) => {
                        return (
                            <span
                                className="para-code-wrap"
                                onClick={() =>{
                                    let ref = {
                                        refname: this.state.businessUnitTreeRefValue.refname,
                                        refpk: this.state.businessUnitTreeRefValue.refpk
                                    }
                                    cacheTools.set('ref', ref);
                                    props.linkTo('/erm/expenseaccount/loancontrol_org/card/index.html', {
                                        status: 'browse',
                                        id: record.values.pk_control.value,
                                        pagecode:'201101JKKZ_CARD'
                                    });
                                }}
                            >
                                {
                                    record.values.paracode && record.values.paracode.value
                                }
                                </span>
                        );
                    };
                }
                return item;
            });

            //设置表格的扩展按钮列
            setTableExtendCol(props, meta, [{
                areaId: 'LoanControl',
                btnAreaId: 'list_inner',
                btnKeys: ['Edit', 'Delete'],
                onButtonClick: (btnKey, record, index)=> {
                    let props = this.props;
                    switch (btnKey) {
                        case 'Edit' :
                            let ref = {
                                refname: this.state.businessUnitTreeRefValue.refname,
                                refpk: this.state.businessUnitTreeRefValue.refpk
                            }
                            cacheTools.set('ref', ref);
                            props.linkTo('/erm/expenseaccount/loancontrol_org/card/index.html', {
                                status: 'edit',
                                id: record.values.pk_control.value,
                                pagecode:'201101JKKZ_CARD'
                            });
                            break;
                        case 'Delete' :
                            setTimeout(() => {
                                props.editTable.deleteTableRowsByRowId(body.body1, record.rowid);
                                requestApi.save({
                                    data: {
                                        'data': props.editTable.getAllData(body.body1),
                                        'userjson': '{isGroup:false}'
                                    },
                                    success: (resData) => {
                                        if (resData.data) {
                                            props.editTable.setTableData(body.body1, resData.data[body.body1]);
                                        } else {
                                            props.editTable.setTableData(body.body1, {rows: []});
                                        }
                                    }
                                })
                            }, 0)
                            break;
                    }
                }
            }]);

            //设置区域模板
            props.meta.setMeta(meta);
            //设置按钮模板
            props.button.setButtons(buttonTpl);

            //设置按钮行为为弹窗
            //props.button.setPopContent('Delete', '确认要删除该信息吗？')
            /* 设置操作列上删除按钮的弹窗提示 */
            this.pubMessage.setPopContentRowDelete(['Delete']);

            //设置按钮的初始可见性
            props.button.setButtonsVisible({'Add': true});

            //设置初始数据
            //获取表格的数据
            requestApi.getTableData({
                data: {
                    pk_org: this.state.businessUnitTreeRefValue.refpk,
                    isGroup: false
                },
                success: (data) => {
                    this.props.editTable.setTableData(body.body1, data.data[body.body1]);

                    if ('back' != this.props.getUrlParam('status')) {
                        let userJson = JSON.parse(data.data['userjson']);
                        this.setState({
                            businessUnitTreeRefValue: {refpk: userJson.pk_org, refname: userJson.name_org}
                        })
                    }
                }
            })

        }
    )

}


