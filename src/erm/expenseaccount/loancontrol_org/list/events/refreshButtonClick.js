import requestApi from '../requestApi';
import 'ssccommon/components/globalPresetVar';

export default function () {
    let body = window.presetVar.body;
    requestApi.getTableData({
        data: {
            pk_org: this.state.businessUnitTreeRefValue.refpk,
            isGroup: false
        },
        success: (data) => {
            this.props.editTable.setTableData(body.body1, data.data[body.body1]);

            if ('back' != this.props.getUrlParam('status')) {
                let userJson = JSON.parse(data.data['userjson']);
                this.setState({
                    businessUnitTreeRefValue: {refpk: userJson.pk_org, refname: userJson.name_org}
                })
            }
            this.pubMessage.refreshSuccess();
        }
    })
}
