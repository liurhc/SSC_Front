

export default function onRowDoubleClick(record,index,props) {
    props.linkTo('/erm/expenseaccount/loancontrol_org/card/index.html', {
        status: 'browse',
        id: record.values.pk_control.value,
        pagecode:'201101JKKZ_CARD'
    });
};