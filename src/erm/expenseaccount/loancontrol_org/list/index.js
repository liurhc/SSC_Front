import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {connect, Provider} from 'react-redux';
import store from './store/store';


import LoanControl_org from './LoanControl_org';

ReactDOM.render((<Provider store={store}><LoanControl_org /></Provider>)
    , document.querySelector('#app'));