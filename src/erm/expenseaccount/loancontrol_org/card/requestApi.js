import requestApi from "ssccommon/components/requestApi";

import {ajax } from 'nc-lightapp-front';

let requestApiOverwrite = {
    ...requestApi,
    //单据模板接口
    tpl: (opt) => {
        ajax({
            url: `/nccloud/erm/ersetting/IndAuthorizeTempletAction.do`,
            data: opt.data,
            success: opt.success
        });
    },
    query:(opt) => {
        ajax({
            url: `/nccloud/erm/loancontrol/LoanControlViewAction.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    save: (opt) => {
        opt.data.rows.forEach((row) => {
            delete row.values.ts;
        });
        opt.data = {
            head: opt.data
        }
        ajax({
            url: `/nccloud/erm/ersetting/IndAuthorizeSaveAction.do`,
            data: opt.data,
            success: (data) => {
                if (data.success) {
                    data = data.data;
                    opt.success(data);
                }
            }
        })
    }
}

export default  requestApiOverwrite;