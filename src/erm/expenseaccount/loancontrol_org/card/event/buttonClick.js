import { ajax, toast, promptBox } from 'nc-lightapp-front';
export default function buttonClick(props, id) {
    let multiLang = props.MutiInit.getIntl(2011);
    switch (id) {
        case 'Cancel':
            promptBox({
                color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                title: multiLang && multiLang.get('2011-0002'),                 // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输
                content: multiLang && multiLang.get('201101JKKJ-0002'),            // 提示内容,非必输
                beSureBtnName: multiLang && multiLang.get('2011-0003'),          // 确定按钮名称, 默认为"确定",非必输
                cancelBtnName: multiLang && multiLang.get('2011-0002'),         // 取消按钮名称, 默认为"取消",非必输
                beSureBtnClick: this.handleClick   // 确定按钮点击调用函数,非必输
            })
            break;
        case 'Save':
            let checkResult = props.form.isCheckNow('base');
            if(checkResult){
                let reqData = props.form.getAllFormValue('base');
                ajax({
                    url: '/nccloud/erm/loancontrol/LoanControlSaveAction.do',
                    data: { 'head': reqData, 'userjson': '{isGroup:false}' },
                    success: (res) => {
                        if (res.success) {
                            props.form.setAllFormValue({ 'base': res.data['LoanControl'] });
                            props.form.setFormStatus('base', 'browse');
                            props.button.setButtonsVisible({ 'Edit': true, 'Save': false, 'Cancel': false });
                            this.setState({ isEffect: true });
                            toast({
                                color: 'success',
                                title: multiLang && multiLang.get('201101JKKZ-0003'),
                            })
    
                            let metanew = props.meta.getMeta();
                            metanew['controlstyleform'].items.find(e => e.attrcode === 'sum').visible = false;
                            metanew['controlstyleform'].items.find(e => e.attrcode === 'nums').visible = false;
                            metanew['controlstyleform'].items.find(e => e.attrcode === 'days').visible = false;
                            metanew['controlstyleform'].items.find(e => e.attrcode === 'term').visible = false;
                            if (props.form.getFormItemsValue('base', 'sum').value != true) {
                                metanew['controlstyleform'].items.find(e => e.attrcode === 'sumvalue').visible = false;
                            }
                            if (props.form.getFormItemsValue('base', 'nums').value != true) {
                                metanew['controlstyleform'].items.find(e => e.attrcode === 'numsvalue').visible = false;
                            }
                            if (props.form.getFormItemsValue('base', 'days').value != true) {
                                metanew['controlstyleform'].items.find(e => e.attrcode === 'daysvalue').visible = false;
                            }
                            if (props.form.getFormItemsValue('base', 'term').value != true) {
                                metanew['controlstyleform'].items.find(e => e.attrcode === 'test').visible = false;
                            }
                            props.meta.setMeta(metanew)
                        }
                    }
                });
            }
            break;
        case 'Edit':
            props.form.setFormStatus('base', 'edit');
            props.button.setButtonsVisible({ 'Edit': false, 'Save': true, 'Cancel': true });
            this.setState({ isEffect: false });

            let metanew = props.meta.getMeta();
            metanew['controlstyleform'].items.find(e => e.attrcode === 'nums').visible = true;
            metanew['controlstyleform'].items.find(e => e.attrcode === 'numsvalue').visible = true;
            metanew['controlstyleform'].items.find(e => e.attrcode === 'days').visible = true;
            metanew['controlstyleform'].items.find(e => e.attrcode === 'daysvalue').visible = true;
            metanew['controlstyleform'].items.find(e => e.attrcode === 'sum').visible = true;
            metanew['controlstyleform'].items.find(e => e.attrcode === 'sumvalue').visible = true;
            metanew['controlstyleform'].items.find(e => e.attrcode === 'term').visible = true;
            metanew['controlstyleform'].items.find(e => e.attrcode === 'test').visible = true;
            props.meta.setMeta(metanew)

            break;
    }
}