import {ajax } from 'nc-lightapp-front';

let requestDomain =  '';

let requestApiOverwrite = {
    //单据模板接口
    tpl: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/expenseaccount/VoucherQuerySchemeAction.do`,
            data: opt.data,
            success: opt.success
        });
    },
    query:(opt) => {
        ajax({
            url: `${requestDomain}/nccloud/erm/expenseaccount/InitSearchAreaAction.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    delete: (opt) => {
        ajax({
            url: `${requestDomain}/nccloud/erm/expenseaccount/InitBillsDeleteAction.do`,
            data: opt.data,
            success: (data) => {
                if (data.success) {
                    data = data.data;
                    opt.success(data);
                }
            }
        })
    },
    CloseInit: (opt) => {
        ajax({
            url: `${requestDomain}/nccloud/erm/expenseaccount/InitBillsCloseAction.do`,
            data: opt.data,
            success: (data) => {
                if (data.success) {
                    data = data.data;
                    opt.success(data);
                }
            }
        })
    },
    UnCloseInit: (opt) => {
        ajax({
            url: `${requestDomain}/nccloud/erm/expenseaccount/InitBillsUncloseAction.do`,
            data: opt.data,
            success: (data) => {
                if (data.success) {
                    data = data.data;
                    opt.success(data);
                }
            }
        })
    },
    queryTradeType: (opt) => {
        ajax({
            url: `${requestDomain}/nccloud/erm/expenseaccount/TradeTypeQueryAction.do`,
            data: opt.data,
            success: (data) => {
                if (data.success) {
                    data = data.data;
                    opt.success(data);
                }
            }
        })
    },
    linkQuery: (opt) => {
        ajax({
            url: `${requestDomain}/nccloud/erm/expenseaccount/InitBillsLinkQueryAction.do`,
            data: opt.data,
            success: (data) => {
                if (data.success) {
                    data = data.data;
                    opt.success(data);
                }
            }
        })
    },
    queryApp: (opt) => {
        ajax({
            url: `${requestDomain}/nccloud/erm/pub/AppInfoQueryAction.do`,
            data: opt.data,
            success: (data) => {
                if (data.success) {
                    data = data.data;
                    opt.success(data);
                }
            }
        })
    },

}

export default  requestApiOverwrite;