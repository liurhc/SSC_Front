import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {createPage, ajax, base, high, toast} from 'nc-lightapp-front';
import {buttonClick, initTemplate, afterEvent, searchClick, onRowDoubleClick, pageInfoClick, onRowDoubleClickForLink, refreshButtonClick} from './events';
import './index.less';
import {EditTable} from 'ssccommon/components/table';
import requestApi from './requestApi'
import FinanceOrg from '../../../../uapbd/refer/org/FinanceOrgTreeRef';
import {
    ProfileStyle,
    ProfileHead,
    ProfileBody,
    BodyLeft,
    BodyRight,
    HeadCenterCustom,
    EmptyArea,
    ButtonGroup
} from 'ssccommon/components/profile';
import routes from './router';
import pubMessage from 'ssccommon/utils/pubMessage';

const { NCUploader,ExcelImport } = high;

class InitBills extends Component {
    constructor(props) {
        super(props);
       // initTemplate.call(this, props);
        this.dataSource = 'erm.expenseaccount.initbills.config'
        this.state = {
            showCloseModal: false,
            showUnCloseModal: false,
            showTradeModal : false,
            linkModal : false,
            tradeType : '2631',
            orgPk : '',
            showUploader: false,
            target: null,
            uploadPk : '',
            selectedPKS:[],
            fianceRef : '',
            status : 'browse'
        };
        this.pubMessage = new pubMessage(props);
    }

    componentWillMount(){
        initTemplate.call(this, this.props );
    }

    //期初关闭
    closeOk() {
        if(this.state.orgPk.length){
            requestApi.CloseInit({
                data : { pkOrg : this.state.orgPk },
                success : (data) => {
                    let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId
                    toast({ content: multiLang && multiLang.get('201103QCDJ-0006'), color: 'success' });//'期初关闭成功'
                }
            })
        }
        this.setState({ showCloseModal: false });
    }
    //期初取消关闭
    unCloseOk(){
        if(this.state.orgPk.length){
            requestApi.UnCloseInit({
                data : { pkOrg : this.state.orgPk },
                success : (data) => {
                    let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId
                    toast({ content: multiLang && multiLang.get('201103QCDJ-0007'), color: 'success' });//'期初取消关闭成功'
                }
            })
        }
        this.setState({ showUnCloseModal: false });
    }
    //选中交易类型
    tradeTypeOk(){
        let rows = this.props.table.getCheckedRows('TradeModal');
        if(rows && rows.length > 0){
            this.setState({tradeType: rows[0].data.values.pk_billtypecode.value});
        }
        this.setState({ showTradeModal: false });
    }
    cancel() {
        this.setState({
            showCloseModal: false,
            showUnCloseModal: false,
            showTradeModal: false
        });
    }
    orgChange(param){
        let selectOrgs = [];
        param.forEach((orgParam) => {
            selectOrgs.push(orgParam.refpk)
        })
        this.setState({
            orgPk : selectOrgs,
            fianceRef : param
        });
    }

    close(){
        this.setState({ linkModal : false});
    }

    onSelectedFn(){
        let rows1 = this.table.getCheckedRows(window.presetVar.head.head1);
        if(rows1.length){
            [window.presetVar.button.delete, window.presetVar.button.export, window.presetVar.button.linkbx, window.presetVar.button.copy, 
                window.presetVar.button.print, window.presetVar.button.printList, window.presetVar.button.accessory].map((button) => {
                    this.button.setButtonDisabled(button, false);
                });
        }else{
            [window.presetVar.button.delete, window.presetVar.button.export, window.presetVar.button.linkbx, window.presetVar.button.copy,
                window.presetVar.button.print, window.presetVar.button.printList, window.presetVar.button.accessory].map((button) => {
                    this.button.setButtonDisabled(button, true);
                });
        }
    }

    //交易类型弹框里的选中事件
    onSelectedTrade(props, moduleId, record, index){
        let selectRows = props.table.getCheckedRows(window.presetVar.modal);
        if(selectRows.length > 1){
            for(let i = 0;i < selectRows.length; i++){
				if(selectRows[i].index != index){
					props.table.selectTableRows(window.presetVar.modal, selectRows[i].index, false);
				}
			}
        }
    };
    
    //交易类型弹框里的全选事件
    onSelectedAllTrade(props,moduleId,isTrue){
        if(isTrue){
            props.table.selectAllRows(moduleId, false);
            let multiLang = props.MutiInit.getIntl(2011); //this.moduleId
            toast({content : multiLang && multiLang.get('201103QCDJ-0010'), color:'warning'});//'该节点不支持表格全选功能'
        }else{
            props.editTable.selectAllRows(moduleId,false);
 
        }
    }

    render() {
        let {form, button, table, editTable, search, modal} = this.props;
        let {NCCreateSearch} = search;
        let {createButton} = button;
        let{createEditTable} = editTable;
        let {createSimpleTable} = table;
        let {createButtonApp} = button;
        let { createModal } = modal;

        // buttonClick.linkysBtnEventConfig.click = buttonClick.linkysBtnEventConfig.click.bind(this);
        let rowDoubleClick =  onRowDoubleClick.bind(this); 
        let rowDoubleClickForLink =  onRowDoubleClickForLink.bind(this); 
        let { showUploader, target } = this.state;
        let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId
        let btnModalConfig = {
            [window.presetVar.button.delete] : {
                // "2011-0004": "删除",
                // "2011-0018": "确认删除该单据吗？"
                title: multiLang && multiLang.get('2011-0004'),
                content: multiLang && multiLang.get('2011-0018'),
            }
        }
        return (
        <div><ProfileStyle
                layout="singleTable"
                {...this.props}
            >
                <ProfileHead
                    // "201103QCDJ-0001": "借款期初单据"
                    title={multiLang && multiLang.get('201103QCDJ-0001')}
                    refreshButtonEvent={refreshButtonClick.bind(this)} 
                    status = { this.state.status }
                >
                    <ButtonGroup
                        areaId="page_header"
                        buttonLimit="4"
                        buttonEvent={buttonClick.bind(this)}
                        modalConfig={btnModalConfig}
                    />
                </ProfileHead>
                <ProfileBody>
                    <EmptyArea className="ssc-profile-search-area">
                        { NCCreateSearch(
                            window.presetVar.searchArea,//模块id
                            {
                                clickSearchBtn: searchClick.bind(this),//   点击按钮事件
                                defaultConditionsNum:1, //默认显示几个查询条件
                                // saveSearchPlan:true  //  是否需要查询方案功能(暂不可用)
                                onAfterEvent: afterEvent(),//编辑后事件
                            }
                        )}
                    </EmptyArea>
                    <EmptyArea>
                        {createSimpleTable(window.presetVar.head.head1, {
                            onAfterEvent: afterEvent,
                            onRowDoubleClick : rowDoubleClick,
                            handlePageInfoChange: pageInfoClick,
                            showCheck:true,
                            showIndex:true,
                            dataSource: this.dataSource,
                            pkname: 'pk_jkbx',
                            onSelected :this.onSelectedFn,
                            onSelectedAll : this.onSelectedFn
                        })}
                    </EmptyArea>
                    <EmptyArea>
                        {showUploader && < NCUploader
                            billId={ this.state.uploadPk }
                            targrt={target}
                            onHide={() => this.setState({showUploader: false})}
                            placement={'bottom'} />
                        }
                    </EmptyArea>
                    {/*期初关闭弹框*/}
                    {
                        multiLang && createModal('closeInit',{
                            title: multiLang.get('201103QCDJ-0002'),// 所属组织
                            content: <div>
                            {FinanceOrg({                                    
                                value : this.state.fianceRef,
                                onChange:this.orgChange.bind(this),
                                isMultiSelectedEnabled:1,
                            })}
                            </div>, //弹框内容，可以是字符串或dom
                            beSureBtnClick: this.closeOk.bind(this), //点击确定按钮事件
                            /* cancelBtnClick: this.close.bind(this), //取消按钮事件回调
                            closeModalEve: this.close.bind(this), //关闭按钮事件回调 */
                            userControl:false,  // 点确定按钮后，是否自动关闭弹出框.true:手动关。false:自动关
                            size:'lg', //  模态框大小 sm/lg/xlg
                            noFooter : false, //是否需要底部按钮,默认true
                            rightBtnName : multiLang.get('2011-0002'), //左侧按钮名称,默认关闭
                            leftBtnName : multiLang.get('2011-0003'), //右侧按钮名称， 默认确认
                        })
                    }
                    {/*取消关闭期初弹框*/}
                    {
                        multiLang && createModal('unCloseInit',{
                            title:  multiLang.get('201103QCDJ-0002'),// 所属组织
                            content: <div>
                            {FinanceOrg({                                    
                                    value : this.state.fianceRef,
                                    onChange:this.orgChange.bind(this),
                                    isMultiSelectedEnabled:1
                                })}
                            </div>, //弹框内容，可以是字符串或dom
                            beSureBtnClick: this.unCloseOk.bind(this), //点击确定按钮事件
                            /* cancelBtnClick: this.close.bind(this), //取消按钮事件回调
                            closeModalEve: this.close.bind(this), //关闭按钮事件回调 */
                            userControl:false,  // 点确定按钮后，是否自动关闭弹出框.true:手动关。false:自动关
                            size:'lg', //  模态框大小 sm/lg/xlg
                            noFooter : false, //是否需要底部按钮,默认true
                            rightBtnName :  multiLang.get('2011-0002'), //左侧按钮名称,默认关闭
                            leftBtnName :  multiLang.get('2011-0003'), //右侧按钮名称， 默认确认
                        })
                    }
                    {/*交易类型弹框*/}
                    {
                        multiLang && createModal('trade',{
                            title: multiLang.get('201103QCDJ-0003'),// 交易类型
                            content: <div>
                            {createSimpleTable('TradeModal', {
                                showIndex : true,
                                showCheck : true,
                                onSelectedAll: this.onSelectedAllTrade,   
                                onSelected:  this.onSelectedTrade ,
                            })}
                            </div>, //弹框内容，可以是字符串或dom
                            beSureBtnClick: this.tradeTypeOk.bind(this), //点击确定按钮事件
                            /* cancelBtnClick: this.close.bind(this), //取消按钮事件回调
                            closeModalEve: this.close.bind(this), //关闭按钮事件回调 */
                            userControl:false,  // 点确定按钮后，是否自动关闭弹出框.true:手动关。false:自动关
                            size:'lg', //  模态框大小 sm/lg/xlg
                            noFooter : false, //是否需要底部按钮,默认true
                            rightBtnName : multiLang.get('2011-0002'), //左侧按钮名称,默认关闭
                            leftBtnName : multiLang.get('2011-0003'), //右侧按钮名称， 默认确认
                        })
                    }
                    {/*联查报销单弹框*/}
                    {
                        multiLang && createModal('linkbx',{
                            title:  multiLang.get('201103QCDJ-0004'),// 交易类型
                            content: <div>
                            {createSimpleTable('LinkModal', {
                                showIndex : true,
                                onRowDoubleClick : rowDoubleClickForLink
                            })}
                            </div>, //弹框内容，可以是字符串或dom
                            // beSureBtnClick: this.tradeTypeOk.bind(this), //点击确定按钮事件
                            /* cancelBtnClick: this.close.bind(this), //取消按钮事件回调
                            closeModalEve: this.close.bind(this), //关闭按钮事件回调 */
                            userControl:false,  // 点确定按钮后，是否自动关闭弹出框.true:手动关。false:自动关
                            size:'xlg', //  模态框大小 sm/lg/xlg
                            noFooter : false, //是否需要底部按钮,默认true
                            rightBtnName :  multiLang.get('2011-0007'), //左侧按钮名称,默认关闭
                            // leftBtnName : multiLang && multiLang.get('2011-0003'), //右侧按钮名称， 默认确认
                        })
                    }
                </ProfileBody>
            </ProfileStyle>
            <div>
                {/* 导入 */}
                {createModal('importModal', {
                    noFooter : true, 
                    className: 'import-modal'
                })}
                {/* 导出 */}
                <ExcelImport 
                    {...Object.assign(this.props)}
                    moduleName = 'erm'//模块名
                    appcode = {this.props.getSearchParam('c')}
                    pagecode = {this.props.getSearchParam('p')}
                    billType = '263X'//单据类型
                    selectedPKS = {this.state.selectedPKS}
                    referVO = {{ignoreTemplate : true}}
                />
            </div>
            </div>
        )
    }
}

InitBills = createPage({
    mutiLangCode: '2011'
})(InitBills);

export default InitBills;