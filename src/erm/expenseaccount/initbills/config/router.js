import {asyncComponent} from 'nc-lightapp-front';
import card from '../../../expenseaccount/loanmanage/card/jkbxBillCard';
import list from '../config/InitBills';

const routes = [
  {
    path: '/list',
    component: list,
  },
  {
    path: '/edit',
    component: card,
  }
];

export default routes;
