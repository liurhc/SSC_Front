import { ajax } from 'nc-lightapp-front';
import requestApi from '../requestApi'
export default function (props, config, pks) {
    requestApi.query({
        data:{
            pks : pks
        },
        success: (data) => {
            if(data && data.data){
                props.table.setAllTableData(window.presetVar.head.head1, data.data[window.presetVar.head.head1]);
            }else{
                props.table.setAllTableData(window.presetVar.head.head1, {
                    areacode:"head",
                    rows:[]
                });
            }

        }
    })
    /*let allpks=[];
    for(let i=1;i<19;i++){
        allpks.push(i)
    }
    if(pks.length<10){
        let i = 11;
        data = {
            head: {

                rows: [{
                    values: {
                        id:{value:i++},
                        bill_code: { value: "56451" },
                        bill_status: {
                            "display": "审批通过",
                            "value": "3"
                        },
                        pk_user: { value: "李四" }
                    }
                }, {
                    values: {
                        id:{value:i++},
                        bill_code: { value: "56451" },
                        bill_status: {
                            "display": "审批通过",
                            "value": "3"
                        },
                        pk_user: { value: "李四" }
                    }
                }, {
                    values: {
                        id:{value:i++},
                        bill_code: { value: "56451" },
                        bill_status: {
                            "display": "审批通过",
                            "value": "3"
                        },
                        pk_user: { value: "李四" }
                    }
                }, {
                    values: {
                        id:{value:i++},
                        bill_code: { value: "56451" },
                        bill_status: {
                            "display": "审批通过",
                            "value": "3"
                        },
                        pk_user: { value: "李四" }
                    }
                }, {
                    values: {
                        id:{value:i++},
                        bill_code: { value: "56451" },
                        bill_status: {
                            "display": "审批通过",
                            "value": "3"
                        },
                        pk_user: { value: "李四" }
                    }
                }, {
                    values: {
                        id:{value:i++},
                        bill_code: { value: "56451" },
                        bill_status: {
                            "display": "审批通过",
                            "value": "3"
                        },
                        pk_user: { value: "李四" }
                    }
                }, {
                    values: {
                        id:{value:i++},
                        bill_code: { value: "56451" },
                        bill_status: {
                            "display": "审批通过",
                            "value": "3"
                        },
                        pk_user: { value: "李四" }
                    }
                }, {
                    values: {
                        id:{value:i++},
                        bill_code: { value: "56451" },
                        bill_status: {
                            "display": "审批通过",
                            "value": "3"
                        },
                        pk_user: { value: "李四" }
                    }
                }
                ]
            }}
    }else{
        let i =1;
        data = {
            head: {
                rows: [
                    {
                        values: {
                            id:{value:i++},
                            bill_code: { value: "123234" },
                            bill_status: {
                                "display": "自由态",
                                "value": "0"
                            },
                            pk_user: { value: "张三" }
                        }
                    }, {
                        values: {
                            id:{value:i++},
                            bill_code: { value: "56451" },
                            bill_status: {
                                "display": "审批通过",
                                "value": "3"
                            },
                            pk_user: { value: "李四" }
                        }
                    }, {
                        values: {
                            id:{value:i++},
                            bill_code: { value: "wew" },
                            bill_status: {
                                "display": "审批通过",
                                "value": "3"
                            },
                            pk_user: { value: "11111" }
                        }
                    }, {
                        values: {
                            id:{value:i++},
                            bill_code: { value: "wr3454" },
                            bill_status: {
                                "display": "审批通过",
                                "value": "3"
                            },
                            pk_user: { value: "aaaaaaa" }
                        }
                    }, {
                        values: {
                            id:{value:i++},
                            bill_code: { value: "ewretr" },
                            bill_status: {
                                "display": "审批通过",
                                "value": "3"
                            },
                            pk_user: { value: "cccccccc" }
                        }
                    }, {
                        values: {
                            id:{value:i++},
                            bill_code: { value: "ewtreytru" },
                            bill_status: {
                                "display": "审批通过",
                                "value": "3"
                            },
                            pk_user: { value: "vvvvvvvvvv" }
                        }
                    }, {
                        values: {
                            id:{value:i++},
                            bill_code: { value: "wrwrewr" },
                            bill_status: {
                                "display": "审批通过",
                                "value": "3"
                            },
                            pk_user: { value: "kkkkkkkkkk" }
                        }
                    }, {
                        values: {
                            id:{value:i++},
                            bill_code: { value: "56451" },
                            bill_status: {
                                "display": "审批通过",
                                "value": "3"
                            },
                            pk_user: { value: "李四" }
                        }
                    }, {
                        values: {
                            id:{value:i++},
                            bill_code: { value: "56451" },
                            bill_status: {
                                "display": "审批通过",
                                "value": "3"
                            },
                            pk_user: { value: "李四" }
                        }
                    }, {
                        values: {
                            id:{value:i++},
                            bill_code: { value: "56451" },
                            bill_status: {
                                "display": "审批通过",
                                "value": "3"
                            },
                            pk_user: { value: "李四" }
                        }
                    },
                ],
                pageInfo: {
                    pageIndex: "0",
                    pageSize: "10",
                    total: "18",
                    totalPage: "2"
                },
                allpks:allpks
            }
        }
    }


    props.table.setAllTableData("head", data.head);*/

    // ajax({
    // 	url: '',
    // 	data: data,
    // 	success: function (res) {
    // 		let { success, data } = res;
    // 		if (success) {
    // 			if (data) {
    // 				props.table.setAllTableData(tableId, data[tableId]);
    // 			} else {
    // 				props.table.setAllTableData(tableId, { rows: [] });
    // 			}
    // 		}
    // 	}
    // });
}
