import requestApi from '../requestApi'
export default function ( record, index) {
    requestApi.queryApp({
        data : {
            tradetype : record.djlxbm.value
        },
        success : (data) => {
             this.props.pushTo('/edit',{
                status: 'browse',
                tradetype : record.djlxbm.value,
                id: record.pk_jkbx.value,
                isinit : true,
                appcode : data.appcode,
                pagecode: data.pagecode
            });
        }
    })
}