import requestApi from '../requestApi'
import {print, toast, cardCache,ajax} from 'nc-lightapp-front';
let { deleteCacheById , getCacheById } = cardCache;

export default function buttonClick() {
    let props = this.props;
    let multiLang = props.MutiInit.getIntl(2011); //this.moduleId
    // let url = '/erm/expenseaccount/loanmanage/card/index.html';
    return{
        [window.presetVar.button.add]: () => {
            requestApi.queryApp({
                data : {
                    tradetype : this.state.tradeType
                },
                success : (data) => {
                    props.pushTo('/edit', {
                        status: 'add',
                        tradetype : this.state.tradeType,
                        isinit : true,
                        appcode : data.appcode,
                        pagecode: data.pagecode
                    });
                }
            })
        },
        [window.presetVar.button.edit]: () => {
            let editrows = props.table.getCheckedRows(window.presetVar.head.head1);
            if (editrows.length){
                let openbillid = editrows[0].data.values.pk_jkbx.value;
                let tradetype = editrows[0].data.values.djlxbm.value;
                requestApi.queryApp({
                    data : {
                        tradetype : tradetype
                    },
                    success : (data) => {
                        props.pushTo('/edit', {
                            status: 'edit',
                            id: openbillid,
                            tradetype : tradetype,
                            isinit : true,
                            appcode : data.appcode,
                            pagecode: data.pagecode
                        });
                    }
                })
            }else{
                toast({ content: multiLang && multiLang.get('201103QCDJ-0005'), color: 'warning' });//'请选择要修改的数据'
            }
        },
        [window.presetVar.button.delete]: () => {
            let delrows = props.table.getCheckedRows(window.presetVar.head.head1);
            let pkArr = [];
            if (delrows.length) {
                delrows.forEach((row) => {
                    props.table.deleteTableRowsByIndex(window.presetVar.head.head1, row.index);
                    pkArr.push(row.data.values.pk_jkbx.value)
                });
                requestApi.delete({
                    data : {
                        djpk :pkArr
                    },
                    success : (data) => {
                        //如果有缓存则删除缓存
                        pkArr.forEach((pk) => {
                            let cachData = getCacheById(pk,this.dataSource);
                            if(cachData){
                                deleteCacheById("pk_jkbx", pk, this.dataSource);
                            }
                        })
                        toast({ content: multiLang && multiLang.get('201103QCDJ-0009'), color: 'success' });//删除成功
                    }
                })
            }
        },
        [window.presetVar.button.trade]: () => {
            props.modal.show('trade');
            requestApi.queryTradeType({
                data : {

                },
                success : (data) => {
                    data&&props.table.setAllTableData(window.presetVar.modal, data[window.presetVar.modal]);
                }
            })
        },
        [window.presetVar.button.close]: () => {
            props.modal.show('closeInit');
        },
        [window.presetVar.button.unclose]: () => {
            props.modal.show('unCloseInit');
        },
        [window.presetVar.button.copy]: () => {
            let copyrows = props.table.getCheckedRows(window.presetVar.head.head1);
            if (copyrows.length){
                let openbillid = copyrows[0].data.values.pk_jkbx.value;
                let tradetype = copyrows[0].data.values.djlxbm.value;
                requestApi.queryApp({
                    data : {
                        tradetype : tradetype
                    },
                    success : (data) => {
                        props.pushTo('/edit', {
                            status: 'add',
                            copyFromBillId: openbillid,
                            tradetype : tradetype,
                            isinit : true,
                            appcode : data.appcode,
                            pagecode: data.pagecode
                        });
                    }
                })
            }
        },
        [window.presetVar.button.print]: () => {
            let printrows = props.table.getCheckedRows(window.presetVar.head.head1);
            if (printrows.length){
                let vdata={};
                vdata.tradetype = printrows[0].data.values.djlxbm.value;
                ajax({
                    url: '/nccloud/erm/pub/AppInfoQueryAction.do',
                    data: vdata,
                    success: (data) => {
                        print(
                            'pdf',  //支持两类: 'html'为模板打印, 'pdf'为pdf打印
                            '/nccloud/erm/expenseaccount/ErmJkbxPrintAction.do', //后台服务url
                            {
                                funcode:data.data.appcode,      //功能节点编码，即模板编码
                                nodekey:'2631_IWEB',     //模板节点标识
                                oids:[printrows[0].data.values.pk_jkbx.value],    // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
                            }
                        )
                    }
                })   
            }
        },
        /* 输出和预览功能不要了
        [window.presetVar.button.preView]: () => {
            let printrows = props.table.getCheckedRows(window.presetVar.head.head1);
            if (printrows.length){
                print(
                    'pdf',  //支持两类: 'html'为模板打印, 'pdf'为pdf打印
                    '/nccloud/erm/expenseaccount/ErmJkbxPrintAction.do', //后台服务url
                    {
                        billtype:printrows[0].data.values.djlxbm.value,  //单据类型
                        funcode:'20110ETLB',      //功能节点编码，即模板编码
                        nodekey:'2631_IWEB',     //模板节点标识
                        oids:printrows[0].data.values.pk_jkbx.value,    // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
                        printTemplateID:"1001Z310000000005G69"
                    }
                )
            }
        },
        [window.presetVar.button.output]: () => {
            let printrows = props.table.getCheckedRows(window.presetVar.head.head1);
            if (printrows.length){
                print(
                    'pdf',  //支持两类: 'html'为模板打印, 'pdf'为pdf打印
                    '/nccloud/erm/expenseaccount/ErmJkbxPrintAction.do', //后台服务url
                    {
                        billtype:printrows[0].data.values.djlxbm.value,  //单据类型
                        funcode:'20110ETLB',      //功能节点编码，即模板编码
                        nodekey:'2631_IWEB',     //模板节点标识
                        oids:printrows[0].data.values.pk_jkbx.value,    // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
                        printTemplateID:"1001Z310000000005G69"
                    }
                )
            }
        }, */
        [window.presetVar.button.printList]: () => {
            let printrows = props.table.getCheckedRows(window.presetVar.head.head1);
            let billPks = [];
            if (printrows.length){
                printrows.forEach((printrow) => {
                    billPks.push(printrow.data.values.pk_jkbx.value);
                });
                print(
                    'pdf',  //支持两类: 'html'为模板打印, 'pdf'为pdf打印
                    '/nccloud/erm/expenseaccount/ErmInitBillsPrintAction.do', //后台服务url
                    {
                        funcode:(props.getUrlParam("appcode")==undefined||props.getUrlParam("appcode")=='')?props.getSearchParam("c"):props.getUrlParam("appcode"),      //功能节点编码，即模板编码
                        nodekey:'2631_IWEB',     //模板节点标识
                        oids:billPks,    // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
                    }
                )
            }
        },
        [window.presetVar.button.linkbx]: () => {
            let linkrows = props.table.getCheckedRows(window.presetVar.head.head1);
            if (linkrows.length){
                requestApi.linkQuery({
                    data : {
                        jkPk : linkrows[0].data.values.pk_jkbx.value
                    },
                    success : (data) => {
                        if (data){
                            props.modal.show('linkbx');
                            props.table.setAllTableData(window.presetVar.modal1, data[window.presetVar.head.head1]);
                        } else {
                            toast({ content: multiLang && multiLang.get('201103QCDJ-0008'), color: 'warning' });//没有关联的单据信息
                        }
                    }
                })
            }
        },
        [window.presetVar.button.accessory]: () => {
            let uploadrows = props.table.getCheckedRows(window.presetVar.head.head1);
            if (uploadrows.length){
                if(this.state.showUploader){
                    this.setState({ showUploader: false });
                }
                else {
                    this.setState({
                        showUploader: true,
                        uploadPk: uploadrows[0].data.values.pk_jkbx.value
                    });
                }
            }
			// 没有勾选数据点附件按钮时的提示
            else{
                toast({ content: multiLang && multiLang.get('201103QCDJ-0012'), color: 'warning' });
            }
        },
        [window.presetVar.button.import]: () => {
            
        },
        [window.presetVar.button.export]: () => {
            let exportrows = this.props.table.getCheckedRows(window.presetVar.head.head1);
            if(exportrows.length){
                let pkArr = [];
                exportrows.forEach((row) => {
                    pkArr.push(row.data.values.pk_jkbx.value);
                });
                this.setState({
                    selectedPKS:pkArr  //传递主键数组,之前nc需要导出的加主键
                },()=>{
                    this.props.modal.show('exportFileModal');//不需要导出的只执行这行代码
                })
            }else{
                toast({ content: multiLang && multiLang.get('201103QCDJ-0011'), color: 'warning' });//'请选择要导出的数据'
            }
        },
    }
}