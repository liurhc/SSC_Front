import { getMultiLang, excelImportconfig } from 'nc-lightapp-front';
import requestApi from '../requestApi';
import Utils from '../../../../../uap/public/utils';
import linkQueryApi from '../../../../public/components/linkquery/linkquerybills'

window.presetVar = {
    ...window.presetVar,
    //状态常量。 用于url中的hash参数 或 框架api的状态
    status: {
        browse: 'browse', //浏览态
        edit: 'edit', //编辑态
        add: 'add', //新增态
        del: 'del'
    },
    page: '201103QCDJ_list',
    head: {
        head1: 'head',
        head2: 'linkbx'
    },
    searchArea: '20110BO',
    modal: 'TradeModal',
    modal1: 'LinkModal',
    button: {
        add: 'Add',
        edit: 'Edit',
        delete: 'Delete',
        trade: 'TradeType',
        close: 'Close',
        unclose: 'Unclose',
        copy: 'Copy',
        preview: 'PreView',
        print: 'Print',
        output: 'Output',
        printList: 'PrintList',
        linkbx: 'LinkBx',
        accessory: 'Accessory',
        import: 'Import',
        export: 'Export',
    },
    pageSize: '10'
}
// window.addEventListener('hashchange', (...hashValue) => {});
export default function (props) {
    const appcode = props.getSearchParam('c');
    const pagecode = props.getSearchParam('p');
    props.createUIDom(
        {
            // pagecode: pagecode,//页面id
            // appcode: appcode
        },
        function (data) {
            getMultiLang({
                moduleId: 2011, domainName: 'erm', currentLocale: 'simpchn', callback: (json) => {
                    data.template[window.presetVar.searchArea].items.forEach((item) => {
                        if (item.attrcode === 'deptid_v' || item.attrcode === 'deptid') {
                            item.isShowUnit = true;
                            item.unitProps = {
                                refType: 'tree',
                                refName: json['201101DLSQ-0011'],//'业务单元',
                                refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                                rootNode: { refname: '业务单元', refpk: 'root' },
                                placeholder: json['201101DLSQ-0011'],//"业务单元",
                                queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                                treeConfig: { name: [json['201101DLSQ-0012']/* '编码' */, json['201101DLSQ-0013']/* '名称' */], code: ['refcode', 'refname'] },
                                isMultiSelectedEnabled: false,
                                //unitProps:unitConf,
                                isShowUnit: false
                            };
                        }
                        if (item.attrcode === 'jkbxr') {
                            item.isShowUnit = true;
                            item.unitProps = {
                                refType: 'tree',
                                refName: json['201101DLSQ-0011'],//'业务单元',
                                refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                                rootNode: { refname: json['201101DLSQ-0011']/* '业务单元' */, refpk: 'root' },
                                placeholder: json['201101DLSQ-0011'],//"业务单元",
                                queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                                treeConfig: { name: [json['201101DLSQ-0012']/* '编码' */, json['201101DLSQ-0013']/* '名称' */], code: ['refcode', 'refname'] },
                                isMultiSelectedEnabled: false,
                                //unitProps:unitConf,
                                isShowUnit: false
                            };
                        }
                        if (item.attrcode === 'djlxbm') {
                            item.queryCondition = function () {
                                return {
                                    parentbilltype: '263X',
                                    ExtRefSqlBuilder: 'nccloud.web.action.erm.ref.sqlbuilder.BilltypeRefSqlBuilder'//交易类型参照
                                }
                            }
                        }
                    });
                    data.template[window.presetVar.head.head1].items.forEach((item) => {
                        if (item.attrcode === 'djbh') {
                            item.render = (text, record, index) => {
                                return (
                                    <span className={'hyperlinks'} onClick={(() => {
                                        requestApi.queryApp({
                                            data: {
                                                tradetype: record.djlxbm.value
                                            },
                                            success: (data) => {
                                                props.pushTo('/edit', {
                                                    status: 'browse',
                                                    tradetype: record.djlxbm.value,
                                                    id: record.pk_jkbx.value,
                                                    isinit: true,
                                                    appcode: data.appcode,
                                                    pagecode: data.pagecode
                                                });
                                            }
                                        })
                                    })}>
                                        {record.djbh.value}
                                    </span>
                                )
                            }
                        }
                    });
                    //联查借款单区域
                    data.template[window.presetVar.head.head2].items.forEach((item) => {
                        if (item.attrcode === 'djbh') {
                            item.render = (text, record, index) => {
                                return (
                                    <span className={'hyperlinks'} onClick={(() => {
                                        linkQueryApi.link({
                                            data: {
                                                openbillid: record.pk_jkbx.value,
                                                tradetype: record.djlxbm.value,
                                                props: props
                                            }
                                        });
                                    })}>
                                        {record.djbh.value}
                                    </span>
                                )
                            }
                        }
                    });

                    let meta = {
                        '20110BO': {
                            moduletype: 'search',
                            items: data.template[window.presetVar.searchArea].items
                        },
                        head: {
                            moduletype: 'table',
                            pagination: true,
                            items: data.template[window.presetVar.head.head1].items
                        },
                        TradeModal: {
                            moduletype: 'table',
                            items: data.template[window.presetVar.modal].items
                        },
                        LinkModal: {
                            moduletype: 'table',
                            items: data.template[window.presetVar.head.head2].items
                        }
                    }
                    meta.pageid = data.template.pageid;
                    props.meta.setMeta(meta);

                    //单据导入的参数
                    let excelimportconfig = excelImportconfig(props, 'erm', '263X', true);
                    props.button.setUploadConfig(window.presetVar.button.import, excelimportconfig);
                    props.button.setButtons(data.button);
                    let selectedRows = props.table.getCheckedRows(window.presetVar.head.head1);
                    if (selectedRows.length) {
                        [window.presetVar.button.delete, window.presetVar.button.export, window.presetVar.button.linkbx, window.presetVar.button.copy,
                            window.presetVar.button.print, window.presetVar.button.printList, window.presetVar.button.accessory].map((button) => {
                                props.button.setButtonDisabled(button, false);
                            });
                    } else {
                        [window.presetVar.button.delete, window.presetVar.button.export, window.presetVar.button.linkbx, window.presetVar.button.copy,
                            window.presetVar.button.print, window.presetVar.button.printList, window.presetVar.button.accessory].map((button) => {
                                props.button.setButtonDisabled(button, true);
                            });
                    }
                }
            })
        }
    )
}
