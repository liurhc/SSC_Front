import requestApi from '../requestApi';
import { cacheTools } from 'nc-lightapp-front';

export default function searchClick(props, searchVal, type) {
    if(searchVal){
        cacheTools.set('searchVal', searchVal);
        for(let i in searchVal.conditions){
            if(searchVal.conditions[i].field === 'djlxbm'){
                searchVal.conditions[i].value.firstvalue = searchVal.conditions[i].display;
            }
        }
        let data={
            querycondition : {
                logic : "and",
                conditions : [searchVal]
            },
            pagecode: window.presetVar.page,
            pageInfo:{
                "pageIndex":-1,
                "pageSize":10,
                "totalPage":"0"
            },
            queryAreaCode:window.presetVar.searchArea,  //查询区编码
            oid:'0001Z310000000001DFH',  //查询模板id，手工添加在界面模板json中，放在查询区，后期会修改
            querytype:'tree'
        };
        requestApi.query({
            data,
            success: (data) => {
                if (type === 'refresh') {
                    this.pubMessage.refreshSuccess()
                } else {
                    this.pubMessage.querySuccess(data ? data.pks.length : 0)
                }

                if(data && data.pks){
                    let allpks = data.pks;
                    data.data[window.presetVar.head.head1].pageInfo = {
                        pageIndex: "0",
                        pageSize: window.presetVar.pageSize,
                        total: allpks.length,
                        totalPage: Math.ceil(allpks.length/window.presetVar.pageSize)
                    }
                    data.data[window.presetVar.head.head1].allpks = allpks;
                    data && data.data && props.table.setAllTableData(window.presetVar.head.head1, data.data[window.presetVar.head.head1]);
                }else{
                    props.table.setAllTableData(window.presetVar.head.head1, {
                        areacode:"head",
                        rows:[],
                        pageInfo : {
                            pageIndex: "0",
                            pageSize: window.presetVar.pageSize,
                            total: 0,
                            totalPage: 0
                        }
                    });
                }
                let selectedRows = this.props.table.getCheckedRows(window.presetVar.head.head1);
                if(selectedRows.length){
                    [window.presetVar.button.delete, window.presetVar.button.export, window.presetVar.button.linkbx, window.presetVar.button.copy,
                        window.presetVar.button.print, window.presetVar.button.printList, window.presetVar.button.accessory].map((button) => {
                            props.button.setButtonDisabled(button, false);
                        });
                }else{
                    [window.presetVar.button.delete, window.presetVar.button.export, window.presetVar.button.linkbx, window.presetVar.button.copy,
                        window.presetVar.button.print, window.presetVar.button.printList, window.presetVar.button.accessory].map((button) => {
                            props.button.setButtonDisabled(button, true);
                        });
                }
            }
        })
    }
}

