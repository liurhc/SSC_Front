import {ajax} from 'nc-lightapp-front';
import requestApi from '../requestApi'
import presetVar from '../presetVar';
import searchClick from './searchClick';

export default function (props) {

   let  appcode = (props.getUrlParam("appcode")==undefined||props.getUrlParam("appcode")=='')?props.getSearchParam("c"):props.getUrlParam("appcode");
   let  pagecode =  (props.getUrlParam("pagecode")==undefined||props.getUrlParam("pagecode")=='')?props.getSearchParam("p"):props.getUrlParam("pagecode");
   presetVar.appcode=appcode;
   presetVar.pagecode2=pagecode;
    props.createUIDom(
        {
            pagecode: pagecode,//页面编码
            appcode: appcode//小应用编码
        },
        (data) => {
            let metas = data.template;
            let tempArr = [];
            let searchitem = metas[presetVar.areaCode.searchArea];
            let headmeta = metas[presetVar.areaCode.billsArea];
            let bodymeta = metas[presetVar.areaCode.bodysArea];
            searchitem.items.forEach((item) => {
                if(item.itemtype == 'refer'){
                    item.isShowUnit=true;
                }

                //  if(item.itemtype == 'refer'){
                //      item.visible = false;
                //  }
                // else{
                //      item.visible = true;
                //  }
                 tempArr.push(item);
             });
            let transtype = "";
            if(data.context.paramMap && data.context.paramMap.transtype){
                transtype = data.context.paramMap.transtype
            };
            if(data.context.paramMap && data.context.paramMap.pagecode){
                window.presetVar.topagecode = data.context.paramMap.pagecode;
            }

            presetVar.tradetype= transtype;
            let meta = {
                ...metas,
                searchArea: {
                    moduletype: 'search',
                    items : tempArr
                },
                head:{
                    moduletype: 'table',
                    items: headmeta.items
                },
                body:{
                    moduletype: 'table',
                    items: bodymeta.items
                }
            }
            props.meta.setMeta(meta);
            searchClick.call(this,props,null, 'defaultSearch');
    });
}
