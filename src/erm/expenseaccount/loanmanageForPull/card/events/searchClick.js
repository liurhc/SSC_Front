import requestApi from '../requestApi';
import presetVar from '../presetVar';
import {toast} from 'nc-lightapp-front';

export default function searchClick(props, searchVal, isDefaultSearch) {
    if(true){
        let data={
            querycondition:searchVal==null?{}:searchVal,
            pagecode: presetVar.pageCode.searchPage,
            pageInfo:{
                "pageIndex":-1,
                "pageSize":10,
                "totalPage":"0"
            },
            queryAreaCode:presetVar.areaCode.searchArea,  //查询区编码
            oid:'0001Z3100000000083QM',  //查询模板id，手工添加在界面模板json中，放在查询区，后期会修改,pub_area中查询areaid
            querytype:'tree',
            tradetype:presetVar.tradetype
        };
        let senddata = {data,"pagecode":presetVar.pagecode2,"tepmid":this.props.meta.getMeta().pageid};
        requestApi.query({
            data:senddata,
            success: (data) => {
                let totalCount = 0;
                if(data){
                    totalCount = data.length;
                    data && this.props.transferTable.setTransferTableValue(presetVar.areaCode.billsArea, presetVar.areaCode.bodysArea, data, 'pk_mtapp_bill', 'pk_mtapp_detail');
                }else{
                    this.props.transferTable.setTransferTableValue(presetVar.areaCode.billsArea, presetVar.areaCode.bodysArea, [], 'pk_mtapp_bill', 'pk_mtapp_detail');
                }
                
                isDefaultSearch != 'defaultSearch' && toast({
                    color: 'success',
                    title: '已成功',
                    content: `查询成功，共${data && data.length || 0 }条`
                })

            }
        })
    }
}

