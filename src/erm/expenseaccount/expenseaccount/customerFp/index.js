import React, { Component } from 'react';
import { createPage,ajax,toast, base,promptBox } from 'nc-lightapp-front';
import requestApi from '../card/requestApi';

const { NCButton } = base;
import getAjax from '../service';
import './index.less';

var formid = 'fpcy_detail';
const pagecode = '201102FPCY';


/*** 
 * 
 * 明细弹框
 * 
 * ***/
export class CustomerFp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }

        var fieldCode = this.props.fieldCode;

        this.initTemplate(props);
        this.useBtnclick =  this.useBtnclick.bind(this);
    }

    componentDidMount() {
        let {self} = this.props;
        if(this.props.onRef instanceof Function) this.props.onRef(this);
        this.props.form.setFormStatus(formid, 'edit');
        // if(!self.state.uploadAlready) this.props.button.setButtonDisabled('fpcy_yr', true); 
    }

    //加载模板信息
    initTemplate(props, callback) {
        props.createUIDom(
            {
                pagecode: pagecode
            },
            (data) => {
                let meta = data.template;
                props.meta.setMeta(meta, callback);
                props.button.setButtons(data.button);

            }
        );

    }

    formatValue = (data) => {
        let temp = {};
        let tempobj = data.values;
        Object.keys(tempobj).forEach(function(item){
            temp[item] = tempobj[item].value;
        })
        return temp;
    }
    // 按钮点击事件
    onButtonClick(props, id) {
        let {self} = this.props;
        let _this= this;
        switch (id) {
            case 'fpcy_confirm':
            case 'fpcy_yr':
                if(!props.form.isCheckNow(formid,'warning')) return;
                let formValues = props.form.getAllFormValue(formid).rows;
                //收支项目
                let szxmid = self.props.form.getAllFormValue("head").rows[0].values.szxmid.value;
                // 财务组织
                let pk_fiorg = self.props.form.getAllFormValue("head").rows[0].values.pk_org.value;
                formValues[0].values.szxmid = szxmid;
                formValues[0].values.pk_fiorg = pk_fiorg;
                ajax({
                    url: '/nccloud/erm/expenseaccount/ErmfpCheckAction.do',
                    data: formValues,
                    success: (data) => {
                        if(data.data.code == "0000") {
                            let distinctRows = this.filterCurRows(data && data.data && data.data.invoice);
                            distinctRows.map(item => {
                                self.props.cardTable.addRow('other',0,item);
                            })
                            
                            console.log(data);
                            self.props.modal.close("detailFPcy");
                            promptBox({
                                content:<div><p>请上传所查验发票对应的影像附件!</p><p style={{color:'red'}}>注意：共享上线单位无发票影像附件财务审核时将驳回！</p></div>,
                                title: "查验成功",
                                beSureBtnName: "确定" ,
                                noCancelBtn: true,
                                beSureBtnClick:function(){
                                    _this.uploadeFile();
                                }
                            });
                        }else {
                            toast({ content: data.data.msg || "数据操作错误", color: 'error' });
                        }
                        
                    }
                })
                break;

            default:
                break;
        }

    }

    filterCurRows = (rows) => {

        if (!rows || rows.length < 1) return;

        let { self } = this.props;
        let curRows = self.props.cardTable.getAllRows('other')
        if (!curRows || curRows.length < 1) return rows;

        let rstRows = [];
        let curInvoiceNO = [];
        curRows.forEach(curRow => {
            curInvoiceNO.push(curRow.values["defitem7"].value);
        });
        rows.forEach(row => {
            if (!curInvoiceNO.includes(row["defitem7"].value)) {
                rstRows.push(row);
            }
        });

        return rstRows;
    }
    uploadeFile() {
        let {self} = this.props;
        let props = self.props;
        let billId = props.getUrlParam('billid') || self.state.billId || "";
        let billtype = window.presetVar.tradetype || "";
        if (billId == "" && !self.state.showUploader) {
            //去后台生成单据主键
            requestApi.generatBillId({
                data: { billtype: billtype },
                success: (data) => {
                    let accessorypkfildname = data["pkfieldName"];
                    billId = data[accessorypkfildname];
                    self.setState({
                        billId: billId,
                        showUploader: !self.state.showUploader
                    });
                    self.accessorybillid = billId;
                },
                error: (data) => {
                    toast({ content: data.message, color: 'danger' });
                }
            });
        } else {
            self.setState({
                billId: billId,
                showUploader: !self.state.showUploader
            });
        }        
    }
    useBtnclick() {
        this.props.button.setButtonDisabled('fpcy_yr', false);
    }
    render() {
        let { createForm } = this.props.form;
        let { createButtonApp } = this.props.button;
        return (
            <div className="my-single-table">
                <div className="nc-bill-form-area">
                    {createForm(formid, {

                    })}
                </div>
                <div className="bottom-button-area">
                    {/* <NCButton onClick={this.uploadeFile.bind(this)}>附件上传</NCButton> */}
                    {createButtonApp({
                        area: 'bottom',
                        buttonLimit: 2,
                        onButtonClick: this.onButtonClick.bind(this),
                        popContainer: document.querySelector('.header-button-area')
                    })}
                </div>
            </div>
        );
    }
}

CustomerFp = createPage({
    billinfo: {
        billtype: 'form',
        pagecode: pagecode,
        bodycode: formid
    },
    initTemplate: () => { }
})(CustomerFp);
