import { ajax } from 'nc-lightapp-front';

export default function getAjax(url, data) {
    return new Promise(function(resolve, reject) {
        ajax({
            url: url,
            data: data,
            sussess: function(data) {
                resolve(data)
            },
            error: function(error) {
                reject(error)
            }
        })
    })
}