import React, { Component } from 'react';
import { createPage, base, high, toast, promptBox, createPageIcon, ajax } from 'nc-lightapp-front';

const { NCSelect, NCModal, NCButton } = base;
const NCOption = NCSelect.NCOption;

import getAjax from '../service';
import './index.less';
import bigPng from './img/big.png'
import resetPng from './img/reset.png'
import rightPng from './img/right.png'
import leftPng from './img/left.png'
import littlePng from './img/little.png'
const formid = 'fpdr_detail_query_zzsfp';
//const tableid = 'fpdr_detail_table_zzsfp';
const pagecode = '201202FPDR';
let current=0;
let currentScale=1;

/***
 *
 * 明细弹框
 *
 * ***/
export class CustomerDr extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableid: 'fpdr_detail_table_zzsfp',
            formid: 'fpdr_detail_query_zzsfp',
            activity: 'zzsfp',
            currentPageIndex: 0,
            beforeShowMoodal: false,
            imgBase: ''
        }

        var fieldCode = this.props.fieldCode;

        this.initTemplate(props, () => {
            this.getTableData();
        });

    }

    // componetDidMount() {
    //     this.getTableData();
    // }
    beforeShow() {
        this.setState({ beforeShowMoodal: !this.state.beforeShowMoodal });
    }
    rotateRight(){
        let ele=document.getElementById('rotate')
            current = (current+90)%360;
            ele.style.transform = `scale(${currentScale})rotate(${current}deg)`;
    }
    rotateLeft(){
        let ele=document.getElementById('rotate')
        current = (current-90)%360;
      //  ele.style.transform = 'scale('+currentScale+')';
        ele.style.transform = `scale(${currentScale})rotate(${current}deg)`;
}
bigger(){
    currentScale=currentScale*1.1;
    if(currentScale<10){
        let ele=document.getElementById('rotate')
        ele.style.transform = `scale(${currentScale})rotate(${current}deg)`; 
    }
}
litler(){
    currentScale=currentScale/1.1;
    if(currentScale>0.2){
        let ele=document.getElementById('rotate')
        ele.style.transform = `scale(${currentScale})rotate(${current}deg)`; 
    }
}
reset(){
    currentScale=1;
    current=0;
        let ele=document.getElementById('rotate')
        ele.style.transform = `scale(1)rotate(0deg)`; 
}
    beforeShowAjax = (sendData = {}, callback=()=>{}) => {
        ajax({
            url: '/nccloud/erm/expenseaccount/AdvancePreviewAction.do',
            data: sendData,
            success: (res) => {
                if (res.data.success == 'true') {
                    callback(res.data.data)
                } else {
                    toast({ content: res.data.msg || "数据操作错误", color: 'error' });
                }
            }
        })
    }
   
    //加载模板信息
    initTemplate(props, callback) {
        props.createUIDom(
            {
                pagecode: pagecode
            },
            (data) => {
                let meta = data.template;
                let material_event = {
                    label: '操作',
                    itemtype: 'customer',
                    attrcode: 'opr',
                    width: '70px',
                    visible: true,
                    fixed: 'right',
                    render: (text, record, index) => {
                        let { values } = record;
                        let { fpdm , fphm , pjlx } = values;
                        let sendData = {};
                        if(fpdm) sendData.fpdm = fpdm.value;
                        if(fphm) sendData.fphm = fphm.value;
                        if(pjlx) sendData.pjlx = pjlx.value;
                        if(values.hcph) sendData.fphm = values.hcph.value;
                        if(values.gzkph) sendData.fphm = values.gzkph.value;
                        let hasAttache = values.hasattache;
                        let preview = "预览";
                        if(pjlx.value && pjlx.value == "invoice"){
                            if(!hasAttache || hasAttache.value != "true"){
                                preview = "";
                            }else{
                                preview = "预览"
                            }
                        }
                        return (
                            <div
                                className="currency-opr-col"
                                onClick={()=> this.beforeShowAjax(sendData, (imgBase)=>{
                                    this.setState({
                                        beforeShowMoodal: true,
                                        imgBase
                                    });
                                })}
                                style={{ cursor: 'pointer' }}
                            >
                                <span>{ preview }</span>
                            </div>
                        );
                    }
                };
                Object.keys(meta).forEach((key) => {
                    if (key && key.indexOf("table") > 0) {
                        meta[key].items.push(material_event);
                    }
                })
                props.meta.setMeta(meta, callback);
                props.button.setButtons(data.button);
            }
        );

    }

    isEmptyObj = (model) => {
        let hasProp = false;
        if (typeof model === "object" && !(model instanceof Array)) {
            for (let prop in model) {
                hasProp = true;
                break;
            }
            if (hasProp) {
                model = [model];
            } else {
                return false;
            }
        }
        return hasProp
    }

    getSelectedData = (type) => {
        let tableData = this.state[type];
        if (!tableData) return [];

        let selectedRows = [];
        // Object.values(tableData).map((rows,inex)=>{
        //     rows.map((row,key)=>{
        //         if(row.selected){
        //             selectedRows.push(row);
        //         }
        //     })
        // })
        Object.values(tableData).map((data, index) => {
            data.rows && data.rows.map((row) => {
                if (row.selected) {
                    selectedRows.push(row);
                }
            })
        })

        return selectedRows;
    }

    renderSelectData = () => {
        let { self } = this.props;
        let _this = this;
        let tableArr = ["zzsfp", "hkkp", "hcp", "kyqcfp", "czcfp", "glffp", "jdfp", "defp", "qtfp"];
        let DataObj = {};
        tableArr.map((item) => {
            // let temp_selectData = _this.props.editTable.getCheckedRows(`fpdr_detail_table_${item}`);
            let temp_selectData = this.getSelectedData(item);
            if (temp_selectData.length > 0) {
                DataObj[item] = temp_selectData;
            }
        })
        // 收支项目
        let szxmid = self.props.form.getAllFormValue("head").rows[0].values.szxmid.value;
        // 财务组织
        let pk_fiorg = self.props.form.getAllFormValue("head").rows[0].values.pk_org.value;
        // DataObj.szxmid = szxmid;
        return { DataObj, szxm: szxmid, pk_fiorg: pk_fiorg };
    }

    // 按钮点击事件
    onButtonClick = (props, id) => {
        // let { tableid } = this.state;
        let { self } = this.props;
        switch (id) {
            case 'fpdr_confirm':
                this.storeCurrentPageData();
                let DataObj = this.renderSelectData();

                if (!this.isEmptyObj(DataObj)) {
                    toast({ title: '请先选中数据！', color: 'warning' });
                    return;
                }
                ajax({
                    url: '/nccloud/erm/expenseaccount/InvoiceIntroduceAction.do',
                    data: DataObj,
                    success: (data) => {
                        if (data.data.code == "0000") {
                            let distinctRows = this.filterCurRows(data && data.data && data.data.invoice);
                            distinctRows.map(item => {
                                self.props.cardTable.addRow('other', 0, item);
                            })
                        } else {
                            toast({ content: data.data.msg || "数据操作错误", color: 'error' });
                        }
                    }
                })
                self.props.modal.close("detailFPdr");
                break;
            case 'fpdr_cancel':
                self.props.modal.close("detailFPdr");
                console.log("");
                break;

            default:
                break;
        }

    }

    clickSearchBtn = (props, data) => {
        let { formid, activity } = this.state;
        let searchValue = this.props.search.getQueryInfo(formid); // 查询条件
        this.state[activity] = {}; //清空保存数据
        this.state[activity].searchValue = searchValue;
        if (!searchValue || !searchValue.querycondition) {
            return;
        }
        // var taskOne = getAjax('post', 'http://localhost:8080/Test', values);
        // taskOne.then(function (data) {
        //     console.log(data)
        // }, function (error) {
        //     console.log(error.status)
        // })
        this.getTableData(searchValue)
    }

    //获取表格数据
    getTableData(searchValue) {
        let { self } = this.props;
        let { tableid, activity } = this.state;
        let _this = this;
        let pageInfo = this.props.editTable.getTablePageInfo(tableid);
        let pageIndex = pageInfo.pageIndex;
        let data = this.state[activity] && this.state[activity][pageIndex];

        if (data) {
            //  let {pageSize,total,totalPage} = data.pageInfo;
            //  let pageInfo1 = {pageIndex,pageSize,total,totalPage}
            _this.props.editTable.setTableData(this.state.tableid, data);
            return;
        }

        ajax({
            url: '/nccloud/erm/expenseaccount/EleInvoiceSWYAction.do',
            data: { pjlx: activity, pageInfo, searchValue },
            success: (data) => {
                let { invoice_list } = data.data;
                if (invoice_list.code == "0000") {
                    _this.props.editTable.setTableData(tableid, invoice_list);

                    //返回有总页数，所以要保留
                    let { pageInfo, rows, allpks } = invoice_list;
                    //只有第一页返回了pageinfo 所以要特殊处理一下
                    //不知道为什么pageIndex 总被该，防止缓存数据时修正一下
                    let data1 = {
                        [pageIndex]: { pageInfo, rows, allpks }
                    }
                    _this.state[activity] = _this.state[activity] || {};
                    Object.assign(_this.state[activity], data1);

                    _this.props.editTable.setStatus(this.state.tableid, 'browse');
                }
                else {
                    toast({ content: data.data.invoice_list.msg || "数据操作错误", color: 'error' });
                }

                // _this.props.cardTable.setTableData(`fpdr_detail_table_${type}`, { rows: data.data.invoice_list.rows });
                // data.data.invoice_list.rows.map(item => {
                //     _this.props.cardTable.addRow(`fpdr_detail_table_${type}`,0,item.values);
                // })
            }
        })
    }

    filterCurRows = (rows) => {

        if (!rows || rows.length < 1) return;

        let { self } = this.props;
        let curRows = self.props.cardTable.getAllRows('other')
        if (!curRows || curRows.length < 1) return rows;

        let rstRows = [];
        let curInvoiceNO = [];
        curRows.forEach(curRow => {
            curInvoiceNO.push(curRow.values["defitem7"].value);
        });
        rows.forEach(row => {
            if (!curInvoiceNO.includes(row["defitem7"].value)) {
                rstRows.push(row);
            }
        });

        return rstRows;
    }

    storeCurrentPageData = () => {

        let { currentPageIndex, tableid, activity } = this.state;
        let rows = this.props.editTable.getAllRows(tableid);
        // let allpks = this.state.allpks;
        // let data = this.state[activity];

        // if (!data[currentPageIndex]) {
        // data.push({[currentPageIndex]:rows});
        // data[currentPageIndex].rows = rows;
        this.state[activity][currentPageIndex].rows = rows;
        this.setState();
        // }
    }

    handleChange = (option) => {
        console.log(option);

        this.storeCurrentPageData();

        let tableid = `fpdr_detail_table_${option}`;
        let formid = `fpdr_detail_query_${option}`;

        let meta = this.props.meta.getMeta();
        meta[tableid].pagination = true; // 分页

        let currentPageIndex = !this.state[option] ? 0 : this.props.editTable.getTablePageInfo(tableid).pageIndex;

        this.props.meta.setMeta(meta, () => {
            this.setState({
                tableid: tableid,
                formid: formid,
                activity: option,
                currentPageIndex
            }, () => { this.getTableData() })
        })

        //this.props.cardTable.addRow(`fpdr_detail_table_${option}`,0,true);
    }

    pageInfoClick = (a, b, c, d, e, f) => {
        this.storeCurrentPageData();
        //缓存当前页数据后更新当前页的index
        let { tableid, activity } = this.state;
        this.state.currentPageIndex = this.props.editTable.getTablePageInfo(tableid).pageIndex;
        this.getTableData(this.state[activity].searchValue)
    }

   
    render() {
        let { search, button, editTable, cardTable, table } = this.props;
        let { tableid, formid } = this.state;
        let { createEditTable } = editTable;
        // let {createCardTable} = cardTable;
        let { createSimpleTable } = table;
        let { createButtonApp } = button;
        let { NCCreateSearch } = search;
        let pjlxdata = [{
            "value": "增值税发票",
            "key": "zzsfp"
        },
            {
                "value": "航空客票",
                "key": "hkkp"
            },
            {
                "value": "火车票",
                "key": "hcp"
            },
            {
                "value": "客运汽车发票",
                "key": "kyqcfp"
            },
            {
                "value": "出租车发票",
                "key": "czcfp"
            },
            {
                "value": "过路费发票",
                "key": "glffp"
            },
            {
                "value": "机打发票",
                "key": "jdfp"
            },
            {
                "value": "定额发票",
                "key": "defp"
            },
            {
                "value": "其他发票",
                "key": "qtfp"
            }
        ]
        let { imgBase } =  this.state
        let myImg = `data:image/png;base64,${imgBase}`;
        return (
            <div className="nc-single-table">
            <NCModal
        zIndex={10000}
        show={this.state.beforeShowMoodal}
        backdropClosable
        onHide={this.beforeShow.bind(this)}
            >
            <NCModal.Header closeButton></NCModal.Header>
           <NCModal.Body>
        <img src={myImg} 
        onClick={this.beforeShow.bind(this)} 
        id="rotate"/>
       </NCModal.Body>
        <NCModal.Footer style={{textAlign:'center'}}>
         <img src={resetPng}   width="40" height="40" onClick={this.reset.bind(this)} />
        <img src={bigPng}   width="40" height="40" onClick={this.bigger.bind(this)} />
        <img src={littlePng} width="40" height="40" onClick={this.litler.bind(this)} />
        <img src={leftPng}   width="40" height="40" onClick={this.rotateLeft.bind(this)} />
        <img src={rightPng}  width="40" height="40" onClick={this.rotateRight.bind(this)} />
        </NCModal.Footer>
        </NCModal>
        {/* 切换区 */}
    <div className="select-pj">
            <div className="label">票据类型：</div>
        <NCSelect
        defaultValue="zzsfp"
        style={{ width: 200, marginRight: 6 }}
        onSelect={this.onSelect}
        onChange={this.handleChange}
            >
            {pjlxdata.map((item, index) => {
                    return (
                        <NCOption key={index} value={item.key}>{item.value}</NCOption>
                )
                })
            }
            </NCSelect>
            </div>
        {/* 查询区 */}
    <div className="nc-singleTable-search-area" style={{ display: formid === 'fpdr_detail_query_zzsfp' ? 'block' : 'none' }}>
        {NCCreateSearch('fpdr_detail_query_zzsfp',//模块id
            {
                clickSearchBtn: this.clickSearchBtn.bind(this), //点击按钮事件
                showAdvBtn: false,
            }
        )}
    </div>
        <div className="nc-singleTable-search-area" style={{ display: formid === 'fpdr_detail_query_hkkp' ? 'block' : 'none' }}>
        {NCCreateSearch('fpdr_detail_query_hkkp',//模块id
            {
                clickSearchBtn: this.clickSearchBtn.bind(this), //点击按钮事件
                showAdvBtn: false,
            }
        )}
    </div>
        <div className="nc-singleTable-search-area" style={{ display: formid === 'fpdr_detail_query_hcp' ? 'block' : 'none' }}>
        {NCCreateSearch('fpdr_detail_query_hcp',//模块id
            {
                clickSearchBtn: this.clickSearchBtn.bind(this), //点击按钮事件
                showAdvBtn: false,
            }
        )}
    </div>
        <div className="nc-singleTable-search-area" style={{ display: formid === 'fpdr_detail_query_kyqcfp' ? 'block' : 'none' }}>
        {NCCreateSearch('fpdr_detail_query_kyqcfp',//模块id
            {
                clickSearchBtn: this.clickSearchBtn.bind(this), //点击按钮事件
                showAdvBtn: false,
            }
        )}
    </div>
        <div className="nc-singleTable-search-area" style={{ display: formid === 'fpdr_detail_query_czcfp' ? 'block' : 'none' }}>
        {NCCreateSearch('fpdr_detail_query_czcfp',//模块id
            {
                clickSearchBtn: this.clickSearchBtn.bind(this), //点击按钮事件
                showAdvBtn: false,
            }
        )}
    </div>
        <div className="nc-singleTable-search-area" style={{ display: formid === 'fpdr_detail_query_glffp' ? 'block' : 'none' }}>
        {NCCreateSearch('fpdr_detail_query_glffp',//模块id
            {
                clickSearchBtn: this.clickSearchBtn.bind(this), //点击按钮事件
                showAdvBtn: false,
            }
        )}
    </div>
        <div className="nc-singleTable-search-area" style={{ display: formid === 'fpdr_detail_query_jdfp' ? 'block' : 'none' }}>
        {NCCreateSearch('fpdr_detail_query_jdfp',//模块id
            {
                clickSearchBtn: this.clickSearchBtn.bind(this), //点击按钮事件
                showAdvBtn: false,
            }
        )}
    </div>
        <div className="nc-singleTable-search-area" style={{ display: formid === 'fpdr_detail_query_defp' ? 'block' : 'none' }}>
        {NCCreateSearch('fpdr_detail_query_defp',//模块id
            {
                clickSearchBtn: this.clickSearchBtn.bind(this), //点击按钮事件
                showAdvBtn: false,
            }
        )}
    </div>
    <div className="nc-singleTable-search-area" style={{ display: formid === 'fpdr_detail_query_qtfp' ? 'block' : 'none' }}>
        {NCCreateSearch('fpdr_detail_query_qtfp',//模块id
            {
                clickSearchBtn: this.clickSearchBtn.bind(this), //点击按钮事件
                showAdvBtn: false,
            }
        )}
    </div>
        <div className='nc-singleTable-table-area' style={{ display: tableid === 'fpdr_detail_table_zzsfp' ? 'block' : 'none' }}>{/* 表格区 */}
        {createEditTable('fpdr_detail_table_zzsfp', {
            handlePageInfoChange: this.pageInfoClick.bind(this),
            showCheck: true,
            showIndex: true,
            showPagination: true
        })}
    </div>
        <div className='nc-singleTable-table-area' style={{ display: tableid === 'fpdr_detail_table_hkkp' ? 'block' : 'none' }}>{/* 表格区 */}
        {createEditTable('fpdr_detail_table_hkkp', {
            handlePageInfoChange: this.pageInfoClick.bind(this),
            showCheck: true,
            showIndex: true,
            showPagination: true
        })}
    </div>
        <div className='nc-singleTable-table-area' style={{ display: tableid === 'fpdr_detail_table_hcp' ? 'block' : 'none' }}>{/* 表格区 */}
        {createEditTable('fpdr_detail_table_hcp', {
            handlePageInfoChange: this.pageInfoClick.bind(this),
            showCheck: true,
            showIndex: true,
            showPagination: true
        })}
    </div>
        <div className='nc-singleTable-table-area' style={{ display: tableid === 'fpdr_detail_table_kyqcfp' ? 'block' : 'none' }}>{/* 表格区 */}
        {createEditTable('fpdr_detail_table_kyqcfp', {
            handlePageInfoChange: this.pageInfoClick.bind(this),
            showCheck: true,
            showIndex: true,
            showPagination: true
        })}
    </div>
        <div className='nc-singleTable-table-area' style={{ display: tableid === 'fpdr_detail_table_czcfp' ? 'block' : 'none' }}>{/* 表格区 */}
        {createEditTable('fpdr_detail_table_czcfp', {
            handlePageInfoChange: this.pageInfoClick.bind(this),
            showCheck: true,
            showIndex: true,
            showPagination: true
        })}
    </div>
        <div className='nc-singleTable-table-area' style={{ display: tableid === 'fpdr_detail_table_glffp' ? 'block' : 'none' }}>{/* 表格区 */}
        {createEditTable('fpdr_detail_table_glffp', {
            handlePageInfoChange: this.pageInfoClick.bind(this),
            showCheck: true,
            showIndex: true,
            showPagination: true
        })}
    </div>
        <div className='nc-singleTable-table-area' style={{ display: tableid === 'fpdr_detail_table_jdfp' ? 'block' : 'none' }}>{/* 表格区 */}
        {createEditTable('fpdr_detail_table_jdfp', {
            handlePageInfoChange: this.pageInfoClick.bind(this),
            showCheck: true,
            showIndex: true,
            showPagination: true
        })}
    </div>
        <div className='nc-singleTable-table-area' style={{ display: tableid === 'fpdr_detail_table_defp' ? 'block' : 'none' }}>{/* 表格区 */}
        {createEditTable('fpdr_detail_table_defp', {
            handlePageInfoChange: this.pageInfoClick.bind(this),
            showCheck: true,
            showIndex: true,
            showPagination: true
        })}
    </div>
    <div className='nc-singleTable-table-area' style={{ display: tableid === 'fpdr_detail_table_qtfp' ? 'block' : 'none' }}>{/* 表格区 */}
        {createEditTable('fpdr_detail_table_qtfp', {
            handlePageInfoChange: this.pageInfoClick.bind(this),
            showCheck: true,
            showIndex: true,
            showPagination: true
        })}
    </div>
        <div className="nc-bill-buttom-area">
            {createButtonApp({
                                 area: 'bottom',
                                 buttonLimit: 2,
                                 onButtonClick: this.onButtonClick.bind(this),
            popContainer: document.querySelector('.header-button-area')
    })}
    </div>
        </div>
    );
    }
}

CustomerDr = createPage({
    billinfo: {
        billtype: 'extcard',
        pagecode: pagecode,
        headcode: ['fpdr_detail_query_zzsfp',
            'fpdr_detail_query_hkkp',
            'fpdr_detail_query_hcp',
            'fpdr_detail_query_kyqcp',
            'fpdr_detail_query_czcfp',
            'fpdr_detail_query_glffp',
            'fpdr_detail_query_jdfp',
            'fpdr_detail_query_defp',
            'fpdr_detail_query_qtfp'
        ],
        bodycode: ['fpdr_detail_table_zzsfp',
            'fpdr_detail_table_hkkp',
            'fpdr_detail_table_hcp',
            'fpdr_detail_table_kyqcp',
            'fpdr_detail_table_czcfp',
            'fpdr_detail_table_glffp',
            'fpdr_detail_table_defp',
            'fpdr_detail_table_jdfp',
            'fpdr_detail_table_qtfp'
        ]
    },
    initTemplate: () => { }
})(CustomerDr);
