import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { SimpleReport } from 'nc-report';

export default class Test extends Component {
	constructor(props) {
		super(props);
	}
	
	disposeSearch(meta, props) {
		let DeptTreeOrgRef = 'nccloud.web.action.erm.ref.sqlbuilder.DeptTreeStopRef';//部门
		// 参照过滤
		let items = meta['light_report'].items
		items.forEach((item) => {
			let filter = item.referFilterAttr;
			if(filter){
				if(filter[item.attrcode] && filter[item.attrcode].length>0)
				{filter[item.attrcode] = filter[item.attrcode].join(",");}
				else{filter[item.attrcode] ='';}
			}else{filter ={};filter[item.attrcode] ='';}

			if (item.attrcode == 'DEPTPK') {
				item.queryCondition = () => {
					let data = props.search.getSearchValByField('light_report', 'ORGPK').value.firstvalue;
					return { pk_org: data, TreeRefActionExt:DeptTreeOrgRef}; // 根据pk_org过滤
				};
			}
		});
		return meta; // 处理后的过滤参照返回给查询区模板
	}
	
	expandSearchVal(items, props, type, queryInfo) {
		// 变量赋值拓展
		console.log(items);
		return items;
    }

	clickAdvBtnEve = (props, searchId) => {}
    
	render() {
		return (
			<div className="table">
				<SimpleReport
					expandSearchVal={this.expandSearchVal.bind(this)}
					disposeSearch={this.disposeSearch.bind(this)}
				/>
			</div>
		);
	}
}

ReactDOM.render(<Test />, document.getElementById('app'));