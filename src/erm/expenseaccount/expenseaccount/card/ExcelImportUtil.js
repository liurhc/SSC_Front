
import Gzip from './../../../../uap/public/api/gzip'
/**
 * @desc 获取导出功能参数//
 * @param 
 * @param 
 * @return 
 */

 const getExcelImportConfig =(url,data,  callback)=> {
    let previousStatus;
    let excelimportconfig = {
        accept: '.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel',
        name: 'file',
        showUploadList: false,
        action: url,
        headers: {
            authorization: 'authorization-text'
        }
    };
    let resultStr = ''
    let paramdata = {data:JSON.stringify(data)};

    excelimportconfig.data = paramdata;
    excelimportconfig.NcUploadOnChange = (info) => {
        const gziptools = new Gzip();
        let response = typeof info.file.response == 'string' ? gziptools.unzip(info.file.response): info.file.response; 
        if(info.file.status == 'uploading' && info.file.status != previousStatus) {
            callback('beginUpload');
            previousStatus = 'uploading';
        }
        
        if (info.file.status !== 'uploading') {
          console.log(info.file, info.fileList);
        }
        if (info.file.status === 'done') {
            //记录成功或失败信息
            if(response.error){
                if(response.error.message){
                    callback(response.error.message);
                    resultStr = response.error.message;
                }
            }
           //记录成功或失败信息
           if(response.success){
                   
                callback(response);
        }
          console.log(`${info.file.name} file uploaded successfully`);
        } else if (info.file.status === 'error') {
          console.log(`${info.file.name} file upload failed.`);
        }
  };

  return {...excelimportconfig};
};

export {getExcelImportConfig}











