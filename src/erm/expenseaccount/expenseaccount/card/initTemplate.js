import { getAttachments, getMultiLang, toast } from 'nc-lightapp-front';
import requestApi from './requestApi';
import 'ssccommon/components/globalPresetVar';
import { setTableGroupExtendCol } from 'ssccommon/components/bill';
import { beforeUpdatePage, updatePage } from 'ssccommon/utils/updatePage';
import getDefDataFromMeta from 'ssccommon/utils/getDefDataFromMeta';

import { setPageStatus } from 'ssccommon/utils/statusController';
import { tableExtendButtonClick, afterEvent } from "./events";
import { setButtonsVisible } from 'ssccommon/utils/buttonUtils';
import logs from 'ssccommon/utils/logs';
import { Detail } from '../detail';
let debugLogs = new logs();

let pagecode = '';
let appcode = '';
window.presetVar = {
    ...window.presetVar,
    head: {
        head1: 'head',
        head2: ''
    },
    body: {

    },
    buttonTpl,
};

let pageButton = presetVar.pageButton;
const statusVar = presetVar.status;
let buttonTpl = '';
// let pageHashChange = null;
export default function (props) {

    window.presetVar.lsBodys = [];
    appcode = window.presetVar.appcode = props.getUrlParam("appcode") || props.getSearchParam("c") || '';
    pagecode = window.presetVar.pagecode = props.getUrlParam("pagecode") || props.getSearchParam("p") || '';
    window.presetVar.pagestatus = props.getUrlParam('status');
    window.presetVar.id = props.getUrlParam('id');
    window.presetVar.tradetype = props.getUrlParam("tradetype") || '';


    let pageStatus = window.presetVar.pagestatus;
    let _this = this;
    //hashchange 回调
    props.setHashChangeCallback(() => {
        debugLogs.timeStart('routeChange');
        routeChange.call(this, props);
        debugLogs.timeEnd('routeChange');

        // let isBrowseStatus = pageStatus == statusVar.browse;
        // if (isBrowseStatus) {
        //     props.cardTable.showColByKey("arap_bxbusitem", 'opr'); //显示扩展列
        // } else {
        //     props.cardTable.hideColByKey("arap_bxbusitem", 'opr'); //隐藏扩展列
        // }
    })

    let createUIDomPromise = new Promise((resolve, reject) => {
        props.createUIDom({}, (data) => {
            resolve(data);
        })
    });

    let getMultiLangPromise = new Promise((resolve, reject) => {
        getMultiLang({
            moduleId: 2011,
            domainName: 'erm',
            currentLocale: 'simpchn',
            callback: (json) => {
                resolve(json);
            }
        })
    });

    console.timeEnd('业务领域执行-开始请求模板接口的耗时：');
    console.time('请求：createUIDom耗时');
    Promise.all([createUIDomPromise, getMultiLangPromise]).then((resultList) => {
        console.timeEnd('请求：createUIDom耗时');
        console.time('meta第1次设置耗时')
        let data = resultList[0];
        let json = resultList[1];

        let meta = metaorder(data.template);
        
        //客商银行账户
        modifierMeta.call(self,props,meta);

        let bodys = Object.keys(data.template.gridrelation);
        for (var i = 1; i <= bodys.length; i++) {
            window.presetVar.body["body" + i] = bodys[i - 1];
        }


        window.presetVar.lsBodys = bodys;
        window.presetVar.pageId = meta.code;

        //根据页面状态，设置区域的初始渲染状态
        meta[presetVar.head.head1].status = pageStatus;
        bodys.forEach((body) => {
            if (meta[body]) {
                if (pageStatus == statusVar.add) { //cardTable新增态也要设置成edit
                    meta[body].status = statusVar.edit;
                } else {
                    meta[body].status = pageStatus;
                }
            }
        });


        buttonTpl = {
            button: (data.button == null || data.button == undefined) ? [] : data.button
        }
        let id = window.presetVar.id || this.state.billId;
        let fileman;
        this.multiLangJson = json;
        id = id == "" ? 0 : id;
        fileman = json['201102BCLF-0040'];
        if (pageStatus == statusVar.add) {
            buttonTpl.button.find((items) => {
                if (items.key == 'fileManager') {
                    items.title = fileman + ' ' + 0 + ' ';
                    return true;
                }
            });
        } else {
            getAttachments(id).then(lists => {
                this.setState({
                    groupLists: lists
                }, () => {
                    let filenum = this.state.groupLists;
    
                    if (filenum.length == 0) {
                        filenum = 0;
                    }
                    buttonTpl.button.find((items) => {
                        if (items.key == 'fileManager') {
                            items.title = fileman + ' ' + filenum + ' ';
                            this.props.button.setButtonTitle(items.key, items.title);
                            return true;
                        }
                    });
                })
            })
        }

        window.presetVar.buttonTpl = buttonTpl;
        // TOP 浏览态也需要操作列，页面初始化时统一加载 ADD
        props.button.hideButtonsByAreas(window.presetVar.head.head1);
        props.button.setButtons(buttonTpl.button);
        //设置表格扩展列
        setTableExtendCol.call(this, props, meta, buttonTpl.button);
        // BTM 浏览态也需要操作列，页面初始化时统一加载
        //兼容应用复制用transtype
        let transtype = "";
        if (data.context.paramMap && data.context.paramMap.transtype) {
            transtype = data.context.paramMap.transtype;
            window.presetVar.pullpagecode = !data.context.paramMap.pullpagecode ? "" : data.context.paramMap.pullpagecode;
            this.setState({ transtype_name: data.context.paramMap.transtype_name });
        }

        window.presetVar.tradetype = window.presetVar.tradetype || transtype;
        if (pageStatus == statusVar.add && !props.getUrlParam('addType') && !props.getUrlParam('copyFromBillId')) {
            let qextend = {
                transtypecode: window.presetVar.tradetype
            };
            requestApi.queryextend({
                data: qextend,
                success: (data) => {
                    let result = JSON.parse(data.data);
                    if (result.success == "true" && result.is_mactrl == 'true') {

                        _this.props.linkTo('/erm/expenseaccount/expenseaccount4Pull/card/index.html#/pull', { isfromSsc: true, appcode: window.presetVar.appcode, pagecode: window.presetVar.pullpagecode });
                        return;
                    } else if (result.success == "false") {
                        toast({ content: result.message, color: 'warning' });
                        return;
                    }
                }
            });
        }
        loadbill.call(this, meta, null);
    })

    function metaorder(meta) {
        let metas = {};
        for (let i = 0; i < window.presetVar.busitemarea.length; i++) {
            let code = window.presetVar.busitemarea[i];
            metas[code] = meta[code];
        }
        for (let m in meta) {
            if (window.presetVar.busitemarea.indexOf(m) < 0 && window.presetVar.otherarea.indexOf(m) < 0) {
                metas[m] = meta[m];
            }
        }
        for (let i = 0; i < window.presetVar.otherarea.length; i++) {
            let code = window.presetVar.otherarea[i];
            metas[code] = meta[code];
        }
        return metas;
    }



}
/**
 * 设置表格扩展列
 */
function setTableExtendCol(props, meta, button) {
    meta["arap_bxbusitem"].showCheck=true;  //费用明细添加checkbox;
    if(meta["other"]) {
        meta["other"].showCheck=true;
    }
    setTableGroupExtendCol(props, meta, button, [], {
        "arap_bxbusitem": {
            onButtonClick: tableExtendButtonClick.bind(this),
            copyRowWithoutCols: ['pk_busitem']
        },
        "bzitem": {
            onButtonClick: tableExtendButtonClick.bind(this),
            copyRowWithoutCols: ['pk_busitem']
        },
        "other": {
            onButtonClick: tableExtendButtonClick.bind(this),
            copyRowWithoutCols: ['pk_busitem']
        },
        "zsitem": {
            onButtonClick: tableExtendButtonClick.bind(this),
            copyRowWithoutCols: ['pk_busitem']
        },
        "er_bxcontrast": {
            onButtonClick: tableExtendButtonClick.bind(this),
            copyRowWithoutCols: ['pk_bxcontrast']
        },
        "er_cshare_detail": {
            onButtonClick: tableExtendButtonClick.bind(this),
            copyRowWithoutCols: ['pk_cshare_detail']
        },
        "accrued_verify": {
            onButtonClick: tableExtendButtonClick.bind(this),
            copyRowWithoutCols: ['pk_accrued_detail']
        }
    });
}

function loadbill(meta, pagestatus) {
    let props = this.props;
    let pageStatus = (pagestatus == null ? props.getUrlParam('status') : pagestatus) || 'add';
    let billdata = setDefData.call(this, meta);
    if (billdata == false) {
        return;
    }
    let userjson = { "pagecode": pagecode, "appcode": appcode };
    let _this = this;
    billdata.userjson = JSON.stringify(userjson);
    let redata = window.presetVar.redata == '' ? "Y" : window.presetVar.redata;
    if (/**pageStatus == statusVar.edit ||***/ pageStatus == statusVar.browse) {
        //redata不需要重新加载单据信息
        if (redata == "N" && pagestatus) {
            hideHeadArea(_this,meta,billdata.head.head.rows[0].values.zyx19.value ? billdata.head.head.rows[0].values.zyx19.value : props.form.getFormItemsValue(presetVar.head.head1,'zyx19').value);
            // TOP 性能优化，增加统一刷新方法 chengwbc ADD
            beforeUpdatePage(props);
            // BTM 性能优化，增加统一刷新方法
            // TOP 性能优化，保存后不需要setMeta chengwbc MOD
            // render.call(this, meta,window.presetVar.buttonTpl.button,pageStatus);
            render.call(this, meta, window.presetVar.buttonTpl.button, pageStatus, false);
            // BTM 性能优化，保存后不需要setMeta
            dataHandle(window.presetVar.reloadHead, props, pageStatus)
            // TOP 性能优化，增加统一刷新方法 chengwbc ADD
            updatePage(props, 'head', window.presetVar.lsBodys);
            // BTM 性能优化，增加统一刷新方法
            return;
        }
        render.call(this, meta, buttonTpl, pagestatus, true);
        let viewbilldata = {};
        viewbilldata.openbillid = props.getUrlParam("id") || props.getUrlParam("billid");
        viewbilldata.tradetype = window.presetVar.tradetype;
        viewbilldata.uid = getCookie("userid");//"1001Z3100000000000AE";这里以后从session中获取
        viewbilldata.pagecode = pagecode;
        viewbilldata.appcode = appcode;
        requestApi.viewBill({
            data: viewbilldata,
            success: (data) => {
                _this.setState({
                    myTabdata:data
                })
                if(data.head.head.rows[0].values.djzt.value == 0) {
                    props.button.setButtonDisabled({
                        [pageButton.billSubmit]: true,
                    });
                }

                if(data.head.head.rows[0].values.djlxbm.value == '264X-Cxx-017') {
                    hideHeadArea(_this,meta,'N');
                }else {
                    hideHeadArea(_this,meta,data.head.head.rows[0].values.zyx19.value);
                }
                
                // TOP 性能优化，增加统一刷新方法 chengwbc ADD
                beforeUpdatePage(props);
                // BTM 性能优化，增加统一刷新方法
                props.form.setAllFormValue({ [presetVar.head.head1]: data.head[presetVar.head.head1] });
                window.presetVar.reloadHead = data.head[presetVar.head.head1];
                // TOP 性能优化，先设置页面状态后加载数据 chengwbc MOVE:1818181430

                // BTM 性能优化，先设置页面状态后加载数据
                dataHandle(window.presetVar.reloadHead, props, pageStatus);
                
                setPageStatus(props, meta, ['head'], window.presetVar.lsBodys, 'browse');
                // BTM 整体设置页面状态
                let iscostshare = props.form.getFormItemsValue("head", ['iscostshare']) == null ? "" : props.form.getFormItemsValue("head", ['iscostshare'])[0].value
                if (iscostshare == true) {
                    _this.setState({ hideTables: [] })
                } else {
                    _this.setState({ hideTables: ['er_cshare_detail'] })
                }

                // "冲销明细"、"核销预提明细"页签没有数据时隐藏 -start
                var bxcontrastData = props.cardTable.getAllRows('er_bxcontrast'); 
                var accruedData = props.cardTable.getAllRows('accrued_verify'); 
                _this.setState((prevState)=>{
                    var hideTables = prevState.hideTables;
                    // if(!bxcontrastData || bxcontrastData.length==0){
                    //     hideTables.push('er_bxcontrast');// 隐藏冲销明细页签
                    // }
                    // if(!accruedData || accruedData.length==0){
                    //     hideTables.push('accrued_verify');// 隐藏核销预提明细页签
                    // }
                    if(!data.bodys.er_bxcontrast) {
                        hideTables.push('er_bxcontrast');// 隐藏冲销明细页签
                    }
                    if(!data.bodys.accrued_verify) {
                        hideTables.push('accrued_verify');// 隐藏核销预提明细页签
                    }
                    return {hideTables:hideTables};
                }); // "冲销明细"、"核销预提明细"页签没有数据时隐藏 -end

                requestApi.setMetasInit(meta, props, data);
                if (data.userjson && JSON.parse(data.userjson)) {
                    let djlx = JSON.parse(data.userjson);
                    if (djlx && djlx.ISCONTRAST && djlx.jkyjye && djlx.jkyjye > 0) {//是否冲借款
                        window.presetVar["ISCONTRAST"] = djlx.ISCONTRAST;
                    }
                    if (djlx && djlx.IS_MACTRL) {//是否冲借款
                        window.presetVar["IS_MACTRL"] = djlx.IS_MACTRL;
                    }
                }
                
                //设置表体数据
                window.presetVar.lsBodys.forEach((body) => {
                    if (data.bodys[body]) {
                        props.cardTable.setTableData(body, data.bodys[body]);
                    }
                });
                updatePage(props, 'head', window.presetVar.lsBodys);

            }
        });
    }
    //新增和拉单新增加载需要loaddata
    else if (pageStatus == statusVar.add) {
        let addtype = props.getUrlParam('addType');
        let copyFromBillId = props.getUrlParam('copyFromBillId');
        if (addtype == "pull") {
            let transfervalue = props.transferTable.getTransferTableSelectedId("head");

            userjson["addType"] = addtype;
            userjson["pulldata"] = transfervalue;
            billdata.userjson = JSON.stringify(userjson);

        }
        if (copyFromBillId) {
            let param = {};
            param.copyFromBillId = copyFromBillId;
            param.tradetype = window.presetVar.tradetype;
            param.pagecode = pagecode;
            param.appcode = appcode;
            requestApi.copyBill({
                data: param,
                success: (data) => {
                    props.button.setButtonDisabled({
                        ['pageTempSave']: false,
                    });

                    beforeUpdatePage(props);
                    props.form.EmptyAllFormValue(presetVar.head.head1);
                    // add by yts 清空支付相关信息
                    data.head[presetVar.head.head1].rows[0].values.zyx22={};
                    data.head[presetVar.head.head1].rows[0].values.zyx26={};
                    data.head[presetVar.head.head1].rows[0].values.zyx16={};
                    data.head[presetVar.head.head1].rows[0].values.zyx17={};
                    props.form.setAllFormValue({ [presetVar.head.head1]: data.head[presetVar.head.head1] });
                    //设置表体数据
                    window.presetVar.reloadHead = data.head[presetVar.head.head1]
                    Object.values(presetVar.body).forEach((body) => {
                        if (data.bodys[body]) {
                            props.cardTable.setTableData(body, data.bodys[body]);
                            props.cardTable.setStatus(body, "edit");
                        } else {
                            props.cardTable.setTableData(body, { rows: [] });
                        }
                    });
                    render.call(this, meta, buttonTpl, pagestatus);
                    dataHandle(window.presetVar.reloadHead, props, pageStatus);
                    //设置页面分摊不可编辑项，收款对象相关不可编辑项，组织本币汇率不可编辑
                    setbilldata(_this, props, data.head[presetVar.head.head1].rows[0].values);
                    this.setState({billId : data.head[presetVar.head.head1].rows[0].values.pk_jkbx.value});//复制后需要将这个变量更新，不然会影像后续的联查功能
                    meta = hideHeadArea(_this,meta,data.head.head.rows[0].values.zyx19.value); // 动态控制表头区域显示
                    // TOP 门户复制的单据打不开 chengwbc ADD
                    props.meta.setMeta(meta);
                    // BTM 门户复制的单据打不开
                }
            });

        } else {
            initData.call(this, meta, billdata, pageStatus);
        }
    }
    //修改更新页面状态即可，复制是需要重新加载数据
    else {

        if (props.getUrlParam("id") || props.getUrlParam("billid")) {
            if (redata == "N" && pagestatus) {
                render.call(this, meta, window.presetVar.buttonTpl.button, pageStatus);
                dataHandle(window.presetVar.reloadHead, props, pageStatus);
                return;
            }
            let billdata = {};
            billdata.openbillid = props.getUrlParam("id") || props.getUrlParam("billid");
            billdata.tradetype = window.presetVar.tradetype;
            billdata.uid = getCookie("userid");//这里以后从session中获取
            billdata.pagecode = pagecode;
            billdata.appcode = appcode;
            requestApi.viewBill({
                data: billdata,
                success: (data) => {
                    beforeUpdatePage(props);
                    if (data.userjson && JSON.parse(data.userjson)) {
                        let djlx = JSON.parse(data.userjson);
                        if (djlx && djlx.ISCONTRAST && djlx.ISCONTRAST == "Y" && djlx.jkyjye && djlx.jkyjye > 0) {//是否冲借款
                            window.presetVar["ISCONTRAST"] = djlx.ISCONTRAST;
                        }
                        if (djlx && djlx.IS_MACTRL) {//是否冲借款
                            window.presetVar["IS_MACTRL"] = djlx.IS_MACTRL;
                        }
                    }
                    props.form.setAllFormValue({ [presetVar.head.head1]: data.head[presetVar.head.head1] });
                    window.presetVar.reloadHead = data.head[presetVar.head.head1];
                    //设置表体数据
                    Object.values(presetVar.body).forEach((body) => {
                        if (data.bodys[body]) {
                            debugLogs.timeStart('setTableData_' + body);
                            props.cardTable.setTableData(body, data.bodys[body]);
                            debugLogs.timeEnd('setTableData_' + body);
                        }
                    });
                    dataHandle(window.presetVar.reloadHead, props, pageStatus)
                    setbilldata(_this, props, data.head[presetVar.head.head1].rows[0].values);
                    debugLogs.timeStart('viewBill');
                }
            });

            meta = hideHeadArea(_this,meta); // 动态控制表头区域显示
            // TOP 编辑态单据刷新页面报错 chengwbc ADD
            props.meta.setMeta(meta);
            // BTM 编辑态单据刷新页面报错
        }
    }
}
function setbilldata(_this, props, headData, useUpdatePage) {
    console.time('billData')
    let {paytarget, isexpamt, iscostshare, bbhl:org_currinfo} = headData;
    let disabledObj = {};
    let visibleObj = {}
    // BTM 性能优化，增加统一刷新方法
    paytarget = paytarget.value//收款对象
    if (paytarget == "0") {
        disabledObj = {'hbbm': true, 'customer': true, "custaccount": true, 'receiver': false, 'custaccount': true, 'freecust': true }
    } else if (paytarget == "1") {
        disabledObj = {'receiver': true, 'customer': true, 'hbbm': false, 'custaccount': false, 'freecust': false }
    } else if (paytarget == "2") {
        disabledObj = {'hbbm': true, 'receiver': true, 'customer': false, 'custaccount': false, 'freecust': true}
    }
    isexpamt = isexpamt.value//收款对象
    if (isexpamt == true || isexpamt == 'Y') {
        visibleObj = {"start_period": true, "total_period": true }
        disabledObj['start_period'] = false;
        disabledObj['total_period'] = false;
        
    } else {
        visibleObj = { "start_period": false, "total_period": false };
        disabledObj['start_period'] = false;
        disabledObj['total_period'] = false;
    }
    iscostshare = iscostshare.value
    if (!iscostshare) {
        _this.setState({ hideTables: ['er_cshare_detail'] })
    }

    // "冲销明细"、"核销预提明细"页签没有数据时隐藏 -start
    var bxcontrastData = props.cardTable.getAllRows('er_bxcontrast'); 
    var accruedData = props.cardTable.getAllRows('accrued_verify'); 
    _this.setState((prevState)=>{
        var hideTables = prevState.hideTables;
        if(!bxcontrastData || bxcontrastData.length==0){
            hideTables.push('er_bxcontrast');// 隐藏冲销明细页签
        }
        if(!accruedData || accruedData.length==0){
            hideTables.push('accrued_verify');// 隐藏核销预提明细页签
        }
        return {hideTables:hideTables};
    }); // "冲销明细"、"核销预提明细"页签没有数据时隐藏 -end

    org_currinfo = org_currinfo.value;
    if (org_currinfo && parseFloat(org_currinfo.value) == parseFloat(1)) {
        disabledObj['bbhl'] = true;
    } else {
        disabledObj['bbhl'] = false;
    }

    props.form.setFormItemsVisible(window.presetVar.head.head1, visibleObj);
    props.form.setFormItemsDisabled(window.presetVar.head.head1, disabledObj);

    if(useUpdatePage !== false){
        // add by yts 修复还没render时，更新界面报setState undefined错误
        setTimeout(()=>{
            updatePage.call(this,props, 'head', window.presetVar.lsBodys);
        })
    }
    console.timeEnd('billData')

}
function initData(meta, billdata, pagestatus) {
    let props = this.props;
    if(!meta || !meta.pageid) return;
    let pageStatus = pagestatus || window.presetVar.pagestatus;
    if (billdata.userjson) {
        billdata.userjson = JSON.stringify({ ...JSON.parse(billdata.userjson), templateid: meta.pageid })
    } else {
        billdata.userjson = JSON.stringify({ templateid: meta.pageid });
    }

    console.timeEnd('meta第1次设置耗时');
    console.time('请求：loadData耗时');
    let _this4 = this;
    requestApi.loaddata({
        meta: meta,
        props: props,
        data: billdata,
        success: (data) => {
            beforeUpdatePage(props);
            render.call(this, meta, buttonTpl, pageStatus, false);
            
            let jsfs = "";
            let pk_org = "";
            let bzbm = "";
            if(data.head['head'] && data.head['head']['rows'] && data.head['head']['rows'][0] && data.head['head']['rows'][0].values){
                if(data.head['head']['rows'][0].values.jsfs && data.head['head']['rows'][0].values.jsfs.display){
                    jsfs = data.head['head']['rows'][0].values.jsfs.display;
                }
                if(data.head['head']['rows'][0].values.pk_org && data.head['head']['rows'][0].values.pk_org.value){
                    pk_org = data.head['head']['rows'][0].values.pk_org.value;
                }
                if(data.head['head']['rows'][0].values.bzbm && data.head['head']['rows'][0].values.bzbm.value){
                    bzbm = data.head['head']['rows'][0].values.bzbm.value;
                }
            }
           
            
            if(jsfs == '银行汇款（软通）' || jsfs == '线下网银' || jsfs == '现金'){
                requestApi.getFkyhzh({
                    data:{
                        pk_org:pk_org,
                        bzbm:bzbm
                    },
                    success: (rsp) => {
                        data.head['head']['rows'][0].values.fkyhzh.value = rsp.data.pk_bankaccsub; 
                        data.head['head']['rows'][0].values.fkyhzh.display = rsp.data.subcode; 
                    
                        let addtype = props.getUrlParam('addType');
                        // TOP 性能优化，设置默认数据 chengwbc ADD
                        props.form.setAllFormValue({ ['head']: data.head['head'] });
                        // BTM 性能优化，设置默认数据

                        if (addtype == "pull") {
                            props.form.setAllFormValue({ [presetVar.head.head1]: data.head[presetVar.head.head1] });
                            window.presetVar.reloadHead = data.head[presetVar.head.head1];
                            //设置表体数据
                            Object.values(presetVar.body).forEach((body) => {
                                if (data.bodys[body]) {
                                    props.cardTable.setTableData(body, data.bodys[body]);
                                    props.cardTable.setStatus(body, 'edit');
                                }
                            });
                        } else {
                            Object.values(presetVar.body).forEach((body) => {
                                if (body !== 'accrued_verify' && body !== 'er_bxcontrast' && body !== 'er_cshare_detail' && body !== 'other') {
                                    props.cardTable.addRow(body, undefined, undefined, false);
                                }
                            });
                        }
                        console.time('dataHandle')
                        dataHandle(window.presetVar.reloadHead, props, pageStatus);
                        console.timeEnd('dataHandle')
                        setbilldata(this, props, data.head[presetVar.head.head1].rows[0].values);
                        
                        props.meta.setMeta(meta);
                        console.timeEnd('meta第2次设置耗时');
                        console.log('总耗时（父页面加载到子页面可用）:', new Date().getTime() - window.top.performance.timing.navigationStart);
                        //by houyb 初始化时调用一次编辑后事件 算出表头本币金额
                        let key = 'vat_amount';
                        let moduleId = 'arap_bxbusitem';
                        let rows =  this.props.cardTable.getAllRows(moduleId);
                        let value4 = props.cardTable.getValByKeyAndIndex(moduleId, 0,key);
                        let changedrows = [{
                            newvalue:{value:value4.value},
                            oldvalue:{value:0},
                            rowid:rows[0].rowid            
                        }]
                        
                        afterEvent.call(_this4,_this4.props,moduleId,key,value4,changedrows);
                        //by houyb 

                    }
                })
            }else{
                let addtype = props.getUrlParam('addType');
                // TOP 性能优化，设置默认数据 chengwbc ADD
                props.form.setAllFormValue({ ['head']: data.head['head'] });
                // BTM 性能优化，设置默认数据

                if (addtype == "pull") {
                    props.form.setAllFormValue({ [presetVar.head.head1]: data.head[presetVar.head.head1] });
                    window.presetVar.reloadHead = data.head[presetVar.head.head1];
                    //设置表体数据
                    Object.values(presetVar.body).forEach((body) => {
                        if (data.bodys[body]) {
                            props.cardTable.setTableData(body, data.bodys[body]);
                            props.cardTable.setStatus(body, 'edit');
                        }
                    });
                } else {
                    Object.values(presetVar.body).forEach((body) => {
                        if (body !== 'accrued_verify' && body !== 'er_bxcontrast' && body !== 'er_cshare_detail' && body !== 'other') {
                            props.cardTable.addRow(body, undefined, undefined, false);
                        }
                    });
                }
                console.time('dataHandle')
                dataHandle(window.presetVar.reloadHead, props, pageStatus);
                console.timeEnd('dataHandle')
                setbilldata(this, props, data.head[presetVar.head.head1].rows[0].values);
                
                props.meta.setMeta(meta);
                console.timeEnd('meta第2次设置耗时');
                console.log('总耗时（父页面加载到子页面可用）:', new Date().getTime() - window.top.performance.timing.navigationStart);
                //by houyb 初始化时调用一次编辑后事件 算出表头本币金额
                let key = 'vat_amount';
                let moduleId = 'arap_bxbusitem';
                let rows =  this.props.cardTable.getAllRows(moduleId);
                let value4 = props.cardTable.getValByKeyAndIndex(moduleId, 0,key);
                let changedrows = [{
                    newvalue:{value:value4.value},
                    oldvalue:{value:0},
                    rowid:rows[0].rowid            
                }]
                
                afterEvent.call(_this4,_this4.props,moduleId,key,value4,changedrows);
                //by houyb 
            }
            
        }
    });

    meta = hideHeadArea(this,meta); // 动态控制表头区域显示
    props.meta.setMeta(meta);
    // BTM 性能优化，数据返回前增加预先描画模板，提升用户体验
}
function setDefData(meta) {
    return getDefDataFromMeta(meta, 'nc.vo.ep.bx.BXHeaderVO');
}

/**
 * 根据表头自定义项19的勾选，动态隐藏/显示表头区域
 * @date 2019-09-05
 */
function hideHeadArea(_this,meta){
    if(!(meta['JWZF_head'] && meta['JNZF_head'])){ // 模板没有境内/外区域时不执行隐藏
        return meta;
    }
    let itemsMap = {};
    if(meta['JWZF_head'].items.length > 0){
        itemsMap['JWZF_head'] = meta['JWZF_head'].items;
    }
    if(meta['JNZF_head'].items.length > 0){
        itemsMap['JNZF_head'] = meta['JNZF_head'].items;
    }
    _this.setState({items:itemsMap}); // 境内/外区域的items维护到state中
    meta['JWZF_head'].items = []; // 隐藏境外区域
    return meta;
}

/**
 * 根据表头自定义项19的勾选，动态隐藏/显示表头区域
 * @date 2019-09-05
 */
function hideHeadArea(_this,meta,zyx19){
    if(!(meta['JWZF_head'] && meta['JNZF_head'])){ // 模板没有境内/外区域时不执行隐藏
        return meta;
    }
    let itemsMap = {};
    if(meta['JWZF_head'].items.length > 0){
        itemsMap['JWZF_head'] = meta['JWZF_head'].items;
    }
    if(meta['JNZF_head'].items.length > 0){
        itemsMap['JNZF_head'] = meta['JNZF_head'].items;
    }
    _this.setState({items:itemsMap}); // 境内/外区域的items维护到state中
    if(zyx19 == 'Y') {
        meta['JNZF_head'].items = []; // 隐藏境外区域
    }else {
        meta['JWZF_head'].items = []; // 隐藏境外区域
    }
    return meta;
}

/**
 * 明细弹框
 * @param {*} props 
 * @param {*} meta 
 * @param {*} tradetype 
 */
function showDetailModal(props,meta,tradetype){
    var modalTitleMap = new Map(); // 弹框标题信息
    if(tradetype == '2641' || tradetype == '264X-Cxx-018'){ // 差旅费报销单、差旅费报销单（经营班子）
        modalTitleMap.set('defitem22','住宿费明细');
        modalTitleMap.set('defitem23','餐费明细');
        modalTitleMap.set('defitem27','交通费明细');
        modalTitleMap.set('defitem25','其他费用总额');
    }
    // 会议费报销单（5000以上）、会议费报销单（5000以下）
    else if(tradetype == '264X-Cxx-015' || tradetype == '264X-Cxx-016') {
        modalTitleMap.set('defitem8','会议场租费');
        modalTitleMap.set('defitem9','会议餐费金额');
        modalTitleMap.set('defitem10','其他费用总额');
    } else {
        return meta; // 其他交易类型直接返回
    }

    meta['arap_bxbusitem'].items.forEach((item,index)=>{
        if(modalTitleMap.has(item.attrcode)){
            item.itemtype = 'customer';
            item.render = (text, record,index) => {
                if(record.values[item.attrcode]) {
                    return (
                        <a
                            className="modal-a"
                            onClick={() => {
                                    props.modal.show('detail', {
                                    title: modalTitleMap[item.attrcode],
                                    content: <Detail fieldCode={item.attrcode} record={record}/>
                                });
                            }}
                            >
                            {record.values[item.attrcode].display || record.values[item.attrcode].value}
                        </a>
                    );
                }
            };
        }
    });
    return meta;
}

// TOP 性能优化，保存后不需要setMeta chengwbc MOD
// function render(meta,button,pagestatus) {
function render(meta, button, pagestatus, setMetaFlag) {
    console.time('bizRender')
    // BTM 性能优化，保存后不需要setMeta
    let props = this.props;
    let pageStatus = pagestatus || window.presetVar.pagestatus;
    if (pageStatus == statusVar.add || pageStatus == statusVar.edit) {
        buttonTpl.button.find((btg) => {
            if (btg.key == 'buttongroup2' && btg.children != null) {
                btg.children.find((btn) => {
                    if (btn.key == 'billSubmit') {
                        props.button.setMainButton([pageButton.billSubmit], false);
                        btn['title'] = this.multiLangJson['2011-0019'];//"保存提交"
                        return true;
                    }
                })
                return true;
            }
        })
    } else {
        buttonTpl.button.find((btg) => {
            if (btg.key == 'buttongroup2' && btg.children != null) {
                btg.children.find((btn) => {
                    if (btn.key == 'billSubmit') {
                        props.button.setMainButton([pageButton.billSubmit], true);
                        btn['title'] = this.multiLangJson['2011-0014'];//"提交"
                        return true;
                    }
                })
                return true;
            }
        })
    }
    renderButtions.call(this, props, meta, pageButton, pageStatus);

    if (setMetaFlag) {
        meta = showDetailModal(props,meta,window.presetVar.tradetype); // 明细弹框
        // meta = hideHeadArea(this,meta); // 动态控制表头区域显示
        props.meta.setMeta(meta);
    }
    // BTM 性能优化，保存后不需要setMeta
    console.timeEnd('bizRender')
}
export { render }
function renderButtions(props, meta, pageButton, pagestatus) {
    //肩部按钮可见状态：页面和表格的肩部按钮，这里逻辑根据实际业务实现。
    let btnsVisibleObj = {};
    for (let btnKey in pageButton) {
        btnsVisibleObj[btnKey] = false;
    }
    let pageStatus = pagestatus || window.presetVar.pagestatus;
    let isAddOrEditStatus = pageStatus == statusVar.edit || pageStatus == statusVar.add;
    let isBrowseStatus = pageStatus == statusVar.browse;
    switch (pageStatus) {
        case statusVar.edit:
        case statusVar.add:
            {
                setPageStatus(props, meta, ['head'], window.presetVar.lsBodys, pageStatus);
                props.cardTable.setStatus('er_bxcontrast', window.presetVar.status.browse);//冲销页签不允许直接修改
            }
            // BTM 整体设置页面状态
            break;
        case statusVar.browse:
            {
                setPageStatus(props, meta, ['head'], window.presetVar.lsBodys, 'browse');
            }
            // BTM 整体设置页面状态
            break;
    }
    window.presetVar.buttonTpl.button.map((one) => {
        if (one.area.indexOf('card_body') > -1 && isBrowseStatus) {
            if (!(one.area.indexOf('card_body_inner') > -1 && one.key.indexOf('_Edit') > -1)) {
                btnsVisibleObj[one.key] = false;
            }

        }
        if (one.area.indexOf('card_body') > -1 && isAddOrEditStatus) {
            if (!(one.area.indexOf('card_body_inner') > -1 && one.key.indexOf('_Edit') > -1)) {
                btnsVisibleObj[one.key] = true;
            }
        }
    });
    //发票信息隐藏扩展列
    if(isBrowseStatus) {
        props.cardTable.hideColByKey("arap_bxbusitem", 'opr');
    }else {
        props.cardTable.showColByKey("arap_bxbusitem", 'opr');
    }
    
    setButtonsVisible(props, btnsVisibleObj);
}
export { renderButtions }
function getCookie(name) {
    var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
    if (arr = document.cookie.match(reg))
        return unescape(arr[2]);
    else
        return "";
}
export { getCookie }
function getRequest1(url) {
    if (!url)
        url = document.location.search;
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substring(url.indexOf("?") + 1);
        var strs = str.split("&");
        for (var i = 0; i < strs.length; i++) {
            theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
        }
    }
    return theRequest;
}
export { getRequest1 }
function getRequestString(name) {
    if (getRequest1()[name] != undefined) {
        return getRequest1()[name];
    }
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}
function dataHandle(head, props, pagestatus) {
    //处理数据和按钮状态
    if (head == null || head.rows[0] == null || head.rows[0].values == null)
        head = props.form.getAllFormValue("head");
    let sxbz = head.rows[0].values.sxbz.value;//单据生效状态
    let spzt = head.rows[0].values.spzt.value;//审批状态
    //let djzt = head.rows[0].values.djzt.value//单据状态
    let djzt = 1;//单据状态
    let iscostshare = head.rows[0].values.iscostshare.value == null || head.rows[0].values.iscostshare.value == undefined ? false : head.rows[0].values.iscostshare.value;
    let btnsVisibleObj = {};
    let pageStatus = pagestatus || window.presetVar.pagestatus;
    let isAddOrEditStatus = pageStatus == statusVar.edit || pageStatus == statusVar.add;
    let isBrowseStatus = pageStatus == statusVar.browse;
    renderButtions.call(this, props, props.meta.getMeta(), window.presetVar.buttonTpl.button, pageStatus);
    let scene = props.getUrlParam('scene');//第三方打开
    let deal = props.getUrlParam('deal');
    if (isAddOrEditStatus) {
        btnsVisibleObj["pageEdit"] = false;//修改--组buttongroup1
        btnsVisibleObj["pageCopy"] = false;//复制--组buttongroup1
        btnsVisibleObj["pageDel"] = false;//删除 --组buttongroup1
        btnsVisibleObj["billRecall"] = false;//收回 --组buttongroup2
        btnsVisibleObj["pageSave"] = true;//保存 --组buttongroup2  
        btnsVisibleObj["pageTempSave"] = true;//暂存 --组buttongroup2
        //审批人门户和作业任务处理修改单据无提交按钮
        if (scene == 'approve' || scene == 'approvesce' || scene == 'zycl') {
            btnsVisibleObj["billSubmit"] = false;//提交 --组buttongroup2            
        } else {
            // add by yts 去除编辑态的保存提交按钮
            btnsVisibleObj["billSubmit"] = false;//提交 --组buttongroup2            
        }
        btnsVisibleObj["buttongroup2"] = true;
        btnsVisibleObj["imageUpload"] = false;//影像扫描 --组buttongroup3
        btnsVisibleObj["imageShow"] = false;//影像查看 --组buttongroup3
        //buttongroup6更多
        //buttongroup6-c联查
        //buttongroup6-o其他
        btnsVisibleObj["invoiceView"] = false;//联查发票buttongroup6-c
        btnsVisibleObj["linkQueryFysq"] = false;//联查费用申请单 --组buttongroup6-c
        btnsVisibleObj["linkQueryFyyt"] = false;//联查费用预提单 --组buttongroup6-c
        btnsVisibleObj["linkQueryJkbx"] = false;//联查借款单 --组buttongroup6-c
        btnsVisibleObj["billApprove"] = false;//联查审批情况 --组buttongroup6-c
        btnsVisibleObj["buttongroup6-c"] = false;//编辑态联查组不显示
        btnsVisibleObj["pagePrint"] = false;//打印  --组buttongroup6-o 
        btnsVisibleObj["invoiceDzfp"] = false;//电子发票--组buttongroup6-o 
        btnsVisibleObj["billInvalid"] = false;//作废--组buttongroup6-o 
        btnsVisibleObj["buttongroup6-o"] = false;//编辑态其他组不显示
        btnsVisibleObj["buttongroup6"] = false;//编辑态更多不显示
        btnsVisibleObj["pageClose"] = true;//取消 编辑态显示取消
        btnsVisibleObj["pageCjk"] = true;//冲借款
        btnsVisibleObj["pageFyyt"] = true;//核销预提
        btnsVisibleObj["pageFpcy"] = true;//发票查验
        btnsVisibleObj["pageFpdr"] = true;//发票导入
        btnsVisibleObj["fileManager"] = true;//附件
        btnsVisibleObj["LinkBudget"] = false;//联查预算
        btnsVisibleObj["LinkVoucher"] = false;//联查凭证 
        btnsVisibleObj['ztd']=false;//粘贴单
    } else {
        if(djzt == "-1") {//作废
            for (let btnKey in pageButton) {
                if (btnKey == pageButton.pageCopy || btnKey == pageButton.imageShow || btnKey == pageButton.pagePrint) {
                    btnsVisibleObj[btnKey] = true;
                } else {
                    btnsVisibleObj[btnKey] = false;
                }

            }
        }else if ((scene == null || scene == undefined || scene == 'bz')) {//制单页面，报账进度查询
            //单据未提交 
            if (spzt == -1 || spzt == null || spzt == 0) {
                btnsVisibleObj["pageEdit"] = true;//修改--组buttongroup1
                btnsVisibleObj["pageDel"] = true;//删除 --组buttongroup1
                btnsVisibleObj["billSubmit"] = true;//提交 --组buttongroup2
                btnsVisibleObj["billInvalid"] = true;//作废--组buttongroup6-o 
                btnsVisibleObj["LinkVoucher"] = false;//联查凭证    
            } else {
                //单据已经提交
                btnsVisibleObj['ztd']=true;
                btnsVisibleObj["pageEdit"] = false;//修改--组buttongroup1   
                btnsVisibleObj["pageDel"] = false;//删除 --组buttongroup1 
                btnsVisibleObj["billSubmit"] = false;//提交 --组buttongroup2
                btnsVisibleObj["billInvalid"] = false;//作废--组buttongroup6-o   
                btnsVisibleObj["LinkVoucher"] = true;//联查凭证
            }
            btnsVisibleObj["pageCopy"] = true;//复制--组buttongroup1
            if (spzt == -1 || spzt == 0 || spzt == null || spzt == 1) {
                btnsVisibleObj["billRecall"] = false;//收回 --组buttongroup2                
            } else {
                btnsVisibleObj["billRecall"] = true;//收回 --组buttongroup2                
            }
            //审批通过后隐藏电子发票按钮
            if (spzt == 1) {
                btnsVisibleObj["invoiceDzfp"] = false;//电子发票--组buttongroup6-o
            } else {
                btnsVisibleObj["invoiceDzfp"] = true;//电子发票--组buttongroup6-o
            }
            btnsVisibleObj["pageSave"] = false;//保存 --组buttongroup2   
            btnsVisibleObj["pageFpdr"] = false;//发票导入 --组buttongroup2   
            btnsVisibleObj["pageFpcy"] = false;//发票查验 --组buttongroup2  
            btnsVisibleObj["pageTempSave"] = false;//暂存 --组buttongroup2  
            btnsVisibleObj["imageUpload"] = true;//影像扫描 --组buttongroup3
            btnsVisibleObj["imageShow"] = true;//影像查看 --组buttongroup3
            //buttongroup6更多
            //buttongroup6-c联查
            //buttongroup6-o其他
            btnsVisibleObj["invoiceView"] = true;//联查发票buttongroup6-c
            btnsVisibleObj["linkQueryFysq"] = true;//联查费用申请单 --组buttongroup6-c
            btnsVisibleObj["linkQueryFyyt"] = true;//联查费用预提单 --组buttongroup6-c
            btnsVisibleObj["linkQueryJkbx"] = true;//联查借款单 --组buttongroup6-c
            btnsVisibleObj["billApprove"] = true;//联查审批情况 --组buttongroup6-c
            btnsVisibleObj["buttongroup6-c"] = true;//编辑态联查组不显示
            btnsVisibleObj["pagePrint"] = true;//打印  --组buttongroup6-o 
            // btnsVisibleObj["invoiceDzfp"] = true;//电子发票--组buttongroup6-o 
            btnsVisibleObj["buttongroup6-o"] = true;//其他组显示
            btnsVisibleObj["buttongroup6"] = true;//更多显示
            btnsVisibleObj["pageClose"] = false;//取消 显示
            btnsVisibleObj["pageCjk"] = false;//冲借款
            btnsVisibleObj["pageFyyt"] = false;//核销预提
            btnsVisibleObj["fileManager"] = true;//附件
            btnsVisibleObj["LinkBudget"] = false;//联查预算
            btnsVisibleObj["LinkVoucher"] = false;//联查凭证
        }
        //审批人门户和作业任务处理
        else if ((scene == 'approve' || scene == 'approvesce' || scene == 'zycl' || scene == 'approvesce')) {
            if (spzt != 1) {
                btnsVisibleObj["pageEdit"] = true;//修改--组buttongroup1
                btnsVisibleObj["invoiceDzfp"] = true;//电子发票--组buttongroup6-o                
            } else {
                btnsVisibleObj["pageEdit"] = false;//修改--组buttongroup1
                btnsVisibleObj["invoiceDzfp"] = false;//电子发票--组buttongroup6-o                
            } 
            btnsVisibleObj["pageCopy"] = false;//复制--组buttongroup1
            btnsVisibleObj["pageDel"] = false;//删除 --组buttongroup1
            btnsVisibleObj["billRecall"] = false;//收回 --组buttongroup2
            btnsVisibleObj["pageFpdr"] = false;//发票导入 --组buttongroup2   
            btnsVisibleObj["pageTempSave"] = false;//暂存 --组buttongroup2 
            btnsVisibleObj["pageFpcy"] = false;//发票查验 --组buttongroup2
            btnsVisibleObj["pageFpcy"] = false;//保存 --组buttongroup2  
            btnsVisibleObj["billSubmit"] = false;//提交 --组buttongroup2
            btnsVisibleObj["buttongroup2"] = false;
            btnsVisibleObj["imageUpload"] = true;//影像扫描 --组buttongroup3
            btnsVisibleObj["imageShow"] = true;//影像查看 --组buttongroup3
            //buttongroup6更多
            //buttongroup6-c联查
            //buttongroup6-o其他
            btnsVisibleObj["invoiceView"] = true;//联查发票buttongroup6-c
            btnsVisibleObj["linkQueryFysq"] = true;//联查费用申请单 --组buttongroup6-c
            btnsVisibleObj["linkMYApprove"] = true;//联查明源
            btnsVisibleObj["linkQueryFyyt"] = true;//联查费用预提单 --组buttongroup6-c
            btnsVisibleObj["linkQueryJkbx"] = true;//联查借款单 --组buttongroup6-c
            btnsVisibleObj["billApprove"] = true;//联查审批情况 --组buttongroup6-c
            btnsVisibleObj["buttongroup6-c"] = true;//编辑态联查组不显示
            btnsVisibleObj["pagePrint"] = true;//打印  --组buttongroup6-o 
            // btnsVisibleObj["invoiceDzfp"] = true;//电子发票--组buttongroup6-o 
            btnsVisibleObj["billInvalid"] = false;//作废--组buttongroup6-o 
            btnsVisibleObj["buttongroup6-o"] = true;//编辑态其他组不显示
            btnsVisibleObj["buttongroup6"] = true;//编辑态更多不显示
            btnsVisibleObj["pageClose"] = false;//取消 编辑态显示取消
            btnsVisibleObj["pageCjk"] = false;//冲借款
            btnsVisibleObj["pageFyyt"] = false;//核销预提
            btnsVisibleObj["fileManager"] = true;//附件
            btnsVisibleObj["LinkBudget"] = true;//联查预算
            btnsVisibleObj["LinkVoucher"] = true;//联查凭证                
        } //报账单据查询只显示联查相关信息
        else if (scene == 'bzcx' ||scene=='lc') {//报账查询场景过来的单子
            if (spzt == 1) {
                btnsVisibleObj["invoiceDzfp"] = false;//电子发票--组buttongroup6-o
            } else {
                btnsVisibleObj["invoiceDzfp"] = true;//电子发票--组buttongroup6-o
            }
            btnsVisibleObj["pageEdit"] = false;//修改--组buttongroup1
            btnsVisibleObj["pageCopy"] = false;//复制--组buttongroup1
            btnsVisibleObj["pageDel"] = false;//删除 --组buttongroup1
            btnsVisibleObj["billRecall"] = false;//收回 --组buttongroup2
            btnsVisibleObj["pageFpdr"] = false;//发票导入 --组buttongroup2 
            btnsVisibleObj["pageTempSave"] = false;//暂存 --组buttongroup2    
            btnsVisibleObj["pageFpcy"] = false;//发票查验 --组buttongroup2  
            btnsVisibleObj["pageFpcy"] = false;//发票查验 --组buttongroup2 
            btnsVisibleObj["billSubmit"] = false;//提交 --组buttongroup2
            btnsVisibleObj["buttongroup2"] = false;
            btnsVisibleObj["imageUpload"] = false;//影像扫描 --组buttongroup3
            btnsVisibleObj["imageShow"] = true;//影像查看 --组buttongroup3
            //buttongroup6更多
            //buttongroup6-c联查
            //buttongroup6-o其他
            btnsVisibleObj["invoiceView"] = true;//联查发票buttongroup6-c
            btnsVisibleObj["linkQueryFysq"] = true;//联查费用申请单 --组buttongroup6-c
            btnsVisibleObj['linkMYApprove'] = true;//联查明源审批详情
            btnsVisibleObj["linkQueryFyyt"] = true;//联查费用预提单 --组buttongroup6-c
            btnsVisibleObj["linkQueryJkbx"] = true;//联查借款单 --组buttongroup6-c
            btnsVisibleObj["billApprove"] = true;//联查审批情况 --组buttongroup6-c
            btnsVisibleObj["buttongroup6-c"] = true;//编辑态联查组不显示
            btnsVisibleObj["pagePrint"] = true;//打印  --组buttongroup6-o
            // btnsVisibleObj["invoiceDzfp"] = true;//电子发票--组buttongroup6-o
            btnsVisibleObj["billInvalid"] = false;//作废--组buttongroup6-o
            btnsVisibleObj["buttongroup6-o"] = true;//编辑态其他组不显示
            btnsVisibleObj["buttongroup6"] = true;//编辑态更多不显示
            btnsVisibleObj["pageClose"] = false;//取消 编辑态显示取消
            btnsVisibleObj["pageCjk"] = false;//冲借款
            btnsVisibleObj["pageFyyt"] = false;//核销预提
            btnsVisibleObj["fileManager"] = true;//附件
            btnsVisibleObj["LinkBudget"] = false;//联查预算
            btnsVisibleObj["LinkVoucher"] = false;//联查凭证
        }
        //费用报销查询，作业管理 只显示联查相关信息
        else if (scene == 'fycx' || scene == 'zycx') {
            if (spzt == 1) {
                btnsVisibleObj["invoiceDzfp"] = false;//电子发票--组buttongroup6-o
            } else {
                btnsVisibleObj["invoiceDzfp"] = true;//电子发票--组buttongroup6-o
            }
            btnsVisibleObj["pageEdit"] = false;//修改--组buttongroup1
            btnsVisibleObj["pageCopy"] = false;//复制--组buttongroup1
            btnsVisibleObj["pageDel"] = false;//删除 --组buttongroup1
            btnsVisibleObj["billRecall"] = false;//收回 --组buttongroup2
            btnsVisibleObj["pageSave"] = false;//保存 --组buttongroup2  
            btnsVisibleObj["pageFpdr"] = false;//发票导入 --组buttongroup2  
            btnsVisibleObj["pageTempSave"] = false;//暂存 --组buttongroup2   
            btnsVisibleObj["pageFpcy"] = false;//发票查验 --组buttongroup2   
            btnsVisibleObj["billSubmit"] = false;//提交 --组buttongroup2
            btnsVisibleObj["buttongroup2"] = false;
            btnsVisibleObj["imageUpload"] = false;//影像扫描 --组buttongroup3
            btnsVisibleObj["imageShow"] = true;//影像查看 --组buttongroup3
            //buttongroup6更多
            //buttongroup6-c联查
            //buttongroup6-o其他
            btnsVisibleObj["invoiceView"] = true;//联查发票buttongroup6-c
            btnsVisibleObj["linkQueryFysq"] = true;//联查费用申请单 --组buttongroup6-c
            btnsVisibleObj["linkQueryFyyt"] = true;//联查费用预提单 --组buttongroup6-c
            btnsVisibleObj["linkQueryJkbx"] = true;//联查借款单 --组buttongroup6-c
            btnsVisibleObj["billApprove"] = true;//联查审批情况 --组buttongroup6-c
            btnsVisibleObj["buttongroup6-c"] = true;//编辑态联查组不显示
            btnsVisibleObj["pagePrint"] = true;//打印  --组buttongroup6-o 
            // btnsVisibleObj["invoiceDzfp"] = true;//电子发票--组buttongroup6-o 
            btnsVisibleObj["billInvalid"] = false;//作废--组buttongroup6-o 
            btnsVisibleObj["buttongroup6-o"] = true;//编辑态其他组不显示
            btnsVisibleObj["buttongroup6"] = true;//编辑态更多不显示
            btnsVisibleObj["pageClose"] = false;//取消 编辑态显示取消
            btnsVisibleObj["pageCjk"] = false;//冲借款
            btnsVisibleObj["pageFyyt"] = false;//核销预提
            btnsVisibleObj["fileManager"] = true;//附件
            btnsVisibleObj["LinkBudget"] = true;//联查预算
            btnsVisibleObj["LinkVoucher"] = true;//联查凭证     
            btnsVisibleObj["linkMYApprove"] = true;//联查明源
        } //消息中心打开单据
        else if (scene == 'notice') {
            if (spzt == 1) {
                btnsVisibleObj["invoiceDzfp"] = false;//电子发票--组buttongroup6-o
            } else {
                btnsVisibleObj["invoiceDzfp"] = true;//电子发票--组buttongroup6-o
            }
            btnsVisibleObj["pageEdit"] = false;//修改--组buttongroup1
            btnsVisibleObj["pageCopy"] = false;//复制--组buttongroup1
            btnsVisibleObj["pageDel"] = false;//删除 --组buttongroup1
            btnsVisibleObj["billRecall"] = false;//收回 --组buttongroup2
            btnsVisibleObj["pageSave"] = false;//保存 --组buttongroup2  
            btnsVisibleObj["pageFpdr"] = false;//发票导入 --组buttongroup2  
            btnsVisibleObj["pageTempSave"] = false;//暂存 --组buttongroup2  
            btnsVisibleObj["pageFpcy"] = false;//发票查验 --组buttongroup2   
            btnsVisibleObj["billSubmit"] = false;//提交 --组buttongroup2
            btnsVisibleObj["buttongroup2"] = false;
            btnsVisibleObj["imageUpload"] = false;//影像扫描 --组buttongroup3
            btnsVisibleObj["imageShow"] = false;//影像查看 --组buttongroup3
            //buttongroup6更多
            //buttongroup6-c联查
            //buttongroup6-o其他
            btnsVisibleObj["invoiceView"] = false;//联查发票buttongroup6-c
            btnsVisibleObj["linkQueryFysq"] = false;//联查费用申请单 --组buttongroup6-c
            btnsVisibleObj["linkQueryFyyt"] = false;//联查费用预提单 --组buttongroup6-c
            btnsVisibleObj["linkQueryJkbx"] = false;//联查借款单 --组buttongroup6-c
            btnsVisibleObj["billApprove"] = false;//联查审批情况 --组buttongroup6-c
            btnsVisibleObj["buttongroup6-c"] = false;//编辑态联查组不显示
            btnsVisibleObj["pagePrint"] = false;//打印  --组buttongroup6-o 
            // btnsVisibleObj["invoiceDzfp"] = true;//电子发票--组buttongroup6-o 
            btnsVisibleObj["billInvalid"] = false;//作废--组buttongroup6-o 
            btnsVisibleObj["buttongroup6-o"] = false;//编辑态其他组不显示
            btnsVisibleObj["buttongroup6"] = false;//编辑态更多不显示
            btnsVisibleObj["pageClose"] = false;//取消 编辑态显示取消
            btnsVisibleObj["pageCjk"] = false;//冲借款
            btnsVisibleObj["pageFyyt"] = false;//核销预提
            btnsVisibleObj["fileManager"] = false;//附件
            btnsVisibleObj["LinkBudget"] = false;//联查预算
            btnsVisibleObj["LinkVoucher"] = false;//联查凭证     
        }else if(scene=='1c'){
            btnsVisibleObj["imageShow"] = true;
            btnsVisibleObj["fileManager"] = true;
            btnsVisibleObj["pagePrint"] = true;
            btnsVisibleObj["buttongroup6"] = true;
        }

    }
    if (deal != undefined && (deal == 'handon' || deal == 'adjust' || deal == 'sscreject' || deal == 'handled')) {
        btnsVisibleObj["pageEdit"] = false;
    }
    //来源于报表联查的单据
    if (scene == 'sscermlink' || scene == 'linksce') {
        btnsVisibleObj["invoiceView"] = true;//联查发票buttongroup6-c
        btnsVisibleObj["linkQueryFysq"] = true;//联查费用申请单 --组buttongroup6-c
        btnsVisibleObj["linkQueryFyyt"] = true;//联查费用预提单 --组buttongroup6-c
        btnsVisibleObj["linkQueryJkbx"] = true;//联查借款单 --组buttongroup6-c
        btnsVisibleObj["buttongroup6-c"] = true;//编辑态联查组不显示
        btnsVisibleObj["fileManager"] = true;//附件
        btnsVisibleObj["LinkBudget"] = true;//联查预算
        btnsVisibleObj["LinkVoucher"] = true;//联查凭证  
        btnsVisibleObj["billApprove"] = true;//联查审批情况
    }

    // props.button.setButtonsVisible(btnsVisibleObj);
    setButtonsVisible(props, btnsVisibleObj);
}
export { dataHandle }
//路由变化监听函数，location.hash及linkTo,pushTo都会调用
function routeChange(props) {
    let pageStatus = props.getUrlParam('status');
    window.presetVar.pagestatus = pageStatus;
    let meta = props.meta.getMeta();
    loadbill.call(this, meta, pageStatus);
}
export { routeChange }

function modifierMeta(props, meta) {
    let formId='head';
    meta[formId].items.map((item) => {
        if (item.attrcode == 'custaccount') {
            item.refName = '客商银行账户';/* 国际化处理： 客商银行账户*/
        }
    });
    if(meta['JNZF_head'] !== undefined){
        meta['JNZF_head'].items.map((item) => {
            if (item.attrcode == 'custaccount') {
                item.refName = '客商银行账户';/* 国际化处理： 客商银行账户*/
            }
        });
    }
    
    return meta;
}