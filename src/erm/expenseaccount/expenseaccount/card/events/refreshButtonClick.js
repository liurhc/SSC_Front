import {render, getCookie, dataHandle} from '../initTemplate'
import requestApi from '../requestApi';
import 'ssccommon/components/globalPresetVar';
import logs from 'ssccommon/utils/logs';
import { debuglog } from 'util';
import {setPageStatus} from 'ssccommon/utils/statusController';
import { cardCache,toast,base } from 'nc-lightapp-front';
const {NCMessage} = base;
let { updateCache } = cardCache;
let debugLogs = new logs();
export default function () {
    let props = this.props;
    let _this = this;
    let pageStatus = window.presetVar.pagestatus==''||window.presetVar.pagestatus==undefined?props.getUrlParam('status'):window.presetVar.pagestatus;
    let appcode = (props.getUrlParam("appcode")==undefined||props.getUrlParam("appcode")=='')?props.getSearchParam("c"):props.getUrlParam("appcode");
    let pagecode =  (props.getUrlParam("pagecode")==undefined||props.getUrlParam("pagecode")=='')?props.getSearchParam("p"):props.getUrlParam("pagecode");
    render.call(this, props.meta.getMeta(),window.presetVar.buttonTpl,pageStatus);
    let viewbilldata = {};
    viewbilldata.openbillid = props.getUrlParam("id")||props.getUrlParam("billid");
    viewbilldata.tradetype = props.getUrlParam("tradetype")||window.presetVar.tradetype;
    viewbilldata.uid = getCookie("userid");//"1001Z3100000000000AE";这里以后从session中获取
    viewbilldata.pagecode = pagecode;
    viewbilldata.appcode = appcode;
    requestApi.viewBill({
        data:viewbilldata,
        success:(data) =>{
            props.form.setAllFormValue({["head"]:data.head["head"]});
            window.presetVar.reloadHead = data.head["head"];
            //设置表体数据
            Object.values(window.presetVar.body).forEach((body) =>{
                if(data.bodys[body]){
                    debugLogs.timeStart('setTableData_'+body);
                    props.cardTable.setTableData(body, data.bodys[body]);
                    debugLogs.timeEnd('setTableData_'+body);
                }
            });
            dataHandle(window.presetVar.reloadHead,props,pageStatus);

            let ls_bodys = [];
            Object.values(window.presetVar.body).forEach((body)=>{
                ls_bodys.push(body);
            });
            setPageStatus(props, props.meta.getMeta(), ['head'], ls_bodys, 'browse');
            // BTM 整体设置页面状态
            //}pa
            let iscostshare = props.form.getFormItemsValue("head",['iscostshare'])==null?"":props.form.getFormItemsValue("head",['iscostshare'])[0].value
            if(iscostshare== true){
                _this.setState({hideTables:[]})
            }else{
                _this.setState({hideTables:['er_cshare_detail']})
            }
            
            // "冲销明细"、"核销预提明细"页签没有数据时隐藏 -start
            var bxcontrastData = props.cardTable.getAllRows('er_bxcontrast'); 
            var accruedData = props.cardTable.getAllRows('accrued_verify'); 
            _this.setState((prevState)=>{
                var hideTables = prevState.hideTables;
                if(!bxcontrastData || bxcontrastData.length==0){
                    hideTables.push('er_bxcontrast');// 隐藏冲销明细页签
                }
                if(!accruedData || accruedData.length==0){
                    hideTables.push('accrued_verify');// 隐藏核销预提明细页签
                }
                return {hideTables:hideTables};
            }); // "冲销明细"、"核销预提明细"页签没有数据时隐藏 -end

            requestApi.setMetasInit(props.meta.getMeta(),props,data);
            if(data.userjson && JSON.parse(data.userjson)){
                let djlx = JSON.parse(data.userjson);
                if(djlx && djlx.ISCONTRAST && djlx.jkyjye && djlx.jkyjye >0){//是否冲借款
                    window.presetVar["ISCONTRAST"]=djlx.ISCONTRAST;
                }
                if(djlx && djlx.IS_MACTRL){//是否冲借款
                    window.presetVar["IS_MACTRL"]=djlx.IS_MACTRL;
                }
                _this.pubMessage.refreshSuccess();

            
               
            }
        }
    });
}
