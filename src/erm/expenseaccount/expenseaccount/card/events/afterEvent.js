import { base, promptBox, toast } from 'nc-lightapp-front';
import requestApi from '../requestApi';
import { updataFormData, updatacardTableData } from 'ssccommon/utils/changePageData';
import { getExcelImportConfig } from "../ExcelImportUtil";
import { beforeUpdatePage, updatePage } from 'ssccommon/utils/updatePage';


export default function afterEvent(props, moduleId, key, value, changedrows, index, attr) {

    let paramdata = {};
    let moduleid = moduleId == "head" ? "headform" : moduleId;
    let pagecode = window.presetVar.pagecode;
    let bodys = window.presetVar.lsBodys;
    let bodyarr = [];
    let hideTable = this.state.hideTables;
    let _this = this;
    bodys.map((one) => {
        if (hideTable.indexOf(one) < 0) {
            bodyarr[bodyarr.length] = one;
        }
    })

    paramdata = {
        "value": value,
        "tableid": moduleid,
        "field": key,
        "uistate": window.presetVar.pagestatus,
        "tradetype": window.presetVar.tradetype,
        "oldvalue": '',
        "pagecode": pagecode,
        "appcode": window.presetVar.appcode
    }
    if (moduleid != "headform") {
        paramdata.changeRowId = changedrows[0].rowid;
    } else {
        paramdata.value = value.value;
    }

    switch (moduleId) {
        case 'head':
            switch (key) {
                case 'pk_org'://表头财务组织
                    if (key == 'pk_org') {
                        props.cardTable.setTableData("other", { rows: [] });
                        props.cardTable.setTableData('er_bxcontrast',{rows:[]},null, false);
                        props.cardTable.setTableData('accrued_verify',{rows:[]},null, false);
                        let hideTables =this.state.hideTables;
                        if(hideTables.findIndex(item=>item==='er_bxcontrast')<0){//隐藏冲销明细
                            debugger
                            hideTables.push('er_bxcontrast')
                        }
                        if(hideTables.findIndex(item=>item==='accrued_verify')<0){//隐藏预提明细
                            debugger
                            hideTables.push('accrued_verify')
                        }
                        this.setState({hideTables:hideTables });
                    }
                    let formHead = props.form.getAllFormValue("head");
                    
                    if(formHead && formHead.rows && formHead.rows[0] && formHead.rows[0].values){
                        let pkOrg = '';
                        let bzBm = '';
                        if(formHead.rows[0].values.pk_org && formHead.rows[0].values.pk_org.value){
                            pkOrg = formHead.rows[0].values.pk_org.value;
                        }
                        if(formHead.rows[0].values.bzbm && formHead.rows[0].values.bzbm.value){
                            bzBm = formHead.rows[0].values.bzbm.value;
                        }
                        requestApi.getFkyhzh({
                            data:{
                                pk_org:pkOrg,
                                bzbm:bzBm
                            },
                            success: (rsp) => {
                                formHead.rows[0].values.fkyhzh.value = rsp.data.pk_bankaccsub; 
                                formHead.rows[0].values.fkyhzh.display = rsp.data.subcode; 
                                formHead.areacode = 'head';
                                props.form.setAllFormValue({ ['head']: formHead });
                            }
                        })
                    }
                case 'pk_org_v'://表头财务组织
                    if (value == undefined || value.value == null) {
                        break;
                    }
                case 'djrq'://表头单据日期
                case 'bzbm'://表头币种
                case 'bbhl'://表头本币汇率
                case 'dwbm_v'://表头借款报销人单位版本
                case 'dwbm'://表头借款报销人单位
                case 'fydwbm_v'://表头费用承担单位版本
                case 'fydwbm'://表头费用承担单位
                case 'pk_payorg_v'://表头支付单位版本
                case 'pk_payorg'://表头支付单位
                case 'deptid_v':// 表头报销人部门版本
                case 'deptid':// 表头报销人部门
                case 'fydeptid_v':// 表头费用承担部门版本
                case 'fydeptid':// 表头费用承担部门
                case "jkbxr"://借款报销人
                case "receiver"://表头收款人
                case "szxmid"://表头收支项目
                    if (key == 'szxmid') {
                        props.cardTable.setTableData("other", { rows: [] })
                    }
                case "skyhzh"://表头个人银行账户
                case "jobid"://表头项目
                case "freecust"://散户
                case "custaccount"://表头客商银行账户
                case "paytarget"://表头收款对象
                    if (key == 'paytarget') {
                        if (value.value == "0") {
                            props.form.setFormItemsDisabled("head", { 'hbbm': true, 'customer': true, "custaccount": true, 'receiver': false, 'custaccount': true, 'freecust': true });
                        } else if (value.value == "1") {
                            props.form.setFormItemsDisabled("head", { 'receiver': true, 'customer': true, 'hbbm': false, 'custaccount': false, 'freecust': false });
                        } else if (value.value == "2") {
                            props.form.setFormItemsDisabled("head", { 'hbbm': true, 'receiver': true, 'customer': false, 'custaccount': false, 'freecust': true });
                        }
                    }
                    if (key == 'szxmid') {
                        this.props.cardTable.setTableData('other', { rows: [] })
                    }
                case "assume_amount"://分摊页签表体承担金额编辑后事件
                case "assume_org"://分摊页签表体承担单位编辑后事件
                case "hbbm"://表头供应商
                case "customer"://表头客户
                case "zyx2": //开会地点(会务费报销单)

                    //表头修改后数据后台需要的结构， 一主一子  一主多子
                    let billdata = props.createHeadAfterEventData('20110ETEA_2641_IWEB', "head", bodyarr, moduleId, key, value);
                    console.log('表头字段change后表头标题数据结构：', billdata)
                    // let send_headdata = {billdata,paramdata};
                    billdata.card.head.userjson = JSON.stringify(paramdata);
                    billdata.templetid = props.meta.getMeta().pageid;
                    // send_headdata.templateid = props.meta.getMeta().pageid;
debugger
                    requestApi.valueChange({
                        data: billdata.card,
                        success: (data) => {
                            //设置表头数据
                            // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                            // props.form.setAllFormValue({[presetVar.head.head1]: requestApi.filterDatas(data.head[presetVar.head.head1])});
                            updataFormData(props, presetVar.head.head1, requestApi.filterDatas(data.head[presetVar.head.head1]));
                            // BTM 性能优化，变更页面数据更新方法
                            window.presetVar.reloadHead = requestApi.filterDatas(data.head[presetVar.head.head1]);
                            let org_currinfo = props.form.getFormItemsValue(presetVar.head.head1, 'bbhl');
                            //add 境外支付切换收款对象为员工时一些字段不自动带出
                            if (key == "paytarget") {
                                if (value.value == 0) {
                                    if (props.meta.getMeta()['JWZF_head'].items.length > 0 && props.meta.getMeta()['JNZF_head'].items.length == 0) {
                                        props.form.setFormItemsValue(presetVar.head.head1, { 'zyx20': "" })    //个人银行账号
                                        props.form.setFormItemsValue(presetVar.head.head1, { 'zyx23': "" })    //银行账户名称
                                        props.form.setFormItemsValue(presetVar.head.head1, { 'zyx25': "" })    //收款方CNAPS号
                                        props.form.setFormItemsValue(presetVar.head.head1, { 'zyx27': "" })    //开户银行
                                        props.form.setFormItemsValue(presetVar.head.head1, { 'zyx28': "" })    //收款方汇入市
                                        props.form.setFormItemsValue(presetVar.head.head1, { 'zyx29': "" })    //收款方汇入省
                                    }
                                }
                            }
                            //end
                            if (key == 'bzbm' && org_currinfo.value != undefined && org_currinfo.value != null && parseFloat(org_currinfo.value) == parseFloat(1)) {
                                props.form.setFormItemsDisabled(presetVar.head.head1, { 'bbhl': true });
                            } else {
                                props.form.setFormItemsDisabled(presetVar.head.head1, { 'bbhl': false });
                            }
                            //设置表体数据
                            bodyarr.forEach((item) => {
                                // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                                // props.cardTable.setTableData(item, requestApi.filterDatas(data.bodys[item]));
                                updatacardTableData(props, item, requestApi.filterDatas(data.bodys[item]));
                                // BTM 性能优化，变更页面数据更新方法
                            })
                            //表头值变化后为表体元数据赋值
                            let headrows = data.head[presetVar.head.head1].rows[0].values;
                            // let headtrr = ['pk_org','szxmid','pk_pcorg','pk_pcorg_v','pk_checkele','jobid','projecttask','pk_resacostcenter','pk_proline','pk_brand','fctno','bzbm','tax_rate','bbhl','groupbbhl','globalbbhl','receiver','freecust','jkbxr','dwbm','skyhzh','custaccount','customer','deptid','paytarget','hbbm'];
                            // let cshare_detail = ['assume_org','assume_dept','pk_iobsclass'];//'fydwbm','fydeptid',
                            let metas = props.meta.getMeta();
                        //     if (key == 'pk_org'){
                        //         metas['er_bxcontrast']='';
                        //        props.meta.setMeta(metas);
                        //    }
                            //Object.keys(presetVar.body).forEach((bodyindex) =>{
                            requestApi.setbodyinitvalue(metas, headrows);
                            // requestApi.handleLoadDataResDada(props.meta.getMeta(), {data},props,moduleid);
                        }
                    })
                    break;
                case "isexpamt"://待摊
                    if (value.value == true) {
                        let accrued_verify = props.cardTable.getAllRows("accrued_verify");
                        if (accrued_verify != null && accrued_verify.length > 0) {
                            props.form.setFormItemsValue('head', { 'isexpamt': { value: false, display: null } })
                            toast({ content: this.multiLangJson['201102BCLF-0051'],/*"报销单已经核销预提，不可进行待摊!",*/color: 'warning' });
                        } else {
                            props.form.setFormItemsVisible('head', { "start_period": true, "total_period": true });
                            props.form.setFormItemsDisabled('head', { 'start_period': false, 'total_period': false });
                        }

                    } else {
                        promptBox({
                            color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                            content: this.multiLangJson['201102BCLF-0052'],//'即将取消标志"待摊",是否继续？' ,            // 提示内容,非必输
                            noFooter: false,                // 是否显示底部按钮(确定、取消),默认显示(false),非必输
                            noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
                            beSureBtnName: this.multiLangJson['2011-0003'],//"确定",          // 确定按钮名称, 默认为"确定",非必输
                            cancelBtnName: this.multiLangJson['2011-0002'],//"取消",          // 取消按钮名称, 默认为"取消",非必输
                            beSureBtnClick: function () {
                                props.form.setFormItemsDisabled('head', { "start_period": true, "total_period": true });
                                props.form.setFormItemsVisible('head', { "start_period": false, "total_period": false });
                            },
                            cancelBtnClick: function () {
                                props.form.setFormItemsValue('head', { 'isexpamt': { value: true, display: null } })
                            }
                        })

                    }
                    break;
                case "iscostshare"://分摊
                    if (value.value == true) {
                        let accrued_verify = props.cardTable.getAllRows("accrued_verify");
                        if (accrued_verify != null && accrued_verify.length > 0) {
                            props.form.setFormItemsValue('head', { 'iscostshare': { value: false } });
                            toast({ content: this.multiLangJson['201102BCLF-0049'],/*"报销单已经核销预提，不可进行分摊!",*/color: 'warning' });

                        } else {
                            setImportConfig(this);//设置导入按钮
                            _this.setState({ hideTables: [] })
                        }
                    } else {
                        promptBox({
                            color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                            title: this.multiLangJson['201101BXBJ-0008'],//"提示信息",                // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输
                            content: this.multiLangJson['201102BCLF-0014'],//'即将删除费用明细数据，是否继续？' ,            // 提示内容,非必输
                            noFooter: false,                // 是否显示底部按钮(确定、取消),默认显示(false),非必输
                            noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
                            beSureBtnName: this.multiLangJson['2011-0003'],//"确定",          // 确定按钮名称, 默认为"确定",非必输
                            cancelBtnName: this.multiLangJson['2011-0002'],//"取消",          // 取消按钮名称, 默认为"取消",非必输
                            beSureBtnClick: function () {
                                _this.setState({ hideTables: ['er_cshare_detail'] })
                            },
                            cancelBtnClick: function () {
                                props.form.setFormItemsValue('head', { 'iscostshare': { value: true, display: null } });
                            }
                        })


                    }
                    break;

                case 'zyx19': // 境外支付消息（自定义项19）-20190905
                    var meta = _this.props.meta.getMeta();
                    if (!(meta['JWZF_head'] && meta['JNZF_head'])) { // 模板没有境内/外区域时不执行此方法
                        break;
                    }
                    if (value.value == true) { // 勾选 "是"
                        // 显示境外区域
                        meta['JWZF_head'].items = this.state.items['JWZF_head'] ? this.state.items['JWZF_head'] : [];
                        //多表头参照过滤
                        meta['JWZF_head'].items.forEach((item) => {
                            // if (item.attrcode == attrCode && attrCode.indexOf('.')<0) {
                            // item.initialvalue = initialvalue;
                            // TOP 性能优化，不显示的数据不用添加参照过滤条件 chengwbc MOD
                            // if(item.itemtype == 'refer'){
                            if (item.itemtype == 'refer' && item.visible == true) {
                                // BTM 性能优化，不显示的数据不用添加参照过滤条件
                                //处理交叉校验规则和参照过滤
                                // filterAllRefParm(item,props, 'JWZF_head');
                                requestApi.filterAllRefParm(item, _this.props, 'head');
                            }
                            if (item.itemtype == "number") {
                                item.replacehoder = 0;
                            }
                            // }
                        });
                        //多表头参照过滤 end
                        _this.props.form.openArea('JWZF_head'); // 展开区域
                        // 清空境内区域字段
                        _this.props.form.setFormItemsValue('head', {
                            'custaccount': { value: 0, display: null }, 'receiver': { value: null, display: null },
                            'zyx20': { value: null, display: null }, 'hbbm': { value: null, display: null }, 'zyx23': { value: null, display: null }, 'zyx27': { value: null, display: null },
                            'zyx29': { value: null, display: null }, 'zyx28': { value: null, display: null }, 'zyx25': { value: null, display: null }, 'iscostshare': { value: false, display: null },
                            'isexpamt': { value: false, display: null }, 'skyhzh': { value: false, display: null }
                        });

                        //add by ouyanggx 切换成境外时清空表体的个人银行账户skyhzh
                        let body = props.cardTable.getAllData('arap_bxbusitem');
                        body.rows.map((row) => {
                            row.values.skyhzh = {};
                        });
                        props.cardTable.setTableData('arap_bxbusitem', body);
                        //add end


                        meta['JNZF_head'].items = []; // 隐藏境内区域
                        _this.props.meta.setMeta(meta);
                    } else { // 勾选 "否"
                        // 显示境内区域
                        meta['JNZF_head'].items = this.state.items['JNZF_head'] ? this.state.items['JNZF_head'] : [];
                        _this.props.form.openArea('JNZF_head'); // 展开区域
                        // 清空境外区域字段
                        _this.props.form.setFormItemsValue('head', {
                            'receiver': { value: null, display: null }, 'zyx5': { value: null, display: null },
                            'zyx6': { value: null, display: null }, 'zyx7': { value: null, display: null }, 'zyx15': { value: null, display: null },
                            'zyx9': { value: null, display: null }, 'zyx11': { value: null, display: null }, 'hbbm': { value: null, display: null }, 'zyx4': { value: null, display: null },
                            'iscostshare': { value: false, display: null }, 'isexpamt': { value: false, display: null }
                        });

                        meta['JWZF_head'].items = []; // 隐藏境外区域
                        _this.props.meta.setMeta(meta);
                    }
                    break;

                // case "bzbm"://币种
                //     requestApi.valueChange({
                //         data: send_data,
                //         success: (data)=> {
                //             console.log(data);handleLoadDataResDada
                //             props.form.setAllFormValue({[presetVar.head.head1]:data.head[presetVar.head.head1]});
                //             Object.keys(data.bodys).forEach((item) =>{
                //                 props.cardTable.setTableData(item, data.bodys[item]);
                //             });
                //         }
                //     });
                //     break;
                default:
                    break;
            }
            break;
        default:
            //表体字段处理
            //满足报销标准核心控制项字段影响因素
            if (key != window.presetVar.showitemkey && (window.presetVar.reimsrc != null && window.presetVar.reimsrc.length > 0 && window.presetVar.reimsrc.includes(key))) {
                //清空当前行报销标准的值，以重新获取
                let newvalue = changedrows[0].newvalue.value == null ? '' : changedrows[0].newvalue.value;
                let oldvalue = changedrows[0].oldvalue.value == null ? '' : changedrows[0].oldvalue.value;
                if (newvalue == oldvalue) {
                    break;
                }
                let billdata = props.createBodyAfterEventData('20110ETEA_2641_IWEB', "head", bodyarr, moduleId, key, value);
                console.log('表头字段change后表头标题数据结构：', billdata);
                paramdata.checkrule = 'true';
                if (value.value == undefined || value.value == null) {
                    paramdata.value = changedrows[0].newvalue.value;
                }
                billdata.card.head.userjson = JSON.stringify(paramdata);
                billdata.templetid = props.meta.getMeta().pageid;

                // let send_headdata = {billdata,paramdata};
                // send_headdata.templateid = props.meta.getMeta().pageid;
                requestApi.valueChange({
                    data: billdata.card,
                    success: (data) => {
                        beforeUpdatePage(props);
                        //设置表头数据
                        let filterdata = requestApi.filterDatas(data.head[presetVar.head.head1]);
                        // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                        // props.form.setAllFormValue({[presetVar.head.head1]: filterdata});
                        updataFormData(props, presetVar.head.head1, filterdata);
                        // BTM 性能优化，变更页面数据更新方法
                        window.presetVar.reloadHead = filterdata;
                        //设置表体数据
                        // props.cardTable.setTableData(presetVar.body.body1, data.data.body[presetVar.body.body1]);
                        let filterbodydatas = requestApi.filterDatas(data.bodys[moduleid]);
                        if (filterbodydatas && filterbodydatas.rows) {
                            let rows = []
                            for (let i = 0; i < filterbodydatas.rows.length; i++) {
                                if (filterbodydatas.rows[i].values && filterbodydatas.rows[i].values.tablecode && filterbodydatas.rows[i].values.tablecode.value == moduleid) {
                                    rows[rows.length] = filterbodydatas.rows[i];
                                }
                            }
                            filterbodydatas.rows = rows;
                        }
                        // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                        // props.cardTable.setTableData(moduleid,filterbodydatas);
                        updatacardTableData(props, moduleid, filterbodydatas);
                        // BTM 性能优化，变更页面数据更新方法
                        // let billdatas = props.createExtCardData(window.presetVar.pageId, window.presetVar.head.head1,bodyarr);
                        // requestApi.handleLoadDataResDada(props.meta.getMeta(), {data:billdatas},props,moduleid);
                        props.cardTable.setTableDataWithResetInitValue(moduleid, filterbodydatas);
                        updatePage(props, presetVar.head.head1, [moduleid]);

                    }
                })
            }
            switch (key) {
                case 'pk_org'://表体财务组织
                case 'djrq'://表体单据日期
                case 'bzbm'://表体币种
                case 'bbhl'://表体本币汇率
                case 'dwbm_v'://表体借款报销人单位版本
                case 'dwbm'://表体借款报销人单位
                case 'fydwbm_v'://表体费用承担单位版本
                case 'fydwbm'://表体费用承担单位
                case 'pk_payorg_v'://表体支付单位版本
                case 'pk_payorg'://表体支付单位
                case "assume_org"://分摊页签表体承担单位编辑后事件
                case "assume_dept"://分摊页签表体承担部门编辑后事件
                case "pk_iobsclass"://分摊页签表体收支项目编辑后事件
                case 'deptid_v':// 表体报销人部门版本
                case 'deptid':// 表体报销人部门
                case 'fydeptid_v':// 表体费用承担部门版本
                case 'fydeptid':// 表体费用承担部门
                case "jkbxr"://借款报销人
                case "receiver"://表体收款人
                case "szxmid"://表体收支项目
                case "skyhzh"://表体个人银行账户
                case "jobid"://表体项目
                case "freecust"://散户
                case "custaccount"://表体客商银行账户
                case "paytarget"://表体收款对象
                case "hbbm"://表体供应商
                case "customer"://表体客户
                case "vat_amount"://表体含税金额
                case 'tni_amount'://表体不含税金额
                case 'tax_amount'://表体税金
                case 'amount'://表体报销金额
                case 'ybje'://原币金额
                case 'tax_rate'://表体税率
                case 'reimrule'://报销标准
                //chenylr,begin
                case 'defitem29':
                case 'defitem1': //参会人数,出发时间
                case 'defitem3':
                case 'defitem5':
                case 'defitem25':
                case 'defitem28':
                case 'defitem27':
                case 'defitem26': //业务招待级别 
                case 'defitem12': //陪同人员 
                case 'defitem6': //实际业务招待费,出差人
                case 'defitem7': //实际外部联络费
                case 'defitem10': //火车席别
                case 'defitem13': //自驾公里数
                case 'defitem11': //职级
                case 'defitem2': //返程时间
                case 'defitem4': //出差地点
                case 'defitem24': //往返火车/机场费
                    //chenylr,end
                    //by houyb
                    if ((window.presetVar.tradetype == '264X-Cxx-015' || window.presetVar.tradetype == '264X-Cxx-016')
                        && (key == 'defitem1' || key == 'defitem5' || key == 'defitem3' || key == 'defitem10')) {
                        //如果是会务费报销单的以上字段，走计算逻辑 
                    } else if (window.presetVar.tradetype != '264X-Cxx-015' && window.presetVar.tradetype != '264X-Cxx-016'
                        && (key == 'defitem28' || key == 'defitem29')) {
                        //如果不是会务费报销单 并且是defitem28 或 defitem29 走计算逻辑
                    } else {
                        //let billdata = props.createHeadAfterEventData('20110ETEA_2641_IWEB', "head",[moduleid], moduleId, key, value);
                        // let data = props.createGridAfterEventData('20521030', moduleid, moduleId, key, changedrows);
                        // let billdata = props.createBodyAfterEventData('20110ETEA_2641_IWEB', 'head', moduleid, moduleId, key, changedrows);
                        let newvalue = changedrows[0].newvalue.value == null ? '' : changedrows[0].newvalue.value;
                        let oldvalue = changedrows[0].oldvalue.value == null ? '' : changedrows[0].oldvalue.value;
                        if (newvalue == oldvalue) {
                            if (key == 'vat_amount' && oldvalue !== newvalue) {
                            } else {
                                break;
                            }
                        }
                        if ((window.presetVar.tradetype == '2641' || window.presetVar.tradetype == '264X-Cxx-018')
                            && (key == 'defitem24' || key == 'defitem13' || key == 'defitem2' || key == 'defitem1')) {
                            tripCalculate.call(this, index);
                        }
                        let billdata = props.createBodyAfterEventData('20110ETEA_2641_IWEB', "head", bodyarr, moduleId, key, value);
                        console.log('表头字段change后表头标题数据结构：', billdata);
                        if (value) {
                            if (value.value == undefined || value.value == null) {
                                paramdata.value = changedrows[0].newvalue.value;
                            }
                        }

                        billdata.card.head.userjson = JSON.stringify(paramdata);
                        billdata.templetid = props.meta.getMeta().pageid;
                        // let send_headdata = {billdata,paramdata};
                        // send_headdata.templateid = props.meta.getMeta().pageid;
                        requestApi.valueChange({
                            data: billdata.card,
                            success: (data) => {
                                beforeUpdatePage(props);
                                //设置表头数据
                                let filterdata = requestApi.filterDatas(data.head[presetVar.head.head1]);
                                // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                                // props.form.setAllFormValue({[presetVar.head.head1]: filterdata});
                                updataFormData(props, presetVar.head.head1, filterdata);
                                // BTM 性能优化，变更页面数据更新方法
                                window.presetVar.reloadHead = filterdata;
                                //设置表体数据
                                // props.cardTable.setTableData(presetVar.body.body1, data.data.body[presetVar.body.body1]);
                                let filterbodydatas = requestApi.filterDatas(data.bodys[moduleid]);
                                if (filterbodydatas && filterbodydatas.rows) {
                                    let rows = []
                                    for (let i = 0; i < filterbodydatas.rows.length; i++) {
                                        if (filterbodydatas.rows[i].values && filterbodydatas.rows[i].values.tablecode && filterbodydatas.rows[i].values.tablecode.value == moduleid) {
                                            rows[rows.length] = filterbodydatas.rows[i];
                                        }
                                    }
                                    filterbodydatas.rows = rows;
                                }
                                // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                                // props.cardTable.setTableData(moduleid,filterbodydatas);
                                updatacardTableData(props, moduleid, filterbodydatas);
                                // BTM 性能优化，变更页面数据更新方法
                                // let billdatas = props.createExtCardData(window.presetVar.pageId, window.presetVar.head.head1,bodyarr);
                                // requestApi.handleLoadDataResDada(props.meta.getMeta(), {data:billdatas},props,moduleid);
                                //houyb setTableData 方法会把currentIndex丢失，换成updateTableData
                                props.cardTable.setTableDataWithResetInitValue(moduleid, filterbodydatas);
                                updatePage(props, presetVar.head.head1, [moduleid]);

                            }
                        })
                        break;
                    }
                //by houyb             
                case 'defitem1':
                case 'defitem2':
                case 'defitem3':
                case 'defitem5':
                case 'defitem8':
                case 'defitem9':
                case 'defitem10':
                case 'defitem13':
                case 'defitem16':
                case 'defitem17':
                case 'defitem19':
                case 'defitem20':
                case 'defitem22':
                case 'defitem23':
                case 'defitem24':
                case 'defitem25':
                case 'defitem27':
                case 'defitem28':
                case 'defitem29':
                case 'defitem30':
                case 'defitem34':
                    if (window.presetVar.tradetype === '264X-Cxx-005') {
                        break;
                    }
                    if (window.presetVar.tradetype === '2641' && !checkValue.call(this, key)) {
                        setTableValueByIndex.call(this, key, '', null, index);
                        return;
                    }
                    calculate.call(this, index);
                    break;
            }
            break;
    }
};


function setImportConfig(prop) {
    let that = prop;
    let props = prop.props;
    let tabledata = [];
    let formdata = props.createExtCardData(props.meta.getMeta().pageid, "head", ['er_cshare_detail']);
    props.meta.getMeta().er_cshare_detail.items.forEach((item) => {
        if (item.attrcode != "opr") {
            tabledata.push({ "visible": item.visible, "key": item.attrcode, "label": item.label, "itemtype": item.itemtype, "initialvalue": item.initialvalue && item.initialvalue.value ? item.initialvalue.value : "" });
        }
    });
    formdata.userjson = JSON.stringify(tabledata);
    let config = getExcelImportConfig('/nccloud/erm/expenseaccount/ExpenseaccountCshareImportAction.do', formdata, (resultinfo) => {
        if (resultinfo == 'beginUpload') {
            // toastArr.push(1);
            //that.props.modal.show('importModal', {
            //content: <ProcessArr toastArr={toastArr} upParams={{"moduleName":'erm',"billType":'263X'}} />
            //});
        } else if (resultinfo.success) {
            let olddata = that.props.editTable.getAllData("er_cshare_detail").rows;
            let len = 0;
            olddata.find((row) => {
                if (row.status == 0) {
                    len++;
                }
            })
            let exceldata = resultinfo.data.bodys.er_cshare_detail;
            that.props.cardTable.setTableData('er_cshare_detail', exceldata);

        }
    });

    that.props.button.setUploadConfig('er_cshare_detail_Import', config);
}

function checkValue(key) {

    let departDate = getTableValue.call(this, 'defitem1'); //出发日期
    let arrivalDate = getTableValue.call(this, 'defitem2');//返程日期 

    if (departDate != 0 && arrivalDate != 0 && departDate.slice(0, 10) > arrivalDate.slice(0, 10)) {
        toast({ color: 'danger', content: this.props.MutiInit.getIntl(2011).get('201101GRSQ-0002') });//开始日期不能晚于结束日期
        return false;
    }

    return true;
}

export function calculate(index) {

    let tradetype = window.presetVar.tradetype || '';

    switch (tradetype) {

        case '2641'://差旅报销单
        case '264X-Cxx-018':
            tripCalculate.call(this, index);
            break;
        case '264X-Cxx-015':    //会务费报销单（5000以下）
        case '264X-Cxx-016':    //会务费报销单（5000以上）
            meetingCalculate.call(this, index);
            break;
        default:
            break;
    }

    if (!this.state.modelEdit || this.state.modelCloseEvent) {
        let key = 'vat_amount'
        let moduleId = 'arap_bxbusitem'
        // let index = this.props.cardTable.getCurrentIndex(moduleId)||0;
        let rows = this.props.cardTable.getRowsByIndexs(moduleId, index);
        let value = this.props.cardTable.getValByKeyAndIndex(moduleId, index, key);
        let changedrows = [{
            newvalue: { value: value.value },
            oldvalue: { value: 0 },
            rowid: rows[0].rowid
        }]
        afterEvent.call(this, this.props, moduleId, key, value, changedrows, index);
        this.state.modelCloseEvent = false;
    }
}

function tripCalculate(index) {

    let departDate = getTableValueByIndex.call(this, 'defitem1', null, index); //出发日期
    let arrivalDate = getTableValueByIndex.call(this, 'defitem2', null, index);//到达日期 
    let foodCost = getTableFloatValueByIndex.call(this, 'defitem23', null, index);//餐补费用
    let daysOnBusiness = GetNumberOfDays(departDate, arrivalDate); //出差天数

    let hotelCost = getTableFloatValueByIndex.call(this, "defitem22", null, index);    //住宿费

    let hotelStandard = daysOnBusiness < 2 ? 0 : hotelCost / (daysOnBusiness - 1);
    let foodStandard = foodCost / daysOnBusiness;  //申请餐费金额 餐补标准

    let insurance = getTableFloatValueByIndex.call(this, 'defitem34', null, index)  //意外保险费
    let otherCost = getTableFloatValueByIndex.call(this, 'defitem30', null, index)  //其他费用
    // let otherFees = insurance+otherCost;
    let otherFees = accAdd(insurance, otherCost);

    let kilometre = getTableFloatValueByIndex.call(this, 'defitem13', null, index);  //自驾公里数
    let oilCost = kilometre * 1;  //油费
    let trainCost = getTableFloatValueByIndex.call(this, 'defitem16', null, index); // 火车费
    let busCost = getTableFloatValueByIndex.call(this, 'defitem17', null, index);   // 长途汽车费
    let taxiCost = getTableFloatValueByIndex.call(this, 'defitem19', null, index);  //目的地市内汽车费
    let roadToll = getTableFloatValueByIndex.call(this, 'defitem20', null, index);  //过路费
    let stationRoundtrip = getTableFloatValueByIndex.call(this, 'defitem24', null, index);  //往返火车机场费用
    let travelCost = addNumbers(oilCost, trainCost, busCost, taxiCost, roadToll, stationRoundtrip); //交通费用
    // let travelCost = oilCost+trainCost+busCost+taxiCost+roadToll+stationRoundtrip;  //交通费用
    let amount = addNumbers(hotelCost, foodCost, otherFees, travelCost);

    setTableValueByIndex.call(this, 'defitem9', daysOnBusiness, null, index);//出差天数
    setTableValueByIndex.call(this, 'defitem21', oilCost, null, index);  //油费
    setTableValueByIndex.call(this, 'defitem29', hotelStandard, null, index); //申请住宿金额
    setTableValueByIndex.call(this, 'defitem28', foodStandard, null, index);
    setTableValueByIndex.call(this, 'defitem25', otherFees, null, index); //其他费用
    setTableValueByIndex.call(this, 'defitem27', travelCost, null, index); // 交通费
    setTableValueByIndex.call(this, 'vat_amount', amount, null, index);  //合计金额    
    setTableValueByIndex.call(this, 'zfbbje', amount, null, index);  //支付本币金额 
}

function meetingCalculate(index) {

    let personNums = getTableFloatValueByIndex.call(this, 'defitem1', null, index);  //参会人数
    let meetingNums = getTableFloatValueByIndex.call(this, 'defitem3', null, index); // 会议场数
    let mealNums = getTableFloatValueByIndex.call(this, 'defitem5', null, index)     // 会议餐数
    let mealTotalCost = getTableFloatValueByIndex.call(this, 'defitem9', null, index);//会议餐费金额
    let rentalTotalCost = getTableFloatValueByIndex.call(this, 'defitem8', null, index);//会议场租费金额
    // let otherCost = getTableFloatValue.call(this,'defitem27');  // 实际其他费用
    let otherTotalCost = getTableFloatValueByIndex.call(this, 'defitem10', null, index);
    let otherCost = personNums < 1 ? 0 : otherTotalCost / personNums;// 实际其他费用
    setTableValueByIndex.call(this, 'defitem27', otherCost, null, index);
    let rentalCost = (personNums < 1 || meetingNums < 1) ? 0 : rentalTotalCost / (meetingNums * personNums);
    setTableValueByIndex.call(this, 'defitem25', rentalCost, null, index);// 实际场租费金额
    let mealCost = (personNums < 1 || mealNums < 1) ? 0 : mealTotalCost / (personNums * mealNums);  // 实际会议餐费金额
    setTableValueByIndex.call(this, 'defitem28', mealCost, null, index);
    // let otherTotalCost = personNums*otherCost;
    // setTableValue.call(this,'defitem10',otherTotalCost);
    // let totalCost = rentalTotalCost+mealTotalCost+otherTotalCost;
    let totalCost = addNumbers(rentalTotalCost, mealTotalCost, otherTotalCost);
    setTableValueByIndex.call(this, 'vat_amount', totalCost, null, index);
}


function getTableValue(key, moduleId) {
    moduleId = moduleId || 'arap_bxbusitem';
    let index = this.props.cardTable.getCurrentIndex(moduleId) || 0;
    let value = this.props.cardTable.getValByKeyAndIndex(moduleId, index, key);
    return (value && value.value) || 0.00;
}
function getTableValueByIndex(key, moduleId, index) {
    moduleId = moduleId || 'arap_bxbusitem';
    let value = this.props.cardTable.getValByKeyAndIndex(moduleId, index, key);
    return (value && value.value) || 0.00;
}
function getTableFloatValue(key, moduleId) {
    return parseFloat(getTableValue.call(this, key, moduleId));
}
function getTableFloatValueByIndex(key, moduleId, index) {
    return parseFloat(getTableValueByIndex.call(this, key, moduleId, index));
}

function setTableValue(key, value, moduleId) {
    moduleId = moduleId || 'arap_bxbusitem';
    if (getTableValue.call(this, key, moduleId) === value)
        return;
    let index = this.props.cardTable.getCurrentIndex(moduleId) || 0;
    this.props.cardTable.setValByKeyAndIndex(moduleId, index, key, { value: value, display: value })
}
function setTableValueByIndex(key, value, moduleId, index) {
    moduleId = moduleId || 'arap_bxbusitem';
    if (getTableValueByIndex.call(this, key, moduleId, index) === value)
        return;
    this.props.cardTable.setValByKeyAndIndex(moduleId, index, key, { value: value, display: value })
}
function GetNumberOfDays(date1, date2) {//获得天数
    if (!date1 || !date2 || date1 === 0.00 || date2 === 0.00)
        return 0;
    //date1：开始日期，date2结束日期
    date1 = date1.split(" ")[0];
    date2 = date2.split(" ")[0];
    var a1 = Date.parse(new Date(date1));
    var a2 = Date.parse(new Date(date2));
    var day = parseInt((a2 - a1) / (1000 * 60 * 60 * 24));//核心：时间戳相减，然后除以天数
    return day + 1
};
/**
 ** 加法函数，用来得到精确的加法结果
 ** 说明：javascript的加法结果会有误差，在两个浮点数相加的时候会比较明显。这个函数返回较为精确的加法结果。
 ** 调用：accAdd(arg1,arg2)
 ** 返回值：arg1加上arg2的精确结果
 **/
function accAdd(arg1, arg2) {
    var r1, r2, m, c;
    try {
        r1 = arg1.toString().split(".")[1].length;
    }
    catch (e) {
        r1 = 0;
    }
    try {
        r2 = arg2.toString().split(".")[1].length;
    }
    catch (e) {
        r2 = 0;
    }
    c = Math.abs(r1 - r2);
    m = Math.pow(10, Math.max(r1, r2));
    if (c > 0) {
        var cm = Math.pow(10, c);
        if (r1 > r2) {
            arg1 = Number(arg1.toString().replace(".", ""));
            arg2 = Number(arg2.toString().replace(".", "")) * cm;
        } else {
            arg1 = Number(arg1.toString().replace(".", "")) * cm;
            arg2 = Number(arg2.toString().replace(".", ""));
        }
    } else {
        arg1 = Number(arg1.toString().replace(".", ""));
        arg2 = Number(arg2.toString().replace(".", ""));
    }
    return (arg1 + arg2) / m;
}

function addNumbers(...nums) {
    let amount = 0;
    nums.forEach(num => {
        if (num) {
            amount = accAdd(amount, num);
        }
    })
    return amount;
}