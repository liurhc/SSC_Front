import { ajax, print, base, promptBox, toast } from 'nc-lightapp-front';
import requestApi from '../requestApi';
import { dataHandle, getCookie } from '../initTemplate';
import { imageScan, imageView } from "sscrp/rppub/components/image";
import { removeDisplayScale } from "ssccommon/utils/dataFilter";
import linkVoucherApi from '../../../../public/components/linkvoucher/linkvoucher';
import linkBudgetApi from '../../../../public/components/linkbudget/linkbudget';
import linkQueryApi from '../../../../public/components/linkquery/linkquerybills';
import { updataFormData, updatacardTableData } from 'ssccommon/utils/changePageData';
import { CustomerFp } from '../../customerFp';
import { CustomerDr } from '../../customerDr';
import logs from 'ssccommon/utils/logs';
import { isNull } from 'util';
let debugLogs = new logs();

let pageButton = window.presetVar.pageButton;
let areaBtnAction = window.presetVar.areaBtnAction;
let statusVar = window.presetVar.status;
const { NCMessage } = base;
let jkCheck = 'false';
let ntbCheck = 'false';
function getRequest1(url) {
    if (!url)
        url = document.location.search;
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substring(url.indexOf("?") + 1);
        var strs = str.split("&");
        for (var i = 0; i < strs.length; i++) {
            theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
        }
    }
    return theRequest;
}

function closeBalanceBox(){
    let value = this.props.form.getFormItemsValue('head','zyx30').value;
    if(value==null||value==0){
        submitBox.call(this,'N');
    }else{
    promptBox({
        color: 'warning',     
        title: '是否关闭！',
        content:  '剩余余额:'+value+",是否关闭余额！",
        beSureBtnName:"是",
        cancelBtnName:"否",
        beSureBtnClick: submitBox.bind(this,'Y'),
        cancelBtnClick: submitBox.bind(this,'N')
    })
    }
}

function submitBox(isCloseBalance){
    let billtype = window.presetVar.tradetype;
    let props = this.props;
    let bodys = window.presetVar.lsBodys;
    let bodyarr = [];
    let multiLang = props.MutiInit.getIntl(2011);
    let hideTable = this.state.hideTables;
    bodys.map((one) => {
        if (hideTable.indexOf(one) < 0) {
            bodyarr[bodyarr.length] = one;
        }
    })
    let sense = props.getUrlParam("scene");
    window.presetVar.lsBodys.forEach((body) => {
        if (body !== 'accrued_verify' && body !== 'er_bxcontrast' && body !== 'er_cshare_detail' && body !== 'other') {
            props.cardTable.filterEmptyRows(body, ['vat_amount'], 'include');
            props.cardTable.closeModel(body);
        }
    });
    let checkResult_sub = props.form.isCheckNow('head') && props.cardTable.checkTableRequired();
    if (checkResult_sub) {
        //by houyb 检查侧拉必填项
        let metass = props.meta.getMeta();
        let allData = props.createExtCardData(props.meta.getMeta().code, "head", bodyarr);
        if ( checkSlide(metass, allData)){
            return;
        }
        //弹出冲借款
        let bxcontrast = props.cardTable.getAllRows("er_bxcontrast");
        let status = window.presetVar.pagestatus == '' ? this.props.getUrlParam("status") : window.presetVar.pagestatus;
        if ((window.presetVar && window.presetVar.ISCONTRAST && window.presetVar.ISCONTRAST == "Y" && bxcontrast.length == 0 && (status == "add" || status == "edit")) && !window.sureAssign) {
            promptBox({
                color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                title: multiLang && multiLang.get('201102BCLF-0002'),
                content: multiLang && multiLang.get('201102BCLF-0008'),
                beSureBtnClick: this.cjk,
                cancelBtnClick: this.submit
            })
            return;
        } else {
            let iscostshare = props.form.getFormItemsValue("head", ['iscostshare']) == null ? "" : props.form.getFormItemsValue("head", ['iscostshare'])[0].value//收款对象
            if (iscostshare == true || iscostshare == "true") {
                let rows = props.cardTable.getAllRows("er_cshare_detail");
                if (rows == null || rows.length == 0) {
                    toast({ content: multiLang && multiLang.get("201102BCLF-0019"), color: 'warning' });
                    return;
                }
            }
            //不录入单据内容直接报错不能提交
            let vat_amount = props.form.getFormItemsValue("head", ['iscostshare']) == null ? "" : props.form.getFormItemsValue("head", ['vat_amount'])[0].value//报销销金额
            if (vat_amount == undefined || vat_amount == null || vat_amount == 0) {
                toast({ content: multiLang && multiLang.get("201102BCLF-0020"), color: 'warning' });
                return;
            }
            let send_data = {};
            let billdata = props.createExtCardData(props.meta.getMeta().code, "head", bodyarr);
            requestApi.delete3status(billdata);
            let paramurl = getRequest1(parent.window.location.hash);
            let paramdata = {};
            console.log(billdata);
            paramdata.openbillid = !(props.getUrlParam("billid") || props.getUrlParam("id")) ? "" : props.getUrlParam("billid") || props.getUrlParam("id");
            paramdata.fromssc = "ssc",
                paramdata.jkCheck = jkCheck,
                paramdata.ntbCheck = ntbCheck,
                paramdata.msg_pk_bill = "",
                paramdata.pk_tradetype = billtype;
            paramdata.pagecode = paramurl.p;
            paramdata.appcode = paramurl.c;
            paramdata.assingUsers = window.assingUsers;
            let appcode = (this.props.getUrlParam("appcode") == undefined || this.props.getUrlParam("appcode") == '') ? this.props.getSearchParam("c") : this.props.getUrlParam("appcode");
            let pagecode = (this.props.getUrlParam("pagecode") == undefined || this.props.getUrlParam("pagecode") == '') ? this.props.getSearchParam("p") : this.props.getUrlParam("pagecode");
            let status = window.presetVar.pagestatus == '' ? this.props.getUrlParam("status") : window.presetVar.pagestatus;
            if (status == "edit") {
                paramdata.status = "edit";
            } else {
                paramdata.status = "save";
            }
            //如果指派点了确定  由于第一次点击保存提交实际上已经保存成功了，指派后再次提交应该走浏览提交逻辑
            if (window.sureAssign) {
                paramdata.status = "edit";
                paramdata.openbillid = window.assignBillId;
            }
            let arr=["264X-Cxx-002","264X-Cxx-008","264X-Cxx-012","264X-Cxx-016"];
            let cc=arr.includes(this.props.form.getFormItemsValue('head','djlxbm').value) ? true:false;
            if(cc){
                paramdata.isCloseBalance = isCloseBalance;
            }else{
                paramdata.isCloseBalance = 'N';
            }
            billdata.head.userjson = JSON.stringify(paramdata);
            billdata.templetid = props.meta.getMeta().pageid;
            billdata.accessorybillid = this.accessorybillid;
            console.log(billdata);
            props.validateToSave(billdata, () => {
                requestApi.submitBill({
                    data: billdata,
                    success: (data) => {//设置了指派
                        ntbCheck = 'false';
                        if (data.workflow && data.billid && data.bill && (data.workflow == 'approveflow' || data.workflow == 'workflow')) {
                            this.setState({
                                compositedata: data,
                                compositedisplay: true
                            });
                            window.assignBillId = data.billid;
                            //重新给页面赋值
                            let newFormData = requestApi.filterDatas(data.bill.head[presetVar.head.head1]);
                            updataFormData(props, presetVar.head.head1, newFormData);
                            window.presetVar.reloadHead = newFormData;
                            bodyarr.forEach((item) => {
                                updatacardTableData(props, item, requestApi.filterDatas(data.bill.bodys[item]));
                            })
                        } else {
                            this.setState({
                                compositedisplay: false
                            });
                            if (data.Alarm && data.Alarm == "Y") {
                                promptBox({
                                    color: 'danger',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                                    title: multiLang && multiLang.get("201102BCLF-0021"),                // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输
                                    content: data.message,             // 提示内容,非必输
                                    noFooter: false,                // 是否显示底部按钮(确定、取消),默认显示(false),非必输
                                    noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
                                    beSureBtnName: multiLang && multiLang.get("201102BCLF-0017"),          // 确定按钮名称, 默认为"确定",非必输
                                    cancelBtnName: multiLang && multiLang.get("201102BCLF-0018"),         // 取消按钮名称, 默认为"取消",非必输
                                    beSureBtnClick: () => {
                                        ntbCheck = 'true';
                                        if (props.getUrlParam('status') === 'browse') {
                                            window.sureAssign = false;
                                        }
                                        debugger;
                                        pageButtonClick.call(this)[pageButton.billSubmit]();
                                    },
                                    cancelBtnClick: () => {
                                        let billid = props.form.getFormItemsValue("head", "pk_jkbx").value;
                                        if (props.getUrlParam('status') === 'browse') {
                                            props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.browse, id: billid, tradetype: billtype, pagecode: paramurl.p, appcode: paramurl.c });
                                        } else {
                                            if (billid) {
                                                props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.edit, id: billid, tradetype: billtype, pagecode: paramurl.p, appcode: paramurl.c });
                                            } else {
                                            }
                                        }
                                        window.assingUsers = [];    //清空指派人信息
                                        window.sureAssign = false;
                                    }
                                })

                            } else if (data.userjson && JSON.parse(data.userjson) && JSON.parse(data.userjson).reimmsg && JSON.parse(data.userjson).reimmsg.length > 0) {
                                let billid = data.head.head.rows[0].values.pk_jkbx.value;
                                promptBox({
                                    color: 'warning',       // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                                    title: multiLang && multiLang.get('201102JCLF_C-0002'),
                                    content: JSON.parse(data.userjson).reimmsg,
                                    noFooter: false,  // 是否显示底部按钮(确定、取消),默认显示(false),非必输
                                    noCancelBtn: true,    // 是否显示取消按钮,，默认显示(false),非必输
                                    beSureBtnName: multiLang && multiLang.get('2011-0003'),
                                    cancelBtnName: multiLang && multiLang.get('2011-0002'),
                                    beSureBtnClick: () => {
                                        window.presetVar.redata = 'Y';
                                        //重新给页面赋值
                                        let newFormData = requestApi.filterDatas(data.head[presetVar.head.head1]);
                                        updataFormData(props, presetVar.head.head1, newFormData);
                                        window.presetVar.reloadHead = newFormData;
                                        bodyarr.forEach((item) => {
                                            updatacardTableData(props, item, requestApi.filterDatas(data.bodys[item]));
                                        })
                                        if (sense && (sense == "approve" || sense == "zycl" || sense == "approvesce")) {
                                            props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.browse, id: billid, tradetype: billtype });
                                        } else {
                                            props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.browse, id: billid, tradetype: billtype, pagecode: paramurl.p, appcode: paramurl.c });
                                        }
                                        window.assingUsers = [];    //清空指派人信息
                                        window.sureAssign = false;
                                    }
                                })
                            } else {
                                console.log(data);
                                // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                                // props.form.setAllFormValue({[presetVar.head.head1]: requestApi.filterDatas(data.head[presetVar.head.head1])});
                                // window.presetVar.reloadHead = requestApi.filterDatas(data.head[presetVar.head.head1]);
                                let newFormData = requestApi.filterDatas(data.head[presetVar.head.head1]);
                                updataFormData(props, presetVar.head.head1, newFormData);
                                window.presetVar.reloadHead = newFormData;
                                // BTM 性能优化，变更页面数据更新方法
                                bodyarr.forEach((item) => {
                                    // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                                    // debugLogs.timeStart('setTableData_' + item);
                                    // props.cardTable.setTableData(item, requestApi.filterDatas(data.bodys[item]));
                                    // debugLogs.timeEnd('setTableData_' + item);
                                    updatacardTableData(props, item, requestApi.filterDatas(data.bodys[item]));
                                    // BTM 性能优化，变更页面数据更新方法
                                })
                                ntbCheck = 'false';
                                let billid = data.head.head.rows[0].values.pk_jkbx.value;
                                // window.open(window.location.origin+'/erm/expenseaccount/expenseaccount/card#status=browse&openbillid='+billid+'&tradetype=2641');
                                // location.hash = `#status=${statusVar.browse}&billid=${billid}&tradetype=${billtype}`;
                                window.presetVar.redata = 'N';
                                if (sense && (sense == "approve" || sense == "zycl" || sense == "approvesce")) {
                                    props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.browse, id: billid, tradetype: billtype, sence: sense });
                                } else {
                                    props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.browse, id: billid, tradetype: billtype, pagecode, appcode: appcode });
                                }
                                props.button.setButtonsVisible({
                                    [pageButton.pageSave]: false,
                                    [pageButton.pageEdit]: false,
                                    [pageButton.billSubmit]: false,
                                    [pageButton.billRecall]: true,
                                    [pageButton.pageDel]: false,
                                    [pageButton.billInvalid]: false,
                                    [pageButton.pageClose]: false
                                });
                                window.assingUsers = [];    //清空指派人信息
                                window.sureAssign = false;
                                // //处理按钮相关状态
                                // dataHandle(data.head.head,props);
                                // props.form.setFormStatus('head', 'browse');
                                // props.billBodys && props.billBodys.forEach((body)=>{
                                //     props.editTable.setStatus(body, 'browse');
                                // });

                            }
                        }
                    },
                    error: (data) => {
                        toast({ content: data.message, color: 'danger' });
                    }
                });
            }, bodyarr, null);

        }


        // props.form.setFormStatus('head', 'browse');
        // props.billBodys.forEach((body)=>{
        //     props.editTable.setStatus(body, 'browse');
        // });

    }
}

//by houyb 检查侧拉必填项
function checkSlide(meta,allData) {
    let flag = false;
    let bodyData = allData["bodys"];
    let strmsg = "";
    Object.keys(meta.gridrelation).forEach((item,index) =>{
        let destEditAreaCode = meta.gridrelation[item].destEditAreaCode;
        if(destEditAreaCode){
            destEditAreaCode.forEach((name)=>{
                let table = meta[name];
                table.items.forEach((propt_item)=>{
                    if(propt_item.required  && propt_item.visible) {
                        if(bodyData[item]){
                            bodyData[item].rows.forEach((rowData)=>{
                                if (!rowData.values[propt_item.attrcode] || !rowData.values[propt_item.attrcode].value ||rowData.values[propt_item.attrcode].value == ""){
                                    flag = true;
                                    strmsg = strmsg + table.name + "  该字段不能为空：" + propt_item.label + "\n";
                                    return;
                                }
                            })
                        }
                    }
                })
            })
        }
    })
    if (strmsg != ""){
        toast({ content:  strmsg|| "表体字段未填写", color: 'danger' });
    }
    return flag;
}


function pageButtonClick() {
    let billtype = window.presetVar.tradetype;
    let props = this.props;
    let bodys = window.presetVar.lsBodys;
    let bodyarr = [];
    let multiLang = props.MutiInit.getIntl(2011);
    let hideTable = this.state.hideTables;
    bodys.map((one) => {
        if (hideTable.indexOf(one) < 0) {
            bodyarr[bodyarr.length] = one;
        }
    })
    return {
        ['pageFpcy']: () => {
            props.modal.show('detailFPcy', {
                title: '发票查验',
                content: <CustomerFp self={this} onRef={this.refbind} />,
                beSureBtnClick: this.beSureBtnClick, //点击确定按钮事件
                cancelBtnClick: this.cancelBtnClick, //取消按钮事件回调
                rightBtnName: '取消', //左侧按钮名称,默认‘取消’
                leftBtnName: '确定', //右侧按钮名称， 默认‘确定’
                noFooter: false,

            })
        },
        ['pageFpdr']: () => {
            props.modal.show('detailFPdr', {
                title: '发票导入',
                content: <CustomerDr self = {this} />,
            })
        },
        ['ztd']:()=>{
            let billno=props.form.getFormItemsValue("head", "djbh")&&props.form.getFormItemsValue("head", "djbh").value;
            let transtypecode=props.form.getFormItemsValue("head", "djlxbm")&&props.form.getFormItemsValue("head", "djlxbm").value
            ajax({
                url: '/nccloud/sscrp/rpbill/RPBillQryAction.do',
                data: {"isComplate":"true","fuzzyQueryKey":[billno],"orderByInfo":[],"pageinfo":{"number":1,"size":1,"totalElements":1,"totalPages":1},"billdate":"ALL",transtypecode},
                success: (res) => {
                    let data ={};
                    if(res.data.rplist.rows.length>0){
                        data= res.data.rplist.rows[0].values
                        this.setState({
                            showModalNum: true, 
                            operationData: data,
                        })
                    }else{
                        ajax({
                            url: '/nccloud/sscrp/rpbill/RPBillQryAction.do',
                            data: {"isComplate":"false","fuzzyQueryKey":[billno],"orderByInfo":[],"pageinfo":{"number":1,"size":1,"totalElements":1,"totalPages":1},"billdate":"ALL",transtypecode},
                            success: (res) => {
                                let data ={};
                                if(res.data.rplist.rows.length>0){
                                    data= res.data.rplist.rows[0].values
                                    this.setState({
                                        showModalNum: true, 
                                        operationData: data,
                                    })
                                }else{
                                    this.setState({
                                        showModalNum: false, 
                                        operationData: data,
                                    })
                                }
                                
                            }
                        });
                    }
                    
                }
            });
         
        },
        ['pageTempSave']: ()=> {
            let href =window.location.href;
            if(href.indexOf("expenseaccount4Pull") > 0 && !props.form.getAllFormValue("head").rows[0].values.pk_item.value){
                toast({
                    content: "单据已被刷新，请重新制单",
                    color:"warning"
                })
                return;
            }
            let billdata = removeDisplayScale(props.createExtCardData(props.meta.getMeta().code, "head", bodyarr));
            requestApi.delete3status(billdata);
            let paramurl = getRequest1(parent.window.location.hash);
            let paramdata = {};
            let sense = props.getUrlParam("scene");
            console.log(billdata);
            paramdata.fromssc = "ssc",
                paramdata.jkCheck = jkCheck,
                paramdata.ntbCheck = ntbCheck,
                paramdata.msg_pk_bill = "",
                paramdata.accessorybillid = this.accessorybillid;
            paramdata.pk_tradetype = billtype;
            paramdata.pagecode = paramurl.p;
            paramdata.appcode = paramurl.c;
            let status = window.presetVar.pagestatus == '' ? this.props.getUrlParam("status") : window.presetVar.pagestatus;
            if (status == "edit") {
                paramdata.status = "edit";
            } else {
                paramdata.status = "save";
            }
            billdata.head.userjson = JSON.stringify(paramdata);
            billdata.templetid = props.meta.getMeta().pageid;
            requestApi.tempsavejkbx({
                data: billdata,
                success: (data) => {
                    props.button.setButtonDisabled({
                        [pageButton.billSubmit]: true,
                    });
                    let items = this.state.items, propsMeta = props.meta.getMeta();
                    if(items.JNZF_head) {
                        propsMeta.JNZF_head.items = items.JNZF_head;
                    }
                    if(items.JWZF_head) {
                        propsMeta.JWZF_head.items = items.JWZF_head;
                    }
                    props.meta.setMeta({...propsMeta});
                    if (sense && (sense == "approve" || sense == "zycl" || sense == "approvesce")) {
                        ntbCheck = 'false';
                    }
                    debugLogs.timeEnd('save_wate');
                    debugLogs.timeStart('save_after');
                    console.log(data);

                    let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId
                    if (data.userjson && JSON.parse(data.userjson) && JSON.parse(data.userjson).reimmsg && JSON.parse(data.userjson).reimmsg.length > 0) {
                        let billid = data.head.head.rows[0].values.pk_jkbx.value;
                        promptBox({
                            color: 'warning',       // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                            title: multiLang && multiLang.get('201102JCLF_C-0002'),
                            content: JSON.parse(data.userjson).reimmsg,
                            noFooter: false,  // 是否显示底部按钮(确定、取消),默认显示(false),非必输
                            noCancelBtn: false,    // 是否显示取消按钮,，默认显示(false),非必输
                            beSureBtnName: multiLang && multiLang.get('2011-0003'),
                            cancelBtnName: multiLang && multiLang.get('2011-0002'),
                            beSureBtnClick: () => {
                                window.presetVar.redata = 'Y';
                                if (sense && (sense == "approve" || sense == "zycl" || sense == "approvesce")) {
                                    props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.browse, id: billid, tradetype: billtype, scene: sense });
                                } else {
                                    props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.browse, id: billid, tradetype: billtype, pagecode: paramurl.p, appcode: paramurl.c });
                                }
                            },   // 确定按钮点击调用函数,非必输
                            cancelBtnClick: () => {
                                let newFormData = requestApi.filterDatas(data.head[presetVar.head.head1]);
                                updataFormData(props, presetVar.head.head1, newFormData);
                                window.presetVar.reloadHead = newFormData;
                                bodyarr.forEach((item) => {
                                    updatacardTableData(props, item, requestApi.filterDatas(data.bodys[item]));
                                })
                                if (sense && (sense == "approve" || sense == "zycl" || sense == "approvesce")) {
                                    props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.edit, id: billid, tradetype: billtype, scene: sense });
                                } else {
                                    props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.edit, id: billid, tradetype: billtype, pagecode: paramurl.p, appcode: paramurl.c });
                                }
                            }
                            // ,   // 确定按钮点击调用函数,非必输
                            // cancelBtnClick: ()=>{
                            //     props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html',{isfromSsc:true,status:statusVar.edit,billid:billid,tradetype:billtype,pagecode:paramurl.p,appcode:paramurl.c});
                            // }  // 取消按钮点击调用函数,非必输
                        })
                    } else {
                        if (data.Alarm && data.Alarm == "Y") {
                            promptBox({
                                color: 'danger',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                                title: multiLang && multiLang.get("201102BCLF-0021"),                // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输
                                content: data.message,             // 提示内容,非必输
                                noFooter: false,                // 是否显示底部按钮(确定、取消),默认显示(false),非必输
                                noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
                                beSureBtnName: multiLang && multiLang.get("201102BCLF-0017"),          // 确定按钮名称, 默认为"确定",非必输
                                cancelBtnName: multiLang && multiLang.get("201102BCLF-0018"),         // 取消按钮名称, 默认为"取消",非必输
                                beSureBtnClick: () => {
                                    ntbCheck = 'true';
                                    pageButtonClick.call(this)[pageButton.pageSave]();
                                },   // 确定按钮点击调用函数,非必输
                                // cancelBtnClick: ()=>{console.log("sdd") },  // 取消按钮点击调用函数,非必输
                            })

                        } else {
                            // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                            let billid = data.head.head.rows[0].values.pk_jkbx.value;
                            let newFormData = requestApi.filterDatas(data.head[presetVar.head.head1]);
                            updataFormData(props, presetVar.head.head1, newFormData);
                            window.presetVar.reloadHead = newFormData;
                            // BTM 性能优化，变更页面数据更新方法
                            bodyarr.forEach((item) => {
                                // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                                // debugLogs.timeStart('setTableData_' + item);
                                // props.cardTable.setTableData(item, requestApi.filterDatas(data.bodys[item]));
                                // debugLogs.timeEnd('setTableData_' + item);
                                updatacardTableData(props, item, requestApi.filterDatas(data.bodys[item]));
                                // BTM 性能优化，变更页面数据更新方法
                            })
                            ntbCheck = 'false';
                            window.presetVar.redata = 'N';
                            if (sense && (sense == "approve" || sense == "zycl" || sense == "approvesce")) {
                                props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.browse, id: billid, tradetype: billtype, scene: sense });
                            } else {
                                props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.browse, id: billid, tradetype: billtype, pagecode: paramurl.p, appcode: paramurl.c });
                            }
                        }
                    }
                    debugLogs.timeEnd('save_after');
                },
                error: (data) => {
                    toast({ content: data.message, color: 'danger' });
                }
            });
        },
        [pageButton.pageSave]: () => {
            let href =window.location.href;
            if(href.indexOf("expenseaccount4Pull") > 0 && !props.form.getAllFormValue("head").rows[0].values.pk_item.value){
                toast({
                    content: "单据已被刷新，请重新制单",
                    color:"warning"
                })
                return;
            }
            if(href.indexOf("#/add?") > -1 && !props.cardTable.getAllRows("accrued_verify")[0]){
                toast({
                    content: "请填写核销预提信息",
                    color:"warning"
                })
                return;
            }
            let bxbusitemRows = props.cardTable.getAllRows("arap_bxbusitem");
            let isBreak = false;
            bxbusitemRows.forEach((row)=>{
               if(row && row.values && (!row.values.bbhl || !row.values.bbhl.value || row.values.bbhl.value == '')){
                    toast({
                        content: "表体汇率不能为空",
                        color:"warning"
                    })
                    isBreak = true;
                    return;
               }
            })
            if(isBreak){
                return;
            }
            // alert(addtype);
            // return ;
            debugLogs.timeStart('save_before');
            //let fpxxdata = props.cardTable.getAllRows("other");
            window.presetVar.lsBodys.forEach((body) => {
                if (body !== 'accrued_verify' && body !== 'er_bxcontrast' && body !== 'er_cshare_detail' && body !== 'other') {
                    props.cardTable.filterEmptyRows(body, ['vat_amount'], 'include');
                    props.cardTable.closeModel(body);
                }
            });
            
            
            let metass = props.meta.getMeta();
            let checkResult = props.form.isCheckNow('head') && props.cardTable.checkTableRequired();
            if (checkResult) {
               
                //弹出冲借款
                let bxcontrast = props.cardTable.getAllRows("er_bxcontrast");
                let allData = props.createExtCardData(props.meta.getMeta().code, "head", bodyarr);
                //by houyb 检查侧拉必填项
                if ( checkSlide(metass, allData)){
                    return;
                }
                if (window.presetVar && window.presetVar.ISCONTRAST && window.presetVar.ISCONTRAST == "Y" && bxcontrast.length == 0) {               
                    promptBox({
                        color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                        title: multiLang && multiLang.get('201102BCLF-0002'),
                        content: multiLang && multiLang.get('201102BCLF-0008'),
                        beSureBtnClick: this.cjk,
                        cancelBtnClick: this.save
                    })
                    let szxmid=this.props.cardTable&&this.props.cardTable.getColValue("arap_bxbusitem", "szxmid",false,false);
                    let hbbm=this.props.form.getFormItemsValue("head", "hbbm");
                    let paytarget=this.props.form.getFormItemsValue("head","paytarget");
                    let tradetype = window.presetVar.tradetype;
                    let fctno=this.props.cardTable.getDataByIndex('arap_bxbusitem',0)&&this.props.cardTable.getDataByIndex('arap_bxbusitem',0).values&&this.props.cardTable.getDataByIndex('arap_bxbusitem',0).values.fctno;
                    this.setState({szxmid,hbbm,fctno,paytarget,tradetype});  
                    return;
                } else {
                    let iscostshare = props.form.getFormItemsValue("head", ['iscostshare']) == null ? "" : props.form.getFormItemsValue("head", ['iscostshare'])[0].value//收款对象
                    if (iscostshare == true || iscostshare == "true") {
                        let rows = props.cardTable.getAllRows("er_cshare_detail");
                        if (rows == null || rows.length == 0) {
                            toast({ content: multiLang && multiLang.get('201102BCLF-0019')/*"表头分摊勾选时,分摊页签不能为空!"*/, color: 'warning' });
                            return;
                        }
                    }
                    //不录入单据内容直接报错不能提交
                    let vat_amount = props.form.getFormItemsValue("head", ['iscostshare']) == null ? "" : props.form.getFormItemsValue("head", ['vat_amount'])[0].value//报销销金额
                    if (vat_amount == undefined || vat_amount == null || vat_amount == 0) {
                        toast({ content: multiLang && multiLang.get('201102BCLF-0020')/*"报销金额不能为空!"*/, color: 'warning' });
                        return;
                    }
                    let billdata = removeDisplayScale(props.createExtCardData(props.meta.getMeta().code, "head", bodyarr));
                    //let fpxxdata = props.cardTable.getAllRows("other");
                    //测试费用表
                    //let arapdata = props.cardTable.getAllRows("arap_bxbusitem");
                    //billdata.bodys.other.rows = fpxxdata;
                    requestApi.delete3status(billdata);
                    let paramurl = getRequest1(parent.window.location.hash);
                    let paramdata = {};
                    let sense = props.getUrlParam("scene");
                    console.log(billdata);
                    paramdata.fromssc = "ssc",
                        paramdata.jkCheck = jkCheck,
                        paramdata.ntbCheck = ntbCheck,
                        paramdata.msg_pk_bill = "",
                        paramdata.accessorybillid = this.accessorybillid;
                    paramdata.pk_tradetype = billtype;
                    paramdata.pagecode = paramurl.p;
                    paramdata.appcode = paramurl.c;
                    let status = window.presetVar.pagestatus == '' ? this.props.getUrlParam("status") : window.presetVar.pagestatus;
                    if (status == "edit") {
                        paramdata.status = "edit";
                    } else {
                        paramdata.status = "save";
                    }
                    billdata.head.userjson = JSON.stringify(paramdata);
                    billdata.templetid = props.meta.getMeta().pageid;
                    console.log(billdata);
                    debugLogs.timeEnd('save_before');
                    debugLogs.timeStart('save_wate');
                    props.validateToSave(billdata, () => {
                        requestApi.savejkbx({
                            data: billdata,
                            success: (data) => {
                                props.button.setButtonDisabled({
                                    [pageButton.billSubmit]: false,
                                });
                                let items = this.state.items, propsMeta = props.meta.getMeta();
                                if(items.JNZF_head) {
                                    propsMeta.JNZF_head.items = items.JNZF_head;
                                }
                                if(items.JWZF_head) {
                                    propsMeta.JWZF_head.items = items.JWZF_head;
                                }
                                props.meta.setMeta({...propsMeta});

                                if (sense && (sense == "approve" || sense == "zycl" || sense == "approvesce")) {
                                    ntbCheck = 'false';
                                }
                                debugLogs.timeEnd('save_wate');
                                debugLogs.timeStart('save_after');
                                console.log(data);

                                let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId
                                if (data.userjson && JSON.parse(data.userjson) && JSON.parse(data.userjson).reimmsg && JSON.parse(data.userjson).reimmsg.length > 0) {
                                    let billid = data.head.head.rows[0].values.pk_jkbx.value;
                                    promptBox({
                                        color: 'warning',       // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                                        title: multiLang && multiLang.get('201102JCLF_C-0002'),
                                        content: JSON.parse(data.userjson).reimmsg,
                                        noFooter: false,  // 是否显示底部按钮(确定、取消),默认显示(false),非必输
                                        noCancelBtn: false,    // 是否显示取消按钮,，默认显示(false),非必输
                                        beSureBtnName: multiLang && multiLang.get('2011-0003'),
                                        cancelBtnName: multiLang && multiLang.get('2011-0002'),
                                        beSureBtnClick: () => {
                                            window.presetVar.redata = 'Y';
                                            if (sense && (sense == "approve" || sense == "zycl" || sense == "approvesce")) {
                                                props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.browse, id: billid, tradetype: billtype, scene: sense });
                                            } else {
                                                props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.browse, id: billid, tradetype: billtype, pagecode: paramurl.p, appcode: paramurl.c });
                                            }
                                        },   // 确定按钮点击调用函数,非必输
                                        cancelBtnClick: () => {
                                            let newFormData = requestApi.filterDatas(data.head[presetVar.head.head1]);
                                            updataFormData(props, presetVar.head.head1, newFormData);
                                            window.presetVar.reloadHead = newFormData;
                                            bodyarr.forEach((item) => {
                                                updatacardTableData(props, item, requestApi.filterDatas(data.bodys[item]));
                                            })
                                            if (sense && (sense == "approve" || sense == "zycl" || sense == "approvesce")) {
                                                props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.edit, id: billid, tradetype: billtype, scene: sense });
                                            } else {
                                                props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.edit, id: billid, tradetype: billtype, pagecode: paramurl.p, appcode: paramurl.c });
                                            }
                                        }
                                        // ,   // 确定按钮点击调用函数,非必输
                                        // cancelBtnClick: ()=>{
                                        //     props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html',{isfromSsc:true,status:statusVar.edit,billid:billid,tradetype:billtype,pagecode:paramurl.p,appcode:paramurl.c});
                                        // }  // 取消按钮点击调用函数,非必输
                                    })
                                } else {
                                    if (data.Alarm && data.Alarm == "Y") {
                                        promptBox({
                                            color: 'danger',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                                            title: multiLang && multiLang.get("201102BCLF-0021"),                // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输
                                            content: data.message,             // 提示内容,非必输
                                            noFooter: false,                // 是否显示底部按钮(确定、取消),默认显示(false),非必输
                                            noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
                                            beSureBtnName: multiLang && multiLang.get("201102BCLF-0017"),          // 确定按钮名称, 默认为"确定",非必输
                                            cancelBtnName: multiLang && multiLang.get("201102BCLF-0018"),         // 取消按钮名称, 默认为"取消",非必输
                                            beSureBtnClick: () => {
                                                ntbCheck = 'true';
                                                pageButtonClick.call(this)[pageButton.pageSave]();
                                            },   // 确定按钮点击调用函数,非必输
                                            // cancelBtnClick: ()=>{console.log("sdd") },  // 取消按钮点击调用函数,非必输
                                        })

                                    } else {
                                        // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                                        let billid = data.head.head.rows[0].values.pk_jkbx.value;
                                        let newFormData = requestApi.filterDatas(data.head[presetVar.head.head1]);
                                        updataFormData(props, presetVar.head.head1, newFormData);
                                        window.presetVar.reloadHead = newFormData;
                                        // BTM 性能优化，变更页面数据更新方法
                                        bodyarr.forEach((item) => {
                                            // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                                            // debugLogs.timeStart('setTableData_' + item);
                                            // props.cardTable.setTableData(item, requestApi.filterDatas(data.bodys[item]));
                                            // debugLogs.timeEnd('setTableData_' + item);
                                            updatacardTableData(props, item, requestApi.filterDatas(data.bodys[item]));
                                            // BTM 性能优化，变更页面数据更新方法
                                        })
                                        ntbCheck = 'false';
                                        window.presetVar.redata = 'N';
                                        if (sense && (sense == "approve" || sense == "zycl" || sense == "approvesce")) {
                                            props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.browse, id: billid, tradetype: billtype, scene: sense });
                                        } else {
                                            props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.browse, id: billid, tradetype: billtype, pagecode: paramurl.p, appcode: paramurl.c });
                                        }
                                    }
                                }
                                debugLogs.timeEnd('save_after');
                            },
                            error: (data) => {
                                toast({ content: data.message, color: 'danger' });
                            }
                        });
                    }, bodyarr, null);

                }

            }
         },
        [pageButton.pageClose]: () => {
            let items = this.state.items, propsMeta = props.meta.getMeta();
            if(items.JNZF_head) {
                propsMeta.JNZF_head.items = items.JNZF_head;
            }
            if(items.JWZF_head) {
                propsMeta.JWZF_head.items = items.JWZF_head;
            }
            props.meta.setMeta({...propsMeta});
            let pagestatus = window.presetVar.pagestatus == '' ? props.getUrlParam('status') : window.presetVar.pagestatus;
            let copyid = props.getUrlParam('copyFromBillId');
            if (pagestatus == statusVar.edit || (copyid && pagestatus == statusVar.add)) {

                let billid = props.form.getFormItemsValue("head", 'pk_jkbx').value || copyid || "";
                // window.open(window.location.origin+'/erm/expenseaccount/expenseaccount/card#status=browse&openbillid='+billid+'&tradetype=2641');
                // location.hash = `#status=${statusVar.browse}&billid=${billid}&tradetype=${billtype}`;
                //props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html',{status:statusVar.browse,id:billid,tradetype:billtype,appcode:appcode,pagecode:pagecode});
                let appcode = (this.props.getUrlParam("appcode") == undefined || this.props.getUrlParam("appcode") == '') ? this.props.getSearchParam("c") : this.props.getUrlParam("appcode");
                let pagecode = (this.props.getUrlParam("pagecode") == undefined || this.props.getUrlParam("pagecode") == '') ? this.props.getSearchParam("p") : this.props.getUrlParam("pagecode");
                let billdata = {};
                billdata.openbillid = billid;
                billdata.tradetype = window.presetVar.tradetype;
                billdata.uid = getCookie("userid");//这里以后从session中获取
                billdata.pagecode = pagecode;
                billdata.appcode = appcode;
                requestApi.viewBill({
                    data: billdata,
                    success: (data) => {
                        props.form.setAllFormValue({ [presetVar.head.head1]: data.head[presetVar.head.head1] });
                        window.presetVar.reloadHead = data.head[presetVar.head.head1];
                        //设置表体数据
                        Object.values(presetVar.body).forEach((body) => {
                            if (data.bodys[body]) {
                                debugLogs.timeStart('setTableData_' + body);
                                props.cardTable.setTableData(body, data.bodys[body]);
                                debugLogs.timeEnd('setTableData_' + body);
                            }
                        });
                        let sense = props.getUrlParam("scene");
                        if (sense && (sense == "approve" || sense == "zycl" || sense == "approvesce")) {
                            props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.browse, id: billid, tradetype: billtype, scene: sense });
                        } else {

                            props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.browse, id: billid, tradetype: billtype, pagecode: pagecode, appcode: appcode });
                        }
                    }
                });


            } else {
                window.onbeforeunload = null;
                window.top.close();

            }

        },
        [pageButton.billSubmit]: () => {
            let self = this;
            let arr=["264X-Cxx-002","264X-Cxx-008","264X-Cxx-012","264X-Cxx-016"];
            let cc=arr.includes(this.props.form.getFormItemsValue('head','djlxbm').value) ? true:false;
            promptBox({
                color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                title: "提交",
                content: "确认提交吗？",
                beSureBtnName:"是",
                cancelBtnName:"否",
                beSureBtnClick: cc?closeBalanceBox.bind(this):submitBox.bind(this)              
            })
        },
        [pageButton.pageCopy]: () => {//单据复制
            // location.hash = `#status=${presetVar.status.add}&copyFromBillId=${props.getUrlParam('billid')}&tradetype=${window.presetVar.tradetype}&appcode=${window.presetVar.appcode}&pagecode=${window.presetVar.pagecode}`;
            let items = this.state.items;
            let rows = props.cardTable.getAllRows('arap_bxbusitem');
            let ispull = "N";
            for (let i = 0; i < rows.length; i++) {
                let row = rows[i];
                if (row.values['pk_mtapp_detail'] != null
                    && row.values['pk_mtapp_detail'] != undefined
                    && row.values['pk_mtapp_detail'].value != null
                    && row.values['pk_mtapp_detail'].value != '') {
                    ispull = 'Y';
                    break;
                }
            }
            if (ispull == 'Y') {
                toast({ content: multiLang && multiLang.get("201102BCLF-0022"), color: 'warning' });
                return;
            } else {
                let  propsMeta = props.meta.getMeta();
                if(items.JNZF_head) {
                    propsMeta.JNZF_head.items = items.JNZF_head;
                }
                if(items.JWZF_head) {
                    propsMeta.JWZF_head.items = items.JWZF_head;
                }
                props.meta.setMeta({...propsMeta});
                props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', {
                    status: presetVar.status.add,
                    copyFromBillId: props.getUrlParam('billid') || props.getUrlParam('id'),
                    tradetype: window.presetVar.tradetype
                    // appcode:window.presetVar.appcode,
                    // pagecode:window.presetVar.pagecode
                });
                window.assingUsers = [];
                //复制出来的单据要先清空附件产生的单据pk
                this.accessorybillid = '';
                this.setState({ billId: '' });
                let multiLang = this.props.MutiInit.getIntl(2011);
                this.props.button.setButtonTitle('fileManager', multiLang && multiLang.get('201102BCLF-0040')/**'附件'**/ + ' ' + 0 + ' ');

            }

        },

        [pageButton.pageDel]: () => {//删除单据

            let param = {};
            param.openbillid = this.props.getUrlParam('billid') || this.props.getUrlParam('id');
            requestApi.deleteBill({
                data: param,
                success: (data) => {
                    if (data.success) {
                        window.onbeforeunload = null;
                        window.top.close();
                    }
                }
            });
        },


        [pageButton.billInvalid]: () => {//单据作废
            // props.modal.show(presetVar.pageButton.billInvalid);
            let param = {};
            param.openbillid = this.props.getUrlParam("id") || this.props.getUrlParam("billid");
            param.tradetype = window.presetVar.tradetype;
            param.pagecode = this.props.getSearchParam("p");
            requestApi.billInvalid({
                data: param,
                success: (data) => {
                    if (data.success) {
                        dataHandle(data.data.head.head, this.props, this.props.getUrlParam("status"));
                        toast({ content: multiLang && multiLang.get("201102BCLF-0054"), color: 'warning' });
                        return;
                    }
                }
            });
        },


        [pageButton.pagePrint]: () => {//打印单据
            let vdata = {};
            vdata.billId = props.getUrlParam('billid') || props.getUrlParam('id');
            vdata.billType = '2641';
            ajax({
                url: '/nccloud/erm/expenseaccount/ErmPrintValidaAction.do',
                data: vdata,
                success: (data) => {
                    if (!data.data.canPrint) {
                        NCMessage.create({ content: data.data.errMesg, color: 'error', position: 'bottom' });
                    } else {
                        print(
                            'pdf',  //支持两类: 'html'为模板打印, 'pdf'为pdf打印
                            '/nccloud/erm/expenseaccount/ErmJkbxPrintAction.do', //后台服务url
                            {
                                billtype: billtype,  //单据类型
                                funcode: (props.getUrlParam("appcode") == undefined || props.getUrlParam("appcode") == '') ? props.getSearchParam("c") : props.getUrlParam("appcode"),      //功能节点编码，即模板编码
                                nodekey: '2641_IWEB',     //模板节点标识
                                oids: [props.getUrlParam('id') || props.getUrlParam('billid')]    // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
                            }
                        )
                    }
                }
            })
        },

        [pageButton.imageUpload]: () => {//影像扫描

            let openbillid = props.getUrlParam('id') || props.getUrlParam('billid');
            let billdata = props.createExtCardData(props.meta.getMeta().code, "head", bodyarr);

            var billInfoMap = {};

            //基础字段 单据pk,单据类型，交易类型，单据的组织
            billInfoMap.pk_billid = openbillid;
            billInfoMap.pk_billtype = billdata.head.head.rows[0].values.pk_billtype.value;
            billInfoMap.pk_tradetype = billdata.head.head.rows[0].values.djlxbm.value;
            billInfoMap.pk_org = billdata.head.head.rows[0].values.pk_org.value;

            //影像所需 FieldMap
            billInfoMap.BillType = billdata.head.head.rows[0].values.djlxbm.value;
            billInfoMap.BillDate = billdata.head.head.rows[0].values.djrq.value;
            billInfoMap.Busi_Serial_No = billdata.head.head.rows[0].values.pk_jkbx.value;
            billInfoMap.pk_billtype = billdata.head.head.rows[0].values.pk_billtype.value;
            billInfoMap.OrgNo = billdata.head.head.rows[0].values.pk_org.value;
            billInfoMap.BillCode = billdata.head.head.rows[0].values.djbh.value;
            billInfoMap.OrgName = billdata.head.head.rows[0].values.pk_org.display;
            billInfoMap.Cash = billdata.head.head.rows[0].values.vat_amount.value;

            imageScan(billInfoMap, 'iweb');

        },


        [pageButton.imageShow]: () => {//影像查看

            let openbillid = props.getUrlParam('id') || props.getUrlParam('billid');
            let billdata = props.createExtCardData(props.meta.getMeta().code, "head", bodyarr);
            var billInfoMap = {};

            //基础字段 单据pk,单据类型，交易类型，单据的组织
            billInfoMap.pk_billid = openbillid;
            billInfoMap.pk_billtype = billdata.head.head.rows[0].values.pk_billtype.value;
            billInfoMap.pk_tradetype = billdata.head.head.rows[0].values.djlxbm.value;
            billInfoMap.pk_org = billdata.head.head.rows[0].values.pk_org.value;
            imageView(billInfoMap, 'iweb');
        },

        [pageButton.invoiceView]: () => {
            let props = this.props;
            const billId = props.form.getFormItemsValue("head", "pk_jkbx").value;
            const billCode = props.form.getFormItemsValue("head", "djbh").value;
            const pk_org = props.form.getFormItemsValue("head", "pk_org").value;
            const tradetype = window.presetVar.tradetype;
            const viewRandom = Math.random();

            this.setState({ sscrpInvoiceData: { billId, billCode, pk_org, tradetype, viewRandom } });
        },

        [pageButton.linkQueryFysq]: () => {
            let billId = this.state.billId || props.getUrlParam('billid') || props.getUrlParam("id");
            ajax({
                url: '/nccloud/erm/loanmanage/LinkFysqAction.do',
                data: {
                    tradetype: window.presetVar.tradetype,//交易类型
                    openBillId: billId//单据主键ok
                },
                success: result => {
                    if (result.data) {
                        if (result.data.tradetype && result.data.pk) {
                            linkQueryApi.link({
                                data: {
                                    openbillid: result.data.pk,
                                    tradetype: result.data.tradetype,
                                    props: this.props
                                }
                            });
                        }
                    }
                    else {
                        let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId
                        toast({ content: multiLang && multiLang.get('201102JCLF_C-0001'), color: 'warning' });
                    }
                }
            });
        },

        [pageButton.linkQueryFyyt]: () => {
            let billId = this.state.billId || props.getUrlParam('billid') || props.getUrlParam("id");
            ajax({
                url: '/nccloud/erm/expenseaccount/expenseaccountLinkQueryAction.do',
                data: {
                    queryType: 'fyyt',
                    tradetype: window.presetVar.tradetype,//交易类型
                    openBillId: billId//单据主键ok
                },
                success: result => {
                    if (result.data) {
                        this.setState({
                            linkData: result,
                            showLinkQueryFyytModel: true,
                            billId: billId,
                        })
                    } else {
                        let multiLang = this.props.MutiInit.getIntl(2011); //"您联查的费用预提单没有数据
                        toast({ content: multiLang && multiLang.get('201102JCLF_C-0006'), color: 'warning' });
                    }
                }
            });
        },

        [pageButton.linkQueryJkbx]: () => {
            let billId = this.state.billId || props.getUrlParam('billid') || props.getUrlParam("id");
            ajax({
                url: '/nccloud/erm/expenseaccount/expenseaccountLinkQueryAction.do',
                data: {
                    queryType: 'jkbx',
                    tradetype: window.presetVar.tradetype,//交易类型
                    openBillId: billId//单据主键ok
                },
                success: result => {
                    if (result.data) {
                        this.setState({
                            linkData: result,
                            showLinkQueryJkbxModel: true,
                            billId: billId
                        })
                    } else {
                        let multiLang = this.props.MutiInit.getIntl(2011); //"您联查的借款单没有数据
                        toast({ content: multiLang && multiLang.get('201102JCLF_C-0007'), color: 'warning' });
                    }
                }
            });
        },

        [pageButton.pageCjk]: () => {
            let hideTables = [];
            this.state.hideTables.forEach((item)=>{
                if(item != 'er_bxcontrast'){
                    hideTables.push(item);
                }
            });
            // 做冲借款操作前，调用setState方法将隐藏的"冲销明细"明细页签显示出来
            this.setState({hideTables:hideTables},()=>{
                let props = this.props;
                if (this.state.showCJKModal) {
                    this.setState({ showCJKModal: false });
                }
                else {
                    let amount = props.form.getFormItemsValue("head", "total").value;
                    if (parseFloat(amount) == 0) {
                        toast({ color: "danger", content: multiLang && multiLang.get("201102BCLF-0024") });
                    } else {
                        window.bxcontrastGrid = props.cardTable.getAllRows("er_bxcontrast");
                        let param = {};
                        param.tradetype = window.presetVar.tradetype;
                        param.pk_jkbx = props.form.getFormItemsValue("head", "pk_jkbx").value;
                        param.jkbxr = props.form.getFormItemsValue("head", "jkbxr").value;
                        param.receiver = props.form.getFormItemsValue("head", "receiver").value;
                        param.bzbm = props.form.getFormItemsValue("head", "bzbm").value;
                        param.djrq = props.form.getFormItemsValue("head", "djrq").value;
                        param.dwbm = props.form.getFormItemsValue("head", "dwbm").value;
                        if (props.form.getFormItemsValue("head", "pk_item").value) {
                            param.pk_item = props.form.getFormItemsValue("head", "pk_item").value;
                        } else {
                            param.pk_item = "";
                        }
                        let bodyBZ = [];
                        let bzjeMap = new Map();
                        let showData = props.createExtCardData(props.meta.getMeta().code, "head", bodyarr);
                        Object.keys(showData.bodys).forEach((item, index) => {
                            if (item == "arap_bxbusitem" || item == "bzitem" || item == "other" || item == "zsitem") {
                                showData.bodys[item].rows.forEach((row) => {
                                    if (row.values.bzbm && row.values.bzbm.value) {
                                        bodyBZ.push(row.values.bzbm);
                                        var amount = !row.values["vat_amount"].value ? "0" : row.values["vat_amount"].value;//取含税金额
                                        if (bzjeMap.has(row.values.bzbm.value)) {
                                            var oldAmount = parseFloat(bzjeMap.get(row.values.bzbm.value));
                                            bzjeMap.delete(row.values.bzbm.value);
                                            bzjeMap.set(row.values.bzbm.value, oldAmount + parseFloat(amount));
                                        } else {
                                            bzjeMap.set(row.values.bzbm.value, amount);
                                        }
                                    }
                                });
                            }
                        });
                        param.bzbms = bodyBZ;
                        param.total = showData.head.head.rows[0].values.total.value;
                        window.cjkdata = param;
                        window.bxjeMap = bzjeMap;
                        let szxmid=this.props.cardTable&&this.props.cardTable.getColValue("arap_bxbusitem", "szxmid",false,false);
                        let hbbm=this.props.form.getFormItemsValue("head", "hbbm");
                        let paytarget=this.props.form.getFormItemsValue("head","paytarget");
                        let tradetype = window.presetVar.tradetype;
                        let fctno=this.props.cardTable.getDataByIndex('arap_bxbusitem',0)&&this.props.cardTable.getDataByIndex('arap_bxbusitem',0).values&&this.props.cardTable.getDataByIndex('arap_bxbusitem',0).values.fctno;
                        this.setState({ showCJKModal: true ,szxmid,hbbm,fctno,paytarget,tradetype});

                    }

                }
            });
        },

        [pageButton.pageFyyt]: () => {
            let hideTables = [];
            this.state.hideTables.forEach((item)=>{
                if(item != 'accrued_verify'){
                    hideTables.push(item);
                }
            });
            // 做核销预提操作前，调用setState方法将隐藏的"核销预提"明细页签显示出来
            this.setState({hideTables:hideTables},()=>{
                let props = this.props;
                if (this.state.showFyytModal) {
                    this.setState({ showFyytModal: false });
                }
                else {
                    let amount = props.form.getFormItemsValue("head", "total").value;
                    if (parseFloat(amount) == 0) {
                        toast({ color: "danger", content: multiLang && multiLang.get("201102BCLF-0023") });
                        // NCMessage.create({content: multiLang && multiLang.get("201102BCLF-0023"), color: 'error', position: 'bottom'});
                    } else if (props.form.getFormItemsValue("head", "pk_item").value) {
                        toast({ color: "danger", content: multiLang && multiLang.get("201102BCLF-0046") });
                    } else if (props.form.getFormItemsValue("head", "iscostshare").value == true) {//分摊
                        toast({ color: "danger", content: multiLang && multiLang.get("201102BCLF-0047") });
                    } else if (props.form.getFormItemsValue("head", "isexpamt").value == true) {//待摊
                        toast({ color: "danger", content: multiLang && multiLang.get("201102BCLF-0048") });
                    } else {
                        window.accruedverifyGrid = props.cardTable.getAllRows("accrued_verify");
                        var param = {};
                        param.pk_jkbxr = props.form.getFormItemsValue("head", "jkbxr").value;
                        param.pk_bxd = props.form.getFormItemsValue("head", "pk_jkbx").value;
                        param.bzbm = props.form.getFormItemsValue("head", "bzbm").value;
                        param.pk_org = props.form.getFormItemsValue("head", "pk_org").value;
                        param.amount = props.form.getFormItemsValue("head", "total").value;
                        window.bxdata = param;
                        this.setState({ showFyytModal: true });
                    }

                }
            });
        },
        [pageButton.invoiceDzfp]: () => {
            let vdata = {};
            vdata.billId = props.getUrlParam('billid') || props.getUrlParam('id');
            vdata.billType = '2641';
            ajax({
                url: '/nccloud/erm/expenseaccount/ErmDzfpAction.do',
                data: vdata,
                success: (data) => {
                    if (!data.data.canOpenDzfp) {
                        // NCMessage.create({content:data.data.errMesg, color: 'error', position: 'bottom'});
                        toast({ content: data.data.errMesg, color: 'warning' });
                    } else {
                        let props = this.props;
                        const billId = props.form.getFormItemsValue("head", "pk_jkbx").value;
                        const billCode = props.form.getFormItemsValue("head", "djbh").value;
                        const pk_org = props.form.getFormItemsValue("head", "pk_org").value;
                        const tradetype = window.presetVar.tradetype;
                        const random = Math.random();

                        this.setState({ sscrpInvoiceData: { billId, billCode, pk_org, tradetype, random } });
                    }
                }
            })

        },


        [pageButton.fileManager]: () => {
            let props = this.props;
            let billId = props.getUrlParam('billid') || this.state.billId || "";
            let billtype = window.presetVar.tradetype || "";
            if (billId == "" && !this.state.showUploader) {
                //去后台生成单据主键
                requestApi.generatBillId({
                    data: { billtype: billtype },
                    success: (data) => {
                        console.log(data);
                        let accessorypkfildname = data["pkfieldName"];
                        billId = data[accessorypkfildname];
                        this.setState({
                            billId: billId,
                            showUploader: !this.state.showUploader
                        });
                        this.accessorybillid = billId;
                    },
                    error: (data) => {
                        toast({ content: data.message, color: 'danger' });
                    }
                });
            } else {
                this.setState({
                    billId: billId,
                    showUploader: !this.state.showUploader
                });
            }
        },
        [pageButton.pageEdit]: () => {
            let props = this.props;
            let meta = props.meta.getMeta();
            props.cardTable.showColByKey("arap_bxbusitem", 'opr');
            let scene = props.getUrlParam('scene');
            //获取单据编号
            let djbh = props.form.getFormItemsValue("head", ['djbh'])[0].value;
            let canEdit = true;
            let isMaker = true;
            //来源于审批中心
            if (scene == 'approve' || scene == 'approvesce') {
                isMaker = false;
                //判断单据是否是当前用户待审批
                ajax({
                    url: '/nccloud/riart/message/list.do',
                    async: false,
                    data: {
                        billno: djbh,
                        isread: 'N'
                    },
                    success: result => {
                        if (result.data) {
                            if (result.data.total < 1) {
                                toast({ content: multiLang && multiLang.get("201102BCLF-0026"), color: 'warning' });
                                canEdit = false;
                            }
                        }
                    }
                });
            }
            //来源于我的作业
            if (scene == 'zycl') {
                isMaker = false;
                //判断单据是否是当前用户待处理
                ajax({
                    url: '/nccloud/ssctp/sscbd/SSCTaskHandlePendingNumAction.do',
                    async: false,
                    data: {
                        billno: djbh
                    },
                    success: result => {
                        if (result.data) {
                            if (result.data.total < 1) {
                                toast({ content: multiLang && multiLang.get("201102BCLF-0027"), color: 'warning' });
                                canEdit = false;
                            }
                        }
                    }
                });
            }
            if (!canEdit) {
                return;
            }

            if (isMaker) {
                //驳回不重走流程单据，设置不可编辑字段
                requestApi.getUnEditAttributes({
                    data: {
                        'pk_org': props.form.getFormItemsValue("head", ['pk_org'])[0].value,
                        'billid': props.form.getFormItemsValue("head", ['pk_jkbx'])[0].value,
                        'billType': props.form.getFormItemsValue("head", ['djlxbm'])[0].value
                    },
                    success: (data) => {
                        if (!(data == undefined || data.length == 0)) {
                            let formobj = {};
                            let arap_bxbusitemobj = [];
                            let otherobj = [];
                            let zsitemobj = [];
                            let bzitemobj = [];
                            let er_cshare_detailobj = [];
                            let er_bxcontrastobj = [];
                            let accrued_verifyobj = [];

                            data.forEach((item) => {
                                if (item.indexOf('er_busitem.') > -1) {
                                    arap_bxbusitemobj.push(item.split('.')[1]);
                                    otherobj.push(item.split('.')[1]);
                                    zsitemobj.push(item.split('.')[1]);
                                    bzitemobj.push(item.split('.')[1]);
                                } else if (item.indexOf('er_cshare_detail.') > -1) {
                                    er_cshare_detailobj.push(item.split('.')[1]);
                                } else if (item.indexOf('er_bxcontrast.') > -1) {
                                    er_bxcontrastobj.push(item.split('.')[1]);
                                } else if (item.indexOf('accrued_verify.') > -1) {
                                    accrued_verifyobj.push(item.split('.')[1]);
                                } else {
                                    formobj[item] = true;
                                }
                            })
                            props.form.setFormItemsDisabled('head', formobj)
                            props.cardTable.setColEditableByKey('arap_bxbusitem', arap_bxbusitemobj)
                            props.cardTable.setColEditableByKey('other', otherobj)
                            props.cardTable.setColEditableByKey('zsitem', zsitemobj)
                            props.cardTable.setColEditableByKey('bzitem', bzitemobj)
                            props.cardTable.setColEditableByKey('er_cshare_detail', er_cshare_detailobj)
                            props.cardTable.setColEditableByKey('er_bxcontrast', er_bxcontrastobj)
                            props.cardTable.setColEditableByKey('accrued_verify', accrued_verifyobj)
                        }
                    }
                })
            }

            if(props.form.getFormItemsValue("head", ['djzt'])[0].value == 1) {
                props.button.setButtonDisabled({
                    ['pageTempSave']: true,
                });
            }
            // BTM 金额字段不需要显示默认值
            props.meta.setMeta(meta);
            // location.hash = `#status=${statusVar.edit}&billid=${this.props.getUrlParam('id')||props.getUrlParam('billid')}&tradetype=${window.presetVar.tradetype}`;
            window.presetVar.redata = 'N';
            props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', {
                status: statusVar.edit,
                id: this.props.getUrlParam('id') || props.getUrlParam('billid'),
                scene: props.getUrlParam('scene'),
                tradetype: window.presetVar.tradetype
                // appcode:window.presetVar.appcode,
                // pagecode:window.presetVar.pagecode,

            });
            
            // props.button.setButtonsVisible({
            //     [pageButton.pageSave]: true,
            //     [pageButton.pageEdit]: false
            // });
            // props.billBodys.forEach((body)=>{
            //     props.editTable.setStatus(body, 'edit');
            // });
        }, [pageButton.billRecall]: () => {//收回
            let param = {};
            param.openbillid = this.props.getUrlParam('id') || this.props.getUrlParam('billid');
            param.pagecode = this.props.getSearchParam("p");
            requestApi.billRecall({
                data: param,
                success: (data) => {
                    let billid = data.head.head.rows[0].values.pk_jkbx.value;
                    // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                    // props.form.setAllFormValue({[presetVar.head.head1]: requestApi.filterDatas(data.head[presetVar.head.head1])});
                    updataFormData(props, presetVar.head.head1, requestApi.filterDatas(data.head[presetVar.head.head1]));
                    // BTM 性能优化，变更页面数据更新方法
                    bodyarr.forEach((item) => {
                        // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                        // debugLogs.timeStart('setTableData_' + item);
                        // props.cardTable.setTableData(item, requestApi.filterDatas(data.bodys[item]));
                        // debugLogs.timeEnd('setTableData_' + item);
                        updatacardTableData(props, item, requestApi.filterDatas(data.bodys[item]));
                        // BTM 性能优化，变更页面数据更新方法
                    })
                    // location.hash = `#status=${statusVar.browse}&id=${billid}&tradetype=${window.presetVar.tradetype}&flow=recall`;
                    window.presetVar.redata = 'N';
                    // 更新head,避免收回后页面按钮状态错误
                    window.presetVar.reloadHead = data.head.head;

                    // props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', {
                    //     status:statusVar.browse,
                    //     id:this.props.getUrlParam('id')||props.getUrlParam('billid'),
                    //     tradetype:window.presetVar.tradetype,
                    //     flow:"recall"
                    // });
                    dataHandle(data.head.head, this.props, this.props.getUrlParam("status"));
                    // props.button.setButtonsVisible({
                    //     [pageButton.pageSave] : false,
                    //     [pageButton.pageEdit] : true
                    // });
                }
            });
        },
        [pageButton.linkVoucher]: () => {
            let paramurl = getRequest1(parent.window.location.hash);
            //如果没取到应用主键再取一下外层,主要适配作业任务打开单据时联查凭证
            if (!paramurl.ar) {
                paramurl = getRequest1(parent.parent.window.location.hash);
            }
            if (paramurl.ar) {
                let billdata = props.createExtCardData(props.meta.getMeta().code, "head", bodyarr);
                if (billdata.head.head.rows.length) {
                    linkVoucherApi.link({
                        data: {
                            props: props,
                            record: {
                                pk_billtype: billdata.head.head.rows[0].values.djlxbm.value,
                                pk_group: billdata.head.head.rows[0].values.pk_group.value,
                                pk_org: billdata.head.head.rows[0].values.pk_org.value,
                                relationID: billdata.head.head.rows[0].values.pk_jkbx.value
                            },
                            appid: paramurl.ar
                        }
                    })
                } else {
                    toast({ content: multiLang && multiLang.get('201102BCLF-0028'), color: 'danger' });
                }
            }
        },
        [pageButton.linkBudget]: () => {
            let billdata = props.createExtCardData(props.meta.getMeta().code, "head", bodyarr);
            if (billdata.head.head.rows.length) {
                linkBudgetApi.link({
                    data: {
                        "tradetype": billdata.head.head.rows[0].values.djlxbm.value,
                        "openbillid": billdata.head.head.rows[0].values.pk_jkbx.value
                    },
                    success: (data) => {
                        this.setState({
                            sourceData: data,
                            showLinkBudget: true
                        })
                    }
                })
            }
        },
        [pageButton.billApprove]: () => {
            let billdata = props.createExtCardData(props.meta.getMeta().code, "head", bodyarr);
            this.setState({
                ShowApproveDetail: true,
                billId: billdata.head.head.rows[0].values.pk_jkbx.value,
                billtype: billdata.head.head.rows[0].values.djlxbm.value
            });
        },
        ["LinkQueryJsd"]: () => {
            let billdata = props.createExtCardData(props.meta.getMeta().code, "head", bodyarr);
            requestApi.linkqueryjsd({
                data: billdata,
                success: (data) => {
                    console.log(data);
                    props.openTo(data.data.ys.url, { "id": data.data.ys.id, "pagecode": data.data.ys.pagecode, "appcode": data.data.ys.appcode, "scene": data.data.ys.scene, "status": data.data.ys.status });
                    props.openTo(data.data.yf.url, { "id": data.data.yf.id, "pagecode": data.data.yf.pagecode, "appcode": data.data.yf.appcode, "scene": data.data.yf.scene, "status": data.data.yf.status });
                }
            });
        }
    }
}
export default pageButtonClick;

