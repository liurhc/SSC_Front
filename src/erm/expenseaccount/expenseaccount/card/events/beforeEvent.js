import {ajax,base} from 'nc-lightapp-front';
import requestApi from '../requestApi';
// let referResultFilterCondition='';
const {NCMessage} = base;
//只对表体做编辑前事件处理
export default function beforeEvent(props,moduleId,key,value,index,changedrows) {
    // let bodys = Object.values(window.presetVar.body);
    // let bodyarr = [];
    // let hideTable = this.state.hideTables;
    // bodys.map((one)=>{
    //     if(hideTable.indexOf(one)<0){
    //         bodyarr[bodyarr.length] = one;
    //     }
    // })
    // let billdata = props.createExtCardData(window.presetVar.pageId, window.presetVar.head.head1,bodyarr);
    // let moduleid =moduleId =="head"?"headform":moduleId;
    // let referResultFilterCondition='';
    // if(moduleid=="head"){
    //     return true;
    // }
    // let meta = props.meta.getMeta();
    // let bodyValues = changedrows.values;
    // let headValues = billdata.head[presetVar.head.head1].rows[0].values;
    // let attrcode  = key;
    //     meta[moduleId].items.forEach((item) => {
    //         if (item.attrcode == attrcode&&item.itemtype == 'refer') {
    //             item.initialvalue = {
    //                 value: bodyValues[attrcode].value,
    //                 display: bodyValues[attrcode].display = null ? bodyValues[attrcode].value : bodyValues[attrcode].display
    //             };
    //             // let filterCondition = referResultFilterCondition[moduleId][item.attrcode];
    //             // if (filterCondition) {
    //             //     item.referFilterAttr = {[item.attrcode]: filterCondition};
    //             // }
    //             //处理交叉校验规则和参照过滤
    //             requestApi.filterAllRefParm(item, bodyValues, headValues,props,moduleId);
    //         }

    //     });
    //     meta[moduleId+"&childform2"].items.forEach((item) => {
    //         if (item.attrcode == attrcode&&item.itemtype == 'refer') {
    //             item.initialvalue = {
    //                 value: bodyValues[attrcode].value,
    //                 display: bodyValues[attrcode].display = null ? bodyValues[attrcode].value : bodyValues[attrcode].display
    //             };
    //             // let filterCondition = referResultFilterCondition[moduleId][item.attrcode];
    //             // if (filterCondition) {
    //             //     item.referFilterAttr = {[item.attrcode]: filterCondition};
    //             // }
    //             //处理交叉校验规则和参照过滤
    //             requestApi.filterAllRefParm(item, bodyValues, headValues,props,moduleId);
    //         }

    //     });

    //add by ouyanggx 将车辆费用报销单的defitem13设置成不能选择非末级
    let meta = props.meta.getMeta();
    meta['arap_bxbusitem'].items.map((item) => {
        var attrcode = item.attrcode;
        if(attrcode == key){
            switch(attrcode) {
                case 'defitem13':

                    if(window.presetVar.tradetype == '264X-Cxx-004') {
                        item.onlyLeafCanSelect = true;
                    }
                    
                    break;
                default:
                    break;
            }
        }
    });
    // add end


        window.presetVar.bodyChangeValuesAll = changedrows;
        return true;
};