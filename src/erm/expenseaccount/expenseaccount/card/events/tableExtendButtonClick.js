/**
 * 表格扩展按钮click事件
 */
import { headSumHandle } from "./commonEvent";
import logs from 'ssccommon/utils/logs';
import afterEvent from './afterEvent';
const body = window.presetVar.body;
let debugLogs = new logs();

function tableExtendButtonClick() {
    debugLogs.log('tableExtendButtonClick');
    let returnObj = {};
    for (let name in window.presetVar.body) {
        debugLogs.log('tableExtendButtonClick:' + name);
        let areaId = window.presetVar.body[name];
        if (areaId !== 'accrued_verify' && areaId !== 'er_bxcontrast' && areaId !== 'er_cshare_detail') {
            returnObj[areaId + '_Delete'] = {
                afterClick: () => {
                    headSumHandle.call(this);
                }
            };
            returnObj[areaId + '_Copy'] = {
                afterClick: () => {
                    headSumHandle.call(this);
                    // let changedrows =  this.props.cardTable.getRowsByIndexs(body.body4, 0);
                    // let value = this.props.cardTable.getValByKeyAndIndex(body.body4, 0,'amount');
                    // let prop = this.props;
                    // afterEvent.call(this,prop,body.body4,'amount',value , changedrows);
                    let key = 'vat_amount'
                    let moduleId = 'arap_bxbusitem'
                    let rows =  this.props.cardTable.getRowsByIndexs(moduleId, 0);
                    let value = this.props.cardTable.getValByKeyAndIndex(moduleId, 0,key);
                    let changedrows = [{
                        newvalue:{value:value.value},
                        oldvalue:{value:0},
                        rowid:rows[0].rowid            
                    }]
                    afterEvent.call(this,this.props,moduleId,key,value,changedrows);
                }
            };
            returnObj[areaId + '_Edit'] = {
                afterClick: (record, index) => {
                    // this.props.cardTable.openModel(body1, statusVar.edit, record, index);
                    this.state.modelEdit =  this.props.getUrlParam("status") != 'browse';
                }
            },
            // 填写明细按钮功能 -20190831 -start
            returnObj['show_detail'] = (record, index) => {

                const bodyAreaCode = 'arap_bxbusitem'; // 表体区域编码
                var meta = this.props.meta.getMeta();
                var buttons = this.props.button.getButtons();

                if (!this.state.detailButtonTag) { // 点击 "填写明细" 按钮
                    const transAreaCode = 'jtf&childform2'; // 交通区域编码
                     const accomAreaCode = 'zsf&childform2'; // 住宿费区域编码
                     const mealsAreaCode = 'cf&childform2'; // 餐费区域编码
                     const otherAreaCode = 'qt&childform2'; // 其他费区域编码
                    // 记录原来的显示字段
                    let itemVisibleMap = {};
                    meta[bodyAreaCode] && meta[bodyAreaCode].items.forEach((item) => {
                        itemVisibleMap[item.attrcode] = item.visible;
                    });
                    this.setState({ detailButtonTag: itemVisibleMap });

                    // 获取侧拉展开中四个区域中的 "可见或可修订" 字段，记录到Set中
                    let itemVisibleTag = new Set();
                    meta[transAreaCode] && meta[transAreaCode].items.forEach((item) => {
                        if (item.visible || item.isrevise) {
                            itemVisibleTag.add(item.attrcode);
                        }
                    });
                    meta[accomAreaCode] && meta[accomAreaCode].items.forEach((item) => {
                        if (item.visible || item.isrevise) {
                            itemVisibleTag.add(item.attrcode);
                        }
                    });
                    meta[mealsAreaCode] && meta[mealsAreaCode].items.forEach((item) => {
                        if (item.visible || item.isrevise) {
                            itemVisibleTag.add(item.attrcode);
                        }
                    });
                    meta[otherAreaCode] && meta[otherAreaCode].items.forEach((item) => {
                        if (item.visible || item.isrevise) {
                            itemVisibleTag.add(item.attrcode);
                        }
                    });
                    // 根据set中的记录，设置表体字段的可见性
                    meta[bodyAreaCode] && meta[bodyAreaCode].items.forEach((item) => {
                        if (itemVisibleTag.has(item.attrcode)) {
                            item.visible = true;
                        }
                    });
                    // 按钮控制
                    buttons.forEach((button) => {
                        if (button.key == 'show_detail') {
                            button.title = '返回';
                        }
                    });
                    this.props.meta.setMeta(meta);
                } else { // 点击 "返回" 按钮
                    this.setState({ detailButtonTag: null });
                    // 还原字段原来显示性
                    meta[bodyAreaCode] && meta[bodyAreaCode].items.forEach((item) => {
                        item.visible = this.state.detailButtonTag[item.attrcode];
                    });
                    // 按钮控制
                    buttons.forEach((button) => {
                        if (button.key == 'show_detail') {
                            button.title = '展开';
                        }
                    });
                    this.props.meta.setMeta(meta);
                }
            } // 填写明细按钮功能 -20190831 -end

        }
    }
    return returnObj;
}

export default tableExtendButtonClick;