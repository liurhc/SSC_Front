/**
 * 电子发票操作
 */
import { ajax,base ,toast} from 'nc-lightapp-front';
import formDownload from './formDownload';
const {NCMessage} = base;

let done = function(info,response){
    let multiLang = this.state.multiLang;
    if(info.file.status === 'done'){
        let vRep;
        if(response.data){
            vRep = response.data;
        }else{
            vRep = info.file.response.data;
        }
        if(vRep.ret){
            this.setState({ fileList: vRep.viewList});
            NCMessage.create({content: multiLang['1056invoice-0010'], color: 'success', position: 'bottom'});
        }else{
            toast({ content: multiLang['1056invoice-0011'] + vRep.emsg, color: 'danger' });
        }
    }
}

let del = function (text, record, index) {
    let multiLang = this.state.multiLang;
    let vdata = {};
    let billId =  this.props.getUrlParam('id')||this.props.getUrlParam('billid');
    vdata.billId = billId;
    vdata.pk_invoice = record.pk_invoice;
    vdata.invoice_code = record.invoice_code;
    vdata.invoice_number = record.invoice_number;
    vdata.file_name = record.file_name;

    ajax({
        url: '/nccloud/sscrp/invoice/DeleteInvoiceAction.do',
        data: vdata,
        success: (data) => {
            if (data.data.ret){
                this.setState({ fileList: data.data.viewList});
                NCMessage.create({content: multiLang && multiLang.get('201102BCLF-0038'), color: 'success', position: 'bottom'});
            }else {
                NCMessage.create({content: multiLang && multiLang.get('201102BCLF-0025')+data.data.retMesg, color: 'error', position: 'bottom'});
            }

        }
    })
}


let beforeDelete = async function (file, fullPath, billId) {
    let props = this.props;
    let multiLang = props.MutiInit.getIntl(2011);
    let vdata= {};
    vdata.billId = billId;
    var canDel = true;
    await ajax({
        url: '/nccloud/erm/expenseaccount/ExpenseaccountDzfpBeforeValidateAction.do',
        data: vdata,
        async:false,
        success: (data) => {
            if (!data.data.ret){
                toast({ content: multiLang && multiLang.get("201102BCLF-0039"), color: 'danger' });
                canDel =  false;
            }
        }
    })
    return canDel;
}

let view = function (text, record, index) {
    let props = this.props;
    let multiLang = props.MutiInit.getIntl(2011);
    let vdata = {};
    let billId = this.props.getUrlParam("id")|| this.props.getUrlParam("billid");
    vdata.billId = billId;
    vdata.invoice_code = record.invoice_code;
    vdata.invoice_number = record.invoice_number;
    vdata.pk_doc = record.pk_doc;

    let params = {};
    let isdoc= [];
    let pk_doc= [];
    let name= [];
    isdoc.push("z");
    pk_doc.push(record.pk_doc);
    name.push(record.file_name);
    params.isdoc =isdoc;
    params.pk_doc = pk_doc;
    params.name = name;
    formDownload({
        params,
        url: '/nccloud/sscrp/invoice/PreViewInvoiceAction.do',
        enctype: 1
    })

    /*ajax({
        url: '/nccloud/erm/expenseaccount/ExpenseaccountPreViewDzfp.do',
        data: vdata,
        success: (data) => {
            if (data.data){
                this.state.showdzpw = true;
                this.state.showDZFPUrl = data.data;
                NCMessage.create({content: '执行成功', color: 'success', position: 'bottom'});
            }else {
                NCMessage.create({content: '执行失败:'+data.data.retMesg, color: 'error', position: 'bottom'});
            }

        }
    })*/
}

export default {view,del,done,beforeDelete}
