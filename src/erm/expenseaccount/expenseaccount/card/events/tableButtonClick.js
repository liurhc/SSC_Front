/**
 * 表格肩部按钮click事件
 */
import {ajax,formDownload, getMultiLang} from 'nc-lightapp-front';
const areaBtnAction = window.presetVar.areaBtnAction;
const statusVar = window.presetVar.status;
import afterEvent from './afterEvent';
const body = window.presetVar.body;
import { headSumHandle } from "./commonEvent";


function tableButtonClick() {

    return {
        ['arap_bxbusitem-add'] : () => {
            let data = this.props.createExtCardData('20110ETEA_2641_IWEB', 'head', 'arap_bxbusitem');
            
            this.props.cardTable.addRow('arap_bxbusitem', undefined, null, false);
        },
        ['fpxx_bxbusitem_del'] : () => {
            let data = this.props.cardTable.getCheckedRows("other");
            //选中的发票号码
            let fphm = [];
            data.forEach(item => {
                let value = item.data.values.defitem7.value.substring(0,item.data.values.defitem7.value.indexOf("-"))==""?item.data.values.defitem7.value:item.data.values.defitem7.value.substring(0,item.data.values.defitem7.value.indexOf("-"));
                if(fphm.indexOf(value) == -1) {
                    fphm.push(value);  //所有选中的数据 筛选出发票号码 一条发票号码对多条明细
                }
            })

            let allRowsData = this.props.cardTable.getAllRows("other");
            let deleteData = [];
            fphm.map(fphmitem => {
                allRowsData.forEach(item => {
                    let value = item.values.defitem7.value.substring(0,item.values.defitem7.value.indexOf("-"))==""?item.values.defitem7.value:item.values.defitem7.value.substring(0,item.values.defitem7.value.indexOf("-"));
                    if(fphmitem == value) {
                        deleteData.push(item);
                    }
                })
            })
            //let fphm = data[0].data.values.defitem6.value;
            //删除所有选中的发票号码相同的发票
            deleteData.forEach((item)=>{
                this.props.cardTable.delRowByRowId("other", item.rowid);
            })
            
        },
        ['arap_bxbusitem_del'] : () => {
            let data = this.props.cardTable.getCheckedRows("arap_bxbusitem");
            let moduleId = 'arap_bxbusitem'
            
            window.__ssc_BillStyleVar.tableRowAndItemCount(moduleId, 'delRows', 0);

            data.forEach((item)=>{
                this.props.cardTable.delRowByRowId("arap_bxbusitem", item.data.rowid);
            })
            let key = 'vat_amount'

            let rows =  this.props.cardTable.getRowsByIndexs(moduleId, 0);
            if(rows.length == 0) {
                // this.props.cardTable.setValByKeyAndIndex('arap_bxbusitem',index,'vat_amount',{value:0,display:0})
                headSumHandle.call(this);
            }else {
                let value = this.props.cardTable.getValByKeyAndIndex(moduleId, 0,key);
                let changedrows = [{
                    newvalue:{value:value.value},
                    oldvalue:{value:0},
                    rowid:rows[0].rowid            
                }]
                afterEvent.call(this,this.props,moduleId,key,value,changedrows);
            }
            
            
        },
        ["er_cshare_detail_Export"]:()=>{
            let props = this.props;
            let tabledata = [];
            let formdata = props.createExtCardData(props.meta.getMeta().pageid, "head", ['er_cshare_detail']);
            let rows = this.props.cardTable.getVisibleRows('er_cshare_detail')
            formdata.bodys.er_cshare_detail.rows=rows;//过滤表体已经删除的行
            props.meta.getMeta().er_cshare_detail.items.forEach((item) =>{
                if(item.visible && item.visible===true && item.attrcode !="opr"){
                    tabledata.push({"key":item.attrcode,"label":item.label});
                }
            });
            formdata.userjson = JSON.stringify(tabledata);
            console.log(formdata);
            const [attrs, el_form] = [
                {
                    target: '_blank',
                    method: 'POST',
                    enctype:'application/x-www-form-urlencoded',
                    type: 'hidden',
                    action: '/nccloud/erm/expenseaccount/ExpenseaccountDownLoadAction.do'
                },
                document.createElement('form')
            ];
            Object.assign(el_form, attrs);

            const input = document.createElement('input');
            input.type = 'hidden';
            input.name = "data";
            input.value = JSON.stringify(formdata);
            //el_form.append(input);
            el_form.appendChild(input);
            document.body.appendChild(el_form);
            el_form.submit();
            document.body.removeChild(el_form);
            return false;
        }

    }
}

export default tableButtonClick;