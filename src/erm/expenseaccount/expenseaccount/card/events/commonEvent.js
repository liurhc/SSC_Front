const { body1} = window.presetVar.body;
const {head1} = window.presetVar.head;

/**
 * 表头合计处理
 */
export function headSumHandle() {
    //需要汇总的金额字段
    let bodyFieldArr = ["amount", "ybje", "cjkybje", "zfybje", "hkybje", "bbje",
        "cjkbbje", "zfbbje", "hkbbje", "globalbbje", "globalcjkbbje", "globalzfbbje", "globalhkbbje",
        "groupbbje", "groupcjkbbje", "groupzfbbje", "grouphkbbje", "tax_amount", "orgtax_amount", "grouptax_amount", "globaltax_amount",
        "tni_amount", "orgtni_amount", "grouptni_amount", "globaltni_amount", "vat_amount", "orgvat_amount", "groupvat_amount", "globalvat_amount"];
    let changeHeadItem = {};
    bodyFieldArr.forEach((bodyField) => {//循环字段
        let totalMny = 0;
        for(let name in window.presetVar.body){//循环业务页签
            let areaId = window.presetVar.body[name];
            if(areaId!=='accrued_verify' && areaId!=='er_bxcontrast' && areaId!=='er_cshare_detail'){
                let rows = this.props.cardTable.getVisibleRows(areaId);
                for(let i in rows){
                    if(rows[i].values[bodyField]!=undefined && rows[i].values[bodyField]!=null && rows[i].values[bodyField].value!=null )
                    totalMny += +rows[i].values[bodyField].value;//汇总
                }
            }
        }
        let headField = bodyField=="amount"?"total":bodyField;//total字段特殊处理
        let scale = this.props.form.getFormItemsValue(head1,headField).scale;//取精度
        changeHeadItem[headField] = {scale: scale, value: totalMny, display: null};
    });
    this.props.form.setFormItemsValue(head1, changeHeadItem)
}
