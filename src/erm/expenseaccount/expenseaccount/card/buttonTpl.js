/**
 *  前端写死的按钮模板。
 *  正常的按钮模板获取应该通过 props.createUIDom 获得
 */
let buttonTpl = {
    button: [
        {
            "id": "00452A435312094d0006J5B2",
            "type": "button",
            "key": "pageEdit",
            "title": "修改",
            "area": "head",
            "children": []
        },
        {
            "id": "00452A410000054d0006J5B2",
            "type": "button",
            "key": "pageCopy",
            "title": "复制",
            "area": "head",
            "children": []
        },
        {
            "id": "0001A410000054d0006J5B2",
            "type": "button",
            "key": "pageDel",
            "title": "删除",
            "area": "head",
            "children": []
        },
        {
            "id": "00452A43531zdbc094d0006J5B3",
            "type": "button_main",
            "key": "billSubmit",
            "title": "提交",
            "area": "head",
            "children": []
        },
        {
            "id": "0001A41000000006J982",
            "type": "button_main",
            "key": "billRecall",
            "title": "收回",
            "area": "head",
            "children": []
        },
        {
            "id": "0001A41000bc00000622J5B2",
            "type": "button_main",
            "key": "mtapp_detail-save",
            "title": "保存",
            "area": "body-shoulder",
            "children": []
        },
        {
            "id": "00452A435312094d0006J5B2",
            "type": "button",
            "key": "pagePrint",
            "title": "打印",
            "area": "head",
            "children": []
        },
        {
            "id": "00452A4yxsm35312094d0006J5B2",
            "type": "button",
            "key": "imageUpload",
            "title": "影像扫描",
            "area": "head",
            "children": []
        },
        {
            "id": "00452A4yxck35312094d0006J5B2",
            "type": "button",
            "key": "imageShow",
            "title": "影像查看",
            "area": "head",
            "children": []
        },
        {
            "id": "00452A4lcfp35312094d0006J5B2",
            "type": "button",
            "key": "invoiceView",
            "title": "联查发票",
            "area": "head",
            "children": []
        },
        {
            "id": "00452A43531zdbc094d0006J5B2",
            "type": "button",
            "key": "pageSave",
            "title": "保存",
            "area": "head",
            "children": []
        },
        
        {
            "id": "00452A43531zdbc094d0006J5B4",
            "type": "button",
            "key": "fileManager",
            "title": "附件管理",
            "area": "head",
            "children": []
        },
        {
            "id": "00452A43531zdbc094d0006J5C4",
            "type": "button",
            "key": "invoiceDzfp",
            "title": "电子发票",
            "area": "head",
            "children": []
        },
        {
            "id": "0001A4100000000622J5B2",
            "type": "button",
            "key": "mtapp_detail-add",
            "title": "新增",
            "area": "body-shoulder",
            "children": []
        },
        {
            "id": "00452A43531zdbc094d0006J5B3",
            "type": "button",
            "key": "pageCjk",
            "title": "冲借款",
            "area": "head",
            "children": []
        },
        {
            "id": "00452A43531zdbc094d0006J5B4",
            "type": "button",
            "key": "pageFyyt",
            "title": "核销预提",
            "area": "head",
            "children": []
        },
        {
            "id": "0001A41000000006J5B2",
            "type": "button",
            "key": "billInvalid",
            "title": "作废",
            "area": "head",
            "children": []
        },
        {
            "id": "00452A4fysq35312094d0006J5B2",
            "type": "button",
            "key": "linkQueryFysq",
            "title": "联查费用申请单",
            "area": "head",
            "children": []
        },
        {
            "id": "00452A4fyyt35312094d0006J5B2",
            "type": "button",
            "key": "linkQueryFyyt",
            "title": "联查费用预提单",
            "area": "head",
            "children": []
        },
        {
            "id": "00452A4jkbx35312094d0006J5B2",
            "type": "button",
            "key": "linkQueryJkbx",
            "title": "联查借款单",
            "area": "head",
            "children": []
        },
        /*{
            "id": "00452A4jkbx35312094d0006J5B2",
            "type": "button",
            "key": "linkBudget",
            "title": "联查预算",
            "area": "head",
            "children": []
        },*/
        {
            "id": "00452A4jkbx35312094d0006J5B2",
            "type": "button",
            "key": "LinkVoucher",
            "title": "联查凭证",
            "area": "head",
            "children": []
        },
        {
            "id": "0001Abj4100000000622J5B2",
            "type": "button_secondary",
            "key": "LinkBudget",
            "title": "联查预算",
            "area": "head",
            "children": []
        }
    ]
}

export default buttonTpl;