import './presetVar';
import React, { Component, PureComponent,Fragment } from 'react';
import { createPage, base, high, promptBox, getAttachments, toast } from 'nc-lightapp-front';
import { tableButtonClick, pageButtonClick, beforeEvent, afterEvent, invoiceEvent, refreshButtonClick } from './events';
import initTemplate from './initTemplate';

import LinkQueryFyytModel from 'ssccommon/linkquery/LinkQueryFyytModel';
import InvoiceUploader from 'sscrp/rppub/components/invoice-uploader'
import InvoiceLink from 'sscrp/rppub/components/invoice-link'
import { getAfterEventParams } from 'ssccommon/utils/formulaUtils'
import AllUserGridRef from '../../../../sscrp/refer/rpbill/AllUserGridRef/index.js'
import requestApi from "./requestApi";
import { updataFormData, updatacardTableData } from 'ssccommon/utils/changePageData';
import GetPageData from 'ssccommon/utils/getPageData';
import pubMessage from 'ssccommon/utils/pubMessage';
import getAttachmentLength from '../../../public/components/linkAttachment/getAttachmentLength';
import Cjk from '../../expenseaccount4Cjk/card/index.js';
import Fyyt from '../../expenseaccount4Fyyt/card/index.js';
import ShowNum from '../../../../sscrp/rpbill/rpbill/list/components/showNum';
import AttachmentOther from './AttachmentOther';
let { NCModal, NCButton, NCPopover, NCIcon, NCTable} = base;
import {
    BillStyle,
    BillHead,
    BillBody,
    Form,
    CardTable,
    CardTableGroup,
    ButtonGroup,
    MultiButtonGroup
} from 'ssccommon/components/bill';

import './index.less';
import LinkQueryJkbxModel from 'ssccommon/linkquery/LinkQueryJkbxModel';
import { renderButtions } from './initTemplate';
const { NCUploader, Inspection, ApproveDetail, ApprovalTrans } = high;

const pageButton = window.presetVar.pageButton;
pageButton.pageCjk = "pageCjk";
pageButton.pageFyyt = "pageFyyt";
let ntbCheck = 'false';
function getRequest1(url) {
    if (!url)
        url = document.location.search;
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substring(url.indexOf("?") + 1);
        var strs = str.split("&");
        for (var i = 0; i < strs.length; i++) {
            theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
        }
    }
    return theRequest;
}

window.assingUsers = [];
window.sureAssign = false;//确定指派
window.assignBillId = '';//有指派的情况下，第一次点保存提交实际保存成功的单据pk

window.bxcontrastGrid = {};
window.accruedverifyGrid = {};

class jkbxBillCard extends Component {
    constructor(props) {
        super();
        this.state = {
            detailButtonTag:null, // 填写明细按钮会显示一些字段，再次点击时返回，所以这里记录原来的字段显示性 -20190902
            uploadAlready: false, // 发票查验 附件上传。
            showCJKModal: false,
            showFyytModal: false,
            showUploader: false,
            modalControl: false,
            fileList: [],
            target: null,
            billId: '',
            jointBillShow: false,
            showLinkQueryFysqModel: false,
            showLinkQueryFyytModel: false,
            showLinkQueryJkbxModel: false,
            sourceData: null,
            showLinkBudget: false,
            allUserGridRefValue: {},
            showBillShare: false,
            hideTables: [],
            ShowApproveDetail: false,
            billtype: '',
            compositedata: '',
            compositedisplay: '',
            transtype_name: '',
            linkData: '',
            groupLists: [],
            iss: false,
            sscrpInvoiceData: {},
            items:{}, // 维护表头隐藏区域items -2019-09-05
            myTabdata:{},
            tableon:true,
            styles:{
                'display':'block'
            },
            btnclass:'iconfont icon table-tabs-icon icon-bottom'
        }
        this.uploadDoneuse = this.uploadDoneuse.bind(this);
        this.refbind = this.refbind.bind(this);
        this.pubMessage = new pubMessage(props);
        this.getPageDataUtil = new GetPageData(props, ['bbhl', 'tax_rate', 'groupbbhl', 'globalbbhl', 'rowno']);
        this.closecjk = this.closecjk.bind(this);
        this.closefyyt = this.closefyyt.bind(this);
        this.cjk = this.cjk.bind(this);
        this.submitBox = this.submitBox.bind(this);
        this.save = this.save.bind(this);
        this.submit = this.submit.bind(this);
        this.multiLangJson = '';
        initTemplate.call(this, props);
        window.pageCjkClick = (cjkdata) => {
            console.log(this.props);
            console.log(props);
            let bodys = Object.values(window.presetVar.body);
            let bodyarr = [];
            let hideTable = this.state.hideTables;
            bodys.map((one) => {
                if (hideTable.indexOf(one) < 0) {
                    bodyarr[bodyarr.length] = one;
                }
            })
            let appcode = (props.getUrlParam("appcode") == undefined || props.getUrlParam("appcode") == '') ? props.getSearchParam("c") : props.getUrlParam("appcode");
            let pagecode = (props.getUrlParam("pagecode") == undefined || props.getUrlParam("pagecode") == '') ? props.getSearchParam("p") : props.getUrlParam("pagecode");

            let billdata = props.createExtCardData(pagecode, "head", bodyarr);
            billdata.head.userjson = JSON.stringify(cjkdata);
            this.setState({ showCJKModal: false });
            billdata.templetid = props.meta.getMeta().pageid;
            requestApi.BillCjk({
                data: billdata,
                success: (data) => {
                    props.form.setAllFormValue({ [presetVar.head.head1]: data.head[presetVar.head.head1] });
                    Object.keys(data.bodys).forEach((item) => {
                        let newitemdata = data.bodys[item];
                        if (item === 'er_bxcontrast') {
                            newitemdata && newitemdata.rows && newitemdata.rows.forEach((itemData) => {
                                itemData.values.cjkybje.scale = itemData.values.cjkybje.value.split('.')[1].length;
                            })
                            props.cardTable.setStatus(item, window.presetVar.status.browse);
                        }
                        newitemdata && props.cardTable.setTableData(item, newitemdata, null, false);
                    })
                },
                error: (data) => {
                    toast({ content: data.message, color: 'danger' });
                }
            });
        };
        window.pageFyytClick = (fyytdata) => {

            console.log(this.props);
            console.log(props);
            let bodys = Object.values(window.presetVar.body);
            let bodyarr = [];
            let hideTable = this.state.hideTables;
            bodys.map((one) => {
                if (hideTable.indexOf(one) < 0) {
                    bodyarr[bodyarr.length] = one;
                }
            })
            let pagecode = (props.getUrlParam("pagecode") == undefined || props.getUrlParam("pagecode") == '') ? props.getSearchParam("p") : props.getUrlParam("pagecode");
            let billdata = props.createExtCardData(pagecode, "head", bodyarr);

            billdata.head.userjson = JSON.stringify(fyytdata);
            this.setState({ showFyytModal: false });

            requestApi.BillFyyt({
                data: billdata,
                success: (data) => {
                    props.form.setAllFormValue({ [presetVar.head.head1]: data.head[presetVar.head.head1] });
                    let verifydata = data.bodys["accrued_verify"];
                    verifydata && props.cardTable.setTableData("accrued_verify", verifydata, null, false);
                },
                error: (data) => {
                    toast({ content: data.message, color: 'danger' });
                }
            });
        };
        // TOP NCCLOUD-61317 增加公式编辑后联动 chengwebc ADD
        props.setRelationAfterEventCallBack && props.setRelationAfterEventCallBack((props, changedField, changedArea, oldCard, newCard, index, rowId) => {
            let params = getAfterEventParams(props, changedField, changedArea, oldCard, newCard, index, rowId);
            params.map((one) => {
                afterEvent.call(this, props, one.moduleId, one.key, one.value, one.changedrows, one.index)
            })
        });
        // BTM NCCLOUD-61317 增加公式编辑后联动 chengwebc
    }

    
    closecjk() {
        this.setState({ showCJKModal: false });
    }
    closefyyt() {
        this.setState({ showFyytModal: false });
    }
    billInvalid() {
        // let param = {};
        // param.openbillid=this.props.getUrlParam("id")||this.props.getUrlParam("billid");
        // param.tradetype=window.presetVar.tradetype;
        // param.pagecode=this.props.getSearchParam("p");
        // requestApi.billInvalid({
        //     data: param,
        //     success: (data) => {
        //         if (data.success) {
        //             location.reload();
        //         }
        //     }
        // });
    }
    componentDidMount() {
    }

    componentDidUpdate() {

    }
    componentWillMount() {
        // var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串   
        var isIE = !!window.ActiveXObject || "ActiveXObject" in window
        if(!isIE) {
            window.onbeforeunload = () => {
                let status = this.props.getUrlParam("status");
                let addType = this.props.getUrlParam('addType');

                if (addType != "pull" && (status == 'edit' || status == 'add')) {
                    return ''/* 国际化处理： 确定要离开吗？*/
                }
            }
        }
    }

    componentWillUnmount() { }
    cancel() {
        this.setState({
            showLinkBudget: false
        })
    }
    affirm(info) {
        this.setState({
            showLinkBudget: false
        })
    }
    closeApprove = () => {
        this.setState({
            ShowApproveDetail: false
        })
    }

    submitBox(){
        let sense = props.getUrlParam("scene");
        window.presetVar.lsBodys.forEach((body) => {
            if (body !== 'accrued_verify' && body !== 'er_bxcontrast' && body !== 'er_cshare_detail') {
                props.cardTable.filterEmptyRows(body, ['vat_amount'], 'include');
                props.cardTable.closeModel(body);
            }
        });
        let checkResult_sub = props.form.isCheckNow('head') && props.cardTable.checkTableRequired();
        if (checkResult_sub) {
            //弹出冲借款
            let bxcontrast = props.cardTable.getAllRows("er_bxcontrast");
            let status = window.presetVar.pagestatus == '' ? this.props.getUrlParam("status") : window.presetVar.pagestatus;
            if ((window.presetVar && window.presetVar.ISCONTRAST && window.presetVar.ISCONTRAST == "Y" && bxcontrast.length == 0 && (status == "add" || status == "edit")) && !window.sureAssign) {
                promptBox({
                    color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                    title: multiLang && multiLang.get('201102BCLF-0002'),
                    content: multiLang && multiLang.get('201102BCLF-0008'),
                    beSureBtnClick: this.cjk,
                    cancelBtnClick: this.submit
                })
                return;
            } else {
                let iscostshare = props.form.getFormItemsValue("head", ['iscostshare']) == null ? "" : props.form.getFormItemsValue("head", ['iscostshare'])[0].value//收款对象
                if (iscostshare == true || iscostshare == "true") {
                    let rows = props.cardTable.getAllRows("er_cshare_detail");
                    if (rows == null || rows.length == 0) {
                        toast({ content: multiLang && multiLang.get("201102BCLF-0019"), color: 'warning' });
                        return;
                    }
                }
                //不录入单据内容直接报错不能提交
                let vat_amount = props.form.getFormItemsValue("head", ['iscostshare']) == null ? "" : props.form.getFormItemsValue("head", ['vat_amount'])[0].value//报销销金额
                if (vat_amount == undefined || vat_amount == null || vat_amount == 0) {
                    toast({ content: multiLang && multiLang.get("201102BCLF-0020"), color: 'warning' });
                    return;
                }
                let send_data = {};
                let billdata = props.createExtCardData(props.meta.getMeta().code, "head", bodyarr);
                requestApi.delete3status(billdata);
                let paramurl = getRequest1(parent.window.location.hash);
                let paramdata = {};
                console.log(billdata);
                paramdata.openbillid = !(props.getUrlParam("billid") || props.getUrlParam("id")) ? "" : props.getUrlParam("billid") || props.getUrlParam("id");
                paramdata.fromssc = "ssc",
                    paramdata.jkCheck = jkCheck,
                    paramdata.ntbCheck = ntbCheck,
                    paramdata.msg_pk_bill = "",
                    paramdata.pk_tradetype = billtype;
                paramdata.pagecode = paramurl.p;
                paramdata.appcode = paramurl.c;
                paramdata.assingUsers = window.assingUsers;
                let appcode = (this.props.getUrlParam("appcode") == undefined || this.props.getUrlParam("appcode") == '') ? this.props.getSearchParam("c") : this.props.getUrlParam("appcode");
                let pagecode = (this.props.getUrlParam("pagecode") == undefined || this.props.getUrlParam("pagecode") == '') ? this.props.getSearchParam("p") : this.props.getUrlParam("pagecode");
                let status = window.presetVar.pagestatus == '' ? this.props.getUrlParam("status") : window.presetVar.pagestatus;
                if (status == "edit") {
                    paramdata.status = "edit";
                } else {
                    paramdata.status = "save";
                }
                //如果指派点了确定  由于第一次点击保存提交实际上已经保存成功了，指派后再次提交应该走浏览提交逻辑
                if (window.sureAssign) {
                    paramdata.status = "edit";
                    paramdata.openbillid = window.assignBillId;
                }
                billdata.head.userjson = JSON.stringify(paramdata);
                billdata.templetid = props.meta.getMeta().pageid;
                billdata.accessorybillid = this.accessorybillid;
                console.log(billdata);
                props.validateToSave(billdata, () => {
                    requestApi.submitBill({
                        data: billdata,
                        success: (data) => {//设置了指派
                            ntbCheck = 'false';
                            if (data.workflow && data.billid && data.bill && (data.workflow == 'approveflow' || data.workflow == 'workflow')) {
                                this.setState({
                                    compositedata: data,
                                    compositedisplay: true
                                });
                                window.assignBillId = data.billid;
                                //重新给页面赋值
                                let newFormData = requestApi.filterDatas(data.bill.head[presetVar.head.head1]);
                                updataFormData(props, presetVar.head.head1, newFormData);
                                window.presetVar.reloadHead = newFormData;
                                bodyarr.forEach((item) => {
                                    updatacardTableData(props, item, requestApi.filterDatas(data.bill.bodys[item]));
                                })
                            } else {
                                this.setState({
                                    compositedisplay: false
                                });
                                if (data.Alarm && data.Alarm == "Y") {
                                    promptBox({
                                        color: 'danger',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                                        title: multiLang && multiLang.get("201102BCLF-0021"),                // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输
                                        content: data.message,             // 提示内容,非必输
                                        noFooter: false,                // 是否显示底部按钮(确定、取消),默认显示(false),非必输
                                        noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
                                        beSureBtnName: multiLang && multiLang.get("201102BCLF-0017"),          // 确定按钮名称, 默认为"确定",非必输
                                        cancelBtnName: multiLang && multiLang.get("201102BCLF-0018"),         // 取消按钮名称, 默认为"取消",非必输
                                        beSureBtnClick: () => {
                                            ntbCheck = 'true';
                                            if (props.getUrlParam('status') === 'browse') {
                                                window.sureAssign = false;
                                            }
                                            pageButtonClick.call(this)[pageButton.billSubmit]();
                                        },
                                        cancelBtnClick: () => {
                                            let billid = props.form.getFormItemsValue("head", "pk_jkbx").value;
                                            if (props.getUrlParam('status') === 'browse') {
                                                props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.browse, id: billid, tradetype: billtype, pagecode: paramurl.p, appcode: paramurl.c });
                                            } else {
                                                if (billid) {
                                                    props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.edit, id: billid, tradetype: billtype, pagecode: paramurl.p, appcode: paramurl.c });
                                                } else {
                                                }
                                            }
                                            window.assingUsers = [];    //清空指派人信息
                                            window.sureAssign = false;
                                        }
                                    })

                                } else if (data.userjson && JSON.parse(data.userjson) && JSON.parse(data.userjson).reimmsg && JSON.parse(data.userjson).reimmsg.length > 0) {
                                    let billid = data.head.head.rows[0].values.pk_jkbx.value;
                                    promptBox({
                                        color: 'warning',       // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                                        title: multiLang && multiLang.get('201102JCLF_C-0002'),
                                        content: JSON.parse(data.userjson).reimmsg,
                                        noFooter: false,  // 是否显示底部按钮(确定、取消),默认显示(false),非必输
                                        noCancelBtn: true,    // 是否显示取消按钮,，默认显示(false),非必输
                                        beSureBtnName: multiLang && multiLang.get('2011-0003'),
                                        cancelBtnName: multiLang && multiLang.get('2011-0002'),
                                        beSureBtnClick: () => {
                                            window.presetVar.redata = 'Y';
                                            //重新给页面赋值
                                            let newFormData = requestApi.filterDatas(data.head[presetVar.head.head1]);
                                            updataFormData(props, presetVar.head.head1, newFormData);
                                            window.presetVar.reloadHead = newFormData;
                                            bodyarr.forEach((item) => {
                                                updatacardTableData(props, item, requestApi.filterDatas(data.bodys[item]));
                                            })
                                            if (sense && (sense == "approve" || sense == "zycl" || sense == "approvesce")) {
                                                props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.browse, id: billid, tradetype: billtype });
                                            } else {
                                                props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.browse, id: billid, tradetype: billtype, pagecode: paramurl.p, appcode: paramurl.c });
                                            }
                                            window.assingUsers = [];    //清空指派人信息
                                            window.sureAssign = false;
                                        }
                                    })
                                } else {
                                    console.log(data);
                                    // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                                    // props.form.setAllFormValue({[presetVar.head.head1]: requestApi.filterDatas(data.head[presetVar.head.head1])});
                                    // window.presetVar.reloadHead = requestApi.filterDatas(data.head[presetVar.head.head1]);
                                    let newFormData = requestApi.filterDatas(data.head[presetVar.head.head1]);
                                    updataFormData(props, presetVar.head.head1, newFormData);
                                    window.presetVar.reloadHead = newFormData;
                                    // BTM 性能优化，变更页面数据更新方法
                                    bodyarr.forEach((item) => {
                                        // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                                        // debugLogs.timeStart('setTableData_' + item);
                                        // props.cardTable.setTableData(item, requestApi.filterDatas(data.bodys[item]));
                                        // debugLogs.timeEnd('setTableData_' + item);
                                        updatacardTableData(props, item, requestApi.filterDatas(data.bodys[item]));
                                        // BTM 性能优化，变更页面数据更新方法
                                    })
                                    ntbCheck = 'false';
                                    let billid = data.head.head.rows[0].values.pk_jkbx.value;
                                    // window.open(window.location.origin+'/erm/expenseaccount/expenseaccount/card#status=browse&openbillid='+billid+'&tradetype=2641');
                                    // location.hash = `#status=${statusVar.browse}&billid=${billid}&tradetype=${billtype}`;
                                    window.presetVar.redata = 'N';
                                    if (sense && (sense == "approve" || sense == "zycl" || sense == "approvesce")) {
                                        props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.browse, id: billid, tradetype: billtype, sence: sense });
                                    } else {
                                        props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.browse, id: billid, tradetype: billtype, pagecode, appcode: appcode });
                                    }
                                    props.button.setButtonsVisible({
                                        [pageButton.pageSave]: false,
                                        [pageButton.pageEdit]: false,
                                        [pageButton.billSubmit]: false,
                                        [pageButton.billRecall]: true,
                                        [pageButton.pageDel]: false,
                                        [pageButton.billInvalid]: false,
                                        [pageButton.pageClose]: false
                                    });
                                    window.assingUsers = [];    //清空指派人信息
                                    window.sureAssign = false;
                                    // //处理按钮相关状态
                                    // dataHandle(data.head.head,props);
                                    // props.form.setFormStatus('head', 'browse');
                                    // props.billBodys && props.billBodys.forEach((body)=>{
                                    //     props.editTable.setStatus(body, 'browse');
                                    // });

                                }
                            }
                        },
                        error: (data) => {
                            toast({ content: data.message, color: 'danger' });
                        }
                    });
                }, bodyarr, null);

            }


            // props.form.setFormStatus('head', 'browse');
            // props.billBodys.forEach((body)=>{
            //     props.editTable.setStatus(body, 'browse');
            // });

        }
    }

    cjk() {
        let hideTables = [];
        this.state.hideTables.forEach((item)=>{
            if(item != 'er_bxcontrast'){
                hideTables.push(item);
            }
        });
        this.setState({hideTables:hideTables});
        let bodys = Object.values(window.presetVar.body);
        let bodyarr = [];
        let hideTable = this.state.hideTables;
        bodys.map((one) => {
            if (hideTable.indexOf(one) < 0) {
                bodyarr[bodyarr.length] = one;
            }
        })
        let param = {};
        param.tradetype = window.presetVar.tradetype;
        param.pk_jkbx = this.props.form.getFormItemsValue("head", "pk_jkbx").value;
        param.jkbxr = this.props.form.getFormItemsValue("head", "jkbxr").value;
        param.receiver = this.props.form.getFormItemsValue("head", "receiver").value;
        param.bzbm = this.props.form.getFormItemsValue("head", "bzbm").value;
        param.djrq = this.props.form.getFormItemsValue("head", "djrq").value;
        param.dwbm = this.props.form.getFormItemsValue("head", "dwbm").value;
        if (this.props.form.getFormItemsValue("head", "pk_item").value) {
            param.pk_item = this.props.form.getFormItemsValue("head", "pk_item").value;
        } else {
            param.pk_item = "~";
        }
        let bodyBZ = [];
        let bzjeMap = new Map();
        let showData = this.props.createExtCardData(this.props.meta.getMeta().code, "head", bodyarr);
        Object.keys(showData.bodys).forEach((item, index) => {
            if (item == "arap_bxbusitem" || item == "bzitem" || item == "other" || item == "zsitem") {
                showData.bodys[item].rows.forEach((row) => {
                    if (row.values.bzbm && row.values.bzbm.value) {
                        bodyBZ.push(row.values.bzbm);
                        var amount = row.values["vat_amount"].value;//取含税金额
                        if (bzjeMap.has(row.values.bzbm.value)) {
                            var oldAmount = parseFloat(bzjeMap.get(row.values.bzbm.value));
                            bzjeMap.delete(row.values.bzbm.value);
                            bzjeMap.set(row.values.bzbm.value, oldAmount + parseFloat(amount));
                        } else {
                            bzjeMap.set(row.values.bzbm.value, amount);
                        }
                    }
                });
            }
        });
        param.bzbms = bodyBZ;
        window.cjkdata = param;
        window.bxjeMap = bzjeMap;
        window.bxcontrastGrid = this.props.cardTable.getAllRows("er_bxcontrast");
        this.setState({ showCJKModal: true });
    }
    save() {
        let statusVar = window.presetVar.status;
        let billtype = window.presetVar.tradetype;
        let bodys = Object.values(window.presetVar.body);
        let bodyarr = [];
        let hideTable = this.state.hideTables;
        let sense = this.props.getUrlParam("scene");
        bodys.map((one) => {
            if (hideTable.indexOf(one) < 0) {
                bodyarr[bodyarr.length] = one;
            }
        })
        let billdata = this.props.createExtCardData(this.props.meta.getMeta().code, "head", bodyarr);

        //不录入单据内容直接报错不能提交
        let multiLang_head = this.props.MutiInit.getIntl(2011); //this.moduleId
        let vat_amount = this.props.form.getFormItemsValue("head", ['iscostshare']) == null ? "" : this.props.form.getFormItemsValue("head", ['vat_amount'])[0].value//报销销金额
        if (vat_amount == undefined || vat_amount == null || vat_amount == 0) {
            toast({ content: multiLang_head && multiLang_head.get('201102BCLF-0020')/*"报销金额不能为空!"*/, color: 'warning' });
            return;
        }

        let paramdata = {};
        console.log(billdata);
        paramdata.fromssc = "ssc",
            paramdata.jkCheck = "",
            paramdata.ntbCheck = ntbCheck,
            paramdata.msg_pk_bill = "",
            paramdata.accessorybillid = this.accessorybillid;
        paramdata.pk_tradetype = window.presetVar.tradetype;
        let pagecode = (this.props.getUrlParam("pagecode") == undefined || this.props.getUrlParam("pagecode") == '') ? this.props.getSearchParam("p") : this.props.getUrlParam("pagecode");
        let appcode = (this.props.getUrlParam("appcode") == undefined || this.props.getUrlParam("appcode") == '') ? this.props.getSearchParam("c") : this.props.getUrlParam("appcode");
        paramdata.pagecode = pagecode;
        paramdata.appcode = appcode;
        let status = this.props.getUrlParam("status");
        if (status == "edit") {
            paramdata.status = "edit";
        } else {
            paramdata.status = "save";
        }
        billdata.head.userjson = JSON.stringify(paramdata);
        billdata.templetid = this.props.meta.getMeta().pageid;
        console.log(billdata);
        this.props.validateToSave(billdata, () => {
            requestApi.savejkbx({
                data: billdata,
                success: (data) => {
                    let props = this.props;
                    console.log(data);
                    let billid = data.head.head.rows[0].values.pk_jkbx.value;
                    let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId
                    if (data.userjson && JSON.parse(data.userjson) && JSON.parse(data.userjson).reimmsg && JSON.parse(data.userjson).reimmsg.length > 0) {
                        promptBox({
                            color: 'warning',       // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                            title: multiLang && multiLang.get('201102JCLF_C-0002'),
                            content: JSON.parse(data.userjson).reimmsg,
                            noFooter: false,  // 是否显示底部按钮(确定、取消),默认显示(false),非必输
                            noCancelBtn: false,    // 是否显示取消按钮,，默认显示(false),非必输
                            beSureBtnName: multiLang && multiLang.get('2011-0003'),
                            cancelBtnName: multiLang && multiLang.get('2011-0002'),
                            beSureBtnClick: () => {
                                window.presetVar.redata = 'Y';
                                if (sense && (sense == "approve" || sense == "zycl" || sense == "approvesce")) {
                                    this.props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.browse, id: billid, tradetype: billtype, scene: sense });
                                } else {
                                    this.props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.browse, id: billid, tradetype: billtype, pagecode: pagecode, appcode: appcode });

                                }
                            },   // 确定按钮点击调用函数,非必输
                            cancelBtnClick: () => {
                                let newFormData = requestApi.filterDatas(data.head[presetVar.head.head1]);
                                updataFormData(props, presetVar.head.head1, newFormData);
                                window.presetVar.reloadHead = newFormData;
                                bodyarr.forEach((item) => {
                                    updatacardTableData(props, item, requestApi.filterDatas(data.bodys[item]));
                                })
                                if (sense && (sense == "approve" || sense == "zycl" || sense == "approvesce")) {
                                    this.props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.edit, id: billid, tradetype: billtype, scene: sense });
                                } else {
                                    this.props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.edit, id: billid, tradetype: billtype, pagecode: pagecode, appcode: appcode });
                                }
                            }  // 取消按钮点击调用函数,非必输
                        })
                    } else {
                        if (data.Alarm && data.Alarm == "Y") {
                            promptBox({
                                color: 'danger',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                                title: multiLang && multiLang.get("201102BCLF-0021"),                // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输
                                content: data.message,             // 提示内容,非必输
                                noFooter: false,                // 是否显示底部按钮(确定、取消),默认显示(false),非必输
                                noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
                                beSureBtnName: multiLang && multiLang.get("201102BCLF-0017"),          // 确定按钮名称, 默认为"确定",非必输
                                cancelBtnName: multiLang && multiLang.get("201102BCLF-0018"),         // 取消按钮名称, 默认为"取消",非必输
                                beSureBtnClick: () => {
                                    ntbCheck = 'true';
                                    this.save();
                                },   // 确定按钮点击调用函数,非必输
                                // cancelBtnClick: ()=>{console.log("sdd") },  // 取消按钮点击调用函数,非必输
                            })
                        } else {
                            let newFormData = requestApi.filterDatas(data.head[presetVar.head.head1]);
                            updataFormData(this.props, presetVar.head.head1, newFormData);
                            window.presetVar.reloadHead = newFormData;
                            // BTM 性能优化，变更页面数据更新方法
                            bodyarr.forEach((item) => {
                                // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                                // debugLogs.timeStart('setTableData_' + item);
                                // props.cardTable.setTableData(item, requestApi.filterDatas(data.bodys[item]));
                                // debugLogs.timeEnd('setTableData_' + item);
                                updatacardTableData(this.props, item, requestApi.filterDatas(data.bodys[item]));
                                // BTM 性能优化，变更页面数据更新方法
                            })
                            ntbCheck = 'false';
                            window.presetVar.redata = 'N';
                            if (sense && (sense == "approve" || sense == "zycl" || sense == "approvesce")) {
                                this.props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.browse, id: billid, tradetype: billtype, scene: sense });
                            } else {
                                this.props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.browse, id: billid, tradetype: billtype, pagecode: pagecode, appcode: appcode });
                            }
                        }

                    }

                },
                error: (data) => {
                    toast({ content: data.message, color: 'danger' });
                }
            });
        }, bodyarr, null);

    }
    submit() {
        let billtype = window.presetVar.tradetype;
        let statusVar = window.presetVar.status;
        let bodys = Object.values(window.presetVar.body);
        let bodyarr = [];
        let hideTable = this.state.hideTables;
        let sense = this.props.getUrlParam("scene");
        bodys.map((one) => {
            if (hideTable.indexOf(one) < 0) {
                bodyarr[bodyarr.length] = one;
            }
        })
        let billdata = this.props.createExtCardData(this.props.meta.getMeta().code, "head", bodyarr);
        let appcode = (this.props.getUrlParam("appcode") == undefined || this.props.getUrlParam("appcode") == '') ? this.props.getSearchParam("c") : this.props.getUrlParam("appcode");
        let pagecode = (this.props.getUrlParam("pagecode") == undefined || this.props.getUrlParam("pagecode") == '') ? this.props.getSearchParam("p") : this.props.getUrlParam("pagecode");
        let paramdata = {};
        console.log(billdata);
        paramdata.openbillid = !(this.props.getUrlParam("billid") || this.props.getUrlParam("id")) ? "" : this.props.getUrlParam("billid") || this.props.getUrlParam("id");
        paramdata.fromssc = "ssc",
            paramdata.jkCheck = "",
            paramdata.ntbCheck = ntbCheck,
            paramdata.msg_pk_bill = "",
            paramdata.pk_tradetype = window.presetVar.tradetype;
        paramdata.pagecode = pagecode;
        paramdata.appcode = appcode;
        let status = this.props.getUrlParam("status");
        if (status == "edit") {
            paramdata.status = "edit";
        } else {
            paramdata.status = "save";
        }
        if (window.sureAssign) {
            paramdata.status = "edit";
        }
        billdata.head.userjson = JSON.stringify(paramdata);
        billdata.templetid = this.props.meta.getMeta().pageid;
        this.props.validateToSave(billdata, () => {
            requestApi.submitBill({
                data: billdata,
                success: (data) => {
                    let props = this.props;
                    let multiLang = this.props.MutiInit.getIntl(2011);
                    if (data.workflow && (data.workflow == 'approveflow' || data.workflow == 'workflow')) {
                        this.setState({
                            compositedata: data,
                            compositedisplay: true
                        });
                        window.assignBillId = data.billid;
                        //重新给页面赋值
                        let newFormData = requestApi.filterDatas(data.bill.head[presetVar.head.head1]);
                        updataFormData(props, presetVar.head.head1, newFormData);
                        window.presetVar.reloadHead = newFormData;
                        bodyarr.forEach((item) => {
                            updatacardTableData(props, item, requestApi.filterDatas(data.bill.bodys[item]));
                        })
                    } else {
                        this.setState({
                            compositedisplay: false
                        });
                        if (data.Alarm && data.Alarm == "Y") {
                            promptBox({
                                color: 'danger',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                                title: multiLang && multiLang.get("201102BCLF-0021"),                // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输
                                content: data.message,             // 提示内容,非必输
                                noFooter: false,                // 是否显示底部按钮(确定、取消),默认显示(false),非必输
                                noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
                                beSureBtnName: multiLang && multiLang.get("201102BCLF-0017"),          // 确定按钮名称, 默认为"确定",非必输
                                cancelBtnName: multiLang && multiLang.get("201102BCLF-0018"),         // 取消按钮名称, 默认为"取消",非必输
                                beSureBtnClick: () => {
                                    ntbCheck = 'true';
                                    this.submit();
                                },
                                cancelBtnClick: () => {
                                    let billid = props.form.getFormItemsValue("head", "pk_jkbx").value;
                                    let paramurl = getRequest1(parent.window.location.hash);
                                    if (props.getUrlParam('status') === 'browse') {
                                        props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.browse, id: billid, tradetype: billtype, pagecode: paramurl.p, appcode: paramurl.c });
                                    } else {
                                        if (billid) {
                                            props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.edit, id: billid, tradetype: billtype, pagecode: paramurl.p, appcode: paramurl.c });
                                        } else {
                                        }

                                    }
                                    window.assingUsers = [];    //清空指派人信息
                                    window.sureAssign = false;
                                }
                            })
                        } else if (data.userjson && JSON.parse(data.userjson) && JSON.parse(data.userjson).reimmsg && JSON.parse(data.userjson).reimmsg.length > 0) {
                            promptBox({
                                color: 'warning',       // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                                title: multiLang && multiLang.get('201102JCLF_C-0002'),
                                content: JSON.parse(data.userjson).reimmsg,
                                noFooter: false,  // 是否显示底部按钮(确定、取消),默认显示(false),非必输
                                noCancelBtn: false,    // 是否显示取消按钮,，默认显示(false),非必输
                                beSureBtnName: multiLang && multiLang.get('2011-0003'),
                                cancelBtnName: multiLang && multiLang.get('2011-0002'),
                                beSureBtnClick: () => {
                                    window.presetVar.redata = 'Y';
                                    let billid = data.head.head.rows[0].values.pk_jkbx.value;
                                    if (sense && (sense == "approve" || sense == "zycl" || sense == "approvesce")) {
                                        this.props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.browse, id: billid, tradetype: billtype, scene: sense });
                                    } else {
                                        this.props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.browse, id: billid, tradetype: billtype, pagecode: pagecode, appcode: appcode });
                                    }
                                    ntbCheck = 'false';
                                }
                            })
                        } else {
                            console.log(data);
                            let newFormData = requestApi.filterDatas(data.head[presetVar.head.head1]);
                            updataFormData(this.props, presetVar.head.head1, newFormData);
                            window.presetVar.reloadHead = newFormData;
                            // BTM 性能优化，变更页面数据更新方法
                            bodyarr.forEach((item) => {
                                // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                                // debugLogs.timeStart('setTableData_' + item);
                                // props.cardTable.setTableData(item, requestApi.filterDatas(data.bodys[item]));
                                // debugLogs.timeEnd('setTableData_' + item);
                                updatacardTableData(this.props, item, requestApi.filterDatas(data.bodys[item]));
                                // BTM 性能优化，变更页面数据更新方法
                            })
                            ntbCheck = 'false';
                            let billid = data.head.head.rows[0].values.pk_jkbx.value;
                            window.presetVar.redata = 'N';
                            if (sense && (sense == "approve" || sense == "zycl" || sense == "approvesce")) {
                                this.props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.browse, id: billid, tradetype: billtype, scene: sense });
                            } else {
                                this.props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: statusVar.browse, id: billid, tradetype: billtype, pagecode: pagecode, appcode: appcode });
                            }
                            this.props.button.setButtonsVisible({
                                [pageButton.pageSave]: false,
                                [pageButton.pageEdit]: false,
                                [pageButton.billSubmit]: false,
                                [pageButton.billRecall]: true,
                                [pageButton.pageDel]: false,
                                [pageButton.billInvalid]: false,
                                [pageButton.pageClose]: false
                            });
                            window.assingUsers = [];    //清空指派人信息
                            window.sureAssign = false;
                        }
                    }


                },
                error: (data) => {
                    toast({ content: data.message, color: 'danger' });
                }
            });
        }, bodyarr, null);

    }
    getAssginUser = (value) => {
        window.assingUsers = value;
        window.sureAssign = true;
        pageButtonClick.call(this)[pageButton.billSubmit]();
        window.assignBillId = '';
    }

    turnOff() {
        this.setState({
            compositedisplay: false
        });
        window.sureAssign = false;
        window.assingUsers = [];
        let props = this.props;
        let paramurl = getRequest1(parent.window.location.hash);
        let billid = window.assignBillId;
        if (!billid) {
            billid = props.form.getFormItemsValue("head", "pk_jkbx").value;
        }
        window.presetVar.redata = 'Y';
        props.linkTo('/erm/expenseaccount/expenseaccount/card/index.html', { isfromSsc: true, status: window.presetVar.status.browse, id: billid, tradetype: window.presetVar.tradetype, pagecode: paramurl.p, appcode: paramurl.c });
    }
    onHideUploader = () => {
        this.setState({
            showUploader: false,
        });
        let id = this.props.getUrlParam('id') || this.props.getUrlParam("billid") ? this.props.getUrlParam('id') || this.props.getUrlParam("billid") : this.state.billId;
        getAttachmentLength(id).then(lists => {
            this.setState({
                groupLists: lists
            });
            let button = this.props.button.getButtons();
            let multiLang = this.props.MutiInit.getIntl(2011);
            button.forEach((items) => {
                if (items.key == 'fileManager') {
                    items.title = multiLang && multiLang.get('201102BCLF-0040') + ' ' + lists + ' ';
                }
                this.props.button.setButtonTitle(items.key, items.title);
            });
            // this.props.button.setButtons(button);
            // renderButtions(this.props,this.props.meta.getMeta(),button,null);
        })
    }

    // 设置冲借款弹框里默认勾选的数据ligru
    setDefaultSelectedJk = (jkData, cjkProps) => {
        let props = this.props;
        let contrastGrid = props.cardTable.getAllRows("er_bxcontrast");
        let contrastPks = [];
        contrastGrid && contrastGrid.length && contrastGrid.forEach((contrastRow) => {
            if (contrastRow.values.pk_busitem && contrastRow.values.pk_busitem.value) {
                let contrastPk = contrastRow.values.pk_busitem.value;
                jkData && jkData.length && jkData.forEach((jk) => {
                    jk && jk.body && jk.body.jk_busitem && jk.body.jk_busitem.rows && jk.body.jk_busitem.rows.forEach((jkRow) => {
                        if (jkRow && jkRow.values && jkRow.values.pk_busitem.value === contrastPk) {
                            contrastPks.push(contrastPk);
                        }
                    })
                })
            }
        });
        cjkProps.transferTable.setTheCheckRows(window.cjkAreaCode.billsArea, contrastPks);

        //如果已经冲过借款，需要重新计算window.bxjeMap中的可冲销金额ligru
        let selectrow = cjkProps.transferTable.getTransferTableSelectedValue();
        selectrow && selectrow.head && selectrow.head.map((data) => {
            data.body.jk_busitem.rows.map((bodys) => {
                let bzbm = bodys.values.bzbm.value;
                window.bxjeMap.set(bzbm, parseFloat(window.bxjeMap.get(bzbm) - parseFloat(bodys.values.cjkybje.value) + parseFloat(bodys.values.hkybje.value)));
            })
        })
        let btnObj = document.querySelector('.area-right .main-button');
        if (btnObj) {
            btnObj.addEventListener('click', function () {
                //判断是否有已选的条目，有就直接return，无则继续执行转单按钮正常逻辑
                let selectdata = cjkProps.transferTable.getTransferTableSelectedValue()
                if (selectdata && selectdata.head && selectdata.head.length > 0) {
                    return;
                }
                window.pageCjkClick(null);
            })
        }
    }

    // 设置核销预提弹框里默认勾选的数据
    setDefaultSelectedYt = (ytData, ytProps) => {
        let props = this.props;
        let accGrid = props.cardTable.getAllRows("accrued_verify");
        console.log(ytData, accGrid);
        let accPks = [];
        accGrid && accGrid.length && accGrid.forEach((accRow) => {
            if (accRow.values.pk_accrued_detail && accRow.values.pk_accrued_detail.value) {
                let accBodyPk = accRow.values.pk_accrued_detail.value;
                ytData && ytData.length && ytData.forEach((ytRow) => {
                    ytRow.body.accrued_detail.rows.forEach((ytBody) => {
                        if (ytBody.values.pk_accrued_detail && ytBody.values.pk_accrued_detail.value && ytBody.values.pk_accrued_detail.value === accBodyPk) {
                            accPks.push(accBodyPk);
                        }
                    })
                })
            }
        })
        ytProps.transferTable.setTheCheckRows(window.fyytAreaCode.billsArea, accPks);
        let btnObj = document.querySelector('.area-right .main-button');
        if (btnObj) {
            btnObj.addEventListener('click', function () {
                //判断是否有已选的条目，有就直接return， 无则继续执行转单按钮正常逻辑
                let selectdata = ytProps.transferTable.getTransferTableSelectedValue()
                if (selectdata && selectdata.head && selectdata.head.length > 0) {
                    return;
                }
                window.pageFyytClick(null);
            })
        }
    }


    modelSave=()=>{
        // headSumHandle.call(this,this.props);
        this.updateHeadAmount();
        pageButtonClick.call(this)['pageSave']();
    }

    beSureBtnClick = () => {
    }

    modelClose=()=>{
        this.state.modelCloseEvent = true;
        this.state.modelEdit = false;
        this.updateHeadAmount();
    }

    updateHeadAmount =()=>{
        let key = 'vat_amount'
        let moduleId = 'arap_bxbusitem'
        //by houyb 离开侧拉页面时，遍历表体所有行执行afterEvnet  解决新增表体明细后表头zfbbje不变问题
        // let rows =  this.props.cardTable.getRowsByIndexs(moduleId, 0);
        let rows =  this.props.cardTable.getAllRows(moduleId);
        for(let index = 0; index< rows.length; index++){
            let value = this.props.cardTable.getValByKeyAndIndex(moduleId, index,key);
            let changedrows = [{
                newvalue:{value:value.value},
                oldvalue:{value:0},
                rowid:rows[index].rowid            
            }]
            afterEvent.call(this,this.props,moduleId,key,value,changedrows);
        }
        //by houyb
    }
    refbind(ref) {
        this.refbind = ref
    }
    uploadDoneuse(res){
        let {success} = res;
        if(success) {
            if( this.refbind.useBtnclick ) this.refbind.useBtnclick();
            this.state.uploadAlready = true;
        }else{
            let {error } = res;
            toast({
                content: error.message,
                zIndex: 9999,
                color: 'danger'})
        }
    }
    render() {
        let { target } = this.state;
        let { createModal } = this.props.modal;  //模态框
        let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId
        let btnModalConfig = {
            [pageButton.pageDel]: {
                // "2011-0004": "删除"
                // "2011-0005": "确认删除该单据吗？"
                title: multiLang && multiLang.get('2011-0004'),
                content: multiLang && multiLang.get('2011-0005'),
            },
            [pageButton.billInvalid]: {
                // "2011-0009": "作废",
                // "2011-0010": "确认作废该单据吗？"
                title: multiLang && multiLang.get('2011-0009'),
                content: multiLang && multiLang.get('2011-0010'),
                beSureBtnClick: this.billInvalid.bind(this)
            }
        }
        //单据分享的内容-begin
        let content = (
            <div style={{ height: '130px', width: '237px' }}>
                {/*"201102BCLF-0005": "分享给",*/}
                <p style={{ color: '#111', height: '50px' }}>{multiLang && multiLang.get('201102BCLF-0005')}</p>
                <AllUserGridRef
                    value={this.state.allUserGridRefValue}
                    onChange={(data) => {
                        this.setState({
                            allUserGridRefValue: data
                        });
                    }}
                    queryCondition={() => {
                        return { 'enablestate': 2 }
                    }}
                    isMultiSelectedEnabled={true}
                />
                <div style={{ height: '90px', marginBottom: '78px', marginTop: '30px' }}>

                    <NCButton
                        style={{ float: 'right', marginBottom: '20px' }}
                        colors="info"
                        onClick={
                            () => {
                                this.setState({
                                    showBillShare: false,
                                    allUserGridRefValue: {}
                                })
                            }
                        }
                        // "2011-0002": "取消",
                        size="sm">
                        {multiLang && multiLang.get('2011-0002')}
                    </NCButton>
                    <NCButton
                        style={{ float: 'right' }}
                        colors="primary"
                        onClick={
                            () => {
                                //分享逻辑
                                if (this.state == null || this.state.allUserGridRefValue == null || this.state.allUserGridRefValue == undefined)
                                    return;
                                let bodys = Object.values(window.presetVar.body);
                                let bodyarr = [];
                                let hideTable = this.state.hideTables;
                                bodys.map((one) => {
                                    if (hideTable.indexOf(one) < 0) {
                                        bodyarr[bodyarr.length] = one;
                                    }
                                })
                                let showData = this.props.createExtCardData(this.props.meta.getMeta().code, "head", bodyarr);
                                let paramdata = {};
                                let paramurl = getRequest1(parent.window.location.hash);
                                let billid = this.props.getUrlParam("id") || this.props.getUrlParam("billid");
                                paramdata.pagecode = paramurl.p;
                                paramdata.appcode = paramurl.c;
                                paramdata.billid = billid;
                                paramdata.tradetype = window.presetVar.tradetype;
                                let receiveruser = "";
                                if (this.state.allUserGridRefValue != null && this.state.allUserGridRefValue.length > 0) {
                                    for (let m = 0; m < this.state.allUserGridRefValue.length; m++)
                                        receiveruser += this.state.allUserGridRefValue[m].refpk + ","
                                }
                                paramdata.receiveruser = receiveruser;
                                showData.head.userjson = JSON.stringify(paramdata);
                                requestApi.billShare({
                                    data: showData,
                                    success: (data) => {
                                        this.setState({
                                            showBillShare: false
                                        })
                                        this.state.allUserGridRefValue = null;
                                        toast({ content: multiLang && multiLang.get("201102BCLF-0043"), color: 'success' });
                                    }
                                });
                                return;
                            }
                        }
                        // "201102BCLF-0006": "分享",
                        size="sm">
                        {multiLang && multiLang.get('201102BCLF-0006')}
                    </NCButton>
                </div>
            </div>
        )

        let status = this.props.getUrlParam("status");
        let scene = this.props.getUrlParam("scene");
        //单据分享的内容-end
                            //  let szxmid=this.props.form.getFormItemsValue("head", "szxmid");
                            //  let hbbm=this.props.form.getFormItemsValue("head", "hbbm");
                            //  let paytarget=this.props.form.getFormItemsValue("head","paytarget");
                            // let tradetype = window.presetVar.tradetype;
                            let {szxmid,hbbm,paytarget,tradetype,fctno}=this.state;
        return (
            <div>
                <div >
                <BillStyle
                    {...this.props}
                >
                    <BillHead title={this.state.transtype_name}
                        refreshButtonEvent={refreshButtonClick.bind(this)}
                    >

                        <ButtonGroup
                            areaId="head"
                            buttonEvent={pageButtonClick.bind(this)}
                            modalConfig={btnModalConfig}
                        />
                        <Form
                            areaId="head"
                            onAfterEvent={afterEvent.bind(this)} />
                    </BillHead>

                    <BillBody>

                        <NCPopover
                            rootClose={false}
                            placement="left"
                            content={content}
                            show={this.state.showBillShare}
                            id="billshare"
                        >
                            <div
                                colors="primary"
                                id="billshareDiv"
                                className={(status == 'browse' && scene != 'notice') == true ? "billshare-icon-show" : "billshare-icon-hide"}
                                onClick={
                                    () => {
                                        this.setState({
                                            showBillShare: true
                                        })
                                    }
                                }
                            >
                                {/*<div  style={{ fontSize: '17px',color: '#a91a1a' }}> {multiLang && multiLang.get('201102BCLF-0007')}*/}
                                <i className='iconfont icon-fenxiang share' ></i>
                                {/*</div>*/}
                            </div>
                        </NCPopover>


                        <CardTableGroup
                            invisibleTables={[]}
                            excludeTableAreaId={null}
                            totalItemCode="vat_amount"
                            totalItemCodeByAreas={ //
                                {
                                    'accrued_verify': 'verify_amount',
                                    'er_bxcontrast': 'cjkybje',
                                    'er_cshare_detail': 'assume_amount'
                                }
                            }
                            modelSave={this.modelSave.bind(this)}
                            onBeforeEvent={beforeEvent.bind(this)}
                            onAfterEvent={afterEvent.bind(this)}
                            invisibleTables={this.state.hideTables}
                            modelClose= {this.modelClose.bind(this)}
                            modelDelRow = {this.updateHeadAmount.bind(this)}
                        >
                            <MultiButtonGroup buttonEvent={tableButtonClick.bind(this)} />
                        </CardTableGroup>

                        {
                            this.state.showUploader && < NCUploader
                                billId={this.props.getUrlParam('id') || this.props.getUrlParam("billid") ? this.props.getUrlParam('id') || this.props.getUrlParam("billid") : this.state.billId}
                                //附件上传报错
                                // beforeDelete={invoiceEvent.beforeDelete.bind(this)}
                                target={target}
                                uploadDone={this.uploadDoneuse}
                                 disableModify={this.props.form.getFormItemsValue("head", 'spzt')&&this.props.form.getFormItemsValue("head", 'spzt').value!="-1"}
                                 onHide={this.onHideUploader.bind(this)}
                                customInterface={
                                    {
                                        queryLeftTree: "/nccloud/erm/pub/ErmAttachmentTreeNodeQueryAction.do",
                                        queryAttachments: "/nccloud/erm/pub/ErmAttachmentQueryAction.do"
                                    }
                                }
                                placement={'bottom'} />
                        }

                        <div>
                            <ApproveDetail
                                show={this.state.ShowApproveDetail}
                                close={this.closeApprove}
                                billtype={this.state.billtype}
                                billid={this.state.billId}
                            />
                        </div>
                        <div>    
                        <ShowNum
                            {...this.props}
                            showModal={this.state.showModalNum}
                            data={this.state.operationData}
                            that={this}
                       />
                       
                    </div>
                        {/* <JointBill
        show={this.state.jointBillShow}
        close={() => this.setState({jointBillShow: false})}
        billCode={this.state.billId}
        {...this.props} /> */}




                    </BillBody>

                </BillStyle>
                <InvoiceUploader
                    // {...this.props}
                    {...this.state.sscrpInvoiceData}
                // 标题
                //"201102BCLF-0001": "电子发票上传列表"
                // title={multiLang && multiLang.get('201102BCLF-0001')}
                />
                <InvoiceLink
                    {...this.state.sscrpInvoiceData}
                    table={this.props.table}
                />



                <NCModal size="xlg" show={
                    this.state.showCJKModal
                }
                    onHide={
                        this.closecjk
                    }

                    
                >

                    <NCModal.Header closeButton="true">
                        {/*"201102BCLF-0002": "冲借款",*/}
                        <NCModal.Title >{multiLang && multiLang.get('201102BCLF-0002')}</NCModal.Title>
                    </NCModal.Header>

                    <NCModal.Body>
                        <Cjk setDefaultSelectedJk={this.setDefaultSelectedJk} defaultSearch={{szxmid,hbbm,tradetype,paytarget,fctno}}    showCJKModal={this.state.showCJKModal} />
                        {/* <div className="area-content" >
            <iframe src={window.location.protocol+"//"+window.location.host+"/nccloud/resources/erm/expenseaccount/expenseaccount4Cjk/card/index.html#status=add&tradetype="+window.presetVar.tradetype}
        width="100%" className="cjkmodel"></iframe>
            </div>
            <div>

            </div> */}
                    </NCModal.Body>
                </NCModal>

                <NCModal size="xlg" show={
                    this.state.showFyytModal
                }
                    onHide={
                        this.closefyyt
                    }

                >
                    <NCModal.Header closeButton="true">
                        {/*"201102BCLF-0003": "核销预提"*/}
                        <NCModal.Title >{multiLang && multiLang.get('201102BCLF-0003')}</NCModal.Title>
                    </NCModal.Header>

                    <NCModal.Body >
                        <Fyyt showFyytModal={this.state.showFyytModal} setDefaultSelectedYt={this.setDefaultSelectedYt} />
                        {/* <div className="area-content" >
            <iframe src={window.location.protocol+"//"+window.location.host+"/nccloud/resources/erm/expenseaccount/expenseaccount4Fyyt/card/index.html#status=add&tradetype="+window.presetVar.tradetype}
        width="100%" className="cjkmodel"></iframe>
            </div> */}
                        <div>

                        </div>
                    </NCModal.Body>
                </NCModal>


                 
                {/* 联查费用申请单 */}
                {/*<LinkQueryFysqModel
                    show={this.state.showLinkQueryFysqModel}
                    close={() => this.setState({showLinkQueryFysqModel: false})}
                    tradetype={window.presetVar.tradetype}
                    openBillId={this.state.billId}
                    {...this.props} />*/}

                {/* 联查费用预提单 */}
                <LinkQueryFyytModel
                    show={this.state.showLinkQueryFyytModel}
                    linkData={this.state.linkData}
                    close={() => this.setState({ showLinkQueryFyytModel: false })}
                    tradetype={window.presetVar.tradetype}
                    openBillId={this.state.billId}
                    {...this.props} />

                {/* 联查借款单 */}
                <LinkQueryJkbxModel
                    show={this.state.showLinkQueryJkbxModel}
                    linkData={this.state.linkData}
                    close={() => this.setState({ showLinkQueryJkbxModel: false })}
                    tradetype={window.presetVar.tradetype}
                    openBillId={this.state.billId}
                    {...this.props} />
                <div>
                    <Inspection
                        show={this.state.showLinkBudget}
                        sourceData={this.state.sourceData}
                        cancel={this.cancel.bind(this)}
                        affirm={this.affirm.bind(this)}
                    />
                </div>
                <div>
                    {/*"201102FYYT-0003": "指派",*/}
                    {this.state.compositedisplay ? <ApprovalTrans
                        title={multiLang && multiLang.get('201102FYYT-0003')}
                        data={this.state.compositedata}
                        display={this.state.compositedisplay}
                        getResult={this.getAssginUser}
                        cancel={this.turnOff.bind(this)}
                    /> : ""}

                </div>
                <AttachmentOther tableData={this.state.myTabdata}/>
                {createModal('detail', { noFooter: true })}
                {createModal('detailFPcy', { noFooter: true })}
                {createModal('detailFPdr', { noFooter: true })}
            </div>
            
            </div>
        )
    }
}

jkbxBillCard = createPage({
    mutiLangCode: '2011',
    billinfo: {
        billtype: 'extcard',
        pagecode: "201102BCLF_C",
        headcode: "head",
        bodycode: ["arap_bxbusitem", "other", "zsitem", "bzitem", "er_cshare_detail", "er_bxcontrast", "accrued_verify"]
    },
    orderOfHotKey: ["arap_bxbusitem", "other", "zsitem", "bzitem"]
})(jkbxBillCard);

export default jkbxBillCard;
