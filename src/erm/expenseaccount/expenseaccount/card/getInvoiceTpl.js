/**
 * 电子发票列表模板
 * @type {{}}
 */
import { invoiceEvent } from './events';

let getInvoiceTpl = function () {
    const _this = this
    let props = _this.props;
    let multiLang = props.MutiInit.getIntl(2011);
    let dzfpTemplate = {

        tpl:[
            {title: multiLang && multiLang.get("201102BCLF-0029"), dataIndex: "file_name", key: "file_name", width: 100},
            {title: multiLang && multiLang.get("201102BCLF-0030"), dataIndex: "invoice_code", key: "invoice_code", width: 100},
            {title: multiLang && multiLang.get("201102BCLF-0031"), dataIndex: "invoice_number", key: "invoice_number", width: 100},
            {title: multiLang && multiLang.get("201102BCLF-0032"), dataIndex: "date", key: "date", width: 100},
            {title: multiLang && multiLang.get("201102BCLF-0033"), dataIndex: "money_amount", key: "money_amount", width: 100},
            {title: multiLang && multiLang.get("201102BCLF-0034"), dataIndex: "name", key: "name", width: 100},
            {
                title: multiLang && multiLang.get("201102BCLF-0035"),
                dataIndex: "controler",
                key: "controler",
                render(text, record, index){
                    return (
                        <div>
                        <a className='list-btn-click' onClick={() => {
                        invoiceEvent.del.call(_this,text, record, index)
                    }
                }>{multiLang && multiLang.get("201102BCLF-0036")}</ a>
                    <span> | </span>
                    <a className='list-btn-click' onClick={ ()=>{
                        invoiceEvent.view.call(_this,text, record, index)
                    }}>{multiLang && multiLang.get("201102BCLF-0037")}</ a>
                    </div>
                )
                }
            }
        ]};
    return dzfpTemplate.tpl;
}


export default getInvoiceTpl;
