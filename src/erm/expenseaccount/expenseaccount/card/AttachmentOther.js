import React, {PureComponent,Fragment} from 'react';
import { createPage, base } from 'nc-lightapp-front';
const { NCTable }=base;
import './AttachmentOther.less';


 class AttachmentOther extends PureComponent{
    constructor(props){
        super(props);
        this.state={
            tableon:true,
        }
        this.taboff=this.taboff.bind(this)
    }
    taboff(){
        this.setState({
            tableon:!this.state.tableon
        })
    }
    render(){
        const {tableData={}}=this.props;
        const {tableon}=this.state;
        let myData=tableData.file&&tableData.file.filter(item=>item&&item.name.indexOf('发票')<0);
        const urlParam=this.props.getUrlParam("scene");
        return(
            <Fragment>
                {urlParam==='approvesce'&&tableData.fileAuthorize?
                    <div className={'myTable-wrap'} >
                    <div className='fjtitle'><i className={`iconfont icon table-tabs-icon tranform-icon ${tableon?'icon-bottom':'icon-right'}`} onClick={this.taboff}></i>附件 (非发票)  <span></span></div>
                    <NCTable
                    className='myTable'
                        columns={[
                            {title:'附件名',key:'previewUrl',dataIndex: 'previewUrl',render:(v,record)=>{
                        return(<a className='attachment-name' href={v} target="_blank">{record.name}</a>)
                        }}]}
                        data={myData}
                        bodyStyle={tableon?{display:'block'}:{display:'none'}}
                    />
                    </div>:
                    null
                }
               
            </Fragment>
        )
    }
   
}
export default createPage({})(AttachmentOther)