import 'ssccommon/components/globalPresetVar';
import presetVar from "../../adjustment/card/presetVar";

//全局变量尽量配置在presetVar中，防止全局变量污染。也方便统一管理。
window.presetVar = {
    ...window.presetVar,
    pageId: '',
    pagecode: '',
    appcode:'',
    pullpagecode:'',
    pagestatus:'',
    reloadHead:'',//一直保存表头最新的数据
    headValuesAll:'',//缓存表头数据
    bodyChangeValuesAll:'',//缓存表体数据
    //表头（主表）变量配置
    head: {
        head1: 'head',
        head2: ''
    },
    //表体（子表）变量配置
    body: {
        body1:"arap_bxbusitem",
    },
    tradetype:'',
    //搜索模板变量配置
    search: {},
    redata:'',
    busitemarea:[
        "head",
        "arap_bxbusitem",
        "arap_bxbusitem&childform1",
        "arap_bxbusitem&childform2",
        "other",
        "other&childform1",
        "other&childform2",
        "zsitem",
        "zsitem&childform1",
        "zsitem&childform2",
        "bzitem",
        "bzitem&childform1",
        "bzitem&childform2",
        ],
     otherarea:[
        "er_cshare_detail",
        "er_cshare_detail&childform1",
        "er_cshare_detail&childform2",
        "er_bxcontrast",
        "er_bxcontrast&childform1",
        "er_bxcontrast&childform2",
        "accrued_verify",
        "accrued_verify&childform1",
        "accrued_verify&childform2",
     ]
};
export default presetVar;