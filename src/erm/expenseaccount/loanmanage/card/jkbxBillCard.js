import './presetVar'
import React, { Component } from 'react';
import { createPage, base, high, pageTo,getAttachments} from 'nc-lightapp-front';
import { tableButtonClick, pageButtonClick, beforeEvent,afterEvent, pageInfoClick, refreshButtonClick } from './events';
import initTemplate from './initTemplate';
import {getCookie, dataHandle, viewInitBill} from './initTemplate'
// import Uploader from 'ssccommon/components/uploader'
import AttachmentOther from '../../../expenseaccount/expenseaccount/card/AttachmentOther';
import {getAfterEventParams} from 'ssccommon/utils/formulaUtils'
import pubMessage from 'ssccommon/utils/pubMessage'
import getAttachmentLength from '../../../public/components/linkAttachment/getAttachmentLength'


import requestApi from "./requestApi";
const { ApproveDetail } = high;
let {NCModal} = base;
import {
    BillStyle,
    BillHead,
    BillBody,
    Form,
    CardTable,
    CardTableGroup,
    ButtonGroup,
    MultiButtonGroup
} from 'ssccommon/components/bill';

import './index.less';
import LinkQueryJkbxModel from 'ssccommon/linkquery/LinkQueryJkbxModel';

const { NCUploader,Inspection,ApprovalTrans } = high;

const pageButton = window.presetVar.pageButton;

window.assingUsers=[]//指派信息
window.sureAssign = false;//确定指派
window.assignBillId = '';//有指派的情况下，第一次点保存提交实际保存成功的单据pk

class jkbxBillCard extends Component {
    constructor(props) {
        super();
        this.dataSource = 'erm.expenseaccount.initbills.config';
        this.cardPagStatus = {
            modify: 1,
            add: 2,
            del: 3
        }
        this.state = {
            showUploader: false,
            modalControl: false,
            fileList:[],
            groupLists:0,
            billId : '',
            showLinkQueryFysqModel: false,
            showLinkQueryJkbxModel: false,
            sourceData: null,
            showLinkBudget: false,
            isinit : props.getUrlParam("isinit"),
	        hideTables:[],
            showBack : false,
            ShowApproveDetail: false,
            billtype: '',
            pageInfoShow : false,
            compositedata : '',
            compositedisplay : '',
            transtype_name:'',
            linkData : '',
            tableData:{},
            items:{} // 维护表头隐藏区域items -2019-09-11
        }
        //initTemplate.call(this, props);
        this.pubMessage = new pubMessage(props);
        if(props.getUrlParam("isinit") === 'true'){
	        if(props.getUrlParam("status") === 'browse'){
                this.setState({ pageInfoShow : true });
            }
            window.setTimeout(() => {
                this.setState({ showBack : true})
            }, 20);
        }
        
        let billid=props.getUrlParam("id")
        if(billid){
        getAttachmentLength(billid).then(lists=>{
        
            this.setState({
                groupLists:lists
            },()=>{
            initTemplate.call(this, props, true);
            })
        })
        }else{
            initTemplate.call(this, props);
        }

        // TOP NCCLOUD-61317 增加公式编辑后联动 chengwebc ADD
        props.setRelationAfterEventCallBack && props.setRelationAfterEventCallBack((props, changedField, changedArea, oldCard, newCard, index, rowId)=>{
            let params = getAfterEventParams(props, changedField, changedArea, oldCard, newCard, index, rowId);
            params.map((one)=>{
                afterEvent.call(this, props, one.moduleId, one.key, one.value, one.changedrows, one.index)
            })
        });
        // BTM NCCLOUD-61317 增加公式编辑后联动 chengwebc

    }

    componentWillMount() {
                   window.onbeforeunload = () => {
                    let status = this.props.getUrlParam("status");
                    let addType = this.props.getUrlParam('addType');
                    if (addType!="pull" && (status == 'edit'||status == 'add')) {
                        return '当前单据未保存，确认离开？'/* 国际化处理： 确定要离开吗？*/
                    }
                }
            }

    // //删除单据
    // billRemove(){
        
    // }
    // billInvalid(){
    //     // let param = {};
    //     // param.pagecode=this.props.getSearchParam("p");
    //     // param.openbillid=this.props.getUrlParam("id");
    //     // param.tradetype=window.presetVar.tradetype;
    //     // requestApi.billInvalid({
    //     //     data: param,
    //     //     success: (data) => {
    //     //         location.reload();
    //     //         if (data.success) {
    //     //             location.reload();
    //     //         }
    //     //     }
    //     // });
    // }
    componentDidMount(props) {
        
    }

    componentDidUpdate(){

    }
    cancel(){
        this.setState({
            showLinkBudget: false
        })
    }
    affirm(info){
        this.setState({
            showLinkBudget: false
        })
    }
    closeApprove = () => {
        this.setState({
            ShowApproveDetail: false
        })
    }
    getAssginUser = (value) => {
        window.assingUsers = value;
        window.sureAssign = true;
        pageButtonClick.call(this)[pageButton.billSubmit]();
        window.assignBillId = '';
    }

    turnOff(){
        this.setState({
            compositedisplay:false
        }) ;
        window.sureAssign = false;
        window.assingUsers = [];
        let props = this.props;
        let billid = window.assignBillId;
        if(!billid){
            billid = props.form.getFormItemsValue("head","pk_jkbx").value;
        }
        window.presetVar.redata = 'Y';
        props.linkTo('/erm/expenseaccount/loanmanage/card/index.html',{scene:props.getUrlParam('scene'),pagecode:props.getSearchParam('p'),isfromSsc:true,status:window.presetVar.status.browse,id:billid,tradetype:window.presetVar.tradetype, ntbCheck:'false'});
    }

    render() {
        let {modal, cardPagination } = this.props;
        let {createModal} = modal;
        const { createCardPagination } = cardPagination;

        let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId
        let btnModalConfig = {
            [pageButton.pageDel]: {
                // "2011-0004": "删除"
                // "2011-0005": "确认删除该单据吗？"
                title: multiLang && multiLang.get('2011-0004'),
                content: multiLang && multiLang.get('2011-0005'),
            },
            [pageButton.billInvalid]: {
                // "2011-0009": "作废",
                // "2011-0010": "确认作废该单据吗？"
                title: multiLang && multiLang.get('2011-0009'),
                content: multiLang && multiLang.get('2011-0010'),
            }
        }
        return (
            <div >
            <BillStyle
                {...this.props}
            >
            <BillHead 
                back={{
                    showBack : this.state.showBack,
                    onClick : () =>{
                        this.props.pushTo('/list',{
                            appcode:'201103QCDJ',
                            pagecode:'201103QCDJ_list'
                        });
                    }
                }}
                title={this.state.transtype_name}
                refreshButtonEvent={refreshButtonClick.bind(this)}
	    	    pageInfoConfig={{
                    show:this.state.pageInfoShow,
                    handlePageInfoChange: pageInfoClick.bind(this),
                    dataSource : this.dataSource
                }} >
                <ButtonGroup
                    areaId="head"
                    buttonEvent={pageButtonClick.bind(this)}
                    modalConfig={btnModalConfig}
                />
                <Form
                areaId="head"
                onAfterEvent={afterEvent.bind(this)}/>
            </BillHead>

            <BillBody>


                    <CardTableGroup
                        excludeTableAreaId={null}
                        totalItemCode="amount"
                        totalItemCodeByAreas={ //
                            {
                                'accrued_verify': 'verify_amount',
                                'er_bxcontrast':'cjkybje',
                                'er_cshare_detail':'assume_amount'
                            }
                        }
                        modelSave={pageButtonClick.call(this)['pageSave']}
                        onBeforeEvent={beforeEvent.bind(this)}
                        onAfterEvent={afterEvent.bind(this)}
                        invisibleTables={this.state.hideTables}
                    >
                    <MultiButtonGroup buttonEvent={tableButtonClick.bind(this)}   />
                </CardTableGroup>

                {
                    this.state.showUploader && 
                    < NCUploader
                    billId={ this.state.billId }
                    disableModify={this.props.form.getFormItemsValue("head", 'spzt')&&this.props.form.getFormItemsValue("head", 'spzt').value!="-1"}
                    // target={target}
                    onHide={() => {
                        this.setState({showUploader: false})
                        getAttachmentLength(this.state.billId).then(lists => {
                            //附件
                            this.props.button.setButtonTitle('fileManager', multiLang && multiLang.get('201102JCLF_C-0013') + ' ' + lists + ' ');
                        })
                    }
                      
                    
                }
                 customInterface={
                                {
                                    queryLeftTree:"/nccloud/erm/pub/ErmAttachmentTreeNodeQueryAction.do",
                                    queryAttachments: "/nccloud/erm/pub/ErmAttachmentQueryAction.do"
                                }
                            }

                    placement={'bottom'} />
                }

                <div>
                    {<ApproveDetail
                        show={this.state.ShowApproveDetail}
                        close={this.closeApprove}
                        billtype={this.state.billtype}
                        billid={this.state.billId}
                    />}
                   <AttachmentOther tableData={this.state.tableData}/>
                </div>

               
            </BillBody>

            </BillStyle>
                {/* {this.state.modalControl&&
                <Uploader
                    {...this.props}
                    modalControl={() => {
                        this.setState({
                            modalControl: false
                        })
                    }}
                    // 标题
                    //"201102BCLF-0001": "电子发票上传列表"
                    title={multiLang && multiLang.get('201102BCLF-0001')}
                    // title="电子发票上传列表"
                    // 上传文件列表
                    fileList={this.state.fileList}
                    // 附件上传限制
                    attachMaxSize={'3'}

                    billId = {this.state.billId}
                    // 上传附件接口
                    uploadAttachment={'/nccloud/erm/expenseaccount/ExpenseaccountDzfp.do'}
                    // 列表模板
                    listTemplate={getInvoiceTpl.call(this)}

                    uploadChange={invoiceEvent.done.bind(this)}
                />
                } */}

               

                <NCModal size="xlg"  show = {
                    this.state.showCJKModal
                    }
                    onHide = {
                         this.closecjk
                    }

                >
    
   
            </NCModal>

                {/* 联查费用申请单 */}
                {/*<LinkQueryFysqModel
                    show={this.state.showLinkQueryFysqModel} 
                    close={() => this.setState({showLinkQueryFysqModel: false})} 
                    tradetype={window.presetVar.tradetype}
                    openBillId={this.state.billId}
                    {...this.props} />*/}

               
                {/* 联查报销单 */}
                <LinkQueryJkbxModel
                    show={this.state.showLinkQueryJkbxModel} 
                    linkData={this.state.linkData}
                    close={() => this.setState({showLinkQueryJkbxModel: false})} 
                    tradetype={window.presetVar.tradetype}
                    openBillId={this.state.billId}
                    {...this.props} />
                <div>
                    <Inspection
                        show={ this.state.showLinkBudget }
                        sourceData = { this.state.sourceData }
                        cancel = { this.cancel.bind(this) }
                        affirm = { this.affirm.bind(this) }
                    />
                </div>
                <div>
                    {/*"201102FYYT-0003": "指派",*/}
                    {this.state.compositedisplay ? <ApprovalTrans
                        title= {multiLang && multiLang.get('201102FYYT-0003')}
                        data={this.state.compositedata}
                        display={this.state.compositedisplay}
                        getResult={this.getAssginUser}
                        cancel={this.turnOff.bind(this)}
                     /> : ""}
                </div>
            </div>
        )
    }
}

jkbxBillCard = createPage({
    mutiLangCode: '2011',
    billinfo:{
        billtype: 'extcard', 
        pagecode: pageTo.getSearchParam("p"), 
        headcode: 'head',
        bodycode: ['jk_busitem', 'er_bxcontrast']
    },
    orderOfHotKey: ['jk_busitem']
})(jkbxBillCard);

export default jkbxBillCard;