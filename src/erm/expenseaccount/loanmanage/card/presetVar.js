import 'ssccommon/components/globalPresetVar';

//全局变量尽量配置在presetVar中，防止全局变量污染。也方便统一管理。
window.presetVar = {
    ...window.presetVar,
    pageId: '',
    pageCode: '20110ETLB_2631_IWEB',
    bodyChangeValuesAll:'',//缓存表体数据
    //表头（主表）变量配置
    head: {
        head1: 'head',
        head2: ''
    },
    //表体（子表）变量配置
    body: {

    },
    tradetype:'',
    redata:'',
    //搜索模板变量配置
    search: {},
};