const { body1} = window.presetVar.body;
const {head1} = window.presetVar.head;

/**
 * 表头合计处理
 */
export function headSumHandle() {
    //需要汇总的金额字段
    let bodyFieldArr = ["amount", "ybje", "zfybje", "hkybje", "bbje",
         "zfbbje", "hkbbje", "globalbbje",  "globalzfbbje", "globalhkbbje",
        "groupbbje", "groupzfbbje", "grouphkbbje"/*, "tax_amount", "orgtax_amount", "grouptax_amount", "globaltax_amount",
        "tni_amount", "orgtni_amount", "grouptni_amount", "globaltni_amount", "vat_amount", "orgvat_amount", "groupvat_amount", "globalvat_amount"*/];
    let changeHeadItem = {};
    bodyFieldArr.forEach((bodyField) => {//循环字段
        let totalMny = 0;
        for(let name in window.presetVar.body){//循环业务页签
            let areaId = window.presetVar.body[name];
            if(areaId === 'jk_busitem'){
                let rows = this.props.cardTable.getVisibleRows(areaId);
                for(let i in rows){
                    totalMny += +rows[i].values[bodyField].value;//汇总
                }
                /* let colVal = this.props.cardTable.getColValue(areaId, bodyField);//取列值
                if(colVal){
                    colVal.forEach((itemVal) => {
                        totalMny += +itemVal.value;//汇总
                    });
                } */
            }
        }
        let headField = bodyField=="amount"?"total":bodyField;//total字段特殊处理
        console.log(this.props.form.getFormItemsValue(head1,headField));
        let scale = this.props.form.getFormItemsValue(head1,headField).scale;//取精度
        changeHeadItem[headField] = {scale: scale, value: totalMny, display: null};
    });
    this.props.form.setFormItemsValue(head1, changeHeadItem)
}
