import { ajax, cardCache } from 'nc-lightapp-front';
import requestApi from '../requestApi';
import {dataHandle} from '../initTemplate';
import {setPageStatus} from 'ssccommon/utils/statusController';
let { getCacheById, updateCache } = cardCache;

export default function (props, pk) {
    let appcode = (props.getUrlParam("appcode")==undefined||props.getUrlParam("appcode")=='')?props.getSearchParam("c"):props.getUrlParam("appcode");
    let pagecode =  (props.getUrlParam("pagecode")==undefined||props.getUrlParam("pagecode")=='')?props.getSearchParam("p"):props.getUrlParam("pagecode");
    let cachData = getCacheById(pk,this.dataSource);
    if(pk){
        if(cachData){
            let data = cachData;
            props.form.setAllFormValue({['head']:data.head['head']});
            //设置表体数据
            Object.values(presetVar.body).forEach((body) =>{
                if(data.bodys[body]){
                    props.cardTable.setTableData(body, data.bodys[body]);
                }
            });
            dataHandle.call(this, data.head["head"],props);
            //if(pageStatus == statusVar.browse){
            let ls_bodys = [];
            Object.values(window.presetVar.body).forEach((body)=>{
                ls_bodys.push(body);
            });
            setPageStatus(props, meta, ['head'], ls_bodys, 'browse');
            props.setUrlParam({'id' : pk});
        }else{
            let viewbilldata = {};
            viewbilldata.openbillid = pk;
            viewbilldata.tradetype = props.getUrlParam("tradetype");
            viewbilldata.uid = getCookie("userid");//"1001Z3100000000000AE";这里以后从session中获取
            viewbilldata.pagecode = pagecode;
            viewbilldata.appcode = appcode;
            requestApi.viewBill({
                data:viewbilldata,
                success:(data) =>{
                    props.form.setAllFormValue({['head']:data.head['head']});
                    //设置表体数据
                    Object.values(presetVar.body).forEach((body) =>{
                        if(data.bodys[body]){
                            props.cardTable.setTableData(body, data.bodys[body]);
                        }
                    });
                    dataHandle.call(this,data.head["head"],props);
                    //if(pageStatus == statusVar.browse){
                    props.form.setFormStatus('head', 'browse');
                    Object.values(window.presetVar.body).forEach((body)=>{
                        props.editTable.setStatus(body, 'browse');
                    });
                    updateCache("pk_jkbx",pk,data,"head", this.dataSource);
                    props.cardPagination.setCardPaginationId({ id: pk, status: this.cardPagStatus.modify });
                    props.setUrlParam({'id' : pk});
                }
            });
        }
    }
}
function getCookie (name){
    var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
    if(arr=document.cookie.match(reg))
        return unescape(arr[2]);
    else
        return "";
}