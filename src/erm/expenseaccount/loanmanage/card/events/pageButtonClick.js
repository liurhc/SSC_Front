import {ajax,print,base,promptBox,cardCache, toast, pageTo} from 'nc-lightapp-front';
import requestApi from '../requestApi';
import {dataHandle, getCookie, getRequest1, loadBill, viewInitBill} from '../initTemplate';
import {imageScan, imageView} from "sscrp/rppub/components/image";
import pubUtils  from "ssccommon/utils/pubUtils";
import {setPageStatus} from 'ssccommon/utils/statusController';
import linkVoucherApi from '../../../../public/components/linkvoucher/linkvoucher'
import linkBudgetApi from '../../../../public/components/linkbudget/linkbudget'
import linkQueryApi from '../../../../public/components/linkquery/linkquerybills'
import {removeEmptyData}  from "ssccommon/utils/dataFilter";
import {updataFormData, updatacardTableData} from 'ssccommon/utils/changePageData';

let pageButton = window.presetVar.pageButton;
let areaBtnAction = window.presetVar.areaBtnAction;
let statusVar = window.presetVar.status;
const { NCMessage } = base;
let jkCheck = 'false';
let ntbCheck = 'false';
let { addCache, updateCache, getNextId, deleteCacheById, getCurrentLastId  } = cardCache;

let body1 = 'jk_busitem';
function pageButtonClick() {
    let billtype = window.presetVar.tradetype;
    let props = this.props;
    let bodys = Object.values(window.presetVar.body);
    let bodyarr = [];
    let hideTable = this.state.hideTables;
    let multiLang = props.MutiInit.getIntl(2011);
    bodys && bodys.length && bodys.map((one)=>{
        if(hideTable && hideTable.indexOf(one)<0){
            bodyarr[bodyarr.length] = one;
        }
    })
    return {
        [pageButton.pageSave]:()=>{
            props.cardTable.filterEmptyRows(body1, ['amount'], 'include');

            let billdata =  props.createExtCardData(pageTo.getSearchParam("p"), "head", bodyarr);
            props.cardTable.closeModel('jk_busitem');
            props.validateToSave(billdata,()=>{
                let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId
                let checkResult = props.form.isCheckNow('head') && props.cardTable.checkTableRequired();
                if (checkResult) {
                    let urlpk = this.props.getUrlParam("id");
                    let send_data = {};
                    //处理表体删除的行，不往后台传
                    for(var i=0,flag=true,len=billdata.bodys.jk_busitem.rows.length;i<len;flag ? i++ : i){
                        if(billdata.bodys.jk_busitem.rows[i] != undefined && billdata.bodys.jk_busitem.rows[i].status == 3){
                            billdata.bodys.jk_busitem.rows.splice(i,1);
                            flag = false;
                        }else{
                            flag = true;
                            //新增行清楚主键
                            if(billdata.bodys.jk_busitem.rows[i] != undefined && billdata.bodys.jk_busitem.rows[i].status == 2){
                                billdata.bodys.jk_busitem.rows[i].values.pk_busitem={};
                            }
                        }
                    }
                    let paramurl = getRequest1(parent.window.location.hash);
                    let paramdata = {};
                    console.log(billdata);
                    paramdata.fromssc = "ssc",
                    paramdata.jkCheck = jkCheck,
                    paramdata.ntbCheck = ntbCheck,
                    paramdata.msg_pk_bill = "",
                    paramdata.accessorybillid = this.accessorybillid;
                    paramdata.pk_tradetype = billtype;
                    paramdata.pagecode=paramurl.p;
                    paramdata.appcode = paramurl.c;
                    paramdata.isinit = this.state.isinit;
                    let status = this.props.getUrlParam("status");
                    if(status =="edit"){
                        paramdata.status = "edit";
                    }else{
                        paramdata.status = "save";
                    }
                    billdata.head.userjson=JSON.stringify(paramdata);
                    send_data={billdata,paramdata};
                    console.log(billdata);
                    let vat_amount = props.form.getFormItemsValue('head',"total").value;
                    if(vat_amount==undefined||vat_amount==null||vat_amount==0||vat_amount=='0.00000000'){
                        toast({content:multiLang && multiLang.get('201102FYYT-0019')/*"金额不能为空!"*/,color:'warning'});
                        return;
                    }
					// TOP NCCLOUD-91597 校验报销标准取模板时需要使用模板ID chengwbc ADD
					billdata.templetid = props.meta.getMeta().pageid;
					// BTM NCCLOUD-91597 校验报销标准取模板时需要使用模板ID
                    requestApi.savejkbx({
                        data:billdata,
                        meta:billdata,
                        success:(data)=>{
                            ntbCheck = 'false';
                        if(data['bugetAlarm']){ 
                            promptBox({
                                color: 'warning',       // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                                title: multiLang && multiLang.get('201102JCLF_C-0002'),//"提示信息"
                                content:  (multiLang && multiLang.get('201102JCLF_C-0003'))+ data['bugetAlarm'] + (multiLang && multiLang.get('201102JCLF_C-0004')),
                                noFooter: false,  // 是否显示底部按钮(确定、取消),默认显示(false),非必输
                                noCancelBtn: false,    // 是否显示取消按钮,，默认显示(false),非必输
                                beSureBtnName:  multiLang && multiLang.get('2011-0003'),
                                cancelBtnName: multiLang && multiLang.get('2011-0002') ,
                                beSureBtnClick: ()=>{
                                    ntbCheck = 'true';
                                    pageButtonClick.call(this)[pageButton.pageSave]();
                                },
                                cancelBtnClick: () => {
                                    jkCheck = 'false';
                                }  // 取消按钮点击调用函数,非必输
                            })
                        }
                        else if(data['jkAlarm']){
                                promptBox({
                                    color: 'warning',       // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                                    title: multiLang && multiLang.get('201102JCLF_C-0002'),//"提示信息"
                                    content:  (multiLang && multiLang.get('201102JCLF_C-0003'))+ data['jkAlarm'] + (multiLang && multiLang.get('201102JCLF_C-0004')),
                                    noFooter: false,  // 是否显示底部按钮(确定、取消),默认显示(false),非必输
                                    noCancelBtn: false,    // 是否显示取消按钮,，默认显示(false),非必输
                                    beSureBtnName:  multiLang && multiLang.get('2011-0003'),
                                    cancelBtnName: multiLang && multiLang.get('2011-0002') ,
                                    beSureBtnClick: ()=>{
                                        jkCheck = 'true';
                                        pageButtonClick.call(this)[pageButton.pageSave]();
                                    }/* ,
                                    cancelBtnClick: () => {
                                        jkCheck = 'false';
                                    } */
                                })
                            }
                            else{
                                let billid = data.head.head.rows[0].values.pk_jkbx.value;
                                if(data.head.head.rows[0].values.qcbz.value){
                                    //期初借款需要更新缓存
                                    if(urlpk && urlpk === billid){
                                        updateCache("pk_jkbx", billid, data, "head",this.dataSource);
                                        props.cardPagination.setCardPaginationId({ id: billid, status: this.cardPagStatus.modify });
                                    } else {
                                        addCache(billid, data, "head", this.dataSource);
                                        props.cardPagination.setCardPaginationId({ id: billid, status: this.cardPagStatus.add });
                                    }
                                    props.pushTo('/edit',{
                                        status: 'browse',
                                        tradetype : window.presetVar.tradetype,
                                        id: billid,
                                        isinit : true
                                    });
                                }else{
                                    if(data.userjson && JSON.parse(data.userjson) && JSON.parse(data.userjson).reimmsg && JSON.parse(data.userjson).reimmsg.length>0)
                                    {
                                        promptBox({
                                            color: 'warning',       // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                                            title: multiLang && multiLang.get('201102JCLF_C-0002'),
                                            content:JSON.parse(data.userjson).reimmsg,
                                            noFooter: false,  // 是否显示底部按钮(确定、取消),默认显示(false),非必输
                                            noCancelBtn: false,    // 是否显示取消按钮,，默认显示(false),非必输
                                            beSureBtnName:multiLang && multiLang.get('2011-0003'),
                                            cancelBtnName: multiLang && multiLang.get('2011-0002') ,
                                            beSureBtnClick: ()=>{
                                                updataFormData(props, "head", requestApi.filterDatas(data.head["head"]));
                                                bodyarr.forEach((item) =>{
                                                    setTimeout(()=>{
                                                        updatacardTableData(props, item, requestApi.filterDatas(data.bodys[item]));
                                                    },0);
                                                })
                                                    // pubUtils.setUrlParams(props, '/erm/expenseaccount/loanmanage/card/index.html', {scene:props.getUrlParam('scene'),pagecode:props.getSearchParam('p'),isfromSsc:true,status:statusVar.browse,id:billid,tradetype:billtype,jkCheck:jkCheck, ntbCheck:ntbCheck});
                                                    props.linkTo('/erm/expenseaccount/loanmanage/card/index.html',{scene:props.getUrlParam('scene'),pagecode:props.getSearchParam('p'),isfromSsc:true,status:statusVar.browse,id:billid,tradetype:billtype, ntbCheck:ntbCheck});
                                            },   // 确定按钮点击调用函数,非必输
                                            cancelBtnClick: ()=>{
                                                jkCheck = 'false';
                                                updataFormData(props, "head", requestApi.filterDatas(data.head["head"]));
                                                bodyarr.forEach((item) =>{
                                                    setTimeout(()=>{
                                                        updatacardTableData(props, item, requestApi.filterDatas(data.bodys[item]));
                                                    },0);
                                                })
                                                // pubUtils.setUrlParams(props, '/erm/expenseaccount/loanmanage/card/index.html', {scene:props.getUrlParam('scene'),pagecode:props.getSearchParam('p'),isfromSsc:true,status:statusVar.edit,id:billid,tradetype:billtype,jkCheck:jkCheck, ntbCheck:ntbCheck});
                                                props.linkTo('/erm/expenseaccount/loanmanage/card/index.html',{scene:props.getUrlParam('scene'),pagecode:props.getSearchParam('p'),isfromSsc:true,status:statusVar.edit,id:billid,tradetype:billtype, ntbCheck:ntbCheck});
                                            }  // 取消按钮点击调用函数,非必输
                                        })
                                    }else{
                                        // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                                        // props.form.setAllFormValue({["head"]: requestApi.filterDatas(data.head["head"])});
                                        // bodyarr.forEach((item) =>{
                                        //     setTimeout(()=>{
                                        //         props.cardTable.setTableData(item, requestApi.filterDatas(data.bodys[item]));
                                        //     },0);
                                        // })
                                        updataFormData(props, "head", requestApi.filterDatas(data.head["head"]));
                                        bodyarr.forEach((item) =>{
                                            setTimeout(()=>{
                                                updatacardTableData(props, item, requestApi.filterDatas(data.bodys[item]));
                                            },0);
                                        })
                                        // BTM 性能优化，变更页面数据更新方法
                                        window.presetVar.redata='N';
                                        // pubUtils.setUrlParams(props, '/erm/expenseaccount/loanmanage/card/index.html', {scene:props.getUrlParam('scene'),pagecode:props.getSearchParam('p'),isfromSsc:true,status:statusVar.browse,id:billid,tradetype:billtype,jkCheck:jkCheck, ntbCheck:ntbCheck});
                                        props.linkTo('/erm/expenseaccount/loanmanage/card/index.html',{scene:props.getUrlParam('scene'),pagecode:props.getSearchParam('p'),isfromSsc:true,status:statusVar.browse,id:billid,tradetype:billtype, ntbCheck:ntbCheck});
                                    }
                                }
                            }
                        },
                        error:(data)=>{
                            // alert(data.message);
                        }
                    });
                }
            },bodys,null)
        },

        [pageButton.billSubmit]:()=>{
            props.cardTable.filterEmptyRows(body1, ['amount'], 'include');

            let billdata =  props.createExtCardData(pageTo.getSearchParam("p"), "head", bodyarr);
            props.validateToSave(billdata,()=>{
                let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId
                let checkResult_sub = props.form.isCheckNow('head') && props.cardTable.checkTableRequired();
                if (checkResult_sub) {
                    let send_data = {};
                    //处理表体删除的行，不往后台传
                    for(var i=0,flag=true,len=billdata.bodys.jk_busitem.rows.length;i<len;flag ? i++ : i){
                        if(billdata.bodys.jk_busitem.rows[i] != undefined && billdata.bodys.jk_busitem.rows[i].status == 3){
                            billdata.bodys.jk_busitem.rows.splice(i,1);
                            flag = false;
                        }else{
                            flag = true;
                            //新增行清楚主键
                            if(billdata.bodys.jk_busitem.rows[i] != undefined && billdata.bodys.jk_busitem.rows[i].status == 2){
                                billdata.bodys.jk_busitem.rows[i].values.pk_busitem={};
                            }
                        }
                    }
                    let paramurl = getRequest1(parent.window.location.hash);
                    let paramdata = {};
                    console.log(billdata);
                    paramdata.openbillid=!props.getUrlParam("id")?"":props.getUrlParam("id");
                    paramdata.fromssc = "ssc",
                    paramdata.jkCheck = jkCheck,
                    paramdata.ntbCheck = ntbCheck,
                    paramdata.msg_pk_bill = "",
                    paramdata.pk_tradetype = billtype;
                    paramdata.pagecode=paramurl.p;
                    paramdata.appcode = paramurl.c;
                    paramdata.assingUsers = window.assingUsers;
                    paramdata.accessorybillid = this.accessorybillid;
                    let status = this.props.getUrlParam("status");
                    if(status =="edit"){
                        paramdata.status = "edit";
                    }else{
                        paramdata.status = "save";
                    }
                    //如果指派点了确定  由于第一次点击保存提交实际上已经保存成功了，指派后再次提交应该走浏览提交逻辑
                    if(window.sureAssign){
                        paramdata.status = "edit";
                        paramdata.openbillid = window.assignBillId;
                    }
                    billdata.head.userjson=JSON.stringify(paramdata);
                    // TOP NCCLOUD-91597 校验报销标准取模板时需要使用模板ID chengwbc ADD
					billdata.templetid = this.props.meta.getMeta().pageid;
					// BTM NCCLOUD-91597 校验报销标准取模板时需要使用模板ID
                    console.log(billdata);
                    requestApi.submitBill({
                        data:billdata,
                        success:(data)=>{
                            ntbCheck = 'false';
                        //设置了指派
                        if(data.workflow && data.billid && data.bill && (data.workflow == 'approveflow'|| data.workflow == 'workflow')){
                            this.setState({
                                compositedata:data,
                                compositedisplay:true,
                            }) ;
                            window.assignBillId = data.billid;
                            updataFormData(props, "head", requestApi.filterDatas(data.bill.head["head"]));
                            bodyarr.forEach((item) =>{
                                setTimeout(()=>{
                                    updatacardTableData(props, item, requestApi.filterDatas(data.bill.bodys[item]));
                                },0);
                            })
                            
                        } else{
                            this.setState({
                                compositedisplay:false
                            }) ;
                            if(data['bugetAlarm']){ 
                                promptBox({
                                    color: 'warning',       // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                                    title: multiLang && multiLang.get('201102JCLF_C-0002'),//"提示信息"
                                    content:  (multiLang && multiLang.get('201102JCLF_C-0003'))+ data['bugetAlarm'] + (multiLang && multiLang.get('201102JCLF_C-0004')),
                                    noFooter: false,  // 是否显示底部按钮(确定、取消),默认显示(false),非必输
                                    noCancelBtn: false,    // 是否显示取消按钮,，默认显示(false),非必输
                                    beSureBtnName:  multiLang && multiLang.get('2011-0003'),
                                    cancelBtnName: multiLang && multiLang.get('2011-0002') ,
                                    beSureBtnClick: ()=>{
                                        ntbCheck = 'true';
                                        if(props.getUrlParam('status') === 'browse'){
                                            window.sureAssign = false;
                                        }
                                        pageButtonClick.call(this)[pageButton.billSubmit]();
                                    },
                                    cancelBtnClick: () => {
                                        let billid = props.form.getFormItemsValue("head","pk_jkbx").value;
                                        if(props.getUrlParam('status') === 'browse'){
                                            props.linkTo('/erm/expenseaccount/loanmanage/card/index.html',
                                            {
                                                scene:props.getUrlParam('scene'),
                                                status:statusVar.browse,
                                                id: billid,
                                                pagecode:props.getSearchParam('p'),
                                                tradetype:billtype
                                            });
                                        }else{
                                            if(billid){
                                                props.linkTo('/erm/expenseaccount/loanmanage/card/index.html',
                                                {
                                                    scene:props.getUrlParam('scene'),
                                                    status:statusVar.edit,
                                                    id: billid,
                                                    pagecode:props.getSearchParam('p'),
                                                    tradetype:billtype
                                                });  
                                            }else{}
                                        }
                                        window.assingUsers = [];    //清空指派人信息
                                        window.sureAssign = false;
                                        jkCheck = 'false';
                                    }
                                })
                            }
                            else if(data['jkAlarm']){
                                promptBox({
                                    color: 'warning',       // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                                    title: multiLang && multiLang.get('201102JCLF_C-0002'),
                                    content: (multiLang && multiLang.get('201102JCLF_C-0003'))+ data['jkAlarm'] + (multiLang && multiLang.get('201102JCLF_C-0004')),
                                    noFooter: false,  // 是否显示底部按钮(确定、取消),默认显示(false),非必输
                                    noCancelBtn: false,    // 是否显示取消按钮,，默认显示(false),非必输
                                    beSureBtnName: multiLang && multiLang.get('2011-0003'),
                                    cancelBtnName: multiLang && multiLang.get('2011-0002') ,
                                    beSureBtnClick: ()=>{
                                        jkCheck = 'true';
                                        pageButtonClick.call(this)[pageButton.billSubmit]();
                                    }//,   // 确定按钮点击调用函数,非必输
                                    //cancelBtnClick: functionCancel  // 取消按钮点击调用函数,非必输
                                })
                            }else if(data.userjson && JSON.parse(data.userjson) && JSON.parse(data.userjson).reimmsg && JSON.parse(data.userjson).reimmsg.length>0)
                            {
                                let billid = data.head.head.rows[0].values.pk_jkbx.value;
                                promptBox({
                                    color: 'warning',       // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                                    title: multiLang && multiLang.get('201102JCLF_C-0002'),
                                    content:JSON.parse(data.userjson).reimmsg,
                                    noFooter: false,  // 是否显示底部按钮(确定、取消),默认显示(false),非必输
                                    noCancelBtn: true,    // 是否显示取消按钮,，默认显示(false),非必输
                                    beSureBtnName:multiLang && multiLang.get('2011-0003'),
                                    cancelBtnName: multiLang && multiLang.get('2011-0002') ,
                                    beSureBtnClick: ()=>{
                                        window.presetVar.redata = 'Y';
                                        //重新给页面赋值
                                        updataFormData(props, 'head', requestApi.filterDatas(data.head["head"]));
                                        bodyarr.forEach((item) =>{
                                            setTimeout(()=>{
                                                updatacardTableData(props, item, requestApi.filterDatas(data.bodys[item]));
                                            },0);
                                        })
                                        props.linkTo('/erm/expenseaccount/loanmanage/card/index.html',{
                                            scene:props.getUrlParam('scene'),
                                            status:statusVar.browse,
                                            id: billid,
                                            pagecode:props.getSearchParam('p'),
                                            tradetype:billtype
                                        });
                                        window.assingUsers = [];    //清空指派人信息
                                        window.sureAssign = false;
                                    }
                                })
                            }
                            
                            else{
                                let billid = data.head.head.rows[0].values.pk_jkbx.value;
                                /* pubUtils.setUrlParams(props, '/erm/expenseaccount/loanmanage/card/index.html', {
                                    scene:props.getUrlParam('scene'),
                                    status:statusVar.browse,
                                    id: billid,
                                    pagecode:props.getSearchParam('p'),
                                    tradetype:billtype
                                }); */
                                // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                                // props.form.setAllFormValue({["head"]: requestApi.filterDatas(data.head["head"])});
                                // bodyarr.forEach((item) =>{
                                //     setTimeout(()=>{
                                //         props.cardTable.setTableData(item, requestApi.filterDatas(data.bodys[item]));
                                //     },0);
                                // })
                                updataFormData(props, 'head', requestApi.filterDatas(data.head["head"]));
                                bodyarr.forEach((item) =>{
                                    setTimeout(()=>{
                                        updatacardTableData(props, item, requestApi.filterDatas(data.bodys[item]));
                                    },0);
                                })
                                // BTM 性能优化，变更页面数据更新方法
                                window.presetVar.redata='N';
                                
                                props.linkTo('/erm/expenseaccount/loanmanage/card/index.html',
                                {
                                    scene:props.getUrlParam('scene'),
                                    status:statusVar.browse,
                                    id: billid,
                                    pagecode:props.getSearchParam('p'),
                                    tradetype:billtype
                                });
                                // location.hash = `#scene=${props.getUrlParam('scene')}&status=${statusVar.browse}&id=${billid}&tradetype=${billtype}`;
                                props.button.setButtonsVisible({
                                    [pageButton.pageSave] : false,
                                    [pageButton.pageEdit] : true
                                });
                                //处理按钮相关状态
                                dataHandle.call(this,data.head.head,props);
                                window.assingUsers = [];    //清空指派人信息
                                window.sureAssign = false;
                            }
                        }
                        },
                        error:(data)=>{
                            toast({ content: data.message, color: 'danger' });
                            // alert(data.message);
                        }
                    });

                    // props.form.setFormStatus('head', 'browse');
                    // props.billBodys.forEach((body)=>{
                    //     props.editTable.setStatus(body, 'browse');
                    // });

                }
            },bodys,null)   
        },

        // case areaKey+'-add' :
        //     props.editTable.addRow(areaKey);
        //     let areaInfo = this.props.areaInfo;
        //     areaInfo[areaKey].rowNumber = this.props.editTable.getNumberOfRows(areaKey);
        //     this.props.changeAreaInfo(areaInfo);
        //     break;
        // case areaKey+'-save' :
        //     props.editTable.setStatus(areaKey, 'browse');
        //     break;
        // case areaKey+'-edit' :
        //     props.editTable.setStatus(areaKey, 'edit');
        //
        //     break;
        // case pageButton.pageEdit:
        //     location.hash = `#status=${statusVar.edit}&openbillid=${props.getUrlParam('openbillid')}`;
        //     props.button.setButtonsVisible({
        //         [pageButton.pageSave] : true,
        //         [pageButton.pageEdit] : false
        //     });
        //     break;
        [pageButton.pageCopy]:()=>{//单据复制
            if(props.getUrlParam('isinit') === true){
                //复制出来的单据要先清空附件产生的单据pk
                this.accessorybillid = '';
                this.setState({ billId: ''});
                props.pushTo('/edit',{
                    status: 'add',
                    tradetype : window.presetVar.tradetype,
                    id: '',//props.getUrlParam('id'),
                    isinit : true,
                    copyFromBillId : props.getUrlParam('id')
                }); 
            } else{
                // pubUtils.setUrlParams(props, '/erm/expenseaccount/loanmanage/card/index.html', {
                //     scene:props.getUrlParam('scene'),
                //     status:presetVar.status.add,
                //     copyFromBillId: props.getUrlParam('id'),
                //     tradetype:window.presetVar.tradetype
                // });
				let rows = props.cardTable.getAllRows('jk_busitem');
                let ispull = "N";
                for(let i=0;i<rows.length;i++){
                    let row = rows[i];
                    if(row.values['pk_mtapp_detail']!=null
                        &&row.values['pk_mtapp_detail']!=undefined
                        &&row.values['pk_mtapp_detail'].value!=null
                        &&row.values['pk_mtapp_detail'].value!=''){
                        ispull = 'Y';
                        break;
                    }
                }
                if(ispull=='Y'){
                    toast({content:multiLang && multiLang.get("201102BCLF-0022"),color:'warning'});
                    return;
                }else{
					props.linkTo('/erm/expenseaccount/loanmanage/card/index.html',
					{
						scene:props.getUrlParam('scene'),
						status:presetVar.status.add,
						copyFromBillId: props.getUrlParam('id'),
						tradetype:window.presetVar.tradetype
					});
					window.assingUsers=[];
					ntbCheck = 'false';
                    jkCheck = 'false';
                     //复制出来的单据要先清空附件产生的单据pk
                    this.accessorybillid = '';
                    this.setState({ billId: ''});
                    let multiLang = this.props.MutiInit.getIntl(2011);
                    this.props.button.setButtonTitle('fileManager', multiLang&&multiLang.get('201102JCLF_C-0013')/**'附件'**/ + ' ' + 0 + ' ');
       
					// location.hash = `#scene=${props.getUrlParam('scene')}&status=${presetVar.status.add}&copyFromBillId=${props.getUrlParam('id')}&tradetype=${window.presetVar.tradetype}`;
				}
            }
         },

        [pageButton.pageDel]:()=>{//删除单据
            // props.modal.show(presetVar.pageButton.pageDel);
            let param = {};
            param.openbillid=this.props.getUrlParam("id");
            let props = this.props;
            requestApi.billRemove({
                data: param,
                success: (data) => {
                    if (data.success) {
                        if(props.getUrlParam('isinit') === true){
                            let pk = props.getUrlParam("id");
                            deleteCacheById("pk_jkbx", pk, this.dataSource);
                            props.cardPagination.setCardPaginationId({ id: pk, status: this.cardPagStatus.del });
                            //删除缓存后查询下一条
                            let nextId = getNextId(pk, this.dataSource);
                            if(nextId){
                                viewInitBill.call(this, nextId);
                            } else{//不存在缓存则直接返回列表
                                props.pushTo('/list',{
                                    appcode:'201103QCDJ',
                                    pagecode:'201103QCDJ_list'
                                });
                            }
                        }else{
                            window.onbeforeunload=null;
                            window.top.close();
                        }
                    }
                }
            });
        },


        [pageButton.billInvalid]:()=>{//单据作废
            // props.modal.show(presetVar.pageButton.billInvalid);
            let param = {};
            param.pagecode=this.props.getSearchParam("p");
            param.openbillid=this.props.getUrlParam("id");
            param.tradetype=window.presetVar.tradetype;
            requestApi.billInvalid({
                data: param,
                success: (data) => {
                     if (data.success) {
                        toast({content:multiLang && multiLang.get("201102BCLF-0054"),color:'warning'});
                        //处理按钮相关状态
                        dataHandle.call(this,data.data.head.head,props);
                        return ;
                    }
                }
            });
        },


        [pageButton.pagePrint]:()=>{//打印单据
            let vdata = {};
            vdata.billId = props.getUrlParam('id');
            vdata.billType = window.presetVar.tradetype;
            ajax({
                url: '/nccloud/erm/expenseaccount/ErmPrintValidaAction.do',
                data: vdata,
                success: (data) => {
                    if (!data.data.canPrint){
                        NCMessage.create({content:data.data.errMesg, color: 'error', position: 'bottom'});
                    }else {
                        print(
                            'pdf',  //支持两类: 'html'为模板打印, 'pdf'为pdf打印
                            '/nccloud/erm/expenseaccount/ErmJkbxPrintAction.do', //后台服务url
                            {
                                funcode:(props.getUrlParam("appcode")==undefined||props.getUrlParam("appcode")=='')?props.getSearchParam("c"):props.getUrlParam("appcode"),      //功能节点编码，即模板编码
                                nodekey:'2631_IWEB',     //模板节点标识
                                oids:[props.getUrlParam('id')]    // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
                            }
                        )
                    }
                }
            })
        },

        [pageButton.imageUpload]:()=>{//影像扫描
            let billdata = props.createExtCardData(pageTo.getSearchParam("p"), "head", bodyarr);
            let openbillid = props.getUrlParam('id');

            var billInfoMap = {};

            //基础字段 单据pk,单据类型，交易类型，单据的组织
            billInfoMap.pk_billid = openbillid;
            billInfoMap.pk_billtype = billdata.head.head.rows[0].values.pk_billtype.value;
            billInfoMap.pk_tradetype = billdata.head.head.rows[0].values.djlxbm.value;
            billInfoMap.pk_org = billdata.head.head.rows[0].values.pk_org.value;

            //影像所需 FieldMap
            billInfoMap.BillType = billdata.head.head.rows[0].values.djlxbm.value;
            billInfoMap.BillDate = billdata.head.head.rows[0].values.creationtime.value;
            billInfoMap.Busi_Serial_No = billdata.head.head.rows[0].values.pk_jkbx.value;
            billInfoMap.pk_billtype = billdata.head.head.rows[0].values.pk_billtype.value;
            billInfoMap.OrgNo = billdata.head.head.rows[0].values.pk_org.value;
            billInfoMap.BillCode = billdata.head.head.rows[0].values.djbh.value;
            billInfoMap.OrgName = billdata.head.head.rows[0].values.pk_org.display;
            billInfoMap.Cash = billdata.head.head.rows[0].values.ybje.value;

            console.log("billInfoMap"+JSON.stringify(billInfoMap));
            imageScan(billInfoMap, 'iweb');
        },


        [pageButton.imageShow]:()=>{//影像查看
            let billdata = props.createExtCardData(pageTo.getSearchParam("p"), "head", bodyarr);
            let openbillid = props.getUrlParam('id');
            var billInfoMap = {};
            //基础字段 单据pk,单据类型，交易类型，单据的组织
            billInfoMap.pk_billid = openbillid;
            billInfoMap.pk_billtype = billdata.head.head.rows[0].values.pk_billtype.value;
            billInfoMap.pk_tradetype = billdata.head.head.rows[0].values.djlxbm.value;
            billInfoMap.pk_org = billdata.head.head.rows[0].values.pk_org.value;
            imageView(billInfoMap,  'iweb');
        },

	    [pageButton.linkQueryFysq]: () => {
            let billId = this.state.billId || props.getUrlParam('id');
            ajax({
                url: '/nccloud/erm/loanmanage/LinkFysqAction.do',
                data: {
                    tradetype: window.presetVar.tradetype,//交易类型
                    openBillId: billId//单据主键ok
                },
                success: result => {
                    if(result.data) {
                        if(result.data.tradetype && result.data.pk ){
                            linkQueryApi.link({
                                data : {
                                    openbillid : result.data.pk,
                                    tradetype : result.data.tradetype,
                                    props : this.props
                                }
                            });
                        }
                    }
                    else{
                        let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId
						toast({ content: multiLang && multiLang.get('201102JCLF_C-0001'), color: 'danger' });//'没有关联的申请单'
                    }
                }
            });
        },

        [pageButton.linkQueryJkbx]: () => {
            let billId = this.state.billId || props.getUrlParam('id');
            requestApi.linkJkbx({
                data : {
                    queryType: 'jkbx',
                    tradetype: window.presetVar.tradetype,//交易类型
                    openBillId: billId//单据主键ok
                },
                success : (data) => {
                    if(data.data){
                        this.setState({
                            linkData: data,
                            showLinkQueryJkbxModel: true,
                            billId: billId
                        })
                    }else{
                        let multiLang = this.props.MutiInit.getIntl(2011); //"您联查的报销单没有数据
                        toast({ content: multiLang && multiLang.get('201103QCDJ-0008'), color: 'warning' });
                    }
                }
            })
        },
        [pageButton.fileManager]: () => {
            let props = this.props;
            let billId = props.getUrlParam('id') || this.state.billId || "";
            let billtype = window.presetVar.tradetype || "";
            if (billId == "" && !this.state.showUploader){
                //去后台生成单据主键
                requestApi.generatBillId({
                    data: {billtype: billtype},
                    success:(data)=>{
                        console.log(data);
                        let accessorypkfildname = data["pkfieldName"];
                        billId = data[accessorypkfildname];
                        this.setState({
                            billId: billId ,
                            showUploader: !this.state.showUploader
                        });
                        this.accessorybillid = billId;
                    },
                    error:(data)=>{
                        // alert(data.message);
                        toast({ content: data.message, color: 'danger' });
                    }
                });
            } else {
                this.setState({
                    billId: billId ,
                    showUploader: !this.state.showUploader
                });
            }
        },
        [pageButton.pageEdit]: ()=>{
            let props = this.props;
            jkCheck = 'false';
            let multiLang = props.MutiInit.getIntl(2011); //this.moduleId
            let scene = props.getUrlParam('scene');
            //获取单据编号
            let djbh = props.form.getFormItemsValue("head",['djbh'])[0].value;
            let canEdit = true;
            let isMaker = true;
            //来源于审批中心
            if(scene == 'approve'  || scene == 'approvesce' ){
                isMaker = false;
                //判断单据是否是当前用户待审批
                ajax({
                    url: '/nccloud/riart/message/list.do',
                    async: false,
                    data: {
                        billno: djbh,
                        isread:'N'
                    },
                    success: result => {
                        if(result.data) {
                            if(result.data.total < 1){
                                //当前单据已审批，不可进行修改操作!
                                toast({content: multiLang && multiLang.get('201102JCLF_C-0009'),color:'warning'});
                                canEdit =false;
                            }
                        }
                    }
                });
            }
            //来源于我的作业
            if(scene == 'zycl'){
                isMaker = false;
                //判断单据是否是当前用户待处理
                ajax({
                    url: '/nccloud/ssctp/sscbd/SSCTaskHandlePendingNumAction.do',
                    async: false,
                    data: {
                        billno: djbh
                    },
                    success: result => {
                        if(result.data) {
                            if(result.data.total < 1){
                                //当前单据已处理，不可进行修改操作!
                                toast({content: multiLang && multiLang.get('201102JCLF_C-0010'),color:'warning'});
                                canEdit =false;
                            }
                        }
                    }
                });
            }
            if(!canEdit){
                return;
            }

            if(isMaker){
                //驳回不重走流程单据，设置不可编辑字段
                requestApi.getUnEditAttributes({
                    data: {
                        'pk_org': props.form.getFormItemsValue("head",['pk_org'])[0].value,
                        'billid': props.form.getFormItemsValue("head",['pk_jkbx'])[0].value,
                        'billType':props.form.getFormItemsValue("head",['djlxbm'])[0].value
                    },
                    success: (data) => {
                        if(!(data==undefined || data.length == 0)){
                            let formobj = {};
                            let jk_busitemobj = [];
                            let er_bxcontrastobj = [];

                            data.forEach((item) => {
                                if(item.indexOf('jk_busitem.')>-1){
                                    jk_busitemobj.push(item.split('.')[1]);
                                }else if(item.indexOf('jk_contrast.')>-1){
                                    er_bxcontrastobj.push(item.split('.')[1]);
                                }else{
                                    formobj[item] = true;
                                }
                            })
                            props.form.setFormItemsDisabled('head',formobj)
                            props.cardTable.setColEditableByKey('jk_busitem',jk_busitemobj)
                            props.cardTable.setColEditableByKey('er_bxcontrast',er_bxcontrastobj)
                        }
                    }
                })
            }


            let billdata =  props.createExtCardData(pageTo.getSearchParam("p"), "head", bodyarr);
            if(billdata && billdata.head.head.rows[0].values.qcbz.value){
                props.pushTo('/edit',{
                    status: 'edit',
                    tradetype : window.presetVar.tradetype,
                    id: props.getUrlParam('id'),
                    isinit : true
                });
            } else{
                // pubUtils.setUrlParams(props, '/erm/expenseaccount/loanmanage/card/index.html', {
                //     scene:props.getUrlParam('scene'),
                //     status:statusVar.edit,
                //     id: props.getUrlParam('id'),
                //     tradetype:window.presetVar.tradetype
                // });
                props.form.setAllFormValue({["head"]: requestApi.filterDatas(billdata.head["head"])});
                bodyarr.forEach((item) =>{
                    setTimeout(()=>{
                        props.cardTable.setTableData(item, requestApi.filterDatas(billdata.bodys[item]));
                    },0);
                })
                window.presetVar.redata='N';
                props.linkTo('/erm/expenseaccount/loanmanage/card/index.html',
                {
                    scene:props.getUrlParam('scene'),
                    status:statusVar.edit,
                    id: props.getUrlParam('id'),
                    tradetype:window.presetVar.tradetype
                });                
                // location.hash = `#scene=${props.getUrlParam('scene')}&status=${statusVar.edit}&id=${props.getUrlParam('id')}&tradetype=${window.presetVar.tradetype}`;
            }
        },['pageAdd']: ()=>{
            let props = this.props;
            if(props.getUrlParam('isinit') === true){
                props.pushTo('/edit',{
                    status: 'add',
                    tradetype : window.presetVar.tradetype,
                    isinit : true
                });
            } else{
                // pubUtils.setUrlParams(props, '/erm/expenseaccount/loanmanage/card/index.html', {
                //     scene:props.getUrlParam('scene'),
                //     status:statusVar.edit,
                //     id: props.getUrlParam('id'),
                //     tradetype:window.presetVar.tradetype
                // });
                props.linkTo('/erm/expenseaccount/loanmanage/card/index.html',
                {
                    scene:props.getUrlParam('scene'),
                    status:statusVar.edit,
                    id: props.getUrlParam('id'),
                    tradetype:window.presetVar.tradetype
                });                 
                // location.hash = `#scene=${props.getUrlParam('scene')}&status=${statusVar.edit}&id=${props.getUrlParam('id')}&tradetype=${window.presetVar.tradetype}`;
            }
        },[pageButton.billRecall]: () => {//收回
            let param = {};
            param.openbillid=this.props.getUrlParam('id');
            param.pagecode=this.props.getSearchParam("p");
            requestApi.billRecall({
                data: param,
                success: (data) => {
                    let billid = data.head.head.rows[0].values.pk_jkbx.value;
                    // pubUtils.setUrlParams(props, '/erm/expenseaccount/loanmanage/card/index.html', {
                    //     status : statusVar.browse,
                    //     id : billid,
                    //     tradetype : window.presetVar.tradetype,
                    //     flow : 'recall'
                    // }); 
                    // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                    // props.form.setAllFormValue({["head"]: requestApi.filterDatas(data.head["head"])});
                    // bodyarr.forEach((item) =>{
                    //     setTimeout(()=>{
                    //         props.cardTable.setTableData(item, requestApi.filterDatas(data.bodys[item]));
                    //     },0);
                    // })
                    updataFormData(props, 'head', requestApi.filterDatas(data.head["head"]));
                    bodyarr.forEach((item) =>{
                        setTimeout(()=>{
                            updatacardTableData(props, item, requestApi.filterDatas(data.bodys[item]));
                        },0);
                    })
                    // BTM 性能优化，变更页面数据更新方法
                    window.presetVar.redata='N';
                    props.linkTo('/erm/expenseaccount/loanmanage/card/index.html',
                    {
                        status : statusVar.browse,
                        id : billid,
                        tradetype : window.presetVar.tradetype,
                        flow : 'recall'
                    });                     
                    /* location.hash = `#status=${statusVar.browse}&id=${billid}&tradetype=${window.presetVar.tradetype}&flow=recall`;
                    props.button.setButtonsVisible({
                        [pageButton.pageSave] : false,
                        [pageButton.pageEdit] : true
                    }); */
                }
            });
        },
        [pageButton.linkVoucher]:() => {
            let multiLang = props.MutiInit.getIntl(2011); //this.moduleId
            let billdata =  props.createExtCardData(pageTo.getSearchParam("p"), "head", bodyarr);
            let paramurl = getRequest1(parent.window.location.hash);
            //如果没取到应用主键再取一下外层,主要适配作业任务打开单据时联查凭证
            if(!paramurl.ar){
                paramurl = getRequest1(parent.parent.window.location.hash);
            }
            if(paramurl.ar){
                if (billdata.head.head.rows.length){
                    linkVoucherApi.link({
                        data  : {
                            props : props,
                            record : {
                                pk_billtype : billdata.head.head.rows[0].values.djlxbm.value,
                                pk_group : billdata.head.head.rows[0].values.pk_group.value,
                                pk_org : billdata.head.head.rows[0].values.pk_org.value,
                                relationID : billdata.head.head.rows[0].values.pk_jkbx.value
                            },
                            appid : paramurl.ar
                        }
                    })
                }
            }else{
                //联查凭证失败，缺少应用主键
                toast({ content: multiLang && multiLang.get('201102JCLF_C-0011'), color: 'danger' });
            }
        },
        [pageButton.linkBudget]:() => {
            let billdata =  props.createExtCardData(pageTo.getSearchParam("p"), "head", bodyarr);
            if (billdata.head.head.rows.length){
                linkBudgetApi.link({
                    data : {
                        "tradetype": billdata.head.head.rows[0].values.djlxbm.value,
                        "openbillid" : billdata.head.head.rows[0].values.pk_jkbx.value
                    },
                    success: (data) => {
                        this.setState({
                            sourceData: data,
                            showLinkBudget: true
                        })
                    }
                })
            }
        },
        ['pageCancel']:() => {
            if(props.getUrlParam("status") == statusVar.add){
                //如果是期初借款点取消需要取当前列表最后一条记录渲染
                let copyid = props.getUrlParam('copyFromBillId');
                if(props.getUrlParam('isinit') === true || copyid){
                    let lastId = getCurrentLastId(this.dataSource) || copyid;
                    let appcode = (props.getUrlParam("appcode")==undefined||props.getUrlParam("appcode")=='')?props.getSearchParam("c"):props.getUrlParam("appcode");
                    let pagecode =  (props.getUrlParam("pagecode")==undefined||props.getUrlParam("pagecode")=='')?props.getSearchParam("p"):props.getUrlParam("pagecode");
                    if(lastId){
                        let viewbilldata = {};
                        viewbilldata.openbillid = lastId;
                        viewbilldata.tradetype = props.getUrlParam("tradetype");
                        viewbilldata.uid = getCookie("userid");
                        viewbilldata.pagecode = pagecode;
                        viewbilldata.appcode = appcode;
                        requestApi.viewBill({
                            data: viewbilldata,
                            success: (data) => {
                                props.form.setAllFormValue({['head']:data.head['head']});
                                props.pushTo('/edit',{
                                    status: 'browse',
                                    tradetype : window.presetVar.tradetype,
                                    id: lastId,
                                    isinit : true
                                });
                            }
                        });

                    }else{//不存在缓存则直接返回列表
                        props.pushTo('/list',{
                            appcode:'201103QCDJ',
                            pagecode:'201103QCDJ_list'
                        });
                    }
                }else{
                    window.onbeforeunload=null;
                    window.top.close();
                }
            }else{
                if(props.getUrlParam('isinit') === true){
                     props.pushTo('/edit',{
                        status: 'browse',
                        tradetype : window.presetVar.tradetype,
                        id: props.getUrlParam('id'),
                        isinit : true
                    }); 
                }else{
                    // pubUtils.setUrlParams(props, '/erm/expenseaccount/loanmanage/card/index.html', {
                    //     scene : props.getUrlParam('scene'),
                    //     status : statusVar.browse,
                    //     id : props.getUrlParam('id'),
                    //     tradetype : window.presetVar.tradetype,
                    // });
                    props.linkTo('/erm/expenseaccount/loanmanage/card/index.html',
                    {
                        scene : props.getUrlParam('scene'),
                        status : statusVar.browse,
                        id : props.getUrlParam('id'),
                        tradetype : window.presetVar.tradetype,
                    });
                    /* let props = this.props;
                    location.hash = `#scene=${props.getUrlParam('scene')}&status=${statusVar.browse}&id=${props.getUrlParam('id')}&tradetype=${window.presetVar.tradetype}`;                 */
                }
            }
        },
        [pageButton.billApprove]: () => {
            let props = this.props;
            let billdata =  props.createExtCardData(pageTo.getSearchParam("p"), "head", bodyarr);
            let pk_billtype = billdata.head.head.rows[0].values.djlxbm.value;
            let billId = billdata.head.head.rows[0].values.pk_jkbx.value || props.getUrlParam('id') || this.state.billId || "";
            this.setState({
                ShowApproveDetail: true,
                billId: billId,
                billtype: pk_billtype
            });
        }
    }
}

export default pageButtonClick;
