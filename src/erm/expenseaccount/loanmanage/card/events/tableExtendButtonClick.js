/**
 * 表格扩展按钮click事件
 */
import {headSumHandle} from "./commonEvent";

function tableExtendButtonClick() {
    let returnObj = {};
    for(let name in window.presetVar.body){
        let areaId = window.presetVar.body[name];
        if(areaId==='jk_busitem'){
            returnObj[areaId +'_Delete']={
                afterClick: () => {
                    headSumHandle.call(this);
                }
            };
            returnObj[areaId +'_Copy']={
                afterClick: () => {
                    headSumHandle.call(this);
                }
            };

        }
    }
    return returnObj;
}

export default tableExtendButtonClick;