import {render, getCookie, dataHandle} from '../initTemplate'
import requestApi from '../requestApi';
import 'ssccommon/components/globalPresetVar';
import { cardCache } from 'nc-lightapp-front';
let { updateCache } = cardCache;

export default function () {
    let props = this.props;
    let appcode = (props.getUrlParam("appcode")==undefined||props.getUrlParam("appcode")=='')?props.getSearchParam("c"):props.getUrlParam("appcode");
    let pagecode =  (props.getUrlParam("pagecode")==undefined||props.getUrlParam("pagecode")=='')?props.getSearchParam("p"):props.getUrlParam("pagecode");
    render.call(this, props.meta.getMeta(), props.button);
    let viewbilldata = {};
    viewbilldata.openbillid = props.getUrlParam("id");
    viewbilldata.tradetype = props.getUrlParam("tradetype")||window.presetVar.tradetype;
    viewbilldata.uid = getCookie("userid");//"1001Z3100000000000AE";这里以后从session中获取
    viewbilldata.pagecode = pagecode;
    viewbilldata.appcode = appcode;
    requestApi.viewBill({
        data:viewbilldata,
        success:(data) =>{
            props.form.setAllFormValue({['head']:data.head['head']});
            //设置表体数据
            Object.values(presetVar.body).forEach((body) =>{
                if(data.bodys[body]){
                    props.cardTable.setTableData(body, data.bodys[body]);
                }
            });
            dataHandle.call(this, data.head["head"],props);
            if(data.head['head'].rows[0].values.qcbz.value){
                updateCache("pk_jkbx",props.getUrlParam("id"),data,"head", this.dataSource);
                props.cardPagination.setCardPaginationId({ id: props.getUrlParam("id"), status: this.cardPagStatus.modify });
            }
            this.pubMessage.refreshSuccess();

        }
    });
}
