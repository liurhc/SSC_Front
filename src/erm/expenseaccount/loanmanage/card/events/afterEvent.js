import {ajax,base,pageTo,toast} from 'nc-lightapp-front';
import requestApi from '../requestApi';
import {updataFormData, updatacardTableData} from 'ssccommon/utils/changePageData';

const {NCMessage} = base;

export default function afterEvent(props,moduleId,key,value,changedrows,index,attr) {

    let paramdata = {};
    let moduleid =moduleId =="head"?"headform":moduleId;
    let pagecode = props.getSearchParam("p");
    let bodys = Object.values(window.presetVar.body);
    let bodyarr = [];
    let hideTable = this.state.hideTables;
    bodys && bodys.length && bodys.map((one)=>{
        if(hideTable && hideTable.indexOf(one)<0){
            bodyarr[bodyarr.length] = one;
        }
    })
    if(moduleid=="headform"){
        let billdata = props.createExtCardData(window.presetVar.pageId, 'head',bodyarr);
        requestApi.handleLoadDataResDada(props.meta.getMeta(), {data:billdata},props);
    }
    paramdata = {
        "value":value,
        "tableid":moduleid,
        "field":key,
        "uistate":props.getUrlParam("status"),
        "tradetype":props.getUrlParam("tradetype")||window.presetVar.tradetype,
        "oldvalue":'',
        "pagecode":window.presetVar.pagecode,
        "appcode":window.presetVar.appcode
    }
    if(moduleid!="headform"){
        paramdata.changeRowId=changedrows[0].rowid;
    }else{
        paramdata.value = value.value;
    }    
    switch(moduleId) {
        case 'head' :
            switch(key) {
                case 'pk_org'://表头财务组织
                case 'djrq'://表头单据日期
                case 'bzbm'://表头币种
                case 'bbhl'://表头本币汇率
                case 'dwbm_v'://表头借款报销人单位版本
                case 'dwbm'://表头借款报销人单位
                case 'fydwbm_v'://表头费用承担单位版本
                case 'fydwbm'://表头费用承担单位
                case 'pk_payorg_v'://表头支付单位版本
                case 'pk_payorg'://表头支付单位
                case 'deptid_v':// 表头报销人部门版本
                case 'deptid':// 表头报销人部门
                case 'fydeptid_v':// 表头费用承担部门版本
                case 'fydeptid':// 表头费用承担部门
                case "jkbxr"://借款报销人
                case "receiver"://表头收款人
                case "szxmid"://表头收支项目
                case "skyhzh"://表头个人银行账户
                case "jobid"://表头项目
                case "freecust"://散户
                case "custaccount"://表头客商银行账户
                case "paytarget"://表头收款对象
                case "pk_contractno"://合同号
                    if(value.value=="0"){
                        props.form.setFormItemsDisabled("head",{'hbbm':true,'customer':true,"custaccount":true,'receiver':false,'custaccount':true,'freecust':true});
                    }else if(value.value=="1"){
                        props.form.setFormItemsDisabled("head",{'receiver':true,'customer':true,'hbbm':false,'custaccount':false,'freecust':false});
                    }else if(value.value=="2"){
                        props.form.setFormItemsDisabled("head",{'hbbm':true,'receiver':true,'customer':false,'custaccount':false,'freecust':true});
                    }
                case "hbbm"://表头供应商
                case "customer"://表头客户
                    // console.log('head数据', props.form, props.form.getAllFormValue('head'));
                    // let headdata = props.form.getAllFormValue('head');
                    // let send_headdata = {headdata,paramdata};
                    // requestApi.valueChange({
                    //     data: send_data,
                    //     success: (data)=> {
                    //         console.log(data);
                    //         props.form.setAllFormValue({['head']:data.head['head']});
                    //         Object.keys(presetVar.body).forEach((item) =>{
                    //             props.cardTable.setTableData(item,data.data.body[item]);
                    //         })
                    //     }
                    // });
                    //   let onlyHeadData = props.createFormAfterEventData('20110ETEA_2641_IWEB', "head", moduleId, key, value);
                    //  console.log('表头字段change后仅有表头数据结构：', onlyHeadData)
                    //表头修改后数据后台需要的结构， 一主一子  一主多子
                    let billdata = props.createHeadAfterEventData(pageTo.getSearchParam("p"), "head", bodyarr, moduleId, key, value);
                    console.log('表头字段change后表头标题数据结构：', billdata)
                    //  let send_headdata = {billdata,paramdata};
                    // // TOP NCCLOUD-91597 校验报销标准取模板时需要使用模板ID chengwbc ADD
	                // send_headdata.templateid = props.meta.getMeta().pageid;
                    billdata.card.head.userjson = JSON.stringify(paramdata);
                    billdata.templetid =props.meta.getMeta().pageid;
	                // BTM NCCLOUD-91597 校验报销标准取模板时需要使用模板ID
                    requestApi.valueChange({
                        data: billdata.card,
                        success: (data)=> {
                            // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                            // //设置表头数据
                            // props.form.setAllFormValue({['head']: requestApi.filterDatas(data.head['head'])});
                            // //设置表体数据
                            // bodyarr.forEach((item) =>{
                            //     setTimeout(()=>{
                            //         props.cardTable.setTableData(item, requestApi.filterDatas(data.bodys[item]));
                            //     },0);
                            // })
                            // 设置表头数据
                            updataFormData(props, 'head', requestApi.filterDatas(data.head['head']));
                            // 设置表体数据
                            bodyarr.forEach((item) =>{
                                 setTimeout(()=>{
                                    updatacardTableData(props, item, requestApi.filterDatas(data.bodys[item]));
                                    if(item=='jk_busitem'){
                                        for(let i=0;i<data.bodys[item].rows.length;i++){
                                            props.cardTable.delChangedRowsOldValue(item,i);
                                        }
                                    }
                                 },0);
                            })
                            // BTM 性能优化，变更页面数据更新方法
                            let org_currinfo = props.form.getFormItemsValue('head','bbhl');
                            if(key=='bzbm' &&org_currinfo.value!=undefined && org_currinfo.value!=null && parseFloat(org_currinfo.value) == parseFloat(1) )
                            {
                                props.form.setFormItemsDisabled('head',{'bbhl':true});
                            }else{
                                props.form.setFormItemsDisabled('head',{'bbhl':false});
                            }
                            //表头值变化后为表体元数据赋值
                            let headrows = data.head['head'].rows[0].values;
                            let headtrr = ['pk_org','szxmid','pk_pcorg','pk_pcorg_v','pk_checkele','jobid','projecttask','pk_resacostcenter','pk_proline','pk_brand','fctno','bzbm','tax_rate','bbhl','groupbbhl','globalbbhl','receiver','freecust','jkbxr','dwbm','skyhzh','custaccount','customer','deptid','paytarget','hbbm'];
                            let cshare_detail = ['assume_org','assume_dept','pk_iobsclass'];//'fydwbm','fydeptid',
                            let metas  = props.meta.getMeta();
                            //Object.keys(presetVar.body).forEach((bodyindex) =>{
                            for (let bodyname in metas.gridrelation) {
                               // let bodyname = presetVar.body[bodyindex];
                                if(metas[bodyname].items!=undefined&&metas[bodyname].items!=null&&bodyname!='er_bxcontrast'&&bodyname!='accrued_verify'&&bodyname!='head'){
                                    metas[bodyname].items.forEach((item) => {
                                        if (headtrr.indexOf(item.attrcode) > -1 || cshare_detail.indexOf(item.attrcode) > -1) {
                                            let keycode = '';
                                            if(item.attrcode=='assume_org'){
                                                keycode = 'fydwbm';
                                            }else if(item.attrcode=='assume_dept'){
                                                keycode = 'fydwbm';
                                            }else if(item.attrcode=='pk_iobsclass'){
                                                keycode = 'szxmid';
                                            }else{
                                                keycode = item.attrcode;
                                            }
                                            if(headrows[keycode]!=undefined && headrows[keycode].value!=undefined){
                                                let valuecode = headrows[keycode].value==undefined?'':headrows[keycode].value;
                                                let displaycode = valuecode==''?'':(headrows[keycode].display==undefined||headrows[keycode].display==''?valuecode:headrows[keycode].display)
                                                let initialvalue={
                                                    value: valuecode,
                                                    display: displaycode
                                                }
                                                item.initialvalue =initialvalue;
                                            }
                                        }
                                    });
                                   // props.editTable.setStatus(bodyname, 'edit');
                                }
                            };
                        }
                    })
                    break;

                case 'zyx19': // 境外支付消息（自定义项19）-20190911
                    var meta = this.props.meta.getMeta();
                    if(!(meta['JWZF_head'] && meta['JNZF_head'])){ // 模板没有境内/外区域时不执行此方法
                        break;
                    }
                    if(value.value == true){ // 勾选 "是"
                        // 显示境外区域
                        meta['JWZF_head'].items = this.state.items['JWZF_head']?this.state.items['JWZF_head']:[];
                        this.props.form.openArea('JWZF_head'); // 展开区域
                        // 清空境内区域字段
                        this.props.form.setFormItemsValue('head',{'custaccount':{value:0,display:null},'receiver':{value:null,display:null},
                            'zyx20':{value:null,display:null},'hbbm':{value:null,display:null},'zyx23':{value:null,display:null},'zyx27':{value:null,display:null},
                            'zyx29':{value:null,display:null},'zyx28':{value:null,display:null},'zyx25':{value:null,display:null},'iscostshare':{value:false,display:null},
                            'isexpamt':{value:false,display:null}});
                        
                            meta['JNZF_head'].items = []; // 隐藏境内区域
                        this.props.meta.setMeta(meta);
                    } else { // 勾选 "否"
                        // 显示境内区域
                        meta['JNZF_head'].items = this.state.items['JNZF_head']?this.state.items['JNZF_head']:[];
                        this.props.form.openArea('JNZF_head'); // 展开区域
                        // 清空境外区域字段
                        this.props.form.setFormItemsValue('head',{'receiver':{value:null,display:null},'zyx5':{value:null,display:null},
                            'zyx6':{value:null,display:null},'zyx7':{value:null,display:null},'zyx15':{value:null,display:null},
                            'zyx9':{value:null,display:null},'zyx11':{value:null,display:null},'hbbm':{value:null,display:null},'zyx4':{value:null,display:null},
                            'iscostshare':{value:false,display:null},'isexpamt':{value:false,display:null}});
                        
                            meta['JWZF_head'].items = []; // 隐藏境外区域
                        this.props.meta.setMeta(meta);
                    }
                    break;
               
                // case "bzbm"://币种
                //     requestApi.valueChange({
                //         data: send_data,
                //         success: (data)=> {
                //             console.log(data);handleLoadDataResDada
                //             props.form.setAllFormValue({['head']:data.head['head']});
                //             Object.keys(data.bodys).forEach((item) =>{
                //                 props.cardTable.setTableData(item, data.bodys[item]);
                //             });
                //         }
                //     });
                //     break;
                default:
                    break;
            }
            break;
        default:
            //表体字段处理
            //满足报销标准核心控制项字段影响因素
            if(key!=window.presetVar.showitemkey && (window.presetVar.reimsrc!=null && window.presetVar.reimsrc.length>0 && window.presetVar.reimsrc.includes(key))){
                //清空当前行报销标准的值，以重新获取
                if(changedrows[0].newvalue.value==changedrows[0].oldvalue.value){
                    break;
                }
                props.cardTable.setValByKeyAndRowId(presetVar.body.body1,changedrows[0].rowid,window.presetVar.showitemkey,{value:0.00, display: 0.00});

                let billdata = props.createHeadAfterEventData(pageTo.getSearchParam("p"), "head", bodyarr, moduleId, key, value);
                paramdata.checkrule='true'
                if(value.value==undefined||value.value==null){
                    paramdata.value = changedrows[0].newvalue.value;
                }
                billdata.card.head.userjson = JSON.stringify(paramdata);
                billdata.templetid =props.meta.getMeta().pageid;
                // let send_headdata = {billdata,paramdata};
                // // TOP NCCLOUD-91597 校验报销标准取模板时需要使用模板ID chengwbc ADD
	            // send_headdata.templateid = props.meta.getMeta().pageid;
	            // BTM NCCLOUD-91597 校验报销标准取模板时需要使用模板ID
                requestApi.valueChange({
                    data: billdata.card,
                    success: (data)=> {
                        // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                        // //设置表头数据
                        // props.form.setAllFormValue({['head']: requestApi.filterDatas(data.head['head'])});
                        // //设置表体数据
                        // props.cardTable.setTableData(moduleid, requestApi.filterDatas(data.bodys[moduleid]));
                        // 设置表头数据
                        updataFormData(props, 'head', requestApi.filterDatas(data.head['head']));
                        // 设置表体数据
                        updatacardTableData(props, moduleid, requestApi.filterDatas(data.bodys[moduleid]));
                        // BTM 性能优化，变更页面数据更新方法
                        let billdatas = props.createExtCardData(window.presetVar.pageId, 'head',bodyarr);
                        requestApi.handleLoadDataResDada(props.meta.getMeta(), {data:billdatas},props);
                        props.cardTable.setTableDataWithResetInitValue(moduleid, requestApi.filterDatas(data.bodys[moduleid]));
                    }
                })
                return;//满足报销标准核心控制项字段影响因素的字段直接返回
            }
            switch (key){
                case 'pk_org'://表头财务组织
                case 'djrq'://表头单据日期
                case 'bzbm'://表头币种
                case 'bbhl'://表头本币汇率
                case 'dwbm_v'://表头借款报销人单位版本
                case 'dwbm'://表头借款报销人单位
                case 'fydwbm_v'://表头费用承担单位版本
                case 'fydwbm'://表头费用承担单位
                case 'pk_payorg_v'://表头支付单位版本
                case 'pk_payorg'://表头支付单位
                case 'deptid_v':// 表头报销人部门版本
                case 'deptid':// 表头报销人部门
                case 'fydeptid_v':// 表头费用承担部门版本
                case 'fydeptid':// 表头费用承担部门
                case "jkbxr"://借款报销人
                case "receiver"://表头收款人
                case "szxmid"://表头收支项目
                case "skyhzh"://表头个人银行账户
                case "jobid"://表头项目
                case "freecust"://散户
                case "custaccount"://表头客商银行账户
                case "paytarget"://表头收款对象
                case "hbbm"://表头供应商
                case "customer"://表头客户
                case  "vat_amount"://表体含税金额
                case 'tni_amount'://表体不含税金额
                case 'amount'://表体金额
                case 'fctno':
                  
                    //let billdata = props.createHeadAfterEventData('20110ETEA_2641_IWEB', "head",[moduleid], moduleId, key, value);
                   // let data = props.createGridAfterEventData('20521030', moduleid, moduleId, key, changedrows);
                   // let billdata = props.createBodyAfterEventData('20110ETEA_2641_IWEB', 'head', moduleid, moduleId, key, changedrows);
                    // if(changedrows[0].newvalue.value==changedrows[0].oldvalue.value){
                    //     break;
                    // }
                    let billdata = props.createHeadAfterEventData(pageTo.getSearchParam("p"), "head", bodyarr, moduleId, key, value);

                    if(key=='fctno'){
                        if(paramdata.tradetype=='263X-Cxx-005'||paramdata.tradetype=='263X-Cxx-006'){
                            let rs=billdata.card.bodys.jk_busitem.rows;
                            for(let i=0;i<rs.length;i++) {
                                if(rs[i].values.fctno.value&&rs[i].values.fctno.value.length>0&&changedrows[0].newvalue.value&&changedrows[0].newvalue.value.length>0&&rs[i].values.fctno.value!=changedrows[0].newvalue.value){
                                    toast({ content: '合同不一致', color: 'danger' });
                                    props.cardTable.setValByKeyAndIndex(moduleid, index, key, { value:"", display:"" });
                                    return;
                                }
                            }
                        }
                    }


                    console.log('表头字段change后表头标题数据结构：', billdata);
                    if(value.value==undefined||value.value==null){
                        paramdata.value = changedrows[0].newvalue.value;
                    }

                    for(var i=0,flag=true,len=billdata.card.bodys.jk_busitem.rows.length;i<len;flag ? i++ : i){
                        if(billdata.card.bodys.jk_busitem.rows[i] != undefined && billdata.card.bodys.jk_busitem.rows[i].status == 3){
                            billdata.card.bodys.jk_busitem.rows.splice(i,1);
                            flag = false;
                        }else{
                            flag = true;
                        }
                    }
                    billdata.card.head.userjson = JSON.stringify(paramdata);
                    billdata.templetid =props.meta.getMeta().pageid;
                    console.log('表头字段change后表头标题数据结构：', billdata);
                    // let send_headdata = {billdata,paramdata};
                    // // TOP NCCLOUD-91597 校验报销标准取模板时需要使用模板ID chengwbc ADD
	                // send_headdata.templateid = props.meta.getMeta().pageid;
	                // BTM NCCLOUD-91597 校验报销标准取模板时需要使用模板ID
                    requestApi.valueChange({
                        data: billdata.card,
                        success: (data)=> {
                            // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                            // //设置表头数据
                            // props.form.setAllFormValue({['head']: requestApi.filterDatas(data.head['head'])});
                            // //设置表体数据
                            // props.cardTable.setTableData(moduleid, requestApi.filterDatas(data.bodys[moduleid]));
                            // 设置表头数据
                            updataFormData(props, 'head', requestApi.filterDatas(data.head['head']));
                            // 设置表体数据
                            updatacardTableData(props, moduleid, requestApi.filterDatas(data.bodys[moduleid]));
                            // BTM 性能优化，变更页面数据更新方法
                            let billdatas = props.createExtCardData(window.presetVar.pageId, 'head',bodyarr);
                            //requestApi.handleLoadDataResDada(props.meta.getMeta(), {data:billdatas},props);
                        }
                    })
                // let onlyHeadData = props.createFormAfterEventData('20521030', 'head', moduleId, key, value);
                // console.log('表头字段change后仅有表头数据结构：', onlyHeadData)
                //表头修改后数据后台需要的结构， 一主一子  一主多子
                //     let headdata = props.createHeadAfterEventData('20110ETEA_2641_IWEB', 'head', moduleId, moduleId, key, value);
                //     console.log('表头字段change后表头标题数据结构：', headBodyData)
                //     let send_headdata = {headdata,paramdata};
                //
                //     requestApi.changeByHead({
                //     data: send_headdata,
                //     success: (data)=> {
                //         //设置表头数据
                //         props.form.setAllFormValue({['head']: data.data.head['head']});
                //         //设置表体数据
                //         props.cardTable.setTableData(presetVar.body.body1, data.data.body[presetVar.body.body1]);
                //
                //     }
                // })
            }
            break;
    }
};