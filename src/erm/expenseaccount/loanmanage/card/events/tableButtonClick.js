/**
 * 表格肩部按钮click事件
 */
import {ajax} from 'nc-lightapp-front';

const areaBtnAction = window.presetVar.areaBtnAction;
const statusVar = window.presetVar.status;
const bodys = window.presetVar.body;

function tableButtonClick() {
    return {
        ['jk_busitem-add'] : () => {
            let data = this.props.createExtCardData('20110ETLB_2631_IWEB', 'head', 'jk_busitem');
            this.props.cardTable.addRow('jk_busitem', undefined, null, false);
        }
    }
}

export default tableButtonClick;