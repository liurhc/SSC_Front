import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {createPage} from 'nc-lightapp-front';
import {initTemplate, afterEvent, searchClick, onRowDoubleClick, buttonClick, refreshButtonClick, pageInfoClick} from './events';
import './index.less';
import {
    ProfileStyle,
    ProfileHead,
    ProfileBody,
    EmptyArea,
    ButtonGroup
} from 'ssccommon/components/profile';
import pubMessage from 'ssccommon/utils/pubMessage'

window.areaCode = {
    searchArea :'20110BQLB',
    billsArea:'head'
};

class Voucher extends Component {
    constructor(props) {
        super(props);
        this.pubMessage = new pubMessage(props);
        initTemplate.call(this, props);
        this.state = {
            showModal: false,
            status : 'browse'
        };
    }

    componentDidMount() {

    }

    close() {
        this.setState({ showModal: false });
    }

    onSelectedFn(){
        let rows1 = this.table.getCheckedRows(window.areaCode.billsArea);
        if(rows1.length){
            this.button.setButtonDisabled('Create', false);
        }else{
            this.button.setButtonDisabled('Create', true);
        }
    }

    render() {
        let {table, search} = this.props;
        let {NCCreateSearch} = search;
        let {createSimpleTable} = table;
        let rowDoubleClick =  onRowDoubleClick.bind(this);
        let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId

        return (
            // "201103PZYM-0001": "月末凭证",
            <ProfileStyle
                layout="singleTable"
                {...this.props}
                id='voucher'
            >

                <ProfileHead
                    // "201103PZYM-0001": "月末凭证"
                    title={multiLang && multiLang.get('201103PZYM-0001')}
                    refreshButtonEvent={refreshButtonClick.bind(this)} 
                    status = { this.state.status }
                >
                    <ButtonGroup
                        area="page_header"
                        buttonLimit="3"
                        buttonEvent={buttonClick.bind(this)}
                    />
                </ProfileHead>
                <ProfileBody>
                    <EmptyArea className="ssc-profile-search-area">
                        { NCCreateSearch(
                            window.areaCode.searchArea,//模块id
                            {
                                clickSearchBtn: searchClick.bind(this),//   点击按钮事件
                                clickPlanEve : searchClick.bind(this),
                                defaultConditionsNum:1, //默认显示几个查询条件
                                // saveSearchPlan:true  //  是否需要查询方案功能(暂不可用)
                                onAfterEvent: afterEvent()//编辑后事件
                            }
                        )}
                    </EmptyArea>
                    <EmptyArea>
                        {createSimpleTable(window.areaCode.billsArea, {
                            onAfterEvent: afterEvent.bind(this),
                            showCheck:true,
                            showIndex:true,
                            onRowDoubleClick : rowDoubleClick,
                            onSelected :this.onSelectedFn,
                            onSelectedAll : this.onSelectedFn,
                            handlePageInfoChange: pageInfoClick,
                        })}
                    </EmptyArea>
                </ProfileBody>
            </ProfileStyle>
        )
    }
}

Voucher = createPage({
    mutiLangCode: '2011'
})(Voucher);

ReactDOM.render(<Voucher/>, document.querySelector('#app'));