/**
 * 表格扩展按钮click事件
 */
import linkVoucherApi from '../../../../public/components/linkvoucher/linkvoucher'

function tableExtendButtonClick() {
    let props = this.props;
    return {
        ['Linkpz'] : (record, index) => {
            linkVoucherApi.link({
                data  : {
                    props : props,
                    record : {
                        pk_billtype : record.djlxbm.value,
                        pk_group : record.pk_group.value,
                        pk_org : record.pk_org.value,
                        relationID : record.pk_jkbx.value
                    },
                    appid : props.getSearchParam('ar')//'0001Z310000000006H22'
                }
            })
        },
    }
}

export default tableExtendButtonClick;