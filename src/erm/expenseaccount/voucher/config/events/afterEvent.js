export default function (props, id, key, value, data, index) {
    if (key === 'isdefault') {
        props.table.setColValueByData(id, key, { value: false }, index)
    }
}