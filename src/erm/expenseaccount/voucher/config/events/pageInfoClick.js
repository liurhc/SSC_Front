import { ajax } from 'nc-lightapp-front';
import requestApi from '../requestApi'
export default function (props, config, pks) {
    requestApi.query({
        data:{
            pks : pks
        },
        success: (data) => {
            if(data && data.data){
                props.table.setAllTableData(window.areaCode.billsArea, data.data[window.areaCode.billsArea]);
            }else{
                props.table.setAllTableData(window.areaCode.billsArea, {
                    areacode:"head",
                    rows:[]
                });
            }

        }
    })
   
}
