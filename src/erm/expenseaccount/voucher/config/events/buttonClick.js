import requestApi from '../requestApi'
import {toast } from 'nc-lightapp-front';
export default function buttonClick() {
    let props = this.props;
    let multiLang = props.MutiInit.getIntl(2011); //this.moduleId
    return{
        ['Create']: () => {
            let rows1 = props.table.getCheckedRows(window.areaCode.billsArea);
            if(rows1.length){
                let pkArr = rows1.map(item => item.data.values.pk_jkbx.value);
                requestApi.deal({
                    data : {
                        bxjkPks :pkArr
                    },
                    success : (data) => {
                        toast({ content: multiLang && multiLang.get('201103PZYM-0003'), color: 'success' });//生成凭证操作成功
                    }
                })
            }else{
                toast({ content: multiLang && multiLang.get('201103PZYM-0002'), color: 'warning' });//请选择需要生成凭证的记录
            }
        }
    }
}


