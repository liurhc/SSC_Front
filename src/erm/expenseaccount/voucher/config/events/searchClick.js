import requestApi from '../requestApi'
import { cacheTools , toast} from 'nc-lightapp-front';

export default function searchClick(props, searchVal, action) {
    if(searchVal){
        let searchResutlTotalCount = 0;
        cacheTools.set('searchVal', searchVal);
        for(let i in searchVal.conditions){
            if(searchVal.conditions[i].field === 'djlxbm'){
                searchVal.conditions[i].value.firstvalue = searchVal.conditions[i].display;
            }
        }
        let data={
            querycondition : {
                logic : "and",
                conditions : [searchVal]
            },
            pagecode: window.presetVar.page,
            pageInfo:{
                "pageIndex":-1,
                "pageSize":10,
                "totalPage":"0"
            },
            queryAreaCode:window.areaCode.searchArea,  //查询区编码
            oid:'0001Z310000000006FPE',  //查询模板id，手工添加在界面模板json中，放在查询区，后期会修改
            querytype:'tree'
        };
        requestApi.query({
            data:data,
            success: (data) => {
                if(data && data.pks){
                    let allpks = data.pks;
                    data.data[window.areaCode.billsArea].pageInfo = {
                        pageIndex: "0",
                        pageSize: window.presetVar.pageSize,
                        total: allpks.length,
                        totalPage: Math.ceil(allpks.length/window.presetVar.pageSize)
                    }
                    data.data[window.areaCode.billsArea].allpks = allpks;
                    data && data.data && props.table.setAllTableData(window.areaCode.billsArea, data.data[window.areaCode.billsArea]);
                    searchResutlTotalCount = allpks.length;
                }else{
                    props.table.setAllTableData(window.areaCode.billsArea, {
                        areacode:"head",
                        rows:[],
                        pageInfo : {
                            pageIndex: "0",
                            pageSize: window.presetVar.pageSize,
                            total: 0,
                            totalPage: 0
                        }
                    });
                }

                if (action == 'refresh') {
                    this.pubMessage.refreshSuccess();
                } else {
                    this.pubMessage.querySuccess(data ? searchResutlTotalCount : 0)
                }
                
            }
        })
    } else {
        if (action == 'refresh') {
            this.pubMessage.refreshSuccess();
        }
    }
}

