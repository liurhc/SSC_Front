import {searchClick} from '../events'
import { cacheTools } from 'nc-lightapp-front';

export default function refreshButtonClick() {
    let obj = cacheTools.get('searchVal');
    searchClick.bind(this)(this.props, obj, 'refresh');
}
