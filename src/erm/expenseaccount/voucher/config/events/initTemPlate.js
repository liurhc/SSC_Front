import {ajax, cacheTools, getMultiLang} from 'nc-lightapp-front';
import {setTableExtendCol} from 'ssccommon/components/profile'
import linkQueryApi from '../../../../public/components/linkquery/linkquerybills'
import linkVoucherApi from '../../../../public/components/linkvoucher/linkvoucher'
import {tableExtendButtonClick} from '../events';

window.presetVar = {
    ...window.presetVar,
    //状态常量。 用于url中的hash参数 或 框架api的状态
    status: {
        browse: 'browse', //浏览态
        edit: 'edit', //编辑态
        add: 'add', //新增态
        del: 'del'
    },
    page : '20110BQLB_MNGLIST',
    pageSize : '10'
}

export default function (props) {
    const appcode = props.getSearchParam('c');
    const pagecode = props.getSearchParam('p');
    let that = this;
    props.createUIDom(
        {
            pagecode: pagecode,//页面id
            appcode: appcode
        },
        function (data) {
            getMultiLang({moduleId: 2011, domainName: 'erm',currentLocale: 'simpchn', callback: (json) => {
                let tempArr = [];
                data.template[window.areaCode.searchArea].items.forEach((item) => {
                    if(item.attrcode === 'deptid_v' || item.attrcode === 'deptid' ){
                        item.isShowUnit = true;
                        item.unitProps = {
                            refType: 'tree',
                            refName: json['201101DLSQ-0011'],//'业务单元',
                            refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                            rootNode:{refname:json['201101DLSQ-0011']/* '业务单元' */,refpk:'root'},
                            placeholder: json['201101DLSQ-0011'],//"业务单元",
                            queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                            treeConfig:{name:[json['201101DLSQ-0012']/* '编码' */, json['201101DLSQ-0013']/* '名称' */],code: ['refcode', 'refname']},
                            isMultiSelectedEnabled: false, 
                            //unitProps:unitConf,
                            isShowUnit:false
                        };
                    }
                    if(item.attrcode === 'jkbxr' ){
                        item.isShowUnit = true;
                        item.unitProps = {
                            refType: 'tree',
                            refName: json['201101DLSQ-0011'],//'业务单元',
                            refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                            rootNode:{refname:json['201101DLSQ-0011']/* '业务单元' */,refpk:'root'},
                            placeholder: json['201101DLSQ-0011'],//"业务单元",
                            queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                            treeConfig:{name:[json['201101DLSQ-0012']/* '编码' */, json['201101DLSQ-0013']/* '名称' */],code: ['refcode', 'refname']},
                            isMultiSelectedEnabled: false,
                            //unitProps:unitConf,
                            isShowUnit:false
                        };
                    }
                    if(item.attrcode === 'djlxbm'){
                        item.queryCondition = function () {
                            return {
                                parentbilltype: "263X,264X",
                                ExtRefSqlBuilder:'nccloud.web.action.erm.ref.sqlbuilder.BilltypeRefSqlBuilder'//交易类型参照
                            }
                        }
                    }
                });
                data.template[window.areaCode.billsArea].items.forEach((item) => {
                    if(item.attrcode === 'djbh'){
                        item.render = (text, record, index) => {
                            return (
                                <span className={'hyperlinks'} onClick={(() => {
                                    linkQueryApi.link({
                                        data : {
                                            openbillid : record.pk_jkbx.value,
                                            tradetype : record.djlxbm.value,
                                            props : props
                                        }
                                    });
                                })}>
                                    {record.djbh.value}
                            </span>
                            )
                        }
                    }
                })
                let meta = {
                    '20110BQLB': {
                        moduletype: 'search',
                        items : data.template[window.areaCode.searchArea].items
                    },
                    head:{
                        moduletype: 'table',
                        pagination: true,
                        items: data.template[window.areaCode.billsArea].items
                    }
                }
                meta.pageid = data.template.pageid;
                props.meta.setMeta(meta);
                props.button.setButtons(data.button);
                props.button.setButtonDisabled('Create', true);
    
                //设置表格的扩展按钮列
                setTableExtendCol(props, meta, [{
                    areaId: window.areaCode.billsArea,
                    buttonVisible: (record, inedx) =>{
                        return ['Linkpz']
                    },
                    onButtonClick: tableExtendButtonClick.bind(that),
                }]);
            }})
        }
    )
}
