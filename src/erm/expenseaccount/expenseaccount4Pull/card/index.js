import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {base,ajax, RenderRouter,toast, createPage} from 'nc-lightapp-front';
import {initTemplate, afterEvent, searchClick} from './events';
import expenseaccountCard from 'src/erm/expenseaccount/expenseaccount/card/jkbxBillCard'
import presetVar from './presetVar';
import requestApi from './requestApi'
import './index.less';
import {
    ProfileStyle,
    ProfileHead,
    ProfileBody,
    EmptyArea
} from 'ssccommon/components/profile';
const{NCButton} = base;

class ExpenseaccountForPull extends Component {
    constructor(props) {
        super(props);
        let {form, button, table, editTable, search} = this.props;
        let {setSearchValue, setSearchValByField, getAllSearchData} = search;
        this.setSearchValByField = setSearchValByField;//设置查询区某个字段值
        this.getAllSearchData = getAllSearchData;//获取查询区所有字段数据
        initTemplate.call(this, props);
    }

    componentDidMount() {
        this.props.transferTable.setTransferTableValue(presetVar.areaCode.billsArea, presetVar.areaCode.bodysArea, [], 'pk_mtapp_bill', 'pk_mtapp_detail');
    }

    render() {
        let {form, button, table, editTable, search,transferTable} = this.props;
        let {NCCreateSearch} = search;
        let {createButton} = button;
        let{createEditTable} = editTable;
        let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId
        let { createTransferTable } = transferTable;
        let tradetype = this.props.getUrlParam("tradetype")||presetVar.tradetype;

        let isShowSelectedList = transferTable.getSelectedListDisplay(presetVar.areaCode.billsArea);
        ;
        let appcode = this.props.getUrlParam("appcode");
        let hxyt = appcode != "201102BCLF" && appcode != "201102BCLF407";
        return (
	    <div><ProfileStyle
                layout="singleTable"
                {...this.props}
                >
                { isShowSelectedList ? 
                    null 
                    : hxyt ? 
                    (
                        <ProfileHead
                        // 201102CZBX-0001": "选择申请单"
                        title={multiLang && multiLang.get('201102CZBX-0001')}
                        >
                        <NCButton comName="NCButton" onClick={() => {
                                this.props.openTo('/erm/expenseaccount/expenseaccount/card/index.html#/add', {  "pagecode": '', "appcode": '' });
                            }
                        }>
                            核销预提
                        </NCButton>
                        </ProfileHead>
                    ) :
                    (
                        <ProfileHead
                        // 201102CZBX-0001": "选择申请单"
                        title={multiLang && multiLang.get('201102CZBX-0001')}
                        >
                        </ProfileHead>
                    )
                }
		        <ProfileBody>
                    {
                        isShowSelectedList ?
                        null
                        :
                        <EmptyArea className="ssc-profile-search-area">
            
                            { NCCreateSearch(
                                    presetVar.areaCode.searchArea ,//模块id
                                {
                                    clickPlanEve:searchClick.bind(this),//点击高级面板中的查询方案事件
                                    clickSearchBtn: searchClick.bind(this),//   点击按钮事件
                                        defaultConditionsNum:1, //默认显示几个查询条件
                                    onAfterEvent: afterEvent()//编辑后事件
                                }
                            )}
                        </EmptyArea>
                    }

           
                    <EmptyArea>
                    {createTransferTable(
                            {
                                headTableId: presetVar.areaCode.billsArea,//表格组件id
                                bodyTableId: presetVar.areaCode.bodysArea,//子表模板id
                                fullTableId: presetVar.areaCode.bodysArea,//主子拉平模板id
                                //点击加号展开，设置表格数据

                                // "201102CZBX-0002": "生成报销单"
                                transferBtnText: multiLang && multiLang.get('201102CZBX-0002'), //转单按钮显示文字
                                containerSelector: '#transferSQToBx', //容器的选择器 必须唯一,用于设置底部已选区域宽度
                                cacheItem:[],
                                onTransferBtnClick: (ids) => {//点击转单按钮钩子函数
                                    if(ids.length >1){
                                        let id= ids[0].head.pk;
                                        for(let i=1;i<ids.length;i++){
                                            if(id != ids[i].head.pk){
                                                // "201102CZBX-0003": "最多选择同一申请记录的不同数据!"
                                                toast({title: "操作错误!",content:multiLang && multiLang.get('201102CZBX-0003')});
                                                break;
                                            }
                                        }

                                    }else{
                                        let pagecode=window.presetVar.topagecode;
                                        this.props.pushTo('/card',{
                                            addType:'pull',
                                            status:'add',
                                            pagecode:pagecode,
                                            appcode:presetVar.appcode,
                                            "tradetype":tradetype
                                        })
                                    }

                                },
                                onChangeViewClick: () => {//点击切换视图钩子函数
                                    if (!this.props.meta.getMeta()[this.fullTableId]) {
                                        initTemplate(this.props); //加载主子拉平模板
                                    }
                                    this.props.transferTable.changeViewType(this.headTableId);

                                }
                            }
                    )}
                </EmptyArea>
            </ProfileBody>
        </ProfileStyle>
        </div>
    )
    }
}

ExpenseaccountForPull = createPage({
    mutiLangCode: '2011'
})(ExpenseaccountForPull);


const routes = [
	{
		path: '/pull',
		component: ExpenseaccountForPull
    },
    {
        path: '/card',
        component: expenseaccountCard
    }
];


(function main(routers,htmlTagid){
  RenderRouter(routers,htmlTagid);
})(routes,"app");

//ReactDOM.render(<ExpenseaccountForPull />, document.querySelector('#app'));