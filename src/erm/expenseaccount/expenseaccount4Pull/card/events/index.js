import initTemplate from './initTemplate';
import afterEvent  from './afterEvent';
import tableModelConfirm from './tableModelConfirm';
import searchClick from './searchClick';
export {initTemplate, afterEvent, tableModelConfirm, searchClick};

