import {ajax} from 'nc-lightapp-front';
import requestApi from '../requestApi'
import presetVar from '../presetVar';
import searchClick from './searchClick';
(function (){
    requestApi.tpl({
        async:false,
        data:"",
        success: (datavalue) => {
            window.defalutvalue = datavalue.data;
        }
    });
}());
export default function (props) {

   let  appcode = (props.getUrlParam("appcode")==undefined||props.getUrlParam("appcode")=='')?props.getSearchParam("c"):props.getUrlParam("appcode");
   let  pagecode =  (props.getUrlParam("pagecode")==undefined||props.getUrlParam("pagecode")=='')?props.getSearchParam("p"):props.getUrlParam("pagecode");
   presetVar.appcode=appcode;
   presetVar.pagecode2=pagecode;
    props.createUIDom(
        {
            // pagecode: "201102CZBX_card",//页面编码
            // appcode: "201102CZBX"//小应用编码
            appcode:appcode,
            pagecode:pagecode
        },
        (data) => {

            let metas = data.template;
            let tempArr = [];
            let searchitem = metas[presetVar.areaCode.searchArea];
            let headmeta = metas[presetVar.areaCode.billsArea];
            let bodymeta = metas[presetVar.areaCode.bodysArea];
            searchitem.items.forEach((item) => {

                 if(item.itemtype == 'refer'){
                     item.isShowUnit=true;
                     // item.visible = true;
                     // item.required = true;
                 }
                if(item.attrcode && item.attrcode=="pk_org" && window.defalutvalue.pk_org){
                    item.initialvalue = {value:window.defalutvalue.pk_org,display:window.defalutvalue.pk_org_value}
                }
                if(item.attrcode && item.attrcode=="pk_currtype" && window.defalutvalue.bzbm){
                    item.initialvalue = {value:window.defalutvalue.bzbm,display:window.defalutvalue.bzbm_value}
                }
                 // else{
                //      item.visible = true;
                //  }
                 tempArr.push(item);
             });
            let transtype = "";
            if(data.context.paramMap && data.context.paramMap.transtype){
                transtype = data.context.paramMap.transtype
            };
            if(data.context.paramMap && data.context.paramMap.pagecode){
                window.presetVar.topagecode = data.context.paramMap.pagecode;
            }

            window.presetVar.tradetype= props.getUrlParam("tradetype")==null||props.getUrlParam("tradetype")==''?transtype:props.getUrlParam("tradetype");
            let meta = {
                ...metas,
                searchArea: {
                    moduletype: 'search',
                    items : tempArr
                },
                head:{
                    moduletype: 'table',
                    items: headmeta.items
                },
                body:{
                    moduletype: 'table',
                    items: bodymeta.items
                }
            }

            props.meta.setMeta(meta);
            searchClick.call(this,props,null, 'defaultSearch');
    });

}

