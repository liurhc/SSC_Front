import requestApi from "ssccommon/components/requestApi";
import {ajax} from 'nc-lightapp-front';

let requestApiOverwrite = {
    ...requestApi,
    //单据模板接口

    getTableData: (opt) => {
        ajax({
            url: `/nccloud/erm/expamortize/ExpAmortizeViewAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    }

}

export default requestApiOverwrite;
