import React, {Component} from 'react';
import {createPage,high,base} from 'nc-lightapp-front';
import {buttonClick, initTemplate, afterEvent,refreshButtonClick} from './events';
import requestApi from './requestApi';
const { NCModal,NCButton} = base;
const { Inspection } = high;

import {
    ProfileStyle,
    ProfileHead,
    ProfileBody,
    HeadCenterCustom,
    ButtonGroup
} from 'ssccommon/components/profile';
import {EditTable} from 'ssccommon/components/table';
import pubMessage from 'ssccommon/utils/pubMessage'


// import BusinessUnitTreeRef from 'uapbd/refer/org/FinanceOrgTreeRef';
import BusinessUnitTreeRef from 'uapbd/refer/org/BusinessUnitTreeRef';


import './index.less';

let body = window.presetVar.body;

class Expamortize extends Component {

    
    constructor(props) {
        super();
        this.treeFormId = "treeForm";
        this.state = {
            businessUnitTreeRefValue: {},
            businessUnitTreeRefValue1: {},
            showModal:false,
            showList:false,
            showLinkBudget: false,
            sourceData: null,
            period:'',
            status: 'browse'
        }
        this.pubMessage = new pubMessage(props);
        initTemplate.call(this, props);
    }

    cancel(){
        this.setState({
            showLinkBudget: false
        })
    }
    affirm(info){
        this.setState({
            showLinkBudget: false
        })
    }
    
    onSelectedFn(){
        let rows1 = this.editTable.getCheckedRows('ExpAmortize');
        if(rows1.length){
            this.button.setButtonDisabled('Amortize', false);
            this.button.setButtonDisabled('Cancel', false);
        }else{
            this.button.setButtonDisabled('Amortize', true);
            this.button.setButtonDisabled('Cancel', true);
        }
    }

    render() {
        let { form } = this.props;
        let { createForm } = form;
        let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId
        buttonClick.AmortizeEventConfig.click = buttonClick.AmortizeEventConfig.click.bind(this);
        return (
        /*档案风格布局*/
        <div>          
            <ProfileStyle
                layout="singleTable"
                {...this.props}
            >
                {/*页面头*/}
                {/*"201103DTFY-0001": "待摊费用摊销",*/}
                <ProfileHead
                    title={multiLang && multiLang.get('201103DTFY-0001')}
                    refreshButtonEvent={refreshButtonClick.bind(this)}
                    status = { this.state.status }
                >
                    <HeadCenterCustom>
                        <span class="ssc-ref-must">*</span>
                        <div style={{width: '204px'}}>
                            <BusinessUnitTreeRef
                                value={this.state.businessUnitTreeRefValue}
                                onChange={(data) => {
                                this.setState({
                                    businessUnitTreeRefValue : data
                                }),

                                requestApi.getTableData({
                                    data: { pk_org : data.refpk,
                                            isGroup : false},
                                    success: (data) => {
                                        if(data.data.ExpAmortize != null){
                                            this.props.editTable.setTableData(body.body1, data.data[body.body1]);
                                        }else{
                                            this.props.editTable.setTableData(body.body1, {rows: []});
                                        }                
                                    }
                                })

                            }}
                            queryCondition={() => {
                                return {test: null}
                            }}
                        />
                     </div>
                        {/*"201103DTFY-0002": "会计期间",*/}
                        &nbsp;&nbsp;
                        <div style={{height:"32px",lineHeight:"32px"}}>{multiLang && multiLang.get('201103DTFY-0002')}: {this.state.period}</div>
                   
                    </HeadCenterCustom>

                    <ButtonGroup
                        area="page_header"
                        buttonLimit="3"
                        buttonEvent={
                            {
                                Amortize: buttonClick.AmortizeEventConfig,
                                Cancel: buttonClick.cancelBtnEventConfig,
                                Print: buttonClick.printBtnEventConfig
                            }

                        }
                    />
                </ProfileHead>

                {/*页面体*/}
                <ProfileBody>
                    <EditTable
                        areaId="ExpAmortize"
                        showCheck= {true}
                        onAfterEvent={afterEvent.bind(this)}
                        onSelected={this.onSelectedFn}
                        onSelectedAll={this.onSelectedFn}
                    > 
                    </EditTable>

                    <NCModal show = {
                        this.state.showModal
                    }
                        size='lg'
                        onHide = {
                            () => this.setState({ showModal: false })
                        }>
                        <NCModal.Header closeButton={true} onHide={() =>close()}>
                            {/*"201103DTFY-0003": "修改摊销期"*/}
                        <NCModal.Title>{multiLang && multiLang.get('201103DTFY-0003')}</NCModal.Title>
                        </NCModal.Header>

                        <NCModal.Body>
                            {createForm(this.treeFormId)}
                        </NCModal.Body>

                        <NCModal.Footer>
                            <NCButton
                            colors="primary"
                            onClick={buttonClick.confirm.bind(this,this.props, this.rowvalue)}
                            >
                                {/*"2011-0003": "确定"*/}
                                {multiLang && multiLang.get('2011-0003')}
                            </NCButton>
                            <NCButton
                            onClick={() => this.setState({ showModal: false })}
                            >
                                {/*"2011-0002": "取消"*/}
                                {multiLang && multiLang.get('2011-0002')}
                            </NCButton>
                        </NCModal.Footer>

                    </NCModal>
                    
                    <NCModal show = {
                        this.state.showList
                    }
                        size='lg'
                        onHide = {
                            () => this.setState({ showList: false })
                        }>
                        <NCModal.Header closeButton={true} onHide={() =>close()}>
                            {/*"201103DTFY-0004": "摊销记录"*/}
                            <NCModal.Title>{multiLang && multiLang.get('201103DTFY-0004')}</NCModal.Title>
                        </NCModal.Header>

                        <NCModal.Body>
                            {this.props.table.createSimpleTable("expamtproc_list", {
                               showIndex: true
                            })}
                        </NCModal.Body>

                    </NCModal>
                </ProfileBody>
            </ProfileStyle>
            <div>
                <Inspection
                    show={ this.state.showLinkBudget }
                    sourceData = { this.state.sourceData }
                    cancel = { this.cancel.bind(this) }
                    affirm = { this.affirm.bind(this) }
                />
            </div>
        </div>
        )
    }
}

Expamortize = createPage({
    mutiLangCode: '2011'
})(Expamortize);

export default Expamortize;
