import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {connect, Provider} from 'react-redux';
import store from './store/store';


import Expamortize from './Expamortize';

ReactDOM.render((<Provider store={store}><Expamortize /></Provider>)
    , document.querySelector('#app'));