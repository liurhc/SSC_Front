import requestApi from '../requestApi';

export default function () {
    let body = window.presetVar.body;
    requestApi.getTableData({
        data: {
            pk_org: this.state.businessUnitTreeRefValue.refpk,
            period:this.state.period,
            isGroup: false
        },
        success: (data) => {
            var time = data.data.period;
            if(data.data.period == undefined){
                time = JSON.parse(data.data['userjson']).period;
            }
            this.setState({
                period:time
            })
            this.pubMessage.refreshSuccess();
            if(data.data.ExpAmortize != null){
                this.props.editTable.setTableData(body.body1, data.data[body.body1]);
            }else{
                this.props.editTable.setTableData(body.body1, {rows: []});
            }
            
        }
    })
}