import 'ssccommon/components/globalPresetVar';
import {setTableExtendCol} from 'ssccommon/components/profile'
import requestApi from '../requestApi';
import {ajax,getMultiLang} from 'nc-lightapp-front';
import linkVoucherApi from '../../../../public/components/linkvoucher/linkvoucher';
import linkBudgetApi from '../../../../public/components/linkbudget/linkbudget';
import linkQueryApi from "../../../../../erm/public/components/linkquery/linkquerybills"

//变量设置规范，如需要
window.presetVar = {
    ...window.presetVar,
    pageId: '',
    head: {
        head1: '',
        head2: ''
    },
    body: {
        body1: 'ExpAmortize',
        body2: ''
    },
    search: {}
};

let body = window.presetVar.body;


export default function (props) {

    props.createUIDom(
        {
            pagecode: '201103DTFY_LIST',//页面id
            appid: '0001Z310000000006H1Y'//注册按钮的id
        },
        (data) => {
            getMultiLang({moduleId: 2011, domainName: 'erm',currentLocale: 'zh-CN', callback: (json) => {
            let meta = data.template;
            let buttonTpl = data.button;

            meta[body.body1].items = meta[body.body1].items.map((item, key) => {
                return item;
            });

            //设置表格的扩展按钮列
            setTableExtendCol(props, meta, [{
                areaId: 'ExpAmortize',
                btnAreaId: 'list_inner',
                width: '210px',               
                btnKeys: ['Edit','linkbill','linkvoucher','linkbudget','linkexpamortize'],
                onButtonClick: (btnKey, record)=> {
                    let props = this.props;
                    switch (btnKey) {
                        case 'Edit' :
                            let data = record.values
                            this.rowvalue = data
                            props.form.setFormItemsValue("treeForm",{'res_period':{value:data.res_period.value,display:null}})
                            this.setState({ showModal: true });
                            break;
                        case 'linkbill' :  //联查单据
                            linkQueryApi.link({
                                data : {
                                    openbillid : record.values.pk_jkbx.value==null?(record.values.pk_payablebill.value == null ? record.values.pk_receivablebill.value : record.values.pk_payablebill.value):record.values.pk_jkbx.value,
                                    tradetype : record.values.bx_pk_billtype.value,
                                    props : props
                                }
                            })
                            break;
                        case 'linkexpamortize' : //联查摊销明细
                            let value = record.values
                            ajax({
                                url: '/nccloud/erm/expenseaccount/ExpenseaccountTempletAction.do',
                                data: {
                                    pagecode: 'expamtproc_list'
                                },
                                success: result => {
                                    let old = this.props.meta.getMeta();
                                    old.expamtproc_list = result.data.Expamtpro;
                                    this.props.meta.setMeta(old);
                                    this.isLoadTemplate = true;      

                                    ajax({
                                        url: `/nccloud/erm/expamortize/linkProcAction.do`,
                                        data: {
                                            "head": {
                                            "areaType": "form",
                                            "rows": [{'values':value}],
                                            "areacode": "head"
                                            },
                                        },
                                        success: (data) => {
                                            this.setState({ showList: true });
                                            if(data.data != null){
                                                this.props.table.setAllTableData('expamtproc_list',data.data.Expamtpro);
                                            }else{
                                                this.props.table.setAllTableData('expamtproc_list',{rows: []});
                                            }
                                            
                                        }
                                    })     
                                }
                            })                                                
                            break;
                        case 'linkvoucher' : //联查凭证
                            linkVoucherApi.link({
                                data  : {
                                    props : props,
                                    record : {
                                        pk_billtype : record.values.pk_billtype.value,
                                        pk_group : record.values.pk_group.value,
                                        pk_org : record.values.pk_org.value,
                                        relationID : record.values.pk_expamtinfo.value
                                    },
                                    appid : props.getSearchParam('ar')//'0001Z310000000006H1E'
                                }
                            })
                            break;
                        case 'linkbudget' : //联查预算
                            linkBudgetApi.link({
                                data : {
                                    "tradetype": record.values.bx_pk_billtype.value,
                                    "openbillid" : record.values.pk_jkbx.value
                                },
                                success: (data) => {
                                    this.setState({
                                        sourceData: data,
                                        showLinkBudget: true
                                    })
                                }
                            })
                            break;

                    }
                }
            }]);

            meta.treeForm = { 
                moduletype: 'form',
                items: [
                {
                    visible:true,
                    disabled: true,
                    //当前剩余摊销期
                    label: json['201103DTFY-0012'],
                    attrcode: 'res_period',
                    itemtype: 'input',
                    required: true,
                    col: 6,
                },
                {
                    visible:true,
                    //修改后剩余摊销期
                    label: json['201103DTFY-0013'],
                    attrcode: 'period',
                    itemtype: 'input',
                    required: true,
                    col: 6,
                },
            ],
            status: 'edit'
            }

            meta.ExpAmortize.items[meta.ExpAmortize.items.length-1].fixed='right'
            //设置区域模板
            props.meta.setMeta(meta);
            //设置按钮模板
            props.button.setButtons(buttonTpl);
            props.button.setButtonDisabled('Amortize', true);
            props.button.setButtonDisabled('Cancel', true);


            //设置按钮的初始可见性
            props.button.setButtonsVisible({'Add': true});

            //设置初始数据
            //获取表格的数据
            requestApi.getTableData({
                data: {
                    pk_org: this.state.businessUnitTreeRefValue.refpk,
                    isGroup: false
                },
                success: (data) => {
                    if(data.data != null){
                        if(data.data.pk_org != null){
                            this.setState({
                                businessUnitTreeRefValue: {refpk: data.data.pk_org, refname: data.data.name_org},
                                period:data.data.period
                            })
                            return;
                        }else{
                            this.props.editTable.setTableData(body.body1, data.data[body.body1]);
                            this.props.editTable.setStatus(body.body1, 'edit');
                            if(data.data.ExpAmortize != undefined){
                                data.data.ExpAmortize.rows.map((row,index) =>{
                                    if(row.values.amt_status.value == false){
                                        this.props.editTable.setEditableRowKeyByIndex(body.body1, index, ["curr_amount"], true);
                                    }
                                })
                            }

                            if ('back' != this.props.getUrlParam('status')) {
                                if(data.data['userjson'] != undefined){
                                    let userJson = JSON.parse(data.data['userjson']);
                                    this.setState({
                                        businessUnitTreeRefValue: {refpk: userJson.pk_org, refname: userJson.name_org},
                                        period:userJson.period
                                    })
                                }
                            }
                        }
                    }
                }
            })

        }
     })}
    )
}
