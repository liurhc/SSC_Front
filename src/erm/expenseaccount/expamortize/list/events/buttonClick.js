import {ajax} from 'nc-lightapp-front';
import requestApi from '../requestApi';
import { base,print } from 'nc-lightapp-front';
const { NCMessage } = base;


let body = window.presetVar.body;
//摊销
let AmortizeEventConfig = {
    click(props){
        let data = props.editTable.getCheckedRows('ExpAmortize')
        let pk_expamtinfo = {value:data.map(value => value.data.values)}


        let values = {

            "head": {
            "areaType": "form",
            "rows": pk_expamtinfo.value.map(v => {
                return {values: v, status: v.status ? "" : v.status.value}
            }),
            "areacode": "head"
        }
        
        }
        ajax({
            url: `/nccloud/erm/expamortize/ExpAmortizeAction.do`,
            data: values,
            success: (data) => {
                let message
                let pk_org
                if(data.data[0].successVO != null){
                    pk_org = data.data[0].successVO.m_headVo.pk_org
                }else{
                    let multiLang = props.MutiInit.getIntl(2011);
                    //请选择摊销单据
                    NCMessage.create({content: multiLang && multiLang.get('201103DTFY-0006'), color: 'warning', position: 'bottomRight'});
                    return 
                }
                if(data.data[0].success == false){
                    NCMessage.create({content: data.data[0].errorMessage, color: 'warning', position: 'bottom'});
                    return
                }
                
                requestApi.getTableData({
                    data: { pk_org : pk_org,
                            isGroup : false},
                    success: (data) => {
                        this.props.editTable.setTableData(body.body1, data.data[body.body1]);
                        this.props.editTable.setStatus(body.body1, 'edit');
                        data.data.ExpAmortize.rows.map((row,index) =>{
                            if(row.values.amt_status.value == false){
                                this.props.editTable.setEditableRowKeyByIndex(body.body1, index, ["curr_amount"], true);
                            }
                        })
                        let multiLang = this.props.MutiInit.getIntl(2011);
                        //摊销成功
                        NCMessage.create({content: multiLang && multiLang.get('201103DTFY-0007'), color: 'success', position: 'bottomRight'});
                    }
                })
            } 
        })   
    }
}

//反摊销
let cancelBtnEventConfig = {
    click(props){
        let data = props.editTable.getCheckedRows('ExpAmortize')
        let pk_expamtinfo = {value:data.map(value => value.data.values)}


        let values = {

            "head": {
            "areaType": "form",
            "rows": pk_expamtinfo.value.map(v => {
                return {values: v, status: v.status ? "" : v.status.value}
            }),
            "areacode": "head"
        }
        
        }
        ajax({
            url: `/nccloud/erm/expamortize/UnExpAmortizeAction.do`,
            data: values,
            success: (data) => {
                let pk_org
                if(data.data[0].successVO != null){
                    pk_org = data.data[0].successVO.m_headVo.pk_org
                    if(data.data[0].errorMessage != null){
                        NCMessage.create({content: data.data[0].errorMessage, color: 'warning', position: 'bottomRight'});
                        return
                    }
                }else{
                    let multiLang = props.MutiInit.getIntl(2011);
                    //请选择反摊销的单据
                    NCMessage.create({content: multiLang && multiLang.get('201103DTFY-0008'), color: 'warning', position: 'bottomRight'});
                    return 
                }
                
                requestApi.getTableData({
                    data: { pk_org : pk_org,
                            isGroup : false},
                    success: (data) => {
                        props.editTable.setTableData(body.body1, data.data[body.body1]);
                        props.editTable.setStatus(body.body1, 'edit');
                        data.data.ExpAmortize.rows.map((row,index) =>{
                            if(row.values.amt_status.value == false){
                                props.editTable.setEditableRowKeyByIndex(body.body1, index, ["curr_amount"], true);
                            }
                        })
                        let multiLang = props.MutiInit.getIntl(2011);
                        //反摊销成功
                        NCMessage.create({content: multiLang && multiLang.get('201103DTFY-0009'), color: 'success', position: 'bottomRight'});
                    }
                })
            } 
        })

    }
}

//打印
let printBtnEventConfig = {
    click(props){
        let data = props.editTable.getCheckedRows('ExpAmortize')
        let pk_expamtinfo = {value:data.map(value => value.data.values)}
        let billPks = [];
        if (pk_expamtinfo.value.length){
            pk_expamtinfo.value.forEach((printrow) => {
                billPks.push(printrow.pk_jkbx.value);
            });
        }
        print(
            'pdf',  //支持两类: 'html'为模板打印, 'pdf'为pdf打印
            '/nccloud/erm/expamortize/ExpamortizePrintAction.do', //后台服务url
            {
                billtype:'2641',  //单据类型
                // funcode:'20110ETLB',      //功能节点编码，即模板编码
                // nodekey:'2641_IWEB',     //模板节点标识
                funcode:'201103DTFY',
                nodekey:'list',
                oids:billPks,    // 功能节点的数据主键
                // printTemplateID: "1001Z31000000000R1YQ"
            }
        )
     
        

    }
}

//修改摊销期
let confirm = function(props, rowvalue, e) {
    let period = props.form.getFormItemsValue('treeForm','period').value

    ajax({
        url: `/nccloud/erm/expamortize/UpdatePeriodAction.do`,
        data: {

            "head": {
            "areaType": "form",
            "rows": [{'values':rowvalue}],
            "areacode": "head"
            },
            userjson:"'period':"+period,
        },
        
        success: (data) => {
            let multiLang = props.MutiInit.getIntl(2011);
            //请输入修改后剩余摊销期
            let message = multiLang && multiLang.get('201103DTFY-0010')
            if(data.data == message){
                NCMessage.create({content: multiLang && multiLang.get('201103DTFY-0010'), color: 'warning', position: 'bottomRight'});
            }else{
                requestApi.getTableData({
                    data: { pk_org : data.data.pk_org,
                            isGroup : false},
                    success: (data) => {
                        this.setState({ showModal: false });
                        props.editTable.setTableData(body.body1, data.data[body.body1]);
                        data.data.ExpAmortize.rows.map((row,index) =>{
                            if(row.values.amt_status.value == false){
                                props.editTable.setEditableRowKeyByIndex(body.body1, index, ["curr_amount"], true);
                            }
                        })
                        let multiLang = props.MutiInit.getIntl(2011);
                        //修改成功
                        NCMessage.create({content: multiLang && multiLang.get('201103DTFY-0011'), color: 'success', position: 'bottomRight'});
                    }
                })
            }
        } 
    })

}

export default { AmortizeEventConfig,cancelBtnEventConfig,confirm,printBtnEventConfig}


