import React, { Component } from 'react';
import Batchcloseaccbook from '../../../../uapbd/orgcloseacc/batch_closeaccbook/main';
import {createPage, getMultiLang} from 'nc-lightapp-front';
/**
 * 财务组织关账
 */
class Closeaccbook_erm extends Component{
    constructor(props){
        super(props);
        /* this.state = {
            title : ""
        }
        getMultiLang({moduleId: 2011, domainName: 'erm',currentLocale: 'simpchn', callback: (json) => {
            this.setState( { 
                title : json['201103FYGZ-0001']
            })
        }}) */
    }
    render(){
        // "201103FYGZ-0001": "费用关账",
        let config = {
            title : "费用关账",//这里不用多语，Batchcloseaccbook组件里边会根据appcode取多语，取不到的情况下才用这个
            appCode: '201103FYGZ',
            showMoudles : {
                '2011'  : true
            }
        }
        return (
            <Batchcloseaccbook {...{config:config}}/>
        )
    }
}
 Closeaccbook_erm = createPage({
    mutiLangCode: '2011'
})(Closeaccbook_erm);
export default Closeaccbook_erm;