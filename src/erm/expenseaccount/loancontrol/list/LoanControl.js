import React, {Component} from 'react';
import {connect} from 'react-redux';
import {createPage, base, high, cacheTools} from 'nc-lightapp-front';
import {buttonClick, initTemplate, afterEvent, onRowDoubleClick, refreshButtonClick} from './events';
import requestApi from './requestApi';
import pubMessage from 'ssccommon/utils/pubMessage';

import {
    ProfileStyle,
    ProfileHead,
    ProfileBody,
    HeadCenterCustom,
    ButtonGroup
} from 'ssccommon/components/profile';
import {EditTable} from 'ssccommon/components/table';

import './index.less';

let body = window.presetVar.body;

class LoanControl extends Component {

    constructor(props) {
        super();
        this.state = {
            status: 'browse'
        }
        this.pubMessage = new pubMessage(props);
        initTemplate.call(this, props);
    }

    componentDidMount() {

    }

    render() {
        let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId
        buttonClick.addBtnEventConfig.click = buttonClick.addBtnEventConfig.click.bind(this);
        return (
            /*档案风格布局*/
            <ProfileStyle
                layout="singleTable"
                {...this.props}
            >
                {/*页面头*/}
                <ProfileHead
                    // "201101JKKJ-0001": "借款控制规则-集团"
                    title={multiLang && multiLang.get('201101JKKJ-0001')}
                    refreshButtonEvent={refreshButtonClick.bind(this)}
                    status = { this.state.status }
                >

                    <ButtonGroup
                        area="page_header"
                        buttonLimit="3"
                        buttonEvent={
                            {
                                Add: buttonClick.addBtnEventConfig
                            }

                        }
                    />
                </ProfileHead>

                {/*页面体*/}
                <ProfileBody>
                    <EditTable
                        areaId="LoanControl"
                        showCheck= {false}
                        onAfterEvent={afterEvent.bind(this)}
                        onRowDoubleClick={onRowDoubleClick.bind(this)}
                    >
                    </EditTable>

                </ProfileBody>
            </ProfileStyle>
        )
    }
}

LoanControl = createPage({
    mutiLangCode: '2011'
})(LoanControl);

export default LoanControl;
