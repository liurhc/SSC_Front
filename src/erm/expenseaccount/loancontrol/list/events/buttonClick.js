import {ajax,cacheTools} from 'nc-lightapp-front';
import requestApi from '../requestApi';

//增加
let addBtnEventConfig = {
    click(props){
        props.linkTo('/erm/expenseaccount/loancontrol/card/index.html', {
            status: 'add',
            pagecode:'201101JKKJ_CARD'
        });
    }
}


export default { addBtnEventConfig}


