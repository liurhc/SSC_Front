

export default function onRowDoubleClick(record,index,props) {
    props.linkTo('/erm/expenseaccount/loancontrol/card/index.html', {
        status: 'browse',
        id: record.values.pk_control.value,
        pagecode:'201101JKKJ_CARD'
    });
};