import requestApi from '../requestApi';
import 'ssccommon/components/globalPresetVar';

export default function () {
    let body = window.presetVar.body;
    requestApi.getTableData({
        data: {
            pk_org: null,
            isGroup: true
        },
        success: (data) => {
            this.props.editTable.setTableData(body.body1, data.data[body.body1]);
            this.pubMessage.refreshSuccess();
        }
    })
}
