import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {connect, Provider} from 'react-redux';
import store from './store/store';


import LoanControl from './LoanControl';

ReactDOM.render((<Provider store={store}><LoanControl /></Provider>)
    , document.querySelector('#app'));