import requestApi from "ssccommon/components/requestApi";
import {ajax} from 'nc-lightapp-front';

let requestApiOverwrite = {
    ...requestApi,
    //单据模板接口

    save: (opt) => {
        ajax({
            url: `/nccloud/erm/loancontrol/LoanControlSaveAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    getTableData: (opt) => {
        ajax({
            url: `/nccloud/erm/loancontrol/LoanControlViewAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    }

}

export default requestApiOverwrite;
