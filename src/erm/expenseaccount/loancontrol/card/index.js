import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, base, ajax, createPageIcon } from 'nc-lightapp-front';
import buttonClick from './event/buttonClick';
import './index.less';
window.bodyCode = "LoanControl";
const { NCBackBtn, NCAffix } = base;
const { NCDiv: Div } = base;

class LoanControlCard extends Component {
    constructor() {
        super();
        this.state = {
            isEffect: false,
        }
    }

    componentWillMount() {
                window.onbeforeunload = () => {
                    let status = this.props.form.getFormStatus('base');              
                    if (status == 'edit'||status == 'add') {
                        return ''; 
                         }
             }
            }
    componentDidMount() {

        this.props.createUIDom(
            {
                pagecode: '201101JKKJ_CARD',
                appcode: '201101JKKJ'
            },
            (data) => {
                let meta = data.template;
                let bmeta = data.button;

                meta['advance'].items.map((item) => {
                    if (item.attrcode == 'djlxbm') {
                        item.queryCondition = () => {
                            return { GridRefActionExt: 'nccloud.web.action.erm.ref.sqlbuilder.TranstypeRefSqlBuilder' };
                        };
                    }
                });
                //设置区域模板
                this.props.meta.setMeta(meta);
                this.props.button.setButtons(bmeta);

                let status = this.props.getUrlParam('status');
                if (status == 'edit') {
                    ajax({
                        url: `/nccloud/erm/loancontrol/LoanControlQueryAction.do`,
                        data: { "pk_control": this.props.getUrlParam('id') },
                        success: (res) => {
                            if (res.success) {
                                this.props.form.setAllFormValue({ 'base': res.data['LoanControl'] });
                                this.props.form.setFormStatus('base', 'edit');
                                this.props.button.setButtonsVisible({ 'Edit': false, 'Save': true, 'Cancel': true });
                            }
                        }
                    });
                } else if (status == 'browse') {
                    ajax({
                        url: `/nccloud/erm/loancontrol/LoanControlQueryAction.do`,
                        data: { "pk_control": this.props.getUrlParam('id') },
                        success: (res) => {
                            if (res.success) {
                                this.props.form.setAllFormValue({ 'base': res.data['LoanControl'] });
                                this.props.form.setFormStatus('base', 'browse');
                                this.props.button.setButtonsVisible({ 'Edit': true, 'Save': false, 'Cancel': false });
                                this.setState({ isEffect: true });
                                // setControlVisible();

                                let metanew = this.props.meta.getMeta();
                                metanew['controlstyleform'].items.find(e => e.attrcode === 'sum').visible = false;
                                metanew['controlstyleform'].items.find(e => e.attrcode === 'nums').visible = false;
                                metanew['controlstyleform'].items.find(e => e.attrcode === 'days').visible = false;
                                metanew['controlstyleform'].items.find(e => e.attrcode === 'term').visible = false;
                                if (this.props.form.getFormItemsValue('base', 'sum').value != true) {
                                    metanew['controlstyleform'].items.find(e => e.attrcode === 'sumvalue').visible = false;
                                }
                                if (this.props.form.getFormItemsValue('base', 'nums').value != true) {
                                    metanew['controlstyleform'].items.find(e => e.attrcode === 'numsvalue').visible = false;
                                }
                                if (this.props.form.getFormItemsValue('base', 'days').value != true) {
                                    metanew['controlstyleform'].items.find(e => e.attrcode === 'daysvalue').visible = false;
                                }
                                if (this.props.form.getFormItemsValue('base', 'term').value != true) {
                                    metanew['controlstyleform'].items.find(e => e.attrcode === 'test').visible = false;
                                }
                                this.props.meta.setMeta(metanew)
                            }
                        }
                    });
                } else {
                    this.props.form.setFormItemsValue('base', { 'bbcontrol2': { value: false } });
                    this.props.form.setFormItemsValue('base', { 'controlattr': { value: 'jkbxr' } });
                    this.props.form.setFormItemsValue('base', { 'controlstyle': { value: '1' } });
                    this.props.form.setFormStatus('base', 'add');
                    this.props.button.setButtonsVisible({ 'Edit': false, 'Save': true, 'Cancel': true });
                }
            }
        )
    }

    setControlVisible = () => {
        // let meta = this.props.meta.getMeta()
        // 找到对应项，也可以用forEach循环、for循环等
        // meta[moduleId].items..itemType = 'input'
        // // 重要！下面那行一定要写
        // this.props.renderItem(moduleType, moduleId, attrcode, null) // 前三个参数，根据模板json填对应值，moduleId是区域id
        // this.props.meta.setMeta(meta)

    }

    render() {
        const { button, form } = this.props;
        let { createForm } = form;
        let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId
        const { createButtonApp } = button;
        return (
            <div>
                <Div areaCode={Div.config.HEADER}>
                    <NCAffix offsetTop={0}>
                        <div className="head">
                            {this.state.isEffect == true ?
                                <NCBackBtn style={{ marginRight: '10px' }} onClick={this.handleClick}></NCBackBtn>
                                : null}
                            {createPageIcon()}
                            {/*"201101JKKJ-0001": "借款控制规则-集团"*/}
                            <div className="title">
                                {multiLang && multiLang.get('201101JKKJ-0001')}
                            </div>
                            <div className="page-button-area">
                                {createButtonApp({ onButtonClick: buttonClick.bind(this), area: 'page_header' })}
                            </div>
                        </div>
                    </NCAffix>
                </Div>
                <Div field="base" areaCode={Div.config.SHOULDER}>
                    <div className="body-area">
                        {createForm('base',
                            { expandArr: ['base', 'controlstyleform', 'advance'] }
                        )}
                    </div>
                </Div>
                <div>
                    {/*"201101JKKJ-0002": "是否确认取消?",*/}
                    {/*"2011-0002": "取消",*/}
                    {/*"2011-0003": "确定",*/}
                </div>
            </div>
        );
    }

    handleClick = () => {
        window.onbeforeunload = null;
        this.props.linkTo('/erm/expenseaccount/loancontrol/list/index.html',
            { pagecode: '201101JKKJ_LIST' }
        );
    }
}

LoanControlCard = createPage({
    mutiLangCode: '2011'
})(LoanControlCard);

ReactDOM.render(<LoanControlCard />, document.querySelector('#app'));