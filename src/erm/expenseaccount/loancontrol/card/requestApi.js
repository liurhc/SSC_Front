import requestApi from "ssccommon/components/requestApi";

import {ajax } from 'nc-lightapp-front';

let requestApiOverwrite = {
    ...requestApi,
    query:(opt) => {
        ajax({
            url: `/nccloud/erm/loancontrol/LoanControlViewAction.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    }
}

export default  requestApiOverwrite;