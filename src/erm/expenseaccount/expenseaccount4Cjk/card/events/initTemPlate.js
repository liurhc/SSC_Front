import {ajax} from 'nc-lightapp-front';
import requestApi from "../requestApi";

export default function (props) {

    props.createUIDom(
        {
            pagecode: "201102CJK",//页面编码
            appcode: props.appcode//小应用编码
        },
        (dataui) => {
            let metas = dataui.template;
            let tempArr = [];
            let searchitem = metas[window.cjkAreaCode.searchArea];
            let headmeta = metas[window.cjkAreaCode.billsArea];
            let bodymeta = metas[window.cjkAreaCode.bodysArea];            
            let bxdata = window.cjkdata;
            let PsndocTreeFilterPath ='nccloud.web.action.erm.ref.sqlbuilder.PsndocTreeGridRef';//人员
            searchitem.items.forEach((item) => {

                if (item.attrcode == 'jkbxr') {
                    item.queryCondition = ()=> {
                        return {tradetype:bxdata.tradetype,pk_org:bxdata.dwbm,org_id:bxdata.dwbm,GridRefActionExt:PsndocTreeFilterPath,DataPowerOperationCode:'fi',/**使用权组**/isDataPowerEnable:'Y'};                
                    }
                }

                 if(item.itemtype == 'refer'){
                     item.isShowUnit=true;
                     // item.visible = false;
                 }
                 if(item.attrcode && item.attrcode=="pk_org"){
                     item.initialvalue = {value:table,display:table}
                 }
                // else{
                 //     item.visible = true;
                 // }
                 tempArr.push(item);
             });
            let meta = {
                ...metas,
                searchArea: {
                    moduletype: 'search',
                    items : tempArr
                },
                head:{
                    moduletype: 'table',
                    items: headmeta.items
                },
                body:{
                    moduletype: 'table',
                    items: bodymeta.items
                }
            }
            props.meta.setMeta(meta);
            let data={
                querycondition:{"conditions":[],"logic":"and"},
                pagecode: window.pageCode.searchPage,
                pageInfo:{
                    "pageIndex":-1,
                    "pageSize":10,
                    "totalPage":"0"
                },
                queryAreaCode:window.cjkAreaCode.searchArea,  //查询区编码
                oid:'0001Z310000000005E4H',  //pub_area 中PK_AREA
                querytype:'tree'
            };

          
                let defaults={szxmid:props.defaultSearch.szxmid?props.defaultSearch.szxmid:[],hbbm:props.defaultSearch.hbbm?props.defaultSearch.hbbm.value:"",paytarget:props.defaultSearch.paytarget?props.defaultSearch.paytarget.value:"",fctno:props.defaultSearch.fctno?props.defaultSearch.fctno.value:""};

                Object.assign(data,defaults);
            


            let bxcontrastGrid = window.bxcontrastGrid;
            let senddata = {'bxcontrastGrid':bxcontrastGrid ,bxdata,data,"pagecode":"20110ETLB_cjk","tepmid":props.meta.getMeta().pageid};
            requestApi.query({
                data:senddata,
                success: (data) => {
                    if(data){
                        this.setTransferBtnDisabled(false);
                        //处理已勾选的情况还款原币金额汇总问题
                        if (data.length > 0) {
                            data.forEach((dataitem) => {
                                let hkybje_sethead=0.00;
                                if(dataitem.body.jk_busitem)
                                for(let num=0;num<dataitem.body.jk_busitem.rows.length;num++){
                                    if(dataitem.body.jk_busitem.rows[num].values.hkybje && dataitem.body.jk_busitem.rows[num].values.hkybje.value!='NaN')
                                    hkybje_sethead+=parseFloat(dataitem.body.jk_busitem.rows[num].values.hkybje.value)
                                }
                                dataitem.head.head.rows[0].values.hkybje.value = hkybje_sethead;
                            });
                        }

                        data && this.props.transferTable.setTransferTableValue(window.cjkAreaCode.billsArea, window.cjkAreaCode.bodysArea, data, 'pk_jkbx', 'pk_busitem');
                        props.setDefaultSelectedJk(data, this.props);
                        
                    }else{
                        this.setTransferBtnDisabled(true);
                        this.props.transferTable.setTransferTableValue(window.cjkAreaCode.billsArea, window.cjkAreaCode.bodysArea, [], 'pk_jkbx', 'pk_busitem');
                    }

                }
            })
    });

}
