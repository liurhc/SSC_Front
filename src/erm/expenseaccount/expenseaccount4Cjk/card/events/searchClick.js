import requestApi from '../requestApi'

export default function searchClick(props, searchVal) {
    if(true){
        let bxdata = window.cjkdata;
        let data={
            querycondition:searchVal==null?{}:searchVal,
            pagecode: window.pageCode.searchPage,
            pageInfo:{
                "pageIndex":-1,
                "pageSize":10,
                "totalPage":"0"
            },
            queryAreaCode:window.cjkAreaCode.searchArea,  //查询区编码
            oid:'0001Z310000000005E4H',  ////pub_area 中PK_AREA
            querytype:'tree'
        };

    
                let defaultSearch={szxmid:props.defaultSearch.szxmid?props.defaultSearch.szxmid:[],hbbm:props.defaultSearch.hbbm?props.defaultSearch.hbbm.value:"",paytarget:props.defaultSearch.paytarget?props.defaultSearch.paytarget.value:"",fctno:props.defaultSearch.fctno?props.defaultSearch.fctno.value:""};

                Object.assign(data,defaultSearch);
        


        let bxcontrastGrid = window.bxcontrastGrid;
        let senddata = {'bxcontrastGrid':bxcontrastGrid ,bxdata,data,"pagecode":"20110ETLB_cjk","tepmid":this.props.meta.getMeta().pageid};
        requestApi.query({
            data:senddata,
            success: (data) => {
                if(data){
                    this.setTransferBtnDisabled(false);
                    data && this.props.transferTable.setTransferTableValue(window.cjkAreaCode.billsArea, window.cjkAreaCode.bodysArea, data, 'pk_jkbx', 'pk_busitem');
                    data && props.setDefaultSelectedJk(data, this.props);
                } else {
                    this.setTransferBtnDisabled(true);
                    this.props.transferTable.setTransferTableValue(window.cjkAreaCode.billsArea, window.cjkAreaCode.bodysArea, [], 'pk_jkbx', 'pk_busitem');
                }

            }
        })
    }
}

