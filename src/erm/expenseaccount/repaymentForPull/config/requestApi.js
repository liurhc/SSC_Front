import requestApi from "ssccommon/components/requestApi";

import {ajax } from 'nc-lightapp-front';

let requestDomain =  '';

let requestApiOverwrite = {
    ...requestApi,
    //单据模板接口
    tpl: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/expenseaccount/HKPullTempletAction.do`,
            data: opt.data,
            success: opt.success
        });
    },
    query:(opt) => {
         ajax({
            url: `${requestDomain}/nccloud/erm/expenseaccount/HkPullQueryBillAction.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    getpageparam:(opt) => {
        ajax({
           url: `${requestDomain}/nccloud/erm/expenseaccount/HKPullGetPageParamAction.do`,
           data: opt.data,
           success: (data) => {
               data = data.data;
               opt.success(data);
           }
       })
   }
}

export default  requestApiOverwrite;