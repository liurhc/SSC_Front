import {ajax,getMultiLang} from 'nc-lightapp-front';
import requestApi from '../requestApi'
import presetVar from '../presetVar';


export default function (props) {

    props.createUIDom(
        {
              pagecode: '201102HKD_card_001',//页面编码
             appcode: '201102HKD'//小应用编码
        },
        (data) => {
            getMultiLang({moduleId: 2011, domainName: 'erm',currentLocale: 'simpchn',callback: (json,status,inlt) => {
            this.multiLangJson = json;
            this.multiLangInlt = json;
            presetVar.pageCode.searchPage=props.getSearchParam("p");
            let metas = data.template;
            let tempArr = [];
            let searchitem = metas[presetVar.areaCode.searchArea];
            let headmeta = metas[presetVar.areaCode.billsArea];
            let bodymeta = metas[presetVar.areaCode.bodysArea];

            let PsndocTreeFilterPath ='nccloud.web.action.erm.ref.sqlbuilder.PsndocTreeGridRef';//人员
            //从后台获取当前登录用户关联人员所属的财务组织
            requestApi.getpageparam({
                data:{},
                success: (pk_org) => {
                    searchitem.items.forEach((item) => {
                        if (item.attrcode == 'jkbxr') {
                            item.queryCondition = ()=> {
                                //return {pk_org: pk_org[0]};
                                return {tradetype:'2647',pk_org:pk_org[0],org_id:pk_org[0],GridRefActionExt:PsndocTreeFilterPath,DataPowerOperationCode:'fi',/**使用权组**/isDataPowerEnable:'Y'};                
                            }
                        }
                    });
                    headmeta.items.forEach((item) => {
                        delete item.width;
                    });
                    bodymeta.items.forEach((item) => {
                        delete item.width;
                        if(item.attrcode == 'defitem50'){
                            if(this.dztz.amount && this.dztz.amount>0){
                                item.visible = true;
                            }else{
                                item.visible = false;
                            }
                        }
                        
                    });
                    let meta = {
                        ...metas,
                        searchArea: {
                            moduletype: 'search',
                            items : searchitem.items
                        },
                        head:{
                            moduletype: 'table',
                            items: headmeta.items
                        },
                        body:{
                            moduletype: 'table',
                            items: bodymeta.items
                        }
                    }
                    props.meta.setMeta(meta);
    
                }
            });

            //加载默认数据
            let param={
                querycondition:{},
                pagecode: '201102HKD_card_001',
                dztz_amount:this.dztz.amount,
                pageInfo:{
                    "pageIndex":-1,
                    "pageSize":10,
                    "totalPage":"0"
                },
                queryAreaCode:presetVar.areaCode.searchArea,  //查询区编码
                oid:'0001Z3100000000067Z7',  //查询模板id，手工添加在界面模板json中，放在查询区，后期会修改
                querytype:'tree'
            };

            requestApi.query({
                data:param,
                success: (jkdata) => {
                    
                    
                    jkdata && props.transferTable.setTransferTableValue(presetVar.areaCode.billsArea, presetVar.areaCode.bodysArea, jkdata, 'pk_jkbx', 'pk_busitem');
    
                    let hasCache = props.transferTable.hasCache('ssc.erm.repayment.pull');
                    
                    if(this.dztz.amount && this.dztz.amount>0){
                        jkdata && this.setRowCheckedByDZTZ(jkdata);
                    }
                    
                    
                    console.log(566, hasCache)
                    
                    
    
                }
            })
        }})
        }
    );
}
