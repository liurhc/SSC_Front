import requestApi from '../requestApi'
import presetVar from '../presetVar';
import { toast } from "nc-lightapp-front"

export default function searchClick(props, searchVal) {
    if(true){
        let data={
            querycondition:searchVal,
            // pagecode: presetVar.pageCode.searchPage,
            dztz_amount:this.dztz.amount,
            pagecode: '201102HKD_card_001',
            pageInfo:{
                "pageIndex":-1,
                "pageSize":10,
                "totalPage":"0"
            },
            queryAreaCode:presetVar.areaCode.searchArea,  //查询区编码
            oid:'0001Z3100000000067Z7',  //查询模板id，手工添加在界面模板json中，放在查询区，后期会修改
            querytype:'tree'
        };
        requestApi.query({
            data:data,
            success: (data) => {
                
                toast({
                    duration: 3,
                    color: 'success',
                    title: "查询成功",
                    content: `已查询到${data ? data.length : 0}条数据。`
                })                

                data && this.props.transferTable.setTransferTableValue(presetVar.areaCode.billsArea, presetVar.areaCode.bodysArea, data, 'pk_jkbx', 'pk_busitem');
                !data && this.props.transferTable.setTransferTableValue(presetVar.areaCode.billsArea, presetVar.areaCode.bodysArea, [], 'pk_jkbx', 'pk_busitem');
                let hasCache = this.props.transferTable.hasCache('ssc.erm.repayment.pull');
                if(this.dztz.amount && this.dztz.amount>0){
                    data && this.setRowCheckedByDZTZ(data);
                }
                
                console.log(566, hasCache)
                
                

            }
        })
    }
}

