import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {connect, Provider} from 'react-redux';
import store from './store/store';


import TreeTable from './TreeTable';

ReactDOM.render((<Provider store={store}><TreeTable /></Provider>)
    , document.querySelector('#app'));