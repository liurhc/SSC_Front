import requestApi from "ssccommon/components/requestApi";
import {ajax} from 'nc-lightapp-front';


let requestDomain = '';
let requestApiOverwrite = {
    ...requestApi,
    //单据模板接口

    save: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/expenseaccount/ReimrulerSaveRuledataAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    getTemplet: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/expenseaccount/ReimrulerTempletAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    getTreeData: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/expenseaccount/ReimrulerGetTreedataAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    getTableData: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/expenseaccount/ReimrulerGetTabledataAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    getSettingData: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/expenseaccount/ReimrulerGetSettingDataAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    saveSetting: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/expenseaccount/ReimrulerSaveSettingDataAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    getControlSettingData: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/expenseaccount/ReimrulerGetControlSettingdataAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    saveControlSetting: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/expenseaccount/ReimrulerSaveControlSettingDataAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    saveQuickCopy: (opt) => {
    ajax({
        url: opt.url || `${requestDomain}/nccloud/erm/expenseaccount/ReimrulerSaveQuickCopyDataAction.do`,
        data: opt.data,
        success: (data) => {
            opt.success(data);
        }
    })
    },
    savedimensionCopy: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/expenseaccount/ReimrulerSaveDimensionCopyDataAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    beforeValueChange: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/expenseaccount/ReimrulerBeforeValueChangeAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    afterValueChange: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/expenseaccount/ReimrulerAfterValueChangeAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    queryMetaParam:(opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscrp/rpbill/FieldMapQryMetaParamAction.do`,
            data: opt.data,
            success: (data) =>{
                opt.success(data);
            }
        })
    },
}

export default requestApiOverwrite;
