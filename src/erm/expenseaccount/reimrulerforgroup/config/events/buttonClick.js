import { ajax, toast, promptBox, formDownload, getMultiLang } from 'nc-lightapp-front';
import requestApi from '../requestApi';
import Utils from '../../../../public/utils';
import tableExtendButtonClick from './tableExtendButtonClick';
import { setTableExtendCol } from "ssccommon/components/profile";

/**
 * 修改
 * @param {*} that
 */
let afterEdit = (that, data) => {
    that.props.syncTree.setNodeDisable("tree1", true);
    that.setState({ refreshStatus: 'nobrowse' });
    that.setState({ treeSearchDisabled: true });
    that.setState({ isEdit: true });
    that.props.button.setButtonDisabled(['DeleteS'], true);
    that.props.button.setButtonVisible(['Add', 'DeleteS', 'Save', 'Cancel', 'Import'], true);
    that.props.button.setButtonVisible(['Export', 'Edit', 'Setting', 'ControlSetting', 'QuickCopy'], false);
    that.props.editTable.setStatus('reimrule', 'edit');
    ctrlEditTableArea: ['reimrule'] //受按钮控制的表格区域

    //初始化导入相关代码
    let toastArr = [];
    let pk_tradetype = that.props.syncTree.getSelectNode("tree1").refcode;
    let excelimportconfig = Utils.getImportConfig("erm", pk_tradetype, toastArr.length, "reimrulerforgroup", (resultinfo) => {
        if (resultinfo == 'beginUpload') {
            toastArr.push(1);
            //that.props.modal.show('importModal', {
            //content: <ProcessArr toastArr={toastArr} upParams={{"moduleName":'erm',"billType":'263X'}} />
            //});
        } else if (resultinfo.success) {
            let olddata = that.props.editTable.getAllData("reimrule").rows;
            let len = 0;
            olddata.find((row) => {
                if (row.status == 0) {
                    len++;
                }
            })
            let exceldata = resultinfo.data.reimrule;
            exceldata.rows.find((row) => {
                that.props.editTable.addRow('reimrule', len, true, row.values);
                len++;
            });
        }
    });
    that.props.button.setUploadConfig('Import', excelimportconfig);
}

//导出
let exportBtnEventConfig = {
    click(props) {
        getMultiLang({
            moduleId: 2011, domainName: 'erm', currentLocale: 'zh-CN', callback: (json) => {
                let reimrulerdata = props.editTable.getAllData("reimrule");
                if (reimrulerdata != undefined && reimrulerdata.rows != undefined && reimrulerdata.rows.length == 0) {
                    toast({ content: json['201101BXBJ-0006'], color: 'warning' });//'报销标准数据为空，不能导出!'
                } else {
                    let data = {
                        "pk_org": this.state.pk_org,
                        "pk_tradetype": props.syncTree.getSelectNode("tree1").refcode,
                        "src": "reimrulerforgroup"
                    }
                    formDownload({
                        params: data,
                        url: '/nccloud/erm/expenseaccount/ReimrulerExportAction.do',
                        enctype: 2
                    })
                }
            }
        })
    }
}

//保存
let saveBtnEventConfig = {
    click(props) {
        getMultiLang({
            moduleId: 2011, domainName: 'erm', currentLocale: 'zh-CN', callback: (json) => {
                let data = props.editTable.getAllData("reimrule");
                let pk_tradetype = props.syncTree.getSelectNode("tree1").refcode;
                let pk_org = this.state.pk_org;
                let src = 'reimrulerforgroup';
                if (data.rows.length == 0) {
                    toast({ content: json['201101BXBJ-0007'], color: 'warning' });//'保存数据不能为空!'
                    return;
                }
                let controlItem = {};
                let meta = props.meta.getMeta();
                meta['reimrule'].items.forEach((item) => {
                    if (item.required && item.visible && item.attrcode != 'amount') {
                        controlItem = item.attrcode;
                    }
                });
                let newrows = [];
                data.rows.forEach((row)=>{
                    if(row.values['amount'].value != '' && row.values[controlItem].value != ''){
                        newrows.push(row);
                    }
                })
                data.rows = newrows;
                let bdata = {
                    head: data,
                    userjson: "{'pk_tradetype':'" + pk_tradetype + "','pk_org':'" + pk_org + "','src':'" + src + "'}"
                }
                requestApi.save({
                    data: bdata,
                    success: (resData) => {
                        props.editTable.setStatus('reimrule', 'browse')

                        if (resData == null || resData.data == null) {
                            props.editTable.setTableData('reimrule', { rows: [] })
                        } else {
                            props.editTable.setTableData('reimrule', resData.data.reimrule)
                        }
                        props.button.setButtonsVisible(['Edit', 'Setting', 'ControlSetting', 'QuickCopy', 'Export'], true);
                        props.button.setButtonsVisible(['Cancel', 'Add', 'Save', 'DeleteS', 'Import'], false);
                        props.syncTree.setNodeDisable("tree1", false);
                        this.setState({ treeSearchDisabled: false });
                        this.setState({ refreshStatus: 'browse' });
                        this.pubMessage.saveSuccess();
                        //toast({duration:2,color:"success",content:json['201101BXBJ-0012']});
                    }
                })
            }
        })
    }
}
/**
 * 取消
 * @param {*} that
 */
let afterCancel = (that) => {
    // 取消编辑态
    that.props.syncTree.setNodeDisable("tree1", false);
    that.setState({ treeSearchDisabled: false });
    that.props.editTable.cancelEdit('reimrule');
    that.setState({ isEdit: false });
    that.setState({ refreshStatus: 'browse' });
    that.props.button.setButtonVisible(['Edit', 'Setting', 'ControlSetting', 'QuickCopy', 'Export'], true);
    that.props.button.setButtonVisible(['Import', 'Cancel', 'Add', 'Save', 'DeleteS'], false);
}
/**
* 配置
* @param {*} that
*/
let afterSetting = (that) => {
    that.props.createUIDom(
        {
            pagecode: '201101BXBJ_Setting',//页面编码
            appcode: '201101BXBJ'//小应用编码
        },
        function (data) {
            let billbeanid;
            let pk_tradetype = that.props.syncTree.getSelectNode("tree1").refcode;
            if (pk_tradetype != undefined && pk_tradetype.startsWith('261')) {
                billbeanid = 'e3167d31-9694-4ea1-873f-2ffafd8fbed8';
            }
            if (pk_tradetype != undefined && pk_tradetype.startsWith('263')) {
                billbeanid = 'e0499b58-c604-48a6-825b-9a7e4d6dacca';
            }
            if (pk_tradetype != undefined && pk_tradetype.startsWith('264')) {
                billbeanid = 'd9b9f860-4dc7-47fa-a7d5-7a5d91f39290';
            }
            let meta = that.props.meta.getMeta();
            meta['reimdimentionbody'] = data.template[window.areaCode.settingArea];
            meta['reimdimentionbody'].items.find((item) => {
                delete item.width;
                if (item.attrcode === "billref") {
                    item.refcode = 'uap/refer/riart/mdTreeByIdRef/index.js';
                    item.isTreelazyLoad = true;
                    item.queryCondition = () => {
                        return { beanId: billbeanid, TreeRefActionExt: '' }
                    };
                }
            });

            that.props.meta.addMeta(meta);

            that.setState({ showConfigModal: true });

            // 设置操作按钮的事件
            setTableExtendCol(that.props, meta, [{
                areaId: window.areaCode.settingArea,
                btnAreaId: 'listbtnarea',
                buttonVisible: (record, index) => {
                    return ['SettingTop', 'Up', 'Down', 'SettingBottom']
                },
                width: '200px',
                buttonLimit: 4,
                onButtonClick: tableExtendButtonClick.bind(that)
            }]);

            let pk_org = that.state.pk_org;
            let bdata = {
                pk_tradetype: pk_tradetype,
                pk_org: pk_org,
                src: "reimrulerforgroup"
            };
            let resultData = {
                rows: []
            };
            requestApi.getSettingData({
                data: bdata,
                success: (resData) => {
                    if (resData.data == null || resData.data.reimdimentionbody == undefined) {
                        that.props.editTable.setTableData('reimdimentionbody', resultData);
                    } else {
                        let rows = resData.data.reimdimentionbody.rows;
                        for (let row of rows) {
                            if (row.values.referential.display == undefined) {
                                row.values.referential["display"] = row.values.referential.value
                            }
                        }
                        that.props.editTable.setStatus('reimdimentionbody', 'edit');
                        that.props.editTable.setTableData('reimdimentionbody', resData.data.reimdimentionbody);
                        that.props.button.setButtonDisabled(['DeleteSetting'], true);
                    }

                }
            })
        })
}
/**
 * 控制配置
 * @param {*} that
 */
let controlSetting = (that) => {
    that.props.createUIDom(
        {
            pagecode: '201101BXBJ_ContrlSetting',//页面编码
            appcode: '201101BXBJ'//小应用编码
        },
        function (data) {
            let billbeanid;
            let pk_tradetype = that.props.syncTree.getSelectNode("tree1").refcode;
            if (pk_tradetype != undefined && pk_tradetype.startsWith('261')) {
                billbeanid = 'e3167d31-9694-4ea1-873f-2ffafd8fbed8';
            }
            if (pk_tradetype != undefined && pk_tradetype.startsWith('263')) {
                billbeanid = 'e0499b58-c604-48a6-825b-9a7e4d6dacca';
            }
            if (pk_tradetype != undefined && pk_tradetype.startsWith('264')) {
                billbeanid = 'd9b9f860-4dc7-47fa-a7d5-7a5d91f39290';
            }
            let meta = that.props.meta.getMeta();
            meta['controlsetting'] = data.template[window.areaCode.controlSettingArea];
            meta['controlsetting'].items.forEach((item) => {
                delete item.width;
                if (item.attrcode === "showitem" || item.attrcode === "controlitem") {
                    item.refcode = 'uap/refer/riart/mdTreeByIdRef/index.js';
                    item.isTreelazyLoad = true;
                    item.queryCondition = () => {
                        return { beanId: billbeanid, TreeRefActionExt: '' }
                    };
                }
            });


            that.setState({ showControlConfigModal: true });
            //let pk_org = that.state.pk_org;
            let reqData = {
                pk_tradetype: pk_tradetype,
                pk_org: null,
                src: "reimrulerforgroup"
            };
            let resultData = {
                rows: []
            };
            requestApi.getControlSettingData({
                data: reqData,
                success: (data) => {
                    if (data.data == undefined || data.data.reimrule == undefined) {
                        that.props.editTable.setTableData('controlsetting', resultData);
                        let centcontrolitemcode = data.data.centcontrolitemcode;
                        let centcontrolitemname = data.data.centcontrolitemname;
                        meta['controlsetting'].items.forEach((item) => {
                            if (item.attrcode == centcontrolitemcode) {
                                item.disabled = true;
                                item.visible = true;
                                item.label = centcontrolitemname;
                            }
                        });
                        that.props.meta.addMeta(meta);
                    } else {
                        that.props.editTable.setTableData('controlsetting', data.data.reimrule);
                        let centcontrolitemcode = JSON.parse(data.data.userjson).centcontrolitemcode;//核心控制项编码
                        let centcontrolitemname = JSON.parse(data.data.userjson).centcontrolitemname;//核心控制项名字
                        meta['controlsetting'].items.forEach((item) => {
                            if (item.attrcode == centcontrolitemcode) {
                                item.disabled = true;
                                item.visible = true;
                                item.label = centcontrolitemname;
                            }
                        });
                        that.props.meta.addMeta(meta);
                    }
                    that.props.editTable.setStatus('controlsetting', 'edit');
                }
            })
        })
}

//删除
let deleteBtnEventConfig = {
    click: (props) => {
        getMultiLang({
            moduleId: 2011, domainName: 'erm', currentLocale: 'zh-CN', callback: (json) => {
                let data = props.editTable.getCheckedRows('reimrule');
                let arr = data.map(item => item.index);

                if (data.length > 0) {
                    props.editTable.deleteTableRowsByIndex('reimrule', arr);
                    toast({ content: json['201101BXBJ-0010'], color: 'success' });//'删除成功'
                //    promptBox({
                //        color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                //        title: json['201101BXBJ-0008'],//"提示信息",                // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输
                //        content: json['201101BXBJ-0009'],//'确定要删除？' ,            // 提示内容,非必输
                //        noFooter: false,                // 是否显示底部按钮(确定、取消),默认显示(false),非必输
                //        noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
                //        beSureBtnName: json['2011-0003'],//"确定",          // 确定按钮名称, 默认为"确定",非必输
                //        cancelBtnName: json['2011-0002'],//"取消" ,          // 取消按钮名称, 默认为"取消",非必输
                //        beSureBtnClick: function () {
                //            props.editTable.deleteTableRowsByIndex('reimrule', arr);
                //            toast({ content: json['201101BXBJ-0010'], color: 'success' });//'删除成功'
                //        }//,   // 确定按钮点击调用函数,非必输
                //        //cancelBtnClick: functionCancel  // 取消按钮点击调用函数,非必输
                //    })
                }
            }
        })
    }
}
//新增
let addBtnEventConfig = {
    ctrlEditTableArea: ['reimrule']
}
//配置-新增
let addModalBtnEventConfig = {
    ctrlEditTableArea: ['reimdimentionbody'],
    ActionCode: 'Add'
}
//配置-删除
let deleteModalBtnEventConfig = {
    click: (props) => {
        getMultiLang({moduleId: 2011, domainName: 'erm', currentLocale: 'zh-CN', callback: (json) => {
        let data = props.editTable.getCheckedRows('reimdimentionbody');
        let arr = data.map(item => item.index);

        if (data.length > 0) {
            promptBox({
                color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                title: json['2011-0004'],//"删除",                // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输
                content: json['201101BXBJ-0009'],//'确定要删除？',            // 提示内容,非必输
                noFooter: false,                // 是否显示底部按钮(确定、取消),默认显示(false),非必输
                noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
                beSureBtnName: json['2011-0003'],//"确定",          // 确定按钮名称, 默认为"确定",非必输
                cancelBtnName: json['2011-0002'],//"取消",          // 取消按钮名称, 默认为"取消",非必输
                beSureBtnClick: () => {
                    props.editTable.deleteTableRowsByIndex('reimdimentionbody', arr);
                    toast({ content: json['201101BXBJ-0010'],/*'删除成功',*/ color: 'success' });
                    props.button.setButtonDisabled(['DeleteSetting'], true);

                }//,   // 确定按钮点击调用函数,非必输
                //cancelBtnClick: functionCancel  // 取消按钮点击调用函数,非必输
            })
        }
    }})
    }
}
//配置快速复制
let querySetingCopy = (that) => {
    that.props.createUIDom(
        {
            pagecode: '201101BXBJ_QuickCopy',//页面编码
            appcode: '201101BXBJ'//小应用编码
        },
        function (data) {
            let meta = that.props.meta.getMeta();
            meta['quickcopy'] = data.template[window.areaCode.quickCopyArea];
            meta['quickcopy'].items.find((item) => {
                delete item.width;
                if (item.attrcode === "pk_org") {
                    item.queryCondition = () => {
                        return {
                            TreeRefActionExt: 'nccloud.web.action.erm.ref.sqlbuilder.BusinessUnitTreeRefBuilder',
                            appcode: that.props.getSearchParam('c')
                        };

                    }
                }
                if (item.attrcode == 'pk_billtypeid') {
                    item.queryCondition = () => {
                        return { parentbilltype: "261X,263X,264X" };
                    }
                }
            });
            that.props.meta.addMeta(meta);
            that.props.form.setFormStatus('quickcopy', 'edit');
        })
    that.setState({ quickcopypage: 'inner' });
    that.setState({ showQuickCopyModal: true });
}
//配置-保存
let saveModalBtnEventConfig = {
    click(props) {
        getMultiLang({
            moduleId: 2011, domainName: 'erm', currentLocale: 'zh-CN', callback: (json) => {
                this.props.editTable.filterEmptyRows('reimdimentionbody', []);
                let data = this.props.editTable.getAllData("reimdimentionbody");
                let pk_tradetype = this.props.syncTree.getSelectNode("tree1").refcode;
                let pk_org = this.state.pk_org;
                let src = 'reimrulerforgroup';

                let bdata = {
                    head: data,
                    userjson: "{'pk_tradetype':'" + pk_tradetype + "','pk_org':'" + pk_org + "','src':'" + src + "'}"
                }
                requestApi.saveSetting({
                    data: bdata,
                    success: (resData) => {
                        if (resData.data.success == 'false' || resData.data.success == false) {
                            let message = resData.data.message == undefined || resData.data.message == '' ? json['201101BXBJ-0011'] : resData.data.message;//'保存失败!'
                            toast({ content: message, color: 'danger' });
                        } else {
                            let meta = this.props.meta.getMeta();
                            let newmeta = {
                                ...resData.data.template,
                            };
                            newmeta['reimdimentionbody'] = meta['reimdimentionbody'];
                            newmeta['controlsetting'] = meta['controlsetting'];
                            newmeta['quickcopy'] = meta['quickcopy'];
                            newmeta['formrelation'] = null;
                            newmeta['reimrule'].items.forEach((item) => {
                                if (item.attrcode == "pk_currtype") {
                                    item.initialvalue = {
                                        value: resData.data.currtype[0],
                                        display: resData.data.currtype[1]
                                    };
                                }
                                if (item.visible == true) {
                                    if (item.attrcode == 'pk_reimtype') {
                                        item.width = '180px';
                                    } else if (item.attrcode == 'pk_deptid') {
                                        item.width = '160px';
                                        item.onlyLeafCanSelect = true;
                                        item.refcode = 'uapbd/refer/org/DeptTreeRef/index';
                                        item.isShowUnit = true;
                                        item.unitProps = {
                                            refType: 'tree',
                                            refName: json['201101BXBJ-0018'],//'业务单元'
                                            refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                                            rootNode: { refname: json['201101BXBJ-0018'],/*'业务单元'*/ refpk: 'root' },
                                            placeholder: json['201101BXBJ-0018'],//'业务单元'
                                            queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                                            treeConfig: { name: [json['201101BXBJ-0019'],/*编码*/ json['201101BXBJ-0019']/*名称*/], code: ['refcode', 'refname'] },
                                            isMultiSelectedEnabled: false,
                                            //unitProps:unitConf,
                                            isShowUnit: false
                                        };
                                    } else {
                                        item.width = '140px';
                                    }
                                }
                            });
                            this.props.meta.setMeta(newmeta);
                            this.setState({ showConfigModal: false });
                            toast({ content: json['201101BXBJ-0012'], color: 'success' });//'保存成功!'
                        }
                    }
                })
            }
        })
    }
}

//配置-取消
let cancelModalBtnEventConfig = {
    click(props) {
        this.props.editTable.setTableData('showConfigModal', { rows: [] });
        this.setState({ showConfigModal: false });
    }
}

//控制配置-保存
let saveControlModalBtnEventConfig = {
    click(props) {
        let multiLang = this.props.MutiInit.getIntl(2011);
        let data = this.props.editTable.getAllData("controlsetting");
        let pk_tradetype = this.props.syncTree.getSelectNode("tree1").refcode;
        let pk_org = this.state.pk_org;
        let src = 'reimrulerforgroup';

        let bdata = {
            head: data,
            userjson: "{'pk_tradetype':'" + pk_tradetype + "','pk_org':'" + pk_org + "','src':'" + src + "'}"
        }
        requestApi.saveControlSetting({
            data: bdata,
            success: (resData) => {
                if(resData.data == undefined){
                    toast({ content: multiLang.get('201101BXBJ-0014'), color: 'warning' });//'!'
                }else{
                    this.setState({ showControlConfigModal: false });
                }
            }
        })
    }
}
//控制配置-取消
let cancelControlModalBtnEventConfig = {
    click(props) {
        this.props.editTable.setTableData('showControlConfigModal', { rows: [] });
        this.setState({ showControlConfigModal: false });
    }
}
/**
 * 快速复制
 * @param {*} that
 */
let quickCopy = (that) => {
    that.props.createUIDom(
        {
            pagecode: '201101BXBJ_QuickCopy',//页面编码
            appcode: '201101BXBJ'//小应用编码
        },
        function (data) {
            let meta = that.props.meta.getMeta();
            meta['quickcopy'] = data.template[window.areaCode.quickCopyArea];
            meta['quickcopy'].items.find((item) => {
                delete item.width;
                if (item.attrcode === "pk_org") {
                    item.queryCondition = () => {
                        return {
                            TreeRefActionExt: 'nccloud.web.action.erm.ref.sqlbuilder.BusinessUnitTreeRefBuilder',
                            appcode: that.props.getSearchParam('c')
                        };

                    }
                }
                if (item.attrcode == 'pk_billtypeid') {
                    item.queryCondition = () => {
                        return { parentbilltype: "261X,263X,264X" };
                    }
                }
            });
            that.props.meta.addMeta(meta);
            that.props.form.setFormStatus('quickcopy', 'edit');
        })
    that.setState({ quickcopypage: 'outer' });
    that.setState({ showQuickCopyModal: true });
}

//快速复制-确定
let saveQuickCopyModalBtnEventConfig = {
    click(props) {
        let multiLang = this.props.MutiInit.getIntl(2011);
        let data = this.props.form.getAllFormValue("quickcopy");
        let pk_tradetype = this.props.syncTree.getSelectNode("tree1").refcode;
        let pk_org = this.state.pk_org;
        let src = 'reimrulerforgroup';
        let quickcopypage = this.state.quickcopypage;
        if(data && data.rows && data.rows[0].values){
            let Ispk_orgnull = data.rows[0].values.pk_org.value==null||data.rows[0].values.pk_org.value.length==0;
            let Ispk_billtypnull = data.rows[0].values.pk_billtypeid.value==null||data.rows[0].values.pk_billtypeid.value.length==0;
            if(Ispk_orgnull && Ispk_billtypnull){
                toast({ content: multiLang.get('201101BXBJ-0015'), color: 'warning' });//【业务单元】【交易类型】不能为空！
                return;
            }else if(Ispk_orgnull){
                toast({ content: multiLang.get('201101BXBJ-0016'), color: 'warning' });//【业务单元】不能为空！
                return;
            }else if(Ispk_billtypnull){
                toast({ content: multiLang.get('201101BXBJ-0017'), color: 'warning' });//【交易类型】不能为空！
                return;
            }
        }
        
        let bdata = {
            head: data,
            userjson: "{'pk_tradetype':'" + pk_tradetype + "','pk_org':'" + pk_org + "','src':'" + src + "'}"
        }
        let _this = this;
        //报销标准快速复制保存
        if (quickcopypage == 'outer') {
            requestApi.saveQuickCopy({
                data: bdata,
                success: (resData) => {
                    this.setState({ showQuickCopyModal: false });
                    let result = JSON.parse(resData.data);
                    let message = result.message;
                    let success = result.success;
                    if (success == 'true') {
                        toast({ content: message, color: 'success' });
                        _this.props.form.EmptyAllFormValue('quickcopy');
                    } else {
                        toast({ content: message, color: 'warning' });
                    }
                }
            })
        } else {//报销标准维度快速复制保存
            requestApi.savedimensionCopy({
                data: bdata,
                success: (resData) => {
                    this.setState({ showQuickCopyModal: false });
                    let result = JSON.parse(resData.data);
                    let message = result.message;
                    let success = result.success;
                    if (success == 'true') {
                        toast({ content: message, color: 'success' });
                        _this.props.form.EmptyAllFormValue('quickcopy');
                    } else {
                        toast({ content: message, color: 'warning' });
                    }
                }
            })
        }
    }
}
//快速复制-取消
let cancelQuickCopyModalBtnEventConfig = {
    click(props) {
        this.props.editTable.setTableData('showQuickCopyModal', { rows: [] });
        this.setState({ showQuickCopyModal: false });
    }
}
/**
 * 树节点选择事件
 */
function treeSelectEvent(key, item) {

    let multiLang = this.props.MutiInit.getIntl(2011);
    let resultData = {
        rows: []
    };
    let tableStatus = this.props.editTable.getStatus('reimrule');
    if (item.refcode == '261X' || item.refcode == '263X' || item.refcode == '264X' || tableStatus == 'edit') {
        this.props.editTable.setTableData('reimrule', resultData);
        this.props.button.setButtonsVisible(['Cancel', 'DeleteS', 'Add', 'Import', 'Save', 'Edit', 'Setting', 'ControlSetting', 'QuickCopy', 'Export'], false);
    } else {
        console.log('点击树节点', key, item)
        //设置表格数据
        let reqData = {
            pk_tradetype: item.refcode,
            pk_org: this.state.pk_org,
            src: "reimrulerforgroup"
        };
        let meta = this.props.meta.getMeta();
        requestApi.getTableData({
            data: reqData,
            success: (data) => {
                this.setState({ pk_tradetype: item.refcode })
                let newmeta = {
                    ...data.data.template,
                };
                newmeta['reimdimentionbody'] = meta['reimdimentionbody'];
                newmeta['controlsetting'] = meta['controlsetting'];
                newmeta['quickcopy'] = meta['quickcopy'];
                newmeta['formrelation'] = null;
                newmeta['reimrule'].items.forEach((item) => {

                    if (item.attrcode == "pk_currtype") {
                        item.initialvalue = {
                            value: data.data.currtype[0],
                            display: data.data.currtype[1]
                        };
                    }
                    if (item.visible == true) {
                        if (item.attrcode == 'pk_reimtype') {
                            item.width = '180px';
                        } else if (item.attrcode == 'pk_deptid') {
                            item.width = '160px';
                            item.width = '160px';
                            item.onlyLeafCanSelect = true;
                            item.refcode = 'uapbd/refer/org/DeptTreeRef/index';
                            item.isShowUnit = true;
                            item.unitProps = {
                                refType: 'tree',
                                refName: multiLang.get('201101BXBJ-0018'),//'业务单元'
                                refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                                rootNode: { refname: multiLang.get('201101BXBJ-0018'),/*'业务单元'*/ refpk: 'root' },
                                placeholder: multiLang.get('201101BXBJ-0018'),//'业务单元'
                                queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                                treeConfig: { name: [multiLang.get('201101BXBJ-0019'),/*'编码'*/ multiLang.get('201101BXBJ-0020')/*'名称'*/], code: ['refcode', 'refname'] },
                                isMultiSelectedEnabled: false,
                                //unitProps:unitConf,
                                isShowUnit: false
                            };
                        } else {
                            item.width = '140px';
                        }
                    }
                });
                this.props.meta.setMeta(newmeta);

                if (data.data.grid == undefined) {
                    this.props.editTable.setTableData('reimrule', resultData);
                } else {
                    this.props.editTable.setTableData('reimrule', data.data.grid.reimrule);
                }
                this.props.button.setButtonsVisible(['Export', 'Edit', 'Setting', 'ControlSetting', 'QuickCopy'], true);
                this.props.button.setButtonsVisible(['Cancel', 'Save', 'Import', 'Add', 'DeleteS'], false);
                this.setState({ refreshStatus: 'browse' });

            }
        })
    }
}

export default { exportBtnEventConfig, quickCopy, controlSetting, afterSetting, afterCancel, saveBtnEventConfig, addBtnEventConfig, treeSelectEvent, afterEdit, deleteBtnEventConfig, saveModalBtnEventConfig, cancelModalBtnEventConfig, deleteModalBtnEventConfig, querySetingCopy, addModalBtnEventConfig, saveControlModalBtnEventConfig, cancelControlModalBtnEventConfig, saveQuickCopyModalBtnEventConfig, cancelQuickCopyModalBtnEventConfig }


