let tplMock = {

    button: [
        {
            "id": "0001A41000000006J5B1",
            "type": "button_main",
            "key": "Edit",
            "title": "修改",
            "area": "page_header",
            "children": []
        },
        {
            "id": "0001A41000000006J5B2",
            "type": "button_main",
            "key": "Setting",
            "title": "配置",
            "area": "page_header",
            "children": []
        },
        {
            "id": "0001A41000000006J5B3",
            "type": "button_main",
            "key": "ControlSetting",
            "title": "控制配置",
            "area": "page_header",
            "children": []
        },
        {
            "id": "0001A41000000006J5B4",
            "type": "button_main",
            "key": "QuickCopy",
            "title": "快速复制",
            "area": "page_header",
            "children": []
        },
        {
            "id": "0001A41000000006J5B5",
            "type": "button_main",
            "key": "Import",
            "title": "导入",
            "area": "page_header",
            "children": []
        },
        {
            "id": "0001A41000000006J5B6",
            "type": "button_main",
            "key": "Export",
            "title": "导出",
            "area": "page_header",
            "children": []
        },
        {
            "id": "0001A41000000006J5B7",
            "type": "button_main",
            "key": "Save",
            "title": "保存",
            "area": "page_header",
            "children": []
        },
        {
            "id": "0001A41000000006J5B8",
            "type": "button_main",
            "key": "Cancel",
            "title": "取消",
            "area": "page_header",
            "children": []
        },
        {
            "id": "0001A4100000000622J5B9",
            "type": "button_main",
            "key": "Add",
            "title": "新增",
            "area": "body-shoulder",
            "children": []
        },
        {
            "id": "0001A4100000000622J5B10",
            "type": "button_main",
            "key": "DeleteS",
            "title": "删除",
            "area": "body-shoulder",
            "children": []
        },
        {
            "id": "0001A41000000006J5B11",
            "type": "button_secondary",
            "key": "Delete",
            "title": "删除",
            "area": "reimrule",
            "children": []
        },
        {
            "id": "0001A41000000006J5B12",
            "type": "button_main",
            "key": "AddSetting",
            "title": "新增",
            "area": "setting_header",
            "children": []
        },
        {
            "id": "0001A41000000006J5B13",
            "type": "button_main",
            "key": "DeleteSetting",
            "title": "删除",
            "area": "setting_header",
            "children": []
        },
        {
            "id": "0001A41000000006J5B13",
            "type": "button_main",
            "key": "QueryCopySetting",
            "title": "快速复制",
            "area": "setting_header",
            "children": []
        },
        {
            "id": "0001ZG10000000000X7U",
            "type": "button_main",
            "key": "Up",
            "title": "上移",
            "area": 'listbtnarea',
            "children": []
        },
        {
            "id": "0001ZG10000000000X7D",
            "type": "button_main",
            "key": "Down",
            "title": "下移",
            "area": 'listbtnarea',
            "children": []
        },
        {
            "id": "0001ZG10000000000X7T",
            "type": "button_main",
            "key": "SettingTop",
            "title": "置顶",
            "area": 'listbtnarea',
            "children": []
        },
        {
          "id": "0001ZG10000000000X7G",
          "type": "button_main",
          "key": "SettingBottom",
          "title": "置底",
          "area": 'listbtnarea',
          "children": []
        }
    ]
}

export default tplMock;