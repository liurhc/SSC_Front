import { ajax, base } from 'nc-lightapp-front';
import requestApi from '../requestApi';


export default function beforeEvent(props, moduleId, item, index, value, record) {
    switch (moduleId) {
        case 'reimrule':
            switch (item.attrcode) {
                // case 'pk_deptid':
                //     let meta = props.meta.getMeta();
                //     meta['reimrule'].items.forEach((item) => {
                //         if (item.attrcode == 'pk_deptid') {
                //             item.onlyLeafCanSelect = true;
                //             item.refcode = 'uapbd/refer/org/DeptTreeRef/index';
                //             item.isShowUnit = true;
                //             item.unitProps = {
                //                 refType: 'tree',
                //                 refName: '业务单元',
                //                 refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                //                 rootNode: { refname: '业务单元', refpk: 'root' },
                //                 placeholder: "业务单元",
                //                 queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                //                 treeConfig: { name: ['编码', '名称'], code: ['refcode', 'refname'] },
                //                 isMultiSelectedEnabled: false,
                //                 //unitProps:unitConf,
                //                 isShowUnit: false
                //             };
                //         }
                //     });
                //     break;
            }
            break;
        case 'reimdimentionbody':
            switch (item.attrcode) {
                case 'referential':
                    let meta = props.meta.getMeta();
                    let datatype = record.values.datatype.value
                    if (datatype == undefined || datatype == null) {
                        break;
                    }

                    if (record.values.datatype.value.startsWith("BS00001")) {
                        let reimdimentionbody = meta['reimdimentionbody'];
                        for (let item in reimdimentionbody.items) {
                            item.disabled = true;
                        }
                        break;
                    }

                    let reqData = {
                        datatype: datatype,
                        settingPagecode: "201101BXBJ_Setting"
                    };
                    requestApi.beforeValueChange({
                        data: reqData,
                        success: (data) => {
                            let billbeanid;
                            let pk_tradetype = this.props.syncTree.getSelectNode("tree1").refcode;
                            if (pk_tradetype != undefined && pk_tradetype.startsWith('261')) {
                                billbeanid = 'e3167d31-9694-4ea1-873f-2ffafd8fbed8';
                            }
                            if (pk_tradetype != undefined && pk_tradetype.startsWith('263')) {
                                billbeanid = 'e0499b58-c604-48a6-825b-9a7e4d6dacca';
                            }
                            if (pk_tradetype != undefined && pk_tradetype.startsWith('264')) {
                                billbeanid = 'd9b9f860-4dc7-47fa-a7d5-7a5d91f39290';
                            }
                            meta['reimdimentionbody'] = data.data.reimdimentionbody;
                            meta['reimdimentionbody'].items.find((item) => {
                                delete item.width;
                                if (item.attrcode === "billref") {
                                    item.refcode = 'uap/refer/riart/mdTreeByIdRef/index.js';
                                    item.isTreelazyLoad = true;
                                    item.queryCondition = () => {
                                        return { beanId: billbeanid, TreeRefActionExt: '' }
                                    };
                                }
                            });
                            props.meta.setMeta(meta);
                            props.editTable.setStatus('reimdimentionbody', 'edit');
                        }
                    });
                    break;
            }
            break;
        case 'controlsetting':
            switch (item.attrcode) {
                case 'controlformula':
                    this.openFormula();
                    break;
            }
            break;
    }
    return true;
};