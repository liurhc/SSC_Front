import {ajax } from 'nc-lightapp-front';

let requestDomain =  '';

let requestApiOverwrite = {
    // 查询接口
    query: (opt) => {
        ajax({
            url: '/nccloud/sscrp/rpbill/BillSearchForErmAction.do',
            data: opt.data,
            success: opt.success
        });
    },
    // 关联单据接口
    relation:(opt) => {
         ajax({
            url: `${requestDomain}/nccloud/erm/expenseaccount/FytzRelationBillAction.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    // 联查关联单据接口
    linkRelationBill: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/expenseaccount/FytzLinkRelationAction.do`,
            data: opt.data,
            async: false,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    // 模糊查询接口
    queryFuzzyKey: (opt) => {
        ajax({
            url: '/nccloud/sscrp/rpbill/QrySearchKeyForErmBillSearchAction.do',
            data: opt.data,
            success: opt.success
        });
    },
}

export default  requestApiOverwrite;