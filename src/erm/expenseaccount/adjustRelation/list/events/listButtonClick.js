import requestApi from '../requestApi'
export default function listButtonClick(actionId, data, index, actionFrom) {
    // console.log('listButtonClick:' + actionId);
    let listData = data;
    if(actionFrom == "detail"){
        listData = this.data.listRows[index].values;
    }
    switch(actionId){
        case 'OpenBill':
            requestApi.openBill({
                data: {
                    billtypeCode: listData.billtypecode.value, 
                    transtypeCode: listData.transtypecode.value,
                    billid: listData.busiid.value
                },
                success: (data) => {
                    this.props.specialOpenTo(
                        data.data.url,
                        {
                            ...data.data.data,
                            scene: 'fycx'
                        }
                    )
                }
            })
        break;
    }
}