import {ajax} from 'nc-lightapp-front';
import requestApi from '../requestApi'
import presetVar from '../presetVar'
import {listButtonClick} from './index'
window.billinfo = {
    tzd_billid: '',
    tzd_billno: '',
    tzd_tradetype: ''
};
export default function (props) {
	const that = this;
    props.createUIDom(
        {
            appcode: "201102TZFY",//小应用编码
            pagecode: "201102GLDJ"//页面编码
        },
        (data) => {
            let meta = data.template;
            meta[window.presetVar.searchArea].items.map((one)=>{
                if(one.attrcode == "pk_tradetype"){
                    one.queryCondition =  {'searchScope': 'erm'};
                    one.fieldValued =  'refcode';
                }
                if(one.attrcode == "pk_org"){
                    one.isRunWithChildren =false;
                    one.isShowDisabledData =true;
                    one.queryCondition =  {'TreeRefActionExt': 'nccloud.web.org.closeaccbook.tool.BusinessUnitTreeRefExt'};
                }
                dealRefShow(one);
            })
            props.meta.setMeta(meta);

            // 默认显示已经关联的数据
            requestApi.linkRelationBill({
                data : {
                    // "tradetype": parent.window.billinfo.tzd_tradetype,
                    "tradetype": window.billinfo.tzd_tradetype,
                    // "tzd_billid" : parent.window.billinfo.tzd_billid,
                    "tzd_billid" : window.billinfo.tzd_billid,
                    "actiontype" : "relationbill"
                },
                success: (data) => {
                    // 设置列表数据
                    data.grid && props.transferTable.setTransferTableValue(window.presetVar.list, '', data.grid.rplist, 'pk_bill', '');
                    
                    // 设置缩略数据
                    data[window.presetVar.card] && this.refs.detail.addDetailData(data[window.presetVar.card]);
                    data && props.setRelationChecked(data, props);
                }
            })
        }
    )

    const dealRefShow = function (data){
        if(data.itemtype == "refer"){
            if(data.fieldDisplayed == "code"){
                data.fieldDisplayed = "refcode";
            }else if(data.fieldDisplayed == "name"){
                data.fieldDisplayed = "refname";
            }
        }
    }
}
