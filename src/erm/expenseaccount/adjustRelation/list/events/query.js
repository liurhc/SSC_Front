import requestApi from '../requestApi'
let queryData = function(isAddPage){
    if(isAddPage){
        // 更新查询条件
        this.queryKey.pageinfo.number++;
    }else{
        this.queryKey.pageinfo.number = 1;
    }

    requestApi.query({
        data: this.queryKey,
        success: (data) => {
            let listRows = [];
            if(isAddPage){
                this.data.listRows.map((one)=>{
                    listRows.push(this.copyData(one));
                })
            }else{
                this.refs.detail.clearAllDetailData();
            }
            data.data[window.presetVar.list].rows.map((one)=>{
                listRows.push(this.copyData(one));
            })
            this.data.listRows = listRows;
            // 设置列表数据
            let listdata = data.data;
            //根据是否查询出数据，设置“关联”按钮是否可用
            if(listdata && listdata.rplist.rows.length>0){
                this.setTransferBtnDisabled(false);
            }else{
                this.setTransferBtnDisabled(true);
            }
            if(listdata){
                listdata && this.props.transferTable.setTransferTableValue(window.presetVar.list, '', 
                {
                    areacode: window.presetVar.list,
                    rows: listRows
                }, 
                'pk_bill', '');
            }else{
                this.props.transferTable.setTransferTableValue(window.presetVar.list, '', [], 'pk_bill', '');
            }
            // 设置缩略数据
            this.refs.detail.addDetailData(data.data[window.presetVar.card]);
            let newState = {};
            newState = this.state;
            // 更新页信息
            this.queryKey.pageinfo = data.data[window.presetVar.list].pageinfo;
            if(this.queryKey.pageinfo.totalPages > this.queryKey.pageinfo.number){
                newState.pageControll = 'notend';
            }else if(this.queryKey.pageinfo.totalPages == this.queryKey.pageinfo.number){
                newState.pageControll = 'end';
            }else{
                newState.pageControll = 'none';
            }
            this.setState(newState);
            setTimeout(()=>{this.canChangPageFlag=true;},100);
            this.pubMessage.querySuccess((((data.data || {})[window.presetVar.list] || {}).pageinfo || {}).totalElements);
        }
    })
}
export default {queryData}