import {ajax} from 'nc-lightapp-front';
import requestApi from '../requestApi'

export default function (props) {

    props.createUIDom(
        {
            pagecode: "201102FYYT_Q",//页面编码
            appcode: "201102BCLF"//小应用编码
        },
        (dataui) => {
            let metas = dataui.template;
            let tempArr = [];
            let searchitem = metas[window.fyytAreaCode.searchArea];
            let headmeta = metas[window.fyytAreaCode.billsArea];
            let bodymeta = metas[window.fyytAreaCode.bodysArea];
            searchitem.items.forEach((item) => {

                 if(item.itemtype == 'refer'){
                     // item.visible = false;
                     item.isShowUnit=true;
                 }
                // else{
                //      item.visible = true;
                //  }
                 tempArr.push(item);
             });
            let meta = {
                ...metas,
                searchArea: {
                    moduletype: 'search',
                    items : tempArr
                },
                head:{
                    moduletype: 'table',
                    items: headmeta.items
                },
                body:{
                    moduletype: 'table',
                    items: bodymeta.items
                }
            }
            props.meta.setMeta(meta);

            let bxdata = window.bxdata;
            let data={
                querycondition:{"conditions":[],"logic":"and"},
                pagecode: window.pageCode.searchPage,
                pageInfo:{
                    "pageIndex":-1,
                    "pageSize":10,
                    "totalPage":"0"
                },
                queryAreaCode:window.fyytAreaCode.searchArea,  //查询区编码
                oid:'0001Z310000000005EFJ',  //pub_area 中PK_AREA
                querytype:'tree'

            };
            let accruedverifyGrid = window.accruedverifyGrid;
            let senddata = {'accruedverifyGrid':accruedverifyGrid, bxdata,data,"pagecode":"201102FYYT_Q","tepmid":this.props.meta.getMeta().pageid};
            requestApi.query({
                data:senddata,
                success: (data) => {
                    if(data){
                        this.setTransferBtnDisabled(false);
                        data && this.props.transferTable.setTransferTableValue(window.fyytAreaCode.billsArea, window.fyytAreaCode.bodysArea, data, 'pk_accrued_bill', 'pk_accrued_detail');
                        data && props.setDefaultSelectedYt(data, this.props);
                    } else {
                        this.setTransferBtnDisabled(true);
                        this.props.transferTable.setTransferTableValue(window.fyytAreaCode.billsArea, window.fyytAreaCode.bodysArea, [], 'pk_accrued_bill', 'pk_accrued_detail');
                    }

                }
            })
    });
}
