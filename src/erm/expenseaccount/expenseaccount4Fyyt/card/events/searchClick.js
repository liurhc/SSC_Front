import requestApi from '../requestApi'

export default function searchClick(props, searchVal) {
    if(true){

        let bxdata = window.bxdata;
        let data={
            querycondition:searchVal==null?{}:searchVal,
            pagecode: window.pageCode.searchPage,
            pageInfo:{
                "pageIndex":-1,
                "pageSize":10,
                "totalPage":"0"
            },
            queryAreaCode:window.fyytAreaCode.searchArea,  //查询区编码
            oid:'0001Z310000000005EFJ',  //pub_area 中PK_AREA
            querytype:'tree'

        };
        let accruedverifyGrid = window.accruedverifyGrid;
        let senddata = {'accruedverifyGrid':accruedverifyGrid, bxdata,data,"pagecode":"201102FYYT_Q","tepmid":this.props.meta.getMeta().pageid};
        requestApi.query({
            data:senddata,
            success: (data) => {
                if(data){
                    this.setTransferBtnDisabled(false);
                    data && this.props.transferTable.setTransferTableValue(window.fyytAreaCode.billsArea, window.fyytAreaCode.bodysArea, data, 'pk_accrued_bill', 'pk_accrued_detail');
                    data && props.setDefaultSelectedYt(data, this.props);
                } else {
                    this.setTransferBtnDisabled(true);
                    this.props.transferTable.setTransferTableValue(window.fyytAreaCode.billsArea, window.fyytAreaCode.bodysArea, [], 'pk_accrued_bill', 'pk_accrued_detail');
                }

            }
        })
    }
}

