import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createPage, base, high } from 'nc-lightapp-front';
import requestApi from './requestApi';
import { buttonClick, initTemplate, afterEvent, beforeEvent, refreshButtonClick } from './events';

import {
    ProfileStyle,
    ProfileHead,
    ProfileBody,
    BodyLeft,
    BodyRight,
    HeadCenterCustom,
    EmptyArea,
    ButtonGroup
} from 'ssccommon/components/profile';
import { SyncTree } from 'ssccommon/components/tree';
import { EditTable } from 'ssccommon/components/table';

import BusinessUnitTreeRef from 'uapbd/refer/org/BusinessUnitTreeRef';
import pubMessage from 'ssccommon/utils/pubMessage'


import './index.less';

const { NCModal, NCButton } = base;
const { FormulaEditor } = high;

class TreeTable extends Component {

    constructor(props) {
        super();
        this.metaparam = {};
        this.state = {
            isEdit: false,
            isShowIndex: true,
            showConfigModal: false,
            showControlConfigModal: false,
            showQuickCopyModal: false,
            pk_org: null,
            orgdisabled: false,
            quickcopypage: 'outer',//报销标准维度快速复制outer和报销标准快速复制inner区分标志保存时用
            clickvalue: "",
            showFormula: false,
            showImportModal: false,
            pk_tradetype: "",
            treeSearchDisabled: false,
            refreshStatus: 'nobrowse'
        }
        this.pubMessage = new pubMessage(props);
        initTemplate.call(this, props);
    }
    openFormula() {

        this.metaparam = this.queryMetaParam() || {};

    }

    queryMetaParam() {

        let metaparam = {};
        if (this.props.syncTree.getSyncTreeValue('tree1') == undefined || this.props.syncTree.getSelectNode("tree1") == null) {
            return metaparam
        }
        let srcbilltype = this.props.syncTree.getSelectNode("tree1").refcode
        if (this.props.syncTree.getSelectNode("tree1").pid != '~') {
            srcbilltype = this.props.syncTree.getSyncTreeValue('tree1', this.props.syncTree.getSelectNode("tree1").pid).refcode
        }

        // 查询
        requestApi.queryMetaParam({
            data: {
                src_billtype: srcbilltype
            },
            success: res => {
                metaparam = {
                    pk_billtype: srcbilltype,
                    bizmodelStyle: res.data.moduleid,
                    classid: res.data.beanid
                }
                this.metaparam = metaparam;
                this.setState({ showFormula: true });
            }
        });
    }


    componentWillMount() {
                window.onbeforeunload = () => {
                      ;
                    let status = this.props.editTable.getStatus('reimrule');        
                    if (status == 'edit'||status == 'add') {
                        return ''; 
                         }
             }
            }

    componentDidMount() {

    }

    render() {
        let { modal } = this.props;
        let { createForm } = this.props.form;
        let { createModal } = modal;
        let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId
        buttonClick.saveModalBtnEventConfig.click = buttonClick.saveModalBtnEventConfig.click.bind(this);
        buttonClick.cancelModalBtnEventConfig.click = buttonClick.cancelModalBtnEventConfig.click.bind(this);
        buttonClick.saveControlModalBtnEventConfig.click = buttonClick.saveControlModalBtnEventConfig.click.bind(this);
        buttonClick.cancelControlModalBtnEventConfig.click = buttonClick.cancelControlModalBtnEventConfig.click.bind(this);
        buttonClick.saveQuickCopyModalBtnEventConfig.click = buttonClick.saveQuickCopyModalBtnEventConfig.click.bind(this);
        buttonClick.cancelQuickCopyModalBtnEventConfig.click = buttonClick.cancelQuickCopyModalBtnEventConfig.click.bind(this);
        buttonClick.saveBtnEventConfig.click = buttonClick.saveBtnEventConfig.click.bind(this);
        buttonClick.exportBtnEventConfig.click = buttonClick.exportBtnEventConfig.click.bind(this);
        let props = this.props;
        let syncTree = this.props.syncTree;
        return (
            <ProfileStyle
                id='reimrulerfororg'
                layout="treeTable"
                {...this.props}
            >
                {/*页面头*/}
                <ProfileHead
                    // "201101BXBZ-0001": "报销标准设置-组织"
                    title={multiLang && multiLang.get('201101BXBZ-0001')}
                    refreshButtonEvent={refreshButtonClick.bind(this)}
                    status={this.state.refreshStatus}
                >
                    <HeadCenterCustom>
                        <div style={{ width: '204px' }}>
                            <BusinessUnitTreeRef
                                value={this.state.pk_org}
                                disabled={this.state.orgdisabled}
                                onChange={(v) => {
                                    this.setState({ pk_org: v });
                                    this.props.syncTree.cancelSelectedNode('tree1');
                                    this.props.button.setButtonsVisible(['Edit', 'Setting', 'ControlSetting', 'QuickCopy', 'Export'], false);
                                    let resultData = {
                                        rows: []
                                    };
                                    this.props.editTable.setTableData('reimrule', resultData);
                                    if(!(v || {}).refpk){
                                        this.props.syncTree.closeNodeByPk('tree1');
                                    } else {
                                        let firstNode = syncTree.getSyncTreeValue('tree1')[0];
                                        let firstNodeKey = firstNode.refpk;
                                        syncTree.setNodeSelected('tree1', firstNodeKey); //选中节点
                                        //syncTree.openNodeByPk('tree1', firstNodeKey); //打开节点
                                        //buttonClick.treeSelectEvent.call(this, firstNodeKey, firstNode) //触发节点选中事件 
                                    }
                                }}
                                queryCondition={() => {
                                    return {
                                        TreeRefActionExt: 'nccloud.web.action.erm.ref.sqlbuilder.BusinessUnitTreeRefBuilder',
                                        appcode: props.getSearchParam('c')
                                    }
                                }}
                            />
                        </div>
                    </HeadCenterCustom>

                    <ButtonGroup
                        area="page_header"
                        buttonLimit="4"
                        buttonEvent={
                            {
                                Edit: { afterClick: () => { buttonClick.afterEdit(this) } },
                                Cancel: { afterClick: () => { buttonClick.afterCancel(this) } },
                                Setting: { click: () => { buttonClick.afterSetting(this) } },
                                ControlSetting: { click: () => { buttonClick.controlSetting(this) } },
                                QuickCopy: { click: () => { buttonClick.quickCopy(this) } },
                                Save: buttonClick.saveBtnEventConfig,
                                Export: buttonClick.exportBtnEventConfig,
                                Add: buttonClick.addBtnEventConfig,
                                DeleteS: buttonClick.deleteBtnEventConfig

                            }

                        }
                    />
                </ProfileHead>

                {/*页面体*/}
                <ProfileBody>
                    <BodyLeft>
                        <SyncTree
                            areaId="tree1"
                            disabledSearch={this.state.treeSearchDisabled}
                            onSelectEve={
                                buttonClick.treeSelectEvent.bind(this)
                            }
                        />
                    </BodyLeft>
                    <BodyRight>
                        <EditTable
                            areaId="reimrule"
                            showIndex={this.state.isShowIndex}
                            showCheck={this.state.isEdit}
                            onAfterEvent={afterEvent.bind(this)}
                            onBeforeEvent={beforeEvent.bind(this)}
                            selectedChange={(props, moduleId, newVal, oldVal) => {
                                if (newVal > 0) {
                                    props.button.setButtonDisabled(['DeleteS'], false);
                                } else if (newVal == 0) {
                                    props.button.setButtonDisabled(['DeleteS'], true);
                                }
                            }}
                        >
                            <ButtonGroup
                                area="body-shoulder"
                                buttonEvent={
                                    {
                                    }
                                }
                            />
                        </EditTable>
                        {/*报销标准配置-维度*/}
                        <NCModal
                            backdrop='static'
                            size="xlg"
                            show={
                                this.state.showConfigModal
                            }
                            onHide={
                                () => {
                                    this.setState({
                                        showConfigModal: false
                                    })
                                }
                            }
                        >
                            <NCModal.Header closeButton="true">
                                {/*"201101BXBZ-0003": "维度"*/}
                                <NCModal.Title>{multiLang && multiLang.get('201101BXBZ-0003')}</NCModal.Title>
                            </NCModal.Header>
                            <NCModal.Body>
                                <ProfileStyle id="modalofreimruler"
                                    layout="singleTable"
                                    {...this.props}
                                >
                                    {/*页面头*/}
                                    <ProfileHead
                                        title={""}
                                    >
                                        <ButtonGroup
                                            area="setting_header"
                                            buttonLimit="3"
                                            buttonEvent={
                                                {
                                                    AddSetting: buttonClick.addModalBtnEventConfig,
                                                    DeleteSetting: buttonClick.deleteModalBtnEventConfig,
                                                    QueryCopySetting: { click: () => { buttonClick.querySetingCopy(this) } }
                                                }
                                            }
                                        />
                                    </ProfileHead>

                                    {/*页面体*/}
                                    <ProfileBody>
                                        <EditTable
                                            areaId="reimdimentionbody"
                                            onAfterEvent={afterEvent.bind(this)}
                                            onBeforeEvent={beforeEvent.bind(this)}
                                            selectedChange={(props, moduleId, newVal, oldVal) => {
                                                if (newVal > 0) {
                                                    props.button.setButtonDisabled(['DeleteSetting'], false);
                                                } else if (newVal == 0) {
                                                    props.button.setButtonDisabled(['DeleteSetting'], true);
                                                }
                                            }}
                                        >
                                            <ButtonGroup
                                                area="body-shoulder"
                                                buttonEvent={
                                                    {
                                                        Add: buttonClick.addBtnEventConfig
                                                    }
                                                }
                                            />
                                        </EditTable>
                                    </ProfileBody>
                                </ProfileStyle>
                            </NCModal.Body>
                            <NCModal.Footer>
                                {/*"2011-0001": "保存",*/}
                                {/*"2011-0002": "取消"*/}
                                <NCButton onClick={buttonClick.saveModalBtnEventConfig.click} colors='primary'>{multiLang && multiLang.get('2011-0001')}</NCButton>
                                <NCButton onClick={buttonClick.cancelModalBtnEventConfig.click}>{multiLang && multiLang.get('2011-0002')}</NCButton>
                            </NCModal.Footer>
                        </NCModal>


                        {/*报销标准-控制配置*/}
                        <NCModal
                            backdrop='static'
                            size="xlg"
                            show={
                                this.state.showControlConfigModal
                            }
                            onHide={
                                () => {
                                    this.setState({
                                        showControlConfigModal: false
                                    })
                                }
                            }>
                            <NCModal.Header closeButton="true">
                                {/*"201101BXBZ-0004": "控制设置"*/}
                                <NCModal.Title>{multiLang && multiLang.get('201101BXBZ-0004')}</NCModal.Title>
                            </NCModal.Header>
                            <NCModal.Body>
                                <ProfileStyle id="modalofreimruler"
                                    layout="singleTable"
                                    {...this.props}
                                >
                                    {/*页面头*/}
                                    <ProfileHead
                                        title={""}
                                    >
                                        <ButtonGroup
                                            area="controlSetting_header"
                                            buttonLimit="3"
                                            buttonEvent={
                                                {
                                                    AddSetting: buttonClick.addModalBtnEventConfig,
                                                    DeleteSetting: buttonClick.deleteModalBtnEventConfig
                                                }
                                            }
                                        />
                                    </ProfileHead>

                                    {/*页面体*/}
                                    <ProfileBody>
                                        <EditTable
                                            areaId="controlsetting"
                                            showCheck={false}
                                            onAfterEvent={afterEvent.bind(this)}
                                            onBeforeEvent={beforeEvent.bind(this)}
                                        >
                                            <ButtonGroup
                                                area="body-shoulder"
                                                buttonEvent={
                                                    {
                                                        Add: buttonClick.addBtnEventConfig
                                                    }
                                                }
                                            />
                                        </EditTable>
                                    </ProfileBody>
                                </ProfileStyle>
                            </NCModal.Body>
                            <NCModal.Footer>
                                {/*"2011-0001": "保存",*/}
                                {/*"2011-0002": "取消"*/}
                                <NCButton onClick={buttonClick.saveControlModalBtnEventConfig.click} colors='primary'>{multiLang && multiLang.get('2011-0001')}</NCButton>
                                <NCButton onClick={buttonClick.cancelControlModalBtnEventConfig.click}>{multiLang && multiLang.get('2011-0002')}</NCButton>
                            </NCModal.Footer>
                        </NCModal>


                        {/*报销标准-快速复制*/}
                        <NCModal
                            backdrop='static'
                            show={
                                this.state.showQuickCopyModal
                            }
                            onHide={
                                () => {
                                    this.setState({
                                        showQuickCopyModal: false
                                    })
                                }
                            }>
                            <NCModal.Header closeButton="true">
                                {/*"201101BXBZ-0005": "复制到"*/}
                                <NCModal.Title>{multiLang && multiLang.get('201101BXBZ-0005')}</NCModal.Title>
                            </NCModal.Header>
                            <NCModal.Body>
                                <div className="form-wrapper">
                                    {createForm("quickcopy", {
                                        //编辑后事件
                                        onAfterEvent: afterEvent.bind(this),
                                    })}
                                </div>
                            </NCModal.Body>
                            <NCModal.Footer>
                                {/*"2011-0002": "取消",*/}
                                {/*"2011-0003": "确定"*/}
                                <NCButton onClick={buttonClick.saveQuickCopyModalBtnEventConfig.click} colors='primary'>{multiLang && multiLang.get('2011-0003')}</NCButton>
                                <NCButton onClick={buttonClick.cancelQuickCopyModalBtnEventConfig.click}>{multiLang && multiLang.get('2011-0002')}</NCButton>
                            </NCModal.Footer>
                        </NCModal>

                        {/*报销标准-公式编辑器*/}
                        <FormulaEditor
                            ref="formulaEditor"
                            value={this.state.clickvalue}
                            show={this.state.showFormula}
                            onOk={value => {
                                this.setState({ showFormula: false });
                                this.props.editTable.setValByKeyAndIndex('controlsetting', this.props.editTable.getClickRowIndex("controlsetting").index, 'controlformula', { value: value, display: value, scale: 0 })
                            }} //点击确定回调
                            onCancel={a => {
                                this.setState({ showFormula: false });
                            }} //点击确定回调
                            isValidateOnOK={false}
                            metaParam={{
                                pk_billtype: this.metaparam.pk_billtype,//"264X",
                                bizmodelStyle: this.metaparam.bizmodelStyle,//"erm",
                                classid: ''//"d9b9f860-4dc7-47fa-a7d5-7a5d91f39290"
                            }}
                            onHide={() => {
                                this.setState({ showFormula: false });
                            }}
                        />
                        <EmptyArea>
                            <div>
                                {/* 导入 */}
                                {createModal('importModal', {
                                    noFooter: true,
                                    className: 'import-modal'
                                })}
                            </div>
                        </EmptyArea>
                    </BodyRight>
                </ProfileBody>
            </ProfileStyle>
        )
    }
}

TreeTable = createPage({
    mutiLangCode: '2011'
})(TreeTable);

export default TreeTable;
