import {ajax,base} from 'nc-lightapp-front';
import requestApi from '../requestApi';


export default function afterEvent(props,moduleId,key,value, changedrows,record,index) {
    console.log('编辑后事件', props,moduleId,key,value, 22, changedrows);
    switch(moduleId) {
        case 'reimrule' :
            let resultRows = [];
            resultRows.push(index);

            let result = new Object();
            result.areaType = "table";
            result.rows = resultRows;

            let reqData = {
                row: result
            };
            if(key.startsWith('def')){
                if(value != undefined && value.refname != undefined){
                    index.values[key].display = value.refname;
                }
                break;
            }
            switch (key) {
                case 'pk_reimtype' :
                    if(value.values != undefined){
                        index.values.pk_reimtype.display = value.refname;
                    }
                    break;
                case 'pk_currtype' :
                    requestApi.afterValueChange({
                        data: reqData,
                        success: (data) => {
                            props.editTable.updateDataByIndexs('reimrule',[{index:record,data:data.data[0]}]);
                        }
                    });
                    console.log(props,moduleId,key,value, changedrows)
                    break;
                case 'amount' :
                    requestApi.afterValueChange({
                    data: reqData,
                     success: (data) => {
                              props.editTable.updateDataByIndexs('reimrule',[{index:record,data:data.data[0]}]);
                            }
                       });
                       console.log(props,moduleId,key,value, changedrows)
                      break;
            }
            break;
        case 'reimdimentionbody' :
            switch (key) {
                case 'billref' :
                    if(value.values != undefined){
                        index.values.billref.value = value.values.fulldisplayname;
                        index.values.billref.display = value.values.fulldisplayname;
                    }
                    break;
            }
            break;
        case 'controlsetting' :
            switch (key) {
                case 'showitem' :
                    if(value.values != undefined){
                        index.values.showitem.value = value.values.fulldisplayname;
                        index.values.showitem.display = value.values.fulldisplayname;
                        index.values.showitem_name.value = value.values.fullpath;
                        index.values.showitem_name.display = value.values.fullpath;
                    }
                    break;
                case 'controlitem' :
                    if(value.values != undefined){
                        index.values.controlitem.value = value.values.fulldisplayname;
                        index.values.controlitem.display = value.values.fulldisplayname;
                        index.values.controlitem_name.value = value.values.fullpath;
                        index.values.controlitem_name.display = value.values.fullpath;
                    }
                    break;
            }
            break;
    }
};