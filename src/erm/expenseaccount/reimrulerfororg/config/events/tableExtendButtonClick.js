import requestApi from "../requestApi";
/**
 * 表格扩展按钮click事件
 */
function tableExtendButtonClick() {
    return {
       
        ['Up'] : (record, index) => {
            if(index!=0){
                this.props.editTable.setValByKeyAndIndex(window.areaCode.settingArea, index-1, 'orders', {value: record.values.orders.value, display:record.values.orders.value});
                this.props.editTable.setValByKeyAndIndex(window.areaCode.settingArea, index, 'orders', {value: parseInt(record.values.orders.value)-1, display:parseInt(record.values.orders.value)-1});
                this.props.editTable.moveRow(window.areaCode.settingArea,index, index-1);
                this.props.editTable.setRowStatus(window.areaCode.settingArea, [index,index-1], '1');
            }
        },
        ['Down'] : (record, index) => {
            let totalIndex = this.props.editTable.getNumberOfRows(window.areaCode.settingArea);
            if(index!=totalIndex-1){
                this.props.editTable.setValByKeyAndIndex(window.areaCode.settingArea, index+1, 'orders', {value: record.values.orders.value, display:record.values.orders.value});
                this.props.editTable.setValByKeyAndIndex(window.areaCode.settingArea, index, 'orders', {value: parseInt(record.values.orders.value)+1, display:parseInt(record.values.orders.value)+1});
                this.props.editTable.moveRow(window.areaCode.settingArea,index, index+1);
                this.props.editTable.setRowStatus(window.areaCode.settingArea, [index,index+1], '1');
            }
        },
        ['SettingTop'] : (record, index) => {
            this.props.editTable.setRowPosition(window.areaCode.settingArea,index,'up');
            let tableData = this.props.editTable.getAllData(window.areaCode.settingArea);
            let changeIndex = []
            tableData.rows.map((row,idx)=>{
                this.props.editTable.setValByKeyAndIndex(window.areaCode.settingArea, idx, 'orders', {value:idx+1, display:idx+1});
                changeIndex.push(idx);
            })
            this.props.editTable.setRowStatus(window.areaCode.settingArea, changeIndex, '1');
        },
        ['SettingBottom'] : (record, index) => {
            this.props.editTable.setRowPosition(window.areaCode.settingArea,index,'down');
            let tableData = this.props.editTable.getAllData(window.areaCode.settingArea);
            let changeIndex = []
            tableData.rows.map((row,idx)=>{
                this.props.editTable.setValByKeyAndIndex(window.areaCode.settingArea, idx, 'orders', {value:idx+1, display:idx+1});
                changeIndex.push(idx);
            })
            this.props.editTable.setRowStatus(window.areaCode.settingArea, changeIndex, '1');
        },
    }
}


export default tableExtendButtonClick;