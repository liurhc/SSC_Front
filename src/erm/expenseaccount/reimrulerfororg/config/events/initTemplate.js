import {base, high, ajax} from 'nc-lightapp-front';
import 'ssccommon/components/globalPresetVar';
import {setTableExtendCol} from 'ssccommon/components/profile'
import requestApi from '../requestApi';

//变量设置规范，如需要
window.presetVar = {
    ...window.presetVar,
    pageId: '',
    head: {
        head1: '',
        head2: ''
    },
    body: {
        body1: 'reimrule',
        body2: ''
    },
    search: {}
};
window.areaCode = {
    billsArea:'reimrule',
    settingArea:'reimdimentionbody',
    controlSettingArea:'reimrule',
    quickCopyArea:'quickcopy'
};

export default function (props) {

    setTimeout(() => {
        //let buttonTpl = tplMock.button;
        props.createUIDom(
            {
                pagecode: '201101BXBJ_config',//页面编码
                appcode: '201101BXBJ'//小应用编码
            },
            (data)=> {
                let meta = data.template;
                let buttonTpl = data.button;
                meta['reimrule'].items.forEach((item) =>{
                    if(item.visible == true){
                        if(item.attrcode == 'pk_reimtype'){
                            item.width = '180px';
                        }else if(item.attrcode == 'pk_deptid'){
                            item.width = '160px';
                        }else{
                            item.width = '120px';
                        }
                    }
                });

                //设置表格的扩展按钮列
                setTableExtendCol(props, meta, [{
                    areaId: 'reimrule',
                    btnKeys: ['Delete', 'UnionQueryProof', 'ShowBill'],
                    onButtonClick: (props, btnKey, record, index) => {
                        props.editTable.deleteTableRowsByIndex('reimrule', index)
                    }
                }]);

                //设置区域模板
                props.meta.setMeta(meta);
                //设置按钮模板
                props.button.setButtons(buttonTpl);

                //设置按钮行为为弹窗
                // props.button.setPopContent('delete','确认要删除该信息吗？') /* 设置操作列上删除按钮的弹窗提示 */

                //设置按钮的初始可见性
                props.button.setButtonsVisible({
                    'Save': false,
                    'Add': false,
                    'Cancel': false,
                    'Edit': false,
                    'Setting': false,
                    'ControlSetting': false,
                    'QuickCopy': false,
                    'Import': false,
                    'Export': false,
                    'DeleteS': false
                });

                //获取树的数据
                requestApi.getTreeData({
                    data: {},
                    success : (data)=> {
                        let treenode = data.data[1].nodes;
                        let orginfo = data.data[0][0];
                        if(typeof(orginfo)!="undefined"){
                            let org = {
                              refpk:orginfo[0],
                              refcode:orginfo[1],
                              refname:orginfo[2]
                         }
                            this.setState({pk_org:org});
                        };
                        //this.state.pk_org = data.data[0];
                        //如果pid为空，则去除pid属性
                        for (let value of treenode) {
                            if (value.pid == null) {
                                delete value.pid
                            }
                        };
                        let {syncTree} = props;
                        let {setSyncTreeData} = syncTree;
                        let newTree = syncTree.createTreeData(treenode); //创建树 组件需要的数据结构
                        setSyncTreeData('tree1', newTree);
                    }
                })
            })
    }, 200)
}


