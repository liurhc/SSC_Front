import {searchClick} from '../events'
import { cacheTools } from 'nc-lightapp-front';
import requestApi from '../requestApi';

export default function refreshButtonClick() {
    let resultData = {
        rows: []
    };
    let reqData = {
        pk_tradetype:this.props.syncTree.getSelectNode("tree1").refcode,
        pk_org:this.state.pk_org.refpk
    };

    let meta = this.props.meta.getMeta();
    requestApi.getTableData({
        data: reqData,
        success: (data) => {
            let newmeta = {
                ...data.data.template,
            };
            newmeta['reimdimentionbody'] = meta['reimdimentionbody'];
            newmeta['controlsetting'] = meta['controlsetting'];
            newmeta['quickcopy'] = meta['quickcopy'];
            newmeta['formrelation'] = null;
            newmeta['reimrule'].items.forEach((item) =>{
                delete item.width;
                if(item.attrcode == "pk_currtype"){
                    item.initialvalue = {
                        value: data.data.currtype[0],
                        display: data.data.currtype[1]
                    };
                }
            });
            this.props.meta.setMeta(newmeta);
            
            let resultData = {
                rows:[]
            };

            if(data.data.grid == undefined){
                this.props.editTable.setTableData('reimrule', resultData);
            }else{
                this.props.editTable.setTableData('reimrule', data.data.grid.reimrule);
            }
            this.props.button.setButtonsVisible(['Export','Edit','Setting','ControlSetting','QuickCopy'],true );
            this.props.button.setButtonsVisible(['Cancel','Save','Import','Add','DeleteS'],false );
            this.pubMessage.refreshSuccess();                     

        }
    })
}
