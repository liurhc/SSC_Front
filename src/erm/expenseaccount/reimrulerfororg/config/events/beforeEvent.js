import {ajax,base} from 'nc-lightapp-front';
import requestApi from '../requestApi';


export default function beforeEvent(props,moduleId,item,index, value,record) {
    switch(moduleId) {
        case 'reimrule':
            switch (item.attrcode) {
                case 'pk_deptid' :
                    let meta = props.meta.getMeta();
                    meta['reimrule'].items.forEach((item) => {
                        if (item.attrcode == 'pk_deptid') {
                            item.refcode = 'uapbd/refer/org/DeptTreeRef/index',
                            item.queryCondition = ()=> {
                                return {pk_org: this.state.pk_org.refpk};
                            }
                        }

                    });
                    break;
            }
            break;
        case 'reimdimentionbody':
            switch (item.attrcode) {
                case 'referential' :
                    let meta = props.meta.getMeta();
                    let datatype = record.values.datatype.value
                    if(datatype == undefined || datatype == null){
                        break;
                    }

                    if(record.values.datatype.value.startsWith("BS00001")){
                        let reimdimentionbody = meta['reimdimentionbody'];
                        for(let item in reimdimentionbody.items){
                            item.disabled = true;
                        }
                        break;
                    }

                    let reqData = {
                        datatype:datatype,
                        settingPagecode: "201101BXBJ_Setting"
                    };
                    requestApi.beforeValueChange({
                        data: reqData,
                        success: (data) => {
                            meta['reimdimentionbody'] = data.data.reimdimentionbody;
                            meta['reimdimentionbody'].items.find((item) => {
                                delete item.width;
                            });
                            props.meta.setMeta(meta);
                            props.editTable.setStatus('reimdimentionbody', 'edit');
                        }
                    });
                    break;
            }
            break;
        case 'controlsetting':
            switch (item.attrcode) {
                case 'controlformula' :
                    this.openFormula();
                    break;
            }
            break;
    }
    return true;
};