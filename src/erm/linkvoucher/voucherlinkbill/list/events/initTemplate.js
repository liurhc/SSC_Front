import {base, high, ajax , cacheTools } from 'nc-lightapp-front';
import 'ssccommon/components/globalPresetVar';
import {setTableExtendCol} from 'ssccommon/components/profile'
import requestApi from '../requestApi';
import linkQueryApi from '../../../../public/components/linkquery/linkquerybills'



//变量设置规范，如需要
window.presetVar = {
    ...window.presetVar,
    pageId: '',
    head: {
        head1: '',
        head2: ''
    },
    body: {
        body1: 'billmodel',
        body2: ''
    },
    search: {}
};


export default function (props) {

    props.createUIDom(
        {
			"pagecode": '201101FYSQLC_list',
            "appcode": '201101FYSQLC'
        },
        function (data) {
            let meta = {};
            meta.billmodel=data.template.billmodel;
            // let buttonTpl = data.button;
            meta.billmodel.items.forEach((item, key) => {
                if(item.attrcode == 'djbh') {
                    item.renderStatus= 'browse';
                    item.render = (text, record, index) => {
                        console.log(record)

                        return (
                            <span
                            style={{textDecoration: 'underline',cursor:'pointer'}} 
                                onClick={() =>{
                                    linkQueryApi.link({
                                        data : {
                                            openbillid : record.pk_jkbx.value,
                                            tradetype : record.djlxbm.value,
                                            props : props
                                        }
                                    })
                                }} 
                            >
                                {
                                    record.djbh && record.djbh.value
                                }
                            </span>
                        );
                    };
                }              
            })

            //设置区域模板
            props.meta.setMeta(meta);
            //设置按钮模板
            // props.button.setButtons(buttonTpl);
            //从缓存中获取数据
            let checkedData = [];
            checkedData = cacheTools.get('checkedData');
            //设置初始数据
            //获取表格的数据
            requestApi.getTableData({
                data: checkedData,
                success: (data) => {
                    props.table.setAllTableData('billmodel', data.billmodel);
                }
            })
        }
    )

    setTimeout(() => {
        

    }, 200)
}


