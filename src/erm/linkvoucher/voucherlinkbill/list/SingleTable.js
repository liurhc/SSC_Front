import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createPage, base, high } from 'nc-lightapp-front';
import {  initTemplate } from './events';
const { NCPopover } = base;
import {
    ProfileStyle,
    ProfileHead,
    ProfileBody,
    HeadCenterCustom,
    ButtonGroup
} from 'ssccommon/components/profile';
import { createSimpleTable } from 'nc-lightapp-front';

import FinanceOrgTreeRef from 'uapbd/refer/org/FinanceOrgTreeRef';

import './index.less';

class SingleTable extends Component {

    constructor(props) {
        super();
        this.state = {
            show: false
        }
        initTemplate.call(this, props);
    }

    componentDidMount() {

    }

    render() {

        let { createSimpleTable } = this.props.table;
        let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId

        return (
            /*档案风格布局*/
            <ProfileStyle
            id='ceshi'

                layout="singleTable"
                {...this.props}
            >
                {/*页面头*/}
                <ProfileHead
                    //单据信息
                    title={multiLang && multiLang.get('2011ERMLCPZ-001')}
                >
                    <HeadCenterCustom>
                        
                        <NCPopover
                            rootClose={false}
                            placement="bottom"
                            show={this.state.show}
                            id="demo2"
                        >
                            <div
                                colors="primary"
                                onClick={
                                    () => {
                                        this.setState({
                                            show: true
                                        })
                                    }
                                }
                            ></div>
                        </NCPopover>

                        
                    </HeadCenterCustom>

                    <ButtonGroup
                        area="page_header"
                        buttonLimit="3"
                       
                    />
                </ProfileHead>

                {/*页面体*/}
                <ProfileBody>
                    {createSimpleTable('billmodel', {
                    })
                    }


                </ProfileBody>
            </ProfileStyle>
        )
    }
}

SingleTable = createPage({})(SingleTable);

export default SingleTable;
