import requestApi from "ssccommon/components/requestApi";
import {ajax} from 'nc-lightapp-front';



let requestDomain = '';
let requestApiOverwrite = {
    ...requestApi,
   
    getTableData: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/expenseaccount/VoucherLinkBillAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data.data);
            }
        }) 
    }

}

export default requestApiOverwrite;
