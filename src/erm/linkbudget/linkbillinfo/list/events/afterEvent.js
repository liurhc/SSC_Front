import requestApi from '../requestApi'
export default function (props, id, key, value, data, index) {
    if (value.length > 0 && key === 'pk_operator') {
        props.editTable.setValByKeyAndIndex(id, index, key, {
            display: value[0].values && value[0].values.user_name.value
        });
        props.editTable.setValByKeyAndIndex(id, index, "pk_org", {
            display: value[0].values && value[0].values.name.value
        });
    } else if(value.length > 0 && key === 'pk_billtypeid'){
        let count = 0;
        if(value.length > 0){
            value.map((item) => {
                if (count === 0){
                    props.editTable.setValByKeyAndIndex(id, index, "pk_billtypeid", {
                        display: item.refname,
                        value: item.refpk
                    });
                } else {
                    props.editTable.pasteRow(id, {
                        status : 2,
                        values : {
                            pk_operator : {
                                display: props.editTable.getValByKeyAndIndex(id, index,"pk_operator").display,
                                value: props.editTable.getValByKeyAndIndex(id, index,"pk_operator").value
                            },
                            pk_org : {
                                display: props.editTable.getValByKeyAndIndex(id, index,"pk_org").display,
                                value: props.editTable.getValByKeyAndIndex(id, index,"pk_org").value
                            },
                            startdate : {
                                display: props.editTable.getValByKeyAndIndex(id, index,"startdate").value,
                                value: props.editTable.getValByKeyAndIndex(id, index,"startdate").value
                            },
                            enddate : {
                                display: props.editTable.getValByKeyAndIndex(id, index,"enddate").value,
                                value: props.editTable.getValByKeyAndIndex(id, index,"enddate").value
                            },
                            pk_billtypeid : {
                                display: item.refname,
                                value: item.refpk
                            }
                        }
                    }, index);
                }
                count++;
            })
        }
    }
}