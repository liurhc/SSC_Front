import linkQueryApi from '../../../../public/components/linkquery/linkquerybills'
export default function (record, index) {
    let that = this;
    linkQueryApi.link({
        data : {
            openbillid : record.pk_jkbx.value,
            tradetype : record.pk_djlx.value,
            props:that
        }
    });
}