import {ajax,getMultiLang} from 'nc-lightapp-front';

window.presetVar = {
    ...window.presetVar,
    //状态常量。 用于url中的hash参数 或 框架api的状态
    status: {
        browse: 'browse', //浏览态
        edit: 'edit', //编辑态
        add: 'add', //新增态
        del: 'del'
    }
}
export default function (props) {
    getMultiLang({moduleId: 2011, domainName: 'erm',currentLocale: 'zh-CN', callback: (json) => {
    /*props.createUIDom(
        {
            pagecode: '201101GRSQ_list',//页面id
            appid: '0001Z310000000006H1C'//注册按钮的id
        },
        function (data) {
            let meta = data.template;
            let tempArr = [];
            meta[window.bodyCode].items.forEach((i) => {
                if (i.itemtype == 'checkbox') {
                    i.itemtype = 'switch';
                    i.options = [
                        {
                            display: '停用',
                            value: 0
                        },
                        {
                            display: '启用',
                            value: 1
                        }
                    ];
                    i.initialvalue = {
                        display: '停用',
                        value: 0
                    }
                }
                if (i.attrcode == 'pk_billtypeid') {
                    i.isMultiSelectedEnabled = true;
                }
                tempArr.push(i);
            });
            meta[window.bodyCode].items =tempArr;
            props.meta.setMeta(meta);
            props.button.setButtons(data.button);
            props.button.setButtonsVisible({
                "Edit" : true,
                "Save" : false,
                "Cancel" : false,
                "Add" : false,
                "Delete" : false
            });
        }
    )*/
    let meta = {
        head: {
            moduletype: 'table',
            items: [
                {
                    visible: true,
                    label: json['2011ERMLCBILL-001'],//'收支项目'
                    attrcode: 'qryobj1',
                    itemtype: 'input',
                    col: 12,
                    initialvalue: {
                        value: '',
                        display: null
                    }
                },
                {
                    visible: true,
                    label: json['2011ERMLCBILL-002'],//'业务单元',
                    attrcode: 'qryobj0',
                    itemtype: 'input',
                    col: 12,
                    initialvalue: {
                        value: '',
                        display: null
                    }
                },
                {
                    visible: true,
                    label: json['2011ERMLCBILL-003'],//'单据类型',
                    attrcode: 'pk_billtype',
                    itemtype: 'input',
                    col: 12,
                    initialvalue: {
                        value: '',
                        display: null
                    }
                },
                {
                    visible: true,
                    label: json['2011ERMLCBILL-004'],//'单据编号',
                    attrcode: 'djbh',
                    itemtype: 'input',
                    col: 12,
                    initialvalue: {
                        value: '',
                        display: null
                    }
                },
                {
                    visible: true,
                    label: json['2011ERMLCBILL-005'],//'单据日期',
                    attrcode: 'djrq',
                    itemtype: 'datepicker',
                    col: 12,
                    initialvalue: {
                        value: '',
                        display: null
                    }
                },
                {
                    visible: true,
                    label: json['2011ERMLCBILL-006'],//'本币发生',
                    attrcode: 'loc',
                    itemtype: 'input',
                    col: 12,
                    initialvalue: {
                        value: '',
                        display: null
                    }
                },
                {
                    visible: true,
                    label: json['2011ERMLCBILL-007'],//'原币发生',
                    attrcode: 'ori',
                    itemtype: 'input',
                    col: 12,
                    initialvalue: {
                        value: '',
                        display: null
                    }
                },
                {
                    visible: true,
                    label: json['2011ERMLCBILL-008'],//'集团发生',
                    attrcode: 'gr_loc',
                    itemtype: 'input',
                    col: 12,
                    initialvalue: {
                        value: '',
                        display: null
                    }
                },
                {
                    visible: true,
                    label: json['2011ERMLCBILL-009'],//'全局发生',
                    attrcode: 'gl_loc',
                    itemtype: 'input',
                    col: 12,
                    initialvalue: {
                        value: '',
                        display: null
                    }
                },
            ]
        }
    }
    props.meta.setMeta(meta);
}})
}
