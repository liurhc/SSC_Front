import requestApi from '../requestApi'

let editBtnEventConfig = {
    click: (props)=>{
        props.editTable.setStatus(window.bodyCode, window.presetVar.status.edit);
        props.button.setButtonsVisible({
            Edit: false,
            Save : true,
            Cancel : true,
            Add : true,
            Delete : true
        });
    }
}

let saveBtnEventConfig = {
    click: (props)=>{
        props.editTable.filterEmptyRows(window.bodyCode);
        let reqData = props.editTable.getAllData(window.bodyCode);
        requestApi.save({
            data: reqData,
            success: (data) => {
                props.editTable.setStatus(window.bodyCode, window.presetVar.status.browse);
                data && props.editTable.setTableData(window.bodyCode, data[window.bodyCode]);
            }
        });
        props.button.setButtonsVisible({
            Edit: true,
            Save : false,
            Cancel : false,
            Add : false,
            Delete : false
        });
    }
}
let addBtnEventConfig = {
    click: (props)=>{
        props.editTable.addRow(window.bodyCode);
    }
}

let deleteBtnEventConfig = {
    click: (props)=>{
        let rows = props.editTable.getCheckedRows(window.bodyCode);
        let arr = rows.map(item => item.index);
        props.editTable.deleteTableRowsByIndex(window.bodyCode, arr);
    }
}

let cancelBtnEventConfig = {
    click: (props)=>{
        props.editTable.filterEmptyRows(window.bodyCode);
        props.editTable.setStatus(window.bodyCode, window.presetVar.status.browse);
        props.button.setButtonsVisible({
            Edit: true,
            Save : false,
            Cancel : false,
            Add : false,
            Delete : false
        });
    }
}

export default {editBtnEventConfig, saveBtnEventConfig, addBtnEventConfig, deleteBtnEventConfig, cancelBtnEventConfig}

