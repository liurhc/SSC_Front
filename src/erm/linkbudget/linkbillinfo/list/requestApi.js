import {ajax } from 'nc-lightapp-front';

let requestDomain =  '';

let requestApiOverwrite = {
    query:(opt) => {
        ajax({
            url: `${requestDomain}/nccloud/erm/expenseaccount/LinkBusiInfoAction.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
}

export default  requestApiOverwrite;