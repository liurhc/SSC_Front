import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {createPage, ajax} from 'nc-lightapp-front';
import {buttonClick, initTemplate, afterEvent, tableModelConfirm, onRowDoubleClick} from './events/index';
import requestApi from './requestApi'
import './index.less';
import {
    ProfileStyle,
    ProfileHead,
    ProfileBody,
    BodyLeft,
    BodyRight,
    HeadCenterCustom,
    EmptyArea,
    ButtonGroup
} from 'ssccommon/components/profile';
import {EditTable} from 'ssccommon/components/table';

class LinkBillInfo extends Component {
    constructor(props) {
        super(props);
        let url = window.location.href;
        this.state = {
            sourceData: '',
            pk_ntbparadimvo : url.split('=')[1]
        };
    }
    componentDidMount() {
        requestApi.query({
            data : {
                "ntbpk" : this.state.pk_ntbparadimvo
            },
            success: (data) => {
                data && this.props.table.setAllTableData('head', data['head']);
            }
        })
    }


    render() {
        const {editTable, table, button} = this.props;
        const {createEditTable} = editTable;
        const {createButton} = button;
        const {createSimpleTable} = table;
        let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId
        return (
            <ProfileStyle
                layout="singleTable"
                {...this.props}
            >
                {/*页面头*/}
                {/*"201103PZYM-0004": "预算联查单据",*/}
                <ProfileHead
                    title={multiLang && multiLang.get('201103PZYM-0004')}
                >
                </ProfileHead>

                {/*页面体*/}
                <ProfileBody>
                    <EmptyArea>
                        {createSimpleTable('head', {
                            onAfterEvent: afterEvent,
                            onRowDoubleClick : onRowDoubleClick,
                            showIndex:true
                        })}
                    </EmptyArea>
                </ProfileBody>
            </ProfileStyle>
        )
    }
}

LinkBillInfo = createPage({
    initTemplate: initTemplate,
    mutiLangCode: '2011'
})(LinkBillInfo);

ReactDOM.render(<LinkBillInfo/>, document.querySelector('#app'));