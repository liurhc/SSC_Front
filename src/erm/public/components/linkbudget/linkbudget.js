import {ajax } from 'nc-lightapp-front';
let linkBudgetApi = {
    link : (opt) => {
        ajax({
            url: opt.url || '/nccloud/erm/expenseaccount/LinkBudgetAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    }
}
export default linkBudgetApi;