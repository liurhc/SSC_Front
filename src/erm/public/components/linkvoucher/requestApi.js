import {ajax } from 'nc-lightapp-front';

let requestDomain =  '';

let requestApiOverwrite = {
    query:(opt) => {
        ajax({
            url: `${requestDomain}/nccloud/erm/expenseaccount/VoucherLinkQueryAction.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    queryUrl: (opt) => {
        ajax({
            url: opt.url || '/nccloud/erm/expenseaccount/AppUrlQueryAction.do',
            data: opt.data,
            success: (data) => {
                if (data.success) {
                    data = data.data;
                    opt.success(data);
                }
            }
        })
    }
}

export default  requestApiOverwrite;