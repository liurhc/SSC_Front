import {cacheTools} from 'nc-lightapp-front';
import requestApi from "./requestApi";
let linkVoucherApi = {
    link : (opt) => {
        let props = opt.data.props;
        let record = opt.data.record;
        let appid = opt.data.appid;
        let voucherMapKey = appid + '_LinkVouchar';
        let prevoucherMapKey = appid +'_Preview';
        let dataArrayForPz = [];
        requestApi.query({
            data: {
                id : record.relationID, //单据pk
                tradetype : record.pk_billtype
            },
            success: (data) => {
                if (data) {
                    if(data.isPreVoucher=="true"){
                        
                        let voucherdata = data.data;
                            let dataForPz = {
                                pk_billtype : voucherdata.pk_billtype,
                                pk_group : 'GLOBLE00000000000000',
                                pk_org : voucherdata.pk_org,
                                relationID : voucherdata.relationID
                            }
                            dataArrayForPz.push(dataForPz);
                        let lbilltype = voucherdata.defdoc1;
                        let senddata = {"messagevo":dataArrayForPz,"desbilltype": ["C0"],"srcbilltype":lbilltype}
                        cacheTools.set(prevoucherMapKey,senddata);
                        voucherMapKey =prevoucherMapKey; 
                    }else{
                        let voucherdata = data.data;
                        for(let i in voucherdata){
                            let dataForPz = {
                                pk_billtype : voucherdata[i].pk_billtype,
                                pk_group : voucherdata[i].pk_group,
                                pk_org : voucherdata[i].pk_org,
                                relationID : voucherdata[i].relationID
                            }
                            dataArrayForPz.push(dataForPz);
                        }
                        cacheTools.set(voucherMapKey,dataArrayForPz);
                    }
                    requestApi.queryUrl({
                        data : {
                            appCode : '10170410',
                            pageCode : '10170410_1017041001'
                        },
                        success : (data) => {
                            let hostname = window.location.hostname;
                            let port = window.location.port;
                            props.openTo(data,
                                {
                                    status : 'browse',
                                    scene : voucherMapKey,
                                    appcode : '10170410',
                                    pagecode : '10170410_1017041001'
                                }
                            );
                        }
                    })
                }
            }
        })
    }
}
export default linkVoucherApi;