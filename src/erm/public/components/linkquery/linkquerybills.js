import { ajax } from 'nc-lightapp-front';
let requestDomain = '';
let linkQueryApi = {
    link: (opt) => {
        let param = opt.data;
        let openbillid = param.openbillid || '';
        let billId = param.billId || '';
        let tradetype = param.tradetype;
        let props = param.props;
        let url = '';
        let appcode = '';
        let pagecode = '';
        let senddata = { openbillid: opt.data.openbillid, tradetype: opt.data.tradetype }
        ajax({
            url: `${requestDomain}/nccloud/erm/pub/AppInfoQueryAction.do`,
            data: senddata,
            success: (data) => {
                if (data.success) {
                    data = data.data;
                    url = data.url;
                    appcode = data.appcode;
                    pagecode = data.pagecode;
                    if (data.scene && data.scene == "linksce") {
                        props.openTo(url, {
                            status: 'browse',
                            id: openbillid,
                            tradetype: tradetype,
                            appcode: appcode,
                            pagecode: pagecode,
                            scene: data.scene
                        });
                    } else {
                        props.openTo(url, {
                            status: 'browse',
                            id: openbillid,
                            tradetype: tradetype,
                            appcode: appcode,
                            pagecode: pagecode,
                            scene: 'lc'
                        });
                    }

                }
            }
        })
    }
}
export default linkQueryApi;