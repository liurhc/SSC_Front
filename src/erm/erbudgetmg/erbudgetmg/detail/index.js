import React, { Component } from 'react';
import { createPage, base, high,toast,promptBox,createPageIcon } from 'nc-lightapp-front';

var tableid = '';
const pagecode = '201102SQFY_DETAIL_A';

var tableidMap = {};//模板区域ID
tableidMap['defitem22'] = 'accom_detail';//住宿费明细
tableidMap['defitem23'] = 'meals_detail';//餐费明细
tableidMap['defitem27'] = 'trans_detail';//交通费明细
tableidMap['defitem25'] = 'other_detail';//其他费用总额

tableidMap['defitem8'] = 'accom_detail';//会议场租费
tableidMap['defitem9'] = 'meals_detail';//会议餐费金额
tableidMap['defitem10'] = 'trans_detail';//其他费用总额

/*** 
 * 
 * 明细弹框
 * 
 * ***/
export class Detail extends React.Component {

    constructor(props) {
        super(props);
        this.state={   
    }

    var fieldCode = this.props.fieldCode;
    tableid = tableidMap[fieldCode];

    this.initTemplate(props,()=>{
        this.getTableData();//获取表格数据
    });
    
}

//加载模板信息
initTemplate(props,callback){
    props.createUIDom(
        {
            pagecode:pagecode
        },
        (data)=>{
            let meta = data.template;
            props.meta.setMeta(meta,callback);

        }
    );

}

//获取表格数据
getTableData(){
    this.props.editTable.setTableData(tableid,{rows:[{values:this.props.record.values}]});
}

    render() {
        let { editTable } = this.props;
        let { createEditTable } = editTable;

        return (
            <div className="nc-single-table">
                <div className="nc-singleTable-table-area">{/* 列表区 */}
                    {createEditTable(tableid,{
                        useFixedHeader:true,
                        showIndex:true,
                        showCheck:true,
                        isAddRow:true
                    })}
                </div>
            </div>
        );    
    }
}

Detail = createPage({
    billinfo:{
        billtype:'grid',
        pagecode:pagecode,
        bodycode:tableid
      },
    initTemplate: ()=>{}
})(Detail);
