
import {ajax,base,toast} from 'nc-lightapp-front';
import requestApi from '../requestApi';
import {headSumHandle} from './commonEvent';
import {updataFormData, updatacardTableData} from 'ssccommon/utils/changePageData';
const {NCMessage} = base;


function afterEvent(props,moduleId,key,value, changedrows,flag,callback) {
    if(value==null)
    if(moduleId =='mtapp_detail' && key=='orig_amount'){
        props.cardTable.setValByKeyAndRowId(moduleId, changedrows.rowid, key, { value: 0.00, display: 0.00 })
        value ={ value: 0.00, display: 0.00 };
    }else{
        return;//值未改变时，直接返回不处理变更
    }
    if(value.value!=null && value.value!=undefined && value.value===changedrows.value)
    return;//值未改变时，直接返回不处理变更
    console.log('编辑后事件',value.value, changedrows.value);
    //触发参照过滤重算
    let allData = props.createMasterChildData(window.presetVar.pageId, window.presetVar.head.head1, window.presetVar.body.body1);
    let headValues = allData.head[presetVar.head.head1].rows[0].values;
    window.presetVar.headValuesAll = headValues;
    //requestApi.handleLoadDataResDada(props.meta.getMeta(), {data:allData});
    
    switch(moduleId) {
        case 'mtapp_detail' :
            //满足报销标准核心控制项字段影响因素
            if(key!=window.presetVar.showitemkey && (window.presetVar.reimsrc!=null && window.presetVar.reimsrc.length>0 && window.presetVar.reimsrc.includes(key))){
                //清空当前行报销标准的值，以重新获取
                props.cardTable.setValByKeyAndRowId(presetVar.body.body1,changedrows[0].rowid,window.presetVar.showitemkey,{value:0.00, display: 0.00});

                let headBodyData = props.createBodyAfterEventData(window.presetVar.pageCode, presetVar.head.head1, presetVar.body.body1,  moduleId, key, changedrows)
                console.log('表体字段change后，表头表体数据结构', headBodyData);
                let data={
                    ...headBodyData,
                    checkrule:'true',
                    templateid: props.meta.getMeta().pageid
                }
                requestApi.changeByBody({
                    data: data,
                    success: (data)=> {
                        // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                        // //设置表头数据
                        // props.form.setAllFormValue({[presetVar.head.head1]: data.data.head[presetVar.head.head1]});
                        // //设置表体数据
                        // props.cardTable.setTableData(presetVar.body.body1, data.data.body[presetVar.body.body1]);
                        //设置表头数据
                        props.beforeUpdatePage();
                        updataFormData(props, presetVar.head.head1, data.data.head[presetVar.head.head1]);
                        //设置表体数据
                        updatacardTableData(props, presetVar.body.body1, data.data.body[presetVar.body.body1]);
                        props.updatePage(presetVar.head.head1, presetVar.body.body1);
                        // BTM 性能优化，变更页面数据更新方法
                    }
                })
                return;//满足报销标准核心控制项字段影响因素的字段直接返回
            }
            switch (key) {
                case 'orig_amount' :
                case 'org_currinfo' :
                case 'assume_org' :
                case 'assume_dept' :
                case 'pk_currtype' :
                //chenylr,begin===
                case 'pk_iobsclass':
                case 'defitem6':
                case 'defitem4':
                case 'defitem9':
                case 'defitem29':
                case 'defitem1':
                case 'defitem3':
                case 'defitem5':
                case 'defitem25':
                case 'defitem28':
                case 'defitem27':
                case 'defitem26': //业务招待级别 
                case 'defitem12': //陪同人员 
                //chenylr,end===
                    
                let headBodyData = props.createBodyAfterEventData(window.presetVar.pageCode, presetVar.head.head1, presetVar.body.body1,moduleId, key, changedrows)
                // TOP NCCLOUD-91597 校验报销标准取模板时需要使用模板ID chengwbc ADD
                headBodyData.templateid = props.meta.getMeta().pageid;
                // BTM NCCLOUD-91597 校验报销标准取模板时需要使用模板ID
                    console.log('表体字段change后，表头表体数据结构', headBodyData);

                    requestApi.changeByBody({
                        data: headBodyData,
                        success: (data)=> {
                                // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                                // //设置表头数据
                                // props.form.setAllFormValue({[presetVar.head.head1]: data.data.head[presetVar.head.head1]});
                                // //设置表体数据
                                // props.cardTable.setTableData(presetVar.body.body1, data.data.body[presetVar.body.body1]);
                                props.beforeUpdatePage();
                                //设置表头数据
                                updataFormData(props, presetVar.head.head1, data.data.head[presetVar.head.head1]);
                                //设置表体数据
                                updatacardTableData(props, presetVar.body.body1, data.data.body[presetVar.body.body1]);
                                props.updatePage(presetVar.head.head1, presetVar.body.body1);
     
                        }
                    })
                    break;
                case 'defitem1':
                case 'defitem2':
                case 'defitem3':
                case 'defitem5':
                case 'defitem13':
                case 'defitem16':
                case 'defitem17':
                case 'defitem19':
                case 'defitem20':
                case 'defitem24':
                case 'defitem25':
                case 'defitem27':
                case 'defitem28':
                case 'defitem29':
                case 'defitem30':
                case 'defitem34':
                    if(props.form.getFormItemsValue('head','pk_tradetype').value === '261X-Cxx-001' && !checkValue.call(this,key)){
                        setTableValue.call(this,key,'');
                        return;
                    }
                    calculate.call(this);
                break;
            }
            break;
        case 'head' :
            switch(key) {
                case 'pk_org': //组织
                case 'billdate': //单据日期
                case 'apply_org': //申请单位
                case 'org_currinfo'://汇率
                case 'apply_dept': //申请部门
                case 'pk_iobsclass': //收支项目
                case 'billmaker': //申请人
                case 'pk_currtype': //币种
                //chenylr,begin
                case 'defitem2': //开会地点


                    //表头修改后数据后台需要的结构， 一主一子  一主多子
                    let headBodyData = props.createHeadAfterEventData(window.presetVar.pageCode, presetVar.head.head1, presetVar.body.body1, moduleId, key, value);
                    // TOP NCCLOUD-91597 校验报销标准取模板时需要使用模板ID chengwbc ADD
                    headBodyData.templateid = props.meta.getMeta().pageid;
                    // BTM NCCLOUD-91597 校验报销标准取模板时需要使用模板ID
                    console.log('表头字段change后表头标题数据结构：', headBodyData)


                    requestApi.changeByHead({
                        data: headBodyData,
                        success: (data)=> {
                            // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                            // //设置表头数据
                            // props.form.setAllFormValue({[presetVar.head.head1]: data.data.head[presetVar.head.head1]});
                            // //设置表体数据
                            // props.cardTable.setTableData(presetVar.body.body1, data.data.body[presetVar.body.body1]);
                            props.beforeUpdatePage();
                            //设置表头数据
                            updataFormData(props, presetVar.head.head1, data.data.head[presetVar.head.head1]);
                            //设置表体数据
                            updatacardTableData(props, presetVar.body.body1, data.data.body[presetVar.body.body1]);
                            // BTM 性能优化，变更页面数据更新方法
                            props.updatePage(presetVar.head.head1, presetVar.body.body1);
                            
                            let org_currinfo = props.form.getFormItemsValue(presetVar.head.head1,'org_currinfo');

                            if(key=='pk_currtype' && parseFloat(org_currinfo.value) == parseFloat(1) )
                            {
                                props.form.setFormItemsDisabled(presetVar.head.head1,{'org_currinfo':true});
                            }else{
                                props.form.setFormItemsDisabled(presetVar.head.head1,{'org_currinfo':false});
                            }
                            
                            props.cardTable.setTableDataWithResetInitValue(presetVar.body.body1, data.data.body[presetVar.body.body1]);
                        }
                    })

                    break;
            }
            break;
    }
}

export default afterEvent;

function checkValue (key){

    let departDate = getTableValue.call(this,'defitem1'); //出发日期
    let arrivalDate = getTableValue.call(this,'defitem2');//返程日期 
    
    if(departDate !=0 && arrivalDate != 0 && departDate.slice(0,10) >  arrivalDate.slice(0,10)){
        toast({ color: 'danger', content: this.props.MutiInit.getIntl(2011).get('201101GRSQ-0002')});//开始日期不能晚于结束日期
        return false;
    }

    return true;
}

function calculate (){

    let tradetype = this.props.form.getFormItemsValue('head','pk_tradetype').value || '';

    switch (tradetype) {
        
        case '261X-Cxx-001'://差旅费申请单
            tripCalculate.call(this);
            break;
        case '261X-Cxx-008':    //会务费申请单（5000以上）
            meetingCalculate.call(this);
            break;
        default:
            break;
    }

    // let changedrows = this.props.cardTable.getVisibleRows('mtapp_detail');
    // window.__ssc_BillStyleVar.tableRowAndItemCount('mtapp_detail', 'all', changedrows);

    if(!this.state.modelEdit || this.state.modelCloseEvent){
        let moduleId = 'mtapp_detail';
        let key = 'orig_amount'
        let changedrows =  this.props.cardTable.getRowsByIndexs(moduleId, 0);
        let value = this.props.cardTable.getValByKeyAndIndex(moduleId, 0,key);
        afterEvent.call(this,this.props,moduleId,key,value , changedrows);
        // headSumHandle.call(this,this.props);
        this.state.modelCloseEvent = false;
        // this.setState({modelCloseEvent:false});
    }
}

function tripCalculate(){
    
    let departDate = getTableValue.call(this,'defitem1'); //出发日期
    let arrivalDate = getTableValue.call(this,'defitem2');//返程日期 
    let daysOnBusiness = GetNumberOfDays(departDate,arrivalDate); //出差天数

    let hotelStandard = getTableFloatValue.call(this,'defitem29'); //申请住宿金额
    let hotelCost = daysOnBusiness<1?0:hotelStandard*(daysOnBusiness-1);    //住宿费

    let foodStandard = getTableFloatValue.call(this,'defitem28');  //申请餐费金额 餐补标准
    let foodCost = foodStandard*daysOnBusiness; //餐补费用

    let insurance = getTableFloatValue.call(this,'defitem34')  //意外保险费
    let otherCost = getTableFloatValue.call(this,'defitem30')  //其他费用
    let otherFees = insurance+otherCost;

    let kilometre = getTableFloatValue.call(this,'defitem13');  //自驾公里数
    let oilCost = kilometre*1;  //油费
    let trainCost = getTableFloatValue.call(this,'defitem16'); // 火车费
    let busCost = getTableFloatValue.call(this,'defitem17');   // 长途汽车费
    let taxiCost = getTableFloatValue.call(this,'defitem19');  //目的地市内汽车费
    let roadToll = getTableFloatValue.call(this,'defitem20');  //过路费
    let stationRoundtrip = getTableFloatValue.call(this,'defitem24');  //往返火车机场费用
    let travelCost = oilCost+trainCost+busCost+taxiCost+roadToll+stationRoundtrip;  //交通费用

    let amount = hotelCost+foodCost+otherFees+travelCost;
    setTableValue.call(this,'defitem9',daysOnBusiness);//出差天数
    setTableValue.call(this,'defitem21',oilCost);  //油费
    setTableValue.call(this,'defitem22',hotelCost); //住宿费
    setTableValue.call(this,'defitem23',foodCost);  //餐补
    setTableValue.call(this,'defitem25',otherFees); //其他费用
    setTableValue.call(this,'defitem27',travelCost); // 交通费
    setTableValue.call(this,'orig_amount',amount);  //合计金额
}

function meetingCalculate(){

    let personNums = getTableFloatValue.call(this,'defitem1');  //参会人数
    let meetingNums = getTableFloatValue.call(this,'defitem3'); // 会议场数
    let mealNums = getTableFloatValue.call(this,'defitem5')     // 会议餐数
    let rentalCost = getTableFloatValue.call(this,'defitem25');  // 实际场租费金额
    let otherCost = getTableFloatValue.call(this,'defitem27');  // 实际其他费用
    let mealCost = getTableFloatValue.call(this,'defitem28');  // 实际会议餐费金额

    let rentalTotalCost = personNums*meetingNums* rentalCost;
    setTableValue.call(this,'defitem8',rentalTotalCost); //会务场租费总金额
    let mealTotalCost = personNums*mealCost*mealNums;
    setTableValue.call(this,'defitem9',mealTotalCost);    //会议餐费总金额
    let otherTotalCost = personNums*otherCost;          
    setTableValue.call(this,'defitem10',otherTotalCost);    //其他费用
    let totalCost = rentalTotalCost+mealTotalCost+otherTotalCost;
    setTableValue.call(this,'orig_amount',totalCost);
}

function getTableValue(key){
    let index = this.props.cardTable.getCurrentIndex('mtapp_detail');
    let value = this.props.cardTable.getValByKeyAndIndex('mtapp_detail',index,key);
    return (value&&value.value)||0.00;
}

function getTableFloatValue(key){
    return parseFloat(getTableValue.call(this,key));
}

function setTableValue(key,value){
    if(getTableValue.call(this,key) === value)
        return;
    let index = this.props.cardTable.getCurrentIndex('mtapp_detail');
    this.props.cardTable.setValByKeyAndIndex('mtapp_detail',index,key,{value:value,display:value})
}

function GetNumberOfDays(date1,date2){//获得天数
    if(!date1 || !date2 || date1 === 0.00 || date2 ===0.00)
        return 0;
    //date1：开始日期，date2结束日期
    var a1 = Date.parse(new Date(date1.slice(0,10)));
    var a2 = Date.parse(new Date(date2.slice(0.10)));
    var day = parseInt((a2-a1)/ (1000 * 60 * 60 * 24));//核心：时间戳相减，然后除以天数
    return day+1
};
