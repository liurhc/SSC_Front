/**
 * 表格肩部按钮click事件
 */
import {ajax} from 'nc-lightapp-front';
import afterEvent from './afterEvent';

const areaBtnAction = window.presetVar.areaBtnAction;
const statusVar = window.presetVar.status;
const {body1, body2} = window.presetVar.body;
import { headSumHandle } from "./commonEvent";

function tableButtonClick(selectindex) {
    return {
        [body1 + areaBtnAction.add] : () => {
            this.props.cardTable.addRow(body1, undefined, null, false);
        },
        [body1 + areaBtnAction.save] : () => {
            this.props.cardTable.setStatus(body1, statusVar.browse);
        },
        [body1 + areaBtnAction.del] : () => {
            let data = this.props.cardTable.getCheckedRows(body1);

            window.__ssc_BillStyleVar.tableRowAndItemCount(body1, 'delBySq', 0);

            data.forEach((item)=>{
                this.props.cardTable.delRowByRowId(body1, item.data.rowid);
            })
            let changedrows =  this.props.cardTable.getRowsByIndexs(body1, 0);
            if(changedrows.length == 0) {
                // this.props.cardTable.setValByKeyAndIndex('mtapp_detail',0,'orig_amount',{value:0,display:0})
                headSumHandle.call(this,this.props);
            }else {
                let value = this.props.cardTable.getValByKeyAndIndex(body1, 0,'orig_amount');
                let prop = this.props;
                afterEvent.call(this,prop,body1,'orig_amount',value , changedrows);
            }
        },
        [body1 + areaBtnAction.edit] : () => {
            this.props.cardTable.setStatus(body1, statusVar.edit);
        }
    }
}

export default tableButtonClick;