import {dataHandle} from '../initTemplate';
import requestApi from '../requestApi';


export default function () {
    let props = this.props;
    let billdata =  props.createMasterChildData(window.presetVar.pageId, window.presetVar.head.head1, window.presetVar.body.body1);
    let billId = billdata.head.head.rows[0].values.pk_mtapp_bill.value || props.getUrlParam('id') || "";
        requestApi.query({
            data: {
                "openbillid": billId,
                "pagecode": window.presetVar.pageCode
            },
            success: (data) => {
                if (data.data.head.head.rows[0].values.pk_tradetype.value == '261X-Cxx-001') {
                    data.data.body.mtapp_detail.rows.forEach((row) => {
                        if(row.values.defitem12);
                        if(row.values.defitem12.value == 'Y'){
                            row.values.defitem12.display = '是'
                        }else if(row.values.defitem12.value == 'N'){
                            row.values.defitem12.display = '否'
                        }
                    });
                }
                //处理按钮状态
                dataHandle(data.data.head[window.presetVar.head.head1],props);

                props.form.setAllFormValue({[window.presetVar.head.head1]: data.data.head[window.presetVar.head.head1]});
                //设置表体数据
                props.cardTable.setTableData(window.presetVar.body.body1, data.data.body[window.presetVar.body.body1]);
                this.pubMessage.refreshSuccess();

           }
        })

}
