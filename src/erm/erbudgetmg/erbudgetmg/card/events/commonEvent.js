import presetVar from '../presetVar';
const body1 = window.presetVar.body.body1;
const head1 = window.presetVar.head.head1;

/**
 * 表头合计处理
 */
export function headSumHandle(prop) {
    let busiitemfield = ['orig_amount', 'rest_amount', 'exe_amount', 'org_amount', 'max_amount',
    'org_exe_amount', 'org_rest_amount', 'group_amount', 'group_rest_amount',
    'group_exe_amount', 'global_amount', 'global_rest_amount', 'global_exe_amount'];
    let busiitemfieldTotal = [0, 0, 0, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 0];
    let rows =  prop.cardTable.getAllRows(body1, false);
    if(rows && rows[0] && rows.length>0){
        for(let m=0;m<rows.length;m++)
        if(rows[m] && rows[m].status!=3)
        {
            for(let i=0;i<busiitemfield.length;i++){
                let countmouny = rows[m].values[busiitemfield[i]].value;
                if(countmouny==null || countmouny=='')
                {
                    countmouny=0;
                }
                busiitemfieldTotal[i]  +=+ countmouny;
            }

        }
   
    }

    for(let i=0;i<busiitemfieldTotal.length;i++){
        let key = busiitemfield[i];
        let changeHeadItem = {
            [busiitemfield[i]]:{ 
                value: busiitemfieldTotal[i],
                display: busiitemfieldTotal[i].toFixed(prop.form.getFormItemsValue(head1,busiitemfield[i]).scale==null?2:prop.form.getFormItemsValue(head1,busiitemfield[i]).scale),
                scale:prop.form.getFormItemsValue(head1,busiitemfield[i]).scale
            }
        }
        prop.form.setFormItemsValue(head1,changeHeadItem);
   }
}
