import linkQueryApi from '../../../../public/components/linkquery/linkquerybills'
export default function (props,record, index) {
    linkQueryApi.link({
        data : {
            openbillid : record.pk_jkbx.value,
            tradetype : record.djlxbm.value,
            props : props
        }
    });
}