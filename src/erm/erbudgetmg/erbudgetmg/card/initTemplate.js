/**
 * 页面初始化逻辑：模板及初始化数据的配置及加载
 */
import {base, high, ajax, getAttachments,getMultiLang} from 'nc-lightapp-front';
import requestApi from './requestApi';
import {setTableExtendCol} from 'ssccommon/components/bill';
import {tableExtendButtonClick} from './events';
import linkQueryApi from '../../../public/components/linkquery/linkquerybills'

import buttonTpl from './buttonTpl';
import { Detail } from '../detail';
const head1 = window.presetVar.head.head1;
const body1 = window.presetVar.body.body1;
const pageButton = presetVar.pageButton;
const statusVar = window.presetVar.status;
const areaBtnAction = window.presetVar.areaBtnAction;
let alreadyLoadData = false;
export default function (props) {
    let pageStatus = props.getUrlParam('status');
    let appcodeParam = props.getUrlParam("appcode");
    let pagecodeParam = props.getUrlParam("pagecode");
    window.presetVar.appCode = (appcodeParam==undefined||appcodeParam=='')?props.getSearchParam("c"):appcodeParam;
    window.presetVar.pageCode =  (pagecodeParam==undefined||pagecodeParam=='')?props.getSearchParam("p"):pagecodeParam;

    //注意：以后模板的获取需要用 props.createUIDom 替代。

    let createUIDomPromise = new Promise((resolve, reject) => {
        props.createUIDom(
            {},
            (data) => {
                resolve(data);   
            }
        )
    })

    let getMultiLangPromise = new Promise((resolve, reject) => {
        getMultiLang({
            moduleId: 2011, domainName: 'erm', currentLocale: 'simpchn', callback: (json) => {
                resolve(json);
            }
        });

    })

    Promise.all([createUIDomPromise, getMultiLangPromise]).then((resultList) => {
        let data = resultList[0];
        let json = resultList[1];
        let meta = data.template;
        let button = data.button;
        // TOP 浏览态也需要显示操作列 ADD
        // 设置按钮模板

        let id = props.getUrlParam('id') ? props.getUrlParam('id') : this.state.billId;
        id = id == "" ? 0 : id;
        getAttachments(id).then(lists => {
            if (lists.length == 0) {
                lists = 0;
            }
            props.button.setButtonTitle('fileManager', json['201102SQFY-0031'] + ' ' + lists + ' ');  //附件
        })

        props.button.hideButtonsByAreas(head1);
        props.button.setButtons(button);
        // 设置表格扩展列
        setTableExtendCol(props, meta, button, [{
            tableAreaId: body1,
            onButtonClick: tableExtendButtonClick.bind(this),
            copyRowWithoutCols: ['pk_mtapp_detail']
        }]);
        // BTM 浏览态也需要显示操作列
        let transtype = data.context.paramMap.transtype;
        this.setState({ Title: data.context.paramMap.transtype_name });
        let tradetypeParam =  props.getUrlParam("tradetype");
        window.presetVar.billtype= (tradetypeParam==undefined||tradetypeParam==null||tradetypeParam=='')?transtype:tradetypeParam;    
       
        //let meta = data.data;
        presetVar.pageId = meta.pageid;
        if (props.getUrlParam('status') == statusVar.browse) {

            //模板处理
            tplHandle.call(this, meta,button);
           
            //业务数据查询
            let billId = props.getUrlParam('id');
            requestApi.query({
                data: {
                    "openbillid": billId,
                    "pagecode": window.presetVar.pageCode
                },
                success: (data) => {
                    if (data.data.head.head.rows[0].values.pk_tradetype.value == '261X-Cxx-001') {
                        data.data.body.mtapp_detail.rows.forEach((row) => {
                            if(row.values.defitem12);
                            if(row.values.defitem12.value == 'Y'){
                                row.values.defitem12.display = '是'
                            }else if(row.values.defitem12.value == 'N'){
                                row.values.defitem12.display = '否'
                            }
                        });
                    }
                    this.setState({
                        tableData:data.data
                    })
                    props.form.setAllFormValue({[head1]: data.data.head[head1]});
                    //设置表体数据
                    props.cardTable.setTableDataWithResetInitValue(body1, data.data.body[body1]);
                    //处理按钮相关状态
                    dataHandle(data.data.head[head1],props);
                }
            })

        } else {
            //加载模板默认数据
            let metadata={
                meta:meta,
                props:props
            }
            requestApi.loadData({
                ...metadata,
                success: (data) => {
                    alreadyLoadData = true;
                    //模板处理
                    tplHandle.call(this, meta,button);

                    //复制操作
                    let copyFromBillId = props.getUrlParam('copyFromBillId');
                    //业务数据查询 (修改操作)
                    let billId = props.getUrlParam('id');
                    if (copyFromBillId) {
                        requestApi.copy({
                            data: {
                                openbillid: copyFromBillId,
                            },
                            success: (data) => {
                                props.form.setAllFormValue({[head1]: data.data.head[head1]});
                                //设置表体数据
                                props.cardTable.setTableData(body1, data.data.body[body1]);
                                props.cardTable.setStatus(head1, statusVar.edit);
                                props.cardTable.setStatus(body1, statusVar.edit);
                                //处理按钮相关状态
                                dataHandle(data.data.head[head1],props);
                            }
                        })
                    }else if (billId) {
                        requestApi.query({
                            data: {
                                "openbillid": billId,
                                "pagecode": window.presetVar.pageCode
                            },
                            success: (data) => {
                                props.form.setAllFormValue({[head1]: data.data.head[head1]});
                                //设置表体数据
                                props.cardTable.setTableDataWithResetInitValue(body1, data.data.body[body1]);

                            }
                        })
                    }else{
                        props.cardTable.addRow(body1, undefined, undefined, false);
                    }

                    
     
      

                    let org_currinfo = this.props.form.getFormItemsValue(window.presetVar.head.head1,'org_currinfo');
                    if( parseFloat(org_currinfo.value) == parseFloat(1) ||  pageStatus == statusVar.browse)
                    {
                        this.props.form.setFormItemsDisabled(window.presetVar.head.head1,{'org_currinfo':true});
                    }else{
                        this.props.form.setFormItemsDisabled(window.presetVar.head.head1,{'org_currinfo':false});
                    }


                }
            });

        }
    })

 /*    props.createUIDom(
        {
            // TOP 不需要传参 DEL
            // pagecode: window.presetVar.pageCode,
            // appcode: window.presetVar.appCode
            // BTM 不需要传参
        },
         (data) => {
            getMultiLang({moduleId: 2011, domainName: 'erm',currentLocale: 'simpchn', callback: (json) => {

        }})
        }
    ); */

    function tplHandle(meta,button) {

        // TOP 浏览态也需要显示操作列 DEL
        // //设置表格扩展列
        //   setTableExtendCol(props, meta, button, [{
        //         tableAreaId: body1,
        //         onButtonClick: tableExtendButtonClick.bind(this),
        //         copyRowWithoutCols: ['pk_mtapp_detail']
        //     }]);
        // //按钮存入组件
        // props.button.setButtons(button);//注意：以后按钮模板的获取需要用 props.createUIDom 替代。
        // if (statusVar.browse == pageStatus) {
        // setTimeout(() => {
        //         props.cardTable.hideColByKey(body1, 'opr'); //隐藏扩展列
        // }, 10);
        // }
        // BTM 浏览态也需要显示操作列
        //肩部按钮可见状态：页面和表格的肩部按钮，这里逻辑根据实际业务实现。
        let btnsVisibleObj = {};
        let isAddOrEditStatus =  pageStatus == statusVar.edit || pageStatus == statusVar.add;
        let isBrowseStatus = pageStatus == statusVar.browse
        
        //如果下列按钮业务逻辑固定，后续可以封装到组件中。
        for (let btnKey in pageButton) {
            btnsVisibleObj[btnKey] = false;
        }
        if (isAddOrEditStatus) { //添加或编辑态 隐藏下列按钮
            btnsVisibleObj[pageButton.pageSave] = true;
            btnsVisibleObj[pageButton.billSubmit] = false;
            btnsVisibleObj[pageButton.billSaveSubmit] = true;
            btnsVisibleObj[pageButton.fileManager] = true;
            btnsVisibleObj[pageButton['mtapp_detail_Add']] = true;
            btnsVisibleObj['mtapp_detail-del'] = true;
            // TOP 浏览态也需要显示操作列 ADD
            btnsVisibleObj['mtapp_detail_Delete'] = true;
            btnsVisibleObj['mtapp_detail_Copy'] = true;
            btnsVisibleObj['mtapp_detail_Insert'] = true;
            // BTM 浏览态也需要显示操作列
            btnsVisibleObj[pageButton['linkVoucher']] = false;
            btnsVisibleObj[pageButton['linkBudget']] = false;
            btnsVisibleObj['buttongroup4_m'] = false;
            //新增态时取消按钮显示
           if(pageStatus == statusVar.add || pageStatus == statusVar.edit)
           btnsVisibleObj[pageButton.pageClose] = true;

        }
        if (isBrowseStatus) { //浏览态 隐藏下列按钮
            btnsVisibleObj[pageButton.pageEdit] = true;
            btnsVisibleObj[pageButton.pageCopy] = true;
            btnsVisibleObj[pageButton.pageDel] = true;
            btnsVisibleObj[pageButton.pagePrint] = true;
            btnsVisibleObj[pageButton.imageShow] = true;
            btnsVisibleObj[pageButton.imageUpload] = true;
            btnsVisibleObj[pageButton.billView] = true;
            btnsVisibleObj[pageButton['linkVoucher']] = false;
            btnsVisibleObj[pageButton['linkBudget']] = false;
            btnsVisibleObj[pageButton.billSaveSubmit] = false;
            btnsVisibleObj[pageButton.pageClose] = false;
            btnsVisibleObj['buttongroup4_m'] = true;
        }

        props.button.setButtonsVisible(btnsVisibleObj);

        //table扩展按钮弹窗确认框设置
         let multiLang = props.MutiInit.getIntl(2011);
         props.button.setPopContent(body1 + areaBtnAction.del,multiLang && multiLang.get('201102SQFY-0032')/**'确认要删除该信息吗？'**/) /* 设置操作列上删除按钮的弹窗提示 */

        switch(pageStatus) {
            case statusVar.edit:
                meta[head1].status = statusVar.edit;
                meta[body1].status = statusVar.edit;
                break;
            case statusVar.add:
                meta[head1].status = statusVar.add;
                meta[body1].status = statusVar.add;
                break;
            case statusVar.browse:
                meta[head1].status = statusVar.browse;
                meta[body1].status = statusVar.browse;
                break;
        }
        
        meta = showDetailModal(props,meta,window.presetVar.billtype); // 明细弹框
        //设置模板
        props.meta.setMeta(meta);
    }

    props.setHashChangeCallback(() => {
        let pagestatusVar = props.getUrlParam('status'); //重新设置状态变量
        //openBillId = props.getUrlParam('id'); //重设单据id
        //presetVar.openBillId = openBillId;
        window.presetVar.status = pagestatusVar;

        let isShowTableExtCol = false; //是否显示表格扩展列
        switch (pagestatusVar) {
            case statusVar.browse:
                isShowTableExtCol = false;
                break;
            case statusVar.add:
            case statusVar.edit:
                isShowTableExtCol = true;

                let metadata = {
                    meta: props.meta.getMeta(),
                    props: props
                }
                //只有以添加/编辑态打开单据时，才会在初始化函数中调用loadData请求，所以如果从浏览态直接打开页面时，是不会加载loaddata的，所以如果页面此时从浏览态转到添加/编辑态，需要加载loadData
                if (!alreadyLoadData) {
                    requestApi.loadData({
                        ...metadata,
                        success: (meta) => {
                            alreadyLoadData = true;
                            props.meta.setMeta(meta);
                        }
                    })
                }
                break;
        }

        // TOP 浏览态也需要显示操作列 DEL
        //浏览态不显示表体扩展列
        //debugger;
        // let isBrowseStatus = pageStatus == statusVar.browse

        // if(isBrowseStatus) {
        //     props.cardTable.hideColByKey("mtapp_detail", 'opr');
        // }else {
        //     props.cardTable.showColByKey("mtapp_detail", 'opr');
        // }
        if (isShowTableExtCol) {
            props.cardTable.showColByKey("mtapp_detail", 'opr'); //显示扩展列
        } else {
            props.cardTable.hideColByKey("mtapp_detail", 'opr'); //隐藏扩展列
        }

        // BTM 浏览态也需要显示操作列

        let copyFromBillId = props.getUrlParam('copyFromBillId');
        if (copyFromBillId) {
            requestApi.copy({
                data: {
                    openbillid: copyFromBillId,
                },
                success: (data) => {
                    props.form.setAllFormValue({ [head1]: data.data.head[head1] });
                    //设置表体数据
                    props.cardTable.setTableData(body1, data.data.body[body1]);
                    props.cardTable.setStatus(head1, statusVar.edit);
                    props.cardTable.setStatus(body1, statusVar.edit);
                    //处理按钮相关状态
                    dataHandle(data.data.head[head1], props);

                    //处理可编辑状态 
                    changeComStatus(pagestatusVar);
                }
            })//复制单据。 因为复制单据操作 既能在初始态触发，又可以被交互态触发。所以没有把该函数写在按钮事件上。
        } else {
            let headData = props.form.getAllFormValue(head1);
            //根据单据状态，改变按钮的显示的状态
            dataHandle(headData, props);
        }

        //处理可编辑状态
        changeComStatus(pagestatusVar);

    })

//改变表头表体组件的编辑状态
function changeComStatus(pageStatus) {
        let changeStatus = null;
        switch (pageStatus) {
            case statusVar.edit:
            changeStatus  = statusVar.edit;
            break;
            case statusVar.add:
            changeStatus  = statusVar.add;
            break;
            case statusVar.browse:
            changeStatus = statusVar.browse;
            break;
        }
        props.form.setFormStatus(head1, changeStatus);
        props.cardTable.setStatus(body1, changeStatus);
        
}

//结尾
}

/**
 * 明细弹框
 * @param {*} props 
 * @param {*} meta 
 * @param {*} tradetype 
 */
function showDetailModal(props,meta,tradetype){

    var modalTitleMap = new Map(); // 弹框标题信息
    if(tradetype === '261X-Cxx-001' || tradetype === '261X-Cxx-009'){ //差旅费申请、差旅费超标申请
        modalTitleMap.set('defitem22','住宿费明细');
        modalTitleMap.set('defitem23','餐费明细');
        modalTitleMap.set('defitem27','交通费明细');
        modalTitleMap.set('defitem25','其他费用总额');
    } else if(tradetype === '261X-Cxx-008'){ // 会务费申请单（5000以上）
        modalTitleMap.set('defitem8','会议场租费');
        modalTitleMap.set('defitem9','会议餐费金额');
        modalTitleMap.set('defitem10','其他费用总额');
    } else {
        return meta; // 其他交易类型直接返回
    }
    meta['mtapp_detail'].items.forEach((item,index)=>{
        if(modalTitleMap.has(item.attrcode)){
            item.itemtype = 'customer';
            item.render = (text, record,index) => {
                return (
                    <a
                        style={{ textDecoration: 'none', cursor: 'pointer' }}
                        onClick={() => {
                                props.modal.show('detail', {
                                title: modalTitleMap.get(item.attrcode),
                                content: <Detail fieldCode={item.attrcode} record={record}/>
                            });
                        }}
                        >
                        {record.values[item.attrcode].display || record.values[item.attrcode].value}{/*待改进*/}
                    </a>
                );
            };
        }
    });
    return meta;
}

function dataHandle(head,props) {
    //处理数据和按钮状态
    if(head==null||head.rows[0]==null|| head.rows[0].values==null)
    return;
    let scene = props.getUrlParam('scene');
    let deal = props.getUrlParam("deal");
    let sxbz = head.rows[0].values.effectstatus.value;//单据生效状态
    let spzt = head.rows[0].values.apprstatus.value;//审批状态
    let djzt =head.rows[0].values.billstatus.value//单据状态
    let gbzt = head.rows[0].values.close_status.value;//关闭状态
    let btnsVisibleObj = {};
    let pageStatus = props.getUrlParam('status');
    let isAddOrEditStatus =  pageStatus == statusVar.edit || pageStatus == statusVar.add;
    if(isAddOrEditStatus)
    {
        //如果下列按钮业务逻辑固定，后续可以封装到组件中。
        for (let btnKey in pageButton) {
            btnsVisibleObj[btnKey] = false;
        }
        //单据提交之后不显示保存提交按钮
        if(spzt==-1 || spzt==null){           
            btnsVisibleObj[pageButton.billSaveSubmit] = true;
        }else{
            btnsVisibleObj[pageButton.billSaveSubmit] = false;
        }
        btnsVisibleObj[pageButton.pageSave] = true;
        btnsVisibleObj[pageButton.billSubmit] = false;
        btnsVisibleObj[pageButton.fileManager] = true;
        btnsVisibleObj[pageButton['mtapp_detail_Add']] = true;
        btnsVisibleObj['mtapp_detail-del'] = true;
        // TOP 浏览态也需要显示操作列 ADD
        btnsVisibleObj['mtapp_detail_Delete'] = true;
        btnsVisibleObj['mtapp_detail_Copy'] = true;
        btnsVisibleObj['mtapp_detail_Insert'] = true;
        // BTM 浏览态也需要显示操作列
        btnsVisibleObj[pageButton['linkVoucher']] = false;
        btnsVisibleObj[pageButton['linkBudget']] = false;
        btnsVisibleObj['buttongroup4_m'] = false;
        //新增态时取消按钮显示
       if(pageStatus == statusVar.add || pageStatus == statusVar.edit)
       btnsVisibleObj[pageButton.pageClose] = true;
       //设置编辑状态
       props.button.setButtonsVisible(btnsVisibleObj);
       return;
    }

     let isBrowseStatus = pageStatus == statusVar.browse

    if(isBrowseStatus) {
        props.cardTable.hideColByKey("mtapp_detail", 'opr');
    }else {
        props.cardTable.showColByKey("mtapp_detail", 'opr');
    }

    for (let btnKey in pageButton) {
        btnsVisibleObj[btnKey] = false;

        //来源于审批人门户、作业处理时 显示 ：修改、影像扫描|影像查看、联查预算、联查凭证、更多（联查审批情况、联查单据、附件、关闭/重启、打印）
        if((scene == 'approve'  || scene == 'approvesce' || scene == 'zycl') ){
             if( spzt=="1" && (btnKey == pageButton.imageShow || btnKey == pageButton.imageUpload ||  btnKey == pageButton.pagePrint || btnKey == pageButton.billView || btnKey == pageButton.billApprove ))
             btnsVisibleObj[btnKey] = isBrowseStatus;
             else if((btnKey == pageButton.pageEdit || btnKey == pageButton.imageShow || btnKey == pageButton.imageUpload ||  btnKey == pageButton.pagePrint || btnKey == pageButton.billView || btnKey == pageButton.billApprove ))
             btnsVisibleObj[btnKey] = isBrowseStatus;
             //审批中心打开的单据点击修改时需要处理按钮状态
        }
        //来源于作业查询时 显示 ：影像查看、联查预算、联查凭证、更多（联查审批情况、联查单据、打印）
        else if((scene == 'zycx' || scene == 'bzcx'||scene == 'fycx')){
             if( (btnKey == pageButton.imageShow ||  btnKey == pageButton.pagePrint || btnKey == pageButton.billView || btnKey == pageButton.billApprove || btnKey == pageButton.fileManager))
             btnsVisibleObj[btnKey] = isBrowseStatus;
        }else{
        //审批状态为初始态，显示：修改|删除|复制、提交、影像扫描|影像查看、更多（附件、作废、打印）
        if(spzt=="-1" && (btnKey == pageButton.pagePrint || btnKey == pageButton.pageCopy || btnKey == pageButton.imageShow || btnKey == pageButton.imageUpload || btnKey == pageButton.billSubmit || btnKey == pageButton.billInvalid || btnKey == pageButton.pageEdit || btnKey == pageButton.pageDel)){
            btnsVisibleObj[btnKey] = isBrowseStatus;
        }
        //审批状态为提交态，显示：复制、收回、影像扫描|影像查看、更多（联查审批情况、附件、打印）
        else if((spzt=="3" || spzt=="2" ) && (btnKey == pageButton.pagePrint || btnKey == pageButton.pageCopy || btnKey == pageButton.imageShow || btnKey == pageButton.imageUpload || btnKey == pageButton.billApprove )){
                btnsVisibleObj[btnKey] = isBrowseStatus;
        }
        //审批状态为通过态，显示：复制、影像查看、更多（联查审批情况、联查单据、附件、关闭/重启、打印）
        else if( spzt=="1" && (btnKey == pageButton.pageCopy || btnKey == pageButton.imageShow ||  btnKey == pageButton.pagePrint || btnKey == pageButton.billView || btnKey == pageButton.billApprove )){
                btnsVisibleObj[btnKey] = isBrowseStatus;
        }
        }

        //已提交显示收回按钮
        if(btnKey == pageButton.billRecall && !(scene == 'approve' || scene == 'approvesce'|| scene == 'zycl' || scene == 'zycx' || scene == 'bzcx'|| scene == 'fycx')){
            if(spzt=="3"){
                btnsVisibleObj[btnKey] = isBrowseStatus;
            }else{
                btnsVisibleObj[btnKey] = false;
            }
        }            

        //已完成单据，隐藏影像扫描，附件管理
         if (btnKey == pageButton.imageUpload /*||btnKey == pageButton.fileManager*/){ 
            if(sxbz=="1"){
                if(scene != 'bzcx' && scene != 'fycx')
                btnsVisibleObj[btnKey] = false;
            }else{
                if(scene != 'zycx' && scene != 'bzcx' && scene != 'fycx'){
                    btnsVisibleObj[btnKey] = isBrowseStatus;
                }
            } 
        }
        //kangfw5 20191021已完成的单据现在也要显示附件按钮
        if (btnKey == pageButton.fileManager) {
            
            if(scene != 'zycx' && scene != 'bzcx' && scene != 'fycx'){
                btnsVisibleObj[btnKey] = isBrowseStatus;
            }
        }
        //已完成、未关闭的单据允许关闭
         if (btnKey == pageButton.billClose){ 
            if(sxbz=="1" && gbzt=="2" && (scene != 'zycx' && scene != 'bzcx' && scene != 'fycx')){
                btnsVisibleObj[btnKey] = isBrowseStatus;
            }else{
                btnsVisibleObj[btnKey] = false;
            } 
        }
      //已关闭的单据允许重启
        if (btnKey == pageButton.billEnable &&  (scene != 'zycx' && scene != 'bzcx' && scene != 'fycx')){ 
            if(sxbz=="1" && gbzt=="1"){
                btnsVisibleObj[btnKey] = isBrowseStatus;
            }else{
                btnsVisibleObj[btnKey] = false;
            }
        }

         //已完成单据显示联查功能
          if (btnKey == pageButton.billView){ 
            if(sxbz=="1"){
                btnsVisibleObj[btnKey] = true;
            }else{
                btnsVisibleObj[btnKey] = false;
            }
        }
        //审批中心或作业平台打开
        if( btnKey == "linkVoucher" || btnKey == "linkBudget"){
            if(scene == 'approve' || scene == 'approvesce' || scene == 'zycl' || scene == 'zycx' ||scene == 'fycx'){
                btnsVisibleObj[pageButton['linkVoucher']] = true;
                btnsVisibleObj[pageButton['linkBudget']] = true;
            }else{
                btnsVisibleObj[pageButton['linkVoucher']] = false;
                btnsVisibleObj[pageButton['linkBudget']] = false;
            }
        }

        //作废的单据影藏保存提交修改等状态
        if(djzt=="-1"){
                if (btnKey == pageButton.pageCopy ||btnKey == pageButton.imageShow ||btnKey == pageButton.pagePrint ){ 
                btnsVisibleObj[btnKey] = true;
                }else{
                btnsVisibleObj[btnKey] = false;
            }
        }

    }

    if(deal!=undefined&&(deal=='handon'||deal=='adjust'||deal=='sscreject'||deal=='handled')){
        btnsVisibleObj[pageButton.pageEdit] = false;
    }
    if( pageStatus == statusVar.browse)
    {
        props.button.setButtonsVisible({
            ['mtapp_detail_Add']: false,
            // TOP 浏览态也需要显示操作列 ADD
            ['mtapp_detail_Delete'] : false,
            ['mtapp_detail-del'] : false,
            ['mtapp_detail_Copy'] : false,
            ['mtapp_detail_Insert'] : false
            // BTM 浏览态也需要显示操作列
        });
    }else{
        props.button.setButtonsVisible({
            ['mtapp_detail_Add']: true,
            // TOP 浏览态也需要显示操作列 ADD
            ['mtapp_detail_Delete'] : true,
            ['mtapp_detail_Copy'] : true,
            ['mtapp_detail_Insert'] : true
            // BTM 浏览态也需要显示操作列
        });
    }


        //来源于报表联查的单据
        if(scene=='sscermlink'){
            for (let btnKey in pageButton) {
                if(  btnKey == pageButton.imageShow  ||  btnKey == pageButton.pagePrint ||  btnKey === pageButton.fileManager || btnKey == "LinkBudget" || btnKey == "LinkVoucher"  )
                 {
                    btnsVisibleObj[btnKey] = isBrowseStatus;
                 }else{
                    btnsVisibleObj[btnKey] = false;
                }
            }
        }
        
    props.button.setButtonsVisible(btnsVisibleObj);
}

export {dataHandle}