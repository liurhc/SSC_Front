/**
 * 全局变量设置文件
 */
import 'ssccommon/components/globalPresetVar';

//全局变量尽量配置在presetVar中，防止全局变量污染。也方便统一管理。
window.presetVar = {
    ...window.presetVar,
    pageId: '',
    pageCode: '201102SQFY_C',
    appCode: '201102SQFY',
    billtype: '2611',
    reimsrc: '',//缓存报销标准核心控制项
    showitemkey: '',//缓存报销标准显示项
    headValuesAll:'',//缓存表头数据
    bodyChangeValuesAll:'',//缓存表体数据
    //表头（主表）变量配置
    head: {
        head1: 'head',
        head2: ''
    },
    //表体（子表）变量配置
    body: {
        body1: 'mtapp_detail'
    },
    //搜索模板变量配置
    search: {},
    pageButton: {
        ...window.presetVar.pageButton,
        'mtapp_detail-add': 'mtapp_detail-add'
    }
};