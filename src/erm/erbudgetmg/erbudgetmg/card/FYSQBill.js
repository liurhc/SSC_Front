/**
 * 费用申请模块业务入口
 */
import './presetVar'
import React, {Component} from 'react';
import {createPage, base, high, getAttachments} from 'nc-lightapp-front';
import {tableButtonClick,pageButtonClick, afterEvent,onRowDoubleClick,refreshButtonClick} from './events';
import initTemplate from './initTemplate';
import {getAfterEventParams} from 'ssccommon/utils/formulaUtils'
import {EditTable} from 'ssccommon/components/table';
const { NCUploader, Inspection, ApproveDetail,ApprovalTrans } = high;
import pubMessage from 'ssccommon/utils/pubMessage';
import getPageData from 'ssccommon/utils/getPageData';
import LinkQueryJkbxModel from 'ssccommon/linkquery/LinkQueryJkbxModel';
import AttachmentOther from '../../../expenseaccount/expenseaccount/card/AttachmentOther';
import  requestApi from './requestApi';
import {updataFormData, updatacardTableData} from 'ssccommon/utils/changePageData';
import {headSumHandle} from './events/commonEvent';


const { NCModal } = base;
import {
    BillStyle,
    BillHead,
    BillBody,
    Form,
    CardTable,
    ButtonGroup
} from 'ssccommon/components/bill';

import {
    ProfileStyle,
    ProfileBody
} from 'ssccommon/components/profile';

import './index.less';

const pageButton = window.presetVar.pageButton;
window.assingUsers=[];
window.sureAssign = false;//确定指派
window.assignBillId = '';//有指派的情况下，第一次点保存提交实际保存成功的单据pk

class FYSQBill extends Component {

    constructor(props) {
        if (NODE_ENV == 'development') {
            window.NODE_ENV = 'development';
        }
       // ;
        //props.linkTo('../../../../demo/bill/master-body/card/index.html#status=add');
        super();
        this.state = {
            btn: [presetVar.body.body1 + presetVar.areaBtnAction.del, presetVar.body.body1 + presetVar.areaBtnAction.edit],
            showModal: false,
            showUploader: false,
            target: null,
            billId : '',
            sourceData: null,
            showLinkBudget: false,
            ShowApproveDetail: false,
            billtype: '',
            compositedata : '',
            compositedisplay : '',
            Title : '',
            linkData:'',
            tableData:{},//附件除发票
            detailButtonTag:null // 填写明细按钮会显示一些字段，再次点击时返回，所以这里记录原来的字段显示性 -20190902
        }
        this.close = this.close.bind(this);
        this.pubMessage = new pubMessage(props);
        this.getPageData = new getPageData(props);
        initTemplate.call(this, props);

        // TOP NCCLOUD-61317 增加公式编辑后联动 chengwebc ADD
        props.setRelationAfterEventCallBack && props.setRelationAfterEventCallBack((props, changedField, changedArea, oldCard, newCard, index, rowId)=>{
            let params = getAfterEventParams(props, changedField, changedArea, oldCard, newCard, index, rowId);
            params.map((one)=>{
                afterEvent.call(this, props, one.moduleId, one.key, one.value, one.changedrows, one.index)
            })
        });
        // BTM NCCLOUD-61317 增加公式编辑后联动 chengwebc
    }


    componentWillMount() {
                   window.onbeforeunload = () => {
                    let status = this.props.getUrlParam("status");
        ;
                    if (status == 'edit'||status == 'add') {
                        return ''/* 国际化处理： 确定要离开吗？*/
                    }
                }
            }

    componentDidMount() {
    }

    close() {
        this.setState({ showModal: false });
    }
    cancel(){
        this.setState({
            showLinkBudget: false
        })
    }
    affirm(info){
        this.setState({
            showLinkBudget: false
        })
    }
    openApprove = () => {
        this.setState({
            ShowApproveDetail: true
        })
    }

    closeApprove = () => {
        this.setState({
            ShowApproveDetail: false
        })
    }

    getAssginUser = (value) => {
        window.assingUsers = value;
        window.sureAssign = true;
        let props = this.props;
        if(props.getUrlParam('status') === 'browse'){
            pageButtonClick.call(this)[pageButton.billSubmit]();
        }else{
            pageButtonClick.call(this)[pageButton.billSaveSubmit]();
        }
        window.assignBillId = '';
    }

    turnOff(){
        this.setState({
            compositedisplay:false
        }) ;
        window.sureAssign = false;
        window.assingUsers = [];
        let id = this.props.getUrlParam('id') ? this.props.getUrlParam('id') : this.state.billId;
        if(id!=null && id.length>0){
     
        }
        let props = this.props;
        let billid = window.assignBillId;
        if(!billid){
            billid = props.form.getFormItemsValue('head', 'pk_mtapp_bill').value;
        }
        props.linkTo('/erm/erbudgetmg/erbudgetmg/card/index.html',{scene:props.getUrlParam('scene'),status:'browse',id:billid,ts:new Date().getTime()});
    }

    onBeforeEvent(props, moduleId, key, value,  index, record,status){
        window.presetVar.bodyChangeValuesAll = record;
        return true;
    }

    onHideUploader = () => {
        this.setState({
            showUploader: false,
        });
        let id = this.props.getUrlParam('id') ? this.props.getUrlParam('id') : this.state.billId;
        let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId        
        getAttachments(id).then(lists => {
            this.props.button.setButtonTitle('fileManager', multiLang&&multiLang.get('201102SQFY-0031')/**'附件'**/ + ' ' + lists + ' ');
        })
    }

    modelSave=()=>{
        // headSumHandle.call(this,this.props);
        this.updateHeadOrig_amount();
        pageButtonClick.call(this)['pageSave']();
    }

    modelClose=()=>{
        this.state.modelCloseEvent = true;
        this.state.modelEdit = false;
    }

    updateHeadOrig_amount =()=>{
        let moduleId = 'mtapp_detail';
        let key = 'orig_amount'
        let changedrows =  this.props.cardTable.getRowsByIndexs(moduleId, 0);
        let value = this.props.cardTable.getValByKeyAndIndex(moduleId, 0,key);
        afterEvent.call(this,this.props,moduleId,key,value , changedrows);
    }

    render() {
        const { modal } = this.props;
        let { createModal } = modal;  //模态框
        let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId
        let btnModalConfig = {
            [pageButton.pageDel] : {
                // "2011-0004": "删除",
                // "2011-0005": "确认删除该单据吗？"
                title: multiLang && multiLang.get('2011-0004'),
                content: multiLang && multiLang.get('2011-0005'),
            },
            [pageButton.billInvalid]: {
                // "2011-0009": "作废",
                // "2011-0010": "确认作废该单据吗？"
                title: multiLang && multiLang.get('2011-0009'),
                content: multiLang && multiLang.get('2011-0010')
            },
            [pageButton.billEnable] : {
                // "2011-0011": "重启",
                // "2011-0012": "确认重启该单据吗？"
                title: multiLang && multiLang.get('2011-0011'),
                content: multiLang && multiLang.get('2011-0012')
            },
            [pageButton.billClose] : {
                // "2011-0007": "关闭"
                // "2011-0013": "确认关闭该单据吗？",
                title: multiLang && multiLang.get('2011-0007'),
                content: multiLang && multiLang.get('2011-0013')
            }
        }
        const {table} = this.props;
        let { createSimpleTable} = table;
        let { showUploader,target } = this.state;
        return (
            <div>
                {createModal('detail', { noFooter: true })}
                <BillStyle
                    {...this.props}
                >
                    <BillHead
                        // "201102SQFY-0001": "费用申请单"
                        title = {this.state.Title}
                        refreshButtonEvent={refreshButtonClick.bind(this)}
                    >
                        <ButtonGroup
                            areaId="head"
                            buttonEvent={pageButtonClick.bind(this)}
                            modalConfig={btnModalConfig}
                        />

                        <Form
                            areaId="head"
                            onAfterEvent={afterEvent.bind(this)}
                        />
                    </BillHead>

                    <BillBody>

                        <CardTable
                            // "201102SQFY-0002": "费用申请单明细"
                            title={multiLang && multiLang.get('201102SQFY-0002')}
                            areaId="mtapp_detail"
                            showCheck={true}
                            totalItemCode="orig_amount"
                            modelSave={this.modelSave.bind(this)}
                            onAfterEvent={afterEvent.bind(this)}
                            onBeforeEvent={this.onBeforeEvent.bind(this)}
                            modelClose= {this.modelClose.bind(this)}
                            modelDelRow = {this.updateHeadOrig_amount.bind(this)}
                        >
                            <ButtonGroup
                                buttonEvent={tableButtonClick.bind(this)}
                            />
                        </CardTable>

                        <div>
                            {showUploader && < NCUploader
                                billId={ this.state.billId }
                                targrt={target}
                                onHide={this.onHideUploader}
                                placement={'bottom'} />
                            }
                        </div>

                        <div>
                            <ApproveDetail
                                show={this.state.ShowApproveDetail}
                                close={this.closeApprove}
                                billtype={this.state.billtype}
                                billid={this.state.billId}
                            />
                            <AttachmentOther tableData={this.state.tableData}/>
                        </div>
                        <LinkQueryJkbxModel
                            show={this.state.showModal}
                            linkData={this.state.linkData}
                            close={() => this.setState({showModal: false})}
                            tradetype={window.presetVar.billtype}
                            openBillId={this.state.billId}
                        {...this.props} />
                        
                    </BillBody>
                </BillStyle>
                <div>
                    <Inspection
                        show={ this.state.showLinkBudget }
                        sourceData = { this.state.sourceData }
                        cancel = { this.cancel.bind(this) }
                        affirm = { this.affirm.bind(this) }
                    />
                </div>
                <div>
                    {/*"201102FYYT-0003": "指派",*/}
                    {this.state.compositedisplay ? <ApprovalTrans
                        title={multiLang && multiLang.get('201102FYYT-0003')}
                        data={this.state.compositedata}
                        display={this.state.compositedisplay}
                        getResult={this.getAssginUser}
                        cancel={this.turnOff.bind(this)}
                     /> : ""}

                </div>
            </div>
        );

    }
}

FYSQBill = createPage({
    mutiLangCode: '2011',
    billinfo:{
        billtype: 'card',  //虽然后台是一主多子数据结构，但是配置成extCard，编辑后请求公式验证会报错。因为会根据实际展现的表体数量来判断是一主多子还是一主一子。
        pagecode: presetVar.pageCode, 
        headcode: presetVar.head.head1,
        bodycode: presetVar.body.body1
    },
    orderOfHotKey: [presetVar.body.body1]
})(FYSQBill);

export default FYSQBill;
