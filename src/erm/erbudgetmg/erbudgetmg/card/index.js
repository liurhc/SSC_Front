import React, {Component} from 'react';
import ReactDOM from 'react-dom';

import FYSQBill from './FYSQBill';

ReactDOM.render((<FYSQBill />)
    , document.querySelector('#app'));