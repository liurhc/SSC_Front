/**
 * 后台接口文件
 */
import requestApi from "ssccommon/components/requestApi";
import { ajax } from 'nc-lightapp-front';
import { toast } from 'nc-lightapp-front';
import { isNullOrUndefined } from "util";
import getDefDataFromMeta from 'ssccommon/utils/getDefDataFromMeta';
let pageCode = window.presetVar.pageCode;
let requestDomain = '';
let requestApiOverwrite = {
    ...requestApi,
    //单据模板接口
    tpl: (opt) => {
        let domain = '';//'https://mock.yonyoucloud.com/mock/172';
        ajax({
            url: opt.url || `${domain}/nccloud/erm/erbudgetmg/FysqTempletAction.do`,
            data: opt.data,
            success: opt.success
        });
    },
    query: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/erbudgetmg/FysqViewBillAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    save: (opt) => {

        //数据适配开始
        opt.data.head[presetVar.head.head1].rows.map((row) => {
            for (let key in row.values) {
                if (row.values[key].value || row.values[key].value == 0) {
                    row.values[key].value += '';
                }
            }
            return row;
        });
        opt.data.body[presetVar.body.body1].rows.map((row) => {
            for (let key in row.values) {
                if (row.values[key].value || row.values[key].value == 0) {
                    row.values[key].value += '';
                }
            }
            return row;
        });
        //数据适配结束
        opt.data.pageid = pageCode;
        opt.data = {
            ...opt.data,
            uistate: opt.status,
            tradetype: window.presetVar.billtype,
            ntbCheck: window.ntbCheck,
            accessorybillid: opt.accessorybillid,
            fromssc: 'N',
            msg_pk_bill: null,
            userjson: "{'pagecode':'" + pageCode + "','appcode':'" + window.presetVar.appCode + "'}"
        }
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/erbudgetmg/FysqSaveBillAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
                window.ntbCheck='false';
            }
        })
    },
    billInvalid: (opt) => {
        //数据适配开始
        opt.data.head[presetVar.head.head1].rows.map((row) => {
            for (let key in row.values) {
                if (row.values[key].value || row.values[key].value == 0) {
                    row.values[key].value += '';
                }
            }
            return row;
        });
        opt.data.body[presetVar.body.body1].rows.map((row) => {
            for (let key in row.values) {
                if (row.values[key].value || row.values[key].value == 0) {
                    row.values[key].value += '';
                }
            }
            return row;
        });
        //数据适配结束
        opt.data.pageid = pageCode;
        opt.data = {
            ...opt.data,
            tradetype: window.presetVar.billtype,
            userjson: "{'pagecode':'" + pageCode + "'}"
        }
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/erbudgetmg/FysqInvalidBillAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    billEnable: (opt) => {
        //数据适配开始
        opt.data.head[presetVar.head.head1].rows.map((row) => {
            for (let key in row.values) {
                if (row.values[key].value || row.values[key].value == 0) {
                    row.values[key].value += '';
                }
            }
            return row;
        });
        opt.data.body[presetVar.body.body1].rows.map((row) => {
            for (let key in row.values) {
                if (row.values[key].value || row.values[key].value == 0) {
                    row.values[key].value += '';
                }
            }
            return row;
        });
        //数据适配结束
        opt.data.pageid = pageCode;
        opt.data = {
            ...opt.data,
            tradetype: window.presetVar.billtype,
            userjson: "{'pagecode':'" + pageCode + "'}"
        }
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/erbudgetmg/FysqRestartBillAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    billClose: (opt) => {
        //数据适配开始
        opt.data.head[presetVar.head.head1].rows.map((row) => {
            for (let key in row.values) {
                if (row.values[key].value || row.values[key].value == 0) {
                    row.values[key].value += '';
                }
            }
            return row;
        });
        opt.data.body[presetVar.body.body1].rows.map((row) => {
            for (let key in row.values) {
                if (row.values[key].value || row.values[key].value == 0) {
                    row.values[key].value += '';
                }
            }
            return row;
        });
        opt.data.pageid = pageCode;
        //数据适配结束
        opt.data = {
            ...opt.data,
            tradetype: window.presetVar.billtype,
            userjson: "{'pagecode':'" + pageCode + "'}"
        }
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/erbudgetmg/FysqCloseBillAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    billRemove: (opt) => {
        //数据适配开始
        let openbillid = "";
        opt.data.head[presetVar.head.head1].rows.map((row) => {
            for (let key in row.values) {
                if (row.values[key].value || row.values[key].value == 0) {
                    if (key === "pk_mtapp_bill")
                        openbillid = row.values[key].value;
                }
            }
            return row;
        });
        //数据适配结束
        opt.data = {
            ...opt.data,
            openbillid: openbillid
        }
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/erbudgetmg/FysqDeleteBillAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    loadData: (opt) => {
        //数据适配开始
        let reqData = {
            uistate: 'add',
            tradetype: window.presetVar.billtype,
            head: {},
            body: {}
        };

        reqData = {
            ...reqData,
            ...genReqParamForLoadData(opt.meta)
        }
        let props = opt.props;
        //数据适配结束
        reqData.nodecode = "2611_IWEB";
        reqData.userjson = "{'pagecode':'" + pageCode + "','appcode':'" + window.presetVar.appCode + "','templateid':'"+(opt.meta || {}).pageid+"'}";
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/erbudgetmg/FysqDefDataAction.do`,
            data: reqData,
            success: (data) => {

                //数据适配开
                if (isJSON(JSON.parse(data.data.userjson).reimsrc))
                    window.presetVar.reimsrc = JSON.parse(JSON.parse(data.data.userjson).reimsrc);//核心控制项-影响因素
                window.presetVar.showitemkey = JSON.parse(data.data.userjson).showitemkey;//显示项   

                let bodyValues = data.data.body[presetVar.body.body1].rows[0].values;
                let bodyInitialvalue =  {};
                for(let body in data.data.body){
                    ((data.data.body[body] || {}).rows || {}).length>0 && (bodyInitialvalue[body] = data.data.body[body].rows[0].values);
                }
                let numberInitialvalueField = ['global_currinfo','group_currinfo','org_currinfo','rowno'];

                opt.meta[presetVar.body.body1].items.forEach((item, index) => {
                    let attrcode = item.attrcode;
                    if (bodyValues[attrcode]) {
                        if(item.itemtype != 'number' || numberInitialvalueField.indexOf(item.attrcode) >= 0){
                            item.initialvalue = {
                                value: bodyValues[attrcode].value,
                                display: bodyValues[attrcode].display = null ? bodyValues[attrcode].value : bodyValues[attrcode].display
                            };
                        } else{
                            //by houyb 表体精度问题,注释
                            // item.initialvalue = {
                            //     // TOP 金额字段不需要显示默认值 chengwebc DEL
                            //     scale: bodyValues[attrcode].scale
                            // };
                        }
                        //by houyb 表体精度问题,注释
                        // if (bodyValues[attrcode].scale != -1) {
                        //     item.scale = bodyValues[attrcode].scale;
                        // }

                    }
                    //处理交叉校验规则和参照过滤
                    filterAllRefParm(item, props, presetVar.body.body1);
                });
                //by houyb 20190921 侧拉编辑添加queryCondition
                // opt.meta['childform2'].items.forEach((item, index) => {
                //     let attrcode = item.attrcode;
                //     if (bodyValues[attrcode]) {
                //         item.scale = bodyValues[attrcode].scale;
                //     }
                //     //处理交叉校验规则和参照过滤
                //     filterAllRefParm(item, props, presetVar.body.body1);

                // });
                // 关联多个展开页签
                let meta = opt.meta;
                for (let table in meta.gridrelation) {
                    let destEditAreas = meta.gridrelation[table].destEditAreaCode || null;
                    if (destEditAreas != null){
                        destEditAreas.forEach((area)=>{
                            meta[area].items.forEach((item) => {
                                if (item.attrcode && item.attrcode.indexOf('.') < 0 && bodyInitialvalue[table][item.attrcode]) {
                                    if ((item.itemtype != 'number' || numberInitialvalueField.indexOf(item.attrcode) >= 0)) {
                                        item.initialvalue = {
                                            value: bodyInitialvalue[table][item.attrcode].value,
                                            display: bodyInitialvalue[table][item.attrcode].display,
                                            scale: bodyInitialvalue[table][item.attrcode].scale,
                                        };
                                    } else {
                                        item.initialvalue = {
                                            // TOP 金额字段不需要显示默认值 chengwebc DEL
                                            //  value: bodyValues[table][item.attrcode].value,
                                            //  display: bodyValues[table][item.attrcode].display,
                                            // BTM 金额字段不需要显示默认值
                                            //by houyb 表体精度问题
                                            // scale: bodyInitialvalue[table][item.attrcode].scale
                                        };
                                        //by houyb 表体精度问题
                                        // item.scale = (bodyInitialvalue[table][item.attrcode] || {scale:-1}).scale;
                                    }
                                }
                                if(item.itemtype == 'refer' && item.visible == true){
                                    filterAllRefParm(item,props,table);
                                }
                            });
                        })
                    }
                }
                //by houyb 20190921 侧拉编辑添加queryCondition
                let headValues = data.data.head[presetVar.head.head1].rows[0].values;
                window.presetVar.headValuesAll = headValues;

                opt.meta[presetVar.head.head1].items.forEach((item) => {
                    let attrcode = item.attrcode;
                    if (headValues[attrcode]) {
                        item.initialvalue = {
                            value: headValues[attrcode].value,
                            scale: headValues[attrcode].scale,
                            display: headValues[attrcode].display = null ? headValues[attrcode].value : headValues[attrcode].display
                        };
                        //处理交叉校验规则和参照过滤
                        filterAllRefParm(item, props, presetVar.head.head1);
                    }

                });
                //适配多表头
                if (opt.meta.formrelation) {
                    opt.meta.formrelation[presetVar.head.head1].forEach((item) => {
                        if (opt.meta[item]) {
                            opt.meta[item].items.forEach((key) => {
                                let attrcode = key.attrcode;
                                if (headValues[attrcode]) {
                                    key.initialvalue = {
                                        value: headValues[attrcode].value,
                                        scale: headValues[attrcode].scale,
                                        display: headValues[attrcode].display = null ? headValues[attrcode].value : headValues[attrcode].display
                                    };
                                    //处理交叉校验规则和参照过滤
                                    filterAllRefParm(key, props, presetVar.head.head1);
                                }
                            })
                        }

                    });
                }

                //数据适配结束
                console.log('opt值：', opt)
                opt.success(opt.meta);
            }
        })
    },
    changeByHead: (opt) => {
        opt.data = {
            ...opt.data,
            userjson: "{'pagecode':'" + pageCode + "','appcode':'" + window.presetVar.appCode +  "','templateid':'" + opt.data.templateid + "'}"
        }
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/erbudgetmg/FysqValueChangedAction.do`,
            data: opt.data,
            async: false,
            success: (data) => {
                opt.success(data);
            }
        });
    },
    changeByBody: (opt) => {
        opt.data = {
            ...opt.data,
            userjson: "{'pagecode':'" + pageCode + "','appcode':'" + window.presetVar.appCode + "','checkrule':'" + opt.data.checkrule + "','templateid':'" + opt.data.templateid + "'}"
        }
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/erbudgetmg/FysqValueChangedAction.do`,
            data: opt.data,
            async: false,
            success: (data) => {
                opt.success(data);
            }
        });
    },
    copy: (opt) => { //单据复制
        opt.data.pageid = pageCode;
        opt.data = {
            ...opt.data,
            userjson: "{'pagecode':'" + pageCode + "'}"
        }
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/erbudgetmg/FysqCopyBillAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    billSubmit: (opt) => {
        let openbillid = "''";
        opt.data.head[presetVar.head.head1].rows.map((row) => {
            for (let key in row.values) {
                if (row.values[key].value || row.values[key].value == 0) {
                    if (key === "pk_mtapp_bill")
                        openbillid = row.values[key].value;
                }
            }
            return row;
        });
        if(opt.assignBillId){
            openbillid = opt.assignBillId;
        }
        opt.data.pageid = pageCode;
        opt.data = {
            ...opt.data,
            accessorybillid: opt.accessorybillid,
            userjson: "{'pagecode':'" + pageCode +
                "','appcode':'" + window.presetVar.appCode + "'" +
                ",'openbillid':" + openbillid +
                ",'tradetype':'" + window.presetVar.billtype + "'" +
                ",'ntbCheck':'" + window.ntbCheck + "'" +
                ",'fromssc':'N'" +
                ",'uistate':" + opt.status +
                ",'assingUsers':" + JSON.stringify(window.assingUsers) +
                "}"
        }
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/erbudgetmg/FysqSubmitAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
                window.ntbCheck ='false';
            },
            error: (data) => {
                toast({ content: data.message, color: 'danger' });
                opt.error(data);
            }
        });
    },
    billRecall: (opt) => {
        let openbillid = "";
        opt.data.head[presetVar.head.head1].rows.map((row) => {
            for (let key in row.values) {
                if (row.values[key].value || row.values[key].value == 0) {
                    if (key === "pk_mtapp_bill")
                        openbillid = row.values[key].value;
                }
            }
            return row;
        });
        opt.data.pageid = pageCode;
        opt.data = {
            ...opt.data,
            userjson: "{'pagecode':'" + pageCode +
                "','openbillid':'" + openbillid +
                "'}"
        }
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/erbudgetmg/FysqRecallAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            },
            error: (data) => {
                toast({ content: data.message, color: 'danger' });
            }
        });
    },
    billView: (opt) => {
        let openbillid = "";
        let pk_tradetype = "";
        opt.data.head[presetVar.head.head1].rows.map((row) => {
            for (let key in row.values) {
                if (row.values[key].value || row.values[key].value == 0) {
                    if (key === "pk_mtapp_bill") { openbillid = row.values[key].value; }
                    if (key === "pk_tradetype") { pk_tradetype = row.values[key].value; }
                }
            }
            return row;
        });
        opt.data.pageid = pageCode;
        opt.data = {
            ...opt.data,
            userjson: "{'pagecode':'linkjkbx_list'" +
                ",'openbillid':'" + openbillid + "'" +
                ",'pk_tradetype':'" + pk_tradetype + "'" +
                "}"
        }
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/erbudgetmg/FysqLinkQueryAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            },
            error: (data) => {
                toast({ content: data.message, color: 'danger' });
            }
        });
    },
    handleLoadDataResDada: (meta, data) => {
        handleLoadDataResDada(meta, data)
    },
    generatBillId: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/action/generatBillId.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            },
            error: (data) => {
                toast({ content: data.message, color: 'danger' });
            }
        });
    },
    getUnEditAttributes:(opt) =>{
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/pub/ErmAttributeEditableAction.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            },
            error:(data) =>{
                toast({content:data.message,color:'warning'});
            }
        });
    },
}

function isJSON(str) {
    if (typeof str == 'string') {
        try {
            JSON.parse(str);
            return true;
        } catch (e) {
            return false;
        }
    }
}


function filterAllRefParm(item, props, flag) {
    let PublicDefaultRefFilterPath = 'nccloud.web.action.erm.ref.sqlbuilder.PublicDefaultRefSqlBuilder';//公共 
    let OrgTreeRefFilterPath ='nccloud.web.action.erm.ref.sqlbuilder.OrgTreeRefSqlBuilder';//组织
    let BusinessUnitTreeRefFilterPath = 'nccloud.web.action.erm.ref.sqlbuilder.BusinessUnitTreeRefSqlBuilder';//组织
    let DeptTreeRefFilterPath = 'nccloud.web.action.erm.ref.sqlbuilder.DeptTreeRefSqlBuilder';//部门
    let CurrtypeRefSqlFilterPath = 'nccloud.web.action.erm.ref.sqlbuilder.CurrtypeRefSqlBuilder';//币种
    let PsndocClassFilterPath = 'nccloud.web.action.erm.ref.sqlbuilder.PsndocClassTreeGridRef';//人员
    let PsndocTreeFilterPath = 'nccloud.web.action.erm.ref.sqlbuilder.PsndocTreeGridRef';//人员
    let SupplierClassTreeRefFilterPath = 'nccloud.web.action.erm.ref.sqlbuilder.SupplierClassTreeRefSqlBuilder';//供应商
    let CustClassDefaultTreeRefFilterPath = 'nccloud.web.action.erm.ref.sqlbuilder.CustClassDefaultTreeRefSqlBuilder';//客户

    //所属组织，待确定是否需要特殊处理
    if (item.attrcode == 'pk_org' || item.attrcode == 'pk_org_v') {
        item.queryCondition = () => {
            let paramurl = getRequest1(parent.window.location.hash);
            //如果没取到应用编码再取一下外层
            if(!paramurl.c){
                paramurl = getRequest1(parent.parent.window.location.hash);
            }
            return { pk_group: (flag == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['pk_group'].value) : (props.cardTable.getClickRowIndex(presetVar.body.body1).record.values['pk_group'].value)), TreeRefActionExt: OrgTreeRefFilterPath, DataPowerOperationCode: 'fi',/**使用权组**/isDataPowerEnable: 'Y' , appcode : paramurl.c}; // 根据pk_group过滤
        };
    }

    //申请单位
    if (item.attrcode == 'apply_org') {

        item.queryCondition = () => {
            let data = (flag == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['pk_group'].value) : (props.cardTable.getClickRowIndex(presetVar.body.body1).record.values['pk_group'].value)); 
            return { pk_group: data, TreeRefActionExt: BusinessUnitTreeRefFilterPath, DataPowerOperationCode: 'fi',/**使用权组**/isDataPowerEnable: 'Y' }; // 根据pk_group过滤
        };
    }
    //承担单位
    if (item.attrcode == 'assume_org') {
        item.queryCondition = () => {
            let data = (flag == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['pk_group'].value) : (props.cardTable.getClickRowIndex(presetVar.body.body1).record.values['pk_group'].value)); 
            return {  pk_group: data, TreeRefActionExt: BusinessUnitTreeRefFilterPath, DataPowerOperationCode: 'fi',/**使用权组**/isDataPowerEnable: 'Y' }; // 根据pk_group过滤
        };
    }
    //申请部门
    if (item.attrcode == 'apply_dept') {
        item.queryCondition = () => {
            let data = props.form.getAllFormValue(presetVar.head.head1).rows[0].values['apply_org'].value;
            return {  pk_org: data, TreeRefActionExt: DeptTreeRefFilterPath, DataPowerOperationCode: 'fi',/**使用权组**/isDataPowerEnable: 'Y' }; // 根据apply_org过滤
        };
    }
    //承担部门
    if (item.attrcode == 'assume_dept') {
        item.queryCondition = () => {
            let data = props.cardTable.getClickRowIndex(presetVar.body.body1).record.values['assume_org'].value;
            return {  pk_org: data, TreeRefActionExt: DeptTreeRefFilterPath, DataPowerOperationCode: 'fi',/**使用权组**/isDataPowerEnable: 'Y' }; // 根据assume_org过滤
        };
    }
    //收支项目
    if (item.attrcode == 'pk_iobsclass') {
        item.queryGridUrl = window.location.origin + "/nccloud/uapbd/ref/InoutBusiClassTreeRef.do";
        let tableData = props.cardTable.getClickRowIndex(presetVar.body.body1);
        item.queryCondition = () => {
            let data =props.form.getAllFormValue(presetVar.head.head1).rows[0].values['apply_org'].value;
            if (flag != presetVar.head.head1) {
                data = tableData && tableData.record.values['assume_org'].value;
            }
            return { pk_org: data, TreeRefActionExt: PublicDefaultRefFilterPath, DataPowerOperationCode: 'fi',/**使用权组**/isDataPowerEnable: 'Y' }; // 根据assume_org过滤
        };
    }

    //供应商
    if (item.attrcode == 'pk_supplier') {
        item.queryCondition = () => {
            let data = (flag == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['pk_org'].value) : (props.cardTable.getClickRowIndex(presetVar.body.body1).record.values['pk_org'].value)); 
            return { pk_org: data, TreeRefActionExt: SupplierClassTreeRefFilterPath, GridRefActionExt: SupplierClassTreeRefFilterPath, DataPowerOperationCode: 'fi',/**使用权组**/isDataPowerEnable: 'Y' }; // 根据pk_org过滤
        };
    }

    //客户
    if (item.attrcode == 'pk_customer') {
        item.queryCondition = () => {
            let data = (flag == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['pk_org'].value) : (props.cardTable.getClickRowIndex(presetVar.body.body1).record.values['pk_org'].value)); 
            return { pk_org: data, TreeRefActionExt: CustClassDefaultTreeRefFilterPath, GridRefActionExt: CustClassDefaultTreeRefFilterPath, DataPowerOperationCode: 'fi',/**使用权组**/isDataPowerEnable: 'Y' }; // 根据pk_org过滤
        };
    }

    //币种
    if (item.attrcode == 'pk_currtype') {
        item.queryCondition = () => {
            return {  GridRefActionExt: CurrtypeRefSqlFilterPath, DataPowerOperationCode: 'fi',/**使用权组**/isDataPowerEnable: 'Y' };
        };
    }

    //人员
    if (item.attrcode == 'pk_salesman' || item.attrcode == 'closeman' || item.attrcode == 'billmaker') {

        item.queryCondition = () => {
            let data = (flag == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['apply_dept'].value) : (props.cardTable.getClickRowIndex(presetVar.body.body1).record.values['apply_dept'].value)); 
            let data_org = (flag == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['apply_org'].value) : (props.cardTable.getClickRowIndex(presetVar.body.body1).record.values['apply_org'].value)); 
    
            return {  org_id: data_org, pk_org: data_org, pk_dept: data, GridRefActionExt: PsndocTreeFilterPath, DataPowerOperationCode: 'fi',/**使用权组**/isDataPowerEnable: 'Y' }; // 根据assume_org过滤
        };

    }

    //人员 增加代理申请处理
    if (item.attrcode == 'billmaker') {
            item.queryCondition = () => {
                let data = (flag == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['apply_dept'].value) : (props.cardTable.getClickRowIndex(presetVar.body.body1).record.values['apply_dept'].value)); 
                let data_org = (flag == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['apply_org'].value) : (props.cardTable.getClickRowIndex(presetVar.body.body1).record.values['apply_org'].value)); 
        
                return { tradetype: window.presetVar.billtype, org_id: data_org, pk_org: data_org,  GridRefActionExt: PsndocTreeFilterPath, DataPowerOperationCode: 'fi',/**使用权组**/isDataPowerEnable: 'Y' }; // 根据assume_org过滤
            };
    
    }

    //项目
    if (item.attrcode == 'pk_project') {
        item.queryCondition = () => {
            let data = (flag == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['pk_org'].value) : (props.cardTable.getClickRowIndex(presetVar.body.body1).record.values['pk_org'].value)); 
        
            return { pk_org: data, GridRefActionExt: PublicDefaultRefFilterPath, DataPowerOperationCode: 'fi',/**使用权组**/isDataPowerEnable: 'Y' };
        };
    }
    //项目任务
    if (item.attrcode == 'pk_wbs') {
        item.queryCondition = () => {
            let data =  (flag == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['pk_org'].value) : (props.cardTable.getClickRowIndex(presetVar.body.body1).record.values['pk_org'].value)); 
        
            return { pk_org: data, TreeRefActionExt: PublicDefaultRefFilterPath, DataPowerOperationCode: 'fi',/**使用权组**/isDataPowerEnable: 'Y' };
        };
    }
    //产品线
    if (item.attrcode == 'pk_proline') {
        item.queryCondition = () => {
            let data =  (flag == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['pk_org'].value) : (props.cardTable.getClickRowIndex(presetVar.body.body1).record.values['pk_org'].value)); 
        
            return {  pk_org: data, GridRefActionExt: PublicDefaultRefFilterPath, DataPowerOperationCode: 'fi',/**使用权组**/isDataPowerEnable: 'Y' };
        };
    }
    //品牌
    if (item.attrcode == 'pk_brand') {
        item.queryCondition = () => {
            let data =  (flag == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['pk_org'].value) : (props.cardTable.getClickRowIndex(presetVar.body.body1).record.values['pk_org'].value)); 
        
            return { pk_org: data, GridRefActionExt: PublicDefaultRefFilterPath, DataPowerOperationCode: 'fi',/**使用权组**/isDataPowerEnable: 'Y' };
        };
    }



    //核算要素
    if (item.attrcode == 'pk_checkele') {
        item.queryCondition = () => {
            let data = (flag == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['pk_org'].value) : (props.cardTable.getClickRowIndex(presetVar.body.body1).record.values['pk_org'].value)); 
        
            return { pk_org: data, TreeRefActionExt: PublicDefaultRefFilterPath, DataPowerOperationCode: 'fi',/**使用权组**/isDataPowerEnable: 'Y' };
        };
    }
    //利润中心
    if (item.attrcode == 'pk_pcorg') {
        item.queryCondition = () => {
            let data = (flag == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['pk_org'].value) : (props.cardTable.getClickRowIndex(presetVar.body.body1).record.values['pk_org'].value)); 
        
            return {  pk_org: data, TreeRefActionExt: PublicDefaultRefFilterPath, DataPowerOperationCode: 'fi',/**使用权组**/isDataPowerEnable: 'Y' };
        };
    }
    //成本中心
    if (item.attrcode == 'pk_resacostcenter') {
        item.queryCondition = () => {
            let data = (flag == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['pk_org'].value) : (props.cardTable.getClickRowIndex(presetVar.body.body1).record.values['pk_org'].value)); 
        
            return {  pk_org: data, TreeRefActionExt: PublicDefaultRefFilterPath, DataPowerOperationCode: 'fi',/**使用权组**/isDataPowerEnable: 'Y' };
        };
    }
    //报销事由
    if (item.attrcode == 'reason') {
        // 设置可以手工录入
        item.checkStrictly = false;
        item.queryCondition = () => {
            let data = (flag == presetVar.head.head1 ? (props.form.getAllFormValue(presetVar.head.head1).rows[0].values['apply_org'].value) : (props.cardTable.getClickRowIndex(presetVar.body.body1).record.values['apply_org'].value));
        
            return { pk_org: data, TreeRefActionExt: PublicDefaultRefFilterPath, DataPowerOperationCode: 'fi',/**使用权组**/isDataPowerEnable: 'Y' }; // 根据assume_org过滤
        };
    }


    //处理交叉校验规则
    if (item.itemtype == 'refer') {
        let oldQueryCondition = item.queryCondition;
        let crossrule_itemkey = item.attrcode;
        item.queryCondition = (obj) => {
            let crossrule_datasout = props.createMasterChildData(presetVar.pageId, presetVar.head.head1, presetVar.body.body1);
            let crossrule_tradetype = props.form.getAllFormValue(presetVar.head.head1).rows[0].values['pk_tradetype'].value
            let crossrule_org = props.form.getAllFormValue(presetVar.head.head1).rows[0].values['pk_org'].value
            let crossrule_datatypes = 'billcard';
            let crossrule_datas = crossrule_datasout;
            if(window.presetVar.bodyChangeValuesAll!=null && window.presetVar.bodyChangeValuesAll.rowid)

            if (window.presetVar.bodyChangeValuesAll) {
                crossrule_datas.body[presetVar.body.body1].rows = [window.presetVar.bodyChangeValuesAll];
            }
            let conditionFlag = true;
            if(oldQueryCondition==null || oldQueryCondition==undefined || oldQueryCondition=='undefined')
            conditionFlag = false;
            let oldData='';
            if(conditionFlag)
            oldData = oldQueryCondition();
            if(oldData==null || oldData==undefined || oldData=='undefined')
            oldData='';
            let config = { 
                crossrule_datas:JSON.stringify(crossrule_datas),
                crossrule_tradetype:crossrule_tradetype,
                crossrule_org:crossrule_org, 
                crossrule_datatypes:crossrule_datatypes,
                crossrule_area:flag,
                crossrule_itemkey:crossrule_itemkey,
                ...oldData
                };

            //参照类型的自定义项字段增加组织过滤
            if(!oldData.pk_org){
                config.pk_org = crossrule_org;
            }
                
                if(obj==undefined || obj==null || obj.refType==undefined || obj.refType==null ){
                    if(config.TreeRefActionExt==null || config.TreeRefActionExt==undefined || config.TreeRefActionExt.length==0)
                    config.TreeRefActionExt = PublicDefaultRefFilterPath; 
                    return config;
                }

                if (obj.refType == 'grid') {
                if(config.GridRefActionExt==null || config.GridRefActionExt==undefined || config.GridRefActionExt.length==0)
                config.GridRefActionExt = PublicDefaultRefFilterPath;
                } else if (obj.refType == 'tree') {
                if(config.TreeRefActionExt==null || config.TreeRefActionExt==undefined || config.TreeRefActionExt.length==0)
                config.TreeRefActionExt = PublicDefaultRefFilterPath;
                } else if (obj.refType == 'gridTree') {
                if(config.GridRefActionExt==null || config.GridRefActionExt==undefined || config.GridRefActionExt.length==0)
                config.GridRefActionExt = PublicDefaultRefFilterPath;
                }
                
                return config;
        }
    }

}

function handleLoadDataResDada(meta, data) {

    if (data.data.body[presetVar.body.body1].rows[0]) {
        let bodyValues = data.data.body[presetVar.body.body1].rows[0].values;
        for (let attrcode in bodyValues) {
            meta[presetVar.body.body1].items.forEach((item) => {

                if (item.attrcode == attrcode) {
                    item.initialvalue = {
                        value: bodyValues[attrcode].value,
                        display: bodyValues[attrcode].display = null ? bodyValues[attrcode].value : bodyValues[attrcode].display
                    };
                    //处理交叉校验规则和参照过滤
                    //filterAllRefParm(item,bodyValues);
                }

            });
        }
    }

    if (data.data.head[presetVar.head.head1].rows[0]) {
        let headValues = data.data.head[presetVar.head.head1].rows[0].values;
        for (let attrcode in headValues) {
            meta[presetVar.head.head1].items.forEach((item) => {
                if (item.attrcode == attrcode) {
                    item.initialvalue = {
                        value: headValues[attrcode].value,
                        scale: headValues[attrcode].scale,
                        display: headValues[attrcode].display = null ? headValues[attrcode].value : headValues[attrcode].display
                    };
                    //处理交叉校验规则和参照过滤
                    //filterAllRefParm(item,headValues);
                }

            });
        }
    }


}

function genReqParamForLoadData(meta) {
    // TOP 修改取模版默认值方法（修改前模版默认值无法默认带出） 190225 MOD
    // let reqData = {
    //     head: {},
    //     body: {}
    // }

    // for (let areaCode in meta) {
    //     let areaData = meta[areaCode];
    //     //满足以下条件才是区域。 relationcode用来区分cardTable
    //     if (!areaData || !areaData.moduletype || !areaData.items || (areaData.hasOwnProperty('relationcode') && areaData.code != 'head')) {
    //         continue;
    //     }
    //     //设定区域初始状态
    //     switch (areaData.moduletype) {
    //         case 'form':
    //             reqData.head[areaCode] = {
    //                 areaType: 'form',
    //                 "areacode": areaCode,
    //                 rows: [{
    //                     'values': {}
    //                 }]
    //             }
    //             areaData.items.forEach((item) => {
    //                 reqData.head[areaCode].rows[0].values[item.attrcode] = {
    //                     value: null
    //                 };
    //             });
    //             break;
    //         case 'table':
    //             reqData.body[areaCode] = {
    //                 areaType: 'table',
    //                 "areacode": areaCode,
    //                 rows: [{
    //                     'values': {}
    //                 }]
    //             }
    //             areaData.items.forEach((item) => {
    //                 reqData.body[areaCode].rows[0].values[item.attrcode] = {
    //                     value: null
    //                 };
    //             });
    //             break;
    //         case 'search':
    //             break;
    //     }
    // }
    // return reqData;
    return getDefDataFromMeta(meta, '', 'body');
    // BTM 修改取模版默认值方法（修改前模版默认值无法默认带出）
}
export default requestApiOverwrite;

function getRequest1(url) {
    if (!url)
        url = document.location.search;
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substring(url.indexOf("?") + 1);
        var strs = str.split("&");
        for (var i = 0; i < strs.length; i++) {
            theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
        }
    }
    return theRequest;
}