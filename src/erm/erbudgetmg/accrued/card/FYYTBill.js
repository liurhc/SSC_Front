/**
 * 费用预提模块业务入口
 */
import './presetVar'
import React, { Component } from 'react';
import { createPage, base, high, getAttachments } from 'nc-lightapp-front';
import { tableButtonClick, pageButtonClick, afterEvent, onRowDoubleClick, refreshButtonClick } from './events';
import initTemplate from './initTemplate';
import {getAfterEventParams} from 'ssccommon/utils/formulaUtils';
import pubMessage from 'ssccommon/utils/pubMessage'
import AttachmentOther from '../../../expenseaccount/expenseaccount/card/AttachmentOther';

const { NCUploader, Inspection, ApproveDetail,ApprovalTrans } = high;

const { NCModal } = base;
import {
    BillStyle,
    BillHead,
    BillBody,
    Form,
    CardTable,
    ButtonGroup
} from 'ssccommon/components/bill';

import {
    ProfileStyle,
    ProfileBody
} from 'ssccommon/components/profile';

import './index.less';

const pageButton = window.presetVar.pageButton;
window.assingUsers = [];
window.sureAssign = false;//确定指派
window.assignBillId = '';//有指派的情况下，第一次点保存提交实际保存成功的单据pk

class FYYTBill extends Component {

    constructor(props) {
        if (NODE_ENV == 'development') {
            window.NODE_ENV = 'development';
        }
        // "";
        //props.linkTo('../../../../demo/bill/master-body/card/index.html#status=add');
        super();
        this.state = {
            btn: [presetVar.body.body1 + presetVar.areaBtnAction.del, presetVar.body.body1 + presetVar.areaBtnAction.edit],
            showModal: false,
            showUploader: false,
            target: null,
            billId: '',
            sourceData: null,
            showLinkBudget: false,
            isEffect: false,
            ShowApproveDetail: false,
            billtype: '',
            compositedata: '',
            compositedisplay: '',
            Title: '',
            tableData:{}//表格数据
        }
        this.close = this.close.bind(this);
        initTemplate.call(this, props);
        this.pubMessage = new pubMessage(props);

        // TOP NCCLOUD-61317 增加公式编辑后联动 chengwebc ADD
        props.setRelationAfterEventCallBack && props.setRelationAfterEventCallBack((props, changedField, changedArea, oldCard, newCard, index, rowId)=>{
            let params = getAfterEventParams(props, changedField, changedArea, oldCard, newCard, index, rowId);
            params.map((one)=>{
                afterEvent.call(this, props, one.moduleId, one.key, one.value, one.changedrows, one.index)
            })
        });
        // BTM NCCLOUD-61317 增加公式编辑后联动 chengwebc
    }

    componentDidMount() {
    }
    componentWillMount() {
                   window.onbeforeunload = () => {
                    let status = this.props.getUrlParam("status");
                    if (status == 'edit'||status == 'add') {
                        return ''/* 国际化处理： 确定要离开吗？*/
                    }
                }
            }


    close() {
        this.setState({ showModal: false });
    }

    cancel() {
        this.setState({
            showLinkBudget: false
        })
    }
    affirm(info) {
        this.setState({
            showLinkBudget: false
        })
    }
    openApprove = () => {
        this.setState({
            ShowApproveDetail: true
        })
    }

    closeApprove = () => {
        this.setState({
            ShowApproveDetail: false
        })
    }

    getAssginUser = (value) => {
        window.assingUsers = value;
        window.sureAssign = true;
        let props = this.props;
        if(props.getUrlParam('status') === 'browse'){
            pageButtonClick.call(this)[pageButton.billSubmit]();
        }else{
            pageButtonClick.call(this)[pageButton.billSaveSubmit]();
        }
        window.assignBillId = '';
    }

    turnOff() {
        this.setState({
            compositedisplay: false
        });
        window.sureAssign = false;
        window.assingUsers = [];
        let props = this.props;
        let billid = window.assignBillId;
        if(!billid){
            billid = props.form.getFormItemsValue('head', 'pk_accrued_bill').value;
        }
        props.linkTo('/erm/erbudgetmg/accrued/card/index.html', { scene: props.getUrlParam('scene'), status: 'browse', id: billid});
    }

    onBeforeEvent(props, moduleId, key, value, index, record, status) {
        window.presetVar.bodyChangeValuesAll = record;
        return true;
    }

    onHideUploader = () => {
        this.setState({
            showUploader: false,
        });
        let id = this.props.getUrlParam('id') ? this.props.getUrlParam('id') : this.state.billId;
        let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId
        getAttachments(id).then(lists => {
            this.props.button.setButtonTitle('fileManager', multiLang.get('201102FYYT-0014')/***'附件'**/ + ' ' + lists + ' ');
        })
    }



    render() {
        let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId
        let btnModalConfig = {
            [pageButton.pageDel]: {
                // "2011-0004": "删除"
                // "2011-0005": "确认删除该单据吗？"
                title: multiLang && multiLang.get('2011-0004'),
                content: multiLang && multiLang.get('2011-0005'),
            },
            [pageButton.billInvalid]: {
                // "2011-0009": "作废",
                // "2011-0010": "确认作废该单据吗？"
                title: multiLang && multiLang.get('2011-0009'),
                content: multiLang && multiLang.get('2011-0010')
            },
            [pageButton.pageClose]: {
                // "2011-0007": "关闭"
                // "2011-0013": "确认关闭该单据吗？",
                title: multiLang && multiLang.get('2011-0007'),
                content: multiLang && multiLang.get('2011-0013')
            }
        }
        const { table } = this.props;
        let { createSimpleTable } = table;
        let { showUploader, target } = this.state;
        return (
            <div>
                <BillStyle
                    {...this.props}
                >
                    <BillHead
                        // "201102FYYT-0001": "费用预提单"
                        // title={multiLang && multiLang.get('201102FYYT-0001')}
                        title={this.state.Title}
                        refreshButtonEvent={refreshButtonClick.bind(this)}
                    >
                        <ButtonGroup
                            areaId="head"
                            buttonEvent={pageButtonClick.bind(this)}
                            modalConfig={btnModalConfig}
                        />

                        <Form
                            areaId="head"
                            onAfterEvent={afterEvent.bind(this)}
                        />
                    </BillHead>

                    <BillBody>
                        <CardTable
                            areaId="accrued_detail"
                            totalItemCode="amount"
                            modelSave={pageButtonClick.call(this)['pageSave']}
                            onAfterEvent={afterEvent.bind(this)}
                            onBeforeEvent={this.onBeforeEvent.bind(this)}
                            showIndex={true}
                        >
                            <ButtonGroup
                                buttonEvent={tableButtonClick.bind(this)}
                            />
                        </CardTable>

                        {this.state.isEffect == true ?
                            <CardTable
                                areaId="accrued_verify"
                                showIndex={true}
                            >
                            </CardTable>
                            : null}

                        <div>
                            {showUploader && < NCUploader
                                onHide={this.onHideUploader}
                                billId={this.state.billId}
                                targrt={target}
                                disableModify={this.props.form.getFormItemsValue('head', 'apprstatus')&&this.props.form.getFormItemsValue(this.formId, 'apprstatus').value!="-1"}
                                placement={'bottom'} />
                            }
                        </div>

                        <div>
                            <ApproveDetail
                                show={this.state.ShowApproveDetail}
                                close={this.closeApprove}
                                billtype={this.state.billtype}
                                billid={this.state.billId}
                            />
                               <AttachmentOther tableData={this.state.tableData}/>
                        </div>

                        <NCModal size="lg" show={
                            this.state.showModal
                        }
                            onHide={
                                this.close
                            }>
                            <NCModal.Header closeButton='true'>
                                {/*"201102FYYT-0002": "单据联查"*/}
                                <NCModal.Title>{multiLang && multiLang.get('201102FYYT-0002')}</NCModal.Title>
                            </NCModal.Header>

                            <NCModal.Body>
                                <div className="area-content">
                                    {createSimpleTable('billmodel', {
                                        onRowDoubleClick: onRowDoubleClick,
                                        showCheck: false,
                                        showIndex: true
                                    })}
                                </div>
                                <div>

                                </div>
                            </NCModal.Body>

                        </NCModal>


                    </BillBody>
                </BillStyle>
                <div>
                    <Inspection
                        show={this.state.showLinkBudget}
                        sourceData={this.state.sourceData}
                        cancel={this.cancel.bind(this)}
                        affirm={this.affirm.bind(this)}
                    />
                </div>
                <div>
                    {/*"201102FYYT-0003": "指派",*/}
                    {this.state.compositedisplay ? <ApprovalTrans
                        title={multiLang && multiLang.get('201102FYYT-0003')}
                        data={this.state.compositedata}
                        display={this.state.compositedisplay}
                        getResult={this.getAssginUser}
                        cancel={this.turnOff.bind(this)}
                    /> : ""}

                </div>
            </div>
        );

    }
}

FYYTBill = createPage({
    mutiLangCode: '2011',
    billinfo: {
        billtype: 'card',
        pagecode: presetVar.pageCode,
        headcode: presetVar.head.head1,
        bodycode: presetVar.body.body1
    },
    orderOfHotKey: [presetVar.body.body1]
    
})(FYYTBill);

export default FYYTBill;
