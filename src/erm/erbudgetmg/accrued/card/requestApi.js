/**
 * 后台接口文件
 */
import requestApi from "ssccommon/components/requestApi";
import { ajax, toast } from 'nc-lightapp-front';
import getDefDataFromMeta from 'ssccommon/utils/getDefDataFromMeta';

let pageCode = window.presetVar.pageCode;
let requestDomain = '';
let referResultFilterCondition = '';
let requestApiOverwrite = {
    ...requestApi,
    //单据模板接口
    tpl: (opt) => {
        let domain = '';//'https://mock.yonyoucloud.com/mock/172';
        ajax({
            url: opt.url || `${domain}/nccloud/erm/erbudgetmg/FysqTempletAction.do`,
            data: opt.data,
            success: opt.success
        });
    },
    query: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/erbudgetmg/AccruedViewAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    save: (opt) => {

        console.log('待保存数据:', opt.data);

        //数据适配开始
        opt.data.head[presetVar.head.head1].rows.map((row) => {
            for (let key in row.values) {
                if (row.values[key].value || row.values[key].value == 0) {
                    row.values[key].value += '';
                }
            }
            return row;
        });
        opt.data.body[presetVar.body.body1].rows.map((row) => {
            for (let key in row.values) {
                if (row.values[key].value || row.values[key].value == 0) {
                    row.values[key].value += '';
                }
            }
            return row;
        });
        //数据适配结束
        opt.data.pageid = pageCode;
        opt.data = {
            ...opt.data,
            uistate: opt.status,
            tradetype: window.presetVar.billtype,
            ntbCheck: window.ntbCheck,
            accessorybillid: opt.accessorybillid,
            fromssc: 'N',
            msg_pk_bill: null,
            userjson: "{'pagecode':'" + pageCode + "','appcode':'" + window.presetVar.appCode + "'}"
        }
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/erbudgetmg/AccruedSaveAction.do`,
            data: opt.data,
            success: (data) => {
                window.ntbCheck ='false';
                opt.success(data);
            }
        })
    },
    billInvalid: (opt) => {
        //数据适配开始
        opt.data.head[presetVar.head.head1].rows.map((row) => {
            for (let key in row.values) {
                if (row.values[key].value || row.values[key].value == 0) {
                    row.values[key].value += '';
                }
            }
            return row;
        });
        opt.data.body[presetVar.body.body1].rows.map((row) => {
            for (let key in row.values) {
                if (row.values[key].value || row.values[key].value == 0) {
                    row.values[key].value += '';
                }
            }
            return row;
        });
        //数据适配结束
        opt.data.pageid = pageCode;
        opt.data = {
            ...opt.data,
            uistate: 'add',
            tradetype: window.presetVar.billtype,
            ntbCheck: 'N',
            accessorybillid: null,
            fromssc: 'N',
            msg_pk_bill: null,
            billstatus: -1,
            userjson: "{'pagecode':" + pageCode + "}"
        }
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/erbudgetmg/AccruedInvalidAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    billRedBack: (opt) => {
        //数据适配开始
        opt.data.head[presetVar.head.head1].rows.map((row) => {
            for (let key in row.values) {
                if (row.values[key].value || row.values[key].value == 0) {
                    row.values[key].value += '';
                }
            }
            return row;
        });
        opt.data.bodys[presetVar.body.body1].rows.map((row) => {
            for (let key in row.values) {
                if (row.values[key].value || row.values[key].value == 0) {
                    row.values[key].value += '';
                }
            }
            return row;
        });
        let openbillid = "";
        opt.data.head[presetVar.head.head1].rows.map((row) => {
            for (let key in row.values) {
                if (row.values[key].value || row.values[key].value == 0) {
                    if (key === "pk_accrued_bill")
                        openbillid = row.values[key].value;
                }
            }
            return row;
        });
        opt.data = {
            ...opt.data,
            openbillid: openbillid,
            userjson: "{'pagecode':" + pageCode + "}"
        }
        //数据适配结束

        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/erbudgetmg/AccruedRedBackAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    billRemove: (opt) => {
        //数据适配开始
        let openbillid = "";
        opt.data.head[presetVar.head.head1].rows.map((row) => {
            for (let key in row.values) {
                if (row.values[key].value || row.values[key].value == 0) {
                    if (key === "pk_accrued_bill")
                        openbillid = row.values[key].value;
                }
            }
            return row;
        });
        //数据适配结束
        opt.data = {
            ...opt.data,
            openbillid: openbillid
        }
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/erbudgetmg/AccruedDeleteAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    loadData: (opt) => {
        //数据适配开始
        let reqData = {
            uistate: "add",
            tradetype: window.presetVar.billtype,
            head: {},
            body: {}
        };
        let props = opt.props;
        reqData = {
            ...reqData,
            ...genReqParamForLoadData(opt.meta)
        }

        //数据适配结束
        reqData.nodecode = window.presetVar.billtype;
        reqData.userjson = "{'pagecode':" + pageCode + "}";
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/erbudgetmg/AccruedDefDataAction.do`,
            data: reqData,
            success: (data) => {
                console.log('loadData值：', data)

                //数据适配开始
                //let referResultFilterCondition = JSON.parse(data.data.userjson);
                //referResultFilterCondition = JSON.parse(JSON.parse(data.data.userjson).crossrule);//交叉校验规则

                let bodyValues = data.data.body[presetVar.body.body1].rows[0].values;
                let numberInitialvalueField = ['global_currinfo','group_currinfo','org_currinfo','rowno'];

                    opt.meta[presetVar.body.body1].items.forEach((item) => {
                        let attrcode = item.attrcode;
                        if (bodyValues[attrcode]) {
                            if(item.itemtype != 'number' || numberInitialvalueField.indexOf(item.attrcode) >= 0){
                                item.initialvalue = {
                                    value: bodyValues[attrcode].value,
                                    display: bodyValues[attrcode].display = null ? bodyValues[attrcode].value : bodyValues[attrcode].display
                                };
                            } else{
                                item.initialvalue = {
                                    // TOP 金额字段不需要显示默认值 chengwebc DEL
                                    scale: bodyValues[attrcode].scale
                                };
                            }
                            
                            if (attrcode.indexOf('defitem') < 0) {
                                item.scale = bodyValues[attrcode].scale;
                            }

                            //处理交叉校验规则和参照过滤
                            filterAllRefParm(item, props, presetVar.body.body1);
                        }

                    });
                    opt.meta[presetVar.body.body1 + '&childform2'].items.forEach((item, index) => {

                        let attrcode = item.attrcode;
                        if (bodyValues[attrcode] || attrcode.indexOf('defitem') < 0) {
                            item.scale = bodyValues[attrcode].scale;
                        }
                        //处理交叉校验规则和参照过滤
                        filterAllRefParm(item, props, presetVar.body.body1);

                    });
     

                let headValues = data.data.head[presetVar.head.head1].rows[0].values;
     
                opt.meta[presetVar.head.head1].items.forEach((item) => {
                        let attrcode = item.attrcode;
                        if (headValues[attrcode]) {
                            item.initialvalue = {
                                value: headValues[attrcode].value,
                                display: headValues[attrcode].display = null ? headValues[attrcode].value : headValues[attrcode].display
                            };
                            if (attrcode.indexOf('defitem') < 0) {
                                item.scale = headValues[attrcode].scale;
                            }

                            //处理交叉校验规则和参照过滤
                            filterAllRefParm(item, props, presetVar.head.head1);
                        }
                    });
                //适配多表头
                if (opt.meta.formrelation) {
                    opt.meta.formrelation[presetVar.head.head1].forEach((item) => {
                        if (opt.meta[item]) {
                            opt.meta[item].items.forEach((key) => {
                                let attrcode = key.attrcode;
                                if (headValues[attrcode]) {
                                    key.initialvalue = {
                                        value: headValues[attrcode].value,
                                        display: headValues[attrcode].display = null ? headValues[attrcode].value : headValues[attrcode].display
                                    };
                                    if (attrcode.indexOf('defitem') < 0) {
                                        key.scale = headValues[attrcode].scale;
                                    }
                                    //处理交叉校验规则和参照过滤
                                    filterAllRefParm(key, props, presetVar.head.head1);
                                }
                            })
                        }

                    });
                }

                //数据适配结束
                console.log('opt值：', opt)
                opt.success(opt.meta);
            }
        })
    },
    changeByHead: (opt) => {
        opt.data = {
            ...opt.data,
            userjson: "{'pagecode':" + pageCode + "}"
        }
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/erbudgetmg/AccruedValueChangedAction.do`,
            data: opt.data,
            async: false,
            success: (data) => {
                //referResultFilterCondition = JSON.parse(JSON.parse(data.data.userjson).crossrule);//交叉校验规则
                opt.success(data);
            }
        });
    },
    changeByBody: (opt) => {
        opt.data = {
            ...opt.data,
            userjson: "{'pagecode':" + pageCode + "}"
        }
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/erbudgetmg/AccruedValueChangedAction.do`,
            data: opt.data,
            async: false,
            success: (data) => {
                //referResultFilterCondition = JSON.parse(JSON.parse(data.data.userjson).crossrule);//交叉校验规则
                opt.success(data);
            }
        });
    },
    copy: (opt) => { //单据复制
        opt.data.pageid = pageCode;
        opt.data = {
            ...opt.data,
            userjson: "{'pagecode':" + pageCode + "}"
        }
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/erbudgetmg/AccruedCopyAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    billSubmit: (opt) => {
        let openbillid = "";
        opt.data.head[presetVar.head.head1].rows.map((row) => {
            for (let key in row.values) {
                if (row.values[key].value || row.values[key].value == 0) {
                    if (key === "pk_accrued_bill")
                        openbillid = row.values[key].value;
                }
            }
            return row;
        });
        if (openbillid == '') {
            openbillid = null; //编辑态需要给openbillid赋值null
        }
        if(opt.assignBillId){
            openbillid = opt.assignBillId;
        }
        opt.data.pageid = pageCode;
        opt.data = {
            ...opt.data,
            accessorybillid: opt.accessorybillid,
            userjson: "{'pagecode':" + pageCode +
                ",'openbillid':" + openbillid +
                ",'tradetype':'" + window.presetVar.billtype +
                "','ntbCheck':'" + window.ntbCheck + "'" +
                ",'fromssc':'N'" +
                ",'uistate':" + opt.data.uistate +
                ",'assingUsers':" + JSON.stringify(window.assingUsers) +
                "}"
        }
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/erbudgetmg/AccruedSubmitAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
                window.ntbCheck ='false';
            },
            error: (data) => {
                toast({content:data.message,color:'danger'});
            }
        });
    },
    billRecall: (opt) => {
        let openbillid = "";
        opt.data.head[presetVar.head.head1].rows.map((row) => {
            for (let key in row.values) {
                if (row.values[key].value || row.values[key].value == 0) {
                    if (key === "pk_accrued_bill")
                        openbillid = row.values[key].value;
                }
            }
            return row;
        });
        opt.data.pageid = pageCode;
        opt.data = {
            ...opt.data,
            userjson: "{'pagecode':" + pageCode +
                ",'openbillid':" + openbillid +
                "}"
        }
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/erbudgetmg/AccruedRecallAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            },
            error: (data) => {
                toast({content:data.message,color:'danger'});
            }
        });
    },
    billView: (opt) => {
        let openbillid = "";
        let pk_tradetype = "";
        opt.data.head[presetVar.head.head1].rows.map((row) => {
            for (let key in row.values) {
                if (row.values[key].value || row.values[key].value == 0) {
                    if (key === "pk_accrued_bill") { openbillid = row.values[key].value; }
                    if (key === "pk_tradetype") { pk_tradetype = row.values[key].value; }
                }
            }
            return row;
        });
        opt.data.pageid = pageCode;
        opt.data = {
            ...opt.data,
            userjson: "{'pagecode':'201101FYSQLC_list'" +
                ",'openbillid':'" + openbillid + "'" +
                ",'pk_tradetype':'" + pk_tradetype + "'" +
                "}"
        }
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/erbudgetmg/AccruedLinkQueryAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            },
            error: (data) => {
                toast({content:data.message,color:'danger'});
            }
        });
    },
    handleLoadDataResDada: (meta, data) => {
        handleLoadDataResDada(meta, data)
    },
    generatBillId: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/action/generatBillId.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            },
            error: (data) => {
                toast({content:data.message,color:'danger'});
            }
        });
    },
    getUnEditAttributes:(opt) =>{
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/pub/ErmAttributeEditableAction.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            },
            error:(data) =>{
                toast({content:data.message,color:'warning'});
            }
        });
    },
}

function filterAllRefParm(item, props, flag) {

    let PublicDefaultRefFilterPath = 'nccloud.web.action.erm.ref.sqlbuilder.PublicDefaultRefSqlBuilder';//公共 
    let OrgTreeRefFilterPath = 'nccloud.web.action.erm.ref.sqlbuilder.OrgTreeRefSqlBuilder';//组织

    if (flag == presetVar.head.head1) {
        if (item.attrcode == 'pk_org' || item.attrcode == 'pk_org_v') {
            item.queryCondition = () => {
                let paramurl = getRequest1(parent.window.location.hash);
                //如果没取到应用编码再取一下外层
                if (!paramurl.c) {
                    paramurl = getRequest1(parent.parent.window.location.hash);
                }
                let data = props.form.getAllFormValue(presetVar.head.head1).rows[0].values['pk_group'].value;
                return { pk_group: data, TreeRefActionExt: OrgTreeRefFilterPath, DataPowerOperationCode: 'fi',/**使用权组**/isDataPowerEnable: 'Y', appcode: paramurl.c }; // 根据pk_group过滤
            };
        }

        if (item.attrcode == 'reason') {
            item.queryCondition = () => {
                return { pk_org: props.form.getAllFormValue(presetVar.head.head1).rows[0].values['pk_org'].value, DataPowerOperationCode: 'fi',/**使用权组**/isDataPowerEnable: 'Y' };
            };
        }

        if (item.attrcode == 'operator') {
            item.queryCondition = () => {
                return { pk_org: props.form.getAllFormValue(presetVar.head.head1).rows[0].values['operator_org'].value, DataPowerOperationCode: 'fi',/**使用权组**/isDataPowerEnable: 'Y' };
            };
        }

        if (item.attrcode == 'operator_dept') {
            item.queryCondition = () => {
                return { pk_org: props.form.getAllFormValue(presetVar.head.head1).rows[0].values['operator_org'].value, DataPowerOperationCode: 'fi',/**使用权组**/isDataPowerEnable: 'Y' };
            };
        }

    }




    if (flag == presetVar.body.body1) {
        if (item.attrcode == 'assume_dept') {
            item.queryCondition = () => {
                return { pk_org: props.cardTable.getClickRowIndex(presetVar.body.body1).record.values['assume_org'].value, enablestate: '2', DataPowerOperationCode: 'fi',/**使用权组**/isDataPowerEnable: 'Y' };
            };
        }

        if (item.attrcode == 'pk_iobsclass') {
            item.queryCondition = () => {
                return { pk_org: props.cardTable.getClickRowIndex(presetVar.body.body1).record.values['assume_org'].value, DataPowerOperationCode: 'fi',/**使用权组**/isDataPowerEnable: 'Y' };
            };
        }

        if (item.attrcode == 'pk_project') {
            item.queryCondition = () => {
                return { pk_org: props.cardTable.getClickRowIndex(presetVar.body.body1).record.values['assume_org'].value, DataPowerOperationCode: 'fi',/**使用权组**/isDataPowerEnable: 'Y' };
            };
        }


        if (item.attrcode == 'pk_supplier') {
            item.queryCondition = () => {
                return { pk_org: props.cardTable.getClickRowIndex(presetVar.body.body1).record.values['assume_org'].value, DataPowerOperationCode: 'fi',/**使用权组**/isDataPowerEnable: 'Y' };
            };
        }

        if (item.attrcode == 'pk_customer') {
            item.queryCondition = () => {
                return { pk_org: props.cardTable.getClickRowIndex(presetVar.body.body1).record.values['assume_org'].value, DataPowerOperationCode: 'fi',/**使用权组**/isDataPowerEnable: 'Y' };
            };
        }
    }




    //处理交叉校验规则
    if (item.itemtype == 'refer') {
        let oldQueryCondition = item.queryCondition;
        let crossrule_itemkey = item.attrcode;
        item.queryCondition = (obj) => {
            let crossrule_datasout = props.createMasterChildData(presetVar.pageId, presetVar.head.head1, presetVar.body.body1);
            let crossrule_tradetype = props.form.getAllFormValue(presetVar.head.head1).rows[0].values['pk_tradetype'].value
            let crossrule_org = props.form.getAllFormValue(presetVar.head.head1).rows[0].values['pk_org'].value
            let crossrule_datatypes = 'billcard';
            let crossrule_datas = crossrule_datasout;

            if (window.presetVar.bodyChangeValuesAll) {
                crossrule_datas.body[presetVar.body.body1].rows = [window.presetVar.bodyChangeValuesAll]
            }
            let conditionFlag = true;
            if (oldQueryCondition == null || oldQueryCondition == undefined || oldQueryCondition == 'undefined')
                conditionFlag = false;
            let oldData = '';
            if (conditionFlag)
                oldData = oldQueryCondition();
            if (oldData == null || oldData == undefined || oldData == 'undefined')
                oldData = '';
            let config = {
                crossrule_datas: JSON.stringify(crossrule_datas),
                crossrule_tradetype: crossrule_tradetype,
                crossrule_org: crossrule_org,
                crossrule_datatypes: crossrule_datatypes,
                crossrule_area: flag,
                crossrule_itemkey: crossrule_itemkey,
                ...oldData
            };

            //参照类型的自定义项字段增加组织过滤
            if(!oldData.pk_org){
                config.pk_org = crossrule_org;
            }

            if (obj == undefined || obj == null || obj.refType == undefined || obj.refType == null) {
                if (config.TreeRefActionExt == null || config.TreeRefActionExt == undefined || config.TreeRefActionExt.length == 0)
                    config.TreeRefActionExt = PublicDefaultRefFilterPath;
                return config;
            }

            if (obj.refType == 'grid') {
                if (config.GridRefActionExt == null || config.GridRefActionExt == undefined || config.GridRefActionExt.length == 0)
                    config.GridRefActionExt = PublicDefaultRefFilterPath;
            } else if (obj.refType == 'tree') {
                if (config.TreeRefActionExt == null || config.TreeRefActionExt == undefined || config.TreeRefActionExt.length == 0)
                    config.TreeRefActionExt = PublicDefaultRefFilterPath;
            } else if (obj.refType == 'gridTree') {
                if (config.GridRefActionExt == null || config.GridRefActionExt == undefined || config.GridRefActionExt.length == 0)
                    config.GridRefActionExt = PublicDefaultRefFilterPath;
            }



            return config;

        }
    }

}


function handleLoadDataResDada(meta, data) {
    let referResultFilterCondition = JSON.parse(data.data.userjson);

    if (data.data.body[presetVar.body.body1].rows[0]) {

        let bodyValues = data.data.body[presetVar.body.body1].rows[0].values;
        for (let attrcode in bodyValues) {

            meta[presetVar.body.body1].items.forEach((item) => {

                if (item.attrcode == attrcode) {
                    item.initialvalue = {
                        value: bodyValues[attrcode].value,
                        display: bodyValues[attrcode].display = null ? bodyValues[attrcode].value : bodyValues[attrcode].display
                    };

                    // let filterCondition = referResultFilterCondition[presetVar.body.body1][item.attrcode];
                    // if (filterCondition) {
                    //     item.referFilterAttr = {[item.attrcode] :  filterCondition};
                    // }
                    //处理交叉校验规则和参照过滤
                    // filterAllRefParm(item, bodyValues);
                }

            });
        }
    }

    let headValues = data.data.head[presetVar.head.head1].rows[0].values;
    for (let attrcode in headValues) {
        meta[presetVar.head.head1].items.forEach((item) => {
            if (item.attrcode == attrcode) {
                item.initialvalue = {
                    value: headValues[attrcode].value,
                    display: headValues[attrcode].display = null ? headValues[attrcode].value : headValues[attrcode].display
                };

                // let filterCondition = referResultFilterCondition[presetVar.head.head1][item.attrcode];
                // if (filterCondition) {
                //     item.referFilterAttr = {[item.attrcode] :  filterCondition};
                // }
                //处理交叉校验规则和参照过滤
                // filterAllRefParm(item, headValues);
            }

        });
    }
}

function genReqParamForLoadData(meta) {
    // TOP 修改取模版默认值方法（修改前模版默认值无法默认带出） 190225 MOD
    // let reqData = {
    //     head: {},
    //     body: {}
    // }
    // for (let areaCode in meta) {
    //     let areaData = meta[areaCode];
    //     //满足以下条件才是区域。 relationcode用来区分cardTable
    //     if (!areaData || (areaCode != 'head' && areaCode != 'accrued_detail')) {
    //         continue;
    //     }
    //     //设定区域初始状态
    //     switch (areaData.moduletype) {
    //         case 'form':
    //             reqData.head[areaCode] = {
    //                 areaType: 'form',
    //                 "areacode": areaCode,
    //                 rows: [{
    //                     'values': {}
    //                 }]
    //             }
    //             areaData.items.forEach((item) => {
    //                 reqData.head[areaCode].rows[0].values[item.attrcode] = {
    //                     value: null
    //                 };
    //             });
    //             break;
    //         case 'table':
    //             reqData.body[areaCode] = {
    //                 areaType: 'table',
    //                 "areacode": areaCode,
    //                 rows: [{
    //                     'values': {}
    //                 }]
    //             }
    //             areaData.items.forEach((item) => {
    //                 reqData.body[areaCode].rows[0].values[item.attrcode] = {
    //                     value: null
    //                 };
    //             });
    //             break;
    //         case 'search':
    //             break;
    //     }
    // }
    // return reqData;
    return getDefDataFromMeta(meta, '', 'body');
    // BTM 修改取模版默认值方法
}
export default requestApiOverwrite;

function getRequest1(url) {
    if (!url)
        url = document.location.search;
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substring(url.indexOf("?") + 1);
        var strs = str.split("&");
        for (var i = 0; i < strs.length; i++) {
            theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
        }
    }
    return theRequest;
}