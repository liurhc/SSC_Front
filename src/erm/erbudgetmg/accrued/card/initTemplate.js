/**
 * 页面初始化逻辑：模板及初始化数据的配置及加载
 */
import { getAttachments,getMultiLang } from 'nc-lightapp-front';
import requestApi from './requestApi';
import { setTableExtendCol } from 'ssccommon/components/bill';
import { tableExtendButtonClick } from './events';
import linkQueryApi from '../../../public/components/linkquery/linkquerybills';
import './index.less';

const head1 = window.presetVar.head.head1;
const body1 = window.presetVar.body.body1;
const body2 = window.presetVar.body.body2;
const pageButton = presetVar.pageButton;
const pageCode = window.presetVar.pageCode;
const statusVar = window.presetVar.status;
const areaBtnAction = window.presetVar.areaBtnAction;
let alreadyLoadData = false;
export default function (props) {
    let pageStatus = props.getUrlParam('status');
    if (props.getSearchParam("p") && props.getSearchParam("p") != "") {
        window.presetVar.pageCode = props.getSearchParam("p");
    } else {
        if (props.getUrlParam("pagecode") && props.getSearchParam("pagecode") != "") {
            window.presetVar.pageCode = props.getUrlParam("pagecode");
        }
    }
    if (props.getSearchParam("c") && props.getSearchParam("c") != "") {
        window.presetVar.appCode = props.getSearchParam("c");

    } else {
        if (props.getUrlParam("appcode") && props.getSearchParam("appcode") != "") {
            window.presetVar.appCode = props.getUrlParam("appcode");
        }
    }
    // if(props.getUrlParam("tradetype")!=null && props.getUrlParam("tradetype")!=undefined)
    // {
    //     window.presetVar.billtype = props.getUrlParam("tradetype");
    // }
    props.createUIDom(
        {
            pagecode: window.presetVar.pageCode,
            appcode: window.presetVar.appCode
        },
        (data) => {
            let meta = data.template;
            let bmeta = data.button;
            let transtype = data.context.paramMap.transtype;

            this.setState({ Title: data.context.paramMap.transtype_name });

            window.presetVar.billtype = (props.getUrlParam("tradetype") && props.getUrlParam("tradetype") == '') ? props.getUrlParam("tradetype") : transtype;

            presetVar.pageId = meta.pageid;
            if (props.getUrlParam('status') == statusVar.browse) {

                //模板处理
                tplHandle.call(this, meta, bmeta);

                //业务数据查询
                let billId = props.getUrlParam('id');
                requestApi.query({
                    data: {
                        "openbillid": billId,
                        "pagecode": window.presetVar.pageCode
                    },
                    success: (data) => {
                        if (data.data.head[head1].rows[0].values.effectstatus.value == 1) {//已生效才显示核销预提页签
                            this.setState({ isEffect: true, tableData:data.data });
                        } else {
                            this.setState({ isEffect: false, tableData:data.data});
                        }
                        props.form.setAllFormValue({ [head1]: data.data.head[head1] });
                        //设置表体数据
                        props.cardTable.setTableData(body1, data.data.bodys[body1]);
                        if (data.data.bodys[body2]) {
                            props.cardTable.setTableData(body2, data.data.bodys[body2]);
                        }
                        dataHandle(data.data.head[head1], props);
                    }
                })
            } else {
                //加载模板默认数据
                let metadata = {
                    meta: meta,
                    props: props
                }
                requestApi.loadData({
                    ...metadata,
                    success: (data) => {
                        alreadyLoadData = true;
                        //模板处理
                        tplHandle.call(this, meta, bmeta);
                        //复制操作
                        let copyFromBillId = props.getUrlParam('copyFromBillId');
                        if (copyFromBillId) {
                            requestApi.copy({
                                data: {
                                    openbillid: copyFromBillId,
                                },
                                success: (data) => {
                                    props.form.setAllFormValue({ [head1]: data.data.head[head1] });
                                    //设置表体数据
                                    props.cardTable.setTableData(body1, data.data.body[body1]);
                                    props.cardTable.setStatus(head1, statusVar.edit);
                                    props.cardTable.setStatus(body1, statusVar.edit);
                                    dataHandle(data.data.head[head1], props);
                                }
                            })
                        };
                        //红冲操作
                        let redBackFromBillId = props.getUrlParam('redBackFromBillId');
                        if (redBackFromBillId) {
                            requestApi.query({
                                data: {
                                    "openbillid": redBackFromBillId,
                                    "pagecode": window.presetVar.pageCode
                                },
                                success: (data) => {
                                    requestApi.billRedBack({
                                        data: data.data,
                                        success: (data) => {
                                            //设置表头数据
                                            props.form.setAllFormValue({ [head1]: data.data.head[head1] })
                                            //设置表体数据
                                            data.data.body && props.cardTable.setTableData(body1, data.data.body[body1]);
                                            let disable = new Object();
                                            Object.keys(data.data.head[head1].rows[0].values).map(key => { if (key != 'reason' && key != 'billdate') return disable[key] = true });//表头除了日期、事由都不可编辑
                                            props.form.setFormItemsDisabled(head1, disable);
                                            let disable1 = [];
                                            Object.keys(data.data.body[body1].rows[0].values).map(key => { if (key != 'amount') return disable1.push(key) });//表体除了金额都不可编辑
                                            props.cardTable.setColEditableByKey(body1, disable1, true);
                                            props.cardTable.setStatus(body1, "edit");

                                            dataHandle(data.data.head[head1], props);
                                        }
                                    })
                                }
                            })
                        };
                        //业务数据查询 (修改操作)
                        let billId = props.getUrlParam('id');
                        if (billId) {
                            requestApi.query({
                                data: {
                                    "openbillid": billId,
                                    "pagecode": window.presetVar.pageCode
                                },
                                success: (data) => {
                                    props.form.setAllFormValue({ [head1]: data.data.head[head1] });
                                    //设置表体数据
                                    props.cardTable.setTableData(body1, data.data.bodys[body1]);
                                }
                            })
                        }else{
                            props.cardTable.addRow(body1, undefined, undefined, false);
                        }
                    }
                });

            }


        }
    );

    function tplHandle(meta, bmeta) {
        setTableExtendCol(props, meta, bmeta, [{
            tableAreaId: body1,
            onButtonClick: tableExtendButtonClick.bind(this),
            copyRowWithoutCols: ['pk_accrued_detail']
        }]);
        props.button.hideButtonsByAreas(head1);
        //按钮存入组件

        let id = props.getUrlParam('id') ? props.getUrlParam('id') : this.state.billId;
        id = id == "" ? 0 : id;
        getAttachments(id).then(lists => {
            if (lists.length == 0) {
                lists = 0;
            }
            getMultiLang({moduleId: 2011, domainName: 'erm',currentLocale: 'simpchn', callback: (json) => {
                props.button.setButtonTitle('fileManager', json['201102FYYT-0014'] + ' ' + lists + ' ');//附件
            }})
        })
        props.button.setButtons(bmeta);

        //肩部按钮可见状态：页面和表格的肩部按钮，这里逻辑根据实际业务实现。
        let btnsVisibleObj = {};
        let isAddOrEditStatus = pageStatus == statusVar.edit || pageStatus == statusVar.add;
        let isBrowseStatus = pageStatus == statusVar.browse
        for (let btnKey in pageButton) {
            btnsVisibleObj[btnKey] = false;
        }
        if (isAddOrEditStatus) {
            btnsVisibleObj[pageButton.pageSave] = true;
            btnsVisibleObj[pageButton.billSubmit] = false;
            btnsVisibleObj[pageButton.billSaveSubmit] = true;
            btnsVisibleObj[pageButton.fileManager] = true;
            btnsVisibleObj[body1 + '_Add'] = true;
            btnsVisibleObj[body1 + '_Delete'] = true;
            btnsVisibleObj[body1 + '_Copy'] = true;
            btnsVisibleObj[body1 + '_Insert'] = true;
            btnsVisibleObj[pageButton['linkVoucher']] = false;
            btnsVisibleObj[pageButton['linkBudget']] = false;
            btnsVisibleObj['buttongroup6'] = false;
            //新增态时取消按钮显示
            if (pageStatus == statusVar.add || pageStatus == statusVar.edit)
                btnsVisibleObj[pageButton.pageClose] = true;
        }
        if (isBrowseStatus) {
            btnsVisibleObj[pageButton.pageEdit] = true;
            btnsVisibleObj[pageButton.pageCopy] = true;
            btnsVisibleObj[pageButton.pageDel] = true;
            btnsVisibleObj[pageButton.pagePrint] = true;
            btnsVisibleObj[pageButton.imageShow] = true;
            btnsVisibleObj[pageButton.imageUpload] = true;
            btnsVisibleObj[pageButton.fileManager] = true;
            btnsVisibleObj[pageButton.billView] = true;
            btnsVisibleObj[pageButton['linkVoucher']] = false;
            btnsVisibleObj[pageButton['linkBudget']] = false;
            btnsVisibleObj[pageButton.billSaveSubmit] = false;
            btnsVisibleObj[pageButton.pageClose] = false;
            btnsVisibleObj[body1 + '_Add'] = false;
            btnsVisibleObj[body1 + '_Delete'] = false;
            btnsVisibleObj[body1 + '_Copy'] = false;
            btnsVisibleObj[body1 + '_Insert'] = false;
            btnsVisibleObj['buttongroup6'] = true;
        }

        props.button.setButtonsVisible(btnsVisibleObj);
        
        let multiLang = props.MutiInit.getIntl(2011);
        //table扩展按钮弹窗确认框设置
        props.button.setPopContent(body1 + areaBtnAction.del, multiLang&&multiLang.get('201102FYYT-0015')/***'确认要删除该信息吗？'**/)
        /* 设置操作列上删除按钮的弹窗提示 */

        switch (pageStatus) {
            case statusVar.edit:
                meta[head1].status = statusVar.edit;
                meta[body1].status = statusVar.edit;
                break;
            case statusVar.add:
                meta[head1].status = statusVar.add;
                meta[body1].status = statusVar.edit;
                break;
            case statusVar.browse:
                meta[head1].status = statusVar.browse;
                meta[body1].status = statusVar.browse;
                break;
        }

        //设置模板
        props.meta.setMeta(meta);

        // if (statusVar.browse == pageStatus) {
        //     props.cardTable.hideColByKey(body1, 'opr');
        // }

        requestApi.tpl({
            data: {
                appcode: '201102JCLF',
                pagecode: '201102JCLF_LCBX'
            },
            success: (data) => {
                let metaModel = data.data;
                meta = {
                    ...meta,
                    billmodel: metaModel.jkdBillModel
                }
                //设置模板,整合弹出层，因多层异步，所以合并
                props.meta.setMeta(meta);

                //联查单据时单据编号显示下划线
                props.table.setTableRender('billmodel', "djbh", (text, record, index) => {
                    return ( <a className = "hyperlinks"
                                onClick = {() => {
                                    linkQueryApi.link({
                                        data : {
                                            openbillid : record.pk_jkbx.value,
                                            tradetype : record.djlxbm.value,
                                            props : props
                                        }
                                    });
                                }} > { record.djbh.value } </ a>
                    )
                })
            }
        });


    }

    props.setHashChangeCallback(() => {
        let pagestatusVar = props.getUrlParam('status'); //重新设置状态变量
        //openBillId = props.getUrlParam('id'); //重设单据id
        //presetVar.openBillId = openBillId;
        window.presetVar.status = pagestatusVar;

        let isShowTableExtCol = false; //是否显示表格扩展列
        switch (pagestatusVar) {
            case statusVar.browse:
                isShowTableExtCol = false;
                break;
            case statusVar.add:
            case statusVar.edit:
                isShowTableExtCol = true;

                //只有以添加/编辑态打开单据时，才会在初始化函数中调用loadData请求，所以如果从浏览态直接打开页面时，是不会加载loaddata的，所以如果页面此时从浏览态转到添加/编辑态，需要加载loadData
                if (!alreadyLoadData) {
                    let metadata = {
                        meta: props.meta.getMeta(),
                        props: props
                    }
                    requestApi.loadData({
                        ...metadata,
                        success: (meta) => {
                            alreadyLoadData = true;
                            props.meta.setMeta(meta);
                        }
                    })
                }
                break;
        }

        //浏览态不显示表体扩展列
        // if (isShowTableExtCol) {
        //     props.cardTable.showColByKey(body1, 'opr'); //显示扩展列
        //     // props.cardTable.showColByKey(body2, 'opr'); //显示扩展列
        // } else {
        //     props.cardTable.hideColByKey(body1, 'opr'); //隐藏扩展列
        //     // props.cardTable.hideColByKey(body2, 'opr'); //隐藏扩展列
        // }

        let copyFromBillId = props.getUrlParam('copyFromBillId');
        if (copyFromBillId) {
            requestApi.copy({
                data: {
                    openbillid: copyFromBillId,
                },
                success: (data) => {
                    props.form.setAllFormValue({ [head1]: data.data.head[head1] });
                    //设置表体数据
                    props.cardTable.setTableData(body1, data.data.body[body1]);
                    //处理按钮相关状态
                    dataHandle(data.data.head[head1], props);
                }
            })//复制单据。 因为复制单据操作 既能在初始态触发，又可以被交互态触发。所以没有把该函数写在按钮事件上。
        } else {
            let headData = props.form.getAllFormValue(head1);
            //根据单据状态，改变按钮的显示的状态
            dataHandle(headData, props);
        }

        //处理可编辑状态
        changeComStatus(pagestatusVar);
    })


    //改变表头表体组件的编辑状态
    function changeComStatus(pageStatus) {
        let changeStatus = null;
        switch (pageStatus) {
            case statusVar.edit:
            case statusVar.add:
                changeStatus = statusVar.edit;
                break;
            case statusVar.browse:
                changeStatus = statusVar.browse;
                break;
        }
        props.form.setFormStatus(head1, changeStatus);
        props.cardTable.setStatus(body1, changeStatus);
        // props.cardTable.setStatus(body2, changeStatus);
    }

}

function dataHandle(head, props) {

    //处理数据和按钮状态
    if (head == null || head.rows[0] == null || head.rows[0].values == null)
        return;
    let scene = props.getUrlParam('scene');
    let deal = props.getUrlParam("deal");
    let sxbz = head.rows[0].values.effectstatus.value;//单据生效状态
    let spzt = head.rows[0].values.apprstatus.value;//审批状态
    let djzt = head.rows[0].values.billstatus.value//单据状态
    let rbzt = head.rows[0].values.redflag.value//红冲状态
    let btnsVisibleObj = {};
    let pageStatus = props.getUrlParam('status');
    let isAddOrEditStatus = pageStatus == statusVar.edit || pageStatus == statusVar.add;
    if (isAddOrEditStatus) {
        for (let btnKey in pageButton) {
            btnsVisibleObj[btnKey] = false;
        }
        btnsVisibleObj[pageButton.pageSave] = true;
        btnsVisibleObj[pageButton.billSubmit] = false;
        if (rbzt != 1 && rbzt != 2) {
            btnsVisibleObj[pageButton.billSaveSubmit] = true;
        }
        btnsVisibleObj[pageButton.fileManager] = true;
        btnsVisibleObj[body1 + '_Add'] = true;
        btnsVisibleObj[body1 + '_Delete'] = true;
        btnsVisibleObj[body1 + '_Copy'] = true;
        btnsVisibleObj[body1 + '_Insert'] = true;
        btnsVisibleObj[pageButton['linkVoucher']] = false;
        btnsVisibleObj[pageButton['linkBudget']] = false;
        btnsVisibleObj['buttongroup6'] = false;
        //新增态时取消按钮显示
        if (pageStatus == statusVar.add || pageStatus == statusVar.edit){
            btnsVisibleObj[pageButton.pageClose] = true;
        }
        props.button.setButtonsVisible(btnsVisibleObj);
        return;
    }

    let isBrowseStatus = pageStatus == statusVar.browse
    for (let btnKey in pageButton) {
        btnsVisibleObj[btnKey] = false;
        //红冲单据优先处理
        if (rbzt == 1) {
            if ((btnKey == pageButton.imageShow || btnKey == pageButton.pagePrint || btnKey == pageButton.pageDel || btnKey == pageButton.fileManager))
                btnsVisibleObj[btnKey] = isBrowseStatus;
        }
        //来源于审批人门户、作业处理时 显示 ：修改、影像扫描|影像查看、联查预算、联查凭证、更多（联查审批情况、联查单据、附件、关闭/重启、打印）
        else if ((scene == 'approve' || scene == 'approvesce' || scene == 'zycl')) {
            if (spzt == "1" && (btnKey == pageButton.imageShow || btnKey == pageButton.imageUpload || btnKey == pageButton.pagePrint || btnKey == pageButton.billView || btnKey == pageButton.billApprove))
                btnsVisibleObj[btnKey] = isBrowseStatus;
            else if ((btnKey == pageButton.pageEdit || btnKey == pageButton.imageShow || btnKey == pageButton.imageUpload || btnKey == pageButton.pagePrint || btnKey == pageButton.billView || btnKey == pageButton.billApprove))
                btnsVisibleObj[btnKey] = isBrowseStatus;
            //审批中心打开的单据点击修改时需要处理按钮状态
        }
        //来源于作业查询时 显示 ：影像查看、联查预算、联查凭证、更多（联查审批情况、联查单据、打印）
        else if ((scene == 'zycx' || scene == 'bzcx' || scene == 'fycx')) {
            if ((btnKey == pageButton.imageShow || btnKey == pageButton.pagePrint || btnKey == pageButton.billView || btnKey == pageButton.billApprove || btnKey == pageButton.fileManager))
                btnsVisibleObj[btnKey] = isBrowseStatus;
        }
        else {
            //审批状态为初始态，显示：修改|删除|复制、提交、影像扫描|影像查看、更多（附件、作废、打印）
            if (spzt == "-1" && (btnKey == pageButton.pagePrint || btnKey == pageButton.pageCopy || btnKey == pageButton.imageShow || btnKey == pageButton.imageUpload || btnKey == pageButton.billSubmit || btnKey == pageButton.billInvalid || btnKey == pageButton.pageEdit || btnKey == pageButton.pageDel)) {
                btnsVisibleObj[btnKey] = isBrowseStatus;
                props.button.setMainButton([pageButton.billSubmit],true);
            }
            //审批状态为提交态，显示：复制、收回、影像扫描|影像查看、更多（联查审批情况、附件、打印）
            else if ((spzt == "3" || spzt == "2" ) && (btnKey == pageButton.pagePrint || btnKey == pageButton.pageCopy || btnKey == pageButton.imageShow || btnKey == pageButton.imageUpload || btnKey == pageButton.billApprove)) {
                btnsVisibleObj[btnKey] = isBrowseStatus;
            }
            //审批状态为通过态，显示：复制、影像查看、更多（联查审批情况、联查单据、附件、关闭/重启、打印）
            else if (spzt == "1" && (btnKey == pageButton.pageCopy || btnKey == pageButton.imageShow || btnKey == pageButton.pagePrint || btnKey == pageButton.billView || btnKey == pageButton.billApprove)) {
                btnsVisibleObj[btnKey] = isBrowseStatus;
            }
        }

        //已生效且非红冲的单据显示红冲按钮
        if (btnKey == pageButton.billRedBack) {
            if (sxbz == "1" && rbzt != 1) {
                btnsVisibleObj[btnKey] = true;
            } else {
                btnsVisibleObj[btnKey] = false;
            }
        }

        //已提交显示收回按钮，红冲单据例外
        if (btnKey == pageButton.billRecall && !(scene == 'approve'  || scene == 'approvesce' || scene == 'zycl' || scene == 'zycx' || scene == 'bzcx' || scene == 'fycx')) {
            if (spzt == "3") {
                btnsVisibleObj[btnKey] = isBrowseStatus;
            } else {
                btnsVisibleObj[btnKey] = false;
            }
        }

        //已完成单据，隐藏影像扫描
        if (btnKey == pageButton.imageUpload) {
            if (sxbz == "1") {
                btnsVisibleObj[btnKey] = false;
            } else {
                if(scene != 'zycx' && scene != 'bzcx' && scene != 'fycx'){
                    btnsVisibleObj[btnKey] = isBrowseStatus;
                }
            }
        }

        //已完成的非红冲单据隐藏附件
        if (btnKey == pageButton.fileManager) {
            if (sxbz == "1" && rbzt != 1) {
                btnsVisibleObj[btnKey] = false;
            } else {
                btnsVisibleObj[btnKey] = isBrowseStatus;
            }
        }

        //已完成单据显示联查单据,红冲单据例外
        if (btnKey == pageButton.billView) {
            if (sxbz == "1" && rbzt != 1) {
                btnsVisibleObj[btnKey] = true;
            } else {
                btnsVisibleObj[btnKey] = false;
            }
        }
        //审批中心或作业平台打开,被红冲的单据也显示
        if (btnKey == "linkVoucher" || btnKey == "linkBudget") {
            if (scene == 'approve'   || scene == 'approvesce'  || scene == 'zycl' || scene == 'zycx'|| scene == 'bzcx' || scene == 'fycx') {
                btnsVisibleObj[pageButton['linkVoucher']] = true;
                btnsVisibleObj[pageButton['linkBudget']] = true;
            } else {
                btnsVisibleObj[pageButton['linkVoucher']] = false;
                btnsVisibleObj[pageButton['linkBudget']] = false;
            }
        }

        //作废的单据影藏保存提交修改等状态
        if (djzt == "-1") {
            if (btnKey == pageButton.pageCopy || btnKey == pageButton.imageShow || btnKey == pageButton.pagePrint) {
                btnsVisibleObj[btnKey] = true;
            } else {
                btnsVisibleObj[btnKey] = false;
            }
        }
    }

    if (deal && (deal == 'handon' || deal == 'adjust' || deal == 'sscreject' || deal == 'handled')) {
        btnsVisibleObj[pageButton.pageEdit] = false;
    }
    if (pageStatus) {
        props.button.setButtonsVisible({
            ['accrued_detail_Add']: false,
            ['accrued_detail_Delete']: false,
            ['accrued_detail_Copy']: false,
            ['accrued_detail_Insert']: false
        });
    }

    //来源于报表联查的单据
    if(scene=='sscermlink'){
        for (let btnKey1 in pageButton) {
            if( btnKey1 == pageButton.imageShow  ||  btnKey1 == pageButton.pagePrint || btnKey1 == pageButton.fileManager ||btnKey1 == "linkVoucher" || btnKey1 == "linkBudget" )
             {
                btnsVisibleObj[btnKey1] = isBrowseStatus;
             }else{
                btnsVisibleObj[btnKey1] = false;
            }
        }
    }
    btnsVisibleObj['linkMYApprove1']=true;//联查明源
    props.button.setButtonsVisible(btnsVisibleObj);
}

export { dataHandle }
