/**
 * 全局变量设置文件
 */
import 'ssccommon/components/globalPresetVar';

//全局变量尽量配置在presetVar中，防止全局变量污染。也方便统一管理。
window.presetVar = {
    ...window.presetVar,
    pageId: '201102FYYT_CARD',
    pageCode: '201102FYYT_CARD',
    billtype: '2621',
    bodyChangeValuesAll:'',//缓存表体数据
    //表头（主表）变量配置
    head: {
        head1: 'head',
        head2: ''
    },
    //表体（子表）变量配置
    body: {
        body1: 'accrued_detail',
        body2: 'accrued_verify'
    },
    //搜索模板变量配置
    search: {}
};