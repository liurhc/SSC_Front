/**
 * 表格肩部按钮click事件
 */

const tableShoulderBtnAction = window.presetVar.tableShoulderBtnAction;
const {body1} = window.presetVar.body;

function tableButtonClick() {
    return {
        [body1 + tableShoulderBtnAction.Add] : () => {
            this.props.cardTable.addRow(body1, undefined, null, false);
        }
    }
}

export default tableButtonClick;