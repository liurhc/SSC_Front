import requestApi from '../requestApi';
import 'ssccommon/components/globalPresetVar';
import { dataHandle } from '../initTemplate'

export default function () {
    const head1 = window.presetVar.head.head1;
    const body1 = window.presetVar.body.body1;
    const body2 = window.presetVar.body.body2;
    let props = this.props;
    let billId = props.getUrlParam('id');
    requestApi.query({
        data: {
            "openbillid": billId,
            "pagecode": window.presetVar.pageCode
        },
        success: (data) => {
            if (data.data.head[head1].rows[0].values.effectstatus.value == 1) {//已生效才显示核销预提页签
                this.setState({ isEffect: true });
            } else {
                this.setState({ isEffect: false });
            }
            props.form.setAllFormValue({ [head1]: data.data.head[head1] });
            //设置表体数据
            props.cardTable.setTableData(body1, data.data.bodys[body1]);
            if (data.data.bodys[body2]) {
                props.cardTable.setTableData(body2, data.data.bodys[body2]);
            }
            dataHandle(data.data.head[head1], props);
            this.pubMessage.refreshSuccess();

        }
    })
}
