/**
 * 表格扩展按钮click事件
 */
import afterEvent from './afterEvent';
import {headSumHandle} from './commonEvent';
const tableInnerBtnAction = window.presetVar.tableInnerBtnAction;
const body1 = window.presetVar.body.body1;
const statusVar = window.presetVar.status;

function tableExtendButtonClick() {
    return {
        [body1 + tableInnerBtnAction.Edit] : (record, index) => {
            this.props.cardTable.openModel(body1, statusVar.edit, record, index);
        },
        [body1 +  tableInnerBtnAction.Delete ] : {
            afterClick: (record, index) => {
                headSumHandle.call(this,this.props);
            }
        },
        [body1 +  tableInnerBtnAction.Copy]:{
            afterClick: (record, index) => {
                 let changedrows =  this.props.cardTable.getRowsByIndexs(body1, 0);
                 let value = this.props.cardTable.getValByKeyAndIndex(body1, 0,'amount');
                 let prop = this.props;
                 afterEvent.call(this,prop,body1,'amount',value , changedrows);
       
            }
        },
    }
}

export default tableExtendButtonClick;