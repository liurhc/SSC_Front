/**
 * 编辑后事件
 */
import {ajax,base} from 'nc-lightapp-front';
import requestApi from '../requestApi';
import {updataFormData, updatacardTableData} from 'ssccommon/utils/changePageData';

const body1 = window.presetVar.body.body1;
const head1 = window.presetVar.head.head1;
const pagecode = window.presetVar.pageCode;

export default function afterEvent(props, moduleId, key, value, changedrows) {
    switch (moduleId) {
        case head1 :
            let headLinkAttrs = ["pk_org"/*组织*/, "billdate"/*单据日期*/, "pk_currtype"/*币种*/, "bbhl"/*汇率*/, "operator_org", "operator_dept", "operator",
                "org_currinfo"/*组织本币汇率*/, "group_currinfo"/*集团本币汇率*/, "global_currinfo"/*全局本币汇率*/
            ];
            if (headLinkAttrs.indexOf(key) > -1) {
                let headCardData = props.createHeadAfterEventData(pagecode, head1, body1, moduleId, key, value);
                requestApi.changeByBody({
                    data: headCardData,
                    success: (data)=> {
                        // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                        // //设置表头数据
                        // props.form.setAllFormValue({[presetVar.head.head1]: data.data.head[presetVar.head.head1]});
                        // //设置表体数据
                        // props.cardTable.setTableData(presetVar.body.body1, data.data.body[presetVar.body.body1]);
                        // 设置表头数据
                        props.beforeUpdatePage();
                        updataFormData(props, presetVar.head.head1, data.data.head[presetVar.head.head1]);
                        // 设置表体数据
                        updatacardTableData(props, presetVar.body.body1, data.data.body[presetVar.body.body1]);
                        // BTM 性能优化，变更页面数据更新方法
                        //requestApi.handleLoadDataResDada(props.meta.getMeta(), data);
                        props.updatePage(presetVar.head.head1, presetVar.body.body1);
                        props.cardTable.setTableDataWithResetInitValue(presetVar.body.body1, data.data.body[presetVar.body.body1]);
                    }
                })
            }
            break;

        case body1 :
            let bodyLinkAttrs = ["amount"/*金额*/, "bbhl"/*汇率*/, "assume_org", "org_currinfo"/*组织本币汇率*/,
                "group_currinfo"/*集团本币汇率*/, "global_currinfo"/*全局本币汇率*/];
            if (bodyLinkAttrs.indexOf(key) > -1) {
                let bodyCardData = props.createBodyAfterEventData(pagecode, head1, body1, moduleId, key, changedrows)
                requestApi.changeByBody({
                    data: bodyCardData,
                    success: (data)=> {
                        // TOP 性能优化，变更页面数据更新方法 chengwbc MOD
                        // //设置表头数据
                        // props.form.setAllFormValue({[presetVar.head.head1]: data.data.head[presetVar.head.head1]});
                        // //设置表体数据
                        // // 暂时写setTimeout,解决费用预提单表体行复制错误问题,等平台改完代码后,再去掉setTimeout
                        // setTimeout(()=>{
                        //     props.cardTable.setTableData(presetVar.body.body1, data.data.body[presetVar.body.body1]);
                        // },0);
                        props.beforeUpdatePage();
                        // 设置表头数据
                        updataFormData(props, presetVar.head.head1, data.data.head[presetVar.head.head1]);
                        // 设置表体数据
                        // 暂时写setTimeout,解决费用预提单表体行复制错误问题,等平台改完代码后,再去掉setTimeout
                        updatacardTableData(props, presetVar.body.body1, data.data.body[presetVar.body.body1]);
                        props.updatePage(presetVar.head.head1, presetVar.body.body1);
                        // BTM 性能优化，变更页面数据更新方法
                        //requestApi.handleLoadDataResDada(props.meta.getMeta(), data);
                    }
                })
            }
            break;
    }
};