import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base } from 'nc-lightapp-front';
let { Message } = base;
const { NCBreadcrumb } = base;
const NCBreadcrumbItem = NCBreadcrumb.NCBreadcrumbItem;
import {initTemplate, afterEvent/**, searchBtnClick, pageInfoClick,tableModelConfirm**/ } from './events';
import './index.less';



class F1ExtendAttribute extends Component {
	constructor(props) {
		super(props);
		this.props = props;

		//表单meta信息
		this.state = {
		};
		initTemplate.call(this,props);
	}


		//请求列表数据
	showData = (billID) => {
		let stat=this.setExtendPaneStat();
		if(billID instanceof Event){
		  	billID=""
		}
		if(stat==='cancel'){
				this.props.form.cancel('extendAttribute');
		}else if(stat==='add'){
				this.props.form.EmptyAllFormValue("extendAttribute");
            	this.props.form.setFormItemsValue("extendAttribute",{"status":{"value":0,"display":null}});
            	this.props.form.setFormStatus('extendAttribute','edit');
		}else{
			let _this=this;
			ajax({
				url: '/nccloud/erm/ermextendattr/ErmExtendAttrAction.do',
				data: {
					pk_billtypecode: billID,
					appcode:'201101JYLXKZ',
					pagecode:'201101JYLXKZ_263X'
				},
				success: (res) => {
					let { success, data } = res;
					if (success) {
						let meta = data.head.extendAttribute;
						_this.props.form.EmptyAllFormValue("extendAttribute");
						_this.props.form.setAllFormValue({"extendAttribute": meta});
						_this.props.form.setFormItemsValue("extendAttribute",{"status":{"value":1,"display":null}});
						_this.props.form.setFormStatus('extendAttribute',stat);
						_this.state.isLoad=false;
					}else{
						_this.props.form.EmptyAllFormValue("extendAttribute");
					}
				}
			});
		}


		};
	componentDidMount() {
		// var scr =document.getElementsByTagName('SCRIPT');
		// var url = scr[scr.length - 1].src;
        if(document.getElementById('extInfo').onclick!=null){
            document.getElementById('extInfo').onclick(this.showData);
        }
        if(document.getElementById('extInfo').method!=null){
            document.getElementById('extInfo').method(this.props.form.getAllFormValue);
        }
	}
	setExtendPaneStat(){
		let extendStat=document.getElementById('extInfo').getAttribute('extendStat');
		return extendStat;

	}
	getExtendData(extendAttribute){
		let obj = this.props.form.getAllFormValue('extendAttribute');
		return obj;
	}

	render() {
	/**领域适配  扩展属性需要是form 如果是其他的在一起讨论*/
		let { form } = this.props;
        let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId
		const { createForm } = form;
		return (
			<div id="finance-reva-pobdoc-list">
				<div className="title-button-area">
                    {/*"201101JYLXKZ-0003": "借款单扩展属性",*/}
					<div className="title-area">{multiLang && multiLang.get('201101JYLXKZ-0003')}</div>
				</div>
				<div className="table-area">
					{createForm('extendAttribute', {
						onAfterEvent: afterEvent
					})}
				</div>
			</div>
		);
	}
}

F1ExtendAttribute = createPage({
    mutiLangCode: '2011'
})(F1ExtendAttribute);

export default F1ExtendAttribute;
