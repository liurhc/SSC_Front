import requestApi from "ssccommon/components/requestApi";

import {ajax } from 'nc-lightapp-front';

let requestDomain =  '';
let requestApiOverwrite = {
    ...requestApi,
    //单据模板接口
    tpl: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/ermextendattr/ExtendAttrTempletAction.do`,
            data: opt.data,
            success: opt.success
        });
    },
    // query:(opt) => {
    //     ajax({
    //         url: `${requestDomain}/nccloud/erm/billcontrast/BillContrastQueryAction.do`,
    //         data: opt.data,
    //         success: (data) => {
    //             data = data.data;
    //             opt.success(data);
    //         }
    //     })
    // },
    add:(opt) => {
        opt.data = {
            ... opt.data,
            userjson:"{'pagecode':"+window.pagecode+"}"
        }
        ajax({
            url: `${requestDomain}/nccloud/erm/billcontrast/BillContrastAddAction.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    save: (opt) => {
        opt.data.rows.forEach((row) => {
            delete row.values.ts;
        });
        opt.data = {
            head: opt.data,
            userjson:"{'pagecode':"+window.pagecode+"}"
        }
        ajax({
            url: `${requestDomain}/nccloud/erm/billcontrast/BillContrastSaveAction.do`,
            data: opt.data,
            success: (data) => {
                if (data.success) {
                    data = data.data;
                    opt.success(data);
                }
            }
        })
    },
    // del: (opt) => {
    //     opt.data.rows.forEach((row) => {
    //         delete row.values.ts;
    //     });
    //
    //     opt.data = {
    //         head:opt.data,
    //         userjson:"{'pagecode':"+window.pagecode+"}"
    //     }
    //     ajax({
    //         url: `${requestDomain}/nccloud/erm/billcontrast/BillContrastDeleteAction.do`,
    //         data: opt.data,
    //         success: (data) => {
    //             if (data.success) {
    //                 data = data.data;
    //                 opt.success(data);
    //             }
    //         }
    //     })
    // },
    valueChange: (opt) => {
        ajax({
            url: `${requestDomain}/nccloud/erm/billcontrast/BillContrastChangeAction.do`,
            data: opt.data,
            success: (data) => {
                if (data.success) {
                    data = data.data;
                    opt.success(data);
                }
            }
        })
    }
}

export default  requestApiOverwrite;
