import React,{ Component } from 'react';
import { createPage, ajax, base,createPageIcon,promptBox,toast } from 'nc-lightapp-front';

const pagecode = '201909YHDZ_SendMessage';
const formid = 'app_20191212SM';

/**
 * 短信通知
 */
export class SendMessage extends Component{
    constructor(props){
        super(props);
        this.state={

        }
        this.initTemplate(this.props);
    }

    initTemplate(props,callBack){
        props.createUIDom(
            {
                pagecode:pagecode
            },
            (data)=>{
                let meta = data.template;
                props.meta.setMeta(meta,callBack);
            }
        );
    }
    getData(){
        let formValue = this.props.form.getAllFormValue(formid);
        return formValue;
    }

    componentDidMount(){
        this.props.onRef(this); // 子组件的this传给父组件
        this.props.form.setFormStatus(formid,'edit');
    }
    render(){
        let { form } = this.props;
        let { createForm } = form;
        return(
            <div className="nc-bill-card">
                <div className='nc-bill-header-area'>
                        <div className="nc-bill-form-area">
                            {createForm(formid, {
                            })}
                        </div>
                </div>
            </div>
        );
    }
}

SendMessage = createPage({
    billinfo:{
        billtype:'form',
        pagecode:pagecode
    }
})(SendMessage);