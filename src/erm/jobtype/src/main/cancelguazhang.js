import React,{ Component } from 'react';
import { createPage, ajax, base,createPageIcon,promptBox,toast } from 'nc-lightapp-front';

const pagecode = '201909YHDZ_ConfirmCancel';
const tableid1 = 'billCancelmatch';
const tableid2 = 'receiptCancelmatch';

/**
 * 取消匹配弹框
 * @data 2019-09-25
 */
export class Cancelguazhang extends Component{
    constructor(props){
        super(props);
        this.state={

        }
        this.initTemplate(this.props);
    }

    initTemplate(props,callBack){
        props.createUIDom(
            {
                pagecode:pagecode
            },
            (data)=>{
                let meta = data.template;
                props.meta.setMeta(meta,callBack);
            }
        );
    }

    componentDidMount(){
        // 待匹配单据数据显示
        let {table1,table2} = this.props;
        let _this=this;
        // ajax({
        //     url:'/nccloud/erm/jobtype/cancelMatch.do',
        //     data:{matchedBill:table1,matchedFlow:table2},
        //     success:(result)=>{
        //        let billmatch = result.data && result.data.billmatch;
        //        let grid = result.data.grid && result.data.grid.receiptCancelmatch;
               
        //     }
        // });
        _this.props.editTable.setTableData(tableid1,table1);
        _this.props.editTable.setTableData(tableid2,table2);

       
    }

    render(){
        let{ editTable } = this.props;
		let {createEditTable} = editTable;
debugger
        return(
            <div className="nc-single-table">
                <div className="nc-singleTable-header-area">
                    <div className="header-title-search-area">{/* 标题 title */}
						<h3 className="title-search-detail">请确认是否取消暂挂的流水</h3>
					</div>
                </div>

				<div className="nc-singleTable-table-area">{/* 列表区 */}
					{createEditTable(tableid2,{
						useFixedHeader:true,
						showIndex:true,
						showCheck:false
					})}
				</div>

            </div>
        );
    }
}

Cancelguazhang = createPage({
    billinfo:{
        billtype:'grid',
        pagecode:pagecode
    }
})(Cancelguazhang);