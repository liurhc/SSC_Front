import React, { Component } from 'react';
import { createPage, ajax, base,createPageIcon,promptBox,toast } from 'nc-lightapp-front';

const pagecode = '201909YHDZ_automatch';
const formid = 'head'; // 表单区域编码

/**
 * 自动匹配弹框组件
 * @date 2019-09-03
 */
export class AutoMatch extends Component {
    constructor(props) {
		super(props);
		this.state={
			
		}
		this.initTemplate(this.props);
    }

	// 加载模板信息
	initTemplate(props,callBack){
		props.createUIDom(
			{
				pagecode:pagecode
			},
			(data)=>{
				let meta = data.template;
				props.meta.setMeta(meta,callBack);
			}
		);
    }
    
    componentDidMount(){
        this.props.onRef(this); // 子组件的this传给父组件
        this.props.form.setFormStatus(formid,'edit');
        // 金额是否相同默认勾选
        this.props.form.setFormItemsValue(formid,{'isMoneySame':{value:true,display:'是'}});
    }

    getData(){
        let formValue = this.props.form.getAllFormValue(formid);
        return formValue;
    }

    afterEventFn = (props,moduleId,key,value,changedrows,index,attr) =>{
        debugger;
        if(key=="isMemoSame" && value.value) {
            this.props.form.setFormItemsValue(moduleId,{"needContainWords":{"value":null,"display":null}})
        }
        if(key=="needContainWords" && value.value) {
            this.props.form.setFormItemsValue(moduleId,{"isMemoSame":{"value":false,"display":'否'}})
        }
    }

    render(){
        let { form } = this.props;
        let { createForm } = form;
        return(
            <div className="nc-bill-card">
                <div className='nc-bill-header-area'>
                        <div className="nc-bill-form-area">
                            {createForm(formid, {
                                onAfterEvent: this.afterEventFn,
                            })}
                        </div>
                </div>
            </div>
        );
    }
}

AutoMatch = createPage({
	billinfo:{
        billtype:'form',
        pagecode:pagecode
    },
	initTemplate: ()=>{}
})(AutoMatch);