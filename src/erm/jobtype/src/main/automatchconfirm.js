import React,{ Component } from 'react';
import { createPage, ajax, base,createPageIcon,promptBox,toast } from 'nc-lightapp-front';

/**
 * 跟取消匹配弹框公用一个模板
 */
const pagecode = '201909YHDZ_ConfirmCancel';
const tableid1 = 'billCancelmatch';
const tableid2 = 'receiptCancelmatch';

/**
 * 自动匹配确认弹框
 * @data 2019-11-01
 */
export class AutomatchConfirm extends Component{
    constructor(props){
        super(props);
        this.state={

        }
        this.initTemplate(this.props);
    }

    initTemplate(props,callBack){
        props.createUIDom(
            {
                pagecode:pagecode
            },
            (data)=>{
                let meta = data.template;
                props.meta.setMeta(meta,callBack);
            }
        );
    }

    componentDidMount(){
        this.props.editTable.setTableData(tableid1,this.props.data && this.props.data.data.billmatch || {rows:[]});
        this.props.editTable.setTableData(tableid2,this.props.data && this.props.data.data.grid.dpp_flow || {rows:[]});
    }

    render(){
        let{ editTable } = this.props;
		let {createEditTable} = editTable;

        return(
            <div className="nc-single-table">

                <div className="nc-singleTable-header-area">
                    <div className="header-title-search-area">{/* 标题 title */}
						{createPageIcon()}
						<h3 className="title-search-detail">匹配单据</h3>
					</div>
                </div>

				<div className="nc-singleTable-table-area">{/* 列表区 */}
					{createEditTable(tableid1,{
						useFixedHeader:true,
						showIndex:true,
						showCheck:false
					})}
				</div>

                <div className="nc-singleTable-header-area">
                    <div className="header-title-search-area">{/* 标题 title */}
						{createPageIcon()}
						<h3 className="title-search-detail">匹配流水</h3>
					</div>
                </div>

				<div className="nc-singleTable-table-area">{/* 列表区 */}
					{createEditTable(tableid2,{
						useFixedHeader:true,
						showIndex:true,
						showCheck:false
					})}
				</div>

            </div>
        );
    }
}

AutomatchConfirm = createPage({
    billinfo:{
        billtype:'grid',
        pagecode:pagecode
    }
})(AutomatchConfirm);