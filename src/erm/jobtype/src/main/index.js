import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base,createPageIcon,promptBox,toast, } from 'nc-lightapp-front';
import {setTableExtendCol} from 'ssccommon/components/profile'
import { AutoMatch } from './automatch';
import { CancelMatch } from './cancelmatch';
import { SendMessage } from './sendmessage';
import { AutomatchConfirm } from '../main/automatchconfirm';
import { Cancelguazhang } from './cancelguazhang';
import {btnIfShow} from '../main/releasebtnshow';
import './index.less'
const { NCTabs, NCModal, NCRadio, } = base;
const tableid1 = 'dpp_detail'; // 表1的区域编码
const tableid2 = 'dpp_flow'; // 表2的区域编码
const pagecode = '201908YHDZ_LIST'; // 页面编码
const searchid = 'dpp_search'; // 查询区编码
const total1 = 'total1'; // 表1金额合计区域编码
const total2 = 'total2'; // 表1金额合计区域编码
var ifshow=false;
var isd=false;
const urls = { // 请求路径
	quereyUrl : '/nccloud/erm/receiptmatch/queryBillAndBankReceipt.do', // 查询请求路径
	autoMatchUrl : '/nccloud/erm/receiptmatch/matchBankReceipt.do', // 自动匹配
	handMatchUrl : '/nccloud/erm/receiptmatch/handMatchBankReceipt.do', // 手动匹配
	checkMatchUrl : '/nccloud/erm/receiptmatch/checkMatch.do', // 查看匹配
	cancelMatchUrl : '/nccloud/erm/receiptmatch/cancelMatch.do', // 取消匹配
	sendNoticeUrl: '/nccloud/erm/receiptmatch/receipSendMessage.do', // 发布通知
	releaseUrl: '/nccloud/erm/receiptmatch/release.do', // 发布
	cancelclaimUrl: '/nccloud/arap/bankrecipt/cancelclaim.do', // 取消认领
	sendMessageUrl:'/nccloud/arap/bankrecipt/sendMessage.do',//发送短信给本地财务
};
var aaaa=null;
class JobType extends Component {
	constructor(props) {
		super(props);
		this.state={
			showModal_finance_01:false,
			chooseType:true,
		}
		//请求权限接口
		this.props.button.setButtonVisible('burenlingSelected',false); //隐藏
		ajax({
			url:'/nccloud/arap/bankrecipt/queryauthority.do',
			success:(result)=>{
				if(result.data.myHashMap.authority){
					ifshow=true
					ifshow&&this.props.button.setButtonVisible('burenlingSelected',true); //显示
				}
			},
			error:(result)=>{
				toast({color:'waring',tital:result.message});
			}
		});
		this.initTemplate(this.props,()=>{
			// 初始化两个表格合计区域内容
			this.props.form.setFormItemsValue(total1,{
				'totalamount1':{value:'0.00',display:'0.00',scale:2},
				'totalamount2':{value:'0.00',display:'0.00',scale:2},
				'count':{value:'0 条',display:'0 条'}
			});
			this.props.form.setFormItemsValue(total2,{
				'totalamount1':{value:'0.00',display:'0.00',scale:2},
				'totalamount2':{value:'0.00',display:'0.00',scale:2},
				'count':{value:'0 条',display:'0 条'}
			});
			
		});
	}
	
	componentDidMount(){
		btnIfShow(this.props);
	}

	// 加载模板信息
	initTemplate(props,callBack){
		var that=this;
		props.createUIDom(
			{
				pagecode:pagecode
			},
			(data)=>{
				let meta = data.template;
				aaaa=data.template;
				meta = this.modifierMeta(this.props,meta,this);
				props.meta.setMeta(meta,callBack);
				
				data.button && props.button.setButtons(data.button,()=>{
					// 按钮状态初始化
					props.button.setDisabled({
						// Match:true // 手动匹配按钮默认置灰
					});
					meta[tableid2].items=meta[tableid2].items.map((item, key) => {
						   item.width = 150;
						   console.log(key,item)
						   if (item.attrcode == 'des_billno') {
							   item.renderStatus = 'browse';
							   item.render = (text, record, index) => {
								   return (
									   <a
									   class=""	
									   style={{ textDecoration: 'underline', cursor: 'pointer' }}
										   onClick={() => {
											   var des_billno=record && record.values.des_billno ? record.values.des_billno.value:"";
											   if(!des_billno){
												  return 
											   }
											   ajax({
												   url: '/nccloud/erm/receiptmatch/queryid.do',
												   data: {
													   des_billno
												   },
												   success: function (res) {
													    props.openTo('/nccloud/resources/gl/voucherBill/voucherBill_list/main/index.html#/view?', {
														   bill_id:res.data.bill_id,
										                    status: 'browse',
															 scene:"bz",
														 tradetype:"VR01-Cxx-02",
										                    appcode: '20020MANGE000403',
										                   })
										   
												   }
											   });			
										   }}
									   >
										   {record && record.values.des_billno &&record.values.des_billno.value||""}
									   </a>
								   );
							   };
						   } 
						   return item;
					   });
					//表格2添加操作列
					meta[tableid2].items.push({
						    attrcode: 'opr',
						    label: "操作",
						    width: 300,
						    fixed: 'right',
						    className: "table-opr", 
						    visible: true,
						    itemtype:'customer',
						    render: (text, record, index) => {
							var buttonAry=[];
							var  ifGuazhang= record.values&&record.values.accounting_status&&record.values.accounting_status.value;
							var rlstatus= record.values&&record.values.claimstatus&&record.values.claimstatus.value;
							var rlhidden= record.values&&record.values.rlhidden&&record.values.rlhidden.value||false;//认领是否隐藏
							var ifaccountflag=record.values&&record.values.accountflag&&record.values.accountflag.value;//取消挂账按钮
							//后端标识几号后不允许挂载
							//
							buttonAry.push('fabu');
							if(  ifGuazhang=="1"){//未挂账
								if(rlstatus=="0"||!rlstatus){//未认领
									if(!rlhidden){
										buttonAry.push("renling");
										ifshow&&buttonAry.push("burenling","guazhang")
									}else{
										ifshow&&buttonAry.push('burenling',"guazhang")
									}
								}else if(rlstatus=="2"){
									buttonAry.push('chakanrl','cancelrl');
								}else if(rlstatus=="3"){
									buttonAry.push("recoverrenling")
								}
								// buttonAry.push("guazhang");
								
							}else{//已挂账
								if(rlstatus=="0"||!rlstatus){//未认领
									if(!ifaccountflag){//后端标识
										buttonAry.push("cancelGuazhang")
									}
										buttonAry.push("ckguazhang",'renling');
								}else if(rlstatus=="2"){
									 buttonAry.push('chakanrl','cancelrl');
								}else if(rlstatus="3"){
									buttonAry.push("recoverrenling")
								}
							//	buttonAry.push("cancelGuazhang");
							}
						        return props.button.createOprationButton(buttonAry, {
						            area: "list_inner",
						            buttonLimit: 6,
						            onButtonClick: (props, key) =>that.onButtonClick.call(that,props, key, record)
						        });
						    }
						});
				});
			}
		);
	}
	modifierMeta(props,meta,_this) {
		// 参照过滤：账号按组织过滤
		meta[searchid].items.forEach((item)=>{
			if(item.attrcode === 'pk_account'){ // 账号
				item.queryCondition = () => {
					// 如果选多个组织，按第一个组织过滤
					let pkOrgValue = props.search.getSearchValByField(searchid, 'pk_org').value.firstvalue;
                    return {
                        pk_org: pkOrgValue
                    };
                }
			}

			if(item.attrcode === 'pk_org'){ // 财务组织
				item.queryCondition = () => {
					// 过滤当前用户所分配的组织
					
                    return {
                        TreeRefActionExt:'nccloud.web.erm.ref.pub.BankReceiptMatchOrgRef' 
                    };
                }
			}

		});
		return meta;
	}

	/** 
	 *  子组件和父组件之间的交互，ref为子组件this;
	 *  后续在父组件中通过this.child调用子组件的方法，如:获取子组件的数据;
	 */
	onRef = (ref) => {
        this.child = ref;
    }

	// 按钮点击事件
	onButtonClick(props,id,record) {
		var tableOneRows = [];
		let self=this;
		tableOneRows = this.props.editTable.getCheckedRows(tableid1);
		var tableTwoRows = [];
		tableTwoRows =  this.props.editTable.getCheckedRows(tableid2);
		switch (id) {
			case 'Match': // 手动匹配
				if(tableOneRows.length<1 || tableTwoRows.length<1){
					toast({title:'每个表格至少选中一条数据！', color:'warning'});
				}else if(tableOneRows.length>1 && tableTwoRows.length>1) {
					toast({title:'两个表选中数据是多对多，不支持此操作！',color:'danger'});
				}
				else {
					promptBox({
						color: 'warning',
						title: '确认匹配',
						content: '是否确认匹配？',
						beSureBtnName: '确认',
						cancelBtnName: '取消',
						beSureBtnClick: () => { // 确认按钮事件
							ajax({
								url:urls['handMatchUrl'],
								data:{matchedBill:tableOneRows,matchedFlow:tableTwoRows},
								success:(result)=>{
									if(result.success){
										toast({color:'success',title:'匹配成功！'});
										self.clickSearchBtn(self.props);
									}
								}
							});
						},
						cancelBtnClick: () => {}, // 取消按钮事件
						closeByClickBackDrop: false
					});
				} 
				break;

			case 'Cancelmatch': // 取消匹配
				var showCancel = false;
				if(tableOneRows.length>0){
					tableOneRows.map(item=> {
						item = item.data.values;
						if(item.batchno && item.batchno.value  && item.batchno.value != "~") {
							showCancel = true
						}
					})
				}

				if(tableTwoRows.length>0){
					tableTwoRows.map(item=> {
						item = item.data.values;
						if(item.batchno && item.batchno.value  &&  item.batchno.value != "~") {
							showCancel = true
						}
					})
				}


				if(!showCancel) {
					toast({title:'请选择已匹配的单据！', color:'warning'});
					return;
				} 
				ajax({
					url:'/nccloud/erm/receiptmatch/cancelMatch.do',
					data:{matchedBill:tableOneRows,matchedFlow:tableTwoRows},
					success:(result)=>{
					   let billmatch = result.data && result.data.billmatch;
					   let grid = result.data.grid && result.data.grid.receiptCancelmatch;
						

						props.modal.show('cancelmatch', {
							 title: '取消匹配确认',
							 content: <CancelMatch table1={billmatch} table2={grid}/>,
							 leftBtnName: '确认',
							 rightBtnName: '取消',
							 beSureBtnClick: ()=>{
								ajax({
									url:'/nccloud/erm/receiptmatch/cancelConfirmMatch.do',
									data:{matchedBill:billmatch,matchedFlow:grid},
									success:(result)=>{
										if(result.success){
											toast({color:'success',title:'取消匹配成功！'});
											self.clickSearchBtn(self.props);
										}
									}
								});
							},
							 cancelBtnClick: () => { this.props.modal.close('automatch') }, // 取消按钮事件回调
							 userControl: false // 点确定按钮后，是否自动关闭弹出框.true:手动关、false:自动关
						});
					}
				});
				break;
			case 'Automatch': // 自动匹配
				if(tableOneRows.length<1 || tableTwoRows.length<1){
					toast({title:'每个表格至少选中一条数据！', color:'warning'});
					break;
				}
				props.modal.show('automatch', {
					 title: '自动匹配',
					 content: <AutoMatch onRef={this.onRef}/>,
					 leftBtnName: '确认',
					 rightBtnName: '取消',
					 beSureBtnClick: ()=>{
						ajax({
							url:urls['autoMatchUrl'],
							data:{
								matchedBill:tableOneRows, // 待匹配单据
								matchedFlow:tableTwoRows, // 待匹配流水
								condition:this.child.getData() // 自动匹配条件
							},
							success:(result)=>{
								if(result.success){
									// toast({color:'success',title:'匹配成功！'});
									// 弹框显示自动匹配成功的数据
									props.modal.show('automatchconfirm',{
										title:'系统自动匹配结果',
										content:<AutomatchConfirm data={result}/>,
										rightBtnName: '关闭',
										cancelBtnClick: () => {
											this.props.modal.close('automatchconfirm');
											this.clickSearchBtn(self.props);
										}
									});
								}
							}
						});
					 },
					 cancelBtnClick: () => { this.props.modal.close('automatch') }, // 取消按钮事件回调
					 userControl: false // 点确定按钮后，是否自动关闭弹出框.true:手动关、false:自动关
				});
				break;
				case 'guazhangSelected': // 挂账 选中表格多行
				 if(tableTwoRows.length<1){
					toast({title:'至少选中一条数据！', color:'warning'});
					break;
				}
                var pks=[];
                   tableTwoRows.forEach((item,index)=>{
					item&&item.data&&item.data.values&&(pks.push(item.data.values.m_pk_bankreceipt.value))
					 });
					 ajax({
						url:`/nccloud/erm/receiptmatch/gz.do`,
						data:{pks:pks},
						success:(result)=>{
							if(result.success){
								 props.openTo('/nccloud/resources/gl/voucherBill/voucherBill_list/main/index.html#/view?', {
									// pks,
									bill_id:result.data.pk_voucher,
				                    status: 'browse',
									 scene:"bz",
								  tradetype:"VR01-Cxx-02",
				                  appcode: '20020MANGE000403',
				                 })
							}
						},
						error:(result)=>{
							toast({color:'warning',title:result.message||'挂账失败'});
						}
					});	
				break;
				case 'guazhang': // 表格挂账单行
				var pks=[record.values.m_pk_bankreceipt.value];
				ajax({
					url:`/nccloud/erm/receiptmatch/gz.do`,
					data:{pks:pks},
					success:(result)=>{
						if(result.success){
							 props.openTo('/nccloud/resources/gl/voucherBill/voucherBill_list/main/index.html#/view?', {
								bill_id:result.data.pk_voucher,
			                    status: 'browse',
								 scene:"bz",
							  tradetype:"VR01-Cxx-02",
			                  appcode: '20020MANGE000403',
			                 })
						}
					},
					error:(result)=>{
						toast({color:'warning',title:result.message||'挂账失败'});
						console.log(result)
					}
				});	
				break;
				case 'burenling':
						var pks=[record.values.m_pk_bankreceipt.value];
						// tableTwoRows.forEach((item,index)=>{
						//  item&&item.data&&item.data.values&&(pks.push(item.data.values.m_pk_bankreceipt.value))						 
						//   });
				ajax({
									url:'/nccloud/arap/bankrecipt/claimnotallowed.do',
									data:{claimnotallowed:true,pks:pks},
									success:(result)=>{
										if(result.success){
											self.clickSearchBtn(self.props);
										}else{
											toast({color:'waring',title:result.errmsg});
										}
									},
									error:(result)=>{
						toast({color:'waring',title:result.errmsg});
									}
								});
				break;
				case 'recoverrenling':
				var pks=[record.values.m_pk_bankreceipt.value];
				ajax({
									url:'/nccloud/arap/bankrecipt/claimnotallowed.do',
									data:{claimnotallowed:false,pks:pks},
									success:(result)=>{
										if(result.data.myHashMap.success){
											self.clickSearchBtn(self.props);
										}else{
											toast({color:'waring',title:result.data.myHashMap.errmsg});
										}
									},
									error:(result)=>{
						                 toast({color:'waring',title:result.message});
									}
								});
				break;
				case 'cancelGuazhang': // 取消挂账
						var table2={areacode: "receiptCancelmatch",rows:[record]}
						var pk_bankreceipt=record.values.m_pk_bankreceipt&&record.values.m_pk_bankreceipt.value;
						var des_billno=record.values.des_billno&&record.values.des_billno.value
						props.modal.show('cancelmatch', {
							 title: '取消挂账',
							 content: <Cancelguazhang table2={table2}/>,
							 leftBtnName: '确认',
							 rightBtnName: '取消',
							beSureBtnClick: ()=>{
								ajax({
									url:'/nccloud/erm/receiptmatch/qxgz.do',
									data:{
										data:[
										[pk_bankreceipt,des_billno]
									]},
									success:(result)=>{
										if(result.success){
											toast({color:'success',title:'取消成功！'});
											self.clickSearchBtn(self.props);
										}
									},
									error:(result)=>{
										toast({color:'waring',title:result.message||'取消失败！'});
									}
								});
							},
							 cancelBtnClick: () => { this.props.modal.close('automatch') }, // 取消按钮事件回调
							 userControl: false // 点确定按钮后，是否自动关闭弹出框.true:手动关、false:自动关
						});
				break;
				case 'renling': // 认领
				self.setState({
					showModal_finance_01:true,
					chooseType:	record.values.m_debitamount&&(record.values.m_debitamount.value>0)?true:false
				},()=>{
					//self.props.form.setFormItemsValue("form_finance_01", { 'busitype': { value:'F2-Cxx-01', display:"F2-Cxx-01非房款收款单" }} );
					if(record.values.m_debitamount&&(record.values.m_debitamount.value>0)){
						self.props.form.setFormItemsValue("form_finance_01", { 'record': { value: record } });
						self.props.form.setFormItemsValue("form_finance_01", { 'busitype': { value:'F2-Cxx-01', display:"F2-Cxx-01非房款收款单" }} );
						self.props.form.setFormItemsValue("form_finance_01", { 'pks': { value: record.values.m_pk_bankreceipt.value } });
						self.props.form.setFormStatus("form_finance_01", "edit");
					}else{
						self.props.form.setFormItemsValue("form_finance_02", { 'record': { value: record } });
						self.props.form.setFormItemsValue("form_finance_02", { 'busitype': { value:'F3-Cxx-11', display:"自动扣款认领单=F3-Cxx-11" }} );
						self.props.form.setFormItemsValue("form_finance_02", { 'pks': { value: record.values.m_pk_bankreceipt.value } });
						self.props.form.setFormStatus("form_finance_02", "edit");
					}
				})
				break;
				case 'cancel': // 关闭弹框
				self.setState({
					showModal_finance_01:false	
				})
				break;
				   /**
         * 领用领用弹出框-确认
         */
        case 'claim_ok':
		    var arr=['F2-Cxx-10','F2-Cxx-09','F2-Cxx-08'];
			var ifgoto=function(busitype){
				 //调用跳转单据页面
				 if(busitype=="F2-Cxx-01"){//非房款收款单
					props.openTo('/nccloud/resources/arap/gatheringbill/gatheringbill/main/index.html#/card', {                             
						status: "add",
						appcode: "20060GBM000400",
						pagecode: "F2-Cxx-01",
					   scene: 'bz',
					   ifturnTo:"true",
				   })
				}else if(busitype=="F2-Cxx-02"){//跟投款收款单
					props.openTo('/nccloud/resources/arap/gatheringbill/gatheringbill/main/index.html#/card', {                             
						status: "add",
						appcode: "20060GBM000406",
						pagecode: "F2-Cxx-02",
					   scene: 'bz',
					   ifturnTo:"true",
				   })
				}else if(busitype=="F2-Cxx-06"){//其他收款单
					props.openTo('/nccloud/resources/arap/gatheringbill/gatheringbill/main/index.html#/card', {                             
						status: "add",
						appcode: "20060GBM000405",
						pagecode: "F2-Cxx-06",
					   scene: 'bz',
					   ifturnTo:"true",
				   })
				}else if(busitype=="F4-Cxx-99"){//支付退回收款单
					props.openTo('/nccloud/resources/cmp/billmanagement/recbill/main/index.html#/card', {                             
						status: "add",
						appcode: "36070RBM400",
						pagecode: "F4-Cxx-99",
					   scene: 'bz',
					   ifturnTo:"true",
				   })
				}else if(busitype=="F5-Cxx-01"){//税金缴纳付款单
					props.openTo('/nccloud/resources/cmp/billmanagement/paybill/main/index.html#/card', {                             
						status: "add",
						appcode: "36070PBR405",
						pagecode: "F5-Cxx-01",
					   scene: 'bz',
					   ifturnTo:"true",
				   })
				}else if(busitype=="F5-Cxx-02"){//总账特殊付款单
					props.openTo('/nccloud/resources/cmp/billmanagement/paybill/main/index.html#/card', {                             
						status: "add",
						appcode: "36070PBR408",
						pagecode: "F5-Cxx-02",
					   scene: 'bz',
					   ifturnTo:"true",
				   })
				}else if(busitype=="F3-Cxx-11"){//自动扣款认领单
					props.openTo('/nccloud/resources/arap/paybill/paybill/main/index.html#/card', {                             
						status: "add",
						appcode: "20080EBM000406",
						pagecode: "F3-Cxx-11",
					   scene: 'bz',
					   ifturnTo:"true",
				   })
				}else if(arr.includes(busitype)){//金融机构利息收入
					ajax({
						url:'/nccloud/arap/queryobj/queryurl.do',
						data:{pk_billtypecode:busitype,status:"bz"},
						success:(res)=>{
							var resdata=JSON.parse(res.data.url);
							props.openTo(resdata.url, {                             
								status: resdata.data.status,
								appcode: resdata.data.appcode,
								pagecode:resdata.data.pagecode,
							    scene: 'bz',
								ifturnTo2:"true",
							})
						}
					})
				}
			}
			    var form_finance=self.state.chooseType?"form_finance_01":"form_finance_02";
				var busitype = self.props.form.getFormItemsValue(form_finance, 'busitype').value;
				 var pks=[self.props.form.getFormItemsValue(form_finance, 'pks').value]				
				 var record=self.props.form.getFormItemsValue(form_finance, 'record').value;
				 record.values.rlhidden={value:true};
				 this.props.form.setFormItemsValue(form_finance,"record");//点击一次认领框隐藏 
				 localStorage.setItem("record", JSON.stringify((record) ));
				if(arr.includes(busitype)){
					ifgoto(busitype)
				}else{
		 		let newdata = {
					customername:record.values.oppunitname&&record.values.oppunitname.value,
					accnum:record.values.def5&&record.values.def5.value,
					pk_org:record.values.m_pk_org&&record.values.m_pk_org.value,//m_pk_bankreceipt
					pk_bank:record.values.m_pk_bank&&record.values.m_pk_bank.value,
					pk_tradetype :busitype,
					des_billno:record.values.des_billno&&record.values.des_billno.value,
				};
				// tableTwoRows.forEach(item=>{
				// 	var Numbertype=item.data.values.accounting_status.value
				// 	if(Numbertype=="2"){
				// 		ifd=true
				// 	}
				// })
				// ifd?newdata.des_billno = des_billno : ''; //2传已挂账
			ajax({
				url:'/nccloud/arap/queryobj/querycustomer.do',
				data: newdata,
				success:(result)=>{
					if(result.success){
					record.values.supplier={};//对方单位
					record.values.pk_balatype1={};
					record.values.pk_balatype2={};
					record.values.pk_currtype={};
					record.values.otheraccount={};
					record.values.pk_tradetypeid={};
					record.values.pk_busiflow={};
					record.values.trade_type={};
					record.values.num={};
						if(!result.data.message&&result.data&&result.data.pk_customer&&result.data.name){
							record.values.supplier={value:result.data.pk_customer,display:result.data.name};//对方单位
						}
						//默认默认“线下网银”--结算方式get
						if(result.data&&result.data["不支付"]){
							record.values.pk_balatype1={display:"不支付",value:result.data["不支付"]};
							record.values.pk_balatype2={display:"线下网银",value:result.data["线下网银"]};
						}	
						if(result.data&&result.data.pk_currtype){
							record.values.pk_currtype={value:result.data.pk_currtype,display:result.data.currtypename};	
						}
						if(result.data&&result.data.local_rate){
							record.values.rate={value:result.data.local_rate,scale:"8"};	
						}else{
							record.values.rate={value:"1",scale:"8"}
						}
						if(result.data&&result.data.oppaccount){
							record.values.otheraccount=JSON.parse(result.data.oppaccount)
						}
						if(result.data&&result.data.oppaccountname){
							record.values.oppaccountname={value:result.data.oppaccountname}
						}
						if(result.data&&result.data.pk_tradetypeid){
							record.values.pk_tradetypeid=JSON.parse(result.data.pk_tradetypeid);
						}
						if(result.data&&result.data.pk_busiflow){
							record.values.pk_busiflow=JSON.parse(result.data.pk_busiflow);
						}
						if(result.data&&result.data.trade_type){
							record.values.trade_type={value:result.data.trade_type}
						}
						if(result.data&&result.data.num){
							record.values.num={value:result.data.num}
						}
					}
					localStorage.setItem("record", JSON.stringify((record) ));
					ifgoto(busitype)
				},
				error:(result)=>{
					//toast({color:'waring',title:'接口错误！'});
					record.values.local_rate={scale:"8"}
					record.values.supplier={};//对方单位
					record.values.pk_balatype1={};
					record.values.pk_balatype2={};
					record.values.pk_currtype={};
					record.values.otheraccount={};
					record.values.pk_tradetypeid={};
					record.values.pk_busiflow={};
					record.values.trade_type={};
					localStorage.setItem("record", JSON.stringify((record) ));
					ifgoto(busitype)
				}
			})
		};
            self.setState({
                showModal_finance_01: false
            });
			break;
			case 'fabu': // 发布流水
				let data = [{
					pk_bankreceipt: record.values['m_pk_bankreceipt'].value,
					checkdate: record.values['checkdate'].value,
					pk_org: record.values['m_pk_org'].value,
					netbanknumber: record.values['netbanknumber'].value,
					creditamount: record.values['m_creditamount'].value,
					debitamount: record.values['m_debitamount'].value,
				}]

				if(record.values['def8'].value=="1"){
					toast({title:'该流水已发布，无需发布，请重新选择！', color:'warning'});
					return;	
				}

				ajax({
					url:urls['releaseUrl'],
					data:data,
					success:(result)=>{
						if(result.success){
							toast({color:'success',title:'发布完成！'});
							self.clickSearchBtn(self.props);
							data[0].pk_org=record.values['m_pk_org'];
							// this.release(data);
						}
					}
				});
				break;
			case 'fabuSelected': // 发布 选中表格多行
				 if(!tableTwoRows || tableTwoRows.length<1){
						toast({title:'至少选中一条数据！', color:'warning'});
						return;
					}
			        let rlzts=[]
					let paramData = [];
					for(let i = 0,l=tableTwoRows.length;i<l;i++) {
						let row = tableTwoRows[i].data;
						rlzts.push(row.values['def8'].value)
						paramData.push({
							pk_bankreceipt: row.values['m_pk_bankreceipt'].value,
							checkdate: row.values['checkdate'].value,
							pk_org: row.values['m_pk_org'].value,
							netbanknumber: row.values['netbanknumber'].value,
							creditamount: row.values['m_creditamount'].value,
							debitamount: row.values['m_debitamount'].value,
						})
					}
					if(rlzts.includes("1")){
						toast({title:'您勾选了已发布的流水，无需发布，请重新选择！', color:'warning'});
						return;	
					}
					

					ajax({
						url:urls['releaseUrl'],
						data:paramData,
						success:(result)=>{
							if(result.success){
								
								toast({color:'success',title:'发布完成！'});
								self.clickSearchBtn(self.props);
								// let releaseData = [];
								// for(let i = 0,l=tableTwoRows.length;i<l;i++) {
								// 	let row = tableTwoRows[i].data;
								// 	releaseData.push({
								// 		pk_bankreceipt: row.values['m_pk_bankreceipt'].value,
								// 		pk_org: row.values['m_pk_org'],
								// 	})
								// }
								// this.release(releaseData);

							}
						}
					});
				break;
				case 'burenlingSelected':
					// 	var newArr=[]
					// if(tableTwoRows.length==0){
					// 	toast({title:'请至少选中一条数据', color:'warning'});		
					// }else if(tableTwoRows.length>0){
					// 	tableTwoRows.forEach(item=> {
					// 	var	o = item.data.status;
					// 	newArr.push(o);	
					// 	})
					// 	if(newArr.includes("1")||newArr.includes("2")||newArr.includes("3")){
							
					// 	toast({title:'你勾选了不正确的流水，该流水非未认领状态，请重新勾选', color:'warning'});	

					// 	}
					// 	else{
					// 	//批量进行不认领操作调接口
					// 	}
					// }
					if(tableTwoRows.length<=0){
					  toast({title:'请至少选中一条数据', color:'warning'});		
					}else {
						try{
							tableTwoRows.forEach(item=>{
								debugger
								var Numbertype=item.data.values.claimstatus.value
								var Numbertype2=item.data.values.accounting_status.value
								if(Numbertype=="1"||Numbertype=="2"||Numbertype=="3"||Numbertype2=="2"){
									throw new Error
								}
							})
							var pks=[];
							debugger
							 tableTwoRows.forEach((item,index)=>{
						  item&&item.data&&item.data.values&&(pks.push(item.data.values.m_pk_bankreceipt.value))						 
							  });
					ajax({
										url:'/nccloud/arap/bankrecipt/claimnotallowed.do',
										data:{claimnotallowed:true,pks:pks},
										success:(result)=>{
											if(result.success){
												self.clickSearchBtn(self.props);
											}
										},
										error:(result)=>{
							toast({color:'waring',title:result.errmsg});
										}
									});
					break;
						}catch(e){
							toast({title:'你勾选了不正确的流水，该流水非未认领状态，请重新勾选', color:'warning'});
						}
					}
				break;
				case "cancelrl":
                    promptBox({
                                                color: 'warning',
                                                title: '取消认领',
                                                content: '是否取消认领？',
                                                beSureBtnName: '确认',
                                                cancelBtnName: '取消',
                                                beSureBtnClick: () => { // 确认按钮事件
                                                    ajax({
                                                        url:urls['cancelclaimUrl'],
                                                        data:{
                                                             claim_tradetype:record.values.claim_tradetype&&record.values.claim_tradetype.value,
                                                             claim_desid:record.values.claim_desid&&record.values.claim_desid.value,
                                                             pk_bankreceipt:record.values.m_pk_bankreceipt&&record.values.m_pk_bankreceipt.value
                                                             },
                                                        success:(result)=>{
                                                            if(result.success){
                                                                toast({color:'success',title:'取消成功！'});
                                                                self.clickSearchBtn(self.props);
                                                            }
                                                        }
                                                    });
                                                },
                                                cancelBtnClick: () => {}, // 取消按钮事件
                                                closeByClickBackDrop: false
                                            });
                break;
            case "chakanrl":
                var claim_tradetype=record.values.claim_tradetype&&record.values.claim_tradetype.value;
                var arr=['F2-Cxx-10','F2-Cxx-09','F2-Cxx-08'];
                         //调用跳转单据页面
                var id=record.values.claim_desid&&record.values.claim_desid.value;
                                    if(claim_tradetype=="F2-Cxx-01"){//非房款收款单
                                            props.openTo('/nccloud/resources/arap/gatheringbill/gatheringbill/main/index.html#/card', {   
                                               id,                          
                                                status: "browse",
                                                appcode: "20060GBM000400",
                                                pagecode: "F2-Cxx-01",
                                               scene: 'bz',
                                           })
                                        }else if(claim_tradetype=="F2-Cxx-02"){//跟投款收款单
                                            props.openTo('/nccloud/resources/arap/gatheringbill/gatheringbill/main/index.html#/card', {                             
                                                    id, 
                                                status: "browse",
                                               appcode: "20060GBM000406",
                                              pagecode: "F2-Cxx-02",
                                                 scene: 'bz',
                                           })
                                        }else if(claim_tradetype=="F2-Cxx-06"){//其他收款单
                                            props.openTo('/nccloud/resources/arap/gatheringbill/gatheringbill/main/index.html#/card', {                             
                                                id,
                                                status: "browse",
                                                appcode: "20060GBM000405",
                                                pagecode: "F2-Cxx-06",
                                               scene: 'bz',
                                           })
                                        }else if(claim_tradetype=="F4-Cxx-99"){//支付退回收款单
                                            props.openTo('/nccloud/resources/cmp/billmanagement/recbill/main/index.html#/card', {                             
                                                id,
                                                status: "browse",
                                                appcode: "36070RBM400",
                                                pagecode: "F4-Cxx-99",
                                               scene: 'bz',
                                           })
                                        }else if(claim_tradetype=="F5-Cxx-01"){//税金缴纳付款单
                                            props.openTo('/nccloud/resources/cmp/billmanagement/paybill/main/index.html#/card', {                             
                                                id,
                                                status: "browse",
                                                appcode: "36070PBR405",
                                                pagecode: "F5-Cxx-01",
                                               scene: 'bz',
                                           })
                                        }else if(claim_tradetype=="F5-Cxx-02"){//总账特殊付款单
                                            props.openTo('/nccloud/resources/cmp/billmanagement/paybill/main/index.html#/card', {                             
                                                id,
                                                status: "browse",
                                                appcode: "36070PBR408",
                                                pagecode: "F5-Cxx-02",
                                               scene: 'bz',
                                           })
                                        }else if(claim_tradetype=="F3-Cxx-11"){//自动扣款认领单
                                            props.openTo('/nccloud/resources/arap/paybill/paybill/main/index.html#/card', {                             
                                                id,
                                                status: "browse",
                                                appcode: "20080EBM000406",
                                                pagecode: "F3-Cxx-11",
                                               scene: 'bz',
                                           })
                                        }else if(arr.includes(claim_tradetype)){//金融机构利息收入
											ajax({
												url:'/nccloud/arap/queryobj/queryurl.do',
												data:{pk_billtypecode:claim_tradetype,status:"bz"},
												success:(res)=>{
													var resdata=JSON.parse(res.data.url);
													props.openTo(resdata.url, {  
														id,                           
														status: "browse",
														appcode: resdata.data.appcode,
														pagecode:resdata.data.pagecode,
														scene: 'bz',
													})
												}
											})
										}
                break;
case 'ckguazhang':
		var des_billno=record && record.values.des_billno ? record.values.des_billno.value:"";
		if(!des_billno){
		   return 
		}
		ajax({
			url: '/nccloud/erm/receiptmatch/queryid.do',
			data: {
				des_billno
			},
			success: function (res) {
				 props.openTo('/nccloud/resources/gl/voucherBill/voucherBill_list/main/index.html#/view?', {
					bill_id:res.data.bill_id,
	                 status: 'browse',
					  scene:"bz",
				  tradetype:"VR01-Cxx-02",
	                 appcode: '20020MANGE000403',
	                  //  pagecode: '20020MANGE_003',
	                })
	
			}
		});	
break;
case 'tz': // 短信通知
		if(tableTwoRows.length<=0){
			toast({title:'请至少选中一条数据', color:'warning'});		
		}else {
			    let rlzts=[]
				var sendmessageData=[];
				tableTwoRows.forEach((item,index)=>{
					rlzts.push(item.data.values['claimstatus'].value)
					item&&item.data&&item.data.values&&(sendmessageData.push(item.data.values))
				  });
				if(rlzts.includes("1")||rlzts.includes("2")){
						toast({title:'您勾选了已认领/已匹配的流水，无需短信通知，请重新选择！', color:'warning'});
						return;	
				}
				
				props.modal.show('sendmessage', {
					 title: '人员',
					 content: <SendMessage  onRef={this.onRef}/>,
					 leftBtnName: '确认',
					 rightBtnName: '取消',
					 beSureBtnClick: ()=>{
						 let pk_defdoc=this.child.getData().rows['0'].values.ry.value;
						 if(!pk_defdoc){
							toast({title:'短信通知人不能为空！', color:'warning'});
							return;	
						 }
					let orgname='m_pk_org';
						let param = {
							sendmessageData,
							pk_defdoc,
							orgname
						}
						this.sendMessage(param);
						this.props.modal.close('sendmessage')
					 },
					 cancelBtnClick: () => { this.props.modal.close('sendmessage') }, // 取消按钮事件回调
					 userControl: true // 点确定按钮后，是否自动关闭弹出框.true:手动关、false:自动关
				});
			}
				break;
			default:
				break;
		}
	
	}
	// 查询按钮事件
	clickSearchBtn(props,data){
		var self=this;
		let searchValue = props.search.getQueryInfo(searchid); // 查询条件
		if(!searchValue || !searchValue.querycondition){
			return;
		}
		ajax({
			url:urls['quereyUrl'],
			data:searchValue,
			success:(result)=>{
				debugger
	            props.editTable.setTableData(tableid1,result.data && result.data.billmatch || {rows:[]});
				props.editTable.setTableData(tableid2,result.data.grid && result.data.grid.dpp_flow || {rows:[]});
				toast({color:'success',content:'查询成功！'});

				// 合计区域显示查询结果条数
				let count1 = result.data && result.data.billmatch && result.data.billmatch.rows.length + ' 条' || '0 条';
				//this.props.form.setFormItemsValue(total1,{'count':{value:count1, display:count1}});
				let count2 = result.data.grid && result.data.grid.dpp_flow && result.data.grid.dpp_flow.rows.length + ' 条' || '0 条';
				//this.props.form.setFormItemsValue(total2,{'count':{value:count2, display:count2}});

				this.props.form.setFormItemsValue(total1,{
					'totalamount1':{value:'0.00',display:'0.00',scale:2},
					'totalamount2':{value:'0.00',display:'0.00',scale:2},
					'count':{value:count1, display:count1}
				});
				this.props.form.setFormItemsValue(total2,{
					'totalamount1':{value:'0.00',display:'0.00',scale:2},
					'totalamount2':{value:'0.00',display:'0.00',scale:2},
					'count':{value:count2, display:count2}
				});
			}
		});
	}

	// 表格1数据选中事件
	onSelected1(props, moduleId, record, index, status){
		var totalMoney1 = 0.00; //表1收款合计
		var totalMoney2 = 0.00; //表1付款合计
		var records = [];
		records = this.props.editTable.getCheckedRows(tableid1);
		records.forEach((record)=>{
			totalMoney1 += Number(record.data.values.re_money.value || '0');
			totalMoney2 += Number(record.data.values.pay_money.value || '0');
		});
		this.props.form.setFormItemsValue(total1,{
			'totalamount1':{value:totalMoney1.toFixed(2),display:totalMoney1.toFixed(2),scale:2},
			'totalamount2':{value:totalMoney2.toFixed(2),display:totalMoney2.toFixed(2),scale:2}
		});
	}

	// 表格1数据全部选中事件
	onSelectedAll1(props, moduleId){
		this.onSelected1(props, moduleId);
	}

	// 表格2数据选中事件
	// 表格2数据选中事件
	onSelected2(props, moduleId, record, index, status){
		var totalMoney1 = 0; //表2收款合计
		var totalMoney2 = 0; //表2付款合计
		var records = [];
		records = this.props.editTable.getCheckedRows(tableid2);
		records.forEach((record)=>{
			totalMoney1 += Number(record.data.values.m_debitamount.value || '0');
			totalMoney2 += Number(record.data.values.m_creditamount.value || '0');
		});
		this.props.form.setFormItemsValue(total2,{
			'totalamount1':{value:totalMoney1.toFixed(2),display:totalMoney1.toFixed(2),scale:2},
			'totalamount2':{value:totalMoney2.toFixed(2),display:totalMoney2.toFixed(2),scale:2}
		});
	}

	// 表格2数据全部选中事件
	onSelectedAll2(props, moduleId){
		this.onSelected2(props, moduleId);
	}

	afterEvent(props, moduleId, key, value, changedrows, i, s, g) {
		if(key==='busitype'){
			props.form.setFormItemsValue(moduleId, { busitype:  { value:i.key, display:i.label }});
		}
	}

	render() {

		let{editTable,button,search,form,table} = this.props;
		let { createForm } = form;
		let {createEditTable} = editTable;
		let {createSimpleTable} = table;
		let { createButtonApp } = button;
		let {NCCreateSearch} = search;
		const { modal } = this.props;
        let { createModal } = modal;  //模态框

		return (
			<div className="nc-single-table">

				<div className="nc-singleTable-header-area">{/* 头部 header */}	
					<div className="header-title-search-area">{/* 标题 title */}
						{createPageIcon()}
						<h2 className="title-search-detail">银行流水对账</h2>
					</div>
					
					<div className="header-button-area">{/* 按钮区-btn-group*/}
						{createButtonApp({
							area: 'head',
							buttonLimit: 3, 
							onButtonClick: this.onButtonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>

				{/* 查询区 */}
				<div className="nc-bill-search-area">
						{NCCreateSearch(
							searchid,//模块id
							{
								clickSearchBtn: this.clickSearchBtn.bind(this), //点击按钮事件
								showAdvBtn: true //显示高级按钮
							}
						)}
				</div>

				<div className="nc-singleTable-header-area">	
					<div className="header-title-search-area">
						{createPageIcon()}
						<h3 className="title-search-detail">待匹配单据</h3>
					</div>
				</div>

				<div className="nc-singleTable-table-area">{/* 列表区 */}
					{createEditTable(tableid1,{
						useFixedHeader:true,
						showIndex:true,
						showCheck:true,
						isAddRow:true,
						onSelected: this.onSelected1.bind(this),
						onSelectedAll: this.onSelectedAll1.bind(this)
					})}
				</div>

				{/* 表1金额合计区域 */}
				<div className="nc-bill-form-area">
                    {createForm(total1, {})}
                </div>

				<div className="nc-singleTable-header-area">	
					<div className="header-title-search-area">
						{createPageIcon()}
						<h3 className="title-search-detail">待匹配流水</h3>
					</div>
					<div className="header-button-area" id="tabelTwoTitle">{/* 按钮区-btn-group*/}
						{createButtonApp({
							area: 'guazhang',
							buttonLimit: 3, 
							onButtonClick: this.onButtonClick.bind(this),
							popContainer: document.querySelector('#tableTwoTitle')
						})}
					</div>
				</div>

				<div className="nc-singleTable-table-area">{/* 列表区 */}
					{createEditTable(tableid2,{
						useFixedHeader:true,
						showIndex:true,
						showCheck:true,
						isAddRow:true,
						onSelected: this.onSelected2.bind(this),
						onSelectedAll: this.onSelectedAll2.bind(this)
					})}
				</div>

				{/* 表2金额合计区域 */}
				<div className="nc-bill-form-area">
                    {createForm(total2, {})}
                </div>
				{/*短信通知人员模态框*/}
                {createModal('sendmessage')}
				{/*自动匹配模态框*/}
				{createModal('automatch')}
				{/*取消匹配模态框*/}
				{createModal('cancelmatch')}
				{/*自动匹配确认框*/}
				{createModal('automatchconfirm',{ hideLeftBtn:true })}


			{/* 生单选择 */}
			<NCModal show={this.state.showModal_finance_01} onHide={this.close} style={{ height: '268px', width: '520px' }}>
					<NCModal.Header>
						<NCModal.Title>生单选择</NCModal.Title>
					</NCModal.Header>

					<NCModal.Body className="showModal-publish">
						<div >
							{createForm(this.state.chooseType?"form_finance_01":"form_finance_02", {
								onAfterEvent: this.afterEvent.bind(this)
							})}
						</div>
					</NCModal.Body>

					<NCModal.Footer>
						{createButtonApp({
							area: 'finance_01',
							buttonLimit: 3,
							onButtonClick: this.onButtonClick.bind(this)
						})}
					</NCModal.Footer>
			</NCModal>
			</div>
		);
	}
	//发布
	release(data){
		ajax({
			url:urls['releaseUrl'],
			data:data,
			success:(result)=>{
				if(result.success){
					if(result.data.success){
					  toast({color:'success',title:'发布成功！'});
					}else{
					  toast({color:'waring',title:result.data.msg});
					}
				}
			}
		});
	 }
	 sendMessage(data){
	   ajax({
		   url:urls['sendMessageUrl'],
		   data:data,
		   success:(result)=>{
			   if(result.success){
				   if(result.data.success){
					 toast({color:'success',title:'发送短信成功！'});
				   }else{
					 toast({color:'waring',title:result.data.errmsg});
				   }
			   }
		   }
	   });
	}
}
JobType = createPage({
	billinfo:{
        billtype:'grid',
        pagecode:pagecode
    },
	initTemplate: ()=>{}
})(JobType);

ReactDOM.render(<JobType />, document.querySelector('#app'));
