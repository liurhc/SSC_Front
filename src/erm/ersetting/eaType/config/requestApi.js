import requestApi from "ssccommon/components/requestApi";
import {ajax} from 'nc-lightapp-front';

let requestApiOverwrite = {
    ...requestApi,
    //单据模板接口

    save: (opt) => {
        ajax({
            url: `/nccloud/erm/ersetting/ReimtypeSaveAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            },
            error: (error) => {
                opt.error(error);
            }
        })
    },
    query: (opt) => {
        ajax({
            url: `/nccloud/erm/ersetting/ReimtypeViewAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
}

export default requestApiOverwrite;
