import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createPage, base, high, toast } from 'nc-lightapp-front';
import { buttonClick, initTemplate, afterEvent, refreshButtonClick } from './events';
const { NCCheckbox } = base;
import requestApi from './requestApi';
import pubMessage from 'ssccommon/utils/pubMessage'

import {
    ProfileStyle,
    ProfileHead,
    ProfileBody,
    HeadCenterCustom,
    ButtonGroup
} from 'ssccommon/components/profile';
import { EditTable } from 'ssccommon/components/table';

import './index.less';

class EaType extends Component {

    constructor(props) {
        super();
        this.state = {
            checkedFlag: false,
            disabled: false,
            status: 'browse'
        }
        this.onChange = this.onChange.bind(this);
        this.pubMessage = new pubMessage(props);
        initTemplate.call(this, props);
    }
    componentWillMount() {
                window.onbeforeunload = () => {
                      ;
                    let status = this.props.editTable.getStatus(window.presetVar.body.body1);          
                    if (status == 'edit') {
                        return ''; 
                         }
             }
            }
    componentDidMount() {

    }

    onChange(e) {
        this.setState({ checkedFlag: e });
        requestApi.query({
            data: { showDef: e },
            success: (data) => {
                if (data.success) {
                    if (data.data) {
                        this.props.editTable.setTableData(window.presetVar.body.body1, data.data[window.presetVar.body.body1]);
                    } else {
                        this.props.editTable.setTableData(window.presetVar.body.body1, { rows: [] });
                    }
                    this.props.editTable.setStatus(window.presetVar.body.body1, 'browse');
                }
            }
        })
    }

    onSelectedFn(){
        let rows1 = this.editTable.getCheckedRows(window.presetVar.body.body1);
        if(rows1.length){
            this.button.setButtonDisabled('BatchDelete', false);
        }else{
            this.button.setButtonDisabled('BatchDelete', true);
        }
    }

    render() {
        let btnModalConfig = {};
        let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId
        if (this.props.editTable.getStatus('ReimType') == 'browse') {
            btnModalConfig = {
                ['BatchDelete']: {
                    beforeClick: () => {
                        let rows = this.props.editTable.getCheckedRows('ReimType');
                        if (rows.length) {
                            return true;
                        } else {
                            // "201101BXLX-0003": "请先选中数据",
                            toast({
                                duration: 2,          // 消失时间，默认是3秒; 值为 infinity 时不消失,非必输
                                color: 'warning',     // 提示类别，默认是 "success",非必输
                                content: multiLang && multiLang.get('201101BXLX-0003')   // 提示内容,非必输
                            })
                            return false;
                        }
                    },
                    // "2011-0004": "删除"
                    title: multiLang && multiLang.get('2011-0004'),
                    content: multiLang && multiLang.get('2011-0018'),
                }
            }
        } else {
            btnModalConfig = {
                ['BatchDelete']: {
                    beforeClick: () => {
                        let rows = this.props.editTable.getCheckedRows('ReimType');
                        if (rows.length) {
                            return 'exec';
                        } else {
                            // "201101BXLX-0003": "请先选中数据",
                            toast({
                                duration: 2,          // 消失时间，默认是3秒; 值为 infinity 时不消失,非必输
                                color: 'warning',     // 提示类别，默认是 "success",非必输
                                content: multiLang && multiLang.get('201101BXLX-0003')   // 提示内容,非必输
                            })
                            return false;
                        }
                    }
                }
            }
        }
        btnModalConfig['Cancel'] = {
            title: multiLang && multiLang.get('2011-0002'),
            content: multiLang && multiLang.get('2011-0006'),
        }
        return (
            /*档案风格布局*/
            <ProfileStyle
                layout="singleTable"
                {...this.props}
            >
                {/*页面头*/}
                < ProfileHead
                    // "201101BXLX-0001": "报销类型设置"
                    title={multiLang && multiLang.get('201101BXLX-0001')}
                    refreshButtonEvent={refreshButtonClick.bind(this)}
                    status = { this.state.status }
                >
                    <HeadCenterCustom>
                        <div className="title-search-detail">
                            <span className='showOff'> <NCCheckbox
                                checked={this.state.checkedFlag}
                                onChange={this.onChange}
                                disabled={this.state.disabled}
                            >{multiLang && multiLang.get('201101BXLX-0002')}</NCCheckbox></span>
                        </div>
                        {/*{multiLang && multiLang.get('201101BXLX-0002')}*/}
                        {/*"201101BXLX-0002": "显示停用"*/}
                    </HeadCenterCustom>

                    <ButtonGroup
                        area="page_header"
                        buttonLimit="3"
                        buttonEvent={
                            buttonClick.bind(this)
                        }
                        modalConfig={btnModalConfig}
                    />
                </ProfileHead >


                {/*页面体*/}
                < ProfileBody >
                    <EditTable
                        areaId="ReimType"
                        onAfterEvent={afterEvent.bind(this)}
                        onSelected={this.onSelectedFn}
                        onSelectedAll={this.onSelectedFn}
                        isAddRow='true'
                    >
                    </EditTable>
                </ProfileBody >
            </ProfileStyle >
        )
    }
}

EaType = createPage({
    mutiLangCode: '2011'
})(EaType);

export default EaType;
