import {base, high, ajax,toast,getMultiLang} from 'nc-lightapp-front';
import 'ssccommon/components/globalPresetVar';
import {setTableExtendCol} from 'ssccommon/components/profile'
import requestApi from '../requestApi';


//变量设置规范，如需要
window.presetVar = {
    ...window.presetVar,
    pageId: '',
    head: {
        head1: '',
        head2: ''
    },
    body: {
        body1: 'ReimType',
        body2: ''
    },
    search: {}
};


export default function (props) {

    props.createUIDom(
        {
            pagecode: '201101BXLX_LIST',
            appid: '0001Z310000000006H1A'
        },
        (data)=> {
            getMultiLang({moduleId: 2011, domainName: 'erm',currentLocale: 'simpchn', callback: (json) => {
            let meta = data.template;
            let button = data.button;

            //设置表格的扩展按钮列
            setTableExtendCol(props, meta, [{
                areaId: window.presetVar.body.body1,
                btnAreaId: 'body_shoulder',
                btnKeys: ['Delete'],
                onButtonClick: (btnKey, record, index)=> {
                    setTimeout(() => {
                        props.editTable.deleteTableRowsByRowId(window.presetVar.body.body1, record.rowid);

                        props.button.setButtonDisabled('BatchDelete', true);

                        if (props.editTable.getStatus(window.presetVar.body.body1) == 'browse') {
                            requestApi.save({
                                data: {'data': props.editTable.getAllData(window.presetVar.body.body1)},
                                success: (resData) => {
                                    if (resData.data) {
                                        props.editTable.setTableData(window.presetVar.body.body1, resData.data[window.presetVar.body.body1]);
                                    } else {
                                        props.editTable.setTableData(window.presetVar.body.body1, {rows: []});
                                    }
                                },
                                error: (data) => {
                                    toast({color: 'danger', content: data.message});
                                    props.editTable.cancelEdit(window.presetVar.body.body1);
                                }
                            })
                        }
                    }, 0)
                }
            }]);

            //设置区域模板
            props.meta.setMeta(meta);
            //设置按钮模板
            props.button.setButtons(button);

            //设置按钮行为为弹窗
            props.button.setPopContent('Delete', json['2011-0005']);//'确认要删除该信息吗？'

            /* 设置操作列上删除按钮的弹窗提示 */

            //设置按钮的初始可见性
            props.button.setButtonsVisible({
                'Edit': true,
                'Save': false,
                'Add': true,
                'Cancel': false,
                'BatchDelete': true
            });

            props.button.setButtonDisabled('BatchDelete', true);
            props.button.setMainButton('Add', true);

            //设置初始数据
            //获取表格的数据
            requestApi.query({
                data: { showDef: this.state.checkedFlag},
                success: (data) => {
                    if (data.success) {
                        if (data.data) {
                            props.editTable.setTableData(window.presetVar.body.body1, data.data[window.presetVar.body.body1]);
                        } else {
                            props.editTable.setTableData(window.presetVar.body.body1, {rows: []});
                        }
                        props.editTable.setStatus(window.presetVar.body.body1, 'browse');
                    }
                }
            })
        }})
        }
    )
}


