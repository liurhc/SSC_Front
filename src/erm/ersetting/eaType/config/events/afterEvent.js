import {ajax,base} from 'nc-lightapp-front';
import requestApi from '../requestApi';


export default function afterEvent(props,moduleId,key,value, changedrows) {
    switch(moduleId) {
        case window.presetVar.body.body1 :
            switch (key) {
                case 'inuse' :
                    requestApi.save({
                        data: {'data' : props.editTable.getAllData(moduleId)},
                        success: (data) => {
                            if (data.success) {
                                this.props.editTable.setTableData(window.presetVar.body.body1, data.data[window.presetVar.body.body1]);
                            }
                        }
                    })
                    break;
            }
            break;
    }
};