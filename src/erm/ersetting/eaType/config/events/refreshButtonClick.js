import requestApi from '../requestApi';
import 'ssccommon/components/globalPresetVar';

export default function () {
    requestApi.query({
        data: { showDef: this.state.checkedFlag},
        success: (data) => {
            if (data.success) {
                if (data.data) {
                    this.props.editTable.setTableData(window.presetVar.body.body1, data.data[window.presetVar.body.body1]);
                } else {
                    this.props.editTable.setTableData(window.presetVar.body.body1, {rows: []});
                }
                this.props.editTable.setStatus(window.presetVar.body.body1, 'browse');
                this.pubMessage.refreshSuccess();
            }
        }
    })
}
