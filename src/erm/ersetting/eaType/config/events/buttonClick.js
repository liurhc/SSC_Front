import {ajax,toast,getMultiLang} from 'nc-lightapp-front';
import requestApi from '../requestApi';

function buttonClick() {
    let props = this.props;
    return {
        ['BatchDelete']: () => {
            let rows = props.editTable.getCheckedRows(window.presetVar.body.body1);
            if (rows.length) {
                let arr = rows.map(item => item.index);
                props.editTable.deleteTableRowsByIndex(window.presetVar.body.body1, arr);
           
                props.button.setButtonDisabled('BatchDelete', true);

                if ('browse' == props.editTable.getStatus(window.presetVar.body.body1)) {
                    requestApi.save({
                        data: {'data': props.editTable.getAllData(window.presetVar.body.body1)},
                        success: (resData) => {
                            if (resData.data) {
                                props.editTable.setTableData(window.presetVar.body.body1, resData.data[window.presetVar.body.body1]);
                            } else {
                                props.editTable.setTableData(window.presetVar.body.body1, {rows: []});
                            }
                        },
                        error: (data) => {
                            toast({color: 'danger', content: data.message});
                            props.editTable.cancelEdit(window.presetVar.body.body1);
                        }
                    })
                }
            }
        },
        ['Add']: () => {
                props.editTable.addRow(window.presetVar.body.body1);
                props.button.setButtonsVisible({
                    'Edit': false,
                    'Save': true,
                    'Add': true,
                    'Cancel': true,
                    'BatchDelete': true
                });
                props.button.setMainButton('Add', false);
                props.button.setPopContent('Delete', null);
                this.setState({disabled: true, status: 'edit'});
        },
        ['Cancel']: () => {
            getMultiLang({moduleId: 2011, domainName: 'erm',currentLocale: 'zh-CN', callback: (json) => {
                props.editTable.cancelEdit(window.presetVar.body.body1);
                props.button.setButtonsVisible({
                    'Edit': true,
                    'Save': false,
                    'Add': true,
                    'Cancel': false,
                    'BatchDelete': true
                });
                props.button.setPopContent('Delete', json['2011-0005']);//'确认要删除该信息吗？'
                props.button.setMainButton('Add', true);
                this.setState({disabled: false, status: 'browse'});
            }})
        },
        ['Save']: () => {
            props.editTable.filterEmptyRows(window.presetVar.body.body1, []);
            let flag = props.editTable.checkRequired(window.presetVar.body.body1, props.editTable.getAllRows(window.presetVar.body.body1));//必输项校验
            if(flag){
                requestApi.save({
                    data: {'data': props.editTable.getAllData(window.presetVar.body.body1)},
                    success: (resData) => {
                        getMultiLang({moduleId: 2011, domainName: 'erm',currentLocale: 'zh-CN', callback: (json) => {
                        if (resData.data) {
                            props.editTable.setTableData(window.presetVar.body.body1, resData.data[window.presetVar.body.body1]);
                        } else {
                            props.editTable.setTableData(window.presetVar.body.body1, {rows: []});
                        }
                        props.button.setButtonsVisible({
                            'Edit': true,
                            'Save': false,
                            'Add': true,
                            'Cancel': false,
                            'BatchDelete': true
                        });
                        props.editTable.cancelEdit(window.presetVar.body.body1);
                        props.editTable.setStatus(window.presetVar.body.body1, 'browse');
                        props.button.setPopContent('Delete', json['2011-0005']);//'确认要删除该信息吗？'
                        props.button.setMainButton('Add', true);
                        this.setState({disabled: false, status: 'browse'});
                        this.pubMessage.saveSuccess();
                    }})
                    },
                    error: (data) => {
                        toast({color: 'danger', content: data.message});
                    }
                })
            }
        },
        ['Edit']: () => {
                //设置按钮的可见状态
                props.button.setButtonsVisible({
                    'Edit': false,
                    'Save': true,
                    'Add': true,
                    'Cancel': true,
                    'BatchDelete': true
                });
                props.editTable.setStatus(window.presetVar.body.body1, 'edit');
                props.button.setPopContent('Delete', null);
                props.button.setMainButton('Add', false);
                this.setState({disabled: true, status: 'edit'});
        }
    }
}


export default buttonClick
