import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {connect, Provider} from 'react-redux';
import store from './store/store';


import EaType from './EaType';

ReactDOM.render((<Provider store={store}><EaType/></Provider>)
    , document.querySelector('#app'));