import { combineReducers } from 'redux';
import globalReducer from 'ssccommon/components/globalReducer';

const actionTypes = {
    changeAreaInfo : 'changeAreaInfo'
}

const initialState = {
    areaInfo: {
        mtapp_detail: {
            rowNumber: 0,
            total: 0
        }
    }
};

function pageReducer (state = initialState, action) {
    switch(action.type) {
        case 'changeAreaInfo' :
            return {
                ...state,
                areaInfo: action.areaInfo
            }
            break;
        default :
            return state;
    }
}

const reducer = combineReducers({
    globalReducer,
    pageReducer
})

export default reducer;