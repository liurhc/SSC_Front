import {ajax} from 'nc-lightapp-front';

let requestDomain =  '';

let requestApiOverwrite = {
    //单据模板接口
    tpl: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/ersetting/IndAuthorizeTempletAction.do`,
            data: opt.data,
            success: opt.success
        });
    },
    queryRole: (opt) => {
        ajax({
            url: `${requestDomain}/nccloud/erm/expenseaccount/RoleListAction.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    query:(opt) => {
        ajax({
            url: `${requestDomain}/nccloud/erm/expenseaccount/SqdlrViewAction.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    save: (opt) => {
        /*opt.data.rows.forEach((row) => {
            delete row.values.ts;
        });
        opt.data = {
            head: opt.data
        }*/
        ajax({
            url: `${requestDomain}/nccloud/erm/expenseaccount/SqdlrSaveAction.do`,
            data: opt,
            success: (data) => {
                if (data.success) {
                    data = data.data;
                    opt.success(data);
                }
            }
        })
    },
    valueChange: (opt) => {
        ajax({
            url: `${requestDomain}/nccloud/erm/expenseaccount/SqdlValueChangeAction.do`,
            data: opt.data,
            success: (data) => {
                if (data.success) {
                    data = data.data;
                    opt.success(data);
                }
            }
        })
    }
}

export default  requestApiOverwrite;