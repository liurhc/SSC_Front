import {getMultiLang} from 'nc-lightapp-front';
import {setTableExtendCol} from 'ssccommon/components/profile'
import {tableExtendButtonClick} from '../events';

window.presetVar = {
    ...window.presetVar,
    //状态常量。 用于url中的hash参数 或 框架api的状态
    status: {
        browse: 'browse', //浏览态
        edit: 'edit', //编辑态
        add: 'add', //新增态
        del: 'del'
    },
    body:{
        bodyCode : 'Sqdlr',//可代理人员
        body1Code : 'SqdlDept',//可代理部门
        body2Code : 'SqdlSpec'//特殊代理
    }
}
export default function (props) {
    const appcode = props.getSearchParam('c');
    const pagecode = props.getSearchParam('p');
    let that = this;
    props.createUIDom(
        {
            pagecode: pagecode,
            appcode: appcode
        },
        (data) => {
            getMultiLang({moduleId: 2011, domainName: 'erm',currentLocale: 'simpchn', callback: (json) => {
                data.template[window.body1Code]&&data.template[window.body1Code].items.forEach((item) => {
                    if(item.attrcode === "pk_user"){
                        item.isMultiSelectedEnabled = true;
                        item.onlyLeafCanSelect = true;
                        item.refcode = 'uapbd/refer/org/DeptTreeRef/index';
                        item.isShowUnit = true;
                        item.unitProps = {
                            refType: 'tree',
                            refName: json['201101DLSQ-0011'] ,//'业务单元',
                            refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                            rootNode:{refname:json['201101DLSQ-0011'] /* '业务单元' */,refpk:'root'},
                            placeholder:json['201101DLSQ-0011'] ,//"业务单元",
                            queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                            treeConfig:{name:[json['201101DLSQ-0012']/* '编码' */, json['201101DLSQ-0013']/* '名称' */],code: ['refcode', 'refname']},
                            isMultiSelectedEnabled: false,
                            //unitProps:unitConf,
                            isShowUnit:false
                        };
                    }
                })
                data.template[window.bodyCode]&&data.template[window.bodyCode].items.forEach((item) => {
                    if(item.attrcode === "pk_user"){
                        item.refcode = 'uapbd/refer/psninfo/PsndocTreeGridRef/index';
                        item.isShowUnit = true;
                        item.unitProps = {
                            refType: 'tree',
                            refName: json['201101DLSQ-0011'],//'业务单元',
                            refCode: 'uapbd.refer.org.BusinessUnitTreeRef',
                            rootNode:{refname: json['201101DLSQ-0011']/* '业务单元' */,refpk:'root'},
                            placeholder: json['201101DLSQ-0011'],//"业务单元",
                            queryTreeUrl: '/nccloud/uapbd/org/BusinessUnitTreeRef.do',
                            treeConfig:{name:[json['201101DLSQ-0012']/* '编码' */, json['201101DLSQ-0013']/* '名称' */],code: ['refcode', 'refname']},
                            isMultiSelectedEnabled: false,
                            //unitProps:unitConf,
                            isShowUnit:false
                        };
                        item.unitCondition = ()=>{
                            return {
                                "isMutiGroup":"N"
                            };
                        };
                        item.isMultiSelectedEnabled = true;
                        item.queryCondition = () => {
                            return {
                                GridRefActionExt:"nccloud.web.action.erm.ref.sqlbuilder.PsndocClassTreeGridRef",
                                pk_group: window.groupPk
                            };
                        }
                    }
                })
    
                let meta = {
                    SqdlSpec: {
                        moduletype: 'form',
                        areaname: json['201101DLSQ-0002'],//'特殊代理',
                        items: [
                            {
                                attrcode: 'issamedept',
                                label: json['201101DLSQ-0007'],//'代理本部门',
                                itemtype: 'checkbox_switch',
                                visible: true,
                                initialvalue: {//控件默认值
                                    value: false,
                                    display: ''
                                }
                            },
                            {
                                attrcode: 'isall',
                                label: json['201101DLSQ-0008'],//'代理所有人',
                                itemtype: 'checkbox_switch',
                                visible: true,
                                initialvalue: {//控件默认值
                                    value: false,
                                    display: ''
                                }
                            }
                        ]
                    },
                    Sqdlr: {},
                    SqdlDept: {},
                    copyModal:{
                        moduletype: 'table',
                        items: [
                            {
                                attrcode: 'role_code',
                                label: json['201101DLSQ-0009'],//'角色编码',
                                itemtype: 'input',
                                initialvalue: {
                                    value: '',
                                    display: ''
                                },
                                visible: true
                            },
                            {
                                attrcode: 'role_name',
                                label: json['201101DLSQ-0010'],//'角色名称',
                                itemtype: 'input',
                                visible: true
                            },
                            {
                                attrcode: 'pk_roler',
                                label: '',
                                itemtype: 'input',
                                visible: false
                            }
                        ]
                    }
                }
                meta[window.bodyCode] = data.template[window.bodyCode];
                meta[window.body1Code] = data.template[window.body1Code];
                //设置表格的扩展按钮列
                setTableExtendCol(props, meta, [{
                    areaId: 'Sqdlr',
                    btnAreaId: 'Sqdlr_inner',
                    buttonVisible: (record, inedx) =>{
                        return ['DelLine']
                    },
                    onButtonClick: tableExtendButtonClick.bind(that),
                }]);
                setTableExtendCol(props, meta, [{
                    areaId: 'SqdlDept',
                    btnAreaId: 'SqdlDept_inner',
                    buttonVisible: (record, inedx) =>{
                        return ['DelLine1']
                    },
                    onButtonClick: tableExtendButtonClick.bind(that),
                }]);
    
                props.meta.setMeta(meta);
                props.button.setButtons(data.button);
                //table扩展按钮弹窗确认框设置
                props.button.setPopContent('DelLine',json['2011-0005']/* '确认要删除该信息吗？' */) /* 设置操作列上删除按钮的弹窗提示 */
                props.button.setPopContent('DelLine1',json['2011-0005']/* '确认要删除该信息吗？' */) /* 设置操作列上删除按钮的弹窗提示 */
                props.button.setButtonsVisible({
                    "Edit" : true,
                    "Save" : false,
                    "Cancel" : false,
                    "Copy" : true,
                    "AddLine1" : false,
                    "AddLine" : false,
                });
            }})
        }
    )
}
