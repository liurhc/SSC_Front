/**
 * 表格扩展按钮click事件
 */
import {buttonClick} from '../events';

function tableExtendButtonClick() {
    let props = this.props;
    return {
        ['DelLine'] : (record, index) => {
            props.editTable.deleteTableRowsByIndex(window.bodyCode, index);
            //浏览态真删
            if(props.editTable.getStatus(window.bodyCode) === 'browse'){
                buttonClick.call(this)['Save']();
            }
        },
        ['DelLine1'] : (record, index) => {
            props.editTable.deleteTableRowsByIndex(window.body1Code, index);
            //浏览态真删
            if(props.editTable.getStatus(window.body1Code) === 'browse'){
                buttonClick.call(this)['Save']();
            }
        },
    }
}

export default tableExtendButtonClick;