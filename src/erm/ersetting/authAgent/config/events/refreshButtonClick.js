import requestApi from '../requestApi';
import {toast } from 'nc-lightapp-front';


export default function refreshButtonClick() {
    if(window.rolePk){
        let props = this.props;
        requestApi.query({
            data : {
                pkRole : window.rolePk
            },
            success: (data) => {
                this.pubMessage.refreshSuccess();

                this.setState({
                    showRight: true
                });
                let busiRows = [];
                let deptRows = [];
                if (data){
                    let returnData = null;
                    if(data[window.bodyCode]){
                        returnData = data[window.bodyCode];
                    }else if(data[window.body1Code]){
                        returnData = data[window.body1Code];
                    }
                    returnData&&returnData.rows.forEach((row) => {
                        if (row.values.keyword.value.indexOf('busiuser') > -1){
                            busiRows.push(row);
                        } else if (row.values.keyword.value.indexOf('pk_deptdoc') > -1){
                            deptRows.push(row);
                        }
                        if (row.values.keyword.value.indexOf('issamedept') > -1){
                            props.form.setFormItemsValue(window.body2Code, {
                                'issamedept' : {
                                    value: row.values.pk_user.value == 'true'?true:false
                                },
                            }, false);
                        } else if (row.values.keyword.value.indexOf('isall') > -1){
                            props.form.setFormItemsValue(window.body2Code, {
                                'isall' : {
                                    value: row.values.pk_user.value == 'true'?true:false
                                },
                            }, false);
                        } else {
                            props.form.setFormItemsValue(window.body2Code, {
                                issamedept : {
                                    value: false
                                },
                                isall : {
                                    value: false
                                }
                            }, false);
                        }
                    })
                } else {
                    props.form.setFormItemsValue(window.body2Code, {
                        issamedept : {
                            value: false
                        },
                        isall : {
                            value: false
                        }
                    }, false);
                }
                props.editTable.setTableData(window.bodyCode, {
                    "rows" : busiRows
                });
                props.editTable.setTableData(window.body1Code, {
                    "rows" : deptRows
                });
            }
        });
    } else {
        let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId
        toast({ content: multiLang && multiLang.get('201101DLSQ-0006'), color: 'warning' });//'请先选择角色'
    }
}
