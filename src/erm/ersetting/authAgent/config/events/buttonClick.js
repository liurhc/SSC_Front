import requestApi from '../requestApi'
import {toast } from 'nc-lightapp-front';
export default function buttonClick() {
    let props = this.props;
    let multiLang = props.MutiInit.getIntl(2011); //this.moduleId
    return{
        ['Edit']: () => {
            if(props.syncTree.getSelectNode('tree1')){
                props.button.setButtonsVisible({
                    "Edit": false,
                    "Save" : true,
                    "Cancel" : true,
                    "Copy" : false,
                    "AddLine1" : true,
                    "AddLine" : true,
                });
                props.editTable.setStatus(window.bodyCode, window.presetVar.status.edit);
                props.editTable.setStatus(window.body1Code, window.presetVar.status.edit);
                props.form.setFormStatus(window.body2Code, window.presetVar.status.edit);
                this.setState({disableOrg : true, disabledSearch : true});
                props.syncTree.setNodeDisable('tree1',true);
                props.button.setPopContent('DelLine','') /* 设置操作列上删除按钮的弹窗提示 */
                props.button.setPopContent('DelLine1','') /* 设置操作列上删除按钮的弹窗提示 */
                this.setState({ status : 'edit'});
            }
            else {
                toast({ content: multiLang && multiLang.get('201101DLSQ-0006'), color: 'warning' });//'请先选择角色'
            }
        },
        ['Save']: () => {
            props.editTable.filterEmptyRows(window.bodyCode, []);
            props.editTable.filterEmptyRows(window.body1Code, []);
            let repeatFlag = props.editTable.checkRequired(window.bodyCode, props.editTable.getAllRows(window.bodyCode)) && props.editTable.checkRequired(window.body1Code, props.editTable.getAllRows(window.body1Code));//必输项校验
            if(repeatFlag){
                let dlrData = props.editTable.getAllData(window.bodyCode);
                let dlDeptData = props.editTable.getAllData(window.body1Code);
                let dlSpecData = props.form.getAllFormValue(window.body2Code);
                if(props.syncTree.getSelectNode('tree1')){
                    let reqData = assemData(props, dlrData, dlDeptData, dlSpecData);
                    requestApi.save({
                        data: reqData,
                        success: (data) => {
                            let pkRole = props.syncTree.getSelectNode('tree1').refpk;
                            props.editTable.setStatus(window.bodyCode, window.presetVar.status.browse);
                            props.editTable.setStatus(window.body1Code, window.presetVar.status.browse);
                            props.form.setFormStatus(window.body2Code, window.presetVar.status.browse);
                            let busiRows = [];
                            let deptRows = [];
                            if (data){
                                let returnData = null;
                                if(data[window.bodyCode]){
                                    returnData = data[window.bodyCode];
                                }else if(data[window.body1Code]){
                                    returnData = data[window.body1Code];
                                }
                                returnData&&returnData.rows.forEach((row) => {
                                    if (row.values.keyword.value.indexOf('busiuser') > -1){
                                        busiRows.push(row);
                                    } else if (row.values.keyword.value.indexOf('pk_deptdoc') > -1){
                                        deptRows.push(row);
                                    }
                                    if (row.values.keyword.value.indexOf('issamedept') > -1){
                                        props.form.setFormItemsValue(window.body2Code, {
                                            'issamedept' : {
                                                value: row.values.pk_user.value == 'true'?true:false
                                            },
                                        }, false);
                                    } else if (row.values.keyword.value.indexOf('isall') > -1){
                                        props.form.setFormItemsValue(window.body2Code, {
                                            'isall' : {
                                                value: row.values.pk_user.value == 'true'?true:false
                                            },
                                        }, false);
                                    } else {
                                        props.form.setFormItemsValue(window.body2Code, {
                                            issamedept : {
                                                value: false
                                            },
                                            isall : {
                                                value: false
                                            }
                                        }, false);
                                    }
                                })
                            } else {
                                props.form.setFormItemsValue(window.body2Code, {
                                    issamedept : {
                                        value: false
                                    },
                                    isall : {
                                        value: false
                                    }
                                }, false);
                            }
                            props.editTable.setTableData(window.bodyCode, {
                                "rows" : busiRows
                            });
                            props.editTable.setTableData(window.body1Code, {
                                "rows" : deptRows
                            });
                            props.button.setButtonsVisible({
                                "Edit": true,
                                "Save" : false,
                                "Cancel" : false,
                                "Copy" : true,
                                "AddLine1": false,
                                "AddLine" : false,
                            });
                            this.setState({disableOrg : false, disabledSearch : false});
                            props.syncTree.setNodeDisable('tree1',false);
                            props.button.setPopContent('DelLine', multiLang && multiLang.get('2011-0005')/* '确认要删除该信息吗？' */) /* 设置操作列上删除按钮的弹窗提示 */
                            props.button.setPopContent('DelLine1', multiLang && multiLang.get('2011-0005')) /* 设置操作列上删除按钮的弹窗提示 */
                            this.setState({ status : 'browse'});
                            this.pubMessage.saveSuccess();
                        }
                    });
                }
                else{
                    toast({ content: multiLang && multiLang.get('201101DLSQ-0006'), color: 'warning' });//'请先选择角色'
                    cancelBtnEventConfig.click(props);
                }
            }
        },
        ['Cancel']: () => {
            props.button.setButtonsVisible({
                "Edit": true,
                "Save" : false,
                "Cancel" : false,
                "Copy" : true,
                "AddLine1": false,
                "AddLine" : false,
            });
            props.editTable.cancelEdit('SqdlDept', () => {
                props.editTable.cancelEdit('Sqdlr');
            });
            // window.setTimeout(() => {
            //     props.editTable.cancelEdit('Sqdlr');
            // }, 20);
            props.form.cancel('SqdlSpec');
            this.setState({disableOrg : false, disabledSearch : false});
            props.syncTree.setNodeDisable('tree1',false);
            props.button.setPopContent('DelLine', multiLang && multiLang.get('2011-0005')) /* 设置操作列上删除按钮的弹窗提示 */
            props.button.setPopContent('DelLine1', multiLang && multiLang.get('2011-0005')) /* 设置操作列上删除按钮的弹窗提示 */
            this.setState({ status : 'browse'});
        },
        ['Copy']: () => {
            if(props.syncTree.getSelectNode('tree1')){
                // this.setState({ showModal: true });
                props.modal.show('copy');
                requestApi.queryRole({
                    data : {
                        "pkOrg": window.orgPk
                    },
                    success: (data) => {
                        data && props.table.setAllTableData('copyModal', {
                            areacode: 'copyModal',
                            rows: data.RoleList.rows
                        });
                    }
                })
            }
            else {
                toast({ content: multiLang && multiLang.get('201101DLSQ-0006'), color: 'warning' });//'请先选择角色'
            }
        },
        ['AddLine']: () => {
            props.editTable.addRow(window.bodyCode);

        },
        ['AddLine1']: () => {
            props.editTable.addRow(window.body1Code);
        },
    }
}

// 将两个表格及一个表单中的信息汇总
function assemData(props, dlrData, dlDeptData, dlSpecData){
    let pkRole = props.syncTree.getSelectNode('tree1').refpk;
    let resultRows = [];
    let dlrMap = new Map();
    let deptMap = new Map();
    //代理的人
    dlrData.rows.forEach((row) => {
        if(row.status != '3'){
            row.values.pk_roler.value = pkRole;
            row.values.keyword.value = 'busiuser';
            if(!dlrMap.get(row.values.pk_user.value)){
                dlrMap.set(row.values.pk_user.value, row);
            }
        }
    });
    dlrMap.size && dlrMap.forEach((dlrRow) => {
        resultRows.push(dlrRow);
    })
    //代理的部门
    dlDeptData.rows.forEach((row) => {
        if(row.status != '3'){
            row.values.pk_roler.value = pkRole;
            row.values.keyword.value = 'pk_deptdoc';
            if(!deptMap.get(row.values.pk_user.value)){
                deptMap.set(row.values.pk_user.value, row);
            }
        }
    });
    deptMap.size && deptMap.forEach((deptRow) => {
        resultRows.push(deptRow);
    })
    //特殊代理
    dlSpecData.rows.forEach((row) => {
        if(row.values.isall){
            let isAllRow = new Object();
            isAllRow.values = {
                'pk_roler' : {
                    'value' : pkRole
                },
                'keyword' : {
                    'value' : 'isall'
                },
                'pk_user' : {
                    'value' : row.values.isall.value || false
                }
            };
            resultRows.push(isAllRow);
        }
        if(row.values.issamedept){
            let deptRow = new Object();
            deptRow.values = {
                'pk_roler' : {
                    'value' : pkRole
                },
                'keyword' : {
                    'value' : 'issamedept'
                },
                'pk_user' : {
                    'value' :  row.values.issamedept.value || false
                }
            }
            resultRows.push(deptRow);
        }
    })

    let result = new Object();
    result.areaType = "table";
    result.rows = resultRows;
    return result;
}

