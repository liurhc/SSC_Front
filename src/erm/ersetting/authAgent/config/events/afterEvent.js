import requestApi from '../requestApi'
export default function (props, id, key, value, data, index) {
    if (id === window.body1Code) {
        if(value.length){
            let count = 0;
            value.forEach((val) => {
                requestApi.valueChange({
                    data : {
                        key : "pk_dept",
                        value : val.refpk
                    },
                    success : (data => {
                        if(count === 0){
                            props.editTable.setValByKeyAndIndex(id, index, "pk_user", {
                                display: val.refname,
                                value : val.refpk
                            });
                            props.editTable.setValByKeyAndIndex(id, index, "pk_org", {
                                display: data.name,
                                value : data.pk
                            });
                        }else{
                            props.editTable.pasteRow(id, {
                                status : 2,
                                values : {
                                    pk_user : {
                                        display: val.refname,
                                        value : val.refpk
                                    },
                                    pk_org : {
                                        display: data.name,
                                        value : data.pk
                                    },
                                    pk_roler : {
                                        display: '',
                                        value : ''
                                    },
                                    keyword : {
                                        display: '',
                                        value : ''
                                    }
                                }
                            }, index);
                        }
                        count++;
                    })
                })
            })
        }
    } else if (id === window.bodyCode){
        if(value.length){
            let count = 0;
            value.forEach((val) => {
                requestApi.valueChange({
                    data : {
                        key : "pk_user",
                        value : val.refpk
                    },
                    success : (data => {
                        if(count === 0){
                            props.editTable.setValByKeyAndIndex(id, index, "pk_user", {
                                display: val.refname,
                                value : val.refpk
                            });
                            props.editTable.setValByKeyAndIndex(id, index, "pk_org", {
                                display: data.name,
                                value : data.pk
                            });
                        }else{
                            props.editTable.pasteRow(id, {
                                status : 2,
                                values : {
                                    pk_user : {
                                        display: val.refname,
                                        value : val.refpk
                                    },
                                    pk_org : {
                                        display: data.name,
                                        value : data.pk
                                    },
                                    pk_roler : {
                                        display: '',
                                        value : ''
                                    },
                                    keyword : {
                                        display: '',
                                        value : ''
                                    }
                                }
                            }, index);
                        }
                        count++;
                    })
                })
            })
        }
       /* requestApi.valueChange({
            data : {
                key : "pk_user",
                value : value.refpk
            },
            success : (data => {
                props.editTable.setValByKeyAndIndex(id, index, "pk_org", {
                    display: data.name,
                    value : data.pk
                });
            })
        })*/
    }
}