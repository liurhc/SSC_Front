let tplMock = {
    
    button: [
        {
            "id": "0001A41000000006J5B1",
            "type": "button_main",
            "key": "Edit",
            "title": "修改",
            "area": "body-shoulder",
            "children": []
        },   
        {
            "id": "0001A4100000000622J5B2",
            "type": "button_main",
            "key": "Add",
            "title": "新增",
            "area": "body-shoulder",
            "children": []
        },
        {
            "id": "0001A41000000006J5B3",
            "type": "button_main",
            "key": "Delete",
            "title": "删除",
            "area": "body-shoulder",
            "children": []
        },
        {
            "id": "0001A41000000006J5B166",
            "type": "button_main",
            "key": "Save",
            "title": "保存",
            "area": "body-shoulder",
            "children": []
        },
        {
            "id": "0001A41000000006J5B2",
            "type": "button_secondary",
            "key": "Cancel",
            "title": "取消",
            "area": "body-shoulder",
            "children": []
        },
        {
            "id": "0001A41000000006J5B1",
            "type": "button_main",
            "key": "Edit1",
            "title": "修改",
            "area": "body-shoulder1",
            "children": []
        },   
        {
            "id": "0001A4100000000622J5B2",
            "type": "button_main",
            "key": "Add1",
            "title": "新增",
            "area": "body-shoulder1",
            "children": []
        },
        {
            "id": "0001A41000000006J5B3",
            "type": "button_main",
            "key": "Delete1",
            "title": "删除",
            "area": "body-shoulder1",
            "children": []
        },
        {
            "id": "0001A41000000006J5B166",
            "type": "button_main",
            "key": "Save1",
            "title": "保存",
            "area": "body-shoulder1",
            "children": []
        },
        {
            "id": "0001A41000000006J5B2",
            "type": "button_secondary",
            "key": "Cancel1",
            "title": "取消",
            "area": "body-shoulder1",
            "children": []
        }
    ]
}

export default tplMock;