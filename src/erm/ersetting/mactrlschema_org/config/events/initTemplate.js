import {base, high, ajax,getMultiLang} from 'nc-lightapp-front';
import 'ssccommon/components/globalPresetVar';
import {setTableExtendCol} from 'ssccommon/components/profile'
import requestApi from '../requestApi';
import buttonClick from './buttonClick';
// import tplMock from  './tplMock';
// const body = window.presetVar.body;
// const areaBtnAction = window.presetVar.areaBtnAction;

export default function (props) {

    props.createUIDom(
        {
            pagecode: window.pagecode,//页面编码
            appcode: window.appcode//小应用编码
        },
        function (data) {
            getMultiLang({moduleId: 2011, domainName: 'erm',currentLocale: 'zh-CN', callback: (json) => {
            let meta = {};
            let buttonTpl ={
                button:(data.button==null||data.button==undefined)?[]:data.button
            }
            meta[window.areaCode.field] = data.template[window.areaCode.field];
            meta[window.areaCode.bill] = data.template[window.areaCode.bill];

            meta[window.areaCode.field].items.find((item) => {
                if(item.attrcode === "fieldname"){
                    item.refcode = 'uap/refer/riart/mdTreeByIdRef/index.js';
                    item.isTreelazyLoad =true;
                    item.queryCondition  = () => {
                        return {beanId:'e3167d31-9694-4ea1-873f-2ffafd8fbed8',TreeRefActionExt:''}
                    }; 
                }
            });
            let JkbxTranstypeRefSqlFilterPath ='nccloud.web.action.erm.ref.sqlbuilder.JkbxTranstypeRefSqlBuilder';//交易类型参照过滤
            meta[window.areaCode.bill].items.find((item) => {
                if(item.attrcode === "pk_src_tradetype"){
                    item.isMultiSelectedEnabled = true;
                    item.refName=json['201101SQKZ-0005']//'交易类型';
                    item.queryCondition  = () => {
                        return {GridRefActionExt:JkbxTranstypeRefSqlFilterPath}
                    };
                }
            })
            //设置区域模板
            props.meta.setMeta(meta);
            //设置按钮模板
            props.button.setButtons(buttonTpl.button, ()=> {
                //设置按钮的初始可见性
                props.button.setButtonsVisible({
                    'Edit': false, 
                    'Save' : false, 
                    'Add': false,
                    'Cancel': false,
                    'Delete': false,
                    'Edit1': false, 
                    'Save1' : false, 
                    'Add1': false,
                    'Cancel1': false,
                    'Delete1': false,
                });
            });
            props.button.setButtonDisabled('Delete', true);
            props.button.setButtonDisabled('Delete1', true);
             //设置表格的扩展按钮列
            setTableExtendCol(props, meta, [{
                areaId: 'mtapp_cfield',
                btnAreaId: 'mtapp_cfield_inner',
                buttonVisible: (record, inedx) =>{
                    return ['DeleteLine']
                },
                onButtonClick: (btnKey, record, index)=> {
                    switch (btnKey) {
                        case 'DeleteLine' :
                            props.editTable.deleteTableRowsByIndex(window.areaCode.field, index);
                            if("browse"==props.editTable.getStatus(window.areaCode.field)){
                                let data = props.editTable.getAllData(window.areaCode.field);
                                let userjson = "{'pagecode':"+window.pagecode+"}";
                                let bdata = {
                                    head:data,
                                    userjson:userjson
                                }
                                requestApi.saveField({
                                    data : bdata,
                                    success: (resData) => {
                                        let ndata = {
                                            areacode : window.areaCode.field,
                                            rows : []
                                        }
                                        if(resData.data!=undefined){
                                            props.editTable.setTableData(window.areaCode.field, resData.data[window.areaCode.field]);
                                        }else{
                                            props.editTable.setTableData(window.areaCode.field, ndata);
                                            props.button.setButtonsVisible({"Add": true,"Edit": false,"Delete":false,"Cancel":false,"Save":false});
                                        }
                                    }
                                })
                            }else{
                                let rowsNew = props.editTable.getCheckedRows(window.areaCode.field);
                                if (!rowsNew.length) {
                                    props.button.setButtonDisabled(['Delete'],true);
                                }
                            }
                        break;
                    }
                }
            }]);
            //设置表格的扩展按钮列
            setTableExtendCol(props, meta, [{
                areaId: 'mtapp_cbill',
                btnAreaId: 'mtapp_cbill_inner',
                buttonVisible: (record, inedx) =>{
                    return ['DeleteLine1']
                },
                onButtonClick: (btnKey, record, index)=> {
                    switch (btnKey) {
                        case 'DeleteLine1' :
                            props.editTable.deleteTableRowsByIndex(window.areaCode.bill, index);
                            if("browse"==props.editTable.getStatus(window.areaCode.bill)){
                                let data = props.editTable.getAllData(window.areaCode.bill);
                                let userjson = "{'pagecode':"+window.pagecode+"}";
                                let bdata = {
                                    head:data,
                                    userjson:userjson
                                }
                                requestApi.saveBill({
                                    data : bdata,
                                    success: (resData) => {
                                        let ndata = {
                                            areacode : window.areaCode.bill,
                                            rows : []
                                        }
                                        if(resData.data!=undefined){
                                            props.editTable.setTableData(window.areaCode.bill, resData.data[window.areaCode.bill]);
                                        }else{
                                            props.editTable.setTableData(window.areaCode.bill, ndata);
                                            props.button.setButtonsVisible({"Add1": true,"Edit1": false,"Delete1":false,"Cancel1":false,"Save1":false});
                                        }
                                    }
                                }) 
                            break;
                        }else{
                            let rowsNew = props.editTable.getCheckedRows(window.areaCode.bill);
                            if (!rowsNew.length) {
                                props.button.setButtonDisabled(['Delete1'],true);
                            }
                        }
                    }
                }
            }]);
            
            //获取树的数据
            requestApi.getTreeData({
                data: {},
                success(data) {
                    let {syncTree} = props;
                    let {setSyncTreeData} = syncTree;
                    let newTree = syncTree.createTreeData(data.data); //创建树 组件需要的数据结构
                    setSyncTreeData('tree', newTree);
                    props.syncTree.setNodeDisable('tree', true);
                }
            })
        }})
        }
    )       
}


