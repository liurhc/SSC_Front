/**
 * 全局变量设置文件
 */
import 'ssccommon/components/globalPresetVar';

//全局变量尽量配置在presetVar中，防止全局变量污染。也方便统一管理。
window.presetVar = {
    ...window.presetVar,
   
    //表体（子表）变量配置
    body: {
        body1:"mtapp_cfield",
        body2:"mtapp_cbill"
    }
    
};