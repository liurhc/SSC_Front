import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {createPage} from 'nc-lightapp-front';
import {buttonClick, initTemplate, afterEvent, refreshButtonClick} from './events';
import requestApi from './requestApi'
import './index.less';
import {
    ProfileStyle,
    ProfileHead,
    ProfileBody,
    ButtonGroup
} from 'ssccommon/components/profile';
import {EditTable} from 'ssccommon/components/table';
import pubMessage from 'ssccommon/utils/pubMessage'


window.bodyCode = 'IndAuthorize';

class IndAuthConfig extends Component {
    constructor(props) {
        super();
        this.pubMessage = new pubMessage(props);
        initTemplate.call(this, props);
        this.state = {status : 'browse'}
    }
    componentWillMount() {
                window.onbeforeunload = () => {
                      ;
                    let status = this.props.editTable.getStatus(window.bodyCode);          
                    if (status == 'edit') {
                        return ''; 
                         }
             }
            }
    componentDidMount() {

        requestApi.query({
            data: {
               
            },
            success: (data) => {
                data && this.props.editTable.setTableData(window.bodyCode, data[window.bodyCode]);
            }
        });
        this.props.button.setButtonsVisible({
            saveButton: false,
            cancelButton: false,
            addButton : false,
            delButton : false
        })
    }

    onSelectedFn(){
        let rows1 = this.editTable.getCheckedRows(window.bodyCode);
        if(rows1.length){
            this.button.setButtonDisabled('Delete', false);
        }else{
            this.button.setButtonDisabled('Delete', true);
        }
    }

    render() {
        let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId
        let btnModalConfig = {
            ['Delete'] : {
                // "2011-0004": "删除"
                // "2011-0018": "确认删除该单据吗？"
                title: multiLang && multiLang.get('2011-0004'),
                content: multiLang && multiLang.get('2011-0018'),
            }
        }
        if (this.state.status == 'edit') {
            btnModalConfig = {};
        }
        return (
            <ProfileStyle
                layout="singleTable"
                {...this.props}
            >
                {/*页面头*/}
                <ProfileHead
                    // "201101GRSQ-0001": "个人授权设置"
                    title={multiLang && multiLang.get('201101GRSQ-0001')}
                    refreshButtonEvent={refreshButtonClick.bind(this)}
                    status = { this.state.status }
                >
                    <ButtonGroup
                        area="page_header"
                        buttonLimit="3"
                        buttonEvent={buttonClick.bind(this)}
                        modalConfig={btnModalConfig}
                    />
                </ProfileHead>

                {/*页面体*/}
                <ProfileBody>
                    <EditTable
                        areaId="IndAuthorize"
                        onAfterEvent={afterEvent.bind(this)}
                        onSelected={this.onSelectedFn}
                        onSelectedAll={this.onSelectedFn}
                    >
                    </EditTable>
                </ProfileBody>
            </ProfileStyle>
        )
    }
}

IndAuthConfig = createPage({
    mutiLangCode: '2011'
})(IndAuthConfig);

ReactDOM.render(<IndAuthConfig/>, document.querySelector('#app'));