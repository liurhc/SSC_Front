import {getMultiLang} from 'nc-lightapp-front';

window.presetVar = {
    ...window.presetVar,
    //状态常量。 用于url中的hash参数 或 框架api的状态
    status: {
        browse: 'browse', //浏览态
        edit: 'edit', //编辑态
        add: 'add', //新增态
        del: 'del'
    }
}
export default function (props) {
    const appcode = props.getSearchParam('c');
    const pagecode = props.getSearchParam('p');
    props.createUIDom(
        {
            pagecode: pagecode,
            appcode: appcode
        },
        function (data) {
            getMultiLang({moduleId: 2011, domainName: 'erm',currentLocale: 'simpchn', callback: (json) => {
                let meta = data.template;
                let tempArr = [];
                meta[window.bodyCode].items.forEach((i) => {
                    if (i.attrcode == 'pk_billtypeid') {
                        i.isMultiSelectedEnabled = true;
                        i.refName = json['201103QCDJ-0003'],//'交易类型';
                        i.queryCondition = () => {
                            return {
                                parentbilltype : '261X,263X,264X'
                            }
                        }
                    }
                    
                    if (i.attrcode == 'pk_operator') {
                        i.queryCondition = () => {
                            let UserNotGroupManagerGridRef ='nccloud.web.action.erm.ref.sqlbuilder.UserNotGroupManagerGridRef';//用户
                            return{GridRefActionExt:UserNotGroupManagerGridRef}
                        }
                        i.refcode = 'uap/refer/riart/userDefaultRefer/index';
                        i.isShowUnit = true;
                        i.unitProps = {
                            refType : 'tree',
                            refName : json['201101DLSQ-0011'],//'业务单元',
                            refCode : 'uapbd.refer.org.BusinessUnitAndGroupTreeRef',
                            rootNode :{refname: json['201101GRSQ-0004']/* '业务单元+集团' */,refpk:'root'},
                            placeholder : json['201101GRSQ-0004'],
                            queryTreeUrl : '/nccloud/uapbd/ref/BusinessUnitAndGroupTreeRef.do',
                            // queryTreeUrl: '/nccloud/uapbd/ref/businessunit.do',
                            treeConfig:{name:[json['201101DLSQ-0012']/* '编码' */, json['201101DLSQ-0013']/* '名称' */],code: ['refcode', 'refname']},
                            isMultiSelectedEnabled: false,
                            //unitProps:unitConf,
                            isShowUnit:false
                        };
                    }
                    tempArr.push(i);
                });
                meta[window.bodyCode].items =tempArr;
                props.meta.setMeta(meta);
                props.button.setButtons(data.button);
                props.button.setButtonsVisible({
                    "Edit" : true,
                    "Save" : false,
                    "Cancel" : false,
                    "Add" : true,
                    "Delete" : true
                });
                props.button.setButtonDisabled('Delete', true);
            }})
        }
    )
}
