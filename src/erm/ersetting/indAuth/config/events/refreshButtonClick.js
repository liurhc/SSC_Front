import requestApi from '../requestApi'

export default function refreshButtonClick() {
    requestApi.query({
        data: {
        },
        success: (data) => {
            data && this.props.editTable.setTableData(window.bodyCode, data[window.bodyCode]);
        }
    });
    this.props.button.setButtonsVisible({
        saveButton: false,
        cancelButton: false,
        addButton : false,
        delButton : false
    });
    this.pubMessage.refreshSuccess();

}
