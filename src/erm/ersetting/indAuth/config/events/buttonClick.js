import requestApi from '../requestApi'
import {toast } from 'nc-lightapp-front';
export default function buttonClick() {
    let props = this.props;
    let multiLang = props.MutiInit.getIntl(2011); //this.moduleId
    return{
        ['Edit']: () => {
            props.editTable.setStatus(window.bodyCode, window.presetVar.status.edit);
            props.button.setMainButton('Add', false);
            props.button.setButtonsVisible({
                Edit: false,
                Save : true,
                Cancel : true,
                Add : true,
                Delete : true
            });
            this.setState({ status : 'edit'});
        },
        ['Save']: () => {
            props.editTable.filterEmptyRows(window.bodyCode,[]);
            let repeatFlag = props.editTable.checkRequired(window.bodyCode, props.editTable.getAllRows(window.bodyCode));//必输项校验
            if(repeatFlag){
                let reqData = props.editTable.getAllData(window.bodyCode);
                if(reqData.rows.length === 0){
                    buttonClick.call(this)['Cancel']();//无数据时
                }
                //校验重复
                let checkStatus = check(reqData, props);
                if(checkStatus){
                    //校验通过才保存
                    requestApi.save({
                        data: reqData,
                        success: (data) => {
                            props.editTable.setStatus(window.bodyCode, window.presetVar.status.browse);
                            data && props.editTable.setTableData(window.bodyCode, data[window.bodyCode]);
                            props.button.setMainButton('Add', true);
                            props.button.setButtonsVisible({
                                Edit: true,
                                Save : false,
                                Cancel : false,
                                Add : true,
                                Delete : true
                            });
                            this.setState({ status : 'browse'});
                            this.pubMessage.saveSuccess();
                        }
                    });
                } 
            }
        },
        ['Add']: () => {
            props.editTable.addRow(window.bodyCode);
            props.button.setMainButton('Add', false);
            props.button.setButtonsVisible({
                Edit: false,
                Save : true,
                Cancel : true,
                Add : true,
                Delete : true
            });
            this.setState({ status : 'edit'});
        },
        ['Delete']: () => {
            let gridStatus = props.editTable.getStatus(window.bodyCode);
            let rows = props.editTable.getCheckedRows(window.bodyCode);
            if(rows.length){
                let arr = rows.map(item => item.index);
                props.editTable.deleteTableRowsByIndex(window.bodyCode, arr);
                props.button.setButtonDisabled('Delete', true);
                if(gridStatus != 'edit'){
                    props.editTable.filterEmptyRows(window.bodyCode);
                    let reqData = props.editTable.getAllData(window.bodyCode);
                    requestApi.save({
                        data: reqData,
                        success: (data) => {
                            props.editTable.setStatus(window.bodyCode, window.presetVar.status.browse);
                            if(data){
                                props.editTable.setTableData(window.bodyCode, data[window.bodyCode]);
                            }else{
                                props.editTable.setTableData(window.bodyCode, {
                                    areacode: 'IndAuthorize',
                                    rows : []
                                });
                            }
                        }
                    });
                }
            }
        },
        ['Cancel']: () => {
            props.button.setMainButton('Add', true);
            props.button.setButtonsVisible({
                Edit: true,
                Save : false,
                Cancel : false,
                Add : true,
                Delete : true
            });
            props.editTable.filterEmptyRows(window.bodyCode);
            props.editTable.cancelEdit(window.bodyCode);
            this.setState({ status : 'browse'});
        }
    }
}

function check(reqData, props) {
    let multiLang = props.MutiInit.getIntl(2011); //this.moduleId
    let addOrUpdateMap = new Map();
    let flag = 1;
    reqData.rows && reqData.rows.forEach((rowData) => {
        if(addOrUpdateMap.get(rowData.values.pk_operator.value)){
            addOrUpdateMap.get(rowData.values.pk_operator.value).push(rowData.values.pk_billtypeid.value);
        }else{
            let arr = [];
            if(rowData.values.startdate.value > rowData.values.enddate.value){
                flag = 0;
                toast({ content: multiLang && multiLang.get('201101GRSQ-0002'), color: 'danger' });//开始日期不能晚于结束日期
            }
            arr.push(rowData.values.pk_billtypeid.value);
            addOrUpdateMap.set(rowData.values.pk_operator.value, arr);
        }
    })

    if(addOrUpdateMap.size === 0){
        flag = 0;
    }
    addOrUpdateMap.size && addOrUpdateMap.forEach((comData) => {
        var arrStr = JSON.stringify(comData);
        for(let i in comData){
            if( arrStr.indexOf(comData[i]) != arrStr.lastIndexOf(comData[i])){
                toast({ content: multiLang && multiLang.get('201101GRSQ-0003'), color: 'danger' });//操作员不能授权相同的单据类型，请重新输入
                flag = 0;
                break;
            }
        }
    })
    return flag;
}

