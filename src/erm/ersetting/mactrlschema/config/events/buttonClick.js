import {ajax,toast,promptBox,getMultiLang} from 'nc-lightapp-front';
import requestApi from '../requestApi';

function buttonClickEvent(){
    return {
        //field编辑
        Edit: () =>{
            //设置按钮的可见状态
            this.props.button.setButtonsVisible({'Edit': false, 'Save' : true, 'Add': true,'Cancel':true,'Delete':true});
            this.props.editTable.setStatus(window.areaCode.field, 'edit');
            setAreaDisableByStatus.bind(this)(window.areaCode.field,'edit');
        },
        //field保存
        Save: () =>{
           getMultiLang({moduleId: 2011, domainName: 'erm',currentLocale: 'simpchn', callback: (json) => {
            this.props.editTable.filterEmptyRows(window.areaCode.field, ['fieldname'], 'include');
            let flag = this.props.editTable.checkRequired(window.areaCode.field, this.props.editTable.getAllRows(window.areaCode.field));//必输项校验
            if(!flag){
                return;
            }
            let data = this.props.editTable.getAllData(window.areaCode.field);
            let userjson = "{'pagecode':"+window.pagecode+"}";
            let bdata = {
                head:data,
                userjson:userjson
            }
            requestApi.saveField({
                data : bdata,
                success: (resData) => {
                    let ndata = {
                        areacode : window.areaCode.field,
                        rows : []
                    }
                    if(resData.data!=undefined){
                        this.props.editTable.setTableData(window.areaCode.field, resData.data[window.areaCode.field]);
                        this.props.button.setButtonsVisible({"Add": true,"Edit": true,"Delete":true,"Cancel":false,"Save":false});
                    }else{
                        this.props.editTable.setTableData(window.areaCode.field, ndata);
                        this.props.button.setButtonsVisible({"Add": true,"Edit": false,"Delete":false,"Cancel":false,"Save":false});
                    }
                    this.props.editTable.setStatus(window.areaCode.field, 'browse')
                    setAreaDisableByStatus.bind(this)(window.areaCode.field,'browse')
                    //toast({duration:2,color:"success",content:json['201101BXBJ-0012']});
                    this.pubMessage.saveSuccess();

                }
            })
        }})},
        //field取消
        Cancel: () => {
            // 取消编辑态
            this.props.editTable.cancelEdit(window.areaCode.field);
            // 设置按钮状态
            this.props.button.setButtonsVisible({'Cancel': false, 'Add': true, 'Save': false,'Edit': true,'Delete': true});
            setAreaDisableByStatus.bind(this)(window.areaCode.field,'browse')
            
        },
        //field删除
        Delete: () => {
            getMultiLang({moduleId: 2011, domainName: 'erm',currentLocale: 'simpchn', callback: (json) => {
            let rows = this.props.editTable.getCheckedRows(window.areaCode.field);
            if (rows.length) { 
                if("browse"==this.props.editTable.getStatus(window.areaCode.field)){
                    let multiLang = this.props.MutiInit.getIntl(2011);   
                    promptBox({
                        color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                        title: json['201101SQKZ-0004'], //"请注意！",                // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输
                        content: multiLang && multiLang.get('2011-0017'),            // 提示内容,非必输
                        beSureBtnClick: this.beSureDeleteField.bind(this)   // 确定按钮点击调用函数,非必输
                    })
                }else{
                    let arr = rows.map(item => item.index);
                    this.props.editTable.deleteTableRowsByIndex(window.areaCode.field, arr);
                    let rowsNew = this.props.editTable.getCheckedRows(window.areaCode.field);
                    if (!rowsNew.length) {
                        this.props.button.setButtonDisabled(['Delete'],true);
                    }
                }
            }  
        }})
        },
        //field增加
        Add: () => {
            let pk_tradetype = this.props.syncTree.getSelectNode("tree").refcode;
            requestApi.addField({
                data: {
                    "pk_org":window.pk_org,
                    "pk_tradetype":pk_tradetype,
                    "pagecode":window.pagecode
                },
                success: (data) => {
                    this.props.editTable.pasteRow(window.areaCode.field,data.data[window.areaCode.field].rows[0],this.props.editTable.getNumberOfRows(window.areaCode.field)-1);
                    this.props.button.setButtonsVisible({'Edit': false, 'Save' : true, 'Add': true,'Cancel': true,'Delete': true});
                    this.props.editTable.setStatus(window.areaCode.field,'edit');
                    setAreaDisableByStatus.bind(this)(window.areaCode.field,'edit')
                }
            });
        },
        //bill编辑
        Edit1: () => {
            this.props.button.setButtonsVisible({'Edit1': false, 'Save1' : true, 'Add1': true,'Cancel1':true,'Delete1':true});
            this.props.editTable.setStatus(window.areaCode.bill, 'edit');
            setAreaDisableByStatus.bind(this)(window.areaCode.bill,'edit') 
        },
        //bill保存
        Save1: () => {
          getMultiLang({moduleId: 2011, domainName: 'erm',currentLocale: 'simpchn', callback: (json) => {
            this.props.editTable.filterEmptyRows(window.areaCode.bill, ['pk_src_tradetype'], 'include');
            let flag = this.props.editTable.checkRequired(window.areaCode.bill, this.props.editTable.getAllRows(window.areaCode.bill));//必输项校验

            if(!flag){
                return;
            }
            let data = this.props.editTable.getAllData(window.areaCode.bill);
            let userjson = "{'pagecode':"+window.pagecode+"}";
            let bdata = {
                head:data,
                userjson:userjson
            }
            requestApi.saveBill({
                data : bdata,
                success: (resData) => {   
                    let ndata = {
                        areacode : window.areaCode.bill,
                        rows : []
                    }
                    if(resData.data!=undefined){
                        this.props.editTable.setTableData(window.areaCode.bill, resData.data[window.areaCode.bill]);
                        this.props.button.setButtonsVisible({"Add1": true,"Edit1": true,"Delete1":true,"Cancel1":false,"Save1":false});
                    }else{
                        this.props.editTable.setTableData(window.areaCode.bill, ndata);
                        this.props.button.setButtonsVisible({"Add1": true,"Edit1": false,"Delete1":false,"Cancel1":false,"Save1":false});
                    }
                    this.props.editTable.setStatus(window.areaCode.bill, 'browse');
                    setAreaDisableByStatus.bind(this)(window.areaCode.bill,'browse');
                    //toast({duration:2,color:"success",content:json['201101BXBJ-0012']});
                    this.pubMessage.saveSuccess();
                }
            })  
        }})},
        //bill取消
        Cancel1: () => {
             // 取消编辑态
            this.props.editTable.cancelEdit(window.areaCode.bill);
            // 设置按钮状态
            this.props.button.setButtonsVisible({'Cancel1': false, 'Add1': true, 'Save1': false,'Edit1': true,'Delete1': true});
            setAreaDisableByStatus.bind(this)(window.areaCode.bill,'browse')
        },
        //bill删除
        Delete1: () => {
            getMultiLang({moduleId: 2011, domainName: 'erm',currentLocale: 'simpchn', callback: (json) => {
            let rows = this.props.editTable.getCheckedRows(window.areaCode.bill);
            if (rows.length) { 
                if("browse"==this.props.editTable.getStatus(window.areaCode.bill)){
                    let multiLang = this.props.MutiInit.getIntl(2011);   
                    promptBox({
                        color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                        title: json['201101SQKZ-0004'], //"请注意！",                // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输
                        content: multiLang && multiLang.get('2011-0017'),            // 提示内容,非必输
                        beSureBtnClick: this.beSureDeleteBill.bind(this)   // 确定按钮点击调用函数,非必输
                    })   
                }else{
                    let arr = rows.map(item => item.index);
                    this.props.editTable.deleteTableRowsByIndex(window.areaCode.bill, arr);
                    let rowsNew = this.props.editTable.getCheckedRows(window.areaCode.bill);
                    if (!rowsNew.length) {
                        this.props.button.setButtonDisabled(['Delete1'],true);
                    }
                }
            } 
        }})
        },
        //bill增加
        Add1: () => {
            let pk_tradetype = this.props.syncTree.getSelectNode("tree").refcode;
            requestApi.addBill({
                data: {
                    "pk_org":window.pk_org,
                    "pk_tradetype":pk_tradetype,
                    "pagecode":window.pagecode
                },
                success: (data) => {
                    this.props.editTable.pasteRow(window.areaCode.bill,data.data[window.areaCode.bill].rows[0],this.props.editTable.getNumberOfRows(window.areaCode.bill)-1);
                    this.props.button.setButtonsVisible({'Edit1': false, 'Save1' : true, 'Add1': true,'Cancel1': true,'Delete1': true});
                    this.props.editTable.setStatus(window.areaCode.bill,'edit');
                    setAreaDisableByStatus.bind(this)(window.areaCode.bill,'edit')
                }
            }); 
        }

    }
}

/* 设置区域状态 */
function setAreaDisableByStatus(aredCode,status){
    let multiLang = this.props.MutiInit.getIntl(2011);
    if("edit"==status){
        this.setState({pageStatus:"edit"});
        if(window.areaCode.field == aredCode){
            this.props.button.setButtonDisabled(['Edit1','Add1','Delete1','Save1','Cancel1'],true);
            this.props.syncTree.setNodeDisable("tree",true);
            this.props.button.setPopContent('DeleteLine', '');
            this.props.editTable.hideColByKey(window.areaCode.bill, 'opr'); //隐藏扩展列
            this.props.button.setMainButton('Add', false);
        }else if(window.areaCode.bill == aredCode){
            this.props.button.setButtonDisabled(['Edit','Add','Delete','Save','Cancel'],true);
            this.props.syncTree.setNodeDisable("tree",true);
            this.props.button.setPopContent('DeleteLine1', '');
            this.props.editTable.hideColByKey(window.areaCode.field, 'opr'); //隐藏扩展列
            this.props.button.setMainButton('Add1', false);
        }
    }
    if("browse"==status){
        this.setState({pageStatus:"browse"});
        this.props.button.setButtonDisabled(['Edit','Add','Save','Cancel'],false);
        this.props.button.setButtonDisabled(['Edit1','Add1','Save1','Cancel1'],false);
        this.props.button.setMainButton('Add', true);
        this.props.button.setMainButton('Add1', true);
        let fieldRows = this.props.editTable.getCheckedRows(window.areaCode.field);
        if(fieldRows.length){
            this.props.button.setButtonDisabled('Delete', false);
        }else{
            this.props.button.setButtonDisabled('Delete', true);
        }
        let billRows = this.props.editTable.getCheckedRows(window.areaCode.bill);
        if(billRows.length){
            this.props.button.setButtonDisabled('Delete1', false);
        }else{
            this.props.button.setButtonDisabled('Delete1', true);
        }
        this.props.editTable.showColByKey(window.areaCode.field, 'opr'); //显示扩展列
        this.props.editTable.showColByKey(window.areaCode.bill, 'opr'); //显示扩展列
        this.props.syncTree.setNodeDisable("tree",false);
        this.props.button.setPopContent('DeleteLine', multiLang && multiLang.get('2011-0005'));
        this.props.button.setPopContent('DeleteLine1', multiLang && multiLang.get('2011-0005'));
    }
}

/**
 * 树节点选择事件
 */
function treeSelectEvent(key, item) {
    window.tradetype = item.refcode;
    if(key=="00000"){
        let nfileddata = {
            areacode : window.areaCode.field,
            rows : []
        }
        this.props.editTable.setTableData(window.areaCode.field, nfileddata);
        this.props.button.setButtonsVisible({"Add": false,"Edit": false,"Delete":false,"Cancel":false,"Save":false});
        let nbilldata = {
            areacode : window.areaCode.bill,
            rows : []
        }
        this.props.editTable.setTableData(window.areaCode.bill, nbilldata);
        this.props.button.setButtonsVisible({"Add1": false,"Edit1": false,"Delete1":false,"Cancel1":false,"Save1":false});  
    }else{
        refreshData.bind(this)();
    }
}

function refreshData(refreshFlag){
    if(window.tradetype==undefined ||window.tradetype=="00000"){
    	let nfileddata = {
            areacode : window.areaCode.field,
            rows : []
        }
        this.props.editTable.setTableData(window.areaCode.field, nfileddata);
        this.props.button.setButtonsVisible({"Add": false,"Edit": false,"Delete":false,"Cancel":false,"Save":false});
        let nbilldata = {
            areacode : window.areaCode.bill,
            rows : []
        }
        this.props.editTable.setTableData(window.areaCode.bill, nbilldata);
        this.props.button.setButtonsVisible({"Add1": false,"Edit1": false,"Delete1":false,"Cancel1":false,"Save1":false});  
        return;
    }
    let fieldParam={
        pk_tradetype:window.tradetype,
        pk_org:window.pk_org,
        pagecode:window.pagecode
    };
    //设置field表格数据
    requestApi.getFieldTableData({
        data: fieldParam,
        success: (data) => {
            let ndata = {
                areacode : window.areaCode.field,
                rows : []
            }
            if(data.data!=undefined){
                this.props.editTable.setTableData(window.areaCode.field, data.data[window.areaCode.field]);
                this.props.button.setButtonsVisible({"Add": true,"Edit": true,"Delete":true,"Cancel":false,"Save":false});
            }else{
                this.props.editTable.setTableData(window.areaCode.field, ndata);
                this.props.button.setButtonsVisible({"Add": true,"Edit": false,"Delete":false,"Cancel":false,"Save":false});
            }
            
        }
    })
    let billParam={
        pk_tradetype:window.tradetype,
        pk_org:window.pk_org,
        pagecode:window.pagecode
    };
    //设置bill表格数据
    requestApi.getBillTableData({
        data: billParam,
        success: (data) => {
            let ndata = {
                areacode : window.areaCode.bill,
                rows : []
            }
            if(data.data!=undefined){
                this.props.editTable.setTableData(window.areaCode.bill, data.data[window.areaCode.bill]);
                this.props.button.setButtonsVisible({"Add1": true,"Edit1": true,"Delete1":true,"Cancel1":false,"Save1":false});

            }else{
                this.props.editTable.setTableData(window.areaCode.bill, ndata);
                this.props.button.setButtonsVisible({"Add1": true,"Edit1": false,"Delete1":false,"Cancel1":false,"Save1":false});
            }   
        }
    })
    let multiLang = this.props.MutiInit.getIntl(2011);
    this.props.button.setPopContent('DeleteLine', multiLang && multiLang.get('2011-0005'));
    this.props.button.setPopContent('DeleteLine1', multiLang && multiLang.get('2011-0005'));
    this.props.button.setButtonDisabled('Delete', true);
    this.props.button.setButtonDisabled('Delete1', true);
    refreshFlag && this.pubMessage.refreshSuccess();
}

export default {buttonClickEvent,treeSelectEvent,refreshData, setAreaDisableByStatus}


