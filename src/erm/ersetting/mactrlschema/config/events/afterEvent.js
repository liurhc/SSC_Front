import {ajax,base} from 'nc-lightapp-front';
import requestApi from '../requestApi';


export default function afterEvent(props,moduleId,key,value, changedrows,index) {
    let rowid = changedrows[0].rowid;
    
    switch(moduleId) {
         case window.areaCode.field :
            switch (key) {
                case 'fieldname' :
                    if(value.refpk==undefined){
                        return;
                    }
                    props.editTable.setValByKeyAndRowId(moduleId, rowid, "fieldcode", {
                        value : value.values.fullpath || value.refcode,
                    });
                    props.editTable.setValByKeyAndRowId(moduleId, rowid, "fieldname", {
                        value : value.values.fulldisplayname || value.refname,
                        display : value.values.fulldisplayname || value.refname,
                    });
                    // requestApi.fieldValueChange({
                    //     data : {
                    //         key : key,
                    //         refcode : value.refcode,
                    //         refname : value.refname,
                    //         pid : value.pid
                    //     },
                    //     success : (data => {
                    //         if(data.data.fieldcode)
                    //         props.editTable.setValByKeyAndRowId(moduleId, rowid, "fieldcode", {
                    //             value : data.data.fieldcode,
                    //         });
                    //         if(data.data.fieldname)
                    //         props.editTable.setValByKeyAndRowId(moduleId, rowid, "fieldname", {
                    //             value : data.data.fieldname,
                    //             display : data.data.fieldname,
                    //         });
                    //     })
                    // });
                    break;
            }
            break;
        case window.areaCode.bill :
            switch (key) {
                case 'pk_src_tradetype' :
                    if(value.length==0){
                        return;
                    }
                    for(let i=0;i<value.length;i++){
                        if(i==0){
                            props.editTable.setValByKeyAndRowId(moduleId, rowid, "pk_src_tradetype", {
                                value : value[i].refpk,
                                display :value[i].refname
                            });
                            requestApi.billValueChange({
                                data : {
                                    key : key,
                                    value : value[i].refpk
                                },
                                success : (data => {                             
                                    if(data.data.src_tradetype)
                                    props.editTable.setValByKeyAndRowId(moduleId, rowid, "src_tradetype", {
                                        value : data.data.src_tradetype,
                                    });
                                    if(data.data.src_billtype)
                                    props.editTable.setValByKeyAndRowId(moduleId, rowid, "src_billtype", {
                                        value : data.data.src_billtype,
                                    });
                                    if(data.data.src_system)
                                    props.editTable.setValByKeyAndRowId(moduleId, rowid, "src_system", {
                                        value : data.data.src_system,
                                    });
                                })
                            })
                        }else{
                            let pk_tradetype = props.syncTree.getSelectNode("tree").refcode;
                            requestApi.addBill({
                                data: {
                                    "pk_org":window.pk_org,
                                    "pk_tradetype":pk_tradetype,
                                    "pagecode":window.pagecode
                                },
                                success: (data) => {
                                    props.editTable.pasteRow(window.areaCode.bill,data.data[window.areaCode.bill].rows[0],props.editTable.getNumberOfRows(window.areaCode.bill)-1);
                                    let row = props.editTable.getAllData(window.areaCode.bill).rows[props.editTable.getNumberOfRows(window.areaCode.bill)-1];
                                    props.editTable.setValByKeyAndRowId(moduleId, row.rowid, "pk_src_tradetype", {
                                        value : value[i].refpk,
                                        display :value[i].refname
                                    });
                                    requestApi.billValueChange({
                                        data : {
                                            key : key,
                                            value : value[i].refpk
                                        },
                                        success : (data => {                            
                                            if(data.data.src_tradetype)
                                            props.editTable.setValByKeyAndRowId(moduleId, row.rowid, "src_tradetype", {
                                                value : data.data.src_tradetype,
                                            });
                                            if(data.data.src_billtype)
                                            props.editTable.setValByKeyAndRowId(moduleId, row.rowid, "src_billtype", {
                                                value : data.data.src_billtype,
                                            });
                                            if(data.data.src_system)
                                            props.editTable.setValByKeyAndRowId(moduleId, row.rowid, "src_system", {
                                                value : data.data.src_system,
                                            });
                                        })
                                    })
                                }
                            })
                            
                        }
                    }
                break;               
            }
            break;
    }
};