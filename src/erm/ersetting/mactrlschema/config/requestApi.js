import requestApi from "ssccommon/components/requestApi";
import {ajax} from 'nc-lightapp-front';


let requestDomain = '';
window.areaCode = {
    field:'mtapp_cfield',
    bill:'mtapp_cbill'
};
window.pagecode = '201101SQKJ_L';
window.appcode = '201101SQKJ';
let requestApiOverwrite = {
    ...requestApi,
    //单据模板接口
    getTemplet: (opt) => {
       ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/mactrlschema/MappCtrlTempletAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        }) 
    },
    saveField: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/mactrlschema/MappCtrlFieldSaveAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    addField: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/mactrlschema/MappCtrlFieldAddAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    saveBill: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/mactrlschema/MappCtrlBillSaveAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    addBill: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/mactrlschema/MappCtrlBillAddAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    billValueChange: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/mactrlschema/MappCtrlBillChangeAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    fieldValueChange: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/mactrlschema/MappCtrlFieldChangeAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
   getTreeData: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/mactrlschema/FysqTradetypeQueryAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    getFieldTableData: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/mactrlschema/MappCtrlFieldQueryAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    getBillTableData: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/mactrlschema/MappCtrlBillQueryAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    }

}

export default requestApiOverwrite;
