import './events/presetVar';

import React, {Component} from 'react';
import {connect} from 'react-redux';
import {createPage, base, high, getMultiLang} from 'nc-lightapp-front';
import requestApi from './requestApi';
import {buttonClick, initTemplate, afterEvent} from './events';
// import FinanceOrgTreeRef from 'uapbd/refer/org/FinanceOrgTreeRef';


import {
    ProfileStyle,
    ProfileHead,
    ProfileBody,
    BodyLeft,
    BodyRight,
    HeadCenterCustom,
    EmptyArea,
    ButtonGroup
} from 'ssccommon/components/profile';
import pubMessage from 'ssccommon/utils/pubMessage';
import {SyncTree} from 'ssccommon/components/tree';
import {EditTable} from 'ssccommon/components/table';


import './index.less';

const {NCCheckbox,NCModal,NCButton} = base;

class TreeTable extends Component {

    constructor(props) {
        super();
        this.state = {
            disableOrg:false,
            financeOrgTreeRefValue: {},
            pageStatus:'browse',
            pubMultiLang : {}
        }

        getMultiLang({moduleId: 1056, domainName: 'sscrp',currentLocale: 'zh-CN', callback: (json) => {
            this.setState({
                pubMultiLang : json
            })
        }})
        this.pubMessage = new pubMessage(props);
        initTemplate.call(this, props);
    }
    beSureDeleteField(){
        let rows = this.props.editTable.getCheckedRows(window.areaCode.field);
        let arr = rows.map(item => item.index);
        this.props.editTable.deleteTableRowsByIndex(window.areaCode.field, arr);
        let data = this.props.editTable.getAllData(window.areaCode.field);
        let userjson = "{'pagecode':"+window.pagecode+"}";
        let bdata = {
            head:data,
            userjson:userjson
        }
        requestApi.saveField({
            data : bdata,
            success: (resData) => {
                let ndata = {
                    areacode : window.areaCode.field,
                    rows : []
                }
                if(resData.data!=undefined){
                    this.props.editTable.setTableData(window.areaCode.field, resData.data[window.areaCode.field]);
                }else{
                    this.props.editTable.setTableData(window.areaCode.field, ndata);
                    this.props.button.setButtonsVisible({"Add": true,"Edit": false,"Delete":false,"Cancel":false,"Save":false});
                }
                buttonClick.setAreaDisableByStatus.bind(this)(window.areaCode.field,'browse')
            }
        })  
    }
    beSureDeleteBill(){
        let rows = this.props.editTable.getCheckedRows(window.areaCode.bill);
        let arr = rows.map(item => item.index);
        this.props.editTable.deleteTableRowsByIndex(window.areaCode.bill, arr);
        let data = this.props.editTable.getAllData(window.areaCode.bill);
        let userjson = "{'pagecode':"+window.pagecode+"}";
        let bdata = {
            head:data,
            userjson:userjson
        }
        requestApi.saveBill({
            data : bdata,
            success: (resData) => {
                let ndata = {
                    areacode : window.areaCode.bill,
                    rows : []
                }
                if(resData.data!=undefined){
                    this.props.editTable.setTableData(window.areaCode.bill, resData.data[window.areaCode.bill]);
                }else{
                    this.props.editTable.setTableData(window.areaCode.bill, ndata);
                    this.props.button.setButtonsVisible({"Add1": true,"Edit1": false,"Delete1":false,"Cancel1":false,"Save1":false});
                }
                buttonClick.setAreaDisableByStatus.bind(this)(window.areaCode.bill,'browse')
            }
        })  
    }

    onSelectedField(){
        let fieldRows = this.editTable.getCheckedRows(window.areaCode.field);
        if(fieldRows.length){
            this.button.setButtonDisabled('Delete', false);
        }else{
            this.button.setButtonDisabled('Delete', true);
        }
    }
    onSelectedBill(){
        let billRows = this.editTable.getCheckedRows(window.areaCode.bill);
        if(billRows.length){
            this.button.setButtonDisabled('Delete1', false);
        }else{
            this.button.setButtonDisabled('Delete1', true);
        }
    }

    componentWillMount() {
                window.onbeforeunload = () => {
                      ;
                     let status = this.props.editTable.getStatus(window.areaCode.bill);
                     let status1 = this.props.editTable.getStatus(window.areaCode.field);                  
                    if (status == 'edit'||status1 == 'edit') {
                        return ''; 
                         }
             }
            }
    componentDidMount() {

    }

    render() {
        let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId

        let btnModalConfig = {
            ['Cancel1']: {
                // "2011-0004": "删除",
                // "2011-0005": "确认删除该单据吗？"
                title:  this.state.pubMultiLang['105601DJYS-0005'],
                content: this.state.pubMultiLang['105601GGZD-0061']
            }
        }
        return (
            <ProfileStyle
                id='matcrl-wrap'
                layout="treeTable"
                {...this.props}
            >
                {/*页面头*/}
                <ProfileHead
                    // "201101SQKJ-0001": "费用申请控制规则-集团"
                    title={multiLang && multiLang.get('201101SQKJ-0001')}
                    refreshButtonEvent={buttonClick.refreshData.bind(this, true)}
                    status = { this.state.pageStatus}  
                >
 
                </ProfileHead>

                {/*页面体*/}
                <ProfileBody>
                    <BodyLeft>
                        <SyncTree
                            areaId="tree"
                            needSearch={false}
                            onSelectEve={
                                buttonClick.treeSelectEvent.bind(this)
                            }
                        />
                    </BodyLeft>

                    <BodyRight>
                         <EditTable
                            height='202px'
                            areaId="mtapp_cfield"
                             // "201101SQKJ-0002": "控制维度"
                            title={multiLang && multiLang.get('201101SQKJ-0002')}
                            onAfterEvent={afterEvent.bind(this)}
                            onSelected={this.onSelectedField}
                            onSelectedAll={this.onSelectedField}
                        >
                            <ButtonGroup
                                area="body-shoulder"
                                buttonEvent={buttonClick.buttonClickEvent.bind(this)}
								disableDefaultCancelPop={false}
                            />
                        </EditTable>
                        <EditTable
                            areaId="mtapp_cbill"
                            // "201101SQKJ-0003": "控制单据"
                            title={multiLang && multiLang.get('201101SQKJ-0003')}
                            onAfterEvent={afterEvent.bind(this)}
                            onSelected={this.onSelectedBill}
                            onSelectedAll={this.onSelectedBill}
                        >
                            <ButtonGroup
                                area="body-shoulder1"
                                buttonEvent={buttonClick.buttonClickEvent.bind(this)}
                                disableDefaultCancelPop={false}
                                modalConfig={btnModalConfig}
                            />
                        </EditTable>
                    
                    </BodyRight>
                </ProfileBody>
            </ProfileStyle>

        )
    }
}

TreeTable = createPage({
    mutiLangCode: '2011'
})(TreeTable);

export default TreeTable;
