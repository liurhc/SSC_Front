import React, {Component} from 'react';
import {connect} from 'react-redux';
import {createPage, base, high} from 'nc-lightapp-front';
import {buttonClick, initTemplate, afterEvent,refreshButtonClick} from './events';
import {
    ProfileStyle,
    ProfileHead,
    ProfileBody,
    HeadCenterCustom,
    ButtonGroup
} from 'ssccommon/components/profile';
import {EditTable} from 'ssccommon/components/table';
import FinanceOrgTreeRef from 'uapbd/refer/org/FinanceOrgTreeRef';
import './index.less';
import reqApi from './requestApi';
import pubMessage from 'ssccommon/utils/pubMessage';

class SingleTable extends Component {

    constructor(props) {
        super();
        initTemplate.call(this, props);
        this.state = {
            financeOrgTreeRefValue: {},
            isSHow: false,
            status: 'browse',
            disableFinanceOrgTreeRef:false
        }
        this.pubMessage = new pubMessage(props);
    }


    componentWillMount() {
                window.onbeforeunload = () => {
                      ;
                    let status = this.props.editTable.getStatus(window.bodycode);        
                    if (status == 'edit') {
                        return ''; 
                         }
             }
            }
    componentDidMount() {

    }

    onSelectedFn(){
        let rows1 = this.editTable.getCheckedRows(window.bodycode);
        if(rows1.length){
            this.button.setButtonDisabled('Delete', false);
        }else{
            this.button.setButtonDisabled('Delete', true);
        }
    }


    render() {
        window.orgflag = this.props.getUrlParam('orgflag');
        // "201101JZDJ-0001": "分摊结转单据对照设置-集团"
        // let titlein= multiLang && multiLang.get('201101JZDJ-0001');
        let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId
        let titlein= multiLang && multiLang.get('201101JZDJ-0001');
        let NCCreateSearchall='';

        if(Object.is(window.orgflag,'org'))
        {
            // "201101JZDJ-0002": "分摊结转单据对照设置-组织",
            // titlein= multiLang && multiLang.get('201101JZDJ-0002');
            titlein= multiLang && multiLang.get('201101JZDJ-0002');
            this.state.isSHow = true;
        }
        let props = this.props;

        return (
            /*档案风格布局*/
            <ProfileStyle
                layout="singleTable"
                {...this.props}
            >
                {/*页面头*/}
                <ProfileHead
                    title={titlein}
                    refreshButtonEvent={refreshButtonClick.bind(this,props)} 
                    status = { this.state.status }
                >
                    <HeadCenterCustom>
                        {this.state.isSHow ==true ? ([<span class="ssc-ref-must">*</span>,<FinanceOrgTreeRef
                            value={this.state.financeOrgTreeRefValue}
                            onChange={(data) => {
                                window.pk_org = data.refpk;
                                this.setState({
                                    financeOrgTreeRefValue: data
                                }) ;
                                reqApi.query({
                                    data: {
                                        "orgflag":window.orgflag,
                                        "pk_org":window.pk_org,
                                        "pagecode":window.pagecode
                                    },
                                    success: (data) => {
                                        data && this.props.editTable.setTableData(window.bodycode, data[window.bodycode]);
                                        let reqData = this.props.editTable.getAllData(window.bodycode);
                                        if(reqData && reqData.rows && reqData.rows.length>0)
                                        {
                                                let rowarr = new Array(reqData.rows.length);
                                                for(let m=0;m<reqData.rows.length;m++){
                                                    let pk_org= reqData.rows[m].values.pk_org.value;
                                                    let pk_group= reqData.rows[m].values.pk_group.value;
                                                    if(!(window.orgflag =='org' && pk_org!=pk_group)){
                                                        rowarr.push(m);
                                                    }
                                                }
                                                if(window.orgflag =='org')
                                                { 
                                                this.props.editTable.setCheckboxDisabled(window.bodycode, rowarr, false); 
                                                }
                                        }
                                    }
                                });
                            }}
                            queryCondition={() => {
                                return {
                                    TreeRefActionExt:'nccloud.web.action.erm.ref.sqlbuilder.BusinessUnitTreeRefBuilder',
                                    appcode : props.getSearchParam('c')
                                }
                            }}
                            disabled={this.state.disableFinanceOrgTreeRef}
                        />]) : null}
                        
                    </HeadCenterCustom>

                    <ButtonGroup
                        area="page_header"
                        buttonLimit="3"
                        buttonEvent={buttonClick.bind(this)}
                    />
                </ProfileHead>

                {/*页面体*/}
                <ProfileBody>
                    <EditTable
                        areaId="billcontrast"
                        onAfterEvent={afterEvent.bind(this)}
                        onSelected={this.onSelectedFn}
                        onSelectedAll={this.onSelectedFn}
                    >

                    </EditTable>

                </ProfileBody>
            </ProfileStyle>
        )
    }
}

SingleTable = createPage({
    mutiLangCode: '2011'
})(SingleTable);

export default SingleTable;
