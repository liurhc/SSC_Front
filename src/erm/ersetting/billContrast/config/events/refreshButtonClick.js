import requestApi from '../requestApi';

export default function refreshButtonClick(props) {
    requestApi.query({
        data: {
            "orgflag":window.orgflag,
            "pk_org":window.pk_org,
            "pagecode":window.pagecode
        },
        success: (data) => {
            data && this.props.editTable.setTableData(window.bodycode, data[window.bodycode]); 
            this.pubMessage.refreshSuccess();               
        }
    });
}

