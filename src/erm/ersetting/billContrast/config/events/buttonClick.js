import {ajax,getMultiLang} from 'nc-lightapp-front';
import requestApi from '../requestApi';
import {toast,promptBox} from 'nc-lightapp-front';
export default function buttonClick() {
let props = this.props;
//新增
return{
    ['Add']: () => {
        requestApi.add({
            data: {
                "orgflag":window.orgflag,
                "pk_org":window.pk_org,
                "pagecode":window.pagecode
            },
            success: (data) => {
              // props.editTable.pasteRow(window.bodycode,data[window.bodycode].rows[0],props.editTable.getNumberOfRows(window.bodycode)-1);
                props.editTable.addRow(window.bodycode,props.editTable.getNumberOfRows(window.bodycode),true,data[window.bodycode].rows[0].values);
                setTimeout(()=>{
                    props.button.setButtonsVisible({'Edit': false, 'Save' : true, 'Add': true,'Cancel': true,'Delete': true})
                },0)
                props.button.setMainButton('Add', false);
                props.button.setMainButton('Save', true);
                props.editTable.setStatus(window.bodycode,'edit');
                this.setState({ status : 'edit',disableFinanceOrgTreeRef:true});
                let reqData = props.editTable.getAllData(window.bodycode);
                if(reqData && reqData.rows && reqData.rows.length>0)
                {
                let rowidarr = new Array(reqData.rows.length);
                let rowarr = new Array(reqData.rows.length);
                for(let m=0;m<reqData.rows.length;m++){
                    let pk_org= reqData.rows[m].values.pk_org.value;
                    let pk_group= reqData.rows[m].values.pk_group.value;
                    if(!(window.orgflag =='org' && pk_org!=pk_group)){
                        rowidarr.push(reqData.rows[m].rowid);
                        rowarr.push(m);
                       
                    }
                 }
                if(window.orgflag =='org')
                 { 
                 props.editTable.setCheckboxDisabled(window.bodycode, rowarr, false);
                 props.editTable.setEditableRowByRowId(window.bodycode, rowidarr, false);
                 }
                 
                }
               
            }
        });

},


//修改

    ['Edit']: () => {
        getMultiLang({moduleId: 2011, domainName: 'erm',currentLocale: 'zh-CN', callback: (json) => {
        //设置按钮的可见状态
        if(window.orgflag =='org' && (window.pk_org==null || window.pk_org.length==0))
        {toast({ content: json['201101JZDJ-0003'], color: 'danger' });return;}//'请选择组织后修改!'
        setTimeout(()=>{
            props.button.setButtonsVisible({'Edit': false, 'Save' : true, 'Add': true,'Cancel': true,'Delete': true})
        },0)
        
        props.editTable.setStatus(window.bodycode,'edit');
        props.button.setMainButton('Add', false);
        props.button.setMainButton('Save', true);
        this.setState({ status : 'edit',disableFinanceOrgTreeRef:true});
        let reqData = props.editTable.getAllData(window.bodycode);
        if(reqData && reqData.rows && reqData.rows.length>0)
        {
                let rowidarr = new Array(reqData.rows.length);
                let rowarr = new Array(reqData.rows.length);
                for(let m=0;m<reqData.rows.length;m++){
                    let pk_org= reqData.rows[m].values.pk_org.value;
                    let pk_group= reqData.rows[m].values.pk_group.value;
                    if(!(window.orgflag =='org' && pk_org!=pk_group)){
                        rowidarr.push(reqData.rows[m].rowid);
                        rowarr.push(m);
                       
                    }
                 }
                 if(window.orgflag =='org')
                 { 
                     props.editTable.setCheckboxDisabled(window.bodycode, rowarr, false);
                     props.editTable.setEditableRowByRowId(window.bodycode, rowidarr, false);
                 }
                
        }
    }})
},


//保存

    ['Save']: () => {
        props.editTable.filterEmptyRows(window.bodycode, []);
        let flag = props.editTable.checkRequired(window.bodycode, props.editTable.getAllRows(window.bodycode));//必输项校验
        if(flag){
            let reqData = props.editTable.getAllData(window.bodycode);
            requestApi.save({
                data: reqData,
                success: (data) => {
                    let ndata = {
                        areacode : window.bodycode,
                        rows : []
                        }
                        if(data && data[window.bodycode]){
                        data && data[window.bodycode] &&  props.editTable.setTableData(window.bodycode, data[window.bodycode]);
                        props.button.setButtonsVisible({'Edit': true, 'Save' : false, 'Add': true,'Cancel': false,'Delete': true});
                        }else{
                        data && data[window.bodycode] &&  props.editTable.setTableData(window.bodycode, ndata);
                        props.button.setButtonsVisible({'Edit': false, 'Save' : false, 'Add': true,'Cancel': false,'Delete': true});
                        }
                    props.editTable.cancelEdit(window.bodycode);
                    this.setState({ status : 'browse',disableFinanceOrgTreeRef:false});
                    props.button.setMainButton('Add', true);
                    props.button.setMainButton('Save', false);
                    this.pubMessage.saveSuccess();
                    let reqData = props.editTable.getAllData(window.bodycode);
                    if(reqData && reqData.rows && reqData.rows.length>0)
                    {
                            let rowarr = new Array(reqData.rows.length);
                            for(let m=0;m<reqData.rows.length;m++){
                                let pk_org= reqData.rows[m].values.pk_org.value;
                                let pk_group= reqData.rows[m].values.pk_group.value;
                                if(!(window.orgflag =='org' && pk_org!=pk_group)){
                                    rowarr.push(m);
                                }
                            }
                            if(window.orgflag =='org')
                            { 
                            props.editTable.setCheckboxDisabled(window.bodycode, rowarr, false); 
                            }
                    }
                }
            });
        }
},

//取消
    ['Cancel']: () => {
        props.editTable.cancelEdit(window.bodycode);
        this.setState({ status : 'browse',disableFinanceOrgTreeRef:false});
        props.button.setMainButton('Add', true);
        props.button.setMainButton('Save', false);
        props.button.setButtonsVisible({'Edit': true, 'Save' : false, 'Add': true,'Cancel': false,'Delete': true});
        requestApi.query({
            data: {
                "orgflag":window.orgflag,
                "pk_org":window.pk_org,
                "pagecode":window.pagecode
            },
            success: (data) => {
                let ndata = {
                    areacode : window.bodycode,
                    rows : []
                    }
                    if(data && data[window.bodycode]){
                        data && data[window.bodycode] &&  props.editTable.setTableData(window.bodycode, data[window.bodycode]);
                        props.button.setButtonsVisible({'Edit': true, 'Save' : false, 'Add': true,'Cancel': false,'Delete': true});
                    }else{
                        props.editTable.setTableData(window.bodycode, ndata);
                        props.button.setButtonsVisible({'Edit': false, 'Save' : false, 'Add': true,'Cancel': false,'Delete': true});
                    }

                    let reqData = props.editTable.getAllData(window.bodycode);
                    if(reqData && reqData.rows && reqData.rows.length>0)
                    {
                            let rowarr = new Array(reqData.rows.length);
                            for(let m=0;m<reqData.rows.length;m++){
                                let pk_org= reqData.rows[m].values.pk_org.value;
                                let pk_group= reqData.rows[m].values.pk_group.value;
                                if(!(window.orgflag =='org' && pk_org!=pk_group)){
                                    rowarr.push(m);
                                }
                            }
                            if(window.orgflag =='org')
                            { 
                            props.editTable.setCheckboxDisabled(window.bodycode, rowarr, false); 
                            }
                    }
            }
        });
},



//删除
    ['Delete']: () => {
       let status=props.editTable.getStatus(window.bodycode);
       if(status=='browse'){
        getMultiLang({moduleId: 2011, domainName: 'erm',currentLocale: 'zh-CN', callback: (json) => {     
        promptBox({
            
            title: json['2011-0004'],//"提示", // 弹框表头信息
            content: json['2011-0018'],//"确定要删除吗？", //'确定退出新增工单', //弹框内容，可以是字符串或dom
            beSureBtnClick: () => {
                del()
            }, //点击确定按钮事件
            cancelBtnClick: () => {
              
            },
            userControl: false, // 点确定按钮后，是否自动关闭弹出框.true:手动关。false:自动关
         
            rightBtnName:json['2011-0007'],//"关闭", //左侧按钮名称,默认关闭
            leftBtnName:json['2011-0003'],//"确认" //右侧按钮名称， 默认确认
            
        })
    }})
       }else{
        del()
       }

       function del(){

        let rows = props.editTable.getCheckedRows(window.bodycode);
        let src_tradetype= null;
        if(rows.values.src_tradetype)
        src_tradetype=rows.values.src_tradetype.value
        let des_tradetype= null;
        if(rows.values.des_tradetype)
        des_tradetype=rows.values.des_tradetype.value
        if(src_tradetype==null || des_tradetype==null || src_tradetype.length==0||des_tradetype.length==0){
            props.button.setButtonDisabled('Delete', true);
            if (rows.length) {
                let arr = rows.map(item => item.index);
                props.editTable.deleteTableRowsByIndex(window.bodycode, arr);
            }
            return;
        }       
        //处理存数的删除
            if (rows.length) {
                let arr = rows.map(item => item.index);
                props.editTable.deleteTableRowsByIndex(window.bodycode, arr);
                props.button.setButtonDisabled('Delete', true);
               
                let reqinData = props.editTable.getAllData(window.bodycode);
                requestApi.del({
                    data: reqinData,
                    success: (data) => {
                        let ndata = {
                            areacode : window.bodycode,
                            rows : []
                            }
                            let status=props.editTable.getStatus(window.bodycode);
                        
                            if(data && data[window.bodycode]){
                                data && data[window.bodycode] &&  props.editTable.setTableData(window.bodycode, data[window.bodycode]);
                                if(status=='browse'){
                                props.button.setButtonsVisible({'Edit': true, 'Save' : false, 'Add': true,'Cancel': false,'Delete': true});
                                props.button.setButtonDisabled('Delete', true);
                                props.button.setMainButton('Add', true);
                                }else{
                                props.button.setButtonDisabled('Delete', true);
                                }
                                let reqData = props.editTable.getAllData(window.bodycode);
                                if(reqData && reqData.rows && reqData.rows.length>0)
                                {
                                        let rowarr = new Array(reqData.rows.length);
                                        for(let m=0;m<reqData.rows.length;m++){
                                            let pk_org= reqData.rows[m].values.pk_org.value;
                                            let pk_group= reqData.rows[m].values.pk_group.value;
                                            if(!(window.orgflag =='org' && pk_org!=pk_group)){
                                                rowarr.push(m);
                                            }
                                        }
                                        if(window.orgflag =='org')
                                        { 
                                        props.editTable.setCheckboxDisabled(window.bodycode, rowarr, false); 
                                        }
                                }
                            }else{
                                if(status=='browse'){
                                props.editTable.setTableData(window.bodycode, ndata);
                                props.button.setButtonsVisible({'Edit': false, 'Save' : false, 'Add': true,'Cancel': false,'Delete': true});
                                }else{
                                props.button.setButtonDisabled('Delete', true);
                                }
                            }
                   
                        
                    }
            });
        
       }
       }
            
   }
 }

}


/* 刷新界面 */
function doQuery(){

    requestApi.query({
        data: {
            "orgflag":window.orgflag,
            "pk_org":window.pk_org,
            "pagecode":window.pagecode
        },
        success: (data) => {
            let ndata = {
                areacode : window.bodycode,
                rows : []
                }
            if(data && data[window.bodycode]){
                data && data[window.bodycode] &&  props.editTable.setTableData(window.bodycode, data[window.bodycode]);
                props.button.setButtonsVisible({'Edit': true, 'Save' : false, 'Add': true,'Cancel': false,'Delete': true});
            }else{
                props.editTable.setTableData(window.bodycode, ndata);
                props.button.setButtonsVisible({'Edit': false, 'Save' : false, 'Add': true,'Cancel': false,'Delete': true});
            }
        }
    });
}
