import {base, high, ajax} from 'nc-lightapp-front';
import 'ssccommon/components/globalPresetVar';
import requestApi from '../requestApi';



//变量设置规范，如需要
window.presetVar = {
    ...window.presetVar,
    pageId: '',
    head: {
        head1: '',
        head2: ''
    },
    body: {
        body1: window.bodycode,
        body2: ''
    },
    search: {}
};


export default function (props) {
    window.orgflag = props.getUrlParam('orgflag');
    let appidStr='201101JZDJ';
    if(window.orgflag=='org')
    {
        appidStr='201101JZDZ';
        window.pagecode='201101JZDZ_ORG';
    }
 
    props.createUIDom(
        {
            pagecode: window.pagecode,
            appcode: appidStr
        },
        function (data) {
           let meta = data.template;
           let button = data.button;

           let BilltypeRefSqlFilterPath ='nccloud.web.action.erm.ref.sqlbuilder.BilltypeRefSqlBuilder';//交易类型参照
           //处理参照过滤
           meta[window.bodycode].items.find((item) => item.attrcode === "src_tradetypeid").queryCondition = function () {
            return {
             parentbilltype: '264X',
             ExtRefSqlBuilder:BilltypeRefSqlFilterPath
             }
        };

          meta[window.bodycode].items.find((item) => item.attrcode === "des_tradetypeid").queryCondition = function () {
                return {
                 parentbilltype: '265X',
                 ExtRefSqlBuilder:BilltypeRefSqlFilterPath
                 }
        };

            //设置区域模板
            props.meta.setMeta(meta);
            //设置按钮模板
            props.button.setButtons(button);

            //设置按钮的初始可见性
            props.button.setButtonsVisible({'Edit': true, 'Save' : false, 'Add': true,'Cancel': false,'Delete': true});

            props.editTable.setStatus(window.bodycode, 'browse');

            //设置初始数据
            //获取表格的数据
            let pk_org =''
            //if(typeof(pk_org)!=='undefined')
            //pk_org = this.props.search.getSearchValByField('searchArea', 'pk_org');

            requestApi.query({
                data: {
                    "orgflag":window.orgflag,
                    "pk_org":window.pk_org,
                    "pagecode":window.pagecode
                },
                success: (data) => {
                    props.button.setButtonsVisible({'Edit': true, 'Save' : false, 'Add': true,'Cancel': false,'Delete': true});
                    data && data[window.bodycode] &&  props.editTable.setTableData(window.bodycode, data[window.bodycode]);
                    
                    let reqData = props.editTable.getAllData(window.bodycode);
                    if(reqData && reqData.rows && reqData.rows.length>0)
                    {
                            let rowarr = new Array(reqData.rows.length);
                            for(let m=0;m<reqData.rows.length;m++){
                                let pk_org= reqData.rows[m].values.pk_org.value;
                                let pk_group= reqData.rows[m].values.pk_group.value;
                                if(!(window.orgflag =='org' && pk_org!=pk_group)){
                                    rowarr.push(m);
                                }
                            }
                            if(window.orgflag =='org')
                            { 
                            props.editTable.setCheckboxDisabled(window.bodycode, rowarr, false); 
                            }
                    }
                    
                }
            });

            props.button.setButtonDisabled('Delete', true);
        }
    )

}
