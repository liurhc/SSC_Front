import requestApi from '../requestApi';

export default function (props, id, key, value, data, index) {
    if (!value  || Object.keys(value).length === 0)  {
        return;
    }
    requestApi.valueChange({
        data : {
            key : key,
            value : value.refpk
        },
        success : (data => {
            if(data.src_tradetype)
            props.editTable.setValByKeyAndIndex(id, index, "src_tradetype", {
                display: data.src_tradetype,
                value : data.src_tradetype
            });
            if(data.src_billtype)
            props.editTable.setValByKeyAndIndex(id, index, "src_billtype", {
                display: data.src_billtype,
                value : data.src_billtype
            });
            if(data.src_billtypeid)
            props.editTable.setValByKeyAndIndex(id, index, "src_billtypeid", {
                display: data.src_billtypeid,
                value : data.src_billtypeid
            });
            if(data.des_tradetype)
            props.editTable.setValByKeyAndIndex(id, index, "des_tradetype", {
                display: data.des_tradetype,
                value : data.des_tradetype
            });
            if(data.des_billtype)
            props.editTable.setValByKeyAndIndex(id, index, "des_billtype", {
                display: data.des_billtype,
                value : data.des_billtype
            });
            if(data.des_billtypeid)
            props.editTable.setValByKeyAndIndex(id, index, "des_billtypeid", {
                display: data.des_billtypeid,
                value : data.des_billtypeid
            });
        })
    })
}