import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {connect, Provider} from 'react-redux';
import store from './store/store';
import SingleTable from './SingleTable';

ReactDOM.render((<Provider store={store}><SingleTable /></Provider>)
    , document.querySelector('#app'));