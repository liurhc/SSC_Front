import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { SimpleReport } from 'nc-report'

export default class Ermloanbalance extends Component {
    constructor (props) {
        super(props)
        this.state = {}
    }

    disposeSearch (meta, props) {
        let DeptTreeOrgRef = 'nccloud.web.action.erm.ref.sqlbuilder.DeptTreeStopRef';//部门
        //获取收支项目参照
        let items = meta['light_report'].items
        items.forEach((item) => {
            let filter = item.referFilterAttr;
            if (filter) {
                if (filter[item.attrcode] && filter[item.attrcode].length > 0) {
                    filter[item.attrcode] = filter[item.attrcode].join(",");
                }
                else {
                    filter[item.attrcode] = '';
                }
            } else {
                filter = {};
                filter[item.attrcode] = '';
            }
            if (item.attrcode == 'deptid') {
                item.queryCondition = () => {
                    let data = props.search.getSearchValByField('light_report', 'pk_org').value.firstvalue;
                    return { pk_org: data,TreeRefActionExt:DeptTreeOrgRef}; // 根据pk_org过滤
                };
            }
            if (item.attrcode == 'jkbxr') {
                item.queryCondition = () => {
                    let pk_org = props.search.getSearchValByField('light_report', 'pk_org').value.firstvalue;
                    let deptid = props.search.getSearchValByField('light_report', 'deptid').value.firstvalue;
                    return {dept: deptid,pk_org: pk_org};
                };
            }
        })
        return meta; // 处理后的过滤参照返回给查询区模板
    }
    /**
     *
     * @param  items: 查询区查询数据，如需拓展查询区参数，请返回与items相同格式的查询数据
     */
    expandSearchVal (items) {
        return items
    }

    /**
      * searchId: 查询区需要的searchId参数
      * 'vname': 需要附默认值的字段
      * {value: '111'}: 显示值，区间为[]，具体可以对照平台查询区修改
      * 'like': 为oprtype字段值
    */
   setDefaultVal(searchId, props) {
    //查询区默认显示字段值

   }

    /**
     * props: props
     * searchId: 查询区需要的searchId参数
     * field: 编辑后的key
     * val: 编辑后的value
    */
    onAfterEvent (props, searchId, field, val) {
    //查询区编辑后事件
    console.log(props)
    console.log(searchId)
    console.log(field)
    console.log(val)
    }

    /**
     * isRange: 查询后数据区域内点击单元格按钮可操作。如需要像升降序按钮那样，则设置 disabled={isRange}属性
     * data: 表格数据
     * coords: 选中的单元格
    */
    CreateNewSearchArea (isRange, data, coords) {
    // button区域业务端自定义的新增按钮
    //return <NCButton disabled={isRange} className="btn" shape="border">新增按钮</NCButton>
    }

    /**
    * transSaveObject: 业务端处理的transSaveObject参数
    * obj: 点击联查下拉的item信息
    * data: 联查需要的参数
    * props: 平台props
    * url: 平台openTo第一个参数
    * urlParams: 平台openTo第二个参数
    * sessonKey: sessionStorage的key
    */
   setConnectionSearch(transSaveObject, obj, data, props, url, urlParams, sessonKey) {
    sessionStorage.setItem(sessonKey, transSaveObject); //处理好的transSaveObject放到浏览器内存中，必须，跳转报表页面会用到

    props.openTo(url, urlParams);
   }


    render() {
        return (
            <div className="table">
                <SimpleReport
                    showAdvBtn={true}
                    setDefaultVal={this.setDefaultVal.bind(this)}
                    expandSearchVal={this.expandSearchVal.bind(this)}
                    disposeSearch={this.disposeSearch.bind(this)}
                    onAfterEvent={this.onAfterEvent.bind(this)}
                    CreateNewSearchArea={this.CreateNewSearchArea.bind(this)}
                    setConnectionSearch={this.setConnectionSearch.bind(this)}
                />
            </div>
        );
    }
}

ReactDOM.render(<Ermloanbalance/>,document.getElementById('app'));