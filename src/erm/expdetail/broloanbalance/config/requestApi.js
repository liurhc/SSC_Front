import requestApi from "ssccommon/components/requestApi";
import {ajax} from 'nc-lightapp-front';

let requestApiOverwrite = {
    ...requestApi,
    //单据模板接口

    getTableData: (opt) => {
        ajax({
            url: `/nccloud/erm/expdetail/LoanBalanceViewAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    query:(opt) => {
        ajax({
            url: `/nccloud/erm/expdetail/JkzbSearchAction.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    }

}

export default requestApiOverwrite;
