import React, {Component} from 'react';
import {createPage} from 'nc-lightapp-front';
import { initTemplate, afterEvent,searchClick} from './events';

import {
    ProfileStyle,
    ProfileHead,
    ProfileBody,
    EmptyArea
} from 'ssccommon/components/profile';
import {EditTable} from 'ssccommon/components/table';

import './index.less';

class Expamortize extends Component {

    
    constructor(props) {
        super();
        this.treeFormId = "treeForm";
        this.state = {
            businessUnitTreeRefValue: {},
            businessUnitTreeRefValue1: {},
            showModal:false,
            showList:false,
            period:''
        }
        initTemplate.call(this, props);
    }

    render() {
        let { search} = this.props;
        let {NCCreateSearch} = search;
        let multiLang = this.props.MutiInit.getIntl(2011); //this.moduleId

        return (
            /*档案风格布局*/
            <ProfileStyle
                layout="singleTable"
                {...this.props}
            >
                {/*页面头*/}
                {/*"201112LBYE-0001": "借款余额",*/}
                <ProfileHead
                    title={multiLang && multiLang.get('201112LBYE-0001')}
                >
                </ProfileHead>

                {/*页面体*/}
                <ProfileBody>
                    <EmptyArea className='ssc-profile-search-area'>
                        { NCCreateSearch(
                            '20110BO',//模块id
                            {
                                clickSearchBtn: searchClick.bind(this),//   点击按钮事件
                                defaultConditionsNum:1, //默认显示几个查询条件
                                onAfterEvent: afterEvent()//编辑后事件
                            }
                        )}
                    </EmptyArea>
                    <EditTable
                        areaId="Loanmanage"
                        showCheck= {false}
                        onAfterEvent={afterEvent.bind(this)}
                    > 
                    </EditTable>
                    
                </ProfileBody>
            </ProfileStyle>
        )
    }
}

Expamortize = createPage({
    mutiLangCode: '2011'
})(Expamortize);

export default Expamortize;
