import 'ssccommon/components/globalPresetVar';
import requestApi from '../requestApi';

//变量设置规范，如需要
window.presetVar = {
    ...window.presetVar,
    pageId: '',
    head: {
        head1: '20110BO',
        head2: ''
    },
    body: {
        body1: 'Loanmanage',
        body2: ''
    },
    search: {}
};

let body = window.presetVar.body;


export default function (props) {

    props.createUIDom(
        {
            pagecode: 'LoanList',//页面id
            appid: '0001Z310000000003LP2'//注册按钮的id
        },
        (data) => {
            let meta = data.template;
            let metas = {
                ...meta,
                searchArea: {
                    moduletype: 'search',
                    items : data.template[window.presetVar.head.head1].items
                },
            }
            meta.pageid = data.template.pageid;
            //设置区域模板
            props.meta.setMeta(metas);
            //设置按钮模板
            // props.button.setButtons(buttonTpl);

            //设置按钮行为为弹窗
            // props.button.setPopContent('Delete', '确认要删除该信息吗？')
            /* 设置操作列上删除按钮的弹窗提示 */

            //设置按钮的初始可见性
            // props.button.setButtonsVisible({'Add': true});

            //设置初始数据
            //获取表格的数据
            requestApi.getTableData({
                data: {
                    pk_org: this.state.businessUnitTreeRefValue.refpk,
                    isGroup: false
                },
                success: (data) => {
                    if(data.data != null){
                        if(data.data.pk_org != null){
                            this.setState({
                                businessUnitTreeRefValue: {refpk: data.data.pk_org, refname: data.data.name_org},
                                period:data.data.period
                            })
                            return;
                        }else{
                            this.props.editTable.setTableData(body.body1, data.data[body.body1]);
                            this.props.editTable.setStatus(body.body1, 'edit');

                            if ('back' != this.props.getUrlParam('status')) {
                                // let userJson = JSON.parse(data.data['userjson']);
                                // this.setState({
                                //     businessUnitTreeRefValue: {refpk: userJson.pk_org, refname: userJson.name_org},
                                //     period:userJson.period
                                // })
                            }
                        }
                    }
                }
            })

        }
    )

}


