import requestApi from '../requestApi'

export default function searchClick(props, searchVal) {
    if(searchVal){
        let data={
            querycondition:searchVal,
            pagecode: 'LoanList',
            pageInfo:{
                "pageIndex":-1,
                "pageSize":10,
                "totalPage":"0"
            },
            queryAreaCode:'20110BO',  //查询区编码
            oid:'0001Z310000000001DFH',  //查询模板id，手工添加在界面模板json中，放在查询区，后期会修改
            querytype:'tree',
            tradetype:props.getUrlParam("tradetype")
        };
        let senddata = {data,"pagecode":"201112GRYE_Q","tepmid":this.props.meta.getMeta().pageid};
        requestApi.query({
            data:senddata,
            success: (data) => {
                if(data){
                    props.editTable.setTableData('Loanmanage', data['Loanmanage']);
                }else{
                    props.editTable.setTableData('Loanmanage', {rows: []});
                }

            }
        })
    }
}

