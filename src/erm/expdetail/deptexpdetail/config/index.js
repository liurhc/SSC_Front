import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { SimpleReport } from 'nc-report'
import {ajax,createPage, getMultiLang} from 'nc-lightapp-front'

export default class Test extends Component {
  constructor (props) {
    super(props)
    this.state = {
      json: {}
    }
  }
  componentWillMount() {
		let callback = (json) => {
		  this.setState({json})
		}
		getMultiLang({moduleId: 2011, currentLocale: 'zh-CN',domainName: 'erm',callback})
	  }	
  disposeSearch (meta, props) {
    // 参照过滤
    let items = meta['light_report'].items;
    let DeptTreeOrgRef = 'nccloud.web.action.erm.ref.sqlbuilder.DeptTreeOrgRef';//组织主管部门
    let OrgTreeManRef = 'nccloud.web.action.erm.ref.sqlbuilder.OrgTreeManRef'//组织

    items.forEach((item) => {
      let filter = item.referFilterAttr;
      if(filter){
          if(filter[item.attrcode] && filter[item.attrcode].length>0)
          {filter[item.attrcode] = filter[item.attrcode].join(",");}
          else{filter[item.attrcode] ='';}
      }else{filter ={};filter[item.attrcode] ='';}
      
      if (item.attrcode == 'assume_org') {
        item.queryCondition = () => {
          let data = props.search.getSearchValByField('light_report', 'pk_group').value.firstvalue;
          return { pk_org: data, TreeRefActionExt:OrgTreeManRef}; 
        }
      }
      if (item.attrcode == 'assume_dept') {
        item.queryCondition = () => {
            let data = props.search.getSearchValByField('light_report', 'assume_org').value.firstvalue;
            return { pk_org: data, TreeRefActionExt:DeptTreeOrgRef }; // 根据pk_org过滤
        };
      }
      if (item.attrcode == 'bx_jkbxr') {
        item.queryCondition = () => {
            let assume_org = props.search.getSearchValByField('light_report', 'assume_org').value.firstvalue;
            let assume_dept = props.search.getSearchValByField('light_report', 'assume_dept').value.firstvalue;
          return {dept: assume_dept,pk_org: assume_org}; 
        };
      }
      let BxTranstypeRefSqlBuilder ='nccloud.web.action.erm.ref.sqlbuilder.BxTranstypeRefSqlBuilder';//交易类型参照过滤
      if (item.attrcode == 'src_tradetype') {
        item.queryCondition = () => {
          return {GridRefActionExt:BxTranstypeRefSqlBuilder}; // 根据assume_org过滤
        };
      }
    })
    return meta; // 处理后的过滤参照返回给查询区模板
  }
  /**
   * 
   * @param  items: 查询区查询数据，如需拓展查询区参数，请返回与items相同格式的查询数据
   */
  expandSearchVal (items) {
    // 变量赋值拓展
    console.log(items)
    if (items.length > 0) {
      items.forEach(item => {
        if (item.field == 'user_name') { // 需要进行拓展的变量
          if (item.value.firstvalue == '11') {
            let obj = {  //obj对象内oprtype为between时firstvalue,secondvalue都有值，其他情况只有firstvalue有值
              field: 'user_other',
              oprtype: 'like',
              value: {firstvalue: '111', secondvalue: '222'}
            }
            items.push(obj)
          }
        }
      })
    }
    return items
  }

 /**
      * searchId: 查询区需要的searchId参数
      * 'vname': 需要附默认值的字段
      * {value: '111'}: 显示值，区间为[]，具体可以对照平台查询区修改
      * 'like': 为oprtype字段值
    */
   setDefaultVal(searchId, props) {
    //查询区默认显示字段值
    //全部
    props.search.setSearchValByField(searchId, 'billState', { value: 'all',display: this.state.json['201111BMMX-0001'] }, '=');
}

/**
  * props: props
  * searchId: 查询区需要的searchId参数
  * field: 编辑后的key
  * val: 编辑后的value
*/
onAfterEvent (props, searchId, field, val) {
  //查询区编辑后事件
  console.log(props)
  console.log(searchId)
  console.log(field)
  console.log(val)
}

/**
  * isRange: 查询后数据区域内点击单元格按钮可操作。如需要像升降序按钮那样，则设置 disabled={isRange}属性
  * data: 表格数据
  * coords: 选中的单元格
*/
CreateNewSearchArea (isRange, data, coords) {
  // button区域业务端自定义的新增按钮
  //return <NCButton disabled={isRange} className="btn" shape="border">新增按钮</NCButton>
}

/**
    * transSaveObject: 业务端处理的transSaveObject参数
    * obj: 点击联查下拉的item信息
    * data: 联查需要的参数
    * props: 平台props
    * url: 平台openTo第一个参数
    * urlParams: 平台openTo第二个参数
    * sessonKey: sessionStorage的key
    */
   setConnectionSearch(transSaveObject, obj, data, props, url, urlParams, sessonKey) {
    sessionStorage.setItem(sessonKey, transSaveObject); //处理好的transSaveObject放到浏览器内存中，必须，跳转报表页面会用到
    if (obj.key == '001'){
      let pkCode = urlParams.reportId;
      let defaultCallBack = (res) => {
        if (res.success) {
          props.openTo(res.data.url, {
            ...res.data,
            status: 'browse'
          });
        }
      };
      ajax({
        url: '/nccloud/erm/expdetail/LinkBillAction.do',
        data: {
          data,
          pkCode
        },
        success: defaultCallBack
      })
    }
    if(obj.key == '002'){
      props.openTo(url, urlParams);
    }
  }

render() {
    return (
        <div className="table">
            <SimpleReport
                showAdvBtn={true}
                setDefaultVal={this.setDefaultVal.bind(this)}
                expandSearchVal={this.expandSearchVal.bind(this)}
                disposeSearch={this.disposeSearch.bind(this)}
                onAfterEvent={this.onAfterEvent.bind(this)}
                CreateNewSearchArea={this.CreateNewSearchArea.bind(this)}
                setConnectionSearch={this.setConnectionSearch.bind(this)}
            />
        </div>
    );
}
}

Test = createPage({
  mutiLangCode: '2011'
})(Test)
ReactDOM.render(<Test />, document.getElementById('app'));
