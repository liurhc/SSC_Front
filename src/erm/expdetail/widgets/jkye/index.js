import React, { Component } from 'react';
import './index.less';
import {ajax, createPage,getMultiLang} from 'nc-lightapp-front'

 class Test1 extends Component {
	constructor(props) {
		super(props);
		this.state = {
			tocommit: '0.00',
			todo: '',
			json: {}
		}
	}
	componentWillMount() {
		let callback = (json) => {
		  this.setState({json})
		}
		getMultiLang({moduleId: 2011, currentLocale: 'zh-CN',domainName: 'erm',callback})
	  }	
	componentDidMount() {
		ajax({
		  url:  '/nccloud/erm/expdetail/LoanBalanceWidgetAction.do',
		  data: {},
		  loading: false,
		  success: (res) => {
			let data = res.data;
			let { tocommit, todo } = this.state;
			let callback= (json) => { 
				if(data.bbye != null){
					if(data.bbye.split(".").length == 1){
						tocommit = (data.bbye+'.00');
					}else if(data.bbye.split(".").length == 2){
						// if(data.bbye.split(".").length == 1){
						// 	document.getElementById('tocommit').innerHTML+=(data.bbye+'0');
						// }
						let bbye = data.bbye.split(".")
						let decimalPart = bbye[1].length == 1 ? (bbye[1] + '0') : bbye[1].substr(0,2)
						let totalPart = `${bbye[0]}.${decimalPart}`
						tocommit = totalPart
					}else{
						tocommit = (data.bbye);
					}
					todo = data.name; 
				}else{
					// "201112GRYE-0001": "人民币",
					todo = json['201112GRYE-0001'];
				}
				this.setState({
					tocommit, todo
				})
			}
			// getMultiLang({moduleId: 2011, location: '/src/erm/public/lang/standard/zh-CN/2011-zh-CN.json',currentLocale: 'zh-CN',callback})			
			getMultiLang({moduleId: 2011, domainName: 'erm',currentLocale: 'zh-CN',callback})			
		}
	  });
	}
	open = (event, para = null) => {
		event.stopPropagation()
		if(para){
			// window.parent.openNew({code:'105602BZPT',name:'报账平台'}, {tab: para});
			this.props.openTo(
				'/erm/expdetail/broloanbalance/config/index.html',
				{
					pagecode: '201112GRYE_Q',
					appcode: '201112LBYE',
                    // "201112GRYE-0002": "借款余额",
					name: this.state.json['201112GRYE-0002'],
					tab: para
				}
			)
		}else{
			// window.parent.openNew({code:'105602BZPT',name:'报账平台'}, null);
			this.props.openTo(
				'/erm/expdetail/broloanbalance/config/index.html',
				{
					pagecode: '201112GRYE_Q',
					appcode: '201112LBYE',
                    // "201112GRYE-0002": "借款余额",
                    name: this.state.json['201112GRYE-0002']
				}
			)
		}
	}
	render() {
		const { tocommit, todo } = this.state;
		return (
            // "201112GRYE-0002": "借款余额",
			<div id="test" class="app1X1 platform-app number" onClick={(e) => {this.open(e, null)}}>
                <div className="title" >{this.state.json['201112GRYE-0002']}</div>
				<div className='app-ye'>
                    <img src="/nccloud/resources/erm/public/image/money.png" />
					<table className='app-ye-table'>
						<tbody >
							<tr className="one">
								<td className="content-one" id="tocommit" style={{textAlign:"center"}}>{tocommit}</td>
							</tr>
							<tr className="one">
								<td className="content-one" id="todo" style={{textAlign:"center"}}>{todo}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		);
	}
}

Test1 = createPage({
    mutiLangCode: '2011'
})(Test1)
export default Test1
// ReactDOM.render(<Test1 />, document.querySelector('#app'));
