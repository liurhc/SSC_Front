import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {connect, Provider} from 'react-redux';
import store from './store/store';


import Matterapp from './Matterapp';

ReactDOM.render((<Provider store={store}><Matterapp /></Provider>)
    , document.querySelector('#app'));