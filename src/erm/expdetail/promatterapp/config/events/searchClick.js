import requestApi from '../requestApi'
import pubMessage from 'ssccommon/utils/pubMessage'

export default function searchClick(props, searchVal) {
    if(searchVal){
        let data={
            querycondition:searchVal,
            pagecode: 'MtappbillList',
            pageInfo:{
                "pageIndex":-1,
                "pageSize":10,
                "totalPage":"0"
            },
            queryAreaCode:'20110MTAMN',  //查询区编码
            oid:'0001Z310000000009R4L',  //查询模板id，手工添加在界面模板json中，放在查询区，后期会修改
            querytype:'tree',
            tradetype:props.getUrlParam("tradetype")
        };
        let senddata = {data,"pagecode":"201113SQYE_Q","tepmid":this.props.meta.getMeta().pageid};
        requestApi.query({
            data:senddata,
            success: (data) => {
                this.pubMessage.querySuccess(data ? data.Mtappbill.rows.length : 0)
                if(data){
                    props.editTable.setTableData('Mtappbill', data['Mtappbill']);
                }else{
                    props.editTable.setTableData('Mtappbill', {rows: []});
                }

            }
        })
    }
}

