import 'ssccommon/components/globalPresetVar';
import requestApi from '../requestApi';


//变量设置规范，如需要
window.presetVar = {
    ...window.presetVar,
    pageId: '',
    head: {
        head1: '',
        head2: ''
    },
    body: {
        body1: 'Mtappbill',
        body2: ''
    },
    search: {}
};

let body = window.presetVar.body;


export default function (props) {

    props.createUIDom(
        {
            pagecode: 'MtappbillList',//页面id
            appid: '0001Z310000000003LP5'//注册按钮的id
        },
        (data) => {
            let meta = data.template;
            let buttonTpl = data.button;

            meta[body.body1].items = meta[body.body1].items.map((item, key) => {
                return item;
            });


            //设置区域模板
            props.meta.setMeta(meta);
            //设置按钮模板
            // props.button.setButtons(buttonTpl);

            //设置按钮行为为弹窗
            // props.button.setPopContent('Delete', '确认要删除该信息吗？')
            /* 设置操作列上删除按钮的弹窗提示 */

            //设置按钮的初始可见性
            // props.button.setButtonsVisible({'Add': true});

            //设置初始数据
            //获取表格的数据
            requestApi.getTableData({
                data: {
                    pk_org: this.state.businessUnitTreeRefValue.refpk,
                    isGroup: false
                },
                success: (data) => {
                    if(data.data != null){
                        if(data.data.pk_org != null){
                            this.setState({
                                businessUnitTreeRefValue: {refpk: data.data.pk_org, refname: data.data.name_org},
                                period:data.data.period
                            })
                            return;
                        }else{
                            this.props.editTable.setTableData(body.body1, data.data[body.body1]);
                            this.props.editTable.setStatus(body.body1, 'edit');

                            if ('back' != this.props.getUrlParam('status')) {
                                // let userJson = JSON.parse(data.data['userjson']);
                                // this.setState({
                                //     businessUnitTreeRefValue: {refpk: userJson.pk_org, refname: userJson.name_org},
                                //     period:userJson.period
                                // })
                            }
                        }
                    }
                }
            })

        }
    )

}


