
import initTemplate from './initTemplate';
import afterEvent from './afterEvent';
import searchClick from './searchClick';

export { initTemplate,afterEvent,searchClick};
