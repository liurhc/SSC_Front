import { toast, output ,ajax,cardCache,base,high,printPreview,printerView} from 'nc-lightapp-front';
const { Refer } = high;
let { NCButton} = base;
const { getDefData } = cardCache;
const areacode = 'print';
function createPrint(_this) {
	let self=this;
	let { form } = self.props;
	let { createForm } = form;
	let ctrltypedata = { 'printsubjlevel': { display: '不汇总', value: '0' } };
		self.props.form.setFormItemsValue('print', ctrltypedata);
		return (
			<div className="nc-theme-form-label-c">
				{createForm(
					'print',
					{
						onAfterEvent: printAfterEvent.bind(_this)
					}
				)}
			</div>
		)
}

function printItem(defaultVal) {
	let json=getDefData('mutilangJson_list', 'gl_voucher.list.mutilangJson');
	let val ={};
	if(defaultVal.length>0){
		 val ={
			value:defaultVal[0].refpk,
			display:defaultVal[0].refname
		}
	}
	return {
		areastatus: 'edit',
		code: 'print',
		moduletype: 'form',
		name: '打印',/* 国际化处理： 打印*/
		status: 'edit',
		items: [
			{
				attrcode: 'printsubjlevel',
				colnum: '1',
				color: '#6E6E77',
				isDataPowerEnable: true,
				itemtype: 'select',
				label: '科目汇总级次',/* 国际化处理： 科目汇总级次*/
				maxlength: '20',
				position: '1',
				visible: true,
				options: [
					{ display: '不汇总', value: '0' },/* 国际化处理： 不汇总*/
					{ display: '1级', value: '1' },/* 国际化处理： 1级*/
					{ display: '2级', value: '2' },/* 国际化处理： 2级*/
					{ display: '3级', value: '3' },/* 国际化处理： 3级*/
					{ display: '4级', value: '4' },/* 国际化处理： 4级*/
					{ display: '5级', value: '5' },/* 国际化处理： 5级*/
					{ display: '6级', value: '6' },/* 国际化处理： 6级*/
					{ display: '7级', value: '7' },/* 国际化处理： 7级*/
					{ display: '8级', value: '8' },/* 国际化处理： 8级*/
					{ display: '按科目设置', value: '9' }/* 国际化处理： 按科目设置*/
				],
				// initialValue: { display: '不汇总', value: '0' }
			},
			{
				attrcode: 'printasslevel',
				colnum: '1',
				color: '#6E6E77',
				isnextrow: true,
				isDataPowerEnable: true,
				itemtype: 'checkbox_switch',
				label: '按辅助项汇总',/* 国际化处理： 按辅助项汇总*/
				maxlength: '20',
				position: '2',
				visible: true
			},
			{
				attrcode: 'printmode',
				colnum: '1',
				color: '#6E6E77',
				isnextrow: true,
				isDataPowerEnable: true,
				itemtype: 'checkbox_switch',
				label: '代账单格式',/* 国际化处理： 代账单格式*/
				maxlength: '20',
				position: '2',
				visible: true
			},
			{
				attrcode: 'printemp',
				colnum: '1',
				color: '#6E6E77',
				isnextrow: true,
				required: true,
				isDataPowerEnable: true,
				itemtype: 'refer',
				label: '选择凭证模板',/* 国际化处理： 选择模板*/
				maxlength: '20',
				position: '1',
				visible: true,
				val:val?val:{},
				refcode: 'sscrp/refer/rpbill/PrintTemplate/index.js',
				queryCondition: () => {
					return {
						appcode: '20020PREPA'
					};
				}
			}
		]
	};
}


function getDefault(){ //取凭证打印模版
	return new Promise((reslove,reject)=>{
	 ajax({
		 url: '/nccloud/gl/glpub/queryprinttemp.do',
		 data: {"pageInfo":{"pageSize":50,"pageIndex":0},"pid":"","keyword":"","defineItems":[],"queryCondition":{"isShowUnit":false,"isDataPowerEnable":true,"appcode":"20020PREPA"}},
		 success: (data) => {
			 reslove(data)
		 },
		 erorr:(err)=>{
			 reject(err)
		 }
	 })
	}) 
 }

function printAfterEvent(props,tableid,key,refValue){
	switch (key){
		case 'printemp':
			if(refValue&&refValue.refpk&&refValue.refpk!=''){
				if(refValue.refcode=='20020PREPA20_nccloud'){//平行记账模版
					this.isParallelPrint=true;
					break;
				}
			}
			this.isParallelPrint=false;
			break;
	}
}
/**
 * 
 * @param {*} url 请求链接
 * @param {*} enctype content-type
 * @param {*} data 入参Array
 * 批量打印可混合凭证和单据打印
 */
function printAll(url,enctype,data){
	
	enctype = ((type) => {
		switch (type) {
			case 1:
				return 'multipart/form-data';
			case 2:
				return 'application/x-www-form-urlencoded';
			case 3:
				return 'text/plain';
			default:
				return 'multipart/form-data';
		}
	})(enctype)
	const [attrs, el_form] = [
		{
			target: '_blank',
			method: 'POST',
			enctype,
			type: 'hidden',
			action: url || ''
		},
		document.createElement('form')
	];
	Object.assign(el_form, attrs);
	   var app = '', appcode = '';
	   let appN = window.parent.location.hash.split("?");
	   if (appN && appN[1]) {
		   let appPrams = appN[1].split('&');
		   if (appPrams && appPrams instanceof Array) {
			   appPrams.forEach((item) => {
				   if (item.indexOf('=') != -1 && item.split('=') && item.split('=') instanceof Array) {
					   if (item.split('=')[0] === 'n') {
						   if (item.split('=')[1]) {
							   app = decodeURIComponent(decodeURIComponent(item.split('=')[1]));
						   }
					   }
					   if (item.split('=')[0] === 'c') {
						   if (item.split('=')[1]) {
							   appcode = decodeURIComponent(decodeURIComponent(item.split('=')[1]));
						   }
					   }
				   }
			   });
		   }
	   }
	   let busiaction = `${app || null}-${window.actionName || null}`;
	   const sysParamJson = {
		   sys_busiaction: busiaction,
		   sys_appcode: appcode,
		   sys_ts: new Date().getTime()
		   // busiaction: busiaction ,
		   // appcode: appcode,
		   // ts: new Date().getTime()
	   }
	   
	   const sysParam = document.createElement('input');
	   const myParam =document.createElement('input');
	   myParam.type = 'hidden';
	   myParam.name='busiParamJson';
	   myParam.value = JSON.stringify(data);
	   sysParam.type = 'hidden';
	   sysParam.name='sysParamJson';
	   sysParam.value=JSON.stringify(sysParamJson);
	   el_form.appendChild(sysParam);
	   el_form.appendChild(myParam);
	   document.body.appendChild(el_form);
	   el_form.submit();
	   document.body.removeChild(el_form);
}

function printButton(_this,props,selectIndex,voucher){
    let json=getDefData('mutilangJson_list', 'gl_voucher.list.mutilangJson');
	return (
		<div>
			{/* <NCButton shape="border" colors="primary" onClick={ confirmPrint.bind(_this,props,selectIndex,voucher,false,isPrintAll) }>预览</NCButton> */}
			{/* <NCButton shape="border" colors="primary" onClick={ confirmPrint.bind(_this,props,selectIndex,voucher,true,true) }>一键打印</NCButton> */}
			<NCButton shape="border" colors="primary" onClick={ confirmPrint.bind(_this,props,selectIndex,voucher,true,false) }>打印</NCButton>
			<NCButton shape="border" colors="primary" onClick={ closePrint.bind(_this)}>取消</NCButton>
		</div>
    )
}

function closePrint() {
	this.props.modal.close('print');
}

function confirmPrint(props,selectIndex,voucher,isPrint=false) {
	let printmode = props.form.getFormItemsValue(areacode, 'printmode').value;
	if (printmode && selectIndex == '-1') {
		toast({ content: '请选择需要打印的分录！', color: 'warning' });/* 国际化处理： 请选择需要打印的分录！*/
		return;
	}
	let selectVoucher=[]
	
	let printemp = props.form.getFormItemsValue(areacode, 'printemp').value;
	if (printemp == '') {
		toast({ content: '请选择模板！', color: 'warning' });/* 国际化处理： 请选择模板！*/
		return;
	}
	let _this=this;
	if(this.state.printAll===true){//一键打印
		let printsubjlevel = props.form.getFormItemsValue(areacode, 'printsubjlevel').value;
		let printasslevel = props.form.getFormItemsValue(areacode, 'printasslevel').value;
		let printdata = {
			printsubjlevel: printsubjlevel, //printsubjlevel：科目汇总到第几级，不汇总是0
			printasslevel: printasslevel ? '-1' : '0', //printasslevel：按辅助项汇总 勾选-1 不勾选0
			printmode: printmode ? '1' : '0', //printmode：代账单格式 0不勾选 1勾选
			selectIndex: selectIndex //selectIndex: 选中第几行分录 从0开始
		};
		this.allMessage.forEach(item=>{
			item.vouchertemplate = printemp,
			item.userjson=printdata
		   })
		printAll('/nccloud/sscrp/bookbinding/onekeyAction.do',2,this.allMessage);
	}else{
		if(voucher){
			if(voucher==''){
				toast({ content: '该凭证不能打印！', color: 'warning' });/* 国际化处理： 该凭证不能打印！*/
				return;
			}
			selectVoucher.push(voucher);
		}else{
			selectVoucher = props.ViewModel.getData('selectVoucher');
			if (this.voucher == 0) {
				toast({ content: '请选择有凭证的单据打印凭证！', color: 'warning' });/* 国际化处理： 请选择需要打印的凭证！*/
				return;
			}
		}
		//校验单据号断号
	ajax({
		url: '/nccloud/gl/voucher/checkprint.do',
		data: this.voucher,
		success: (res) => {
			let {data}=res;
			if(data){
				if(data.failure){
					if(selectVoucher.length==1){
						toast({ content: '本类凭证存在断号，取消打印', color: 'warning' });/* 国际化处理： #本类凭证存在断号，取消打印#*/
						return;
					}else{
						props.modal.show('batchhint');
						let rows=data.failure.gl_voucher.rows;
						let errMessageArr = [];
						rows.forEach((val) => {
								let err = val.values;
								err.result = { display: '失败', value: '失败' };/* 国际化处理： 失败,失败*/
								err.reason = {
									display: '本类凭证存在断号，取消打印',/* 国际化处理： #本类凭证存在断号，取消打印#*/
									value: '本类凭证存在断号，取消打印',/* 国际化处理： #本类凭证存在断号，取消打印#*/
								};
								errMessageArr.push({ values: err });
							
						});
						props.table.setAllTableData('batchhint', { rows: errMessageArr });
					}
				}

				if (data.success && data.success.length > 0) {
					let printsubjlevel = props.form.getFormItemsValue(areacode, 'printsubjlevel').value;
					let printasslevel = props.form.getFormItemsValue(areacode, 'printasslevel').value;
					let printdata = {
						printsubjlevel: printsubjlevel, //printsubjlevel：科目汇总到第几级，不汇总是0
						printasslevel: printasslevel ? '-1' : '0', //printasslevel：按辅助项汇总 勾选-1 不勾选0
						printmode: printmode ? '1' : '0', //printmode：代账单格式 0不勾选 1勾选
						selectIndex: selectIndex //selectIndex: 选中第几行分录 从0开始
					};

					// print(
					// 	'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
					// 	'/nccloud/gl/voucher/printvoucher.do', //后台服务url
					// 	{
					// 		billtype: 'C0', //单据类型
					// 		appcode: '20020PREPA', //功能节点编码，即模板编码
					// 		//nodekey: printemp,     //模板节点标识
					// 		printTemplateID: printemp,
					// 		oids: data.success, // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
					// 		userjson: JSON.stringify(printdata)
					// 	},
					// 	false
					// );

					if (isPrint) {
						//打印并预览
						printerView(
							props,
							'/nccloud/gl/voucher/printNoPreview.do', //后台服务url
							{
								billtype: 'C0', //单据类型
								appcode: '20020PREPA', //功能节点编码，即模板编码
								nodekey: '',     //模板节点标识
								printTemplateID: printemp,
								oids: data.success, // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
								userjson: JSON.stringify(printdata),
								type:'1',
								// ip:'10.11.117.15',
								// port:'80',
								//socketIp:'10.11.115.124',
								//download:'preview',
								//socketIp:'10.11.115.147',
								realData:"true"
							},
							false
						);
						// //专岗打印
						// printOnClient(
						// 	props,
						// 	'/nccloud/gl/voucher/printNoPreview.do', //后台服务url
						// 	{
						// 		billtype: 'C0', //单据类型
						// 		appcode: '20020PREPA', //功能节点编码，即模板编码
						// 		//nodekey: printemp,     //模板节点标识
						// 		printTemplateID: printemp,
						// 		oids: data.success, // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
						// 		userjson: JSON.stringify(printdata),
						// 		type: '1',
						// 		// ip: '10.11.117.58',
						// 		// port: '80',
						// 		// socketIp: '10.11.115.147'
						// 	},
						// 	false
						// );
					} else {
						//预览
						printPreview(
							props,
							'/nccloud/gl/voucher/printNoPreview.do', //后台服务url
							{
								billtype: 'C0', //单据类型
								appcode: '20020PREPA', //功能节点编码，即模板编码
								nodekey: '',     //模板节点标识
								printTemplateID: printemp,
								oids: data.success, // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
								userjson: JSON.stringify(printdata),
								type:'1',
								// ip:'10.11.117.15',
								// port:'80',
								//socketIp:'10.11.115.124',
								//download:'preview',
								//socketIp:'10.11.115.147',
								realData:"true"
							},
							false
						);
					}
				}
			}
		}
	});
	}
	props.modal.close('print');
}

export {
    createPrint,
    printButton,
	printItem,
	printAll,
	getDefault
}