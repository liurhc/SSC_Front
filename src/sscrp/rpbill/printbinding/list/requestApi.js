import { ajax } from "nc-lightapp-front";

let requestDomain = "";

let requestApiOverwrite = {
  //根据单号精准查询
  billQuery:opt=>{
    ajax({
      url: "/nccloud/sscrp/bookbinding/querybill.do",
      data: opt.data,
      success: opt.success,
      error:opt.error
    });
  },
  // 打印
  printAction: (opt) => {
    ajax({
        url: '/nccloud/sscrp/rpbill/PrintAction.do',
        data: opt.data,
        success: (data) => {
            opt.success(data)
        }
    })
},
// 查看单据
openBill: opt => {
  ajax({
    url: "/nccloud/sscrp/rpbill/BrowseBillAction.do",
    data: opt.data,
    success: data => {
      opt.success(data);
    }
  });
},
getFile: opt => {// 获取附件
  ajax({
    url: "/nccloud/baseapp/times/timesfileurlService.do",
    data: opt.data,
    success: data => {
      opt.success(data);
    }
  });
},
downloadFile: opt => {// 批量下载附件
  ajax({
    url: "/service/BillfileDownloadService",
    method:"get",
    params:opt.data,
    success: data => {
      opt.success(data);
    }
  });
},
queryFile: opt=>{
  ajax({
    url: "/nccloud/sscrp/bookbinding/BillfileVerify.do",
    method:"post",
    data:opt.data,
    success: data => {
      opt.success(data);
    }
  })
},
bulkPrint:(opt)=>{// 批量打印凭证
  ajax({
    url: '/nccloud/sscrp/bookbinding/queryPrintMessage.do',
    data: opt.data,
    success: (data) => {
        opt.success(data)
    }
})
}
};

export default requestApiOverwrite;
