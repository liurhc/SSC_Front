import { Component } from "react";
import { createPage, base, toast,print } from "nc-lightapp-front";
const { NCSelect,NCButton,NCRadio,NCTable,NCCheckbox,NCPagination,NCPopover} = base;
const {NCOption} = NCSelect;
// import WorkbenchDetail from "ssccommon/components/workbench-detail";
import api from './requestApi';
import pubMessage from 'ssccommon/utils/pubMessage';
import { initTemplate, listButtonClick, query, list,abc} from "./events/index";
import presetVar from "./presetVar";
import requestApi from "./requestApi";
import {createPrint,printButton,printAll} from './print';
import multiSelect from './multiSelect';
import "./index.less";
import {
    ProfileStyle,
    ProfileHead,
    ProfileBody,
    HeadCenterCustom,
    EmptyArea 
} from 'ssccommon/components/profile';

const MultiSelectTable = multiSelect(NCTable, NCCheckbox);
const HoverDiv =(props)=>{
  const {children}=props;
  return(
    <NCPopover content={<div>{children}</div>} trigger="hover" placement="top">
      <div className='hover-wrapper'>{children}</div>
    </NCPopover>
  )
}

class PrintBinding extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showType: "list",
      fuzzyquerychild: [],
      billVal:[],
      selectedValue: false,
      menuShowType: {
        type: "icon-Group",
        title: "缩略显示"
        // "105602BZCX-0008":"缩略显示"
        //title:multiLang && multiLang.get('105602BZCX-0008')
      },
      filesList:[],//附件
      openkey:[],
      tableData:[],//表格数据
      // allPages:0,// 总页数
      activePage:1,//页码
      // totle:0,// 总条数
      checkedArray:[],// 每行对应的布尔值
      checkedAll:false,//是否全选
      selectData:[],//选中的数据
      printAll:false, // 是否一键打印
      // pageSize:10,
      // 页信息
      pageinfo: {
        number: 1,
        size: 10,
        totalElements: 0,
        totalPages: 0
      },
      param:{}
    };
    this.firstSearch=true;
    this.pubMessage = new pubMessage(props);
    // 查询条件
    this.queryKey = {
      searchtype:0,//单据编号搜索用0，条件搜索为1
      // 页信息
      pageinfo: {
        number: 1,
        size: 10,
        totalElements: 0,
        totalPages: 0
      },
    };
    // 缓存查询结果数据
    this.data = {
      listRows: [],
      cardRows: []
    };
    // 延时控制标记
    // 翻页
    this.canChangPageFlag = true;
    // 模糊查询
    this.fuzzyKey = "";
    // 页面初始化
    initTemplate.call(this, props);
  }
  componentDidMount() {
    // let a=[];
    // for(let i=0;i<3;i++){
    //    a[i] = window.open('/service/BillfileDownloadService?billno=264X20191012121&pk_bill=112XP554A')
    //   a[i].close();
    // }
    
 
    
    // api.downloadFile({
    //   data: {"pk_bill":"0001A3100000000L8ZEU","billno":"264X2017041801452"},
    //   success: data => {
    //     debugger
    //   },
    //   error:(res, url)=>{
    //     console.error(res);
    // let msgContent = JSON.stringify(res.message);
    // toast({ color: 'danger', content: msgContent, container: toastContainer, mark: msgContent + url });
    //   }
    // });
    // Firefox
    // window.addEventListener("DOMMouseScroll", ev => {
    //   ev = ev || window.event;
    //   let onOff = null;
    //   if (ev.detail < 0) {
    //     onOff = true;
    //   } else {
    //     onOff = false;
    //   }
    //   let t = document.documentElement.scrollTop || document.body.scrollTop;
    //   let h =
    //     document.documentElement.scrollHeight || document.body.scrollHeight;
    //   let bh =
    //     document.documentElement.clientHeight || document.body.clientHeight;
    //   if (
    //     !onOff &&
    //     h - bh - t < 30 &&
    //     Number(this.queryKey.pageinfo.totalPages) > Number(this.queryKey.pageinfo.number) &&
    //     this.canChangPageFlag
    //   ) {
    //     this.canChangPageFlag = false;
    //     setTimeout(() => {
    //       query.queryData.call(this, true);
    //     }, 300);
    //   }
    // });
    // // 其他浏览器
    // window.onmousewheel = ev => {
    //   ev = ev || window.event;
    //   let onOff = null;
    //   if (ev.wheelDelta > 0) {
    //     onOff = true;
    //   } else {
    //     onOff = false;
    //   }
    //   let t = document.documentElement.scrollTop || document.body.scrollTop;
    //   let h =
    //     document.documentElement.scrollHeight || document.body.scrollHeight;
    //   let bh =
    //     document.documentElement.clientHeight || document.body.clientHeight;
    //   if (
    //     !onOff &&
    //     h - bh - t < 30 &&
    //     Number(this.queryKey.pageinfo.totalPages) > Number(this.queryKey.pageinfo.number) &&
    //     this.canChangPageFlag
    //   ) {
    //     this.canChangPageFlag = false;
    //     setTimeout(() => {
    //       query.queryData.call(this, true);
    //     }, 300);
    //   }
    // };
  }
  componentWillUnmount(){
    window.removeEventListener("DOMMouseScroll", ev => {})
  }
  componentDidUpdate(nextProps,nexState){
    const {selectData} = this.state;
    const { setButtonDisabled } = this.props.button;
    if(selectData.length>0){
      setButtonDisabled({
        print_bill:false,
        print_vouchers:false,
        download_all:false,
        print_all:false
      })
     }
    //  else if(nexState.selectedValue!==this.state.selectedValue){
    //    return ;
    //   } else {
    //     setButtonDisabled({
    //       print_bill:true,
    //       print_vouchers:true,
    //       download_all:true,
    //       print_all:true
    //     })
    //   }
    }
  renderColumns=()=>{
    this.columns=[
      // {
      //   title: (<div>
      //     <NCCheckbox
      //   className="table-checkbox"
      //   checked={this.state.checkedAll}
      //   onChange={this.onAllCheckChange}
      //   /><span>序号</span>
      //   </div>),
      //   key: 'checkbox',
      //   dataIndex: 'checkbox',
      //   width: 80,
      //   fixed: "left",
      //   render: (text, record, index) => {
      //     return (
      //       <div>
      //         <NCCheckbox
      //         checked={this.state.checkedArray[index]}
      //         onChange={this.onCheckboxChange.bind(text, record, index)}
      //       />
      //       <span>{this.state.pageinfo.size*(this.state.pageinfo.number-1)+index+1}</span>
      //       </div>
            
      //     );
      //   }
      // },
      { title: '单据编码', dataIndex: 'values.billno.value', key: 'billno', width: 150,render:(v,record,index)=>{
      return(<a className='jump' onClick={listButtonClick.bind(this,"OpenBill", record, index)}><HoverDiv>{v}</HoverDiv></a>)
      } },
      { id: '123', title: '报账单位', dataIndex: 'values.pk_org.display', key: 'pk_org', width: 100,render:(val,record,i)=>{
      return(<HoverDiv>{val}</HoverDiv>)
      } },
      { title: '交易类型', dataIndex: 'values.pk_tradetype.display', key: 'pk_tradetype', width: 100 ,render:(val)=>{
        return(<HoverDiv>{val}</HoverDiv>)
      }},
      { title: '单据日期', dataIndex: 'values.billdate.value', key: 'billdate',width:100,render:(val)=>{
        return(<HoverDiv>{val}</HoverDiv>)
      } },
      { title: '金额', dataIndex: 'values.amount.value', key: 'amount',width:120 },
      { title: '凭证号', dataIndex: 'values.voucher_num.value', key: 'voucher_num',width:80,render:(val,record,index)=>{
        return(<a className='jump' onClick={this.dbClick.bind(this,record,index)}><HoverDiv>{val}</HoverDiv></a>)
      } },
      { title: '凭证日期', dataIndex: 'values.voucher_date.value', key: 'voucher_date',width:100 },
      { title: '凭证金额', dataIndex: 'values.voucher_money.value', key: 'voucher_money',width:120 },
      { title: '报账人', dataIndex: 'values.billmaker.display', key: 'billmaker',width:80,render:(val)=>{
        return(<HoverDiv>{val}</HoverDiv>)
      }},
      { title: '事由', dataIndex: 'values.remark.value', key: 'remark',width:100,render:(val)=>{
      return(<HoverDiv>{val}</HoverDiv>)
      } },
      { title:'操作', dataIndex:'options',key:'op',render:(val,record)=>{
        return(<div>
          <NCButton className='print' onClick={this.onButtonClick.bind(this,record,'dydj')} colors="secondary">打印单据</NCButton>
          {record.values&&record.values.voucherid?<NCButton className='print' onClick={this.onButtonClick.bind(this,record,'dypz')} colors="secondary">打印凭证</NCButton>:null}
          </div>)
      }
    },
    { title:'展开附件', dataIndex:'expand',key:'expand',width:80,render:(val,record,i)=>{}}
    ]
    return this.columns;
  }
//   onAllCheckChange=(allval)=>{//全选
// //  debugger
//  const {tableData} = this.state;
//   let arr=[];
//   for(let i = 0;i<tableData.length;i++){
//     if(allval===true){
//       arr[i]=true;
//     }else{
//       arr[i]=false;
//     }
//   }
//   if(allval===true){
//     this.setState({
//       checkedAll:allval,
//       checkedArray:arr,
//       selectData:tableData.slice(0)
//     })
//   }else{
//     this.setState({
//       checkedAll:allval,
//       checkedArray:arr,
//       selectData:[]
//     })
//   }
  
// }

  checkHandleChange=(selectedList,record,index)=>{//selectedList选中的数据
    // debugger
    this.setState({
      selectData:selectedList
    })
  }

  // onCheckboxChange=(currentRow,i,val)=>{
  //   // debugger
  //   const {checkedArray,selectData}=this.state;
  //   let c=[];
  //   const notAllSelect = checkedArray.some(item=>item===false)
  //   if(notAllSelect===true){// 找到未选中的取消全选
  //     this.setState({
  //       checkedAll:false
  //     })
  //   }
  //   if(val===true){//增
  //     this.state.selectData.splice(i,1,currentRow);
  //     checkedArray[i]=true;
  //     this.setState({
  //       selectData:this.state.selectData,
  //       checkedArray
  //     })
  //   }else{//删
  //     // this.a.splice(i,1,'');
  //     checkedArray[i]=false;
  //     checkedArray.map((v,index)=>{
  //       if(v===true){
  //         c.push(data[index]);
  //       }
  //     })
  //     this.setState({
  //       selectData:c,
  //       checkedArray
  //     })
  //   }
    
  // }

  handleSelect=(eventKey)=> {//翻页
    // debugger
    this.pageChange(eventKey);
    // this.setState({
    //   activePage: eventKey,
    //   // tableData:[]
    // });
}

  menuClick = () => {
    // "105602BZCX-0008":"缩略显示",
    // "105602BZCX-0009":"列表显示"
    let multiLang = this.props.MutiInit.getIntl(1056);  
    if (this.state.menuShowType.type == "icon-xingzhuang") {
      this.setState({
        menuShowType: {
          type: "icon-Group",
          title: multiLang && multiLang.get("105602BZCX-0008")
        },
        showType: "list"
      });
    } else {
      this.setState({
        menuShowType: {
          type: "icon-xingzhuang",
          title: multiLang && multiLang.get("105602BZCX-0009")
        },
        showType: "card"
      });
    }
  };
  // 模糊查询输入内容变化
  onFuzzyChange(data) {
    this.fuzzyKey = data;
    let newQueryKey = {};
    let multiLang = this.props.MutiInit.getIntl(1056);
    for (let child in this.queryKey) {
      newQueryKey[child] = this.queryKey[child];
    }
    newQueryKey.fuzzyQueryKey = data;
    setTimeout(
      (that, data) => {
        if (data == that.fuzzyKey) {
          requestApi.queryFuzzyKey({
            data: newQueryKey,
            success: data => {
              for (let childName in data.data) {
                data.data[childName].value =
                  data.data[childName].value + "=" + that.fuzzyKey;
                //105602BZCX-0049:包含
                data.data[childName].key =
                  data.data[childName].key + (multiLang && multiLang.get("105602BZCX-0049")) + that.fuzzyKey;
              }
              that.setState({ fuzzyquerychild: data.data });
            }
          });
        }
      },
      500,
      this,
      data
    );
  }
 
  
  pageChange(number){
    const { param } = this.state;
    // if(number){//翻页
    //   // this.queryKey.pageinfo.number=number;
    //   // this.queryKey.pageinfo.totalPages=this.state.allPages;
    //   // this.queryKey.pageinfo.totalElements=this.state.totle;
    //   this.setState({
    //     pageinfo:{ 
    //       ...pageinfo,
    //       number 
    //     }
    //   })
    // }
    this.queryMessage(number,param);

  }

  searchClick(props, data, type,queryinfo){//点击搜索按钮
    const {conditions}=data;
    const param={};
    if(conditions.length>0){//有查询条件
      conditions.map(item=>{
        if(item.field==="billdate"||item.field==="voucher_date"||item.field==='voucher_money'){//单据日期和凭证日期格式约定
          param[item.field]=`${item.value.firstvalue},${item.value.secondvalue}`;
        }else{
          param[item.field]=item.value.firstvalue;
        }
        
      })
      this.queryKey.searchtype=1;
      this.firstSearch=true;
      this.queryMessage(1,param);
      // this.queryKey.param = param;
      // this.setState({
      //   activePage:1,
      //   tableData:[],
      //   param,
      //   checkedArray:[true]
      // })
      // this.pageChange(param);
      // query.queryData.call(this,'dj');
    }
    // else if(this.queryKey.billnos.length>0){// 有单据编码清空查询
    //   this.queryKey.searchtype=0;
    //   delete this.queryKey.param;
      
    //   this.queryKey.pageinfo= {
    //     number: 1,
    //     size: 50,
    //     totalElements: 0,
    //     totalPages: 0
    //   }
    //   query.queryData.call(this);
    // }
    
    
  }

 
  // 复制列表数据
  copyData(data) {
    let newData = {};
    newData.values = {};
    for (let child in data.values) {
      newData.values[child] = {
        display: data.values[child].display,
        value: data.values[child].value,
        scale: data.values[child].scale
      };
    }
    return newData;
  }
  // 双击列表行切换到卡片态浏览
  // onRowDoubleClick = (values, idx, props, events) => {
  //   requestApi.openBill({
  //     data: {
  //       billtypeCode: values.billtypecode.value,
  //       transtypeCode: values.transtypecode.value,
  //       billid: values.busiid.value
  //     },
  //     success: data => {
  //       this.props.specialOpenTo(data.data.url, {
  //         ...data.data.data,
  //         scene: "bzcx"
  //       });
  //     }
  //   });
  // };

  // 处理排序后的模板变化
  handleTempletAfterSort (sortParam,tempId){
    if(sortParam[0].order=='flatscend'){
        this.queryKey.orderByInfo = [];
    }else {
        this.queryKey.orderByInfo = sortParam;
    }
    let sortObj = {};
    sortParam.forEach(item => {
        sortObj[item.field] = item;
    });
    let meta = this.props.meta.getMeta()
    meta[window.presetVar[tempId]].items.forEach(item => {
        //保存返回的column状态，没有则终止order状态
        if(sortObj[item.attrcode]){
            item.order = sortObj[item.attrcode].order;
            item.orderNum = sortObj[item.attrcode].orderNum;
        }else {
            item.order = "flatscend";
            item.orderNum = "";
        }
    })
    this.props.meta.setMeta(meta);
  }

  onButtonClick=(record,id)=>{
    // debugger;
    // const {table}=this.props;
    // const {getCheckedRows}=table;
    // const selectedData = getCheckedRows(presetVar.list);
    const {selectData}=this.state;
    this.pks=[];
    let dataArr=[];
    let b;
    // debugger;
    // const a=[],b=[],c=[];
    if(selectData.length>0){
      this.voucher=[];
      this.allMessage = [],
      selectData.forEach((item)=>{
        if(item.values.voucherid){
          this.voucher.push(item.values.voucherid.value);
        }
        this.pks.push(item.values.busiid.value);
        dataArr.push({
          "billtypeCode":item.values.billtypecode.value,
          "transtypeCode":item.values.pk_tradetype.value,
          "billid":item.values.busiid.value,
          "billno":item.values.billno.value,
          "m_ctemplateid":item.values.m_ctemplateid?item.values.m_ctemplateid.value:''
        })
        this.allMessage.push({
          "billtypeCode":item.values.billtypecode.value,
          "transtypeCode":item.values.pk_tradetype.value,
          "billid":item.values.busiid.value,
          "billno":item.values.billno.value,
          "m_ctemplateid":item.values.m_ctemplateid?item.values.m_ctemplateid.value:'',
          "voucherno":item.values.voucherid?item.values.voucherid.value:'',
        })
      })
    }else{
      this.voucher=[];
    }
    if(id==="print_bill"){ //批量打印单据
      // debugger
      if(dataArr<=0){
        toast({ content: '请选择要打印的单据', color: 'warning' });
        return;
      }
      requestApi.bulkPrint({
        data:dataArr,
        success: res => {
          const faultBillnos = res.data&&res.data.faultBillno;
          let hasNoMould='';
          if(res.data.message==='true'){
            printAll('/nccloud/sscrp/bookbinding/onekeyAction.do',2,dataArr);
          }else{//失败
            toast({ content: `打印失败`, color: 'danger' });
          }
          if(faultBillnos&&faultBillnos.length>0){
            hasNoMould = faultBillnos.join(',');
            toast({ content: `单据${hasNoMould}没有模版`, color: 'danger' });
          }
        }
      });
    }else if(id==='print_vouchers'){ //批量打印凭证
      if(this.voucher.length>0){
        this.setState({
        printAll:false
       });
        this.props.modal.show('print');
      }else{
        toast({ content: '请选择有凭证的单据打印凭证！', color: 'warning' });
      }
    }else if(id==='download_all'){// 一键下载
      let urls=[],fileParam = [];
     if( dataArr.length>0){
      fileParam = dataArr.map(item=>{
        return {
          billno:item.billno,
          pk_bill:item.billid
        }
      })
      requestApi.queryFile({
        data: fileParam,
        success:data=>{
          data.data.forEach((item,i)=>{
            urls[i] = window.open(`/service/BillfileDownloadService?billno=${item.billno}&pk_bill=${item.pk_bill}`);
            // urls[i].close();
          })
        }
      })
     }
    }else if(id==='print_all'){//一键打印
     if(this.allMessage.length>0){
       this.setState({
        printAll:true
       });
      this.props.modal.show('print');
     }
    }
    if(id==="dydj"){ //打印单据
          const {billtypecode,pk_tradetype,busiid}=record.values;
      requestApi.printAction({
        data: {"billtypeCode":billtypecode.value,"transtypeCode":pk_tradetype.value,"billid":busiid.value},
        success: data => {
          print(
            data.data.filetype,
            data.data.url, 
            {
                billtype: data.data.paradata.billtype,
                funcode: data.data.paradata.funcode,
                appcode: data.data.paradata.appcode?data.data.paradata.appcode:data.data.paradata.funcode,
                nodekey: data.data.paradata.nodekey,
                userjson:JSON.stringify({"transtype":data.data.paradata.transtype}),
                oids: data.data.paradata.oids    // 功能节点的数据主键
                //printTemplateID: data.data.paradata.printTemplateID
            }
        ) 
         
        }
      });
    }else if(id==='dypz'){//打印凭证
      if(record.values.voucherid){
        this.voucher=[record.values.voucherid.value];
        this.props.modal.show('print');
      }else{
        toast({ content: '该单据没有凭证', color: 'warning' });
      }
    }
    // debugger;
  }

  handleChange=(value)=>{//是否批量
    this.setState({
      selectedValue: value
    });
  }


  //列表双击表体
	dbClick = ( record, index) => {
    this.props.openTo('/nccloud/resources/gl/gl_voucher/pages/main/index.html#/Welcome?', {                             
      appcode: "20021QUERY",
      id: record.values.voucherid.value,
      ifshowQuery: false,
      n:null ,
      pagecode: "20021005card",
      status: "browse",
      step: false,
      type: "query",
        })
	};

  onSearchAfterEvent=(field,val)=>{//条件查询完处理
   if(field){
     this.setState({
      // billVal:[],
      tableData:[]
     })
   }
  // delete this.queryKey.billnos;
  }

  deletSelect=(value,type)=>{// 删除搜索条
    // debugger
   const dIndex = this.state.billVal.findIndex(v=>v===value)
   this.state.billVal.splice(dIndex,1);
    if(value){
      this.setState({
        billVal:this.state.billVal,
      },()=>{
        if(this.state.billVal.length==0){
          delete this.queryKey.billnos;
        }
      })
    }
  }


  queryMessage=(n=1,params)=>{// 请求查询接口默认n为1
    const { setButtonDisabled } = this.props.button;
    const { pageinfo,param } = this.state;
    // debugger
    const newParam ={...param,...params}
    if(this.state.selectedValue===true){//批量查询，做去重
      let obj={},tab=[];
    
      api.billQuery({
        data: {pageinfo:{...pageinfo,number:n},billnos:this.queryKey.billnos?[...this.queryKey.billnos]:undefined,param:newParam,orderByInfo:this.queryKey.orderByInfo},
        success: data => {
          if(data.data.result&&data.data.result.length>1||data.data.result.length==1&&this.firstSearch==false){
            this.state.tableData = [...this.state.tableData,...data.data.result];
            toast({ content: `查到${data.data.pageinfo.totalElements}条数据`, color: 'success' });
          this.state.tableData.forEach((item,index,arr)=>{//去重
            // debugger
            if(!obj[item.values.billno.value+(item.values.voucherid?item.values.voucherid.value:'-')]){
              tab.push(item);
              obj[item.values.billno.value+(item.values.voucherid?item.values.voucherid.value:'-')]=true;
            }
          })
          tab.forEach((item,index)=>{
            item.key=index+1
          })
          this.setState({
            tableData:tab,
            pageinfo:{
              ...this.state.pageinfo,
              totalPages:Number(data.data.pageinfo.totalPages),
              totalElements:Number(data.data.pageinfo.totalElements),
            },
            param:newParam,
            // selectData:[],//选中的数据
          })
          }else if(data.data.result&&data.data.result.length==1&&this.firstSearch==true){
            // debugger
            this.firstSearch = false;
            data.data.result[0]._checked=true;
            this.state.tableData = [...this.state.tableData,...data.data.result];
            this.setState({
              tableData:data.data.result,
              pageinfo:{
                ...this.state.pageinfo,
                totalPages:Number(data.data.pageinfo.totalPages),
                totalElements:Number(data.data.pageinfo.totalElements),
              },
               param:newParam,
              // selectData:data.data.result,//选中的数据
            })
            setButtonDisabled({
              print_bill:false,
              print_vouchers:false,
              download_all:false,
              print_all:false
            })
          }else{
            toast({ content: '未查询到相关单据', color: 'warning' });
            this.setState({//查询失败也要保存查询条件
              param: newParam
            })
          }
          
        
        }});
    }else{//非批量查询暂不去重
      api.billQuery({
        data: {pageinfo:{...pageinfo,number:n},billnos:this.queryKey.billnos?[...this.queryKey.billnos]:undefined,param:newParam,orderByInfo:this.queryKey.orderByInfo},
        success: data => {
          if(data.data.result&&data.data.result.length>1){
            data.data.result.forEach((item,index)=>{
              item.key=index+1
            })
            toast({ content: `查到${data.data.pageinfo.totalElements}条数据`, color: 'success' });
            this.setState({
              tableData:data.data.result,
              pageinfo:{
                ...this.state.pageinfo,
                totalPages:Number(data.data.pageinfo.totalPages),
                totalElements:Number(data.data.pageinfo.totalElements),
                number:Number(data.data.pageinfo.number),
              },
              param:newParam,
              // selectData:[],//选中的数据
            })
          }else if(data.data.result&&data.data.result.length==1){
           
            data.data.result[0]._checked=true;
            
            this.setState({
              tableData:data.data.result,
              pageinfo:{
                ...this.state.pageinfo,
                totalPages:Number(data.data.pageinfo.totalPages),
                totalElements:Number(data.data.pageinfo.totalElements),
              },
               param:newParam,
              // selectData:data.data.result,//选中的数据
            })
            setButtonDisabled({
              print_bill:false,
              print_vouchers:false,
              download_all:false,
              print_all:false
            })
          }else{
            this.setState({//查询失败也要保存查询条件
              param: newParam
            })
            toast({ content: '未查询到相关单据', color: 'warning' });
          }
          
        
        },
      error:error=>{
        console.error(error);
        toast({ content: '查询失败', color: 'warning' });
      }
      });
    }
  }

  selectValue=(data,type)=>{ //选中搜索条，单据号搜索
    // this.queryKey.searchtype=0;
    const { param } = this.state;
    if(this.state.billVal.length==0){
      this.firstSearch=true;
    }
    if(data){
      this.setState({
        billVal:[...this.state.billVal,data]
      })
      // this.setState({ fuzzyquerychild: [] });
      // 更新查询条件
      this.queryKey.billnos = [data];
      // this.props.search.clearSearchArea(presetVar.searchArea);//清空搜索区
     
      // 查询数据
      this.queryMessage(1,param);
        
    }

  }

  getData = (expanded, record) => {
   const {value:busiid} = record.values.busiid;
  //  debugger;
   if(expanded===true&&(this.state.filesList.findIndex(item=>item.pk_bill===busiid)<0)){//展开时请求
    api.getFile({
      data: {pk_bill:busiid},
      success: data => {
        if(data.data.files&&data.data.files.length>0){
          this.state.filesList=[...this.state.filesList,data.data];
          // const obj = {},newList=[];
          // this.state.filesList.forEach((item,index)=>{//去重
          //   if(!obj[item.pk_bill]){
          //     newList.push(item);
          //     obj[item.pk_bill]=true;
          //   }
          // })
          this.setState({
            filesList: this.state.filesList
          })
        }
      }});
   }
  }

  selectChange=(v)=>{ // 排序条件选择
    const {pageinfo:{number},param}=this.state;
    if(this.state.param){
      const current = v.split('-');
      this.queryKey.orderByInfo=[{order:current[0],field:current[1]}];
      this.queryMessage(number,param,this.queryKey.orderByInfo);
    }
  }
  expandedRowRender=(record, index, indent)=>{
    // debugger
    const {filesList} =this.state;
    const currentFiles=filesList.filter(item=>item.pk_bill===record.values.busiid.value)
    
    if(currentFiles){//列表里找到附件存在的行
      return(
        <div>
            {currentFiles.length>0?currentFiles.map(item=>{
              if(item.files&&item.files.length>0){
               return item.files.map(file=>(
                    <div className='attachment'>
                      <a href={file.previewUrl} target="_blank">{file.filename}</a>
                      <div className="attachment_right">
                      <a href={file.previewUrl} target="_blank">
                        <NCButton colors="secondary" className="preview">
                          附件预览
                        </NCButton>
                      </a>
                      <a href={file.fileurl} target="_blank">
                        <NCButton colors="secondary">附件下载</NCButton>
                      </a>
                      </div>
                  </div>))
              }
            }):null
          }
        </div>
      )
    }else{
      return (<span></span>);
    }
   
    
  }

  pageSizeChange=(val)=>{
    this.setState({
      // tableData:[],//改变size最好清空表格
      pageinfo:{
        ...this.state.pageinfo,
        size:val
      }
    })
  }

  clearSearchHandle=(arr)=>{
    this.setState({
      param:{}
    })
  }

  render() {
    const { table, search, button } = this.props;
    const {pageinfo:{totalPages},tableData} =this.state;
    const { createButtonApp } = button;
    const { createSimpleTable} = table;
    let { NCCreateSearch } = search;
    let multiLang = this.props.MutiInit.getIntl(1056);
    // this.props.button.setButtonDisabled('print_bill', true);
    let isShow = type => {
      if (this.state.showType == "list" && type == "list") {
        return "show";
      } else if (this.state.showType == "card" && type == "card") {
        return "show";
      } else {
        return "hide";
      }
    };
    let addMoreOnOff = this.data.listRows.length > 0 && Number(this.queryKey.pageinfo.totalPages) > Number(this.queryKey.pageinfo.number)

    return (
        <ProfileStyle
            layout="singleTable"
            {...this.props}
            id="rpBillList"
        >
            <ProfileHead
                title={'打印装订'}
            >
                <HeadCenterCustom>
                    <div id='bill-search--fuzzyqry'>
                        <NCSelect
                            className="fuzzyquery"
                            height="24px"
                            tags
                            searchPlaceholder={
                                multiLang && multiLang.get("105602BZCX-0010")
                            }
                            placeholder={multiLang && multiLang.get("105602BZCX-0011")}
                            data={this.state.fuzzyquerychild}
                            value={this.state.billVal}
                            // onSearch={this.onFuzzyChange.bind(this)}
                            onDeselect={this.deletSelect.bind(this)}
                            // onChange={this.onFuzzySelected.bind(this)}
                            onSelect={this.selectValue.bind(this)}
                        />
                        <span className="width130">是否批量查询</span>
                        {/* <NCSwitch
                        fieldid="userflag"
                          // checked={this.state.checked}
                          // onChange={this.onChange}
                        /> */}
                        <NCRadio.NCRadioGroup
                          name="fruit"
                          selectedValue={this.state.selectedValue}
                          onChange={this.handleChange.bind(this)}
                          >
                            <NCRadio value={true} >是</NCRadio>
                            <NCRadio value={false} >否</NCRadio>
                        </NCRadio.NCRadioGroup>
                        {/* <NCSwitch
                         onChange={()=>{
                          ifsearch=!ifsearch
                      
                            }  
                          }
                        >
                        </NCSwitch> */}
                    </div>
                    <i
                        className={`menu-type-icon iconfont ${
                            this.state.menuShowType.type
                            }`}
                        title={this.state.menuShowType.title}
                        onClick={this.menuClick}
                    />
                </HeadCenterCustom>
            </ProfileHead>
            <ProfileBody>
                <EmptyArea className='ssc-profile-search-area'>
                    {/* "105602BZCX-0024":"快速查询" */}
                    <div className="create-search">
                        {NCCreateSearch(presetVar.searchArea, {
                            clickSearchBtn: this.searchClick.bind(this),
                            defaultConditionsNum: 1,
                            onAfterEvent:this.onSearchAfterEvent,
                            showAdvBtn: false,
                            advSearchClearEve:this.clearSearchHandle
                            // searchBtnName: multiLang && multiLang.get('105602BZCX-0024')
                        })}
                    </div>
                </EmptyArea>
                <div className={isShow("list")}>
                  <div className='print-button-area'>
                  <NCSelect
                      className="sortKey"
                      placeholder={'排序条件'}
                      // data={[{value:'1'},{value:'2'}]}
                      onChange={this.selectChange}
                  >
                    <NCOption value={'ascend-voucher_num'}>按凭证号升序</NCOption>
                    <NCOption value={'descend-voucher_num'}>按凭证号降序</NCOption>
                    <NCOption value={'ascend-amount'}>按金额升序</NCOption>
                    <NCOption value={'descend-amount'}>按金额降序</NCOption>
                    <NCOption value={'ascend-billdate'} >按单据日期升序</NCOption>
                    <NCOption value={'descend-billdate'}>按单据日期降序</NCOption>
                  </NCSelect>
                  {createButtonApp({
                    area: 'print',
                    buttonLimit: 2, 
                    onButtonClick: this.onButtonClick.bind(this),
                    popContainer: document.querySelector('.print-button-area')
                  })}
                  </div>
                  <div className="demo-checkbox">
            </div>
                    <MultiSelectTable 
                      bordered
                      columns={ this.renderColumns()}
                      onExpand={this.getData}
                      multiSelectConfig={{inverse: true}} 
                      // onExpandedRowsChange={this.onExpandChangeHandle}
                      // expandedRowKeys={[1]}
                      // haveExpandIcon={(record, index)=>{if(index>1){
                      //   return true
                      // }}}
                      pageNumber={this.state.pageinfo.number}
                      pageSize={this.state.pageinfo.size}
                      dragborder={true} 
                      // bodyDisplayInRow={true}
                      expandIconColumnIndex={12}
                      // expandIconAsCell={true}
                      defaultExpandAllRows={true}
                      syncHover={false}
                      isDrag={true}
                      data={tableData}
                      expandedRowRender={this.expandedRowRender.bind(this)}
                      getSelectedDataFunc={this.checkHandleChange.bind(this)}
                      scroll={{ x: true }}
                    />
                    <div className={'page-wrap'}>
                      
                      <NCSelect
                        value={this.state.pageinfo.size}
                        style={{ width: 85 }}
                        onSelect={this.pageSizeChange}
                        className="page-size"
                        showClear={false}
                      >
                        <Option value={10}>
                          <div className='select-single-line-and-ellipsis'>10条/页</div>
                        </Option>
                        <Option value={20}>
                          <div className='select-single-line-and-ellipsis'>20条/页</div>
                        </Option>
                        <Option value={50}>
                          <div className='select-single-line-and-ellipsis'>50条/页</div>
                        </Option>
                      </NCSelect>
                      {totalPages>0&&
                      this.state.selectedValue!=true&&
                      <NCPagination
                        // first
                        // last
                        prev
                        next
                        // boundaryLinks
                        items={totalPages}
                        maxButtons={3}
                        activePage={this.state.pageinfo.number}
                        onSelect={this.handleSelect} 
                      />}
                    </div>
                    
                    {/* {createSimpleTable('rplist', {
                        showCheck:true,
                        showIndex: true,
                        lazyload: false,
                        isAddRow: true,// 失去焦点是否自动增行
                        onRowDoubleClick:this.dbClick.bind(this),
                        allowTotalRows: () => { return [3,5] },
                         noTotalCol: ['amount'],
                         lazyload:true,
                         height:300,
                         setCellClass:(index, record, attrcode) => { if(attrcode=='amount') return 'calssname' },
                         merge:(index, attrcode)=>{if(index==0&&attrcode== 'amount'){ return {colSpan:2,rowSpan:1} } if(index==0&&attrcode=='voucher_date'){ return {colSpan:0,rowSpan:1} }},
                        sort:{
                          mode:'single',
                          backSource:true,
                          sortFun:(sortParam)=> {
                              this.handleTempletAfterSort(sortParam,"list");
                              query.queryData.call(this);
                          }
                      }
                    })} */}
                    {/* "105602BZCX-0048":"滑动加载更多" */}
                    {/* <div
                        className='scroll-add-more'
                        style={{ display: addMoreOnOff ? 'block' : 'none' }}
                    >{multiLang && multiLang.get('105602BZCX-0048')}</div> */}
                </div>
                {/* <div className={isShow("card")}>
                    <WorkbenchDetail
                        ref="detail"
                        btns={this.state.btns}
                        props={this.props}
                        doAction={listButtonClick.bind(this)}
                        getBtns={() => {
                            if (this.queryKey.isComplate == "true") {
                                return ["Print", "Copy"];
                            } else {
                                return ["Print", "Copy", "RemindingApproval"];
                            }
                        }}
                    />
                    <div
                        className='scroll-add-more'
                        style={{ display: addMoreOnOff ? 'block' : 'none' }}
                    >{multiLang && multiLang.get('105602BZCX-0048')}
                    </div>
                </div> */}
                {
						this.props.modal.createModal('print', {
						title: '选择打印模板', // 弹框表头信息/* 国际化处理： 打印*/
            content: createPrint.call(this,this), //弹框内容，可以是字符串或dom
						//beSureBtnClick: confirmPrint.bind(this, this.props, -1), //点击确定按钮事件
						userControl: true,
						size:'sm',
						//cancelBtnClick: closePrint.bind(this),
						className: 'modal-h-auto modal-createform',
						showCustomBtns:true,
						customBtns:printButton(this,this.props, -1,this.voucher)
					})}
            </ProfileBody>
            
        </ProfileStyle>
    );
  }
}
PrintBinding = createPage({
  mutiLangCode: "1056"
})(PrintBinding);
export default PrintBinding;
