import React, { PureComponent } from "react";
import PropTypes from 'prop-types';
/**
 * 参数: 过滤表头
 * @param {*} Table
 * @param {*} Checkbox
 * @param {*} Popover
 * @param {*} Icon
 */
export default function multiSelect(Table, Checkbox) {
    return class MultiSelect extends PureComponent {
        static propTypes = {
          autoCheckedByClickRows: PropTypes.bool, //行点击时，是否自动勾选复选框
        };
        static defaultProps = {
          getSelectedDataFunc:()=>{},
        }
    
        constructor(props) {
          super(props);
          let obj = this.getCheckedOrIndeter(props.data);
          this.state = {
            ...obj,
            data:Object.assign({},...props.data),
          }
        }
        componentWillReceiveProps(nextProps){
            if('data' in nextProps){
              let obj = this.getCheckedOrIndeter(nextProps.data);
              if(nextProps.data.length>0&&nextProps.data.length!=this.props.data.length){//设置默认选中
                this.getSelectData(nextProps.data);
              }
              this.setState({
                ...obj,
                data:nextProps.data,
              })
            }
          }

          getSelectData=(data)=>{
              let checkGroup=[];
             data.forEach(item=>{
                if(item._checked==true){
                  checkGroup.push(item)
                }
            })
            this.props.getSelectedDataFunc(checkGroup,undefined,undefined,data);
          }

          shouldComponentUpdate(nextProps,nextState){
// debugger
//             if((nextProps.data.length==this.props.data.length&&nextProps.pageNumber!==this.props.pageNumber)||nextState.checkedAll!==this.state.checkedAll||(this.props.data[0]&&nextProps.data[0].values.busiid.value!==this.props.data[0].values.busiid.value)){
//                 return true
//             }
        
            if(nextProps.pageSize!==this.props.pageSize){
    
                return false
            }else {
                return true
            }
          }

            /**
         * 重写：判断数据是否全部选中
         */
        checkAllSelected = ( data ) => {
            if(!this.isArray(data))return false;
            if(data.length == 0)return false;
            let count = 0;
            let disabledCount = 0;
            let length = 0;
            let getTree = ( arr ) => {
            arr.forEach( item  => {
                length++;
                if(item._checked && !item._disabled){
                count ++;
                }
                else if(item._disabled){
                disabledCount ++;
                }
                if(item.children){
                getTree(item.children);
                }
            })
            }
            getTree(data);
            if(length == count + disabledCount && count>0){
            return "all";
            }
            return count == 0?false:"indeter";
        }

        /**
         * 判断是否是数组
         * @param {*} o 
         */
        isArray(o){
            return Object.prototype.toString.call(o)=='[object Array]';
        }

          getCheckedOrIndeter(data){
            let obj = {};
            let checkStatus = this.checkAllSelected(data);
            if(!checkStatus){
              obj.checkedAll = false;
              obj.indeterminate = false;
              return obj;
            }
            if(checkStatus == 'indeter'){
              obj.indeterminate = true;
              obj.checkedAll = false;
            }else if(checkStatus == 'all'){
              obj.checkedAll = true;
              obj.indeterminate = false;
            }
            return obj;
          }

          onAllCheckChange=()=>{
            let {data,checkedAll,indeterminate} = this.state;
            let check = false;
            if(checkedAll){
              check = false;
            }else{
              check = true;
            }
            let selectList = [];
            
            data.forEach(item => {
              if( item.children ){
                let res = this.setTree(item,check, true);
                selectList = selectList.concat(res);
              }
              else {
                if(!item._disabled){
                  item._checked = check;
                }
               
                if(item._checked){
                  selectList.push(item);
                }
              }
            });
            if(selectList.length > 0){
              indeterminate = true;
            }else{
              indeterminate = false;
            }
            this.setState({
              indeterminate:indeterminate,
              checkedAll:check
            });
            this.props.getSelectedDataFunc(selectList,undefined,undefined,data);
          }

          onCheckboxChange = (text, record, index) => () => {
              // debugger
            let {data} = this.state;
            let selectList = [];
            // record._checked = record._checked?false:true;
            let flag = record._checked ? false : true;
            if (record.children) {
              this.setTree(record, flag, this.props.autoSelect);
            }
            else {
              record._checked = flag;
            }
            let obj = this.getCheckedOrIndeter(data);
            this.setState({
              data:data,
              ...obj
            })
            data.forEach((da)=>{
              if(da.children){
                selectList = selectList.concat(this.getTree(da,'_checked',true))
              }
              else if(da._checked){
                selectList.push(da);
              }
            })
            this.props.getSelectedDataFunc(selectList,record,index,data);
          };

          getDefaultColumns=(columns)=>{
            let {multiSelectConfig} = this.props;
            let {checkedAll,indeterminate} = this.state;
            let checkAttr = {checked:checkedAll?true:false};
            const data = this.props.data;
            const dataLength = data.length;
            let disabledCount = 0;
            indeterminate?checkAttr.indeterminate = true:"";
            //设置表头Checkbox是否可以点击
            data.forEach((item,index,arr)=>{
              if(item._disabled){
                disabledCount++;
              }
            })
      
            let _defaultColumns =[{
                className: 'u-table-multiSelect-column',
                title: (
                    <div>
                        <Checkbox
                            className="table-checkbox"
                            {...checkAttr}
                            // {...multiSelectConfig}
                            disabled={disabledCount==dataLength?true:false}
                            onChange={this.onAllCheckChange}
                        />
                        <span>序号</span>
                    </div>
                  
                ),
                key: "checkbox",
                dataIndex: "checkbox",
                fixed:"left",
                width: 80, 
                render: (text, record, index) => {
                  let attr = {};
                  record._disabled?attr.disabled = record._disabled:"";
                  return (
                      <div>
                          <Checkbox
                            key={index}
                            // className="table-checkbox"
                            {...attr}
                            {...multiSelectConfig}
                            checked={record._checked}
                            onChange={this.onCheckboxChange(text, record, index)}
                            />
                            <span>{this.props.pageSize*(this.props.pageNumber-1)+index+1}</span>
                      </div>
                  )
                }
              },
            //   {
            //       title:'序号',
            //       key:'index',
            //       dataIndex:'index',
            //       width:60,
            //       render:(v,record,index)=>{
            //           return(

            //             this.props.pageSize*(this.props.pageNumber-1)+index+1
            //           )
            //       }
            //   }
            ]
              return _defaultColumns.concat(columns);
          }

          render(){
            const {columns, expandIconColumnIndex} = this.props;
            const {data} = this.state;
              return(
                <Table {...this.props} 
                columns={this.getDefaultColumns(columns)} 
                data={this.props.data} 
                // onRowClick={this.onRowClick}
                // expandIconColumnIndex={expandIconColumnIndex ? expandIconColumnIndex+1 : 1}
                />
              )
          }
    }
}
