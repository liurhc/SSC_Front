import requestApi from "../requestApi";
let queryData = function(isAddPage) {
  // debugger;
  if (isAddPage==true) {
    // 更新查询条件
    this.queryKey.pageinfo.number++;
    this.queryKey.pageinfo.size=Number(this.queryKey.pageinfo.size);
  } else {
    this.queryKey.pageinfo.number = 1;
    this.queryKey.pageinfo.size=Number(this.queryKey.pageinfo.size);
  }
  this.queryKey.pageStatus = this.state.pageStatus;
  if (this.queryKey.queryKey == false) {
    return;
  }
  requestApi.billQuery({
    data: this.queryKey,
    success: data => {
      // debugger;
      let listRows = [];
      if (isAddPage===true) {
        this.data.listRows.map(one => {
          listRows.push(this.copyData(one));
        });
      } else if(isAddPage==='dj'){
        this.data.listRows.map(one => {
          listRows.push(this.copyData(one));
        });
      }else{
        this.refs.detail.clearAllDetailData();
        this.pubMessage.querySuccess((data.data.pageinfo.totalElements || [])|| []);
      }
      data.data.result.map(one => {
        listRows.push(this.copyData(one));
      });
      let hash = {}; 
      const data2 = listRows.reduce((preVal, curVal) => {
        // debugger;
          hash[curVal.values.billno.value] ? '' : hash[curVal.values.billno.value] = true && preVal.push(curVal); 
          return preVal 
      }, [])
      listRows=data2;
      this.data.listRows = listRows;
      this.props.table.setAllTableData(window.presetVar.list, {
        areacode: window.presetVar.list,
        rows: listRows
      });
      // 设置缩略数据
      // this.refs.detail.addDetailData(data.data[window.presetVar.card]);
      let newState = {};
      newState = this.state;
      // 更新页信息
      this.queryKey.pageinfo = data.data.pageinfo;
      if (this.queryKey.pageinfo.totalPages > this.queryKey.pageinfo.number) {
        newState.pageControll = "notend";
      } else if (
        this.queryKey.pageinfo.totalPages == this.queryKey.pageinfo.number
      ) {
        newState.pageControll = "end";
      } else {
        newState.pageControll = "none";
      }
      this.setState(newState);
      setTimeout(() => {
        this.canChangPageFlag = true;
      }, 100);
    }
  });
};
export default { queryData };
