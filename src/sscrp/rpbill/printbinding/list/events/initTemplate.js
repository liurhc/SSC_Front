import presetVar from "../presetVar";
import { listButtonClick } from "./index";
const pagecode = '105602DYZD_list'; 
const tableid  ='rplist'
import {printItem,getDefault} from'../print';
export default function(props,callBack) {
  const that = this;
  props.createUIDom({pagecode:pagecode}, function(data) {
    let meta = data.template;
				// meta = this.modifierMeta(this.props,meta,this);
    data.button && props.button.setButtons(data.button,()=>{})
    meta[window.presetVar.searchArea].items.map(one => {
      if(one.attrcode == "pk_tradetype"){
          one.fieldValued = 'refcode';
          one.queryCondition={
            TreeRefActionExt:'nccloud.web.sscrp.reg.tradetype.BindingFilterTradetype'// 参照条件过滤
          }
      }
      if(one.attrcode == "pk_org"){
        one.isRunWithChildren =false;
        one.isShowDisabledData =true;
        one.queryCondition={
          TreeRefActionExt:'nccloud.web.sscrp.ref.org.BindingFilterOrg'// 参照条件过滤
        };
    }
      dealRefShow(one);
    });
    props.meta.setMeta(meta,callBack);
         data.button && props.button.setButtons(data.button,()=>{
                // 按钮状态初始化
                props.button.setDisabled({
                  print_bill:true,
                  print_vouchers:true,// 手动匹配按钮默认置灰
                  print_all:true,
                  download_all:true
                });
             meta[tableid].items=meta[tableid].items.map((item, key) => {
             item.width = 150;
             console.log(key,item)
             if (item.attrcode == 'opr') {
               item.renderStatus = 'browse';
               item.render = (text, record, index) => {
                 return (
                   <a
                   class=""	
                   style={{ textDecoration: 'underline', cursor: 'pointer' }}
                   >
             
                   </a>
                 );
               };
             } 
             return item;
                   });
    })
    getDefault.call(that).then(res=>{
    const defaultData = res.data.rows.filter(item=>{
     if(item.refname.indexOf('默认')>=0){
        return true
      }
     })
      meta['print'] = printItem.call(that,defaultData); 
    });
    
    //打印
      meta[tableid].items.push({
                      attrcode: 'opr',
                      label: '操作',
                      itemtype: 'customer',
                      // fixed: 'right',
                      className: 'table-opr',
                      visible: true,
                      width: 200,
                      render: (text, record, index) => {
                    var buttonAry=[];
                    buttonAry.push('dydj');
                    if(record.voucherid){
                      buttonAry.push('dypz');
                    }
                          return props.button.createOprationButton(buttonAry, {
                              area: "list_inner",
                              buttonLimit: 6,
                              onButtonClick: (props, key) =>that.onButtonClick.call(that,props, key, record)
                          });
                      }
                  });
    props.table.setTableRender(
      presetVar.list,
      "billno",
      (text, record, index) => {
        return (
          <a
            className="billnoa"
            onClick={() => {
              listButtonClick.call(that, "OpenBill", record, index);
            }}
          >
            {record.billno.value}
          </a>
        );
      }
    );
    // 自定义状态列
    // props.table.setTableRender(
    //   presetVar.list,
    //   "billstatus",
    //   (text, record, index) => {
    //     return (
    //       <div className="bill-status-wrap">
    //         <span
    //           className={
    //             record.billstatus.value == "0"
    //               ? "not-complete-status"
    //               : "complete-status"
    //           }
    //         />
    //         <span>{record.billstatus.display}</span>
    //       </div>
    //     );
    //   }
    // );
  });
  
  const dealRefShow = function(data) {
    if (data.itemtype == "refer") {
      if (data.fieldDisplayed == "code") {
        data.fieldDisplayed = "refcode";
      } else if (data.fieldDisplayed == "name") {
        data.fieldDisplayed = "refname";
      }
    }
  };
}
