import requestApi from "../requestApi";
export default function listButtonClick(actionId, data, index, actionFrom) {
  // console.log('listButtonClick:' + actionId);
  let listData = data;
  if (actionFrom == "detail") {
    listData = this.data.listRows[index].values;
  }
  switch (actionId) {
    case "OpenBill":
      // debugger;
      requestApi.openBill({
        data: {
          billtypeCode: listData.values.billtypecode.value,
          transtypeCode: listData.values.pk_tradetype.value,
          billid: listData.values.busiid.value
        },
        success: data => {
          // debugger;
          this.props.specialOpenTo(data.data.url, {
            ...data.data.data,
            scene: "bzcx"
          });
        }
      });

      // debugger;
      break;
    default:
      break;
  }
}
