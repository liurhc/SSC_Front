import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import {createPage, ajax, base, high} from 'nc-lightapp-front'
const { NCAffix } = base
const { ApproveDetail } = high
import {buttonClick, initTemplate, afterEvent, tableModelConfirm, listButtonClick} from './events'

import requestApi from './requestApi'
import './index.less';

import WorkbenchTab from 'ssccommon/components/workbench-tab'
import WorkbenchSearcharea from 'ssccommon/components/workbench-searcharea'
import WorkbenchDetail from 'ssccommon/components/workbench-detail'
import NewBill from './components/newBill'
import ShowNum from './components/showNum'

class RpBillList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showType: 'list',
            showNumbers:{},
            searcharea:{
                conditions:[]
            },
            showModalNum:false,
            operationDat:{},
            pageControll: 'none',
            fuzzyquerychild: [],
            btns:[],
            currentKey: 'false',
            approveDetailConfig: {billtype: '', billid: ''},
            showApproveDetail: false
        }
        // 查询条件
        this.queryKey={
            // 未完成（false）/已完成（true）
            isComplate: 'false',
            fuzzyQueryKey: [],
            orderByInfo:[],
            // 页信息
            pageinfo:{
                number: 1,
                size: 20,
                totalElements: 0,
                totalPages: 0
            }
        }
        // 缓存查询结果数据
        this.data={
            listRows:[],
            cardRows:[]
        }
        // 延时控制标记
        // 翻页
        this.canChangPageFlag = true;
        // 模糊查询
        this.fuzzyKey = "";

        // 页面初始化
        initTemplate.call(this, props);
    }
    componentDidMount() {
        // Firefox
        window.addEventListener('DOMMouseScroll', (ev) => {
            ev = ev || window.event
            let onOff = null
            if(ev.detail < 0){
                onOff = true
            }else{
                onOff = false
            }
            let t = document.documentElement.scrollTop || document.body.scrollTop
            let h = document.documentElement.scrollHeight || document.body.scrollHeight
            let bh = document.documentElement.clientHeight || document.body.clientHeight
            if(
                !onOff 
                && h-bh-t < 30 
                && this.queryKey.pageinfo.totalPages > this.queryKey.pageinfo.number 
                && this.canChangPageFlag
            ) {
                this.canChangPageFlag = false
                setTimeout(() => { this.queryData(true) }, 300)
            }
        })
        // 其他浏览器
        window.onmousewheel = (ev) => {
            ev = ev || window.event
            let onOff = null
            if(ev.wheelDelta > 0){
                onOff = true
            }else{
                onOff = false
            }
            let t = document.documentElement.scrollTop || document.body.scrollTop
            let h = document.documentElement.scrollHeight || document.body.scrollHeight
            let bh = document.documentElement.clientHeight || document.body.clientHeight
            if(
                !onOff 
                && h-bh-t < 30 
                && this.queryKey.pageinfo.totalPages > this.queryKey.pageinfo.number 
                && this.canChangPageFlag
            ) {
                this.canChangPageFlag = false
                setTimeout(() => { this.queryData(true) }, 300)
            }
        }
        // 审批详情位置
        let approveDetailDom = document.getElementsByClassName('approve-detail').length > 0 ? document.getElementsByClassName('approve-detail')[0] : null
        if (approveDetailDom) {
            approveDetailDom.style.position = 'fixed'
            console.log('approveDetailDom Position Fixed')
        }
    }
    // 已完成/未完成 页签切换
    selectTabChange(selectKey){
        // 更新查询条件
        this.queryKey.isComplate=selectKey;
        this.setState({currentKey: selectKey}, () => {
            // 初始化页面
            initTemplate.call(this, this.props);
        })
    }
    // 查询条件选择变更
    handleConditionChange(attrcode, value){
        // 更新查询条件
        this.queryKey[attrcode]=value;
        // 查询数据
        this.queryData();
    }
    // 列表/缩略切换
    showTypeClick(type){
        this.setState({showType: type});
    }
    // 模糊查询输入内容变化
    onFuzzyChange(data){
        this.fuzzyKey = data;
        let newQueryKey = {};
        let multiLang = this.props.MutiInit.getIntl(1056);
        for(let child in this.queryKey){
            newQueryKey[child] = this.queryKey[child];
        }
        newQueryKey.fuzzyQueryKey = data;
        setTimeout((that, data)=>{
            if(data == that.fuzzyKey){
                requestApi.queryFuzzyKey({
                    data: newQueryKey,
                    success: (data) => {
                        for(let childName in data.data){
                            data.data[childName].value = data.data[childName].value+"="+that.fuzzyKey;
                            //105602BZCX-0049:包含
                            data.data[childName].key = data.data[childName].key + (multiLang && multiLang.get("105602BZCX-0049")) + that.fuzzyKey;
                        }
                        that.setState({fuzzyquerychild:data.data});
                    }
                });
            }
        },500, this, data);
    }
    // 模糊查询选择
    onFuzzySelected(data){
        this.setState({fuzzyquerychild:[]})
        // 更新查询条件
        this.queryKey.fuzzyQueryKey=data;
        // 查询数据
        this.queryData();
    }

    // 查询数据（参数：是否是翻页动作）
    queryData(isAddPage){
        if(isAddPage){
            // 更新查询条件
            this.queryKey.pageinfo.number++;
        }else{
            this.queryKey.pageinfo.number = 1;
        }
        let _this = this;
        requestApi.query({
            data: this.queryKey,
            success: (data) => {
                // console.log('查询返回',data);
                let listRows = [];
                if(isAddPage){
                    _this.data.listRows.map((one)=>{
                        listRows.push(_this.copyData(one));
                    })
                }else{
                    _this.refs.detail.clearAllDetailData();
                }
                data.data[window.presetVar.list].rows.map((one)=>{
                    listRows.push(_this.copyData(one));
                })
                _this.data.listRows = listRows;
                // 设置列表数据
                _this.props.table.setAllTableData(_this.queryKey.isComplate=="false" ? window.presetVar.list:"rplist_done", {
                    areacode: _this.queryKey.isComplate=="false" ? window.presetVar.list:"rplist_done",
                    rows: listRows
                });
                // 设置缩略数据
                _this.refs.detail.addDetailData(data.data[window.presetVar.card]);
                let newState = {};
                newState = _this.state;
                // 更新未完成数量
                if(data.data.todonum != null){
                    newState.showNumbers={false:data.data.todonum};
                }
                // 更新查询条件区域
                data.data.searchArea.items.map((newOne)=>{
                    newState.searcharea.conditions.map((oldOne, index)=>{
                        if(newOne.attrcode == oldOne.attrcode){
                            newState.searcharea.conditions[index] = newOne;
                        }
                    })
                })
                // 更新页信息
                _this.queryKey.pageinfo = data.data[window.presetVar.list].pageinfo;
                if(_this.queryKey.pageinfo.totalPages > _this.queryKey.pageinfo.number){
                    newState.pageControll = 'notend';
                }else if(_this.queryKey.pageinfo.totalPages == _this.queryKey.pageinfo.number){
                    newState.pageControll = 'end';
                }else{
                    newState.pageControll = 'none';
                }
                _this.setState(newState);
                setTimeout(()=>{_this.canChangPageFlag=true;},100);
            }
        })
    }

    // 复制列表数据
    copyData(data){
        let newData = {};
        newData.values = {};
        for(let child in data.values){
            newData.values[child] = {
                display: data.values[child].display,
                value: data.values[child].value,
                scale: data.values[child].scale 
            }
        }
        return newData;
    }

    // 列表行双击事件
    onRowDoubleClick = (values, idx, props, events) => {
        listButtonClick.call(this, "OpenBill", values, idx, 'list');
    }
    // 处理排序后的模板变化
    handleTempletAfterSort (sortParam,tempId){
        if(sortParam[0].order=='flatscend'){
            this.queryKey.orderByInfo = [];
        }else {
            this.queryKey.orderByInfo = sortParam;
        }
        let sortObj = {};
        sortParam.forEach(item => {
             sortObj[item.field] = item;
        });
        let meta = this.props.meta.getMeta()
        meta[window.presetVar[tempId]].items.forEach(item => {
            //保存返回的column状态，没有则终止order状态
            if(sortObj[item.attrcode]){
                item.order = sortObj[item.attrcode].order;
                item.orderNum = sortObj[item.attrcode].orderNum;
            }else {
                item.order = "flatscend";
                item.orderNum = "";
            }
        })
        this.props.meta.setMeta(meta);
    }

    render() {
        
        const {button, table} = this.props
        const {createSimpleTable} = table
        let multiLang = this.props.MutiInit.getIntl(1056)
        
        let isShow = (type) =>{
            if(this.state.showType == "list" && type == "list"){
                return "show";
            }else if(this.state.showType == "card" && type == "card"){
                return "show";
            }else{
                return "hide";
            }
        }

        let addMoreOnOff = this.data.listRows.length > 0 && this.queryKey.pageinfo.totalPages > this.queryKey.pageinfo.number

        const {approveDetailConfig: {billtype, billid}, showApproveDetail} = this.state

        return (
            <div id="rpBillList" className={addMoreOnOff ? 'add-more-wrap' : ''}>
                <div className="container">
                    <NCAffix offsetTop={0}>
                        <NewBill parentPage={this} {...this.props} />
                    </NCAffix>
                    <NCAffix offsetTop={44}>
                        <div className="workbench-tab">
                            {/* "105602BZPT-0017":"未完成",
                        "105602BZPT-0018":"已完成", */}
                            <WorkbenchTab
                                tabs={[{ attrcode: "false", label: multiLang && multiLang.get('105602BZPT-0017') }, { attrcode: "true", label: multiLang && multiLang.get('105602BZPT-0018') }]}
                                showNumbers={this.state.showNumbers}
                                selectTabChange={this.selectTabChange.bind(this)}
                                currentKey={this.state.currentKey}
                            />
                        </div>
                    </NCAffix>
                    <div>
                        <WorkbenchSearcharea
                            cusType="rp"
                            twoColNums="2"
                            conditions={this.state.searcharea.conditions}
                            handleConditionChange={this.handleConditionChange.bind(this)}
                            fuzzyquerychild = {this.state.fuzzyquerychild}
                            onFuzzyChange={this.onFuzzyChange.bind(this)}
                            onFuzzySelected={this.onFuzzySelected.bind(this)}
                            showTypeClick={this.showTypeClick.bind(this)}
                        />
                    </div>
                    <div className={isShow("list")} style={{position: 'relative'}}>
                        <div style={{display: this.queryKey.isComplate=="false" ? 'block' : 'none'}}>
                            {
                                createSimpleTable(
                                    window.presetVar.list,
                                    {
                                        showIndex:true,
                                        adaptionHeight:true,
                                        onAfterEvent: afterEvent.bind(this),
                                        onRowDoubleClick: this.onRowDoubleClick,
                                        sort:{
                                            mode:'single',
                                            backSource:true,
                                            sortFun:(sortParam)=> {
                                                this.handleTempletAfterSort(sortParam,"list");
                                                this.queryData();
                                            }
                                        }
                                        
                                    }
                                )
                            }
                        </div>
                        <div style={{display: this.queryKey.isComplate=="false" ? 'none' : 'block'}}>
                            {
                                createSimpleTable(
                                    window.presetVar.list_done,
                                    {
                                        showIndex:true,
                                        adaptionHeight:true,
                                        onAfterEvent: afterEvent.bind(this),
                                        onRowDoubleClick: this.onRowDoubleClick,
                                        sort:{
                                            mode:'single',
                                            backSource:true,
                                            sortFun:(sortParam)=> {
                                                this.handleTempletAfterSort(sortParam,"list_done");
                                                this.queryData();
                                            }
                                        }
                                    }
                                )
                            }
                        </div>
                        
                        {/* "105602BZCX-0048":"滑动加载更多" */}
                        <div
                            className='scroll-add-more'
                            style={{display: addMoreOnOff ? 'block' : 'none'}}
                        >{multiLang && multiLang.get('105602BZCX-0048')}</div>
                    </div>
                    <div className={isShow("card")}>
                        <WorkbenchDetail  
                            ref="detail"
                            btns={this.state.btns}
                            props={this.props}
                            doAction={listButtonClick.bind(this)}
                            getBtns = {()=>{
                                if(this.queryKey.isComplate == 'true'){
                                    return ['Print','Copy','ztd', 'spxq'];
                                }else{
                                    return ['Print','Copy','RemindingApproval','ztd', 'spxq'];
                                }
                            }}
                            buttonArea={'list'}
                        />
                        <div
                            className='scroll-add-more'
                            style={{display: addMoreOnOff ? 'block' : 'none'}}
                        >{multiLang && multiLang.get('105602BZCX-0048')}</div>
                    </div>
                </div>
                <ShowNum
					{...this.props}
                    showModal={this.state.showModalNum}
                    data={this.state.operationData}
                    that={this}
                />
                <ApproveDetail
                    show={showApproveDetail}
                    close={() => {
                        this.setState({
                            showApproveDetail: false,
                            approveDetailConfig: {billtype: '', billid: ''}
                        })
                    }}
                    billtype={billtype}
                    billid={billid}
                />
            </div>
        )
    }
}
RpBillList = createPage({
    mutiLangCode:'1056'
})(RpBillList);
export default RpBillList;