import React, {Component} from 'react';
import {base, ajax,print} from 'nc-lightapp-front';
const {NCFormControl,NCButton,NCModal,NCMessage} = base;

class ShowNum extends Component {
    constructor(props) {
        super(); 
        this.state={
            searchValue:'1'
        }
    }


    save = () =>{
        let multiLang = this.props.MultiInit.getLangData(1056).intl; 
        if(this.state.searchValue == null || this.state.searchValue==''){
            //105602BZPT-0050:请输入打印份数
            NCMessage.create({content: multiLang && multiLang.get("105602BZPT-0050"), color: 'warning', position: 'topRight'});
            return;
        }
        if (!(/(^[1-9]\d*$)/.test(this.state.searchValue))){
            //105602BZPT-0051:请输入正确的份数
            NCMessage.create({content: multiLang && multiLang.get("105602BZPT-0051"), color: 'warning', position: 'topRight'});
            return;
        }
        if(this.state.searchValue>100){
            //105602BZPT-0052:打印份数不能大于100
            NCMessage.create({content: multiLang && multiLang.get("105602BZPT-0052"), color: 'warning', position: 'topRight'});
            return;
        }
        print(
            'pdf',  //支持两类: 'html'为模板打印, 'pdf'为pdf打印
            '/nccloud/sscrp/rpbill/RPBillPrintAction.do', //后台服务url
            {
                  
                funcode:'105602BZPT',      //功能节点编码，即模板编码
                nodekey:'ZTD',     //模板节点标识
                oids:[this.props.data.pk_bill.value],    // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
                userjson:JSON.stringify({"num":this.state.searchValue}),
                
            }
        );
        this.state.searchValue='1';
        this.props.that.setState({showModalNum: false});
    }

    cancel = () => {
        this.state.searchValue='1';
        this.props.that.setState({showModalNum: false});
    }
    render() {
        let {data} = this.props;
        let multiLang = this.props.MultiInit.getLangData(1056).intl
        return (
             <div>
                <NCModal show = {this.props.showModal} classname='showNumIndex' id="shownum">
                {/*105602BZPT-0053：打印粘贴单*/}
                <NCModal.Header>
                        <NCModal.Title>{multiLang && multiLang.get("105602BZPT-0053")}</NCModal.Title>
                    </NCModal.Header>
                    <NCModal.Body>
                        <div>
                             <div>
                                 <NCFormControl
                                    placeholder={multiLang && multiLang.get("105602BZPT-0070")}
                                    value={this.state.searchValue}
                                    onChange={(searchValue) => {
                                        this.setState({ searchValue:searchValue });
                                    }}
                                 />
                            </div>
                        </div>
                    </NCModal.Body>
                    {/*105602BZCX-0042：确定，105601DJYS-0005：取消*/}
                    <NCModal.Footer>
                        <NCButton colors="primary" onClick={this.save.bind(this)} >{multiLang && multiLang.get("105602BZCX-0042")}</NCButton>
                        <NCButton onClick={this.cancel.bind(this)}>{multiLang && multiLang.get("105601DJYS-0005")}</NCButton>
                    </NCModal.Footer>
                </NCModal>
            </div>
        )
    }
}
export default ShowNum;