import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {createPage,ajax, base} from 'nc-lightapp-front';
const {NCIcon, NCModal, NCButton} = base;
import './newBill.less';
import requestApi from '../requestApi'
import rpBillList from '../rpBillList';

let targetBlank = null
let targetMenuBlank = null

import imgAll from '../../../../public/image/rpbill_list_menu/all.png'
const imgList = [
    require('../../../../public/image/rpbill_list_menu/1.png'),
    require('../../../../public/image/rpbill_list_menu/2.png'),
    require('../../../../public/image/rpbill_list_menu/3.png'),
    require('../../../../public/image/rpbill_list_menu/4.png'),
    require('../../../../public/image/rpbill_list_menu/5.png'),
    require('../../../../public/image/rpbill_list_menu/6.png'),
    require('../../../../public/image/rpbill_list_menu/7.png'),
    require('../../../../public/image/rpbill_list_menu/8.png'),
    require('../../../../public/image/rpbill_list_menu/9.png'),
    require('../../../../public/image/rpbill_list_menu/10.png'),
    require('../../../../public/image/rpbill_list_menu/11.png'),
    require('../../../../public/image/rpbill_list_menu/12.png'),
    require('../../../../public/image/rpbill_list_menu/13.png')
]

class NewBill extends Component {
    constructor(props) {
        super(props)
        this.state = {
            commonBills:[],
            showMore: false,
            showAllNav: false,
            showModal: false,
            menus: []
        }
    }
    componentDidMount() {
        this.menuQry.call(this)

        targetBlank = document.getElementById('target-blank')
        targetMenuBlank = document.getElementById('target-menu-blank')
    }
    // 查询常用菜单
    menuQry(){
        requestApi.usrMenuQry({
            success: (data) => {
                // let allBills = []
                // data.data.allmenulist.map((billOne)=>{
                //     billOne.subFunc.map((tradeOne)=>{
                //         allBills.push({
                //             name: tradeOne.name, 
                //             tradetype: tradeOne.code,
                //             billtype: billOne.code,
                //             isCommon: tradeOne.isCommon
                //         })
                //     })
                // })

                const commonBills = data.data.usermenulist.map( billOne => ({
                    name: billOne.name, 
                    tradetype: billOne.code,
                    billtype: billOne.parentcode
                }))
                
                this.setState({
                    commonBills,
                    showMore: commonBills.length + 1 > 14
                })
            }
        })
    }
    newBill(billtype, tradetype){
        ajax({
            url: '/nccloud/sscrp/rpbill/MakeBillAction.do',
            data:{billtypeCode: billtype, transtypeCode: tradetype},
            success: (data) => {
                this.props.specialOpenTo(data.data.url,
                    {
                        ...data.data.data,
                        scene: 'bz'
                    },
                    (window)=>{
                        if(window){
                            let loop = setInterval(()=> { 
                                if(window.closed) {
                                   
                                 this.props.parentPage.queryData(false);
                                 clearInterval(loop);
                                    }    
                                 }, 500);
                          
   

                        }
                        
                           
                    }
                )
                this.setState({showAllNav: false, showModal: false})
            }
        })
    }
    // 查询所有菜单 并展示
    showMenuControl(){
        requestApi.usrMenuQry({
            success: (data)=>{
                this.setState({menus: data.data.allmenulist,showModal: true},()=>{
                    const menusClientRect = this.refs.menuAll.getBoundingClientRect();
                    const menusControl = document.getElementById('menus-control')
                    menusControl.style.left = (menusClientRect.left - 10) + 'px' 
                    menusControl.style.top = (menusClientRect.top + 20) + 'px'
                    targetBlank.style.height = Math.max(document.body.scrollHeight,document.documentElement.scrollHeight) + 'px'
                    targetBlank.style.width = Math.max(document.body.scrollWidth,document.documentElement.scrollWidth) + 'px'
                })
            }
        })
        // setTimeout(() => {
        //     const menusClientRect = this.refs.menuAll.getBoundingClientRect();
        //     const menusControl = document.getElementById('menus-control')
        //     menusControl.style.left = (menusClientRect.left - 10) + 'px' 
        //     menusControl.style.top = (menusClientRect.top + 20) + 'px'
        //     targetBlank.style.height = Math.max(document.body.scrollHeight,document.documentElement.scrollHeight) + 'px'
        //     targetBlank.style.width = Math.max(document.body.scrollWidth,document.documentElement.scrollWidth) + 'px'
        // }, 20)
    }
    addMenu(data, idxArr){
        let payload = {
            head: {
                areaType: 'form',
                areacode: 'head',
                rows: [{
                    status: '2',
                    values: {
                        transtypecode: {value: data.code}
                    }
                }]
            }
        }
        requestApi.usrMenuSave({
            data: payload,
            success: (data) => {
                if (data.success) {
                    let menus = this.state.menus
                    menus[idxArr[0]].subFunc[idxArr[1]].isCommon = !menus[idxArr[0]].subFunc[idxArr[1]].isCommon
                    this.setState({menus})
                }
            }
        })
    }
    delMenu(data, idxArr){
        let payload = {
            head: {
                areaType: 'form',
                areacode: 'head',
                rows: [{
                    status: '3',
                    values: {
                        transtypecode: {value: data.code}
                    }
                }]
            }
        }
        requestApi.usrMenuSave({
            data: payload,
            success: (data) => {
                if(data.success) {
                    let menus = this.state.menus
                    menus[idxArr[0]].subFunc[idxArr[1]].isCommon = !menus[idxArr[0]].subFunc[idxArr[1]].isCommon
                    this.setState({menus})
                }
            }
        })
    }
    closeMenusControl() {
        this.setState({showModal: false, showMore: false, showAllNav: false})
        this.menuQry.call(this)
        targetBlank.style.height = '0px'
        targetBlank.style.width = '0px'
    }

    render() {
        let multiLang = this.props.MutiInit.getIntl(1056);
        const {showAllNav, showMore, commonBills} = this.state

        const eachNavTop = (index) => {
            if (!this.refs.menuAll) return '0px'
            const {height} = this.refs.menuAll.getBoundingClientRect()
            return Math.floor((index + 1) / 7) * height + 'px'
        }

        const eachNavLeft = (index) => {
            if (!this.refs.menuAll) return '1px'
            const {width} = this.refs.menuAll.getBoundingClientRect()
            return ((index + 1) % 7) * width + 1 + 'px' 
        }

        const currentNav = () => {
            if (this.state.showMore) {
                let ret = []
                if (this.state.showAllNav) {
                    ret = this.state.commonBills.map((one, index, self)=>{
                        if (index < self.length - 1) {
                            return(
                                [
                                    <div
                                        className='child-bill'
                                        key={"commonBill_"+index}
                                        onClick={this.newBill.bind(this, one.billtype, one.tradetype)}
                                        style={{top: eachNavTop(index), left: eachNavLeft(index)}}
                                        title={one.name}
                                    >
                                        <img src={imgList[ index % 12 ]} />
                                        <span>{one.name}</span>
                                    </div>
                                ]
                            ) 
                        } else {
                            return(
                                [
                                    <div
                                        className='child-bill'
                                        key={"commonBill_"+index}
                                        onClick={this.newBill.bind(this, one.billtype, one.tradetype)}
                                        style={{top: eachNavTop(index), left: eachNavLeft(index)}}
                                        title={one.name}
                                    >
                                        <img src={imgList[ index % 12 ]} />
                                        <span>{one.name}</span>
                                        {/* "105602BZPT-0012":"收起", */}
                                        <a
                                            onClick={(ev)=>{
                                                ev.stopPropagation()
                                                this.setState({showAllNav: !this.state.showAllNav})
                                                targetMenuBlank.style.height = '0px'
                                                targetMenuBlank.style.width = '0px'
                                            }}
                                            className="show-all-control"
                                        >{ multiLang && multiLang.get('105602BZPT-0012') }</a>
                                    </div>
                                ]
                            )
                        }
                    }) 
                } else {
                    ret = this.state.commonBills.map((one, index, self)=>{
                        if (index < 13) {
                            if (index < 12) {
                                return(
                                    [
                                        <div
                                            className='child-bill'
                                            key={"commonBill_"+index}
                                            onClick={this.newBill.bind(this, one.billtype, one.tradetype)}
                                            style={{top: eachNavTop(index), left: eachNavLeft(index)}}
                                            title={one.name}
                                        >
                                            <img src={imgList[ index % 12 ]} />
                                            <span>{one.name}</span>
                                        </div>
                                    ]
                                ) 
                            } else {
                                return(
                                    [
                                        <div
                                            className='child-bill'
                                            key={"commonBill_"+index}
                                            onClick={this.newBill.bind(this, one.billtype, one.tradetype)}
                                            style={{top: eachNavTop(index), left: eachNavLeft(index)}}
                                            title={one.name}
                                        >
                                            <img src={imgList[ index % 12 ]} />
                                            <span>{one.name}</span>
                                            {/* "105602BZPT-0013":"更多" */}
                                            <a
                                                onClick={(ev)=>{
                                                    ev.stopPropagation()
                                                    this.setState({showAllNav: !this.state.showAllNav})
                                                    targetMenuBlank.style.height = Math.max(document.body.scrollHeight,document.documentElement.scrollHeight) + 'px'
                                                    targetMenuBlank.style.width = Math.max(document.body.scrollWidth,document.documentElement.scrollWidth) + 'px'
                                                }}
                                                className="show-all-control"
                                            >{ multiLang && multiLang.get('105602BZPT-0013') }</a>
                                        </div>
                                    ]
                                )
                            }
                        }
                    }) 
                }
                return ret
            }
            return this.state.commonBills.map((one, index)=>{
                return(
                    [
                        <div
                            className='child-bill'
                            key={"commonBill_"+index}
                            onClick={this.newBill.bind(this, one.billtype, one.tradetype)}
                            style={{top: eachNavTop(index), left: eachNavLeft(index)}}
                            title={one.name}
                        >
                            <img src={imgList[ index % 12 ]} />
                            <span>{one.name}</span>
                        </div>
                    ]
                )    
            })
        }

        const menus = this.state.menus.map((item, index) => {
            return (
                <div
                    className='item-box'
                    key={`itembox_${index}`}
                >
                    <div className='item-title' style={{textAlign: 'right'}}>
                        <span>{item.name}</span>
                    </div>
                    <div className='item-content clearfix'>
                        {
                            item.subFunc.map((each, idx) => {
                                if (each.isCommon) {
                                    return (
                                        <div className='each-detail-wrap'>
                                            <span
                                                className='each-detail common-detail selected-detail'
                                                key={`itemdetail_${index}`}
                                                onClick={this.newBill.bind(this, item.code, each.code)}
                                            >
                                                {each.name}
                                                <i 
                                                    className='control-icon del-icon'
                                                    onClick={(e) => {
                                                        e = e || window.event
                                                        e.stopPropagation()
                                                        this.delMenu.call(this, each, [index, idx])
                                                    }}
                                                >-</i>
                                            </span>
                                            <span className='parting-line'>|</span>
                                        </div>
                                    )
                                } else {
                                    return (
                                        <div className='each-detail-wrap'>
                                            <span
                                                className='each-detail common-detail'
                                                key={`itemdetail_${index}`}
                                                onClick={this.newBill.bind(this, item.code, each.code)}
                                            >
                                                {each.name}
                                                <i 
                                                    className='control-icon add-icon'
                                                    onClick={(e) => {
                                                        e = e || window.event
                                                        e.stopPropagation()
                                                        this.addMenu.call(this, each, [index, idx])
                                                    }}
                                                >+</i>
                                            </span>
                                            <span className='parting-line'>|</span>
                                        </div>
                                    )
                                }
                            })
                        }
                    </div>
                </div>
            )
        })

        const billBoxHeight =
            showAllNav
            ?
            // (Math.ceil((commonBills.length + 1) / 7) * 42 + 1 + 'px')
            ('85px')
            :
            (
                showMore
                ?
                ('85px')
                :
                ((Math.ceil((commonBills.length + 1) / 7) * 42 + 1 + 'px'))
            )
        
        const borderBottom = (this.state.commonBills.length > 6 && this.state.showAllNav)
            ?
            '1px solid #e6e6e6'
            :
            'none'

        return (
            <div id='newbill' className='clearfix'>
                {/* TODO：多语 */}
                <div className='bill-box' style={{height: billBoxHeight, borderBottom}}>
                    <div
                        className='child-bill'
                        onClick={this.showMenuControl.bind(this)}
                        ref='menuAll'
                    >
                        <img src={imgAll} />
                        {/* "105602BZPT-0014":"全部菜单", */}
                        <span className="all">{ multiLang && multiLang.get('105602BZPT-0014') }</span>
                    </div>
                    {
                        currentNav()
                    }
                    <div className='shadow-layout'
                        style={{
                            height: (Math.ceil((commonBills.length + 1) / 7) * 42 + 1 + 'px'),
                            display: this.state.showAllNav ? 'block' : 'none'
                        }}
                    ></div>
                </div>
                {/* 所有列表展示浮层 */}
                {
                    this.state.showModal && (
                        <div id='menus-control' >
                            <div className='menus-body'>
                                {
                                    menus
                                }
                            </div>
                        </div>
                    )
                }

                <div
                    id='target-blank'
                    onClick={this.closeMenusControl.bind(this)}
                ></div>

                <div
                    id='target-menu-blank'
                    onClick={() => {
                        this.setState({showAllNav: false})
                        targetMenuBlank.style.height = '0px'
                        targetMenuBlank.style.width = '0px'
                    }}
                ></div>
            </div>
        )
    }
}
NewBill = createPage({
    mutiLangCode:'1056'
})(NewBill);
export default NewBill;