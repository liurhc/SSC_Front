import { ajax , getMultiLang } from 'nc-lightapp-front';
import requestApi from '../requestApi'
import listButtonClick from './listButtonClick'

let changedInitialValue = false

export default function(props) {
    window.presetVar = {
        ...window.presetVar,
        pageId: 'rp',
        list: 'rplist',
        list_done:'rplist_done',
        card: 'rpcard'
    };
    const that = this
    props.createUIDom({
    }, (data) => {
        getMultiLang({moduleId: 1056, domainName: 'sscrp',currentLocale: 'zh-CN', callback: (json) => {
            let btns = [];
            let l_render;
            if (this.queryKey.isComplate == "true") {
                btns = ["Print", "Copy", "ztd", "spxq"];
                l_render = (text, record, index) => {
                    return props.button.createOprationButton(["ztd", "Copy", "Print", "spxq"], {
                        area: "list",
                        buttonLimit: 3,
                        onButtonClick: (props, btnKey) => {
                            listButtonClick.call(that, btnKey, record, index);
                        }
                    });
                }
            } else {
                btns = ["Print", "Copy", "RemindingApproval", "ztd", "spxq"];
                l_render = (text, record, index) => {
                    return props.button.createOprationButton(["ztd", "Copy", "RemindingApproval", "Print", "spxq"], {
                        area: "list",
                        buttonLimit: 3,
                        onButtonClick: (props, btnKey) => {
                            listButtonClick.call(that, btnKey, record, index);
                        }
                    });
                }
            }
            //105602BZPT-0045:操作
            let event = {
                label: json['105602BZPT-0045'],
                itemtype: 'customer',
                attrcode: 'opr',
                visible: true,
                width: '160px',
                fixed: 'right',
                render: l_render
            };
            //105602BZPT-0054：打印，105602BZPT-0055：复制，105602BZPT-0056：提醒审批，105602BZPT-0057：粘贴单，105602BZPT-0058：审批详情
            props.button.setButtons(
                [
                    {
                        "id": "0001A41000000006J5B1",
                        "type": "button_main",
                        "key": "Print",
                        "title": json['105602BZPT-0054'],
                        "area": "list",
                        "children": []
                    },
                    {
                        "id": "0001A41000000006J5B1",
                        "type": "button_main",
                        "key": "Copy",
                        "title": json['105602BZPT-0055'],
                        "area": "list",
                        "children": []
                    },
                    {
                        "id": "0001A41000000006J5B1",
                        "type": "button_main",
                        "key": "RemindingApproval",
                        "title": json['105602BZPT-0056'],
                        "area": "list",
                        "children": []
                    },
                    {
                        "id": "0001A41000000006J5B1",
                        "type": "button_main",
                        "key": "ztd",
                        "title": json['105602BZPT-0057'],
                        "area": "list",
                        "children": []
                    },
                    {
                        "id": "0001A41000000006J5B1",
                        "type": "button_main",
                        "key": "spxq",
                        "title": json['105602BZPT-0058'],
                        "area": "list",
                        "children": []
                    }
                ]
            );

            data.template[window.presetVar.list].pagination = false;
            data.template[window.presetVar.list_done].pagination = false;
            
            data.template[window.presetVar.list].items.push(event);
            data.template[window.presetVar.list_done].items.push(event);
            // 未完成模板调整不排序字段
            data.template[window.presetVar.list].items.map((item)=>{
                if(item.attrcode =='imagestate'|| item.attrcode =='opr'|| item.attrcode =='cur_node'){
                    item.isSort = false;
                }else{
                    item.sorter = (e) => {
                        console.log(e);
                    }
                }
            })
            // 已完成模板调整不排序字段
            data.template[window.presetVar.list_done].items.map((item)=>{
                if(item.attrcode =='imagestate'|| item.attrcode =='opr'|| item.attrcode =='cur_node'){
                    item.isSort = false;
                }else{
                    item.sorter = (e) => {
                        console.log(e);
                    }
                }
            })
            props.meta.setMeta(data.template);
            // 单据号列自定义
            //105603JDCX-0023：驳回
            props.table.setTableRender(window.presetVar.list, "billno", (text, record, index) => {
                if(record.isreject && record.isreject.value == 'Y'){
                    return([
                        (
                            <a 
                                className = "billnoa"
                                onClick = {() => {
                                    listButtonClick.call(that, "OpenBill", record, index);
                                }}
                            > { record.billno.value } </a>
                        ),
                        <span className="billno-reject">{json['105603JDCX-0023']}</span>
                    ])
                }else{
                    return (
                        <a 
                            className = "billnoa"
                            onClick = {() => {
                                listButtonClick.call(that, "OpenBill", record, index);
                            }}
                        > { record.billno.value } </a>
                    )
                }
            })
            props.table.setTableRender(window.presetVar.list_done, "billno", (text, record, index) => {
                if(record.isreject && record.isreject.value == 'Y'){
                    return([
                        (
                            <a 
                                className = "billnoa"
                                onClick = {() => {
                                    listButtonClick.call(that, "OpenBill", record, index);
                                }}
                            > { record.billno.value } </a>
                        ),
                        <span className="billno-reject">{json['105603JDCX-0023']}</span>
                    ])
                }else{
                    return (
                        <a 
                            className = "billnoa"
                            onClick = {() => {
                                listButtonClick.call(that, "OpenBill", record, index);
                            }}
                        > { record.billno.value } </a>
                    )
                }
            })
            if (!changedInitialValue) {
                changedInitialValue = true
                let process = props.getUrlParam("process");
                if (process) {
                    data.template.searchArea.items.map((one, index) => {
                        if (one.attrcode == 'process') {
                            one.initialvalue = { value: process };
                        }
                    })
                    this.queryKey['process'] = process;
                }
            }
            if (this.queryKey.isComplate == "true") {
                this.setState({ searcharea: { conditions: data.template.searchArea_done.items }, btns: btns });
                // 初始化动态查询条件
                // data.template.searchArea_done.items.map((one) => {
                //     if (one.initialvalue) {
                //         this.queryKey[one.attrcode] = one.initialvalue.value;
                //     } else {
                //         this.queryKey[one.attrcode] = "";
                //     }
                // })
            } else {
                this.setState({ searcharea: { conditions: data.template.searchArea.items }, btns: btns });
                // 初始化动态查询条件
                // data.template.searchArea.items.map((one) => {
                //     if (one.initialvalue) {
                //         this.queryKey[one.attrcode] = one.initialvalue.value;
                //     } else {
                //         this.queryKey[one.attrcode] = "";
                //     }
                // })
            }
            
            // 查询默认条件数据
            this.queryData();
        }})
    })
}