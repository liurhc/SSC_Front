let getListButtonRender =()=>{
    let l_render;
    if(this.queryKey.isComplate == "true"){
        l_render = (text, record, index) =>{
            return props.button.createOprationButton(["Print","Copy"], {
                area: "list",
                buttonLimit: 3,
                onButtonClick: (props, btnKey) => {
                    listButtonClick(btnKey, record, index);
                }
            });
        }
    }else{
        l_render = (text, record, index) =>{
            return props.button.createOprationButton(["Print","Copy","RemindingApproval"], {
                area: "list",
                buttonLimit: 3,
                onButtonClick: (props, btnKey) => {
                    listButtonClick(btnKey, record, index);
                }
            });
        }
    }
    let event = {
        label: '操作',
        attrcode: 'opr',
        visible: true,
        render: l_render
    };
}
export default {getListButtonRender}