import requestApi from '../requestApi'
import {base, print,toast } from 'nc-lightapp-front'
const {NCMessage} = base

export default function listButtonClick(actionId, data, index, actionFrom) {
    let listData = data;
    if(actionFrom == "detail"){
        listData = this.data.listRows[index].values
    }
    switch(actionId){
        case "OpenBill":
            requestApi.openBill({
                data: {
                    billtypeCode: listData.billtypecode.value, 
                    transtypeCode: listData.transtypecode.value,
                    billid: listData.busiid.value
                },
                success: (data) => {
                    this.props.specialOpenTo(
                        data.data.url,
                        {
                            ...data.data.data,
                            scene: 'bz'
                        },
                        (window)=>{
                            if(window){
                                let loop = setInterval(()=> { 
                                    if(window.closed) {
                                        if(this.queryKey.isComplate=='false'){
                                            this.queryData(false);
                                        }
                                    
                                     clearInterval(loop);
                                        }    
                                     }, 500);
                              
       
    
                            }
                            
                               
                        }
                    )
                }
            })
        break

        case 'Print': // 打印
            requestApi.printAction({
                data: {
                    billtypeCode: listData.billtypecode.value, 
                    transtypeCode: listData.transtypecode.value,
                    billid: listData.busiid.value
                },
                success: (data) => {
                    if (data.success) {
                        print(
                            data.data.filetype,
                            data.data.url, 
                            {
                                billtype: data.data.paradata.billtype,
                                funcode: data.data.paradata.funcode,
                                appcode: data.data.paradata.funcode,
                                nodekey: data.data.paradata.nodekey,
                                userjson:JSON.stringify({"transtype":data.data.paradata.transtype}),
                                oids: data.data.paradata.oids    // 功能节点的数据主键
                                //printTemplateID: data.data.paradata.printTemplateID
                            }
                        ) 
                    }
                }
            })
        break

        case 'Copy': // 复制
            requestApi.copyAction({
                data: {
                    billtypeCode: listData.billtypecode.value, 
                    transtypeCode: listData.transtypecode.value,
                    billid: listData.busiid.value
                },
                success: (data) => {
                    if(data.data.success){
                        this.props.specialOpenTo(
                            data.data.url,
                             {
                                 ...data.data.data,
                                 scene: 'bz'
                            }
                         )
                    } else {
                        toast({color:"warning",content:data.data.message});
                    }
                }
            })
        break

        case 'RemindingApproval': // 提醒审批
            requestApi.urgeApprove({
                data: {
                    billtypeCode: listData.billtypecode.value, 
                    transtypeCode: listData.transtypecode.value,
                    billid: listData.busiid.value
                },
                success: (data) => {
                    if (data.success) {
                        toast({color:"success",content:data.data.message});
                    }
                }
            })
        break

        case 'ztd': // 黏贴单
            this.setState({
                showModalNum: true, 
                operationData: listData,
            })
        break

        case 'spxq': // 审批详情
            const {transtypecode, busiid} = listData
            this.setState({showApproveDetail: true}, () => {
                this.setState(
                    {
                        approveDetailConfig: {
                            billtype: transtypecode.value,
                            billid: busiid.value
                        }
                    }
                )
            })
        break

        default:
            console.log('列表按钮事件', actionId, data, index)
        break
    }
}


