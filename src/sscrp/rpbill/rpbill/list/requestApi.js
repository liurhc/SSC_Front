import {ajax } from 'nc-lightapp-front';

let requestDomain =  '';

let requestApiOverwrite = {
    //单据模板接口
    tpl: (opt) => {
        ajax({
            url: '/nccloud/sscrp/rpbill/RPBillTempQryAction.do',
            data: opt.data,
            success: opt.success
        });
    },
    // 查询接口
    query: (opt) => {
        ajax({
            url: '/nccloud/sscrp/rpbill/RPBillQryAction.do',
            data: opt.data,
            success: opt.success
        });
    },
    // 查询接口
    queryFuzzyKey: (opt) => {
        ajax({
            url: '/nccloud/sscrp/rpbill/QrySearchKeyAction.do',
            data: opt.data,
            success: opt.success
        });
    },
    // 查看单据
    openBill: (opt) => {
        ajax({
            url: '/nccloud/sscrp/rpbill/BrowseBillAction.do',
            data: opt.data,
            success: (data) => {
                opt.success(data)
            }
        });
    },
    // 用户菜单查询 所有
    usrMenuQry: (opt) => {
        ajax({
            url: '/nccloud/sscrp/rpbill/MenuQryAction.do',
            success: (data) => {
                opt.success(data)
            }
        })
    },
    // 用户菜单修改 增加/删除
    usrMenuSave: (opt) => {
        ajax({
            url: '/nccloud/sscrp/rpbill/UserMenuSaveAction.do',
            data: opt.data,
            success: (data) => {
                opt.success(data)
            }
        })
    },
    // 打印
    printAction: (opt) => {
        ajax({
            url: '/nccloud/sscrp/rpbill/PrintAction.do',
            data: opt.data,
            success: (data) => {
                opt.success(data)
            }
        })
    },
    // 复制
    copyAction: (opt) => {
        ajax({
            url: '/nccloud/sscrp/rpbill/CopyAction.do',
            data: opt.data,
            success: (data) => {
                opt.success(data)
            }
        })
    },
    // 提醒审批
    urgeApprove: (opt) => {
        ajax({
            url: '/nccloud/sscrp/rpbill/UrgeApproveAction.do',
            data: opt.data,
            success: (data) => {
                opt.success(data)
            }
        })
    }
}

export default  requestApiOverwrite;