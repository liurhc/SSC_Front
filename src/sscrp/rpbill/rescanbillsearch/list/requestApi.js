import { ajax } from "nc-lightapp-front";

let requestDomain = "";

let requestApiOverwrite = {
  // 查询接口
  query: opt => {
    ajax({
      url: "/nccloud/sscrp/rpbill/RescanBillSearchAction.do",
      data: opt.data,
      success: opt.success
    });
  },
  // 查询接口
  queryFuzzyKey: opt => {
    ajax({
      url: "/nccloud/sscrp/rpbill/RescanKeySearchAction.do",
      data: opt.data,
      success: opt.success
    });
  },
  // 查看单据
  openBill: opt => {
    ajax({
      url: "/nccloud/sscrp/rpbill/BrowseBillAction.do",
      data: opt.data,
      success: data => {
        opt.success(data);
      }
    });
  },
  reScan: opt => {
    ajax({
      url: "/nccloud/web/image/SSCImageReScanAction.do",
      data: opt.data,
      success: opt.success
    });
  },
};

export default requestApiOverwrite;
