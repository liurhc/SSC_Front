import requestApi from "../requestApi";
let queryData = function(isAddPage) {
  if (isAddPage) {
    // 更新查询条件
    this.queryKey.pageinfo.number++;
  } else {
    this.queryKey.pageinfo.number = 1;
  }
  this.queryKey.pageStatus = this.state.pageStatus;
  if (this.queryKey.queryKey == false) {
    return;
  }
  requestApi.query({
    data: this.queryKey,
    success: data => {
      let listRows = [];
      if (isAddPage) {
        this.data.listRows.map(one => {
          listRows.push(this.copyData(one));
        });
      } else {
        this.refs.detail.clearAllDetailData();
        this.pubMessage.querySuccess((((data.data || {})[window.presetVar.list] || {}).pageinfo || {}).totalElements);
      }
      data.data[window.presetVar.list].rows.map(one => {
        listRows.push(this.copyData(one));
      });
      this.data.listRows = listRows;
      this.props.editTable.setTableData(window.presetVar.list, {
        areacode: window.presetVar.list,
        rows: listRows
      });
      let newState = {};
      newState = this.state;
      // 更新页信息
      this.queryKey.pageinfo = data.data[window.presetVar.list].pageinfo;
      if (this.queryKey.pageinfo.totalPages > this.queryKey.pageinfo.number) {
        newState.pageControll = "notend";
      } else if (
        this.queryKey.pageinfo.totalPages == this.queryKey.pageinfo.number
      ) {
        newState.pageControll = "end";
      } else {
        newState.pageControll = "none";
      }
      this.setState(newState);
      setTimeout(() => {
        this.canChangPageFlag = true;
      }, 100);
    }
  });
};
export default { queryData };
