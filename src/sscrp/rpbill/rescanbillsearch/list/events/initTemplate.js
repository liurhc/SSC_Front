import presetVar from "../presetVar";
import {imageuploadBrowserByUrl} from "../../../../rppub/components/image";//修改文件找不到由'sscrp/rppub/components/image'改为相对路径
import {setTableExtendCol} from 'ssccommon/components/profile';
import requestApi from "../requestApi";

export default function(props) {
  const that = this;
  props.createUIDom({}, function(data) {
    let meta = data.template;
    meta[window.presetVar.searchArea].items.map(one => {
      if(one.attrcode == "pk_tradetype"){
          one.fieldValued = 'refcode';
      }
      if(one.attrcode == "pk_org"){
        one.isRunWithChildren =false;
        one.isShowDisabledData =true;
        one.queryCondition = () => {
          let paramurl = getRequest1(parent.window.location.hash);
          //如果没取到应用编码再取一下外层
          if(!paramurl.c){
              paramurl = getRequest1(parent.parent.window.location.hash);
          }
          return {TreeRefActionExt:'nccloud.web.sscrp.rpbill.action.OrgTreeRefSqlBuilder',DataPowerOperationCode:'fi',/**使用权组**/isDataPowerEnable:'Y', appcode : paramurl.c}; // 根据pk_group过滤
      };
    }
      dealRefShow(one);
    });
    //设置表格的扩展按钮列
    setTableExtendCol(props, meta, [{
      areaId: presetVar.list,
      btnAreaId: presetVar.list+'_inner',
      buttonVisible: (record, inedx) =>{
          return ['rescan']
      },
      onButtonClick: (buttonId, record, index) => {
        console.log(record.values.pk_bill);
        var billInfoMap = {};
        //基础字段 单据pk,单据类型，交易类型，单据的组织
        billInfoMap.pk_billid = record.values.busiid.value;
        billInfoMap.pk_billtype = record.values.billtypecode.value;
        billInfoMap.pk_tradetype = record.values.transtypecode.value;
        billInfoMap.pk_org = record.values.pk_org.value;

        //影像所需 FieldMap
        billInfoMap.BillType = record.values.billtypecode.value;
        billInfoMap.BillDate = record.values.creationtime.value;
        billInfoMap.Busi_Serial_No = record.values.busiid.value;
        billInfoMap.pk_billtype = record.values.billtypecode.value;
        billInfoMap.OrgNo = record.values.pk_org.value;
        billInfoMap.BillCode = record.values.billno.value;
        billInfoMap.OrgName = record.values.pk_org.display;
        billInfoMap.Cash = record.values.amount.value;
        billInfoMap.billmaker = record.values.billmaker.value;
        billInfoMap.billdate = record.values.billdate.value;

        requestApi.reScan({
          data : billInfoMap,
          success : data => {
            if (data.data.success == "true") {
                var factoryCode = data.data.factoryCode;
                var ipAddr = data.data.ipaddr;
                var paraStr = '';
                if (factoryCode == "tchzt2") {
                    paraStr = data.data.parastr;
                } else {
                    paraStr = encodeURI(encodeURI(data.data.parastr));
                }
                imageuploadBrowserByUrl(factoryCode, ipAddr, paraStr);
            } else {
                alert(data['error']['message']);
            }
          }
        })
      }
    }]);
    
    //单据编号设置超链接
    // this.props.renderItem('table', presetVar.list, 'billno',<div>1212</div>);
    meta[presetVar.list].items.map(one => {
      if(one.attrcode == "billno"){
        one.renderStatus = 'browse',
          one.render = (text, record, index) => {
            return (
              <a
                className="billnoa"
                onClick={() => {
                  requestApi.openBill({
                    data: {
                      billtypeCode: record.values.billtypecode.value,
                      transtypeCode: record.values.transtypecode.value,
                      billid: record.values.busiid.value
                    },
                    success: data => {
                      props.specialOpenTo(data.data.url, {
                        ...data.data.data,
                        scene: "bzcx"
                      });
                    }
                  });
                }}
              >
                {record.values.billno.value}
              </a>
            );
      }
    }})
    props.meta.setMeta(meta);

    props.button.setButtons(data.button);
  });
  const dealRefShow = function(data) {
    if (data.itemtype == "refer") {
      if (data.fieldDisplayed == "code") {
        data.fieldDisplayed = "refcode";
      } else if (data.fieldDisplayed == "name") {
        data.fieldDisplayed = "refname";
      }
    }
  };
}

function getRequest1(url) {
  if (!url)
      url = document.location.search;
  var theRequest = new Object();
  if (url.indexOf("?") != -1) {
      var str = url.substring(url.indexOf("?") + 1);
      var strs = str.split("&");
      for (var i = 0; i < strs.length; i++) {
          theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
      }
  }
  return theRequest;
}
export{getRequest1}