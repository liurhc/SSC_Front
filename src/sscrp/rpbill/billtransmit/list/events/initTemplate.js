import presetVar from "../presetVar";
import { listButtonClick } from "./index";
import {getMultiLang} from 'nc-lightapp-front';

export default function(props) {
  const that = this;
  props.createUIDom(
    {},
    function(data) {
      getMultiLang({moduleId: 1056, domainName: 'sscrp',currentLocale: 'zh-CN', callback: (json) => {
        let meta = data.template;
        meta["transferSearchArea"].items.map(one => {
          if (one.attrcode == "pk_tradetype") {
            one.queryCondition = { searchScope: "transmitbill" };
            one.fieldValued =  'refcode';
          }
          dealRefShow(one);
        });
        meta["newSearchArea"].items.map(one => {
          if (one.attrcode == "new_billmaker") {
            one.queryCondition = { enablestate: 2 };
          }
          dealRefShow(one);
        });
        //105602BZPT-0045:操作,105602BZPT-0046:移除
        meta.selectedlist.items.push({
          label: json['105602BZPT-0045'],
          attrcode: "opr",
          visible: true,
          fixed: 'right',
          width: '80px',
          render: (text, record, index) => {
            return (
              <a
                className="selected-remove-one"
                onClick={() => {
                  listButtonClick.call(that, "remove", record, index);
                }}
              >
                {json['105602BZPT-0046']}
              </a>
            );
          }
        });
        props.meta.setMeta(meta);
        props.button.setButtons(data.button);

        props.table.setTableRender(
          presetVar.list,
          "billno",
          (text, record, index) => {
            return (
              <a
                className="billnoa"
                onClick={() => {
                  listButtonClick.call(that, "OpenBill", record, index);
                }}
              >
                {record.billno.value}
              </a>
            );
          }
        );
        props.table.setTableRender(
          "selectedlist",
          "billno",
          (text, record, index) => {
            return (
              <a
                className="billnoa"
                onClick={() => {
                  listButtonClick.call(that, "OpenBill", record, index);
                }}
              >
                {record.billno.value}
              </a>
            );
          }
        );
        // 自定义状态列
        props.table.setTableRender(
          presetVar.list,
          "billstatus",
          (text, record, index) => {
            return (
              <div className="bill-status-wrap">
                <span
                  className={
                    record.billstatus.value == "0"
                      ? "not-complete-status"
                      : "complete-status"
                  }
                />
                <span>{record.billstatus.display}</span>
              </div>
            );
          }
        );
      }})
    }
  );
  const dealRefShow = function(data) {
    if (data.itemtype == "refer") {
      if (data.fieldDisplayed == "code") {
        data.fieldDisplayed = "refcode";
      } else if (data.fieldDisplayed == "name") {
        data.fieldDisplayed = "refname";
      }
    }
  };
}
