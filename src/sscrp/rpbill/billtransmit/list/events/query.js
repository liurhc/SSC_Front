import requestApi from "../requestApi";
let queryData = function(isAddPage) {
  if (isAddPage) {
    // 更新查询条件
    this.queryKey.pageinfo.number++;
  } else {
    this.queryKey.pageinfo.number = 1;
  }
  this.queryKey.pageStatus = this.state.pageStatus;
  if (this.queryKey.queryKey == false) {
    return;
  }
  if (this.state.pageStatus === "BusinessTransfer") {
    let billmakerflag = false;
    if (this.queryKey.queryKey && this.queryKey.queryKey.conditions) {
      this.queryKey.queryKey.conditions.map(one => {
        if (one.field == "old_billmaker") {
          billmakerflag = true;
        }
      });
    }
    if (!billmakerflag) {
      return;
    }
  }
  requestApi.query({
    data: this.queryKey,
    success: data => {
      let listRows = [];
      if (isAddPage) {
        this.data.listRows.map(one => {
          listRows.push(this.copyData(one));
        });
      } else {
        this.refs.detail.clearAllDetailData();
      }
      data.data[window.presetVar.list].rows.map(one => {
        listRows.push(this.copyData(one));
      });
      this.data.listRows = listRows;
      // let selectedList = [];
      // if (this.state.pageStatus === "BusinessTransfer") {
      //   let rplist = this.props.table.getAllTableData("rplist").rows;
      //   if (rplist && rplist.length > 0) {
      //     rplist.forEach((ele, index) => {
      //       if (ele.selected) selectedList.push(index);
      //     });
      //   }
      // }
      this.props.table.setAllTableData(window.presetVar.list, {
        areacode: window.presetVar.list,
        rows: listRows
      });
      // if (this.state.pageStatus === "BusinessTransfer") {
      //   setTimeout(() => {
      //     this.props.table.selectTableRows("rplist", selectedList, true);
      //   }, 100);
      // }
      // 设置缩略数据
      this.refs.detail.addDetailData(data.data[window.presetVar.card]);
      let newState = {};
      newState = this.state;
      // 更新页信息
      this.queryKey.pageinfo = data.data[window.presetVar.list].pageinfo;
      if (this.queryKey.pageinfo.totalPages > this.queryKey.pageinfo.number) {
        newState.pageControll = "notend";
      } else if (
        this.queryKey.pageinfo.totalPages == this.queryKey.pageinfo.number
      ) {
        newState.pageControll = "end";
      } else {
        newState.pageControll = "none";
      }
      newState.selectAmount = 0;
      this.setState(newState);
      setTimeout(() => {
        this.canChangPageFlag = true;
      }, 100);
      //this.pubMessage.querySuccess();
      this.pubMessage.querySuccess((((data.data || {})[window.presetVar.list] || {}).pageinfo || {}).totalElements);
    }
  });
};
export default { queryData };
