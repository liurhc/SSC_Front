import requestApi from "../requestApi";
export default function listButtonClick(actionId, data, index, actionFrom) {
  // console.log('listButtonClick:' + actionId);
  let listData = data;
  if (actionFrom == "detail") {
    listData = this.data.listRows[index].values;
  }
  switch (actionId) {
    case "OpenBill":
      requestApi.openBill({
        data: {
          billtypeCode: listData.billtypecode.value,
          transtypeCode: listData.transtypecode.value,
          billid: listData.busiid.value
        },
        success: data => {
          this.props.specialOpenTo(data.data.url, {
            ...data.data.data,
            scene: "bzcx"
          });
        }
      });
      break;
    case "remove":
      let selectedList = this.props.table
        .getAllTableData("rplist")
        .rows.map(ele => ele.values.billno.value);
      let idx = -1;
      selectedList.forEach((ele, _index) => {
        if (ele == data.billno.value) idx = _index;
      });
      this.props.table.selectTableRows("rplist", [idx], false);
      const that = this;
      setTimeout(() => {
        setNewData.call(that, index);
        that.setState({selectAmount: that.state.selectAmount - 1})
      }, 100);
      break;
    default:
      break;
  }
}

const setNewData = function(index) {
  let list = this.props.table.getAllTableData("selectedlist").rows;
  list.splice(index, 1);
  this.props.table.setAllTableData("selectedlist", {
    areacode: "selectedlist",
    rows: list
  });
};
