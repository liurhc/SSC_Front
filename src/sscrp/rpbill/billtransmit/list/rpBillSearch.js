import { Component } from "react";
import { createPage, base, toast, createPageIcon } from "nc-lightapp-front";
const { NCIcon, NCModal, NCButton, NCAffix, NCBackBtn } = base;
import WorkbenchDetail from "ssccommon/components/workbench-detail";
import pubMessage from 'ssccommon/utils/pubMessage';
import { initTemplate, listButtonClick, query } from "./events/index";
import presetVar from "./presetVar";
import requestApi from "./requestApi";
import "./index.less";
import './index.css'

class RpBillSearch extends Component {
  constructor(props) {   
    super(props);
	let multiLang = this.props.MutiInit.getIntl(1056);
    this.state = {
      showType: "list",
      fuzzyquerychild: [],
      menuShowType: {
        type: "icon-Group",
        //title: "缩略显示"
        // "105602BZCX-0008":"缩略显示"
        title:multiLang && multiLang.get('105602BZCX-0008')
      },
      pageStatus: "BusinessTransfer",
      billMaker: null,
      selectAmount: 0,
      newMaker: null,
      showModal: false
    };
    this.pubMessage = new pubMessage(props);
    // 查询条件
    this.queryKey = {
      fuzzyQueryKey: [],
      // 页信息
      pageinfo: {
        number: 1,
        size: 20,
        totalElements: 0,
        totalPages: 0
      },
      queryKey: {}
    };
    // 缓存查询结果数据
    this.data = {
      listRows: [],
      cardRows: []
    };
    // 延时控制标记
    // 翻页
    this.canChangPageFlag = true;
    // 页面初始化
    initTemplate.call(this, props);
  }
  componentDidMount() {
    // Firefox
    window.addEventListener("DOMMouseScroll", ev => {
      if (this.state.pageStatus === "selected") return;
      ev = ev || window.event;
      let onOff = null;
      if (ev.detail < 0) {
        onOff = true;
      } else {
        onOff = false;
      }
      let t = document.documentElement.scrollTop || document.body.scrollTop;
      let h =
        document.documentElement.scrollHeight || document.body.scrollHeight;
      let bh =
        document.documentElement.clientHeight || document.body.clientHeight;
      if (
        !onOff &&
        h - bh - t < 30 &&
        this.queryKey.pageinfo.totalPages > this.queryKey.pageinfo.number &&
        this.canChangPageFlag
      ) {
        this.canChangPageFlag = false;
        setTimeout(() => {
          query.queryData.call(this, true);
        }, 300);
      }
    });
    // 其他浏览器
    window.onmousewheel = ev => {
      if (this.state.pageStatus === "selected") return;
      ev = ev || window.event;
      let onOff = null;
      if (ev.wheelDelta > 0) {
        onOff = true;
      } else {
        onOff = false;
      }
      let t = document.documentElement.scrollTop || document.body.scrollTop;
      let h =
        document.documentElement.scrollHeight || document.body.scrollHeight;
      let bh =
        document.documentElement.clientHeight || document.body.clientHeight;
      if (
        !onOff &&
        h - bh - t < 30 &&
        this.queryKey.pageinfo.totalPages > this.queryKey.pageinfo.number &&
        this.canChangPageFlag
      ) {
        this.canChangPageFlag = false;
        setTimeout(() => {
          query.queryData.call(this, true);
        }, 300);
      }
    };
  }
  menuClick = () => {
    // "105602BZCX-0008":"缩略显示",
    // "105602BZCX-0009":"列表显示"
    let multiLang = this.props.MutiInit.getIntl(1056);
    if (this.state.menuShowType.type == "icon-xingzhuang") {
      this.setState({
        menuShowType: {
          type: "icon-Group",
          title: multiLang && multiLang.get("105602BZCX-0008")
        },
        showType: "list"
      });
    } else {
      this.setState({
        menuShowType: {
          type: "icon-xingzhuang",
          title: multiLang && multiLang.get("105602BZCX-0009")
        },
        showType: "card"
      });
    }
  };
  // 模糊查询选择
  onFuzzySelected(data) {
    this.setState({ fuzzyquerychild: [] });
    // 更新查询条件
    this.queryKey.fuzzyQueryKey = data;
    // 查询数据
    query.queryData.call(this);
  }
  // 复制列表数据
  copyData(data) {
    let newData = {};
    newData.values = {};
    for (let child in data.values) {
      newData.values[child] = {
        display: data.values[child].display,
        value: data.values[child].value,
        scale: data.values[child].scale
      };
    }
    return newData;
  }
  searchClick(props, data, type) {
    this.queryKey.queryKey = data;

    query.queryData.call(this);
  }
  // 转出人
  changeBillMaker = (key, data) => {
    if (key === "old_billmaker") {
      this.setState({ billMaker: data });
      this.props.table.selectTableRows("rplist", [], false);
      // this.props.table.setAllTableData("selectedlist", { rows: [] });
    }
  };
  // 接收人
  changeNewMaker = (key, data) => {
    this.setState({ newMaker: data });
  };
  // 单选
  simpleOnselected = (props, listKey, data, index, check) => {
    if (check) {
      this.setState({ selectAmount: this.state.selectAmount + 1 });
    } else {
      this.setState({ selectAmount: this.state.selectAmount - 1 });
    }
  };
  // 全选
  allselect = (props, key, check, amount) => {
    if (check) {
      this.setState({ selectAmount: amount });
    } else {
      this.setState({ selectAmount: 0 });
    }
  };
  // 查看已选
  viewselected = () => {
    let selectedList = this.props.table
      .getCheckedRows("rplist")
      .map(ele => ele.data);
    this.props.table.setAllTableData("selectedlist", {
      areacode: "selectedlist",
      rows: selectedList
    });
    this.setState({ pageStatus: "selected" });
  };
  // 确认提交
  sureSubmitHandle = () => {
    let multiLang = this.props.MutiInit.getIntl(1056);
    // "105602BZCX-0032":"没有选中单据",
    // "105602BZCX-0033":"没有选择业务接收人",
    // "105602BZCX-0034":"没有选择业务转出人",
    let selectedList = this.props.table.getCheckedRows("rplist");
    if (!selectedList || selectedList.length == 0) {
      toast({
        color: "warning",
        content: multiLang && multiLang.get("105602BZCX-0032")
      });
      return;
    }
    if (!this.state.newMaker || !this.state.newMaker.refpk) {
      toast({
        color: "warning",
        content: multiLang && multiLang.get("105602BZCX-0033")
      });
      return;
    }
    if (!this.state.billMaker || !this.state.billMaker.refpk) {
      toast({
        color: "warning",
        content: multiLang && multiLang.get("105602BZCX-0034")
      });
      return;
    }
    if (this.state.newMaker.refpk == this.state.billMaker.refpk) {
      toast({
        color: "warning",
        content: multiLang && multiLang.get("105602BZCX-0043")
      });
      return;
    }
    this.setState({ showModal: true });
  };
  // 确认提交
  billMakerTransmit = () => {
    let params = {
      new_billmaker: this.state.newMaker.refpk,
      old_billmaker: this.state.billMaker.refpk,
      pk_bills: this.props.table
        .getCheckedRows("rplist")
        .map(ele => ele.data.values.pk_bill.value)
    };
    let multiLang = this.props.MutiInit.getIntl(1056);
    // "105602BZCX-0035":"业务转交成功",
    // "105602BZCX-0036":"业务转交失败",
    requestApi.billMakerTransmit({
      data: params,
      success: data => {
        if (data.success && data.data.transmit) {
          toast({
            content: multiLang && multiLang.get("105602BZCX-0035")
          });
          this.props.table.setAllTableData(window.presetVar.list, {
            // 清空
            areacode: window.presetVar.list,
            rows: []
          });
        } else {
          toast({
            color: "warning",
            content: multiLang && multiLang.get("105602BZCX-0036")
          });
        }
        this.setState({ showModal: false, selectAmount: 0 });
      }
    });
  };
  // 
  onRowDoubleClick = (values, idx, props, events) => {
    requestApi.openBill({
      data: {
        billtypeCode: values.billtypecode.value,
        transtypeCode: values.transtypecode.value,
        billid: values.busiid.value
      },
      success: data => {
        this.props.specialOpenTo(data.data.url, {
          ...data.data.data,
          scene: "bzcx"
        });
      }
    });
  };

  render() {
    const { table, search, button } = this.props;
    let { createButtonApp } = button;
    const { createSimpleTable } = table;
    let { NCCreateSearch } = search;
    const { pageStatus } = this.state;
    let multiLang = this.props.MutiInit.getIntl(1056);
    const {onRowDoubleClick} = this

    let isShow = type => {
      if (this.state.showType == "list" && type == "list") {
        return "show";
      } else if (this.state.showType == "card" && type == "card") {
        return "show";
      } else {
        return "hide";
      }
    };

    let addMoreOnOff = this.data.listRows.length > 0 && this.queryKey.pageinfo.totalPages > this.queryKey.pageinfo.number

    const BusinessTransfer = (
      <div className="container">
        <NCAffix offsetTop={0}>
          <div className="title-block clearfix">
            {/* "105602BZCX-0031":"业务转交test" */}
            {createPageIcon()}
            <h3>{multiLang && multiLang.get("105602BZCX-0031")}</h3>
          </div>
          <div className="create-search business-transfer--create-search">
            {NCCreateSearch(
              // "105602BZCX-0024":"快速查询"
              "transferSearchArea", //模块id
              {
                clickSearchBtn: this.searchClick.bind(this),
                defaultConditionsNum: 1,
                showAdvBtn: false,
                // searchBtnName: multiLang && multiLang.get('105602BZCX-0024') ,
                onAfterEvent: this.changeBillMaker
              }
            )}
          </div>
        </NCAffix>
        <div className={isShow("list")} style={{ paddingBottom: "40px" }}>
          {createSimpleTable(presetVar.list, {
            showIndex: true,
            showCheck: true,
            onSelected: this.simpleOnselected,
            onSelectedAll: this.allselect,
            onRowDoubleClick
          })}
          {/* "105602BZCX-0048":"滑动加载更多" */}
          <div
              className='scroll-add-more'
              style={{display: addMoreOnOff ? 'block' : 'none'}}
          >{multiLang && multiLang.get('105602BZCX-0048')}</div>
        </div>
        <div className={isShow("card")} style={{ paddingBottom: "40px" }}>
          <WorkbenchDetail
            ref="detail"
            btns={this.state.btns}
            props={this.props}
            doAction={listButtonClick.bind(this)}
            getBtns={() => {
              if (this.queryKey.isComplate == "true") {
                return ["Print", "Copy"];
              } else {
                return ["Print", "Copy", "RemindingApproval"];
              }
            }}
          />
          <div
              className='scroll-add-more'
              style={{display: addMoreOnOff ? 'block' : 'none'}}
          >{multiLang && multiLang.get('105602BZCX-0048')}</div>
        </div>
        {/* // "105602BZCX-0026":"合计：",
                    // "105602BZCX-0027":"条",
                    // "105602BZCX-0028":"转出人：" ,
                    //"105602BZCX-0047":"接收人"*/}
        <div className="controler-box-fixed clearfix">
          <div className="content">
            <div className="left-area">
              {createButtonApp({
                area: "page_footer_left",
                onButtonClick: this.viewselected
              })}
              <span>
                {multiLang && multiLang.get("105602BZCX-0026")}{" "}
                {this.state.selectAmount}{" "}
                {multiLang && multiLang.get("105602BZCX-0027")}
              </span>
              <span>
                {multiLang && multiLang.get("105602BZCX-0028")}
                {this.state.billMaker ? this.state.billMaker.refname : ""}
              </span>
              <span>
                {multiLang && multiLang.get("105602BZCX-0047")}{" "}
              </span>
              {NCCreateSearch("newSearchArea", {
                hideSearchCondition: true,
                hideBtnArea: true,
                onAfterEvent: this.changeNewMaker
              })}
            </div>
            <div className="right-area">
              {createButtonApp({
                area: "page_footer_right",
                onButtonClick: this.sureSubmitHandle
              })}
            </div>
          </div>
        </div>
      </div>
    );

    const selected = (
      <div className="container">
        <NCAffix offsetTop={0}>
          <div className="title-block clearfix">
            <NCBackBtn onClick={() => {
                this.setState({
                  pageStatus: "BusinessTransfer"
                });
              }}/>
            {/* "105602BZCX-0037":"已选列表" */}
            <h3>{multiLang && multiLang.get("105602BZCX-0037")}</h3>
            {createButtonApp({
              area: "selected_header",
              onButtonClick: () => {
                this.props.table.selectAllRows("rplist", false);
                this.props.table.setAllTableData("selectedlist", {
                  areacode: "selectedlist",
                  rows: []
                });
                this.setState({ selectAmount: 0 });
              }
            })}
          </div>
        </NCAffix>
        <div style={{ paddingBottom: "40px" }}>
          {createSimpleTable("selectedlist", {
            showIndex: true,
            onRowDoubleClick
          })}
        </div>
        <div className="controler-box-fixed clearfix">
          <div className="content">
            <div className="left-area">
              {createButtonApp({
                area: "selected_left",
                onButtonClick: () => {
                  this.setState({
                    pageStatus: "BusinessTransfer"
                  });
                }
              })}
              {/* "105602BZCX-0026":"合计：",
                "105602BZCX-0027":"条",
                "105602BZCX-0028":"转出人：",
                "105602BZCX-0047":"接收人" */}
              <span>
                {multiLang && multiLang.get("105602BZCX-0026")}{" "}
                {this.state.selectAmount}{" "}
                {multiLang && multiLang.get("105602BZCX-0027")}
              </span>
              <span>
                {multiLang && multiLang.get("105602BZCX-0028")}{" "}
                {this.state.billMaker ? this.state.billMaker.refname : ""}
              </span>
              <span>
                {multiLang && multiLang.get("105602BZCX-0047")}{" "}
              </span>
              {NCCreateSearch("newSearchArea", {
                hideSearchCondition: true,
                hideBtnArea: true,
                onAfterEvent: this.changeNewMaker
              })}
            </div>
            <div className="right-area">
              {createButtonApp({
                area: "page_footer_right",
                onButtonClick: this.sureSubmitHandle
              })}
            </div>
          </div>
        </div>
      </div>
    );

    return (
      <div id="rpBillList" className={addMoreOnOff ? 'add-more-wrap' : ''}>
        {/* 业务转交 */}
        {/*定位到底部，白色背景铺满*/}
        <div className="white"></div>
        <div
          style={{
            display: pageStatus === "BusinessTransfer" ? "block" : "none"
          }}
        >
          {BusinessTransfer}
        </div>
        {/* 已选列表 */}
        <div style={{ display: pageStatus === "selected" ? "block" : "none" }}>
          {selected}
        </div>
        {/* 确认转交弹窗 */}
        <NCModal show={this.state.showModal}>
          {/* "105602BZCX-0038":"业务转交提示",
                    "105602BZCX-0039":"确定进行业务转交？",
                    "105602BZCX-0040":"注：请仔细核对信息，谨慎操作！",
                    "105602BZCX-0041":"关闭",
                    "105602BZCX-0042":"确认" */}
          <NCModal.Header>
            <NCModal.Title>
              {multiLang && multiLang.get("105602BZCX-0038")}
            </NCModal.Title>
          </NCModal.Header>
          <NCModal.Body>
            <div>{multiLang && multiLang.get("105602BZCX-0039")}</div>
            <span
              style={{
                color: "#ccc",
                fontSize: "12px"
              }}
            >
              {multiLang && multiLang.get("105602BZCX-0040")}
            </span>
          </NCModal.Body>
          <NCModal.Footer>
            <NCButton colors="primary" onClick={this.billMakerTransmit}>
              {/* multiLang && multiLang.get("105602BZCX-0042") */ '确定'}
            </NCButton>
            <NCButton
              onClick={() => {
                this.setState({ showModal: false });
              }}
            >
              {/* multiLang && multiLang.get("105602BZCX-0041") */ '取消'}
            </NCButton>
          </NCModal.Footer>
        </NCModal>
      </div>
    );
  }
}
RpBillSearch = createPage({
  mutiLangCode: "1056"
})(RpBillSearch);
export default RpBillSearch;
