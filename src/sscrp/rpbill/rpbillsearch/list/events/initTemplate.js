import presetVar from "../presetVar";
import { listButtonClick } from "./index";

export default function(props) {
  const that = this;
  props.createUIDom({}, function(data) {
    let meta = data.template;
    meta[window.presetVar.searchArea].items.map(one => {
      if(one.attrcode == "pk_tradetype"){
          one.fieldValued = 'refcode';
      }
      if(one.attrcode == "pk_org"){
        one.isRunWithChildren =false;
        one.isShowDisabledData =true;
    }
      dealRefShow(one);
    });
    props.meta.setMeta(meta);
        data.template[window.presetVar.list].items.map((item)=>{
            if(item.attrcode==='billno' 
                || item.attrcode==='pk_tradetype'
                || item.attrcode==='pk_currtype'
                || item.attrcode==='remark'
                || item.attrcode==='amount'
                || item.attrcode==='pk_sscuser'
                || item.attrcode==='billdate'
                || item.attrcode==='billmaker'
                || item.attrcode=="pk_org"
                || item.attrcode=="billstatus"){
                item.isSort = true;
            }else{
                item.isSort = false;
            }
        })
    props.table.setTableRender(
      presetVar.list,
      "billno",
      (text, record, index) => {
        return (
          <a
            className="billnoa"
            onClick={() => {
              listButtonClick.call(that, "OpenBill", record, index);
            }}
          >
            {record.billno.value}
          </a>
        );
      }
    );
    // 自定义状态列
    props.table.setTableRender(
      presetVar.list,
      "billstatus",
      (text, record, index) => {
        return (
          <div className="bill-status-wrap">
            <span
              className={
                record.billstatus.value == "0"
                  ? "not-complete-status"
                  : "complete-status"
              }
            />
            <span>{record.billstatus.display}</span>
          </div>
        );
      }
    );
  });
  const dealRefShow = function(data) {
    if (data.itemtype == "refer") {
      if (data.fieldDisplayed == "code") {
        data.fieldDisplayed = "refcode";
      } else if (data.fieldDisplayed == "name") {
        data.fieldDisplayed = "refname";
      }
    }
  };
}
