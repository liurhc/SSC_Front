import 'ssccommon/components/globalPresetVar';

/**页面全局变量 */
let currentVar = {
    pageId: 'rpbillsearch',
    list: 'rplist',
    card: 'rpcard',
    searchArea: 'searchArea'
}
window.presetVar = {
    ...window.presetVar,
    ...currentVar
};
export default currentVar
