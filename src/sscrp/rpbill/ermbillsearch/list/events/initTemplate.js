import {ajax} from 'nc-lightapp-front';
import requestApi from '../requestApi'
import presetVar from '../presetVar'
import {listButtonClick} from './index'

export default function (props) {
	const that = this;
    props.createUIDom(
        {
            //pagecode:     window.presetVar.pageId,//页面id
            //appid: ' '//注册按钮的id
        },
        function (data) {
            let meta = data.template;
            meta[window.presetVar.searchArea].items.map((one)=>{
                if(one.attrcode == "pk_tradetype"){
                    one.queryCondition =  {'searchScope': 'erm'};
                    one.fieldValued =  'refcode';
                }
                if(one.attrcode == "pk_org"){
                    one.isRunWithChildren =false;
                    one.isShowDisabledData =true;
                    //one.queryCondition =  {'TreeRefActionExt': 'nccloud.web.org.closeaccbook.tool.BusinessUnitTreeRefExt'};
                }
                dealRefShow(one);
            })
            // 过滤不排序的字段
            meta[window.presetVar.list].items.map((item)=>{
                if(item.attrcode =='cur_node' || item.attrcode =='billstatus'){
                    item.isSort = false;
                }else{
                    item.sorter = (e) => {
                        console.log(e);
                    }
                }
            })
            props.meta.setMeta(meta);

            props.table.setTableRender(presetVar.list, "billno", (text, record, index)=>{
                return(
                    <a className="billnoa" onClick={() => {
                        listButtonClick.call(that, "OpenBill", record, index);
                    }}>{record.billno.value}</a>
                )
            })

            // 自定义状态列
            props.table.setTableRender(presetVar.list, "billstatus", (text, record, index)=>{
                return(
                    <div className='bill-status-wrap'>
                        <span className={record.billstatus.value == '0' ? 'not-complete-status' : 'complete-status'}></span>
                        <span>{record.billstatus.display}</span>
                    </div>
                )
            })
        }
    )

    const dealRefShow = function (data){
        if(data.itemtype == "refer"){
            if(data.fieldDisplayed == "code"){
                data.fieldDisplayed = "refcode";
            }else if(data.fieldDisplayed == "name"){
                data.fieldDisplayed = "refname";
            }
        }
    }
}
