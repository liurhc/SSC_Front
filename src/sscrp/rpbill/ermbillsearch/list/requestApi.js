import {ajax } from 'nc-lightapp-front';

let requestDomain =  '';

let requestApiOverwrite = {
    // 查询接口
    query: (opt) => {
        ajax({
            url: '/nccloud/sscrp/rpbill/BillSearchForErmAction.do',
            data: opt.data,
            success: opt.success
        });
    },
    // 查询接口
    queryFuzzyKey: (opt) => {
        ajax({
            url: '/nccloud/sscrp/rpbill/QrySearchKeyForErmBillSearchAction.do',
            data: opt.data,
            success: opt.success
        });
    },
    // 查看单据
    openBill: (opt) => {
        ajax({
            url: '/nccloud/sscrp/rpbill/BrowseBillAction.do',
            data: opt.data,
            success: (data) => {
                opt.success(data)
            }
        });
    },
    // 用户菜单查询 所有
    usrMenuQry: (opt) => {
        ajax({
            url: '/nccloud/sscrp/rpbill/MenuQryAction.do',
            success: (data) => {
                opt.success(data)
            }
        })
    },
    // 用户菜单修改 增加/删除
    usrMenuSave: (opt) => {
        ajax({
            url: '/nccloud/sscrp/rpbill/UserMenuSaveAction.do',
            data: opt.data,
            success: (data) => {
                opt.success(data)
            }
        })
    }
}

export default  requestApiOverwrite;