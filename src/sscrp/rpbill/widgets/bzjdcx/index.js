import React, { Component } from 'react';
import './index.less';
import {ajax, createPage, getMultiLang} from 'nc-lightapp-front'

 class Test1 extends Component {
	constructor(props) {
		super(props);
		this.state = {
			json: {},
			res: {}
		}
	}
	componentWillMount() {
		let callback = (json) => {
			this.setState({json})
		}
		getMultiLang({moduleId: 1056, currentLocale: 'zh-CN',domainName: 'sscrp',callback})
	}	
	componentDidMount() {
		ajax({
		  url:  '/nccloud/sscrp/rpbill/RPbillScheduleWidgetAction.do',
		  data: {},
		  loading: false,
		  success: (res) => {
              this.setState({res: res.data});
              console.log(this.state.res)
              // document.getElementById('tocommit').innerHTML=(res.data.tocommit);
              // document.getElementById('todo').innerHTML=(res.data.todo);
              // document.getElementById('reject').innerHTML=(res.data.reject);
		  }
	  });
	}
	open = (event, para = null) => {
		event.stopPropagation()
		if(para){
			// window.parent.openNew({code:'105602BZPT',name:'报账平台'}, {tab: para});
			// "105603JDCX-0019":"报账平台",
			this.props.openTo(
				'/sscrp/rpbill/rpbill/list/index.html',
				{
					pagecode: '105602BZPT_list',
					appcode: '105602BZPT',
					name: this.state.json['105603JDCX-0019'],
					process: para
				}
			)
		}else{
			// window.parent.openNew({code:'105602BZPT',name:'报账平台'}, null);
				// "105603JDCX-0019":"报账平台",
			this.props.openTo(
				'/sscrp/rpbill/rpbill/list/index.html',
				{
					pagecode: '105602BZPT_list',
					appcode: '105602BZPT',
					name: this.state.json['105603JDCX-0019']
				}
			)
		}
	}
	render() {
		// "105603JDCX-0020":"我的报账",
			// "105603JDCX-0021":"待提交",
			// "105603JDCX-0022":"处理中",
			// "105603JDCX-0023":"驳回",
		return (
			<div id="test" className="app1X2 platform-app number expense" onClick={(e) => {this.open(e, null)}}>
                <div className="title">{ this.state.json['105603JDCX-0020'] }</div>

				<div className="content">
                    <img className="img" src="/nccloud/resources/sscrp/public/image/ywicon1.png"/>
						<div className="account clearfix">
							<div className="submit" onClick={(e) => {this.open(e, 'TOCOMMIT')}}>
                                <div className="content-one tocommitNum" id = 'tocommit' >{this.state.res.tocommit}</div>
								<div className="title-one word">{ this.state.json['105603JDCX-0021'] }</div>
							</div>
							<div className="submit" onClick={(e) => {this.open(e, 'TODO')}}>
                                <div className="content-one todoNum" id = 'todo'  >{this.state.res.todo}</div>
								<div className="title-one word">{ this.state.json['105603JDCX-0022'] }</div>
							</div>
							<div className="submit"  onClick={(e) => {this.open(e, 'REJECT')}}>
                                <div  className="content-one rejectNum" id = 'reject' >{this.state.res.reject}</div>
								<div className="title-one word">{ this.state.json['105603JDCX-0023'] }</div>
							</div>
						</div>
				</div>
			</div>
		);
	}
}

Test1 = createPage({
	mutiLangCode:'1056'
})(Test1)
export default Test1
// ReactDOM.render(<Test1 />, document.querySelector('#app'));
