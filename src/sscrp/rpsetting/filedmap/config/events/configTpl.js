let configTpl = {
    
    
  /*  "fieldmap_config": {
        "code": "fieldmap_config",
        "items": [
          {
            "itemtype": "input",
            "visible": false,
            "label": "主键",
            "attrcode": "pk_fieldmap_b",
            "maxlength": "20"
          },
          {
            "itemtype": "input",
            "visible": true,
            "disabled": true,
            "label": "目标字段编码",
            "attrcode": "destcode",
            "maxlength": "256",
            "width": "150px"
          },
          {
            "itemtype": "input",
            "visible": true,
            "disabled": true,
            "label": "目标字段名称",
            "attrcode": "destname",
            "maxlength": "256",
            "width": "150px"
          },
          {
            "itemtype": "checkbox_switch",
            "visible": true,
            "label": "报账卡片显示",
            "attrcode": "rpcardshowflag",
            "maxlength": "1",
            "width": "150px"
          },
          {
            "itemtype": "checkbox_switch",
            "visible": true,
            "label": "任务卡片显示",
            "attrcode": "taskcardshowflag",
            "maxlength": "1",
            "width": "150px"
          },
          {
            "itemtype": "input",
            "visible": true,
            "label": "卡片显示名称",
            "attrcode": "cardshowname",
            "maxlength": "256",
            "width": "150px"
          },
          {
            "itemtype": "input",
            "visible": true,
            "label": "显示顺序",
            "attrcode": "rowno",
            "maxlength": "3",
            "width": "150px"
          },          
          {
            "itemtype": "select",
            "visible": true,
            "label": "卡片显示宽度（列）",
            "attrcode": "cardshowlenth",
            "maxlength": "1",
            "width": "150px",
            "options": [
              {
                "display": "1",
                "value": "1"
              },
              {
                "display": "2",
                "value": "2"
              },
              {
                "display": "3",
                "value": "3"
              }
            ]
          },
          {
            "itemtype": "select",
            "visible": false,
            "label": "规则类型",
            "attrcode": "ruletype",
            "maxlength": "1",
            "width": "150px",
            "options": [
              {
                "display": "赋值",
                "value": "1"
              },
              {
                "display": "映射",
                "value": "2"
              },
              {
                "display": "公式",
                "value": "3"
              }
            ]
          },
          {
            "itemtype": "input",
            "visible": false,
            "label": "规则",
            "attrcode": "ruledata",
            "maxlength": "512",
            "width": "150px"
          },
          {
            "itemtype": "input",
            "visible": false,
            "label": "规则名称",
            "attrcode": "rulename",
            "maxlength": "256",
            "width": "150px"
          },
          {
            "itemtype": "input",
            "visible": false,
            "label": "对应公共字段",
            "attrcode": "pk_sscpublicfield",
            "maxlength": "20",
            "width": "150px"
          },
          {
            "itemtype": "checkbox_switch",
            "visible": true,
            "label": "报账列表显示",
            "attrcode": "rplistshowflag",
            "maxlength": "1",
            "width": "150px"
          },
          {
            "itemtype": "checkbox_switch",
            "visible": true,
            "label": "任务列表显示",
            "attrcode": "tasklistshowflag",
            "maxlength": "1",
            "width": "150px"
          },
          {
            "itemtype": "input",
            "visible": true,
            "label": "显示公式",
            "attrcode": "detailshowformula",
            "maxlength": "512",
            "width": "150px"
          },
          {
            "itemtype": "checkbox_switch",
            "visible": false,
            "label": "模糊匹配",
            "attrcode": "fuzzymatch",
            "maxlength": "1",
            "width": "150px"
          },
          {
            "itemtype": "checkbox_switch",
            "visible": false,
            "label": "是否自定义",
            "attrcode": "isdef",
            "maxlength": "20",
            "width": "150px"
          }       
        ],
        "moduletype": "table",
        "name": "单据映射规则明细",
        "pagination": false
      } */
}

export default configTpl;