import {ajax,base} from 'nc-lightapp-front';
import requestApi from '../requestApi';


export default function afterEvent(props,moduleId,key,value, changedrows) {
    
    //console.log('编辑后事件', props,moduleId,key,value, 22, changedrows);
    switch(moduleId) {
        case 'fieldmap_b' :
            switch (key) {
                case 'ruletype' :
                    // if(value == 1){
                    //     props.meta.getMeta()['fieldmap_b'].items.find(e=>e.attrcode == 'ruledata').itemtype = 'input'
                    //     props.meta.getMeta()['fieldmap_b'].items.find(e=>e.attrcode == 'ruledata').refcode = ''
                    //     props.renderItem('table','fieldmap_b', 'ruledata', null)
                    // }else if(value == 2){
                    //     props.meta.getMeta()['fieldmap_b'].items.find(e=>e.attrcode == 'ruledata').itemtype = 'refer'
                    //     props.meta.getMeta()['fieldmap_b'].items.find(e=>e.attrcode == 'ruledata').refcode = 'uap/refer/riart/MDTreebyBilltype/index.js'
                    //     props.meta.getMeta()['fieldmap_b'].items.find(e=>e.attrcode == 'ruledata').queryCondition = function (){
                    //         let srcbilltype = props.syncTree.getSelectNode("tree").refcode
                    //         if(props.syncTree.getSelectNode("tree").pid != '~'){
                    //             srcbilltype = props.syncTree.getSyncTreeValue('tree',props.syncTree.getSelectNode("tree").pid).refcode
                    //         }
                    //         return {
                    //             //beanId : 'd9b9f860-4dc7-47fa-a7d5-7a5d91f39290'
                    //             src_billtype: srcbilltype
                    //         }
                    //     }
                    //     props.renderItem('table','fieldmap_b', 'ruledata', null)
                    // }else if(value == 3){
                    //     // props.renderItem(
                    //     //     "table",
                    //     //     "fieldmap_b",
                    //     //     "ruledata",
                    //     //     <div className="formula-div">
                    //     //       <input
                    //     //         type="text"
                    //     //         ref={ref => {
                    //     //           this.formulaInputId = ref;
                    //     //         }}
                    //     //         placeholder="填写公式"
                    //     //         className="formula-input"
                    //     //       />
                    //     //       <span
                    //     //         className="icon-refer"
                    //     //         onClick={() => {
                    //     //           this.openFormula();
                    //     //         }}
                    //     //         style={{ cursor: "pointer" }}
                    //     //       />
                    //     //     </div>
                    //     //   );
                    //     props.meta.getMeta()['fieldmap_b'].items.find(e=>e.attrcode == 'ruledata').itemtype = 'input'
                    //     props.meta.getMeta()['fieldmap_b'].items.find(e=>e.attrcode == 'ruledata').refcode = ''
                    //     props.renderItem('table','fieldmap_b', 'ruledata', null)
                    // }
                    
                    break;
                case 'ruledata':
                    let rulename = ''
                    let ruledata = ''
                    if(!(value.refname == undefined)){
                        if(value.pid == 'root'){
                            rulename = value.refname.replace(/[ ]/g,"").substring(value.refcode.length,value.refname.replace(/[ ]/g,"").length)
                            ruledata = value.refcode
                        }else{
                            rulename = value.refname.replace(/[ ]/g,"").substring(value.refcode.substring(value.pid.length+1,value.refcode.length).length,value.refname.replace(/[ ]/g,"").length)
                            ruledata = value.refcode
                        }
                        
                        props.editTable.setValByKeyAndIndex('fieldmap_b',props.editTable.getClickRowIndex("fieldmap_b").index,'ruledata',{value: ruledata, display:ruledata, scale:0 })
                        props.editTable.setValByKeyAndIndex('fieldmap_b',props.editTable.getClickRowIndex("fieldmap_b").index,'rulename',{value: rulename, display:rulename, scale:0 })
                    }
                    
            }
            break;
    }
};