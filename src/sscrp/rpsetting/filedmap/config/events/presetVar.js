/**
 * 全局变量设置文件
 */
import 'ssccommon/components/globalPresetVar';

//全局变量尽量配置在presetVar中，防止全局变量污染。也方便统一管理。
window.presetVar = {
    ...window.presetVar,
    pageId: '',
    pageCode: 'fieldmap_b',
    //表头（主表）变量配置
    head: '',
    //表体（子表）变量配置
    body: 'fieldmap_b',
    //搜索模板变量配置
    search: {},
    modalTable: 'fieldmap_config'
};