import {ajax} from 'nc-lightapp-front';
import requestApi from '../requestApi';
import {setTableExtendCol} from 'ssccommon/components/profile';
import tableExtendButtonClick from './tableExtendButtonClick';
const body = window.presetVar.body;
const areaBtnAction = window.presetVar.areaBtnAction;
//编辑
let editBtnEventConfig = {
    beforeClick: (props)=>{
        //设置按钮的可见状态
        props.button.setButtonsVisible({'Edit': false, 'Save' : true, 'Add': true});
    }, //前处理事件。 return为false则中断处理后续事件
    ctrlEditTableArea: ['fieldmap_b'], //受按钮控制的表格区域
    afterClick: function afterClick(props) {
          // 设置树不可用
          props.syncTree.setNodeDisable('tree', true);
          this.setState({disabledSearch:true});
    }
      
        
}


//保存
let saveBtnEventConfig = {
    
    click(props) {
        
        props.editTable.filterEmptyRows("fieldmap_b", ['destcode','destname'], 'include');
        let data = props.editTable.getAllData("fieldmap_b");
        let rows = props.editTable.checkRequired("fieldmap_b", (data || {rows:[]}).rows);
        if(!rows){
            return
        }
        /*data.rows.map((row)=>{
            if(row.status == 3){
                data.rows.remove(row)
            }
            if(row.values.ruledata == null || row.values.ruledata.value == null){
                data.rows.remove(row)
            }
        })*/
        for(var i=0,flag=true,len=data.rows.length;i<len;flag ? i++ : i){
            if(data.rows[i] != undefined && data.rows[i].status == 3){
                //data.rows.remove(data.rows[i]);
                data.rows.splice(i,1);
                flag = false;
            }else{
                flag = true;
            }
            if(data.rows[i] != undefined && (data.rows[i].values.ruledata == null || data.rows[i].values.ruledata.value == null)){
                //data.rows.remove(data.rows[i]);
                data.rows.splice(i,1);
                flag = false;
            }else{
                flag = true;
            }
        }
        let srcbillortranstype = props.syncTree.getSelectNode("tree").refcode;
        let userjson = {"srcbillortranstype": srcbillortranstype};
        let bdata = {
            head:data,
            userjson:"{'srcbillortranstype':'"+srcbillortranstype+"'}"
        }
        requestApi.save({
            data : bdata,
            success: (resData) => {
                props.editTable.setStatus('fieldmap_b', 'browse')
                //隐藏表格扩展列
                props.editTable.hideColByKey('fieldmap_b', 'opr');
                //props.editTable.setTableData('fieldmap_b', resData.data.fieldmap_b);
                if(resData.data!=undefined){
                    if(resData.data.fieldmap_b!=undefined){
                        props.editTable.setTableData('fieldmap_b', resData.data.fieldmap_b);
                    }else{
                        resData.data.fieldmap_config.areacode = "fieldmap_b";
                        props.editTable.setTableData('fieldmap_b', resData.data.fieldmap_config);
                    }
                }
                props.button.setButtonsVisible({'Cancel': false, 'Add': false, 'Save': false,'Config': true,'Displayconfig': true});
                // 设置树可用
                props.syncTree.setNodeDisable('tree', false);
                this.setState({disabledSearch:false});
                
            }
        })
    }
}

//配置映射
let configBtnEventConfig = {
    /*click(props) {
        requestApi.config({
            data : {},
            success: (resData) => {
               // props.editTable.setTableData('areaId', null)
                props.button.setButtonsVisible({'Edit': false, 'Save' : true, 'Add': true});
            }
        })            
    }*/  
    click:function click(props) {
        console.log('点击配置按钮')
        let data = props.editTable.getAllData("fieldmap_b");
        if(data.rows.length == 0 && props.syncTree.getSelectNode("tree").pid!='~'){
            //props.modal.show("config");
            let srctranstype = props.syncTree.getSelectNode("tree").refcode;
            let srcbilltype = props.syncTree.getSyncTreeValue('tree',props.syncTree.getSelectNode("tree").pid).refcode;
            let data = {
                'isAll' : 'Y',
                'src_transtype' : srctranstype,
                'src_billtype' : srcbilltype
            }
            requestApi.config({
                data : data,
                success: (resData) => {
                    //props.editTable.setTableData('fieldmap_b', resData.data.fieldmap_b)
                    if(resData.data!=undefined){
                        if(resData.data.fieldmap_b!=undefined){
                            props.editTable.setTableData('fieldmap_b', resData.data.fieldmap_b);
                        }else{
                            resData.data.fieldmap_config.areacode = "fieldmap_b";
                            props.editTable.setTableData('fieldmap_b', resData.data.fieldmap_config);
                        }
                    }
                    props.button.setButtonsVisible({'Cancel': false, 'Add': false, 'Save': false,'Config': true,'Displayconfig': true});
                    // 设置树可用
                    props.syncTree.setNodeDisable('tree', false);
                    // 设置树组件查询区禁用
                    this.setState({disabledSearch:false});
                }
            })
        }else {
            let arr = ['billno','billtypecode','pk_tradetype','busiid','transtypecode','pk_group'];
            props.editTable.setStatus('fieldmap_b', 'edit');
            // //设置表格扩展列
            // setTableExtendCol(props, props.meta.getMeta('fieldmap_b'), [{
            //     tableAreaId: 'fieldmap_b',
            //     btnAreaId: 'fieldmap_b',
            //     buttonVisible: (record, index) => {
            //         return [body + areaBtnAction.del]   
            //     },
            //     onButtonClick: tableExtendButtonClick.bind(this)
                
            // }]);
            props.editTable.showColByKey('fieldmap_b', 'opr');
            //设置按钮的可见状态
            props.button.setButtonsVisible({'Add': true, 'Save' : true, 'Cancel': true,"Config": false,"Displayconfig": false});
            
            // 设置树不可用
            props.syncTree.setNodeDisable('tree', true);
            this.setState({disabledSearch:true});
            
            let rowids = [];
            let rowids2 = [];
            for(var key in data.rows){                          
                if(key != 'remove' && isInArray(arr,data.rows[key].values.destcode.value)){
                   /*  setTimeout(() => { 
                        props.editTable.setEditableRowByRowId('fieldmap_b', data.rows[key].rowid, false)
                        // props.editTable.setEditableRowByIndex('fieldmap_b', key, false)
                    }, 10)  */
                    rowids.push(data.rows[key].rowid);
                }
                if(key != 'remove' && !data.rows[key].values.isdef.value){
                    /* setTimeout(() => { 
                        props.editTable.setEditableRowKeyByRowId('fieldmap_b', data.rows[key].rowid, ['destcode','destname'], false)
                        // props.editTable.setEditableRowKeyByRowId('fieldmap_b', data.rows[key].rowid, 'destname', false)
                    }, 10)  */
                    rowids2.push(data.rows[key].rowid);
                }                     
            }
            setTimeout(() => {
                props.editTable.setEditableRowByRowId('fieldmap_b', rowids, false) 
                props.editTable.setEditableRowKeyByRowId('fieldmap_b', rowids2, ['destcode','destname'], false)
            }, 0) 
                       
        }
    } 
}

function isInArray(arr,value){
    for(var i = 0; i < arr.length; i++){
        if(value === arr[i]){
            return true;
        }
    }
    return false;
}

/**
 * 确认按钮点击
 */
function beSureBtnClick() {

    let srctranstype = this.props.syncTree.getSelectNode("tree").refcode;
    let srcbilltype = this.props.syncTree.getSyncTreeValue('tree',this.props.syncTree.getSelectNode("tree").pid).refcode;
    let data = {
        'isAll' : 'Y',
        'src_transtype' : srctranstype,
        'src_billtype' : srcbilltype
    }
    requestApi.config({
        data : data,
        success: (resData) => {
            this.props.editTable.setTableData('fieldmap_b', resData.data.fieldmap_b)
            this.props.button.setButtonsVisible({'Cancel': false, 'Add': false, 'Save': false,'Config': true,'Displayconfig': true});
            // 设置树可用
            props.syncTree.setNodeDisable('tree', false);
            this.setState({disabledSearch:false});
            
        }
    })
}

/**
 * 取消按钮点击
 */
function cancelBtnClick(props){
    let srctranstype = this.props.syncTree.getSelectNode("tree").refcode;
    let srcbilltype = this.props.syncTree.getSyncTreeValue('tree',this.props.syncTree.getSelectNode("tree").pid).refcode;
    let data = {
        'isAll' : 'N',
        'src_transtype' : srctranstype,
        'src_billtype' : srcbilltype
    }
    requestApi.config({
        data : data,
        success: (resData) => {
            this.props.editTable.setTableData('fieldmap_b', resData.data.fieldmap_b)
            this.props.button.setButtonsVisible({'Cancel': false, 'Add': false, 'Save': false,'Config': true,'Displayconfig': true});
            // 设置树可用
            props.syncTree.setNodeDisable('tree', false);
            this.setState({disabledSearch:false});
            
        }
    })
}


//取消
let cancelBtnEventConfig  = {
    click: function click(props) {
         // 取消编辑态
         props.editTable.cancelEdit('fieldmap_b');
         //隐藏表格扩展列
         props.editTable.hideColByKey('fieldmap_b', 'opr');
         // 设置按钮状态
         props.button.setButtonsVisible({
             'Add': false,
             'Save': false,
             'Cancel':false,
             'Config':true,
             'Displayconfig':true
         });
        // 设置树可用
        props.syncTree.setNodeDisable('tree', false);
        // 设置树查询区可用
        this.setState({disabledSearch:false});
        
    }
}

//删除
let deleteBtnEventConfig = {
    click: (props)=>{
        let data = props.editTable.getCheckedRows('fieldmap_b');
        let arr = data.map(item => item.index);
        this.props.editTable.deleteTableRowsByIndex('fieldmap_b', arr);
    }
}

//增加
let addBtnEventConfig = {
    //ctrlEditTableArea:['fieldmap_b']
    click: (props)=>{
        //console.log('点击配置按钮')
        props.editTable.addRow('fieldmap_b');
        let index = props.editTable.getAllData('fieldmap_b').rows.length.toString();
        props.editTable.setValByKeyAndIndex('fieldmap_b',index-1,'rowno',{value: index});
        props.editTable.setValByKeyAndIndex('fieldmap_b',index-1,'cardshowlenth',{value: '1',display: '1'});
        props.editTable.setValByKeyAndIndex('fieldmap_b',index-1,'isdef',{value: true});
        //let props = this.props;
        //let defaultValue={};
        //props.cardTable.addRow("fieldmap_b", undefined, defaultValue);
    }
}

let displayConfigBtnEventConfig = {
    click(props){
        this.setState({showConfigModal:true});
        setTimeout(() => {
            props.editTable.setTableData('fieldmap_config',props.editTable.getAllData("fieldmap_b"));
        }, )
        
        props.editTable.setStatus('fieldmap_config', 'edit');
    }
}

let saveModalBtnEventConfig = {
    click(props){
        let data = this.props.editTable.getAllData("fieldmap_config");
        let srcbillortranstype = this.props.syncTree.getSelectNode("tree").refcode;
        let userjson = {"srcbillortranstype": srcbillortranstype};
        let bdata = {
            head:data,
            userjson:"{'srcbillortranstype':'"+srcbillortranstype+"'}"
        }
        requestApi.save({
            data : bdata,
            success: (resData) => {
                this.setState({showConfigModal:false});
                this.props.editTable.setTableData('fieldmap_config',{rows:[]});
                //this.props.editTable.setTableData('fieldmap_b', resData.data.fieldmap_b);
                setTimeout(() => {
                    if(resData.data!=undefined){
                        if(resData.data.fieldmap_b!=undefined){
                            this.props.editTable.setTableData('fieldmap_b', resData.data.fieldmap_b);
                        }else{
                            resData.data.fieldmap_config.areacode = "fieldmap_b";
                            this.props.editTable.setTableData('fieldmap_b', resData.data.fieldmap_config);
                        }
                    }
                },0)
                
            }
        })
    }
}

let cancelModalBtnEventConfig = {
    click(props){
        this.props.editTable.setTableData('fieldmap_config',{rows:[]});
        this.setState({showConfigModal:false});
    }
}

/**
 * 按钮点击
 */
function configBtnClick(key, item) {

     //设置按钮的可见状态
     props.button.setButtonsVisible({'Add': true, 'Save' : true, 'Cancel': true});
    // 设置树不可用
    props.syncTree.setNodeDisable('tree', true);
    this.setState({disabledSearch:true});
    

}

/**
 * 树节点选择事件
 */
function treeSelectEvent(key, item) {

    //console.log('点击树节点', key, item)
    this.props.editTable.setStatus('fieldmap_b', 'browse');
    //隐藏表格扩展列
    this.props.editTable.hideColByKey('fieldmap_b', 'opr');
    if(Object.is('~', key)){
        this.props.button.setButtonsVisible({"Config": false,"Displayconfig": false,'Add': false, 'Save' : false, 'Cancel': false});
    }else{
        this.props.button.setButtonsVisible({"Config": true,"Displayconfig": true,'Add': false, 'Save' : false, 'Cancel': false});
    }

    //设置表格数据
    requestApi.getTableData({
        data: {srcBillOrTranstype:item.refcode},
        success: (data) => {
            let ndata = {
                areacode : "fieldmap_b",
                rows : []
            }
            if(data.data!=undefined){
                if(data.data.fieldmap_b!=undefined){
                    this.props.editTable.setTableData('fieldmap_b', data.data.fieldmap_b);
                }else{
                    data.data.fieldmap_config.areacode = "fieldmap_b";
                    this.props.editTable.setTableData('fieldmap_b', data.data.fieldmap_config);
                }
            }else{
                this.props.editTable.setTableData('fieldmap_b', ndata);
            }
            
        }
    })

}

export default {editBtnEventConfig, saveBtnEventConfig, addBtnEventConfig, treeSelectEvent, configBtnEventConfig, configBtnClick, cancelBtnEventConfig, deleteBtnEventConfig,displayConfigBtnEventConfig,saveModalBtnEventConfig,cancelModalBtnEventConfig,beSureBtnClick,cancelBtnClick}


