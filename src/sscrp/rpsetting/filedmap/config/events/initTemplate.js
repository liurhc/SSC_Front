import {base, high, ajax} from 'nc-lightapp-front';
import 'ssccommon/components/globalPresetVar';
import {setTableExtendCol} from 'ssccommon/components/profile'
import requestApi from '../requestApi';
import buttonClick from './buttonClick';
import tableExtendButtonClick from './tableExtendButtonClick';
import tplMock from  './tplMock';
import configTpl from  './configTpl';
const body = window.presetVar.body;
const areaBtnAction = window.presetVar.areaBtnAction;
const modalTable = window.presetVar.modalTable;

export default function (props) {

    props.createUIDom(
        {
            //pagecode: 'fieldmap_b',//页面id
            //appid: '0001ZG10000000006H1A'//注册按钮的id
        },
        (data) => {
            let meta = data.template;
            //meta['fieldmap_config'] = configTpl['fieldmap_config'];
            let buttonTpl = data.button;
            //设置modal表格的扩展按钮列
            setTableExtendCol(props, meta, [{
                width: '200px',
                areaId: modalTable,
                btnAreaId: 'fieldmap_config_area',
                buttonVisible: (record, index) => {
                      return ['SettingTop','Up','Down','SettingBottom']
                },
                buttonLimit:4,
                onButtonClick: tableExtendButtonClick.bind(this)
            }]);
            //设置表格扩展列
            setTableExtendCol(props, meta, [{
                tableAreaId: body,
                btnAreaId: body,
                buttonVisible: (record, index) => {
                    return [body + areaBtnAction.del]   
                },
                onButtonClick: tableExtendButtonClick.bind(this)
                
            }]);
            // meta.fieldmap_b.items.map((item)=>{
            //     if(item.attrcode=='ruledata'){
            //         item.render = function(text,record,index){
            //             let ruletype = record.values.ruletype.value;
            //             if(ruletype == undefined || ruletype == null || ruletype == ''){
            //                 return ''
            //             }else if(ruletype == 3){
            //                 return (<div className="formula-div">
            //                         <input
            //                         type="text"
            //                         //   ref={ref => {
            //                         //     this.formulaInputId = ref;
            //                         //   }}
            //                         //placeholder="更多范围"
            //                         className="formula-input"
            //                         />
            //                         <span
            //                         className="icon-refer"
            //                         onClick={() => {
            //                             _this.openFormula();
            //                         }}
            //                         style={{ cursor: "pointer" }}
            //                         />
            //                     </div>)
            //             }else if(ruletype == 1){
            //                 return ''
            //             }
                        
            //         }
            //     }
            // })

            //设置区域模板
            props.meta.setMeta(meta);
            //设置按钮模板
            props.button.setButtons(buttonTpl);

            //设置按钮行为为弹窗
            // props.button.setPopContent('delete','确认要删除该信息吗？') /* 设置操作列上删除按钮的弹窗提示 */
            //table扩展按钮弹窗确认框设置
            //props.button.setPopContent(body + areaBtnAction.del,'确认要删除该信息吗？') /* 设置操作列上删除按钮的弹窗提示 */

            //设置按钮的初始可见性
            props.button.setButtonsVisible({'Cancel': false, 'Add': false, 'Save': false, "Config": false,"Displayconfig": false});

            //设置初始数据
            //获取树的数据
            requestApi.getTreeData({
                data: {},
                success(data) {
                    
                    let {syncTree} = props;
                    let {setSyncTreeData} = syncTree;
                                     
                    delete data.data.nodes[data.data.nodes.length-1].pid;  
                    let newTree = syncTree.createTreeData(data.data.nodes); //创建树 组件需要的数据结构
                    setSyncTreeData('tree', newTree);
                }
            })

        }
    )

       
}


