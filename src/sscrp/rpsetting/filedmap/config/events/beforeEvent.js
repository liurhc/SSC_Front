import {ajax,base} from 'nc-lightapp-front';
import requestApi from '../requestApi';


export default function beforeEvent(props,moduleId,item,index, value,record) {
    
    switch(moduleId) {
        case 'fieldmap_b':
            switch (item.attrcode) {
                case 'ruledata' :
                    let ruletype = record.values.ruletype.value
                    if(ruletype == undefined || ruletype == null || ruletype == ''){
                        break;
                    }
                    if(ruletype == 1){
                        props.meta.getMeta()['fieldmap_b'].items.find(e=>e.attrcode == 'ruledata').itemtype = 'input'
                        props.meta.getMeta()['fieldmap_b'].items.find(e=>e.attrcode == 'ruledata').refcode = ''
                        props.renderItem('table','fieldmap_b', 'ruledata', null)
                    }else if(ruletype == 2){
                        value.display = value.value
                        props.meta.getMeta()['fieldmap_b'].items.find(e=>e.attrcode == 'ruledata').itemtype = 'refer'
                        props.meta.getMeta()['fieldmap_b'].items.find(e=>e.attrcode == 'ruledata').refcode = 'uap/refer/riart/MDTreebyBilltype/index.js'
                        props.meta.getMeta()['fieldmap_b'].items.find(e=>e.attrcode == 'ruledata').queryCondition = function (){
                            let srcbilltype = props.syncTree.getSelectNode("tree").refcode
                            if(props.syncTree.getSelectNode("tree").pid != '~'){
                                srcbilltype = props.syncTree.getSyncTreeValue('tree',props.syncTree.getSelectNode("tree").pid).refcode
                            }
                            return {
                                //beanId : 'd9b9f860-4dc7-47fa-a7d5-7a5d91f39290'
                                src_billtype: srcbilltype
                            }
                        }
                        props.renderItem('table','fieldmap_b', 'ruledata', null)
                    }else if(ruletype == 3){
                        // props.renderItem(
                        //     "table",
                        //     "fieldmap_b",
                        //     "ruledata",
                        //     <div className="formula-div">
                        //       <input
                        //         type="text"
                        //         ref={ref => {
                        //           this.formulaInputId = ref;
                        //         }}
                        //         placeholder="填写公式"
                        //         className="formula-input"
                        //       />
                        //       <span
                        //         className="icon-refer"
                        //         onClick={() => {
                        //           this.openFormula();
                        //         }}
                        //         style={{ cursor: "pointer" }}
                        //       />
                        //     </div>
                        //   );
                        props.meta.getMeta()['fieldmap_b'].items.find(e=>e.attrcode == 'ruledata').itemtype = 'input'
                        props.meta.getMeta()['fieldmap_b'].items.find(e=>e.attrcode == 'ruledata').refcode = ''
                        props.renderItem('table','fieldmap_b', 'ruledata', null)

                        this.state.clickvalue = value.value
                        // this.clickvalue.value = value.value;
                        // this.clickvalue.code = item.attrcode;
                        this.openFormula();
                    }
                    // this.state.clickvalue = value.value
                    // let ruletype = record.values.ruletype.value
                    // if(ruletype == undefined || ruletype == null || ruletype == ''){
                    //     break;
                    // }
                    // if(ruletype == 3){
                    //     this.openFormula();
                    // }
                    break;
                /* case 'detailshowformula' :
                    this.state.clickvalue = value.value
                    this.openFormula2();
                    break; */
                    
            }
            // if(!record.values.isdef.value){
            //     props.editTable.setEditableRowKeyByRowId(moduleId, record.rowid, 'destcode', false)
            // }
            break;
        // case 'fieldmap_config' :
        //     switch (item.attrcode) {
        //         case 'detailshowformula':
        //             this.params.clickvalue = value.value
        //             this.openFormula2();
        //             break;
        //     }
            
    }
    return true;
};