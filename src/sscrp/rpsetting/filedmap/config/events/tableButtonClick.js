/**
 * 表格肩部按钮click事件
 */
import {ajax} from 'nc-lightapp-front';

const areaBtnAction = window.presetVar.areaBtnAction;
const statusVar = window.presetVar.status;
const body = window.presetVar.body;

function tableButtonClick() {
    return {
        [body + areaBtnAction.add] : () => {
            let props = this.props;
            let defaultValue={};
            props.meta.getMeta()[body].items.map((one)=>{
                if(one.initialvalue != null && one.initialvalue.value != null && one.initialvalue.value != ""){
                    defaultValue[one.attrcode] = {display: one.initialvalue.display, value: one.initialvalue.value}
                }
            })
            props.cardTable.addRow(body, undefined, defaultValue);
        }
    }
}

export default tableButtonClick;