/**
 * 表格扩展按钮click事件
 */
import {ajax} from 'nc-lightapp-front';
import { toast } from "nc-lightapp-front";

const areaBtnAction = window.presetVar.areaBtnAction;
const body = window.presetVar.body;
const modalTable = window.presetVar.modalTable;

function tableExtendButtonClick() {
    return {
        [body + areaBtnAction.del ] : (record, index) => {
            
            //this.props.cardTable.delRowsByIndex(body, index);
            console.log(this.props.editTable.deleteTableRowsByIndex, body, index)
            let multiLang = this.props.MutiInit.getIntl(1056);
            if(!record.values.isdef.value){
                //105601DJYS-0059:非自定义映射关系不允许删除;
                toast({ color: "warning", content: multiLang && multiLang.get('105601DJYS-0059') });
            }else{
                this.props.editTable.deleteTableRowsByIndex(body, index, true);
            }
            
        },
        ['Up'] : (record, index) => {
            if(index!=0){
                this.props.editTable.setValByKeyAndIndex(modalTable, index-1, 'rowno', {value: record.values.rowno.value, display:record.values.rowno.value});
                this.props.editTable.setValByKeyAndIndex(modalTable, index, 'rowno', {value: (parseInt(record.values.rowno.value)-1)+"", display:parseInt(record.values.rowno.value)-1});
                this.props.editTable.moveRow(modalTable,index, index-1);
                this.props.editTable.setRowStatus(modalTable, [index,index-1], '1');
            }
        },
        ['Down'] : (record, index) => {
            let totalIndex = this.props.editTable.getNumberOfRows(modalTable);
            if(index!=totalIndex-1){
                this.props.editTable.setValByKeyAndIndex(modalTable, index+1, 'rowno', {value: record.values.rowno.value, display:record.values.rowno.value});
                this.props.editTable.setValByKeyAndIndex(modalTable, index, 'rowno', {value: (parseInt(record.values.rowno.value)+1)+"", display:parseInt(record.values.rowno.value)+1});
                this.props.editTable.moveRow(modalTable,index, index+1);
                this.props.editTable.setRowStatus(modalTable, [index,index+1], '1');
            }
        },
        ['SettingTop'] : (record, index) => {
            this.props.editTable.setRowPosition(modalTable,index,'up');
            let tableData = this.props.editTable.getAllData(modalTable);
            let changeIndex = []
            tableData.rows.map((row,idx)=>{
                this.props.editTable.setValByKeyAndIndex(modalTable, idx, 'rowno', {value:(idx+1)+"", display:idx+1});
                changeIndex.push(idx);
            })
            this.props.editTable.setRowStatus(modalTable, changeIndex, '1');
        },
        ['SettingBottom'] : (record, index) => {
            this.props.editTable.setRowPosition(modalTable,index,'down');
            let tableData = this.props.editTable.getAllData(modalTable);
            let changeIndex = []
            tableData.rows.map((row,idx)=>{
                this.props.editTable.setValByKeyAndIndex(modalTable, idx, 'rowno', {value:(idx+1)+"", display:idx+1});
                changeIndex.push(idx);
            })
            this.props.editTable.setRowStatus(modalTable, changeIndex, '1');
        }
    }
}

export default tableExtendButtonClick;