import requestApi from "ssccommon/components/requestApi";
import {ajax} from 'nc-lightapp-front';


let requestDomain = '';
let requestApiOverwrite = {
    ...requestApi,
    //单据模板接口

    save: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscrp/rpbill/FieldMapSaveAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
        /*setTimeout(() => {
            let resData = {};
            opt.success(resData)
        });*/
    },
    query: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscrp/rpbill/FieldMapQryAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    config: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscrp/rpbill/FieldMapBillToTranstypeAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    getTreeData: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscrp/rpbill/FieldMapQryBillTypeAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
        /*setTimeout(() => {
            opt.success(mockData.treeData)
        })*/
    },
    getTableData: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscrp/rpbill/FieldMapQryAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
        /*setTimeout(() => {
            opt.success(mockData.tableData)
        })*/
    },
    queryMetaParam:(opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscrp/rpbill/FieldMapQryMetaParamAction.do`,
            data: opt.data,
            success: (data) =>{
                opt.success(data);
            }
        })
    },
    queryMetaParam2:(opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscrp/rpbill/FieldMapQryMetaParamAction.do`,
            data: opt.data,
            success: (data) =>{
                opt.success(data);
            }
        })
    }

}

export default requestApiOverwrite;
