import './events/presetVar';

import React, {Component} from 'react';
import {connect} from 'react-redux';
import {createPage, base, high} from 'nc-lightapp-front';
import requestApi from './requestApi';
import {tableButtonClick, buttonClick, initTemplate, afterEvent, beforeEvent} from './events';


import {
    ProfileStyle,
    ProfileHead,
    ProfileBody,
    BodyLeft,
    BodyRight,
    HeadCenterCustom,
    EmptyArea,
    ButtonGroup
} from 'ssccommon/components/profile';
import {SyncTree} from 'ssccommon/components/tree';
import {EditTable} from 'ssccommon/components/table';


import './index.less';

const {NCCheckbox,NCModal,NCButton} = base;
const { FormulaEditor } = high;

class TreeTable extends Component {

    constructor(props) {
        super();
        this.metaparam={};
        this.formulaInputValue = ""; // 公式内容
        this.state = {
            showConfigModal:false,
            clickvalue:"",
            disabledSearch: false
        }
        initTemplate.call(this, props);
    }

    componentWillMount() {
        window.onbeforeunload = () => {
            let status = this.props.editTable.getStatus('fieldmap_b');
            if (status === 'edit') {
                return '';
            }
        }
    }

    componentDidMount() {
    }
    openFormula() {
        
        this.metaparam = this.queryMetaParam() || {};
        //console.log("测试", "123");
       
    }

    openFormula2() {
        this.metaparam = this.queryMetaParam2() || {};
    }

    queryMetaParam  ()  {
        
        let metaparam = []
        // if(Object.keys(this.props.meta.getMeta()).length == 0){
        //     return metaparam
        // }
        if(this.props.syncTree.getSyncTreeValue('tree') == undefined || this.props.syncTree.getSelectNode("tree") == null){
            return metaparam
        }
        let srcbilltype = this.props.syncTree.getSelectNode("tree").refcode
        if(this.props.syncTree.getSelectNode("tree").pid != '~'){
            srcbilltype = this.props.syncTree.getSyncTreeValue('tree',this.props.syncTree.getSelectNode("tree").pid).refcode
        }
    
        // 查询
        requestApi.queryMetaParam({
            data: {
                src_billtype: srcbilltype
            },
            success: res => {
                // metaparam.push("src_billtype",srcbilltype)
                // metaparam.push("bizmodelStyle",res.data.moduleid)
                // metaparam.push("classid",res.data.beanid)
                metaparam = {
                    pk_billtype :srcbilltype,
                    bizmodelStyle : res.data.moduleid,
                    classid : res.data.beanid
                }
                this.metaparam = metaparam;
                this.setState({ showFormula: true });
            }
        });
    }

    queryMetaParam2  ()  {
        
        let metaparam = []
        if(this.props.syncTree.getSyncTreeValue('tree') == undefined || this.props.syncTree.getSelectNode("tree") == null){
            return metaparam
        }
        let srcbilltype = this.props.syncTree.getSelectNode("tree").refcode
        if(this.props.syncTree.getSelectNode("tree").pid != '~'){
            srcbilltype = this.props.syncTree.getSyncTreeValue('tree',this.props.syncTree.getSelectNode("tree").pid).refcode
        }
    
        // 查询
        requestApi.queryMetaParam2({
            data: {
                src_billtype: srcbilltype
            },
            success: res => {
                metaparam = {
                    pk_billtype :srcbilltype,
                    bizmodelStyle : res.data.moduleid,
                    classid : res.data.beanid
                }
                this.metaparam = metaparam;
                this.setState({ showFormula2: true });
            }
        });
    }

    render() {
        let multiLang = this.props.MutiInit.getIntl(1056);
        let { createEditTable } = this.props.editTable;
        buttonClick.displayConfigBtnEventConfig.click = buttonClick.displayConfigBtnEventConfig.click.bind(this);
        buttonClick.saveModalBtnEventConfig.click = buttonClick.saveModalBtnEventConfig.click.bind(this);
        buttonClick.cancelModalBtnEventConfig.click = buttonClick.cancelModalBtnEventConfig.click.bind(this);
        buttonClick.configBtnEventConfig.click = buttonClick.configBtnEventConfig.click.bind(this);
        buttonClick.cancelBtnEventConfig.click = buttonClick.cancelBtnEventConfig.click.bind(this);
        buttonClick.saveBtnEventConfig.click = buttonClick.saveBtnEventConfig.click.bind(this);
        const {modal} = this.props;
        let { createModal } = modal;
        // let metaparam = {}
        // if(this.queryMetaParam()!=undefined){
        //     this.metaparam = this.queryMetaParam() || {};
        // }
        
        return (
            <ProfileStyle
                layout="treeTable"
                {...this.props}
            >
                {/*页面头*/}
                {/* 105601DJYS-0001':单据映射 */}
                <ProfileHead
                    title={ multiLang && multiLang.get('105601DJYS-0001') }
                >                  
                    <ButtonGroup
                        area="page_header"
                        buttonLimit="4"
                        buttonEvent={
                            {
                                Config: buttonClick.configBtnEventConfig,
                                Add: buttonClick.addBtnEventConfig,
                                Cancel: buttonClick.cancelBtnEventConfig,
                                Save: buttonClick.saveBtnEventConfig,
                                Displayconfig:buttonClick.displayConfigBtnEventConfig
                                //Delete: buttonClick.deleteBtnEventConfig
                                //buttonClick : configBtnClick
                            }
                        }
                    />
                </ProfileHead>
                {/*页面体*/}
                <ProfileBody>
                    <BodyLeft>
                        <SyncTree
                            areaId="tree"
                            onSelectEve={
                                buttonClick.treeSelectEvent.bind(this)
                            }
                            disabledSearch={this.state.disabledSearch}
                        />
                    </BodyLeft>

                    <BodyRight>

                        <EditTable
                            areaId="fieldmap_b"
                            //title="可代理部门"
                            showIndex={false}
                            showCheck={false}
                            onAfterEvent={afterEvent.bind(this)}
                            onBeforeEvent={beforeEvent.bind(this)}
                        >
                        {/* <ButtonGroup
                            area="body-shoulder"
                            buttonEvent={
                                {
                                    Config: buttonClick.configBtnEventConfig,
                                    Add: buttonClick.addBtnEventConfig,
                                    Cancel: buttonClick.cancelBtnEventConfig,
                                    Save: buttonClick.saveBtnEventConfig,
                                    Displayconfig:buttonClick.displayConfigBtnEventConfig
                                    //Delete: buttonClick.deleteBtnEventConfig
                                    //buttonClick : configBtnClick
                                }
                            }
                            //buttonEvent={tableButtonClick.bind(this)}
                        /> */}
                        </EditTable>
                        <NCModal
                            show = {
                                this.state.showConfigModal
                            }
                            onHide = {
                                ()=>{buttonClick.cancelModalBtnEventConfig.click(this.props);}
                            }
                            size="xlg"
                            backdrop='false'
                        >
                            <NCModal.Header closeButton={true}>
                                 {/*"105601DJYS-0002":"显示配置",*/}
                            <NCModal.Title>{ multiLang && multiLang.get('105601DJYS-0002') }</NCModal.Title>
                            </NCModal.Header>

                            <NCModal.Body>
                                <div className="nc-singleTable-table-area">
                                    {createEditTable('fieldmap_config', {
                                            onAfterEvent: afterEvent,
                                            onBeforeEvent: beforeEvent,
                                            onRowClick: '',
                                            showIndex: false,
                                            selectedChange:"",
                                            isAddRow:false,
                                            height:403,
                                            showCheck:false,
                                            params:this.state
                                    })} 
                                </div>
                            </NCModal.Body>
                            {/* "105601DJYS-0004":"保存"
                            "105601DJYS-0005":"取消" */}
                            <NCModal.Footer>
                                <NCButton onClick={buttonClick.saveModalBtnEventConfig.click} colors='primary'>{ multiLang && multiLang.get('105601DJYS-0004') }</NCButton>
                                <NCButton onClick={buttonClick.cancelModalBtnEventConfig.click}>{ multiLang && multiLang.get('105601DJYS-0005') }</NCButton>
                            </NCModal.Footer>

                        </NCModal> 
                        <EmptyArea>
                            <div>
                                {/* {createModal('config', {
                                    title: multiLang && multiLang.get('105601DJYS-0003'),
                                    content: multiLang && multiLang.get('105601DJYS-0006'),
                                    beSureBtnClick: buttonClick.beSureBtnClick.bind(this),
                                    cancelBtnClick: buttonClick.cancelBtnClick.bind(this)
                                })} */}
                                <FormulaEditor
                                  ref="formulaEditor"
                                  value={
                                    this.state.clickvalue
                                  }
                                  show={this.state.showFormula}
                                  /*attrConfig={[
                                    {
                                      tab: "公共字段",
                                      TabPaneContent: PublicField,
                                      params: { name: this.state.publicFieldData }
                                    }
                                  ]}*/
                                  //noShowAttr = {['表和字段','元数据属性']}
                                  // formulaConfig={[{tab:'常用', TabPaneContent: Example, params: {}}]}
                                  onOk={value => {
                                    this.setState({ showFormula: false });
                                    // this.formulaInputId.value = value;
                                    this.props.editTable.setValByKeyAndIndex('fieldmap_b',this.props.editTable.getClickRowIndex("fieldmap_b").index,'ruledata',{value: value, display:value, scale:0 })
                                
                                }} //点击确定回调
                                  onCancel={a => {
                                    this.setState({ showFormula: false });
                                  }} //点击确定回调
                                  isValidateOnOK={false}
                                  metaParam={{
                                    //pk_billtype: this.props.syncTree.getSelectNode("tree").refcode,//"264X"
                                    pk_billtype: this.metaparam.pk_billtype,//"264X",
                                    bizmodelStyle: this.metaparam.bizmodelStyle,//"erm",
                                    //classid: this.metaparam.classid//"d9b9f860-4dc7-47fa-a7d5-7a5d91f39290"
                                    classid: ''
                                  }}
                                  onHide={() => {
                                    this.setState({ showFormula: false });
                                  }}
                                />
                            </div>
                        </EmptyArea>
                    </BodyRight>
                </ProfileBody>
            </ProfileStyle>
        )
    }
}

TreeTable = createPage({
    mutiLangCode:'1056'
})(TreeTable);

export default TreeTable;
