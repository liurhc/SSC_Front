import React, {Component} from 'react';
import {createPage} from 'nc-lightapp-front';
import {initTemplate} from './events';
import {
    ProfileStyle,
    ProfileHead,
    ProfileBody,
    ButtonGroup
} from 'ssccommon/components/profile';
import {EditTable} from 'ssccommon/components/table';

// import FinanceOrgTreeRef from 'uapbd/refer/org/FinanceOrgTreeRef';

import {buttonEvent} from './events/index'

import './index.less';

class PublicField extends Component {

    constructor(props) {
        super();
        this.state = {}
        initTemplate.call(this, props); 
    }

    componentWillMount() {
        window.onbeforeunload = () => {
            let status = this.props.editTable.getStatus(window.presetVar.listArea);
            if (status === 'edit') {
                return '';
            }
        }
    }

    componentDidMount() {

    }
    render() {
        let multiLang = this.props.MutiInit.getIntl(1056)
        return (
            /*档案风格布局*/
            <ProfileStyle
                layout="singleTable"
                {...this.props}
            >
                {/*页面头*/}
                {/* 105601GGZD-0007":"公共字段管理 */}
                <ProfileHead
                    title={ multiLang && multiLang.get('105601GGZD-0007') } 
                >
                    <ButtonGroup
                        area={window.presetVar.btnArea}
                        ctrlEditTableArea={[window.presetVar.listArea]}
                        buttonEvent={buttonEvent.bind(this)}
                    />
                </ProfileHead>
                {/*页面体*/}
                <ProfileBody>
                    <EditTable
                        showCheck={false}
                        isAddRow={true}
                        areaId={window.presetVar.listArea}
                    />
                </ProfileBody>
            </ProfileStyle>
        )
    }
}
PublicField = createPage({
    mutiLangCode:'1056'
})(PublicField);
export default PublicField;