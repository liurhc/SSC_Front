export default [
    {
        "id": "0001A41000000006J5B1",
        "type": "button",
        "key": "Add",
        "title": "新增",
        "area": 'btnArea',
        "children": []
    },
    {
        "id": "0001A41000000006J5B1",
        "type": "button",
        "key": "Edit",
        "title": "修改",
        "area": 'btnArea',
        "children": []
    },
    {
        "id": "0001A41000000006J5B1",
        "type": "button_main",
        "key": "Save",
        "title": "保存",
        "area": 'btnArea',
        "children": []
    },
    {
        "id": "0001A41000000006J5B1",
        "type": "button",
        "key": "Cancel",
        "title": "取消",
        "area": 'btnArea',
        "children": []
    },
    {
        "id": "0001A41000000006J5B1",
        "type": "button",
        "key": "Delete",
        "title": "删除",
        "area": 'listbtnarea',
        "children": []
    }
]