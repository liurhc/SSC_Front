import {toast} from 'nc-lightapp-front';

function buttonEvent() {
    let props = this.props;
    let multiLang = this.props.MutiInit.getIntl(1056);
    return{
        Save:{
            url:'/nccloud/sscrp/rpbill/PublicFieldSaveAction.do',
            beforeClick: ()=>{
                props.editTable.filterEmptyRows(window.presetVar.listArea, []);
                return true;
            },
            afterClick: function(ncProps, data){
                if(data){
                    let userjson = JSON.parse(data.data.userjson);
                    if(userjson.message!=null){
                        toast({content : userjson.message,color : 'warning'});
                    }
                }
                props.button.setMainButton('Add', true);
                //设置按钮行为为弹窗
                //105601GGZD-0060:确认要删除该信息吗？
                props.button.setPopContent('Delete',multiLang && multiLang.get('105601GGZD-0060')) /* 设置操作列上删除按钮的弹窗提示 */
            }
        },
        Delete:{
            url:'/nccloud/sscrp/rpbill/PublicFieldSaveAction.do',
            afterClick: function(areaId, btncode, record, index, ncProps, data){
                if(data){
                    let userjson = JSON.parse(data.data.userjson);
                    if(userjson.message!=null){
                        toast({content : userjson.message,color : 'warning'});
                    }
                }
            }
        },
        Add:{
            afterClick: function(ncProps){
                props.button.setMainButton('Add', false);
                //设置按钮行为为弹窗
                props.button.setPopContent('Delete') /* 设置操作列上删除按钮的弹窗提示 */
            }
        },
        Edit:{
            afterClick: function(ncProps){
                props.button.setMainButton('Add', false);
                //设置按钮行为为弹窗
                props.button.setPopContent('Delete') /* 设置操作列上删除按钮的弹窗提示 */               
            }
        },
        Cancel:{
            afterClick: function(ncProps){
                props.button.setMainButton('Add', true);
                //设置按钮行为为弹窗
                props.button.setPopContent('Delete',multiLang && multiLang.get('105601GGZD-0060')) /* 设置操作列上删除按钮的弹窗提示 */
            }
        }
    }
}
export default buttonEvent