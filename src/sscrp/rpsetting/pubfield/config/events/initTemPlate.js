import {getMultiLang} from 'nc-lightapp-front';
import 'ssccommon/components/globalPresetVar';
import {setTableExtendCol} from 'ssccommon/components/profile'
import requestApi from '../requestApi';
// import fixedButton from './fixedButton';
import {buttonEvent} from './index'
import { beforeUpdatePage, updatePage } from 'ssccommon/utils/updatePage';


export default function (props) {

    let that = this;
    //变量设置规范，如需要
    window.presetVar = {
        ...window.presetVar,
        pageId: '20110pubfield',
        listArea: 'publicfield',
        btnArea: 'btnArea',
        listBtnArea: 'listbtnarea'
    };
    let createUIDomPromise = new Promise((resolve, reject) => {
        props.createUIDom(
            {},
            (data) => {
                resolve(data);   
            }
        )
    })

    let getMultiLangPromise = new Promise((resolve, reject) => {
        getMultiLang({
            moduleId: 1056, domainName: 'sscrp',currentLocale: 'zh-CN', callback: (json) => {
                resolve(json);
            }
        });

    })

    Promise.all([createUIDomPromise, getMultiLangPromise]).then((resultList) => {
        let data = resultList[0];
        let json = resultList[1];
         //设置区域模板
         props.meta.setMeta(data.template);
         

         //设置初始数据
         //获取表格的数据
         requestApi.getTableData({
             data: {},
             success: (data) => {
                beforeUpdatePage(props);
                props.editTable.setTableData(window.presetVar.listArea, data[window.presetVar.listArea]);
                updatePage(props, window.presetVar.listArea);
             }
         })
         //设置按钮模板
         //props.button.setButtons(fixedButton);
         props.button.setButtons(data.button);
         //设置表格的扩展按钮列
         setTableExtendCol(props, data.template, [{
             areaId: window.presetVar.listArea,
             btnAreaId: window.presetVar.listBtnArea,
             buttonVisible: (record, index) => {
                 return ['Delete']
             },
             onButtonClick: buttonEvent.bind(that)
         }]); 
         

         //设置按钮行为为弹窗
         props.button.setPopContent('Delete',json['105601GGZD-0060']) /*105601GGZD-0060:确认要删除该信息吗？ 设置操作列上删除按钮的弹窗提示 */

         //设置按钮的初始可见性
         props.button.setButtonsVisible({'Save': false, 'Cancel': false});
    })
}
