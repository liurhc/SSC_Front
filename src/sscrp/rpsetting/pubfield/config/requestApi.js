import requestApi from "ssccommon/components/requestApi";
import {ajax } from 'nc-lightapp-front';

let requestApiOverwrite = {
    ...requestApi,
    getTableData:(opt) => {
        ajax({
            url: `/nccloud/sscrp/rpbill/PublicFieldQryAction.do`,
            data: opt.data,
            async:false,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    }
}
export default  requestApiOverwrite;