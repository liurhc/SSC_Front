import logs from 'ssccommon/utils/logs';
let debugLogs = new logs();
let useUpdatePageFlag = localStorage.getItem('useUpdatePageFlag') || 'true';
let beforeUpdatePage = function(props) {
    if (useUpdatePageFlag == 'true') {
        debugLogs.timeStart('beforeUpdatePage');
        props.beforeUpdatePage();
        debugLogs.timeEnd('beforeUpdatePage');
    }
}
let updatePage = function(props, headAreas, bodyAreas) {
    if (useUpdatePageFlag == 'true') {
        debugLogs.timeStart('updatePage' + 'bodyAreas:' + JSON.stringify(headAreas) + '; bodyAreas:' + JSON.stringify(bodyAreas));
        props.updatePage(headAreas, bodyAreas);
        debugLogs.timeEnd('updatePage' + 'bodyAreas:' + JSON.stringify(headAreas) + '; bodyAreas:' + JSON.stringify(bodyAreas));
    }
}
export { beforeUpdatePage, updatePage };