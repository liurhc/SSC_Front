import logs from 'ssccommon/utils/logs';
let debugLogs = new logs();
let setPageData = function (props, headAreas, bodyAreas, data) {
    headAreas && headAreas.map((one) => {

    })
}
let updataPageData = function (props, headAreas, bodyAreas, data) {

}
let setPageDataByGridrelation = function (props, headAreas, data) {
    let gridrelation = (props.meta.getMeta() || {}).gridrelation;
    let bodyAreas = [];
    if (gridrelation) {
        for (let name in gridrelation) {
            bodyAreas.push(name);
        }
    }
    setPageData(props, headAreas, bodyAreas, data);
}
let updataPageDataByGridrelation = function (props, headAreas, data) {
    let gridrelation = (props.meta.getMeta() || {}).gridrelation;
    let bodyAreas = [];
    if (gridrelation) {
        for (let name in gridrelation) {
            bodyAreas.push(name);
        }
    }
    updataPageData(props, headAreas, bodyAreas, data);
}
let setFormData = function (props, formId, data) {
    debugLogs.timeStart('setFormData:' + formId);
    props.form.setAllFormValue({ [formId]: data });
    debugLogs.timeEnd('setFormData:' + formId);
}
let updataFormData = function (props, formId, data) {
    debugLogs.timeStart('setFormData:' + formId);
    props.form.setAllFormValue({ [formId]: data });
    debugLogs.timeEnd('setFormData:' + formId);
}
let setcardTableData = function (props, tableId, data) {
    debugLogs.timeStart('setcardTableData:' + tableId);
    props.cardTable.setTableData(tableId, data);
    debugLogs.timeEnd('setcardTableData:' + tableId);
}
let updatacardTableData = function (props, tableId, data) {
    debugLogs.timeStart('updateTableData:' + tableId);
    props.cardTable.updateDataByRowId(tableId, data);
    debugLogs.timeEnd('updateTableData:' + tableId);
}
export {
    setPageData, updataPageData, setPageDataByGridrelation, updataPageDataByGridrelation,
    setFormData, updataFormData, setcardTableData, updatacardTableData
}