let setUrlParams = function(props, url, params){
    props.setUrlParam(params);
}
let setUrlParam = function(key, value){
    let params = window.location.hash.split('&');
    let have = false;
    params.map((one, index)=>{
        if(one.indexOf(key)>=0){
            have = true;
            params[index] = key + '=' + value;
        }
    })
    if(have==false){
        params.push(key + '=' + value);
    }
    window.location.hash = params.join('&');
}
let setDefaultValue = function(data, head, flag){
    let referResultFilterCondition = JSON.parse(data.data.userjson);
    this.props.form.setAllFormValue({[head]: data.data.head[head]});
    let headValues = data.data.head[head].rows[0].values;
    this.props.meta.getMeta()[head].items.forEach((item) => {
        if(headValues.hasOwnProperty(item.attrcode)){
            let defValue = headValues[item.attrcode];
            if(defValue.value != null){
                item.initialvalue = {
                    value: defValue.value,
                    display: defValue.display || defValue.value
                };
            }
            let filterCondition = referResultFilterCondition[head][item.attrcode];
            if (filterCondition) {
                item.referFilterAttr = {[item.attrcode] :  filterCondition};
            }
        }
    });
    for(let body in this.props.meta.getMeta().gridrelation){
        let bodyValues = data.data.body[body].rows[0].values;
        this.props.meta.getMeta()[body].items.forEach((item) => {
            if(bodyValues.hasOwnProperty(item.attrcode)){
                let defValue = bodyValues[item.attrcode];
                if(defValue.value != null){
                    item.initialvalue = {
                        value: defValue.value,
                        display: defValue.display || defValue.value
                    };
                }
                let filterCondition = referResultFilterCondition[body][item.attrcode];
                if (filterCondition) {
                    item.referFilterAttr = {[item.attrcode] :  filterCondition};
                }
            }
        });
    }
}
/**
 * 取得pagecode
 * @param {*} props 
 */
let getPageCode=function(props){
    return props.getSearchParam('p');
}
/**
 * 取得appcode
 * @param {*} props 
 */
let getAppCode=function(props){
    return props.getSearchParam('c');
}
export default {setUrlParam, setDefaultValue, setUrlParams}