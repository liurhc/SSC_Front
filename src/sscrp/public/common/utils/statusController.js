import logs from 'ssccommon/utils/logs';
let debugLogs = new logs();
let setPageStatusForMeta = function(props, meta, headAreaIDs, bodyAreaIds, status){
    let hasChangeFlag = false;
    headAreaIDs.map((one)=>{
        if(meta[one] && meta[one].status != status){
            meta[one].status = status;
            hasChangeFlag = true;
        }
    })
    bodyAreaIds.map((one)=>{
        if(meta[one] && meta[one].status != status){
            meta[one].status = status;
            hasChangeFlag = true;
        }
    })
    return hasChangeFlag;
}
let setPageStatus = function(props, meta, headAreaIDs, bodyAreaIds, status){
    let hasChangeFlag = false;
    //新增单据打开时性能优化
    let addfalg = false;
    if(status == 'add')
    {
        props.beforeUpdatePage();
        addfalg = true;
    }
  

    headAreaIDs.map((one)=>{
        // if(props.form.getFormStatus(one) != status){
            debugLogs.timeStart('setFormStatus_' + one + '_' + status);
            // 平台问题，设置表单的新增态时必须使用meta

            // meta[one] && (meta[one].status = status);
            props.form.setFormStatus(one, status);
            
            debugLogs.timeEnd('setFormStatus_' + one + '_' + status);
            hasChangeFlag = true;
        // }
    })


    bodyAreaIds.map((one)=>{
        if(props.cardTable.getStatus(one) != status){
            if(status == 'add'){
                status = 'edit';
            }
            debugLogs.timeStart('setTableStatus_' + one + '_' + status);
            props.cardTable.setStatus(one, status);
            debugLogs.timeEnd('setTableStatus_' + one + '_' + status);
            hasChangeFlag = true;
        }
    })
     //新增单据打开时性能优化
    //if(addfalg)
   // props.updatePage(headAreaIDs,bodyAreaIds)
    // headAreaIDs.map((one)=>{
    //     if(meta[one] && meta[one].status != status){
    //         debugLogs.timeStart('setFormStatus_' + one + '_' + status);
    //         meta[one].status = status;
    //         debugLogs.timeEnd('setFormStatus_' + one + '_' + status);
    //         hasChangeFlag = true;
    //     }
    // })
    // bodyAreaIds.map((one)=>{
    //     if(meta[one] && meta[one].status != status){
    //         debugLogs.timeStart('setTableStatus_' + one + '_' + status);
    //         meta[one].status = status;
    //         debugLogs.timeEnd('setTableStatus_' + one + '_' + status);
    //         hasChangeFlag = true;
    //     }
    // })
    return hasChangeFlag;
}
let showColByKey = function(props, meta, bodyAreaIds, fieldId){
    let hasChangeFlag = false;
    bodyAreaIds.map((one)=>{
        let field = meta[one].items.find(e=>e.attrcode === fieldId);
        if((field || {}).visible != true){
            field.visible = true;
            hasChangeFlag = true;
        }
    })
    return hasChangeFlag;
}
let hideColByKey = function(props, meta, bodyAreaIds, field){
    let hasChangeFlag = false;
    bodyAreaIds.map((one)=>{
        let field = meta[one].items.find(e=>e.attrcode === fieldId);
        if((field || {}).visible != false){
            field.visible = false;
            hasChangeFlag = true;
        }
    })
    return hasChangeFlag;
}

let getPageStatus=function(props){
    return props.getUrlParam('status');
}
export {setPageStatusForMeta, setPageStatus, getPageStatus, showColByKey, hideColByKey}