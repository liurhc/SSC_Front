
class logs{
    constructor() {
        this.debugFlag = localStorage.getItem('useDebugLog') || '';
        this.number = {};
    }
    sumNumBer(key){
        if(this.debugFlag != '' && (
            this.debugFlag.indexOf('[A]') >= 0 || this.debugFlag.indexOf('[N]') >= 0
            || this.debugFlag.indexOf('[' + key + ']') >= 0
        )){
            if(!this.number[key]){
                this.number[key] = 0;
                setTimeout(()=>{
                    this.number[key] = null;
                },10000);
            }
            this.number[key] = this.number[key] + 1;
            console.log('Start:'+(key + '------------------------------------------------------------').substring(0,50) 
            + 'Number:'+((this.number[key] + '') + '----------').substring(0,10) 
            + ((new Date()).getTime() + ''));
        }
    }
    log(data){
        if(this.debugFlag != '' && (
            this.debugFlag.indexOf('[A]') >= 0 || this.debugFlag.indexOf('[L]') >= 0
            || this.debugFlag.indexOf('[' + data + ']') >= 0
        )){
            console.log(data);
        }
    }
    debugTimeToLog(key, msg){
        if(this.debugFlag != '' && (
            this.debugFlag.indexOf('[A]') >= 0 || this.debugFlag.indexOf('[TL]') >= 0
            || this.debugFlag.indexOf('[' + key + ']') >= 0
        )){
            console.log((key + '------------------------------------------------------------').substring(0,50) + (new Date()).getTime());
            if(msg){
                console.log('          ' + msg);
            }
        }
    }
    timeStart(key){
        switch(this.debugFlag){
            case '[A]':
                console.time(key);
                console.log('Start:'+(key + '------------------------------------------------------------').substring(0,50) + ((new Date()).getTime() + ''));
                break;
            case '[T]':
                console.time(key);
                break;
            case '[L]':
                console.log('Start:'+key);
                break;
            case '[TL]':
                console.log('Start:'+(key + '------------------------------------------------------------').substring(0,50) + ((new Date()).getTime() + ''));
                break;
        }
    }
    timeEnd(key){
        switch(this.debugFlag){
            case '[A]':
                console.timeEnd(key);
                console.log('End--:'+(key + '------------------------------------------------------------').substring(0,50) + ((new Date()).getTime() + ''));
                break;
            case '[T]':
                console.timeEnd(key);
                break;
            case '[L]':
                console.log('End--:'+key);
                break;
            case '[TL]':
                console.log('End--:'+(key + '------------------------------------------------------------').substring(0,50) + ((new Date()).getTime() + ''));
                break;
        }
    }
} 
export default logs;
