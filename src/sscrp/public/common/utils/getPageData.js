class getPageData{
    constructor(props, notAmountFields) {
        this.props = props;
        this.notAmountFields = notAmountFields || [];
    }
    removeTableEmptyData(data){
        if(data.bodys){
            for(let bodyName in data.bodys){
                if(data.bodys[bodyName].rows){
                    data.bodys[bodyName].rows.map((one)=>{
                        if(one.values){
                            for(let name in one.values){
                                if(!one.values[name].value){
                                    delete one.values[name];
                                }
                            }
                        }
                    })
                }
            }
        }
        return data;
    }
    getPageDataAndRemoveTableEmptyData(pageId, headId, tableIds){
        let refData = this.getPageData(pageId, headId, tableIds);
        return this.removeTableEmptyData(refData);
    }
    getPageData(pageId, headId, tableIds){
        let pageData = null;
        if(tableIds.length == 1){
            pageData = this.props.createMasterChildData(pageId, headId, tableIds[0]);
        }else{
            pageData = this.props.createExtCardData(pageId, headId, tableIds);
        }
        if(pageData != null){
            // 循环所有表体
            tableIds.map((tableOne)=>{
                //循环所有行
                (((pageData.bodys || {})[tableOne] || {}).rows || []).map((rowOne)=>{
                    // 只有新增的行数据才需要处理
                    if(rowOne.status == '2'){
                        let meta = this.props.meta.getMeta();
                        // 循环所有字段
                        (meta[tableOne].items || []).map((fieldOne)=>{
                            // 金额的字段才做补充
                            if(fieldOne.itemtype == 'number' 
                                && fieldOne.attrcode.indexOf('defitem') < 0
                                && this.notAmountFields.indexOf(fieldOne.attrcode) < 0){
                                    if(rowOne.values[fieldOne.attrcode] && rowOne.values[fieldOne.attrcode].value == null){
                                        rowOne.values[fieldOne.attrcode].value = 0;
                                    }
                            }
                        })
                    }
                });
            })
        }
        return pageData;
    }
}export default getPageData;