import { toast, getMultiLang } from 'nc-lightapp-front';

class pubMessage {
    constructor(props) {
        this.props = props;
        this.multiLang = {};
        getMultiLang({moduleId: 1056, domainName: 'sscrp',currentLocale: 'zh-CN', callback: (json) => {
            this.multiLang = json;
        }})
    }
    querySuccess(number = 0) {
        if(number==0)
        {
            // toast({content: `未查询出符合条件的数据`,color:'warning'});//UE要求0的时候用预警
            toast({content: this.multiLang['105601GGZD-0069'],color:'warning'});//UE要求0的时候用预警
        }else{
            // toast({ content: `查询成功，共${number}条。`});
            toast({ content: this.multiLang['105601GGZD-0065'] + "，" + this.multiLang['105601GGZD-0062'] + number + this.multiLang['105601GGZD-0063']});
        }
    }
    refreshSuccess() {
        toast({ title: this.multiLang['105601GGZD-0066']/* '刷新成功！' */ })
    }
    setPopContentRowDelete(buttons) {
        buttons.map((one) => {
            this.props.button.setPopContent(one, this.multiLang['105601GGZD-0060']/* '确认要删除该行数据吗？' */);
        })
    }
    saveSuccess() {
        toast({ title: this.multiLang['105601GGZD-0067']/* '保存成功！' */ })
    }
    copySuccess() {
        toast({ title: this.multiLang['105601GGZD-0071']/* '复制成功！' */ })
    }
}
export default pubMessage
