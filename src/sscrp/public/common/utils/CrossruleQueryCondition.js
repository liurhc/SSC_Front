class CrossruleQueryCondition {
    /**
     * 
     * @param {*} props 
     * @param {object} params 
     * @param {string} params.headName 表头名称
     * @param {string | string[]} params.bodyName 表体名称 单表体为字符串、多表体为字符数组
     * @param {string} params.tradetypeFeild 交易类型字段名 默认pk_tradetype
     * @param {string} params.pkOrgField 组织字段名 默认pk_org
     * @param {boolean} params.isExtbillcard 是否为多表体 true是/false否 默认false
     * @param {string} params.getCurrentRowRecord 取得当前表体操作行数据函数
     * @param {string} params.publicDefaultGridRef 交叉校验规则默认处理类 默认nccloud.web.action.erm.ref.sqlbuilder.PublicDefaultRefSqlBuilder
    */
    constructor(props, params) {
        let { headName, bodyName, tradetypeFeild, pkOrgField, isExtbillcard, getCurrentRowRecord, publicDefaultGridRef} = params;
        this.props = props;
        this.headName = headName;
        this.bodyName = bodyName;
        this.tradetypeFeild = tradetypeFeild || 'pk_tradetype';
        this.pkOrgField = pkOrgField || 'pk_org';
        this.getCurrentRowRecord = getCurrentRowRecord;
        this.isExtbillcard = isExtbillcard || false;
        this.publicDefaultGridRef = publicDefaultGridRef || 'nccloud.web.action.erm.ref.sqlbuilder.PublicDefaultRefSqlBuilder';
    }
    /**
     * 设置交叉校验规则默认处理类
     * @param {*} className 处理类名（包含包路径）
     */
    setPublicDefaultGridRef(className) {
        this.publicDefaultGridRef = className;
    }
    /**
     * 设置取得当前表体操作行数据函数
     * @param {*} getCurrentRowRecord 
     */
    setGetCurrentRowRecordFunction(getCurrentRowRecord) {
        this.getCurrentRowRecord = getCurrentRowRecord;
    }
    /**
     * 设置参照过滤事件
     * @param {object} item 模板对象
     * @param {string} item.itemtype 字段类型
     * @param {string} item.attrcode 字段编码
     * @param {function} item.queryCondition 参照回掉方法
     * @param {string} areaCode 区域编码
     */
    setCondition(item, areaCode) {
        if (item.itemtype == 'refer') {
            let oldQueryCondition = item.queryCondition;
            let crossrule_itemkey = item.attrcode;
            item.queryCondition = (obj) => {
                let crossrule_datasout = null;
                let crossrule_datatypes = null;
                // 取得页面数据
                if (this.isExtbillcard == true) {
                    crossrule_datasout = this.props.createExtCardData(this.props.meta.getMeta().code, this.headName, this.bodyName);
                    crossrule_datatypes = 'extbillcard';
                } else {
                    crossrule_datasout = this.props.createMasterChildData(this.props.meta.getMeta().code, this.headName, this.bodyName);
                    crossrule_datatypes = 'billcard';
                }
                // 取得交易类型
                let crossrule_tradetype = this.props.form.getFormItemsValue(this.headName, this.tradetypeFeild).value;
                // 取得财务组织
                let crossrule_org = this.props.form.getFormItemsValue(this.headName, this.pkOrgField).value;

                let crossrule_datas = null;
                let currentRowRecord = null;
                if (this.getCurrentRowRecord) {
                    currentRowRecord = this.getCurrentRowRecord();
                }

                // 如果操作的是表体则只取表体当前行数据
                if (areaCode != this.headName && currentRowRecord) {
                    if (this.isExtbillcard == true) {
                        crossrule_datasout.bodys[areaCode].rows = [currentRowRecord]
                    } else {
                        crossrule_datasout.body[areaCode].rows = [currentRowRecord]
                    }
                    crossrule_datas = crossrule_datasout;
                } else {
                    crossrule_datas = crossrule_datasout;
                }

                this.changBillData(crossrule_datas);

                let oldData = '';
                if (oldQueryCondition)
                    oldData = oldQueryCondition();
                if (oldData == null || oldData == undefined || oldData == 'undefined')
                    oldData = '';

                let config = {
                    crossrule_datas: JSON.stringify(crossrule_datas),
                    crossrule_tradetype: crossrule_tradetype,
                    crossrule_org: crossrule_org,
                    crossrule_datatypes: crossrule_datatypes,
                    crossrule_area: areaCode,
                    crossrule_itemkey: crossrule_itemkey,
                    ...oldData
                };

                //参照类型的自定义项字段增加组织过滤
                if (!oldData.pk_org) {
                    config.pk_org = crossrule_org;
                }

                if (obj == undefined || obj == null || obj.refType == undefined || obj.refType == null) {
                    if (config.TreeRefActionExt == null || config.TreeRefActionExt == undefined || config.TreeRefActionExt.length == 0)
                        config.TreeRefActionExt = this.publicDefaultGridRef;
                    return config;
                }

                if (obj.refType == 'grid') {
                    if (config.GridRefActionExt == null || config.GridRefActionExt == undefined || config.GridRefActionExt.length == 0)
                        config.GridRefActionExt = this.publicDefaultGridRef;
                } else if (obj.refType == 'tree') {
                    if (config.TreeRefActionExt == null || config.TreeRefActionExt == undefined || config.TreeRefActionExt.length == 0)
                        config.TreeRefActionExt = this.publicDefaultGridRef;
                } else if (obj.refType == 'gridTree') {
                    if (config.GridRefActionExt == null || config.GridRefActionExt == undefined || config.GridRefActionExt.length == 0)
                        config.GridRefActionExt = this.publicDefaultGridRef;
                }
                return config;
            }
        }
    }
    /**
     * 优化数据，取出不必要的信息，提升传输效率
     * @param {*} data 
     */
    changBillData(data) {
        if (this.isExtbillcard == true) {

        }
    }
} export default CrossruleQueryCondition