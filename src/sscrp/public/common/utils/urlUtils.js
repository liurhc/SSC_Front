/**
 * 设置URL参数
 * 
 * @param {*} props 
 * @param {*} url 
 * @param {*} params 
 */
let setUrlParam = function(props, url, params){
    props.setUrlParam(params);
}
/**
 * 取得URL参数
 *
 * @param {*} props 
 * @param {*} key 
 */
let getUrlParam = function(props, key){
    return props.getUrlParam('key');
}

export {setUrlParam, getUrlParam}