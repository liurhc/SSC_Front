import { setTimeout } from "timers";
import logs from 'ssccommon/utils/logs';
let debugLogs = new logs();

let allButtons = [];
let getAllButtons = function(buttons){
    buttons && buttons.map((buttonOne)=>{
        allButtons.push(buttonOne);
        if((buttonOne.children || []).length>0){
            getAllButtons(buttonOne.children);
        }
    });
}
let setButtonsVisible = function(props, btnsVisibleObj){
    
    let buttons = props.button.getButtons();
    allButtons = [];
    getAllButtons(buttons);
    let newBtnsVisibleObj = {};
    allButtons && allButtons.map((buttonOne)=>{
        if(btnsVisibleObj[buttonOne.key] != null
            && buttonOne.visible != btnsVisibleObj[buttonOne.key]){
            newBtnsVisibleObj[buttonOne.key] = btnsVisibleObj[buttonOne.key];
        }
    })
    if(Object.keys(newBtnsVisibleObj).length > 0){
        debugLogs.timeStart('setButtonsVisible');
        props.button.setButtonsVisible(newBtnsVisibleObj);
        debugLogs.timeEnd('setButtonsVisible');
    }
}
export {setButtonsVisible }