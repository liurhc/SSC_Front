import CrossruleQueryCondition from 'ssccommon/utils/CrossruleQueryCondition';
/**
 * 编辑态页面初始设置
 */
class InitEditForBillCard {
    /**
     * 编辑态页面初始设置　构造方法
     * 
     * @param {*} props 
     * @param {object} params 参数
     * @param {object} params.meta meta
     * @param {string} params.headName 表头名称 默认从formrelation取得
     * @param {string | string[]} params.bodyName 表体名称 单表体为字符串、多表体为字符数组
     * @param {string} params.bodyCardName 表体侧拉附加串　默认childform2
     * @param {boolean} params.hasCrossrule 是否使用交叉校验规则　true是/false否　默认true
     * @param {boolean} params.isExtbillcard 是否为多表体 true是/false否 默认false
     * @param {boolean} params.bodyMoneyNoValue 表体金额不设置默认值 true是/false否 默认true
     * @param {string[]} params.numberInitialvalueField 表体非金额的字段列表
     * @param {string[]} params.withoutBodyName 不进行初始化操作的表体列表
     * @param {string} params.tradetypeFeild 交易类型字段名 默认pk_tradetype
     * @param {string} params.pkOrgField 组织字段名 默认pk_org
     * @param {string} params.publicDefaultGridRefClass 交叉校验规则默认处理类 默认nccloud.web.action.erm.ref.sqlbuilder.PublicDefaultRefSqlBuilder
     * @param {function} params.refQueryConditionFunction 参照过滤处理函数
     * @param {function} params.getCurrentRowRecordFunction 取得当前表体操作行数据函数
     */
    constructor(props, params) {
        this.props = props;
        let {
            meta,
            headName,
            bodyName,
            bodyCardName,
            hasCrossrule,
            isExtbillcard,
            bodyMoneyNoValue,
            numberInitialvalueField,
            withoutBodyName,
            tradetypeFeild,
            pkOrgField,
            publicDefaultGridRefClass,
            refQueryConditionFunction,
            getCurrentRowRecordFunction
        } = params;
        // 默认值相关参数
        this.isExtbillcard = isExtbillcard || false;
        this.withoutBodyName = withoutBodyName || [];
        this.meta = meta || this.props.meta.getMeta();
        this.headName = headName;
        // 如果没有设置表头名称，则从formrelation中取得表头名称
        if (!this.headName && this.meta && this.meta.formrelation) {
            for (let name in this.meta.formrelation) {
                this.headName = name;
                break;
            }
        }
        // 如果formrelation没有取到表头名称，则设置默认名称
        this.headName = this.headName || 'head';
        // 设置表体名称
        this.bodyName = bodyName;
        // 如果没有设置表体名称，则从gridrelation中取得表体名称
        if (!this.bodyName && (this.meta || {}).gridrelation) {
            if (this.isExtbillcard) {
                this.bodyName = [];
                for (let name in this.meta.gridrelation) {
                    if (this.withoutBodyName.indexOf(name) < 0) {
                        this.bodyName.push(name);
                    }
                }
            } else {
                for (let name in this.meta.gridrelation) {
                    this.bodyName = name;
                    break;
                }
            }
        }
        this.bodyCardName = bodyCardName || 'childform2';
        this.refQueryConditionFunction = refQueryConditionFunction;
        this.bodyMoneyNoValue = bodyMoneyNoValue || true;
        this.numberInitialvalueField = numberInitialvalueField;
        // 交叉校验规则相关参数
        this.hasCrossrule = hasCrossrule || true;
        this.tradetypeFeild = tradetypeFeild || 'pk_tradetype';
        this.pkOrgField = pkOrgField || 'pk_org';
        this.getCurrentRowRecordFunction = getCurrentRowRecordFunction;
        this.publicDefaultGridRefClass = publicDefaultGridRefClass || 'nccloud.web.action.erm.ref.sqlbuilder.PublicDefaultRefSqlBuilder';

        // 交叉校验规则处理类
        this.crossruleQueryCondition = new CrossruleQueryCondition(this.props,
            {
                headName: this.headName,
                bodyName: this.bodyName,
                tradetypeFeild: this.tradetypeFeild,
                pkOrgField: this.pkOrgField,
                isExtbillcard: this.isExtbillcard,
                getCurrentRowRecord: this.getCurrentRowRecordFunction,
                publicDefaultGridRef: this.publicDefaultGridRefClass
            }
        );
    }
    /**
     * 编辑态页面初始设置
     * 
     * @param {object} data 初始数据
     * @param {object} data.head 表头数据 
     * @param {object} data.body 单表体数据
     * @param {object} data.bodys 多表体数据
     */
    doInit(data) {
        // 取得表体列表
        let bodyArray = [];
        if (this.isExtbillcard) {
            this.bodyName.map((one) => {
                if (this.withoutBodyName.indexOf(one) < 0) {
                    bodyArray.push({
                        bodyNames: [one, one + '&' + this.bodyCardName],
                        bodyListName: one,
                        bodyCardName: one + '&' + this.bodyCardName
                    });
                }
            })
        } else {
            bodyArray.push({
                bodyNames: [this.bodyName, this.bodyCardName],
                bodyListName: this.bodyName,
                bodyCardName: this.bodyCardName
            });
        }
        // 取得表头数据
        let headValues = data.head[this.headName].rows[0].values;
        // 取得表头区域列表
        let headNames = [this.headName];
        this.meta.formrelation && this.meta.formrelation[this.headName] && this.meta.formrelation[this.headName].map((one) => {
            headNames.push(one);
        })
        // 设置表头默认数据
        headNames.map((headNameOne) => {
            this.meta[headNameOne].items.forEach((item) => {
                if (headValues[item.attrcode]) {
                    // 设置默认值
                    item.initialvalue = {
                        value: headValues[item.attrcode].value,
                        display: headValues[item.attrcode].display || headValues[item.attrcode].value
                    };
                    // 设置精度
                    item.scale = headValues[item.attrcode].scale;
                }
                // 处理参照过滤
                this.refQueryConditionFunction && this.refQueryConditionFunction(item, this.props, headNameOne);
                // 处理交叉校验规则
                if (this.hasCrossrule) {
                    this.crossruleQueryCondition.setCondition(item, headNameOne);
                }
            });
        });

        // 设置表体默认数据
        bodyArray.map((bodyOne) => {
            let bodyValues = {};
            if (this.isExtbillcard) {
                bodyValues = data.bodys[bodyOne.bodyListName].rows[0].values;
            } else {
                bodyValues = data.body[bodyOne.bodyListName].rows[0].values;
            }
            bodyOne.bodyNames.map((bodyListCardOne) => {
                this.meta[bodyListCardOne].items.forEach((item) => {
                    if (bodyValues[item.attrcode]) {
                        // 设置默认值
                        if (this.bodyMoneyNoValue) {
                            if (item.itemtype != 'number' || this.numberInitialvalueField.indexOf(item.attrcode) >= 0) {
                                item.initialvalue = {
                                    value: bodyValues[item.attrcode].value,
                                    display: bodyValues[item.attrcode].display || bodyValues[item.attrcode].value,
                                };
                            } else {
                                item.initialvalue = {
                                    scale: bodyValues[item.attrcode].scale
                                };
                            }
                        } else {
                            item.initialvalue = {
                                value: bodyValues[item.attrcode].value,
                                display: bodyValues[item.attrcode].display || bodyValues[item.attrcode].value,
                            };
                        }
                        // 设置精度
                        item.scale = bodyValues[item.attrcode].scale;
                    }
                    // 处理参照过滤
                    this.refQueryCondition && this.refQueryCondition(item, props, bodyListCardOne);
                    // 处理交叉校验规则
                    if (this.hasCrossrule) {
                        this.crossruleQueryCondition.setCondition(item, bodyListCardOne);
                    }
                })
            })
        })
    }
} export default InitEditForBillCard