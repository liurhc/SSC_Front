/**
 * 生成一个不重复的ID，用在页面中出现多个重复的echarts图标，防止id重复，只能描绘第一个
 */
let randomId = function GenNonDuplicateID(randomLength){
    return Number(Math.random().toString().substr(3,randomLength) + Date.now()).toString(36)
}
export default randomId;