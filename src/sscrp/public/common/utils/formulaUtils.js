let getAfterEventParams = function(props, changedField, changedArea, oldPageCard, newPageCard, index, rowId){
    let oldCard = {};
    let newCard = {};
    let changedCard = [];
    let headArea = 'head';
    if('arap_bxzb' == changedArea){
        headArea = changedArea;
    }
    if((oldPageCard.card.head[headArea].rows || []).length > 0){
        oldCard.head = oldPageCard.card.head[headArea].rows[0];
        newCard.head = newPageCard.head[headArea].rows[0];
    }
    if(changedArea!=headArea && index != null){//传入的只有当前行记录，取0即可
        oldCard.body = oldPageCard.card.bodys ? oldPageCard.card.bodys[changedArea].rows[0] : oldPageCard.card.body[changedArea].rows[0];
        newCard.body = newPageCard.bodys ? newPageCard.bodys[changedArea].rows[0] : newPageCard.body[changedArea].rows[0];
    }

    if(oldCard.head){
        for(let fieldName in oldCard.head.values){
            if(!(fieldName == changedField && changedArea == headArea) && oldCard.head.values[fieldName].value != newCard.head.values[fieldName].value){
                changedCard.push({
                    moduleId: headArea,
                    key: fieldName,
                    value: newCard.head.values[fieldName],
                    changedrows: [{
                        rowid: newCard.head.rowid,
                        oldvalue : {
                            value: oldCard.head.values[fieldName].value
                        },
                        newvalue : {
                            value: newCard.head.values[fieldName].value
                        }
                    }],
                    index: index
                })
            }
        }
    }
    if(oldCard.body){
        for(let fieldName in oldCard.body.values){
            if(fieldName != changedField && oldCard.body.values[fieldName].value != newCard.body.values[fieldName].value){
                changedCard.push({
                    moduleId: changedArea,
                    key: fieldName,
                    value: newCard.body.values[fieldName].value,
                    changedrows: [{
                        rowid: newCard.body.rowid,
                        oldvalue : {
                            value: oldCard.body.values[fieldName].value
                        },
                        newvalue : {
                            value: newCard.body.values[fieldName].value
                        }
                    }],
                    index: index
                })
            }
        }
    }
    return changedCard;
}
export {getAfterEventParams}