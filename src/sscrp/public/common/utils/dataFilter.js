import logs from 'ssccommon/utils/logs';
let debugLogs = new logs();

/**
 * 取出空数据
 * @param {*} data 
 */
let removeEmptyData=function(data){
    debugLogs.timeStart('removeEmptyData');
    if(data.bodys){
        for(let bodyName in data.bodys){
            if(data.bodys[bodyName].rows){
                data.bodys[bodyName].rows.map((one)=>{
                    if(one.values){
                        for(let name in one.values){
                            if(!one.values[name].value){
                                delete one.values[name];
                            }
                        }
                    }
                })
            }
        }
    }
    if(data.head){
        for(let headName in data.head){
            if(data.head[headName].rows){
                data.head[headName].rows.map((one)=>{
                    if(one.values){
                        for(let name in one.values){
                            if(!one.values[name].value){
                                delete one.values[name];
                            }
                        }
                    }
                })
            }
        }
    }
    debugLogs.timeEnd('removeEmptyData');
    return data;
}

/**
 * 单据上传电文去掉Display和Scale
 * @param {*} data 
 */
let removeDisplayScale = function(data){
    debugLogs.timeStart('removeDisplayScale');
    if(data.bodys){
        for(let bodyName in data.bodys){
            if(data.bodys[bodyName].rows){
                data.bodys[bodyName].rows.map((one)=>{
                    if(one.values){
                        for(let name in one.values){
                            if(!one.values[name].value){
                                delete one.values[name];
                            }
                        }
                    }
                })
            }
        }
    }
    if(data.head){
        for(let headName in data.head){
            if(data.head[headName].rows){
                data.head[headName].rows.map((one)=>{
                    if(one.values){
                        for(let name in one.values){
                            if(!one.values[name].value){
                                delete one.values[name];
                            }
                        }
                    }
                })
            }
        }
    }
    debugLogs.timeEnd('removeDisplayScale');
    return data;
}
export {removeDisplayScale, removeEmptyData}