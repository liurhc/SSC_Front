import React, {Component} from 'react';


class Form extends Component {
    constructor() {
        super();
    }

    render() {
        const {createForm} = this.props.ncProps.form;
        return (
            <div className="area">
                { this.props.title ?
                    <div className="area-head">
                        <div className="title">
                            {this.props.title}
                        </div>
                    </div>
                    :
                    null
                }
                {createForm(this.props.uid, {
                    onAfterEvent: this.props.onAfterEvent
                })}
            </div>
        )
    }
}

export default Form
