import React, {Component} from 'react'
import {DongbaToLocalTime,getMultiLang} from 'nc-lightapp-front'
import moment from 'moment'

import './index.less';

class WorkbenchDetail extends Component {
    constructor(props) {
        super();
        this.state = {
            rows: [],
            templets: {},
            multiLang : {}
        }
        getMultiLang({moduleId: 1056, domainName: 'sscrp',currentLocale: 'zh-CN', callback: (json) => {
            this.setState({
                multiLang : json
            })
        }})
    }

    clearAllDetailData() {
        this.setState({
            rows: [],
            templets: {}
        });
    }

    addDetailData(config) {
        let oldRows = this.state.rows;
        let oldTemplets = this.state.templets;
        config.rows.map((one)=>{
            oldRows.push(one);
        })
        Object.assign(oldTemplets, config.templets);
        this.setState({
            rows: oldRows,
            templets: oldTemplets
        });
    }


    btnClick = (type, data) => {
        this.props.btnClick(type, data)
    }

    // 单据日期多时区处理
    getLocalTimeObject = (BeijinVal) => {
        return DongbaToLocalTime(moment(BeijinVal))
    }

    getLocalTime = (_i) => {
        let month = _i.getMonth() + 1
        if (month < 10) month = `0${month}`
        let date = _i.getDate()
        if (date < 10) date = `0${date}`
        return `${_i.getFullYear()}-${month}-${date}`
    }

    render() {
        const {props, getBtns, buttonArea} = this.props;

        let getTemplateShowValue=(rowOne,templateOne)=>{
            let itemTpl = rowOne.values[templateOne.attrcode]
            if(
                itemTpl && 
                itemTpl.display
            ){
                if (templateOne.attrcode === 'billdate') {
                    let { _i } = this.getLocalTimeObject(itemTpl.display)
                    return this.getLocalTime(_i)
                } else {
                    return itemTpl.display
                }
            }else if(itemTpl){
                if (itemTpl.value) {
                    if (templateOne.attrcode === 'billdate') {
                        let { _i } = this.getLocalTimeObject(itemTpl.value)
                        return this.getLocalTime(_i)
                    } else {
                        return itemTpl.value
                    }
                } else {
                    return null
                }
            }else{
                return null
            }
        }

        let getBtnsP = (data, index) => {
            return props.button.createOprationButton(getBtns((data || {}).values, index), {
                area: buttonArea,
                buttonLimit: 3,
                onButtonClick: (props, btnKey) => {
                    // console.log('明细按钮事件', btnKey, data, index, "detail");
                    this.props.doAction.call(this, btnKey, (data || {}).values, index, "detail")
                }
            });;
        }

        return(
            <div className="ssc-cells" id="WorkbenchDetail">
            {
                this.state.rows.map((rowOne,index) => {
                    if(rowOne == null){
                        return "";
                    }
                    // let ssctaskregister = data.ssctaskregister.id;
                    // let ssctaskregisterName = data.ssctaskregister.name;
                    let thisTemplate = this.state.templets[rowOne.tradetypecode] == null 
                        ? 
                        [] 
                        : 
                        this.state.templets[rowOne.tradetypecode]
                    // 显示顺序
                    thisTemplate.sort((a, b) => a.position - b.position)
                    let rollbackClassName = "";
                    // if(data.previousoprate == "驳回"||data.previousoprate == "共享驳回"){
                    //     rollbackClassName="rollback_background_detail";
                    // }
                    let getBillNoLink=(rowOne, index)=>{
                        if(rowOne!=null && rowOne.values != null && rowOne.values.billno != null && rowOne.values.billno.value != null){
                            let ret = [
                                (
                                    <a 
                                        className="billnoa" 
                                        onClick={this.props.doAction.bind(this, "OpenBill", rowOne.values, index, "detail")}>
                                        {rowOne.values.billno.value}
                                    </a>
                                )
                            ]
                            if (rowOne.values.urgent && rowOne.values.urgent.value == 'Y') {
                                ret.push(<span className="billno-reject">{this.state.multiLang['1056common-0022']/*紧急*/}</span>)
                            }
                            if (rowOne.values.isreject && rowOne.values.isreject.value == 'Y') {
                                ret.push(<span className="billno-reject">{this.state.multiLang['1056common-0023']/*驳回*/}</span>)
                            }
                            if(rowOne.values.ismereject && rowOne.values.ismereject.value == 'Y'){
                                ret.push(<span className="billno-reject">{this.state.multiLang['1056common-0024']/*本人驳回*/}</span>)
                            }
                            if(rowOne.values.isleaderreject && rowOne.values.isleaderreject.value == 'Y'){
                                ret.push(<span className="billno-reject">{this.state.multiLang['1056common-0025']/*上级驳回*/}</span>)
                            }
                            if (rowOne.values.isappointed && rowOne.values.isappointed.value == 'Y') {
                                ret.push(<span className="billno-reject">{this.state.multiLang['1056common-0026']/*强制分配*/}</span>)
                            } 
                            return ret
                        }else{
                            return null
                        }
                    }
                    return (
                        <div className={rollbackClassName+" ssc-cell"} key={"detailArray_"+index}>
                            <div className="cell-top">
                                {/* TODO：多语 */}
                                <span className="num">{this.state.multiLang['1056common-0027']/*单据编号*/}</span>
                                <span className="cell-name">
                                    {getBillNoLink(rowOne, index)}
                                </span>
                                <div className="right">
                                    {getBtnsP(rowOne, index)}
                                </div>
                            </div>
                            <div className="cell">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>
                                                {index+1}
                                            </td>
                                            <td>
                                                <ul className='clearfix'>
                                                    {
                                                        thisTemplate.map((templateOne, templateIndex) => {
                                                            return (
                                                                templateOne.visible
                                                                ?
                                                                (<li
                                                                    key={"thisTemplate_"+templateIndex}
                                                                    className={'item-template-' + templateOne.colnum}
                                                                >
                                                                    <span className="field-key">
                                                                        {templateOne.label + '：'}
                                                                    </span>
                                                                    <span
                                                                        className="field-value"
                                                                        title={getTemplateShowValue(rowOne,templateOne)}
                                                                    >
                                                                        {
                                                                            getTemplateShowValue(rowOne,templateOne)
                                                                        }
                                                                    </span>
                                                                </li>)
                                                                :
                                                                null
                                                            )
                                                        })
                                                    }
                                                </ul>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>   
                        </div>
                    )
                })
            }
            </div>
        )
    }
}
export default WorkbenchDetail;
