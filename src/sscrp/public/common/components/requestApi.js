import {ajax as ncAjax} from 'nc-lightapp-front';

let ajax = (opt)=> {
    //TODO

    ncAjax(opt);
}

let requestApi = {

    //单据模板接口
    tpl: (opt) => {
        ajax({
            url: opt.url || '/nccloud/erm/expenseaccount/ExpenseaccountTempletAction.do',
            data: opt.data,
            success: opt.success
        });
    },
    queryUrl: (opt) => {
        ajax({
            url: opt.url || '/nccloud/erm/expenseaccount/AppUrlQueryAction.do',
            data: opt.data,
            success: (data) => {
                if (data.success) {
                    data = data.data;
                    opt.success(data);
                }
            }
        })
    },
    generatBillId:(opt) =>{
        ajax({
            url: opt.url || `/nccloud/erm/action/generatBillId.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            },
            error:(data) =>{
                alert(data.message);
            }
        });
    },
}

export default requestApi;
