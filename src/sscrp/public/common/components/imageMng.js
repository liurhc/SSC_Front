import {ajax, getMultiLang} from 'nc-lightapp-front'

var util = {};

let multiLang = null; 
getMultiLang({moduleId: 1056, domainName: 'sscrp',currentLocale: 'zh-CN', callback: (json) => {
    multiLang = json;
}})

//封装简易的Ajax
!function(util) {
    var vanillaAjax = {
        get: function(url, fn) {
            var xhr = new XMLHttpRequest();  // XMLHttpRequest对象用于在后台与服务器交换数据
            xhr.open('GET', url, true);
            xhr.onreadystatechange = function() {
                if (xhr.readyState == 4 && xhr.status == 200 || xhr.status == 304) { // readyState == 4说明请求已完成
                    fn.call(this, xhr.responseText);  //从服务器获得数据
                }
            };
            xhr.send();
        },
        post: function (url, data, fn) {         // datat应为'a=a1&b=b1'这种字符串格式，在jq里如果data为对象会自动将对象转成这种字符串格式
            var xhr = new XMLHttpRequest();
            xhr.open("POST", url, true);
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");  // 添加http头，发送信息至服务器时内容编码类型
            xhr.onreadystatechange = function() {
                if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 304)) {  // 304未修改
                    fn.call(this, JSON.parse(xhr.responseText));
                }
            };
            if (!data) {
                data={};
            }
            xhr.send(data);
        }
    }
    util.ajax = vanillaAjax;
}(util);

//jsonp组件
!function(util) {
    var jsonp = function(url, opts, fn){
        if ('function' == typeof opts) {
            fn = opts;
            opts = {};
        }
        if (!opts) opts = {};

        var prefix = opts.prefix || '__jp';
        var count = 0;
        // use the callback name that was passed if one was provided.
        // otherwise generate a unique name by incrementing our counter.
        var id = opts.name || (prefix + (count++));

        var param = opts.param || 'callback';
        var timeout = null != opts.timeout ? opts.timeout : 60000;
        var enc = encodeURIComponent;
        var target = document.getElementsByTagName('script')[0] || document.head;
        var script;
        var timer;


        if (timeout) {
            timer = setTimeout(function(){
                cleanup();
                if (fn) fn(new Error('Timeout'));
            }, timeout);
        }

        function cleanup(){
            if (script.parentNode) script.parentNode.removeChild(script);
            window[id] = null;
            if (timer) clearTimeout(timer);
        }

        function cancel(){
            if (window[id]) {
                cleanup();
            }
        }

        window[id] = function(data){
            cleanup();
            if (fn) fn(null, data);
        };

        // add qs component
        url += (~url.indexOf('?') ? '&' : '?') + param + '=' + enc(id);
        url = url.replace('?&', '?');

        // create script
        script = document.createElement('script');
        script.src = url;
        target.parentNode.insertBefore(script, target);

        return cancel;
    }
    util.jsonp = jsonp;
}(util);

//appendHtml 组件
!function(util) {
    var appendHTML = function (el, htmlStr) {
        var tempDiv = document.createElement('div');
        var fragment = document.createDocumentFragment();
        tempDiv.innerHTML = htmlStr;
        var nodes = tempDiv.childNodes;
        for (var i = 0, length = nodes.length; i < length; i++) {
            fragment.appendChild(nodes[i].cloneNode(true));
        }
        el.appendChild(fragment)
    }
    util.appendHTML = appendHTML;
}(util);


var w = 1440;
var h = 900;
var l = 1440;
var t = 900;

//影像扫描
function imageScan(data, openbillid, tradetype, checkscanway) {
    var data = data;
    if (!data)
        return;
    if (openbillid != null && openbillid != undefined) {
        data.head.openbillid = openbillid;
    }
    var param = {};
    param.tradetype = tradetype;
    var dataJson = JSON.stringify(data);
    var compressType = '';
    var compression = false;

    param.bill = dataJson;
    param.compressType = compressType;
    param.compression = compression;
    param.checkscanway = checkscanway;
    // var data2 = "tradetype="+param.tradetype+"&bill="+param.bill+"&compressType="+param.compressType+"&compression=" + param.compression+"&checkscanway="+param.checkscanway;
    ajax({
        url: '/nccloud/web/image/ImageUploadAction.do?time=' + new Date().getTime(), 
        data: param, 
        // headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'},
        success: function(data) {
            if (data["success"] == "true" || data["success"] == true) {
                var factoryCode = data["data"]["myHashMap"]["factoryCode"];
                var ipAddr = data["data"]["myHashMap"]["ipaddr"];
                var paraStr = '';
                if (factoryCode == "tchzt2") {
                    paraStr = data["data"]["myHashMap"]["parastr"];
                } else {
                    paraStr = encodeURI(encodeURI(data["data"]["myHashMap"]["parastr"]));
                }
                imageuploadBrowserByUrl(factoryCode, ipAddr, paraStr);
            } else {
                alert(data['error']['message']);
            }
        }
    });

}


function imageuploadBrowserByUrl(factoryCode, ipAddr, paraStr) {
    if (factoryCode == "tchzt") {
        var data = {
            url_img : "http://" + ipAddr + "/TIMS-Server/nc-jsp/imageupload.jsp?" + paraStr,
            url_download : "http://" + ipAddr + "/TIMS-Server/nc-jsp/downloadActivex.jsp"
        }
        return openTBrowser(data);
    } else if (factoryCode == "tchzt2") {
        return openTBrowser(paraStr);
    } else {
        window.open(paraStr);
    }
}

/**
 * 影像查看
 * @param data
 * @param openbillid
 * @param tradetype
 * @param permission
 */
function imageView(data, openbillid, tradetype, permission) {
    if (!data) {
        return;
    }
    if (openbillid != null && openbillid != undefined) {
        data.head.openbillid = openbillid;
    }
    var param = {};
    param.tradetype = tradetype;
    var dataJson = JSON.stringify(data);
    var compressType = '';
    var compression = false;
    param.bill = dataJson; //此处必须编码，否则接口报decode fail.错误
    param.compressType = compressType;
    param.compression = compression;
    // param.permission = permission;
    // var data2 = "tradetype="+param.tradetype+"&bill="+param.bill+"&compressType="+param.compressType+"&compression=" + param.compression;
    ajax({
        url: '/nccloud/web/image/ImageShowAction.do?time=' + new Date().getTime(), 
        data: param, 
        // headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'},
        success: function(data) {
            if ((data["success"] == "true" || data["success"] == true) && data["data"]["myHashMap"]) {
                var ipAddr = data["data"]["myHashMap"]["ipaddr"];
                if (!ipAddr  || Object.keys(ipAddr).length === 0)  {
                    return;
                }
                var paraStr = encodeURI(encodeURI(data["data"]["myHashMap"]["parastr"]));
                var factoryCode = data["data"]["myHashMap"]["factoryCode"];
                imageshowBrowserByUrl(factoryCode, ipAddr, paraStr);
            } else {
                alert(data['error']["message"]);
            }
        }
    })
}

function imageshowBrowserByUrl(factoryCode, ipAddr, paraStr) {
    if (factoryCode == "tchzt") {
        // paraStr += "&author=author";
        // var data = {
        //     url_img : "http://" + ipAddr + "/TIMS-Server/nc-jsp/imageshow.jsp?" + paraStr,
        //     url_download : "http://" + ipAddr + "/TIMS-Server/nc-jsp/downloadActivex.jsp"
        // }
        // return openTBrowser(data);
    } else {
        if (window.opener == null) {
            window.imageBrowserWin = window.open(paraStr, multiLang['1056image-0039'], "width=" + w + ",height=" + h + ",left=" + l + ",top=" + t + ",toolbar=no, menubar=no, scrollbars=no, resizable=yes, location=no, status=no");
        } else {
            window.imageBrowserWin = window.opener.open(paraStr, multiLang['1056image-0039'], "width=" + w + ",height=" + h + ",left=" + l + ",top=" + t + ",toolbar=no, menubar=no, scrollbars=no, resizable=yes, location=no, status=no");
        }
    }
}


var protocol_name = "TURLProtocol:";
function openTBrowser(dataStr) {
    if (dataStr.indexOf("{") > -1 && dataStr.indexOf("}") > -1) { 
        var data = eval('(' + dataStr + ')'); 
        if (data.url_opensoft == null || data.url_opensoft == undefined || data.url_opensoft == "") {
            alert(multiLang['1056image-0035']);
            return;
        }
        if (data.url_opensoft.indexOf(protocol_name) > -1) {
            var uuid = "";
            var basePath = "http://" + data.ip + ":" + data.port + "/TIMS-Server/";
            util.jsonp(basePath + "softController/beforeOpenSoft?res=NC", function(error, result) {
                if (error) {
                    alert(multiLang['1056image-0036']);

                } else {
                    if (typeof result == 'string') {
                        result = JSON.parse(result);
                    }
                    if (result.error == "0") {
                        uuid = result.uuid;
                        data.wait_time = parseInt(result.time);
                    } else {
                        data.wait_time = 8;
                    }
                    window.onbeforeunload = function(event) {
                        return multiLang['1056image-0037'];
                    }

                    var finalurl = data.url_opensoft + "&uuid=" + uuid;
                    util.appendHTML(document.body, '<iframe style="display:none;" src="' + finalurl + '"></iframe>');
                    window.setTimeout(function () {
                        window.onbeforeunload = null;
                        util.jsonp(basePath + "softController/checkOpenStatus?res=NC&uuid=" + uuid, function(error, result) {
                            if (error) {
                                alert(multiLang['1056image-0038']);
                            } else {
                                 if (result == "no_file") {
                                     window.open(data.url_download, "Download", "height=400, width=800, top=100, left=100, toolba=no, menubar=no, scrollbars=no, resizable=no, location=no, status=no");
                                 } else if (result == "no_soft") {
                                     window.open(data.url_download, "Download", "height=400, width=800, top=100, left=100, toolba=no, menubar=no, scrollbars=no, resizable=no, location=no, status=no");
                                 }
                            }

                        });
                    }, data.wait_time * 1000);
                }

            });
        }
    } else {
        window.open(dataStr, multiLang['1056image-0039'], "toolbar=no, menubar=no, scrollbars=no, resizable=yes, location=no, status=no");
    }
};

/**
 * imageScan 影像扫描
 * imageView 影像查看
 */
export {imageScan, imageView}
