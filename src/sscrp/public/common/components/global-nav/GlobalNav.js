import React, {Component} from 'react';
import { toast,ajax,getMultiLang } from 'nc-lightapp-front';


const btnData = [
    {
        txt: '',
        config: {
            appcode: '700101GXZX',
            pagecode: '700101GXZX_C',
            path: '/ssctp/sscsetting/sscunit/config/'
        }
    },
    {
        txt: '',
        config: {
            appcode: '700102WTGX',
            pagecode: '700102WTGX_L',
            path: '/ssctp/sscsetting/clientage/config/'
        }
    },
    {
        txt: '',
        config: {
            appcode: '700103PZGZ',
            pagecode: '700103PZGZ_C',
            path: '/ssctp/sscsetting/workinggroup/config/'
        }
    },
    {
        txt: '',
        config: {
            appcode: '700104PZYH',
            pagecode: '700104PZYH_C',
            path: '/ssctp/sscsetting/workinggroupuser/config/'
        }
    },
    {
        txt: '',
        config: {
            appcode: '700105TQGZ',
            pagecode: '700105TQGZ_card',
            path: '/ssctp/sscsetting/sscladerule/config/'
        }
    },
    {
        txt: '',
        config: {
            appcode: '700106SYXJ',
            pagecode: '700106SYXJ_list_001',
            path: '/ssctp/sscsetting/sscpriority/config/'
        }
    }
]


class GlobalNav extends Component {
    constructor() {
        super();
        this.state = {
            activeIdx: 0,
            clickedIdx: -1,
            multiLang : {}
        }
        getMultiLang({moduleId: 1056, domainName: 'sscrp',currentLocale: 'zh-CN', callback: (json) => {
            this.setState({
                multiLang : json
            })
        }})

       

    }

    componentDidMount() {
        this.setState({ activeIdx: this.props.defaultActIdx || 0 }) // 设置默认焦点按钮
    }

    handleChangeActive = (idx) => {
        this.setState({ activeIdx: idx })
    }

    linkByClickedIdx = () => {
        let {clickedIdx} = this.state
        this.props.linkTo(
            btnData[clickedIdx].config.path,
            {
                pagecode: btnData[clickedIdx].config.pagecode,
                appcode: btnData[clickedIdx].config.appcode
            }
        )
    }

    checkSkipAuth = (idx)=>{
        let isAuthOk = true;
        const {linkTo, saveBeforeLink, handleTruncation} = this.props
        // debugger
        ajax({
            url: '/nccloud/ssctp/sscbd/NavigationAuthorityAction.do',
            data: "",
            success: (res) => {
                isAuthOk =  res.data.is_haspower;
                if(isAuthOk){
                    if (saveBeforeLink) {
                        this.setState({clickedIdx: idx})
                        handleTruncation()
                    } else {
                        this.handleChangeActive(idx) // 改变选中
                        linkTo(                      // 跳转
                            btnData[idx].config.path,
                            {
                                pagecode: btnData[idx].config.pagecode,
                                appcode: btnData[idx].config.appcode
                            }
                        )
                    }
                }else {
                    toast({ content: res.data.hint_message, color: 'danger' });
                }
            }
        })
    }

    render() {
       
        let btns = []
       
        btnData.forEach((each, idx) => {
            if(idx==0){
                each.txt=this.state.multiLang['1056common-0001'];
            } else if(idx==1){
                each.txt=this.state.multiLang['1056common-0002'];
            } else if(idx==2){
                each.txt=this.state.multiLang['1056common-0003'];
            } else if(idx==3){
                each.txt=this.state.multiLang['1056common-0004'];
            } else if(idx==4){
                each.txt=this.state.multiLang['1056common-0005'];
            } else if(idx==5){
                each.txt=this.state.multiLang['1056common-0006'];
            }
            btns.push(
                <span
                className={idx == this.state.activeIdx ? "tab-btn active-tab-btn" : "tab-btn"}
                onClick={() => {this.checkSkipAuth(idx)}}
                >{ each.txt }</span>
            )
        })
        return (
            <div className="header--tabs">
                {btns}
            </div>
        )
       
    }
}

export default GlobalNav
