import React, {Component} from 'react';

class SyncTree extends Component {
    static comName = 'SyncTree';

    constructor() {
        super();
    }

    render() {
        let {title, areaId, ...syncTreeProps} = this.props;

        const {ncProps} = window._ssc_gy_profile ;

        const {createSyncTree} = ncProps.syncTree;


        return (
            createSyncTree({
                showLine: true,
                needEdit: false,
                selectedForInit: true,
                ...syncTreeProps,
                treeId: areaId,
                //showLine: true,
                // needSearch:false //是否需要查询框，默认为true,显示。false: 不显示
                // needEdit:false  //是否需要编辑节点功能，默认为true,可编辑；false：不可编辑
                // editNodeCallBack: this.editNodeCallBack.bind(this), //编辑节点回调方法
                //addNodeCallBack: this.addNodeCallBack.bind(this), //  新增节点回调方法
                //delNodeCallBack: this.delNodeCallBack.bind(this), //删除节点回调方法
                //onSelectEve: this.onSelectEve.bind(this), //选择节点回调方法
                // onSelectedChange:this.onSelectChange.bind(this)  //监听选择节点改变事件
                // defaultExpandAll:true,   //初始化展开所有节点  ，默认参数为false,不展开
                // onMouseEnterEve: this.onMouseEnterEve.bind(this) //鼠标滑过节点事件
            })
        )
    }
}

export default SyncTree
