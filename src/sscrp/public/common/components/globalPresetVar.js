window._SSC_GLOBAL = {
    billHashChange: null
}

//防止后续平台更改成spa应用导致变量冲突，启用新的全局常量配置
const globalPresetVar = {
    status: {
        browse: 'browse', //浏览态
        edit: 'edit', //编辑态
        add: 'add', //新增态
        del: 'del'
      },
      //表格肩部按钮动态
    tableShoulderBtnAction: {
        Add: '_Add' //增行
    },
     //表格内部按钮常量
    tableInnerBtnAction: {
        Edit: '_Edit', //展开
        Delete: '_Delete', //删除
        Copy: '_Copy', //复制行
        Insert: '_Insert' //插入
    },
    pageButton: {
        Save: 'Save',
        Submit: 'Submit',
        SaveSubmit: 'SaveSubmit',
        Recall: 'Recall',
        Cancel: 'Cancel',
        PageCancel: 'pageCancel',//误删
        File: 'File',
        Edit: 'Edit',
        Delete: 'Delete',
        Copy: 'Copy',
        ImageShow: 'ImageShow',
        ImageUpload: 'ImageUpload',
        Print: 'Print',
        Invalid: 'Invalid',
        LinkApprovalDetail: 'LinkApprovalDetail',
        LinkBudget: 'LinkBudget', //联查预算
        LinkVoucher: 'LinkVoucher', //联查凭证
        Relation: 'Relation', //关联单据
        LinkRelationBill: 'LinkRelationBill', //联查单据
    }
}


//下面的变量 弃用, 请使用上面的
//弃用 弃用 弃用， 变量使用方式请参照 费用调整单。
//弃用 弃用 弃用， 变量使用方式请参照 费用调整单。
//弃用 弃用 弃用， 变量使用方式请参照 费用调整单。
//弃用 弃用 弃用， 变量使用方式请参照 费用调整单。
//弃用 弃用 弃用， 变量使用方式请参照 费用调整单。
window.presetVar = {
    isDevelopEnv: NODE_ENV == 'development',
    //状态常量。 用于url中的hash参数 或 框架api的状态
    status: {
      browse: 'browse', //浏览态
      edit: 'edit', //编辑态
      add: 'add', //新增态
      del: 'del'
    },
    //区域无关的公共按钮,页面级别按钮
    pageButton: {
        pageAdd: 'pageAdd', //新增
        pageSave: 'pageSave', //保存
        pageEdit: 'pageEdit', //页面修改
        pageClose: 'pageClose', //页面关闭
        pageCancel: 'Cancel',
        pageCopy: 'pageCopy', //复制单据
        pageDel: 'pageDel', //删除单据
        billInvalid: 'billInvalid', //单据作废
        billEnable: 'billEnable', //单据重启
        billClose: 'billClose', //单据关闭
        pagePrint: 'pagePrint', //单据打印
        imageUpload: 'imageUpload', //影像扫描
        imageShow: 'imageShow', //影像查看
        invoiceView: 'invoiceView', //联查发票
        invoiceDzfp: 'invoiceDzfp', //电子发票
        billSubmit: 'billSubmit', //提交
        billRecall: 'billRecall', //收回
        fileManager: 'fileManager', //附件管理
        billView: 'billView', //单据联查
        billRedBack: 'billRedBack', //红冲
        linkQueryFysq: 'linkQueryFysq', //联查费用申请单
        linkQueryFyyt: 'linkQueryFyyt', //联查费用预提单
        linkQueryJkbx: 'linkQueryJkbx', //联查借款/报销单
        linkBudget: 'LinkBudget', //联查预算
        linkVoucher: 'LinkVoucher', //联查凭证
        billApprove: 'billApprove', //单据审批 
        billSaveSubmit: 'billSaveSubmit', //单据保存提交
        ...window.presetVar //防止误用，后执行globalPresetVar
    },
    /**
     * @deprecated
     * 区域相关的按钮动作常量; 使用：区域id + 区域按钮行常量
     */
    areaBtnAction: {
        add: '-add',
        save: '-save',
        edit: '-edit',
        del: '-del',
        copy: '-copy',
        insert: '-insert',
        more: '-more'
    },
    //表格肩部按钮动态
    tableShoulderBtnAction: {
        Add: '_Add' //增行
    },
     //表格内部按钮常量
    tableInnerBtnAction: {
        Edit: '_Edit', //展开
        Delete: '_Delete', //删除
        Copy: '_Copy', //复制行
        Insert: '_Insert' //插入
    }
}


export default globalPresetVar;
