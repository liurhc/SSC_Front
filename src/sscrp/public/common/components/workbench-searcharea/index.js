import React, {Component} from 'react';
import {connect} from 'react-redux';
import {base, ajax,getMultiLang} from 'nc-lightapp-front';
const {NCSelect, NCIcon, NCFormControl, NCCheckbox, NCMessage ,NCDatePicker} = base;
const {NCRangePicker ,NCTZDatePickerRangeDay} = NCDatePicker;
import './index.less';

class WorkbenchSearcharea extends Component {
    constructor(props) {
        super();
        this.state = {
            menuShowType:{
                type: "icon-Group",
                title: '',
                showMore: false,
                barcodeVal: '',
                checked: false,
                canOpenPage: false
            },
            multiLang : {}
        }
        getMultiLang({moduleId: 1056, domainName: 'sscrp',currentLocale: 'zh-CN', callback: (json) => {
            this.setState({
                multiLang : json
            })
        }})
    }

    handleConditionChange(attrcode, data){
        let newState = {};
        newState[attrcode] = data;
        this.setState(newState);
        this.props.handleConditionChange(attrcode, data);
        this.updateTradetypeOptions() 
    }

    menuClick = () => {
        if(this.state.menuShowType.type == "icon-xingzhuang"){
        this.setState({menuShowType:{type: "icon-Group", title: this.state.multiLang['1056common-0007']/*缩略显示*/}});
            if(this.props.showTypeClick != null){
                this.props.showTypeClick("list");
            }
        }else{
            this.setState({menuShowType:{type: "icon-xingzhuang", title: this.state.multiLang['1056common-0008']/*列表显示*/}});
            if(this.props.showTypeClick != null){
                this.props.showTypeClick("card");
            }
        }
    }

    componentDidUpdate() {
        this.updateTradetypeOptions()
    }

    updateTradetypeOptions = () => {
        const pk_tradetype = document.getElementById('trade-type-condition')
        if (pk_tradetype) {
            let children = pk_tradetype.children
            const PK_TRADE_TYPE_WIDTH = pk_tradetype.clientWidth
            let total_width = 70
            let sclice_idx = -1
            for (let i = 0; i < children.length; i++) {
                if (children[i].tagName == 'SPAN') continue
                children[i].style.display = 'inline-block'
            }
            for (let i = 0; i < children.length; i++) {
                if (children[i].tagName == 'SPAN') continue
                // total_width += children[i].offsetWidth + 20
                total_width += children[i].offsetWidth
                if (total_width >= PK_TRADE_TYPE_WIDTH) {
                    sclice_idx = i
                    break
                }
            }
            // console.log('sclice_idx: ', sclice_idx)
            if (sclice_idx > -1) {
                if (!this.state.showMore) {
                    for (let i = 0; i < children.length; i++) {
                        if (children[i].tagName == 'SPAN') continue
                        if (i >= sclice_idx) {
                            children[i].style.display = 'none'
                        } else {
                            children[i].style.display = 'inline-block'
                        }
                    }
                }
            }
            if (sclice_idx == -1) {
                let moreViewBtn = document.getElementById('more-view-btn')
                if (moreViewBtn) {
                    moreViewBtn.style.display = 'none'
                }
            } else {
                let moreViewBtn = document.getElementById('more-view-btn')
                if (moreViewBtn) {
                    if(this.state.showMore) {
                        moreViewBtn.style.display = 'none'
                    } else {
                        moreViewBtn.style.display = 'block'
                    }
                }
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.conditions != this.props.conditions) {
            this.setState({showMore: false}, () => {this.updateTradetypeOptions()})
        }
        return true
    }

    barcodeQuery = () => {
        const {
            barcodeApi,
            queryKey,
            queryDataCallback,
            specialOpenTo,
            cusType
        } = this.props

        const {
            barcodeVal,
            checked,
            canOpenPage
        } = this.state

        if(!barcodeApi) return

        barcodeApi({
            data: {
                ...queryKey,
                barcode: barcodeVal
            },
            success: (data) => {
                if (checked && canOpenPage) { // 自动打开
                    const {
                        tasklist: {
                            rows
                        }
                    } = data
                    if (rows.length == 0) { // 提示为空
                        NCMessage.create({content: this.state.multiLang['1056common-0016']/*扫描条码未查询到单据*/, color: 'warning', position: 'top'})
                    } else { // 跳转
                        const {values: {
                            billtypecode,
                            transtypecode,
                            busiid
                        }} = rows[0]

                        ajax({
                            url: '/nccloud/sscrp/rpbill/BrowseBillAction.do',
                            data: {
                                billtypeCode: billtypecode.value, 
                                transtypeCode: transtypecode.value,
                                billid: busiid.value
                            },
                            success: (data) => {
                                specialOpenTo(
                                    data.data.url,
                                    {
                                        ...data.data.data,
                                        scene: cusType.split('_')[1]
                                    }
                                )
                            }
                        })
                    }
                }
                queryDataCallback(data)
            }
        })
    }

    resetState = () => {
        let newState = this.state
        for (let attr in newState) {
            if (attr != 'menuShowType'&&attr != 'multiLang') {
                newState[attr] = ''
            }
        }
        this.setState(newState)
    }

    render() {
        const {conditions, cusType, twoColNums} = this.props;
        const {fuzzyquerychild} = this.props;
        const {onFuzzyChange, onFuzzySelected, hideBarCode,onPickDateChange, queryKey} = this.props;
        const {showMore} = this.state;
        this.state.menuShowType.title = this.state.multiLang['1056common-0007']/*缩略显示*/;
        let getActive = (conditionOne,key) => {
            if(
                (
                    (this.state[conditionOne.attrcode] == null || this.state[conditionOne.attrcode] == "") 
                    && conditionOne.initialvalue != null 
                    && conditionOne.initialvalue.value == key
                ) 
                || this.state[conditionOne.attrcode] == key){
                return 'active';
            }else{
                return '';
            }
        }

        let getCondition = (conditionOne) => {
            let ulId = (conditionOne.attrcode === 'pk_tradetype' || conditionOne.attrcode === 'transtypecode') ? 'trade-type-condition' : ''
            switch(conditionOne.itemtype){
                case 'select':
                    return (
                        <ul id={ulId}>
                            {
                                conditionOne.options.map((one, index) => {
                                    return(
                                        <li
                                            key={"conditionOne_"+conditionOne.itemtype+"_"+index} 
                                            className={"query-key "+getActive(conditionOne, one.value)}
                                            onClick={this.handleConditionChange.bind(this, conditionOne.attrcode, one.value)}
                                        >{one.display}</li>
                                    )
                                })
                            }
                            <span 
                                style={{
                                    display: (conditionOne.attrcode === 'pk_tradetype' || conditionOne.attrcode === 'transtypecode') && showMore ? 'block' : 'none'
                                }}
                                className='arrow-up'
                                onClick={() => {this.setState({showMore: false}, () => {this.updateTradetypeOptions()})}}
                            >
                                {this.state.multiLang['1056common-0009']/*收起*/}
                                <NCIcon type='uf-arrow-up'/>
                            </span>
                        </ul>
                    );
                case 'select':
                    return (
                        <input/>
                    );
                default:
                    return null;
            }
        }

        let getQueyKeys=()=>{
            
            let refHtml = []
            for(let i=0;i<conditions.length;i++){
                let className = "key-one-row";
                let moreViewId = (conditions[i].attrcode === 'pk_tradetype' || conditions[i].attrcode === 'transtypecode') ? 'more-view-btn' : ''
                if(i>=twoColNums) className = "key-two-row";
                if (
                    conditions[i].attrcode === 'pk_tradetype' 
                    ||
                    conditions[i].attrcode === 'transtypecode'
                ) className = "key-two-row tradetype-wrap"
                refHtml.push(
                    <div className={className}>
                        <table>
                            <tbody>
                                <tr>
                                    <td className='td-label'>
                                        {conditions[i].label}：
                                    </td>
                                    <td colSpan="2" className='td-con'>
                                        {getCondition(conditions[i])}
                                        <span 
                                            onClick={() => {this.setState({showMore: true}, () => {this.updateTradetypeOptions()})}}
                                            className='arrow-down'
                                            id={moreViewId}
                                            style={{
                                                display: (conditions[i].attrcode === 'pk_tradetype' || conditions[i].attrcode === 'transtypecode') && !showMore ? 'block' : 'none'
                                            }}
                                        >
                                            {this.state.multiLang['1056common-0010']/*更多*/}
                                            <NCIcon type='uf-arrow-down'/>
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                )
            }
            let rangePickHtml = <div  className="key-one-row">
                    <table>
                        <tbody>
                            <tr>
                                <td >完成时间范围:</td>
                                <td>
                                    <NCRangePicker 
                                        renderFooter={() => '额外页脚'} 
                                        placeholder={'开始 ~ 结束'}
                                        onChange={onPickRange.bind(this)}
                                        value={this.state.pickDate}
                                    />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>;
            if(queryKey && queryKey.taskstatus == "handled"){
                refHtml.push(rangePickHtml);
            }
            return refHtml;
        }
        let onPickRange=(date)=>{
            this.state.pickDate = date;
            onPickDateChange(date);
        }
        let getCusQueryKey=()=>{
            if(cusType=="rp"){
                return([
                        <td className='td-label' style={{top: '-7px'}}>
                             <span className='more-condition'>{this.state.multiLang['1056common-0011']/*模糊搜索：*/}</span>
                        </td>,
                        <td>
                            <div>
                            <NCSelect
                                className="fuzzyquery"
                                height="24px"
                                tags
                                searchPlaceholder={this.state.multiLang['1056common-0012']/*标签模式*/}
                                placeholder={this.state.multiLang['1056common-0013']/*模糊查询*/}
                                data={fuzzyquerychild}
                                onSearch={onFuzzyChange.bind(this)}
                                onChange={onFuzzySelected.bind(this)}
                                >
                            </NCSelect> 
                            </div>  
                        </td>
                    ]
                )
            }else if(cusType.indexOf("tp") > -1){
                return([
                        <td className='td-label' style={{top: '-7px', display: hideBarCode ? 'none' : ''}}>
                            <span className='more-condition'>{this.state.multiLang['1056common-0014']/*更多条件：*/}</span>
                        </td>,
                        <td>
                            <div className={this.state.barcodeVal && this.state.barcodeVal.length > 0 ? "barcode barcode-focus" : "barcode"} style={{display: hideBarCode ? 'none' : ''}}>
                                <NCFormControl
                                    value={this.state.barcodeVal}
                                    onChange={(val) => {
                                        if (val) {
                                            this.setState({barcodeVal: val, canOpenPage: true})
                                        } else {
                                            this.setState({barcodeVal: val, canOpenPage: false}, () => {
                                                this.barcodeQuery()
                                            })
                                        }
                                    }}
                                    type='search'
                                    onSearch={this.barcodeQuery}
                                />
                                <i className='iconfont icon-saoyisao'></i>
                            </div>
                            <div className="auto-open" style={{display: hideBarCode ? 'none' : ''}}>
                                <NCCheckbox
                                    onChange={() => {
                                        this.setState({checked: !this.state.checked})
                                    }}
                                    checked={this.state.checked}                    
                                >{this.state.multiLang['1056common-0015']/*自动打开：*/}</NCCheckbox>
                            </div>
                            <div style={{marginLeft: hideBarCode ? '18px' : ''}}>
                                <NCSelect
                                    className="fuzzyquery"
                                    height="24px"
                                    tags
                                    searchPlaceholder={this.state.multiLang['1056common-0012']/*标签模式*/}
                                    placeholder={this.state.multiLang['1056common-0013']/*模糊查询*/}
                                    data={fuzzyquerychild}
                                    onSearch={onFuzzyChange.bind(this)}
                                    onChange={onFuzzySelected.bind(this)}
                                >
                                </NCSelect>  
                            </div>
                        </td>
                    ]
                )
            }
        }

        return (
            <div id="SysComponentsWorkbenchSearcharea">
                <div>
                    <div> 
                        {getQueyKeys()}
                        <div className="key-two-row">
                            <table>
                                <tbody>
                                    <tr>
                                        {getCusQueryKey()}
                                        <td className="menu-icon-td" style={{
                                            display: this.props.disabledTypeChange ? 'none' : 'block'
                                        }}>
                                            <i 
                                                className={`menu-type-icon iconfont ${this.state.menuShowType.type}`}
                                                title={this.state.menuShowType.title}
                                                onClick={this.menuClick}
                                            ></i>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default WorkbenchSearcharea;