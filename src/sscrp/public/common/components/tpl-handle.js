/**
 * @param props 业务实体的props
 * @param tplData 模板数据
 * @param store 数据状态管理对象
 * @param cbOpts 模板扩展字段配置
 */
function tplHandle(props, tplData, store, cbOpts) {

    if (arguments.length < 4 || !this) {
        console.error('提醒 By GY：\n tplHandled调用出错，新的调用方式为 tplHandle.call(this, props, meta, store，')

    }
    let pageStatus = props.getUrlParam('status');
    if (pageStatus == presetVar.status.add) {
        pageStatus = presetVar.status.edit;
    }

    cbOpts || (cbOpts = {});
    let billHeads = [];
    let billBodys = [];
    let associatedTable = [];
    for (let areaCode in tplData) {
        let areaData = tplData[areaCode];
        //满足以下条件才是区域。 relationcode用来区分cardTable
        if (!areaData ||!areaData.moduletype || !areaData.items) {
            continue;
        }

        if (areaData.moduletype == 'form' && tplData.gridrelation[areaData.relationcode]) {
            continue;
        }
        //设定区域初始状态
        areaData.status = pageStatus;
        switch (areaData.moduletype) {
            case 'form' :
                cbOpts.head && cbOpts.head(areaData, areaCode);
                billHeads.push(areaCode);
                break;
            case 'table' :
                if (!areaData.bizType) {
                    cbOpts.body && cbOpts.body(areaData, areaCode);
                    billBodys.push(areaCode);
                    if (tplData.gridrelation[areaCode]) {
                        areaData.isCardTable = true;
                    }
                } else if (areaData.bizType == 'associated') {
                    associatedTable.push(areaCode);
                }
                break;
            case 'search' :
                break;
        }
    }

    store.dispatch({type: 'changeBillHeads', billHeads: billHeads});
    store.dispatch({type: 'changeBillBodys', billBodys: billBodys});
    store.dispatch({type: 'changeAssociatedTable', associatedTable: associatedTable});
}

export default tplHandle;