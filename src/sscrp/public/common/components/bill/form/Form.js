import React, {Component} from 'react';


class Form extends Component {
    static comName = 'Form';

    constructor() {
        super();
    }

    render() {
        const {createForm} = this.props.ncProps.form;
        return (
            <div className="nc-bill-form-area">
                {createForm(this.props.areaId, {
                    onAfterEvent: this.props.onAfterEvent
                })}
            </div>
        )
    }
}

export default Form
