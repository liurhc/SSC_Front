import React, { Component } from 'react';
import './CardTable.less';
import classnames from 'classnames';

class CardTable extends Component {
    static comName = 'CardTable';

    constructor(props) {
        super();
    }

    render() {
        let buttonGroup = null;

        let { invisibleTables, children, title, ncProps, areaId, onAfterEvent, totalItemCode, ...cardTableProps } = this.props;

        React.Children.forEach(children, (child) => {
            if (typeof child.type == 'function') {
                switch (child.type.comName) {
                    case 'ButtonGroup':
                        let buttonsClickFn = child.props.buttonEvent || (() => { return {} });
                        let buttonsClickObj = buttonsClickFn();
                        if (!buttonsClickObj[areaId + '_Add'] || !buttonsClickObj[areaId + '_Add'].actionCode) {
                            buttonsClickObj[areaId + '_Add'] = {...buttonsClickObj[areaId + '_Add'], actionCode: 'BodyAdd' };
                        }
                        buttonsClickFn = function () {
                            return buttonsClickObj
                        }
                        
                        buttonGroup = React.cloneElement(child, { 
                            ncProps, 
                            ctrlAreaId: areaId,
                            buttonEvent: buttonsClickFn 
                        });
                        break;
                    default:
                        console.warn('CardTable标签仅渲染ButtonGroup标签')
                }
            }

        });

        window.__ssc_BillStyleVar.tablesCount[areaId] || (window.__ssc_BillStyleVar.tablesCount[areaId] = {
            rowCount: 0,
            totalItemCount: 0,
            totalItemCode: totalItemCode
        })
        let tableCount = window.__ssc_BillStyleVar.tablesCount[areaId];

        let Count = tableCount.totalItemCount.toString();
        let num = Count.split(".");
        if(num.length == 1) {
            Count += ".00";
        }else if (num.length > 1) {
            let n = num[1];
            if(n.length == 1) {
                Count+="0";
            }
        }
        tableCount.totalItemCount = Count;
        let { rowCount, totalItemCount } = tableCount;

        this.getTableHead = () => {
            return (
                [buttonGroup]
            );
        };

        this.getTableLeftHead = () => {
            if(window.presetVar.billtype != "261X-Cxx-001")
            return (
                <span ref={(obj) => {tableCount.totalBarDom = obj;}}  style={{ color: '#555', fontSize: '14px', lineHeight: '40px'}}>
                   {/*  共 */}
                   {(this.props.multiLang['105601GGZD-0062'] || '') + rowCount + (this.props.multiLang['105601GGZD-0063'] || '')}
                   {/* 条 */}
                    {totalItemCode ? "，" + (this.props.multiLang['105601GGZD-0064'] || '')/* 合计 */ + totalItemCount : null}
                </span>
            )
        }
        return (
            <div className={classnames("nc-bill-tableTab-area", {"invisible-card-table" : invisibleTables && invisibleTables.includes(areaId)})}>
                {ncProps.cardTable.createCardTable(areaId, {
                    tableHead: this.getTableHead,
                    tableHeadLeft: this.getTableLeftHead,
                    modelSave: null,//
                    showIndex: true,
                     isAddRow: false,
                    ...cardTableProps,
                    onAfterEvent: (props, moduleId, key, value, changedrows,index,attr) => {
                        if (totalItemCode && totalItemCode == key) {
                            window.__ssc_BillStyleVar.tableRowAndItemCount(areaId, 'rowChange', changedrows);
                            //window.__ssc_BillStyleVar.tableRowAndItemCount(areaId, ncProps.cardTable);
                        }
                        onAfterEvent && onAfterEvent(props, moduleId, key, value, changedrows,index,attr);
                    }
                })}
            </div>
        )
    }
}

export default CardTable
