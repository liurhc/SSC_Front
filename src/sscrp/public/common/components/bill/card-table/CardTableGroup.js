import React, { Component } from 'react';
import CardTable from './CardTable';
import ButtonGroup from '../btn-group/ButtonGroup';

import { base } from 'nc-lightapp-front';

const { NCAnchor, NCScrollElement, NCScrollLink } = base;

class CardTableGroup extends Component {
    static comName = 'CardTableGroup';

    constructor(props) {
        super(props)
    }

    render() {
        let { children, hideTables, ...otherProps } = this.props;
        const ncProps = otherProps.ncProps;

        const { meta } = ncProps;
        const tplData = meta.getMeta();

        if (!tplData || Object.keys(tplData).length === 0) {
            return null;
        }

        let multiShoulderBtnConfig = {};
        if (children) {
            React.Children.forEach(children, (child) => {
                if (typeof child.type == 'function') {
                    switch (child.type.comName) {
                        case 'MultiButtonGroup':
                        let { buttonEvent, btnAreaIdByTableAreas} = child.props;
                        multiShoulderBtnConfig = {
                            buttonEvent: buttonEvent,
                            btnAreaIdByTableAreas: btnAreaIdByTableAreas
                        }
                        break;
                    }
                }
            })
        }
        
        let sholderBtnEvent = multiShoulderBtnConfig.buttonEvent || (() => {return {}});
        let shoulderBtnClickObj = sholderBtnEvent();
        
        let btnAreaIdByTableAreas = multiShoulderBtnConfig.btnAreaIdByTableAreas || {};
        const totalItemCodeByAreas = this.props.totalItemCodeByAreas || {};

        let anchorAreaArr = [
            <NCScrollLink
                        to='forminfo'
                        spy={true}
                        smooth={true}
                        duration={300}
                        offset={-100}
                    >
                        <p>{this.props.multiLang['1056common-0017']/*表单信息*/}</p>
                    </NCScrollLink>
        ];
        let excludeTableAreaId = this.props.excludeTableAreaId || [];
        let cardTableArr = [];
        for (let areaId in tplData) {
            let areaObj = tplData[areaId];
            if (areaObj && areaObj.moduletype == 'table' && !excludeTableAreaId.includes(areaId)) {

                let addDefaultKey = areaId + '_Add';
                if (!shoulderBtnClickObj[addDefaultKey] || !shoulderBtnClickObj[addDefaultKey].actionCode) {
                    shoulderBtnClickObj[addDefaultKey] = { 
                        ...shoulderBtnClickObj[addDefaultKey],
                        actionCode: 'BodyAdd' 
                    };
                }

                //表格
                if (!hideTables || !hideTables.includes(areaId)){
                    let cardTableInst = (
                        <NCScrollElement name={`anchor-${areaId}`} key={areaId}>
                            <CardTable
                                {...otherProps}
                                areaId={areaId}
                                totalItemCode={totalItemCodeByAreas[areaId] || this.props.totalItemCode}
                                modelSave={this.props.modelSave}
                                onAfterEvent={this.props.onAfterEvent}
                                fromParent='CardTableGroup'
                                invisibleTables={this.props.invisibleTables}
                                showCheck= {areaObj.showCheck}
                            >
                                <ButtonGroup
                                    areaId= {btnAreaIdByTableAreas[areaId] || `${areaId}_card_body`}
                                    buttonEvent={() => { return shoulderBtnClickObj}}
                                    ctrlAreaId={areaId}
                                />
                            </CardTable>
                        </NCScrollElement>
    
                    )
    
                    cardTableArr.push(cardTableInst);
                    //areaVisible 为模板中区域下的配置，为true是，说明区域内有可见字段。为false则没有可见字段。
                    //invisibleTables 为业务代码逻辑（硬编码）中表体区域可见性控制。
                    if (areaObj.areaVisible && (!this.props.invisibleTables || !this.props.invisibleTables.includes(areaId))) {
                        anchorAreaArr.push(
                            <NCScrollLink key={areaId}
                                to={`anchor-${areaId}`}
                                spy={true}
                                smooth={true}
                                duration={300}
                                offset={-100}
                            >
                                <p>{areaObj.name}</p>
                            </NCScrollLink>
                        )
                    }
                    
                }
                
            }
        }

        return (
            [
                <NCAnchor>
                    {anchorAreaArr}
                </NCAnchor>
                ,
                cardTableArr

            ]
        )
    }
}

export default CardTableGroup;