import BodyDelete from './tableExtendButtons/BodyDelete';
import BodyMore from './tableExtendButtons/BodyMore';
import BodyInsert from './tableExtendButtons/BodyInsert';
import BodyCopy from './tableExtendButtons/BodyCopy';

export function addDefaultBtnForBtnTpl(buttonTpl, tableAreaId) {
    /* //默认肩部 新增按钮
    buttonTpl.push({
        "id": 'gy_' + Math.random(new Date().getTime()),
        "type": "button_main",
        "key": `${tableAreaId}_Add`,
        "title": "增行",
        "area": tableAreaId + '_card_body',
        "children": []
    });

    //默认表格 扩展按钮
    buttonTpl.push(
        {
            "id": 'gy_' + Math.random(new Date().getTime()),
            "type": "button_main",
            "key": `${tableAreaId}_Edit`,
            "title": "展开",
            "area": tableAreaId + '_card_body_inner',
            "children": []
        }
    )
    buttonTpl.push(
        {
            "id": 'gy_' + Math.random(new Date().getTime()),
            "type": "button_main",
            "key": `${tableAreaId}_Delete`,
            "title": "删除",
            "area": tableAreaId,
            "children": []
        }
    )
    buttonTpl.push(
        {
            "id": 'gy_' + Math.random(new Date().getTime()),
            "type": "button_main",
            "key": `${tableAreaId}_Copy`,
            "title": "复制行",
            "area": tableAreaId,
            "children": []
        }
    )
    buttonTpl.push(
        {
            "id": 'gy_' + Math.random(new Date().getTime()),
            "type": "button_main",
            "key": `${tableAreaId}_Insert`,
            "title": "插入行",
            "area": tableAreaId,
            "children": []
        }
    ) */
}

/**
 * 默认按钮可见
 * @param {*} tableAreaId 
 * @param {*} customConfig 
 * @param {*} record 
 * @param {*} index 
 */
export function defaultBtnVisibleHandle(btnAreaId, customConfig, record, index, buttonTpl) {

    let buttonVisible = customConfig.buttonVisible;
    let visibleBtns = null;

    if (!buttonVisible && buttonTpl) {
        buttonVisible = () => {
            return buttonTpl.map((btn) => {
                if (btn.area == btnAreaId) {
                    return btn.key;
                }
            })
        };
        visibleBtns = buttonVisible();
    } else {
        visibleBtns = buttonVisible(record, index);
    }

    /* visibleBtns = visibleBtns.concat(
        [`${tableAreaId}-BodyCardEdit`, `${tableAreaId}-BodyDelete`, `${tableAreaId}-BodyCopy`, `${tableAreaId}-BodyInsert`]
    ); */
    return visibleBtns;
}

/**
 * 默认按钮事件触发配置
 */
export function defaultButtonEventTriggerConfig(props, tableAreaId, onButtonClick) {
    let buttonsClickConfig = onButtonClick();

    let editDefaultKey = `${tableAreaId}_Edit`;
    if (!buttonsClickConfig[editDefaultKey] || !buttonsClickConfig[editDefaultKey].actionCode) {
        buttonsClickConfig[editDefaultKey] = {
            ...buttonsClickConfig[editDefaultKey],
            actionCode: 'BodyCardEdit'
        }
    }

    let delDefaultKey = `${tableAreaId}_Delete`;
    if (!buttonsClickConfig[delDefaultKey] || !buttonsClickConfig[delDefaultKey].actionCode) {
        buttonsClickConfig[delDefaultKey] = {
            ...buttonsClickConfig[delDefaultKey],
            actionCode: 'BodyDelete'
        }
    }

    let insertDefaultKey = `${tableAreaId}_Insert`;
    if (!buttonsClickConfig[insertDefaultKey] || !buttonsClickConfig[insertDefaultKey].actionCode) {
        buttonsClickConfig[insertDefaultKey] = {
            ...buttonsClickConfig[insertDefaultKey],
            actionCode: 'BodyInsert'
        }
    }

    let copyDefaultKey = `${tableAreaId}_Copy`;
    if (!buttonsClickConfig[copyDefaultKey] || !buttonsClickConfig[copyDefaultKey].actionCode) {
        buttonsClickConfig[copyDefaultKey] = {
            ...buttonsClickConfig[copyDefaultKey],
            actionCode: 'BodyCopy'
        }
    }
    //张颖老师说不去要气泡
    //props.button.setPopContent(delDefaultKey, '确认要删除该信息吗？');

    return buttonsClickConfig;
}

/**
 * 默认按钮事件条用
 */
export function defaultButtonEventDispatch(props, btnKey, buttonsClickConfig, tableAreaId, record, index, copyRowWithoutCols) {
    let btnsClickFn = buttonsClickConfig;
    let btnClickConfig = btnsClickFn[btnKey]
    if (typeof btnClickConfig == 'object') {
        let defaultKey = btnClickConfig['actionCode'] || btnKey;
        switch (defaultKey) {
            case 'BodyDelete':
                btnEventHandle(props, btnsClickFn, btnKey, tableAreaId, record, index, BodyDelete, btnClickConfig);
                break;
            case 'BodyMore':
                console.warn('BodyMore已废弃，请改用BodyCardEdit。 by guoyangf@yonyou.com')
                btnEventHandle(props, btnsClickFn, btnKey, tableAreaId, record, index, BodyMore, btnClickConfig);
                break;
            case 'BodyCardEdit':
                btnEventHandle(props, btnsClickFn, btnKey, tableAreaId, record, index, BodyMore, btnClickConfig);
                break;
            case 'BodyInsert':
                btnEventHandle(props, btnsClickFn, btnKey, tableAreaId, record, index, BodyInsert, btnClickConfig);
                break;
            case 'BodyCopy':
                btnEventHandle(props, btnsClickFn, btnKey, tableAreaId, record, index, BodyCopy, btnClickConfig, copyRowWithoutCols);
                break;
            default:
                break;
        }
    } else if (typeof btnClickConfig == 'function') {
        btnClickConfig(record, index);
    }


}

function btnEventHandle(props, btnsClickFn, btnKey, tableAreaId, record, index, defaultFn, btnKeyConfig, copyRowWithoutCols) {

    let { beforeClick, click, afterClick } = btnKeyConfig;
    if (click) {
        console.warn('此按钮组事件代码实现结构不符合规范，请参考【费用申请单】里按钮组事件代码的规范写法！ \n by guoyangf@yonyou.com');
        click();
    } else {
        let beforeResult = beforeClick && beforeClick(record, index) || !beforeClick; //1、按钮可见性控制。 2、自定义的请求参数。返回的结果如果不为空，则用于ajax请求的参数
        defaultFn && beforeResult && defaultFn(props, btnsClickFn, btnKey, tableAreaId, record, index, copyRowWithoutCols);
        afterClick && afterClick(record, index);
    }
}