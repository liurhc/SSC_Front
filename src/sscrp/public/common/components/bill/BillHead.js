import React, { Component } from 'react';

import { ajax,base, createPageIcon,toast} from 'nc-lightapp-front';
const {NCScrollElement, NCBackBtn, NCAffix, NCRefreshBtn} = base;
class BillHead extends Component {
    static comName = 'BillHead';

    constructor(props) {
        super();
        this.state = {
            isShowRefreshBtn: props.ncProps.getUrlParam('status') == 'browse' ,
            ifChange:false,
        }

        window.addEventListener('hashchange', () => {
            let isShowRefreshBtn = false;
            if (props.ncProps.getUrlParam('status') == 'browse' ) {
                isShowRefreshBtn = true;
            }
            this.setState({
                isShowRefreshBtn
           
            })
        }) 
    }

    handleClick(){
        if(this.props.refreshButtonEvent){
            this.props.refreshButtonEvent();
        }
    }

     gotoLink=(link)=>{
		    if(link){
			        window.open(link)
		    };
	}
    render() {

        let buttonGroup = null;
        let billForm = null;
        let self=this;
        let { children, ...otherProps } = this.props;
        const { createCardPagination } = otherProps.ncProps.cardPagination;
        const { profileLayoutList } = otherProps;
        React.Children.forEach(children, (child) => {
            if (typeof child.type == 'function') {
                switch (child.type.comName) { //此处获取函数名不兼容。后续处理
                    case 'ButtonGroup':
                        buttonGroup = React.cloneElement(child, otherProps);
                        break;
                    case 'Form':
                        billForm = React.cloneElement(child, otherProps);
                        break;
                    default:
                        console.warn('BillHead组件仅接受ButtonGroup子组件')
                }
            }

        });

        if (billForm) {
            billForm = (
                <NCScrollElement name='forminfo'>
                    {billForm}
                </NCScrollElement>
            )
        }
        return (
            <div className="nc-bill-top-area">
                <NCAffix offsetTop={0} >
                <div className="nc-bill-header-area">
                    {(this.props.back || {}).showBack == true ? (
                        <div style={{float: 'left'}}>
                            {
                                <NCBackBtn onClick={ this.props.back.onClick }></NCBackBtn>
                            }
                        </div>
                    ) : (null)
                    }
                    <div className="header-title-search-area">
                        {createPageIcon()}
                        <h2 className='title-search-detail'>{this.props.title}</h2>
                    </div>
                    <div className="header-button-area">
                        {
                            buttonGroup
                        }
                    </div>
                    <NCRefreshBtn style={{ cursor : 'pointer', display : this.state.isShowRefreshBtn?'inherit':'none'}} onClick={ this.handleClick.bind(this) }></NCRefreshBtn>
                    {(this.props.pageInfoConfig || {}).show == true ? (
                        <div className='header-cardPagination-area' style={{float: 'right'}}>
                            {
                                createCardPagination({
                                    handlePageInfoChange: this.props.pageInfoConfig.handlePageInfoChange,
                                    dataSource : this.props.pageInfoConfig.dataSource
                                })
                            }
                        </div>
                    ) : (null)
                    }
                </div>
                </NCAffix>
                {billForm}
            </div>


        )
    }
}

export default BillHead
