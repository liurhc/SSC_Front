import BodyDelete from './tableExtendButtons/BodyDelete'
import BodyMore from './tableExtendButtons/BodyMore'
import BodyInsert from './tableExtendButtons/BodyInsert'
import BodyCopy from './tableExtendButtons/BodyCopy'
import { getMultiLang } from 'nc-lightapp-front';

import {
    addDefaultBtnForBtnTpl, 
    defaultBtnVisibleHandle, 
    defaultButtonEventTriggerConfig,
    defaultButtonEventDispatch
} from './defaultButtonHandle';

export default function (props, meta, buttonTpl, optArray) {
    return new Promise((resolve, reject) => {
        getMultiLang({moduleId: 1056, domainName: 'sscrp',currentLocale: 'zh-CN', callback: (json) => {
            let paramLen = arguments.length;
            let hasBtnTpl = false;
            if (paramLen == 3) {
                optArray = buttonTpl;
                buttonTpl = null;
            } else if (paramLen == 4) {
                hasBtnTpl = true;
            } else {
                console.error('setTableExtndeCol调用参数错误')
                return;
            }
        
            optArray = optArray || [];
            optArray.forEach((config) => {
                let {tableAreaId, btnAreaId, onButtonClick, buttonVisible ,buttonLimit}= config;
        
                btnAreaId || (btnAreaId = tableAreaId + '_card_body_inner');
        
                if (hasBtnTpl) {
                    addDefaultBtnForBtnTpl(buttonTpl, tableAreaId);
                }
        
                //按钮点击
                onButtonClick = onButtonClick || (() => { return {} });
                    
                let buttonsClickConfig = defaultButtonEventTriggerConfig(props, tableAreaId, onButtonClick);
        
                let material_event = {
                    label: json['105602BZPT-0045']/* '操作' */,
                    itemtype: 'customer',
                    attrcode: 'opr',
                    width: '150px',
                    visible: true,
                    fixed: 'right',
                    render: (text, record, index) => {
        
                        let visibleBtns = defaultBtnVisibleHandle(btnAreaId, config, record, index, buttonTpl);
        
                        return props.button.createOprationButton(visibleBtns, {
                            area: btnAreaId,
                            buttonLimit:buttonLimit || 3,
                            onButtonClick: (props, btnKey) => {
                                if (typeof onButtonClick == 'function') {
                                    defaultButtonEventDispatch(props,btnKey, buttonsClickConfig, tableAreaId, record, index, config.copyRowWithoutCols); 
                                }
        
                            }
                        });
                    }
                };
                meta[tableAreaId].items.push(material_event);
                resolve();
            })       
        }})
    })
    
}