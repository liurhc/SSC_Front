import {
    addDefaultBtnForBtnTpl, 
    defaultBtnVisibleHandle, 
    defaultButtonEventTriggerConfig,
    defaultButtonEventDispatch
} from './defaultButtonHandle';
import { getMultiLang } from 'nc-lightapp-front';

export default function (props, tplData, buttonTpl, excludeTableAreaId, opt) {
    getMultiLang({moduleId: 1056, domainName: 'sscrp',currentLocale: 'zh-CN', callback: (json) => {
        opt = opt || {};

        for (let areaId in tplData) {
            let areaObj = tplData[areaId];
    
            let buttonsClickConfig = null;
            let onButtonClick = null;
    
            //对表格区域添加扩展列
            if (areaObj && areaObj.moduletype == 'table' && !excludeTableAreaId.includes(areaId)) {
    
                let tableAreaId = areaId;
    
                let btnAreaId = null;
    
                let customConfig = opt[tableAreaId] || {};
    
                btnAreaId = customConfig.btnAreaId || tableAreaId + '_card_body_inner';
    
                if (buttonTpl) {
                    addDefaultBtnForBtnTpl(buttonTpl, tableAreaId);
                }
    
                //按钮点击
                onButtonClick = customConfig.onButtonClick || (() => { return {} });
                
                buttonsClickConfig = defaultButtonEventTriggerConfig(props, tableAreaId, onButtonClick);
    
                let material_event = {
                    label: json['105602BZPT-0045']/* '操作' */,
                    itemtype: 'customer',
                    attrcode: 'opr',
                    width: '150px',
                    visible: true,
                    fixed: 'right',
                    render: (text, record, index) => {
                        
                        let visibleBtns = defaultBtnVisibleHandle(btnAreaId, customConfig, record, index, buttonTpl);
                        
                        return props.button.createOprationButton(visibleBtns, {
                            area: btnAreaId,
                            buttonLimit: customConfig.buttonLimit || 3,
                            onButtonClick: (props, btnKey) => {
                                if (typeof onButtonClick == 'function') {
                                    defaultButtonEventDispatch(props,btnKey, buttonsClickConfig, tableAreaId, record, index, customConfig.copyRowWithoutCols); 
                                }
    
                            }
                        });
                    }
                };
                tplData[tableAreaId].items.push(material_event);
            }
        }
    }})
}