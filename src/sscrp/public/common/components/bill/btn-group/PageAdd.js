const statusVar = window.presetVar.status;
export default function(props, btnKey, btnKeyConfig, afterClick){
    pubUtils.setUrlParam('status', statusVar.add);
    afterClick && afterClick();
}