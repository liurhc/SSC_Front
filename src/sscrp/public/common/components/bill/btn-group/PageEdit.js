import pubUtils from 'ssccommon/utils/pubUtils';
const statusVar = window.presetVar.status;
export default function(props, btnKey, btnKeyConfig, afterClick){
    pubUtils.setUrlParam('status', statusVar.edit);
    afterClick && afterClick();
}