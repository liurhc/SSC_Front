import {ajax} from 'nc-lightapp-front';
const statusVar = window.presetVar.status;
export default function(props, btnKey, btnKeyConfig, afterClick){
    let thisConfig = btnKeyConfig[btnKey];
    if(thisConfig.url == null){
        return;
    }
    let data = undefined;
    let bodyCodes = Object.getOwnPropertyNames(props.meta.getMeta().gridrelation);
    if(bodyCodes.length == 1){
        data = props.createMasterChildData(props.meta.getMeta().code, 'head', bodyCodes[0]);
    }else{
        data = props.createExtCardData(props.meta.getMeta().code, 'head', bodyCodes);
    }
    if(thisConfig.url != null){
        if(thisConfig.getUrlParam != null){
            data = thisConfig.getUrlParam(data);
        }
        ajax({
            url: thisConfig.url,
            data: data,
            success: (data) => {
                if(afterClick){
                    afterClick(data);
                }else{
                    location.hash = `#status=${statusVar.browse}&billId=${props.form.getFormItemsValue('head', 'pk_doc').value}`;
                }
            }
        })
    }else{
        afterClick && afterClick();
    }
}