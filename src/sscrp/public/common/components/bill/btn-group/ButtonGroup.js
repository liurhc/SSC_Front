import React, { Component } from 'react';
import { base,promptBox , getMultiLang,ajax,toast} from 'nc-lightapp-front';
import PageAdd from './PageAdd'
import PageEdit from './PageEdit';
import PageCancel from './PageCancel';
import PageSave from './PageSave';
import globalPresetVar from 'ssccommon/components/globalPresetVar';


const { NCDiv: Div } = base;



import BodyAdd from './BodyAdd'

const pageButton = globalPresetVar.pageButton;

import './index.less';
/**
 * 按钮组组件
 * 在该组件中判断按钮权限
 * 按钮权限状态通过redux控制
 */
class ButtonGroup extends Component {
    static comName = 'ButtonGroup';

    constructor(props) {
        super();
        this.state = {
            multiLang : {}
        }
        this.multiLangPromise = new Promise((resolve, reject) => {
            getMultiLang({moduleId: 1056, domainName: 'sscrp',currentLocale: 'zh-CN', callback: (json) => {
                resolve(json);
            }})
        })
/*         getMultiLang({moduleId: 1056, domainName: 'sscrp',currentLocale: 'zh-CN', callback: (json) => {
            this.setState({
                multiLang : json
            })
        }}) */
    }

    showModal(btnModalConfig, btnKey) {
        this.multiLangPromise.then((data) => {
            promptBox({
                color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                title: btnModalConfig.title,                // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输
                content: btnModalConfig.content,           // 提示内容,非必输
                beSureBtnName: data['105602BZCX-0044'],//"确定",          // 确定按钮名称, 默认为"确定",非必输
                cancelBtnName: data['105601DJYS-0005'],//"取消",           // 取消按钮名称, 默认为"取消",非必输
                beSureBtnClick:  this.props.buttonEvent()[btnKey],   // 确定按钮点击调用函数,非必输
                cancelBtnClick: null  // 取消按钮点击调用函数,非必输
            })
        })
        
    }

    btnClick(ncProps, btnKey) {
        // debugger;
        let btnKeyConfig = null;

        if (typeof this.props.buttonEvent == 'function') {
            let isLatestConfig = false;

            isLatestConfig = this.props.buttonEvent();

            if (!this.props.disableDefaultCancelPop && (!this.props.modalConfig || !this.props.modalConfig[pageButton.Cancel] || !this.props.modalConfig[pageButton.PageCancel] || !this.props.modalConfig[window.presetVar.pageButton.pageClose]) && (btnKey == pageButton.Cancel || btnKey == pageButton.PageCancel || btnKey == window.presetVar.pageButton.pageClose)) {
                //ncProps.modal.show(btnKey);
                this.multiLangPromise.then((data) => {
                    this.showModal({title: data['105601DJYS-0005'],content:  data['105601GGZD-0061']/* '确定要取消吗？' */}, btnKey);
                })
                return;
            }
            if (this.props.modalConfig) {
                let btnModalConfig = this.props.modalConfig[btnKey];
                if (btnModalConfig) {
                    if (btnModalConfig.beforeClick) {
                        switch (btnModalConfig.beforeClick()) {
                            case true:
                               // ncProps.modal.show(btnKey);
                                this.showModal(btnModalConfig, btnKey);
                                return;
                                break;
                            case 'exec':
                                break;
                            case false:
                                return;
                                break;
                        }
                    } else {
                        // ncProps.modal.show(btnKey);
                        this.showModal(btnModalConfig, btnKey);
                        return;
                    }
                }
            } else if (!isLatestConfig) { //函数switch按钮注册方式（废弃） 例：let pageButton = function(props, btnKey) { switch(btnKey) {}}
                console.warn('此按钮组事件代码实现结构不符合规范，请参考【费用申请单】里按钮组事件代码的规范写法！ \n by guoyangf@yonyou.com');
                this.props.buttonEvent(ncProps, btnKey);
                return;
            }

            //函数返回对象注册方式（标准）  函数返回json对象。每个key对应一个按钮事件的配置。 按钮事件的value为json则使用默认事件。为函数则重写默认click事件。
            btnKeyConfig = this.props.buttonEvent()[btnKey];
            if (typeof btnKeyConfig == 'function') {
                btnKeyConfig();
                return;
            } else if (typeof btnKeyConfig == 'object') {
                //this.btnKeyConfig = this.props.buttonEvent();
            }
        } else { //对象按钮注册方式。（废弃）  例： {Add： {beforeClick:() => {}， click: () => {}}， afterClick : () => {}}
            console.warn('此按钮组事件代码实现结构不符合规范，请参考【费用申请单】里按钮组事件代码的规范写法！ \n by guoyangf@yonyou.com');
            btnKeyConfig = this.props.buttonEvent[btnKey];
        }

        let defaultKey = (btnKeyConfig || {})['actionCode'] || btnKey;
        switch (defaultKey) {
            /* case pageButton.pageAdd:
                this.btnEventHandle(btnKey, PageAdd, btnKeyConfig);
                break; */
            case pageButton.Edit:
                //编辑
                this.btnEventHandle(btnKey, PageEdit, btnKeyConfig);
                break;
            case pageButton.Save:
                //保存单据
                this.btnEventHandle(btnKey, PageSave, btnKeyConfig);
                break;
            case 'Delete':
            //this.btnEventHandle(btnKey, Delete);
            // break;
            case pageButton.Cancel:
                this.btnEventHandle(btnKey, PageCancel, btnKeyConfig);
                break;
            case 'BodyAdd':
                this.btnEventHandle(btnKey, BodyAdd, btnKeyConfig);
                break;
            case 'linkMYApprove':  //备用金,预付款,报销单,还款单 联查明源链接
                    let a=this.props.ncProps.form.getFormItemsValue("head",'pk_jkbx').value
                    let b=this.props.ncProps.form.getFormItemsValue("head",'djlxbm').value
                    ajax({
                        url:'/nccloud/nc/myerp/erpurl.do',
                        data:{pk_bill:a ,tradeType:b }, 
                        success:(result)=>{
                            if(result.data.code==1){
                            var arr=result.data.url
                                if(arr){
                                        window.open(arr)
                                };
                            }else{
                                toast({color:'danger',title:result.data.msg});
                            }
                        },
                        error:(result)=>{
                        toast({color:'danger',title:result.message});
                        }
                    });
                break;
                case 'linkMYApprove1': // 预提单 联查明源链接
                        let pk_accrued_bill=this.props.ncProps.form.getFormItemsValue("head",'pk_accrued_bill').value
                        let pk_tradetype=this.props.ncProps.form.getFormItemsValue("head",'pk_tradetype').value
                        ajax({
                            url:'/nccloud/nc/myerp/erpurl.do',
                            data:{pk_bill:pk_accrued_bill,tradeType:pk_tradetype }, 
                            success:(result)=>{
                                if(result.data.code==1){
                                var arr=result.data.url
                                    if(arr){
                                            window.open(arr)
                                    };
                                }else{
                                    toast({color:'danger',title:result.data.msg});
                                }
                            },
                            error:(result)=>{
                            toast({color:'danger',title:result.message});
                            }
                        });
                    break;
                    case 'linkMYApprove2':    //计提单 联查明源链接
                            let pk_freeform=this.props.ncProps.form.getFormItemsValue("head",'pk_freeform').value
                            let transtype=this.props.ncProps.form.getFormItemsValue("head",'transtype').value
                            ajax({
                                url:'/nccloud/nc/myerp/erpurl.do',
                                data:{pk_bill:pk_freeform,tradeType:transtype }, 
                                success:(result)=>{
                                    if(result.data.code==1){
                                    var arr=result.data.url
                                        if(arr){
                                                window.open(arr)
                                        };
                                    }else{
                                        toast({color:'danger',title:result.data.msg});
                                    }
                                },
                                error:(result)=>{                    
                                toast({color:'danger',title:result.message});
                                }
                            });
                        break;
            default:
                this.btnEventHandle(btnKey, null, btnKeyConfig);
                break;

        }
    }

    btnEventHandle(btnKey, defaultFn, btnKeyConfig) {

        let { beforeClick, click, afterClick } = btnKeyConfig || {};
        if (click) {
            console.warn('此按钮组事件代码实现结构不符合规范，请参考【费用申请单】里按钮组事件代码的规范写法！ \n by guoyangf@yonyou.com');
            click(this.props.ncProps);
        } else {
            let beforeResult = beforeClick && beforeClick(this.props.ncProps) || !beforeClick; //1、按钮可见性控制。 2、自定义的请求参数。返回的结果如果不为空，则用于ajax请求的参数
            defaultFn && beforeResult && defaultFn.bind(this, this.props.ncProps, btnKey, this.btnKeyConfig, afterClick)();
        }
    }

    render() {

        let { ncProps, areaId, ctrlAreaId, modalConfig, ...btnGroupProps } = this.props;

        let jsx = [];
        jsx.push(
            <Div className='ct-shoulder-btn-wrap' field={ctrlAreaId} areaCode={Div.config.HEADER}>
                {this.props.ncProps.button.createButtonApp({
                    area: areaId || ctrlAreaId + '_card_body',
                    buttonLimit: 3,
                    ...btnGroupProps,
                    onButtonClick: this.btnClick.bind(this)
                })}
            </Div>
        )
        /* if (modalConfig) {
            for (let btnKey in modalConfig) {
                let btnModal = modalConfig[btnKey];
                jsx.push(ncProps.modal.createModal(btnKey, {
                    title: btnModal.title,
                    content: btnModal.content,
                    beSureBtnClick: this.props.buttonEvent()[btnKey]
                }))
            }
        }
        if (!modalConfig || !modalConfig[pageButton.Cancel]) {
            jsx.push(ncProps.modal.createModal(pageButton.Cancel, {
                title: '提示',
                content: '确定取消吗',
                beSureBtnClick: this.props.buttonEvent()[pageButton.Cancel]
            }))
        } */
        return jsx;

    }
}

export default ButtonGroup
