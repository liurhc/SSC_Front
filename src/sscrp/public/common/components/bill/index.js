import './index.less';

window.__ssc_gy = {};
export { default as BillStyle } from './BillStyle';
export { default as BillHead } from './BillHead';
export { default as BillBody } from './BillBody';
export { default as CardTable } from './card-table/CardTable';
export { default as CardTableGroup } from './card-table/CardTableGroup';
export { default as Form } from './form/Form';
export { default as ButtonGroup } from './btn-group/ButtonGroup';
export { default as MultiButtonGroup } from './btn-group/MultiButtonGroup';

export { default as setTableExtendCol } from './setTableExtendCol';
export { default as setTableGroupExtendCol } from './setTableGroupExtendCol';









