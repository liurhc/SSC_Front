import React, { Component } from 'react';

class BillBody extends Component {
    static comName = 'BillBody';

    constructor() {
        super();
    }

    render() {
        let billTable = [];
        let billTableGroup = null;
        let otherJsx = [];

        let JsxBySort = []; //子组件排序

        let { children, ...otherProps } = this.props;

        React.Children.forEach(children, (child) => {
            switch (child && child.type && child.type.comName) { //此处获取函数名不兼容。后续处理
                case 'CardTable':
                    billTable.push(React.cloneElement(child, otherProps));
                    break;
                case 'CardTableGroup':
                    billTableGroup = React.cloneElement(child, otherProps);
                    break;
                case 'Form':
                    console.error('请把BillBody标签里的Form标签移到BillHead标签中。 by guoyangf@yonyou.com');
                    break;
                default:
                    otherJsx.push(child)
                //console.warn('BillBody组件内，对子组件：', child, '不做处理')
            }
        })

        return [
            <div className="nc-bill-bottom-area">
                {billTable}
                {billTableGroup}
            </div>,
            ...otherJsx
        ]
    }
}

export default BillBody
