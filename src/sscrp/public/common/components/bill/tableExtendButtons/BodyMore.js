export default function(props, btnsClickFn, btnKey, tableAreaId, record, index){
    if(props.cardTable.getStatus(tableAreaId) == 'browse'){
        props.cardTable.toggleRowView(tableAreaId, record);
    }else{
        props.cardTable.openModel(tableAreaId, 'edit', record, index);
    }
}