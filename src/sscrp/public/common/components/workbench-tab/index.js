import React, {Component} from 'react';
import {connect} from 'react-redux';
import {base} from 'nc-lightapp-front';
const {NCTabs, NCIcon, NCButton} = base;
const NCTabPane = NCTabs.NCTabPane;

import './index.less';

const showTypeList="uf-navmenu";
const showTypeCard="uf-4square";
class WorkbenchTab extends Component {
    constructor(props) {
        super()
    }

    buttonClick(key) {
        this.props.btnsClick(key)
    }

    render() {
        
        const {tabs, showNumbers, showBtnsRight, btnsRight, defaultActiveKey, superBtn} = this.props
        const {selectTabChange, currentKey} = this.props

        let btns
        let defaultActiveTab = defaultActiveKey;
        if(defaultActiveTab == null){
            if(tabs != null && tabs.length>0){
                defaultActiveTab = tabs[0].attrcode;
            }
        }
        if (showBtnsRight) {
            btns = btnsRight.map(each => (
                <NCButton
                    colors={ each.color }
                    onClick={ this.buttonClick.bind(this, each.key) }>
                    {each.name}</NCButton>
            ))
        }
        return (
            <div id="SysComponentsWorkbenchTab">
                <NCTabs 
                    defaultActiveKey={defaultActiveTab}
                    activeKey={currentKey || tabs[0].attrcode}
                    navtype="turn" 
                    contenttype="moveleft" 
                    onChange={selectTabChange.bind(this)}
                >
                    {
                        tabs.map((one, index)=>{
                            if(showNumbers[one.attrcode]!=null){
                                return (
                                    <NCTabPane key={one.attrcode} tab={one.label+" "+showNumbers[one.attrcode]}>
                                    </NCTabPane>
                                )
                            }else{
                                return (
                                    <NCTabPane key={one.attrcode} tab={one.label}>
                                    </NCTabPane>
                                )
                            }
                            
                        })
                    }
                </NCTabs>

                {
                    showBtnsRight ? (
                        <div className="tab-btns">
                            {btns}
                            {!!superBtn && superBtn}
                        </div>
                    ) : null
                }
            </div>
        )
    }
}

export default WorkbenchTab;