import React, {Component} from 'react';

class EditTable extends Component {
    static comName = 'EditTable';

    constructor() {
        super();
    }

    render() {
        let buttonGroup = null;

        let {children, title, areaId, ...editTableProps} = this.props;

        const {ncProps} = window._ssc_gy_profile ;

        const { createEditTable } = ncProps.editTable;

        children && React.Children.forEach(children, (child) => {
            if (child && child.type) {
                switch (child.type.comName) { //此处获取函数名不兼容。后续处理
                    case 'ButtonGroup':
                        buttonGroup = child;
                        if(window.SSC_EditTable == null){
                            window.SSC_EditTable = {};
                        }
                        window.SSC_EditTable[areaId] = {hasButtonGroup:true, buttonGroupArea: child.props.area};
                        break;
                    default:
                        console.warn('EditTable组件内，对子组件：', child, '不做处理')
                }
            }

        });

        return (
            <div className={"area "+ (this.props.className || '')}>
                { this.props.title || buttonGroup ?
                    <div className="area-head">
                        <div className="title">
                            {this.props.title}
                        </div>
                        <div className="btn-group">
                            {buttonGroup}
                        </div>
                    </div>

                    :
                    null
                }
                {createEditTable(areaId, {
                    showCheck: true,
                    showIndex: true,
                    isAddRow: false,
                    ...editTableProps
                })}
            </div>
        )
    }
}

export default EditTable
