import React, {Component} from 'react';

class BodyRight extends Component {
    static comName = 'BodyRight';

    constructor() {
        super();
    }

    render() {
        return this.props.children || null;
    }
}

export default BodyRight
