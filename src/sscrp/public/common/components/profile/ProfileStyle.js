import React, {Component} from 'react';

// import FinanceOrgTreeRef from 'uapbd/refer/org/FinanceOrgTreeRef';


class ProfileStyle extends Component {
    constructor() {
        super();
        window._ssc_gy_profile = {

        }
    }

    /* recursionTag(tag, otherProps) {
        if (tag && tag.props && tag.props.children &&  tag.type.comName ) {
            tag.props.children = React.Children.map(tag.props.children, (child) => {
                let newChild = this.recursionTag(child, otherProps);
                if (newChild && newChild.type && newChild.type.comName) {
                    newChild = React.cloneElement(newChild, otherProps);
                    //console.log(newChild.type.comName, 118899, newChild.props, tag);
                }
                return newChild || null;
            });
            if (tag.props.children.length == 1) {
                tag.props.children = tag.props.children.pop();
            }
        }
        return tag;
    } */

    render() {
        let profileHead = null;
        let profileBody = null;
        let otherJsx = [];

        let profileLayoutList = {
            singleTable: 'singleTable',
            treeTable: 'treeTable'
        }
        let {children, layout, id, ...otherProps} = this.props;

        window._ssc_gy_profile = {
            ...window._ssc_gy_profile,
            layout,
            profileLayoutList,
            ncProps: otherProps
        }
       

        //this.recursionTag(this, otherProps);

        React.Children.forEach(this.props.children, (child) => {
            if (child && child.type) {
                if (child.type.comName) {
                    switch (child.type.comName) { //此处获取函数名不兼容。后续处理
                        case 'ProfileHead':
                            profileHead = child;
                            break;
                        case 'ProfileBody':
                            profileBody = child
                            break;
                        default:
                            otherJsx.push(child);
                    }
                } else {
                    otherJsx.push(child);
                }
            } else {
                otherJsx.push(child);
            }

        });
        return (
            (() => {
                switch (this.props.layout) {
                    case profileLayoutList.treeTable :
                        return (
                            <div id={id} className="nc-bill-tree-table">
                                <div className='bottom-bgc-for-bill-tree-table'></div>
                                {profileHead}
                                {profileBody}
                                {otherJsx}
                            </div>
                        )
                    case profileLayoutList.singleTable:
                        return (
                            <div id={id} className="nc-single-table">
                                <div className='bottom-bgc-for-single-table'></div>
                                {/* 头部 header */}
                                {profileHead}
                                {/*查询区*/}
                                {/*<div className="nc-singleTable-search-area">

                                </div>*/}
                                {/* 列表区 */}
                                {profileBody}
                                {otherJsx}
                            </div>

                        );
                        break;
                }

            })()
        )
    }
}

export default ProfileStyle
