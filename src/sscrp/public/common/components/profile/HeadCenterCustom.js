import React, {Component} from 'react';

class HeadCenterCustom extends Component {
    static comName = 'HeadCenterCustom';

    constructor() {
        super();
    }

    render() {
        let headCenterCustomContent = null;

        const {children} = this.props;
        const {profileLayoutList, layout} = window._ssc_gy_profile;
        
        if (!children) {
            return null;
        }
        headCenterCustomContent = React.Children.map(children, (child) => {
            switch(layout) {
                case profileLayoutList.treeTable :
                    return child
                    break;
                case profileLayoutList.singleTable:
                    return (
                        <div className="title-search-detail">
                            {child}
                        </div>
                    )
                    break;
            }
        });

        return headCenterCustomContent;
    }
}

export default HeadCenterCustom
