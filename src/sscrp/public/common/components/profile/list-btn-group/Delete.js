import {ajax} from 'nc-lightapp-front';

export default function (buttonConfig, areaId, ncProps, record, index) {
    let beforeResult = buttonConfig.beforeClick && buttonConfig.beforeClick(areaId, "Delete", record, index, ncProps) || !buttonConfig.beforeClick;
    if(!beforeResult){
        return;
    }

    if(ncProps.editTable.getStatus(areaId) == null || ncProps.editTable.getStatus(areaId) == "browse"){
        let saveUrl = buttonConfig.url;
        if(saveUrl != null){
            ncProps.editTable.delRow(areaId, index);
            let saveData = {};
            saveData[areaId] = ncProps.editTable.getAllData(areaId);
            let delsign= false;
            ajax({
                url: saveUrl,
                data: saveData,
                success: (data)=>{
                    data && ncProps.editTable.setTableData(areaId, data.data[areaId]);
                    buttonConfig.afterClick && buttonConfig.afterClick(areaId, "Delete", record, index, ncProps, data);
                    delsign=true;
                }
            })
            if(!delsign){
                ncProps.editTable.cancelEdit(areaId);
            }

        }else{
            buttonConfig.afterClick && buttonConfig.afterClick(areaId, "Delete", record, index, ncProps);
        }
    }else{
        ncProps.editTable.delRow(areaId, index);
        buttonConfig.afterClick && buttonConfig.afterClick(areaId, "Delete", record, index, ncProps);
    }





}
