import React, { Component } from 'react';

class ProfileBody extends Component {
    static comName = 'ProfileBody';
    constructor() {
        super();
    }

    render() {
        let bodyLeft = null;
        let bodyRight = null;
        let bodyTop = null;
        let singleTableBodyContent = null;

        const { children } = this.props;
        const {profileLayoutList, layout, ncProps} = window._ssc_gy_profile ;

        const { DragWidthCom } = ncProps;

        if  (!children) {
            return null;
        }
        React.Children.forEach(children, (child) => {
            if (layout == profileLayoutList.treeTable) {
                if (child && child.type && child.type.comName) {
                    switch (child.type.comName) { //此处获取函数名不兼容。后续处理
                        case 'BodyLeft':
                            bodyLeft = child;
                            break;
                        case 'BodyRight':
                            bodyRight = child;
                            break;
                        case 'BodyTop':
                            bodyTop = child;
                            break;
                        default:
                            console.warn('ProfileBody组件内，对子组件：', child, '不做处理')
                    }
                } else {
                    console.error('左树右表布局中，ProfileBody只能包含BodyTop、BodyLeft、BodyRight组件');
                }

            } else if (layout == profileLayoutList.singleTable) {
                singleTableBodyContent = children;
            }

        });
        return (() => {
            switch (layout) {
                case profileLayoutList.treeTable:
                    return (
                        [
                            <div>
                                {bodyTop}
                            </div>,
                            <div className="body-area tree-table">
                                <DragWidthCom
                                    // 左树区域
                                    leftDom={
                                        <div className="left-area tree-area">
                                            {bodyLeft}
                                        </div>
                                    }
                                    // 右表区
                                    rightDom={
                                        <div className="right-area table-area">
                                            {bodyRight}
                                        </div>
                                    }
                                    defLeftWid='20%'      // 默认左侧区域宽度，px/百分百
                                />
                            </div>
                        ]
                    );
                    break;
                case profileLayoutList.singleTable:
                    return (
                        <div className="nc-singleTable-table-area">
                            {singleTableBodyContent}
                        </div>
                    )
                    break;
            }
        })()
    }
}


export default ProfileBody
