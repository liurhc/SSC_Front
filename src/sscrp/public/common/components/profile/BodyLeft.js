import React, {Component} from 'react';

class BodyLeft extends Component {
    static comName = 'BodyLeft';

    constructor() {
        super();
    }

    render() {
        return (this.props.children || null)
    }
}

export default BodyLeft
