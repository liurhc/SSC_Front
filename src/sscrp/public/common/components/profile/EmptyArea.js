import React, {Component} from 'react';

/**
 * 空区域
 */
class EmptyArea extends Component {
    static comName = 'EmptyArea';

    constructor() {
        super();
    }

    render() {
        return (
            <div className={`empty-area area ${this.props.className || ''}`}>
                { this.props.title ?
                    <div className="area-head">
                        <div className="title">
                            {this.props.title}
                        </div>
                    </div>
                    :
                    null
                }
                {this.props.children}
            </div>
        )
    }
}

export default EmptyArea
