import React, {Component} from 'react';

class BodyTop extends Component {
    static comName = 'BodyTop';

    constructor() {
        super();
    }

    render() {
        return (this.props.children || null)
    }
}

export default BodyTop
