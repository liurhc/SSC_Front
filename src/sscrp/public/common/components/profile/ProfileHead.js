import React, {Component} from 'react';
import {base, createPageIcon} from 'nc-lightapp-front';
const {NCRefreshBtn}=base;

class ProfileHead extends Component {
    static comName = 'ProfileHead';
    constructor() {
        super();
    }

    handleClick(){
        if(this.props.refreshButtonEvent){
            this.props.refreshButtonEvent();
        }
    }

    render() {
        let buttonGroup = null, customButton = null;
        let headCenterCustom = null;

        const {children} = this.props;
        const {profileLayoutList, layout} = window._ssc_gy_profile ;

        React.Children.forEach(children, (child) => {
            if (child && child.type) {
                switch (child.type.comName) { //此处获取函数名不兼容。后续处理
                    case 'ButtonGroup':
                        buttonGroup = child;
                        break;
                    case 'HeadCenterCustom':
                        headCenterCustom = child
                        break;
                    default:
                        if(child.props.comName === "NCButton") {
                            customButton = child
                        }
                        console.warn('ProfileHead组件内，对子组件：', child, '不做处理')
                }
            }

        });
        return (() => {
            switch(layout) {
                case profileLayoutList.treeTable:
                    return (
                        <div className="header">
                            {createPageIcon()}
                            <div className="header-left title">
                                {this.props.title}
                            </div>
                            <div className="header-middle">
                                {headCenterCustom}
                            </div>
                            <div className="header-right btn-group">
                                {buttonGroup}
                                {customButton}
                            </div>
                            <NCRefreshBtn style={{ cursor : 'pointer', display : this.props.status === 'browse'?'inherit':'none'}} onClick={ this.handleClick.bind(this) }></NCRefreshBtn>
                        </div>
                    );
                    break;
                case profileLayoutList.singleTable:
                    return (
                        <div className="nc-singleTable-header-area">
                            {/* 标题 title */}
                            <div className="header-title-search-area">
                                {createPageIcon()}
                                <h2 className="title-search-detail">
                                    {this.props.title}
                                </h2>
                                {/*除了h2标题外，小组件都要外层嵌套一个div且className命名为title-search-detail，如下*/}
                                {headCenterCustom}
                            </div>
                            {/* 按钮区  btn-group */}
                            <div className="header-button-area">
                                {buttonGroup}
                                {customButton}
                            </div>
                            <NCRefreshBtn style={{ cursor : 'pointer', display : this.props.status === 'browse'?'inherit':'none'}} onClick={ this.handleClick.bind(this) }></NCRefreshBtn>
                        </div>
                    )
                    break;
            }
        })()
    }
}

export default ProfileHead
