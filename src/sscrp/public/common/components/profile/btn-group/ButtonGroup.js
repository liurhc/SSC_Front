import React, { Component } from 'react';
import { base, promptBox, getMultiLang } from 'nc-lightapp-front';
import Edit from './Edit';
import Save from './Save';
import Add from './Add';
import Delete from './Delete';
import Cancel from './Cancel';
import globalPresetVar from 'ssccommon/components/globalPresetVar';
const pageButton = globalPresetVar.pageButton;


const { NCDiv: Div } = base;

/**
 * 按钮组组件
 * 在该组件中判断按钮权限
 * 按钮权限状态通过redux控制
 */
class ButtonGroup extends Component {
    static comName = 'ButtonGroup';
    constructor(props) {
        super();
        this.state = {
            multiLang : {}
        }
        getMultiLang({moduleId: 1056, domainName: 'sscrp',currentLocale: 'zh-CN', callback: (json) => {
            this.setState({
                multiLang : json
            })
        }})
        this.regBtn = props.buttonEvent || props.onButtonClick;

        let warnText = `区域：${props.area || props.areaId} 里，` + '按钮组事件代码实现结构不符合规范，请参考【费用申请单】里按钮组事件代码的规范写法！ \n by guoyangf@yonyou.com';
        if (typeof this.regBtn == 'function') {
            let isLatestConfig = false;
            isLatestConfig = this.regBtn();
            if (typeof isLatestConfig == 'object') {
                this.configType = 'std';
            } else {
                this.configType = 'switch';
                presetVar.isDevelopEnv && console.warn(warnText);
            }
        } else if (typeof this.regBtn == 'object') {
            this.configType = 'oldProfile';
            presetVar.isDevelopEnv && console.warn(warnText);
        }
    }

    showModal(btnModalConfig, btnKey) {
        promptBox({
            color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
            title: btnModalConfig.title,                // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输
            content: btnModalConfig.content,           // 提示内容,非必输
            beSureBtnName: this.state.multiLang['105602BZCX-0044'],//"确定",          // 确定按钮名称, 默认为"确定",非必输
            cancelBtnName: this.state.multiLang['105601DJYS-0005'],//"取消",           // 取消按钮名称, 默认为"取消",非必输
            beSureBtnClick:  this.beSureBtnClick.bind(this, btnKey),   // 确定按钮点击调用函数,非必输
            cancelBtnClick: null  // 取消按钮点击调用函数,非必输
        })
    }

    onButtonClick(ncProps, btnKey) {
        let multiLang = ncProps.MutiInit.getIntl(1056);
        if (!this.props.disableDefaultCancelPop && (!this.props.modalConfig || !this.props.modalConfig[pageButton.Cancel]) && btnKey == pageButton.Cancel) {
            //ncProps.modal.show(btnKey);
            this.showModal({title: this.state.multiLang['105601DJYS-0005'],content: this.state.multiLang['105601GGZD-0061']/* '确定要取消吗？' */}, btnKey);
            return;
        }
        if (this.props.modalConfig) {
            let btnModalConfig = this.props.modalConfig[btnKey];
            if (btnModalConfig) {
                if (btnModalConfig.beforeClick) {
                    switch (btnModalConfig.beforeClick()) {
                        case true:
                            //ncProps.modal.show(btnKey);
                            this.showModal(btnModalConfig, btnKey);
                            return;
                            break;
                        case 'exec': //直接执下面handleClick
                            break;
                        case false:
                            return;
                            break;
                    }
                } else {
                    //ncProps.modal.show(btnKey);
                    this.showModal(btnModalConfig, btnKey);
                    return;
                }
            }
        }
        this.handleClick(ncProps, btnKey)
    }

    //modal框 确认按钮
    beSureBtnClick(btnKey) {
        this.handleClick(this.ncProps, btnKey);
    }

    handleClick(ncProps, btnKey) {
        switch (this.configType) {
            case 'switch':
                this.regBtn(ncProps, btnKey);
                return;
                break;
            case 'std':
                let btnKeyConfig = this.regBtn()[btnKey];
                this.buttonEvent = this.regBtn();
                if (typeof btnKeyConfig == 'function') {
                    btnKeyConfig();
                    return;
                } else {
                    this.btnKeyConfig = btnKeyConfig;
                    //注意此处没有return
                }
                break;
            case 'oldProfile':
                this.btnKeyConfig = this.regBtn[btnKey];
                this.buttonEvent = this.regBtn;
                break;
            default:
                console.warn('不识别的配置方式')
        }

        let defaultKey = (this.btnKeyConfig || {})['ActionCode'] || btnKey;
        switch (defaultKey) {
            case 'Add':
                this.btnEventHandle(btnKey, Add);
                break;
            case 'Edit':
                debugger;
                this.btnEventHandle(btnKey, Edit);
                break;
            case 'Save':
                this.btnEventHandle(btnKey, Save);
                break;
            case 'Delete':
                this.btnEventHandle(btnKey, Delete);
                break;
            case 'Cancel':
                this.btnEventHandle(btnKey, Cancel);
                break;
            default:
                this.btnEventHandle(btnKey, null);
        }

    }

    btnEventHandle(btnKey, defaultFn) {
        if (!this.btnKeyConfig) {
            // console.warn(`未找到 ${btnKey} 按钮的配置  by gy`)
            this.btnKeyConfig = {};
        }
        let { beforeClick, click, afterClick } = this.btnKeyConfig;
        if (click) {
            click(this.ncProps)
        } else {
            let beforeResult = beforeClick && beforeClick(this.ncProps) || !beforeClick; //1、按钮可见性控制。 2、自定义的请求参数。返回的结果如果不为空，则用于ajax请求的参数
            defaultFn && beforeResult && defaultFn.bind(this, this.btnKeyConfig, afterClick, this.props.ctrlEditTableArea, this.props, this.buttonEvent)();
        }
    }

    render() {

        let {area, areaId, modalConfig, buttonEvent, onButtonClick, ...btnGroupProps } = this.props;

        const {ncProps} = window._ssc_gy_profile ;

        this.ncProps = ncProps;
        this.btnKeyConfig = null;
        this.regBtn = buttonEvent || onButtonClick;

        let jsx = [];
        jsx.push(
            <Div field={area || areaId} areaCode={Div.config.HEADER} style={{fontSize: '0'}}>
                {
                    ncProps.button.createButtonApp({
                        area: area || areaId,
                        buttonLimit: 3,
                        ...btnGroupProps,
                        onButtonClick: this.onButtonClick.bind(this)
                    })
                }
            </Div>
        );

        /* if (modalConfig) {
            for (let btnKey in modalConfig) {
                let btnModal = modalConfig[btnKey];
                jsx.push(ncProps.modal.createModal(btnKey, {
                    title: btnModal.title,
                    content: btnModal.content,
                    beSureBtnClick: this.beSureBtnClick.bind(this, btnKey)
                }))
            }
        }
        if (!modalConfig || !modalConfig[pageButton.Cancel]) {
            jsx.push(ncProps.modal.createModal(pageButton.Cancel, {
                className: 'junior',
                title: '提示',
                content: '确定要取消吗？',
                beSureBtnClick: this.beSureBtnClick.bind(this, pageButton.Cancel)
            }))
        } */
        return jsx;
    }
}

export default ButtonGroup
