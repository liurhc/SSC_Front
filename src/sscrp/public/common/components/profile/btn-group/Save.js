import {ajax, toast} from 'nc-lightapp-front';

export default function (btnEventConfig, afterClick, defCtrlEditTableArea, buttonGroupprops, buttonEvent) {
    let statusVar = window.presetVar.status;

    const {ncProps} = window._ssc_gy_profile ;

    let ctrlEditTableArea = btnEventConfig.ctrlEditTableArea || defCtrlEditTableArea || [];
    let ctrlFormArea = buttonGroupprops.ctrlFormArea || [];

    let saveUrl = btnEventConfig.url;

    let filterEmptyRows = btnEventConfig.filterEmptyRows;
    if (filterEmptyRows) {
        filterEmptyRows = Object.values(filterEmptyRows);
    }

    let getSendParam = btnEventConfig.getSendParam;

    let reqData = {
        data: {}
    }

    if(saveUrl != null && ctrlEditTableArea.lenght != 0){
        let saveData = {};
        let flag = true;
            ctrlEditTableArea.forEach((areaId) => {
                flag = ncProps.editTable.checkRequired(areaId,[]);
                if (filterEmptyRows) {
                    ncProps.editTable.filterEmptyRows(...filterEmptyRows)
                }
                saveData[areaId] = ncProps.editTable.getAllData(areaId);
            })
            ctrlFormArea.forEach((areaId) => {
                saveData[areaId] = ncProps.form.getAllFormValue(areaId);
            })

            if(getSendParam != null){
                saveData = btnEventConfig.getSendParam(saveData);
            }
        if(flag){
            ajax({
                url: saveUrl,
                data: saveData,
                success: (data)=>{
                    if(btnEventConfig.showSuccessMsg != false){
                        toast({title: '保存成功'});
                    }
                    ctrlEditTableArea.forEach((areaId) => {
                        let buttonsVisible={};
                        ncProps.button.getButtons().map((btnOne, index)=>{
                            if(this.props.area == btnOne.area){
                                if(btnOne.children){
                                    btnOne.children.map((btnOneChild, index)=>{
                                        if(this.props.area == btnOneChild.area){
                                            // if(btnOne.key == 'Add' || ((buttonEvent || {})[btnOne.key] || {}).ActionCode == 'Add'){
                                            //     buttonsVisible[btnOne.key]=true;
                                            // }else 
                                            if(btnOneChild.key == 'Edit' || ((buttonEvent || {})[btnOneChild.key] || {}).ActionCode == 'Edit'){
                                                buttonsVisible[btnOneChild.key]=true;
                                            }else if(btnOneChild.key == 'Save' || ((buttonEvent || {})[btnOneChild.key] || {}).ActionCode == 'Save'){
                                                buttonsVisible[btnOneChild.key]=false;
                                            }else if(btnOneChild.key == 'Cancel' || ((buttonEvent || {})[btnOneChild.key] || {}).ActionCode == 'Cancel'){
                                                buttonsVisible[btnOneChild.key]=false;
                                            }
                                        }
                                    });
                                }
                                if(btnOne.key == 'Edit' || ((buttonEvent || {})[btnOne.key] || {}).ActionCode == 'Edit'){
                                    buttonsVisible[btnOne.key]=true;
                                }else if(btnOne.key == 'Save' || ((buttonEvent || {})[btnOne.key] || {}).ActionCode == 'Save'){
                                    buttonsVisible[btnOne.key]=false;
                                }else if(btnOne.key == 'Cancel' || ((buttonEvent || {})[btnOne.key] || {}).ActionCode == 'Cancel'){
                                    buttonsVisible[btnOne.key]=false;
                                }
                            }
                        })
                        ncProps.button.setButtonsVisible(buttonsVisible);
    
                        ncProps.editTable.setStatus(areaId, 'browse');
                        data && data.data && data.data[areaId] && ncProps.editTable.setTableData(areaId, data.data[areaId]);
                        // 处理后事件
                        afterClick && afterClick(ncProps, data);
                        
                    })
                    ctrlFormArea.forEach((areaId) => {
                        let buttonsVisible={};
                        ncProps.button.getButtons().map((btnOne, index)=>{
                            if(this.props.area == btnOne.area){
                                if(btnOne.children){
                                    btnOne.children.map((btnOneChild, index)=>{
                                        if(this.props.area == btnOneChild.area){
                                            // if(btnOne.key == 'Add' || ((buttonEvent || {})[btnOne.key] || {}).ActionCode == 'Add'){
                                            //     buttonsVisible[btnOne.key]=true;
                                            // }else 
                                            if(btnOneChild.key == 'Edit' || ((buttonEvent || {})[btnOneChild.key] || {}).ActionCode == 'Edit'){
                                                buttonsVisible[btnOneChild.key]=true;
                                            }else if(btnOneChild.key == 'Save' || ((buttonEvent || {})[btnOneChild.key] || {}).ActionCode == 'Save'){
                                                buttonsVisible[btnOneChild.key]=false;
                                            }else if(btnOneChild.key == 'Cancel' || ((buttonEvent || {})[btnOneChild.key] || {}).ActionCode == 'Cancel'){
                                                buttonsVisible[btnOneChild.key]=false;
                                            }
                                        }
                                    });
                                }
                                if(btnOne.key == 'Edit' || ((buttonEvent || {})[btnOne.key] || {}).ActionCode == 'Edit'){
                                    buttonsVisible[btnOne.key]=true;
                                }else if(btnOne.key == 'Save' || ((buttonEvent || {})[btnOne.key] || {}).ActionCode == 'Save'){
                                    buttonsVisible[btnOne.key]=false;
                                }else if(btnOne.key == 'Cancel' || ((buttonEvent || {})[btnOne.key] || {}).ActionCode == 'Cancel'){
                                    buttonsVisible[btnOne.key]=false;
                                }
                                // else if(btnOne.key == 'Add' || ((buttonEvent || {})[btnOne.key] || {}).ActionCode == 'Add'){
                                //     buttonsVisible[btnOne.key]=false;
                                // } 
                            }
                        }) 
                        ncProps.button.setButtonsVisible(buttonsVisible); 
                        ncProps.form.setFormStatus(areaId, 'browse');
                        data && data.data && data.data[areaId] && ncProps.form.setAllFormValue({[areaId]:data.data[areaId]});
                        // 处理后事件
                        afterClick && afterClick(ncProps, data);
                        
                    })
                }
            })
        }
    }





    //档案页面无状态
    /*ctrlEditTableArea.forEach((areaId) => {
        ncProps.editTable.setStatus(areaId, statusVar.browse);

        console.log(8888, ncProps.editTable.getAllData(areaId))
        reqData.data = {...reqData.data, ...ncProps.editTable.getAllData(areaId)}

    })

    console.log(88, JSON.stringify(reqData))
    ajax({
        url: btnEventConfig.reqUrl,
        data: reqData,
        success: (data) => {

        }
    })*/

    /*ctrlFormArea.forEach((areaId) => {
        ncProps.form.setStatus(areaId, statusVar.browse);

    })*/
}