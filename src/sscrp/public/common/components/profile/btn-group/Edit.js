export default function (btnEventConfig, afterClick, defCtrlEditTableArea, buttonGroupprops, buttonEvent) {

    let statusVar = window.presetVar.status;

    const {ncProps} = window._ssc_gy_profile ;

    let ctrlEditTableArea = btnEventConfig.ctrlEditTableArea || defCtrlEditTableArea || [];
    let ctrlFormArea = buttonGroupprops.ctrlFormArea || [];

    //档案页面无状态
    ctrlEditTableArea.forEach((areaId) => {
        ncProps.editTable.setStatus(areaId, statusVar.edit);
        if(window.SSC_EditTable != null && window.SSC_EditTable[areaId] != null && window.SSC_EditTable[areaId].hasButtonGroup){
            
        }
    })
    ctrlFormArea.forEach((areaId) => {
        ncProps.form.setFormStatus(areaId, statusVar.edit);

    })

    let buttonsVisible={};
    ncProps.button.getButtons().map((btnOne, index)=>{
        if(this.props.area == btnOne.area){
            if(btnOne.children){
                btnOne.children.map((btnOneChild, index)=>{
                    if(this.props.area == btnOneChild.area){
                        // if(btnOne.key == 'Add' || ((buttonEvent || {})[btnOne.key] || {}).ActionCode == 'Add'){
                        //     buttonsVisible[btnOne.key]=true;
                        // }else 
                        if(btnOneChild.key == 'Edit' || ((buttonEvent || {})[btnOneChild.key] || {}).ActionCode == 'Edit'){
                            buttonsVisible[btnOneChild.key]=false;
                        }else if(btnOneChild.key == 'Save' || ((buttonEvent || {})[btnOneChild.key] || {}).ActionCode == 'Save'){
                            buttonsVisible[btnOneChild.key]=true;
                        }else if(btnOneChild.key == 'Cancel' || ((buttonEvent || {})[btnOneChild.key] || {}).ActionCode == 'Cancel'){
                            buttonsVisible[btnOneChild.key]=true;
                        }
                    }
                });
            }
            if(this.props.area == btnOne.area){
                // if(btnOne.key == 'Add' || ((buttonEvent || {})[btnOne.key] || {}).ActionCode == 'Add'){
                //     buttonsVisible[btnOne.key]=true;
                // }else 
                if(btnOne.key == 'Edit' || ((buttonEvent || {})[btnOne.key] || {}).ActionCode == 'Edit'){
                    buttonsVisible[btnOne.key]=false;
                }else if(btnOne.key == 'Save' || ((buttonEvent || {})[btnOne.key] || {}).ActionCode == 'Save'){
                    buttonsVisible[btnOne.key]=true;
                }else if(btnOne.key == 'Cancel' || ((buttonEvent || {})[btnOne.key] || {}).ActionCode == 'Cancel'){
                    buttonsVisible[btnOne.key]=true;
                }
            }
        }
    })
    ncProps.button.setButtonsVisible(buttonsVisible);

    


    // 处理后事件
    afterClick && afterClick(ncProps);
}