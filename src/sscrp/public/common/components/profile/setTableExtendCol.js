import Delete from './list-btn-group/Delete';
import { getMultiLang } from 'nc-lightapp-front';

export default (props, meta, optArray) => {
    getMultiLang({moduleId: 1056, domainName: 'sscrp',currentLocale: 'zh-CN', callback: (json) => {
        optArray.forEach((config) => {
            let {areaId,tableAreaId, btnAreaId, btnKeys, buttonVisible, onButtonClick, buttonLimit, width}= config;
            areaId = areaId || tableAreaId;
            btnAreaId || (btnAreaId = areaId);
            if (btnKeys) {
                presetVar.isDevelopEnv && console.warn(`${areaId}区域，`, '表格扩展列的 btnKeys 配置属性不建议使用，请使用 buttonVisible 代替,用法请参考【费用申请单】。 by guoyangf@yonyou.com');
            }
            let material_event = {
                label: json['105602BZPT-0045']/* '操作' */,
                itemtype: 'customer',
                attrcode: 'opr',
                width: width || '150px',
                visible: true,
                fixed: 'right', //引发双排操作列的问题
                render: (text, record, index) => {
                    return props.button.createOprationButton(btnKeys || (buttonVisible && buttonVisible(record, index)), {
                        area: btnAreaId,
                        buttonLimit: buttonLimit || 3,
                        onButtonClick: (props, btnKey) => {
    
                            if (typeof onButtonClick == 'function') {
                                let btnsClickConfig = onButtonClick(btnKey, record, index);
                                if (typeof btnsClickConfig == 'object') {
                                    if (typeof btnsClickConfig[btnKey] == 'object' && btnsClickConfig[btnKey].click == null) {
                                        let defaultKey = btnsClickConfig[btnKey].ActionCode || btnKey;
                                        switch(defaultKey) {
                                            case 'Delete':
                                                Delete(btnsClickConfig[btnKey], areaId, props, record, index)
                                                break;
                                            default:
                                                onButtonClick(props, btnKey, record, index);
                                                break;
                                        }
                                    }else if (typeof btnsClickConfig[btnKey] == 'object' && btnsClickConfig[btnKey].click != null) {
                                        btnsClickConfig[btnKey].click(btnKey, record, index);
                                    } else if(typeof btnsClickConfig[btnKey] == 'function') {
                                        btnsClickConfig[btnKey](record, index)
                                    }
                                } else {
                                    presetVar.isDevelopEnv && console.warn(`${areaId}区域，`, '表格扩展按钮点击事件代码实现结构不规范，请参考【费用申请单】里按钮组事件代码的规范写法！ \n by guoyangf@yonyou.com');
                                }
                            }
                        }
                    });
                }
            };
            meta[areaId].items.push(material_event);
            // TOP 平台没有刷新，暂时增加强制刷新 ADD
            props.meta.setMeta(meta);
            // BTM 平台没有刷新，暂时增加强制刷新
        })
    }})
}