const actionTypes = {
    changeBillHeads: 'changeBillHeads',
    changeBillBodys: 'changeBillBodys',
    changeAssociatedTable: 'changeAssociatedTable'
}

const initialState = {
    billHeads: [],
    billBodys: [],
    associatedTable: []
};

function globalReducer (state = initialState, action) {
    switch(action.type) {
        case actionTypes.changeBillHeads :
            return {
                ...state,
                billHeads: action.billHeads
            }
            break;
        case actionTypes.changeBillBodys :
            return {
                ...state,
                billBodys: action.billBodys
            }
            break;
        case actionTypes.changeAssociatedTable :
            return {
                ...state,
                associatedTable: action.associatedTable
            }
            break;
        default :
            return state;
    }
}

export default globalReducer;