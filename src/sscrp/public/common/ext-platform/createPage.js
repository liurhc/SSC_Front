import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, base, ajax } from 'nc-lightapp-front';
import {
    createTransferTable,
    createTransferList,
    createMultiTransferTable,
    getTransferTableValue,
    setTransferTableValue,
    setChildTransferTableData,
    getTransferTableSelectedValue,
    setTransferListValue,
    getSelectedListDisplay,
    getTransformFormDisplay,
    setTransformFormStatus,
    changeNextTransformForm,
    returnTransferTableList,
    getTransformFormAmount,
    setMultiTransformFormStatus,
    changeViewType,
    getTransferTableSelectedId,
    getTransferTableMultiSelectedId,
    getTransformFormCompleteStatus,
    setTransferListValueByIndex,
    setMultiSelectedValue,
    setRowDataByIndex
} from 'ssccommon/ext-platform/transferTable';


export default ({ initTemplate, mutiLangCode, billinfo }) => (App) => {

    class extApp extends Component {
        constructor(props) {
            super(props);
            this.state = {
                transferTable: {},
                meta: {}
            }
            let meta = {
                setMeta: (meta, callback) => {
                    this.setState({
                        meta: meta
                    });
                    props.meta.setMeta(meta, callback);
                },
                addMeta: (meta) => {
                    props.meta.addMeta(meta);
                    setTimeout(() => {
                        this.setState({
                            meta: props.meta.getMeta()
                        })
                    })
                },
                removeMeta: (meta) => {
                    props.meta.removeMeta(meta);
                    setTimeout(() => {
                        this.setState({
                            meta: props.meta.getMeta()
                        })
                    })
                }
            }
            this.output = {
                meta: { ...props.meta, ...meta },
                extTransferTable: {
                    createTransferTable: createTransferTable.bind(this), //创建转单已选列表
                    createTransferList: createTransferList.bind(this), //创建卡片型转单
                    createMultiTransferTable: createMultiTransferTable.bind(this), //创建多来源转单
                    getTransferTableValue: getTransferTableValue.bind(this), //获取表格数据
                    setTransferTableValue: setTransferTableValue.bind(this), //更新表格数据
                    setChildTransferTableData: setChildTransferTableData.bind(this),
                    getTransferTableSelectedValue: getTransferTableSelectedValue.bind(this),
                    setTransferListValue: setTransferListValue.bind(this),
                    getSelectedListDisplay: getSelectedListDisplay.bind(this),
                    getTransformFormDisplay: getTransformFormDisplay.bind(this),
                    setTransformFormStatus: setTransformFormStatus.bind(this),
                    changeNextTransformForm: changeNextTransformForm.bind(this),
                    returnTransferTableList: returnTransferTableList.bind(this),
                    getTransformFormAmount: getTransformFormAmount.bind(this),
                    setMultiTransformFormStatus: setMultiTransformFormStatus.bind(this),
                    changeViewType: changeViewType.bind(this),
                    getTransferTableSelectedId: getTransferTableSelectedId.bind(this),
                    getTransferTableMultiSelectedId: getTransferTableMultiSelectedId.bind(this),
                    getTransformFormCompleteStatus: getTransformFormCompleteStatus.bind(this),
                    setTransferListValueByIndex: setTransferListValueByIndex.bind(this),
                    setMultiSelectedValue: setMultiSelectedValue.bind(this),
                    setRowDataByIndex: setRowDataByIndex.bind(this)
                }
            }
        }

        render() {
            return <App {...this.props} {...this.output} />
        }
    }

    return createPage(
        { initTemplate, mutiLangCode, billinfo }
    )(extApp);

}