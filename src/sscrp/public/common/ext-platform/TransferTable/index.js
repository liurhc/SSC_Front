/*
 * Created by wangshhj on 2018/1/16.
 */
import { createTransferTable, createTransferList } from "./create"
import { createMultiTransferTable } from "./multi"
import { 
    getTransferTableValue, 
    setTransferTableValue, 
    setChildTransferTableData, 
    getTransferTableSelectedValue, 
    setTransferListValue,
    getSelectedListDisplay,
    getTransformFormDisplay,
    setTransformFormStatus,
    changeNextTransformForm,
    returnTransferTableList,
    getTransformFormAmount,
    setMultiTransformFormStatus,
    changeViewType,
    getTransferTableSelectedId,
    getTransferTableMultiSelectedId,
    getTransformFormCompleteStatus,
    setTransferListValueByIndex,
    setMultiSelectedValue,
    setRowDataByIndex
} from "./methods"
export {setRowDataByIndex} from './ext-methods';
export { 
    createTransferTable, 
    createTransferList,
    createMultiTransferTable,
    getTransferTableValue, 
    setTransferTableValue, 
    setChildTransferTableData, 
    getTransferTableSelectedValue, 
    setTransferListValue,
    getSelectedListDisplay,
    getTransformFormDisplay,
    setTransformFormStatus,
    changeNextTransformForm,
    returnTransferTableList,
    getTransformFormAmount,
    setMultiTransformFormStatus,
    changeViewType,
    getTransferTableSelectedId,
    getTransferTableMultiSelectedId,
    getTransformFormCompleteStatus,
    setTransferListValueByIndex,
    setMultiSelectedValue
}