export function setRowDataByIndex(HeadTableAreaId, record, rowIndex) {
    let transferTable = this.state.transferTable;
    if (HeadTableAreaId && rowIndex != null && record) {
        if (record.parentRowId) {
            transferTable[HeadTableAreaId][HeadTableAreaId].bodyData.data[record.parentRowId].rows[rowIndex].values = record;

        } else {
            transferTable[HeadTableAreaId][HeadTableAreaId].outerData[rowIndex].values = record;

        }
        this.setState({
            transferTable: transferTable
        });
    } else {
        console.error('setRowDataByIndex方法缺失参数');
    }
    
}