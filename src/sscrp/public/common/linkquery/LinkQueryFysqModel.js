import React, { Component } from 'react';
import { createPage, ajax, base ,getMultiLang} from 'nc-lightapp-front';

const {NCModal} = base;

import linkQueryApi from "../../../../erm/public/components/linkquery/linkquerybills";
/**
 * 联查费用申请单
 */
class LinkQueryFysqModel extends Component {

    constructor(props) {
        super(props);
        this.isLoadTemplate = false;
        this.templateCode = 'fysqBillModel';
        this.state = {
            multiLang : {}
        }
        getMultiLang({moduleId: 1056, domainName: 'sscrp',currentLocale: 'zh-CN', callback: (json) => {
            this.setState({
                multiLang : json
            })
        }})
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.show && !this.isLoadTemplate){
            ajax({
                url: '/nccloud/erm/expenseaccount/ExpenseaccountTempletAction.do',
                data: {
                    pagecode: 'linkfysq_list'
                },
                success: result => {
                    this.props.meta.setMeta(result.data);
                    this.isLoadTemplate = true;
                }
            });
        }
    }

    queryData = () => {
        const {show, tradetype, openBillId, editTable} = this.props;
        if(show) {
            ajax({
                url: '/nccloud/erm/expenseaccount/expenseaccountLinkQueryAction.do',
                data: {
                    queryType: 'apply',
                    tradetype: tradetype,//交易类型
                    openBillId: openBillId//单据主键ok
                },
                success: result => {
                    if(result.data) {
                        editTable.setTableData(this.templateCode, result.data[this.templateCode]);
                    }
                }
            });
        }
    }

    onRowDoubleClick = record => {
        linkQueryApi.link({
            data : {
                billId : record.values.pk_mtapp_bill.value,
                tradetype : record.values.pk_tradetype.value,
                props : this.props
            }
        });
    }

    render() {

        const { show, close } = this.props;
        const { createEditTable } = this.props.editTable;

        return (
            <NCModal show = {show} size="lg" onEnter={this.queryData}>
                <NCModal.Header  closeButton={true} onHide={() =>close()}>
                    {this.state.multiLang['1056common-0028']/*联查费用申请单*/}
                </NCModal.Header>
                <NCModal.Body>
                    {createEditTable(this.templateCode, {
                        showIndex: true,
                        onRowDoubleClick : this.onRowDoubleClick,
                    })}
                </NCModal.Body>
            </NCModal>
        );
    }
}

export default createPage({})(LinkQueryFysqModel);