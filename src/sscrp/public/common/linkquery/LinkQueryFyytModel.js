import React, { Component } from 'react';
import { createPage, ajax, base, getMultiLang } from 'nc-lightapp-front';

const { NCModal } = base;

import linkQueryApi from "../../../../erm/public/components/linkquery/linkquerybills";

/**
 * 联查费用预提单
 */
class LinkQueryFyytModel extends Component {

    constructor(props) {
        super(props);
        this.state = {
            templateCode: false,
            templateCode: 'fyytBillModel',
            multiLang: {}
        }
        getMultiLang({
            moduleId: 1056, domainName: 'sscrp', currentLocale: 'zh-CN', callback: (json) => {
                this.setState({
                    multiLang: json
                })
            }
        })
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.show && !this.state.isLoadTemplate) {
            let props = this.props;
            let that = this;
            props.createUIDom(
                {
                    appcode: '201102BCLF',
                    pagecode: '201102JCLF_LCYT'
                },
                function (data) {
                    props.meta.setMeta(data.template);
                    that.setState({ isLoadTemplate: true });
                    props.table.setTableRender(that.state.templateCode, "billno", (text, record, index) => {
                        return (<a className="hyperlinks"
                            onClick={() => {
                                linkQueryApi.link({
                                    data: {
                                        openbillid: record.pk_accrued_bill.value,
                                        tradetype: record.pk_tradetype.value,
                                        props: props
                                    }
                                });
                            }} > {record.billno.value} </ a>
                        )
                    })

                }
            )
        }
    }

    queryData = () => {
        const { table } = this.props;
        table.setAllTableData(this.state.templateCode, this.props.linkData.data[this.state.templateCode]);
        /* if(show) {
            ajax({
                url: '/nccloud/erm/expenseaccount/expenseaccountLinkQueryAction.do',
                data: {
                    queryType: 'fyyt',
                    tradetype: tradetype,//交易类型
                    openBillId: openBillId//单据主键ok
                },
                success: result => {
                    if(result.data) {
                        editTable.setTableData(this.templateCode, result.data[this.templateCode]);
                    }
                }
            });
        } */
    }

    onRowDoubleClick = record => {
        linkQueryApi.link({
            data: {
                openbillid: record.values.pk_accrued_bill.value,
                tradetype: record.values.pk_tradetype.value,
                props: this.props
            }
        });
    }

    render() {

        const { show, close, table } = this.props;
        const { createSimpleTable } = table;

        return (
            <NCModal show={show} size="lg" onEnter={this.queryData}>
                <NCModal.Header closeButton={true} onHide={() => close()}>
                    {this.state.multiLang['1056common-0029']/*联查费用预提单*/}
                </NCModal.Header>
                <NCModal.Body>
                    {createSimpleTable(this.state.templateCode, {
                        showIndex: true,
                        onRowDoubleClick: this.onRowDoubleClick,
                    })}
                </NCModal.Body>
            </NCModal>
        );
    }
}

export default createPage({})(LinkQueryFyytModel);