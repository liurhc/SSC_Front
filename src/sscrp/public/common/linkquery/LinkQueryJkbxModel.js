import React, { Component } from 'react';
import { createPage, ajax, base,getMultiLang } from 'nc-lightapp-front';

const { NCModal } = base;

import linkQueryApi from "../../../../erm/public/components/linkquery/linkquerybills";
import './index.less';

/**
 * 借款联查报销单或者报销联查借款单
 */
let templateCode = 'jkdBillModel';
let isLoadTemplate = false;
class LinkQueryJkbxModel extends Component {

    constructor(props) {
        super(props);
        // this.isLoadTemplate = false;
        this.templateCode = 'jkdBillModel';
        this.state = {
            multiLang : {}
        }
        getMultiLang({moduleId: 1056, domainName: 'sscrp',currentLocale: 'zh-CN', callback: (json) => {
            this.setState({
                multiLang : json
            })
        }})
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.show && !isLoadTemplate){
            let props = this.props;
            props.createUIDom(
                {
                    appcode: '201102JCLF',
                    pagecode: '201102JCLF_LCBX'
                },
                function (data) {
                    props.meta.setMeta(data.template);
                    isLoadTemplate = true;
                    props.table.setTableRender(templateCode, "djbh", (text, record, index) => {
                        return ( <a className = "hyperlinks"
                                    onClick = {() => {
                                        linkQueryApi.link({
                                            data : {
                                                openbillid : record.pk_jkbx.value,
                                                tradetype : record.djlxbm.value,
                                                props : props
                                            }
                                        });
                                    }} > { record.djbh.value } </ a>
                        )
                    })
                }
            )
        }
    }

    queryData = () => {
        const {table} = this.props;
        table.setAllTableData(this.templateCode, this.props.linkData.data[this.templateCode]);
    //    const {show, tradetype, openBillId, table} = this.props;
    //    if(show) {
    //        ajax({
    //            url: '/nccloud/erm/expenseaccount/expenseaccountLinkQueryAction.do',
    //            data: {
    //                queryType: 'jkbx',
    //                tradetype: tradetype,//交易类型
    //                openBillId: openBillId//单据主键ok
    //            },
    //            success: result => {
    //                if(result.data) {
    //                    table.setAllTableData(this.templateCode, result.data[this.templateCode]);
    //                }
    //            }
    //        });
    //    }
    }

    onRowDoubleClick = record => {
        linkQueryApi.link({
            data : {
                openbillid : record.values.pk_jkbx.value,
                tradetype : record.values.djlxbm.value,
                props : this.props
            }
        });
    }

    render() {

        const { show, close, tradetype, table } = this.props;
        const { createSimpleTable  } = table;

        return (
            <NCModal show = {show} size="xlg" onEnter={this.queryData}>
                <NCModal.Header  closeButton={true} onHide={() =>close()}>
                    {tradetype.indexOf("264") === 0 ? this.state.multiLang['1056common-0030']/*联查借款单*/ : (tradetype.indexOf("261") === 0 ? this.state.multiLang['1056common-0035']/*联查单据*/:this.state.multiLang['1056common-0031']/*联查报销单*/)}
                </NCModal.Header>
                <NCModal.Body>
                    {createSimpleTable (this.templateCode, {
                        showIndex: true,
                        onRowDoubleClick : this.onRowDoubleClick,
                    })}
                </NCModal.Body>
            </NCModal>
        );
    }
}

export default createPage({})(LinkQueryJkbxModel);