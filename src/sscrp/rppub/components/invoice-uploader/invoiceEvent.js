/**
 * 电子发票操作
 */
import { ajax,base ,toast} from 'nc-lightapp-front';
import formDownload from './formDownload';
const { NCMessage } = base;

let done = function(info, response, that){
    let multiLang = that.state.multiLang;
    if(info.file.status === 'done'){
        that.setState({showLoading: false})
        if(response.success) {
            let vRep;
            if(response.data){
                vRep = response.data;
            }else{
                vRep = info.file.response.data;
            }
            if(vRep.ret){
                that.setState({ fileList: vRep.viewList});
                toast({ content: multiLang["1056invoice-0010"], color: 'success' });
            }else{
                toast({ content: multiLang["1056invoice-0011"] + vRep.emsg, color: 'danger' });
            }
        } else {
            toast({ content: multiLang["1056invoice-0011"] + response.error.message, color: 'danger' });
        }
        
        that.setState({showLoading: false})
    }
}

let query = function(billId, that) {
    let multiLang = that.state.multiLang;
    ajax({
        url: '/nccloud/sscrp/invoice/ViewInvoiceAction.do',
        data: { billId },
        success: (data) => {
            if (data.data.emsg){
                NCMessage.create({content: multiLang["1056invoice-0011"] + data.data.retMesg, color: 'error', position: 'bottom'});
            }else {
                that.setState({ fileList: data.data.data });
            }
        }
    })
}

let del = function (text, record, index, that) {
    let multiLang = that.state.multiLang;
    let vdata = {};
    vdata.billId = that.props.billId;
    vdata.pk_org = that.props.pk_org;
    vdata.pk_invoice = record.pk_invoice;
    vdata.invoice_code = record.invoice_code;
    vdata.invoice_number = record.invoice_number;
    vdata.file_name = record.file_name;

    ajax({
        url: '/nccloud/sscrp/invoice/DeleteInvoiceAction.do',
        data: vdata,
        success: (data) => {
            if(data.success) {
                if (data.data.ret){
                    that.setState({ fileList: data.data.viewList});
                    //NCMessage.create({content: multiLang["1056invoice-0010"], color: 'success', position: 'bottom'});
                    toast({ content: multiLang["1056invoice-0010"], color: 'success' });
                }else {
                    toast({ content: multiLang["1056invoice-0011"] + data.data.retMesg, color: 'danger' });
                }
            } else {
                toast({ content: multiLang["1056invoice-0011"] + data.error.message, color: 'danger' });
            }
            

        }
    })
}

let beforeUpload = function() {
    const { billId, billCode, pk_org, tradetype } = this.state;
    if(billId == "" || billCode == "" || pk_org == "" || tradetype == "") {
        NCMessage.create({content: multiLang["1056invoice-0013"], color: 'error', position: 'bottom'});
        return false;
    }
    this.setState({showLoading: true})
}


let beforeDelete = async function (file, fullPath, billId) {
    // let props = this.props;
    // let multiLang = props.MutiInit.getIntl(2011);
    // let vdata= {};
    // vdata.billId = billId;
    // var canDel = true;
    // await ajax({
    //     url: '/nccloud/erm/expenseaccount/ExpenseaccountDzfpBeforeValidateAction.do',
    //     data: vdata,
    //     async:false,
    //     success: (data) => {
    //         if (!data.data.ret){
    //             toast({ content: multiLang && multiLang.get("201102BCLF-0039"), color: 'danger' });
    //             canDel =  false;
    //         }
    //     }
    // })
    // return canDel;
}

let view = function (text, record, index, that) {
    ajax({
        url: '/nccloud/sscrp/invoice/InvoiceOriginUrlAction.do',
        data: {
            billId: record.pk_invoice,
            pk_org: that.props.pk_org,
        },
        success: (res) => {
            if(res.success) {
                window.open(res.data.data);
            } else {
                toast({ content: that.state.multiLang["1056invoice-0011"] + response.error.message, color: 'danger' });
            }
        }
    })
}

export default { view, del, done, beforeDelete, query, beforeUpload }
