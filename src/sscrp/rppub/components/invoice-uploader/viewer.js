import React, { Component } from 'react'
import './viewer.less'

class Viewer extends Component {
    constructor(props) {
        super(props)
    }

    close = () => {
        this.props.closeViewer && this.props.closeViewer()
    }

    render() {

        let { url, show } = this.props

        return (
            show 
            &&
            (
                <section className="lightapp-component-ncviewer">
                    <figure className="ncviewer-image">
                        <img src={url} />
                    </figure>
                    <nav
                        className="ncviewer-close"
                        onClick={(eve) => {
                            this.close()
                        }}
                    >X</nav>
                </section>
            )
        )
    }

}

export default Viewer