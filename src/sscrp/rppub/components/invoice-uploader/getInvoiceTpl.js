/**
 * 电子发票列表模板
 * @type {{}}
 */
import invoiceEvent from './invoiceEvent';

let getInvoiceTpl = function (multiLang) {
    let that = this;
    let dzfpTemplate = {
        tpl:[
            {title: multiLang["1056invoice-0001"], dataIndex: "file_name", key: "file_name", width: 160},
            {title: multiLang["1056invoice-0002"], dataIndex: "invoice_code", key: "invoice_code", width: 100},
            {title: multiLang["1056invoice-0003"], dataIndex: "invoice_number", key: "invoice_number", width: 80},
            {title: multiLang["1056invoice-0004"], dataIndex: "date", key: "date", width: 85},
            // {title: multiLang["1056invoice-0005"], dataIndex: "money_amount", key: "money_amount", width: 60},
            {title: multiLang["1056invoice-0015"], dataIndex: "vat_amount", key: "vat_amount", width: 70},
            {title: multiLang["1056invoice-0006"], dataIndex: "name", key: "name", width: 160},
            {
                title: multiLang["1056invoice-0007"],
                dataIndex: "controler",
                key: "controler",
                width: 80,
                render(text, record, index){
                    return (
                        <div key="controler1">
                        <a className="invoice-link" onClick={() => {
                            invoiceEvent.del(text, record, index, that)
                        }
                    }>{multiLang["1056invoice-0008"]}</ a>
                        <span> | </span>
                        <a className="invoice-link" onClick={() => {
                            invoiceEvent.view(text, record, index, that)
                        }
                    }>{multiLang["1056invoice-0009"]}</ a>
                        </div>
                    )
                }
            }
        ]};
    return dzfpTemplate.tpl;
}


export default getInvoiceTpl;
