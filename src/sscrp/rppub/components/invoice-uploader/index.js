import React, { Component } from 'react'
import { Icon, Upload, Table } from 'tinper-bee'
import Gzip from './gzip'
import Cipher, { opaqueDecrypt, getCookie } from "./cipher";
const Dragger = Upload.Dragger
// 公共基础组件
import { base, getMultiLang } from 'nc-lightapp-front'
import getInvoiceTpl from './getInvoiceTpl'
import  invoiceEvent  from './invoiceEvent'
import './index.less'

const { NCLoading } = base;

// const DEFAULT = {
// 	UP_TEXT: '点击或者将文件拖拽到这里上传',
// 	UP_SIZE: '最大支持',
// 	UP_TYPE: '支持格式: .rar .zip .doc .docx .pdf .jpg .gif .bpm ...'
// }

const defaultProps = {
	billId: '',//单据id
	billCode: '',//单据编码
	pk_org: '',//财务组织
	tradetype: ''//交易类型
}

let cckk = getCookie("cookiets") || Date.now(); //双cookie验证 liuxis
// 做一次 数据转型
cckk = isNaN(cckk) ? cckk : Number(cckk);
// 加密 bbqin
let cipherFlag = localStorage.getItem("rockin");
let aeskey = opaqueDecrypt(localStorage.getItem("cowboy"));
let cckks = cckk + "";
aeskey = cckks + aeskey.substring(0, aeskey.length - cckks.length);
// 将是否加密缓存到组件单例上
Cipher.CipherFlag = cipherFlag === "true";

class InvoiceUploader extends Component {
	constructor(props) {
		super(props)
		this.state = {
			multiLang : {},
			fileList: [],
			show: false,
			showLoading: false
        }
	}

	componentWillReceiveProps = nextProps => {
		
		if(this.props.random == null && nextProps.random != null) {
			getMultiLang({moduleId: 1056, domainName: 'sscrp', currentLocale: 'zh-CN', callback: (json) => {
				this.setState({
					multiLang : json
				}) 
				this.listTemplate = getInvoiceTpl.call(this, json)
			}})
		}
		if(this.props.random !== nextProps.random && nextProps.random != null) {
			this.setState({show: true})
			invoiceEvent.query(nextProps.billId, this)
		}
	}

	uploadChange = (info) => {
		const gziptools = new Gzip();
		let  {response} = info.file;
		if(gziptools.isGzipOpen()) {
			response =
                    typeof response === "string"
                        ? Cipher.decrypt(response, aeskey)
                        : response;
			response  = gziptools.unzip(response);
		}
        invoiceEvent.done(info, response, this)
	}

	render() {
		let {
			billId,//单据id
			billCode,//单据编码
			pk_org,//财务组织
			tradetype,//交易类型
			size,
			accept = ".pdf",
			multiple = false,
			attachMaxSize = 3
		} = this.props

		const up_size = +size >= 0 ? {
			size
		} : {}
		const up_accept = typeof accept === 'string' ? {
			accept
		} : {}

		let attrs = {
			multiple,
			showUploadList: false,
			...up_accept,
			...up_size,
			data: { billId, billCode, pk_org, tradetype },
			beforeUpload: invoiceEvent.beforeUpload.bind(this),
			action: '/nccloud/sscrp/invoice/UploadInvoiceAction.do',
			onChange: this.uploadChange
		}

		return (
			<div id='invoice-uploader' style={this.state.show ? {} : {display: "none"}}>
				<div className="upload-mask"></div>
				<section className={ `lightapp-component-ncuploader ${ this.state.show ? "disabled-bg-btn" : ""}`}>
					<div className='lightapp-component-ncuploader-header'>
						<span>{this.props.title || this.state.multiLang['1056invoice-0014']}</span>
						<div className="upload-close" onClick={() => {
							this.setState({show: false});
						}}>
							<i class="icon iconfont icon-guanbi"></i>
						</div>
					</div>
					<section className="ncuploader-content">
						<hgroup className="ncuploader-content-main">
							<div className="ncuploader-content-uploader">
								<Dragger {...attrs}>
									<p className="u-upload-drag-icon ncuploader-content-icon">
										<Icon type="inbox" className="uf-upload" />
									</p>
									<p className="u-upload-text ncuploader-content-text">{this.state.multiLang['1056common-0018']}</p>
									<p className="u-upload-hint ncuploader-content-hint">{`${this.state.multiLang['1056common-0019']} ${attachMaxSize}MB; ${this.state.multiLang['1056common-0020']}`}</p>
								</Dragger>
							</div>
							<div className="ncuploader-content-lists" >
								<Table
									columns={this.listTemplate}
									data={this.state.fileList}
									emptyText={() => this.state.multiLang['1056invoice-0012']}
								/> 
							</div>
						</hgroup>
					</section>
				</section>
				<NCLoading
                    container={this}
                    show={this.state.showLoading} >
                    {/* 加载中 */}
                </NCLoading>
			</div>
		);
	}
}

// 类的默认值
InvoiceUploader.defaultProps = defaultProps
export default InvoiceUploader