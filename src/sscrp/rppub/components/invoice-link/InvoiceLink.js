import React, {Component} from 'react';
import { createPage, ajax, base,toast,getMultiLang } from 'nc-lightapp-front';
import { initTemplate} from './events';

const {NCModal} = base;

class InvoiceLink extends Component {

    constructor(props) {
        super(props);
        this.state = {
            show: false,
            multiLang : {}
        }
    }

    componentWillReceiveProps = nextProps => {
		if(this.props.viewRandom == null && nextProps.viewRandom != null) {
			getMultiLang({moduleId: 1056, domainName: 'sscrp', currentLocale: 'zh-CN', callback: (json) => {
				this.setState({
					multiLang : json
				}) 
				initTemplate.call(this, nextProps);
			}})
		}
		if(this.props.viewRandom !== nextProps.viewRandom && nextProps.viewRandom != null) {
			this.setState({show: true})
			// invoiceEvent.query(nextProps.billId, this)
		}
	}

    query() {
        ajax({
            url: '/nccloud/sscrp/invoice/InvoiceBillQueryAction.do',
            data: {
                billId: this.props.billId,
            },
            success: (res) => {
                if (res.data != null) {
                    this.props.table.setAllTableData('pvinv_invoicebill', res.data['pvinv_invoicebill']);
                } else {
                    this.props.table.setAllTableData('pvinv_invoicebill', { rows: [] });
                }
            }
        })
    }

    getUrl(pk_invoice) {
        ajax({
            url: '/nccloud/sscrp/invoice/InvoiceOriginUrlAction.do',
            data: {
                billId: pk_invoice,
                pk_org: this.props.pk_org,
            },
            success: (res) => {
                if(res.success) {
                    window.open(res.data.data);
                } else {
                    toast({ content: this.state.multiLang["1056invoice-0011"] + response.error.message, color: 'danger' });
                }
            }
        })
    }

    render() {
        const { createSimpleTable } = this.props.table;
        return (
            <div className="invoice-link">
                <NCModal show = {this.state.show} size="xlg" onEnter={this.query.bind(this)} >
                    <NCModal.Header closeButton={true} onHide={()=> this.setState({ show: false })}>
                        <NCModal.Title>{this.state.multiLang['2011-FP-0001']}</NCModal.Title>
                    </NCModal.Header>
                        {createSimpleTable('pvinv_invoicebill', {
                            showIndex: true,
                            showTotal:true,
                            noTotalCol:['fpdm','fphm','bauthen']
                        })}
                </NCModal>
            </div>
        )
    }
}
let InvoiceLinkModalDom = createPage({
    mutiLangCode: '1056'
})(InvoiceLink);
export default InvoiceLinkModalDom
