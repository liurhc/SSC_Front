import React, {Component} from 'react';
import { createPage, ajax, base,toast,getMultiLang } from 'nc-lightapp-front';
import { initTemplate} from './events';

const {NCModal} = base;

class JointBillModal extends Component {

    constructor(props) {
        super(props);
        initTemplate.call(this, props);
        this.checkModule= false; // 校验是否安装增值税模块标识
        this.state = {
            isShow:false,
            multiLang : {}
        }
        getMultiLang({moduleId: 1056, domainName: 'sscrp',currentLocale: 'zh-CN', callback: (json) => {
            this.setState({
                multiLang : json
            })
        }})
    }

    componentWillReceiveProps(nextProps) {
        let multiLang = this.props.MutiInit.getIntl(1056);
        if (nextProps.show && !this.checkModule) {
            ajax({
                url: '/nccloud/sscrp/rpbill/SSCSysPvinvInitCheckAction.do',
                data: {},
                success: (res) => {
                    console.log('验证是否安装', res);
                    this.checkModule = true;
                    if (res.data) {
                        this.setState({ isShow: true });
                    } else {
                        this.setState({ isShow: false });
                        toast({ content: multiLang && multiLang.get('2011-FP-0002'), color: 'danger' });
                    }
                }
            });
        }
    }


    query() {
        if (this.props.show) {
            ajax({
                url: '/nccloud/sscrp/invoice/InvoiceBillQueryAction.do',
                data: {
                    openBillId:this.props.billCode,
                },
                success: (res) => {
                    console.log('res', res);
                    if (res.data != null) {
                        this.props.editTable.setTableData('pvinv_invoicebill', res.data['pvinv_invoicebill']);
                    } else {
                        this.props.editTable.setTableData('pvinv_invoicebill', { rows: [] });
                    }
                }
            })
        }
    }

    render() {
        let {show} = this.props;
        const { createEditTable } = this.props.editTable;
        return (
            <div>
                <NCModal show = {this.state.isShow ? show:this.state.isShow} size="xlg" onEnter={this.query.bind(this)} >
                    <NCModal.Header closeButton={true} onHide={(e)=>this.props.close()}>
                        <NCModal.Title>{this.state.multiLang['2011-FP-0001']}</NCModal.Title>
                    </NCModal.Header>
                        {createEditTable('pvinv_invoicebill', {
                            showIndex: true,
                            showTotal:true,
                            noTotalCol:['fpdm','fphm','bauthen']
                        })}
                </NCModal>
            </div>
        )
    }
}
let JointBillModalDom = createPage({
    mutiLangCode: '1056'
})(JointBillModal);
export default JointBillModalDom
