import { high } from 'nc-lightapp-front';

//打印模板参照
//过滤需要appcode
const { Refer } = high;
export default  function (props = {}) {
    delete props.foolValue;//去掉组件内部的值
    const foolValue = props.val;
    var conf = {
        multiLang:{
			domainName:'gl',
			currentLocale: 'simpchn', 
			moduleId: 'glrefer'
		},
        refType: 'grid',
        refName: 'glrefer-000030',/* 国际化处理： 打印模板*/
        refCode: 'gl.refer.voucher.PrintTemplateGridRef',
        queryGridUrl: '/nccloud/gl/glpub/queryprinttemp.do',
        isMultiSelectedEnabled: false,
        foolValue,
        columnConfig: [
            {
                name: [ 'glrefer-000031'],/* 国际化处理： 模板*/
                code: [ 'refname']
            }
        ]
    };

    return <Refer {...Object.assign(conf, props) } />
}
