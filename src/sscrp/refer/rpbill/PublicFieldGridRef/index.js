import { high } from 'nc-lightapp-front';

const { Refer } = high;

export default function (props = {}) {
    var conf = {
        multiLang: {
            moduleId: '1056',
            domainName: 'sscrp',
            currentLocale: 'simpchn'
        },
        refType: 'grid',
        refName: 'refer-publicfield-0001',
        refCode: 'sscrp.refer.rpbill.PublicFieldGridRef',
        queryGridUrl: '/nccloud/sscrp/rpbill/PublicFieldGridRefAction.do',
        isMultiSelectedEnabled: false,
        columnConfig: [{name: ['refer-0001','refer-0002'],code:['refcode','refname']}]
    };

    return <Refer {...Object.assign(conf,props)} />
}