!
function(e, r) {
    "object" == typeof exports && "object" == typeof module ? module.exports = r(require("nc-lightapp-front")) : "function" == typeof define && define.amd ? define(["nc-lightapp-front"], r) : "object" == typeof exports ? exports["sscrp/refer/rpbill/AllUserGridRef/index"] = r(require("nc-lightapp-front")) : e["sscrp/refer/rpbill/AllUserGridRef/index"] = r(e["nc-lightapp-front"])
} (window,
function(e) {
    return function(e) {
        var r = {};
        function t(n) {
            if (r[n]) return r[n].exports;
            var o = r[n] = {
                i: n,
                l: !1,
                exports: {}
            };
            return e[n].call(o.exports, o, o.exports, t),
            o.l = !0,
            o.exports
        }
        return t.m = e,
        t.c = r,
        t.d = function(e, r, n) {
            t.o(e, r) || Object.defineProperty(e, r, {
                configurable: !1,
                enumerable: !0,
                get: n
            })
        },
        t.r = function(e) {
            Object.defineProperty(e, "__esModule", {
                value: !0
            })
        },
        t.n = function(e) {
            var r = e && e.__esModule ?
            function() {
                return e.
            default
            }:
            function() {
                return e
            };
            return t.d(r, "a", r),
            r
        },
        t.o = function(e, r) {
            return Object.prototype.hasOwnProperty.call(e, r)
        },
        t.p = "../../../../",
        t(t.s = 1063)
    } ({
        1 : function(r, t) {
            r.exports = e
        },
        1063 : function(e, r, t) {
            e.exports = t(460)
        },
        460 : function(e, r, t) {
            "use strict";
            Object.defineProperty(r, "__esModule", {
                value: !0
            }),
            r.
        default = function() {
                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                return React.createElement(n, Object.assign({
                    multiLang: {
                        moduleId: '1056',
                        domainName: 'sscrp',
                        currentLocale: 'simpchn'
                    },
                    refType: "grid",
                    refName: "refer-alluser-0001",
                    placeholder: "refer-alluser-0001",
                    refCode: "sscrp.refer.rpbill.AllUserGridRef",
                    queryGridUrl: "/nccloud/sscrp/rpbill/AllUserRefGridAction.do",
                    isMultiSelectedEnabled: !1,
                    columnConfig: [{
                        name: ["refer-0001", "refer-0002", "refer-alluser-0002", "refer-alluser-0003"],
                        code: ["refcode", "refname", "org_name", "dept_name"]
                    }]
                },
                e))
            };
            var n = t(1).high.Refer
        }
    })
});