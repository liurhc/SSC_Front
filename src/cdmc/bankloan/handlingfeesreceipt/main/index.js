import { RenderRouter, asyncComponent } from 'nc-lightapp-front';

// 手续费回单还款-列表
const list = asyncComponent(() => import(/*webpackChunkName:"/cdmc/bankloan/handlingfeesreceipt/list/list"*/  /* webpackMode: "eager" */ '../list'));
// 手续费回单还款-卡片
const card = asyncComponent(() => import(/*webpackChunkName:"/cdmc/bankloan/handlingfeesreceipt/card/card"*/  /* webpackMode: "eager" */ '../card'));

const routes = [
    {
		path: '/',
		component: list,
		exact: true
	},
	{
		path: '/list',
		component: list
	},
	{
		path: '/card',
		component: card
	}
];

(function main(routers, htmlTagid) {
    RenderRouter(routers, htmlTagid);
})(routes, "app");