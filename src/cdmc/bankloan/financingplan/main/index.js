import { RenderRouter, asyncComponent } from 'nc-lightapp-front';

// 银行贷款合同还款-列表
const list = asyncComponent(() => import(/*webpackChunkName:"/cdmc/bankloan/financingplan/list/list"*/  /* webpackMode: "eager" */ '../list'));
// 银行贷款合同还款-卡片
const card = asyncComponent(() => import(/*webpackChunkName:"/cdmc/bankloan/financingplan/card/card"*/  /* webpackMode: "eager" */ '../card'));

const routes = [
    {
		path: '/',
		component: list,
		exact: true
	},
	{
		path: '/list',
		component: list,
	},
	{
		path: '/card',
		component: card
	}
];

(function main(routers, htmlTagid) {
    RenderRouter(routers, htmlTagid);
})(routes, "app");