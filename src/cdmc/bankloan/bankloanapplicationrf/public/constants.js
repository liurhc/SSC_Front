/**
 * 银行贷款申请评审单节点-常量定义
 * @date 2019-12-20
 */

export const appcode = '3663041BLARF'; // 应用编码

// 列表页面常量

export const listPagecode = '3663041BLARF_LIST'; // 页面编码

export const listQueryarea = 'searcharea'; // 查询区编码

export const listTablearea = 'tablearea'; // 表格区编码
//卡片页面常量

export const cardPagecode = '3663041BLARF_CARD'; // 页面编码

export const cardHeadarea = 'head'; // 表头

export const cardBodyarea1 = 'table1'; // 表体

export const cardBodyarea2 = 'table2'; // 表体

export const cardBodyarea3 = 'table3'; // 表体





