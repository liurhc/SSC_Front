import { cardPagecode } from '../../public/constants';

/** 卡片页面模板初始化 */
export function initTemplate(props,callBack){
    props.createUIDom(
        { pagecode: cardPagecode },
        (data)=>{
            let meta = data.template;
            props.meta.setMeta(meta, callBack);
            data.button && props.button.setButtons(data.button,()=>{
                // 此处可以设置按钮初始状态
            });
        }
    );
}