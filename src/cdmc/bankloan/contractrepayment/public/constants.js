/**
 * 银行贷款合同还款节点-常量定义
 * @date 2019-12-17
 */

export const appcode = '36630BLCR'; // 应用编码

// 列表页面常量

export const listPagecode = '36630BLCR_LIST'; // 页面编码

export const listQueryarea = 'searcharea'; // 查询区编码

export const listTablearea = 'tablearea'; // 表格区编码


// 卡片页面常量

export const cardPagecode = '36630BLCR_CARD'; // 页面编码

export const cardHeadarea = 'head'; // 银行贷款合同还款表头

export const cardBodyarea1 = 'table1'; // 银行贷款合同还款子表

export const cardBodyarea2 = 'table2'; // 担保信息区域

export const cardBodyarea3 = 'table3'; // 保函信息区域


