import { ajax, toast } from 'nc-lightapp-front';
import { cardHeadarea, cardBodyarea1, cardBodyarea2, cardBodyarea3,cardBodyarea4,cardBodyarea5,cardBodyarea6} from '../../public/constants';

/** 初始化卡片页面数据 */
export function initCardData(props){
    let billid = props.getUrlParam('billid');
    debugger;
    ajax({
        url:'/nccloud/cdmc/contractre/ContractreCardQuertAction.do',
        data:{ billid:billid },
        success:(result)=>{
            let data = result.data; 

            // 表头数据赋值
            if(data.head && data.head.head){
                props.form.setFormItemsValue(cardHeadarea, data.head.head.rows[0].values || {});
            } else {
                toast({color:'danger',content:'单据已被删除！请刷新页面！'});
            }

            // 放款计划
            if(data.head && data.table1){
                props.cardTable.setTableData(cardBodyarea1, data.table1.table1 || {rows:[]});
            }

            // 还款计划
            if(data.head && data.table2){
                props.cardTable.setTableData(cardBodyarea2, data.table2.table2 || {rows:[]});
            }

            // 合同执行情况
            if(data.head && data.table3){
                props.cardTable.setTableData(cardBodyarea3, data.table3.table3 || {rows:[]});
            }
            // 担保信息
            if(data.head && data.table4){
                props.cardTable.setTableData(cardBodyarea4, data.table4.table4 || {rows:[]});
            }
            // 销售进度还本
            if(data.head && data.table5){
                props.cardTable.setTableData(cardBodyarea5, data.table5.table5 || {rows:[]});
            }
            // 保函信息
            if(data.head && data.table6){
                props.cardTable.setTableData(cardBodyarea6, data.table6.table6 || {rows:[]});
            }
        },
        error:(errorMsg)=>{
            toast({color:'danger',content:'查询单据失败：'+ errorMsg});
        }
    });
}