import React,{ Component } from 'react';
import { base, createPage, high, ajax, toast, createPageIcon } from 'nc-lightapp-front';
import { listQueryarea, listTablearea } from '../public/constants';
import { searchEvent } from './events/searchEvent';
import { initTemplate } from './events/initTemplate';
import { onRowDoubleClick } from './events/onRowDoubleClick';
import { pageInfoClick } from './events/pageInfoClick';
/**
 * 开函登记-列表
 * @date 2019-12-17
 */
class List extends Component{
    constructor(props){
        super(props);
        this.state = {
            
        };
        initTemplate(this.props);
    }

    render(){

        let { table, search } = this.props;
        let { createSimpleTable } = table;
        let { NCCreateSearch } = search;

        return(
            <div className="nc-bill-list">

                {/* 头部区域 */}
                <div className="nc-bill-header-area">
                    <div className="header-title-search-area">
                        {createPageIcon()} {/* 单据标题 */}
                        <h2 className="title-search-detail">票据付款</h2>
                    </div>
                </div>

                {/* 查询区 */}
                <div className="nc-bill-search-area">
                    {NCCreateSearch(listQueryarea,{
                        clickSearchBtn: searchEvent.bind(this), // 查询事件
                        showAdvBtn: true // 是否显示高级按钮
                    })}
                </div>

                {/* 表格区域 */}
                <div className="nc-bill-table-area">
                    {createSimpleTable(listTablearea,{
                        showIndex: true,
                        onRowDoubleClick: onRowDoubleClick.bind(this), // 双击事件
                        handlePageInfoChange: pageInfoClick.bind(this) // 分页组件点击事件
                    })}
                </div>

            </div>
        ); 
    }
}

List = createPage({})(List);
export default List;