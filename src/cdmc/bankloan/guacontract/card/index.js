import React,{ Component } from 'react';
import { base, createPage, high, ajax, toast, createPageIcon } from 'nc-lightapp-front';
import { cardPagecode, cardHeadarea, cardBodyarea1, cardBodyarea2, cardBodyarea3,cardBodyarea4,cardBodyarea5,cardBodyarea6 } from '../public/constants';
import { initTemplate } from './events/initTemplate';
import { backList } from './events/backList';
import { initCardData } from './events/initCardData';

const { NCBackBtn } = base;

/** 
 * 银行贷款合同-卡片
 * @date 2019-12-17
 */
class Card extends Component {
    constructor(props){
        super(props);
        this.state = {

        };
        initTemplate(this.props);
    }

    componentDidMount(){
        // 初始化卡片数据
        initCardData(this.props);
    }

    render(){

        let { cardTable,form } = this.props;
        let { createCardTable } = cardTable;
        let { createForm } = form;

        return(
            <div className="nc-bill-card">
                <div className="nc-bill-top-area">

                     {/* 头部区域 */}
                    <div className="nc-bill-header-area">
                        <div><NCBackBtn onClick={backList.bind(this)}/></div>
                        <div className="header-title-search-area">
                            {createPageIcon()} { /* 单据标题图标 */ }
                            <h2 className="title-search-detail">担保合同</h2>
                        </div>
                    </div>
                    {/* 单据表头区域 */}
                    <div className="nc-bill-form-area">
                        {createForm(cardHeadarea,{

                        })}
                    </div>

                </div>

                {/* 抵押信息 */}
                <div className="nc-bill-table-area">
                    {createCardTable(cardBodyarea1,{

                    })}
                </div>

                {/* 质押信息 */}
                <div className="nc-bill-table-area">
                    {createCardTable(cardBodyarea2,{

                    })}
                </div>

                {/* 保证信息 */}
                <div className="nc-bill-table-area">
                    {createCardTable(cardBodyarea3,{

                    })}
                </div>

            </div>
        );
    }
}

Card = createPage({})(Card);
export default Card;