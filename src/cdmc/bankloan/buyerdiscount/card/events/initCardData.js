import { ajax, toast } from 'nc-lightapp-front';
import { cardHeadarea, cardBodyarea1, cardBodyarea2, cardBodyarea3,cardBodyarea4,cardBodyarea5,cardBodyarea6} from '../../public/constants';

/** 初始化卡片页面数据 */
export function initCardData(props){
    let billid = props.getUrlParam('billid');
    ajax({
        url:'/nccloud/cdmc/buyerdiscount/BuyerDiscountCardQueryAction.do',
        data:{ billid:billid },
        success:(result)=>{
            let data = result.data; 

            // 表头数据赋值
            if(data.head && data.head.head){
                props.form.setFormItemsValue(cardHeadarea, data.head.head.rows[0].values || {});
            } else {
                toast({color:'danger',content:'单据已被删除！请刷新页面！'});
            }

            // 表体数据赋值
            // if(data.head && data.table1){
            //     props.cardTable.setTableData(cardBodyarea1, data.table1.table1 || {rows:[]});
            // }
        },
        error:(errorMsg)=>{
            toast({color:'danger',content:'查询单据失败：'+ errorMsg});
        }
    });
}