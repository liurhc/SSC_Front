
/** 列表页面表格行双击事件 */
export function onRowDoubleClick(record, index, e){

    let billid = record.pk_accept.value; // 单据主键
    this.props.pushTo('/card',{
        billid: billid, 
        status: 'browse'
    });
}