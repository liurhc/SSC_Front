/**
 * 银行贷款合同节点-常量定义
 * @date 2019-12-17
 */

export const appcode = '366304APAC'; // 应用编码

// 列表页面常量

export const listPagecode = '366304AC_LIST'; // 页面编码

export const listQueryarea = 'searcharea'; // 查询区编码

export const listTablearea = 'tablearea'; // 表格区编码


//卡片页面常量

export const cardPagecode = '366304APAC_CARD'; // 页面编码

export const cardHeadarea = 'head'; // 表头


