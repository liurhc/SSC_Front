import { ajax, toast } from 'nc-lightapp-front';
import { listQueryarea, listTablearea } from '../../public/constants';

/**
 *  列表页面查询按钮事件
 */
export function searchEvent(props,searchVal){
    if(searchVal){
        let pageInfo = props.table.getTablePageInfo(listTablearea);
        let queryInfo = props.search.getQueryInfo(listQueryarea);
        queryInfo.pageInfo = pageInfo;
        ajax({
            url:'/nccloud/cdmc/buyerdiscount/BuyerDiscountListQueryAction.do',
            data:queryInfo, 
            success:(result)=>{
                if(result.data && result.data.grid && result.data.grid.tablearea){
                    props.table.setAllTableData(listTablearea, result.data.grid.tablearea);
                } else{
                    props.table.setAllTableData(listTablearea, {rows:[]});
                }
            },
            error:(errorMsg)=>{
                alert(errorMsg);
            }
        });
    }
}