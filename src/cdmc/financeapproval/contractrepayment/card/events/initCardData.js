import { ajax, toast } from 'nc-lightapp-front';
import { cardHeadarea, cardBodyarea1, cardBodyarea2, cardBodyarea3} from '../../public/constants';

/** 初始化卡片页面数据 */
export function initCardData(props){
    let billid = props.getUrlParam('id');
    ajax({
        url:'/nccloud/cdmc/contractrepayment/ContractRepaymentApprovalCardQueryAction.do',
        data:{ billid:billid },
        success:(result)=>{
            let data = result.data;

            // 表头数据赋值
            if(data.billcard && data.billcard.head){
                props.form.setFormItemsValue(cardHeadarea, data.billcard.head.head.rows[0].values || {});
            } else {
                toast({color:'danger',content:'单据已被删除！请刷新页面！'});
            }

            // 银行贷款合同还款子表数据
            if(data.billcard && data.billcard.body){
                props.cardTable.setTableData(cardBodyarea1, data.billcard.body.table1 || {rows:[]});
            }

            // 担保数据赋值
            if(data.billcard && data.table2){
                props.cardTable.setTableData(cardBodyarea2, data.table2.table2 || {rows:[]});
            }

            // 保函数据赋值
            if(data.billcard && data.table3){
                props.cardTable.setTableData(cardBodyarea3, data.table3.table3 || {rows:[]});
            }
        },
        error:(errorMsg)=>{
            toast({color:'danger',content:'查询单据失败：'+ errorMsg});
        }
    });
}