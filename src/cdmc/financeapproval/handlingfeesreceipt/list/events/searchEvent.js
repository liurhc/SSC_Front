import { ajax, toast } from 'nc-lightapp-front';
import { listQueryarea, listTablearea } from '../../public/constants';

/**
 *  列表页面查询按钮事件
 */
export function searchEvent(props,searchVal){
    if(searchVal){
        let pageInfo = props.table.getTablePageInfo(listTablearea);
        let queryInfo = props.search.getQueryInfo(listQueryarea);
        queryInfo.pageInfo = pageInfo;
        ajax({
            url:'/nccloud/cdmc/handlingfeesreceipt/listquery.do',
            data:queryInfo,
            success:(result)=>{
                if(result.data && result.data.grid && result.data.grid.tablearea){
                    props.table.setAllTableData(listTablearea, result.data.grid.tablearea);
                } else{
                    toast({color:'warning', content:'未查询到符合条件的数据！' });
                    props.table.setAllTableData(listTablearea, {rows:[]});
                }
            },
            error:(errorMsg)=>{
                toast({color:'danger', content:'查询单据失败：' + errorMsg });
            }
        });
    }
}