import { RenderRouter, asyncComponent } from 'nc-lightapp-front';

// 银行贷款合同还款-卡片
const card = asyncComponent(() => import(/*webpackChunkName:"/cdmc/financeapproval/contractre/card/card"*/  /* webpackMode: "eager" */ '../card'));

const routes = [
	{
		path: '/card',
		component: card
	}
];

(function main(routers, htmlTagid) {
    RenderRouter(routers, htmlTagid);
})(routes, "app");