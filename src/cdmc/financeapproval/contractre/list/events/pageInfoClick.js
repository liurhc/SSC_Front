import { ajax, toast } from 'nc-lightapp-front';
import { listQueryarea, listTablearea } from '../../public/constants';

/** 分页组件点击事件 */
export function pageInfoClick(props, config, pks){

    ajax({
        url:'/nccloud/cdmc/contractre/ContractreQueryByPksAction.do',
        data:{ pks:pks }, 
        success:(result)=>{
            if(result.data && result.data.grid && result.data.grid.tablearea){
                props.table.setAllTableData(listTablearea, result.data.grid.tablearea);
            } else{
                toast({color:'warning', content:'未查询到符合条件的数据！' });
                props.table.setAllTableData(listTablearea, {rows:[]});
            }
        },
        error:(errorMsg)=>{
            toast({color:'danger', content:'查询单据失败：' + errorMsg });
        }
    });
}