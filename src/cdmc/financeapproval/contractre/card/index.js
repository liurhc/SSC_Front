import React,{ Component } from 'react';
import { base, createPage, high, ajax, toast, createPageIcon } from 'nc-lightapp-front';
const {NCUploader}=high;
import { cardPagecode, cardHeadarea, cardBodyarea1, cardBodyarea2, cardBodyarea3,cardBodyarea4,cardBodyarea5,cardBodyarea6 } from '../public/constants';
import { initTemplate } from './events/initTemplate';
import { backList } from './events/backList';
import { initCardData } from './events/initCardData';
import  './index.less';

const { NCBackBtn } = base;

/** 
 * 银行贷款合同-卡片
 * @date 2019-12-17
 */
class Card extends Component {
    constructor(props){
        super(props);
        this.state = {
            showUploader: false, //附件模态框
			target: null,
        };
        initTemplate(this.props);
    }

    componentDidMount(){
        // 初始化卡片数据
        debugger;
        initCardData(this.props);
    }
    onHideUploader = () => {
		this.setState({
			showUploader: false
		});
	};
    onButtonClick=(props,id,record)=>{
        debugger
        if(id==='AttachManage'){
            let flag = props.getUrlParam('status');
			// if (
			// 	flag == 'add' ||
			// 	props.getUrlParam('copyFlag') == 'copy' ||
			// 	props.getUrlParam('writebackFlag') == 'redBack'
			// ) 
			this.setState({
				showUploader: true,
				target: null
			});
        }
    }
    
    render(){

        let { cardTable,form,button } = this.props;
        const {createButtonApp}=button;
        let { createCardTable } = cardTable;
        let { createForm } = form;
        let { showUploader, target } = this.state;

        return(
            <div className="nc-bill-card">
                <div className="nc-bill-top-area">

                     {/* 头部区域 */}
                    <div className="nc-bill-header-area">
                        {/* <div className='icon'><NCBackBtn onClick={backList.bind(this)}/></div> */}
                        
                        <div className="header-title-search-area">
                            {createPageIcon()} { /* 单据标题图标 */ }
                            <h2 className="title-search-detail">银行贷款合同</h2>
                        </div>
                        {createButtonApp({
                    area: 'card_head',
                    buttonLimit: 2, 
                    onButtonClick: this.onButtonClick.bind(this),
                    popContainer: document.querySelector('.nc-bill-header-area')
                         })}
                    </div>
                    {/* 单据表头区域 */}
                    <div className="nc-bill-form-area">
                        {createForm(cardHeadarea,{

                        })}
                    </div>

                </div>

                {/* 放款计划 */}
                <div className="nc-bill-table-area">
                    {createCardTable(cardBodyarea1,{

                    })}
                </div>

                {/* 还款计划 */}
                <div className="nc-bill-table-area">
                    {createCardTable(cardBodyarea2,{

                    })}
                </div>

                {/* 合同执行情况 */}
                <div className="nc-bill-table-area">
                    {createCardTable(cardBodyarea3,{

                    })}
                </div>
                 {/* 担保信息 */}
                 <div className="nc-bill-table-area">
                    {createCardTable(cardBodyarea4,{

                    })}
                </div>
                 {/* 销售进度还本 */}
                 <div className="nc-bill-table-area">
                    {createCardTable(cardBodyarea5,{

                    })}
                </div>
                {/* 保函信息 */}
                <div className="nc-bill-table-area">
                    {createCardTable(cardBodyarea6,{

                    })}
                </div>
                {/* {附件管理} */}
							{showUploader && (
								<NCUploader
									billId={
										this.props.form.getFormItemsValue('head', 'pk_contract') ? (
											this.props.form.getFormItemsValue('head', 'pk_contract').value
										) : null
									}
									billNo={
										this.props.form.getFormItemsValue('head', 'contractcode') ? (
											this.props.form.getFormItemsValue('head', 'contractcode').value
										) : null
									}
									target={target}
									placement={'bottom'}
									beforeUpload={this.beforeUpload}
									onHide={this.onHideUploader}
									close={() => this.closeModal()}
								/>
							)}
            </div>
        );
    }
}

Card = createPage({})(Card);
export default Card;