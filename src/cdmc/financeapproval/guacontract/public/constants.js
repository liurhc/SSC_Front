/**
 * 银行贷款合同节点-常量定义
 * @date 2019-12-17
 */

export const appcode = '366304APGC'; // 应用编码

// 列表页面常量

export const listPagecode = '366304GC_LIST'; // 页面编码

export const listQueryarea = 'searcharea'; // 查询区编码

export const listTablearea = 'tablearea'; // 表格区编码


//卡片页面常量

export const cardPagecode = '366304APGC_CARD'; // 页面编码

export const cardHeadarea = 'head'; // 表头

export const cardBodyarea1 = 'table1'; // 放款计划

export const cardBodyarea2 = 'table2'; // 还款计划 

export const cardBodyarea3 = 'table3'; // 合同执行情况


