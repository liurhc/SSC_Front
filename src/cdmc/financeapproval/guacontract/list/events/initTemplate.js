import { listPagecode, listTablearea } from '../../public/constants';

/** 列表页面模板初始化 */
export function initTemplate(props,callBack){
    props.createUIDom(
        { pagecode: listPagecode },
        (data)=>{ 
            let meta = data.template;

            meta[listTablearea].pagination = true; // 分页

            props.meta.setMeta(meta, callBack);
            data.button && props.button.setButtons(data.button,()=>{
                // 此处可以设置按钮初始状态
            });
        }
    );
}