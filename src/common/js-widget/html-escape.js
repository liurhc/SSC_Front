function htmlEscape(str) {
    return String(str)
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;');
}
export default htmlEscape;