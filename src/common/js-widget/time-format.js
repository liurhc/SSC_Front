// 对Date的扩展，将 Date 转化为指定格式的String
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
// 例子：
// timeFormat(new Date(), "yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
// timeFormat(new Date(), "yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18
function timeFormat(dateObj, fmt) {
    let o = {
        "M+": dateObj.getMonth() + 1,                 //月份
        "d+": dateObj.getDate(),                    //日
        "h+": dateObj.getHours(),                   //小时
        "m+": dateObj.getMinutes(),                 //分
        "s+": dateObj.getSeconds(),                 //秒
        "q+": Math.floor((dateObj.getMonth() + 3) / 3), //季度
        "S": dateObj.getMilliseconds(),             //毫秒
        "W": getWeek(dateObj)                       //周
    };
    if (/(y+)/.test(fmt))
        fmt = fmt.replace(RegExp.$1, (dateObj.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (let k in o)
        if (new RegExp("(" + k + ")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

function getWeek(dateObj) {
    var weeks = new Array("日", "一", "二", "三", "四", "五", "六");
    return '周' + weeks[dateObj.getDay()];
}

let oneDayTime = 24 * 3600 * 1000;
let oneWeekTime = 7 * oneDayTime;
let oneMonthTime = 30*oneDayTime;
let oneYearTime = 365*oneDayTime;


function timeAgo(dateObj) {
    let time = dateObj.getTime();
    let currDateObj = new Date();
    let currTime = currDateObj.getTime();
    currDateObj.setHours(0);
    currDateObj.setMinutes(0);
    currDateObj.setSeconds(0);
    currDateObj.setMilliseconds(0);
    let currDateZeroTime = currDateObj.getTime();
    let currDateRemainTime = currTime - currDateZeroTime;
    let difTime = currTime - time;
    //下面是短路判断。
    if (difTime < currDateRemainTime) { //时间差小于当天剩余的时间
        return timeFormat(dateObj, 'hh:mm');
    }
    if (difTime < (oneDayTime + currDateRemainTime)) { //时间差大于当天剩余时间并且小于（当天剩余的时间+一天时间）
        return '昨天 ' + timeFormat(dateObj, 'hh:mm');
    }
    if (difTime < oneWeekTime) {
        return timeFormat(dateObj, 'W hh:mm');
    }
    if (difTime < oneYearTime) {
        return timeFormat(dateObj, 'MM-dd hh:mm')
    }
    return timeFormat(dateObj, 'yyyy-MM-dd hh:mm')
    /*if (difTime < oneMonthTime) { //时间差大于上面时间 并且 小于一个月时间
     return parseInt(difTime/oneDayTime) + '天前'
     }
     if (difTime < oneYearTime) {
     return parseInt(difTime/oneMonthTime) + '月前'
     }
     return parseInt(difTime/oneYearTime) + '年前';*/
}

export {timeFormat, timeAgo};