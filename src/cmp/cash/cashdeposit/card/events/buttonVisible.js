import {
    cardCache
} from 'nc-lightapp-front';
let {
    getDefData
} = cardCache;
import {
    constant
} from '../../config/config';

export const buttonVisible = function (props, billstatus) {

    let status = props.getUrlParam('status');
    // scene:'linksce'
    let scene = props.getUrlParam('scene');
    let isfiplink = getDefData(constant.fipscene_key, constant.cacheDataSource);
    // let isfiplink = getDefData(constant.fipscene_key, this.cacheDataSource);
    let statusflag = status === 'browse' ? false : true;
    let btnflag = false;
    // 按钮默认隐藏
    props.button.setButtonVisible(
        [
            'addgroup',
            'addBtn',
            'editBtn',
            'deleteBtn',
            'copyBtn',
            'savegroup',
            'saveBtn',
            'saveaddBtn',
            'savesubmitBtn',
            'submitBtn',
            'unsubmitBtn',
            'settleBtn',
            'unsettleBtn',
            'cancelBtn',
            'makebillBtn',
            'imgBtn',
            'imggroup',
            'imgreviewBtn',
            'imgscanBtn',
            'joinquery',
            'joinquerygroup',
            'cashbalanceBtn',
            'bankaccbalanceBtn',
            'approveopinionBtn',
            'voucherBtn',
            'printBtn',
            'printgroup',
            'outputBtn',
            'enclosureBtn',
            'refreshBtn'
        ],
        btnflag
    );
    //新增、编辑、复制状态
    if (statusflag) {
        // 显示按钮：保存、保存新增、保存提交、取消
        props.button.setButtonVisible(
            [
                'savegroup',
                'saveBtn',
                'saveaddBtn',
                'savesubmitBtn',
                'cancelBtn'
            ], !btnflag
        );
    }
    //浏览
    if (props.getUrlParam('status') === 'browse') {

        if (isfiplink) {
            props.button.setButtonVisible(
                [
                    'enclosureBtn',
                    'joinquery',
                    'joinquerygroup',
                    'cashbalanceBtn',
                    'bankaccbalanceBtn',
                    'approveopinionBtn',
                    'voucherBtn',
                    'printBtn',
                    'printgroup',
                    'outputBtn'
                ], !btnflag);
        } else {
            if (scene) {
                props.button.setButtonVisible(
                    [
                        'enclosureBtn',
                        'joinquery',
                        'joinquerygroup',
                        'cashbalanceBtn',
                        'bankaccbalanceBtn',
                        'approveopinionBtn',
                        'voucherBtn',
                        'printBtn',
                        'printgroup',
                        'outputBtn'
                    ], !btnflag);
            } else {

                // 公共显示按钮
                props.button.setButtonVisible(
                    [
                        'addgroup',
                        'addBtn',
                        'copyBtn',
                        'imgBtn',
                        'imggroup',
                        'imgreviewBtn',
                        'imgscanBtn',
                        'joinquery',
                        'joinquerygroup',
                        'cashbalanceBtn',
                        'bankaccbalanceBtn',
                        'printBtn',
                        'printgroup',
                        'outputBtn',
                        'enclosureBtn',
                        'refreshBtn'
                    ], !btnflag
                );

                switch (billstatus) {
                    case '1':
                        props.button.setButtonVisible(
                            [
                                'editBtn',
                                'deleteBtn',
                                'submitBtn',
                            ], !btnflag
                        );

                        break;
                    case '2':
                        props.button.setButtonVisible(
                            [
                                'unsubmitBtn',
                                'approveopinionBtn',
                            ], !btnflag);

                        break;
                    case '3':
                        props.button.setButtonVisible(
                            [
                                'unsubmitBtn',
                                'settleBtn',
                                'approveopinionBtn',
                            ], !btnflag);

                        break;
                    case '4':
                        props.button.setButtonVisible(
                            [
                                'unsettleBtn',
                                'makebillBtn',
                                'approveopinionBtn',
                                'voucherBtn',
                            ], !btnflag
                        );
                        break;
                    default:
                        // props.button.setButtonVisible(
                        //     [
                        //         'addgroup',
                        //         'addBtn'
                        //     ], !btnflag
                        // );
                        // 公共显示按钮
                        props.button.setButtonVisible(
                            [
                                'copyBtn',
                                'imgBtn',
                                'imggroup',
                                'imgreviewBtn',
                                'imgscanBtn',
                                'joinquery',
                                'joinquerygroup',
                                'cashbalanceBtn',
                                'bankaccbalanceBtn',
                                'printBtn',
                                'printgroup',
                                'outputBtn',
                                'enclosureBtn',
                                'refreshBtn'
                            ], btnflag
                        );
                        break;
                }
            }
        }
    }
    if (status === 'copy') {
        props.form.setFormStatus(constant.formcode1, 'edit');
    } else {
        props.form.setFormStatus(constant.formcode1, status);
    }
    props.form.setFormStatus(constant.formcode3, "browse");
}