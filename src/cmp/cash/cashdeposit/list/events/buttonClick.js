import {
	toast,
	print,
	getBusinessInfo,
	promptBox,
	output
} from 'nc-lightapp-front';
import {
	constant,
	requesturl
} from '../../config/config';
import {
	commondata
} from '../../../../public/utils/constant';

import { imageScan, imageView } from 'sscrp/rppub/components/image'; // 影像扫描和查看
import { submit, unsubmit, settle, unsettle } from '../btnclick/btnclick';
import { linkVoucherApp } from '../../../../../tmpub/pub/util/LinkUtil'; //凭证

const businessInfo = getBusinessInfo();
export default function buttonClick(props, id) {

	const iweb = commondata.iweb;
	const billtype = constant.billtype;
	const printfuncode = constant.printfuncode;
	const printnodekey = constant.printnodekey;
	const printtemplateid = constant.printtemplateid;
	const appcode = constant.appcode;

	// 公共选择数据
	function searchdata() {
		let selectdata = props.table.getCheckedRows(constant.ltablecode);
		//数据校验
		if (selectdata.length == 0) {
			toast({
				color: 'warning',
				content: this.state.json['36070DC-000026'] /* 国际化处理： 请选择数据*/
			});
			return;
		}
		return selectdata;
	};

	// 公共设置请求数据(删除、提交、收回、结算、取消结算)
	function getrequestdata() {
		let selectdata = searchdata();
		let pksArr = [];
		let pktsmap = {};
		let indexmap = {};
		//处理选择数据
		selectdata.forEach((val) => {
			let pk = val.data.values.pk_cashdeposit.value;
			let ts = val.data.values.ts.value;
			pksArr.push(pk); //主键数组
			pktsmap[pk] = ts;
			indexmap[pk] = val.index;
		});

		//自定义请求数据
		let data = {
			pageCode: constant.lpagecode,
			pktsmap: pktsmap,
			indexmap: indexmap,
			pks: pksArr
		};
		return data;
	}

	switch (id) {
		// 新增
		case 'addBtn':
			props.table.selectAllRows(this.tableId,false);
			props.pushTo(constant.cardpath, {
				status: 'add',
				addsrc: 'list',
				addid: this.state.addid
			});
			break;

			//复制
		case 'copyBtn':
			let copyData = searchdata();
			if (copyData.length != 1) {
				toast({
					color: 'warning',
					content: this.state.json['36070DC-000027'] /* 国际化处理： 只能选择一条数据!*/
				});
				return;
			}
			let copyid = 0;
			copyData.forEach((val) => {
				copyid = val.data.values.pk_cashdeposit.value;
			});
			props.table.selectAllRows(this.tableId,false);
			props.pushTo(constant.cardpath, {
				status: 'copy',
				id: copyid
			});

			break;

			//删除，可以支持批量
		case 'deleteBtn':
			let selectdata = props.table.getCheckedRows(constant.ltablecode);
			//数据校验
			if (selectdata.length == 0) {
				toast({
					color: 'warning',
					content: this.state.json['36070DC-000026'] /* 国际化处理： 请选择数据*/
				});
				return;
			}
			promptBox({
				color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
				title: this.state.json['36070DC-000008'], // 弹框表头信息/* 国际化处理： 删除*/ // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输
				content: this.state.json['36070DC-000031'], //this.modalContent(), //弹框内容，可以是字符串或dom/* 国际化处理： 确认删除所选数据吗?*/ // 提示内容,非必输
				// noFooter: false,                // 是否显示底部按钮(确定、取消),默认显示(false),非必输
				// noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
				// beSureBtnName: "确定",          // 确定按钮名称, 默认为"确定",非必输
				// cancelBtnName: "取消",         // 取消按钮名称, 默认为"取消",非必输
				// hasCloseBtn:false,             //显示“X”按钮，默认不显示，不显示是false，显示是true
				beSureBtnClick: this.delConfirm.bind(this),   // 确定按钮点击调用函数,非必输
				// cancelBtnClick: functionCancel,  // 取消按钮点击调用函数,非必输
				// closeBtnClick:functionClose, //关闭按钮点击调用函数，非必输
				// closeByClickBackDrop:false,//点击遮罩关闭提示框，默认是false点击不关闭,点击关闭是true
			})

			break;

			//提交
		case 'submitBtn':
		businessInfo
			submit.call(this, props, getrequestdata());
			this.emptychoicebox();
			break;

			// 收回
		case 'unsubmitBtn':
			unsubmit.call(this, props, getrequestdata());
			this.emptychoicebox();
			break;

			//结算
		case 'settleBtn':
			settle.call(this, props, getrequestdata());
			this.emptychoicebox();
			break;

			// 取消结算
		case 'unsettleBtn':
			unsettle.call(this, props, getrequestdata());
			this.emptychoicebox();
			break;

			//影像查看
		case 'imgreviewBtn':

			let imgreviewData = searchdata();
			if (imgreviewData.length != 1) {
				toast({
					color: 'warning',
					content: this.state.json['36070DC-000027'] /* 国际化处理： 只能选择一条数据!*/
				});
				return;
			}
			var billInfoMap = {};
			//处理选择数据
			imgreviewData.forEach((val) => {
				billInfoMap.pk_billid = val.data.values.pk_cashdeposit.value;
				billInfoMap.pk_billtype = val.data.values.billtypecode.value;
				billInfoMap.pk_tradetype = val.data.values.billtypecode.value;
				billInfoMap.pk_org = val.data.values.pk_org.value;
			});
			imageView(billInfoMap, iweb);
			break;
			//影像扫描
		case 'imgscanBtn':

			let imgscanData = searchdata();
			if (imgscanData.length != 1) {
				toast({
					color: 'warning',
					content: this.state.json['36070DC-000027'] /* 国际化处理： 只能选择一条数据!*/
				});
				return;
			}
			var billInfoMap = {};
			//处理选择数据
			imgscanData.forEach((val) => {
				billInfoMap.pk_billid = val.data.values.pk_cashdeposit.value;
				billInfoMap.pk_billtype = val.data.values.billtypecode.value;
				billInfoMap.pk_tradetype = val.data.values.billtypecode.value;
				billInfoMap.pk_org = val.data.values.pk_org.value;

				billInfoMap.BillType = val.data.values.billtypecode.value;
				billInfoMap.BillDate = val.data.values.creationtime.value;
				billInfoMap.Busi_Serial_No = val.data.values.pk_cashdeposit.value; //.pk_mtapp_bill.value;
				billInfoMap.pk_billtype = val.data.values.pk_billtype.value; //pk_billtype.value;
				billInfoMap.OrgNo = val.data.values.pk_org.value; //pk_org.value;
				billInfoMap.BillCode = val.data.values.billno.value; //vbillno.value;
				billInfoMap.OrgName = val.data.values.pk_org.display; //pk_org.display;
				billInfoMap.Cash = val.data.values.money.value; //orig_amount.value;
			});

			imageScan(billInfoMap, iweb);

			break;


			// 联查账户余额
		case 'cashbalanceBtn':
			let cashbalanceData = searchdata();
			if (cashbalanceData.length != 1) {
				toast({
					color: 'warning',
					content: this.state.json['36070DC-000027'] /* 国际化处理： 只能选择一条数据!*/
				});
				return;
			}
			let cashbalanceArr = [];
			//处理选择数据
			cashbalanceData.forEach((val) => {
				let pk_cashaccount, pk_org
				if (val.data.values.pk_cashaccount && val.data.values.pk_cashaccount.value != null) {
					pk_cashaccount = val.data.values.pk_cashaccount.value;
				}
				if (val.data.values.pk_org && val.data.values.pk_org.value != null) {
					pk_org = val.data.values.pk_org.value;
				}
				//修改请求联查方式
				let query_data = {
					pk_org: pk_org, //财务组织id
					pk_account: null, //银行账户id，没有可不写，和现金账户二选一
					pk_cashaccount: pk_cashaccount //现金账户id，没有可不写
				}
				cashbalanceArr.push(query_data); //现金账户
			});
			this.setState({
				showOriginalData: cashbalanceArr,
				showOriginal: true,
			});
			break;

			// 联查银行账户余额
		case 'bankaccbalanceBtn':
			let bankaccbalanceData = searchdata();
			if (bankaccbalanceData.length != 1) {
				toast({
					color: 'warning',
					content: this.state.json['36070DC-000027'] /* 国际化处理： 只能选择一条数据!*/
				});
				return;
			}
			let bankaccbalanceArr = [];
			//处理选择数据
			bankaccbalanceData.forEach((val) => {
				let pk_bankaccount, pk_org
				if (val.data.values.pk_bankaccount && val.data.values.pk_bankaccount.value != null) {
					pk_bankaccount = val.data.values.pk_bankaccount.value;
				}
				if (val.data.values.pk_org && val.data.values.pk_org.value != null) {
					pk_org = val.data.values.pk_org.value;
				}
				//修改请求联查方式
				let query_data = {
					pk_org: pk_org, //财务组织id
					pk_account: pk_bankaccount, //银行账户id，没有可不写，和现金账户二选一
					pk_cashaccount: null //现金账户id，没有可不写
				}
				bankaccbalanceArr.push(query_data); //现金账户
			});
			this.setState({
				showOriginalData: bankaccbalanceArr,
				showOriginal: true,
			});
			break;

			// 联查凭证
		case 'voucherBtn':
			let voucherData = searchdata();
			if (voucherData.length != 1) {
				toast({
					color: 'warning',
					content: this.state.json['36070DC-000027'] /* 国际化处理： 只能选择一条数据!*/
				});
				return;
			}
			let  pk_cashdepositv,billnov,pk_groupv,pk_orgv
			//处理选择数据
			voucherData.forEach((val) => {
				pk_cashdepositv = val.data.values.pk_cashdeposit.value;
				billnov = val.data.values.billno.value;
				pk_groupv = val.data.values.pk_group.value;
				pk_orgv = val.data.values.pk_org.value;
			});

			linkVoucherApp(
				this.props,
				pk_cashdepositv,
				pk_groupv,
				pk_orgv,
				billtype,
				billnov,
			);

			break;

			// 打印
		case 'printBtn':
			let printData = searchdata();
			let pks = [];
			printData.forEach((item) => {
				pks.push(item.data.values.pk_cashdeposit.value);
			});
			print(
				'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
				requesturl.print, {
					// billtype: billtype, //单据类型
					// funcode: printfuncode, //功能节点编码，即模板编码
					appcode: appcode, //appcode
					nodekey: printnodekey, //模板节点标识
					// printTemplateID: printtemplateid, //模板id
					oids: pks // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
				}
			);
			break;

			// 输出
		case 'outputBtn':
			let putputData = searchdata();
			let opks = [];
			putputData.forEach((item) => {
				opks.push(item.data.values.pk_cashdeposit.value);
			});
			output({
				url: requesturl.print,
				data: {
					nodekey: printnodekey,
					appcode: appcode,
					oids: opks,
					outputType: 'output'
				}
			});
			// this.setState({
			// 		outputData: {
			// 			funcode: printfuncode, //功能节点编码，即模板编码
			// 			nodekey: printnodekey, //模板节点标识
			// 			printTemplateID: printtemplateid, //模板id
			// 			oids: opks,
			// 			outputType: commondata.outputType
			// 		} //打印输出使
			// 	},
			// 	() => {
			// 		this.refs.printOutput.open();
			// 	}
			// );
			break;

			// 联查审批意见
		case 'approveopinionBtn':
			let linkapproveData = searchdata();
			if (linkapproveData.length != 1) {
				toast({
					color: 'warning',
					content: this.state.json['36070DC-000027'] /* 国际化处理： 只能选择一条数据!*/
				});
				return;
			}
			let pk_cashdeposit
			//处理选择数据
			linkapproveData.forEach((val) => {
				if (val.data.values.pk_cashdeposit && val.data.values.pk_cashdeposit.value != null) {
					pk_cashdeposit = val.data.values.pk_cashdeposit.value;
				}
			});
			this.setState({
				billId: pk_cashdeposit, //单据id
				approveshow: !this.state.approveshow
			})
			break;

			// 附件
		case 'enclosureBtn':
			let enclosureData = searchdata();
			let pk_cashdeposite, billno
			//处理选择数据
			enclosureData.forEach((val) => {
				if (val.data.values.billno && val.data.values.billno.value != null) {
					billno = val.data.values.billno.value;
				}
				if (val.data.values.pk_cashdeposit && val.data.values.pk_cashdeposit.value != null) {
					pk_cashdeposite = val.data.values.pk_cashdeposit.value;
				}
			});
			this.setState({
				billId: pk_cashdeposite, //单据id
				billno: billno, // 单据编号
				showUploader: !this.state.showUploader,
				target: null
			})
			break;

			//刷新
		case 'refreshBtn':
			this.refreshHtml();
			break;
	}
}