
import {
    constant
} from '../../config/config';

export const buttonVisible = function (props) {

    let status = props.getUrlParam('status');
    
    let btnflag = false;
    // 按钮默认隐藏
    props.button.setButtonVisible(
        [
            'joinquery',
            'joinquerygroup',
            'cashbalanceBtn',
            'bankaccbalanceBtn',
            'approveopinionBtn',
            'voucherBtn',
            'printBtn',
            'printgroup',
            'outputBtn',
            'enclosureBtn'
        ],
        btnflag
    );
    //浏览
    if (props.getUrlParam('status') === 'browse') {
        props.button.setButtonVisible(
            [
                'joinquery',
                'joinquerygroup',
                'cashbalanceBtn',
                'bankaccbalanceBtn',
                'approveopinionBtn',
                'voucherBtn',
                'printBtn',
                'printgroup',
                'outputBtn',
                'enclosureBtn'
            ], !btnflag);
    }
    // 设置form的编辑性
    props.form.setFormStatus(constant.formcode1, status);
    props.form.setFormStatus(constant.formcode3, "browse");
}