import {
    ajax,
    base,
    toast,
    NCMessage,
    cacheTools
} from 'nc-lightapp-front';
import {
    constant
} from '../../config/config';

export const buttonVisible = function (props) {

    let status = props.getUrlParam('status');


    let src = props.getUrlParam('src');
    let statusflag = status === 'browse' ? false : true;
    let btnflag = false;
    // 默认按钮隐藏
    props.button.setButtonVisible(
        [
            'joinquery',
            'joinquerygroup',
            'cashbalanceBtn',
            'bankaccbalanceBtn',
            'voucherBtn',
            'approveopinionBtn',
            'printBtn',
            'printgroup',
            'outputBtn',
            'enclosureBtn',
            'refreshBtn'
        ],
        btnflag
    );

    //浏览
    if (status === 'browse') {

        props.button.setButtonVisible(
            [
                'joinquery',
                'joinquerygroup',
                'cashbalanceBtn',
                'bankaccbalanceBtn',
                'voucherBtn',
                'approveopinionBtn',
                'printBtn',
                'printgroup',
                'outputBtn',
                'enclosureBtn'
            ], !btnflag);

    }
    props.form.setFormStatus(constant.formcode1, status);
    props.form.setFormStatus(constant.formcode3, "browse");

}