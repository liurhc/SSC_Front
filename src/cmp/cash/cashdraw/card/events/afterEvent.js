import { ajax, promptBox } from 'nc-lightapp-front';
import { constant, requesturl } from '../../config/config';
export default function afterEvent(props, moduleId, key, value, changedrows, i, s, g) {
	let cpagecode=constant.cpagecode;
	let formcode1=constant.formcode1;
	// 财务组织编辑后事件
	if (key === 'pk_org') {
		let changedata = props.createFormAfterEventData(cpagecode, formcode1);
		let pkorg=props.form.getFormItemsValue(moduleId, 'pk_org').value;
		let newvalue = pkorg;
		let oldvalue = changedrows.value;
		let oldorgDis=changedrows.display;
		if (oldvalue != newvalue && oldvalue != null) {
			this.setState({
				oldorg: oldvalue,
				oldorgDis:oldorgDis
			});
			promptBox({
				color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
				title: this.state.json['36070WC-000013'], // 弹框表头信息/* 国际化处理： 确认修改*/
				content: this.state.json['36070WC-000014'], //this.modalContent(), //弹框内容，可以是字符串或dom/* 国际化处理： 是否修改组织，这样会清空您录入的信息?*/
				// noFooter: false,                // 是否显示底部按钮(确定、取消),默认显示(false),非必输
				// noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
				// beSureBtnName: "确定",          // 确定按钮名称, 默认为"确定",非必输
				// cancelBtnName: "取消",         // 取消按钮名称, 默认为"取消",非必输
				// hasCloseBtn:false,             //显示“X”按钮，默认不显示，不显示是false，显示是true
				beSureBtnClick: this.ortBeSureBtnClick.bind(this),   // 确定按钮点击调用函数,非必输
				cancelBtnClick: this.orgCancelBtnClick.bind(this),  // 取消按钮点击调用函数,非必输
				// closeBtnClick:functionClose, //关闭按钮点击调用函数，非必输
				// closeByClickBackDrop:false,//点击遮罩关闭提示框，默认是false点击不关闭,点击关闭是true
			});
		}

		if (oldvalue == null) {
			ajax({
				url: requesturl.orgchange,
				data: changedata,
				success: (res) => {
					if (res.success) {
						let olcrate = res.data[formcode1].rows[0].values.olcrate.value;
						if(olcrate){
							props.form.setFormItemsDisabled(moduleId,{'olcrate':true});
						}
						props.form.setAllFormValue({ [formcode1]: res.data[formcode1] });
					}
				}
			});
		}
		if(pkorg){
			props.resMetaAfterPkorgEdit();//选择主组织以后，恢复其他字段的编辑性
		}
	}

	if (key === 'pk_currency') {
		props.form.setFormItemsDisabled(moduleId,{'olcrate':false});
		
		let currency = props.form.getFormItemsValue(moduleId, 'pk_currency').value;
		let changedata = props.createFormAfterEventData(cpagecode, formcode1);
		if (currency) {
			ajax({
				url: requesturl.currencychange,
				data: changedata,
				success: (res) => {
					if (res.success) {
						let olcrate,cashaccount,pk_cashaccount,pkcurrency
						if(res.data[formcode1].rows[0].values.olcrate){
							olcrate = res.data[formcode1].rows[0].values.olcrate.value;
						}
						
						if(res.data[formcode1].rows[0].values.pk_cashaccount){
							pk_cashaccount = res.data[formcode1].rows[0].values.pk_cashaccount.value;
							cashaccount = res.data[formcode1].rows[0].values.pk_cashaccount.display;
						}

						if(res.data.userjson){
							pkcurrency = res.data.userjson;
						}
						if(changedata.form[formcode1].rows[0].values.pk_currency.value === pkcurrency){
							props.form.setFormItemsDisabled(moduleId,{'olcrate': true});
						}
						props.form.setAllFormValue({ [formcode1]: res.data[formcode1] });
						props.form.setFormItemsValue(moduleId, {
							pk_bankaccount: { display: null, value: null }
						});
					}
				}
			});
		}else{
			props.form.setFormItemsValue(moduleId, {
				pk_cashaccount: { display: null, value: null }
			});
			props.form.setFormItemsValue(moduleId, {
				pk_bankaccount: { display: null, value: null }
			});
			props.form.setFormItemsValue(moduleId, { olcrate: { display: null, value: null } });
			props.form.getFormItemsValue();
		}
	}

	if (key === 'olcrate') {
		let olcrate = props.form.getFormItemsValue(moduleId, 'olcrate').value;
		let money = props.form.getFormItemsValue(moduleId, 'money').value;
		let scale = props.form.getFormItemsValue(moduleId, 'olcmoney').scale;
		if (olcrate && money) {
			let olcrateevent = props.createFormAfterEventData(cpagecode, formcode1);
			ajax({
				url: requesturl.oclmoneyevent,
				data: olcrateevent,
				success: (res) => {
					if(res.data){
						this.props.form.setAllFormValue({ [formcode1]: res.data[formcode1] });
					}
				}
			});
			
		}
	}

	if (key === 'money') {
		let olcrate = props.form.getFormItemsValue(moduleId, 'olcrate').value;
		let money = props.form.getFormItemsValue(moduleId, 'money').value;
		if (olcrate && money) {
			let moneyevent = props.createFormAfterEventData(cpagecode, formcode1);
			ajax({
				url: requesturl.oclmoneyevent,
				data: moneyevent,
				async:false, // 同步
				success: (res) => {
					if(res.data){
						this.props.form.setAllFormValue({ [formcode1]: res.data[formcode1] });
					}
				}
			});
		}
	}

	if (key === 'pk_bankaccount') {
		let  bankdocname,pk_bankdoc
		if(i){
			bankdocname=i.refcode;
			pk_bankdoc=i.refpk;
			props.form.setFormItemsValue(moduleId, {
				'pk_bankaccount': {
					display: bankdocname,
					value: pk_bankdoc
				}
			});
		}
	}

}
