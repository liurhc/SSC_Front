import { constant } from '../../config/config'
/**
 * -表格双击事件
 * @param {*}  
 */

export default function onrowDoubleclick(record, index, props, e)  {
    this.props.pushTo(constant.cardpath, {
        status: 'browse',
		id: record.pk_cashdraw.value
    });
}