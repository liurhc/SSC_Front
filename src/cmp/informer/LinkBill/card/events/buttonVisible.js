/**
 * @auther zhanghe
 * 列表置灰
 */
let tableId = 'table';
let formId = 'form';
export const buttonVisible = function (props) {
	let pk = props.form.getFormItemsValue(formId, ['pk_lower'])[0].value;	
	
	if (!pk) {
		//没有选中
		//props.button.setButtonDisabled(['query'], true);
		props.button.setButtonVisible(['query'], false);
	} else {
		//props.button.setButtonDisabled(['query'], false);
		props.button.setButtonVisible(['query'], true);
	}
	
}



