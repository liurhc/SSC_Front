/**
 * @auther zhanghe
 * 列表置灰
 */
let tableId = 'table';
export const buttonVisible = function (props) {
	let pk = props.table.getAllTableData(tableId).rows[0].values.pk_lower.value;
	
	
	if (!pk) {
		//没有选中
		//props.button.setButtonDisabled(['query'], true);
		props.button.setButtonVisible(['query'], false);
	} else {
		//props.button.setButtonDisabled(['query'], false);
		props.button.setButtonVisible(['query'], true);
	}
	
}



