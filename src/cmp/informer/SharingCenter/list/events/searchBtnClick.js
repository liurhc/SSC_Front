import { ajax, cardCache, toast } from 'nc-lightapp-front';
let { setDefData } = cardCache;
import * as CONSTANTS from '../constants';
let { dataSource } = CONSTANTS;
//点击查询，获取查询区数据
export default function clickSearchBtn(props, searchVal) {
    if (searchVal) {
        setDefData(dataSource, this.listDataSource, searchVal);
        let queryInfo = props.search.getQueryInfo('search');
        let pageInfo = props.table.getTablePageInfo('table');
        let oid = queryInfo.oid;
        let data = {
            querycondition: searchVal,
            pageInfo: pageInfo,
            pageCode: '36070AI_L05',
            queryAreaCode: 'search',  //查询区编码
            oid: oid,  //查询模板id，手工添加在界面模板json中，放在查询区，后期会修改
            querytype: 'tree'
        };
        ajax({
            url: '/nccloud/cmp/informer/informerquery.do',
            data: data,
            success: (res) => {
                let { success, data } = res;
                if (success) {
                    if (data) {
                        this.props.table.setAllTableData(this.tableId, data[this.tableId]);
                        toast({
                            color: 'success'
                        });
                    } else {
                        this.props.table.setAllTableData(this.tableId, { rows: [] });
                        toast({
                            color: 'warning',
                            content: '未查询出符合条件的数据!'
                        });
                    }

                }
            }
        });
    }

};
