import buttonClick from './buttonClick';
import initTemplate from './initTemplate';
import pageInfoClick from './pageInfoClick';
import buttonDisable from './buttonDisable';
export { buttonClick, initTemplate, pageInfoClick, buttonDisable };
