export default function buttonDisable(props, moudleId, values, index, falg) {
	let selectedData = props.table.getCheckedRows(moudleId);
	if (selectedData.length == 1) {
		if (values.generateflag) {
			let generateflag = values.generateflag.value;
			if (generateflag == 'hasgenerate') {//已生成
				props.button.setButtonDisabled([
					'publish', 'unpublish'
				], true);
				props.button.setButtonDisabled([
					'printbtn'
				], false);
			} else if (generateflag == 'nogenerate') {//不生成
				props.button.setButtonDisabled([
					'publish', 'unpublish'
				], true);
				props.button.setButtonDisabled([
					'printbtn'
				], false);
			} else if (generateflag == 'hasrelease' || generateflag == 'hasclaim' || generateflag == 'nclaunch' || generateflag == 'hasnogenerate') {//已认领,NC发起
				props.button.setButtonDisabled([
					'publish', 'unpublish', 'printbtn'
				], false);
			}
		}
	} else if (selectedData.length > 1) {
		props.button.setButtonDisabled([
			'publish', 'unpublish', 'printbtn'
		], false);
	} else if (selectedData.length == 0) {
		props.button.setButtonDisabled([
			'publish', 'unpublish'
		], true);
	}
}