import { ajax, base, toast } from 'nc-lightapp-front';
import { tableId, pagecode } from '../constants';
let { NCMessage } = base;
/**
 * 具体业务操作
 * @param {*} props 
 * @param {*} url 
 * @param {*} message 
 */
export function busiOperate(that, key, props, url, opername) {
    const selectedData = props.table.getCheckedRows(tableId);
    if (selectedData.length == 0) {
        NCMessage.create({ content: that.props.MutiInit.getIntl("36070AISCC") && that.props.MutiInit.getIntl("36070AISCC").get('36070AISCC-000000'), color: 'warning' });/* 国际化处理： 请选择数据！*/
        return;
    } else {
        let pks = [];
        let ts;
        selectedData.forEach((val) => {
            let pk;
            pk = val.data.values.pk_informerrelease.value;
            pks.push(pk);
        });
        let oldlenght = pks.length;
        let data = {
            pks: pks
        };
        ajax({
            url: url,
            data,
            success: function (res) {
                that.getdata();
                let total;
                let failNum;
                let successNum;
                let content;
                if(res.data){
                    total = res.data.total;
                    failNum = res.data.failNum;
                    successNum = res.data.successNum;
                    content = '共' + opername + total + '条，';
                    content = content+'成功' + successNum + '条 ,';
                    content = content+ '失败' + failNum + '条';
                }
                if (res.data.errormessage) {
                     //全部失败
                     if(failNum == total){
                        toast({
                            duration: 'infinity', 
                            color: 'danger', 
                            title: opername +' 全部失败!', 
                            content: content, 
                            groupOperation: true,
                            TextArr: [ '展开', '收起', '关闭' ],
                            groupOperationMsg: res.data.errormessage.split("\n") 
                        });
                    }
                    //部分失败
                    else{
                        toast({
                            duration: 'infinity', 
                            color: 'warning', 
                            title: opername +' 部分失败!', 
                            content: content, 
                            groupOperation: true, 
                            TextArr: [ '展开', '收起', '关闭' ], 
                            groupOperationMsg: res.data.errormessage.split("\n") 
                        });
                    }
                } else {
                    let tabledata = props.table.getAllTableData(tableId);
                    if (key == 'unpublish' && oldlenght == tabledata.rows.length) {
                        props.pushTo("/list", {
                            status: 'browse'
                        });
                    } else {
                        toast({
                            duration: 3,
                            color: 'success', 
                            title: opername +' 全部成功!', 
                            content: content, 
                            groupOperation: true 
                        });
                    }
                }
            }
        });
    }
}
