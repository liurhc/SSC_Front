import buttonClick from './buttonClick';
import initTemplate from './initTemplate';
import afterEvent from './afterEvent';
import pageInfoClick from './pageInfoClick';
import buttonDisable from './buttonDisable';
export { buttonClick, initTemplate, afterEvent, pageInfoClick,buttonDisable};
