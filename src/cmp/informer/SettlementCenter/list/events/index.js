import searchBtnClick from './searchBtnClick';
import initTemplate from './initTemplate';
import pageInfoClick from './pageInfoClick';
import buttonClick from './buttonClick';
import afterEvent from './afterEvent';
import searchAfterEvent from './searchAfterEvent';
import buttonDisable from './buttonDisable';
export { buttonDisable,searchBtnClick, pageInfoClick,initTemplate,buttonClick,afterEvent,searchAfterEvent};
