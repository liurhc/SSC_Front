/**
 * 公共配置
 */
//按钮平铺显示数量
export const button_limit = 5;
//模块编码
export const module_id = '3607';
export const module_name = 'cmp';
//来源模块
export const sourceModel_FTS = "CMP";
//单据类型
export const PaymentMergeBillType = "36D1";
export const PaymentBillType = "36D1";


