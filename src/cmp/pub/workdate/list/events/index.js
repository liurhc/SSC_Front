import searchBtnClick from './searchBtnClick';
import initTemplate from './initTemplate';
import pageInfoClick from './pageInfoClick';
import buttonClick from './buttonClick';
import beSureBtnClick from './beSureBtnClick';
import tableButtonClick from './tableButtonClick';
export { searchBtnClick, pageInfoClick, initTemplate, buttonClick, beSureBtnClick, tableButtonClick };