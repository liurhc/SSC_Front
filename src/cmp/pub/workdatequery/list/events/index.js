import searchBtnClick from './searchBtnClick';
import initTemplate from './initTemplate';
import pageInfoClick from './pageInfoClick';
import buttonClick from './buttonClick';
export { searchBtnClick, pageInfoClick,initTemplate,buttonClick };