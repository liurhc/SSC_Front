/*
 * @Author: zhanghe 
 * @PageInfo: 查询区参照过滤
 * @Date: 2018-04-25 09:43:24 
 */
import { ajax, base } from 'nc-lightapp-front';
import { REF21_CONST } from '../const';

export default function referEvent(props, meta) {
	//参照
	meta[REF21_CONST.searchId].items.find((e) => e.attrcode === 'pk_org').isMultiSelectedEnabled = true;	
	
	meta[REF21_CONST.searchId].items.map((item) => {
		//设置参照面板不显示主组织
		if (item.attrcode === 'pk_org') {
			item.queryCondition = () => {
				item.isTreelazyLoad = false;
				return {
					funcode: '36070APM',
					TreeRefActionExt: 'nccloud.web.cmp.ref.CMPApplyOrgBuilder'
				};
			};
		}
		
	});
}
