import { createPage, ajax, base, cardCache } from 'nc-lightapp-front'; 
import buttonClick from './buttonClick'; 
import { searchBtnClick, listInitData } from './searchBtnClick';
//引入常量定义
import { APP_INFO, LIST_PAGE_INFO, URL_INFO, UI_CONF, CARD_PAGE_INFO, SRCTYPE, CACHE_KEY ,PROP_EXT_OBJ} from '../../cons/constant';
import { SCENE } from "../../../../../tmpub/pub/cons/constant";
import { buttonVisible } from "./buttonVisible";
import { go2card, loadSearchCache, getCahceValue, autoLoadData } from "../../util/index";
import { loadSearchCondition } from '../../../../pub/utils/CMPButtonUtil';
import { setPropCache, getPropCache, loadMultiLang } from "../../../../../tmpub/pub/util/index";
let { NCPopconfirm, NCIcon ,NCTooltip} = base;
import { setDefOrg2AdvanceSrchArea, setDefOrg2ListSrchArea } from "../../../../../tmpub/pub/util/index";
//引入内部账户参照
import { AccidGridRef } from "../../../../../tmpub/refer/accid/AccidGridRef";

export default function (props) {  	//从页面级缓存中获取页面对象（List对象）
	const that = getPropCache(props, APP_INFO.FUNCODE, PROP_EXT_OBJ.CONTAIN);
	//构建DOM请求(平台提供api)
	props.createUIDom(
		{ pagecode: LIST_PAGE_INFO.PAGE_CODE },
		(data) => {
			if (!data) {
				return;
			}
			//处理模板
			if (data.template) {
				let meta = data.template;
				//个性化处理模板
				meta = modifierMeta.call(that, props, meta, data)
				//高级查询区域加载默认业务单元
				setDefOrg2AdvanceSrchArea(props, LIST_PAGE_INFO.SEARCH_CODE, data);
				//业务数据初始化
				initData.call(that, props);
				//加载模板数据
				props.meta.setMeta(meta);
				//列表查询区域加载默认业务单元
				setDefOrg2ListSrchArea(props, LIST_PAGE_INFO.SEARCH_CODE, data);
			}
			//处理按钮
			if (data.button) {
				let button = data.button;
				props.button.setButtons(button);
				buttonVisible(props);
				props.button.setPopContent('deletetableBtn', loadMultiLang(props, '36070APM--000032'));/* 国际化处理： 确认要删除吗?*/
				props.button.setPopContent('generatetableBtn', loadMultiLang(props, '36070APM--000013'));/* 国际化处理： 您确定要生成付款单据吗？一旦生成，则不可以取消生成！*/
				
			}
		}
	)
}

function modifierMeta(props, meta, data) {
	const that = this;
	//有默认业务单元
	if (data.context && data.context.pk_org) {
		let { pk_org, org_Name } = data.context;
		meta[LIST_PAGE_INFO.SEARCH_CODE].items.map((item) => {
			if (item.attrcode == 'pk_org') {
				item.initialvalue = {
					display: org_Name, value: pk_org
				}
			}
		});
	}
	//修改列渲染样式
	meta[LIST_PAGE_INFO.SEARCH_CODE].items = meta[LIST_PAGE_INFO.SEARCH_CODE].items.map((item, key) => {
		// item.visible = true;
		// item.col = '3';
		return item;
	});
	//参照
	meta[LIST_PAGE_INFO.SEARCH_CODE].items.find((e) => e.attrcode === 'pk_org').isMultiSelectedEnabled = true;
	meta[LIST_PAGE_INFO.TABLE_CODE].pagination = true;
	//查询区域参照过滤
	meta[LIST_PAGE_INFO.SEARCH_CODE].items.map((item) => {
		//组织参照过滤
		if (item.attrcode == 'pk_org') {
			item.queryCondition = () => {
				item.isTreelazyLoad = false;
				return {
					funcode: '36070APM',
					TreeRefActionExt: 'nccloud.web.tmpub.filter.FinanceOrgPermissionFilter'
				};
			};
		}			
	});

	//付款银行账号
	meta[LIST_PAGE_INFO.SEARCH_CODE].items.map((item) => {
		if (item.attrcode == 'pk_bankacc_p') {
			item.queryCondition = () => {
				let pk_org=props.search.getSearchValByField(LIST_PAGE_INFO.SEARCH_CODE,'pk_org').value.firstvalue;
				let currtype=props.search.getSearchValByField(LIST_PAGE_INFO.SEARCH_CODE,'pk_currtype').value.firstvalue;
				return {
					pk_orgs: pk_org,
					pk_currtype: currtype,
					refnodename: props.MutiInit.getIntl("36070PBR") && this.props.MutiInit.getIntl("36070PBR").get('36070PBRCOMP-000017'),/* 国际化处理： 使用权参照*/
					noConditionOrg:'Y',
					GridRefActionExt: 'nccloud.web.cmp.ref.CMPBatchOrgBankaccSubDefaultGridRefSqlBuilder' //自定义参照过滤条件
				};
			};
		}
	});

	meta[LIST_PAGE_INFO.TABLE_CODE].items = meta[LIST_PAGE_INFO.TABLE_CODE].items.map((item, key) => {
		item.width = 150;
		if (item.attrcode == 'vbillno') {
			item.render = (text, record, index) => {
				return (
					<a
						// style={{ textDecoration: 'underline', cursor: 'pointer' }}
						style={{  cursor: 'pointer' }}
						onClick={() => {
							go2card(props, { pagecode: CARD_PAGE_INFO.PAGE_CODE, status: 'browse', id: record.pk_apply.value }, that.getState.bind(that));
						}}
					>
						{
							record.vbillno ? record.vbillno.value : ''
						}
					</a>
				);
			};
		}
		return item;
	});
	//添加操作列	
	meta[LIST_PAGE_INFO.TABLE_CODE].items.push({
		attrcode: 'opr',
		label: loadMultiLang(props, '36070APM--000016'),//操作/* 国际化处理： 操作*/
		width: 200,
		fixed: 'right',
		className: "table-opr",
		visible: true,
		itemtype:'customer',
		render: (text, record, index) => {
			let buttonArr = getBodyButton(record);
			return props.button.createOprationButton(buttonArr, {
				area: "list_inner",
				buttonLimit: UI_CONF.BUTTON_LIMIT,
				onButtonClick: (props, key) => buttonClick.call(that, props, key, text, record, index)
			});
		}
	});
	return meta;
}
/**
 * 获取表体按钮组
 * @param {*} record 行数据
 */
function getBodyButton(record) {
	//单据状态
	let billstatus = record && record.busistatus && record.busistatus.value;	
	let buttonAry;
	//待提交
	if (billstatus == '1') {		
		buttonAry = ["edittableBtn", "deletetableBtn","committableBtn"];
	}
	//待审批
	else if (billstatus == '2') {
		buttonAry = ["backtableBtn"];
	}
	//待生成
	else if (billstatus == '3' ||billstatus == '4') {
		buttonAry = ["generatetableBtn","backtableBtn"];
	}	
	else {
		buttonAry = [];
	}
	return buttonAry;
}

function initData(props) { 
	let pk_ntbparadimvo = props.getUrlParam('pk_ntbparadimvo');
	if (pk_ntbparadimvo) {
		//如果有预算明细主键，则表明是从预算联查过滤，进行数据初始化
		listInitData.call(this, props);
	} else {
		//从缓存中加载数据
		getCahceValue(props, this.updateState.bind(this));
	}	
}

