export default function(props) {
	if (this.state.queryInfo == null) {
		this.props.button.setButtonDisabled([ 'Refresh' ], true);
	} else {
		this.props.button.setButtonDisabled([ 'Refresh' ], false);
	}
}
