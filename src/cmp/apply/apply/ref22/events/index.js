import { buttonClick } from './buttonClick';
import { initTemplate } from './initTemplate';
import afterEvent from './afterEvent';
import pageInfoClick from './pageInfoClick';
import { buttonVisible } from './buttonVisible';
import { bodyBtnVisible } from "./bodyBtnVisible";
export { buttonClick, afterEvent, initTemplate, pageInfoClick, buttonVisible, bodyBtnVisible }; 


