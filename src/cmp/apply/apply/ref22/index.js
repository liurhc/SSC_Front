/**
 * 付款申请转单卡片页面 
 * @author zhanghe 
 * @version 1.0
 */
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast, high, cardCache ,cacheTools } from 'nc-lightapp-front'; 
let { NCFormControl, NCAnchor, NCScrollLink, NCScrollElement, NCAffix, NCBackBtn ,NCButton } = base;
import { Radio } from 'tinper-bee';
import { buttonClick, initTemplate, afterEvent, pageInfoClick, buttonVisible } from './events';
import axios from 'axios';
//引入常量定义
import { APP_INFO, CARD_PAGE_INFO, LIST_PAGE_INFO, URL_INFO, UI_CONF, SHOW_MODE, ITEM_INFO ,TRAN_CARD_PAGE_INFO, TRAN_LIST_PAGE_INFO} from '../cons/constant';
import { REF21_CONST } from '../ref21/const';
//引入内转api
import { versionControl, loadData2Card, qryDataByPK, loadNeedDealDataByPK, loadCopyData, repaintView, save } from "../util/index";
//引入附件组件，审批详情,联查预算
const { NCUploader, ApproveDetail, Inspection ,Refer ,BillTrack} = high;
import InvoiceUploader from 'sscrp/rppub/components/invoice-uploader';
import InvoiceLink from 'sscrp/rppub/components/invoice-link';

// import JointBill from '../../../../sscrp/refer/jointbill/JointBillTableRef/JointBillModal';




class Card extends Component {
	constructor(props) {
		super(props);		
		this.state = {
			//附件框是否显示
			showUploader: false,
			//单据主键
			billID: '',
			//单据编码
			billNO: '',
			//是否行复制模式
			isRowCopy: false,	
			//是否显示审批详情
			showApproveDetail: false,				
			//是否显示预算计划
			showNtbDetail: false,
			//预算计划数据
			ntbdata: null,
			//指派数据
			assignData: null,
			//是否显示指派
			assignShow: false,
			//旧数据
			showbilltrack: false,//联查单据
			showbilltrackpk:'',//联查单据pk
			showbilltracktype:'',//联查单据类型
			vbillno:'',//单据编号,
			tpflag:true,
			data: [],
			tradetype: {}
		}
		initTemplate.call(this, props);
	}

	componentWillMount() {
        // 关闭浏览器
        window.onbeforeunload = () => {
	        let status = this.props.getUrlParam('status'); 
            if (status !== 'browse') {
                return this.props.MutiInit.getIntl("36070APM") && this.props.MutiInit.getIntl("36070APM").get('36070APM--000112');			
            }
        };
	}



	
	componentDidMount() {		 
		let id = this.props.getUrlParam(URL_INFO.PARAM.ID);
		let id21 = cacheTools.get('21TO36D1Pks');//采购付款计划推付款申请传给的pk
		let status = this.props.getUrlParam('status');
		
		if (id) {
			let status = this.props.getUrlParam('status');
			let pk = this.props.getUrlParam('id');
			let ts = this.props.getUrlParam('ts');
			if (status == SHOW_MODE.COPY) {
				//加载复制数据
				loadCopyData(this.props, pk, this.updateState.bind(this));
			}else if(status == SHOW_MODE.BROWSER || status == SHOW_MODE.EDIT){			
				//查询页面数据
				qryDataByPK(this.props, pk, this.updateState.bind(this));
			}else {
				loadData2Card(this.props, () => {
					//界面重绘
					repaintView(this.props);
					this.setState({
						billID: null, billNO: null
					})
				});
			}
		}
		let transfer = this.props.getUrlParam('srcbilltype') === 'ref22';
		if (transfer) {
			let transferIds = this.props.transferTable.getTransferTableSelectedId(CARD_PAGE_INFO.HEAD_CODE);
			this.getTransferValue(transferIds);
		} 
	}
	/**
	 * 更新状态机
	 * @param {*} obj 
	 */
	updateState(obj) {
		if (obj && Object.keys(obj).length > 0) {
			this.setState(obj);
		}
	}
	
	getTransferValue = (transferIds) => {
		if(transferIds){
			//从缓存中获取查询区域条件
			let queryVO = cardCache.getDefData(REF21_CONST.searchId, REF21_CONST.Ref21DataSource);
			let pkMapTs={};
			let index = 0;
			let pk = null;
			let ts =null;
			while (index < transferIds.length) {
				//获取行主键值
				pk = transferIds[index] && transferIds[index].head && transferIds[index].head.pk;
				//获取行ts时间戳
				ts = transferIds[index] && transferIds[index].head && transferIds[index].head.ts;				
				//判空
				if (pk && ts) {
					pkMapTs[pk] = ts;
				}
				index++;
			}
			ajax({
				url: URL_INFO.TRANLIST.CARDQRY,
				data: {pkMapTs,queryVO},
				success: (res) => {
					let { success, data } = res;
					if (success) {
						// let { head, body } = data[0];
						if (data) {
							this.props.transferTable.setTransferListValue(TRAN_CARD_PAGE_INFO.HEAD_CODE, data);	
						}
						// if (head) {
						// 	props.form.setAllFormValue({ [CARD_PAGE_INFO.HEAD_CODE]: head[CARD_PAGE_INFO.HEAD_CODE] });							
						// }
						// if (body) {
						// 	props.cardTable.setTableData(CARD_PAGE_INFO.BODY_CODE, body[CARD_PAGE_INFO.BODY_CODE]);
						// } 
					}else{
						this.props.transferTable.setTransferListValue(TRAN_CARD_PAGE_INFO.HEAD_CODE, []);	
					}
				} 
			});
		}else{
			this.props.transferTable.setTransferListValue(TRAN_CARD_PAGE_INFO.HEAD_CODE, []);	
		}
		
	};
	
	//获取列表肩部信息
	getTableHeadButton = (buttons) => {
		let { createButton, createButtonApp } = this.props.button;
		return (
			<div className="shoulder-definition-area">
				<div className="definition-icons">
					{this.props.cardTable.createBrowseIcons(CARD_PAGE_INFO.BODY_CODE, {
						iconArr: ['close', 'open', 'max'],
					})}
					{
						createButtonApp({ onButtonClick: buttonClick.bind(this), buttonLimit: UI_CONF.BUTTON_LIMIT, area: 'card_body' })
					}
				</div>
			</div>
		);
	};
	/**
	 * 渲染切换上下页组件
	 */
	renderCardChange = () => {
		let { createCardPagination } = this.props.cardPagination;
		let status = this.props.getUrlParam('status');
		if (status == 'browse') {
			return (createCardPagination({
				dataSource: APP_INFO.DATA_SOURCE_TRANS, 
				handlePageInfoChange: pageInfoClick.bind(this)
			}));
		}
	}

	/**
	 * 关闭审批详情
	 */
	link2ListPage = () => {
		this.props.pushTo(URL_INFO.LIST_PAGE_URL, {
			status: 'browse',
			pagecode: LIST_PAGE_INFO.PAGE_CODE
		});
	}
	render() {		
		const { createBillHeadInfo } = this.props.BillHeadInfo;
		let { cardTable, form, button, modal, cardPagination ,transferTable} = this.props;
		let buttons = this.props.button.getButtons();
		let multiLang = this.props.MutiInit.getIntl(APP_INFO.MODULE_ID);
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createCardPagination } = cardPagination;
		let { createButton, createButtonApp } = button;
		let { createModal } = modal;	
		let status = this.props.getUrlParam('status');
		let { showUploader, billID, billNO, showInnerAccInfo, pk_inneracc, showApproveDetail, showNtbDetail, ntbdata, showBankAccBalance, bankAccBalanceParam ,tpflag ,showbilltrack,showbilltracktype,showbilltrackpk, assignData, assignShow} = this.state;
		const that = this;
		const { createTransferList } = transferTable;
		return (
			<div id="transferCard" className="nc-bill-transferList">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						{createBillHeadInfo({
							title: this.props.MutiInit.getIntl("36070APM") && this.props.MutiInit.getIntl("36070APM").get('36070APM--000023'),
							backBtnClick: () => {
								this.props.pushTo(URL_INFO.LIST_PAGE_URL, {
									status: 'browse',
									pagecode: LIST_PAGE_INFO.PAGE_CODE
								});
							}
						})}
						{/* {(status == 'browse') && <NCBackBtn onClick={that.link2ListPage.bind(that)}></NCBackBtn>}
						<h2>{this.props.MutiInit.getIntl("36070APM") && this.props.MutiInit.getIntl("36070APM").get('36070APM--000023')}:{status =='add'?'':billNO}</h2>国际化处理： 付款申请 */}
					</div>	
					<div className="header-button-area">
						{
							createButtonApp({ onButtonClick: buttonClick.bind(that), buttonLimit: UI_CONF.BUTTON_LIMIT, area: 'card_head' })
						}
					</div>
				</div>
				<div className="nc-bill-transferList-content">
					{createTransferList({
						//表格组件id
						headcode: CARD_PAGE_INFO.HEAD_CODE,
						transferListId: TRAN_CARD_PAGE_INFO.HEAD_CODE, //转单列表id
						onTransferItemSelected: (record, status) => {
							//转单缩略图被选中时的钩子函数
							let isEdit = status ? SHOW_MODE.BROWSER : SHOW_MODE.EDIT;							
							if(isEdit == SHOW_MODE.BROWSER){
								this.props.setUrlParam({status:SHOW_MODE.BROWSER})
								let id = record.head[CARD_PAGE_INFO.HEAD_CODE].rows[0].values.pk_apply.value;
								if(id){
									this.props.setUrlParam({ id: id });	
									this.props.form.setFormStatus([CARD_PAGE_INFO.HEAD_CODE], isEdit);
									this.props.cardTable.setStatus(CARD_PAGE_INFO.BODY_CODE, isEdit);
									this.props.form.setAllFormValue({[CARD_PAGE_INFO.HEAD_CODE]: record.head[CARD_PAGE_INFO.HEAD_CODE]});
									this.props.cardTable.setTableData(CARD_PAGE_INFO.BODY_CODE, record.body[CARD_PAGE_INFO.BODY_CODE]);														
								}
							}else {
								this.props.delUrlParam('id');
								this.props.setUrlParam({status:SHOW_MODE.ADD})
								this.props.form.setFormStatus([CARD_PAGE_INFO.HEAD_CODE], isEdit);
								this.props.cardTable.setStatus(CARD_PAGE_INFO.BODY_CODE, isEdit);
								this.props.form.setAllFormValue({[CARD_PAGE_INFO.HEAD_CODE]: record.head[CARD_PAGE_INFO.HEAD_CODE]});
								this.props.cardTable.setTableData(CARD_PAGE_INFO.BODY_CODE, record.body[CARD_PAGE_INFO.BODY_CODE]);							
							
							}
							repaintView(this.props);
						},
						onTransferItemClick: (record, index, status) => {
							//点击转单缩略图的钩子函数
							let isEdit = status ? SHOW_MODE.BROWSER : SHOW_MODE.EDIT;
							// this.props.form.setFormStatus(CARD_PAGE_INFO.HEAD_CODE, isEdit);
							// this.props.cardTable.setStatus(CARD_PAGE_INFO.BODY_CODE, isEdit);
							if(isEdit == SHOW_MODE.BROWSER){
								this.props.setUrlParam({status:SHOW_MODE.BROWSER})
								let id = record.head[CARD_PAGE_INFO.HEAD_CODE].rows[0].values.pk_apply.value;
								this.props.setUrlParam({ id: id });	
							}else {
								this.props.delUrlParam('id');
								this.props.setUrlParam({status:SHOW_MODE.ADD})
								this.props.setUrlParam({status:SHOW_MODE.ADD})
							}	
							this.props.form.setFormStatus([CARD_PAGE_INFO.HEAD_CODE], isEdit);
							this.props.cardTable.setStatus(CARD_PAGE_INFO.BODY_CODE, isEdit);							
							this.props.form.setAllFormValue({ [CARD_PAGE_INFO.HEAD_CODE]: record.head[CARD_PAGE_INFO.HEAD_CODE] });
							this.props.cardTable.setTableData(CARD_PAGE_INFO.BODY_CODE, record.body[CARD_PAGE_INFO.BODY_CODE]);							
							repaintView(this.props);						
						}
					})}
					<div className="transferList-content-right nc-bill-card" id="paybill-card">
						<NCScrollElement name='forminfo'>
						<div className="form-area">
							{createForm(CARD_PAGE_INFO.HEAD_CODE, {
								// expandArr: [CARD_PAGE_INFO.HEAD_CODE, 'form_innertransfer_05', 'form_innertransfer_06'],
								onAfterEvent: afterEvent.bind(this)
							})}
						</div>
						</NCScrollElement>
						<NCScrollElement name='businfo'>
							<div className="nc-bill-table-area">
								{/* {this.getTableHeadButton(buttons)} */}
								{createCardTable(CARD_PAGE_INFO.BODY_CODE, {
									tableHead: this.getTableHeadButton.bind(this, buttons),
									modelSave: this.saveBill,
									onAfterEvent: afterEvent.bind(this),
									showCheck: true,
									showIndex: true
								})}
							</div>
						</NCScrollElement>
						
						{/** 附件 **/}
						<div className="nc-faith-demo-div2">
							{showUploader &&
								<NCUploader
									onHide={() => {
										this.setState({
											showUploader: false
										})
									}}
									billId={billID}
									target={null}
									placement={'bottom'}
									billNo={billNO}
								/>
							}
						</div>
						{/** 联查工作流 **/}
						<div>
							{showApproveDetail && <ApproveDetail
								show={showApproveDetail}
								billtype={APP_INFO.BILLTYPE}
								billid={billID}
								close={() => {
									this.setState({
										showApproveDetail: false
									})
								}}
							/>}
						</div>
						{/** 联查预算 **/}
						<div>
							<Inspection
								show={showNtbDetail}
								sourceData={ntbdata}
								cancel={() => {
									this.setState({ showNtbDetail: false })
								}}
								affirm={() => {
									this.setState({ showNtbDetail: false })
								}}
							/>
						</div>
						<div>
						<InvoiceLink 
               {...this.state.sscrpLinkInvoiceData}
                table={this.props.table}
                />
				</div> 
						{/** 联查单据 **/}
						<div>
							<BillTrack
								show={this.state.showbilltrack}
								close={()=>{
									this.setState({showbilltrack: false})
								}}
								pk={this.state.showbilltrackpk}  //单据id
								type={this.state.showbilltracktype}  //单据类型
							/>
						</div>

						<div>
							<InvoiceLink 
						{...this.state.sscrpLinkInvoiceData}
							table={this.props.table}
							/>
							</div> 
							<div>
			                <InvoiceUploader
							{...this.state.sscrpInvoiceData}
							/>
			               <InvoiceLink 
							{...this.state.sscrpInvoiceData}
							table={this.props.table}
							/>
						</div> 
					
						{/** 审批流指派 **/}
						<div>
							{assignShow && <ApprovalTrans
								title={'指派'}
								data={assignData}
								display={assignShow}
								getResult={(value) => {
									let extParam = {};
									if (value) {
										extParam['content'] = JSON.stringify(value);
									}
									//关闭指派框
									this.setState({ assignShow: false, assignData: null });
									cardOperator(this.props, CARD_PAGE_INFO.PAGE_CODE, CARD_PAGE_INFO.HEAD_CODE, [], ITEM_INFO.PK, URL_INFO.COMMON.COMMIT, '提交成功！', APP_INFO.DATA_SOURCE, repaintView.bind(this, this.props), false, extParam);
								}}
								cancel={() => {
									this.setState({ assignShow: false, assignData: null })
								}}
							/>}
						</div>
						
					</div>
				</div>
			</div>
		)
	}
}
Card = createPage({
	mutiLangCode: APP_INFO.MODULE_ID,
	billinfo: {
		billtype: 'card',
		pagecode: CARD_PAGE_INFO.PAGE_CODE,
		headcode: CARD_PAGE_INFO.HEAD_CODE,
		bodycode: CARD_PAGE_INFO.BODY_CODE
	},
	// initTemplate: initTemplate
})(Card);

export default Card;
