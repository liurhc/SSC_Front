//主子表卡片
import React, { Component } from 'react';
import { createPage, ajax, base, toast, high } from 'nc-lightapp-front';
import { buttonClick, initTemplate, afterEvent } from './events';
import { jsondata } from "./jsondata";
import { Templatedata } from "../config/Templatedata";
import { buttonVisable } from "./events/buttonVisable";//自定义按钮显示
import { setSourceFlag } from '../util/setSourceFlag.js';//设置来源
import { commonurl } from '../../../public/utils/constant';//附件改造使用
let {  NCScrollElement, NCAffix } = base;
const { Inspection } = high;//联查计划预算
const { NCUploader, ApproveDetail } = high;//附件相关
const { BillTrack, PrintOutput } = high;//联查单据
const { NCDiv: Div } = base;
const { NCBackBtn } = base;//返回button

/**
 * 模版配置参数
 */
let form_id = Templatedata.card_formid;
let search_id = Templatedata.list_searchid;
let table_id = Templatedata.card_tableid;
let module_id = Templatedata.card_tableid;
let page_id = Templatedata.card_pageid;

class Card extends Component {
	constructor(props) {
		super(props);
		this.formId = form_id;
		this.searchId = search_id;
		this.moduleId = module_id;
		this.tableId = table_id;
		this.pageId = page_id;
		this.state = {
			showInspection: false,//联查预算
			sourceData: null,//联查预算数据源
			showbilltrack: false,//联查单据
			showbilltrackpk: '',//联查单据pk
			showbilltracktype: '',//联查单据类型
			show: false,//审批意见是否显示
			billid: '',//审批意见单据pk
			billtype: '',//审批意见单据类型
			billno: '', // 单据编号
			billId: '',//单据pk
			showUploader: false,//控制附件弹出框
			target: null,//控制弹出位置
			tradebtnflag: true,
			pasteflag: false,//表体中按钮的显隐性状态
			tradetype: 'D4',
			tradename: this.props.MutiInit.getIntl("36070RBMAPP") && this.props.MutiInit.getIntl("36070RBMAPP").get('36070RBMAPP-000018'),/* 国际化处理： 收款结算单*/
			tradepk: '0000Z6000000000000F4',
			org_value: '',//切换组织取消使用
			org_display: '',//切换组织取消使用
			outputData: '',//打印输出使用
			tableindex: null
		};
		initTemplate.call(this, props);
	}

	componentDidMount() {
		console.log('===========联查=======', '审批');
		this.refresh();
	}
	//加载数据刷新数据
	refresh = () => {
		this.toggleShow();
		//查询单据详情
		if (this.props.getUrlParam('status') === 'browse') {

			if (this.props.getUrlParam('id') &&
				this.props.getUrlParam('id').length > 0) {
				let data = { pk: this.props.getUrlParam('id'), pageid: this.pageId };
				let that = this;
				ajax({
					url: '/nccloud/cmp/recbill/recbillquerycard.do',
					data: data,
					success: (res) => {
						if (res.data) {
							if (res.data.head) {
								this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
								//页签赋值
								let billno = res.data.head[this.formId].rows[0].values.bill_no.value;
								let source_flag = res.data.head[this.formId].rows[0].values.source_flag.value;
								this.setState({
									billno: billno

								});
								// this.source_flag(source_flag);
								setSourceFlag.call(this,source_flag);
							}
							if (res.data.body) {
								this.props.cardTable.setTableData(that.tableId, res.data.body[that.tableId]);
							}
						} else {
							this.props.form.EmptyAllFormValue(this.formId);
							this.props.cardTable.setTableData(this.tableId, { rows: [] });
						}
					}
				});
			}else{
				toast({ color: 'warning', content: this.props.MutiInit.getIntl("36070RBMAPP") && this.props.MutiInit.getIntl("36070RBMAPP").get('36070RBMAPP-000002') });/* 国际化处理： 操作失败，无数据!*/
			}
		}
	}
	//切换页面状态
	toggleShow = () => {
		let status = this.props.getUrlParam('status');
		this.props.cardTable.setStatus(this.tableId, 'edit');
		let flag = status === 'browse' ? false : true;
		this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', !flag);//设置看片翻页的显隐性
		//不存在id就隐藏翻页工具
		this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false);
		if (status == 'browse') {
			this.props.cardTable.setStatus(this.tableId, 'browse');
			this.props.form.setFormStatus(this.formId, 'browse');
		} else {
			this.props.cardTable.setStatus(this.tableId, 'edit');
			this.props.form.setFormStatus(this.formId, 'edit');
		}
		if (status != 'browse') {
			this.setState({
				tradebtnflag: false
			});
		}
		buttonVisable.call(this, this.props);//按钮的显隐性
	};
	//卡片返回按钮
	handleClick = () => {
		//先跳转列表
	}

	//按钮显示
	getButtonNames = (codeId) => {
		if (codeId === 'edit' || codeId === 'add' || codeId === 'save') {
			return 'main-button';
		} else {
			return 'secondary - button';
		}
	};
	//关闭审批意见页面
	closeApprove = () => {
		this.setState({
			show: false
		})
	}
	// 附件的关闭点击
	onHideUploader = () => {
		this.setState({
			showUploader: false
		})
	}

	//获取列表肩部信息,肩部按钮
	getTableHead = (buttons, tableId) => {
		let { createButton } = this.props.button;
		return (
			<div className="shoulder-definition-area">

				<div className="definition-icons">
					{this.props.cardTable.createBrowseIcons(tableId, {
						iconArr: ['close', 'open', 'max'],
						maxDestAreaId: 'finance-fts-commissionpayment-card'
					})}
					{/* 应用注册按钮 */}
					{this.props.button.createButtonApp({
						area: Templatedata.card_body,
						buttonLimit: 3,
						onButtonClick: buttonClick.bind(this),
						popContainer: document.querySelector('.header-button-area')
					})}

				</div>
			</div>
		);
	};
	render() {
		let { cardTable, form, button, ncmodal, cardPagination, editTable } = this.props;
		let buttons = this.props.button.getButtons();
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createCardPagination } = cardPagination;
		let { createEditTable } = editTable;
		let { createButton, createButtonApp } = button;
		let { createModal } = ncmodal;
		let { showUploader, target, billno, billId } = this.state;//附件相关内容变量
		return (
			<div className="nc-bill-card">
				<div className="nc-bill-top-area">
					<NCAffix>
						<Div areaCode={Div.config.HEADER}>
							<div className="nc-bill-header-area">
								<div className="header-title-search-area">
									<h2 className='title-search-detail'>
										{false && <NCBackBtn onClick={this.handleClick}></NCBackBtn>}
										{this.props.MutiInit.getIntl("36070RBMAPP") &&
											this.props.MutiInit.getIntl("36070RBMAPP").get('36070RBMAPP-000039')}
										{' : '}{this.state.billno}</h2></div>{/* 国际化处理： 收款结算*/}
								<div className="header-button-area">
									{/* 按钮适配 第三步:在页面的 dom 结构中创建按钮组，传入显示的区域，绑定按钮事件*/}
									{/* {createButtonApp("page_header", {onButtonClick: buttonClick.bind(this) })} */}
									{createButtonApp({
										area: Templatedata.card_head,
										buttonLimit: 14,
										onButtonClick: buttonClick.bind(this)
									})}
								</div>
							</div>
						</Div>
					</NCAffix>
					<NCScrollElement name='forminfo'>
						<div className="nc-bill-form-area">
							{createForm(this.formId, {
								expandArr: [jsondata.form1],
								onAfterEvent: afterEvent.bind(this)
							})}
						</div>
					</NCScrollElement>
				</div>
				<NCScrollElement name='businfo'>
					<Div field="shoulder1" areaCode={Div.config.SHOULDER}>
						<div className="nc-bill-bottom-area">
							<div className="nc-bill-table-area">
								{/* {this.getTableHead(buttons, this.tableId)} */}
								{createCardTable(this.tableId, {
									tableHead: this.getTableHead.bind(this, buttons, this.tableId),
									modelSave: this.saveBill,
									onAfterEvent: afterEvent.bind(this),
									showCheck: true
								})}
							</div>
						</div>
					</Div>
				</NCScrollElement>
			
				<div className="nc-faith-demo-div2">
					{/* 这里是附件上传组件的使用，需要传入三个参数 */}
					{showUploader &&
						<NCUploader
							billId={billId}
							target={target}
							placement={'bottom'}
							billNo={billno}
							onHide={this.onHideUploader}
							customInterface={
								{
									queryLeftTree: commonurl.lefttreequery,
									queryAttachments: Templatedata.annex_url
								}
							}//附件改造
						/>
					}
				</div>
				{/* 审批意见 */}
				<div>
					<ApproveDetail
						show={this.state.show}
						close={this.closeApprove}
						billtype={this.state.billtype}
						billid={this.state.billid}
					/>
				</div>
				{/* 联查单据 */}
				<div>
					<BillTrack
						show={this.state.showbilltrack}
						close={() => {
							this.setState({ showbilltrack: false })
						}}
						pk={this.state.showbilltrackpk}  //单据id
						type={this.state.showbilltracktype}  //单据类型
					/>
				</div>
				{/* 联查计划预算 */}
				<div>
					<Inspection
						show={this.state.showInspection}
						sourceData={this.state.sourceData}
						cancel={() => {
							this.setState({ showInspection: false })
						}}
						affirm={() => {
							this.setState({ showInspection: false })
						}}
					/>
				</div>
				{/* 打印输出 */}
				<div>
					<PrintOutput
						ref="printOutput"
						url='/nccloud/cmp/recbill/recbillprintcard.do'
						data={this.state.outputData}
						callback={this.onSubmit}
					/>
				</div>
			</div>
		);
	}
}

Card = createPage({
	mutiLangCode: "36070RBMAPP",
	billinfo: {
		billtype: 'card',//一主一子
		pagecode: Templatedata.card_pageid,
		headcode: Templatedata.card_formid,
		bodycode: Templatedata.card_tableid,
	},
	orderOfHotKey: [Templatedata.card_formid, Templatedata.card_tableid]//快捷键
})(Card);

// ReactDOM.render(<Card />, document.querySelector('#app'));
export default Card;
