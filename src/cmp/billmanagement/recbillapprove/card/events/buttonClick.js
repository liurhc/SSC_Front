import { ajax, base, toast, cacheTools, print, output } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";//配置的id和area信息
import { imageScan, imageView } from "../../../../../sscrp/public/common/components/imageMng";//影像处理
import { MakeBillApp } from '../../../../public/utils/Makebill';//制单
import { linkApp, linkVoucherApp } from '../../../../public/utils/LinkUtil';//凭证
export default function (props, id) {
  let page_Id = Templatedata.card_pageid;
  //财务组织
  let org_val = props.form.getFormItemsValue(this.formId, 'pk_org').value;
  let org_display = props.form.getFormItemsValue(this.formId, 'pk_org').display;
  //联查凭证
  let voucher_billtype = Templatedata.voucher_billtype;
  let voucher_calchekey = Templatedata.voucher_calchekey;
  let voucher_code = Templatedata.voucher_code;
  let voucher_appid = Templatedata.voucher_appid;
  let voucher_appcode = Templatedata.voucher_appcode;
  //制单
  let makebill_billtype = Templatedata.makebill_billtype;
  let makebill_cachekey = Templatedata.makebill_cachekey;
  let makebill_code = Templatedata.makebill_code;
  let makebill_appid = Templatedata.makebill_appid;
  let makebill_appcode = Templatedata.makebill_appcode;
  //关联结算信息
  let settle_code = Templatedata.settle_code;
  let settle_pageid = Templatedata.settle_pageid;
  let settle_callback = Templatedata.settle_callback;
  let settle_cachekey = Templatedata.settle_cachekey;
  let settle_appid = Templatedata.settle_appid;
  let settle_src = Templatedata.settle_src;
  let callback_appcode = Templatedata.callback_appcode;
  let callback_pagecode = Templatedata.callback_pagecode;
  //联查协同单据
  let synbill_src = Templatedata.synbill_paybillsrc;
  let synbill_key = Templatedata.synbill_cachekey;
  let synbill_paybillcode = Templatedata.synbill_paybillcode;
  let synbill_paybillappid = Templatedata.synbill_paybillappid;
  let synbill_pagecode = Templatedata.synbill_pagecode;
  //打印
  let printcard_billtype = Templatedata.printcard_billtype;
  let printcard_funcode = Templatedata.printcard_funcode;
  let printcard_nodekey = Templatedata.printcard_nodekey;
  let printcard_templetid = Templatedata.printcard_templetid;
  //审批意见
  let approve_billtype = Templatedata.approve_billtype;
  //联查单据
  let billtrack_billtype = Templatedata.billtrack_billtype;

  switch (id) {

    //刷新按钮
    case 'refreshBtn':
      this.refresh();
      break;

    //附件
    case 'annexBtn':
      let pk_rec_2 = props.form.getFormItemsValue(this.formId, 'pk_recbill').value;;//单据pk
      let bill_no_2 = props.form.getFormItemsValue(this.formId, 'bill_no').value;;//单据编号
      if (!pk_rec_2) {
        toast({ color: 'warning', content: this.props.MutiInit.getIntl("36070RBMAPP") && this.props.MutiInit.getIntl("36070RBMAPP").get('36070RBMAPP-000002') });/* 国际化处理： 操作失败，无数据!*/
        return;
      }
      this.setState({
        billId: pk_rec_2,//单据pk
        billno: bill_no_2,//附件管理使用单据编号
        showUploader: !this.state.showUploader,
        target: null
      })
      break;

    //打印
    case 'printBtn':
      if (!this.props.form.getFormItemsValue(this.formId, 'pk_recbill').value) {
        toast({ color: 'warning', content: this.props.MutiInit.getIntl("36070RBMAPP") && this.props.MutiInit.getIntl("36070RBMAPP").get('36070RBMAPP-000002') });/* 国际化处理： 操作失败，无数据!*/
        return;
      }
      if (props.form.getFormItemsValue(this.formId, 'trade_type').value) {
        printcard_billtype = props.form.getFormItemsValue(this.formId, 'trade_type').value;
      }
      debugger;
      print(
        'pdf',  //支持两类: 'html'为模板打印, 'pdf'为pdf打印
        '/nccloud/cmp/recbill/recbillprintcard.do',
        {
          nodekey: printcard_nodekey,     //模板节点标识：单据模版初始化
          appcode: printcard_funcode,
          oids: [this.props.form.getFormItemsValue(this.formId, 'pk_recbill').value]

        }
      );

      break;

    //输出
    case 'outputBtn':
      if (!this.props.form.getFormItemsValue(this.formId, 'pk_recbill').value) {
        toast({ color: 'warning', content: this.props.MutiInit.getIntl("36070RBMAPP") && this.props.MutiInit.getIntl("36070RBMAPP").get('36070RBMAPP-000002') });/* 国际化处理： 操作失败，无数据!*/
        return;
      }
      let oids = [];
      oids.push(this.props.form.getFormItemsValue(this.formId, 'pk_recbill').value);
      output({
        url: '/nccloud/cmp/recbill/recbillprintcard.do',
        data: {
          nodekey: printcard_nodekey,
          appcode: printcard_funcode,
          oids,
          outputType: 'output'
        }
      });
      break

    //联查单据  
    case 'linkquerybillBtn':
      if (!this.props.form.getFormItemsValue(this.formId, 'pk_recbill').value) {
        toast({ color: 'warning', content: this.props.MutiInit.getIntl("36070RBMAPP") && this.props.MutiInit.getIntl("36070RBMAPP").get('36070RBMAPP-000002') });/* 国际化处理： 操作失败，无数据!*/
        return;
      }
      let showbilltrackpk = this.props.form.getFormItemsValue(this.formId, 'pk_recbill').value;
      if (this.props.form.getFormItemsValue(this.formId, 'bill_type').value) {
        billtrack_billtype = this.props.form.getFormItemsValue(this.formId, 'bill_type').value;
      }
      if (showbilltrackpk) {
        this.setState({
          showbilltrack: true,//显示联查单据
          showbilltracktype: billtrack_billtype,//单据类型
          showbilltrackpk: showbilltrackpk//单据pk
        });
      }
      break
    //查看审批意见 
    case 'querymsgBtn':
      if (!this.props.form.getFormItemsValue(this.formId, 'pk_recbill').value) {
        toast({ color: 'warning', content: this.props.MutiInit.getIntl("36070RBMAPP") && this.props.MutiInit.getIntl("36070RBMAPP").get('36070RBMAPP-000002') });/* 国际化处理： 操作失败，无数据!*/
        return;
      }
      let billid = this.props.form.getFormItemsValue(this.formId, 'pk_recbill').value;
      if (this.props.form.getFormItemsValue(this.formId, 'trade_type').value) {
        approve_billtype = this.props.form.getFormItemsValue(this.formId, 'trade_type').value;
      }
      if (billid) {
        this.setState({
          show: true,
          billtype: approve_billtype,//单据类型
          billid: billid//单据pk
        });
      }
      break
    //联查凭证   
    case 'queryvoucherBtn':
      if (!this.props.form.getFormItemsValue(this.formId, 'pk_recbill').value) {
        toast({ color: 'warning', content: this.props.MutiInit.getIntl("36070RBMAPP") && this.props.MutiInit.getIntl("36070RBMAPP").get('36070RBMAPP-000002') });/* 国际化处理： 操作失败，无数据!*/
        return;
      }
      if (this.props.form.getFormItemsValue(this.formId, 'trade_type').value) {
        voucher_billtype = props.form.getFormItemsValue(this.formId, 'trade_type').value;
      }
      linkVoucherApp(
        props,
        this.props.form.getFormItemsValue(this.formId, 'pk_recbill').value,
        'nc.vo.cmp.bill.RecBillAggVO',
        voucher_appcode,
        voucher_billtype,
        this.props.form.getFormItemsValue(this.formId, 'bill_no').value,
      );


      break

    //联查计划预算
    case 'queryconsumeBtn':


      if (!props.form.getFormItemsValue(this.formId, 'pk_recbill').value) {
        toast({ color: 'warning', content: this.props.MutiInit.getIntl("36070RBMAPP") && this.props.MutiInit.getIntl("36070RBMAPP").get('36070RBMAPP-000013') });/* 国际化处理： 无数据，无法进行操作!*/
        return;
      }
      let queryconsume_pk = props.form.getFormItemsValue(this.formId, 'pk_recbill').value;
      console.log(queryconsume_pk);
      debugger;
      let queryconsume_data = { pk: queryconsume_pk, pageid: this.pageId };
      console.log(queryconsume_data);
      ajax({
        url: '/nccloud/cmp/recbill/linkplan.do',
        data: queryconsume_data,
        success: (res) => {
          let { success, data } = res;
          if (res.data) {
            if (res.data.hint && res.data.hint.length > 0) {
              toast({ color: 'warning', content: res.data.hint });
              return;
            } else {
              this.setState({
                showInspection: true,
                sourceData: res.data
              });
            }
          }
        }
      });
      break;
    //联查协同单据
    case 'querysynbillBtn':

      let querysynbillArr = [];

      let querysynbillBtn_pk_upbill, querysynbillBtn_pk_recbill
      if (props.form.getFormItemsValue(this.formId, 'pk_recbill').value) {
        querysynbillBtn_pk_recbill = props.form.getFormItemsValue(this.formId, 'pk_recbill').value;
        querysynbillArr.push(querysynbillBtn_pk_recbill);//上后主键
      }
      if (props.form.getFormItemsValue(this.formId, 'pk_upbill ').value) {
        querysynbillBtn_pk_upbill = props.form.getFormItemsValue(this.formId, 'pk_upbill ').value;
        querysynbillArr.push(querysynbillBtn_pk_upbill);//上后主键
      }

      if (!(querysynbillBtn_pk_upbill && querysynbillBtn_pk_recbill)) {
        toast({ color: 'warning', content: this.props.MutiInit.getIntl("36070RBMAPP") && this.props.MutiInit.getIntl("36070RBMAPP").get('36070RBMAPP-000002') });/* 国际化处理： 操作失败，无数据!*/
        return;
      }

      let confirmdate = {
        pks: querysynbillArr
      }
      ajax({
        url: '/nccloud/cmp/recbill/linkbillconfirm.do',
        data: confirmdate,
        success: (res) => {
          let { success, data } = res;
          if (success) {
            //处理选择数据
            cacheTools.set(synbill_key, querysynbillArr);
            let querysynbillBtn_appOption = {
              code: synbill_paybillcode,
              name: this.props.MutiInit.getIntl("36070RBMAPP") && this.props.MutiInit.getIntl("36070RBMAPP").get('36070RBMAPP-000014'),/* 国际化处理： 付款结算管理*/
              pk_appregister: synbill_paybillappid
            };
            let querysynbillBtn_type = {
              type: null
            };
            let querysynbillBtn_query = {
              status: 'browse',
              src: synbill_src,
              callback: '1'
            }
            props.openTo('/cmp/billmanagement/paybill/linkcard/index.html',
              {
                appcode: synbill_paybillcode,
                pagecode: synbill_pagecode,
                status: 'browse',
                src: synbill_src,
                name: this.props.MutiInit.getIntl("36070RBMAPP") && this.props.MutiInit.getIntl("36070RBMAPP").get('36070RBMAPP-000015'),/* 国际化处理： 付款结算联查*/
              });
          }
        }
      });

      break
    //附件
    case 'cardannexBtn':
      let pk_rec = props.form.getFormItemsValue(this.formId, 'pk_recbill').value;;//单据pk
      let bill_no = props.form.getFormItemsValue(this.formId, 'bill_no').value;;//单据编号
      this.setState({
        billId: pk_rec,//单据pk
        billno: bill_no,//附件管理使用单据编号
        showUploader: !this.state.showUploader,
        target: null
      })
      break;
    //影像查看

    case 'imageviewBtn':

        let showData = this.props.createMasterChildData(this.pageId, this.formId, this.tableId);
        let billShowInfoMap = {};
        let openShowbillid = this.props.form.getFormItemsValue(this.formId, 'pk_recbill').value;
        if (!openShowbillid) {
          toast({ color: 'warning', content: this.props.MutiInit.getIntl("36070RBMAPP") && this.props.MutiInit.getIntl("36070RBMAPP").get('36070RBMAPP-000002') });/* 国际化处理： 操作失败，无数据!*/
        }
        billShowInfoMap['pk_billid'] = openShowbillid;
        billShowInfoMap['pk_billtype'] = this.props.form.getFormItemsValue(this.formId, 'pk_billtypeid').value;
        billShowInfoMap['pk_tradetype'] = this.props.form.getFormItemsValue(this.formId, 'trade_type').value;
        billShowInfoMap['pk_org'] = this.props.form.getFormItemsValue(this.formId, 'pk_org').value;
        //查询数据
        imageView(billShowInfoMap, 'iweb');

      break;
    //影像扫描
    case 'imagescanBtn':

        let ScanData = this.props.createMasterChildData(this.pageId, this.formId, this.tableId);
        let openbillid = this.props.form.getFormItemsValue(this.formId, 'pk_recbill').value;
        if (!openbillid) {
          toast({ color: 'warning', content: this.props.MutiInit.getIntl("36070RBMAPP") && this.props.MutiInit.getIntl("36070RBMAPP").get('36070RBMAPP-000002') });/* 国际化处理： 操作失败，无数据!*/
        }
        let billInfoMap = {};
        billInfoMap['pk_billid'] = openbillid;
        billInfoMap['pk_billtype'] = ScanData.head[this.formId].rows[0].values.pk_billtypeid.value;
        billInfoMap['pk_tradetype'] = ScanData.head[this.formId].rows[0].values.trade_type.value;;
        billInfoMap['pk_org'] = ScanData.head[this.formId].rows[0].values.pk_org.value;;
        billInfoMap['BillType'] = ScanData.head[this.formId].rows[0].values.bill_type.value;
        billInfoMap['BillDate'] = ScanData.head[this.formId].rows[0].values.creationtime.value;
        billInfoMap['Busi_Serial_No'] = ScanData.head[this.formId].rows[0].values.pk_recbill.value;
        billInfoMap['OrgNo'] = ScanData.head[this.formId].rows[0].values.pk_org.value;
        billInfoMap['BillCode'] = ScanData.head[this.formId].rows[0].values.bill_no.value;
        billInfoMap['OrgName'] = ScanData.head[this.formId].rows[0].values.pk_org.value;
        billInfoMap['Cash'] = ScanData.head[this.formId].rows[0].values.primal_money.value;

        imageScan(billInfoMap, 'iweb');
      break;
    default:
      break
  }
}
