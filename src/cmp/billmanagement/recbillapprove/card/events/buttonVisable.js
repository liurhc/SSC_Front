//修改页面状态--button的显隐性
import { createPage, ajax, base, toast } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";//配置的id和area信息

let tableId = Templatedata.card_tableid;
let formId = Templatedata.card_formid;
let page_id = Templatedata.card_pageid;

export const buttonVisable = function (props) {

    console.log(props);//打印日志。
    let status = props.getUrlParam('status');
    let billstatus = props.getUrlParam('billno');//获取单据状态
    let pasteflag = this.state.pasteflag || false;//表体肩部按钮
    if(status == 'browse'){
        this.setState({
            tradebtnflag: true//收款交易类型
        });
    }
    //审批直接显示按钮：
    //联查（单据、计划预算）、打印、附件管理
            props.button.setButtonVisible(
                [
                    'linkquerybillBtn',
                    'queryconsumeBtn',
                    'linkquery',
                    'printgroup',
                    'printBtn',
                    'outputBtn',
                    'annexgroup',
                    'annexBtn'
                ], true);

}
