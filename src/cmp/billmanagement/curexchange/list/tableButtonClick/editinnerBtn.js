import { createPage, ajax, base, high, toast, cardCache } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";
import { BatchToast } from '../../../../public/CMPMessage.js';
//缓存
let { setDefData, getDefData,
    getCurrentLastId, getCacheById,
    updateCache, addCache, getNextId,
    deleteCacheById } = cardCache;

/**
 * [外币兑换]-编辑按钮
 * @param {*} props  
 */
export const editinnerBtn = function (record, index) {

    this.props.pushTo(Templatedata.tablebutton_editinnerBtn, {
        status: 'edit',
        id: record.pk_cruexchange.value,
        pagecode: this.pageCode
    });

}
