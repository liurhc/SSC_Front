/**
 * [外币兑换]-编辑修改按钮
 * @param {*} props  
 */
export const cardEditBtn = function () {
    
    //1,直接走后台查询一边数据即可
    this.props.pushTo('/card', {
        status: 'edit',
        id: this.props.getUrlParam('id'),
        bill_no: this.props.form.getFormItemsValue(this.formId, 'busistatus').value,
        pagecode: this.pageId
    })
    this.refresh();
    //2,不走后台直接修改页面状态即可
    //未执行
}
