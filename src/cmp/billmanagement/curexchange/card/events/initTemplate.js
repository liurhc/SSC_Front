import { base, ajax } from 'nc-lightapp-front';
import refer from './refer';//refer参照
import { Templatedata } from "../../config/Templatedata";//配置的id和area信息
import { buttonVisable } from "./buttonVisable";//按钮显隐性
import { orgVersionUtil } from "../../util/orgVersionUtil";//多版本显示
let { NCPopconfirm } = base;
const { NCMessage } = base;
const formId = Templatedata.card_formid;
const tableId = Templatedata.card_tableid;
const pageId = Templatedata.card_pageid;
const appid = Templatedata.card_appid;
const buyform = Templatedata.buyform;
const sellform = Templatedata.sellform;
const resultform = Templatedata.resultform;
const chargeform = Templatedata.chargeform;
const printcard_funcode = Templatedata.printcard_funcode;
const appcode = Templatedata.app_code;
export default function (props) {
	let self = this;
	props.createUIDom(
		{
			pagecode: pageId,//页面id
			appcode: appcode,//小应用code
			appid: appid//注册按钮的id
		},
		function (data) {
			if (data) {
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.template) {
					let meta = data.template;
					// modifierMeta(self,props, meta);
					meta[formId].items.forEach(item=>{
						if(item.attrcode==="busitype"){ //存业务类型初始值
							self.val = item.initialvalue&&item.initialvalue.value||"EXCHANGE"
						}
					})
					modifierMeta.call(self, props, meta);
					// props.meta.setMeta(meta);
					props.meta.setMeta(meta, () => {
						if (props.getUrlParam('status') == 'add') {
							props.resMetaAfterPkorgEdit();
							props.initMetaByPkorg();//单据有主组织，新增时,将其他字段设置为不可编辑. 
						}
					});
					self.refresh();//加载数据,第一次加载数据使用
					if (props.getUrlParam('status') === 'browse') {
						props.form.setFormStatus(formId, 'browse');
					} else {
						props.form.setFormStatus(formId, 'edit');
					}
				}
				//修改页面状态
				togglePageShow.call(self, props);
			}
		}
	)
}

//根据页面状态，修改编辑态表格
function togglePageShow(props) {
	let status = props.getUrlParam('status');
	let billstatus = props.getUrlParam('billno');//获取单据状态
	let flag = status === 'browse' ? false : true;
	orgVersionUtil.call(this, props, this.formId)//多版本视图显隐性
	buttonVisable.call(this, props);//控制按钮显隐性	
}

function modifierMeta(props, meta) {
	let self = this;
	let status = props.getUrlParam('status');
	//防止复制的卡片字段错乱
	if (status === 'copy') {
		meta[formId].status = 'edit';
	} else {
		meta[formId].status = status;
	}

	//参照过滤《分组后只能指定form过滤》
	//买入信息:买入账户过滤
	meta[buyform].items.map((item) => {
		//买入账户
		if (item.attrcode == 'pk_buyacct') {
			item.queryCondition = () => {
				let org_Val = props.form.getFormItemsValue(formId, 'pk_org');
				if (org_Val && org_Val.value) {
					org_Val = org_Val.value;
				} else {
					org_Val = null;
				}
				let buycurr_Val = props.form.getFormItemsValue(formId, 'pk_buycurrtype');//币种过滤暂不支持
				if (buycurr_Val && buycurr_Val.value) {
					buycurr_Val = buycurr_Val.value;
				} else {
					buycurr_Val = null;
				}
				let bill_date = props.form.getFormItemsValue(formId, 'billdate').value;//单据日期，业务日期
				return {
					pk_org: org_Val,
					bill_date: bill_date,
					pk_currtype: buycurr_Val,
					refnodename: '使用权参照',/* 国际化处理： 使用权参照*/
					isDisableDataShow: false,//默认只加载启用的账户
					noConditionOrg: 'N',
					GridRefActionExt: 'nccloud.web.cmp.ref.CMPBankaccGridRefNotInnerAccSqlBuilder'//自定义增加的过滤条件
				};
			};
		}
	});

	meta[sellform].items.map((item) => {
		//卖出账户
		if (item.attrcode == 'pk_sellacct') {
			item.queryCondition = () => {
				let org_sell_Val = props.form.getFormItemsValue(formId, 'pk_org');
				if (org_sell_Val && org_sell_Val.value) {
					org_sell_Val = org_sell_Val.value;
				} else {
					org_sell_Val = null;
				}
				let sellcurr_Val = props.form.getFormItemsValue(formId, 'pk_sellcurrtype');
				if (sellcurr_Val && sellcurr_Val.value) {
					sellcurr_Val = sellcurr_Val.value;
				} else {
					sellcurr_Val = null;
				}
				let bill_date = props.form.getFormItemsValue(formId, 'billdate').value;//单据日期，业务日期
				return {
					pk_org: org_sell_Val,
					bill_date: bill_date,
					pk_currtype: sellcurr_Val,
					refnodename: '使用权参照',/* 国际化处理： 使用权参照*/
					isDisableDataShow: false,//默认只加载启用的账户
					noConditionOrg: 'N',
					GridRefActionExt: 'nccloud.web.cmp.ref.CMPRecBillBankaccSubDefaultGridRefSqlBuilder'//自定义增加的过滤条件
				};
			};
		}
	});

	meta[chargeform].items.map((item) => {
		//手续费账户
		if (item.attrcode == 'pk_paychargeacct') {
			item.queryCondition = () => {
				let org_charge_Val = props.form.getFormItemsValue(formId, 'pk_org');
				if (org_charge_Val && org_charge_Val.value) {
					org_charge_Val = org_charge_Val.value;
				} else {
					org_charge_Val = null;
				}
				let chargecurr_Val = props.form.getFormItemsValue(formId, 'pk_chargecurrtype');
				if (chargecurr_Val && chargecurr_Val.value) {
					chargecurr_Val = chargecurr_Val.value;
				} else {
					chargecurr_Val = null;
				}
				let bill_date = props.form.getFormItemsValue(formId, 'billdate').value;//单据日期，业务日期
				return {
					pk_org: org_charge_Val,
					bill_date: bill_date,
					pk_currtype: chargecurr_Val,
					refnodename: '使用权参照',/* 国际化处理： 使用权参照*/
					isDisableDataShow: false,//默认只加载启用的账户
					noConditionOrg: 'N',
					GridRefActionExt: 'nccloud.web.cmp.ref.CMPRecBillBankaccSubDefaultGridRefSqlBuilder'//自定义增加的过滤条件
				};
			};
		}

	});
	//买入币种
	meta[buyform].items.map((item) => {
		// 币种根据：卖出币种过滤
		if (item.attrcode == 'pk_buycurrtype') {
			item.queryCondition = () => {
				let sellCode = props.form.getFormItemsValue(formId, 'pk_sellcurrtype');
				if (sellCode && sellCode.value) {
					sellCode = sellCode.value;
				} else {
					sellCode = null;
				}
				return {
					codePk: sellCode,
					GridRefActionExt: 'nccloud.web.cmp.ref.CMPCurexchangeCurrtypeGridRefSqlBuilder'
				};
			};
		}
	});
	//卖出币种
	meta[sellform].items.map((item) => {
		// 币种根据：买入币种过滤
		if (item.attrcode == 'pk_sellcurrtype') {
			item.queryCondition = () => {
				let buyCode = props.form.getFormItemsValue(formId, 'pk_buycurrtype');
				if (buyCode && buyCode.value) {
					buyCode = buyCode.value;
				} else {
					buyCode = null;
				}
				return {
					codePk: buyCode,
					GridRefActionExt: 'nccloud.web.cmp.ref.CMPCurexchangeCurrtypeGridRefSqlBuilder'
				};
			};
		}
	});
	meta[formId].items.map((item) => {
		// 发送发组织，接收方组织：根据用户权限过滤
		if (item.attrcode == 'pk_org') {
			item.queryCondition = () => {
				return {
					funcode: printcard_funcode,
					TreeRefActionExt: 'nccloud.web.tmpub.filter.FinanceOrgPermissionFilter'
				};
			};
		}
	});
	//财务组织:全加载
	meta[formId].items.find((e) => e.attrcode === 'pk_org').isTreelazyLoad = false;
	//设置参照refer-<前台自定义-目前取消这种方式>
	// props.renderItem('form', formId, 'pk_buycurrtype', refer('pk_buycurrtype'));
	// props.renderItem('form', formId, 'pk_sellcurrtype', refer('pk_sellcurrtype'));
	// props.renderItem('form', formId, 'pk_org', refer('pk_org'));
	// props.renderItem('form', formId, 'pk_buyacct', refer('pk_buyacct'));
	// props.renderItem('form', formId, 'pk_sellacct', refer('pk_sellacct'));
	// props.renderItem('form', formId, 'pk_chargecurrtype', refer('pk_chargecurrtype'));
	// props.renderItem('form', formId, 'pk_paychargeacct', refer('pk_paychargeacct'));
	return meta;
}
