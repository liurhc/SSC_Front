import { ajax } from 'nc-lightapp-front';
import { changeOrgShow } from "../buttonClick/changeOrgShow";
import { judgeCurrtype } from "./judgeCurrtype";


export default function afterEvent(props, moduleId, key, value, changedrows, i) {

	//相同值不变化--不同值可以进行编辑后
	let check_init = props.createFormAfterEventData(this.pageId, moduleId, moduleId, key, value);
	if (check_init.oldvalue.value == check_init.newvalue.value) {
		return;
	}
	//点选财务组织，编辑后事件
	if (key === 'pk_org') {
		let testorg = props.createFormAfterEventData(this.pageId, moduleId, moduleId, key, value);
		//禁止参照差选和取消调用
		if (testorg.oldvalue.value == null && testorg.newvalue.value == null) {
			return;
		}
		if (value.value == null || value.display == null) {
			//叉选组织/清空组织的时候调用	
			changeOrgShow.call(this);
		} else {
			props.resMetaAfterPkorgEdit();//选择主组织以后，恢复其他字段的编辑性
		}
		if (value.value && Object.keys(value.value).length != 0) {
			let orgdata = props.createFormAfterEventData(this.pageId, moduleId, moduleId, key, value);
			let org_new = orgdata.newvalue.value;//新输入值
			let org_new_dly = orgdata.newvalue.display;//新输入值
			let org_old = orgdata.oldvalue.value;//修改之前的值
			let org_old_dly = orgdata.oldvalue.display;//修改之前的显示值
			if (org_new != org_old) {
				if (org_old != null) {
					//叉选组织后取消后还要赋值
					this.setState({
						org_value: org_old,
						org_display: org_old_dly
					});
				} else if (org_old == null && org_new != null) {
					this.setState({
						org_value: org_new,
						org_display: org_new_dly
					});
				}
				//先清空之前做的修改
				if (org_old != null && org_old_dly != null) {
					//首次填写财务组织，不进行清空				
					changeOrgShow.call(this);
				} else {
					// props.resMetaAfterPkorgEdit();//选择主组织以后，恢复其他字段的编辑性
					//编辑后渲染数据
					ajax({
						url: '/nccloud/cmp/curexchange/curexchangeorgafterevent.do',
						data: orgdata,
						success: (res) => {
							if (res.success) {
								// props.form.EmptyAllFormValue(moduleId);
								// props.form.setFormStatus(moduleId, 'edit');
								//后台只进行了相关数据操作
								props.form.setAllFormValue({ [moduleId]: res.data[moduleId] });
								//手续费汇率精度处理
								let chargerate = res.data[moduleId].rows[0].values.chargeolcrate.value;
								if (chargerate) {
									// chargerate = parseFloat(chargerate).toFixed(5);
									props.form.setFormItemsValue(moduleId, { 'chargeolcrate': { display: chargerate, value: chargerate } });
								}

								this.props.resMetaAfterPkorgEdit();//选择主组织以后，恢复其他字段的编辑性
							}
						}
					});
				}

			}
		} else {
			props.initMetaByPkorg();//单据有主组织，新增时,将其他字段设置为不可编辑. 
		}
	}
	//编辑后----->业务类型
	else if (key === 'busitype') {
		//业务类型：1，买入外汇；2，卖出外汇；3，外币兑换
		let busitype = props.form.getFormItemsValue(moduleId, 'busitype').value;
		//[买入外汇]
		//设置编辑属性
		//1，PK_BUYCURRTYPE可以进行编辑
		//2，BUYOLCRATE可以进行编辑
		//3,PK_SELLCURRTYPE不可以进行编辑
		if (busitype === 'BUY') {
			props.form.setFormItemsDisabled(moduleId, { 'pk_sellcurrtype': true });//卖出币种
			props.form.setFormItemsDisabled(moduleId, { 'sellolcrate': true });//卖出本币汇率
			props.form.setFormItemsDisabled(moduleId, { 'sellolcamount': true });//卖出本币金额
			props.form.setFormItemsDisabled(moduleId, { 'pk_buycurrtype': false });//买入币种
			props.form.setFormItemsDisabled(moduleId, { 'buyolcrate': false });//买入本币汇率
			//卖出信息赋值
			let buytypedata = props.createFormAfterEventData(this.pageId, moduleId, moduleId, key, value);
			let buytype_new = buytypedata.newvalue.value;
			let buytype_old = buytypedata.oldvalue.value;
			let buytype_data = {
				key: key,
				form: buytypedata
			}
			if (buytype_new != buytype_old) {

				ajax({
					url: '/nccloud/cmp/curexchange/afterHandler.do',
					data: buytype_data,
					success: (res) => {
						let { success, data } = res;
						if (success) {
							props.form.setAllFormValue({ [this.formId]: res.data[this.formId] });
						}
					}
				});
			}

		}
		//[卖出外汇]
		//设置编辑属性
		//1,PK_SELLCURRTYPE可以编辑
		//2,SELLOLCRATE可以编辑
		//3,PK_BUYCURRTYPE不可以进行编辑
		if (busitype === 'SELL') {

			props.form.setFormItemsDisabled(moduleId, { 'pk_buycurrtype': true });
			props.form.setFormItemsDisabled(moduleId, { 'buyolcrate': true });
			props.form.setFormItemsDisabled(moduleId, { 'buyolcamount': true });
			props.form.setFormItemsDisabled(moduleId, { 'pk_sellcurrtype': false });
			props.form.setFormItemsDisabled(moduleId, { 'sellolcrate': true });
			props.form.setFormItemsDisabled(moduleId, { 'sellolcamount': true });
			//卖出外汇
			let selltypedata = props.createFormAfterEventData(this.pageId, moduleId, moduleId, key, value);
			let selltype_new = selltypedata.newvalue.value;
			let selltype_old = selltypedata.oldvalue.value;
			let selltype_data = {
				key: key,
				form: selltypedata
			}
			if (selltype_new != selltype_old) {

				ajax({
					url: '/nccloud/cmp/curexchange/afterHandler.do',
					data: selltype_data,
					success: (res) => {
						let { success, data } = res;
						if (success) {
							props.form.setAllFormValue({ [this.formId]: res.data[this.formId] });
						}
					}
				});
			}


		}
		//[外币兑换]
		//1,PK_BUYCURRTYPE可以编辑
		//2,PK_SELLCURRTYPE可以编辑
		//3,BUYOLCRATE可以编辑
		//4,SELLOLCRATE可以编辑
		//5,PK_BUYCURRTYPE设置为null，并且调用此编辑后事件
		//6,PK_SELLCURRTYPE设置为null，并且调用此编辑后事件
		//7,GAINORLOSS设置为null
		if (busitype === 'EXCHANGE') {
			//编辑性设置
			props.form.setFormItemsDisabled(moduleId, { 'pk_buycurrtype': false });
			props.form.setFormItemsDisabled(moduleId, { 'buyolcrate': false });
			props.form.setFormItemsDisabled(moduleId, { 'pk_buyacct': false });
			props.form.setFormItemsDisabled(moduleId, { 'buyamount': false });
			props.form.setFormItemsDisabled(moduleId, { 'tradeprice': false });
			props.form.setFormItemsDisabled(moduleId, { 'pk_sellcurrtype': false });
			props.form.setFormItemsDisabled(moduleId, { 'sellolcrate': false });
			props.form.setFormItemsDisabled(moduleId, { 'pk_sellacct': false });
			props.form.setFormItemsDisabled(moduleId, { 'sellamount': false });

			let exchangedata = props.createFormAfterEventData(this.pageId, moduleId, moduleId, key, value);
			let exchange_new = exchangedata.newvalue.value;
			let exchange_old = exchangedata.oldvalue.value;
			let exchtype_data = {
				key: key,
				form: exchangedata
			}
			if (exchange_new != exchange_old) {
				ajax({
					url: '/nccloud/cmp/curexchange/afterHandler.do',
					data: exchtype_data,
					success: (res) => {
						let { success, data } = res;
						if (success) {
							props.form.setAllFormValue({ [this.formId]: res.data[this.formId] });
						}
					}
				});
			}
		}
	}
	//编辑后----->卖出金额
	else if (key === 'sellamount') {

		let afterdata = props.createFormAfterEventData(this.pageId, moduleId, moduleId, key, value);
		let newvalue = afterdata.newvalue.value;
		let oldvalue = afterdata.oldvalue.value;
		let formdata = {
			key: key,
			form: afterdata
		}
		if (newvalue != oldvalue) {

			ajax({
				url: '/nccloud/cmp/curexchange/afterHandler.do',
				data: formdata,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						props.form.setAllFormValue({ [this.formId]: res.data[this.formId] });
					}
				}
			});
		}

	}
	//编辑后----->买入金额
	else if (key === 'buyamount') {
		let afterdata = props.createFormAfterEventData(this.pageId, moduleId, moduleId, key, value);
		let newvalue = afterdata.newvalue.value;
		let oldvalue = afterdata.oldvalue.value;
		let formdata = {
			key: key,
			form: afterdata
		}
		if (newvalue != oldvalue) {

			ajax({
				url: '/nccloud/cmp/curexchange/afterHandler.do',
				data: formdata,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						props.form.setAllFormValue({ [this.formId]: res.data[this.formId] });
					}
				}
			});
		}
	}
	//编辑后----->手续费金额
	else if (key === 'chargeamount') {
		let afterdata = props.createFormAfterEventData(this.pageId, moduleId, moduleId, key, value);
		let newvalue = afterdata.newvalue.value;
		let oldvalue = afterdata.oldvalue.value;
		let formdata = {
			key: key,
			form: afterdata
		}
		if (newvalue != oldvalue) {

			ajax({
				url: '/nccloud/cmp/curexchange/afterHandler.do',
				data: formdata,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						props.form.setAllFormValue({ [this.formId]: res.data[this.formId] });
					}
				}
			});
		}
	}
	//编辑后----->交易价
	else if (key === 'tradeprice') {
		let afterdata = props.createFormAfterEventData(this.pageId, moduleId, moduleId, key, value);
		let newvalue = afterdata.newvalue.value;
		let oldvalue = afterdata.oldvalue.value;
		let formdata = {
			key: key,
			form: afterdata
		}
		if (newvalue != oldvalue) {

			ajax({
				url: '/nccloud/cmp/curexchange/afterHandler.do',
				data: formdata,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						props.form.setAllFormValue({ [this.formId]: res.data[this.formId] });
					}
				}
			});
		}

	}
	//编辑后----->买入币种
	else if (key === 'pk_buycurrtype') {
		//显隐性控制
		//本币币种
		let local_currtype = props.form.getFormItemsValue(moduleId, 'pk_chargecurrtype').value;
		let buy_currtype = props.form.getFormItemsValue(moduleId, 'pk_buycurrtype').value;
		//控制显隐性
		if (local_currtype != buy_currtype) {
			props.form.setFormItemsDisabled(moduleId, { 'buyolcrate': false });
		} else {
			props.form.setFormItemsDisabled(moduleId, { 'buyolcrate': true });
		}

		let afterdata = props.createFormAfterEventData(this.pageId, moduleId, moduleId, key, value);
		let newvalue = afterdata.newvalue.value;
		let oldvalue = afterdata.oldvalue.value;
		let formdata = {
			key: key,
			form: afterdata
		}
		if (newvalue != oldvalue) {

			ajax({
				url: '/nccloud/cmp/curexchange/afterHandler.do',
				data: formdata,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						props.form.setAllFormValue({ [this.formId]: res.data[this.formId] });
						//判断所选币种是否本币币种
						judgeCurrtype.call(
							this,
							res.data[this.formId].rows[0].values.pk_org.value,
							res.data[this.formId].rows[0].values.pk_buycurrtype.value,
							'buyolcrate'
						);
					}
				}
			});
		}
	}
	//编辑后----->卖出币种
	else if (key === 'pk_sellcurrtype') {
		//显隐性控制
		let local_currtype = props.form.getFormItemsValue(moduleId, 'pk_chargecurrtype').value;
		let sell_currtype = props.form.getFormItemsValue(moduleId, 'pk_sellcurrtype').value;
		//币种等于组织本币的设置汇率显隐性
		if (local_currtype != sell_currtype) {
			props.form.setFormItemsDisabled(moduleId, { 'sellolcrate': false });
		} else {
			props.form.setFormItemsDisabled(moduleId, { 'sellolcrate': true });
		}
		let afterdata = props.createFormAfterEventData(this.pageId, moduleId, moduleId, key, value);
		let newvalue = afterdata.newvalue.value;
		let oldvalue = afterdata.oldvalue.value;
		let formdata = {
			key: key,
			form: afterdata
		}
		if (newvalue != oldvalue) {

			ajax({
				url: '/nccloud/cmp/curexchange/afterHandler.do',
				data: formdata,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						props.form.setAllFormValue({ [this.formId]: res.data[this.formId] });
						//判断所选币种是否本币币种
						judgeCurrtype.call(
							this,
							res.data[this.formId].rows[0].values.pk_org.value,
							res.data[this.formId].rows[0].values.pk_sellcurrtype.value,
							'sellolcrate'
						);
					}
				}
			});
		}
	}
	//编辑后----->手续费币种
	else if (key === 'pk_chargecurrtype') {
		let afterdata = props.createFormAfterEventData(this.pageId, moduleId, moduleId, key, value);
		let newvalue = afterdata.newvalue.value;
		let oldvalue = afterdata.oldvalue.value;
		let formdata = {
			key: key,
			form: afterdata
		}
		if (newvalue != oldvalue) {

			ajax({
				url: '/nccloud/cmp/curexchange/afterHandler.do',
				data: formdata,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						props.form.setAllFormValue({ [this.formId]: res.data[this.formId] });
						//判断所选币种是否本币币种
						judgeCurrtype.call(
							this,
							res.data[this.formId].rows[0].values.pk_org.value,
							res.data[this.formId].rows[0].values.pk_chargecurrtype.value,
							'chargeolcrate'
						);
					}
				}
			});
		}
	}
	//编辑后----->买入本币汇率
	else if (key === 'buyolcrate') {
		let afterdata = props.createFormAfterEventData(this.pageId, moduleId, moduleId, key, value);
		let newvalue = afterdata.newvalue.value;
		let oldvalue = afterdata.oldvalue.value;
		let formdata = {
			key: key,
			form: afterdata
		}
		if (newvalue != oldvalue) {

			ajax({
				url: '/nccloud/cmp/curexchange/afterHandler.do',
				data: formdata,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						props.form.setAllFormValue({ [this.formId]: res.data[this.formId] });
					}
				}
			});
		}
	}
	//编辑后----->卖出本币汇率
	else if (key === 'sellolcrate') {
		let afterdata = props.createFormAfterEventData(this.pageId, moduleId, moduleId, key, value);
		let newvalue = afterdata.newvalue.value;
		let oldvalue = afterdata.oldvalue.value;
		let formdata = {
			key: key,
			form: afterdata
		}
		if (newvalue != oldvalue) {

			ajax({
				url: '/nccloud/cmp/curexchange/afterHandler.do',
				data: formdata,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						props.form.setAllFormValue({ [this.formId]: res.data[this.formId] });
					}
				}
			});
		}
	}
}
