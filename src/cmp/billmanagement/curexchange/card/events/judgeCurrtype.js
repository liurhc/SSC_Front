import { createPage, ajax, base, high, toast, cardCache, print, output } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";
//缓存
let { setDefData, getDefData,
    getCurrentLastId, getCacheById,
    updateCache, addCache, getNextId,
    deleteCacheById } = cardCache;

/**
 * [外币兑换]-判断所选币种是否本币币种
 *  @param {*} pk_org 财务组织 
 *  @param {*} pk_currtype 币种
 *  @param {*} key 本币汇率key
 */
export const judgeCurrtype = function (pk_org, pk_currtype, key) {
    //====判断是否选择的本币币种,从而设置本币汇率编辑性=======
    let data = {
        pk_currtype: pk_currtype,
        pk_org: pk_org
    }
    ajax({
        url: '/nccloud/cmp/curexchange/judgecurrtype.do',
        data: data,
        success: (res) => {
            if (res.success) {
                if (res.data) {
                    //币种是组织本币币种
                    this.props.form.setFormItemsDisabled(this.formId, { key: true });
                } else {
                    //组织不是本币币种
                    this.props.form.setFormItemsDisabled(this.formId, { key: false });
                }
            }
        }
    });
}
