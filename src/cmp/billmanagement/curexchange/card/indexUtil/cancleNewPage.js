import { createPage, ajax, base, high, toast, cardCache } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";
import { BatchToast } from '../../../../public/CMPMessage.js';
//缓存
let { setDefData, getDefData,
    getCurrentLastId, getCacheById,
    updateCache, addCache, getNextId,
    deleteCacheById } = cardCache;

/**
 * [外币兑换index]-取消---跳转浏览态页面
 * 
 * @param status:单据状态
 * @param pk:跳转数据pk
 */
export const cancleNewPage = function (pk, status) {
    this.props.pushTo('/card', {
        status: 'browse',
        id: pk,
        pk: status,
        pagecode: this.pageId
    })
    this.refresh();
}
