import { createPage, ajax, base, high, toast, cardCache } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";
import { BatchToast } from '../../../../public/CMPMessage.js';
import { resetEditData } from "./resetEditData";
//缓存
let { setDefData, getDefData,
    getCurrentLastId, getCacheById,
    updateCache, addCache, getNextId,
    deleteCacheById } = cardCache;

/**
 * [外币兑换index]-组织改变数据
 * @param {*}  
 */
export const changeOrgConfirm = function () {
    //恢复之前的值，设置edit状态
    this.props.form.cancel(this.formId);
    resetEditData.call(this);
    this.props.form.setFormStatus(this.formId, 'edit');
    let org_data = this.props.createFormAfterEventData(this.pageId, this.formId, this.formId, 'pk_org', this.value);
    org_data.newvalue = org_data.oldvalue;//使用上述方法,newvalue不存在值
    let newvalue = org_data.newvalue;
    let test = this.props.form.getFormItemsValue(this.formId, 'pk_org').value;
    if (newvalue.value == null) {
        //新组织未选择
        this.props.form.setFormItemsValue(this.formId, { 'pk_org': { value: null, display: null } });
        return;
    }
    // this.props.form. EmptyAllFormValue(this.formId);
    if (org_data) {
        ajax({
            url: '/nccloud/cmp/curexchange/curexchangeorgafterevent.do',
            data: org_data,
            async: false,
            success: (res) => {
                let { success, data } = res;
                if (success) {
                    this.props.form.setAllFormValue({ [this.formId]: res.data[this.formId] });
                }
            }
        });
    }

}
