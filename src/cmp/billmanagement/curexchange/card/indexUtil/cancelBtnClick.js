import { createPage, ajax, base, high, toast, cardCache } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";
import { BatchToast } from '../../../../public/CMPMessage.js';
//缓存
let { setDefData, getDefData,
    getCurrentLastId, getCacheById,
    updateCache, addCache, getNextId,
    deleteCacheById } = cardCache;

/**
 * [外币兑换index]-取消确认按钮
 * @param {*}  
 */
export const cancelBtnClick = function () {
    if (this.state.org_value && this.state.org_value.length > 0) {
        this.props.form.setFormItemsValue(this.formId, { 'pk_org': { value: this.state.org_value, display: this.state.org_display } });
    }
    //设置为编辑态
    this.props.resMetaAfterPkorgEdit();
}
