import { createPage, ajax, base, high, toast, cardCache } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";
import { BatchToast } from '../../../../public/CMPMessage.js';
//缓存
let { setDefData, getDefData,
    getCurrentLastId, getCacheById,
    updateCache, addCache, getNextId,
    deleteCacheById } = cardCache;

/**
 * [外币兑换index]-跳转空白card页面
 * @param {*}  
 */
export const cancleSkyPage = function () {
    this.props.form.EmptyAllFormValue(this.formId);
    this.props.pushTo('/card', {
        status: 'browse',
        id: '',
        pk: '',
        pagecode: this.pageId
    })
    this.billno=null;
    this.props.resMetaAfterPkorgEdit();
    this.toggleShow();//切换页面状态
}
