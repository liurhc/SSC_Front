
import { createPage, ajax, base, toast } from 'nc-lightapp-front';

export const orgAfterEvent = function (props, value, mouldeId) {
    if (value && mouldeId) {
        ajax({
            url: '/nccloud/cmp/pub/getpara.do',
            //参数返回类型type， int ,string,boolean
            //组织pk_org
            //参数编码paracode 
            data: { paracode: 'CMP49', pk_org: value, type: 'boolean' },
            success: function (res) {
                let { success, data } = res;
                if (res.data.CMP49) {
                    let meta = props.meta.getMeta();
                    let item = meta['head'].items.find(e => e.attrcode === 'objecttype')
                    item.options = [
                        {
                            display:
                                props.MutiInit.getIntl('36070PBR') &&
                                props.MutiInit.getIntl('36070PBR').get('36070PBR-000000') /* 国际化处理： 客户*/,
                            value: '0'
                        },
                        {
                            display:
                                props.MutiInit.getIntl('36070PBR') &&
                                props.MutiInit.getIntl('36070PBR').get('36070PBR-000001') /* 国际化处理： 供应商*/,
                            value: '1'
                        },
                        {
                            display:
                                props.MutiInit.getIntl('36070PBR') &&
                                props.MutiInit.getIntl('36070PBR').get('36070PBR-000002') /* 国际化处理： 部门*/,
                            value: '2'
                        },
                        {
                            display:
                                props.MutiInit.getIntl('36070PBR') &&
                                props.MutiInit.getIntl('36070PBR').get('36070PBR-000003') /* 国际化处理： 人员*/,
                            value: '3'
                        },
                        {
                            display: //'散户',
                            props.MutiInit.getIntl("36070PBR") && props.MutiInit.getIntl("36070PBR").get('36070PBR-000004'),/* 国际化处理： 散户*/
                            value: '4'
                        }
                    ];
                    let tableMeta = props.meta.getMeta();
                    let tableItem = meta['paybilldetail_table'].items.find(e => e.attrcode === 'objecttype')
                    tableItem.options = [
                        {
                            display:
                                props.MutiInit.getIntl('36070PBR') &&
                                props.MutiInit.getIntl('36070PBR').get('36070PBR-000000') /* 国际化处理： 客户*/,
                            value: '0'
                        },
                        {
                            display:
                                props.MutiInit.getIntl('36070PBR') &&
                                props.MutiInit.getIntl('36070PBR').get('36070PBR-000001') /* 国际化处理： 供应商*/,
                            value: '1'
                        },
                        {
                            display:
                                props.MutiInit.getIntl('36070PBR') &&
                                props.MutiInit.getIntl('36070PBR').get('36070PBR-000002') /* 国际化处理： 部门*/,
                            value: '2'
                        },
                        {
                            display:
                                props.MutiInit.getIntl('36070PBR') &&
                                props.MutiInit.getIntl('36070PBR').get('36070PBR-000003') /* 国际化处理： 人员*/,
                            value: '3'
                        },
                        {
                            display: //'散户',
                            props.MutiInit.getIntl("36070PBR") && props.MutiInit.getIntl("36070PBR").get('36070PBR-000004'),/* 国际化处理： 散户*/
                            value: '4'
                        }
                    ];

                } else {
                    let meta = props.meta.getMeta();
                    let item = meta[mouldeId].items.find(e => e.attrcode === 'objecttype')
                    item.options =		item.options = [
                        {
                            display:
                                props.MutiInit.getIntl('36070PBR') &&
                                props.MutiInit.getIntl('36070PBR').get('36070PBR-000000') /* 国际化处理： 客户*/,
                            value: '0'
                        },
                        {
                            display:
                                props.MutiInit.getIntl('36070PBR') &&
                                props.MutiInit.getIntl('36070PBR').get('36070PBR-000001') /* 国际化处理： 供应商*/,
                            value: '1'
                        },
                        {
                            display:
                                props.MutiInit.getIntl('36070PBR') &&
                                props.MutiInit.getIntl('36070PBR').get('36070PBR-000002') /* 国际化处理： 部门*/,
                            value: '2'
                        },
                        {
                            display:
                                props.MutiInit.getIntl('36070PBR') &&
                                props.MutiInit.getIntl('36070PBR').get('36070PBR-000003') /* 国际化处理： 人员*/,
                            value: '3'
                        }
                       
                    ];
                }
            }
        });
    }

}
