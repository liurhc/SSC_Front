//结账--单表
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast } from 'nc-lightapp-front';
import {
	buttonClick, initTemplate, afterEvent, searchBtnClick, pageInfoClick,
	buttonUsability, onClinckRow, onSelectedClick
} from './events';
import { Templatedata } from "../config/Templatedata";//配置的id和area信息
import './index.less';
const { NCDiv: Div, NCIcon: Icon } = base;
class SettleSingleTable extends Component {
	constructor(props) {
		super(props);
		this.props = props;
		this.tableId = Templatedata.list_tableid;
		this.searchId = Templatedata.list_searchid;
		this.pageId = Templatedata.list_pageid;
		this.state = {
			settleMonth: ''//待结账月份
		}
		initTemplate.call(this, props);
	}
	//组件初始加载
	componentDidMount() {
	}
	//请求列表数据
	getData = () => {
		//未使用
		ajax({
			url: '/nccloud/reva/pobdoc/query.do',
			data: {
				pageid: '20520100'
			},
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if (data && data[this.tableId]) {
						this.props.table.setAllTableData(this.tableId, data[this.tableId]);
					} else {
						this.props.table.setAllTableData(this.tableId, { rows: [] });
					}
				}
			}
		});

	};
	//刷新列表
	refresh = () => {
		
		let refreshpageInfo = this.props.table.getTablePageInfo(this.tableId);//分页
		let refreshsearchVal = this.props.search.getAllSearchData(this.searchId);//查询condition
		if (refreshsearchVal) {

			let refreshdata = {
				"conditions": refreshsearchVal.conditions,
				"pagecode": this.pageId,
				"pageInfo": refreshpageInfo,
				"queryAreaCode": this.searchId,
				"oid": "",
				"queryType": "simple"
			};
			ajax({
				url: '/nccloud/cmp/settleaccount/settleaccountquery.do',
				data: refreshdata,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						if (data && data[this.tableId]) {

							//待结账月份赋值
							let rows = data[this.tableId].rows;
							let isSettled = null;
							rows.forEach((val) => {
								debugger
								if (!isSettled) {
									if (val.values.stype_flag.value == '2') {
										isSettled = val.values.stype_flag.value;//状态标识：1：未启用；2：未结账；3：已结账
										let title_month = (this.props.MutiInit.getIntl("36070SA") &&
											this.props.MutiInit.getIntl("36070SA").get('36070SA-000018')) + ' : ';
										this.setState({
											settleMonth: title_month + val.values.findAccMonth.value//待结账月份
										});
									}
								}
							})

							this.props.table.setAllTableData(this.tableId, res.data[this.tableId]);
						} else {
							if (data.message) {
								toast({ color: 'warning', content: data.message });/* 国际化处理： 请选择单条数据，进行结账处理!*/
							}
							this.props.table.setAllTableData(this.tableId, { rows: [], pageInfo: { pageIndex: 0, pageSize: 10, total: 0, totalPage: 0 } });
						}
					}
				}
			});
			buttonUsability.call(this, this.props, '');//列表按钮显影性
		}

	}
	//高级查询内容[未使用]
	others() {
		//其他搜索条件，如停启用等
		return <div>
		</div>;/* 国际化处理： 嘎嘎嘎*/
	}
	//结账
	settleConfirm = () => {

		const settleAData = this.props.table.getCheckedRows(this.tableId);
		let that = this;
		if (settleAData.length != 1) {
			toast({ color: 'warning', content: this.props.MutiInit.getIntl("36070SA") && this.props.MutiInit.getIntl("36070SA").get('36070SA-000000') });/* 国际化处理： 请选择单条数据，进行结账处理!*/
			return;
		}
		// //选中行index
		// let index = settleAData[0].index;
		// props.table.setValByKeyAndIndex(tableid, index, 'sel_flag', { value:'true' })
		//选中数据设置相应字段为sel_flag=true;
		//请求vo参数
		let findAccMonth = null;//会计期间年月
		let findPkorg = null;//所选组织
		let AccYear = null;//会计年
		let AccMonth = null;//会计月
		let findAccMonthEnd = null;//会计期间开始时间
		let findAccMonthFirst = null;//会计期间结束时间
		let findAccMonthArea = null;//会计期间
		let findMonthAreastype = null;//期间状态
		let sel_flag = true;//选择即为true
		let stype_flag = null;
		let settleVo = [];//请求数据

		let allData = this.props.table.getAllTableData(this.tableId).rows;//获取全部数据

		for (let val of allData) {

			findAccMonth = val.values.findAccMonth.value;
			findPkorg = val.values.pk_org.value;
			AccYear = val.values.AccYear.value;
			AccMonth = val.values.AccMonth.value;
			findAccMonthFirst = val.values.findAccMonthFirst.value;
			findAccMonthEnd = val.values.findAccMonthEnd.value;
			findAccMonthArea = val.values.findAccMonthArea.value;
			findMonthAreastype = val.values.findMonthAreastype.value;
			stype_flag = val.values.stype_flag.value;
			if (val.selected && val.selected == true) {
				// sel_flag = true;//选中行数
				//自定义请求数据vo
				let t_data = {
					'findAccMonth': findAccMonth,
					'pk_org': findPkorg,
					'pageid': this.pageId,
					'AccYear': AccYear,
					'AccMonth': AccMonth,
					'findAccMonthFirst': findAccMonthFirst,
					'findAccMonthEnd': findAccMonthEnd,
					'findAccMonthArea': findAccMonthArea,
					'findMonthAreastype': findMonthAreastype,
					'sel_flag': true,
					'stype_flag': stype_flag
				};
				settleVo.push(t_data);

				break;//跳出循环不进行加入数组

			} else {

				//自定义请求数据vo
				let data = {
					'findAccMonth': findAccMonth,
					'pk_org': findPkorg,
					'pageid': this.pageId,
					'AccYear': AccYear,
					'AccMonth': AccMonth,
					'findAccMonthFirst': findAccMonthFirst,
					'findAccMonthEnd': findAccMonthEnd,
					'findAccMonthArea': findAccMonthArea,
					'findMonthAreastype': findMonthAreastype,
					'sel_flag': false,
					'stype_flag': stype_flag
				};
				settleVo.push(data);
			}
		};

		ajax({
			url: '/nccloud/cmp/settleaccount/settleonreckoningover.do',
			data: {
				'settlevo': settleVo
			},
			success: (res) => {
				let { success, data } = res;
				if (success) {
					debugger;
					if (res.data.isAction) {
						//成功了也要增加提示
						let messageArr = [];
						if (res.data.tranData1) {
							messageArr = res.data.tranData1;
						}
						toast(
							{
								title: this.props.MutiInit.getIntl("36070SA") && this.props.MutiInit.getIntl("36070SA").get('36070SA-000002'),/* 国际化处理： 操作成功*/
								// duration: '3',  // 消失时间，默认是3秒; 值为 infinity 时不消失,非必输
								color: 'success',
								content: this.props.MutiInit.getIntl("36070SA") && this.props.MutiInit.getIntl("36070SA").get('36070SA-000003'),/* 国际化处理： 结账成功*/
								groupOperation: true,
								TextArr: [this.props.MutiInit.getIntl("36070SA") && this.props.MutiInit.getIntl("36070SA").get('36070SA-000004'), this.props.MutiInit.getIntl("36070SA") && this.props.MutiInit.getIntl("36070SA").get('36070SA-000005'), this.props.MutiInit.getIntl("36070SA") && this.props.MutiInit.getIntl("36070SA").get('36070SA-000006')],/* 国际化处理： 展开,收起,关闭*/
								groupOperationMsg: messageArr
							}
						);
						that.refresh();
					} else {
						//失败信息
						let mesArr = res.data.tranData1;//返回信息
						let errrorArr = [];
						if (mesArr) {
							errrorArr = mesArr
						}
						toast(
							{
								duration: 'infinity',  // 消失时间，默认是3秒; 值为 infinity 时不消失,非必输
								color: 'warning',
								title: this.props.MutiInit.getIntl("36070SA") && this.props.MutiInit.getIntl("36070SA").get('36070SA-000007'),/* 国际化处理： 操作失败*/
								groupOperation: true,
								TextArr: [this.props.MutiInit.getIntl("36070SA") && this.props.MutiInit.getIntl("36070SA").get('36070SA-000004'), this.props.MutiInit.getIntl("36070SA") && this.props.MutiInit.getIntl("36070SA").get('36070SA-000005'), this.props.MutiInit.getIntl("36070SA") && this.props.MutiInit.getIntl("36070SA").get('36070SA-000006')],/* 国际化处理： 展开,收起,关闭*/
								content: this.props.MutiInit.getIntl("36070SA") && this.props.MutiInit.getIntl("36070SA").get('36070SA-000008'),/* 国际化处理： 请注意！存在月末检查不合格单据*/
								groupOperationMsg: errrorArr
							}
						);
						that.refresh();
					}


				}
			}
		});
	};
	//取消结账
	unsettleConfirm = () => {
		let self = this;
		const unSettleAccountData = this.props.table.getCheckedRows(this.tableId);
		debugger;
		if (unSettleAccountData.length != 1) {
			toast({ color: 'warning', content: this.props.MutiInit.getIntl("36070SA") && this.props.MutiInit.getIntl("36070SA").get('36070SA-000001') });/* 国际化处理： 请选择单条数据，进行取消结账处理!*/
			return;
		}

		//请求参数
		let un_findAccMonth = null;//会计期间年月
		let un_findPkorg = null;//所选组织
		let un_AccYear = null;//会计年
		let un_AccMonth = null;//会计月
		let un_findAccMonthEnd = null;//会计期间开始时间
		let un_findAccMonthFirst = null;//会计期间结束时间
		let un_findAccMonthArea = null;//会计期间
		let un_findMonthAreastype = null;//期间状态
		let un_sel_flag = true;//选择即为true
		let un_stype_flag = null;//期间状态标识

		let un_settleVo = [];//请求数据
		let un_allData = this.props.table.getAllTableData(this.tableId).rows;//获取全部数据

		for (let val of un_allData) {

			un_findAccMonth = val.values.findAccMonth.value;
			un_findPkorg = val.values.pk_org.value;
			un_AccYear = val.values.AccYear.value;
			un_AccMonth = val.values.AccMonth.value;
			un_findAccMonthFirst = val.values.findAccMonthFirst.value;
			un_findAccMonthEnd = val.values.findAccMonthEnd.value;
			un_findAccMonthArea = val.values.findAccMonthArea.value;
			un_findMonthAreastype = val.values.findMonthAreastype.value;
			un_stype_flag = val.values.stype_flag.value;

			if (val.selected && val.selected == true) {
				// sel_flag = true;//选中行数
				//自定义请求数据vo
				let un_t_data = {
					'findAccMonth': un_findAccMonth,
					'pk_org': un_findPkorg,
					'pageid': this.pageId,
					'AccYear': un_AccYear,
					'AccMonth': un_AccMonth,
					'findAccMonthFirst': un_findAccMonthFirst,
					'findAccMonthEnd': un_findAccMonthEnd,
					'findAccMonthArea': un_findAccMonthArea,
					'findMonthAreastype': un_findMonthAreastype,
					'sel_flag': true,
					'stype_flag': un_stype_flag
				};
				un_settleVo.push(un_t_data);

				break;//跳出循环不进行加入数组

			} else {

				//自定义请求数据vo
				let un_data = {
					'findAccMonth': un_findAccMonth,
					'pk_org': un_findPkorg,
					'pageid': this.pageId,
					'AccYear': un_AccYear,
					'AccMonth': un_AccMonth,
					'findAccMonthFirst': un_findAccMonthFirst,
					'findAccMonthEnd': un_findAccMonthEnd,
					'findAccMonthArea': un_findAccMonthArea,
					'findMonthAreastype': un_findMonthAreastype,
					'sel_flag': false,
					'stype_flag': un_stype_flag
				};
				un_settleVo.push(un_data);
			}
		};

		ajax({
			url: '/nccloud/cmp/settleaccount/settleoncancelreckoning.do',
			data: {
				'settlevo': un_settleVo
			},
			success: (res) => {
				let { success, data } = res;
				if (success) {
					toast(
						{
							color: 'success',
							content: this.props.MutiInit.getIntl("36070SA") && this.props.MutiInit.getIntl("36070SA").get('36070SA-000009')
						});/* 国际化处理： 取消结账成功!*/
					self.refresh();
				}
			}
		});
	};
	render() {
		let { table, search } = this.props;
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		return (
			<div className="nc-bill-list settleaccount-list">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						{/* 标题区*/}
						<div className="title-search-title">
							{this.props.MutiInit.getIntl("36070SA") &&
								this.props.MutiInit.getIntl("36070SA").get('36070SA-000015')}{/* 国际化处理： 资金结账*/}
						</div>
						{/* 查询区 */}
						<div className="title-search-detail-search">
							{NCCreateSearch(this.searchId, {
								clickSearchBtn: searchBtnClick.bind(this),
								onAfterEvent: afterEvent.bind(this),
								defaultConditionsNum: 2,
								showAdvBtn: false,
								hideBtnArea: true
							})}
						</div>
						{/* 待结月份*/}
						<div className="title-info">{' '}{this.state.settleMonth}
						</div>
						{/* 提示区 */}
						<div className="mark">
							<div className="mark_inco">
								<Icon type='uf-i-c-2' className="mark_inco_i" />
							</div>
							<div className="footer-container">
								{this.props.MutiInit.getIntl("36070SA") &&
									this.props.MutiInit.getIntl("36070SA").get('36070SA-000011')}
								{' : '}
								{this.props.MutiInit.getIntl("36070SA") &&
									this.props.MutiInit.getIntl("36070SA").get('36070SA-000010')}
							</div>
						</div>
					</div>
					<div className="header-button-area">
						{/* 按钮区 */}
						{this.props.button.createButtonApp({
							area: Templatedata.list_head,
							buttonLimit: 7,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}

					</div>
				</div>
				{/* 国际化处理： 1.开始结账 2.月末检查 3.月度工作报告 4.完成结账*/}
				{/* <div className="table-detail-info">
					{this.props.MutiInit.getIntl("36070SA") && this.props.MutiInit.getIntl("36070SA").get('36070SA-000010')}
				</div>  */}
				<div className="nc-bill-table-area">
					{createSimpleTable(this.tableId, {
						handlePageInfoChange: pageInfoClick,
						onSelected: onSelectedClick.bind(this),//列表控制列表按钮是否可用
						onSelectedAll: buttonUsability.bind(this, this.props, ''),//列表控制列表按钮是否可用
						onRowClick: onClinckRow.bind(this),// 点击行事件
						// selectedChange: selectedChange.bind(this),
						showCheck: true

					})}
				</div>
			</div>
		);
	}
}

SettleSingleTable = createPage({
	mutiLangCode: Templatedata.list_moduleid
})(SettleSingleTable);

ReactDOM.render(<SettleSingleTable />, document.querySelector('#app'));
