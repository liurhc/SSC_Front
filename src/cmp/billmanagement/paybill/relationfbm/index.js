import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base,createPageIcon,promptBox,toast } from 'nc-lightapp-front';
import { card_from_id,card_table_id } from '../cons/constant.js';

const pagecode = '36070PBR_C08'; // 页面编码
const formid = 'head'; // 票据表头区域编码
const tableid = 'table'; // 表体区域编码


/**
 * 联查票据弹框
 * @date 2019-10-22
 */
export class RelationFbm extends Component{

    constructor(props){
        super(props);
        this.state = {

        }
        this.initTemplate(this.props);
    }

    // 加载模板
    initTemplate(props,callBack){
        props.createUIDom(
            {
                pagecode:pagecode
            },
            (data)=>{
                let meta = data.template;
                props.meta.setMeta(meta,callBack);
                this.initData(); // 加载页面数据
            }
        );
    }

    // 初始化票据信息数据
    initData(){
        ajax({
            url: '/nccloud/cmp/paybills/fbmBillLinkQuery.do',
            data: {
                fbmbillno: this.props.fbmbillno.value, // 票据编号
                pk_register: this.props.pk_register.value // 票据主键
            },
            success: (res)=>{
                if(res.success && res.data){
                    
                    if(res.data.pgrid && res.data.pgrid[formid] && res.data.pgrid[formid].rows[0] 
                            && res.data.pgrid[formid].rows[0].values){
                        // 表头赋值
                        this.props.form.setFormItemsValue(formid, res.data.pgrid[formid].rows[0].values);
                    }
                    // 表体数据赋值
                    if(res.data.gridb && res.data.gridb[tableid]){
                        this.props.cardTable.setTableData(tableid, res.data.gridb[tableid]);
                    }
                    
                }
            } 
        });
        
    }

    render(){

        let { form,cardTable } = this.props;
        let { createForm } = form;
        let { createCardTable } = cardTable;

        return(
            <div className="nc-bill-card">

                <div className="nc-bill-header-area">
                    <div className="header-title-search-area">
                        {createPageIcon()}
						<h2 className="title-search-detail">票据综合台账</h2>
					</div>
                </div>

                <div className="nc-bill-form-area">
					{createForm(formid, {})}
				</div>

                <div className="nc-bill-bottom-area">
					<div className="nc-bill-table-area">
						{createCardTable(tableid, {
                            showIndex: true,
                            useFixedHeader:true
						})}
					</div>
				</div>

            </div>
        );
    }
}

RelationFbm = createPage({
    billinfo:{
        billtype:'card',
        pagecode:pagecode
    },
    initTemplate:()=>{}
})(RelationFbm);