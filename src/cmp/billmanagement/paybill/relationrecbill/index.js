import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base,createPageIcon,promptBox,toast } from 'nc-lightapp-front';

const pagecode = '36070RBM_L01';
const tableid = 'table_recbill_01';
const searchid = 'search_recbill_01';

/**
 * 联查收款结算单弹框
 * @date 2019-10-14
 */
export class RelationRecbill extends Component{

    constructor(props){
        super(props);
        this.state = {
            pks:[]
        }
        this.initTemplate(this.props);
    }

    // 加载模板
    initTemplate(props,callBack){
        props.createUIDom(
            {
                pagecode:pagecode
            },
            (data)=>{
                let meta = data.template;
                meta[tableid].pagination = true; // 分页
                props.meta.setMeta(meta,callBack);
                data.button && props.button.setButtons(data.button,()=>{});
            }
        );
    }

    componentDidMount(){
        this.props.parent.getChild(this); // 把当前组件的this传给父组件
    }

    // 查询按钮事件
    clickSearchBtn(props, searchVal){
        var def17 = { // 自定义项17：关联及生成单据标识，"1"表示已关联
            datatype: '56',
            display: '1',
            field: 'def17',
            isIncludeSub: false,
            oprtype: "=",
            refurl: "",
            value:{
                firstvalue: '1',
                secondvalue: ""
            }
        };
        var effectFlag = { // 生效状态
            datatype: "203",
            display: "生效",
            field: "effect_flag",
            isIncludeSub: false,
            oprtype: "=",
            refurl: "",
            value:{
                firstvalue: "10",
                secondvalue: ""
            } 
        };
        searchVal.conditions.push(def17); // 只查未生成下游单据的收款结算
        searchVal.conditions.push(effectFlag); // 只查已生效的收款结算


        let pageInfo = props.table.getTablePageInfo(tableid);
        let conditions = searchVal.conditions;
        let oid = props.meta.getMeta()[searchid].oid; // 查询模板id
      
        let searchData = {//查询区条件
            querycondition: searchVal,
            custcondition: {
                logic: 'and', //逻辑操作符，and、or
                conditions: []
            },
            pageInfo: pageInfo,
            pageCode: pagecode,
            queryAreaCode: searchid, //查询区编码
            oid: oid, //查询模板id，手工添加在界面模板json中，放在查询区，后期会修改
            querytype: 'tree'
        };
        let splicSearchData = {//查询区+页签条件
            querycondition: searchVal,
            custcondition: {
                logic: 'and', //逻辑操作符，and、or
                conditions: []
            },
            pageInfo: pageInfo,
            pageCode: pagecode,
            queryAreaCode: searchid, //查询区编码
            oid: oid,
            querytype: 'tree'
        };
        
		if(!searchData || !searchData.querycondition){
			return;
        }
        let datas = { // 查询按钮请求数据
            'searchArea': searchData,
            'splicSearchArea': splicSearchData
        }

        ajax({
            url:'/nccloud/cmp/recbill/recbillquery.do',
            data:datas,
            success:(result)=>{
                let { success,data } = result;
                if(success){
                    if (data && data.totalnum && data.totalnum == '0') {
                        toast({ color: 'warning', content: '未查询出符合条件的数据!'});
                        this.props.table.setAllTableData(tableid, { rows: [], pageInfo: { pageIndex: 0, pageSize: 10, total: 0, totalPage: 0 } });
                    }
                    else {
                        toast({color: 'success', content:'查询成功！'});
                        if (data && data.grid) {
                            this.props.table.setAllTableData(tableid, data.grid[tableid]);//表格赋值
                        }else{
                            this.props.table.setAllTableData(tableid, { rows: [], pageInfo: { pageIndex: 0, pageSize: 10, total: 0, totalPage: 0 } });
                        }
                    }
                }
            }
        });
    }

    // 按钮事件
    onButtonClick(props,id){
        switch(id){
            case 'generate': // 生成单据
                let selectedData = props.table.getCheckedRows(tableid);
                if(selectedData && selectedData.length!=1){
                    toast({title:'生成单据操作只能选择一条数据！',color:'danger'});
                    break;
                }
                // 跳转到特殊付款单卡片页面
                props.parent.pushtoCardPage();
                break;
        }
    }

    // 获取选中的收款结算的单据主键
    getRecbillid(){
        let selectedData = this.props.table.getCheckedRows(tableid);
        return selectedData[0].data.values.pk_recbill.value;
    }

    // 表格数据选中事件
    onSelected(){

    }

    // 分页按钮事件
    pageInfoClick(props, config, pks){
        debugger;
        this.props.table.selectAllRows(tableid, false);//清空所选行数
        //分页根据pks查询数据
        let data = {
            "pks": pks,
            "pageid": pagecode
        };
        ajax({
            url: '/nccloud/cmp/recbill/recbillquerybyids.do',
            data: data,
            success: (res) => {
                let { success, data } = res;
                if (success) {
                    if(data){
                        this.props.table.setAllTableData(tableid, data[tableid]);
                    }else{
                        this.props.table.setAllTableData(tableid, { rows: [], pageInfo: { pageIndex: 0, pageSize: 10, total: 0, totalPage: 0 } });
                    }
                }
            }
        });
    }

    render(){

        let { table,search,button } = this.props;
        let { createButtonApp } = button;
        let { createSimpleTable } = table;
        let { NCCreateSearch } = search;

        return(
            <div className="nc-single-table">

                <div className="nc-bill-header-area">
                    <div className="header-title-search-area">
                        {createPageIcon()}
						<h2 className="title-search-detail">联查收款结算单</h2>
					</div>
                    <div className="header-button-area">{/* 按钮区 */}
                        {createButtonApp({
                            area:'head',
                            buttonLimit: 3,
                            onButtonClick: this.onButtonClick.bind(this),
                            popContainer: document.querySelector('.header-button-area')
                        })}
                    </div>
                </div>

                <div className="nc-bill-search-area">
                    {NCCreateSearch(
                        searchid,
                        {
                            clickSearchBtn:this.clickSearchBtn.bind(this),// 查询按钮事件
                            showAdvBtn: true // 显示高级查询按钮
                        }
                    )}
                </div>

                <div className="nc-singleTable-table-area">{/* 表格区 */}
                    {createSimpleTable(
                        tableid,
                        {
                            useFixedHeader:true,
                            showIndex:true,
                            showCheck:true,
                            isAddRow:true,
                            onSelected:this.onSelected.bind(this),
                            handlePageInfoChange: this.pageInfoClick.bind(this)
                        }
                    )}
                </div>

            </div>
        );
    }
}

RelationRecbill = createPage({
    billinfo:{
        billtype:'grid',
        pagecode:pagecode
    },
    initTemplate:()=>{}
})(RelationRecbill);