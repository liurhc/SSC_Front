import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base,createPageIcon,promptBox,toast } from 'nc-lightapp-front';
import { card_from_id,card_table_id } from '../cons/constant.js';

const pagecode = '36070PBR_C06'; // 页面编码
// 区域编码
const formid = 'head'; // 合同表头
const tableid1 = 'table1'; // 放款区域编码
const tableid2 = 'table2'; // 还款
const tableid3 = 'table3'; // 合同执行情况
const tableid4 = 'table4'; // 担保
const tableid5 = 'table5'; // 销售进项成本
const tableid6 = 'table6'; // 保函


/**
 * 联查合同弹框
 * @date 2019-10-20
 */
export class RelationContract extends Component{

    constructor(props){
        super(props);
        this.state = {

        }
        this.initTemplate(this.props);
    }

    // 加载模板
    initTemplate(props,callBack){
        props.createUIDom(
            {
                pagecode:pagecode
            },
            (data)=>{
                let meta = data.template;
                props.meta.setMeta(meta,callBack);
                this.initData(); // 加载页面数据
            }
        );
    }

    // 初始化合同信息数据
    initData(){        
        ajax({
            url: '/nccloud/cmp/paybills/loanContractLinkQuery.do',
            data: {
                cont_code: this.props.cont_code.value, // 合同编号
                pk_contract: this.props.pk_contract.value // 合同主键
            },
            success: (res)=>{
                if(res.success && res.data){
                    
                    if(res.data.pgrid && res.data.pgrid[formid] && res.data.pgrid[formid].rows[0] 
                            && res.data.pgrid[formid].rows[0].values){
                        // 表头赋值
                        this.props.form.setFormItemsValue(formid, res.data.pgrid[formid].rows[0].values);
                    }
                    // 表体数据赋值-共六个表体，按照后台返回的数据结构进行赋值
                    for(let i=1; i<7; i++){
                        let gridIndex = 'gridb' + i;
                        let tableIndex = 'table' + i;
                        if(res.data[gridIndex] && res.data[gridIndex][tableIndex]){
                            this.props.cardTable.setTableData(tableIndex, res.data[gridIndex][tableIndex]);
                        }
                    }
                }
            }   
        });
    }

    render(){

        let { form,cardTable } = this.props;
        let { createForm } = form;
        let { createCardTable } = cardTable;

        return(
            <div className="nc-bill-card">

                <div className="nc-bill-header-area">
                    <div className="header-title-search-area">
                        {createPageIcon()}
						<h2 className="title-search-detail">银行贷款合同</h2>
					</div>
                </div>

                <div className="nc-bill-form-area">
					{createForm(formid, {})}
				</div>

                <div className="nc-bill-bottom-area">
					<div className="nc-bill-table-area">
						{createCardTable(tableid1, {
                            showIndex: true,
                            useFixedHeader:true
						})}
					</div>

                    <div className="nc-bill-table-area">
						{createCardTable(tableid2, {
                            showIndex: true,
                            useFixedHeader:true
						})}
					</div>

                    <div className="nc-bill-table-area">
						{createCardTable(tableid3, {
                            showIndex: true,
                            useFixedHeader:true
						})}
					</div>

                    <div className="nc-bill-table-area">
						{createCardTable(tableid4, {
                            showIndex: true,
                            useFixedHeader:true
						})}
					</div>

                    <div className="nc-bill-table-area">
						{createCardTable(tableid5, {
                            showIndex: true,
                            useFixedHeader:true
						})}
					</div>

                    <div className="nc-bill-table-area">
						{createCardTable(tableid6, {
                            showIndex: true,
                            useFixedHeader:true
						})}
					</div>
				</div>

            </div>
        );
    }
}

RelationContract = createPage({
    billinfo:{
        billtype:'card',
        pagecode:pagecode
    },
    initTemplate:()=>{}
})(RelationContract);