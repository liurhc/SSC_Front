import { asyncComponent } from 'nc-lightapp-front';
import List from '../list';
import card from '../card'

//const card = asyncComponent(() => import(/* webpackChunkName: "cmp/billmanagement/paybill/card/card" */  '../card'));
//const linkcard = asyncComponent(() => import(/* webpackChunkName: "cmp/billmanagement/paybill/linkcard/card" */'../linkcard'));


const routes = [
	{
		path: '/',
		component: List,
		exact: true
	},
	{
		path: '/list',
		component: List
	},
	{
		path: '/card',
		component: card
	}
	// {
	// 	path: '/linkcard',
	// 	component: linkcard
	// }
];

export default routes;
