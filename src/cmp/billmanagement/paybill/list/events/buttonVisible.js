import { ajax, base, toast, cardCache } from 'nc-lightapp-front';
let { setDefData, getDefData } = cardCache;
import { PAYBILL_CONST } from '../../cons/constant.js';

export const buttonVisible = function (props) {
	let tradeType = JSON.parse(sessionStorage.getItem('sessionTP'));
	if (tradeType && tradeType.refcode == 'F5-Cxx-99') {
		props.button.setButtonVisible(
			['recbill'],
			true
		);

	} else {
		props.button.setButtonVisible(
			['recbill'],
			false
		);
	}
	let ISlINK = getDefData(PAYBILL_CONST.link_key, PAYBILL_CONST.paybillCacheKey);
	if (ISlINK) {
		props.button.setButtonVisible(
			[
				'save',
				'delete',
				'copy',
				'delete',
				'commit',
				'uncommit',
				'BaseImageShow',
				'BaseImageScan',
				'Associate',
				'unassociate',
				'Refresh'
			],
			false
		);
	}
};
