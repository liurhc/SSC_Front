import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast, print, getMultiLang, promptBox,createPageIcon } from 'nc-lightapp-front';
let { NCFormControl, NCAnchor, NCScrollLink, NCScrollElement, NCAffix, NCButton,NCSelect } = base;
const { NCBackBtn} = base; //返回button
const NCOption = NCSelect.NCOption;
import { buttonClick, initTemplate, pageInfoClick, afterEvent, tableButtonClick, beforeEvent} from './events';
import { addline} from './events/addLine';
import { cardCache } from 'nc-lightapp-front';
let { getCacheById, updateCache, addCache, getNextId, deleteCacheById } = cardCache;
import { PAYBILL_CONST } from '../cons/constant.js';
import { commonurl } from '../../../public/utils/constant';
import { high } from 'nc-lightapp-front';
var arrList=[];
const { Refer, NCUploader, BillTrack, ApproveDetail, PrintOutput, Inspection, ApprovalTrans } = high;
import { buttonVisible } from './events/buttonVisible';
import { orgVersionView } from '../../../../tmpub/pub/util/version/index.js'; //多版本显示
import InvoiceUploader from 'sscrp/rppub/components/invoice-uploader';
import InvoiceLink from 'sscrp/rppub/components/invoice-link';
import { saveMultiLangRes } from '../../../../tmpub/pub/util';
class Card extends Component {
	constructor(props) {
		super(props);
		this.formId = PAYBILL_CONST.card_from_id;
		// this.searchId = '20521030';
		this.moduleId =PAYBILL_CONST.appcode; //模块id
		this.tableId = PAYBILL_CONST.card_table_id;
		this.pageId = PAYBILL_CONST.card_page_id;
		if(this.props.getUrlParam('pagecode')){
			this.pageId =this.props.getUrlParam('pagecode');
		}
		this.billno = '';
		this.billId = '';
		this.tradeType = 'trade_type';//单据控制规则交易类型字段名称（也可传递的单据类型）
		this.formVOClassName = 'BillVO';//form表单的vo类名
		this.tableVOClassName = 'BillDetailVO';//table表体的vo类名
		this.compositedata='';
		
		this.state = {
			showUploader: false,
			target: null,
			billtype: '',
			tradetype: '',
			oldorg: '',
			oldorgDis: '',
			shoWIns: false,
			showAppr: false,
			sourceData: null,
			pasteflag: false,
			billCodeModalShow: false,
			billCode: '',
			openflag: 'true',
			compositedata: '',
			getAssginUsedr: '',
			compositedisplay: false,
			show: false,
			protoPayMny: {},
			showTradeBtn:false,
			outputData: {
				funcode: '36070PBR', //功能节点编码，即模板编码
				nodekey: 'NCCLOUD', //模板节点标识
				printTemplateID: '1001Z610000000004R6L', //模板id
				oids: [],
				outputType: 'output'
			},
			sscrpInvoiceData:{},
			sscrpLinkInvoiceData:{},
			ifChange:false
		};
		initTemplate.call(this, props);
	}
	componentDidMount() {
		
		
	};

	deaultAddLine=(Datacontext)=>{
	    let context = Datacontext.context;
		if (context.pk_org) {
	    if (this.props.cardTable.getNumberOfRows(PAYBILL_CONST.card_table_id) <= 0) {
		   this.props.cardTable.addRow(this.tableId);
			let dataArr = ['pk_currtype','pk_balatype','note_type','pk_tradetypeid','pk_account','pk_oppaccount','objecttype',
								 'local_rate','group_rate','global_rate','pk_supplier','mon_account','accountname','pk_customer', 'pk_busiman', 'accountopenbank', 'accountcode',
									'pk_accountname','pk_org'];
			addline(this.props, dataArr,0);
		 }
			 
		}
	}
	
	initData = () => {
		let src = this.props.getUrlParam('src');
		if (src === 'settlement') {
			let settlePk = this.props.getUrlParam('pk_settle');
			if (settlePk) {
				this.QuerySettleData(settlePk);
			}
		} else {
			this.pageShow();
		}
	};
	componentWillMount() {

		let callback = (json) => {
			 this.setState({ json });
			 saveMultiLangRes(this.props,json);//缓存多语资源
			initTemplate.call(this, this.props);
		};
		getMultiLang({ moduleId:{ 
			[ 'tmpub']:['3601'],
			['cmp']: [PAYBILL_CONST.appcode, '36070']
		   } , callback });
		// 关闭浏览器
		window.onbeforeunload = () => {
			let closeStatus =this.props.getUrlParam('status');
			if (closeStatus&&closeStatus!= 'browse') {
				return '';
			}
		};
		
	};

	QuerySettleData = (pk_settle) => {
		let sendArr = {
			pk: pk_settle,
			pageid: '36070PBR_D5_card'
		};
		ajax({
			url: '/nccloud/cmp/paybills/associate.do',
			data: sendArr,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if (res.data) {
						if (res.data.head) {
							this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
							let sourceFlag = res.data.head[this.formId].rows[0].values.source_flag.value;
							this.sourceFlagTranslate(sourceFlag);
						}
						if (res.data.body) {
							this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
						}
						// this.props.cardTable.setStatus(this.tableId, 'edit');
						// this.props.form.setFormStatus(this.formId, 'edit');
						this.toggleShow();
					}
				}
			}
		});
	};

	pageShow = () => {
		if (this.props.getUrlParam('status') === 'add') {
			// this.props.form.EmptyAllFormValue('head');
			// this.props.cardTable.setTableData(this.tableId, { rows: [] });
			let addData = this.props.createMasterChildData(PAYBILL_CONST.card_page_id, this.formId, this.tableId);
			let url = '/nccloud/cmp/paybills/paybillconstructor.do'; // 原来单据构造请求
			let fromrecbill = this.props.getUrlParam('fromrecbill');
			if(fromrecbill ==='Y'){ // 根据收款结算生成的单据
				/*addData = {
					pageid: '36070PBR_D5_card',
					recbillid: this.props.getUrlParam('recbillid'), // 关联收款结算主键
					tradetype: 'F5-Cxx-99' // 特殊付款单交易类型编码
				}*/
				addData.recbillid = this.props.getUrlParam('recbillid'), // 关联收款结算主键
				addData.tradetype = 'F5-Cxx-99' // 特殊付款单交易类型编码
				url = '/nccloud/cmp/paybills/paybilldefvalue.do' // 换成新的单据构造请求
			}
			ajax({
				url: url,
				data: addData,
				success: (res) => {
					if (res.success) {
						if (res.data) {
							//F5判断是否从银行挂账流水跳转
									if(window.location.hash.includes("ifturnTo=")){
										localStorage.setItem("yhlsgz", "bianji");
										var record=JSON.parse(localStorage.getItem('record'))
										var  heads=res.data.head[this.formId].rows["0"].values;
										var  tables=res.data.body[this.tableId].rows["0"].values
										heads.pk_upbill =record.values.m_pk_bankreceipt;//F2表体top_billid后端要求
										heads.pk_org=record.values.m_pk_org;//财务组织
										tables.pk_org=record.values.m_pk_org;//财务组织
										record.values.num&&record.values.num.value&&(heads.num=record.values.num);//挂账凭证号
                                        heads.oppunitname =record.values.oppunitname ;//对方单位
                                        heads.accounting_status=record.values.accounting_status;//挂账状态
										heads.pk_oppaccount=record.values.m_pk_bank;//银行账户-付款银行账号
										heads.pk_account=record.values.otheraccount;//银行账户-付款银行账号
										tables.pk_oppaccount=record.values.m_pk_bank;//银行账户-付款银行账号
										tables.pk_account=record.values.otheraccount;//银行账户-付款银行账号
										heads.primal_money=record.values.m_creditamount;//原币借发生额
										heads.def10=record.values.m_creditamount;//原币贷发生额
										heads.def10.scale=heads.primal_money.scale="2"
										heads.pk_supplier=record.values.supplier;//对方单位
										heads.def8=record.values.checkdate;//流水时间
										tables.pk_supplier=record.values.supplier;//对方单位
										heads.pk_balatype=record.values.pk_balatype1;    //结算方式
										tables.pk_balatype=record.values.pk_balatype1;  //结算方式
										heads.def23=record.values.oppaccountname||{};  //
										if(record.values.otheraccount&&record.values.otheraccount.display){
											tables.accountcode={value:record.values.otheraccount.display}
										}
										if(record.values.oppaccountname&&record.values.oppaccountname.value){
											tables.accountname={value:record.values.oppaccountname.value}
											heads.pk_accountname={value:record.values.oppaccountname.value ,display:record.values.oppaccountname.value}
										}
										heads.pk_busiflow=record.values.pk_busiflow;
										heads.pk_tradetypeid=record.values.pk_tradetypeid;
										heads.trade_type=record.values.trade_type;
										if(record.values.trade_type&&record.values.trade_type.value){
											heads.pk_tradetypeid.pk_billtypecode={display:record.values.trade_type.value,value:record.values.trade_type}
										}
										record.values.pk_currtype&&record.values.pk_currtype.value&&(heads.pk_currtype=record.values.pk_currtype)&&(tables.pk_currtype=record.values.pk_currtype);//币种
										heads.local_rate=record.values.rate;//汇率
								       tables.local_rate=record.values.rate;//汇率
										tables.memo=record.values.explanation;//摘要
										tables.pay_primal=record.values.m_creditamount;//原币贷发生额
										tables.pay_local={value:parseFloat(record.values.m_creditamount.value)*parseFloat(record.values.rate.value)};
										tables.pay_primal.scale="2"
									    //tables.objtype={value:"0",display:"供应商"};//对方单位
									}


							if (res.data.head) {
								this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
								if (this.props.getUrlParam('tradetypepk') && this.props.getUrlParam('tradetypename')) {
									this.props.form.setFormItemsValue('head', {
										pk_tradetypeid: {
											value: this.props.getUrlParam('tradetypepk'),
											display: this.props.getUrlParam('tradetypename')
										}
									});

									this.props.form.setFormItemsValue('head', {
										trade_type: {
											value: this.props.getUrlParam('tradetypecode'),
											display: this.props.getUrlParam('tradetypecode')
										}
									});
								}
								// 添加判断条件，如果是参照收款结算生成的单据，重新设置'pk_tradetypeid'和'trade_type'字段的值 -20191113
								if (this.props.getUrlParam('fromrecbill') === 'Y') {
									let tradeType = JSON.parse(sessionStorage.getItem('sessionTP'));
									this.props.form.setFormItemsValue('head', {
										pk_tradetypeid: {
											value: tradeType.refpk,
											display: tradeType.refname
										}
									});
									this.props.form.setFormItemsValue('head', {
										trade_type: {
											value: tradeType.refcode,
											display: tradeType.refcode
										}
									});
								}
								let sourceFlag = res.data.head[this.formId].rows[0].values.source_flag.value;
								this.sourceFlagTranslate(sourceFlag);
								// 					//来源系统翻译
							}
							let pk_org = res.data.head[this.formId].rows[0].values.pk_org;
							if (res.data.body && pk_org && pk_org.value) {
								this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
							}
							if(window.location.hash.includes("ifturnTo=")){
							//	pk_org&&afterEvent.call(this, this.props,"head","pk_org",record.values.m_pk_org,{},"",heads)
							}
							this.billno = '';
							this.billId = '';
							this.toggleShow();
							// 				//}
						} else {
							this.props.form.setAllFormValue({ [this.formId]: { rows: [] } });
							this.props.cardTable.setTableData(this.tableId, { rows: [] });
						}
					}
				}
			});
		}
		//查询单据详情
		//浏览 browse
		if (this.props.getUrlParam('status') == 'browse') {
			if (this.props.getUrlParam('op') == 'reverse') {
				toast({
					color: 'success',
					content:
						this.props.MutiInit.getIntl('36070PBR') &&
						this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000099')
				}); /* 国际化处理： 红冲成功*/
			}

			if (this.props.getUrlParam('op') == 'cancel') {
				
			
				this.billno = '';
				this.props.form.EmptyAllFormValue(this.formId);
				this.props.cardTable.setTableData(this.tableId, { rows: [] });
				this.toggleShow();
			} else {
				let data = { pk: this.props.getUrlParam('id'), pageid: this.pageId };
				ajax({
					url: '/nccloud/cmp/billmanagement/querybypk.do',
					data: data,
					success: (res) => {
						if (res.data) {
							 let billId;
							if (res.data.head) {
								this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
								let billno = res.data.head[this.formId].rows[0].values.bill_no.value;
							    billId = res.data.head[this.formId].rows[0].values.pk_paybill.value;
								this.billno = billno;
								this.billId = billId;
							}
							if (res.data.body) {
								this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
							}
							let sourceFlag = res.data.head[this.formId].rows[0].values.source_flag.value;
							this.sourceFlagTranslate(sourceFlag);
							// 增加缓存
							//addCache(billId, res.data, PAYBILL_CONST.card_from_id, PAYBILL_CONST.paybillCacheKey);
							this.toggleShow();
						} else {
							this.props.form.setAllFormValue({ [this.formId]: { rows: [] } });
							this.props.cardTable.setTableData(this.tableId, { rows: [] });
						}
					},
					error: (res) => {
						toast({
							color: 'warning',
							content:res.message
						});
						this.billno = '';
						this.billId='';
						this.props.form.EmptyAllFormValue(this.formId);
						this.props.cardTable.setTableData(this.tableId, { rows: [] });
                        this.toggleShow();
					}
				});
				//}
			}
		}
		if (this.props.getUrlParam('status') === 'edit') {
			let data = { pk: this.props.getUrlParam('id'), pageid: this.pageId };
			ajax({
				url: '/nccloud/cmp/billmanagement/querybypk.do',
				data: data,
				success: (res) => {
					if (res.data) {
						let is_cf = false;
						if (res.data.head) {
							this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
							this.props.form.setFormItemsDisabled(this.formId, { pk_org: true });
							let bill_no = res.data.head[this.formId].rows[0].values.bill_no.value;
							if (res.data.head[this.formId].rows[0].values.is_cf.value) {
								is_cf = true;
							}
							let sourceFlag = res.data.head[this.formId].rows[0].values.source_flag;
							this.billno = bill_no;
						}
						if (res.data.body) {
							this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
							this.setPknoteVal.call(this);
						}
						if (is_cf) {
							let mnyData = {};
							let data = res.data.body[this.tableId].rows;
							for (let item of data) {
								if (item.values.pay_primal.value) {
									mnyData[item.values.pk_paybill_detail.value] = item.values.pay_primal.value;
								}
							}
							this.setState({
								protoPayMny: mnyData
							});
						}
						//
						let sourceFlag = res.data.head[this.formId].rows[0].values.source_flag.value;
						this.sourceFlagTranslate(sourceFlag);
						this.toggleShow();
					} else {
						this.props.form.setAllFormValue({ [this.formId]: { rows: [] } });
						this.props.cardTable.setTableData(this.tableId, { rows: [] });
					}
					//设置组织不可以编辑
					this.props.form.setFormItemsDisabled(this.formId, { pk_org: true });
				}
			});
		}
		//浏览edit
		//copy
		if (this.props.getUrlParam('status') === 'copy') {
			//let data = { pk: [ this.props.getUrlParam('id') ], pageid: '36070WC_C01' };
			let data = { pk: this.props.getUrlParam('id'), pageid: this.pageId };
			ajax({
				url: '/nccloud/cmp/paybills/copy.do',
				data: data,
				success: (res) => {
					if (res.data) {
						if (res.data.head) {
							this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
							this.props.form.setFormItemsDisabled(this.formId, { pk_org: true });
							this.billno = '';
							//来源系统翻译
							let sourceFlag = res.data.head[this.formId].rows[0].values.source_flag.value;
							this.sourceFlagTranslate(sourceFlag);
						}
						if (res.data.body) {
							this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
							//this.setPknoteVal.call(this);
						}
						this.toggleShow();
					} else {
						this.props.form.setAllFormValue({ [this.formId]: { rows: [] } });
						this.props.cardTable.setTableData(this.tableId, { rows: [] });
					}
					//设置组织不可以编辑
					this.props.form.setFormItemsDisabled(this.formId, { pk_org: true });
				}
			});
		} else {
			//this.props.cardTable.addRow(this.tableId);
		}
		//红冲
		if (this.props.getUrlParam('status') === 'reverse') {
			//let data = { pk: [ this.props.getUrlParam('id') ], pageid: '36070WC_C01' };
			let data = { pk: this.props.getUrlParam('id'), pageid: this.pageId };
			ajax({
				url: '/nccloud/cmp/paybills/reverse.do',
				data: data,
				success: (res) => {
					if (res.data) {
						toast({
							color: 'warning',
							content:
								this.props.MutiInit.getIntl('36070PBR') &&
								this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000099') /* 国际化处理：红冲成功！*/
						});

						if (res.data.head) {
							this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
							this.props.form.setFormItemsDisabled(this.formId, { pk_org: true });
							//来源系统翻译
							let sourceFlag = res.data.head[this.formId].rows[0].values.source_flag.value;
							this.sourceFlagTranslate(sourceFlag);
						}
						if (res.data.body) {
							this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
							//this.setPknoteVal.call(this);
						}
						this.toggleShow();
					} else {
						this.props.form.setAllFormValue({ [this.formId]: { rows: [] } });
						this.props.cardTable.setTableData(this.tableId, { rows: [] });
					}
					//设置组织不可以编辑
					this.props.form.setFormItemsDisabled(this.formId, { pk_org: true });
				}
			});
		}
	};
	sourceFlagTranslate = (sourceFlag) => {
		if (sourceFlag) {
			let val = sourceFlag;
			switch (val) {
				case '2':
					this.props.form.setFormItemsValue('head', {
						source_flag: {
							value: val,
							display:
								this.props.MutiInit.getIntl('36070PBR') &&
								this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000019') /* 国际化处理： 现金管理*/
						}
					});
					break;
				case '9':
					this.props.form.setFormItemsValue('head', {
						source_flag: {
							value: val,
							display:
								this.props.MutiInit.getIntl('36070PBR') &&
								this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000020') /* 国际化处理： 协同单据*/
						}
					});
					break;
				case '5':
					this.props.form.setFormItemsValue('head', {
						source_flag: {
							value: val,
							display:
								this.props.MutiInit.getIntl('36070PBR') &&
								this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000021') /* 国际化处理： 资金结算*/
						}
					});
					break;
				case '8':
					this.props.form.setFormItemsValue('head', {
						source_flag: {
							value: val,
							display:
								this.props.MutiInit.getIntl('36070PBR') &&
								this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000022') /* 国际化处理： 票据管理*/
						}
					});
					break;
				case '6':
					this.props.form.setFormItemsValue('head', {
						source_flag: {
							value: val,
							display:
								this.props.MutiInit.getIntl('36070PBR') &&
								this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000023') /* 国际化处理： 网上银行*/
						}
					});
					break;
			}
		}
	};

	//切换页面状态
	toggleShow = () => {
		let status = this.props.getUrlParam('status');
		let backShow= this.showBackButton();
		if(this.props.getUrlParam('scene')){
			this.setState({
				showTradeBtn:false
			});
		}
		let flag = status === 'browse' ? false : true;

		if (flag) {
			this.props.cardTable.setStatus(this.tableId, 'edit');	
			this.props.form.setFormStatus(this.formId, 'edit');
		} else {
			this.props.cardTable.setStatus(this.tableId, status);
			this.props.form.setFormStatus(this.formId, status);
		}
		//组织之外的字段不可以编辑
		if (status === 'add') {
			let pk_org = this.props.form.getFormItemsValue('head', 'pk_org');
			this.props.ViewModel.setData('status', status);
			if (!pk_org || !pk_org.value) {
				this.props.form.setFormItemsDisabled(this.formId, { pk_org: false }); //财务组织、
				this.props.resMetaAfterPkorgEdit();
				this.props.initMetaByPkorg();
				this.props.button.setButtonDisabled([ 'addline', 'delline', 'copyline' ], true);
			} else {
				this.props.resMetaAfterPkorgEdit();
			}
		}
		if (status === 'copy') {
			this.props.form.setFormItemsDisabled(this.formId, { pk_org: true }); //财务组织
			this.props.ViewModel.setData('status', status);
			this.props.resMetaAfterPkorgEdit();
		}

		if (status === 'browse') {
			this.props.ViewModel.setData('status', status);
			this.props.BillHeadInfo.setBillHeadInfoVisible({
				showBackBtn: true&&backShow, //控制显示返回按钮: true为显示,false为隐藏 ---非必传
				showBillCode: true, //控制显示单据号：true为显示,false为隐藏 ---非必传
				billCode: this.billno //修改单据号---非必传
			});
		} else if (status === 'edit') {
			this.props.BillHeadInfo.setBillHeadInfoVisible({
				showBackBtn: false, //控制显示返回按钮: true为显示,false为隐藏 ---非必传
				showBillCode: true //控制显示单据号：true为显示,false为隐藏 ---非必传
			});
		} else {
			this.props.BillHeadInfo.setBillHeadInfoVisible({
				showBackBtn: false, //控制显示返回按钮: true为显示,false为隐藏 ---非必传
				showBillCode: false //控制显示单据号：true为显示,false为隐藏 ---非必传
			});
		}
		buttonVisible.call(this, this.props);
		this.buttonUsability.call(this);
		orgVersionView.call(this, this.props, 'head', 'pk_org', 'pk_org_v'); //多版本视图显隐性
		//pk_note 赋值
		//this.setPknoteVal();
		//this.protoPayInfo();
		//this.forceUpdate();
	};

	showBackButton=()=> {
		let show= true; 
		if(this.props.getUrlParam('auto')){
          show =false; 
		}else{
		    if(this.props.getUrlParam('scene')){
				 if(this.props.getUrlParam('scene')=='linksce'||this.props.getUrlParam('scene')=='zycl') {
					show =false; 
				 }
			}	
		}
        return show;
	}
	//保存校验
	saveBeforeEvent = (CardData) => {
		let headData = CardData.head[this.formId].rows[0];
		let i = 0;
		if (headData.values.mon_account.value) {
			i = i + 1;
		}
		if (headData.values.note_no.value) {
			i = i + 1;
		}
		if (headData.values.pk_oppaccount.value) {
			i = i + 1;
		}
		if (i > 1) {
			toast({
				color: 'warning',
				content:
					this.props.MutiInit.getIntl('36070PBR') &&
					this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000024')
			}); /* 国际化处理： 票据号、银行账户、现金账户不能同时有值*/
			return false;
		}

		let checkData = CardData.body[this.tableId].rows;
		if (checkData.length == 0) {
			toast({
				color: 'warning',
				content:
					this.props.MutiInit.getIntl('36070PBR') &&
					this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000025')
			}); /* 国际化处理： 请检查必输项是否填写22*/
			return false;
		}

		for (let item of checkData) {
			if (!item.values.pay_local.value) {
				toast({
					color: 'warning',
					content:
						this.props.MutiInit.getIntl('36070PBR') &&
						this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000026')
				}); /* 国际化处理： 请检查必输项是否填写33*/
				return false;
			}
		}
		return true;
	};
	//保存校验
	protoPayCheck = (CardData) => {
		let checkData = CardData.body[this.tableId].rows;
		if (checkData.length == 0) {
			toast({
				color: 'warning',
				content:
					this.props.MutiInit.getIntl('36070PBR') &&
					this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000025')
			}); /* 国际化处理： 请检查必输项是否填写22*/
			return true;
		}

		for (let item of checkData) {
			if (!item.values.cf_type || !item.values.cf_type.value) {
				toast({
					color: 'warning',
					content:
						this.props.MutiInit.getIntl('36070PBR') &&
						this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000101')
				}); /* 国际化处理： 请检查必输项是否填写33*/
				return true;
			}
		}
	};
	setPknoteVal = () => {
		let rowNum = this.props.cardTable.getNumberOfRows('paybilldetail_table'); //表体table行数
		for (let i = 0; i < rowNum; i++) {
			let pk_note = this.props.cardTable.getValByKeyAndIndex('paybilldetail_table', i, 'note_no');
			if (pk_note) {
				this.props.cardTable.setValByKeyAndIndex('paybilldetail_table', i, 'pk_note', {
					value: pk_note.value,
					display: pk_note.display
				});
			} else {
				this.props.cardTable.setValByKeyAndIndex('paybilldetail_table', i, 'pk_note', {
					value: null,
					display: null
				});
			}
		}
	};
  //检查是否是独立结算信息
	checkSettleInfo=()=>{
		let isfromindependent=this.props.form.getFormItemsValue('head', 'isfromindependent');
		if(isfromindependent&&isfromindependent.value==='1'){
			toast({
				color: 'warning',
				content:
					this.props.MutiInit.getIntl('36070PBR') &&
					this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000113') /* 国际化处理：不允许增删独立结算信息行记录！*/
			});
			return true;
		} else{
			return false;
		}
	}
	//保存单据
	saveBill = () => {
		if (this.props.getUrlParam('copyFlag') === 'copy') {
			this.props.form.setFormItemsValue(this.formId, { pk_paybill: null });
			this.props.form.setFormItemsValue(this.formId, { ts: null });
		}
		//过滤表格空行
		//校验表单必输字段
		let CardData = this.props.createMasterChildData(this.pageId, this.formId, this.tableId);

		let flag = this.props.form.isCheckNow(this.formId);
		if (!this.props.cardTable.checkTableRequired(this.tableId)) {
			return;
		}
		if (!flag) {
			return;
		}

		let url = '/nccloud/cmp/paybills/save.do'; //新增保存
		let cacheFlag = true;
		if (this.props.getUrlParam('status') === 'edit') {
			url = '/nccloud/cmp/paybills/update.do'; //修改保存
			cacheFlag = false;
		}

		if (this.props.getUrlParam('status') === 'add' && this.props.getUrlParam('src') === 'settlement') {
			url = '/nccloud/cmp/paybills/assave.do'; //修改保存
			cacheFlag = true;
		}
		if (this.props.getUrlParam('status') === 'edit' && this.props.getUrlParam('op') === 'protopay') {
			url = '/nccloud/cmp/paybills/commisionpay.do'; //承付修改
			if (this.protoPayCheck(CardData)) {
				return;
			}
			cacheFlag = false;
		}
		// if (this.props.getUrlParam('status') === 'edit' && this.props.getUrlParam('op') === 'unprotopay') {
		// 	url = '/nccloud/cmp/paybills/canlecompay.do'; //承付修改
		// 	cacheFlag = false;
		// }

		ajax({
			url: url,
			data: CardData,
			success: (res) => {
				let pk_paybill = null;
				let pagecode = '';
				let bill_no = '';
				if (res.success) {
					if (res.data) {
						//放入缓存
						toast({
							color: 'success',
							content:
								this.props.MutiInit.getIntl('36070PBR') &&
								this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000005')
						}); /* 国际化处理： 保存成功*/
						if (res.data.head && res.data.head[this.formId]) {
							this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });

							pk_paybill = res.data.head[this.formId].rows[0].values.pk_paybill.value;
							bill_no = res.data.head[this.formId].rows[0].values.bill_no.value;
							//	pagecode = res.data.head[this.formId].rows[0].values.trade_type.value;
							let ntbMessage = res.data.head[this.formId].rows[0].values.ntberrmsg.value;
							if (ntbMessage) {
								toast({
									color: 'warning',
									content: ntbMessage
								});
							}
						}
						this.billno = bill_no;
						this.billId=pk_paybill;
						this.props.beforeUpdatePage();
					    
						if (res.data.body && res.data.body[this.tableId]) {	
							let body=res.data.body;
							 body[PAYBILL_CONST.card_table_id] = this.props.cardTable.updateDataByRowId(
								this.tableId,
								res.data.body[this.tableId]
							);
							if (body&&body[PAYBILL_CONST.card_table_id]) {
								res.data.body = body;
							}
						}
						if (cacheFlag) {
							addCache(pk_paybill, res.data, PAYBILL_CONST.card_from_id, PAYBILL_CONST.paybillCacheKey,res.data.head[this.formId].rows[0].values);
						} else {
							updateCache(
								PAYBILL_CONST.paybill_pkname,
								pk_paybill,
								res.data,
								PAYBILL_CONST.card_from_id,
								PAYBILL_CONST.paybillCacheKey,res.data.head[this.formId].rows[0].values
							);
						}

						//this.updateCacheData(PAYBILL_CONST.paybill_pkname,bill_pk, res.data, PAYBILL_CONST.card_from_id, PAYBILL_CONST.paybillCacheKey);
					}
				}
				//this.props.pushTo("/card",{status:"browse",id: pk_paybill,pagecode: pagecode});
				this.props.setUrlParam({
					status: 'browse',
					id: pk_paybill
					//pagecode: pagecode
				});

				this.toggleShow();
				this.props.updatePage(this.formId, this.tableId, null);
			}
		});
	};
	protoPayInfo = () => {
		if (this.props.getUrlParam('op') === 'unprotopay' || this.props.getUrlParam('op') === 'protopay') {
			this.props.cardTable.setColEditableByKey('paybilldetail_table', 'cf_type', false);
			this.props.cardTable.setColEditableByKey('paybilldetail_table', 'cf_status', true);
			this.props.cardTable.setColEditableByKey('paybilldetail_table', 'refusenote', false);
			this.props.cardTable.setColEditableByKey('paybilldetail_table', 'is_refuse', false);
		}

		if (this.props.getUrlParam('op') === 'unprotopay') {
			this.props.cardTable.setColValue('paybilldetail_table', 'cf_type', {
				display: null,
				value: null
			});
			this.props.cardTable.setColValue('paybilldetail_table', 'cf_status', {
				display: null,
				value: null
			});
			this.props.cardTable.setColValue('paybilldetail_table', 'refusenote', {
				display: null,
				value: null
			});
			this.props.cardTable.setColValue('paybilldetail_table', 'is_refuse', {
				display: null,
				value: null
			});
		}
	};
	cardConstructor = (key) => {};

	cmp49Handle = (props, org, moduleId) => {
		ajax({
			url: '/nccloud/cmp/pub/getpara.do',
			//参数返回类型type， int ,string,boolean
			//组织pk_org
			//参数编码paracode
			data: { paracode: 'CMP49', pk_org: org, type: 'boolean' },
			success: function(res) {
				let { success, data } = res;
				if (res.data.CMP49) {
					if (moduleId == 'head') {
						let meta = props.meta.getMeta();
						let item = meta['head'].items.find((e) => e.attrcode === 'objecttype');
						let itemform = meta['childform1'].items.find((e) => e.attrcode === 'objecttype');
						item.options = [
							{
								display: '',
								value: ''
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000000') /* 国际化处理： 客户*/,
								value: '0'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000001') /* 国际化处理： 供应商*/,
								value: '1'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000002') /* 国际化处理： 部门*/,
								value: '2'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000003') /* 国际化处理： 人员*/,
								value: '3'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000004') /* 国际化处理： 散户*/,
								value: '4'
							}
						];

						let tableItem = meta['paybilldetail_table'].items.find((e) => e.attrcode === 'objecttype');
						tableItem.options = [
							{
								display: '',
								value: ''
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000000') /* 国际化处理： 客户*/,
								value: '0'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000001') /* 国际化处理： 供应商*/,
								value: '1'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000002') /* 国际化处理： 部门*/,
								value: '2'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000003') /* 国际化处理： 人员*/,
								value: '3'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000004') /* 国际化处理： 散户*/,
								value: '4'
							}
						];
						itemform.options = [
							{
								display: '',
								value: ''
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000000') /* 国际化处理： 客户*/,
								value: '0'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000001') /* 国际化处理： 供应商*/,
								value: '1'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000002') /* 国际化处理： 部门*/,
								value: '2'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000003') /* 国际化处理： 人员*/,
								value: '3'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000004') /* 国际化处理： 散户*/,
								value: '4'
							}
						];
						
						props.renderItem('form', 'head', 'objecttype', null);
						props.renderItem('table', 'paybilldetail_table', 'objecttype', null);
						props.renderItem('form', 'childform1', 'objecttype', null);
						props.meta.setMeta(meta);
					}
					if (moduleId == 'paybilldetail_table') {
						let meta = props.meta.getMeta();
						let tableItem = meta['paybilldetail_table'].items.find((e) => e.attrcode === 'objecttype');
						let itemform = meta['childform1'].items.find((e) => e.attrcode === 'objecttype');
						tableItem.options = [
							{
								display: '',
								value: ''
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000000') /* 国际化处理： 客户*/,
								value: '0'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000001') /* 国际化处理： 供应商*/,
								value: '1'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000002') /* 国际化处理： 部门*/,
								value: '2'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000003') /* 国际化处理： 人员*/,
								value: '3'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000004') /* 国际化处理： 散户*/,
								value: '4'
							}
						];
						itemform.options = [
							{
								display: '',
								value: ''
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000000') /* 国际化处理： 客户*/,
								value: '0'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000001') /* 国际化处理： 供应商*/,
								value: '1'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000002') /* 国际化处理： 部门*/,
								value: '2'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000003') /* 国际化处理： 人员*/,
								value: '3'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000004') /* 国际化处理： 散户*/,
								value: '4'
							}
						];
						props.renderItem('table', 'paybilldetail_table', 'objecttype', null);
						props.renderItem('form', 'childform1', 'objecttype', null);
						props.meta.setMeta(meta);
					}
				} else {
					if (moduleId == 'head') {
						let meta = props.meta.getMeta();
						let item = meta['head'].items.find((e) => e.attrcode === 'objecttype');
						let itemform = meta['childform1'].items.find((e) => e.attrcode === 'objecttype');

						item.options = [
							{
								display: '',
								value: ''
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000000') /* 国际化处理： 客户*/,
								value: '0'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000001') /* 国际化处理： 供应商*/,
								value: '1'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000002') /* 国际化处理： 部门*/,
								value: '2'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000003') /* 国际化处理： 人员*/,
								value: '3'
							}
						];
						let tableItem = meta['paybilldetail_table'].items.find((e) => e.attrcode === 'objecttype');
						tableItem.options = [
							{
								display: '',
								value: '0'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000000') /* 国际化处理： 客户*/,
								value: '0'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000001') /* 国际化处理： 供应商*/,
								value: '1'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000002') /* 国际化处理： 部门*/,
								value: '2'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000003') /* 国际化处理： 人员*/,
								value: '3'
							}
						];
						itemform.options = [
							{
								display: '',
								value: ''
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000000') /* 国际化处理： 客户*/,
								value: '0'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000001') /* 国际化处理： 供应商*/,
								value: '1'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000002') /* 国际化处理： 部门*/,
								value: '2'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000003') /* 国际化处理： 人员*/,
								value: '3'
							}
							
						];

						props.renderItem('form', 'head', 'objecttype', null);
						props.renderItem('table', 'paybilldetail_table', 'objecttype', null);
						props.renderItem('form', 'childform1', 'objecttype', null);
						props.meta.setMeta(meta);
					}
					if (moduleId == 'paybilldetail_table') {
						let meta = props.meta.getMeta();
						let tableItem = meta['paybilldetail_table'].items.find((e) => e.attrcode === 'objecttype');
						let itemform = meta['childform1'].items.find((e) => e.attrcode === 'objecttype');
						tableItem.options = [
							{
								display: '',
								value: ''
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000000') /* 国际化处理： 客户*/,
								value: '0'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000001') /* 国际化处理： 供应商*/,
								value: '1'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000002') /* 国际化处理： 部门*/,
								value: '2'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000003') /* 国际化处理： 人员*/,
								value: '3'
							}
						];
						itemform.options = [
							{
								display: '',
								value: ''
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000000') /* 国际化处理： 客户*/,
								value: '0'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000001') /* 国际化处理： 供应商*/,
								value: '1'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000002') /* 国际化处理： 部门*/,
								value: '2'
							},
							{
								display:
									props.MutiInit.getIntl('36070PBR') &&
									props.MutiInit.getIntl('36070PBR').get('36070PBR-000003') /* 国际化处理： 人员*/,
								value: '3'
							}
							
						];
						props.renderItem('form', 'childform1', 'objecttype', null);
						props.renderItem('table', 'paybilldetail_table', 'objecttype', null);
						props.meta.setMeta(meta);
					}
				}
			}
		});
	};

	//删除单据
	delConfirm = () => {
		let deleteId = this.props.form.getFormItemsValue(this.formId, 'pk_paybill').value;
		//this.props.getUrlParam('id');
		let pkMapTs = {};
		pkMapTs[deleteId] = this.props.form.getFormItemsValue(this.formId, 'ts').value;
		let data = {
			pkMapTs,
			pageid: PAYBILL_CONST.card_page_id,
			pk: deleteId
		};
		ajax({
			url: '/nccloud/cmp/billmanagement/delete.do',
			data: data,
			// data: {
			// 	pk: deleteId,
			// 	ts: this.props.form.getFormItemsValue(this.formId, 'ts').value
			// },
			success: (res) => {
				if (res.success) {
					let { data, status } = res.data;
					if (status == 1) {
						toast({
							color: 'success',
							content:
								this.props.MutiInit.getIntl('36070PBR') &&
								this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000028')
						}); /* 国际化处理： 删除成功*/

						//let idObj = { id: this.props.getUrlParam('id'), status: 3 };
						let url_id = getNextId(deleteId, PAYBILL_CONST.paybillCacheKey);
						//删除缓存
						deleteCacheById(PAYBILL_CONST.paybill_pkname, deleteId, PAYBILL_CONST.paybillCacheKey);
						//let url_id = this.props.cardPagination.getNextCardPaginationId(idObj);
						if (!url_id) {
							this.billno = '';
							this.props.form.EmptyAllFormValue(this.formId);
							//this.props.form.setFormStatus(this.formId, 'browse');
							this.props.cardTable.setTableData(this.tableId, { rows: [] });
							this.props.setUrlParam({
								status: 'browse',
								id: ''
							});
							this.toggleShow();
						} else {
							pageInfoClick.call(this, this.props, url_id);
						}
					} else {
						let msgData = data[0];
						toast({ color: 'danger', content: msgData.msg });
					}
				}
			}
		});
	};

	//审批按钮操作事件
	approveRadio = (val) => {
		this.setState(
			{
				approveType: val
			},
			() => console.log(this.state)
		);
	};
	//输入意见输入框
	suggestChange = (val) => {
		this.setState({
			suggestion: val
		});
	};
	createApprove = () => {
		//审批流程
		let { approveDetail } = this.props;
		let { ApproveDetails, approveType, approveList, suggestion, billID } = this.state;
		return approveDetail.create('demo1', {
			data: ApproveDetails,
			approveType,
			suggestion,
			approveList,
			needInput: true,
			approveRadio: this.approveRadio.bind(this),
			suggestChange: this.suggestChange.bind(this),
			billID
		});
	};
	//审批单据
	approve = (name) => {
		let { approveType, suggestion, ApproveDetails } = this.state;
		let approveResult =
			approveType == 'approve' ? 'Y' : approveType == 'Noapprove' ? 'N' : approveType == 'reject' ? 'R' : '';
		let jumpToActivity = approveType == 'approve' ? null : ApproveDetails[ApproveDetails.length - 2].activityID;
		let data = {
			approveResult,
			jumpToActivity,
			checknote: suggestion,
			billid: this.props.form.getFormItemsValue(this.formId, 'crevecontid').value,
			ts: this.props.form.getFormItemsValue(this.formId, 'ts').value,
			billOrTranstype: this.props.form.getFormItemsValue(this.formId, 'vtrantypecode').value,
			//hasApproveflowDef: this.props.form.getFormItemsValue(this.formId, 'bcloseflag').value,
			userid: '1001A41000000000592P',
			actionname: name ? name : 'APPROVE'
		};
		ajax({
			url: '/nccloud/reva/revebill/approve.do',
			data,
			success: (res) => {
				if (res.data) {
					console.log(res);
				}
			}
		});
	};
	getButtonNames = (codeId) => {
		if (codeId === 'edit' || codeId === 'add') {
			return 'main-button';
		} else {
			return 'secondary-button';
		}
	};
	//获取列表肩部信息


	//获取列表肩部信息
	getTableHead = (buttons, tableId) => {
		let { createButton } = this.props.button;
		return (
			<div className="shoulder-definition-area">
				<div className="definition-icons">
					{this.props.cardTable.createBrowseIcons(tableId, {
						iconArr: [ 'close', 'open', 'max' ],
						maxDestAreaId: 'finance-fts-commissionpayment-card'
					})}
					{this.props.button.createButtonApp({
						area: 'card_body',
						buttonLimit: 4,
						onButtonClick: buttonClick.bind(this),
						popContainer: document.querySelector('.header-button-area')
					})}
				</div>
			</div>
		);
	};
	beSureBtnClick = () => {
		let pk_org = this.props.form.getFormItemsValue('head', 'pk_org').value;
		let pk_org_dis = this.props.form.getFormItemsValue('head', 'pk_org').display;
		let oldType= this.props.form.getFormItemsValue('head', 'pk_tradetypeid');
		let oldTypeCode= this.props.form.getFormItemsValue('head', 'trade_type');
		this.props.form.cancel('head');

		this.props.form.setFormItemsValue('head', {
			pk_org: {
				value: pk_org,
				display: pk_org_dis
			}
		});
		if(oldType){
			this.props.form.setFormItemsValue('head', {
				pk_tradetypeid: {
					value: oldType.value,
					display: oldType.display
				}
			});
		}
		if(oldTypeCode&&oldTypeCode.value){
			this.props.form.setFormItemsValue('head', {
				trade_type: {
					value: oldTypeCode.value,
					display: oldTypeCode.value
				}
			});
		}
		this.props.form.setFormStatus(this.formId, 'edit');
		//this.props.cardTable.setTableData(this.tableId, { rows: [] });
		//this.props.cardTable.resetTableData(this.tableId);
		this.props.cardTable.setStatus(this.tableId, 'edit');
		let eventdata = {};
		if (!pk_org) {
			this.props.initMetaByPkorg();
			this.props.button.setButtonDisabled([ 'addline' ], true);
			this.props.cardTable.setTableData(this.tableId, { rows: [] });
			return;
		} else {
			this.props.cardTable.setTableData(this.tableId, { rows: [] });
			this.props.cardTable.addRow(this.tableId);
			this.props.button.setButtonDisabled([ 'addline', 'delline', 'copyline' ], false);
			this.cmp49Handle.call(this, this.props, pk_org);
			eventdata = this.props.createHeadAfterEventData(
				'36070PBR_D5_card',
				'head',
				'paybilldetail_table',
				'head',
				'pk_org',
				this.value
			);
			eventdata.newvalue = {};
			eventdata.oldvalue = {};
			//eventdata.card.head.head.rows[0].values.pk_org.value=pk_org;
			//eventdata.card.head.head.rows[0].values.pk_org.display=pk_org_dis;
			ajax({
				url: '/nccloud/cmp/paybills/orgchange.do',
				data: eventdata,
				success: (res) => {
					if (res.success) {
						if (res.data) {
							if (res.data.head) {
								this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
							}
							if (res.data.body) {
								this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
							}
						} else {
							this.props.form.setAllFormValue({ [this.formId]: { rows: [] } });
							this.props.cardTable.setTableData(this.tableId, { rows: [] });
						}
					}
					this.toggleShow();
				}
			});
		}
		//清空表格
		//this.props.form.EmptyAllFormValue('head');
	};
	cancelBtnClick = () => {
		console.log(
			this.props.MutiInit.getIntl('36070PBR') && this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000029'),
			this.state.oldorg
		); /* 国际化处理： 修改之前的财务组织*/
		this.props.form.setFormItemsValue('head', {
			pk_org: { value: this.state.oldorg, display: this.state.oldorgDis }
		});
		this.props.form.setFormStatus(this.formId, 'edit');
	};
	beforeUpload(billId, fullPath, file, fileList) {
		// 参数：单据id，当前选中分组path、当前上传文件对象，当前文件列表
		console.log(billId, fullPath, file, fileList);

		const isJPG = file.type === 'image/jpeg';
		if (!isJPG) {
			alert(
				this.props.MutiInit.getIntl('36070PBR') &&
					this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000030')
			); /* 国际化处理： 只支持jpg格式图片*/
		}
		const isLt2M = file.size / 1024 / 1024 < 2;
		if (!isLt2M) {
			alert(
				this.props.MutiInit.getIntl('36070PBR') &&
					this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000031')
			); /* 国际化处理： 上传大小小于2M*/
		}
		return isJPG && isLt2M;
		// 备注： return false 不执行上传  return true 执行上传
	}
	cancel = () => {
		this.setState({
			shoWIns: false
		});
	};
	affirm = (info) => {
		console.log(info);
		this.setState({
			shoWIns: false
		});
	};
	closeModal = () => {
		this.setState({
			billCodeModalShow: false
		});
	};
	click = () => {
		// alert('1');
		// this.setState({
		//  sourceData: text1,
		//  show: true
		// })
	};
	// 附件的关闭点击
	onHideUploader = () => {
		this.setState({
			showUploader: false
		});
		
	}; //卡片返回按钮

	handleClick = () => {
		//保证
		
			this.props.pushTo('/list');
			
	};
	updateCacheData = (pk_field, pkvalue, cacheData, moduleId, datasource) => {

		let cache = getCacheById(pkvalue, datasource);
		if (cache) {
			updateCache(pk_field, pkvalue, cacheData, moduleId, datasource);
		} else {
			addCache(pkvalue, cacheData, moduleId, datasource);
		}
	};
	closeApprove = () => {
		this.setState({
			showAppr: false
		});
	};
	//指派
	getAssginUsedr = (value) => {
		this.setState(
			{
				getAssginUsedr: value
			},
			() => {
				buttonClick.call(this, this.props, 'commitConfirm');
			}
		);
	};
	turnOff = () => {
		this.setState({
			compositedisplay: false
		});
	};

	buttonUsability = () => {
		let Rows = this.props.cardTable.getCheckedRows(this.tableId);
		if (Rows && Rows.length >= 1) {
			this.props.button.setButtonDisabled([ 'addline', 'delline', 'copyline' ], false);
		} else {
			this.props.button.setButtonDisabled([ 'delline', 'copyline' ], true);
		}
	};
	getCacheDataById=(pk)=>{
	let CacheData = getCacheById(pk, PAYBILL_CONST.paybillCacheKey); 
	if (CacheData) {
		this.props.setUrlParam({
			status: 'browse'		
		});
	    let billno = CacheData.head[PAYBILL_CONST.card_from_id].rows[0].values.bill_no.value;
        let  billId = CacheData.head[PAYBILL_CONST.card_from_id].rows[0].values.pk_paybill.value;
		this.props.form.setAllFormValue({ [this.formId]: CacheData.head[PAYBILL_CONST.card_from_id] });
		this.props.cardTable.setTableData(this.tableId, CacheData.body[PAYBILL_CONST.card_table_id]);
		this.billno = billno;
		this.billId = billId;
		this.toggleShow();
		return true;
	} else{
		return false;
	}
	}
	gotoLink=(link)=>{
		    if(link){
			        window.open(link)
		    };
	}
	render() {
		let self=this;
		let { cardTable, form, button, modal, cardPagination } = this.props;
		const { createBillHeadInfo } = this.props.BillHeadInfo;
		let { getDefData } = cardCache;
		let buttons = this.props.button.getButtons();
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createCardPagination } = cardPagination;
		let { createButton, createButtonApp } = button;
		const { ncmodal } = this.props;
		let { createModal } = ncmodal;
		let status = this.props.getUrlParam('status');
		let ISlINK = getDefData(PAYBILL_CONST.link_key, PAYBILL_CONST.paybillCacheKey);
		let tradeType = JSON.parse(sessionStorage.getItem('sessionTP'));
		// // 按钮新加杨晓东
		// let showTradeBtn=true;
		// if(this.props.getUrlParam('scene')){
		// 	showTradeBtn=false;
		// }

		//this.props.getUrlParam('status');
		//let { createModal } = modal;
		let { showUploader, target, tradetype, shoWIns, sourceData, showNCbackBtn } = this.state;
		return (
			<div className="nc-bill-card">
				<div className="nc-bill-top-area">
					<NCAffix>
						<div className="nc-bill-header-area">
							<div className="header-title-search-area">
								
									{createBillHeadInfo({
										title:tradeType&& tradeType.refname?tradeType&& tradeType.refname:
											this.props.MutiInit.getIntl('36070PBR') &&
											this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000045'),
										billCode: this.billno,
										backBtnClick: () => {
											this.handleClick();
										}
									})}
							
							</div>

							<div className="header-button-area">
								<div className="button-app-wrapper">
									{this.props.ViewModel.getData('status') === 'browse' && !ISlINK && this.state.showTradeBtn && (
										<Refer
											placeholder={
												this.props.MutiInit.getIntl('36070PBR') &&
												this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000032')
											} /* 国际化处理： 单据模板类型*/
											refName={
												this.props.MutiInit.getIntl('36070PBR') &&
												this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000033')
											} /* 国际化处理： 付款交易类型*/
											refCode={'tradetype001'}
											refType={'grid'}
											queryGridUrl={'/nccloud/riart/ref/fiBillTypeTableRefAction.do'}
											queryCondition={{
												parentbilltype: 'F5' //过滤条件
											}}
											value={this.state.tradetype}
											onChange={(value) => {
												this.setState(
													{
														tradetype: value
													},
													function() {
														sessionStorage.setItem(
															'sessionTP',
															JSON.stringify(this.state.tradetype)
														);
													}
												);
											}}
											isMultiSelectedEnabled={false}
											clickContainer={
												<NCButton>
													{this.props.MutiInit.getIntl('36070PBR') &&
														this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000033')}
												</NCButton>
											} /* 国际化处理： 付款交易类型*/
										/>
									)}
								</div>
								<div>
									{this.props.button.createButtonApp({
										area: 'card_head',
										buttonLimit: 4,
										onButtonClick: buttonClick.bind(this)
										//popContainer: document.querySelector('.header-button-area')
									})}
								</div>
							</div>
							<div className="header-cardPagination-area" style={{ float: 'right' }}>
								{createCardPagination({
									dataSource: PAYBILL_CONST.paybillCacheKey,
									handlePageInfoChange: pageInfoClick.bind(this)
								})}
							</div>
						</div>
					</NCAffix>
					<NCScrollElement name="forminfo">
						<div className="nc-bill-form-area">
							{createForm(this.formId, {
								onBeforeEvent: beforeEvent.bind(this),
								onAfterEvent: afterEvent.bind(this)
							})}
						</div>
					</NCScrollElement>
				</div>
				<NCScrollElement name="businfo">
					<div className="nc-bill-bottom-area">
						<div className="nc-bill-table-area">
							{/* {this.getTableHead(buttons, this.tableId)} */}
							{createCardTable(this.tableId, {
								tableHead: this.getTableHead.bind(this, buttons, this.tableId),
								modelSave: ()=>{
									this.saveBill();
									this.props.cardTable.closeModel(PAYBILL_CONST.card_table_id);
								},
								onAfterEvent: afterEvent.bind(this),
								onBeforeEvent: beforeEvent.bind(this),
								showCheck: true,
								showIndex: true,
								onSelected: this.buttonUsability.bind(this),
								onSelectedAll: this.buttonUsability.bind(this),
								modelAddRowBefore:()=> {this.checkSettleInfo()},
								modelAddRow: (props, moduleId, index) => {	
										index = Number(index) + Number(1);
										addline(props, PAYBILL_CONST.DataArr, index);	
								 }
							})}
						</div>
					</div>
				</NCScrollElement>
				<div className="nc-faith-demo-div2">
					{/* 这里是附件上传组件的使用，需要传入三个参数 */}
					{showUploader && (
						<NCUploader
							billId={this.billId}
							target={target}
							placement={'bottom'}
							billNo={this.billno}
							disableModify={this.props.form.getFormItemsValue(this.formId, 'bill_status')&&(this.props.form.getFormItemsValue(this.formId, 'bill_status').value!="-10"&&this.props.form.getFormItemsValue(this.formId, 'bill_status').value!="-99")}
							onHide={this.onHideUploader}
							customInterface={
								{
									queryLeftTree: commonurl.lefttreequery,
									queryAttachments: PAYBILL_CONST.upload_url
								}
							}

						/>
					)}
				</div>
				<div>
					<ApproveDetail
						show={this.state.showAppr}
						close={this.closeApprove}
						billid={this.billId}
						billtype={this.state.billtype}
					/>
				</div>
				<div>
					<BillTrack
						show={this.state.show}
						close={() => {
							this.setState({ show: false });
						}}
						pk={this.billId}
						type="F5" //单据类型
						//billtype={this.state.billtype}
					/>
				</div>
				<div>
					<Inspection
						show={this.state.shoWIns}
						sourceData={this.state.sourceData}
						cancel={this.cancel.bind(this)}
						affirm={this.affirm.bind(this)}
					/>
				</div>
				<div>
					<PrintOutput
						ref="printOutput"
						url="/nccloud/cmp/paybills/paybillsprint.do"
						data={this.state.outputData}
						//callback={this.onSubmit}
					/>
				</div>
				
				<div>
                <InvoiceLink 
               {...this.state.sscrpLinkInvoiceData}
                table={this.props.table}
                />
				</div> 
				<div>
					{this.state.compositedisplay ? (
						<ApprovalTrans
							title={	this.props.MutiInit.getIntl('36070PBR') &&this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000114')}
							data={this.state.compositedata}
							//data={this.compositedata}
							display={this.state.compositedisplay}
							getResult={this.getAssginUsedr}
							cancel={this.turnOff}
						/>
					) : (
						''
					)}
				</div>
				<div>
				<InvoiceUploader
               {...this.state.sscrpInvoiceData}
                />
				</div>    
				<div>
			    <InvoiceLink 
               {...this.state.sscrpInvoiceData}
                table={this.props.table}
                />
                </div>   
				{createModal('delete', {
					title:
						this.props.MutiInit.getIntl('36070PBR') &&
						this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000035'), // 弹框表头信息/* 国际化处理： 确认删除*/
					content:
						this.props.MutiInit.getIntl('36070PBR') &&
						this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000036'), //this.modalContent(), //弹框内容，可以是字符串或dom/* 国际化处理： 确认删除所选数据吗?*/
					beSureBtnClick: this.delConfirm, //点击确定按钮事件
					//cancelBtnClick: this.cancelBtnClick, //取消按钮事件回调
					userControl: false, // 点确定按钮后，是否自动关闭弹出框.true:手动关。false:自动关
					noFooter: false, //是否需要底部按钮,默认true
					rightBtnName:
						this.props.MutiInit.getIntl('36070PBR') &&
						this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000037'), //左侧按钮名称,默认关闭/* 国际化处理： 取消*/
					leftBtnName:
						this.props.MutiInit.getIntl('36070PBR') &&
						this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000038') //右侧按钮名称， 默认确认/* 国际化处理： 确认*/
				})}
				{createModal('cancelModal', {
					title:
						this.props.MutiInit.getIntl('36070PBR') &&
						this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000039'), // 弹框表头信息/* 国际化处理： 确认取消*/
					content:
						this.props.MutiInit.getIntl('36070PBR') &&
						this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000040'), //this.modalContent(), //弹框内容，可以是字符串或dom/* 国际化处理： 是否确认取消*/
					userControl: false, // 点确定按钮后，是否自动关闭弹出框.true:手动关。false:自动关
					noFooter: false, //是否需要底部按钮,默认true
					rightBtnName:
						this.props.MutiInit.getIntl('36070PBR') &&
						this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000037'), //左侧按钮名称,默认关闭/* 国际化处理： 取消*/
					leftBtnName:
						this.props.MutiInit.getIntl('36070PBR') &&
						this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000038') //右侧按钮名称， 默认确认/* 国际化处理： 确认*/
				})}
				{createModal('addNode', {
					title:
						this.props.MutiInit.getIntl('36070PBR') &&
						this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000041'), // 弹框表头信息/* 国际化处理： 确认修改*/
					content:
						this.props.MutiInit.getIntl('36070PBR') &&
						this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000042'), //this.modalContent(), //弹框内容，可以是字符串或dom/* 国际化处理： 是否修改组织，这样会清空您录入的信息?*/
					beSureBtnClick: this.beSureBtnClick, //点击确定按钮事件
					cancelBtnClick: this.cancelBtnClick, //取消按钮事件回调
					userControl: false, // 点确定按钮后，是否自动关闭弹出框.true:手动关。false:自动关
					noFooter: false, //是否需要底部按钮,默认true
					rightBtnName:
						this.props.MutiInit.getIntl('36070PBR') &&
						this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000037'), //左侧按钮名称,默认关闭/* 国际化处理： 取消*/
					leftBtnName:
						this.props.MutiInit.getIntl('36070PBR') &&
						this.props.MutiInit.getIntl('36070PBR').get('36070PBR-000038') //右侧按钮名称， 默认确认/* 国际化处理： 确认*/
				})}

				{/*联查合同的弹框*/}
				{this.props.modal.createModal('linkcontract',{size:'xlg',noFooter:true})};
				
			</div>
		);
	}
}

Card = createPage({
	mutiLangCode: '36070PBR',
	billinfo: {
		billtype: 'card',
		pagecode: PAYBILL_CONST.card_page_id,
		headcode: PAYBILL_CONST.card_from_id,
		bodycode: PAYBILL_CONST.card_table_id
	},
	orderOfHotKey: [ PAYBILL_CONST.card_from_id,PAYBILL_CONST.card_table_id]
})(Card);
export default Card;

// Card = createPage({
// 	// initTemplate: initTemplate,
//mutiLangCode: '36070PBR'
// })(Card);

// ReactDOM.render(<Card />, document.querySelector('#app'));
