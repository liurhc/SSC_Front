import { ajax, toast } from 'nc-lightapp-front';
import { ObjectTypeHandle, NoteTypeHandle, RateHandle,ObjectTypeHandleforFormReleation } from '../../util/ReferChangeEvent';
import { BodyBeforeEvent, FormBeforeEvent } from '../../util/TableRefFilter';
import { RetailHandle } from '../../util/RetailHandle';
import { PAYBILL_CONST } from '../../cons/constant.js';
export default function beforeEvent(props, moduleId, key, value, index, record) {
	let pk_org = props.form.getFormItemsValue(PAYBILL_CONST.card_from_id, 'pk_org');

	if (moduleId == PAYBILL_CONST.card_from_id) {
		switch (key) {
			case 'objecttype': //收款财务组织
				this.cmp49Handle.call(this, props, pk_org && pk_org.value, moduleId);
				return true;
				case 'pk_account': //收款银行账户
				let objectType =props.form.getFormItemsValue(moduleId, 'objecttype');
				if (!objectType || !objectType.value) {
					return true;
				}
				ObjectTypeHandle(props, moduleId, objectType.value, null);
				ObjectTypeHandleforFormReleation(props, moduleId, objectType.value,null);
				return true;
				break;
			 default:
			  FormBeforeEvent.call(this,props, moduleId, key);
				return true; //默认单元格都可操作
				break;
		}
	}
	if (moduleId == PAYBILL_CONST.card_table_id) {
		switch (key) {
			case 'pk_account': //收款银行账户
				let objectType = record.values.objecttype;
				if (!objectType || !objectType.value) {
					return true;
				}
				ObjectTypeHandle(props, moduleId, objectType.value, index);
				return true;
			case 'note_no': //票据号
				let noteType = record.values.note_type;
				if (noteType && noteType.value) {
					ajax({
						url: '/nccloud/cmp/pub/noteTypeHandler.do',
						data: { pk: noteType.value },
						success: function(res) {
							NoteTypeHandle.call(this, props, moduleId, res.data.note_type, index);
						}
					});
				}
				return true;
				break;

			case 'accounttype':
				RetailHandle(props, index, record);
				return true;
				break;
			case 'accountopenbank':
				RetailHandle(props, index, record);
				return true;
				break;
			case 'accountname':
				RetailHandle(props, index, record);
				return true;
				break;
			case 'local_rate': //收款财务组织
				RateHandle(props, moduleId, index, record);
				return true;
				break;
			//增加散户
			case 'objecttype': //收款财务组织
				this.cmp49Handle.call(this, props, pk_org && pk_org.value, moduleId);
				return true;
				break;
			default:
			BodyBeforeEvent.call(this,props, moduleId, key, value, index, record);
			return true; //默认单元格都可操作
			 break;
		}
	}
	return true;
}
