import { base, ajax } from 'nc-lightapp-front';
let { NCPopconfirm } = base;
import { buttonVisible } from './buttonVisible';
import {RowDetailButton} from '../../../../../tmpub/pub/util/RowDetailButton'; 
import { PAYBILL_CONST } from '../../cons/constant.js';
import tableButtonClick from './tableButtonClick';
import { RefFilter, FormRefFilter } from '../../util/TableRefFilter';
let formId = PAYBILL_CONST.card_from_id;
let tableId =PAYBILL_CONST.card_table_id;

let pageId = PAYBILL_CONST.card_page_id;
let childformId = 'childform1';
export default function(props) {
	let that = this;

	if (props.getUrlParam('pagecode')) {
		pageId = props.getUrlParam('pagecode');
	}
	props.createUIDom(
		{
			pagecode: pageId //页面id
			//  appcode: '36070PBR',
			//  appid: '0001Z61000000001PJBL' //注册按钮的id
		},
		function(data) {
			if (data) {
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button, () => {
						buttonVisible.call(that, props);
					});
				
				}
				if (data.template) {
					let meta = data.template;
					 modifierMeta(that, props, meta);
					 props.meta.setMeta(meta, () => {
						//给票据设置默认值
						if (props.getUrlParam('status') == 'add') {	
							let pk_org =props.form.getFormItemsValue('head', 'pk_org');
							if (!pk_org || !pk_org.value) {
								props.initMetaByPkorg();
								//props.form.setFormItemsDisabled('head', { pk_org: false }); //财务组织、
							}
						} 
					});
					//存在组织 先增行 传到增行
					if (props.getUrlParam('status') == 'add') {	
						that.deaultAddLine.call(that,data);
					}
					that.initData.call(that);
				};
			}
		}
	);
}

function modifierMeta(that, props, meta) {
	let status = props.getUrlParam('status');
	meta[formId].status = status;
	meta[tableId].status = status;

	//form 去除缓存 历史记录
	meta[formId].items.find((e) => e.attrcode === 'pk_org').isMultiSelectedEnabled = false;
	//财务组织:全加载
	meta[formId].items.find((e) => e.attrcode === 'pk_org').isTreelazyLoad = false;
	meta[formId].items.find((e) => e.attrcode === 'pk_org').showHistory = false;
	meta[formId].items.find((e) => e.attrcode === 'mon_account').showHistory = false;
	meta[formId].items.find((e) => e.attrcode === 'pk_supplier').showHistory = false;
	// //form参照过滤
	// meta[formId].items.map((item) => {
	// 	 FormRefFilter.call(this, props, item);
	// });

	//table去除缓存 历史记录
	meta[tableId].items.find((e) => e.attrcode === 'mon_account').showHistory = false;
	meta[tableId].items.find((e) => e.attrcode === 'pk_account').showHistory = false;
	meta[tableId].items.find((e) => e.attrcode === 'pk_supplier').showHistory = false;
	meta[tableId].items.find((e) => e.attrcode === 'pk_oppaccount').showHistory = false;
	//table参照过滤
	// meta[tableId].items.map((item) => {
	// 	RefFilter.call(this, props, item);
	// });
	meta[childformId].items.find((e) => e.attrcode === 'mon_account').showHistory = false;
	meta[childformId].items.find((e) => e.attrcode === 'pk_account').showHistory = false;
	meta[childformId].items.find((e) => e.attrcode === 'pk_supplier').showHistory = false;
	meta[childformId].items.find((e) => e.attrcode === 'pk_oppaccount').showHistory = false;
	//侧拉form 参照过滤
	meta[childformId].items.map((item) => {
		RefFilter.call(this, props, item);
	});

	
	let porCol = {
		attrcode: 'opr',
		//label: multiLang && multiLang.get('20521030-0005'),
		label:
			that.props.MutiInit.getIntl('36070PBR') &&
			that.props.MutiInit.getIntl('36070PBR').get('36070PBR-000018') /* 国际化处理： 操作*/,
		visible: true,
		itemtype: 'customer',
		width: '210px',
		fixed: 'right',
		render: (text, record, index) => {
			let  status= props.cardTable.getStatus(tableId);
			let buttonAry =
				props.getUrlParam('status') === 'browse'
					? ['']
					: that.state.pasteflag ? [ 'CopyAtLine' ] : [ 'openedit', 'copybody', 'insertline', 'deleteline' ];
					return status === 'browse' ? <span class="row-edit-option row-btn-Open"
					 onClick={()=>{
						props.cardTable.toggleRowView(PAYBILL_CONST.card_table_id, record);
					 }}>
					{record.expandRowStatus ? props.MutiInit.getIntl('36070PBR') && props.MutiInit.getIntl('36070PBR').get('36070PBR-000116'):
				      props.MutiInit.getIntl('36070PBR') && props.MutiInit.getIntl('36070PBR').get('36070PBR-000115')
				 } 
					</span>
					// <RowDetailButton
					// 	record={record}
					// 	bodyAreaCode={tableId}
					// 	props={props}
					// />
					: (<div className="currency-opr-col">{
						props.button.createOprationButton(buttonAry, {
							area: "card_body_inner",
							buttonLimit:3,
							onButtonClick: (props, key) => tableButtonClick.call(that, props, key, text, record, index)
						})
					}</div>
					);

			// return props.button.createOprationButton(buttonAry, {
			// 	area: 'card_body_inner',
			// 	buttonLimit: 3,
			// 	onButtonClick: (props, key) => tableButtonClick.call(that, props, key, text, record, index)
			// });
		}
	};
	meta[tableId].items.push(porCol);

	return meta;
}
