import { createPage, ajax, base, high, toast,cacheTools, cardCache, print, output } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";
import { BatchToast } from '../../../../public/CMPMessage.js';//批量提示语句
import { MakeBillApp } from '../../../../public/utils/Makebill';//制单
import { linkApp, linkVoucherApp } from '../../../../public/utils/LinkUtil';//凭证
//缓存
let { setDefData, getDefData,
    getCurrentLastId, getCacheById,
    updateCache, addCache, getNextId,
    deleteCacheById } = cardCache;

/**
 * [收款]-联查计划预算
 * @param {*} props 
 * @param {*} config 
 * @param {*} pks 
 */
export const getLinkplanData = function (serval) {
    ajax({
        url: '/nccloud/cmp/recbill/linkplanquery.do',
        data: { pk: this.props.getUrlParam('pk_ntbparadimvo'), pageid: this.pageId },
        success: (res) => {
            let { success, data } = res;
            if (success) {
                if (data) {
                    this.props.table.setAllTableData(this.tableId, data[this.tableId]);
                } else {
                    this.props.table.setAllTableData(this.tableId, { rows: [], pageInfo: { pageIndex: 0, pageSize: 10, total: 0, totalPage: 0 } });
                }
            }
        }
    });
}
