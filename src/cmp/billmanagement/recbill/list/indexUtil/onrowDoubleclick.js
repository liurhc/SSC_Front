import { createPage, ajax, base, high, toast, cardCache } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";

//缓存
let { setDefData, getDefData,
    getCurrentLastId, getCacheById,
    updateCache, addCache, getNextId,
    deleteCacheById } = cardCache;

/**
 * [收款index]-表格双击事件
 * @param {*}  
 */
export const onrowDoubleclick = function (record, index, props, e) {
    let isvoucherlink = getDefData(this.linkkey, this.dataSource);//是否凭证联查单据
    let isotherlink = getDefData(this.linkscekey, this.dataSource);//是否来自其他单据联查
    if (isvoucherlink || isotherlink) {
        //凭证联查收款结算页面或者其他单据联查此单据
        this.props.pushTo('/card', {
            status: 'browse',
            id: record.pk_recbill.value,
            billno: record.bill_status.value,
            pagecode: record.trade_type.value,
            scene: 'linksce',//来自列表的查询
            src: 'list'//来自列表可以显示返回箭头
        });
    }else{
        this.props.pushTo('/card', {
            status: 'browse',
            id: record.pk_recbill.value,
            billno: record.bill_status.value,
            pagecode: record.trade_type.value
        });
    }
}
