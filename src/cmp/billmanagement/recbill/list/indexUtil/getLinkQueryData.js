import { createPage, ajax, base, high, toast,cacheTools, cardCache, print, output } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";
import { BatchToast } from '../../../../public/CMPMessage.js';//批量提示语句
import { MakeBillApp } from '../../../../public/utils/Makebill';//制单
import { linkApp, linkVoucherApp } from '../../../../public/utils/LinkUtil';//凭证
//缓存
let { setDefData, getDefData,
    getCurrentLastId, getCacheById,
    updateCache, addCache, getNextId,
    deleteCacheById } = cardCache;

/**
 * [收款]-获取联查条件
 * @param {*} props 
 * @param {*} config 
 * @param {*} pks 
 */
export const getLinkQueryData = function (searchData) {
    let sendArr = {
        'pks': searchData,
        'pageid': this.pageId
    }
    ajax({
        url: '/nccloud/cmp/recbill/recbilllinkbill.do',
        data: sendArr,
        success: (res) => {
            let { success, data } = res;
            if (success) {
                if (data) {
                    let rowlenght = data[this.tableId].rows;
                    console.log(rowlenght);
                    let src = this.props.getUrlParam('src');
                    if (rowlenght.length == 1) {
                        let record = rowlenght[0];
                        //1条数据跳转到卡片页面
                        this.props.linkTo('/cmp/billmanagement/recbill/card/index.html', {
                            status: 'browse',
                            id: record.values.pk_recbill.value,
                            billno: record.values.bill_status.value
                        });

                    } else {
                        //多条数据跳转到列表页面
                        this.props.table.setAllTableData(this.tableId, data[this.tableId]);
                    }
                } else {
                    this.props.table.setAllTableData(this.tableId, { rows: [], pageInfo: { pageIndex: 0, pageSize: 10, total: 0, totalPage: 0 } });
                }
            }
        }
    });
}
