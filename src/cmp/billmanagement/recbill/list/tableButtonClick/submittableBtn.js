import {  ajax,toast } from 'nc-lightapp-front';
import { BatchToast } from '../../../../public/CMPMessage.js';//批量提示语句
/**
 * [收款结算]-提交按钮
 * @param {*} props 
 * @param {*} config 
 * @param {*} pks 
 */
export const submittableBtn = function (record, index) {

    // 未关联付款类单据的支付退回收款单不能提交
    if(record.trade_type.value=='F4-Cxx-99' && !record.def16.value){
        toast({ color: 'warning', content:'未关联付款类单据的支付退回收款单不能提交!' });
        return;
    }

    let submitdataArr = [];
    let tsmpa2 = {
        'pk': record.pk_recbill.value,
        'ts': record.ts.value,
        'index': index
    }
    let listTsmap2 = [];
    listTsmap2.push(tsmpa2);
    submitdataArr.push(record.pk_recbill.value);
    let submitdata = {
        'pks': submitdataArr,
        'pageid': this.pageId,
        'ts': record.ts.value,
        'listTsmap': listTsmap2
    };

    ajax({
        url: '/nccloud/cmp/recbill/listsubmit.do',
        data: submitdata,
        success: (res) => {
            let { success, data } = res;
            let { status, sumNumIndex, successNumIndex, failNumIndex, message, successPks, successIndex, gridList, appointmap } = res.data;
            if (success) {
                //提交--指派
                if (appointmap && appointmap.workflow &&
                    (appointmap.workflow == 'approveflow' ||
                        appointmap.workflow == 'workflow')) {
                    this.compositedata = appointmap;
                    this.setState({
                        compositedisplay: true,
                        record: record,
                        index: index
                    });
                    
                } else {
                    BatchToast.call(this, 'COMMIT', status, sumNumIndex, successNumIndex, failNumIndex, message, null);
                    //加载更新缓存数据
                    if (gridList != null && gridList.length > 0) {
                        let gridRows = res.data.gridList;
                        gridRows.forEach((val) => {
                            let test = val.index;
                            let value = val.rows.values;
                            let submitUpdateDataArr = [{
                                index: val.index,
                                data: { values: val.rows.values }//自定义封装数据
                            }];
                            this.props.table.updateDataByIndexs(this.tableId, submitUpdateDataArr);
                        });
                    }
                }

            }
        }
    });
}
