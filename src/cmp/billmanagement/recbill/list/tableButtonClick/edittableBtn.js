/**
 * [外币兑换]-修改按钮
 * @param {*} props 
 * @param {*} config 
 * @param {*} pks 
 */
export const edittableBtn = function (record, index) {
    let pagecode = this.state.tradeCode;//跳转卡片使用交易类型
    if (record.trade_type.value) {
        pagecode = record.trade_type.value;
    }
    this.props.pushTo('/card', {
        status: 'edit',
        id: record.pk_recbill.value,
        pagecode: pagecode
    });
}
