import { createPage, ajax, base, toast, cacheTools } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";//配置的id和area信息
import { MakeBillApp } from '../../../../public/utils/Makebill';//制单
import { BatchToast } from '../../../../public/CMPMessage.js';//删除信息调用方法
import { submitAssginBtn } from '../tableButtonClick/submitAssginBtn.js';
import { cachepagecode } from '../buttonClick/cachepagecode.js';
import { edittableBtn } from '../tableButtonClick/edittableBtn.js';
import { deletetableBtn } from '../tableButtonClick/deletetableBtn.js';
import { makebilltableBtn } from '../tableButtonClick/makebilltableBtn.js';
import { submittableBtn } from '../tableButtonClick/submittableBtn.js';
import { unsubmittableBtn } from '../tableButtonClick/unsubmittableBtn.js';
import { redbillBtn } from '../tableButtonClick/redbillBtn.js';

export default function tableButtonClick(props, key, text, record, index) {
    //缓存相关
    let { deleteCacheId, addCacheId } = this.props.table;
    cachepagecode.call(this);//所选交易类型，pagecode
    switch (key) {
        //list总操作列中动作
        case 'edittableBtn':
        edittableBtn.call(this,record, index);        
            break;
        //删除
        case 'deletetableBtn':
        deletetableBtn.call(this,record, index);           
            break;
        //制单
        case 'makebilltableBtn':
        makebilltableBtn.call(this,record, index);
            break;
        case 'approvetableBtn':
            toast({ color: 'warning', content: this.props.MutiInit.getIntl("36070RBM") && this.props.MutiInit.getIntl("36070RBM").get('36070RBM-000011') });/* 国际化处理： 功能待开发*/
            break;
        //提交
        case 'submittableBtn':
        submittableBtn.call(this,record, index);
            break;
        //提交指派
        case 'submitAssginBtn':
        submitAssginBtn.call(this,record,index);
            break;
        //收回
        case 'unsubmittableBtn':
        unsubmittableBtn.call(this,record, index);
            break;
        //红冲
        case 'redbillBtn':
        redbillBtn.call(this,record, index);
            break;
    }
}
