import { createPage, ajax, base, toast, cardCache } from 'nc-lightapp-front';
let { NCPopconfirm, NCIcon, NCTooltip } = base;
import tableButtonClick from './tableButtonClick';
import buttonUsability from './buttonUsability';
import { buttonVisible } from './buttonVisible';//凭证联查单据使用按钮控制
import { Templatedata } from "../../config/Templatedata";//配置的id和area信息
import { setDefOrg2AdvanceSrchArea } from '../../../../../tmpub/pub/util/index';
import { setDefOrg2ListSrchArea } from '../../../../../tmpub/pub/util/index';
let searchId = Templatedata.list_searchid;
let tableId = Templatedata.list_tableid;
let pageId = Templatedata.list_pageid;
let moudleId = Templatedata.list_moduleid;
let appcode = Templatedata.app_code;
let printcard_funcode = Templatedata.printcard_funcode;
//缓存
let { setDefData, getDefData } = cardCache;
export default function (props) {
	let self = this;
	props.createUIDom(
		{
			pagecode: pageId//页面id
		},
		function (data) {
			if (data) {
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button, () => {
						buttonVisible.call(self, props);//按钮控制显隐性
					});
					// props.button.setButtons(button);
					props.button.setPopContent('deletetableBtn', props.MutiInit.getIntl("36070RBM") && props.MutiInit.getIntl("36070RBM").get('36070RBM-000101')); /* 设置操作列上删除按钮的弹窗提示 */
					// props.button.setPopContent('submittableBtn', '确认要提交该信息吗？'); /* 设置操作列上删除按钮的弹窗提示 */
					// props.button.setPopContent('makebilltableBtn', '确认要该信息制单吗？'); /* 设置操作列上删除按钮的弹窗提示 */
					// props.button.setPopContent('unsubmittableBtn', '确认要收回该信息吗？'); /* 设置操作列上删除按钮的弹窗提示 */
				}
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(self, props, meta);
					setDefOrg2AdvanceSrchArea(props, self.searchId, data);//高级查询区赋值
					setAdvTradetype(props, self.searchId, data);//高级查询区交易类型赋值
					props.meta.setMeta(meta);
					setDefOrg2ListSrchArea(props, self.searchId, data);//普通查询区赋值
				}

				buttonUsability.call(self, props, '');//列表按钮显影性
			}
		}
	)
}

function seperateDate(date) {
	if (typeof date !== 'string') return;
	let result = date.split(' ') && date.split(' ')[0];
	return result;
}
//高级查询区赋值交易类型
function setAdvTradetype(props, areaCode, data) {
	//判空
	if (!props || !props.search || !areaCode || !data.template || !data.context) {
		return;
	}
	let meta = data.template;
	//获取默认业务单元
	//发布小应用默认交易类型赋值
	if (data.context && data.context.paramMap) {
		let { transtype, pk_transtype, transtype_name } = data.context.paramMap;
		if (transtype && pk_transtype && transtype_name) {
			//遍历查询区域字段 发布小应用给交易类型赋值
			meta[areaCode].items.map((item) => {
				if (item.attrcode == 'pk_tradetypeid') {
					item.visible = true;
					item.initialvalue = { 'display': transtype_name, 'value': pk_transtype }
				}
			});
			//若存储值是字符串，可以直接存储
			sessionStorage.setItem("sessionTP", JSON.stringify(transtype));
			sessionStorage.setItem("sessionName", JSON.stringify(transtype_name));
			sessionStorage.setItem("sessionpk", JSON.stringify(pk_transtype));
			//支持网问题-begin:
			sessionStorage.setItem("billname", JSON.stringify(transtype_name));
			console.log('transtype:', JSON.parse(sessionStorage.getItem("sessionTP")));
			console.log('transtype_name:', JSON.parse(sessionStorage.getItem("sessionName")));
			console.log('pk_transtype:', JSON.parse(sessionStorage.getItem("sessionpk")));
		}

	}

}
function modifierMeta(props, meta) {
	meta[searchId].items = meta[searchId].items.map((item, key) => {
		// item.visible = true;
		// item.col = '11';
		return item;
	})
	let self2 = this;
	//操作列点击事件
	meta[tableId].items = meta[tableId].items.map((item, key) => {
		// item.width = 150;
		//单据编号
		if (item.attrcode == 'bill_no') {
			item.render = (text, record, index) => {
				return (
					//自定义的tip提示语句
					// <NCTooltip placement="top"
					// 	overlay={record.bill_no ? record.bill_no.value : ''}>
					<a
						style={{ cursor: 'pointer' }}
						onClick={() => {
							let isvoucherlink = getDefData(this.linkkey, this.dataSource);//是否凭证联查单据
							let isotherlink = getDefData(this.linkscekey, this.dataSource);//是否来自其他单据联查
							if (isvoucherlink || isotherlink) {
								//凭证联查收款结算页面或者其他单据联查此单据
								props.pushTo('/card', {
									status: 'browse',
									id: record.pk_recbill.value,
									billno: record.bill_status.value,
									pagecode: record.trade_type.value,
									scene: 'linksce',//来自列表的查询
									src: 'list'//来自列表可以显示返回箭头
								});
							} else {
								props.pushTo('/card', {
									status: 'browse',
									id: record.pk_recbill.value,
									billno: record.bill_status.value,
									pagecode: record.trade_type.value
								});
							}

						}}
					>
						{record && record.bill_no && record.bill_no.value}
					</a>
					// </NCTooltip>
				);
			};
		}
		else if (item.attrcode == 'dbilldate') {
			item.render = (text, record, index) => {
				return (
					<span>
						{record.dbilldate && seperateDate(record.dbilldate.value)}
					</span>
				);
			};
		}
		return item;
	});
	//添加操作列
	meta[tableId].items.push({
		attrcode: 'opr',
		label: this.props.MutiInit.getIntl("36070RBM") && this.props.MutiInit.getIntl("36070RBM").get('36070RBM-000018'),/* 国际化处理： 操作*/
		fixed: 'right',
		width: '200px',
		visible: true,
		itemtype: 'customer',
		render: (text, record, index) => {

			let buttonAry = [];

			if (record && record.bill_status && record.bill_status.value == '-10') {
				/* 单据状态：已保存  */
				buttonAry = ["submittableBtn", "edittableBtn", "deletetableBtn"];
			} else if (record && record.bill_status && record.bill_status.value == '-99') {
				/* 单据状态：暂存 */
				buttonAry = ["edittableBtn", "deletetableBtn"];
			} else if (record.bill_status && record.bill_status.value == '8') {
				/* 单据状态：签字 */
				buttonAry = ["makebilltableBtn"];
			} else if (record.bill_status && record.bill_status.value == '-1') {
				/* 单据状态：待审批 */
				buttonAry = ["unsubmittableBtn"];
			} else if (record.bill_status && record.bill_status.value == '1') {
				/* 单据状态：审批通过:针对没有审批流直接退回 */
				buttonAry = ["unsubmittableBtn"];
			}
			//生效状态:0-未生效，10-生效，5-未生成凭证
			if (record && record.effect_flag && record.effect_flag.value == '10') {
				buttonAry.push('redbillBtn');//红冲
			}
			//如果是凭证联查单据的话去掉操作列按钮
			if (getDefData(this.linkkey, this.dataSource)) {
				buttonAry = [];
			}
			//如果是被联查单据，不显示操作列按钮
			if (getDefData(this.linkscekey, this.dataSource)) {
				buttonAry = [];
			}
			return props.button.createOprationButton(buttonAry, {
				area: Templatedata.list_inner,
				buttonLimit: 3,
				onButtonClick: (props, key) => tableButtonClick.call(self2, props, key, text, record, index)
			});


		}
	});
	//组织用户权限过滤
	meta[searchId].items.map((item) => {
		// 发送发组织，接收方组织：根据用户权限过滤
		if (item.attrcode == 'pk_org') {
			item.queryCondition = () => {
				return {
					funcode: printcard_funcode,
					TreeRefActionExt: 'nccloud.web.tmpub.filter.FinanceOrgPermissionFilter'
				};
			};
		}
		//收款银行账户(子表)
		if (item.attrcode == 'items.pk_account') {
			item.queryCondition = () => {
				let search_org_value2 = props.search.getSearchValByField(searchId, 'pk_org');//所选组织
				if (search_org_value2 && search_org_value2.value.firstvalue) {
					search_org_value2 = search_org_value2.value.firstvalue;
				} else {
					search_org_value2 = null;
				}
				let search_buycurr_value = props.search.getSearchValByField(searchId, 'items.pk_currtype');//所选买入币种
				if (search_buycurr_value && search_buycurr_value.value.firstvalue) {
					search_buycurr_value = search_buycurr_value.value.firstvalue;
				} else {
					search_buycurr_value = null;
				}

				return {
					pk_orgs: search_org_value2,
					pk_currtype: search_buycurr_value,
					refnodename: '使用权参照',/* 国际化处理： 使用权参照*/
					isDisableDataShow: false,//默认只加载启用的账户
					noConditionOrg: 'Y',//是否加载参照默认条件
					GridRefActionExt: 'nccloud.web.cmp.ref.CMPBatchOrgBankaccSubDefaultGridRefSqlBuilder'//自定义增加的过滤条件
				};
			}
		}

	});
	//设置参照可以多选
	meta[searchId].items.find((e) => e.attrcode === 'pk_org').isMultiSelectedEnabled = true;
	//财务组织:全加载
	meta[searchId].items.find((e) => e.attrcode === 'pk_org').isTreelazyLoad = false;
	return meta;
}
