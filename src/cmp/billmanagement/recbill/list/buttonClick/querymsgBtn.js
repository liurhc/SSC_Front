import { createPage, ajax, base, high, toast,cacheTools, cardCache, print, output } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";
import { BatchToast } from '../../../../public/CMPMessage.js';//批量提示语句
import { MakeBillApp } from '../../../../public/utils/Makebill';//制单
import { linkApp, linkVoucherApp } from '../../../../public/utils/LinkUtil';//凭证
//缓存
let { setDefData, getDefData,
    getCurrentLastId, getCacheById,
    updateCache, addCache, getNextId,
    deleteCacheById } = cardCache;

/**
 * [收款]-审批意见按钮
 * @param {*} props 
 * @param {*} config 
 * @param {*} pks 
 */
export const querymsgBtn = function () {
    let approvemsgData = this.props.table.getCheckedRows(this.tableId);
    //数据校验
    if (approvemsgData.length != 1) {
        toast({
            color: 'warning',
            content: this.props.MutiInit.getIntl("36070RBM") && this.props.MutiInit.getIntl("36070RBM").get('36070RBM-000083')/* 国际化处理： 请选择单条数据，查看审批意见!*/
        });
        return;
    }
    //处理选择数据
    let billid;
    let approve_billtype;
    approvemsgData.forEach((val) => {

        if (val.data.values.pk_recbill && val.data.values.pk_recbill.value) {
            billid = val.data.values.pk_recbill.value;
        }
        if (val.data.values.trade_type && val.data.values.trade_type.value) {
            approve_billtype = val.data.values.trade_type.value;
        }

    });
    if (billid) {
        this.setState({
            show: true,
            billtype: approve_billtype,//单据类型
            billid: billid//单据pk
        });
    }
}
