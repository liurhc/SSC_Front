import { createPage, ajax, base, high, cacheTools, toast, cardCache } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";
import { BatchToast } from '../../../../public/CMPMessage.js';//批量提示语句
//缓存
let { setDefData, getDefData,
    getCurrentLastId, getCacheById,
    updateCache, addCache, getNextId,
    deleteCacheById } = cardCache;

/**
 * [收款]-提交按钮
 * @param {*} props 
 * @param {*} config 
 * @param {*} pks 
 */
export const submitBtn = function () {
    let submitData = this.props.table.getCheckedRows(this.tableId);
    //数据校验
    if (submitData.length == 0) {
        toast({ color: 'warning', content: this.props.MutiInit.getIntl("36070RBM") && this.props.MutiInit.getIntl("36070RBM").get('36070RBM-000093') });/* 国际化处理： 请选择数据,进行提交!*/
        return
    }

    let submitdataArr = [];
    let listTsmap = [];//ts的list类型
    //处理选择数据
    submitData.forEach((val) => {
        // 未关联付款类单据的支付退回收款单不能提交
        if(val.data.values.trade_type.value=='F4-Cxx-99' && !val.data.values.def16.value){
            toast({ color: 'danger', content:val.data.values.bill_no.value+'未关联付款类单据,不能提交!' });
        } else {
            submitdataArr.push(val.data.values.pk_recbill.value);//主键数组
            let tsmap = {
                'pk': val.data.values.pk_recbill.value,
                'ts': val.data.values.ts.value,
                'index': val.index
            }
            console.log(val.data.values.ts.value,'提交的ts');
            listTsmap.push(tsmap);
        }
    });
    //自定义请求数据
    let submitdata = {
        'pks': submitdataArr,
        'pageid': this.pageId,
        'listTsmap': listTsmap
    };

    ajax({
        url: '/nccloud/cmp/recbill/listsubmit.do',
        data: submitdata,
        success: (res) => {
            let { success, data } = res;
            let { status, sumNumIndex, successNumIndex, failNumIndex, message, successPks, successIndex, gridList, appointmap } = res.data;
            if (success) {
                //提交--指派
                if (appointmap && appointmap.workflow &&
                    (appointmap.workflow == 'approveflow' ||
                        appointmap.workflow == 'workflow')) {
                    this.compositedata = appointmap;
                    this.setState({
                        compositedisplay: true
                    });

                } else {
                    BatchToast.call(this, 'COMMIT', status, sumNumIndex, successNumIndex, failNumIndex, message, null);
                    //加载更新缓存数据
                    if (gridList != null && gridList.length > 0) {
                        let gridRows = res.data.gridList;
                        gridRows.forEach((val) => {
                            let test = val.index;
                            let value = val.rows.values;
                            let submitUpdateDataArr = [{
                                index: val.index,
                                data: { values: val.rows.values }//自定义封装数据
                            }];
                            this.props.table.updateDataByIndexs(this.tableId, submitUpdateDataArr);
                        });
                    }
                }
            }
        }
    });
}
