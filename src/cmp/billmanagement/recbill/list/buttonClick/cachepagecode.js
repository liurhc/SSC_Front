import { createPage, ajax, base, high, cacheTools,toast, cardCache } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";
//缓存
let { setDefData, getDefData,
    getCurrentLastId, getCacheById,
    updateCache, addCache, getNextId,
    deleteCacheById } = cardCache;

/**
 * [收款]-从缓存中获取pagecode(tradecode)，不用交易类型得到不同pagecode,适用于新增和复制
 * @param {*} props 
 * @param {*} config 
 * @param {*} pks 
 */
export const cachepagecode = function () {
    let tradecode = JSON.parse(sessionStorage.getItem("sessionTP"));
    let pagecode = this.state.tradeCode;
    console.log(pagecode, 'add_pageid');
    if (tradecode) {
        this.setState({
            tradeCode: tradecode
        });
    }
}
