import { createPage, ajax, base, high, toast,cacheTools, cardCache, print, output } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";
import { BatchToast } from '../../../../public/CMPMessage.js';//批量提示语句
import { MakeBillApp } from '../../../../public/utils/Makebill';//制单
import { linkApp, linkVoucherApp } from '../../../../public/utils/LinkUtil';//凭证
//缓存
let { setDefData, getDefData,
    getCurrentLastId, getCacheById,
    updateCache, addCache, getNextId,
    deleteCacheById } = cardCache;

/**
 * [收款]-联查单据按钮
 * @param {*} props 
 * @param {*} config 
 * @param {*} pks 
 */
export const linkquerybillBtn = function () {
    let linkquerybillData = this.props.table.getCheckedRows(this.tableId);
    //数据校验
    if (linkquerybillData.length != 1) {
        toast({
            color: 'warning',
            content: this.props.MutiInit.getIntl("36070RBM") && this.props.MutiInit.getIntl("36070RBM").get('36070RBM-000082')/* 国际化处理： 请选择单条数据，联查单据!*/
        });
        return;
    }
    //处理选择数据
    let showbilltrackpk;
    let billtrack_billtype;
    linkquerybillData.forEach((val) => {

        if (val.data.values.pk_recbill && val.data.values.pk_recbill.value) {
            showbilltrackpk = val.data.values.pk_recbill.value;
        }
        if (val.data.values.bill_type && val.data.values.bill_type.value) {
            billtrack_billtype = val.data.values.bill_type.value;
        }


    });

    if (showbilltrackpk) {
        this.setState({
            showbilltrack: true,//显示联查单据
            showbilltracktype: billtrack_billtype,//单据类型
            showbilltrackpk: showbilltrackpk//单据pk
        });
    }
}
