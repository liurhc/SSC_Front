import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base,createPageIcon,promptBox,toast } from 'nc-lightapp-front';

const pagecode = '360704SM_L01';
const tableid = 'tablearea';
const searchid = 'searcharea';

/**
 * 关联弹框
 * @date 2019-09-16
 */
export class Relation extends Component{
    constructor(props){
        super(props);
        this.state = {
            pks:[]
        }
        this.initTemplate(this.props);
    }

    // 加载模板
    initTemplate(props,callBack){
        props.createUIDom(
            {
                pagecode:pagecode
            },
            (data)=>{
                let meta = data.template;
                meta[tableid].pagination = true; // 分页
                props.meta.setMeta(meta,callBack);
                data.button && props.button.setButtons(data.button,()=>{});
            }
        );
    }

    // 查询区渲染完成回调函数
    renderCompleteEvent(){
        // 给查询区设置默认值
        let org = this.props.parent.props.form.getFormItemsValue(this.props.parent.formId,'pk_org');
        let localMoney = this.props.parent.props.form.getFormItemsValue(this.props.parent.formId,'local_money'); // 本币金额
        this.props.search.setSearchValByField(searchid,'pk_org', org);
        this.props.search.setSearchValByField(searchid,'orglocal', localMoney);
    }

    // 查询按钮事件
    clickSearchBtn(props, searchVal){
        var settlestatus = { // 结算状态
            datatype: '203',
            display: '结算成功',
            field: 'settlestatus',
            isIncludeSub: false,
            oprtype: "=",
            refurl: "",
            value:{
                firstvalue: "5",
                secondvalue: ""
            }
        };
        var billtypes = { // 单据类型
            datatype: '1',
            display: 'F3,F5,264X,263X',
            field: 'pk_billtype',
            isIncludeSub: false,
            oprtype: "=",
            refurl: "",
            value:{
                firstvalue: "F3,F5,264X,263X",
                secondvalue: ""
            }
        };
        
        searchVal.conditions.push(settlestatus); // 只查询结算成功的单据
        searchVal.conditions.push(billtypes); // 只查询付款类单据
        
        let pageInfo = props.table.getTablePageInfo(tableid);
        let conditions = searchVal.conditions;
        let oid = props.meta.getMeta()[searchid].oid; // 查询模板id
        let searchData = { // 查询条件
            querycondition: searchVal,
            custcondition: {
                logic: 'and', //逻辑操作符，and、or
                conditions
            },
            pageInfo: pageInfo,
            pageCode: pagecode,
            queryAreaCode: searchid, // 查询区编码
            oid: oid, // 查询模板id
            querytype: 'tree'
        };
    
		if(!searchData || !searchData.querycondition){
			return;
		}
        ajax({
            url:'/nccloud/cmp/settlement/settlelistquery.do',
            data:searchData,
            success:(result)=>{
                let { success,data } = result;
                if(data && data.vos){
                    
                    props.table.setAllTableData(tableid, data.vos[tableid]);
                    // 保存当前页的pks，用于在操作时候刷新页面数据
					let pks = [];
					data.vos[tableid].rows.forEach((val, index) => {
						let pk = val.values.pk_settlement.value;
						pks.push(pk);
					});
					this.setState({ pks: pks });
                }
                else{
                    toast({color:'warning', content:'未查询到符合条件的数据！'});
                    props.table.setAllTableData(tableid, {rows:[]});
                }
            }
        });
    }

    // 按钮事件
    onButtonClick(props,id){
        switch(id){
            case 'addtagBtn':
                let selectedData = props.table.getCheckedRows(tableid);
                if(selectedData && selectedData.length!=1){
                    toast({title:'关联标记操作只能选择一条数据！',color:'danger'});
                    break;
                }
                ajax({
                    url: '/nccloud/cmp/recbill/relationtag.do',
                    data: {
                        billType:selectedData[0].data.values.pk_billtype.value, // 关联单据类型
                        billid:selectedData[0].data.values.pk_busibill.value, // 关联单据主键
                        recbillid:this.props.recbillid, // 收款结算单主键
                        tradeType:this.props.trade_type // 收款结算单交易类型编码
                    },
                    success:(result)=>{
                        if(result.success){
                            toast({title:'关联成功！',color:'success'});
                            this.props.parent.props.modal.close('relation');
                            this.props.parent.refresh.call();
                        }
                    }
                });
                break;
        }
    }

    // 表格数据选中事件
    onSelected(){

    }

    // 分页按钮事件
    pageInfoClick(props, config, pks){
        //分页根据pks查询数据
        let data = {
            "pks": pks,
            "pageid": pagecode
        };
        ajax({
            url: '/nccloud/cmp/settlement/settlelistquerypks.do',
            data: data,
            success: (res) => {
                let { success, data } = res;
                if (success) {
                    if(data){
                        props.table.setAllTableData(tableid, data[tableid]);
                        this.setState({
                            pks:pks,
                        })
                    }else{
                        props.table.setAllTableData(tableid, {rows:[]});
                    }
                    
                }
            }
        });
    }

    render(){

        let { table,search,button } = this.props;
        let { createButtonApp } = button;
        let { createSimpleTable } = table;
        let { NCCreateSearch } = search;

        return(
            <div className="nc-single-table">

                <div className="nc-bill-header-area">
                    <div className="header-title-search-area">
                        {createPageIcon()}
						<h2 className="title-search-detail">单据查询</h2>
					</div>
                    <div className="header-button-area">{/* 按钮区 */}
                        {createButtonApp({
                            area:'head',
                            buttonLimit: 3,
                            onButtonClick: this.onButtonClick.bind(this),
                            popContainer: document.querySelector('.header-button-area')
                        })}
                    </div>
                </div>

                <div className="nc-bill-search-area">
                    {NCCreateSearch(
                        searchid,
                        {
                            clickSearchBtn:this.clickSearchBtn.bind(this),// 查询按钮事件
                            showAdvBtn: true, // 显示高级查询按钮
                            renderCompleteEvent: this.renderCompleteEvent.bind(this),  // 查询区渲染完成回调函数 
                        }
                    )}
                </div>

                <div className="nc-singleTable-table-area">{/* 表格区 */}
                    {createSimpleTable(
                        tableid,
                        {
                            useFixedHeader:true,
                            showIndex:true,
                            showCheck:true,
                            isAddRow:true,
                            onSelected:this.onSelected.bind(this),
                            handlePageInfoChange: this.pageInfoClick.bind(this)
                        }
                    )}
                </div>

            </div>
        );
    }
}

Relation = createPage({
    billinfo:{
        billtype:'grid',
        pagecode:pagecode
    },
    initTemplate:()=>{}
})(Relation);