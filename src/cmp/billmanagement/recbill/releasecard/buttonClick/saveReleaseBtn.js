import { createPage, ajax, base, high, toast, cardCache, print, output } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";
//缓存
let { setDefData, getDefData,
    getCurrentLastId, getCacheById,
    updateCache, addCache, getNextId,
    deleteCacheById } = cardCache;

/**
 * [到账通知认领收款结算单]-认领保存
 * @param {*} props 
 * @param {*} config 
 * @param {*} pks 
 */
export const saveReleaseBtn = function () {
    let firstStatus = this.props.getUrlParam('status');
    //自定义校验table
    let checkTableNum = this.props.cardTable.getNumberOfRows(this.tableId);//表体table行数
    for (let i = 0; i < checkTableNum; i++) {
        let isCheckTable = this.props.cardTable.getValByKeyAndIndex(this.tableId, i, 'rec_primal');//收款原币金额
        if (isCheckTable && isCheckTable.value) {
        } else {
            toast({ color: 'warning', content: this.props.MutiInit.getIntl("36070RBM") && this.props.MutiInit.getIntl("36070RBM").get('36070RBM-000000') });/* 国际化处理： 收款金额未填写！*/
            return;
        }
    }
    let is_flag = this.props.form.isCheckNow(this.formId);//form必输项校验
    let is_tableflag = this.props.cardTable.checkTableRequired(this.tableId);//table必输项校验

    if (is_flag && is_tableflag) {
        let CardData = this.props.createMasterChildData(this.pageId, this.formId, this.tableId);
        let url = '/nccloud/cmp/recbill/releasesave.do'//认领保存
        ajax({
            url: url,
            data: CardData,
            success: (res) => {

                if (res.success) {
                    if (res.data) {
                        toast({ color: 'success', content: this.props.MutiInit.getIntl("36070RBM") && this.props.MutiInit.getIntl("36070RBM").get('36070RBM-000001') });/* 国际化处理： 保存成功*/
                        if (res.data.head) {
                            this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
                        }
                        if (res.data.body) {
                            this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
                        }
                        let release_pk = res.data.head[this.formId].rows[0].values.pk_recbill.value;
                        let release_status = res.data.head[this.formId].rows[0].values.bill_status.value;
                        let release_billno = res.data.head[this.formId].rows[0].values.bill_no.value;
                        this.billno = release_billno;
                        //跳转下一个缩略图
                        this.props.transferTable.setTransformFormStatus('leftarea', {
                            status: 'browse',
                            onChange: (current, next, currentIndex) => {
                                this.props.transferTable.setTransferListValueByIndex('leftarea', current, currentIndex);
                                //刷新左侧列表数据
                                let size = this.props.transferTable.getTransformFormAmount('leftarea');
                                let nextIndex = currentIndex + 1;
                                if (nextIndex == size) {
                                    nextIndex = size;
                                }
                                this.props.transferTable.setTransferListValueByIndex('leftarea', res.data, currentIndex);
                            }
                        });
                    }
                }
            }
        });
    }
}
