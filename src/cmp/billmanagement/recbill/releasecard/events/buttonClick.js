import {  promptBox } from 'nc-lightapp-front';
import { saveReleaseBtn } from '../buttonClick/saveReleaseBtn.js';
export default function (props, id) {
  //验证公式
  let check_CardData = props.createMasterChildData(this.pageId, this.formId, this.tableId);
  let saveobj = {};
  saveobj[this.tableId] = 'cardTable';
  switch (id) {
    //刷新按钮
    case 'refreshBtn':
      this.refresh();
      break
    //取消
    case 'cancelBtn':
      promptBox({
        color: "warning",
        title: this.props.MutiInit.getIntl("36070RBM") && this.props.MutiInit.getIntl("36070RBM").get('36070RBM-000041'),/* 国际化处理： 提示*/
        content: this.props.MutiInit.getIntl("36070RBM") && this.props.MutiInit.getIntl("36070RBM").get('36070RBM-000043'),/* 国际化处理： 确定要取消吗?*/
        beSureBtnClick: this.cancelConfirm.bind(this) //使用call直接執行了
      });
      break;
    //保存
    case 'saveBtn':
      //验证公式
      this.props.validateToSave(check_CardData, this.saveBill(), saveobj, '');
      break;
    //保存新增
    case 'saveaddBtn':
      //验证公式
      this.props.validateToSave(check_CardData, this.saveAddBill(), saveobj, '');
      break
    //保存提交
    case 'savesubmitBtn':
      //验证公式
      this.props.validateToSave(check_CardData, this.saveSubBill(), saveobj, '');
      break
    //body删除
    case 'deletebodyBtn':
      promptBox({
        color: "warning",
        title: this.props.MutiInit.getIntl("36070RBM") && this.props.MutiInit.getIntl("36070RBM").get('36070RBM-000110'),/* 国际化处理： 提示*/
        content: this.props.MutiInit.getIntl("36070RBM") && this.props.MutiInit.getIntl("36070RBM").get('36070RBM-000039'),/* 国际化处理： 你确定要删除吗?*/
        beSureBtnClick: this.delBodyConfirm.bind(this) //使用call直接執行了
      });
      break

    //认领保存
    case 'saveReleaseBtn':
      saveReleaseBtn.call(this);
      break;
    //认领取消
    case 'cancelReleaseBtn':
      this.releasetoggleShow('browse');
      break;
    //认领退出
    case 'quitReleaseBtn':
      promptBox({
        color: "warning",
        content: this.props.MutiInit.getIntl("36070RBM") && this.props.MutiInit.getIntl("36070RBM").get('36070RBM-000103'),/* 国际化处理： 确定要退出认领吗?*/
        beSureBtnClick: this.cancelReleaseConfirm.bind(this), //使用call直接執行了
      });
      break;
    //认领修改
    case 'editReleaseBtn':
      this.releasetoggleShow('edit');
      break;
    default:
      break;
  }
}
