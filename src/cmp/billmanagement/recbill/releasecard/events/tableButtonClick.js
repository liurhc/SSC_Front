import { Templatedata } from "../../config/Templatedata";//配置的id和area信息
import { createPage, ajax, base, toast } from 'nc-lightapp-front';
import { openBtn } from "../tableButtonClick/openBtn";
import { deletelineBtn } from "../tableButtonClick/deletelineBtn";
import { editmoreBtn } from "../tableButtonClick/editmoreBtn";
/**
 * 操作列中按钮
 * @param {*} props 
 * @param {*} key 
 * @param {*} text 
 * @param {*} record 
 * @param {*} index 
 */
export default function tableButtonClick(props, key, text, record, index) {

    switch (key) {
        //展开
        case 'openBtn':
            openBtn.call(this, record, index);
            break;
        //删行
        case 'deletelineBtn':
            deletelineBtn.call(this, record, index);
            break;
        //编辑展开
        case 'editmoreBtn':
            editmoreBtn.call(this, record, index);
            break;
    }
}
