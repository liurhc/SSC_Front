
export const SCENE = {
    //默认场景
    DEFFAULT: 'defaultsce',
    //审批场景
    APPROVE: 'approvesce',
    //联查场景
    LINK: 'linksce',
    //其他场景
    OTHER: 'othersce'
}
