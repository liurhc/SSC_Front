import {  ajax } from 'nc-lightapp-front';
import { setSourceFlag } from '../../util/setSourceFlag.js';//设置来源

/**
 * [收款结算]-被联查--卡片加载数据
 * 注意：单据参数id为单据pk[不能为数组]
 * @param {*} props 
 * @param {*} config 
 * @param {*} pks 
 */
export const linkLoadQueryData = function () {
    /**
     * 编辑态和浏览态
     */
    if (this.props.getUrlParam('status') === 'edit' || 
        this.props.getUrlParam('status') === 'browse') {
        if (this.props.getUrlParam('id') &&
            this.props.getUrlParam('id').length > 0) {
            let data = { pk: this.props.getUrlParam('id'), pageid: this.pageId };
            ajax({
                url: '/nccloud/cmp/recbill/recbillquerycard.do',
                data: data,
                success: (res) => {
                    if (res.data) {
                        if (res.data.head) {
                            this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
                            //页签赋值
                            let billno = res.data.head[this.formId].rows[0].values.bill_no.value;
                            let source_flag = res.data.head[this.formId].rows[0].values.source_flag.value;
                            setSourceFlag.call(this, source_flag);
                            //按钮显示控制[单据状态]
                            this.props.setUrlParam({
                                billno: res.data.head[this.formId].rows[0].values.bill_status.value
                            });
                            this.billno = billno;

                        }
                        if (res.data.body) {
                            this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
                        }
                     
                     this.linkToggleShow();//切换被联查页面状态,
                       
                    } else {
                        //清空数据
                        this.props.form.EmptyAllFormValue(this.formId);
                        this.props.cardTable.setTableData(this.tableId, { rows: [] });
                    }

                    if (this.props.getUrlParam('status') === 'edit') {
                        //设置组织不可以编辑
                        this.props.form.setFormItemsDisabled(this.formId, { 'pk_org': true });
                        this.props.form.setFormItemsDisabled(this.formId, { 'pk_org_v': true });
                        this.props.form.setFormItemsDisabled(this.formId, { 'pk_fiorg': true });
                        this.props.form.setFormItemsDisabled(this.formId, { 'pk_fiorg_v': true });
                        this.props.form.setFormItemsDisabled(this.formId, { 'pk_pcorg_v': true });
                        this.props.form.setFormItemsDisabled(this.formId, { 'pk_pcorg': true });
                    }
                }
                // error: (res) => {
                //     //并发处理，卡片直接报错，列表中缓存删除
                //     toast({ color: 'warning', content: res.message });/* 国际化处理： 并发错误,单据未查询到!*/
                //     this.deleteId = this.props.getUrlParam('id');//删除单据pk
                //     this.deleteCacheData();//删除list缓存
                //     //清空列表
                //     this.props.form.EmptyAllFormValue(this.formId);
                //     this.props.cardTable.setTableData(this.tableId, { rows: [] });
                //     this.props.setUrlParam({
                //         status: 'browse',
                //         id: '',
                //         billno: '',
                //         pagecode: this.pageId
                //     });
                //     this.billno = null;
                //     this.linkToggleShow();//切换被联查页面状态
                // }
            });
        }

    }
    /**
     * 复制，暂时没有用
     */
    if (this.props.getUrlParam('status') === 'copy') {
        // /清空表单form所有数据
        this.props.form.EmptyAllFormValue(this.formId);
        let data = { pk: this.props.getUrlParam('id'), pageid: this.pageId };
        ajax({
            url: '/nccloud/cmp/recbill/recbillcopy.do',
            data: data,
            success: (res) => {
                if (res.data) {
                    if (res.data.head) {
                        this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
                        let source_flag = res.data.head[this.formId].rows[0].values.source_flag.value;
                        setSourceFlag.call(this, source_flag);
                        // this.source_flag(source_flagc);
                    }
                    if (res.data.body) {
                        this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
                    }
                    this.billno = '';
                    this.linkToggleShow();//切换被联查页面状态,
                } else {
                    //清空数据
                    this.props.form.EmptyAllFormValue(this.formId);
                    this.props.cardTable.setTableData(this.tableId, { rows: [] });
                }
                //设置组织不可以编辑
                this.props.form.setFormItemsDisabled(this.formId, { 'pk_org': true });
            }
        });
    }
    //报账中心-->点击菜单--->新增--->需要添加交易类型
    //新增--->需要添加交易类型
    if (this.props.getUrlParam('status') === 'add') {
        // /清空表单form所有数据
        this.props.form.EmptyAllFormValue(this.formId);
        //清空table所有数据
        this.props.cardTable.setTableData(this.tableId, { rows: [] });
        //处理session数据
        let tradetype = JSON.parse(sessionStorage.getItem("sessionTP"));
        let tradename = JSON.parse(sessionStorage.getItem("sessionName"));
        let tradepk = JSON.parse(sessionStorage.getItem("sessionpk"));

        //1-新增之前设置默认值，保证后台能够获取到
        if (this.state.curr_pk_org) {
            //默认值设置财务组织
            this.props.form.setFormItemsValue(this.formId,
                {
                    'pk_org': {
                        value: this.state.curr_pk_org,
                        display: this.state.curr_orgname
                    },
                    'pk_org_v': {
                        value: this.state.curr_pk_org_v,
                        display: this.state.curr_orgname_v
                    }
                }
            );
        }
        //默认值：收款交易类型
        if (tradetype && tradetype.length > 0) {
            //不规范财务组织处理
            let fdStart = tradetype.indexOf("D5");
            let fdStartf = tradetype.indexOf("F5");
            if (fdStart == 0 || fdStartf == 0) {
                tradetype = '';
                tradename = '';
                tradepk = '';
            }
        }
        if (tradetype && tradetype.length > 0) {
            this.props.form.setFormItemsValue(this.formId, { 'trade_type': { display: tradename, value: tradetype } });
        }
        if (tradepk && tradepk.length > 0) {
            this.props.form.setFormItemsValue(this.formId, { 'pk_tradetypeid': { display: tradename, value: tradepk } });
        }
        //改造：防止模版设置默认值后卡片显示不出来
        let addCardData = this.props.createMasterChildData(this.pageId, this.formId, this.tableId);

        ajax({
            url: '/nccloud/cmp/recbill/recbilladdevent.do',
            data: addCardData,
            success: (res) => {
                if (res.data) {
                    let isContainBoay = false;//是否包含表体显示[有组织显示，没有组织不显示]

                    //F4判断是否从银行挂账流水跳转
                    if(window.location.hash.includes("ifturnTo=")){
                        var record=JSON.parse(localStorage.getItem('record'))
                        var  heads=res.data.head[this.formId].rows["0"].values;
                        var  tables=res.data.body[this.tableId].rows["0"].values;
                        heads.pk_upbill=record.values.m_pk_bankreceipt;//F4表体pk_upbill后端要求
                        heads.pk_org=record.values.m_pk_org;//财务组织
                        tables.pk_org=record.values.m_pk_org;//财务组织
                        record.values.num&&record.values.num.value&&(heads.num=record.values.num);//挂账凭证号
                        heads.oppunitname =record.values.oppunitname ;//对方单位
                        heads.accounting_status=record.values.accounting_status;//挂账状态
                        heads.pk_oppaccount=record.values.otheraccount;//银行账户--收款银行账号
                        heads.pk_account=record.values.m_pk_bank;//银行账户--收款银行账号
                        tables.pk_oppaccount=record.values.otheraccount;//银行账户--收款银行账号
                        tables.pk_account=record.values.m_pk_bank;//银行账户--收款银行账号
                        heads.def8=record.values.checkdate;//流水时间
                        heads.pk_supplier=record.values.supplier;//对方单位
                        tables.pk_supplier=record.values.supplier;
                        heads.pk_balatype=record.values.pk_balatype2;    //结算方式
						tables.pk_balatype=record.values.pk_balatype2;  //结算方式
                        heads.primal_money=record.values.m_debitamount;//原币借发生额
                        heads.def10=record.values.m_debitamount;//流水金额
                        heads.primal_money.scale=heads.def10.scale="2";//
                        record.values.pk_currtype&&record.values.pk_currtype.value&&(heads.pk_currtype=record.values.pk_currtype)&&(tables.pk_currtype=record.values.pk_currtype);//币种
                        heads.rate=record.values.rate;//汇率
						tables.rate=record.values.rate;//汇率
                        //表体
                        if(record.values.explanation&&record.values.explanation.value){
                            record.values.explanation.display=record.values.explanation.value;
                            tables.memo=record.values.explanation;//摘要
                        }
                        tables.rec_primal=record.values.m_debitamount;//原币借发生额
                        heads.local_money=tables.rec_local={value:parseFloat(record.values.m_debitamount.value)*parseFloat(record.values.rate.value)};
                        heads.local_money.scale=tables.rec_primal.scale=heads.def10.scale=tables.rec_local.scale="2";//
                        tables.objecttype=record.values.supplier&&record.values.supplier.value?{value:"1",display:"供应商"}:{value:"",display:""};//对方单位
                        heads.objecttype=record.values.supplier&&record.values.supplier.value?{value:"1",display:"供应商"}:{value:"",display:""};//对方单位
                    }

                    if (res.data.head) {
                        //设置组织可以编辑
                        this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
                        this.props.form.setFormItemsDisabled(this.formId, { 'pk_org': false });
                        this.props.form.setFormItemsVisible(this.formId, { 'pk_org_v': false });
                        if (this.state.curr_pk_org) {
                            //是否包含预置财务组织
                            isContainBoay = true;
                            this.props.resMetaAfterPkorgEdit();
                            //财务组织赋值
                            !window.location.hash.includes("ifturnTo=")&&this.props.form.setFormItemsValue(this.formId,
                                {
                                    'pk_org': {
                                        value: this.state.curr_pk_org,
                                        display: this.state.curr_orgname
                                    },
                                    'pk_org_v': {
                                        value: this.state.curr_pk_org_v,
                                        display: this.state.curr_orgname_v
                                    }
                                }
                            );
                        } else {
                            //没有预设组织
                            this.props.resMetaAfterPkorgEdit();
                            this.props.initMetaByPkorg();
                        }
                        /**
                         * 收款交易类型设置
                         */
                        if (tradetype && tradetype.length > 0) {
                            this.props.form.setFormItemsValue(this.formId, { 'trade_type': { display: tradename, value: tradetype } });
                        }
                        if (tradepk && tradepk.length > 0) {
                            this.props.form.setFormItemsValue(this.formId, { 'pk_tradetypeid': { display: tradename, value: tradepk } });
                        }
                        /**
                         * 来源系统---翻译----赋值
                         */
                        let source_Flag = res.data.head[this.formId].rows[0].values.source_flag.value;
                        setSourceFlag.call(this, source_Flag);
                        // this.source_flag(source_Flag);
                    }
                    if (res.data.body && isContainBoay) {
                        this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
                    }
                    this.billno = '';
                    this.toggleShow();//切换页面状态,
                } else {
                    //清空数据
                    this.props.form.EmptyAllFormValue(this.formId);
                    this.props.cardTable.setTableData(this.tableId, { rows: [] });
                }
            }
        });
    }
}
