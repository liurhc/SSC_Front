import { createPage, ajax, base, high, toast, cardCache, print, output } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";
import { checkNoteno, setLastPkNoteNo } from "../events/checkNoteno.js";//选择票据号是否增加表体行
import { setSourceFlag } from '../../util/setSourceFlag.js';//设置来源
//缓存
let { setDefData, getDefData,
    getCurrentLastId, getCacheById,
    updateCache, addCache, getNextId,
    deleteCacheById } = cardCache;

/**
 * [外币兑换]-卡片加载数据
 * @param {*} props 
 * @param {*} config 
 * @param {*} pks 
 */
export const loadQueryData = function () {
    
    //查询单据详情
    if (this.props.getUrlParam('status') === 'edit' ||
        this.props.getUrlParam('status') === 'browse') {
        if (this.props.getUrlParam('id') &&
            this.props.getUrlParam('id').length > 0) {
            let data = { pk: this.props.getUrlParam('id'), pageid: this.pageId };
            ajax({
                url: '/nccloud/cmp/recbill/recbillquerycard.do',
                data: data,
                success: (res) => {
                    if (res.data) {
                        if (res.data.head) {
                            this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
                            //页签赋值
                            let billno = res.data.head[this.formId].rows[0].values.bill_no.value;
                            let source_flag = res.data.head[this.formId].rows[0].values.source_flag.value;
                            setSourceFlag.call(this, source_flag);
                            // this.source_flag(source_flag);//设置系统来源
                            //按钮显示控制[单据状态]
                            this.props.setUrlParam({
                                billno: res.data.head[this.formId].rows[0].values.bill_status.value
                            });
                            this.billno = billno;

                        }
                        if (res.data.body) {
                            this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
                        }
                        this.toggleShow();//切换页面状态,
                    } else {
                        //清空数据
                        this.props.form.EmptyAllFormValue(this.formId);
                        this.props.cardTable.setTableData(this.tableId, { rows: [] });
                    }

                    if (this.props.getUrlParam('status') === 'edit') {
                        //设置组织不可以编辑
                        this.props.form.setFormItemsDisabled(this.formId, { 'pk_org': true });
                        this.props.form.setFormItemsDisabled(this.formId, { 'pk_org_v': true });
                        this.props.form.setFormItemsDisabled(this.formId, { 'pk_fiorg': true });
                        this.props.form.setFormItemsDisabled(this.formId, { 'pk_fiorg_v': true });
                        this.props.form.setFormItemsDisabled(this.formId, { 'pk_pcorg_v': true });
                        this.props.form.setFormItemsDisabled(this.formId, { 'pk_pcorg': true });
                    }
                }
                // error: (res) => {
                //     //并发处理，卡片直接报错，列表中缓存删除
                //     toast({ color: 'warning', content: res.message });/* 国际化处理： 并发错误,单据未查询到!*/
                //     this.deleteId = this.props.getUrlParam('id');//删除单据pk
                //     this.deleteCacheData();//删除list缓存
                //     //清空列表
                //     this.props.form.EmptyAllFormValue(this.formId);
                //     this.props.cardTable.setTableData(this.tableId, { rows: [] });
                //     this.props.setUrlParam({
                //         status: 'browse',
                //         id: '',
                //         billno: '',
                //         pagecode: this.pageId
                //     });
                //     this.billno = null;
                //     this.toggleShow();//切换页面状态
                // }
            });
        }

    }
    //复制
    if (this.props.getUrlParam('status') === 'copy') {
        // /清空表单form所有数据
        this.props.form.EmptyAllFormValue(this.formId);
        let data = { pk: this.props.getUrlParam('id'), pageid: this.pageId };
        ajax({
            url: '/nccloud/cmp/recbill/recbillcopy.do',
            data: data,
            success: (res) => {
                if (res.data) {
                    if (res.data.head) {
                        this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
                        let source_flag = res.data.head[this.formId].rows[0].values.source_flag.value;
                        setSourceFlag.call(this, source_flag);
                        // this.source_flag(source_flagc);
                    }
                    if (res.data.body) {
                        this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);

                    }
                    this.billno = '';
                    this.toggleShow();//切换页面状态,
                } else {
                    //清空数据
                    this.props.form.EmptyAllFormValue(this.formId);
                    this.props.cardTable.setTableData(this.tableId, { rows: [] });
                }
                //设置组织不可以编辑
                this.props.form.setFormItemsDisabled(this.formId, { 'pk_org': true });
            }
        });
    }
    //新增--->需要添加交易类型
    if (this.props.getUrlParam('status') === 'add') {
        // /清空表单form所有数据
        this.props.form.EmptyAllFormValue(this.formId);
        //清空table所有数据
        this.props.cardTable.setTableData(this.tableId, { rows: [] });
        //处理session数据
        let tradetype = JSON.parse(sessionStorage.getItem("sessionTP"));
        let tradename = JSON.parse(sessionStorage.getItem("sessionName"));
        let tradepk = JSON.parse(sessionStorage.getItem("sessionpk"));
        if (tradetype) {
            let fdStart = tradetype.indexOf("D5");
            let fdStartf = tradetype.indexOf("F5");

            if (fdStart == 0 || fdStartf == 0) {
                tradetype = '';
                tradename = '';
                tradepk = '';
            }
        }
        let data = { pk: this.tableId, pageid: this.pageId, billtype: tradetype };
        ajax({
            url: '/nccloud/cmp/recbill/recbilladdevent.do',
            data: data,
            success: (res) => {
                if (res.data) {
                    let isContainBoay = false;//是否包含表体显示[有组织显示，没有组织不显示]
                    if (res.data.head) {
                        if (res.data.head[this.formId].rows[0].values.pk_org.value
                            && res.data.head[this.formId].rows[0].values.pk_org.display) {
                            isContainBoay = true;
                            this.org_value = res.data.head[this.formId].rows[0].values.pk_org.value;
                            this.org_display = res.data.head[this.formId].rows[0].values.pk_org.display;
                            this.props.resMetaAfterPkorgEdit();
                            this.props.cardTable.setStatus(this.tableId, 'edit');
                        } else {
                            this.props.resMetaAfterPkorgEdit();
                            this.props.initMetaByPkorg();//此方法不可以调用2次，不然rest失败
                        }
                        this.props.form.setFormItemsDisabled(this.formId, { 'pk_org': false });//组织可以进行编辑
                        this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
                        /**
                         * 收款交易类型设置
                         */
                        if (tradetype && tradetype.length > 0) {
                            this.props.form.setFormItemsValue(this.formId, { 'trade_type': { display: tradename, value: tradetype } });
                        }
                        if (tradepk && tradepk.length > 0) {
                            this.props.form.setFormItemsValue(this.formId, { 'pk_tradetypeid': { display: tradename, value: tradepk } });
                        }
                        /**
                         * 来源系统---翻译----赋值
                         */
                        let source_Flag = res.data.head[this.formId].rows[0].values.source_flag.value;
                        setSourceFlag.call(this, source_Flag);
                        // this.source_flag(source_Flag);
                    }
                    if (res.data.body && isContainBoay) {
                        this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
                    }
                    this.billno = '';
                    this.toggleShow();//切换页面状态,
                } else {
                    //清空数据
                    this.props.form.EmptyAllFormValue(this.formId);
                    this.props.cardTable.setTableData(this.tableId, { rows: [] });
                }
            }
        });
    }
}
