import { ajax, base, toast } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";//配置的id和area信息

/**
 * [收款]-被联查卡片表体按钮是否可用
 * @param {*} props 
 * @param {*} hasOrg 是否有组织true/false
 */
export const linkButtonVisable = function () {
    let status = 'browse';//页面请求状态被联查默认是browse
    this.setState({ isTradeshow: false });//按钮控制显示交易类型
    this.props.button.setButtonVisible(
        [
            'saveBtn',
            'savesubmitBtn',
            'saveaddBtn',
            'cancelBtn',
            'annexBtn',
            'annexgroup',
            'addbodyBtn',
            'deletebodyBtn',
            'copybodyBtn',
            'unsubmitBtn',
            'redbillBtn',
            'makebillBtn',
            'queryvoucherBtn',
            'rectradetypeBtn',
            'linkquery',
            'printgroup',
            'addBtn',
            'editBtn',
            'deleteBtn',
            'copyBtn',
            'subimtBtn',
            'imagegroup',
            'moreoperateBtn',
            'querysynbillBtn',
            'addBtn',
            'copyBtn',
            'querymsgBtn',
            'imagegroup',
            'linksettleBtn',
            'unlinksettleBtn',
            'refreshBtn'
        ], false);
    this.props.button.setButtonVisible(
        [
            'imagegroup',//影像
            'annexBtn',//附件
            'linkquery',//联查
            'linkquery1',
            'linkquerybillBtn',//联查单据
            'querymsgBtn',//审批详情
            'querysynbillBtn',//协同单据
            'queryvoucherBtn',//凭证
            // 'printgroup',//原单据打印按钮在更多里面显示不对
            'cardprintBtn',//新增联查打印按钮20181228
            'cardprintBtn_2',//新增联查打印按钮
            'cardoutputBtn'//新增联查输出按钮
        ], true);
      //body 按钮的显隐性控制
    this.props.button.setButtonVisible(
        ['addbodyBtn', 'deletebodyBtn', 'copybodyBtn', 'cancelLineBtn', 'copyLineLastBtn'],
        false);
}
