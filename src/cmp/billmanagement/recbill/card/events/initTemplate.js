import tableButtonClick from './tableButtonClick';
import { Templatedata } from "../../config/Templatedata";//配置的id和area信息
import { buttonVisable } from "./buttonVisable";//按钮显隐性
import { orgVersionUtil } from "../../util/orgVersionUtil";//多版本显示
import { RowDetailButton } from "../../../../../tmpub/pub/util/RowDetailButton";//表体展开和收起
let formId = Templatedata.card_formid;
let tableId = Templatedata.card_tableid;
let pageId = Templatedata.card_pageid;
let appId = Templatedata.card_appid;
let appcode = Templatedata.app_code;
export default function (props) {
	let self = this;
	if (props.getUrlParam('pagecode')) {
		pageId = props.getUrlParam('pagecode');
	}
	props.createUIDom(
		{
			pagecode: pageId//页面id
		},
		function (data) {
			if (data) {
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					// props.button.setPopContent('deletelineBtn', '确认要删除这条信息吗？');
					// props.button.setPopContent('deletebodyBtn', '确认要删除这些信息吗？'); 
				}
				if (data.template) {
					let meta = data.template;
					modifierMeta.call(self, props, meta)
					props.meta.setMeta(meta);
					if (props.getUrlParam('status') == 'browse') {
						props.cardTable.setStatus(tableId, 'browse');
					} else {
						props.cardTable.setStatus(tableId, 'edit');
					}
					//默认组织赋值显示
					let context = data.context;
					if (context.pk_org) {
						self.setState({
							curr_pk_org: context.pk_org,
							curr_orgname: context.org_Name,
							curr_pk_org_v: context.pk_org_v,
							curr_orgname_v: context.org_v_Name,
						});
						self.org_value = context.pk_org;
						self.org_display = context.org_Name
					}

					//发布小应用默认交易类型赋值
					if (data.context && data.context.paramMap) {
						let { transtype, pk_transtype, transtype_name } = data.context.paramMap;
						if (transtype && pk_transtype && transtype_name) {
							//若存储值是字符串，可以直接存储
							sessionStorage.setItem("sessionTP", JSON.stringify(transtype));
							sessionStorage.setItem("sessionName", JSON.stringify(transtype_name));
							sessionStorage.setItem("sessionpk", JSON.stringify(pk_transtype));
							//支持网问题-begin:
							sessionStorage.setItem("billname", JSON.stringify(transtype_name));
						}
					}

					//如果是[关联结算信息]新增就不用处理
					if (props.getUrlParam('src')) {

					} else {
						//设置新增的时候除了组织之外其他的不可以编辑
						if (props.getUrlParam('status') == 'add') {
							props.resMetaAfterPkorgEdit();
							props.initMetaByPkorg();
						}
					}
					self.initData();//首次加载数据
				}
				//修改页面状态
				togglePageShow.call(self, props);
			}
		}
	)
}

//根据页面状态，修改编辑态表格
function togglePageShow(props) {
	orgVersionUtil.call(this, props, this.formId)//多版本视图显隐性
	buttonVisable.call(this, props);//按钮显隐性
}
//页面加载meta
function modifierMeta(props, meta) {
	let self = this;
	let status = props.getUrlParam('status');
	if (status === 'browse') {
		meta[formId].status = status;
		meta[tableId].status = status;
	} else {
		meta[formId].status = 'edit';
		meta[tableId].status = 'edit';
	}
	//组织权限
	meta[formId].items.map((item) => {
		//发送发组织，接收方组织：根据用户权限过滤
		if (item.attrcode == 'pk_org') {
			item.queryCondition = () => {
				return {
					funcode: props.getSearchParam('c'),//appcode获取
					TreeRefActionExt: 'nccloud.web.tmpub.filter.FinanceOrgPermissionFilter'
				};
			};
		}
	});
	// let multiLang = props.MutiInit.getIntl(moudleId);
	let porCol = {
		attrcode: 'opr',
		label: this.props.MutiInit.getIntl("36070RBM") && this.props.MutiInit.getIntl("36070RBM").get('36070RBM-000018'),/* 国际化处理： 操作*/
		fixed: 'right',
		itemtype: 'customer',
		visible: true,
		width: '200px',
		render(text, record, index) {
			//操作列展开和收起适配
			let status = props.cardTable.getStatus(tableId);
			let buttonAry = status === "browse"
				? ["openBtn"]
				: (self.state.pasteflag ?
					["copythisBtn"] : ["editmoreBtn", "copylineBtn", "addlineBtn", "deletelineBtn"]);
			return  status === 'browse' ?
				<RowDetailButton
					record={record}
					bodyAreaCode={tableId}
					props={props}
				/> :
				(
					<div className="currency-opr-col">
						{
							props.button.createOprationButton(buttonAry, {
								area: Templatedata.card_body_inner,
								buttonLimit: 3,
								onButtonClick: (props, key) => tableButtonClick.call(self, props, key, text, record, index)
							})
						}
					</div>
				);
		}
	};
	meta[tableId].items.push(porCol);
	//财务组织:全加载
	meta[formId].items.find((e) => e.attrcode === 'pk_org').isTreelazyLoad = false;
	return meta;
}
