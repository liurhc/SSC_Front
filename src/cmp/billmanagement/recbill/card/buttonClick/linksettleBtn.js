import { createPage, ajax, base, high, toast, cardCache, print, output,cacheTools } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";
//缓存
let { setDefData, getDefData,
    getCurrentLastId, getCacheById,
    updateCache, addCache, getNextId,
    deleteCacheById } = cardCache;

/**
 * [外币兑换]-关联结算按钮
 * @param {*} props 
 * @param {*} config 
 * @param {*} pks 
 */
export const linksettleBtn = function () {
  
    this.props.openTo('/cmp/settlementmanagement/settlepublic/list/index.html',
        {
            appcode: Templatedata.settle_code,
            pagecode: Templatedata.settle_pageid,
            status: 'browse',
            src: Templatedata.settle_src,
            callback: Templatedata.settle_callback,  //回调页面
            name: this.props.MutiInit.getIntl("36070RBM") && this.props.MutiInit.getIntl("36070RBM").get('36070RBM-000013'),/* 国际化处理： 关联结算信息*/
            callbackappcode: Templatedata.callback_appcode,
            callbackpagecode: Templatedata.callback_pagecode

     });
}
