import { createPage, ajax, base, high, toast, cardCache, print, output } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";
//缓存
let { setDefData, getDefData,
    getCurrentLastId, getCacheById,
    updateCache, addCache, getNextId,
    deleteCacheById } = cardCache;

/**
 * [外币兑换]-修改按钮
 * @param {*} props 
 * @param {*} config 
 * @param {*} pks 
 */
export const editBtn = function () {
    /**
        * 加载模版----->根据交易类型
        */
    let edit_tradetype = this.pageId
    if (this.props.form.getFormItemsValue(this.formId, 'trade_type').value) {
        edit_tradetype =  this.props.form.getFormItemsValue(this.formId, 'trade_type').value
    }
    this.props.pushTo('/card', {
        status: 'edit',
        id: this.props.getUrlParam('id'),
        pagecode: edit_tradetype,
        bill_no: this.props.form.getFormItemsValue(this.formId, 'bill_status').value//单据状态
    })
    this.refresh();
}
