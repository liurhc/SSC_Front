import { createPage, ajax, base, high, toast, cardCache, print, output } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";
//缓存
let { setDefData, getDefData,
  getCurrentLastId, getCacheById,
  updateCache, addCache, getNextId,
  deleteCacheById } = cardCache;

/**
 * [外币兑换]-联查单据按钮
 * @param {*} props 
 * @param {*} config 
 * @param {*} pks 
 */
export const linkquerybillBtn = function () {
  if (!this.props.form.getFormItemsValue(this.formId, 'pk_recbill').value) {
    toast({ color: 'warning', content: this.props.MutiInit.getIntl("36070RBM") && this.props.MutiInit.getIntl("36070RBM").get('36070RBM-000003') });/* 国际化处理： 操作失败，无数据!*/
    return;
  }
  let showbilltrackpk = this.props.form.getFormItemsValue(this.formId, 'pk_recbill').value;
  let billtrack_billtype = '';
  if (this.props.form.getFormItemsValue(this.formId, 'bill_type').value) {
    billtrack_billtype = this.props.form.getFormItemsValue(this.formId, 'bill_type').value;
  }
  if (showbilltrackpk) {
    this.setState({
      showbilltrack: true,//显示联查单据
    });
    this.showbilltrackpk = showbilltrackpk;//单据pk
    this.showbilltracktype = billtrack_billtype;//联查单据类型
  }
}
