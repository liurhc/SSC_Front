import { createPage, ajax, base, high, toast, cacheTools, cardCache, print, output } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";
import { BatchToast } from '../../../../public/CMPMessage.js';//批量提示语句
import { MakeBillApp } from '../../../../public/utils/Makebill';//制单
import { linkApp, linkVoucherApp } from '../../../../public/utils/LinkUtil';//凭证
//缓存
let { setDefData, getDefData,
    getCurrentLastId, getCacheById,
    updateCache, addCache, getNextId,
    deleteCacheById } = cardCache;

/**
 * [收款协同]-联查凭证
 * @param {*} props 
 * @param {*} config 
 * @param {*} pks 
 */
export const queryvoucherBtn = function () {
    if (!this.props.form.getFormItemsValue(this.formId, 'pk_recbill').value) {
        toast({ color: 'warning', content: this.props.MutiInit.getIntl("36070RBMCP") && this.props.MutiInit.getIntl("36070RBMCP").get('36070RBMCP-000012') });/* 国际化处理： 操作失败，无数据!*/
        return;
    }
    let voucher_billtype=Templatedata.voucher_billtyp;
    if (this.props.form.getFormItemsValue(this.formId, 'trade_type').value) {
        voucher_billtype = this.props.form.getFormItemsValue(this.formId, 'trade_type').value;
    }
    linkVoucherApp(
        this.props,
        this.props.form.getFormItemsValue(this.formId, 'pk_recbill').value,
        'nc.vo.cmp.bill.RecBillAggVO',
        Templatedata.voucher_appcode,
        voucher_billtype,
        this.props.form.getFormItemsValue(this.formId, 'bill_no').value,
    );
}
