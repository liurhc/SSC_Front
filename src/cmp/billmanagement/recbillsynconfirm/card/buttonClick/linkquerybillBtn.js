import { createPage, ajax, base, high, toast, cacheTools, cardCache, print, output } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";
import { BatchToast } from '../../../../public/CMPMessage.js';//批量提示语句
import { MakeBillApp } from '../../../../public/utils/Makebill';//制单
import { linkApp, linkVoucherApp } from '../../../../public/utils/LinkUtil';//凭证
//缓存
let { setDefData, getDefData,
    getCurrentLastId, getCacheById,
    updateCache, addCache, getNextId,
    deleteCacheById } = cardCache;

/**
 * [收款协同]-联查单据
 * @param {*} props 
 * @param {*} config 
 * @param {*} pks 
 */
export const linkquerybillBtn = function () {
    if (!this.props.form.getFormItemsValue(this.formId, 'pk_recbill').value) {
        toast({ color: 'warning', content: this.props.MutiInit.getIntl("36070RBMCP") && this.props.MutiInit.getIntl("36070RBMCP").get('36070RBMCP-000012') });/* 国际化处理： 操作失败，无数据!*/
        return;
    }
    let showbilltrackpk = this.props.form.getFormItemsValue(this.formId, 'pk_recbill').value;
    let billtrack_billtype = Templatedata.billtrack_billtype;
    if (this.props.form.getFormItemsValue(this.formId, 'bill_type').value) {
        billtrack_billtype = this.props.form.getFormItemsValue(this.formId, 'bill_type').value;
    }
    if (showbilltrackpk) {
        this.setState({
            showbilltrack: true,//显示联查单据
        });
        this.showbilltrackpk = showbilltrackpk;//单据pk
        this.showbilltracktype = billtrack_billtype;//单据类型
    }
}
