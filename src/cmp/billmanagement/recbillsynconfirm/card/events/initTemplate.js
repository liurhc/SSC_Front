import { base, ajax } from 'nc-lightapp-front';
import tableButtonClick from './tableButtonClick';
import { Templatedata } from "../../config/Templatedata";//配置的id和area信息
import { buttonVisable } from "./buttonVisable";//按钮显隐性
import { orgVersionUtil } from "../../util/orgVersionUtil";//多版本显示
import { RowDetailButton } from "../../../../../tmpub/pub/util/RowDetailButton";//表体展开和收起
const formId = Templatedata.card_formid;
const tableId = Templatedata.card_tableid;
const pageId = Templatedata.card_pageid;
const moudleId = Templatedata.list_moduleid;
let appcode = Templatedata.app_code;
let printcard_funcode = Templatedata.printcard_funcode;
export default function (props) {
	let self = this;
	props.createUIDom(
		{
			pagecode: pageId,//页面id
			appcode: appcode,
			appid: Templatedata.card_appid//注册按钮的id
		},
		function (data) {
			if (data) {
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					// props.button.setPopContent('deletelineBtn', '确认要删除这条信息吗？'); 
				}
				if (data.template) {
					let meta = data.template;
					modifierMeta.call(self, props, meta)
					props.meta.setMeta(meta);
					if (props.getUrlParam('status') == 'browse') {
						props.cardTable.setStatus(tableId, 'browse');
					} else {
						props.cardTable.setStatus(tableId, 'edit');
						//  props.cardTable.setEditableByIndex(tableId, 0, 'pk_currtype', false);
					}
				}
				//修改页面状态
				togglePageShow.call(self, props);

			}
		}
	)
}

//根据页面状态，修改编辑态表格
function togglePageShow(props) {

	let status = props.getUrlParam('status');
	let billstatus = props.getUrlParam('billno');//获取单据状态
	let flag = status === 'browse' ? false : true;
	orgVersionUtil.call(this, this.props, this.formId)//多版本视图显隐性
	buttonVisable.call(this, props);//按钮的显隐性
}

function modifierMeta(props, meta) {
	let self = this;
	let status = props.getUrlParam('status');
	if (status === 'copy') {
		meta[formId].status = 'edit';
		meta[tableId].status = 'edit';
	} else {
		meta[formId].status = status;
		meta[tableId].status = status;
	}
	let porCol = {
		attrcode: 'opr',
		label: this.props.MutiInit.getIntl("36070RBMCP") && this.props.MutiInit.getIntl("36070RBMCP").get('36070RBMCP-000016'),/* 国际化处理： 操作*/
		fixed: 'right',//固定操作列
		itemtype: 'customer',
		visible: true,
		width: '200px',
		render(text, record, index) {
			//展开和收起使用
			let status = props.cardTable.getStatus(tableId);
			let buttonAry =
				props.getUrlParam("status") === "browse"
					? ["openBtn"]
					: (self.state.pasteflag ?
						["copythisBtn"] : ['editmoreBtn', "copylineBtn", "addlineBtn", "deletelineBtn"]);
			return status === 'browse' ?
				<RowDetailButton
					record={record}
					bodyAreaCode={tableId}
					props={props}
				/> :
				(
					<div className="currency-opr-col">
						{
							props.button.createOprationButton(buttonAry, {
								area: Templatedata.card_body_inner,
								buttonLimit: 3,
								onButtonClick: (props, key) => tableButtonClick.call(self, props, key, text, record, index)
							})
						}
					</div>
				);

		}
	};
	//组织用户权限过滤
	meta[formId].items.map((item) => {
		// 发送发组织，接收方组织：根据用户权限过滤
		if (item.attrcode == 'pk_org') {
			item.queryCondition = () => {
				return {
					funcode: printcard_funcode,
					TreeRefActionExt: 'nccloud.web.tmpub.filter.FinanceOrgPermissionFilter'
				};
			};
		}
	});
	//财务组织:全加载
	meta[formId].items.find((e) => e.attrcode === 'pk_org').isTreelazyLoad = false;
	meta[tableId].items.push(porCol);

	return meta;
}
