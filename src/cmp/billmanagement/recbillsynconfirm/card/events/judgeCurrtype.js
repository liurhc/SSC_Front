import { createPage, ajax, base, high, toast, cardCache, print, output } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";
//缓存
let { setDefData, getDefData,
    getCurrentLastId, getCacheById,
    updateCache, addCache, getNextId,
    deleteCacheById } = cardCache;

/**
 * [收款结算]-判断所选币种是否本币币种
 * @param {*} value 
 */
export const judgeCurrtype = function (pk_org, pk_currtype) {
    //====判断是否选择的本币币种,从而设置本币汇率编辑性=======
    let data = {
        pk_currtype: pk_currtype,
        pk_org: pk_org
    }
    ajax({
        url: '/nccloud/cmp/recbill/judgecurrtype.do',
        data: data,
        success: (res) => {
            if (res.success) {
                debugger
                if (res.data) {
                    //币种是组织本币币种
                    //组织不是本币币种
                    this.props.form.setFormItemsDisabled(this.formId, { 'local_rate': true });
                    // this.props.cardTable.setColEditableByKey(this.tableId,'local_rate',false);
                } else {
                    //组织不是本币币种
                    this.props.form.setFormItemsDisabled(this.formId, { 'local_rate': false });
                    // this.props.cardTable.setColEditableByKey(this.tableId,'local_rate',true);
                    // let total = this.props.cardTable.getNumberOfRows(this.tableId);//表体table行数
                    // for (let index = 0; index < total; index++) {
                    //     this.props.cardTable.setEditableByIndex(this.tableId, index, 'local_rate', true);
                    // }
                }
            }
        }
    });
}
/**
 * [收款结算]-判断所选币种是否本币币种
 * @param {*} pk_org 
 * @param {*} pk_currtype 
 * @param {*} index 变化的列表index 
 */
export const judgeTableCurrtype = function (pk_org, pk_currtype,index) {
    //====判断是否选择的本币币种,从而设置本币汇率编辑性=======
    debugger
    let data = {
        pk_currtype: pk_currtype,
        pk_org: pk_org
    }
    ajax({
        url: '/nccloud/cmp/recbill/judgecurrtype.do',
        data: data,
        success: (res) => {
            if (res.success) {
                debugger
                if (res.data) {
                    //币种是组织本币币种
                    this.props.cardTable.setEditableByIndex(this.tableId, index, 'local_rate', false);
                } else {
                    //组织不是本币币种
                    this.props.cardTable.setEditableByIndex(this.tableId, index, 'local_rate', true);
                }
            }
        }
    });
}
