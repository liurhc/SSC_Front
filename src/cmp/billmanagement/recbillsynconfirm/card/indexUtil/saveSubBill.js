import { createPage, ajax, base, high, toast, cacheTools, cardCache, print, output } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";
import { BatchToast } from '../../../../public/CMPMessage.js';//批量提示语句
import { MakeBillApp } from '../../../../public/utils/Makebill';//制单
import { linkApp, linkVoucherApp } from '../../../../public/utils/LinkUtil';//凭证
import { setSourceFlag } from '../../util/setSourceFlag.js';//设置来源
//缓存
let { setDefData, getDefData,
    getCurrentLastId, getCacheById,
    updateCache, addCache, getNextId,
    deleteCacheById } = cardCache;

/**
 * [收款协同]-保存提交
 * @param {*} props 
 * @param {*} config 
 * @param {*} pks 
 */
export const saveSubBill = function () {
    let url = '/nccloud/cmp/recbill/recbillsynconfirmsubmit.do'//协同确认保存提交
    let savesubmitBtnData = this.props.createMasterChildData(this.pageId, this.formId, this.tableId);
    let savesubmitBtnflag = this.props.form.isCheckNow(this.formId);
    let sb_tableflag = this.props.cardTable.checkTableRequired(this.tableId);//table必输项校验
    let saveSubStatus = this.props.getUrlParam('status');
    if (savesubmitBtnflag && sb_tableflag) {
        ajax({
            url: url,
            data: savesubmitBtnData,
            success: (res) => {
                let pk_paybill = null;
                let { success, data } = res;
                if (res.success) {
                    //提交--指派
                    if (data && data.workflow &&
                        (data.workflow == 'approveflow' ||
                            data.workflow == 'workflow')) {
                        //渲染数据
                        if (data.billcard) {
                            if (res.data.billcard.head) {
                                this.props.form.setAllFormValue({ [this.formId]: res.data.billcard.head[this.formId] });
                                let source_Flag = res.data.billcard.head[this.formId].rows[0].values.source_flag.value;
                                let billno = res.data.billcard.head[this.formId].rows[0].values.bill_no.value;
                                let billstatue = res.data.billcard.head[this.formId].rows[0].values.bill_status.value;
                                let pk_recbill = res.data.billcard.head[this.formId].rows[0].values.pk_recbill.value;
                                setSourceFlag.call(this, source_Flag);
                                this.billno = billno;
                                //增加缓存
                                let firstStatus = this.props.getUrlParam('status');
                                if (!firstStatus || firstStatus == 'add' || firstStatus == 'copy') {
                                    //新增缓存
                                    addCache(pk_recbill, res.data.billcard, this.formId, this.dataSource, res.data.billcard.head[this.formId].rows[0].values);
                                } else {
                                    //更新缓存
                                    updateCache(this.pkname, pk_recbill, res.data.billcard, this.formId, this.dataSource, res.data.billcard.head[this.formId].rows[0].values);
                                }
                                //跳转页面
                                this.props.setUrlParam({
                                    status: 'browse',
                                    id: pk_recbill,
                                    billno: billstatue,
                                    pagecode: this.pageId
                                });
                                this.toggleShow();//切换页面状态
                            }
                            if (res.data.billcard.body) {
                                this.props.cardTable.setTableData(this.tableId, res.data.billcard.body[this.tableId]);
                            }
                        }
                        //弹出指派框
                        this.compositedata = data;
                        this.setState({
                            compositedisplay: true,
                        });
                    } else if (res.data) {
                        toast({ color: 'success', content: this.props.MutiInit.getIntl("36070RBMCP") && this.props.MutiInit.getIntl("36070RBMCP").get('36070RBMCP-000002') });/* 国际化处理： 保存提交成功*/
                        if (res.data.head) {
                            this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
                            let subbillno = res.data.head[this.formId].rows[0].values.bill_no.value;
                            this.billno = subbillno;
                        }
                        //根据后台返回的数据进行缓存处理<pass>
                        if (res.data.body && res.data.body[this.tableId]) {
                            let body = res.data.body;//差异缓存处理
                            body[this.tableId] = this.props.cardTable.updateDataByRowId(this.tableId, res.data.body[this.tableId])
                            if (body) {
                                res.data.body = body;//差异缓存处理
                            }
                        }
                        let sub_pk_recbill = res.data.head[this.formId].rows[0].values.pk_recbill.value;
                        let sub_billstatue = res.data.head[this.formId].rows[0].values.bill_status.value;
                        //增加缓存
                        let card_sub_status = this.props.getUrlParam('status');
                        if (!card_sub_status || card_sub_status == 'add' || card_sub_status == 'copy') {
                            //新增缓存
                            addCache(sub_pk_recbill, res.data, this.formId, this.dataSource, res.data.head[this.formId].rows[0].values);
                        } else {
                            //更新缓存
                            updateCache(this.pkname, sub_pk_recbill, res.data, this.formId, this.dataSource, res.data.head[this.formId].rows[0].values);
                        }
                        this.props.setUrlParam({
                            status: 'browse',
                            id: sub_pk_recbill,
                            billno: sub_billstatue,
                            pagecode: this.pageId
                        });
                        let source_Flag = res.data.head[this.formId].rows[0].values.source_flag.value;
                        // this.source_flag(source_Flag);//来源系统翻译
                        setSourceFlag.call(this, source_Flag);
                        this.toggleShow();//切换页面状态
                    }
                }

            }
        });
    }
}
