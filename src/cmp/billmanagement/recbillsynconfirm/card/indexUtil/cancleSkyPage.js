import { createPage, ajax, base, high, toast, cacheTools, cardCache, print, output } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";
import { BatchToast } from '../../../../public/CMPMessage.js';//批量提示语句
import { MakeBillApp } from '../../../../public/utils/Makebill';//制单
import { linkApp, linkVoucherApp } from '../../../../public/utils/LinkUtil';//凭证
//缓存
let { setDefData, getDefData,
    getCurrentLastId, getCacheById,
    updateCache, addCache, getNextId,
    deleteCacheById } = cardCache;

/**
 * [收款协同]-空页面
 * @param {*} props 
 * @param {*} config 
 * @param {*} pks 
 */
export const cancleSkyPage = function () {
    this.props.form.EmptyAllFormValue(this.formId);
    this.props.cardTable.setTableData(this.tableId, { rows: [] });
    this.props.pushTo('/card', {
        status: 'browse',
        id: '',
        billno: '',
        pagecode: this.pageId
    })
    this.props.resMetaAfterPkorgEdit();
    this.toggleShow();//切换页面状态
}
