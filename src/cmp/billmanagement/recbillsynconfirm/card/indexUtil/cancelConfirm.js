import { createPage, ajax, base, high, toast, cacheTools, cardCache, print, output } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";
import { BatchToast } from '../../../../public/CMPMessage.js';//批量提示语句
import { MakeBillApp } from '../../../../public/utils/Makebill';//制单
import { linkApp, linkVoucherApp } from '../../../../public/utils/LinkUtil';//凭证
//缓存
let { setDefData, getDefData,
    getCurrentLastId, getCacheById,
    updateCache, addCache, getNextId,
    deleteCacheById } = cardCache;

/**
 * [收款协同]-取消
 * @param {*} props 
 * @param {*} config 
 * @param {*} pks 
 */
export const cancelConfirm = function () {
    //编辑中的取消操作
    if (this.props.getUrlParam('status') === 'edit') {
        let edit_pk = this.props.getUrlParam('id');
        let bill_no = this.props.form.getFormItemsValue(this.formId, 'bill_status').value;
        this.cancleNewPage(edit_pk, bill_no);
    }
}
