import { createPage, ajax, base, high, toast, cacheTools,cardCache,print,output } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";
import { BatchToast } from '../../../../public/CMPMessage.js';//批量提示语句
import { MakeBillApp } from '../../../../public/utils/Makebill';//制单
import { linkApp, linkVoucherApp } from '../../../../public/utils/LinkUtil';//凭证
import { pageInfoClick } from '../events';
//缓存
let { setDefData, getDefData,
    getCurrentLastId, getCacheById,
    updateCache, addCache, getNextId,
    deleteCacheById } = cardCache;

/**
 * [收款协同]-删除
 * @param {*} props 
 * @param {*} config 
 * @param {*} pks 
 */
export const delConfirm = function () {
    let data = {
        'pk': this.props.getUrlParam('id'),
        'ts': this.props.form.getFormItemsValue(this.formId, 'ts').value
    };
    //删除后直接进入下一行
    let delpk = this.props.getUrlParam('id');
    if (delpk) {
        this.deleteId=delpk//删除单据pk
    }
    /**
     * id：数据主键的值
     * dataSource: 缓存数据命名空间
     */
    let nextId = getNextId(delpk, this.dataSource);
    ajax({
        url: '/nccloud/cmp/recbill/carddelete.do',
        data: data,
        success: (res) => {
            if (res.success) {
                toast({ color: 'success', content: this.props.MutiInit.getIntl("36070RBMCP") && this.props.MutiInit.getIntl("36070RBMCP").get('36070RBMCP-000031') });/* 国际化处理： 删除成功*/
                this.deleteCacheData();//删除缓存
                if (nextId != null) {
                    pageInfoClick.call(this, this.props, nextId);
                } else {
                    this.cancleSkyPage();//跳转空白页面
                }
            }
        }
    });
}
