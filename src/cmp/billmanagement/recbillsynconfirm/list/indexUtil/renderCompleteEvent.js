import { createPage, ajax, base, high, toast, cacheTools, cardCache, print, output } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";
import { BatchToast } from '../../../../public/CMPMessage.js';//批量提示语句
import { MakeBillApp } from '../../../../public/utils/Makebill';//制单
import { linkApp, linkVoucherApp } from '../../../../public/utils/LinkUtil';//凭证
//缓存
let { setDefData, getDefData,
    getCurrentLastId, getCacheById,
    updateCache, addCache, getNextId,
    deleteCacheById } = cardCache;

/**
 * [收款协同]-回复值
 * @param {*} props 
 * @param {*} config 
 * @param {*} pks 
 */
export const renderCompleteEvent = function () {
    let cachesearch = getDefData(this.searchKey, this.dataSource);
    if (cachesearch && cachesearch.conditions) {
        for (let item of cachesearch.conditions) {
            if (item.field == 'bill_date') {
                // 时间类型特殊处理
                let time = [];
                time.push(item.value.firstvalue);
                time.push(item.value.secondvalue);
                this.props.search.setSearchValByField(this.searchId, item.field,
                    { display: item.display, value: time });
            } else {
                this.props.search.setSearchValByField(this.searchId, item.field,
                    { display: item.display, value: item.value.firstvalue });
            }
        }
    }
}
