import { createPage, ajax, base, high, toast, cardCache } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";
import { BatchToast } from '../../../../public/CMPMessage.js';

//缓存
let { setDefData, getDefData,
    getCurrentLastId, getCacheById,
    updateCache, addCache, getNextId,
    deleteCacheById } = cardCache;

/**
 * [收款index]-表格双击事件
 * @param {*}  
 */
export const onrowDoubleclick = function (record, index, props, e) {

    this.props.pushTo('/card', {
        status: 'browse',
        id: record.pk_recbill.value,
        billno: record.bill_status.value
    });
}
