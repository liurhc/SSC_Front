import { createPage, ajax, base, high, toast, cacheTools, cardCache, print, output } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";
import { BatchToast } from '../../../../public/CMPMessage.js';//批量提示语句
import { MakeBillApp } from '../../../../public/utils/Makebill';//制单
import { linkApp, linkVoucherApp } from '../../../../public/utils/LinkUtil';//凭证
//缓存
let { setDefData, getDefData,
    getCurrentLastId, getCacheById,
    updateCache, addCache, getNextId,
    deleteCacheById } = cardCache;

/**
 * [收款协同]-联查凭证
 * @param {*} props 
 * @param {*} config 
 * @param {*} pks 
 */
export const queryvoucherBtn = function () {
    let voucherData = this.props.table.getCheckedRows(this.tableId);
    //数据校验
    if (voucherData.length != 1) {
        toast({
            color: 'warning',
            content: this.props.MutiInit.getIntl("36070RBMCP") && 
            this.props.MutiInit.getIntl("36070RBMCP").get('36070RBMCP-000055')/* 国际化处理： 请选择单条，联查凭证!*/
        });
        return;
    }
    let vorcher_type = Templatedata.voucher_billtype;
    let vorcher_pk = '';
    let vorcher_billno = '';
    voucherData.forEach((val) => {
        vorcher_pk = val.data.values.pk_recbill.value;//主键数组
        vorcher_type = val.data.values.trade_type.value;
        vorcher_billno = val.data.values.bill_no.value;
    });
    linkVoucherApp(
        this.props,
        vorcher_pk,
        'nc.vo.cmp.bill.RecBillAggVO',
        Templatedata.voucher_appcode,
        vorcher_type,
        vorcher_billno
    );
}
