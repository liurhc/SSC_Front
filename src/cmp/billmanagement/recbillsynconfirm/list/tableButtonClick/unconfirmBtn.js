import { createPage, ajax, base, high, toast, cacheTools,cardCache,print,output } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";
import { BatchToast } from '../../../../public/CMPMessage.js';//批量提示语句
import { MakeBillApp } from '../../../../public/utils/Makebill';//制单
import { linkApp, linkVoucherApp } from '../../../../public/utils/LinkUtil';//凭证
//缓存
let { setDefData, getDefData,
    getCurrentLastId, getCacheById,
    updateCache, addCache, getNextId,
    deleteCacheById } = cardCache;

/**
 * [收款协同]-取消确认
 * @param {*} props 
 * @param {*} config 
 * @param {*} pks 
 */
export const unconfirmBtn = function (record,index) {
    let unconfirmBtnArr = [];
    unconfirmBtnArr.push(record.pk_recbill.value);
    let unconfirmBtnData = {
        'pks': unconfirmBtnArr,
        'pageid': this.pageId,
        'ts':  record.ts.value
    };

    ajax({
        url: '/nccloud/cmp/recbill/recbillunsynconfirm.do',
        data: unconfirmBtnData,
        success: (res) => {
            toast({ color: 'success', content: this.props.MutiInit.getIntl("36070RBMCP") && this.props.MutiInit.getIntl("36070RBMCP").get('36070RBMCP-000059') });/* 国际化处理： 取消确认成功*/
            let updateDataArr = [{
                index: index,
                data: { values: res.data.head[this.tableId].rows[0].values }
            }];
            this.props.table.updateDataByIndexs(this.tableId, updateDataArr);
        }
    });
}
