import { asyncComponent } from 'nc-lightapp-front';

const card = asyncComponent(() => import(/* webpackChunkName: "cmp/billmanagement/curexchangeapprove/card/card" *//* webpackMode: "eager" */ '../card'));

const routes = [

	{
		path: '/card',
		component: card
	}
];

export default routes;
