import { base, ajax } from 'nc-lightapp-front';
import refer from './refer';//refer参照
import { Templatedata } from "../../config/Templatedata";//配置的id和area信息
import { buttonVisable } from "./buttonVisable";//按钮显隐性
let { NCPopconfirm } = base;
const { NCMessage } = base;
const formId = Templatedata.card_formid;
const tableId = Templatedata.card_tableid;
const pageId = Templatedata.card_pageid;
const appid = Templatedata.card_appid;
const buyform = Templatedata.buyform;
const sellform = Templatedata.sellform;
const resultform = Templatedata.resultform;
const chargeform = Templatedata.chargeform;
const printcard_funcode = Templatedata.printcard_funcode;
const appcode = Templatedata.app_code;
export default function (props) {
	let self = this;
	props.createUIDom(
		{
			pagecode: pageId,//页面id
			appcode: appcode,//小应用code
			appid: appid//注册按钮的id
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					modifierMeta(props, meta)
					modifierMeta.call(self,props, meta);
					props.meta.setMeta(meta);
					if (props.getUrlParam('status') === 'browse') {
						props.form.setFormStatus(formId, 'browse');
					} else {
						props.form.setFormStatus(formId, 'edit');
					}
				}

				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				//修改页面状态
				togglePageShow(props);
			}
		}
	)
}

//根据页面状态，修改编辑态表格
function togglePageShow(props) {
	let status = props.getUrlParam('status');
	let billstatus = props.getUrlParam('billno');//获取单据状态
	let flag = status === 'browse' ? false : true;
	buttonVisable(props);//控制按钮显隐性	
}

function modifierMeta(props, meta) {
	let self = this;
	let status = props.getUrlParam('status');
	meta[formId].status = status;
	meta[tableId].status = status;
	
	return meta;
}
