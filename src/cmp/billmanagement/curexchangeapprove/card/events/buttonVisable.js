//修改页面状态--button的显隐性
import { ajax } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";

let formId = Templatedata.card_formid;
let tableId = Templatedata.card_tableid;
let pageid = Templatedata.card_pageid;

export const buttonVisable = function (props) {
    //联查（买入账户余额、卖出账户余额、手续费账户余额）、打印、附件管理
    props.button.setButtonVisible(
        [
            'cardlinksearchBtn',
            'cardbuybalanceBtn',
            'cardsellbalanceBtn',
            'cardchargebalanceBtn',
            'cardprintBtn',
            'cardoutputBtn',
            'cardaccessoryBtn'],
        true
    );
}
