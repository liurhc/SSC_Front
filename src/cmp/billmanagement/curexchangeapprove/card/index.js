//单表卡片
import React, { Component } from 'react';
import { createPage, ajax, base, toast, high } from 'nc-lightapp-front';
import { buttonClick, initTemplate, afterEvent } from './events';
import { jsondata } from "./jsondata";
import { Templatedata } from "../config/Templatedata";//配置的id和area信息
import { buttonVisable } from "./events/buttonVisable";//按钮显隐控制
import NCCOriginalBalance from '../../../public/restmoney/list/index';
let { NCScrollElement, NCAffix } = base;
const { NCUploader, ApproveDetail, PrintOutput } = high;//附件打印审批意见相关
const { NCDiv: Div } = base;//样式更新
const { NCBackBtn } = base;
class Card extends Component {
	constructor(props) {
		super(props);
		this.formId = Templatedata.card_formid;
		this.searchId = Templatedata.list_searchid;
		this.moduleId = Templatedata.list_moduleid;
		this.tableId = Templatedata.card_tableid;
		this.pageId = Templatedata.card_pageid;
		this.state = {
			show: false,//审批意见是否显示
			billid: '',//审批意见单据pk
			billtype: '',//审批意见单据类型
			billno: '', // 单据编号
			billId: '',//单据pk
			showUploader: false,//控制附件弹出框
			target: null,//控制弹出位置
			org_value: '',//切换组织取消使用
			org_display: '',//切换组织取消使用
			outputData: '',//输出使用
			showOriginal: false, //联查余额
			showOriginalData: '',//联查余额

		};
		initTemplate.call(this, props);
	}
	componentDidMount() {
		console.log('--------------------','审批联查');
		this.refresh();
	}
	//加载刷新数据
	refresh = () => {
		this.toggleShow();//切换页面状态
		//查询单据详情[浏览卡片]
		if (this.props.getUrlParam('status') === 'browse') {

			if (this.props.getUrlParam('id') &&
				this.props.getUrlParam('id').length > 0) {
				//后台grid只接受pageid。
				let data = { pk: this.props.getUrlParam('id'), pageid: this.pageId };
				ajax({
					url: '/nccloud/cmp/curexchange/curexchangecardquery.do',
					data: data,
					success: (res) => {
						//data要看返回的id，而不是后台设置的id
						if (res.data) {
							this.props.form.setAllFormValue({ [this.formId]: res.data[this.formId] });
							if (res.data[this.formId].rows) {
								//页签赋值
								let billno = res.data[this.formId].rows[0].values.vbillno.value;
								this.setState({
									billno: billno
								});
							}
						} else {
							this.props.form.EmptyAllFormValue(this.formId);
						}

					}
				});
			}else{
				toast({ color: 'warning', content: this.props.MutiInit.getIntl("36070FCEAPP") && this.props.MutiInit.getIntl("36070FCEAPP").get('36070FCEAPP-000011') });/* 国际化处理： 操作失败，无数据!*/
				return;
			}

		}
	}
	//关闭审批意见页面
	closeApprove = () => {
		this.setState({
			show: false
		})
	}
	// 附件的关闭点击
	onHideUploader = () => {
		this.setState({
			showUploader: false
		})
	}
	//切换页面状态
	toggleShow = () => {
		let status = this.props.getUrlParam('status');
		let flag = status === 'browse' ? false : true;
		this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', !flag);//设置看片翻页的显隐性
		if (!this.props.getUrlParam('id') || this.props.getUrlParam('id').length <= 0) {
			//不存在id就隐藏翻页工具
			this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false);//设置看片翻页的显隐性
		}
		if (status === 'browse') {
			this.props.form.setFormStatus(this.formId, status);
		} else {
			this.props.form.setFormStatus(this.formId, 'edit');
		}
		buttonVisable(this.props);//按钮的显隐性
	};

	getButtonNames = (codeId) => {
		if (codeId === 'cancelBtn' || codeId === 'savesubmitBtn' || codeId === 'saveBtn' || codeId === 'cardCopyBtn' || codeId === 'cardAddBtn'
			|| codeId === 'cardSubmitBtn' || codeId === 'cardUnsubmitBtn' || codeId === 'cardEditBtn' || codeId === 'cardDeleteBtn'
			|| codeId === 'cardSettleBtn' || codeId === 'cardUnsettleBtn') {
			return 'main-button'
		} else {
			return 'secondary - button'
		}
	};
	//卡片返回按钮
	handleClick = () => {
		//先跳转列表
	}
	render() {
		let { cardTable, form, button, ncmodal, cardPagination } = this.props;
		let buttons = this.props.button.getButtons();
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		let { createForm } = form;
		let { createCardPagination } = cardPagination;
		let { createButtonApp } = this.props.button;
		let { createCardTable } = cardTable;
		let { createButton } = button;
		let { createModal } = ncmodal;
		let { showUploader, target, billno, billId } = this.state;//附件相关内容变量
		return (

			<div className="nc-bill-extCard">
				<NCAffix>
					<Div areaCode={Div.config.HEADER}>
						<div className="nc-bill-header-area">
							<div className="header-title-search-area">
								<h2 className='title-search-detail'>
									{false && <NCBackBtn onClick={this.handleClick}></NCBackBtn>}
									{this.props.MutiInit.getIntl("36070FCEAPP") &&
										this.props.MutiInit.getIntl("36070FCEAPP").get('36070FCEAPP-000042')}{' : '}{this.state.billno}
								</h2>{/* 国际化处理： 外币兑换*/}
							</div>
							<div className="header-button-area">
								{createButtonApp({
									area: Templatedata.card_head,
									buttonLimit: 6,
									onButtonClick: buttonClick.bind(this)
								})}
							</div>
						</div>
					</Div>
				</NCAffix>
				<NCScrollElement name='forminfo'>
					<div className="nc-bill-form-area">
						{createForm(this.formId, {
							expandArr: [jsondata.form1, jsondata.form2, 
								jsondata.form3, jsondata.form4, jsondata.form5
								, jsondata.form6],
							onAfterEvent: afterEvent.bind(this)
						})}
					</div>
				</NCScrollElement>
				<NCScrollElement name='businfo'>

				</NCScrollElement>
				<div className="nc-faith-demo-div2">
					{/* 这里是附件上传组件的使用，需要传入三个参数 */}
					{showUploader &&
						<NCUploader
							billId={billId}
							target={target}
							placement={'bottom'}
							billNo={billno}
							onHide={this.onHideUploader}
						/>
					}
				</div>
				{/* 审批意见 */}
				<div>
					<ApproveDetail
						show={this.state.show}
						close={this.closeApprove}
						billtype={this.state.billtype}
						billid={this.state.billid}
					/>
				</div>
				{/* 打印输出 */}
				<div>
					<PrintOutput
						ref="printOutput"
						url='/nccloud/cmp/curexchange/curexchangeprint.do'
						data={this.state.outputData}
						callback={this.onSubmit}
					/>
				</div>
					{/* 联查余额 */}
					<div>
					<NCCOriginalBalance
						showmodal={this.state.showOriginal}
						showOriginalData={this.state.showOriginalData}
						// 点击确定按钮的回调函数
						onSureClick={() => {
							//关闭对话框
							this.setState({
								showOriginal: false
							})
						}}
						onCloseClick={() => {
							//关闭对话框
							this.setState({
								showOriginal: false
							})
						}}
					>
					</NCCOriginalBalance>

				</div>
			</div>

		);
	}
}
Card = createPage({
	mutiLangCode: '36070FCEAPP',
	billinfo: {
		billtype: 'form',
		pagecode: Templatedata.card_pageid,
		headcode: Templatedata.card_formid
	}
})(Card);
// ReactDOM.render(<Card />, document.querySelector('#app'));
export default Card;
