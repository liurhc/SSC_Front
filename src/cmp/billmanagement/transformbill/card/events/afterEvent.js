import { ajax, promptBox } from 'nc-lightapp-front';
import { constant, requesturl } from '../../config/config';
import { commondata } from '../../../../public/utils/constant';
export default function afterEvent(props, moduleId, key, value, changedrows, i, s, g) {

	let cpagecode = constant.cpagecode;
	let formcode1 = constant.formcode1;

	// 财务组织
	if (key === 'pk_org') {

		let changedata = props.createFormAfterEventData(cpagecode, formcode1);

		let newvalue = props.form.getFormItemsValue(moduleId, 'pk_org').value;
		let oldvalue = changedrows.value;
		let oldorgDis = changedrows.display;

		if (oldvalue == null) {
			ajax({
				url: requesturl.orgchange,
				data: changedata,
				success: (res) => {
					if (res.success) {

						let olcrate = res.data[formcode1].rows[0].values.olcrate.value;
						let sourceflag = res.data[formcode1].rows[0].values.sourceflag.value;
						if (olcrate) {
							props.form.setFormItemsDisabled(moduleId, {
								'olcrate': true
							});
						}
						props.form.setAllFormValue({
							[formcode1]: res.data[formcode1]
						});
						// props.form.getFormItemsValue();
						if(sourceflag){
							// this.sourceflag(sourceflag);
						}
					}
				}
			});
		}

		if (oldvalue != newvalue && oldvalue != null) {
			this.setState({
				oldorg: oldvalue,
				oldorgDis: oldorgDis
			});
			promptBox({
				color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
				title: this.state.json['36070TBR-000017'], // 弹框表头信息/* 国际化处理： 确认修改*/
				content: this.state.json['36070TBR-000018'], //this.modalContent(), //弹框内容，可以是字符串或dom/* 国际化处理： 是否修改组织，这样会清空您录入的信息?*/
				// noFooter: false,                // 是否显示底部按钮(确定、取消),默认显示(false),非必输
				// noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
				// beSureBtnName: "确定",          // 确定按钮名称, 默认为"确定",非必输
				// cancelBtnName: "取消",         // 取消按钮名称, 默认为"取消",非必输
				// hasCloseBtn:false,             //显示“X”按钮，默认不显示，不显示是false，显示是true
				beSureBtnClick: this.orgBeSureBtnClick.bind(this),   // 确定按钮点击调用函数,非必输
				cancelBtnClick: this.orgCancelBtnClick.bind(this),  // 取消按钮点击调用函数,非必输
				// closeBtnClick:functionClose, //关闭按钮点击调用函数，非必输
				// closeByClickBackDrop:false,//点击遮罩关闭提示框，默认是false点击不关闭,点击关闭是true
			})
		}

		if(newvalue){
			props.resMetaAfterPkorgEdit(); //选择主组织以后，恢复其他字段的编辑性
		}
	}

	// 币种
	if (key === 'pk_currtype') {
		props.form.setFormItemsDisabled(moduleId, {
			'olcrate': false
		});

		// 清空账户和银行
		// 划出银行
		props.form.setFormItemsValue(moduleId, {
			'transformoutbank': {
				display: null,
				value: null
			}
		}); 
		// 划出账户
		props.form.setFormItemsValue(moduleId, {
			'transformoutaccount': {
				display: null,
				value: null
			}
		});
		// 划入银行
		props.form.setFormItemsValue(moduleId, {
			'transforminbank': {
				display: null,
				value: null
			}
		});
		// 划入账户
		props.form.setFormItemsValue(moduleId, {
			'transforminaccount': {
				display: null,
				value: null
			}
		});

		let changedata = props.createFormAfterEventData(cpagecode, formcode1);
		let currency = props.form.getFormItemsValue(moduleId, 'pk_currtype').value;
		if (currency) {
			ajax({
				url: requesturl.currencychange,
				data: changedata,
				success: (res) => {
					if (res.success) {
						let olcrate, pkcurrency
						if (res.data[formcode1].rows[0].values.olcrate) {
							olcrate = res.data[formcode1].rows[0].values.olcrate.value;
						}

						if (res.data.userjson) {
							pkcurrency = res.data.userjson;
						}
						if (changedata.form[formcode1].rows[0].values.pk_currtype.value === pkcurrency) {
							props.form.setFormItemsDisabled(moduleId, {
								'olcrate': true
							});
						}
						props.form.setAllFormValue({ [formcode1]: res.data[formcode1] });
					}
				}
			});
		} else {
			props.form.setFormItemsValue(moduleId, {
				olcrate: {
					display: null,
					value: null
				}
			});
		}
	}

	// 组织本地汇率
	if (key === 'olcrate') {
		let olcrate = props.form.getFormItemsValue(moduleId, 'olcrate').value;
		let amount = props.form.getFormItemsValue(moduleId, 'amount').value;
		if (olcrate && amount) {
			let olcrateevent = props.createFormAfterEventData(cpagecode, formcode1);
			ajax({
				url: requesturl.oclmoneyevent,
				data: olcrateevent,
				success: (res) => {
					if(res.data){
						this.props.form.setAllFormValue({ [formcode1]: res.data[formcode1] });
					}
				}
			});

		}
	}

	if (key === 'amount') {
		let olcrate = props.form.getFormItemsValue(moduleId, 'olcrate').value;
		let amount = props.form.getFormItemsValue(moduleId, 'amount').value;
		if (olcrate && amount) {
			let amountevent = props.createFormAfterEventData(cpagecode, formcode1);
			ajax({
				url: requesturl.oclmoneyevent,
				data: amountevent,
				async:false, // 同步
				success: (res) => {
					if(res.data){
						this.props.form.setAllFormValue({ [formcode1]: res.data[formcode1] });
					}
				}
			});
		}
	}
	// 划出银行
	if (key === 'transformoutbank') {
		props.form.setFormItemsValue(moduleId, {
			'transformoutaccount': {
				display: null,
				value: null
			}
		});
	}

	// 划出账户
	if (key === 'transformoutaccount') {

		let transformoutaccount = props.form.getFormItemsValue(moduleId, 'transformoutaccount');
		let transformoutbank = props.form.getFormItemsValue(moduleId, 'transformoutbank');
		let bankdocname, pk_bankdoc, accname, accpk
		accname = i.refcode;
		accpk = i.refpk;
		props.form.setFormItemsValue(moduleId, {
			'transformoutaccount': {
				display: accname,
				value: accpk
			}
		});

		if(transformoutaccount.value && !transformoutaccount.value == ''){
			if (transformoutbank.value == '' || transformoutbank.value == null) {
				if (i) {
					bankdocname = i.values['bd_bankdoc.name'].value;
					pk_bankdoc = i.values['bd_bankdoc.pk_bankdoc'].value;
					props.form.setFormItemsValue(moduleId, {
						'transformoutbank': {
							display: bankdocname,
							value: pk_bankdoc
						}
					});
				}
			}
		}
	}

	// 划入银行
	if (key === 'transforminbank') {
		props.form.setFormItemsValue(moduleId, {
			'transforminaccount': {
				display: null,
				value: null
			}
		});
	}

	// 划入账户
	if (key === 'transforminaccount') {
		let transforminaccount = props.form.getFormItemsValue(moduleId, 'transforminaccount');
		let transforminbank = props.form.getFormItemsValue(moduleId, 'transforminbank');
		let bankdocname, pk_bankdoc, inaccname, inaccpk
		inaccname = i.refcode;
		inaccpk = i.refpk;
		props.form.setFormItemsValue(moduleId, {
			'transforminaccount': {
				display: inaccname,
				value: inaccpk
			}
		});

		if(transforminaccount.value && !transforminaccount.value == ''){

			let bd_bankaccsubcode = i.refcode;
			let bd_bankaccsubname = i.refname;
			props.form.setFormItemsValue(moduleId, {
				'inaccount_name': {
					value: bd_bankaccsubname
				}
			});
			props.form.setFormItemsValue(moduleId, {
				'inaccount_num': {
					value: bd_bankaccsubcode
				}
			});

			if (transforminbank.value == '' || transforminbank.value == null) {
				if (i) {
					bankdocname = i.values['bd_bankdoc.name'].value;
					pk_bankdoc = i.values['bd_bankdoc.pk_bankdoc'].value;
	
					props.form.setFormItemsValue(moduleId, {
						'transforminbank': {
							display: bankdocname,
							value: pk_bankdoc
						}
					});
				}
			}
		}
	}

}
