import buttonClick from './buttonClick';
import initTemplate from './initTemplate';
import afterEvent from './afterEvent';
import pageInfoClick from './pageInfoClick';
import { constant, requesturl } from '../../config/config';
import { orgVersionUtil } from '../../config/orgVersionUtil';
import { saveBtn,saveaddBtn,savesubmitBtn } from '../btnClicks/btnClick';
export { buttonClick, afterEvent, initTemplate, pageInfoClick, constant, orgVersionUtil,saveBtn,saveaddBtn,savesubmitBtn };
