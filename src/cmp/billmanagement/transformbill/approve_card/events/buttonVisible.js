import {
    ajax,
    base,
    toast,
    NCMessage,
    cacheTools
} from 'nc-lightapp-front';
import {
    constant,
    requesturl
} from '../../config/config';

export const buttonVisible = function (props) {

    let status = props.getUrlParam('status');
    let btnflag = false;

    // 默认全部按钮隐藏
    props.button.setButtonVisible(
        [
            'imgBtn',
            'imggroup',
            'imgreviewBtn',
            'imgscanBtn',
            'enclosureBtn',
            'joinquery',
            'joinquerygroup',
            'querybillBtn',
            'voucherBtn',
            'inaccbalanceBtn',
            'outaccbalanceBtn',
            'printBtn',
            'printgroup',
            'outputBtn'
        ],
        btnflag
    );
    //浏览
    if (status === 'browse') {
        props.button.setButtonVisible(
            [
                'imgBtn',
                'imggroup',
                'imgreviewBtn',
                'imgscanBtn',
                'enclosureBtn',
                'joinquery',
                'joinquerygroup',
                'querybillBtn',
                'voucherBtn',
                'inaccbalanceBtn',
                'outaccbalanceBtn',
                'printBtn',
                'printgroup',
                'outputBtn'
            ], !btnflag);
    }
    props.form.setFormStatus(constant.formcode1, status);
    props.form.setFormStatus(constant.formcode4, "browse");
}