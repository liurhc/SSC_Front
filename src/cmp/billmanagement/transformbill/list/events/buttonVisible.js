import {
    cardCache
} from 'nc-lightapp-front';
let {
    getDefData
} = cardCache;
import {
    constant,
} from '../../config/config';

export const buttonVisible = function (props) {
    let isfiplink = getDefData(constant.fipscene_key, this.cacheDataSource);
    if (isfiplink) {
        props.button.setButtonVisible(
            [
                'btngroup',
                'addBtn',
                'deleteBtn',
                'copyBtn',
                'submitBtn',
                'submitgroup',
                'unsubmitBtn',
                'settleBtn',
                'settlegroup',
                'unsettleBtn',
                'entrustBtn',
                'entrustgroup',
                'unentrustBtn',
                'paymentgroupBtn',
                'paymentgroup',
                'cyberbankeditBtn',
                'onlinepaymentBtn',
                'updatecyberbankBtn',
                'img',
                'imgreviewBtn',
                'imgscanBtn',
                'redhandleBtn',
                'refreshBtn'
            ],
            false
        );
    }

}