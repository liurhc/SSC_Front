//主子表卡片
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast, cacheTools, high } from 'nc-lightapp-front';
import { buttonClick, initTemplate, afterEvent, pageInfoClick } from './events';
import { jsondata } from "./jsondata";
import { Templatedata } from "../config/Templatedata";
let { NCFormControl, NCAnchor, NCScrollLink, NCScrollElement, NCAffix } = base;
const { Inspection } = high;//联查计划预算
const { NCUploader } = high;//附件相关
const { ApproveDetail } = high;//审批详情相关
const { BillTrack } = high;//联查单据
//联查使用参数
let cacheTools_paybill_key = Templatedata.cacheTools_paybill_key;
let cacheTools_informer_key = Templatedata.cacheTools_informer_key;
let cacheTools_informer_src = Templatedata.cacheTools_informer_src;
let cacheTools_paybill_src = Templatedata.cacheTools_paybill_src;

class Card extends Component {
	constructor(props) {
		super(props);
		this.formId = Templatedata.card_formid;
		this.searchId = Templatedata.list_searchid;
		this.moduleId = Templatedata.list_moduleid;
		this.tableId = Templatedata.card_tableid;
		this.pageId = Templatedata.card_pageid;
		this.state = {
			showInspection: false,//联查预算
			sourceData: null,//联查预算数据源
			showbilltrack: false,//联查单据
			showbilltrackpk: '',//联查单据pk
			showbilltracktype: '',//联查单据类型
			show: false,//审批意见是否显示
			billid: '',//审批意见单据pk
			billtype: '',//审批意见单据类型
			billno: '', // 单据编号
			billId: '',//单据pk
			showUploader: false,//控制附件弹出框
			target: null,//控制弹出位置
			tradebtnflag: true,
			tradetype: 'D4',
			tradename: this.props.MutiInit.getIntl("36070RBMLINK") && this.props.MutiInit.getIntl("36070RBMLINK").get('36070RBMLINK-000013'),/* 国际化处理： 收款结算单*/
			tradepk: '0000Z6000000000000F4'
		};
		initTemplate.call(this, props);
	}
	componentDidMount() {
		//被联查处理信息
		let link_src = this.props.getUrlParam("src");
		/**
		 * 联查
		 */
		if (link_src && link_src.length > 0) {
			this.getLinkquery();
			this.toggleShow();
		} else {
			this.refresh();
		}
	}

	//被联查入口
	getLinkquery = () => {
		//联查数据状态
		let Status = this.props.setUrlParam('status');
		if (Status && Status == 'browse') {
			this.props.cardTable.setStatus(this.tableId, 'browse');
		}
		//联查来源
		let link_src_2 = this.props.getUrlParam("src");

		//联查1：付款结算单联查
		if (link_src_2 && link_src_2 == cacheTools_paybill_src) {
			//联查处理
			let paybillsData = cacheTools.get(cacheTools_paybill_key);
			console.log(paybillsData, this.props.MutiInit.getIntl("36070RBMLINK") && this.props.MutiInit.getIntl("36070RBMLINK").get('36070RBMLINK-000014'));/* 国际化处理： 付款结算*/
			if (paybillsData && paybillsData.length > 0) {
				this.getLinkQueryData(paybillsData);
			}
		}
		//联查2：到账通知联查
		if (link_src_2 && link_src_2 == cacheTools_informer_src) {
			//联查处理
			let informerData = cacheTools.get(cacheTools_informer_key);
			console.log(informerData, this.props.MutiInit.getIntl("36070RBMLINK") && this.props.MutiInit.getIntl("36070RBMLINK").get('36070RBMLINK-000015'));/* 国际化处理： 到账通知*/
			if (informerData && informerData.length > 0) {

				this.getLinkQueryData(informerData);
			}
		}


	}
	GetQuery = (query) => {
		let theRequest = {};
		if (query.indexOf('?') != -1) {
			let str = query.substr(1);
			if (str.indexOf('&') != -1) {
				let strs = str.split('&');
				for (let i = 0; i < strs.length; i++) {
					theRequest[strs[i].split('=')[0]] = strs[i].split('=')[1];
				}
			} else {
				theRequest[str.split('=')[0]] = str.split('=')[1];
			}
		}
		return theRequest;
	};
	//联查单据
	getLinkQueryData = (searchData) => {
		//测试数据
		// let testArr = ["1001G5100000000016SC", "1001G5100000000017B2", "1001G510000000001JK6"];
		console.log(searchData);
		let sendArr = {
			'pks': searchData,
			'pageid': this.pageId
		}
		ajax({
			url: '/nccloud/cmp/recbill/recbilllinkbill.do',
			data: sendArr,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if (res.data) {
						if (res.data.head) {

							this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
							//页签赋值
							let billno = res.data.head[this.formId].rows[0].values.bill_no.value;
							let source_flag = res.data.head[this.formId].rows[0].values.source_flag.value;
							this.setState({
								billno: billno

							});
							this.source_flag(source_flag);
							// this.props.form.setFormItemsValue(this.formId, { 'source_flag': { display: '现金管理', value: source_flag } });
						}
						if (res.data.body) {
							this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
						}

					} else {
						// this.props.form.setAllFormValue({ [this.formId]: { rows: [] } });
						// this.props.cardTable.setTableData(this.tableId, { rows: [] });
					}

				}
			}
		});
	}
	//处理来源系统显示值
	source_flag = (source_flag) => {

		if (source_flag == '2') {
			this.props.form.setFormItemsValue(this.formId, { 'source_flag': { display: this.props.MutiInit.getIntl("36070RBMLINK") && this.props.MutiInit.getIntl("36070RBMLINK").get('36070RBMLINK-000016'), value: source_flag } });/* 国际化处理： 现金管理*/
		} else if (source_flag == '5') {
			this.props.form.setFormItemsValue(this.formId, { 'source_flag': { display: this.props.MutiInit.getIntl("36070RBMLINK") && this.props.MutiInit.getIntl("36070RBMLINK").get('36070RBMLINK-000017'), value: source_flag } });/* 国际化处理： 资金结算*/
		} else if (source_flag == '6') {
			this.props.form.setFormItemsValue(this.formId, { 'source_flag': { display: this.props.MutiInit.getIntl("36070RBMLINK") && this.props.MutiInit.getIntl("36070RBMLINK").get('36070RBMLINK-000018'), value: source_flag } });/* 国际化处理： 网上银行*/
		} else if (source_flag == '8') {
			this.props.form.setFormItemsValue(this.formId, { 'source_flag': { display: this.props.MutiInit.getIntl("36070RBMLINK") && this.props.MutiInit.getIntl("36070RBMLINK").get('36070RBMLINK-000019'), value: source_flag } });/* 国际化处理： 票据管理*/
		} else if (source_flag == '9') {
			this.props.form.setFormItemsValue(this.formId, { 'source_flag': { display: this.props.MutiInit.getIntl("36070RBMLINK") && this.props.MutiInit.getIntl("36070RBMLINK").get('36070RBMLINK-000020'), value: source_flag } });/* 国际化处理： 协同单据*/
		} else if (source_flag == '104') {
			this.props.form.setFormItemsValue(this.formId, { 'source_flag': { display: this.props.MutiInit.getIntl("36070RBMLINK") && this.props.MutiInit.getIntl("36070RBMLINK").get('36070RBMLINK-000021'), value: source_flag } });/* 国际化处理： 资产管理*/
		} else if (source_flag == '105') {
			this.props.form.setFormItemsValue(this.formId, { 'source_flag': { display: this.props.MutiInit.getIntl("36070RBMLINK") && this.props.MutiInit.getIntl("36070RBMLINK").get('36070RBMLINK-000022'), value: source_flag } });/* 国际化处理： 网上报销*/
		}


	}
	//加载刷新数据
	refresh = () => {
		this.toggleShow();
		//查询单据详情
		if (this.props.getUrlParam('status') === 'edit' ||
			this.props.getUrlParam('status') === 'browse') {
			if (!this.props.getUrlParam('id')) {
				return;
			}
			let data = { pk: this.props.getUrlParam('id'), pageid: this.pageId };
			let that = this;
			ajax({
				url: '/nccloud/cmp/recbill/recbillquerycard.do',
				data: data,
				success: (res) => {
					if (res.data) {
						if (res.data.head) {
							//页面渲染数据
							this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
							//页签赋值
							let billno = res.data.head[this.formId].rows[0].values.bill_no.value;
							let source_flag = res.data.head[this.formId].rows[0].values.source_flag.value;
							this.setState({
								billno: billno

							});
							this.source_flag(source_flag);
							// this.props.form.setFormItemsValue(this.formId, { 'source_flag': { display: '现金管理', value: source_flag } });
						}
						if (res.data.body) {
							this.props.cardTable.setTableData(that.tableId, res.data.body[that.tableId]);
							// this.props.cardTable.setEditableByKey(that.tableId, '0', 'local_rate', false);


						}
					} else {
						this.props.form.setAllFormValue({ [this.formId]: { rows: [] } });
						this.props.cardTable.setTableData(this.tableId, { rows: [] });

					}
					if (this.props.getUrlParam('status') === 'edit') {
						//设置组织不可以编辑
						this.props.form.setFormItemsDisabled(this.formId, { 'pk_org': true });
					}
				}
			});
		}
		//复制
		if (this.props.getUrlParam('status') === 'copy') {

			// /清空表单form所有数据
			this.props.form.EmptyAllFormValue(this.formId);
			this.setState({
				billno: ''

			});
			if (!this.props.getUrlParam('id')) {
				return;
			}
			let data = { pk: this.props.getUrlParam('id'), pageid: this.pageId };
			let that = this;
			ajax({
				url: '/nccloud/cmp/recbill/recbillcopy.do',
				data: data,
				success: (res) => {
					if (res.data) {
						if (res.data.head) {
							this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
						}
						if (res.data.body) {
							this.props.cardTable.setTableData(that.tableId, res.data.body[that.tableId]);

						}
					} else {
						this.props.form.setAllFormValue({ [this.formId]: { rows: [] } });
						this.props.cardTable.setTableData(this.tableId, { rows: [] });

					}

					//设置组织不可以编辑
					this.props.form.setFormItemsDisabled(this.formId, { 'pk_org': true });
				}
			});
		}
		//新增
		if (this.props.getUrlParam('status') === 'add') {

			// /清空表单form所有数据
			this.props.form.EmptyAllFormValue(this.formId);
			this.setState({
				billno: ''

			});
			//清空table所有数据
			this.props.cardTable.setTableData(this.tableId, { rows: [] });
			let data = { pk: this.tableId, pageid: this.pageId };
			let that = this;
			ajax({
				url: '/nccloud/cmp/recbill/recbilladdevent.do',
				data: data,
				success: (res) => {
					if (res.data) {
						if (res.data.head) {
							this.props.initMetaByPkorg();//单据有主组织，新增时,将其他字段设置为不可编辑. 
							this.props.form.setFormItemsDisabled(this.formId, { 'pk_org': false });//组织可以进行编辑
							this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
							//把所有form中字段不可以编辑，知道选择org之后
							//仅仅组织可以进行编辑
							// this.props.form.setFormItemsDisabled(this.formId,{ 'pk_currtype': true,'bill_date':true,'pk_balatype':true
							// ,'pk_account':true,'mon_account':true,'note_type':true,'note_no':true,'objecttype':true,'pk_customer':true,'pk_oppaccount':true});
						}
						if (res.data.body) {
							this.props.cardTable.setTableData(that.tableId, res.data.body[that.tableId]);
							// this.props.cardTable.setEditableByIndex(that.tableId, 0, 'pk_currtype', false);
							//把所有table中字段不可以编辑，知道选择org之后
							this.props.cardTable.setStatus(that.tableId, 'browse');

						}
					} else {
						this.props.form.setAllFormValue({ [this.formId]: { rows: [] } });
						this.props.cardTable.setTableData(this.tableId, { rows: [] });

					}
				}
			});
		}
	}
	//切换页面状态
	toggleShow = () => {

		console.log("buttons", this.props.button.getButtons());
		let status = this.props.getUrlParam('status');
		let billstatus = this.props.getUrlParam('billno');//获取单据状态
		this.props.cardTable.setStatus(this.tableId, 'edit');
		let flag = status === 'browse' ? false : true;
		this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', !flag);//设置看片翻页的显隐性
		this.props.form.setFormStatus(this.formId, status);
		if (status == 'browse') {
			this.props.cardTable.setStatus(this.tableId, 'browse');
		} else {
			this.props.cardTable.setStatus(this.tableId, 'edit');

		}

		if (status != 'browse') {
			//新增or修改or复制：保存，保存提交，保存新增，取消，附件
			this.props.button.setButtonVisible(['addBtn', 'editBtn', 'deleteBtn'
				, 'copyBtn', 'subimtBtn', 'unsubmitBtn', 'rectradetypeBtn', 'linksettleBtn'
				, 'imagegroup', 'moreoperateBtn'
				, 'editBtn', 'subimtBtn', 'unsubmitBtn'], false);
			//保存，保存提交，保存新增，取消，附件
			this.props.button.setButtonVisible(['saveBtn', 'savesubmitBtn', 'saveaddBtn'
				, 'cancelBtn', 'annexBtn', 'addbodyBtn', 'deletebodyBtn', 'copybodyBtn', 'openBtn',
				'copylineBtn', 'addlineBtn', 'deletelineBtn', 'editmoreBtn'], true);

		} else {
			//浏览态状态过滤	
			if (billstatus && billstatus === '-1') {
				//待审批
				this.props.button.setButtonVisible(['saveBtn', 'savesubmitBtn', 'saveaddBtn'
					, 'cancelBtn', 'annexBtn', 'subimtBtn', 'editBtn', 'deleteBtn'
					, 'addbodyBtn', 'deletebodyBtn'
					, 'copybodyBtn'], false);
				//新增，复制，收款交易类型，关联结算信息，收回，影像，更多
				this.props.button.setButtonVisible(['addBtn', 'copyBtn', 'rectradetypeBtn'
					, 'unsubmitBtn', 'imagegroup', 'moreoperateBtn'], true);

			} else if (billstatus && billstatus === '-99') {
				//暂存态
				this.props.button.setButtonVisible(['saveBtn', 'savesubmitBtn', 'saveaddBtn'
					, 'cancelBtn', 'annexBtn', 'subimtBtn', 'addbodyBtn', 'deletebodyBtn', 'copybodyBtn'
					, 'unsubmitBtn'], false);
				//新增，复制，收款交易类型，关联结算信，影像，更多	
				this.props.button.setButtonVisible(['addBtn', 'copyBtn', 'rectradetypeBtn'
					, 'imagegroup', 'moreoperateBtn'], true);
			} else if (billstatus && billstatus === '8') {
				//签字态
				this.props.button.setButtonVisible(['saveBtn', 'savesubmitBtn', 'saveaddBtn'
					, 'cancelBtn', 'annexBtn', 'subimtBtn', 'editBtn', 'deleteBtn'
					, 'addbodyBtn', 'deletebodyBtn', 'copybodyBtn'
					, 'unsubmitBtn'], false);
				//新增，复制，收款交易类型，关联结算信，影像，更多	
				this.props.button.setButtonVisible(['addBtn', 'copyBtn', 'rectradetypeBtn'
					, 'imagegroup', 'moreoperateBtn'], true);
			} else if (billstatus && billstatus === '1') {
				//审批通过
				this.props.button.setButtonVisible(['saveBtn', 'savesubmitBtn', 'saveaddBtn'
					, 'cancelBtn', 'annexBtn', 'subimtBtn', 'editBtn', 'deleteBtn'
					, 'addbodyBtn', 'deletebodyBtn', 'copybodyBtn'
					, 'unsubmitBtn'], false);
				//新增，复制，收款交易类型，关联结算信，影像，更多	
				this.props.button.setButtonVisible(['addBtn', 'copyBtn', 'rectradetypeBtn'
					, 'imagegroup', 'moreoperateBtn'], true);
			} else if (billstatus && billstatus === '2') {
				//审批中
				this.props.button.setButtonVisible(['saveBtn', 'savesubmitBtn', 'saveaddBtn'
					, 'cancelBtn', 'annexBtn', 'subimtBtn', 'editBtn', 'deleteBtn'
					, 'addbodyBtn', 'deletebodyBtn', 'copybodyBtn'
					, 'unsubmitBtn'], false);
				//新增，复制，收款交易类型，关联结算信，影像，更多	
				this.props.button.setButtonVisible(['addBtn', 'copyBtn', 'rectradetypeBtn'
					, 'imagegroup', 'moreoperateBtn'], true);
			} else if (billstatus && billstatus === '0') {
				//审批失败
				this.props.button.setButtonVisible(['saveBtn', 'savesubmitBtn', 'saveaddBtn'
					, 'cancelBtn', 'annexBtn', 'subimtBtn', 'deleteBtn'
					, 'addbodyBtn', 'deletebodyBtn', 'copybodyBtn'
					, 'unsubmitBtn'], false);
				//新增，修改，复制，收款交易类型，关联结算信，影像，更多	
				this.props.button.setButtonVisible(['addBtn', 'copyBtn', 'editBtn', 'rectradetypeBtn'
					, 'imagegroup', 'moreoperateBtn'], true);
			} else if (billstatus && billstatus === '9') {
				//未确认
				this.props.button.setButtonVisible(['saveBtn', 'savesubmitBtn', 'saveaddBtn'
					, 'cancelBtn', 'annexBtn', 'subimtBtn', 'editBtn', 'deleteBtn'
					, 'addbodyBtn', 'deletebodyBtn', 'copybodyBtn'
					, 'unsubmitBtn'], false);
				//新增，复制，收款交易类型，关联结算信，影像，更多	
				this.props.button.setButtonVisible(['addBtn', 'copyBtn', 'rectradetypeBtn'
					, 'imagegroup', 'moreoperateBtn'], true);
			} else if (billstatus && billstatus === '-10') {
				//保存态
				this.props.button.setButtonVisible(['saveBtn', 'savesubmitBtn', 'saveaddBtn'
					, 'cancelBtn', 'annexBtn', 'addbodyBtn', 'deletebodyBtn', 'copybodyBtn'
					, 'unsubmitBtn'], false);
				//新增，修改，删除复制，收款交易类型，关联结算信息，收回，影像，更多
				this.props.button.setButtonVisible(['addBtn', , 'editBtn', 'deleteBtn', 'copyBtn', 'rectradetypeBtn'
					, 'subimtBtn', 'imagegroup', 'moreoperateBtn'], true);
			}

		}


	};
	//删除单据
	delConfirm = () => {

		let data = {
			'pk': this.props.getUrlParam('id')
		};

		ajax({
			url: '/nccloud/cmp/recbill/recbilldelete.do',
			data: data,
			success: (res) => {
				if (res.success) {
					toast({ color: 'success', content: this.props.MutiInit.getIntl("36070RBMLINK") && this.props.MutiInit.getIntl("36070RBMLINK").get('36070RBMLINK-000023') });/* 国际化处理： 删除成功*/
					this.props.linkTo('/cmp/billmanagement/recbilllink/list/index.html');
				}
			}
		});
	};

	changeOrgConfirm = () => {
		//组织
		let pk_org_val = this.props.form.getFormItemsValue(this.formId, 'pk_org').value;
		let pk_org_dly = this.props.form.getFormItemsValue(this.formId, 'pk_org').display;
		//恢复之前的值，设置edit状态
		this.props.form.cancel(this.formId);
		this.props.form.setFormStatus(this.formId, 'edit');
		//table清楚之前的状态
		this.props.cardTable.resetTableData(this.tableId);
		this.props.cardTable.setStatus(this.tableId, 'edit');
		let org_data = this.props.createHeadAfterEventData(this.pageId, this.formId, this.tableId, this.formId, 'pk_org', this.value);
		ajax({
			url: '/nccloud/cmp/recbill/recbillorgafterevent.do',
			data: org_data,
			success: (res) => {
				if (res.success) {
					if (res.data) {
						if (res.data.head) {
							//设置form的编辑属性

							this.props.resMetaAfterPkorgEdit();//选择主组织以后，恢复其他字段的编辑性
							//组织本币币种
							let currtype = res.data.head[this.formId].rows[0].values.pk_currtype.value;
							let currtypedly = res.data.head[this.formId].rows[0].values.pk_currtype.display;
							//页面渲染,不能用这种方式，否则的话无法设置form和table的编辑性
							// props.form.setAllFormValue({ [moduleId]: res.data.head[moduleId] });
							//查询获取的币种汇率
							let re_local_rate_form = res.data.head[this.formId].rows[0].values.local_rate.value;
							let re_local_money_from = res.data.head[this.formId].rows[0].values.local_money.value;



							if (pk_org_dly && pk_org_val) {
								this.props.form.setFormItemsValue(this.formId, { 'pk_org': { display: pk_org_dly, value: pk_org_val } });

							}

							if (re_local_rate_form) {//汇率
								re_local_rate_form = (re_local_rate_form * 1).toFixed(5);
								this.props.form.setFormItemsValue(this.formId, { 'local_rate': { display: re_local_rate_form, value: re_local_rate_form } });
								let totolnum = this.props.cardTable.getNumberOfRows(this.tableId);
								this.props.cardTable.setValByKeyAndIndex(this.tableId, 0, 'local_rate', { value: re_local_rate_form, display: re_local_rate_form });//给表体字段赋值

								let table_rec_primal = this.props.cardTable.getValByKeyAndIndex(this.tableId, 0, 'rec_primal');//组织原币金额
								if (table_rec_primal && table_rec_primal.value) {
									//本币金额
									let result_value = (table_rec_primal.value * re_local_rate_form).toFixed(2);
									this.props.cardTable.setValByKeyAndIndex(this.tableId, 0, 'rec_local', { value: result_value, display: result_value });//给表体字段赋值
								}
							}


						}
						if (res.data.body) {
							this.props.cardTable.setStatus(this.tableId, 'edit');

						}
					}

				}
			}
		});

	}
	//保存单据
	saveBill = () => {
		if (this.props.getUrlParam('copyFlag') === 'copy') {
			this.props.form.setFormItemsValue(this.formId, { crevecontid: null });
			this.props.form.setFormItemsValue(this.formId, { ts: null });
		}

		//过滤表格空行
		// this.props.cardTable.filterEmptyRows(this.tableId);
		let isCheckTable = this.props.cardTable.getValByKeyAndIndex(this.tableId, 0, 'rec_primal');//收款原币金额
		if (isCheckTable && isCheckTable.value) {

		} else {
			toast({ color: 'warning', content: this.props.MutiInit.getIntl("36070RBMLINK") && this.props.MutiInit.getIntl("36070RBMLINK").get('36070RBMLINK-000024') });/* 国际化处理： 收款金额未填写*/
			return;
		}
		let flag = this.props.form.isCheckNow(this.formId);
		let s_tableflag = this.props.cardTable.getCheckedRows(this.tableId);
		if (flag && s_tableflag) {

			let CardData = this.props.createMasterChildData(this.pageId, this.formId, this.tableId);
			let url = '/nccloud/cmp/recbill/recbillinsert.do'//新增保存
			if (this.props.getUrlParam('status') === 'edit') {
				url = '/nccloud/cmp/recbill/recbillupdate.do'//修改保存
			}
			ajax({
				url: url,
				data: CardData,
				success: (res) => {
					let pk_paybill = null;
					if (res.success) {
						if (res.data) {
							toast({ color: 'success', content: this.props.MutiInit.getIntl("36070RBMLINK") && this.props.MutiInit.getIntl("36070RBMLINK").get('36070RBMLINK-000000') });/* 国际化处理： 保存成功*/

							let pk_recbill = res.data.head[this.formId].rows[0].values.pk_recbill.value;
							let billstatue = res.data.head[this.formId].rows[0].values.bill_status.value;
							// window.location.href = "/cmp/billmanagement/recbill/card#status=browse&id=" + pk_recbill + "&billno=" + billstatue;
							this.props.linkTo('/cmp/billmanagement/recbilllink/card/index.html', {
								status: 'browse',
								id: pk_recbill,
								billno: billstatue,
								pagecode: this.pageId
							})
							this.componentDidMount();
						}
					}

				}
			});
		}
	};

	getButtonNames = (codeId) => {
		if (codeId === 'edit' || codeId === 'add' || codeId === 'save') {
			return 'main-button';
		} else {
			return 'secondary - button';
		}
	};

	//获取列表肩部信息,肩部按钮
	getTableHead = (buttons, tableId) => {
		let { createButton } = this.props.button;
		return (
			<div className="shoulder-definition-area">

				<div className="definition-icons">
					{this.props.cardTable.createBrowseIcons(tableId, {
						iconArr: ['close', 'open', 'max'],
						maxDestAreaId: 'finance-fts-commissionpayment-card'
					})}
					{/* 应用注册按钮 */}
					{this.props.button.createButtonApp({
						area: Templatedata.card_body,
						buttonLimit: 3,
						onButtonClick: buttonClick.bind(this),
						popContainer: document.querySelector('.header-button-area')
					})}

					{/*	{buttons.map((v) => {
						if (v.btncode == 'addline') {
							return createButton(v.btncode, {
								name: v.btnname,
								onButtonClick: buttonClick.bind(this),
								buttonColor: this.getButtonNames(v.btncode)
							});
						}
					})}
					{buttons.map((v) => {
						if (v.btncode == 'delline') {
							return createButton(v.btncode, {
								name: v.btnname,
								onButtonClick: buttonClick.bind(this),
								buttonColor: this.getButtonNames(v.btncode)
							});
						}
					})}
					{buttons.map((v) => {
						if (v.btncode == 'copyline') {
							return createButton(v.btncode, {
								name: v.btnname,
								onButtonClick: buttonClick.bind(this),
								buttonColor: this.getButtonNames(v.btncode)
							});
						}
					})} */}


				</div>
			</div>
		);
	};

	render() {
		let { cardTable, form, button, modal, cardPagination, editTable } = this.props;
		let buttons = this.props.button.getButtons();
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createCardPagination } = cardPagination;
		let { createEditTable } = editTable;
		let { createButton, createButtonApp } = button;
		let { createModal } = modal;
		let { showUploader, target, billno, billId } = this.state;//附件相关内容变量
		return (
			<div className="nc-bill-card">
				<NCAnchor>
					<NCScrollLink
						to='forminfo'
						spy={true}
						smooth={true}
						duration={300}
						offset={-100}
					>
						<p>
							{this.props.MutiInit.getIntl("36070RBMLINK") &&
								this.props.MutiInit.getIntl("36070RBMLINK").get('36070RBMLINK-000029')}
						</p>{/* 国际化处理： 表头信息*/}
					</NCScrollLink>
					<NCScrollLink
						to='businfo'
						spy={true}
						smooth={true}
						duration={300}
						offset={-100}
					>
						<p>
							{this.props.MutiInit.getIntl("36070RBMLINK") &&
								this.props.MutiInit.getIntl("36070RBMLINK").get('36070RBMLINK-000030')}
						</p>{/* 国际化处理： 表尾信息*/}
					</NCScrollLink>
				</NCAnchor>
				<NCAffix>
					<div className="nc-bill-header-area">
						<div className="header-title-search-area"><h2 className='title-search-detail'>
							{this.props.MutiInit.getIntl("36070RBMLINK") &&
								this.props.MutiInit.getIntl("36070RBMLINK").get('36070RBMLINK-000031')}
							:{this.state.billno}</h2></div>{/* 国际化处理： 收款结算*/}
						{/* <div className='header-cardPagination-area' style={{ float: 'right' }}>{createCardPagination({
						handlePageInfoChange: pageInfoClick.bind(this)
					})}</div> */}
						<div className="header-button-area">
							{/* 按钮适配 第三步:在页面的 dom 结构中创建按钮组，传入显示的区域，绑定按钮事件*/}
							{/* {createButtonApp("page_header", {onButtonClick: buttonClick.bind(this) })} */}
							{this.props.button.createButtonApp({
								area: Templatedata.card_head,
								buttonLimit: 10,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}

						</div>
					</div>
				</NCAffix>
				<NCScrollElement name='forminfo'>
					<div className="nc-bill-form-area">
						{createForm(this.formId, {
							expandArr: [jsondata.form1],
							onAfterEvent: afterEvent.bind(this)
						})}
					</div>
				</NCScrollElement>
				<NCScrollElement name='businfo'>
					<div className="nc-bill-table-area">
						{this.getTableHead(buttons, this.tableId)}
						{createCardTable(this.tableId, {
							tableHead: this.getTableHead.bind(this, buttons, this.tableId),
							modelSave: this.saveBill,
							onAfterEvent: afterEvent.bind(this),
							showCheck: true
						})}
					</div>
				</NCScrollElement>
				{createModal('delete', {
					title: this.props.MutiInit.getIntl("36070RBMLINK") && this.props.MutiInit.getIntl("36070RBMLINK").get('36070RBMLINK-000025'),/* 国际化处理： 删除确认*/
					content: this.props.MutiInit.getIntl("36070RBMLINK") && this.props.MutiInit.getIntl("36070RBMLINK").get('36070RBMLINK-000026'),/* 国际化处理： 你确定要删除吗?*/
					beSureBtnClick: this.delConfirm
				})}
				{createModal('changeorg', {
					title: this.props.MutiInit.getIntl("36070RBMLINK") && this.props.MutiInit.getIntl("36070RBMLINK").get('36070RBMLINK-000027'),/* 国际化处理： 确认修改*/
					content: this.props.MutiInit.getIntl("36070RBMLINK") && this.props.MutiInit.getIntl("36070RBMLINK").get('36070RBMLINK-000028'),/* 国际化处理： 是否修改组织，这样会清空您录入的信息?*/
					beSureBtnClick: this.changeOrgConfirm
				})}
				<div className="nc-faith-demo-div2">
					{/* 这里是附件上传组件的使用，需要传入三个参数 */}
					{showUploader &&
						<NCUploader
							billId={billId}
							target={target}
							placement={'bottom'}
							billNo={billno}
						/>
					}
				</div>
				{/* 审批意见 */}
				<div>
					<ApproveDetail
						show={this.state.show}
						close={this.closeApprove}
						billtype={this.state.billtype}
						billid={this.state.billid}
					/>
				</div>
				{/* 联查单据 */}
				<div>
					<BillTrack
						show={this.state.showbilltrack}
						close={() => {
							this.setState({ showbilltrack: false })
						}}
						pk={this.state.showbilltrackpk}  //单据id
						type={this.state.showbilltracktype}  //单据类型
					/>
				</div>

				{/* 联查计划预算 */}
				<div>
					<Inspection
						show={this.state.showInspection}
						sourceData={this.state.sourceData}
						cancel={() => {
							this.setState({ showInspection: false })
						}}
						affirm={() => {
							this.setState({ showInspection: false })
						}}
					/>
				</div>
			</div>
		);
	}
}

Card = createPage({
	mutiLangCode: Templatedata.list_moduleid
})(Card);

ReactDOM.render(<Card />, document.querySelector('#app'));
