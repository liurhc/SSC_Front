import {ajax} from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";//配置的id和area信息

let page_id=Templatedata.card_pageid;

export default function (props, pks) {
    console.log(pks);
    // 后台还没更新，暂不可用
    if(pks){
        let dataArr = [];
        dataArr.push(pks);//主键数组
        let data = {
            "pks": dataArr,
            "pageid": page_id
        };
        ajax({
            url: '/nccloud/cmp/recbill/recbillquerycardbyid.do',
            data: data,
            success: (res) => {
                if (res.data) {
                    let billno ='';
                    let urlbillno = '';
                    if (res.data.head) {
                         billno = res.data.head[this.formId].rows[0].values.bill_no.value;
                         urlbillno = res.data.head[this.formId].rows[0].values.bill_status.value;
                        this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
                    }
                    if (res.data.bodys) {
                        this.props.cardTable.setTableData(this.tableId, res.data.bodys[this.tableId]);
                       
                    }
                    // props.setUrlParam(pks)//动态修改地址栏中的id的值
                     // window.location.href = "/cmp/billmanagement/recbill/card#status=browse&id="+pks+"&billno="+urlbillno;
                    props.linkTo('/cmp/billmanagement/recbilllink/card/index.html', {
                        status: 'browse',
                        id: pks,
                        billno: urlbillno,
                        pagecode:this.pageId
                    });
                    this.toggleShow();
                   
                    this.setState({
                        billno: billno

                    });
                    
                } else {
                    this.props.form.setAllFormValue({ [this.formId]: { rows: [] } });
                    this.props.cardTable.setTableData(this.tableId, { rows: [] });
                    
                }
            }
        });
    }
   
}
