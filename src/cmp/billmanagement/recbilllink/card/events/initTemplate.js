import { base, ajax } from 'nc-lightapp-front';
import tableButtonClick from './tableButtonClick';
import { Templatedata } from "../../config/Templatedata";//配置的id和area信息
// 先引入参照，进行表格参照过滤
import BankaccSubDefaultGridTreeRef from '../../../../../uapbd/refer/pub/BankaccSubDefaultGridTreeRef';
import CashAccountGridRef from '../../../../../uapbd/refer/sminfo/CashAccountGridRef';
import CustBankAccGridRef from '../../../../../uapbd/refer/pub/CustBankAccGridRef';
import Bill4CmpReceiveGridRef from '../../../../../fbm/refer/fbm/Bill4CmpReceiveGridRef';
import FundPlanTreeRef from '../../../../../uapbd/refer/fiacc/FundPlanTreeRef';
import InoutBusiClassTreeRef from '../../../../../uapbd/refer/fiacc/InoutBusiClassTreeRef';	
const formId = Templatedata.card_formid;
const tableId = Templatedata.card_tableid;
const pageId = Templatedata.card_pageid;
const moudleId = Templatedata.list_moduleid;
let { NCPopconfirm } = base;
export default function (props) {
	let self = this;
	props.createUIDom(
		{
			pagecode: pageId,//页面id
			appid: Templatedata.card_appid//注册按钮的id
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					modifierMeta.call(self,props, meta)
					props.meta.setMeta(meta);

					// if (props.getUrlParam('status') == 'browse') {
					// 	props.cardTable.setStatus(tableId, 'browse');
					// } else {
					// 	props.cardTable.setStatus(tableId, 'edit');
					// 	//  props.cardTable.setEditableByIndex(tableId, 0, 'pk_currtype', false);
					// }

				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				//修改页面状态
				togglePageShow(props);

			}
		}
	)
}

//根据页面状态，修改编辑态表格
function togglePageShow(props) {

	let status = props.getUrlParam('status');
	let billstatus = props.getUrlParam('billno');//获取单据状态
	let flag = status === 'browse' ? false : true;

	if (status != 'browse') {
		//新增or修改or复制：保存，保存提交，保存新增，取消，附件
		props.button.setButtonVisible(['addBtn', 'editBtn', 'deleteBtn'
			, 'copyBtn', 'subimtBtn', 'unsubmitBtn', 'rectradetypeBtn', 'linksettleBtn'
			, 'imagegroup', 'moreoperateBtn', 'printgroup', 'annexgroup',
			, 'editBtn', 'subimtBtn', 'unsubmitBtn',''], false);
		//保存，保存提交，保存新增，取消，附件
		props.button.setButtonVisible(['saveBtn', 'savesubmitBtn', 'saveaddBtn'
			, 'cancelBtn', 'annexBtn','moreoperateBtn','annexgroup','printgroup','addbodyBtn','deletebodyBtn','copybodyBtn','openBtn',
			'copylineBtn','addlineBtn','deletelineBtn','editmoreBtn'], true);
	} else {
		//浏览态状态过滤	
		if (billstatus && billstatus === '-1') {
			//待审批
			props.button.setButtonVisible(['saveBtn', 'savesubmitBtn', 'saveaddBtn'
				, 'cancelBtn', 'annexBtn', 'subimtBtn', 'editBtn', 'deleteBtn'
				, 'addbodyBtn', 'deletebodyBtn'
				, 'copybodyBtn'], false);
			//新增，复制，收款交易类型，关联结算信息，收回，影像，更多
			props.button.setButtonVisible(['addBtn', 'copyBtn', 'rectradetypeBtn'
				, 'unsubmitBtn', 'imagegroup', 'moreoperateBtn'], true);

		} else if (billstatus && billstatus === '-99') {
			//暂存态
			props.button.setButtonVisible(['saveBtn', 'savesubmitBtn', 'saveaddBtn'
				, 'cancelBtn', 'annexBtn', 'subimtBtn', 'addbodyBtn', 'deletebodyBtn', 'copybodyBtn'
				, 'unsubmitBtn'], false);
			//新增，复制，收款交易类型，关联结算信，影像，更多	
			props.button.setButtonVisible(['addBtn', 'copyBtn', 'rectradetypeBtn'
				, 'imagegroup', 'moreoperateBtn'], true);
		} else if (billstatus && billstatus === '8') {
			//签字态
			props.button.setButtonVisible(['saveBtn', 'savesubmitBtn', 'saveaddBtn'
				, 'cancelBtn', 'annexBtn', 'subimtBtn', 'editBtn', 'deleteBtn'
				, 'addbodyBtn', 'deletebodyBtn', 'copybodyBtn'
				, 'unsubmitBtn'], false);
			//新增，复制，收款交易类型，关联结算信，影像，更多	
			props.button.setButtonVisible(['addBtn', 'copyBtn', 'rectradetypeBtn'
				, 'imagegroup', 'moreoperateBtn'], true);
		} else if (billstatus && billstatus === '1') {
			//审批通过
			props.button.setButtonVisible(['saveBtn', 'savesubmitBtn', 'saveaddBtn'
				, 'cancelBtn', 'annexBtn', 'subimtBtn', 'editBtn', 'deleteBtn'
				, 'addbodyBtn', 'deletebodyBtn', 'copybodyBtn'
				, 'unsubmitBtn'], false);
			//新增，复制，收款交易类型，关联结算信，影像，更多	
			props.button.setButtonVisible(['addBtn', 'copyBtn', 'rectradetypeBtn'
				, 'imagegroup', 'moreoperateBtn'], true);
		} else if (billstatus && billstatus === '2') {
			//审批中
			props.button.setButtonVisible(['saveBtn', 'savesubmitBtn', 'saveaddBtn'
				, 'cancelBtn', 'annexBtn', 'subimtBtn', 'editBtn', 'deleteBtn'
				, 'addbodyBtn', 'deletebodyBtn', 'copybodyBtn'
				, 'unsubmitBtn'], false);
			//新增，复制，收款交易类型，关联结算信，影像，更多	
			props.button.setButtonVisible(['addBtn', 'copyBtn', 'rectradetypeBtn'
				, 'imagegroup', 'moreoperateBtn'], true);
		} else if (billstatus && billstatus === '0') {
			//审批失败
			props.button.setButtonVisible(['saveBtn', 'savesubmitBtn', 'saveaddBtn'
				, 'cancelBtn', 'annexBtn', 'subimtBtn', 'deleteBtn'
				, 'addbodyBtn', 'deletebodyBtn', 'copybodyBtn'
				, 'unsubmitBtn'], false);
			//新增，修改，复制，收款交易类型，关联结算信，影像，更多	
			props.button.setButtonVisible(['addBtn', 'copyBtn', 'editBtn', 'rectradetypeBtn'
				, 'imagegroup', 'moreoperateBtn'], true);
		} else if (billstatus && billstatus === '9') {
			//未确认
			props.button.setButtonVisible(['saveBtn', 'savesubmitBtn', 'saveaddBtn'
				, 'cancelBtn', 'annexBtn', 'subimtBtn', 'editBtn', 'deleteBtn'
				, 'addbodyBtn', 'deletebodyBtn', 'copybodyBtn'
				, 'unsubmitBtn'], false);
			//新增，复制，收款交易类型，关联结算信，影像，更多	
			props.button.setButtonVisible(['addBtn', 'copyBtn', 'rectradetypeBtn'
				, 'imagegroup', 'moreoperateBtn'], true);
		} else if (billstatus && billstatus === '-10') {
			//保存态
			props.button.setButtonVisible(['saveBtn', 'savesubmitBtn', 'saveaddBtn'
				, 'cancelBtn', 'annexBtn', 'addbodyBtn', 'deletebodyBtn', 'copybodyBtn'
				, 'unsubmitBtn'], false);
			//新增，修改，删除复制，收款交易类型，关联结算信息，收回，影像，更多
			props.button.setButtonVisible(['addBtn', , 'editBtn', 'deleteBtn', 'copyBtn', 'rectradetypeBtn'
				, 'subimtBtn', 'imagegroup', 'moreoperateBtn'], true);
		}

	}
}

function modifierMeta(props, meta) {
	let status = props.getUrlParam('status');
	//参照过滤-现金账户
	//条件根据pk_org进行过滤
	meta[formId].items.map((item) => {
		if (item.attrcode == 'mon_account') {
			item.queryCondition = () => {
				let mon_account_org_value = props.form.getFormItemsValue(formId, 'pk_org').value;
				let mon_account_currtype_value = props.form.getFormItemsValue(formId, 'pk_currtype').value;
				return {
					pk_org: mon_account_org_value,
					pk_currtype: mon_account_currtype_value,
					GridRefActionExt: 'nccloud.web.cmp.ref.CMPCashAccountDefaultBuilder'//自定义增加的过滤条件-现金账户
				};
			};
		}
	});
	//table中现金账户
	meta[tableId].items.map((item) => {
		if (item.attrcode == 'mon_account') {
			item.render = function (text, record, index) {
				return (
					CashAccountGridRef({
						queryCondition: () => {
							let cash_org_data = props.form.getFormItemsValue(formId, 'pk_org').value;
							let cash_curr_type = props.cardTable.getValByKeyAndIndex(tableId, index, 'pk_currtype');
							if (cash_curr_type && cash_curr_type.value) {
								cash_curr_type = cash_curr_type.value;
							} else {
								cash_curr_type = null;
							}
							return {
								pk_org: cash_org_data, // 这里对record.values.materiel要做一下非空校验
								pk_currtype: cash_curr_type,
								GridRefActionExt: 'nccloud.web.cmp.ref.CMPCashAccountDefaultBuilder'//自定义增加的过滤条件-现金账户
							};
						}
					})
				);
			}
		}

	});
	//参照过滤--供应商档案
	meta[formId].items.map((item) => {
		if (item.attrcode == 'pk_supplier') {
			item.queryCondition = () => {
				let pk_supplier_data = props.form.getFormItemsValue(formId, 'pk_group').value;
				return {
					pk_org: pk_supplier_data
					// ExtRefSqlBuilder: 'nccloud.web.cmp.ref.CMPRecBillSupplierGridRefSqlBuilder'//自定义参照过滤条件
				};
			};
		}
	});
	//付款银行账户过滤
	//objecttype（交易对象类型）
	//参数为queryCondition中的accclass ，1=客户银行账户，2=客商银行账户，3=供应商银行账户
	meta[formId].items.map((item) => {
		if (item.attrcode == 'pk_oppaccount') {
			item.queryCondition = () => {

				// let form_orgdata = props.form.getFormItemsValue(formId, 'pk_org').value;
				//客户档案
				let pk_customer = props.form.getFormItemsValue(formId, 'pk_customer').value;
				let pk_customer_dly = props.form.getFormItemsValue(formId, 'pk_customer').display;
				let pk_cust = null;
				if (pk_customer && pk_customer_dly) {
					pk_cust = pk_customer;
				}
				//供应商档案
				let pk_supplier = props.form.getFormItemsValue(formId, 'pk_supplier').value;
				let pk_supplier_dly = props.form.getFormItemsValue(formId, 'pk_supplier').display;
				let acclass = null;//默认显示客户
				//对象交易类型
				let objecttype = props.form.getFormItemsValue(formId, 'objecttype').value;
				//0表示客户
				if (objecttype == 0) {
					acclass = 1;
				}

				return {
					pk_cust: pk_cust,
					accclass: acclass,
					GridRefActionExt: 'nccloud.web.cmp.ref.CMPRecBillCustBankAccGridRefSqlBuilder'//自定义参照过滤条件
				};
			};
		}
	});
	//table中付款银行账户
	meta[tableId].items.map((item) => {
		if (item.attrcode == 'pk_oppaccount') {
			item.render = function (text, record, index) {
				return (
					CustBankAccGridRef({
						queryCondition: () => {

							// let form_orgdata = props.form.getFormItemsValue(formId, 'pk_org').value;
							//客户档案
							let parent_cus = props.cardTable.getValByKeyAndIndex(tableId, index, 'pk_customer');
							let pk_tab_customer = null;
							if (parent_cus && parent_cus.value) {
								pk_tab_customer = parent_cus.value;
							}
							let pk_tab_cust = null;
							if (pk_tab_customer != null) {
								pk_tab_cust = pk_tab_customer;
							}
							//供应商档案
							let pk_tb_supplier = props.form.getFormItemsValue(formId, 'pk_supplier').value;
							let pk_tb_supplier_dly = props.form.getFormItemsValue(formId, 'pk_supplier').display;
							let tb_acclass = null;//默认显示客户
							//对象交易类型
							let tb_par_obj = props.cardTable.getValByKeyAndIndex(tableId, index, 'objecttype');
							let tb_objecttype = null;
							if (tb_par_obj && tb_par_obj.value) {
								tb_objecttype = tb_par_obj.value;
							}

							//0表示客户
							if (tb_objecttype == 0) {
								tb_acclass = 1;
							}
							//供应商没有做处理
							return {
								pk_cust: pk_tab_cust,
								accclass: tb_acclass,
								GridRefActionExt: 'nccloud.web.cmp.ref.CMPRecBillCustBankAccGridRefSqlBuilder'//自定义参照过滤条件
							};
						}
					})
				);
			}
		}

	});
	//收款银行账号过滤
	//过滤条件：币种+组织form
	meta[formId].items.map((item) => {
		if (item.attrcode == 'pk_account') {
			item.queryCondition = () => {
				let org_value = props.form.getFormItemsValue(formId, 'pk_org');
				if (org_value && org_value.value) {
					org_value = org_value.value;
				} else {
					org_value = null;
				}
				let cur_type = props.form.getFormItemsValue(formId, 'pk_currtype');
				if (cur_type && cur_type.value) {
					cur_type = cur_type.value;
				} else {
					cur_type = null;
				}
				return {
					pk_org: org_value,
					pk_currtype: cur_type,
					GridRefActionExt: 'nccloud.web.cmp.ref.CMPRecBillBankaccSubDefaultGridRefSqlBuilder'//自定义增加的过滤条件
				};
			};
		}
	});
	//过滤条件：币种+组织table
	meta[tableId].items.map((item) => {
		if (item.attrcode == 'pk_account') {
			item.render = function (text, record, index) {
				return (
					BankaccSubDefaultGridTreeRef({
						queryCondition: () => {

							let tableorg_data = props.form.getFormItemsValue(formId, 'pk_org');
							if (tableorg_data && tableorg_data.value) {
								tableorg_data = tableorg_data.value;
							} else {
								tableorg_data = null;
							}
							let tab_pk_currtype = props.cardTable.getValByKeyAndIndex(tableId, index, 'pk_currtype');
							if (tab_pk_currtype && tab_pk_currtype.value) {
								tab_pk_currtype = tab_pk_currtype.value;
							} else {
								tab_pk_currtype = null;
							}

							return {
								pk_currtype: tab_pk_currtype,
								pk_org: tableorg_data,
								GridRefActionExt: 'nccloud.web.cmp.ref.CMPRecBillBankaccSubDefaultGridRefSqlBuilder'//自定义增加的过滤条件
							};
						}
					})
				);
			}
		}
	});
	//票据号过滤:组织+币种+票据类型
	meta[formId].items.map((item) => {
		if (item.attrcode == 'note_no') {
			item.queryCondition = () => {
				let note_no_org_value = props.form.getFormItemsValue(formId, 'pk_org').value;//所选组织
				let note_no_curr_value = props.form.getFormItemsValue(formId, 'pk_currtype').value;//币种
				let note_type_value = props.form.getFormItemsValue(formId, 'note_type').value;//票据类型

				return {
					pk_org: note_no_org_value,
					pk_currtype: note_type_value,
					pk_curr: note_no_curr_value
				};
			};
		}
	});
	//table中票据号过滤
	meta[tableId].items.map((item) => {
		if (item.attrcode == 'note_no') {
			item.render = function (text, record, index) {
				return (
					Bill4CmpReceiveGridRef({
						queryCondition: () => {
							let tb_note_no_data = props.form.getFormItemsValue(formId, 'pk_org').value;
							let tb_note_curr_type = props.cardTable.getValByKeyAndIndex(tableId, index, 'pk_currtype');
							let tb_note_no_type = props.cardTable.getValByKeyAndIndex(tableId, index, 'note_type');

							if (tb_note_curr_type && tb_note_curr_type.value) {
								tb_note_curr_type = tb_note_curr_type.value;
							}
							if (tb_note_no_type && tb_note_no_type.value) {
								tb_note_no_type = tb_note_no_type.value;
							}

							return {
								pk_org: tb_note_no_data,
								pk_currtype: tb_note_no_type,
								pk_curr: tb_note_curr_type
							};
						}
					})
				);
			}
		}

	});
	//table中资金项目计划
	meta[tableId].items.map((item) => {
		if (item.attrcode == 'bankroll_projet') {
			item.render = function (text, record, index) {
				return (
					FundPlanTreeRef({
						queryCondition: () => {
							let bankroll_projet_org = props.form.getFormItemsValue(formId, 'pk_org').value;
							let bankroll_projet_group = props.form.getFormItemsValue(formId, 'pk_group').value;
							return {
								pk_org: bankroll_projet_org,
								pk_group: bankroll_projet_group
							};
						}
					})
				);
			}
		}

	});
	//table中收支项目
	meta[tableId].items.map((item) => {
		if (item.attrcode == 'pk_recproject') {
			item.render = function (text, record, index) {
				return (
					InoutBusiClassTreeRef({
						queryCondition: () => {
							let pk_recproject_org = props.form.getFormItemsValue(formId, 'pk_org').value;
							let pk_recproject_group = props.form.getFormItemsValue(formId, 'pk_group').value;
							return {
								pk_org: pk_recproject_org
							};
						}
					})
				);
			}
		}

	});
	let multiLang = props.MutiInit.getIntl(moudleId);
	let porCol = {
		attrcode: 'opr',
		label: this.props.MutiInit.getIntl("36070RBMLINK") && this.props.MutiInit.getIntl("36070RBMLINK").get('36070RBMLINK-000012'),/* 国际化处理： 操作*/
		fixed: 'right',
		itemtype: 'customer',
		visible: true,
		width: '200px',
		render(text, record, index) {
			let buttonAry =
				props.getUrlParam("status") === "browse"
					? ["openBtn"]
					: ["openBtn", 'editmoreBtn', "copylineBtn", "addlineBtn", "deletelineBtn"];
			return props.button.createOprationButton(buttonAry, {
				area: Templatedata.card_body_inner,
				buttonLimit: 3,
				onButtonClick: (props, key) => tableButtonClick(props, key, text, record, index)
			});

		}
	};
	meta[tableId].items.push(porCol);

	return meta;
}
