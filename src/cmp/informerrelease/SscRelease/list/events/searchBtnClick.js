import {ajax ,toast} from 'nc-lightapp-front';
//点击查询，获取查询区数据
export default function clickSearchBtn(props,searchVal) { 
    if(searchVal){
        let searchVal = this.props.search.getAllSearchData(this.searchId);
        let pageInfo = props.table.getTablePageInfo(this.tableId);
		
		//动态获取oid
        if (this.props.meta.getMeta()[this.searchId].oid) {
            this.oid = this.props.meta.getMeta()[this.searchId].oid;
		}

        let data={
            querycondition: searchVal,
            custcondition: {
				logic: 'or', //逻辑操作符，and、or
				conditions:[]
			},
            pageInfo:pageInfo, 
            pagecode: '36070AIPSSC_L01',
            queryAreaCode:'search',  //查询区编码
            oid: this.oid,  //查询模板id，手工添加在界面模板json中，放在查询区，后期会修改
            querytype:'tree'
        };
        ajax({
            url: '/nccloud/cmp/release/sscquery.do',
            data: data,
            success: (res) => {
                let { success, data } = res;
                if (success) {
                    if(data){
                        this.props.table.setAllTableData(this.tableId, data[this.tableId]);
                        toast({ color: 'success' });
                    }else{
                        this.props.table.setAllTableData(this.tableId, { rows: [] });              
                        toast({ color: 'warning', content: this.props.MutiInit.getIntl("36070AIPSSC") && this.props.MutiInit.getIntl("36070AIPSSC").get('36070AIPSSC--000003') });
                    }
                }
            }
        });
    }
    
};