import searchBtnClick from './searchBtnClick';
import initTemplate from './initTemplate';
import pageInfoClick from './pageInfoClick';
import buttonClick from './buttonClick';
import afterEvent from './afterEvent';
import { buttonVisible } from "./buttonVisible";
export { searchBtnClick, pageInfoClick,initTemplate,buttonClick,afterEvent,buttonVisible};