
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, getMultiLang, toast, createPageIcon } from 'nc-lightapp-front';
const { NCTabs, NCModal, NCRadio, } = base;
const NCTabPane = NCTabs.NCTabPane;
import { buttonClick, initTemplate, searchBtnClick, pageInfoClick, tableModelConfirm, afterEvent, buttonVisible } from './events';
import * as CONSTANTS from './constants';
import '../css/index.less';
let { tableId, searchId, pagecode, moduleId, oid, formId_01, MODULE_ID } = CONSTANTS;

class SettlementCenterList extends Component {
	constructor(props) {
		super(props);
		this.moduleId = MODULE_ID;
		this.searchId = searchId;
		this.tableId = tableId;
		this.pagecode = pagecode;
		this.oid = oid;
		this.state = {
			showModal_publish: false,
			showModal_finance_01: false
		};
		this.close_publish = this.close_publish.bind(this);
		this.open_publish = this.open_publish.bind(this);

		initTemplate.call(this, props)
	}
	//关闭发布弹窗
	close_publish() {
		this.setState({ showModal_publish: false });
	}
	//打开发布弹窗
	open_publish() {
		this.setState({ showModal_publish: true });
	}

	componentWillMount() {
		let callback = (json) => {
			initTemplate.call(this, this.props);
		};
		getMultiLang({ moduleId: this.moduleId, domainName: 'cmp', callback });
	}

	//刷新
	refresh = () => {
		let pageInfo = this.props.table.getTablePageInfo(this.tableId);
		let refreshsearchVal = this.props.search.getAllSearchData(searchId);
		if (refreshsearchVal == false) {
			return;
		}

		//动态获取oid
		if (this.props.meta.getMeta()[this.searchId].oid) {
			this.oid = this.props.meta.getMeta()[this.searchId].oid;
		}

		let data = {
			querycondition: refreshsearchVal,
			pageInfo: pageInfo,
			pagecode: pagecode,
			queryAreaCode: searchId,  //查询区编码
			oid: this.oid,  //查询模板id，手工添加在界面模板json中，放在查询区，后期会修改
			querytype: 'tree'
		};
		ajax({
			url: '/nccloud/cmp/release/sscquery.do',
			data: data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if (data) {
						this.props.table.setAllTableData(this.tableId, data[this.tableId]);
						toast({ color: 'success' });
					} else {
						this.props.table.setAllTableData(this.tableId, { rows: [] });
						toast({ color: 'warning', content: this.props.MutiInit.getIntl("36070AIPSSC") && this.props.MutiInit.getIntl("36070AIPSSC").get('36070AIPSSC--000003') });
					}
				}
			}
		});
	}
	render() {
		let { table, button, search, form } = this.props;
		let buttons = this.props.button.getButtons();
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { createButtonApp } = button;
		let { createForm } = form;
		let { createButton } = button;
		return (
			<div className="nc-bill-list">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						{createPageIcon()}
						<h2 className="title-search-detail">{this.props.MutiInit.getIntl("36070AIPSSC") && this.props.MutiInit.getIntl("36070AIPSSC").get('36070AIPSSC--000000')}</h2>
					</div>
					<div className="header-button-area">
						{createButtonApp({
							area: 'list_head',
							buttonLimit: 3,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				<div className="nc-bill-search-area">
					{NCCreateSearch(searchId, {
						clickSearchBtn: searchBtnClick.bind(this),
						defaultConditionsNum: 2, //默认显示几个查询条件
						// showAdvBtn: true
					})}
					{/* {NCCreateSearch(searchId, {
						clickSearchBtn: searchBtnClick.bind(this),
						defaultConditionsNum: 4 //默认显示几个查询条件
					})} */}
				</div>
				<div className="nc-bill-table-area">
					{createSimpleTable(tableId, {
						handlePageInfoChange: pageInfoClick,
						tableModelConfirm: tableModelConfirm,
						showCheck: true,
						showIndex: true,
						onSelected: buttonVisible.bind(this, this.props),
						onSelectedAll: buttonVisible.bind(this, this.props)
					})}
				</div>
				{/* 生单补录信息：付款结算单，收款结算单*/}
				<NCModal show={this.state.showModal_finance_01} onHide={this.close} style={{ height: '268px', width: '520px' }}>
					<NCModal.Header>
						<NCModal.Title>{this.props.MutiInit.getIntl("36070AIPSSC") && this.props.MutiInit.getIntl("36070AIPSSC").get('36070AIPSSC--000001')}</NCModal.Title>
					</NCModal.Header>

					<NCModal.Body className="showModal-publish">
						<div >
							{createForm(formId_01, {
								onAfterEvent: afterEvent.bind(this)
							})}
						</div>
					</NCModal.Body>

					<NCModal.Footer>
						{createButtonApp({
							area: 'finance_01',
							buttonLimit: 3,
							onButtonClick: buttonClick.bind(this)
						})}
					</NCModal.Footer>

				</NCModal>
			</div>

		);
	}
}

SettlementCenterList = createPage({
	//initTemplate: initTemplate,
	mutiLangCode: MODULE_ID
})(SettlementCenterList);

ReactDOM.render(<SettlementCenterList />, document.querySelector('#app'));
