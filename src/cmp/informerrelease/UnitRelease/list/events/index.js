import searchBtnClick from './searchBtnClick';
import initTemplate from './initTemplate';
import pageInfoClick from './pageInfoClick';
import buttonClick from './buttonClick';
import afterEvent from './afterEvent';
export { searchBtnClick, pageInfoClick,initTemplate,buttonClick,afterEvent};