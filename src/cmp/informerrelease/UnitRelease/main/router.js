import { asyncComponent } from 'nc-lightapp-front';
import SettlementCenterList from '../list';

const card = asyncComponent(() => import(/* webpackChunkName: "cmp/informerrelease/UnitRelease/card/card" */'../card'));

const routes = [
	{
		path: '/',
		component: SettlementCenterList,
		exact: true
	},
	{
		path: '/list',
		component: SettlementCenterList
	},
	{
		path: '/card',
		component: card
	}
];

export default routes;
