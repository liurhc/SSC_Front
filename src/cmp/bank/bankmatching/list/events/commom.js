import moment from 'moment';
import { formatMoney, width, resolveColumn } from '../../../commom';
const format = "YYYY-MM-DD";

export function buttonConfig () {
    let { isFullScreen, isShow }= this.state;
    return [
        {content: this.lang('0044'), path: 'switch', show: false},
        {content: this.lang('0014'), path: 'corp', show: !isShow},
        {content: this.lang('0015'), path: 'bank', show: !isShow},
        {content: <i className={`iconfont icon-zui${isFullScreen ? 'xiao' : 'da'}hua`} />, path: 'full', show: false},
        {content: <i className="iconfont icon-shuaxin1" />, path: 'refresh', show: false},
    ];
}

export function bankColumns (data) {
    let list= [
        { 
            title: this.lang('0005'),
            key: 'index', 
            dataIndex: 'index', 
            width: '80px',
            className: 'pleft20', 
            render: (text, record, index) => {
                return (
                    <div>{index=== data.length - 1 ? this.lang('0036') : (index + 1)}</div>
                );
            }
        },
        { 
            title: this.lang('0016'), 
            key: 'date', 
            dataIndex: 'date', 
            width, 
        },
        { 
            title: this.lang('0008'), 
            key: 'explanation', 
            dataIndex: 'explanation', 
            width, 
        },
        { 
            title: this.lang('0013'), 
            key: 'checkStyleName', 
            dataIndex: 'checkStyleName', 
            width, 
        },
        { 
            title: this.lang('0009'), 
            key: 'checkNo', 
            dataIndex: 'checkNo', 
            width, 
        },
        { 
            title: this.lang('0006'), 
            key: 'debitamount', 
            dataIndex: 'debitamount', 
            className: 'money-right', 
            width, 
            render: text => {
                return (
                    <div>{text ? Number(text).formatMoney() : ''}</div>
                );
            } 
        },
        { 
            title: this.lang('0007'), 
            key: 'creditamount', 
            dataIndex: 'creditamount', 
            className: 'money-right', 
            width,  
            render: text => {
                return (
                    <div>{text ? Number(text).formatMoney() : ''}</div>
                );
            } 
        },
        { 
            title: this.lang('0030'), 
            key: 'caved', 
            dataIndex: 'caved', 
            width, 
            render: (text) => {
                return (
                    <div>{text=== true ? this.lang('0032') : text=== false ? this.lang('0033') : ''}</div>
                );
            }
        },
        { 
            title: this.lang('0034'), 
            key: 'batchNumber', 
            dataIndex: 'batchNumber', 
            width, 
        },
        { 
            title: this.lang('0035'), 
            key: 'netBankNumber', 
            dataIndex: 'netBankNumber',
            width, 
        },
    ];
    return resolveColumn(list);
}

export function corpColumns (data) {
    let list= [
        { 
            title: this.lang('0005'),
            key: 'index', 
            dataIndex: 'index', 
            width: '80px',
            className: 'pleft20', 
            render: (text, record, index) => {
                return (
                    <div>{index=== data.length-1 ? this.lang('0036') : (index + 1)}</div>
                );
            }
        },
        { 
            title: this.lang('0016'), 
            key: 'date', 
            dataIndex: 'date',
            width, 
        },
        { 
            title: this.lang('0001'), 
            key: 'pk_corp', 
            dataIndex: 'pk_corp', 
            width, 
        },
        { 
            title: this.lang('0010'), 
            key: 'pk_accLink', 
            dataIndex: 'pk_accLink', 
            width: '200px', 
        },
        { 
            title: this.lang('0011'), 
            key: 'pk_subject', 
            dataIndex: 'pk_subject', 
            width, 
        },
        { 
            title: this.lang('0012'), 
            key: 'pk_ass', 
            dataIndex: 'pk_ass', 
            width, 
        },
        { 
            title: this.lang('0008'), 
            key: 'explanation', 
            dataIndex: 'explanation',
            width, 
        },
        { 
            title: this.lang('0031'), 
            key: 'receiptNo', 
            dataIndex: 'receiptNo',
            width, 
        },
        { 
            title: this.lang('0017'), 
            key: 'checkDate', 
            dataIndex: 'checkDate',
            width, 
        },
        { 
            title: this.lang('0013'), 
            key: 'checkStyleName', 
            dataIndex: 'checkStyleName',
            width, 
        },
        { 
            title: this.lang('0009'), 
            key: 'checkNo', 
            dataIndex: 'checkNo', 
            width, 
        },
        { 
            title: this.lang('0006'), 
            key: 'debitamount', 
            dataIndex: 'debitamount', 
            className: 'money-right', 
            width, 
            render: text => {
                return (
                    <div>{text ? Number(text).formatMoney() : ''}</div>
                );
            } 
        },
        { 
            title: this.lang('0007'), 
            key: 'creditamount', 
            dataIndex: 'creditamount',
            className: 'money-right',
            width,  
            render: text => {
                return (
                    <div>{text ? Number(text).formatMoney() : ''}</div>
                );
            }  
        },
        { 
            title: this.lang('0030'), 
            key: 'caved', 
            dataIndex: 'caved', 
            width, 
            render: (text) => {
                return (
                    <div>{text=== true ? this.lang('0032') : text=== false ? this.lang('0033') : ''}</div>
                );
            }
        },
        { 
            title: this.lang('0034'), 
            key: 'batchNumber', 
            dataIndex: 'batchNumber',
            width, 
        },
        { 
            title: this.lang('0035'), 
            key: 'netBankNumber', 
            dataIndex: 'netBankNumber', 
            width, 
        },
    ];
    return resolveColumn(list);
}

export function list (search) {
    let list= [
        {
            itemtype: 'refer',
            label: this.lang('0001'),
            code: 'm_Pk_Corp',
            required: true,
            show: true,
            config: {
                placeholder: this.lang('0001'),
                refName: this.lang('0001'),
                name: 'm_Pk_Corp',
                queryTreeUrl: '/nccloud/uapbd/org/FinanceOrgAllGroupAllDataRefTree.do',
                refType: 'tree',
                refCode: 'uapbd.refer.org.FinanceOrgAllGroupAllDataTreeRef',
                isMultiSelectedEnabled: false,
                isTreelazyLoad:false,
                queryCondition: {
                    isDataPowerEnable: 'Y',
                    isShowDisabledData:'N', 
                    TreeRefActionExt: 'nccloud.web.cmp.ref.CMPUserPermissionOrgAllGroupBuilder'
                },
                value: {
                    refname: search.m_Pk_CorpName, 
                    refpk: search.m_Pk_Corp
                }
            }
        },
        {
            itemtype: 'refer',
            label: this.lang('0002'),
            code: 'm_Pk_Account',
            required: true,
            show: true,
            config: {
                placeholder: this.lang('0002'),
                refName: this.lang('0002'),
                name: 'm_Pk_Account',
                queryGridUrl: '/nccloud/cmp/refer/CMPContrastAccGridRef.do',
                // columnConfig: [{name: [ this.lang('0003'), this.lang('0004') ], code: [ 'refcode', 'refname' ]}],
                refType: 'grid',
                refCode: 'cmp.refer.bankcontrast.CMPContrastAccGridRef',
                isMultiSelectedEnabled: false,
                value: {
                    refname: search.m_Pk_AccountName, 
                    refpk: search.m_Pk_Account
                },
                disabled: !search.m_Pk_Corp,
                queryCondition: {pkOrgArr: search.m_Pk_Corp},
            }
        },
        {
            itemtype: 'rangepicker',
            label: this.lang('0016'),
            code: 'm_strDate',
            required: true,
            show: search.type=== 'true',
            config: {
                placeholder: this.lang('0016'),
                name: 'm_strDate',
                value: search.m_strDate ? [search.m_strDate, search.m_strEndDate] : []
            }
        },
        {
            itemtype: 'rangepicker',
            label: this.lang('0017'),
            code: 'n_pjdate',
            show: search.type=== 'true',
            config: {
                maxlength: 20,
                placeholder: this.lang('0017'), 
                name: 'n_pjdate',
                value: search.n_pjdate1 ? [search.n_pjdate1, search.n_pjdate2] : []
            }
        },
        {
            itemtype: 'select',
            label: this.lang('0019'),
            code: 'm_blnChecked',
            show: search.type=== 'true',
            config: {
                maxlength: 40,
                placeholder: this.lang('0019'),
                name: 'm_blnChecked',
                value: search.m_blnChecked== null ? undefined : search.m_blnChecked,
                selectValue: this.lang(search.m_blnChecked ? '0021' : '0022')
            },
            options: [
                {
                    display: this.lang('0021'),
                    value: true
                },
                {
                    display: this.lang('0022'),
                    value: false
                },
            ]
        },
        {
            itemtype: 'select',
            label: this.lang('0023'),
            code: 'moneyAspect',
            show: search.type=== 'true',
            config: {
                maxlength: 40,
                placeholder: this.lang('0023'),
                name: 'moneyAspect',
                value: search.moneyAspect=== '-1' ? undefined : search.moneyAspect,
                selectValue: search.moneyAspect=== '-1' ? undefined : this.lang(search.moneyAspect=== '1' ? '0007' : '0006')
            },
            options: [
                {
                    display: this.lang('0006'),
                    value: '0'
                },
                {
                    display: this.lang('0007'),
                    value: '1'
                },
            ]
        },
        {
            itemtype: 'refer',
            label: this.lang('0013'),
            code: 'm_strCheckStyle',
            show: search.type=== 'true',
            config: {
                maxlength: 20,
                placeholder: this.lang('0013'),
                refName: this.lang('0013'),
                name: 'm_strCheckStyle',
                queryGridUrl: '/nccloud/uapbd/sminfo/BalanceTypeGridRef.do',
                // columnConfig: [{name: [ this.lang('0003'), this.lang('0004') ],code: [ 'refcode', 'refname' ]}],
                refType: 'grid',
                refCode: 'uapbd.refer.sminfo.BalanceTypeGridRef',
                isMultiSelectedEnabled: false,
                value: {
                    refname: search.m_strCheckStyleName, 
                    refpk: search.m_strCheckStyle
                }
            }
        },
        {
            itemtype: 'rangenum',
            label: this.lang('0037'),
            code: 'moneyArea',
            show: search.type=== 'true',
            config: {
                maxlength: 20,
                name: 'moneyArea',
                values: [search.moneyArea1, search.moneyArea2]
            }
        },
        {
            itemtype: 'datepicker',
            label: this.lang('0024'),
            code: 'bankEndDate',
            required: true,
            show: search.type!== 'true',
            config: {
                placeholder: this.lang('0024'),
                name: 'bankEndDate',
                value: search.bankEndDate
            }
        },
        {
            itemtype: 'num',
            label: this.lang('0025'),
            code: 'Deadline',
            show: search.type!== 'true',
            config: {
                maxlength: 20,
                placeholder: this.lang('0025'),
                name: 'Deadline',
                scale: 0,
                value: search.Deadline
            }
        },
    ];
    return list.filter(item => item.show);
}

export const searchData1= {
    type: 'true',
    moneyAspect: '-1',
    m_blnChecked: null,
    m_strDate: moment().format('YYYY-MM-01'),
    m_strEndDate: moment().format(format),
}; 

export const searchData2= {
    type: 'false',
    bankEndDate: moment().format(format),
}; 

export function headerConfig (headReceipt, search) {
    return [
        {
            label: this.lang('0001'),
            value: search.m_Pk_CorpName
        },
        {
            label: this.lang('0002'),
            value: search.m_Pk_AccountName
        },
        {
            label: this.lang('0026'),
            value: headReceipt.bankAccCode
        },
    ];
}
