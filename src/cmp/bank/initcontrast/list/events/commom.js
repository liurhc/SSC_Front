import { base } from 'nc-lightapp-front';
import { listOperation, startClick, stopClick, settleClick } from '../../public';
import { showMoney, width, deepClone, setPageProp } from '../../../commom';
const { NCPopconfirm, NCDropdown, NCMenu } = base;

export function list (search) {
    return [
        {
            itemtype: 'refer',
            label: this.lang('0001'),
            code: 'm_pk_corp',
            required: true,
            config: {
                placeholder: this.lang('0001'),
                refName: this.lang('0001'),
                name: 'm_pk_corp',
                queryTreeUrl: '/nccloud/uapbd/org/FinanceOrgAllGroupAllDataRefTree.do',
                refType: 'tree',
                refCode: 'uapbd.refer.org.FinanceOrgAllGroupAllDataTreeRef',
                isMultiSelectedEnabled: false,
                isTreelazyLoad: false,
                queryCondition: {
                    isDataPowerEnable: 'Y',
                    isShowDisabledData:'N', 
                    TreeRefActionExt: 'nccloud.web.cmp.ref.CMPUserPermissionOrgAllGroupBuilder'
                },
                value: {
                    refname: search.m_pk_corpName, 
                    refpk: search.m_pk_corp
                }
            }
        }
    ];
}

export function columns() {
    let { page, size }= this.state.pages;
    return [
        { 
            title: this.lang('0005'),
            key: 'key', 
            dataIndex: 'key',
            className: 'pleft20', 
            width: '80px',
            render: (text, record, index) => {
                return (
                    <div>{(page - 1) * size + index + 1}</div>
                );
            } 
        },
        { 
            title: this.lang('0001'), 
            key: 'orgName', 
            dataIndex: 'orgName', 
            width,
        },
        { 
            title: this.lang('0002'),
            key: 'm_contrastaccountname', 
            dataIndex: 'm_contrastaccountname', 
            width,
            render: (text, record) => {
                return (
                    <div
                        style={{color: '#007ace', cursor: 'pointer'}}
                        onClick= {() => {
                            setPageProp.call(this, 'initcontrastSearch', this.state.searchMap);
                            this.props.pushTo('/card', {
                                status: 'browse',
                                m_pk_corp: record.m_pk_corp,
                                m_pk_contrastaccount: record.m_pk_contrastaccount,
                                pagecode: '3607ACAS_C01',
                            });
                        }}
                    >{text}</div>
                );
            }
        },
        { 
            title: this.lang('0026'),
            key: 'currnetname', 
            dataIndex: 'currnetname',
            width: '100px',  
        },
        { 
            title: this.lang('0027'),
            key: 'm_source', 
            dataIndex: 'm_source', 
            width: '100px',  
            render: (text, record) => {
                return (
                    <div>{text== 1 ? this.lang('0028') : this.lang('0029')}</div>
                );
            } 
        },
        { 
            title: this.lang('0009'),
            key: 'm_startdate', 
            dataIndex: 'm_startdate',
            width: '100px',  
        },
        { 
            title: this.lang('0030'),
            key: 'm_debitamount', 
            dataIndex: 'm_debitamount', 
            className: 'money-right',
            width,
            render: text => {
                return (
                    <div>{ showMoney(text) }</div>
                );
            }
        },
        { 
            title: this.lang('0017'),
            key: 'm_stopdate', 
            dataIndex: 'm_stopdate',  
            width: '100px',  
        },
        { 
            title: this.lang('0008'),
            key: 'operation', 
            dataIndex: 'operation', 
            width: '200px',
            fixed: 'right',
            render: (text, record, index) => {
                let start= record.m_startdate;
                let end= record.m_stopdate;
                let showArr= listOperation.call(this, start, end);
                let dropArr= [];
                if (showArr.length > 3) {
                    dropArr= showArr.slice(2);
                    showArr= showArr.slice(0, 2);
                }
                return (
                    <div className="single-line-and-ellipsis">
                        <span className="opration-wrapper">
                        {
                            showArr.map((item, index) => {
                                return (
                                    <div className="nc-hotkeys-wrapper" style={{display: 'inline-block'}}>
                                        {
                                            ['delete.do', 'cancelstart.do', 'Unsettle.do'].includes(item.path) ?
                                                <NCPopconfirm
                                                    trigger="click"
                                                    placement="top"
                                                    content={this.lang(item.path=== 'delete.do' ? '0063' : item.path=== 'cancelstart.do' ? '0064' : '0066')}
                                                    onClose={btnClick.bind(this, item, record)}
                                                >
                                                    <a className="row-edit-option">
                                                        {item.content}
                                                    </a>
                                                </NCPopconfirm>
                                            :
                                                <a
                                                    className="row-edit-option"
                                                    onClick={btnClick.bind(this, item, record)}
                                                >
                                                    {item.content}
                                                </a>
                                        }
                                    </div>
                                )
                            })
                        }
                        {dropArr.length ? <NCDropdown
                            trigger={['click']}
                            overlay={dropDownMenu.call(this, dropArr, record)}
                            animation="slide-up"
                            overlayClassName="dropdown-component-list"
                            // onVisibleChange={visible => this.setState({visible})}
                            >
                                <a
                                    href="javascript:void(0)"
                                    className="row-more"
                                >
                                    {this.lang('0076')}
                                    {/* {this.state.visible ? (
                                        <i className="iconfont icon-hangcaozuoxiangshang1" />
                                    ) : (
                                        <i className="iconfont icon-hangcaozuoxiala1" />
                                    )} */}
                                </a>
                        </NCDropdown> : null
                        }
                        </span>
                    </div>
                );
            } 
        },
    ];
}

function dropDownMenu (dropArr, record) {
    return <NCMenu
        className='apply-dropdown'
        onClick={items => {
            setTimeout(() => {
                btnClick.call(this, dropArr[items.key], record);
            }, 0)
        }}
    >
        {
            dropArr.map((item, index) => {
                if (['delete.do', 'cancelstart.do', 'Unsettle.do'].includes(item.path)) {
                    return (
                        <NCPopconfirm
                            trigger="click"
                            placement="top"
                            content={this.lang(item.path=== 'delete.do' ? '0063' : item.path=== 'cancelstart.do' ? '0064' : '0066')}
                            onClose={btnClick.bind(this, item, record)}
                        >
                            <span className="u-dropdown-menu-item">
                                {item.content}
                            </span>
                        </NCPopconfirm>
                    );
                } else {
                    return (<NCMenu.Item key={index}>{item.content}</NCMenu.Item>);
                }
            })	
        }
    </NCMenu>;
}

function btnClick (item, record) {
    this.setState({
        listRecord: deepClone(record)
    });
    switch (item.path) {
        case 'edit':
            setPageProp.call(this, 'initcontrastSearch', this.state.searchMap);
            this.props.pushTo('/card', {
                status: item.path,
                m_pk_corp: record.m_pk_corp,
                m_pk_contrastaccount: record.m_pk_contrastaccount,
                pagecode: '3607ACAS_C01',
            });
            break;
        case 'change':
            setPageProp.call(this, 'initcontrastSearch', this.state.searchMap);
            this.props.pushTo('/card', {
                status: item.path,
                m_pk_corp: record.m_pk_corp,
                m_pk_contrastaccount: record.m_pk_contrastaccount,
                pagecode: '3607ACAS_C01',
            });
            break;
        case 'delete.do':
            this.statusOperation(item.path, {m_pk_contrastaccount: record.m_pk_contrastaccount}, item.msg);
            break;
        case 'start.do':
            startClick.call(this, record);
            break;
        case 'stop.do':
            stopClick.call(this, record);
            break;
        case 'settle.do':
            settleClick.call(this, record);
            break;
        case 'cancelstart.do':
            this.statusOperation('start.do', {
                m_startdate: record.m_startdate,
                actiontype: '2',
                m_pk_corp: record.m_pk_corp,
                m_pk_contrastaccount: record.m_pk_contrastaccount,
                m_pk_accountstart: record.m_pk_accountstart
            }, item.msg);
            break;
        case 'Unsettle.do':
            this.statusOperation(item.path, {
                m_pk_contrastaccount: record.m_pk_contrastaccount, 
                m_years: record.m_years, 
                m_pk_corp: record.m_pk_corp
            }, item.msg);
            break;
    }
}