import { base } from 'nc-lightapp-front';
import moment from 'moment';
import { showMoney, AccSub, getScale, formatMoney, width, resolveColumn } from '../../../commom';
const { NCPopconfirm, NCIcon } = base;
const format= 'YYYY-MM-DD';

//按钮状态
export function buttonConfig (status= 'list') {
    let isShow= this.state.dataSelect.length> 0;
    let list= [
        {content: this.lang('0013'), msg: this.lang('0013') + this.lang('0043'), path: 'audit.do', show: status=== 'list' && isShow},
        {content: this.lang('0014'), msg: this.lang('0014') + this.lang('0043'), path: 'unaudit.do', show: status=== 'list' && isShow},
        {content: this.lang('0017'), path: status=== 'detail' ? 'printdetail.do' : 'printlist.do', show: this.state.isShow, isSeperate: true},
        {content: this.lang('0039'), path: 'output', show: this.state.isShow},
        {content: <i className="iconfont icon-shuaxin1" />, path: 'querydetail.do', show: true},
    ];
    // return list.filter((item, index) => item.show);
    return list;
}

//列表状态
export function listOperation (approver) {
    let list= [
        {content: this.lang('0013'), msg: this.lang('0013') + this.lang('0043'), path: 'audit.do', show: !approver},
        {content: this.lang('0014'), msg: this.lang('0014') + this.lang('0043'), path: 'unaudit.do', show: approver},
        {content: this.lang('0015'), msg: this.lang('0015') + this.lang('0043'), path: 'settle.do', show: false},
        {content: this.lang('0016'), msg: this.lang('0016') + this.lang('0043'), path: 'opposite.do', show: false},
        {content: this.lang('0020'), path: 'querydetail.do', show: true},
        {content: this.lang('0021'), path: 'matching', show: true},
    ];
    return list.filter((item, index) => item.show);
}

export function list (search) {
    return [
        {
            itemtype: 'refer',
            label: this.lang('0022'),
            code: 'pkOrgArr',
            required: true,
            config: {
                placeholder: this.lang('0022'),
                refName: this.lang('0022'),
                name: 'pkOrgArr',
                queryTreeUrl: '/nccloud/uapbd/org/FinanceOrgAllGroupAllDataRefTree.do',
                refType: 'tree',
                refCode: 'uapbd.refer.org.FinanceOrgAllGroupAllDataTreeRef',
                isMultiSelectedEnabled: true,
                queryCondition: {
                    TreeRefActionExt: 'nccloud.web.cmp.ref.CMPUserPermissionOrgAllGroupBuilder',
                    isShowDisabledData:'N', 
                },
                isTreelazyLoad:false,
                value: search.pkOrgArr
            }
        },
        {
            itemtype: 'refer',
            label: this.lang('0023'),
            code: 'pkContrastAccountArr',
            required: true,
            config: {
                placeholder: this.lang('0023'),
                refName: this.lang('0023'),
                name: 'pkContrastAccountArr',
                queryGridUrl: '/nccloud/cmp/refer/CMPContrastAccGridRef.do',
                refType: 'grid',
                refCode: 'cmp.refer.bankcontrast.CMPContrastAccGridRef',
                isMultiSelectedEnabled: true,
                value: search.pkContrastAccountArr,
                disabled: !search.pkOrgArr || !search.pkOrgArr.length,
                queryCondition: {pkOrgArr: search.pkOrgArr && search.pkOrgArr.map(item => item.refpk).join(',')},
            }
        },
        {
            itemtype: 'datepicker',
            label: this.lang('0026'),
            code: 'endDate',
            config: {
                placeholder: this.lang('0026'),
                name: 'endDate',
                value: search.endDate
            }
        },
        {
            itemtype: 'rangepicker',
            label: this.lang('0027'),
            code: 'firstAuditDate',
            config: {
                placeholder: this.lang('0027'),
                name: 'firstAuditDate',
                value: search.firstAuditDate ? [search.firstAuditDate, search.secondAuditDate] : []
            }
        },
        {
            itemtype: 'refer',
            label: this.lang('0028'),
            code: 'auditPersonPk',
            config: {
                placeholder: this.lang('0028'),
                refName: this.lang('0028'),
                name: 'auditPersonPk',
                queryGridUrl: '/nccloud/riart/ref/userDefaultRefTreeAction.do',
                // columnConfig: [{name: [ this.lang('0024'), this.lang('0025'), this.lang('0029'), this.lang('0030') ],code: [ 'refcode', 'refname', 'groupname', 'name' ]}],
                refType: 'grid',
                refCode: 'riart.refer.userDefault.UserDefaultRefAction',
                isMultiSelectedEnabled: false,
                value: {
                    refname: search.auditPersonPkName, 
                    refpk: search.auditPersonPk
                }
            }
        }
    ];
}

export function columns() {
    let { page, size }= this.state.pages;
    let list= [
        { 
            title: this.lang('0046'), 
            key: 'index', 
            dataIndex: 'index',
            width: '60px',
            fixed: true,
            render: (text, record, index) => {
                return (
                    <div>{(page - 1) * size + index + 1}</div>
                );
            } 
        },
        { 
            title: this.lang('0022'), 
            key: 'pk_org', 
            dataIndex: 'pk_org',
            width,
            render: text => {
                return (
                    <div>{text && text.split('&&&')[1]}</div>
                );
            } 
        },
        { 
            title: this.lang('0031'), 
            key: 'account', 
            dataIndex: 'account', 
            width,
        },
        { 
            title: this.lang('0032'), 
            key: 'linkmemo', 
            dataIndex: 'linkmemo', 
            width,
        },
        { 
            title: this.lang('0033'), 
            key: 'startdate', 
            dataIndex: 'startdate', 
            width,
        },
        { 
            title: this.lang('0034'), 
            key: 'begdate', 
            dataIndex: 'begdate', 
            width,
        },
        { 
            title: this.lang('0026'), 
            key: 'enddate', 
            dataIndex: 'enddate', 
            width,
        },
        { 
            title: this.lang('0038'),
            key: 'bankbalance', 
            dataIndex: 'bankbalance', 
            className: 'money-right', 
            width,
            render: text => {
                return (
                    <div>{showMoney(text)}</div>
                );
            } 
        },
        { 
            title: this.lang('0009'), 
            key: 'bankbalanceafter', 
            dataIndex: 'bankbalanceafter',  
            className: 'money-right',
            width, 
            render: text => {
                return (
                    <div>{showMoney(text)}</div>
                );
            }   
        },
        { 
            title: this.lang('0006'), 
            key: 'billbalance', 
            dataIndex: 'billbalance', 
            className: 'money-right', 
            width,
            render: text => {
                return (
                    <div>{showMoney(text)}</div>
                );
            }  
        },
        { 
            title: this.lang('0009'), 
            key: 'billbalanceafter', 
            dataIndex: 'billbalanceafter', 
            className: 'money-right', 
            width,
            render: text => {
                return (
                    <div>{showMoney(text)}</div>
                );
            }  
        },
        { 
            title: this.lang('0035'), 
            key: 'gap', 
            dataIndex: 'gap',
            className: 'money-right', 
            width,
            render: (text, record) => {
                let gap= AccSub(record.bankbalanceafter || 0, record.billbalanceafter || 0);
                let scale= getScale(record.bankbalanceafter);
                return (
                    <div>{gap ? Number(gap).formatMoney(scale) : ''}</div>
                );
            }
        },
        { 
            title: this.lang('0028'), 
            key: 'approver', 
            dataIndex: 'approver',
            width,
        },
        { 
            title: this.lang('0027'), 
            key: 'approvedate', 
            dataIndex: 'approvedate',
            width,
        },
        { 
            title: this.lang('0036'), 
            key: 'operation', 
            dataIndex: 'operation', 
            width: '300px',
            fixed: 'right',
            render: (text, record) => {
                let showArr= listOperation.call(this, record.approver);
                return (
                    <div className="single-line-and-ellipsis">
                        <span className="opration-wrapper">
                        {
                            showArr.map((item, index) => {
                                return (
                                    <div className="nc-hotkeys-wrapper" style={{display: 'inline-block'}}>
                                        {item.path=== 'unaudit.do' ? 
                                            <NCPopconfirm
                                                trigger="click"
                                                placement="top"
                                                content={this.lang('0037')}
                                                onClose={audit.bind(this, record, item)}
                                            >
                                                <a className="row-edit-option">
                                                    {item.content}
                                                </a> 
                                            </NCPopconfirm> : 
                                            <a
                                                className="row-edit-option" 
                                                onClick={() => {
                                                    if (item.path=== 'querydetail.do') {//余额调节表
                                                        this.setState(
                                                            {   
                                                                printRecord: record,
                                                                record: {
                                                                    ...record, 
                                                                    pk_org: record.pk_org && record.pk_org.split('&&&')[0],
                                                                    // 财务组织名称
                                                                    org_name: record.pk_org && record.pk_org.split('&&&')[1]
                                                                }
                                                            }, () => {
                                                                this.btnRequire(item.path, {balanceAdjustVo: record}, item.msg);
                                                            }
                                                        );
                                                    } else if (item.path=== 'matching') {//联查勾对
                                                        this.props.openTo('/cmp/bank/bankmatching/main/index.html#/list', {
                                                            data: JSON.stringify({
                                                                m_Pk_Corp: record.pk_org && record.pk_org.split('&&&')[0],
                                                                m_Pk_CorpName: record.pk_org && record.pk_org.split('&&&')[1],
                                                                m_Pk_Account: record.pk_account,
                                                                m_Pk_AccountName: record.account,
                                                                m_strDate: record.begdate || record.startdate,
                                                                m_strEndDate: record.enddate,
                                                                appcode: '3607DQMS',
                                                                pagecode: '3607DQMS_L01',
                                                            })
                                                        });
                                                    } else {//确认、取消确认
                                                        audit.call(this, record, item);
                                                    }
                                                }}
                                            >
                                                {item.content}
                                            </a>
                                        }
                                    </div>
                                )
                            })
                        }
                        </span>
                    </div>
                );
            } 
        },
    ];
    return resolveColumn(list);
}

function audit (record, item) {
    let data= {
        balanceAdjustArr: [{
            ...record, 
            pk_org: record.pk_org && record.pk_org.split('&&&')[0]
        }]
    };
    if (item.path=== 'opposite.do') {
        data.confirmFlag= 'false';
    };
    this.setState(
        {
            record,
            isBatch: false
        }, () => {
            this.btnRequire(item.path, data, item.msg);
        }
    );
}

export const searchData= {
    endDate: moment().format(format)
};