import { base } from 'nc-lightapp-front';
import { Num } from '../../../commom';
const { NCCheckbox, NCRadio, NCSwitch } = base;

export const autoSearchData= {
    m_date: true, 				// boolean true 制单日期 false 票据日期
    m_blnSameCheckStyle: true, 	// boolean 结算方式相同
    m_blnSameVoucherNo: false, 	// boolean 凭证号相同
    m_bSameNumber: true, 		//boolean标识码相同
    m_bSameTranserial: false, 	//boolean银行交易流水号相同
    m_blnSameCheckNo: true, 	//boolean票据号相同
    m_bOppUnitName: false, 		//boolean 对方单位相同
    m_iDateSpan: 12,			//日期相差多少天
    gap: true,                  //日期相差
};

export function onChange(val, name) {
    let { autoSearch }= this.state;
    switch (name) {
        case 'gap':
            autoSearch.gap= val;
            if (!val) {
                autoSearch.m_iDateSpan= null;
                autoSearch.m_date= null;
            } else {
                autoSearch.m_iDateSpan= 12;
            }
            break;
        case 'm_blnSameCheckNo':
            autoSearch.m_blnSameCheckNo= val;
            !val && (autoSearch.m_iAfterBit= null);
            break;
        default: 
            autoSearch[name]= val; 
    }
    this.setState({autoSearch});
}

// 快速对账内容
export function autoContent() {
    let { autoSearch }= this.state;
    return (
        <ul className="quick-content auto">
            <li>
                <span className="width130">{this.lang('0062')}</span>
                <NCSwitch
                    checked={autoSearch.gap}
                    onChange={e => onChange.call(this, e, 'gap')}
                />
                <div/>
                <span className="marginL147 marginR16">{this.lang('0090')}</span>
                <Num
                    value={autoSearch.m_iDateSpan || ''}
                    scale={0}
                    disabled={!autoSearch.gap}
                    onChange={e => onChange.call(this, e, 'm_iDateSpan')}
                />
                <div/>
                <NCRadio.NCRadioGroup
                    selectedValue={autoSearch.m_date}
                    name="gap"
                    onChange={e => onChange.call(this, e, 'm_date')}
                >
                    <NCRadio disabled={!autoSearch.gap} value={true}>{this.lang('0048')}</NCRadio>
                    <NCRadio disabled={!autoSearch.gap} value={false}>{this.lang('0030')}</NCRadio>
                </NCRadio.NCRadioGroup>
            </li>
            <li>
                <span className="width130">{this.lang('0063')}</span>
                <NCCheckbox
                    checked={autoSearch.m_blnSameCheckNo}
                    onChange={e => onChange.call(this, e, 'm_blnSameCheckNo')}
                />
                <Num
                    value={autoSearch.m_iAfterBit || ''}
                    disabled={!autoSearch.m_blnSameCheckNo}
                    scale={0}
                    className="width90"
                    onChange={e => onChange.call(this, e, 'm_iAfterBit')}
                /><span className="width30">{this.lang('0065')}</span>
            </li>
            <li>
                <span className="width130">{this.lang('0066')}</span>
                <NCCheckbox
                    checked={autoSearch.m_blnSameCheckStyle}
                    onChange={e => onChange.call(this, e, 'm_blnSameCheckStyle')}
                />
            </li>
            <li>
                <span className="width130">{this.lang('0067')}({this.lang('0068')})</span>
                <NCCheckbox
                    checked={autoSearch.m_blnSameVoucherNo}
                    onChange={e => onChange.call(this, e, 'm_blnSameVoucherNo')}
                />
            </li>
            <li>
                <span className="width130">{this.lang('0069')}</span>
                <NCCheckbox
                    checked={autoSearch.m_bSameNumber}
                    onChange={e => onChange.call(this, e, 'm_bSameNumber')}
                />
            </li>
            <li>
                <span className="width130">{this.lang('0070')}</span>
                <NCCheckbox
                    checked={autoSearch.m_bOppUnitName}
                    onChange={e => onChange.call(this, e, 'm_bOppUnitName')}
                />
            </li>
            <li>
                <span className="width130">{this.lang('0107')}</span>
                <NCCheckbox
                    checked={autoSearch.m_bSameTranserial}
                    onChange={e => onChange.call(this, e, 'm_bSameTranserial')}
                />
            </li>
        </ul>
    );
}

export function autoClick() {
    let { autoSearch, searchMap }= this.state;
    this.btnRequire('autocontrast.do', {glContrastVo: {m_pk_contrastaccount: searchMap.m_Pk_Account}, qconvo: autoSearch, queryVO: {...searchMap, m_Pk_Account: [searchMap.m_Pk_Account]}}, this.lang.call(this, '0007')+ this.lang.call(this, '0092'))
}