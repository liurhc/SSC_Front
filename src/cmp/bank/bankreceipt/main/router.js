import { asyncComponent } from 'nc-lightapp-front';
import List from '../list';

const routes = [
	{
		path: '/',
		component: List,
		exact: true
	},
	{
		path: '/list',
		component: List
	},
];

export default routes;
