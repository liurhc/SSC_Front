export const list = (search, code) => {
    return [
        {
            itemtype: 'refer',
            label: code,
            code: 'm_pk_corp',
            required: true,
            config: {
                placeholder: code, 
                refName: code, 
                name: 'm_pk_corp',
                queryTreeUrl: '/nccloud/uapbd/org/FinanceOrgAllGroupAllDataRefTree.do',
                refType: 'tree',
                refCode: 'uapbd.refer.org.FinanceOrgAllGroupAllDataTreeRef',
                isMultiSelectedEnabled: false,
                isTreelazyLoad: false,
                queryCondition: {
                    isDataPowerEnable: 'Y',
                    isShowDisabledData:'N', 
                    TreeRefActionExt: 'nccloud.web.cmp.ref.CMPUserPermissionOrgAllGroupBuilder'
                },
                value: {
                    refname: search.m_pk_corpName, 
                    refpk: search.m_pk_corp
                }
            }
        }
    ];
}