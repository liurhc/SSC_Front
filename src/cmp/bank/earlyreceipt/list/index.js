import React, { Component } from 'react';
import { createPage, ajax, base, high, toast, createPageIcon } from 'nc-lightapp-front';
import { list } from './events/common';
import { Search, getLangCode, PageJump, width, getProp, setPageProp, resolveColumn, getTableHeight } from '../../commom';
import './index.less';
const { NCButton, NCTable, NCAffix, NCDiv } = base;
const moduleId= '360796';

class List extends Component {
	constructor(props) {
		super(props);
		this.state = {
			dataList: [],
			searchMap: {},
			pages: {
				page: 1,
				size: 10
			}
		};
		this.moduleId= moduleId;
		this.lang= getLangCode.bind(this);
	}

	componentDidMount () {
		let searchMap= getProp.call(this, 'earlyreceiptSearch');
		if (searchMap) {
			this.setState(
				{
					searchMap
				}, () => {
					if (searchMap.m_pk_corp) {
						this.getList();
					}
				}
			);
		} else {
			ajax({
				url: '/nccloud/cmp/contrastcommon/defaultorg.do',
				loading: false,
				success: (res) => {
					let { success, data } = res;
					let { searchMap }= this.state;
					if (success) {
						searchMap.m_pk_corp= data.orgId;
						searchMap.m_pk_corpName= data.orgName;
						this.setState({
							searchMap
						}, () => {
							if (searchMap.m_pk_corp) {
								this.getList();
							}
						});
					}
				}
			});
		}
	}
	
	getList = () => {
		ajax({
			url: '/nccloud/cmp/initcontrast/query.do',
			data: {
				m_pk_corp : this.state.searchMap.m_pk_corp
			},
			success: (res) => {
				let { success, data } = res;
				if (success) {
					this.setState({
						dataList: (data || []).filter(item => item.m_startdate)
					});
				} else {
					this.setState({
						dataList: [],
					});
				}
			},
		});
	}

	columns = () => {
		let { searchMap, pages }= this.state;
		let { page, size }= pages;
		let list= [
			{ 
				title: this.lang('0005'),
				key: 'key', 
				dataIndex: 'key', 
				width: '80px',
				className: 'pleft20',
				render: (text, record, index) => {
					return (
						<div>{(page - 1) * size + index + 1}</div>
					);
				} 
			},
			{ 
				title: this.lang('0001'), 
				key: 'orgName', 
				dataIndex: 'orgName', 
				width,
			},
			{ 
				title: this.lang('0002'),
				key: 'm_contrastaccountname', 
				dataIndex: 'm_contrastaccountname', 
				width,
				render: (text, record) => {
					return (
						<div
							style={{color: '#007ace', cursor: 'pointer'}}
							onClick={() => {
								setPageProp.call(this, 'earlyreceiptSearch', searchMap);
								this.props.openTo('/cmp/bank/initcontrast/main/index.html#/card', {
									status: 'browse',
									source: 'earlyreceipt',
									m_pk_corp: record.m_pk_corp,
									m_pk_contrastaccount: record.m_pk_contrastaccount,
									pagecode: '3607ACAS_C01',
									appcode: '3607ACAS'
								});
							}}
						>{text}</div>
					);
				}
			},
			{ 
				title: this.lang('0009'),
				key: 'currnetname', 
				dataIndex: 'currnetname', 
				width, 
			},
			{ 
				title: this.lang('0010'), 
				key: 'm_source', 
				dataIndex: 'm_source', 
				width,
				render: (text, record) => {
					return (
						<div>{text== 1 ? this.lang('0011') : this.lang('0012')}</div>
					);
				} 
			},
			{ 
				title: this.lang('0013'),
				key: 'm_startdate', 
				dataIndex: 'm_startdate', 
				width,
			},
			{ 
				title: this.lang('0014'),
				key: 'm_debitamount', 
				dataIndex: 'm_debitamount', 
				className: 'money-right',
				width,
				render: text => {
					return (
						<div>{text ? Number(text).formatMoney() : ''}</div>
					);
				}
			},
			{ 
				title: this.lang('0015'),
				key: 'm_stopdate', 
				dataIndex: 'm_stopdate',  
				width,
			},
			{ 
				title: this.lang('0008'), 
				key: 'operation', 
				dataIndex: 'operation', 
				width: '150px',
				fixed: 'right',
				render: (text, record) => {
					return (
						<span className="opration-wrapper">
							<a
								className="row-edit-option"
								onClick={() => {
									setPageProp.call(this, 'earlyreceiptSearch', searchMap);
									this.props.pushTo('/card', {
										pk_corp: record.m_pk_corp,
										orgName: record.orgName,
										m_pk_contrastaccount: record.m_pk_contrastaccount,
										m_contrastaccountname: record.m_contrastaccountname,
										m_source: record.m_source,
										pagecode:'3607ER_C01',
									});
								}} 
							>{this.lang('0016')}</a>
						</span>
					);
				} 
			},
		];
		return resolveColumn(list);
	}

	render() {
		let { dataList, searchMap, pages }= this.state;
		const { size, page }= pages;
		return (
			<div className="nc-bill-list bank-earlyreceipt">
				<NCAffix>
					<NCDiv areaCode={NCDiv.config.HEADER} className="nc-bill-header-area">
						<div className="header-title-search-area">
							{createPageIcon && createPageIcon()}
							<h2 className="title-search-detail">{this.lang('0017')}</h2>
							<Search 
								list={list(searchMap, this.lang('0001'))} 
								onChange={(name, val) => {
									searchMap.m_pk_corp= val.refpk;
									searchMap.m_pk_corpName= val.refname;
									this.setState({searchMap}, () => {
										if (searchMap.m_pk_corp) {
											this.getList();
										}
									});
								}}
							/>
						</div>
						<div className="header-button-area">
							<span className="button-app-wrapper">
								<NCButton 
									className="refresh-component"
									onClick={() => {
										if (!searchMap.m_pk_corp) {
											toast({color: 'warning', content: this.lang('0062')});
											return;
										}
										pages.page= 1;
										this.setState(
											{
												pages
											}, () => {
												this.getList();
											}
										);
									}}
									>
									<i className="iconfont icon-shuaxin1" />
								</NCButton>	
							</span>
						</div> 
					</NCDiv>
				</NCAffix>
				
				<div className="nc-bill-table-area">
					<NCTable
						columns= {this.columns()}
						data= {dataList.filter((item, index) => index>= (page-1)*size && index< page*size)}
						rowKey= {record => record.m_pk_contrastaccount}
						scroll={{x: true, y: getTableHeight()}}
						bodyStyle= {{minHeight: '410px'}}
					/>
					<PageJump
						pageSize = {size}
						activePage = {page}
						maxPage = {Math.ceil(dataList.length/size)}
						totalSize = {dataList.length}
						onChangePageIndex = {val => {
							pages.page= val;
							setTimeout(() => {
								this.setState({pages});
							}, 100);
						}}
						onChangePageSize = {val => {
							pages.size= val;
							setTimeout(() => {
								this.setState({pages});
							}, 100);
						}}
						lang={{
							all: this.lang('0070'),
							bar: this.lang('0071'),
							jump: this.lang('0072'),
							num: this.lang('0073'),
							ok: this.lang('0074'),
							pageLang: this.lang('0075')
						}}
					/>
				</div>	
			</div>
		);
	}
}

List = createPage({
	// initTemplate: initTemplate,
	mutiLangCode: moduleId
})(List);
export default List;
// ReactDOM.render(<List />, document.querySelector('#app'));