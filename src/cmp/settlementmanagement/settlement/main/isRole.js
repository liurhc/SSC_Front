import { ajax } from 'nc-lightapp-front';

export default function isRole(props) {
    ajax({
        url: '/nccloud/cmp/settlement/isrole.do',
        data: "",
        success: (data) => {
            props.button.setButtonVisible('signBtn', data.data.signBtn);
            props.button.setButtonVisible('antiSignBtn', data.data.antiSignBtn);
            props.button.setButtonVisible('rtpay', data.data.rtpay);
            props.button.setButtonVisible('confirmBtn', data.data.confirmBtn);
            props.button.setButtonVisible('settleBtn', data.data.settleBtn);
            props.button.setButtonVisible('hangBtn', data.data.hangBtn);
            props.button.setButtonVisible('cancelhang', data.data.cancelhang);
            this.setState({
                isRole: data.data
            })
        }

    })
}