import {  ajax, toast, cacheTools,promptBox } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";//配置的id和area信息
import {  commondata } from '../../../../public/utils/constant';
import { MakeBillApp } from '../../../../public/utils/Makebill';//制单
/**
 * [结算]-列表操作列
 */
export default function tableButtonClick(props, key, text, record, index) {
    let _this = this;
    switch (key) {
        // 修改
        case 'innerEdit':
            props.pushTo('/card', {
                status: 'edit',
                id: record.pk_settlement.value
            });
            break;
        // 签字
        case 'innerSign':

            let signpk = [];
            let signts = [];
            signpk.push(record.pk_settlement.value);
            signts.push(record.ts.value);
            let signdata = {
                'pks': signpk,
                'tss': signts
            };

            ajax({
                url: Templatedata.settlesign,
                data: signdata,
                success: (res) => {
                    let { success, data } = res;
                    if (success) {
                        toast({ color: 'success', content: _this.getLangCode('000000') });/* 国际化处理： 签字成功*/
                        _this.refreshPks();
                        //props.table.deleteTableRowsByIndex(table_id, index);
                    }
                }
            });

            break;
        //取消签字
        case 'innerAntiSign':

            let antisignpk = [];
            let antisignts = [];
            let pktsmaps = {};
            antisignpk.push(record.pk_settlement.value);
            antisignts.push(record.ts.value);
            pktsmaps[record.pk_settlement.value] = record.ts.value;
            let AntiSigndata = {
                'pks': antisignpk,
                'tss': antisignts,
                'pktsmap': pktsmaps
            };
            ajax({
                url: Templatedata.settleantisign,
                data: AntiSigndata,
                success: (res) => {
                    let { success, data } = res;
                    if (success) {
                        toast({ color: 'success', content: _this.getLangCode('000001') });/* 国际化处理： 取消签字成功*/
                        _this.refreshPks();
                        //props.table.deleteTableRowsByIndex(table_id, index);
                    }
                }
            });
            break;
        //结算
        case 'innerSettle':

            let settlepks = [];
            let settlets = [];
            settlepks.push(record.pk_settlement.value);
            settlets.push(record.ts.value);
            let settledata = {
                'pks': settlepks,
                'tss': settlets
            };
            ajax({
                url: Templatedata.settlesettle,
                data: settledata,
                success: (res) => {
                    let { success, data } = res;
                    if (success) {
                        if (data && data.message && data.message.errmsg && data.message.errmsg.length > 0) {
                            let errmsg = data.message.errmsg;
                            //结算失败
                            _this.settlementBatchToast(commondata.SETTLE, 0, 1, errmsg);
                        }else{
                            //结算成功
                            _this.settlementBatchToast(commondata.SETTLE, 1, 0, []);
                        }
                        // console.log(data);
                        // toast({ color: 'success', content: _this.getLangCode('000002') });/* 国际化处理： 结算成功*/
                        // _this.refreshPks();
                        _this.refreshByData([index], data);
                        //props.table.deleteTableRowsByIndex(table_id, index);
                    }
                }
            });
            break;
        //取消结算
        case 'innerAntiSettle':

            let antisettlepk = [];
            let antisettlets = [];
            antisettlepk.push(record.pk_settlement.value);
            antisettlets.push(record.ts.value);
            let antiSettledata = {
                'pks': antisettlepk,
                'tss': antisettlets
            };
            ajax({
                url: Templatedata.settleantisettle,
                data: antiSettledata,
                success: (res) => {
                    let { success, data } = res;
                    if (success) {
                        if (data && data.message && data.message.errmsg && data.message.errmsg.length > 0) {
                            // 取消结算失败
                            let errmsg = data.message.errmsg;
                            _this.settlementBatchToast(commondata.UNSETTLE, 0, 1, errmsg);
                        } else {
                            // 取消结算成功
                            _this.settlementBatchToast(commondata.UNSETTLE, 1, 0, []);
                        }
                        // _this.refreshPks();
                        _this.refreshByData([index], data);
                        //props.table.deleteTableRowsByIndex(table_id, index);
                    }
                }
            });
            break;
        //制单
        case 'makebillBtn':

            //需要调用各自单据的制单
            let makebillData = record;
            //let pk_billtype='2201';
            //数据校验
            let makebillarr = [];
            //处理选择数据,结算成功的才可以制单
            let pk_busibill, pk_billtype;
            let settlestatus = record.settlestatus.value;
            if (settlestatus != 5) {
                toast({
                    color: 'info',
                    content:
                        this.props.MutiInit.getIntl('360704SM') &&
                        this.props.MutiInit.getIntl('360704SM').get('360704SM-000013')
                }); /* 国际化处理： 结算成功才可以制单！*/
                return;
            }
            // 业务单据id
            if (record.pk_busibill && record.pk_busibill.value != null) {
                pk_busibill = record.pk_busibill.value;
            }
            // 业务单据类型
            if (record.pk_billtype && record.pk_billtype.value != null) {
                pk_billtype = record.pk_billtype.value;
            }
            let makebill = [];
            makebill.push(pk_billtype);
            makebill.push(pk_busibill);
            makebillarr.push(makebill);
            //处理选择数据
            let makebill_cachekey = Templatedata.list_appid + '_MadeBill';
            cacheTools.set(makebill_cachekey, makebillarr);

            let makebillappcode = commondata.voucherappcode;
            let makebillpagecode = commondata.voucherpagecode;
            MakeBillApp(this.props, makebillappcode, makebillarr);
            break;
        // 支付变更
        case 'settlePayChangeBtn':
            // 支付变更，需要变更的是子表数据，我不知道是哪个子表
            // this.props.modal.show('redHandleModal');
            // if (!checked || checked.length==0) {
            //     toast({ color: 'warning', content: this.props.MutiInit.getIntl("36070OP") && this.props.MutiInit.getIntl("36070OP").get('36070OP-000001') });/* 国际化处理： 请选择需要支付变更的子表数据!*/
            //     return;
            // }
            // let paychengevo = checked[0];
            // // 子表的支付状态，2为支付失败
            // let settlevalue = record.settlestatus.value;
            // if (!settlevalue || settlevalue!='2') {
            //     toast({ color: 'warning', content: this.props.MutiInit.getIntl("36070OP") && this.props.MutiInit.getIntl("36070OP").get('36070OP-000002') });/* 国际化处理： 支付失败的单据才可进行支付变更!*/
            //     return;
            // }
            //     let pk_org = record.pk_org.value;
            //     if (!pk_org) {
            //         toast({ color: 'warning', content: this.props.MutiInit.getIntl("36070OP") && this.props.MutiInit.getIntl("36070OP").get('36070OP-000002') });/* 国际化处理： 支付失败的单据才可进行支付变更!*/
            //         return;
            //     }
            //     let changeddata = {
            //         pk_org:pk_org
            //     };
            //     ajax({
            //         url: Templatedata.settlePayChangeBtn,
            //         data: data,
            //         success: (res) => {
            //             let {success,data} = res;
            //             if (success) {
            //                 // 表示校验成功，可以进行支付变更。需要跳转去支付变更新增页

            //             }
            //         }
            //     });
            break;
        // 结算红冲
        case 'redHandleBtn':

            let pks = [];
            let tss = [];
            //处理选择数据
            let pktsmap = {};
            //此处可校验，挑选满足条件的进行操作
            let pk = record.pk_settlement.value;
            let ts = record.ts.value;
            pks.push(pk);//主键数组
            tss.push(ts);
            pktsmap[pk] = ts;
            let data = {
                pks: pks,
                tss: tss,
                pktsmap: pktsmap
            };
            ajax({
                url: Templatedata.settleredhandle,
                data: data,
                success: (res) => {
                    let { success, data } = res;
                    if (success) {
                        toast({ color: 'success', content: this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000021') });/* 国际化处理： 红冲成功*/
                        _this.refreshPks();
                    }
                }
            });
            break;
        //网上转账
        case 'innerNetpayBtn':
            //设置ca弹框--使用工资清单单据结算支付使用
            let trade_code = record.tradertypecode.value;
            this.setState({
                tradecode: trade_code,
            });
            //设置表格的选中状态:防止在补录保存的时候校验失败
            this.props.table.selectAllRows(this.tableId, false);
            this.props.table.selectTableRows(this.tableId, [index], true);
            // 网上转账
            // 对数据状态进行校验，非签字态不可进行网上转账
            // 结算状态
            let netsettlestatus = record.settlestatus.value;
            // 签字人
            let netpk_signer = record.pk_signer.value;
            let error = [];
            if (netpk_signer && (netsettlestatus == '0' || netsettlestatus == '2')) {
                // 结算状态为未结算'0'为未结算，是未结算且签字人不为空，可以进行网上支付

            } else {
                let billcode = record.billcode.value;
                error.push(billcode);
            }
            if (error && error.length > 0) {
                let content = (this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000029'))
                    + error.join(', ')
                    + (this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000068'));/* 国际化处理： 单据编号 , 不可进行网上转账操作！*/
                toast({ color: 'warning', content: content });
                return;
            } else {
                let pk = record.pk_settlement.value;
                let ts = record.ts.value;
                let pks = [];
                let tss = [];
                let pktsmap = {};
                let indexs = [];
                pks.push(pk);
                tss.push(ts);
                pktsmap[pk] = ts;
                indexs.push(index);
                let data = {
                    pks: pks,
                    tss: tss,
                    pktsmap: pktsmap,
                };
                // 此处需要先校验是否可以网银支付，然后再弹框
                ajax({
                    url: Templatedata.netpayValidate,
                    data: data,
                    success: (res) => {
                        let { success } = res;
                        if (success) {
                            // 成功即代表校验通过，否则就会抛出异常数据、
                            // 成功即代表校验通过，否则就会抛出异常数据
                            _this.paydata = data;
                            // _this.isinnerpay = true;//表示操作列中进行的网上转账
                            // props.modal.show('netPayModal');
                            promptBox({
                                color: "warning",
                                title: this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000066'),/* 国际化处理： 网上支付*/
                                content: this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000067'),/* 国际化处理： 确定进行网上支付?*/
                                beSureBtnClick: this.netPayProcess.bind(this), //使用call直接執行了
                              });
                        }
                    }
                });
            }
            break;
    }
}


