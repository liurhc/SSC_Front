import searchBtnClick from './searchBtnClick';
import initTemplate from './initTemplate';
import pageInfoClick from './pageInfoClick';
import buttonClick from './buttonClick';
import tableButtonClick  from './tableButtonClick';
import buttonUsable from './buttonUsable';
export { searchBtnClick, pageInfoClick,initTemplate,buttonClick,tableButtonClick ,buttonUsable};
