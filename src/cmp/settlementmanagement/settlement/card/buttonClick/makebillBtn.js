import { createPage, ajax, base, toast, cacheTools, print, cardCache } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";
import { MakeBillApp } from '../../../../public/utils/Makebill';//制单
//缓存
let { setDefData, getDefData,
    getCurrentLastId, getCacheById,
    updateCache, addCache, getNextId,
    deleteCacheById } = cardCache;

/**
 * [外币兑换]-制单按钮
 * @param {*} props  
 */
// export const makebillBtn = function () {
//     if (!this.props.form.getFormItemsValue(this.formId, 'pk_busibill').value) {
//         toast(
//             {
//                  color: 'warning', 
//                  content: this.props.MutiInit.getIntl("36070FCE") &&
//                   this.props.MutiInit.getIntl("36070FCE").get('36070FCE-000011')
//                  });/* 国际化处理： 操作失败，无数据!*/
//         return;
//     }
//     let makebillArr = [];
//     let arr = [];
//     let pk_billtype='2201';
//      // 业务单据类型
//     if (this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value != null) {
//         pk_billtype = this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value;
//     }
//     //处理选择数据
//     let pk_makebill = this.props.form.getFormItemsValue(this.formId, 'pk_busibill').value;
//     makebillArr.push(pk_billtype);
//     makebillArr.push(pk_makebill);
//     arr.push(makebillArr);
//     MakeBillApp(this.props, '10170410', arr);
// }


//张宏建 李子云 补丁
export const makebillBtn = function () {
    if (!this.props.form.getFormItemsValue(this.formId, 'pk_busibill').value) {
        console.log('操作失败:', '无业务单据pk');
        return;
    }
    let makebillArr = [];
    let arr = [];
    //处理选择数据
    let pk_makebill = this.props.form.getFormItemsValue(this.formId, 'pk_busibill').value;
    let pk_billtype = '2201';
    if (this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value) {
        pk_billtype = this.props.form.getFormItemsValue(this.formId, 'pk_tradetype').value;
    }
    //制单参数修改
    MakeBillApp(this.props, Templatedata.bill_funcode, pk_makebill, pk_billtype);
}
