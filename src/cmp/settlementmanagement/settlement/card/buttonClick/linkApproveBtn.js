import { createPage, ajax, base, high, toast,cacheTools, cardCache, print, output } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";

/**
 * [结算网银]-审批意见按钮
 * @param {*} props 
 * @param {*} config 
 * @param {*} pks 
 */
export const linkApproveBtn = function () {
    if (!this.props.form.getFormItemsValue(this.formId, 'pk_settlement').value) {
        toast({ color: 'warning', content: this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000083') });/* 国际化处理： 操作失败*/
        return;
    }
    let billid = this.props.form.getFormItemsValue(this.formId, 'pk_settlement').value;
    let approve_billtype=Templatedata.card_settlebilltype;
    if (this.props.form.getFormItemsValue(this.formId, 'settlebilltype').value) {
        approve_billtype = this.props.form.getFormItemsValue(this.formId, 'settlebilltype').value;
    }
    if (billid) {
        this.setState({
            approveShow: true,
            approveBilltype: approve_billtype,//单据类型
            approveBillid: billid//单据pk
        });
    }
}
