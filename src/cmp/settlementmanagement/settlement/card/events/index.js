import buttonClick from './buttonClick';
import initTemplate from './initTemplate';
import initTemplateApp from './initTemplateApp';
import afterEvent from './afterEvent';
import pageInfoClick from './pageInfoClick'
import tableButtonClick  from './tableButtonClick';
export { buttonClick, afterEvent, initTemplate, initTemplateApp, pageInfoClick,tableButtonClick };
