import { ajax, toast, promptBox, print, deepClone, cardCache} from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";
let { updateCache } = cardCache;
import {
    sourceModel_CMP, SHOWMODEL_ZHIFU,
    commondata, getappurl,
    SHOWMODEL_LIULAN, SHOWMODEL_BULU,
    PAYMODEL_COMBINEPAY
} from "../../../../public/utils/constant.js";
import Sign from '../../../../../tmpub/pub/util/ca';
import { makebillBtn } from '../buttonClick/makebillBtn.js';
import { imageView } from '../../../../../sscrp/public/common/components/sscImageMng.js'
import { linkVoucherApp } from "../../../../../tmpub/pub/util/LinkUtil.js";
import { linkApproveBtn } from '../buttonClick/linkApproveBtn.js';

export default async function (props, id) {
    let pk = props.form.getFormItemsValue(this.formId, 'pk_settlement').value;
    let alldata = props.form.getAllFormValue(this.formId);      //有值有对象
    //let alldata2 = props.form.getAllFormValue(this.searchId);
    //   let alldata3 = props.form.getAllFormValue(this.tableId);     //有对象，无值
    //let alldata4 = props.form.getAllFormValue(this.pageId);
    //子表选择的数据
    let checked = props.cardTable.getCheckedRows(this.tableId);
    //   let checked1 = props.table.getCheckedRows(this.formId);
    //   let checked2 = props.table.getCheckedRows(this.pageId);
    //   let checked3 = props.table.getCheckedRows(this.moduleId);
    //特殊处理，用于工资清单支付前弹出ca框使用
    let trade_code = null;
    if (props.form.getFormItemsValue(this.formId, 'tradertypecode') && props.form.getFormItemsValue(this.formId, 'tradertypecode').value) {
        trade_code = props.form.getFormItemsValue(this.formId, 'tradertypecode').value;
        this.setState({
            tradecode: trade_code,
        });
    }
    //结算状态
    let setlle_status = props.form.getFormItemsValue(this.formId, 'settlestatus').value;
    //单据编号
    let setlle_billno = props.form.getFormItemsValue(this.formId, 'billcode').value;
    let selectedData = alldata.rows;

    let pks = [];
    let tss = [];
    //处理选择数据
    let pktsmap = {};
    selectedData.forEach((val) => {
        //此处可校验，挑选满足条件的进行操作
        let innerpk = val.values.pk_settlement.value;
        let ts = val.values.ts.value;
        pks.push(innerpk);//主键数组
        tss.push(ts);
        pktsmap[innerpk] = ts;
    });
    let data = {
        'pks': pks,
        'tss': tss,
        'pageid': Templatedata.card_pageid,
        'iscard': true,//卡片请求标识
        pktsmap: pktsmap
    };
    switch (id) {
        case 'hangBtn':
            ajax({
                url: Templatedata.hangurl,
                data: pks,
                success: (data) => {
                    this.refreshCard(pk, null, true);
                }
            })
            break;
        case 'cancelhang':
            ajax({
                url: Templatedata.cancelhangurl,
                data: pks,
                success: (data) => {
                    this.refreshCard(pk, null, true);
                }
            })
            break;
        case 'confirmBtn':
            ajax({
                url: Templatedata.faulturl,
                data: pks,
                success: (data) => {
                    this.refreshCard(pk, null, true);
                }
            })
            break;
        case 'rtpay':
            let newData = [{
                data: {
                }
            }];
            newData[0].data = selectedData[0];
            newData[0].index = 0;
            ajax({
                url: Templatedata.rtpal,
                data: newData,
                success: (res) => {
                    let { success, data } = res;
                    if (success) {
                        if (data && data.success) {
                            // 后台有返回错误信息
                            toast({ content: data.message, color: 'success' });
                            this.refreshCard(pk, null, true);
                        } else {
                            toast({ content: data.message, color: 'warning' });
                        }
                    }
                }
            });
            break;
        //删除,无
        //保存，留着备用
        case 'saveBtn':
            // let changedata = props.cardTable.getChangedRows(this.tableId);
            // let savedata = props.createMasterChildData(Templatedata.card_pageid, 
            //     Templatedata.card_formid, this.tableId);
            // 验证公式，保存时调用
            // this.props.validateToSave( data , this.saveBill() , null , null )
            this.saveBill();
            break;
        //删除,无
        case 'delete':
            this.props.modal.show('delete');
            break;
        //取消操作
        case 'cancelBtn':
            // alert(this.props.getUrlParam('status'));
            // 取消弹框借用红冲弹框一用
            promptBox({
                color: "warning",
                title: this.getLangCode('000088'),/* 国际化处理： 确认取消*/
                beSureBtnClick: this.onCancel.bind(this)
            });
            break;
        //签字
        case 'signBtn':
            // let signData = this.props.createMasterChildData(this.pageId, this.formId, this.tableId);
            //签字
            ajax({
                url: Templatedata.settlesign,
                data: data,
                success: (res) => {
                    let { success, data } = res;
                    if (success) {
                        if (data && data.message && data.message.errmsg && data.message.errmsg.length > 0) {
                            toast({ color: 'warning', content: data.message.errmsg[0] });
                        } else {
                            toast({ color: 'success', content: this.getLangCode('000000') });/* 国际化处理： 结算成功*/
                        }
                        if (data.vos) {
                            this.props.beforeUpdatePage();//打开开关
                            if (data.vos.head) {
                                this.props.form.setAllFormValue({ [this.formId]: data.vos.head[this.formId] });
                                //页签赋值
                                let billno = data.vos.head[this.formId].rows[0].values.billcode.value;
                                let billId = data.vos.head[this.formId].rows[0].values.pk_settlement.value;
                                this.billno = billno;
                                this.billId = billId;// 单据id，用于刷新卡片页，附件上传
                                // 更新缓存
                                updateCache(
                                    'pk_settlement',
                                    billId,
                                    res.data.vos,
                                    this.formId,
                                    this.listDataSource,
                                    res.data.vos.head[this.formId].rows[0].values
                                );
                            }
                            if (data.vos.body) {
                                // this.props.cardTable.setTableData(this.tableId, { rows: [] });
                                this.props.cardTable.setTableData(this.tableId, data.vos.body[this.tableId]);
                            }

                            this.props.setUrlParam({
                                status: 'browse'
                            });
                            // this.refreshCard(pk, null, true);
                            // 此处均由卡片页本页面设置按钮显隐性
                            this.toggleShowBydata(null);
                            // 设置编辑性，此处先不做处理，因为会报错
                            // this.setEditableByDirection();
                            // 此处调用组织多版本展示，结算需要
                            this.formMultiVersionProcess();
                            this.props.updatePage(this.formId, this.tableId);//关闭开关
                        } else {
                            this.refreshCard(pk, null, true);
                        }
                    }
                }
            });


            break;
        //取消签字
        case 'antiSignBtn':
            // let antiSignData = this.props.createMasterChildData(this.pageId, this.formId, this.tableId);
            ajax({
                url: Templatedata.settleantisign,
                data: data,
                success: (res) => {
                    let { success, data } = res;
                    if (success) {

                        if (data && data.message && data.message.errmsg && data.message.errmsg.length > 0) {
                            toast({ color: 'warning', content: data.message.errmsg[0] });
                        } else {
                            toast({ color: 'success', content: this.getLangCode('000001') });/* 国际化处理： 结算成功*/
                        }
                        if (data.vos) {
                            this.props.beforeUpdatePage();//打开开关
                            if (data.vos.head) {
                                this.props.form.setAllFormValue({ [this.formId]: data.vos.head[this.formId] });
                                //页签赋值
                                let billno = data.vos.head[this.formId].rows[0].values.billcode.value;
                                let billId = data.vos.head[this.formId].rows[0].values.pk_settlement.value;
                                this.billno = billno;
                                this.billId = billId;// 单据id，用于刷新卡片页，附件上传
                                // 更新缓存
                                updateCache(
                                    'pk_settlement',
                                    billId,
                                    res.data.vos,
                                    this.formId,
                                    this.listDataSource,
                                    res.data.vos.head[this.formId].rows[0].values
                                );
                            }
                            if (data.vos.body) {
                                // this.props.cardTable.setTableData(this.tableId, { rows: [] });
                                this.props.cardTable.setTableData(this.tableId, data.vos.body[this.tableId]);
                            }

                            this.props.setUrlParam({
                                status: 'browse'
                            });
                            // 此处均由卡片页本页面设置按钮显隐性
                            this.toggleShowBydata(null);
                            // 设置编辑性，此处先不做处理，因为会报错
                            // this.setEditableByDirection();
                            // 此处调用组织多版本展示，结算需要
                            this.formMultiVersionProcess();
                            this.props.updatePage(this.formId, this.tableId);//关闭开关
                        } else {
                            this.refreshCard(pk, null, true);
                        }
                    }
                }
            });
            break;

        //结算
        case 'settleBtn':
            // let settleData = this.props.createMasterChildData(this.pageId, this.formId, this.tableId);
            if (setlle_status == 5) {
                let before = this.getLangCode('000029');/* 国际化处理： 单据编号 */
                let after = this.getLangCode('000036');/* 国际化处理：  不可结算！*/
                let content = before + ':' + setlle_billno + after;
                toast({ color: 'danger', content: content });
                return;
            }
            ajax({
                url: Templatedata.settlesettle,
                data: data,
                success: (res) => {
                    let { success, data } = res;
                    if (success) {
                        if (data && data.message && data.message.errmsg && data.message.errmsg.length > 0) {
                            toast({ color: 'warning', content: data.message.errmsg[0] });
                        } else {
                            toast({ color: 'success', content: this.getLangCode('000002') });/* 国际化处理： 结算成功*/
                        }
                        if (data.vos) {
                            // this.setFormAndTableData(data.voscard);
                            this.props.beforeUpdatePage();//打开开关
                            if (data.vos.head) {
                                this.props.form.setAllFormValue({ [this.formId]: data.vos.head[this.formId] });
                                //页签赋值
                                let billno = data.vos.head[this.formId].rows[0].values.billcode.value;
                                let billId = data.vos.head[this.formId].rows[0].values.pk_settlement.value;
                                this.billno = billno;
                                this.billId = billId;// 单据id，用于刷新卡片页，附件上传
                                // 更新缓存
                                updateCache(
                                    'pk_settlement',
                                    billId,
                                    res.data.vos,
                                    this.formId,
                                    this.listDataSource,
                                    res.data.vos.head[this.formId].rows[0].values
                                );
                            }
                            if (data.vos.body) {
                                // this.props.cardTable.setTableData(this.tableId, { rows: [] });
                                this.props.cardTable.setTableData(this.tableId, data.vos.body[this.tableId]);
                            }

                            this.props.setUrlParam({
                                status: 'browse'
                            });
                            // 此处均由卡片页本页面设置按钮显隐性
                            this.toggleShowBydata(null);
                            // 设置编辑性，此处先不做处理，因为会报错
                            // this.setEditableByDirection();
                            // 此处调用组织多版本展示，结算需要
                            this.formMultiVersionProcess();
                            this.props.updatePage(this.formId, this.tableId);//关闭开关
                        } else {
                            this.refreshCard(pk, null, true);//刷新页面
                        }
                    }
                }
            });
            break;
        //取消结算
        case 'antiSettleBtn':
            // let antiSettleData = this.props.createMasterChildData(this.pageId, this.formId, this.tableId);
            ajax({
                url: Templatedata.settleantisettle,
                data: data,
                success: (res) => {
                    let { success, data } = res;
                    if (success) {
                        if (data && data.message && data.message.errmsg && data.message.errmsg.length > 0) {
                            toast({ color: 'warning', content: data.message.errmsg[0] });
                        } else {
                            toast({ color: 'success', content: this.getLangCode('000003') });/* 国际化处理： 取消结算成功*/
                        }

                        if (data.vos) {
                            this.props.beforeUpdatePage();//打开开关
                            if (data.vos.head) {
                                this.props.form.setAllFormValue({ [this.formId]: data.vos.head[this.formId] });
                                //页签赋值
                                let billno = data.vos.head[this.formId].rows[0].values.billcode.value;
                                let billId = data.vos.head[this.formId].rows[0].values.pk_settlement.value;
                                this.billno = billno;
                                this.billId = billId;// 单据id，用于刷新卡片页，附件上传
                                // 更新缓存
                                updateCache(
                                    'pk_settlement',
                                    billId,
                                    res.data.vos,
                                    this.formId,
                                    this.listDataSource,
                                    res.data.vos.head[this.formId].rows[0].values
                                );
                            }
                            if (data.vos.body) {
                                // this.props.cardTable.setTableData(this.tableId, { rows: [] });
                                this.props.cardTable.setTableData(this.tableId, data.vos.body[this.tableId]);
                            }

                            this.props.setUrlParam({
                                status: 'browse'
                            });
                            // 此处均由卡片页本页面设置按钮显隐性
                            this.toggleShowBydata(null);
                            // 设置编辑性，此处先不做处理，因为会报错
                            // this.setEditableByDirection();
                            // 此处调用组织多版本展示，结算需要
                            this.formMultiVersionProcess();
                            this.props.updatePage(this.formId, this.tableId);//关闭开关
                        } else {
                            this.refreshCard(pk, null, true);//刷新页面
                        }
                    }
                }
            });
            break;
        //委托办理，不支持批量操作，需要修改，需要进行强制CA用户校验
        case 'commitToFTSBtn':
            let error = [];
            selectedData.forEach((val) => {
                //此处可校验，挑选满足条件的进行操作
                let settlestatus = val.values.settlestatus && val.values.settlestatus.value;
                // 业务单据状态 '8'表示签字态
                let busistatus = val.values.busistatus && val.values.busistatus.value;
                // 签字人
                let pk_signer = val.values.pk_signer && val.values.pk_signer.value;
                if (busistatus && busistatus == '8' && settlestatus && settlestatus == '0') {
                    // 签字态未结算的单据可以委托

                } else {
                    // 否则不能进行委托办理
                    let billcode = val.data.values.billcode.value;
                    error.push(billcode);
                }
            });
            if (error && error.length > 0) {
                let content = (this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000029'))
                    + error.join(', ')
                    + (this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000070'));/* 国际化处理： 单据编号 , 不可进行委托办理操作！*/
                toast({ color: 'warning', content: content });
                return;
            }
            // 委托办理需要强制CA校验
            // 强制弹框输入密码
            let result = await Sign({
                data: null,
                encryptVOClassName: null,
                isSign: false,
                isKey: true,
            })
            if (result.isStop) {
                return;
            }
            ajax({
                url: Templatedata.settlecommit,
                data: data,
                success: (res) => {
                    let { success, data } = res;
                    if (success) {
                        toast({ color: 'success', content: this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000004') });/* 国际化处理： 委托办理成功*/
                        if (data.vos) {
                            this.renderCardData(data.vos);
                        } else {
                            this.refreshCard(pk, null, true);
                        }
                    }
                }
            });
            break;
        // 取消委托
        case 'cancelCommitToFTSBtn':
            ajax({
                url: Templatedata.settlecancelcommit,
                data: data,
                success: (res) => {
                    let { success, data } = res;
                    if (success) {
                        toast({ color: 'success', content: this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000065') });/* 国际化处理： 委托办理成功*/
                        if (data.vos) {
                            this.renderCardData(data.vos);
                        } else {
                            this.refreshCard(pk, null, true);
                        }
                    }
                }
            });
            break;


        /*******支付组 */
        //网上转账
        case 'netpayBtn':
            // 网上转账校验
            // 对数据状态进行校验，非签字态不可进行网上转账
            let new_error = [];
            selectedData.forEach((val) => {
                // 结算状态
                let settlestatus = val.values.settlestatus && val.values.settlestatus.value;
                // 签字人
                let pk_signer = val.values.pk_signer && val.values.pk_signer.value;
                if (pk_signer && (settlestatus == '0' || settlestatus == '2' || settlestatus == '6')) {
                    // 结算状态为未结算'0'为未结算，是未结算且签字人不为空，可以进行网上支付
                    // 支付失败的单据可以继续进行网上支付
                } else {
                    let billcode = val.values.billcode.value;
                    new_error.push(billcode);
                }
            })
            if (new_error && new_error.length > 0) {
                let content = (this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000029'))
                    + new_error.join(', ')
                    + (this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000068'));/* 国际化处理： 单据编号 , 不可进行网上转账操作！*/
                toast({ color: 'warning', content: content });
                return;
            } else {
                // 此处需要先校验是否可以网银支付，然后再弹框
                ajax({
                    url: Templatedata.netpayValidate,
                    data: data,
                    success: (res) => {
                        let { success, data } = res;
                        if (success) {
                            // 成功即代表校验通过，否则就会抛出异常数据
                            promptBox({
                                color: "warning",
                                title: this.getLangCode('000066'),   /// this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000066'),/* 国际化处理： 网上支付*/
                                content: this.getLangCode('000067'), // this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000067'),/* 国际化处理： 确定进行网上支付?*/
                                beSureBtnClick: this.netPayProcess.bind(this), //使用call直接執行了
                            });
                        }
                    }
                });
            }
            break;
        //合并支付
        case 'combinpayBtn':
            //toast({ color: 'warning', content: '功能待开发' });
            this.setState({
                modelType: PAYMODEL_COMBINEPAY,
                modalValue: PAYMODEL_COMBINEPAY
            }, () => {
                this.loadBuLuInfo(data);
            });
            break;
        //补录网银信息,支持批量，不需要选择子表
        case 'preparenetBtn':
            //toast({ color: 'info', content: '功能验证' });
            //数据校验
            //   if (checked && checked.length != 1) {
            //       toast({ color: 'warning', content: this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000005') });/* 国际化处理： 请选择1条子表数据*/
            //       return;
            //   }
            // 补录需要补录子表的数据
            this.setState({
                modelType: SHOWMODEL_BULU,
                modalValue: SHOWMODEL_BULU
            }, () => {
                this.loadBuLuInfo(data);
            });
            break;
        //结算红冲
        case 'redHandleBtn':

            // 先校验再提示模态框
            if (!selectedData || selectedData.length != 1) {
                toast({ color: 'warning', content: this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000042') });/* 国际化处理： 请选择1条数据*/
                return;
            }
            //处理选择数据
            let redpks = [];
            let errorM = [];
            let pk_tradetype = null;
            selectedData.forEach((val) => {
                //此处可校验，挑选满足条件的进行操作
                // 结算失败的才可进行结算红冲操作
                // 结算状态为支付失败的单据才可以进行红冲操作
                let settlestatus = val.values.settlestatus.value;
                //交易类型
                pk_tradetype = val.values.pk_tradetype.value;
                // 2为支付失败单据,6为部分成功的单据
                if (settlestatus == '2' || settlestatus == '6') {
                    let pk = val.values.pk_settlement.value;
                    // 主键数组
                    redpks.push(pk);
                } else {
                    errorM.push(val.values.billcode.value);
                }
            });
            //单据类型

            if (pk_tradetype && pk_tradetype == 'DS') {
                // 收款类及工资发放不能 结算红冲
                toast({ color: 'danger', content: (this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000100')) });//国际化处理：'工资发放不能进行结算红冲!'
                return;
            }
            if (redpks.length == 0) {
                let content = this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000043');/* 国际化处理： 您选择的数据不可进行红冲操作！*/
                if (errorM.length != 0) {
                    content = (this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000029'))
                        + errorM.join(', ')
                        + (this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000044'))/* 国际化处理： 单据编号 , 不可进行红冲操作！*/
                }
                toast({ color: 'warning', content: content });
                return;
            }

            promptBox({
                color: "warning",
                title: this.getLangCode('000022'),// this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000022'),/* 国际化处理： 结算红冲*/
                content: this.getLangCode('000023'),// this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000023'),/* 国际化处理： 结算红冲操作不可逆,确定是否继续?*/
                beSureBtnClick: this.redHandleProcess.bind(this)
            });
            break;

        /**联查组 */
        //联查单据，
        case 'linkQueryBillBtn':
            let linkquerybillData = selectedData;
            //数据校验
            if (linkquerybillData.length != 1) {
                toast({ color: 'warning', content: this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000006') });/* 国际化处理： 请选择单条数据，联查单据!*/
                return;
            }
            //处理选择数据
            let showbilltrackpk;
            let billtrack_billtype;
            linkquerybillData.forEach((val) => {
                val.values.pk_settlement.value
                if (val.values.pk_busibill && val.values.pk_busibill.value) {
                    showbilltrackpk = val.values.pk_busibill.value;
                }
                if (val.values.pk_billtype && val.values.pk_billtype.value) {
                    billtrack_billtype = val.values.pk_billtype.value;
                }
            });
            if (showbilltrackpk) {
                this.setState({
                    showbilltrack: true,                    //显示联查单据
                    showbilltracktype: billtrack_billtype,  //单据类型
                    showbilltrackpk: showbilltrackpk        //单据pk
                });
            }
            break;
        //联查凭证,验证已跳转，联查的是业务单据的凭证，结算单本身不产生凭证
        case 'linkVoucherBtn':
            // toast({ color: 'info', content: this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000007') });/* 国际化处理： 联查凭证*/

            let val = selectedData[0];

            let pk_group, pkorg, pk_busibill, pk_billtype, billcode;
            if (val.values.pk_group && val.values.pk_group.value != null) {
                pk_group = val.values.pk_group.value;
            }
            if (val.values.pk_org && val.values.pk_org.value != null) {
                pkorg = val.values.pk_org.value;
            }
            // 业务单据id
            if (val.values.pk_busibill && val.values.pk_busibill.value != null) {
                pk_busibill = val.values.pk_busibill.value;
            }
            // 业务单据类型<有交易类型优先传递>
            if (val.values.pk_tradetype && val.values.pk_tradetype.value != null) {
                pk_billtype = val.values.pk_tradetype.value;
            } else if (val.values.pk_billtype && val.values.pk_billtype.value != null) {
                pk_billtype = val.values.pk_billtype.value;
            }
            // 业务单据编号
            if (val.values.billcode && val.values.billcode.value != null) {
                billcode = val.values.billcode.value;
            }
            /**
             * 联查凭证小应用
             * @param {*} props 页面内置对象
             * @param {*} billID 单据主键
             * @param {*} pk_group 集团
             * @param {*} pk_org 组织
             * @param {*} billOrTransType 单据类型或交易类型
             * @param {*} billNO 单据编号
             */
            linkVoucherApp(
                props,
                pk_busibill,
                pk_group,
                pkorg,
                pk_billtype,
                billcode
            )
            break;
        //联查余额
        case 'linkRestMoneyBtn':
            let checkeddata = checked;
            //数据校验
            if (checkeddata && checkeddata.length != 1) {
                toast({ color: 'warning', content: this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000005') });/* 国际化处理： 请选择1条子表数据*/
                return;
            }
            let restmoneyarr = [];
            //处理选择数据
            checkeddata.forEach((val) => {

                let restpk_org, restpk_account, restpk_cashaccount;
                if (val.data.values.pk_org && val.data.values.pk_org.value != null) {
                    restpk_org = val.data.values.pk_org.value;
                }
                if (val.data.values.pk_account && val.data.values.pk_account.value != null) {
                    restpk_account = val.data.values.pk_account.value;
                }
                if (val.data.values.pk_cashaccount && val.data.values.pk_cashaccount.value != null) {
                    restpk_cashaccount = val.data.values.pk_cashaccount.value;
                }
                let data = {
                    // 财务组织id
                    pk_org: restpk_org,
                    // 银行账户id，没有可不写，和现金账户二选一        
                    pk_account: restpk_account,
                    // 现金账户id，没有可不写  
                    pk_cashaccount: restpk_cashaccount
                }
                restmoneyarr.push(data);
            });
            for (let index = 0; index < restmoneyarr.length; index++) {
                let val = restmoneyarr[index];
                if (!val.pk_account && !val.pk_cashaccount) {
                    restmoneyarr.splice(index, 1);
                }
            }
            // if (restmoneyarr.length==0) {
            //     toast({ color: 'warning', content: this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000076') });/* 国际化处理： 请选择有本方账户或现金账户的单据进行联查余额*/
            //     return;
            // }
            this.setState({
                showOriginalData: restmoneyarr,
                showOriginal: true,
            });
            break;
        //联查网银信息
        case 'linkNetBankBtn':
            // 联查网银信息就是浏览网银补录信息
            let neterror = [];
            selectedData.forEach((val) => {
                // 结算状态
                let settlestatus = val.values.settlestatus && val.values.settlestatus.value;
                // 签字人
                let pk_signer = val.values.pk_signer && val.values.pk_signer.value;
                if (settlestatus == '0') {
                    // 结算状态为未结算'0'为未结算，未结算不可联查网银信息
                    let billcode = val.values.billcode.value;
                    neterror.push(billcode);
                }
            })
            if (neterror && neterror.length > 0) {
                let content = (this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000029'))
                    + neterror.join(', ')
                    + (this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000079'));/* 国际化处理： 单据编号 , 不可进行网上转账操作！*/
                toast({ color: 'warning', content: content });
                return;
            }
            this.setState({
                modelType: SHOWMODEL_LIULAN,
                modalValue: SHOWMODEL_LIULAN
            }, () => {
                this.loadBuLuInfo(data);
            });
            break;
        //联查支付确认单
        case 'linkPayAffirmBtn':
            // 联查前准备工作
            // 联查前准备工作
            let affirmpks = [];
            let affirmerror = [];
            // 控制只有支付失败和支付成功的才可以查询支付确认单
            selectedData.forEach((val) => {
                //此处可校验，挑选满足条件的进行操作
                let settlestatus = val.values.settlestatus.value;
                // 0为未结算
                // if (settlestatus == '0') {
                //     affirmerror.push(val.values.billcode.value);
                // } else {
                //     let pk = val.values.pk_settlement.value;
                // 主键数组
                //     affirmpks.push(pk);
                // }
                // 2018-10-29 问题号：87565 支付确认单不加判断，后台控制；
                let pk = val.values.pk_settlement.value;
                // 主键数组
                affirmpks.push(pk);
            });
            if (affirmpks.length == 0) {
                let content = '';
                if (affirmerror.length != 0) {
                    content = (this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000029'))
                        + affirmerror.join(', ')
                        + (this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000076'))/* 国际化处理：没有进行过网上支付，不能查询支付确认单！*/
                }
                toast({ color: 'warning', content: content });
                return;
            }
            ajax({
                url: Templatedata.linkpayaffirm,
                data: data,
                success: (res) => {
                    let { success, data } = res;
                    if (success) {
                        // toast({ color: 'success', content: '联查支付确认单查询出的数据成功' });
                        console.log(data.yurrefs, 'yurrefs');
                        if (data.yurrefs) {
                            let opentoUrl = '/obm/ebankconfirmpay/confirmpay/main/index.html#/list';
                            // if (data.yurrefs.length > 1) {
                            //     opentoUrl = '/obm/ebankconfirmpay/confirmpay/main/index.html#/list';
                            // }
                            props.openTo(opentoUrl,
                                {
                                    appcode: '36100CONFM',
                                    pagecode: '36100CONFM_L01',
                                    yurrefs: data.yurrefs,
                                    id: data.yurrefs,
                                    type: 'link',
                                    status: 'browse',
                                });
                        } else {
                            toast({ color: 'success', content: this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000076') });/* 国际化处理： 没有支付确认单*/
                        }
                    }
                }
            });
            break;

        //联查影像查看
        case 'imageReview':
            let imageMap = {};
            imageMap['pk_billid'] = selectedData[0].values.pk_busibill.value;//业务单据主键
            imageMap['pk_billtype'] = selectedData[0].values.pk_billtypeid.value;//单据类型
            imageMap['pk_tradetype'] = selectedData[0].values.pk_tradetype.value;//交易类型
            imageMap['pk_org'] = selectedData[0].values.pk_org.value;//组织
            //查询数据
            imageView(imageMap, 'iweb');
            break;

        //联查审批意见
        case 'linkApproveBtn':
            linkApproveBtn.call(this);
            break;

        //打印
        case 'printBtn':
            print(
                'pdf',  //支持两类: 'html'为模板打印, 'pdf'为pdf打印
                Templatedata.settleprint,  // 后台url
                {
                    //billtype:'D5',      //单据类型
                    // funcode:'360704SML',   //功能节点编码，即模板编码
                    appcode: Templatedata.bill_funcode,
                    nodekey: 'NCC360704SM',     //模板节点标识
                    // printTemplateID:'0001A810000000049OXO',  //模板id
                    oids: pks
                }
            );
            break;
        // 输出按钮
        case 'outputBtn':
            let outputdata = {
                // funcode:'20521030',      //小应用编码
                nodekey: 'NCC360704SM',     //模板节点标识
                appcode: Templatedata.bill_funcode,
                oids: pks,    // 功能节点的数据主键  oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印
                outputType: 'output'
            }
            this.setState({
                'outputdata': outputdata
            }, () => {
                this.refs.printOutput.open()
            })
            break;
        //修改
        case 'editBtn':
            const formId = 'form_settle_tail';
            const tableId = Templatedata.card_tableid;
            let meta = this.props.meta.getMeta();
            meta[formId].items.map((item) => {
                if (item.attrcode == 'def12') {
                    let pk_org = props.form.getFormItemsValue('table_settle_head', 'pk_org')&&props.form.getFormItemsValue('table_settle_head', 'pk_org').value;
                    let pk_currtype = props.table.getAllTableData('table_settle_detail').rows[0].values.pk_currtype.value;
                        item.queryCondition =  {
                                pk_org:pk_org,
                                pk_currtype:pk_currtype,
                                GridRefActionExt:'nccloud.web.cmp.settlementmanagement.action.Def12QuertByPk'
                        }
                }
            })
            let status = 'edit';
            this.props.cardTable.setStatus(this.tableId, status);
            this.props.form.setFormStatus(this.formId, status);
            this.props.button.setButtonVisible(Templatedata.allBtn, false);
            this.props.button.setButtonVisible(Templatedata.saveGroup, true);
            this.props.button.setButtonVisible(Templatedata.splitGroup, true);
            this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false);
            props.setUrlParam({ 'status': 'edit' });
            // 组织多版本
            this.formMultiVersionProcess();
            // 设置编辑性
            this.setEditableByDirection();
            break;
        //附件
        case 'additionBtn':
            //toast({ color: 'info', content: '功能待开发' });
            let additionbillno = selectedData[0].values.billcode.value;
            let additionbillid = selectedData[0].values.pk_settlement.value;

            this.setState({
                showUploader: !this.state.showUploader,
                target: null
            })
            this.billId = additionbillid;
            this.billno = additionbillno;
            break;
        //拆行
        case 'splitLineBtn':

            if (!checked || checked.length != 1) {
                toast({ color: 'warning', content: this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000009') });/* 国际化处理： 请选择一行子表数据！*/
                return;
            }
            let splitdata = deepClone(checked[0]);
            /****
             * bodyVO.setPk_detail(null);
			if(bodyVO.getDirection().equals(CmpConst.Direction_Pay)){
				bodyVO.setPay(new UFDouble(0.0, currDigit));
				bodyVO.setPaylocal(new UFDouble(0.0));
				bodyVO.setGrouppaylocal(new UFDouble(0.0));
				bodyVO.setGlobalpaylocal(new UFDouble(0.0));
				bodyVO.setPay_last(UFDouble.ZERO_DBL);
				bodyVO.setPaylocal_last(UFDouble.ZERO_DBL);
				bodyVO.setGlobalpaylocal_last(UFDouble.ZERO_DBL);
				bodyVO.setGrouppaylocal_last(UFDouble.ZERO_DBL);
			}
			if(bodyVO.getDirection().equals(CmpConst.Direction_Receive)){
				bodyVO.setReceive(new UFDouble(0.0, currDigit));
				bodyVO.setReceivelocal(new UFDouble(0.0));
				bodyVO.setGroupreceivelocal(new UFDouble(0.0));
				bodyVO.setGlobalreceivelocal(new UFDouble(0.0));

			} 
			bodyVO.setBankrelated_code(null);
			bodyVO.setSettlelineno(row + 1);
             * 
             * 
             */
            // 组装数据
            let bodyvo = splitdata.data.values;
            let splitindex = splitdata.index;
            splitdata.data.status = 2;
            bodyvo.pk_detail = { value: '' };
            let direction = bodyvo.direction.value;
            let bodypaymny = bodyvo.pay.value;
            let paydigit = bodyvo.pay.scale;//付款原币金额精度
            let receivedigit = bodyvo.receive.scale;//收款原币金额精度

            let zerovalue = { value: '0', scale: paydigit };
            let receive_zerovalue = { value: '0', scale: receivedigit };
            // 0收1付
            if (direction == 1) {
                bodyvo.pay = zerovalue;
                bodyvo.paylocal = zerovalue;
                bodyvo.grouppaylocal = zerovalue;
                bodyvo.globalpaylocal = zerovalue;
                bodyvo.pay_last = zerovalue;
                bodyvo.paylocal_last = zerovalue;
                bodyvo.globalpaylocal_last = zerovalue;
                bodyvo.grouppaylocal_last = zerovalue;
            }
            if (direction == 0) {
                bodyvo.receive = receive_zerovalue;
                bodyvo.receivelocal = receive_zerovalue;
                bodyvo.groupreceivelocal = receive_zerovalue;
                bodyvo.globalreceivelocal = receive_zerovalue;
            }
            let settlelineno = bodyvo.settlelineno.value;
            bodyvo.settlelineno = { value: settlelineno + 1 };
            this.props.cardTable.insertRowsAfterIndex(this.tableId, splitdata.data, splitindex);
            break;
        //合并行
        case 'combineLineBtn':
            if (!checked || checked.length == 1) {
                toast({ color: 'warning', content: this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000010') });/* 国际化处理： 请选择要合并的子表数据！*/
                return;
            }
            /***bodyVO.getPk_billdetail()+ bodyVO.getBusilineno() */
            let combin = checked;
            let set = new Set();
            let pk_details = [];
            let pk_detail = '';
            let pk_settlement = [];
            let indexs = [];
            combin.forEach((val, index) => {
                let valvalues = val.data.values;
                indexs.push(val.index);
                let billdetail = valvalues.pk_billdetail.value;
                let busilineno = valvalues.busilineno.value;
                set.add(billdetail + busilineno);
                if (index == 0) {
                    pk_settlement.push(valvalues.pk_settlement.value);
                    pk_detail = valvalues.pk_detail.value;
                } else {
                    // 选中第一条作为子表基准，其他的往第一条上合并，防止后台pk_detail乱了
                    pk_details.push(valvalues.pk_detail.value);
                }
            });
            if (set.size != 1) {
                toast({ color: 'warning', content: this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000011') });/* 国际化处理： 不是同一行拆分出来的不能合并行！*/
                return;
            }
            // 合并行ajax
            let combindata = {
                pk_details: pk_details,
                pk_detail: pk_detail,
                pks: pk_settlement,
                pageid: Templatedata.card_pageid
            }
            ajax({
                url: Templatedata.settlecombinline,
                data: combindata,
                success: (res) => {
                    let { success, data } = res;
                    if (success) {
                        //toast({ color: 'success', content: '委托办理成功' });
                        if (res.data.body) {
                            //this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);

                            // updateDataByIndexs('tableDemo', [{index: 1,data: {status: '0,values: {}}}, ...])
                            // 数组移除并返回第一个
                            let updateindex = indexs.shift();
                            this.props.cardTable.updateDataByIndexs(this.tableId, [{ index: updateindex, data: res.data.body[this.tableId].rows[0] }]);
                            this.props.cardTable.delRowsByIndex(this.tableId, indexs);

                            //(this.tableId, res.data.body[this.tableId]);

                            // this.props.cardTable.setEditableByKey(this.tableId, '0', 'local_rate', false);
                            // let directions = res.data.body[this.tableId].rows;
                            // directions.forEach((ele , index) => {
                            // 	let direction = ele.values.direction.value;
                            // 	if (direction == 0) {
                            // 		//0收1付,0将付款原币金额pay置为不可编辑,1将收款原币置为不可编辑receive
                            // 		this.props.cardTable.setEditableByIndex(this.tableId, index, 
                            // 				['pay','pk_currtype_last','pk_inneraccount'], false);
                            // 		//this.props.cardTable.setEditableByIndex(this.tableId, index, '', false);
                            // 	}else {
                            // 		this.props.cardTable.setEditableByIndex(this.tableId, index, 'receive', false);
                            // 	}
                            // });
                        }
                    }
                }
            });

            /***
             * while(iter.hasNext()) {
				SettlementBodyVO nextbody = iter.next();
				SettleUtils.convertNullToDefaultValue(new SettlementBodyVO[]{nextbody});
				body.setPay(nextbody.getPay().add(body.getPay()));
				body.setPaylocal(nextbody.getPaylocal().add(body.getPaylocal()));
				body.setGrouppaylocal(nextbody.getGrouppaylocal().add(body.getGrouppaylocal()));
				body.setGlobalpaylocal(nextbody.getGlobalpaylocal().add(body.getGlobalpaylocal()));

				if(head.getDirection().equals(Direction.PAY.VALUE)){
					body.setPay_last(nextbody.getPay_last().add(body.getPay_last()));
					body.setPaylocal_last(nextbody.getPaylocal_last().add(body.getPaylocal_last()));
					body.setGrouppaylocal_last(nextbody.getGrouppaylocal_last().add(body.getGrouppaylocal_last()));
					body.setGlobalpaylocal_last(nextbody.getGlobalpaylocal_last().add(body.getGlobalpaylocal_last()));
					SettleUtils.setRealPayInfo(body, head.getBusi_billdate());
				}
				body.setReceive(nextbody.getReceive().add(body.getReceive()));
				body.setReceivelocal(nextbody.getReceivelocal().add(body.getReceivelocal()));
				body.setGroupreceivelocal(nextbody.getGroupreceivelocal().add(body.getGroupreceivelocal()));
				body.setGlobalreceivelocal(nextbody.getGlobalreceivelocal().add(body.getGlobalreceivelocal()));

			}
             * 
             */
            // 被合并的行变成这一个
            // let bodyvo = combin[0];
            // let bodycon = bodyvo.data.values;
            // combin.forEach((val,index)=>{
            //     if (index == 0) {
            //         continue;
            //     }
            //     let valvo = val.data.values;
            //     bodycon.pay = {value:Number(valvo.pay.value) + Number(bodycon.pay.value)};
            //     bodycon.paylocal = {value:Number(valvo.paylocal.value) + Number(bodycon.paylocal.value)};
            //     bodycon.grouppaylocal = {value:Number(valvo.grouppaylocal.value) + Number(bodycon.grouppaylocal.value)};
            //     bodycon.globalpaylocal = {value:Number(valvo.globalpaylocal.value) + Number(bodycon.globalpaylocal.value)};
            //     if (bodycon.direction.value == 1) {
            //         bodycon.pay_last = {value:Number(valvo.pay_last.value) + Number(bodycon.pay_last.value)};
            //         bodycon.paylocal_last = {value:Number(valvo.paylocal_last.value) + Number(bodycon.paylocal_last.value)};
            //         bodycon.grouppaylocal_last = {value:Number(valvo.grouppaylocal_last.value) + Number(bodycon.grouppaylocal_last.value)};
            //         bodycon.globalpaylocal_last = {value:Number(valvo.globalpaylocal_last.value) + Number(bodycon.globalpaylocal_last.value)};

            //     }
            //     bodycon.receive = {value:Number(valvo.receive.value) + Number(bodycon.receive.value)};
            //     bodycon.receivelocal = {value:Number(valvo.receivelocal.value) + Number(bodycon.receivelocal.value)};
            //     bodycon.groupreceivelocal = {value:Number(valvo.groupreceivelocal.value) + Number(bodycon.groupreceivelocal.value)};
            //     bodycon.globalreceivelocal = {value:Number(valvo.globalreceivelocal.value) + Number(bodycon.globalreceivelocal.value)};


            // });



            break;
        //制单，
        case 'makebillBtn':
            makebillBtn.call(this);
            break;
        //更多按钮组里的
        case 'refreshBtn':
            this.refreshCard(pk, null, true);
            break;
        case 'settlePayChangeBtn':
            // 支付变更
            if (!checked || checked.length == 0) {
                toast({ color: 'warning', content: this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000063') });/* 国际化处理： 请选择需要支付变更的子表数据!*/
                return;
            }
            //新增-->支付变更只能表体单条进行操作
            if (!checked || checked.length > 1) {
                toast({ color: 'warning', content: '只能选择一条记录进行支付变更！' });/* 国际化处理： 请选择需要支付变更的子表数据!*/
                return;
            }
            let paychengevo = checked[0];
            // 子表的支付状态，2为支付失败
            let settlevalue = paychengevo.data.values.settlestatus.value;
            if (!settlevalue || settlevalue != '2') {
                toast({ color: 'warning', content: this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000064') });/* 国际化处理： 支付失败的单据才可进行支付变更!*/
                return;
            }
            let pk_org = paychengevo.data.values.pk_org.value;
            if (!pk_org) {
                toast({ color: 'warning', content: this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000064') });/* 国际化处理： 支付失败的单据才可进行支付变更!*/
                return;
            }
            // 主表主键
            let changepk_settlement = paychengevo.data.values.pk_settlement.value;
            // 子表主键
            let changepk_detail = paychengevo.data.values.pk_detail.value;
            let changeddata = {
                pk_org: pk_org,
                pk: changepk_settlement,
                ts: paychengevo.data.values.ts.value
            };
            ajax({
                url: Templatedata.settlePayChangeValidate,
                data: changeddata,
                success: (res) => {
                    let { success, data } = res;
                    if (success) {
                        // 表示校验成功，可以进行支付变更。需要跳转去支付变更新增页
                        props.openTo(Templatedata.settlechangeurl,
                            {
                                status: 'add',   // 页面状态
                                srcid: changepk_settlement,
                                src: 'settlement',
                                pk_detail: changepk_detail,
                                appcode: Templatedata.settlechangeappcode,
                                pagecode: Templatedata.settlechangepagecode
                            });
                    }
                }
            });
            break;

        case 'collectionBtn':
            this.openmodal()
            break
        default:
            break
    }
}
