import { base, ajax } from 'nc-lightapp-front';
let { NCPopconfirm } = base;
import tableButtonClick from './tableButtonClick';
import { Templatedata } from "../../config/Templatedata";//配置的id和area信息
const formId = Templatedata.card_formid;
const tableId = Templatedata.card_tableid;
const pageId = Templatedata.card_pageid;
const moudleId = Templatedata.list_moduleid;
let bill_funcode = Templatedata.bill_funcode;
// 先引入参照，进行表格参照过滤，核算归属权参照
import BankaccSubDefaultGridTreeRef from '../../../../../uapbd/refer/pub/BankaccSubDefaultGridTreeRef';
// 这个是现金账户
import CashAccountGridRef from '../../../../../uapbd/refer/sminfo/CashAccountGridRef';
// 使用权参照
import BankaccSubUseTreeGridRef from '../../../../../uapbd/refer/pub/BankaccSubUseTreeGridRef';
// import { RowDetailButton } from "../../../../../tmpub/pub/util/RowDetailButton";
export default function (props) {
	let _this = this;
	props.createUIDom(
		{
			pagecode: pageId, //页面id
			//appid: Templatedata.card_appid//注册按钮的id
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(_this,props, meta,_this);
					props.meta.setMeta(meta);
					if (props.getUrlParam('status') == 'edit') {
						props.cardTable.setStatus(tableId, 'edit');
					} else {
						props.cardTable.setStatus(tableId, 'browse');
						//  props.cardTable.setEditableByIndex(tableId, 0, 'pk_currtype', false);
					}
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button, () => {
						let status = props.getUrlParam('status');
						let billstatus = props.getUrlParam('billno');//获取单据状态
						let flag = status === 'browse' ? false : true;
						// 此处不控制按钮显隐性，由index里控制
						// if (status != 'browse') {
						// 	//新增or修改or复制：保存，保存提交，保存新增，取消，附件
						// 	props.button.setButtonVisible(Templatedata.allBtn, false);

						// } else {
						// 	//浏览态状态过滤	
							
						// }
					});
				}
			}
		}
	)
}

function modifierMeta(props, meta, that) {
	let status = props.getUrlParam('status');
	meta[formId].status = status;
	meta[tableId].status = status;

	//参照过滤-现金账户
	//条件根据pk_org进行过滤
	// meta[formId].items.map((item) => {
	// 	if (item.attrcode == 'mon_account') {
	// 		item.queryCondition = () => {
	// 			let data = props.form.getFormItemsValue(formId, 'pk_org').value;
	// 			return { pk_org: data };
	// 		};
	// 	}
	// });
	//table中现金账户
	meta[tableId].items.map((item) => {
		// if (item.attrcode == 'mon_account') {
		// 	item.render = function(text, record, index) {
		// 		return (
		// 			CashAccountGridRef({
		// 			  queryCondition: () => {
		// 				let cash_org_data = props.form.getFormItemsValue(formId, 'pk_org').value;
		// 					return {
		// 						pk_org: cash_org_data // 这里对record.values.materiel要做一下非空校验
		// 					};
		// 				}
		// 			})
		// 		);
		// 	}
		// };
		// 结算方式
		// if (item.attrcode == 'pk_balatype') {
		// 	item.render = function(text, record, index) {
		// 		return (
		// 			CashAccountGridRef({
		// 			  queryCondition: () => {
		// 				let cash_org_data = props.form.getFormItemsValue(formId, 'pk_org').value;
		// 					return {
		// 						pk_org: cash_org_data // 这里对record.values.materiel要做一下非空校验
		// 					};
		// 				}
		// 			})
		// 		);
		// 	}
		// };
		// 本方账户
		if (item.attrcode == 'pk_account') {
			item.showHistory = false;
			item.render = function(text, record, index) {
				return (
					BankaccSubUseTreeGridRef({
						queryCondition : () => {
						let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
							return {
								pk_org: pk_org, // 这里对record.values.materiel要做一下非空校验
								noConditionOrg:'Y',
								refnodename:props.MutiInit.getIntl("360704SM") && props.MutiInit.getIntl("360704SM").get('360704SM-000015')/* 国际化处理： 使用权参照*/
							};
						}
					})
				);
			}
		};
		// 现金账户
		if (item.attrcode == 'pk_cashaccount') {
			item.showHistory = false;
			item.render = function(text, record, index) {
				return (
					CashAccountGridRef({
					  queryCondition: () => {
						let cash_org_data = props.form.getFormItemsValue(formId, 'pk_org').value;
							return {
								pk_org: cash_org_data ,// 这里对record.values.materiel要做一下非空校验
								refnodename:props.MutiInit.getIntl("360704SM") && props.MutiInit.getIntl("360704SM").get('360704SM-000072')/* 国际化处理： 现金账户*/
							};
						}
					})
				);
			}
		};
		// 对方账户
		if (item.attrcode == 'oppaccount') {
			item.showHistory = false;
		};
		if (item.attrcode == 'pk_oppaccount') {
			item.showHistory = false;
		};
	// 	if (item.attrcode == 'pk_org') {
	// 		item.render = function (text, record, index) {
	// 			item.queryCondition = () => {
	//                 return {
	//                     funcode: bill_funcode,
	//                     TreeRefActionExt: 'nccloud.web.cmp.ref.CMPUserPermissionOrgBuilder'
	//                 };
	//             }
	// 		}
	// 	}
		//财务组织:权限过滤
		// meta[tableId].items.find((e) => e.attrcode === 'pk_org').isTreelazyLoad = false;
	});
	let multiLang = props.MutiInit.getIntl(moudleId);
	let porCol = {
		attrcode: 'opr',
		label: this.props.MutiInit.getIntl("360704SM") && this.props.MutiInit.getIntl("360704SM").get('360704SM-000016'),/* 国际化处理： 操作*/
		fixed: 'right',
		itemtype: 'customer',
		visible: true,
		width: '200px',
		render(text, record, index) {
			let buttonAry =
				props.getUrlParam("status") === "browse"
				? ["openbrowse"]
				: ['editmoreBtn', "copylineBtn", "addlineBtn", "deletelineBtn"];
			// return props.button.createOprationButton(buttonAry, {
			// 	area: Templatedata.card_body_inner,
			// 	buttonLimit: 3,
			// 	onButtonClick: (props, key) => tableButtonClick(props, key, text, record, index)
			// });
			return props.getUrlParam("status") === 'browse' ?
				// 浏览态
				<span onClick={
					() => props.cardTable.toggleRowView(Templatedata.card_tableid,record)
				} >
					{record.expandRowStatus ? that.getLangCode('000085'):that.getLangCode('000084')}
				</span>
				// <RowDetailButton
				// 	record={record}
				// 	bodyAreaCode={Templatedata.card_tableid}
				// 	props={props}
				// />
				: 
				// 编辑态
				props.button.createOprationButton(buttonAry, {
					area: Templatedata.card_body_inner,
					buttonLimit: 3,
					onButtonClick: (props, key) => tableButtonClick(props, key, text, record, index)
				});

		}
	};
	meta[tableId].items.push(porCol);

	return meta;
}
