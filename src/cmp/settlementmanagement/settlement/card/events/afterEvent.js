import { ajax, toast } from 'nc-lightapp-front';
import { Templatedata } from "../../config/Templatedata";//配置的id和area信息
import { NoteTypeHandle, ObjectTypeHandle } from "../buttonClick/ReferChangeEvent.js";

export default function afterEvent(props, moduleId, key, value, changedrows, i, s, g) {

	console.log(props, moduleId, key, value, changedrows, i, s, g)
    
	let tablie_id = Templatedata.card_tableid;
	let form_id = Templatedata.card_formid;
	let page_id = Templatedata.card_pageid;
	//收款原币金额编辑后事件
	if (key === 'receive') {
		
		//table中编辑后事件
		//table中编辑后事件操作表格该行i
		if (moduleId === tablie_id) {
			let table_pk_oppaccount = value.refpk;
			//实付组织本币汇率
			let zuzhibenbihuilv = props.cardTable.getValByKeyAndIndex(moduleId, i, 'paylocalrate_last');
			//let zuzhibenbihuilv2 = changedrows'paylocalrate_last';

			//实付套汇汇率
			let taohuihuilv = props.cardTable.getValByKeyAndIndex(moduleId, i, 'changerate');

			//子表-->实付原币金额：pay_last     实付本币金额：paylocal_last
			//汇率：
			//主表-->原币金额：primal
			//联动赋值
			props.cardTable.setValByKeyAndIndex(tablie_id, i, 'oppaccountcode', { value: table_pk_oppaccount, display: table_pk_oppaccount });//给表体字段赋值
	
			// 联动赋值：收款本币金额=收款原币金额*本币汇率
			let money_value = value;
			let money_rate = props.cardTable.getValByKeyAndIndex(tablie_id, i, 'localrate');// 本币汇率
			if (money_value && money_rate && money_rate.value) {
				money_rate = money_rate.value;
				let money_local_money = (money_value * money_rate).toFixed(2);//保留2位小数
				props.cardTable.setValByKeyAndIndex(tablie_id, i, 'receivelocal', { value: money_local_money, display: money_local_money });//给收款本币金额字段赋值
			}
		}
	}
	//付款原币金额编辑后事件
	if (key === 'pay') {
		//table中编辑后事件
		//table中编辑后事件操作表格该行i
		if (moduleId === tablie_id) {
			//let table_pk_oppaccount = value.refpk;
			//实付组织本币汇率
			let zuzhibenbihuilv = props.cardTable.getValByKeyAndIndex(moduleId, i, ['paylocalrate_last']);
			let currtype_table_data = props.createBodyAfterEventData(page_id, form_id, moduleId, moduleId, key, changedrows, i);
			let benbihuilv = currtype_table_data.card.body.table_settle_detail.rows["0"].values.paylocalrate_last;

			//实付套汇汇率
			let taohuihuilv = props.cardTable.getValByKeyAndIndex(moduleId, i, 'changerate');
			//子表-->实付原币金额：pay_last     实付本币金额：paylocal_last
			//汇率：
			//主表-->原币金额：primal
			let paylast = Number(value);
			let paymny = value;
			let pay_last = (Number(value)/Number(benbihuilv.value)).toFixed(2);
			//联动赋值
			/**
			 * 付款原币金额：pay；
			 * 付款组织本币金额：paylocal = pay
			 * 需要联动----->
			 * 组织本币汇率：paylocalrate_last
			 * 实付套汇汇率：changerate
			 * 子表-->
			 * 实付原币金额：pay_last = pay/汇率    
			 * 实付本币金额：paylocal_last = pay
			 * 汇兑差额：0
			 */
			props.cardTable.setValByKeyAndIndex(tablie_id, i, 'pay_last', { value: pay_last, display: pay_last });//给表体字段赋值			
			props.cardTable.setValByKeyAndIndex(moduleId, i, 'paylocal_last', { value: paymny, display: paymny });
			props.cardTable.setValByKeyAndIndex(moduleId, i, 'paylocal', { value: paymny, display: paymny });
										
		}
	}
	//实付币种修改，需要联动汇率
	if (key === 'pk_currtype_last') {
		let currtype = value;
		let currcode = value.refcode;  //币种code
		let currname = value.refname;  //币种名称
		let currpk   = value.refpk;    //币种pk
		let currhuilv = value.values.currdigit.value;  //貌似是精度

		
		//此处去根据币种查询汇率
		//清除table选择的收款银行信息
		// props.cardTable.setValByKeyAndIndex(moduleId, i, 'pk_account', { value: null, display: null });//给表体字段赋值
		// props.cardTable.setValByKeyAndIndex(moduleId, i, 'pk_oppaccount', { value: null, display: null });//给表体字段赋值

		let table_org = props.form.getFormItemsValue(form_id, 'pk_org').value;
		let table_org_dly = props.form.getFormItemsValue(form_id, 'pk_org').display;
		let table_currtype = props.cardTable.getValByKeyAndIndex(moduleId, i, 'pk_currtype');

		if (table_currtype && table_currtype.display) {
			//请求后台获取币种和汇率
			let currtype_table_data = props.createBodyAfterEventData(page_id, form_id, moduleId, moduleId, key, changedrows, i);
			let currtype_table_newvalue = currtype_table_data.changedrows[0].newvalue.value;
			let currtype_table_oldvalue = currtype_table_data.changedrows[0].oldvalue.value;
			if (currtype_table_newvalue != currtype_table_oldvalue) {
				// 此处将pk_currtype字段更换为pk_currtype_last字段的值，因为后台取币种取的pk_currtype，
				// 而结算字表取的是pk_currtype_last
				let currtype = currtype_table_data.card.body.table_settle_detail.rows["0"].values.pk_currtype;
				let currtype_last = currtype_table_data.card.body.table_settle_detail.rows["0"].values.pk_currtype_last;
				if(currtype.value != currtype_last.value){
					currtype_table_data.card.body.table_settle_detail.rows["0"].values.pk_currtype_last = currtype;
					currtype_table_data.card.body.table_settle_detail.rows["0"].values.pk_currtype = currtype_last;
				}
				ajax({
					url: '/nccloud/cmp/recbill/recbillcurrtypebodyafterevent.do',
					data: currtype_table_data,
					success: (res) => {
						if (res.success) {
							if (res.data) {
								if (res.data.head) {
									//页面渲染
									// props.form.setAllFormValue({ [form_id]: res.data.head[form_id] });
									//币种保持不变
									//props.cardTable.setValByKeyAndIndex(moduleId, i, key, { value: form_currtype, display: form_currtype_dly });//给表体币种字段赋值
								}
								if (res.data.body) {
									/**
									 * 本币：是该组织的本位币，如在中国就是人民币
									 * 原币：是单据选择的支付币种，
									 * 
									 * 
									 * 付款原币金额：pay；
									 * 付款组织本币金额(需要组织币种，和业务币种的汇率)：paylocal * (paylocal/pay)	
									 * 需要联动----->
									 * 实付组织本币汇率：paylocalrate_last
									 * 实付套汇汇率：changerate
									 * 子表-->
									 * 实付币种：currtype_last
									 * 实付原币金额：pay_last = pay * 汇率    
									 * 实付组织本币金额：paylocal_last = pay_last * 汇率
									 * 汇兑差额： changebalance = paylocal_last - paylocal
									 * 实付全局本币金额: globalpaylocal_last
									 */
									//付款原币金额
									let paymny = props.cardTable.getValByKeyAndIndex(moduleId, i, 'pay').value;
									let paylocal = props.cardTable.getValByKeyAndIndex(moduleId, i, 'paylocal').value;
									let paylocalrate = Number(paylocal)/Number(paymny);
									//返回币种汇率
									let table_rate = res.data.body[moduleId].rows[i].values.local_rate;
									//计算汇率金额
									if (table_rate && table_rate.value) {
										//实付原币金额
										let actmny_last = (Number(paymny)*Number(table_rate.value)).toFixed(2);
										//汇兑差额
										//实付组织本币金额
										let paylocal_last = (Number(paymny)*Number(paylocalrate)).toFixed(2);
										let changebalancemny = paylocal_last - paymny;
										props.cardTable.setValByKeyAndIndex(moduleId, i, 'paylocal_last', { value: paylocal_last, display: paylocal_last });
										props.cardTable.setValByKeyAndIndex(moduleId, i, 'paylocal', { value: paymny, display: paymny });
										props.cardTable.setValByKeyAndIndex(moduleId, i, 'paylocalrate_last', { value: table_rate.value, display: table_rate.value });
										props.cardTable.setValByKeyAndIndex(moduleId, i, 'changerate', { value: table_rate.value, display: table_rate.value });
										props.cardTable.setValByKeyAndIndex(moduleId, i, 'pay_last', { value: actmny_last, display: actmny_last });
										props.cardTable.setValByKeyAndIndex(moduleId, i, 'changebalance', { value: changebalancemny, display: changebalancemny });
									}


									//所选币种
									// let or_table_val = props.cardTable.getValByKeyAndIndex(moduleId, i, 'pk_currtype').value;
									// let or_table_dly = props.cardTable.getValByKeyAndIndex(moduleId, i, 'pk_currtype').display;

									// //props.cardTable.setTableData(moduleId, res.data.body[moduleId]);
									// //返回本币币种
									// let re_table_currtype = res.data.body[moduleId].rows[i].values.pk_currtype.value;
									// //返回币种汇率
									// //let re_table_rate = res.data.body[moduleId].rows[i].values.local_rate;
									// //返回的原币金额，不需要
									// let re_table_localmoney = res.data.body[moduleId].rows[i].values.rec_local;

									// //给表体币种字段赋值
									// if (re_table_rate && re_table_rate.value) {
									// 	props.cardTable.setValByKeyAndIndex(moduleId, i, 'local_rate', { value: re_table_rate.value, display: re_table_rate.value });
									// }
									// //给表体币种字段赋值
									// if (re_table_localmoney && re_table_localmoney.value) {
									// 	props.cardTable.setValByKeyAndIndex(moduleId, i, 'rec_local', { value: re_table_localmoney.value, display: re_table_localmoney.value });
									// }

									// if (or_table_val == re_table_currtype) {
									// 	//1.组织当前币种为组织本币时，组织本币汇率恒等于1,且不可编辑
									// 	props.cardTable.setEditableByIndex(moduleId, i, 'local_rate', false);
									// } else {
									// 	//2,交易币种和本币不一致，后台返回币种，并且可以编辑
									// 	props.cardTable.setEditableByIndex(moduleId, i, 'local_rate', true);
									// }

								}
							} else {
								// props.form.setAllFormValue({ [moduleId]: { rows: [] } });
								// props.cardTable.setTableData(tablie_id, { rows: [] });
							}
						}
					}
				});
			}
		}
	}

	//收款原币金额,仅限table中操作不涉及form
	//录入原币金额后，应自动计算单价price /数量rec_count/pay_count
	//price = rec_primal/rec_count
	if (key === 'rec_primal') {
		let money_value = value;

		let money_currtype = props.cardTable.getValByKeyAndIndex(tablie_id, i, 'pk_currtype');
		let money_rate = props.cardTable.getValByKeyAndIndex(tablie_id, i, 'local_rate');

		if (money_currtype && money_currtype.value && money_value && money_rate && money_rate.value) {
			money_currtype = money_currtype.value;
			money_rate = money_rate.value;
			let money_local_money = (money_value * money_rate).toFixed(2);//保留2位小数
			props.cardTable.setValByKeyAndIndex(tablie_id, i, 'rec_local', { value: money_local_money, display: money_local_money });//给表体字段赋值
		}

		//第一行数据赋值给form
		// let money_rec_primal = props.cardTable.getValByKeyAndIndex(tablie_id, 0, 'rec_primal');
		// let money_rec_local = props.cardTable.getValByKeyAndIndex(tablie_id, 0, 'rec_local');

		// if (money_rec_primal && money_rec_primal.value && money_rec_local && money_rec_local.value) {
		// 	props.form.setFormItemsValue(form_id, { 'primal_money': { display: money_rec_primal.value, value: money_rec_primal.value } });//原币金额
		// 	props.form.setFormItemsValue(form_id, { 'local_money': { display: money_rec_local.value, value: money_rec_local.value } });//本币金额
		// }

	}

	//可以进行编辑的汇率
	if (key === 'local_rate') {

		//form中汇率编辑后事件
		if (moduleId === form_id) {
			let local_rate_value = value.value;
			let primal_money_form = props.form.getFormItemsValue(moduleId, 'primal_money').value;
			let primal_money_form_dly = props.form.getFormItemsValue(moduleId, 'primal_money').display;
			if (primal_money_form && local_rate_value && primal_money_form_dly) {
				let local_money_form = (primal_money_form * local_rate_value).toFixed(2);
				if (local_money_form) {
					//form中赋值
					props.form.setFormItemsValue(moduleId, { 'local_money': { display: local_money_form, value: local_money_form } });//原币金额

				}
			}
			let money_rec_primal = props.cardTable.getValByKeyAndIndex(tablie_id, i, 'rec_primal');
			if (money_rec_primal && money_rec_primal.value) {
				let local_money_form = (money_rec_primal.value * local_rate_value).toFixed(2);
				props.cardTable.setValByKeyAndIndex(tablie_id, 0, 'rec_local', { value: local_money_form, display: local_money_form });//给表体字段赋值
			}

		}
		//table中编辑后
		if (moduleId === tablie_id) {
			//
			let local_rate_value_table = value;
			//原币金额
			let table_rec_primal = props.cardTable.getValByKeyAndIndex(tablie_id, i, 'rec_primal');
			if (table_rec_primal && table_rec_primal.value) {
				let local_money_table = (table_rec_primal.value * local_rate_value_table).toFixed(2);
				props.cardTable.setValByKeyAndIndex(tablie_id, i, 'rec_local', { value: local_money_table, display: local_money_table });//给表体字段赋值
			}
		}

	}
	//付款银行账号
	if (key === 'pk_oppaccount') {
		//1、散户的时候将对方银行账户写入对方银行账户编码中;2、非散户的时候，银行账户币种回写到表体
		//table中编辑后事件
		//table中编辑后事件操作表格该行i
		if (moduleId === tablie_id) {
			let table_pk_oppaccount = value.refpk;
			let table_objtype = props.cardTable.getValByKeyAndIndex(tablie_id, i, 'objecttype');//对象交易类型

			let pk_currtype_from_account;
			if (table_objtype) {
				table_objtype = table_objtype.value;
				if (table_objtype == '4') {
					//散户:对方银行账户编码
					props.cardTable.setValByKeyAndIndex(tablie_id, i, 'oppaccountcode', { value: table_pk_oppaccount, display: table_pk_oppaccount });//给表体字段赋值
				} else {
					//非散户
					//分别给相应字段赋值，根据银行账号返回json数据进行赋值
					// 如果修改付款账号信息，删除了收款账号的话，应当删除其他信息
					// props.cardTable.setValByKeyAndIndex(tablie_id, i, 'oppaccountname', { value: null, display: null });//给表体字段赋值
					// props.cardTable.setValByKeyAndIndex(tablie_id, i, 'oppaccountcode', { value: null, display: null });//给表体字段赋值
					// props.cardTable.setValByKeyAndIndex(tablie_id, i, 'oppaccountopenbank', { value: null, display: null });//给表体字段赋值
					// pk_currtype_from_account=pk_currtype
				}
			} else {
				// 如果修改付款账号信息，删除了收款账号的话，应当删除其他信息
				props.cardTable.setValByKeyAndIndex(tablie_id, i, 'oppaccountname', { value: null, display: null });//给表体字段赋值
				props.cardTable.setValByKeyAndIndex(tablie_id, i, 'oppaccountcode', { value: null, display: null });//给表体字段赋值
				props.cardTable.setValByKeyAndIndex(tablie_id, i, 'oppaccountopenbank', { value: null, display: null });//给表体字段赋值
			}
		}
	}
	
	//本方银行账号(本方账户)  张宏建 李子云 和代码
	if (key === 'pk_account') {
		//1、自定给accountnum  本方银行账号  赋值(display)
		//table中编辑后事件操作表格该行i
		if (moduleId === tablie_id) {
			let direction = props.cardTable.getValByKeyAndIndex(tablie_id, i, 'direction');
			//cmp_zhanghjr_begin:任意方向的都可以修改本方账号-->解决付款类型单据修改本方账户报错：签名被篡改
			if (direction && direction.value) {
				let table_pk_account = value.refcode;//账号code
				console.log('本方账号=', table_pk_account);
				let values = value.values;
				if (values) {
					let accnum = values['bd_bankaccsub.accnum'] && values['bd_bankaccsub.accnum'].value;
					console.log('本方账号accnum=', accnum);
					if (accnum) {
						props.cardTable.setValByKeyAndIndex(tablie_id, i, 'accountnum', { value: accnum, display: accnum });//给表体字段赋值
					} else {
						props.cardTable.setValByKeyAndIndex(tablie_id, i, 'accountnum', { value: null, display: null });//给表体字段赋值
					}
				}
			}
			//cmp_zhanghjr_end
		}
	}
	
//票据类型[暂时去掉影像其他功能]
  if (key === 'pk_notetype') {
	//form中编辑后事件
	if (moduleId === form_id) {
		//1-动态修改票据号字段信息
		//类型的票据大类为银行汇票或商业汇票时，票据号应该是参照型，
		//所选票据类型的票据大类为支票或其他时，票据号应该是备注型，可以手工录入。
		let note_type_val = value.value;
		if (note_type_val) {
			ajax({
				url: '/nccloud/cmp/pub/noteTypeHandler.do',
				data: { pk: note_type_val },
				success:  (res) =>  {
					NoteTypeHandle.call(this, props, moduleId, res.data.pk_notetype);
				}
			});
		}
	}
	//table中编辑后事件操作表格该行i
	if (moduleId === tablie_id) {
		//1-动态修改票据号字段信息
		//类型的票据大类为银行汇票或商业汇票时，票据号应该是参照型，
		//所选票据类型的票据大类为支票或其他时，票据号应该是备注型，可以手工录入。
		let note_type_tabl_val = value.refpk;
		if (note_type_tabl_val) {
			ajax({
				url: '/nccloud/cmp/pub/noteTypeHandler.do',
				data: { pk: note_type_tabl_val },
				success: (res) => {
					NoteTypeHandle.call(this,props, moduleId, res.data.note_type);
					// 判断是否是电票
					if(res.data.e_note){
						// 是直连电票可编辑
						props.cardTable.setEditableByIndex(tablie_id, i, "direct_ecds", true);
					} else {
						// 不是不可编辑
						props.cardTable.setEditableByIndex(tablie_id, i, "direct_ecds", false);
					}
				}
			});
		}
	}
 }
 //结算方式、本方银行账户联动
 //def11结算方式，def12本方银行账户
 if(key==='def11'||
    key==='def12' 
 ){
	let currencyFormVal = '';
	let currencyFormDly = '';
	//表头
	if (props.form.getFormItemsValue('table_settle_head', key)) {
		currencyFormVal = props.form.getFormItemsValue('table_settle_head', key).value;
		currencyFormDly = props.form.getFormItemsValue('table_settle_head', key).display;
	}
	 if (moduleId === 'table_settle_head') { 
		if (key === 'def11') {
			props.cardTable.setColValue('table_settle_detail', 'pk_balatype', {
				display: currencyFormDly,
				value: currencyFormVal
			});
		}
		else if (key === 'def12') {
			props.cardTable.setColValue('table_settle_detail', 'pk_account', {
				display: currencyFormDly,
				value: currencyFormVal
			});
		}
	 }
 }
}
