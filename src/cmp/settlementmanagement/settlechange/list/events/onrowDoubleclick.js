import { constant } from '../../config/config'
/**
 * -表格双击事件
 * @param {*}  
 */

export default function onrowDoubleclick(record, index, props, e)  {
    this.props.pushTo(constant.cardpath, {
        status: 'browse',
		billstatus: record.busistatus.value,
		id: record.pk_settlechange.value
    });
}