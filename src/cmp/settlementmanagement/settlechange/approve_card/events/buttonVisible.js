
import {
    constant
} from '../../config/config';

export const buttonVisible = function (props) {

    let status = props.getUrlParam('status');
    let btnflag = false;
    // 默认按钮隐藏
    props.button.setButtonVisible(
        [
            'editgroup',
            'editBtn',
            'deleteBtn',
            'savegroup',
            'saveBtn',
            'savesubmitBtn',
            'submitBtn',
            'unsubmitBtn',
            'cancelBtn',
            'preparenetBtn',
            'joinquery',
            'joinquerygroup',
            'linksettlementBtn',
            'bankaccbalanceBtn',
            'enclosureBtn',
            'printBtn',
            'printgroup',
            'outputBtn',
            'refreshBtn'
        ],
        btnflag
    );

    //浏览
    if (status === 'browse') {
        props.button.setButtonVisible(
            [
                'joinquery',
                'joinquerygroup',
                'linksettlementBtn',
                'bankaccbalanceBtn',
                'enclosureBtn',
                'printBtn',
                'printgroup',
                'outputBtn'
            ], !btnflag);
    }
    props.form.setFormStatus(constant.formcode1, status);
}