import { ajax, cardCache } from 'nc-lightapp-front';
import { constant,requesturl } from '../../config/config.js';
let { getCacheById, updateCache } = cardCache;
export default function (props, pks) {
    // 后台还没更新，暂不可用
    let data = {
        pk: pks,
        pageCode: constant.cpagecode
    };
    let billpk = this.billpk;
    let cardData = getCacheById(pks, this.cacheDataSource);
    if(cardData){
		this.props.form.setAllFormValue({ [this.formId]: cardData.head[this.formId] });
		// let billstatus = cardData.head[this.formId].busistatus.value;
		// buttonVisible(props,billstatus);
	}else{
        ajax({
            url: requesturl.querycard,
            data: data,
            success: (res) => {
                if (res) {
                    if (res.data) {
                        this.props.form.setAllFormValue({ [this.formId]: res.data[this.formId] });
                        let billno = res.data[this.formId].rows[0].values.vbillno.value;
                        let id = res.data[this.formId].rows[0].values[billpk].value;
                        props.setUrlParam(pks)//动态修改地址栏中的id的值
                        this.setState({ billno: ':'+billno });
                        this.props.BillHeadInfo.setBillHeadInfoVisible({
                            // showBackBtn: false, //控制显示返回按钮: true为显示,false为隐藏 ---非必传
                            showBillCode: true, //控制显示单据号：true为显示,false为隐藏 ---非必传
                            billCode: billno
                        });
                        updateCache(billpk, id, this.formId, this.cacheDataSource);
                    }
                } else {
                    this.props.form.setAllFormValue({ [this.formId]: { rows: [] } });
                }
            }
        });
    }
    
}
