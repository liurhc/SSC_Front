import { ajax } from 'nc-lightapp-front';
import { constant,requesturl } from '../../config/config.js';
export default function afterEvent(props, moduleId, key, value, changedrows, i, s, g) {
	let cpagecode=constant.cpagecode;
	let formcode1=constant.formcode1;
	if (key === 'pk_org') {
		let chagedata = {
			pageCode: cpagecode,
			pkorg: props.form.getFormItemsValue(moduleId, 'pk_org').value
		};
		if (chagedata.pkorg) {
			ajax({
				url: requesturl.orgchange,
				data: chagedata,
				success: (res) => {
					if (res.success) {
						let pk_currency = res.data[formcode1].rows[0].values.pk_currency.value;
						let currency = res.data[formcode1].rows[0].values.pk_currency.display;
						let gllcrate = res.data[formcode1].rows[0].values.gllcrate.value;
						let olcrate = res.data[formcode1].rows[0].values.olcrate.value;
						let pk_cashaccount = res.data[formcode1].rows[0].values.pk_cashaccount.value;
						let cashaccount = res.data[formcode1].rows[0].values.pk_cashaccount.display;
						props.form.setFormItemsValue(moduleId, {
							pk_currency: { display: currency, value: pk_currency }
						});
						props.form.setFormItemsValue(moduleId, {
							pk_cashaccount: { display: cashaccount, value: pk_cashaccount }
						});
						props.form.setFormItemsValue(moduleId, { olcrate: { display: olcrate, value: olcrate } });
						props.form.setFormItemsValue(moduleId, { gllcrate: { display: gllcrate, value: gllcrate } });
						props.form.getFormItemsValue();
					}
				}
			});
		}else{
			props.form.setFormItemsValue(moduleId, {
				pk_currency: { display: null, value: null }
			});
			props.form.setFormItemsValue(moduleId, {
				pk_cashaccount: { display: null, value: null }
			});
			props.form.setFormItemsValue(moduleId, { olcrate: { display: null, value: null } });
			props.form.setFormItemsValue(moduleId, { gllcrate: { display: null, value: null } });
			props.form.getFormItemsValue();
		}
		props.resMetaAfterPkorgEdit();//选择主组织以后，恢复其他字段的编辑性
	}

	if( key === 'pk_bank' ){
		props.form.setFormItemsValue(moduleId, { pk_account: { display: null, value: null } });
	}

	if( key === 'pk_account'){
		let pk_account = props.form.getFormItemsValue(moduleId, 'pk_account').value;
		let pk_bank = props.form.getFormItemsValue(moduleId, 'pk_bank').value;
		let bankdocname, pk_bankdoc, accname, accpk
		accname = i.refcode;
		accpk = i.refpk;
		props.form.setFormItemsValue(moduleId, {
			'pk_account': {
				display: accname,
				value: accpk
			}
		});

		if(pk_account.value && !pk_account.value == ''){
			if (pk_bank.value == '' || pk_bank.value == null) {
				if (i) {
					bankdocname = i.values['bd_bankdoc.name'].value;
					pk_bankdoc = i.values['bd_bankdoc.pk_bankdoc'].value;
					props.form.setFormItemsValue(moduleId, {
						'pk_bank': {
							display: bankdocname,
							value: pk_bankdoc
						}
					});
				}
			}
		}
	}
}
