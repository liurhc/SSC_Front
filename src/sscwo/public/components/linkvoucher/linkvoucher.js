import {cacheTools} from 'nc-lightapp-front';
import requestApi from "../../../../sscrp/public/common/components/requestApi";
let linkVoucherApi = {
    link : (opt) => {
        let props = opt.data.props;
        let record = opt.data.record;
        let appid = opt.data.appid;
        let appCode = opt.data.appCode;
        let pageCode = opt.data.pageCode;
        let voucherMapKey = appCode + '_LinkVouchar'
        let dataArrayForPz = [];
        dataArrayForPz.push({
            pk_billtype : record.pk_billtype,
            pk_group : record.pk_group,
            pk_org : record.pk_org,
            relationID : record.relationID
        });
        cacheTools.set(voucherMapKey,dataArrayForPz);
        requestApi.queryUrl({
            data : {
                appCode : '10170410',
                pageCode : '10170410_1017041001'
            },
            success : (data) => {
                let hostname = window.location.hostname;
                let port = window.location.port;
                props.openTo( data,
                    {
                        status : 'browse',
                        scene : voucherMapKey,
                        appcode: '10170410',
                        pagecode: '10170410_1017041001'
                    }
                );
            }
        })
    },
    preview : (opt) => {
        let props = opt.data.props;
        let record = opt.data.record;
        let appid = opt.data.appid;
        let appCode = opt.data.appCode;
        let pageCode = opt.data.pageCode;
        let voucherMapKey = appCode + '_Preview'
        let dataArrayForPz = {
            "messagevo":[{
                "billVO":record.billVO,
                "messageinfo":{
                    "pk_billtype" : record.pk_billtype,
                    "pk_group" : record.pk_group,
                    "pk_org" : record.pk_org,
                    "relationID" : record.relationID,
                    "pk_system":"WAP",
                    "busidate":record.billVO.parentVO.billdate
                }
            }],
            "desbilltype":["C0"],
            "srcbilltype":record.billVO.parentVO.billtype,
            "billID":record.relationID
        };
        cacheTools.set(voucherMapKey,dataArrayForPz);
        requestApi.queryUrl({
            data : {
                appCode : '10170410',
                pageCode : '10170410_1017041001'
            },
            success : (data) => {
                let hostname = window.location.hostname;
                let port = window.location.port;
                props.openTo( data,
                    {
                        status : 'browse',
                        scene : voucherMapKey,
                        appcode: '10170410',
                        pagecode: '10170410_1017041001'
                    }
                );
            }
        })
    }
}
export default linkVoucherApi;