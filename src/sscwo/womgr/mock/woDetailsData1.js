
let woDetailsData1 = {
	"areas": {
		"head": {
			"code": "head",
			"items": [{
					"itemtype": "input",
					"label": "工单类型主键",
					"code": "pk_doct",
					"maxlength": "20",
					"metapath": "pk_doc",
					"visible": false
				},
				{
					"itemtype": "input",
					"label": "编码",
					"code": "doc_code",
					"maxlength": "20",
					"visible": true,
					"metapath": "doc_code"
				},
				{
					"itemtype": "input",
					"label": "名称",
					"code": "doc_name",
					"maxlength": "20",
					"visible": true,
					"metapath": "doc_name"
				},
				{
					"itemtype": "refer",
					"label": "分类",
					"code": "pk_doctype",
					"maxlength": "20",
					"visible": true,
					"metapath": "pk_doctype.typename"
				},
				{
					"itemtype": "select",
					"col": "4",
					"label": "单据结构",
					"code": "doc_structure",
					"maxlength": "1",
					"visible": true,
					"metapath": "doc_structure",
					"options": [{
							"display": "单主表",
							"value": "single"
						},
						{
							"display": "主子表",
							"value": "multi"
						}
					]
				},
				{
					"itemtype": "input",
					"label": "对应主表表名称",
					"code": "doc_tablename",
					"maxlength": "20",
					"visible": true,
					"metapath": "doc_tablename"
				},
				{
					"itemtype": "input",
					"label": "对应子表表名称",
					"code": "doc_subtablename",
					"maxlength": "20",
					"visible": true,
					"metapath": "doc_subtablename"
				},
				{
					"itemtype": "input",
					"label": "发布元数据",
					"code": "ispubmetadata",
					"maxlength": "20",
					"visible": true,
					"metapath": "ispubmetadata"
				},
				{
					"itemtype": "input",
					"label": "发布小应用",
					"code": "ispubapplet",
					"maxlength": "20",
					"visible": true,
					"metapath": "ispubapplet"
				},
				{
					"itemtype": "input",
					"label": "备注",
					"code": "remark",
					"maxlength": "20",
					"visible": true,
					"metapath": "remark"
				}
			],
			"moduletype": "form",
			"name": "设置工单信息",
			"pagination": false,
			"vometa": "webdbl.wapdoc_doc",
			"clazz": "nccloud.web.sscwo.cpdoc.view.CpDocDisplayVO"
		},
		"cpdocmaster": {
			"code": "cpdocmaster",
			"items": [{
					"itemtype": "input",
					"label": "工单属性主键",
					"code": "pk_attribute",
					"maxlength": "20",
					"visible": false,
					"metapath": "pk_attribute"
				},
				{
					"itemtype": "input",
					"label": "字段名称",
					"code": "name",
					"maxlength": "20",
					"visible": true,
					"metapath": "name"
				},
				{
					"itemtype": "input",
					"label": "显示名称",
					"code": "displayname",
					"maxlength": "20",
					"visible": true,
					"metapath": "displayname"
				},
				{
					"itemtype": "input",
					"label": "类型样式",
					"code": "type_style",
					"maxlength": "20",
					"visible": false,
					"metapath": "type_style"
				}
			],
			"moduletype": "table",
			"name": "主表字段",
			"pagination": false,
            "vometa": "webdbl.wapdoc_attribute",
            "clazz": "uap.lfw.dbl.vo.CpDocAttributeVO"
		},
		"cpdocchild": {
			"code": "cpdocchild",
			"items": [{
					"itemtype": "input",
					"label": "工单属性主键",
					"code": "pk_attribute",
					"maxlength": "20",
					"visible": false,
					"metapath": "pk_attribute"
				},
				{
					"itemtype": "input",
					"label": "字段名称",
					"code": "name",
					"maxlength": "20",
					"visible": true,
					"metapath": "name"
				},
				{
					"itemtype": "input",
					"label": "显示名称",
					"code": "displayname",
					"maxlength": "20",
					"visible": true,
					"metapath": "displayname"
				},
				{
					"itemtype": "input",
					"label": "类型样式",
					"code": "type_style",
					"maxlength": "20",
					"visible": false,
					"metapath": "type_style"
				}
			],
			"moduletype": "table",
			"name": "子表字段",
			"pagination": false,
            "vometa": "webdbl.wapdoc_attribute",
            "clazz": "uap.lfw.dbl.vo.CpDocAttributeVO"
		},
		"cpdocmaster\u0026childform1": {
			"areastatus":"browse",
			"code": "cpdocmaster\u0026childform1",
			"items": [{
					"itemtype": "input",
					"label": "工单属性主键",
					"code": "pk_attribute",
					"maxlength": "20",
					"visible": false,
					"metapath": "pk_attribute"
				},
				{
					"itemtype": "input",
					"label": "字段名称",
					"code": "name",
					"maxlength": "20",
					"visible": true,
					"metapath": "name"
				},
				{
					"itemtype": "input",
					"label": "显示名称",
					"code": "displayname",
					"maxlength": "20",
					"visible": true,
					"metapath": "displayname"
				},
				{
					"itemtype": "input",
					"label": "类型样式",
					"code": "type_style",
					"maxlength": "20",
					"visible": false,
					"metapath": "type_style"
				}
			],
			"moduletype": "form",
			"name": "主表属性",
			"pagination": false,
			"relationcode":"cpdocmaster",
            "vometa": "webdbl.wapdoc_attribute",
            "clazz": "uap.lfw.dbl.vo.CpDocAttributeVO"
		},
		"cpdocmaster\u0026childform2": {
			"areastatus":"edit",
			"code": "cpdocmaster\u0026childform2",
			"items": [{
					"itemtype": "input",
					"label": "工单属性主键",
					"code": "pk_attribute",
					"maxlength": "20",
					"visible": false,
					"metapath": "pk_attribute"
				},
				{
					"itemtype": "input",
					"label": "字段名称",
					"code": "name",
					"maxlength": "20",
					"visible": true,
					"metapath": "name"
				},
				{
					"itemtype": "input",
					"label": "显示名称",
					"code": "displayname",
					"maxlength": "20",
					"visible": true,
					"metapath": "displayname"
				},
				{
					"itemtype": "input",
					"label": "类型样式",
					"code": "type_style",
					"maxlength": "20",
					"visible": false,
					"metapath": "type_style"
				}
			],
			"moduletype": "form",
			"name": "主表属性",
			"pagination": false,
			"relationcode":"cpdocmaster",
            "vometa": "webdbl.wapdoc_attribute",
			"clazz": "uap.lfw.dbl.vo.CpDocAttributeVO"
		},
		"cpdocchild\u0026childform1": {
			"areastatus":"browse",
			"code": "cpdocchild\u0026childform1",
			"items": [{
					"itemtype": "input",
					"label": "工单属性主键",
					"code": "pk_attribute",
					"maxlength": "20",
					"visible": false,
					"metapath": "pk_attribute"
				},
				{
					"itemtype": "input",
					"label": "字段名称",
					"code": "name",
					"maxlength": "20",
					"visible": true,
					"metapath": "name"
				},
				{
					"itemtype": "input",
					"label": "显示名称",
					"code": "displayname",
					"maxlength": "20",
					"visible": true,
					"metapath": "displayname"
				},
				{
					"itemtype": "input",
					"label": "类型样式",
					"code": "type_style",
					"maxlength": "20",
					"visible": false,
					"metapath": "type_style"
				}
			],
			"moduletype": "form",
			"name": "子表属性",
			"pagination": false,
			"relationcode":"cpdocchild",
            "vometa": "webdbl.wapdoc_attribute",
            "clazz": "uap.lfw.dbl.vo.CpDocAttributeVO"
		},
		"cpdocchild\u0026childform2": {
			"areastatus":"edit",
			"code": "cpdocchild\u0026childform2",
			"items": [{
					"itemtype": "input",
					"label": "工单属性主键",
					"code": "pk_attribute",
					"maxlength": "20",
					"visible": false,
					"metapath": "pk_attribute"
				},
				{
					"itemtype": "input",
					"label": "字段名称",
					"code": "name",
					"maxlength": "20",
					"visible": true,
					"metapath": "name"
				},
				{
					"itemtype": "input",
					"label": "显示名称",
					"code": "displayname",
					"maxlength": "20",
					"visible": true,
					"metapath": "displayname"
				},
				{
					"itemtype": "input",
					"label": "类型样式",
					"code": "type_style",
					"maxlength": "20",
					"visible": false,
					"metapath": "type_style"
				}
			],
			"moduletype": "form",
			"name": "子表属性",
			"pagination": false,
			"relationcode":"cpdocchild",
            "vometa": "webdbl.wapdoc_attribute",
            "clazz": "uap.lfw.dbl.vo.CpDocAttributeVO"
		}
	},
	"code": "cpdoc",
	"name": "工单",
	"headcode":"head"
};
export default woDetailsData1;
