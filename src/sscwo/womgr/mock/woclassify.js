
let woclassify = {
    "data":{
        "code": "cpdoctype",
        "name": "工单分类",
        "pageid": "cpdoctype",
        "cpdoctype": {
            "items": [
                {
                    "itemtype": "input",
                    "label": "工单分类主键",
                    "maxlength": 20,
                    "visible": true,
                    "attrcode": "pk_doctype"
                },
                {
                    "itemtype": "input",
                    "label": "工单分类编码",
                    "maxlength": 20,
                    "visible": true,
                    "attrcode": "typecode"
                },
                {
                    "itemtype": "input",
                    "label": "工单分类名称",
                    "maxlength": 20,
                    "visible": true,
                    "attrcode": "typename"
                },
                {
                    "itemtype": "label",
                    "label": "所属组织",
                    "maxlength": 20,
                    "attrcode": "pk_org"
                },
                {
                    "itemtype": "label",
                    "label": "集团",
                    "maxlength": 20,
                    "disabled": true,
                    "attrcode": "pk_group"
                },
                {
                    "itemtype": "label",
                    "label": "dr",
                    "maxlength": 3,
                    "attrcode": "dr"
                },
                {
                    "itemtype": "datepicker",
                    "label": "时间戳",
                    "maxlength": 19,
                    "attrcode": "ts"
                }
            ],
            "moduletype": "table",
            "pagination": false,
            "code": "cpdoctype",
            "name": "工单分类"
        },
        "gridrelation": {
            "cpdoctype": {
                "srcAreaCode": "cpdoctype",
                "tabRelation": [
                    "cpdoctype"
                ]
            }
        },
        "formrelation": {}
    },
    "success":true
};

export default woclassify;
