let woDetailsData = {
  "data": {
    "template": {
      "code": "4520008005_card",
      "name": "借用申请",
      "pageid": "4520008005_card",
      "bodyvos": {
        "items": [
          {
            "itemtype": "refer",
            "visible": true,
            "width": "150px",
            "label": "资产类型",
            "maxlength": "20",
            "refcode": "uapbd/refer/am/CategoryTreeRef/index.js",
            "attrcode": "pk_category"
          },
          {
            "itemtype": "input",
            "visible": true,
            "width": "150px",
            "label": "资产详细描述",
            "maxlength": "80",
            "attrcode": "spec"
          },
          {
            "itemtype": "input",
            "visible": true,
            "width": "150px",
            "label": "用途",
            "maxlength": "80",
            "attrcode": "model"
          },
          {
            "itemtype": "number",
            "visible": true,
            "width": "150px",
            "label": "数量",
            "required": true,
            "attrcode": "quantity"
          },
          {
            "itemtype": "datepicker",
            "visible": true,
            "width": "150px",
            "label": "借用日期",
            "maxlength": "19",
            "required": true,
            "attrcode": "expect_start_date"
          },
          {
            "itemtype": "datepicker",
            "visible": true,
            "width": "150px",
            "label": "预计归还日期",
            "maxlength": "19",
            "attrcode": "expect_end_date"
          },
          {
            "itemtype": "select",
            "visible": true,
            "width": "150px",
            "label": "借用情况",
            "attrcode": "borrow_condition",
            "options": [
              {
                "display": "未借用",
                "value": "0"
              },
              {
                "display": "已借用",
                "value": "1"
              },
              {
                "display": "不处理",
                "value": "2"
              }
            ]
          },
          {
            "itemtype": "input",
            "label": "申请单子表主键",
            "maxlength": "20",
            "attrcode": "pk_apply_b"
          },
          {
            "itemtype": "refer",
            "visible": true,
            "width": "150px",
            "label": "集团",
            "maxlength": "20",
            "attrcode": "pk_group"
          },
          {
            "itemtype": "refer",
            "label": "资产组织最新版本",
            "maxlength": "20",
            "attrcode": "pk_org"
          },
          {
            "itemtype": "input",
            "label": "行号",
            "maxlength": "30",
            "attrcode": "rowno"
          },
          {
            "itemtype": "refer",
            "label": "设备编码",
            "maxlength": "20",
            "attrcode": "pk_equip"
          },
          {
            "itemtype": "input",
            "label": "卡片编号",
            "maxlength": "40",
            "disabled": true,
            "attrcode": "pk_equip.card_code"
          },
          {
            "itemtype": "input",
            "visible": true,
            "width": "150px",
            "label": "期初",
            "maxlength": "1",
            "disabled": true,
            "attrcode": "pk_equip.is_pre"
          },
          {
            "itemtype": "input",
            "label": "设备名称",
            "maxlength": "200",
            "attrcode": "equip_name"
          },
          {
            "itemtype": "refer",
            "label": "物料编码",
            "maxlength": "20",
            "attrcode": "pk_material_v"
          },
          {
            "itemtype": "refer",
            "label": "物料编码版本",
            "maxlength": "20",
            "attrcode": "pk_material"
          },
          {
            "itemtype": "input",
            "visible": true,
            "width": "150px",
            "label": "序列号",
            "maxlength": "80",
            "attrcode": "serial_num"
          },
          {
            "itemtype": "refer",
            "label": "仓库",
            "maxlength": "20",
            "attrcode": "pk_warehouse"
          },
          {
            "itemtype": "datepicker",
            "label": "领用日期",
            "maxlength": "19",
            "attrcode": "start_date"
          },
          {
            "itemtype": "datepicker",
            "visible": true,
            "width": "150px",
            "label": "实际归还日期",
            "maxlength": "19",
            "attrcode": "end_date"
          },
          {
            "itemtype": "refer",
            "label": "项目",
            "maxlength": "20",
            "attrcode": "pk_jobmngfil"
          },
          {
            "itemtype": "refer",
            "label": "资产组织",
            "maxlength": "20",
            "attrcode": "pk_org_v"
          },
          {
            "itemtype": "input",
            "label": "技术要求",
            "maxlength": "200",
            "attrcode": "memo"
          },
          {
            "itemtype": "switch",
            "label": "关闭",
            "maxlength": "1",
            "disabled": true,
            "attrcode": "close_flag"
          },
          {
            "itemtype": "input",
            "label": "预置字符1",
            "maxlength": "128",
            "attrcode": "pre_text1"
          },
          {
            "itemtype": "input",
            "label": "预置字符2",
            "maxlength": "128",
            "attrcode": "pre_text2"
          },
          {
            "itemtype": "input",
            "label": "预置字符3",
            "maxlength": "128",
            "attrcode": "pre_text3"
          },
          {
            "itemtype": "input",
            "label": "预置字符4",
            "maxlength": "128",
            "attrcode": "pre_text4"
          },
          {
            "itemtype": "input",
            "label": "预置字符5",
            "maxlength": "128",
            "attrcode": "pre_text5"
          },
          {
            "itemtype": "input",
            "label": "预置字符6",
            "maxlength": "128",
            "attrcode": "pre_text6"
          },
          {
            "itemtype": "input",
            "label": "预置字符7",
            "maxlength": "128",
            "attrcode": "pre_text7"
          },
          {
            "itemtype": "input",
            "label": "预置字符8",
            "maxlength": "128",
            "attrcode": "pre_text8"
          },
          {
            "itemtype": "input",
            "label": "预置字符9",
            "maxlength": "128",
            "attrcode": "pre_text9"
          },
          {
            "itemtype": "input",
            "label": "预置字符10",
            "maxlength": "128",
            "attrcode": "pre_text10"
          },
          {
            "itemtype": "input",
            "label": "自定义项1",
            "maxlength": "101",
            "attrcode": "def1"
          },
          {
            "itemtype": "input",
            "label": "自定义项2",
            "maxlength": "101",
            "attrcode": "def2"
          },
          {
            "itemtype": "input",
            "label": "自定义项3",
            "maxlength": "101",
            "attrcode": "def3"
          },
          {
            "itemtype": "input",
            "label": "自定义项4",
            "maxlength": "101",
            "attrcode": "def4"
          },
          {
            "itemtype": "input",
            "label": "自定义项5",
            "maxlength": "101",
            "attrcode": "def5"
          },
          {
            "itemtype": "input",
            "label": "自定义项6",
            "maxlength": "101",
            "attrcode": "def6"
          },
          {
            "itemtype": "input",
            "label": "自定义项7",
            "maxlength": "101",
            "attrcode": "def7"
          },
          {
            "itemtype": "input",
            "label": "自定义项8",
            "maxlength": "101",
            "attrcode": "def8"
          },
          {
            "itemtype": "input",
            "label": "自定义项9",
            "maxlength": "101",
            "attrcode": "def9"
          },
          {
            "itemtype": "input",
            "label": "自定义项10",
            "maxlength": "101",
            "attrcode": "def10"
          },
          {
            "itemtype": "input",
            "label": "自定义项11",
            "maxlength": "101",
            "attrcode": "def11"
          },
          {
            "itemtype": "input",
            "label": "自定义项12",
            "maxlength": "101",
            "attrcode": "def12"
          },
          {
            "itemtype": "input",
            "label": "自定义项13",
            "maxlength": "101",
            "attrcode": "def13"
          },
          {
            "itemtype": "input",
            "label": "自定义项14",
            "maxlength": "101",
            "attrcode": "def14"
          },
          {
            "itemtype": "input",
            "label": "自定义项15",
            "maxlength": "101",
            "attrcode": "def15"
          },
          {
            "itemtype": "input",
            "label": "自定义项16",
            "maxlength": "101",
            "attrcode": "def16"
          },
          {
            "itemtype": "input",
            "label": "自定义项17",
            "maxlength": "101",
            "attrcode": "def17"
          },
          {
            "itemtype": "input",
            "label": "自定义项18",
            "maxlength": "101",
            "attrcode": "def18"
          },
          {
            "itemtype": "input",
            "label": "自定义项19",
            "maxlength": "101",
            "attrcode": "def19"
          },
          {
            "itemtype": "input",
            "label": "自定义项20",
            "maxlength": "101",
            "attrcode": "def20"
          },
          {
            "itemtype": "datepicker",
            "label": "时间戳",
            "maxlength": "19",
            "attrcode": "ts"
          }
        ],
        "moduletype": "table",
        "pagination": false,
        "code": "bodyvos",
        "name": "借用申请明细"
      },
      "childform1": {
        "items": [
          {
            "itemtype": "refer",
            "col": "4",
            "visible": true,
            "label": "资产类型",
            "maxlength": "20",
            "refcode": "uapbd/refer/am/CategoryTreeRef/index.js",
            "attrcode": "pk_category"
          },
          {
            "itemtype": "input",
            "col": "4",
            "visible": true,
            "label": "资产详细描述",
            "maxlength": "80",
            "attrcode": "spec"
          },
          {
            "itemtype": "input",
            "col": "4",
            "visible": true,
            "label": "用途",
            "maxlength": "80",
            "attrcode": "model"
          },
          {
            "itemtype": "input",
            "col": "4",
            "visible": true,
            "label": "数量",
            "required": true,
            "attrcode": "quantity"
          },
          {
            "itemtype": "datepicker",
            "col": "4",
            "visible": true,
            "label": "借用日期",
            "maxlength": "19",
            "required": true,
            "attrcode": "expect_start_date"
          },
          {
            "itemtype": "datepicker",
            "col": "4",
            "visible": true,
            "label": "预计归还日期",
            "maxlength": "19",
            "attrcode": "expect_end_date"
          },
          {
            "itemtype": "select",
            "col": "4",
            "visible": true,
            "label": "借用情况",
            "attrcode": "borrow_condition",
            "options": [
              {
                "display": "未借用",
                "value": "0"
              },
              {
                "display": "已借用",
                "value": "1"
              },
              {
                "display": "不处理",
                "value": "2"
              }
            ]
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "申请单子表主键",
            "maxlength": "20",
            "attrcode": "pk_apply_b"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "集团",
            "maxlength": "20",
            "attrcode": "pk_group"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "资产组织最新版本",
            "maxlength": "20",
            "attrcode": "pk_org"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "行号",
            "maxlength": "30",
            "attrcode": "rowno"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "设备编码",
            "maxlength": "20",
            "attrcode": "pk_equip"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "卡片编号",
            "maxlength": "40",
            "disabled": true,
            "attrcode": "pk_equip.card_code"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "期初",
            "maxlength": "1",
            "disabled": true,
            "attrcode": "pk_equip.is_pre"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "设备名称",
            "maxlength": "200",
            "attrcode": "equip_name"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "物料编码",
            "maxlength": "20",
            "attrcode": "pk_material_v"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "物料编码版本",
            "maxlength": "20",
            "attrcode": "pk_material"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "序列号",
            "maxlength": "80",
            "attrcode": "serial_num"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "仓库",
            "maxlength": "20",
            "attrcode": "pk_warehouse"
          },
          {
            "itemtype": "datepicker",
            "col": "4",
            "label": "领用日期",
            "maxlength": "19",
            "attrcode": "start_date"
          },
          {
            "itemtype": "datepicker",
            "col": "4",
            "label": "实际归还日期",
            "maxlength": "19",
            "attrcode": "end_date"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "项目",
            "maxlength": "20",
            "attrcode": "pk_jobmngfil"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "资产组织",
            "maxlength": "20",
            "attrcode": "pk_org_v"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "技术要求",
            "maxlength": "200",
            "attrcode": "memo"
          },
          {
            "itemtype": "switch",
            "col": "4",
            "label": "关闭",
            "maxlength": "1",
            "disabled": true,
            "attrcode": "close_flag"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符1",
            "maxlength": "128",
            "attrcode": "pre_text1"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符2",
            "maxlength": "128",
            "attrcode": "pre_text2"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符3",
            "maxlength": "128",
            "attrcode": "pre_text3"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符4",
            "maxlength": "128",
            "attrcode": "pre_text4"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符5",
            "maxlength": "128",
            "attrcode": "pre_text5"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符6",
            "maxlength": "128",
            "attrcode": "pre_text6"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符7",
            "maxlength": "128",
            "attrcode": "pre_text7"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符8",
            "maxlength": "128",
            "attrcode": "pre_text8"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符9",
            "maxlength": "128",
            "attrcode": "pre_text9"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符10",
            "maxlength": "128",
            "attrcode": "pre_text10"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项1",
            "maxlength": "101",
            "attrcode": "def1"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项2",
            "maxlength": "101",
            "attrcode": "def2"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项3",
            "maxlength": "101",
            "attrcode": "def3"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项4",
            "maxlength": "101",
            "attrcode": "def4"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项5",
            "maxlength": "101",
            "attrcode": "def5"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项6",
            "maxlength": "101",
            "attrcode": "def6"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项7",
            "maxlength": "101",
            "attrcode": "def7"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项8",
            "maxlength": "101",
            "attrcode": "def8"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项9",
            "maxlength": "101",
            "attrcode": "def9"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项10",
            "maxlength": "101",
            "attrcode": "def10"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项11",
            "maxlength": "101",
            "attrcode": "def11"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项12",
            "maxlength": "101",
            "attrcode": "def12"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项13",
            "maxlength": "101",
            "attrcode": "def13"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项14",
            "maxlength": "101",
            "attrcode": "def14"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项15",
            "maxlength": "101",
            "attrcode": "def15"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项16",
            "maxlength": "101",
            "attrcode": "def16"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项17",
            "maxlength": "101",
            "attrcode": "def17"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项18",
            "maxlength": "101",
            "attrcode": "def18"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项19",
            "maxlength": "101",
            "attrcode": "def19"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项20",
            "maxlength": "101",
            "attrcode": "def20"
          },
          {
            "itemtype": "datepicker",
            "col": "4",
            "label": "时间戳",
            "maxlength": "19",
            "attrcode": "ts"
          }
        ],
        "moduletype": "form",
        "relationcode": "bodyvos",
        "areastatus": "edit",
        "code": "childform1",
        "name": "明细子表1"
      },
      "childform2": {
        "items": [
          {
            "itemtype": "refer",
            "col": "4",
            "visible": true,
            "label": "资产类型",
            "maxlength": "20",
            "refcode": "uapbd/refer/am/CategoryTreeRef/index.js",
            "attrcode": "pk_category"
          },
          {
            "itemtype": "input",
            "col": "4",
            "visible": true,
            "label": "资产详细描述",
            "maxlength": "80",
            "attrcode": "spec"
          },
          {
            "itemtype": "input",
            "col": "4",
            "visible": true,
            "label": "用途",
            "maxlength": "80",
            "attrcode": "model"
          },
          {
            "itemtype": "number",
            "col": "4",
            "visible": true,
            "label": "数量",
            "required": true,
            "attrcode": "quantity"
          },
          {
            "itemtype": "datepicker",
            "col": "4",
            "visible": true,
            "label": "借用日期",
            "maxlength": "19",
            "required": true,
            "attrcode": "expect_start_date"
          },
          {
            "itemtype": "datepicker",
            "col": "4",
            "visible": true,
            "label": "预计归还日期",
            "maxlength": "19",
            "attrcode": "expect_end_date"
          },
          {
            "itemtype": "select",
            "col": "4",
            "visible": true,
            "label": "借用情况",
            "attrcode": "borrow_condition",
            "options": [
              {
                "display": "未借用",
                "value": "0"
              },
              {
                "display": "已借用",
                "value": "1"
              },
              {
                "display": "不处理",
                "value": "2"
              }
            ]
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "申请单子表主键",
            "maxlength": "20",
            "attrcode": "pk_apply_b"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "集团",
            "maxlength": "20",
            "attrcode": "pk_group"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "资产组织最新版本",
            "maxlength": "20",
            "attrcode": "pk_org"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "行号",
            "maxlength": "30",
            "attrcode": "rowno"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "设备编码",
            "maxlength": "20",
            "attrcode": "pk_equip"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "卡片编号",
            "maxlength": "40",
            "disabled": true,
            "attrcode": "pk_equip.card_code"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "期初",
            "maxlength": "1",
            "disabled": true,
            "attrcode": "pk_equip.is_pre"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "设备名称",
            "maxlength": "200",
            "attrcode": "equip_name"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "物料编码",
            "maxlength": "20",
            "attrcode": "pk_material_v"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "物料编码版本",
            "maxlength": "20",
            "attrcode": "pk_material"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "序列号",
            "maxlength": "80",
            "attrcode": "serial_num"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "仓库",
            "maxlength": "20",
            "attrcode": "pk_warehouse"
          },
          {
            "itemtype": "datepicker",
            "col": "4",
            "label": "领用日期",
            "maxlength": "19",
            "attrcode": "start_date"
          },
          {
            "itemtype": "datepicker",
            "col": "4",
            "label": "实际归还日期",
            "maxlength": "19",
            "attrcode": "end_date"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "项目",
            "maxlength": "20",
            "attrcode": "pk_jobmngfil"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "资产组织",
            "maxlength": "20",
            "attrcode": "pk_org_v"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "技术要求",
            "maxlength": "200",
            "attrcode": "memo"
          },
          {
            "itemtype": "switch",
            "col": "4",
            "label": "关闭",
            "maxlength": "1",
            "disabled": true,
            "attrcode": "close_flag"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符1",
            "maxlength": "128",
            "attrcode": "pre_text1"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符2",
            "maxlength": "128",
            "attrcode": "pre_text2"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符3",
            "maxlength": "128",
            "attrcode": "pre_text3"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符4",
            "maxlength": "128",
            "attrcode": "pre_text4"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符5",
            "maxlength": "128",
            "attrcode": "pre_text5"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符6",
            "maxlength": "128",
            "attrcode": "pre_text6"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符7",
            "maxlength": "128",
            "attrcode": "pre_text7"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符8",
            "maxlength": "128",
            "attrcode": "pre_text8"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符9",
            "maxlength": "128",
            "attrcode": "pre_text9"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符10",
            "maxlength": "128",
            "attrcode": "pre_text10"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项1",
            "maxlength": "101",
            "attrcode": "def1"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项2",
            "maxlength": "101",
            "attrcode": "def2"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项3",
            "maxlength": "101",
            "attrcode": "def3"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项4",
            "maxlength": "101",
            "attrcode": "def4"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项5",
            "maxlength": "101",
            "attrcode": "def5"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项6",
            "maxlength": "101",
            "attrcode": "def6"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项7",
            "maxlength": "101",
            "attrcode": "def7"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项8",
            "maxlength": "101",
            "attrcode": "def8"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项9",
            "maxlength": "101",
            "attrcode": "def9"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项10",
            "maxlength": "101",
            "attrcode": "def10"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项11",
            "maxlength": "101",
            "attrcode": "def11"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项12",
            "maxlength": "101",
            "attrcode": "def12"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项13",
            "maxlength": "101",
            "attrcode": "def13"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项14",
            "maxlength": "101",
            "attrcode": "def14"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项15",
            "maxlength": "101",
            "attrcode": "def15"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项16",
            "maxlength": "101",
            "attrcode": "def16"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项17",
            "maxlength": "101",
            "attrcode": "def17"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项18",
            "maxlength": "101",
            "attrcode": "def18"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项19",
            "maxlength": "101",
            "attrcode": "def19"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项20",
            "maxlength": "101",
            "attrcode": "def20"
          },
          {
            "itemtype": "datepicker",
            "col": "4",
            "label": "时间戳",
            "maxlength": "19",
            "attrcode": "ts"
          }
        ],
        "moduletype": "form",
        "relationcode": "bodyvos",
        "areastatus": "browse",
        "code": "childform2",
        "name": "明细子表2"
      },
      "head": {
        "items": [
          {
            "itemtype": "refer",
            "col": "4",
            "visible": true,
            "label": "资产组织",
            "maxlength": "20",
            "refcode": "uapbd/refer/org/AssetOrgGridRef/index.js",
            "required": true,
            "attrcode": "pk_org"
          },
          {
            "itemtype": "input",
            "col": "4",
            "visible": true,
            "label": "单据编码",
            "maxlength": "40",
            "disabled": true,
            "attrcode": "bill_code"
          },
          {
            "itemtype": "datepicker",
            "col": "4",
            "label": "制单时间",
            "maxlength": "19",
            "disabled": true,
            "attrcode": "billmaketime"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "创建人",
            "maxlength": "20",
            "disabled": true,
            "attrcode": "creator"
          },
          {
            "itemtype": "select",
            "col": "4",
            "visible": true,
            "label": "单据状态",
            "disabled": true,
            "attrcode": "bill_status",
            "options": [
              {
                "display": "自由态",
                "value": "0"
              },
              {
                "display": "已提交",
                "value": "1"
              },
              {
                "display": "审批中",
                "value": "2"
              },
              {
                "display": "审批通过",
                "value": "3"
              },
              {
                "display": "审批未通过",
                "value": "4"
              },
              {
                "display": "关闭",
                "value": "6"
              }
            ]
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "审批人",
            "maxlength": "20",
            "disabled": true,
            "attrcode": "auditor"
          },
          {
            "itemtype": "datepicker",
            "col": "4",
            "label": "创建时间",
            "maxlength": "19",
            "disabled": true,
            "attrcode": "creationtime"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "visible": true,
            "label": "借用部门",
            "maxlength": "20",
            "refcode": "uapbd/refer/orgv/DeptVersionTreeRef/index.js",
            "required": true,
            "attrcode": "pk_usedept_v"
          },
          {
            "itemtype": "datepicker",
            "col": "4",
            "label": "审批时间",
            "maxlength": "19",
            "disabled": true,
            "attrcode": "audittime"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "最后修改人",
            "maxlength": "20",
            "disabled": true,
            "attrcode": "modifier"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "visible": true,
            "label": "借用人",
            "maxlength": "20",
            "refcode": "uapbd/refer/psninfo/PsndocTreeGridRef/index.js",
            "required": true,
            "attrcode": "pk_user"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "审批意见",
            "maxlength": "200",
            "disabled": true,
            "attrcode": "check_opinion"
          },
          {
            "itemtype": "datepicker",
            "col": "4",
            "label": "最后修改日期",
            "maxlength": "19",
            "disabled": true,
            "attrcode": "modifiedtime"
          },
          {
            "itemtype": "input",
            "col": "4",
            "visible": true,
            "label": "申请说明",
            "maxlength": "80",
            "attrcode": "reason"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "申请单主键",
            "maxlength": "20",
            "attrcode": "pk_apply"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "申请人",
            "maxlength": "20",
            "required": true,
            "attrcode": "pk_applier"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "集团",
            "maxlength": "20",
            "attrcode": "pk_group"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "单据类型",
            "maxlength": "4",
            "disabled": true,
            "attrcode": "bill_type"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "使用权组织版本",
            "maxlength": "20",
            "disabled": true,
            "attrcode": "pk_unit_used"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "使用权",
            "maxlength": "20",
            "required": true,
            "attrcode": "pk_unit_used_v"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "使用部门版本",
            "maxlength": "20",
            "disabled": true,
            "attrcode": "pk_usedept"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "申请部门版本",
            "maxlength": "20",
            "attrcode": "pk_appydept"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "申请部门",
            "maxlength": "20",
            "attrcode": "pk_appydept_v"
          },
          {
            "itemtype": "switch",
            "col": "4",
            "label": "完成标记",
            "maxlength": "1",
            "disabled": true,
            "attrcode": "complete_flag"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "交易类型编码",
            "maxlength": "20",
            "attrcode": "transi_type"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "业务类型",
            "maxlength": "20",
            "disabled": true,
            "attrcode": "busi_type"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "交易类型",
            "maxlength": "20",
            "attrcode": "pk_transitype"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "备注",
            "maxlength": "200",
            "attrcode": "memo"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符1",
            "maxlength": "128",
            "attrcode": "pre_text1"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符2",
            "maxlength": "128",
            "attrcode": "pre_text2"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符3",
            "maxlength": "128",
            "attrcode": "pre_text3"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符4",
            "maxlength": "128",
            "attrcode": "pre_text4"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符5",
            "maxlength": "128",
            "attrcode": "pre_text5"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符6",
            "maxlength": "128",
            "attrcode": "pre_text6"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符7",
            "maxlength": "128",
            "attrcode": "pre_text7"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符8",
            "maxlength": "128",
            "attrcode": "pre_text8"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符9",
            "maxlength": "128",
            "attrcode": "pre_text9"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符10",
            "maxlength": "128",
            "attrcode": "pre_text10"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项1",
            "maxlength": "101",
            "attrcode": "def1"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项2",
            "maxlength": "101",
            "attrcode": "def2"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项3",
            "maxlength": "101",
            "attrcode": "def3"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项4",
            "maxlength": "101",
            "attrcode": "def4"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项5",
            "maxlength": "101",
            "attrcode": "def5"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项6",
            "maxlength": "101",
            "attrcode": "def6"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项7",
            "maxlength": "101",
            "attrcode": "def7"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项8",
            "maxlength": "101",
            "attrcode": "def8"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项9",
            "maxlength": "101",
            "attrcode": "def9"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项10",
            "maxlength": "101",
            "attrcode": "def10"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项11",
            "maxlength": "101",
            "attrcode": "def11"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项12",
            "maxlength": "101",
            "attrcode": "def12"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项13",
            "maxlength": "101",
            "attrcode": "def13"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项14",
            "maxlength": "101",
            "attrcode": "def14"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项15",
            "maxlength": "101",
            "attrcode": "def15"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项16",
            "maxlength": "101",
            "attrcode": "def16"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项17",
            "maxlength": "101",
            "attrcode": "def17"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项18",
            "maxlength": "101",
            "attrcode": "def18"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项19",
            "maxlength": "101",
            "attrcode": "def19"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项20",
            "maxlength": "101",
            "attrcode": "def20"
          },
          {
            "itemtype": "datepicker",
            "col": "4",
            "label": "时间戳",
            "maxlength": "19",
            "attrcode": "ts"
          }
        ],
        "moduletype": "form",
        "code": "head",
        "name": "BorrowApplyHeadVO"
      },
      "gridrelation": {
        "bodyvos": {
          "destBrowseAreaCode": "childform2",
          "destEditAreaCode": "childform1",
          "srcAreaCode": "bodyvos",
          "tabRelation": [
            "bodyvos"
          ]
        }
      },
      "formrelation": {
        "head": []
      }
    }
  },
  "success": true
}
