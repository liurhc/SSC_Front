let dat={
  "data": {
    "code": "cpdoc",
    "name": "工单",
    "pageid": "cpdoc",
    "cpdocmaster_childform2": {
      "items": [
        {
          "itemtype": "input",
          "label": "工单属性主键",
          "maxlength": "20",
          "attrcode": "pk_attribute"
        },
        {
          "itemtype": "input",
          "visible": true,
          "label": "字段名称",
          "maxlength": "20",
          "attrcode": "name"
        },
        {
          "itemtype": "input",
          "visible": true,
          "label": "显示名称",
          "maxlength": "20",
          "attrcode": "displayname"
        },
        {
          "itemtype": "input",
          "visible": true,
          "label": "类型",
          "maxlength": "20",
          "attrcode": "type"
        },
        {
          "itemtype": "input",
          "visible": true,
          "label": "默认值",
          "maxlength": "10",
          "attrcode": "defaultvalue"
        },
        {
          "itemtype": "checkbox_switch",
          "visible": true,
          "label": "允许为空",
          "maxlength": "1",
          "attrcode": "nullable"
        },
        {
          "itemtype": "checkbox_switch",
          "visible": true,
          "label": "卡片显示",
          "maxlength": "1",
          "attrcode": "visibleoncard"
        },
        {
          "itemtype": "checkbox_switch",
          "visible": true,
          "label": "列表显示",
          "maxlength": "1",
          "attrcode": "visibleongrid"
        }
      ],
      "moduletype": "form",
      "relationcode": "cpdocmaster",
      "areastatus": "edit",
      "code": "cpdocmaster_childform2",
      "name": "主表属性"
    },
    "cpdocmaster_childform1": {
      "items": [
        {
          "itemtype": "input",
          "label": "工单属性主键",
          "maxlength": "20",
          "attrcode": "pk_attribute"
        },
        {
          "itemtype": "input",
          "visible": true,
          "label": "字段名称",
          "maxlength": "20",
          "attrcode": "name"
        },
        {
          "itemtype": "input",
          "visible": true,
          "label": "显示名称",
          "maxlength": "20",
          "attrcode": "displayname"
        },
        {
          "itemtype": "input",
          "visible": true,
          "label": "类型",
          "maxlength": "20",
          "attrcode": "type"
        },
        {
          "itemtype": "input",
          "visible": true,
          "label": "默认值",
          "maxlength": "10",
          "attrcode": "defaultvalue"
        },
        {
          "itemtype": "checkbox_switch",
          "visible": true,
          "label": "允许为空",
          "maxlength": "1",
          "attrcode": "nullable"
        },
        {
          "itemtype": "checkbox_switch",
          "visible": true,
          "label": "卡片显示",
          "maxlength": "1",
          "attrcode": "visibleoncard"
        },
        {
          "itemtype": "checkbox_switch",
          "visible": true,
          "label": "列表显示",
          "maxlength": "1",
          "attrcode": "visibleongrid"
        }
      ],
      "moduletype": "form",
      "relationcode": "cpdocmaster",
      "areastatus": "browse",
      "code": "cpdocmaster_childform1",
      "name": "主表属性"
    },
    "cpdocchild": {
      "items": [
        {
          "itemtype": "input",
          "label": "工单属性主键",
          "maxlength": "20",
          "attrcode": "pk_attribute"
        },
        {
          "itemtype": "input",
          "visible": true,
          "label": "字段名称",
          "maxlength": "20",
          "attrcode": "name"
        },
        {
          "itemtype": "input",
          "visible": true,
          "label": "显示名称",
          "maxlength": "20",
          "attrcode": "displayname"
        },
        {
          "itemtype": "input",
          "visible": true,
          "label": "类型",
          "maxlength": "20",
          "attrcode": "type"
        },
        {
          "itemtype": "input",
          "visible": true,
          "label": "默认值",
          "maxlength": "10",
          "attrcode": "defaultvalue"
        },
        {
          "itemtype": "checkbox_switch",
          "visible": true,
          "label": "允许为空",
          "maxlength": "1",
          "attrcode": "nullable"
        },
        {
          "itemtype": "checkbox_switch",
          "visible": true,
          "label": "卡片显示",
          "maxlength": "1",
          "attrcode": "visibleoncard"
        },
        {
          "itemtype": "checkbox_switch",
          "visible": true,
          "label": "列表显示",
          "maxlength": "1",
          "attrcode": "visibleongrid"
        }
      ],
      "moduletype": "table",
      "pagination": false,
      "code": "cpdocchild",
      "name": "子表字段"
    },
    "cpdocchild_childform2": {
      "items": [
        {
          "itemtype": "input",
          "label": "工单属性主键",
          "maxlength": "20",
          "attrcode": "pk_attribute"
        },
        {
          "itemtype": "input",
          "visible": true,
          "label": "字段名称",
          "maxlength": "20",
          "attrcode": "name"
        },
        {
          "itemtype": "input",
          "visible": true,
          "label": "显示名称",
          "maxlength": "20",
          "attrcode": "displayname"
        },
        {
          "itemtype": "input",
          "visible": true,
          "label": "类型",
          "maxlength": "20",
          "attrcode": "type"
        },
        {
          "itemtype": "input",
          "visible": true,
          "label": "默认值",
          "maxlength": "10",
          "attrcode": "defaultvalue"
        },
        {
          "itemtype": "checkbox_switch",
          "visible": true,
          "label": "允许为空",
          "maxlength": "1",
          "attrcode": "nullable"
        },
        {
          "itemtype": "checkbox_switch",
          "visible": true,
          "label": "卡片显示",
          "maxlength": "1",
          "attrcode": "visibleoncard"
        },
        {
          "itemtype": "checkbox_switch",
          "visible": true,
          "label": "列表显示",
          "maxlength": "1",
          "attrcode": "visibleongrid"
        }
      ],
      "moduletype": "form",
      "relationcode": "cpdocchild",
      "areastatus": "edit",
      "code": "cpdocchild_childform2",
      "name": "子表属性"
    },
    "head": {
      "items": [
        {
          "itemtype": "input",
          "col": "4",
          "label": "工单类型主键",
          "maxlength": "20",
          "attrcode": "pk_doc"
        },
        {
          "itemtype": "input",
          "col": "4",
          "visible": true,
          "label": "编码",
          "maxlength": "20",
          "attrcode": "doc_code"
        },
        {
          "itemtype": "input",
          "col": "4",
          "visible": true,
          "label": "名称",
          "maxlength": "20",
          "attrcode": "doc_name"
        },
        {
          "itemtype": "refer",
          "col": "4",
          "visible": true,
          "label": "分类",
          "maxlength": "20",
          "attrcode": "pk_doctype"
        },
        {
          "itemtype": "select",
          "col": "4",
          "visible": true,
          "label": "单据结构",
          "maxlength": "1",
          "attrcode": "doc_structure",
          "options": [
            {
              "display": "单主表",
              "value": "single"
            },
            {
              "display": "主子表",
              "value": "multi"
            }
          ]
        },
        {
          "itemtype": "input",
          "col": "4",
          "visible": true,
          "label": "对应主表名称",
          "maxlength": "20",
          "attrcode": "doc_tablename"
        },
        {
          "itemtype": "input",
          "col": "4",
          "visible": true,
          "label": "对应子表名称",
          "maxlength": "20",
          "attrcode": "doc_subtablename"
        },
        {
          "itemtype": "input",
          "col": "4",
          "visible": true,
          "label": "元数据",
          "maxlength": "20",
          "attrcode": "ispubmetadata"
        },
        {
          "itemtype": "input",
          "col": "4",
          "visible": true,
          "label": "小应用",
          "maxlength": "20",
          "attrcode": "ispubapplet"
        },
        {
          "itemtype": "input",
          "col": "4",
          "visible": true,
          "label": "备注",
          "maxlength": "20",
          "attrcode": "remark"
        }
      ],
      "moduletype": "form",
      "code": "head",
      "name": "设置工单信息"
    },
    "cpdocmaster": {
      "items": [
        {
          "itemtype": "input",
          "label": "工单属性主键",
          "maxlength": "20",
          "attrcode": "pk_attribute"
        },
        {
          "itemtype": "input",
          "visible": true,
          "label": "字段名称",
          "maxlength": "20",
          "attrcode": "name"
        },
        {
          "itemtype": "input",
          "visible": true,
          "label": "显示名称",
          "maxlength": "20",
          "attrcode": "displayname"
        },
        {
          "itemtype": "input",
          "visible": true,
          "label": "类型",
          "maxlength": "20",
          "attrcode": "type"
        },
        {
          "itemtype": "input",
          "visible": true,
          "label": "默认值",
          "maxlength": "10",
          "attrcode": "defaultvalue"
        },
        {
          "itemtype": "checkbox_switch",
          "visible": true,
          "label": "允许为空",
          "maxlength": "1",
          "attrcode": "nullable"
        },
        {
          "itemtype": "checkbox_switch",
          "visible": true,
          "label": "卡片显示",
          "maxlength": "1",
          "attrcode": "visibleoncard"
        },
        {
          "itemtype": "checkbox_switch",
          "visible": true,
          "label": "列表显示",
          "maxlength": "1",
          "attrcode": "visibleongrid"
        }
      ],
      "moduletype": "table",
      "pagination": false,
      "code": "cpdocmaster",
      "name": "主表字段"
    },
    "cpdocchild_childform1": {
      "items": [
        {
          "itemtype": "input",
          "label": "工单属性主键",
          "maxlength": "20",
          "attrcode": "pk_attribute"
        },
        {
          "itemtype": "input",
          "visible": true,
          "label": "字段名称",
          "maxlength": "20",
          "attrcode": "name"
        },
        {
          "itemtype": "input",
          "visible": true,
          "label": "显示名称",
          "maxlength": "20",
          "attrcode": "displayname"
        },
        {
          "itemtype": "input",
          "visible": true,
          "label": "类型",
          "maxlength": "20",
          "attrcode": "type"
        },
        {
          "itemtype": "input",
          "visible": true,
          "label": "默认值",
          "maxlength": "10",
          "attrcode": "defaultvalue"
        },
        {
          "itemtype": "checkbox_switch",
          "visible": true,
          "label": "允许为空",
          "maxlength": "1",
          "attrcode": "nullable"
        },
        {
          "itemtype": "checkbox_switch",
          "visible": true,
          "label": "卡片显示",
          "maxlength": "1",
          "attrcode": "visibleoncard"
        },
        {
          "itemtype": "checkbox_switch",
          "visible": true,
          "label": "列表显示",
          "maxlength": "1",
          "attrcode": "visibleongrid"
        }
      ],
      "moduletype": "form",
      "relationcode": "cpdocchild",
      "areastatus": "browse",
      "code": "cpdocchild_childform1",
      "name": "子表属性"
    },
    "gridrelation": {
      "cpdocchild": {
        "destBrowseAreaCode": "cpdocchild_childform1",
        "destEditAreaCode": "cpdocchild_childform2",
        "srcAreaCode": "cpdocchild",
        "tabRelation": [
          "cpdocchild"
        ]
      },
      "cpdocmaster": {
        "destBrowseAreaCode": "cpdocmaster_childform1",
        "destEditAreaCode": "cpdocmaster_childform2",
        "srcAreaCode": "cpdocmaster",
        "tabRelation": [
          "cpdocmaster"
        ]
      }
    },
    "formrelation": {
      "head": []
    }
  },
  "success": true
}
export default dat;
