

let woDetailsData = {
  "data": {
    "template": {
      "code": "4520008005_card",
      "name": "借用申请",
      "pageid": "4520008005_card",
      "bodyvos": {
        "items": [
          {
            "itemtype": "input",
            "label": "工单属性主键",
            "maxlength": 20,
            "visible":true,
            "attrcode": "pk_attribute"
          },
          {
            "itemtype": "input",
            "label": "工单属性名称",
            "maxlength": 20,
                      "visible":true,
            "attrcode": "name"
          },
          {
            "itemtype": "input",
            "label": "工单属性显示名称",
            "maxlength": 20,
                      "visible":true,
            "attrcode": "displayname"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
                      "visible":true,
            "maxlength": 20,
            "attrcode": "type_style"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
                      "visible":true,
            "maxlength": 20,
            "attrcode": "type"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
                      "visible":true,
            "maxlength": 20,
            "attrcode": "typename"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 20,
            "attrcode": "fieldname"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 20,
            "attrcode": "fieldtype"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 20,
            "attrcode": "refname"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 20,
            "attrcode": "describe"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 20,
            "attrcode": "refcodeorname"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 1,
            "attrcode": "ishidden"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 1,
            "attrcode": "nullable"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 1,
            "attrcode": "readonly"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 1,
            "attrcode": "isvisibleonreflist"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 1,
            "attrcode": "ispublish"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 1,
            "attrcode": "ishasmd"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 20,
            "attrcode": "precise"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 10,
            "attrcode": "defaultvalue"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 10,
            "attrcode": "length"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 1,
            "attrcode": "enablestate"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 1,
            "attrcode": "isgrant"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 1,
            "attrcode": "locallanguage"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 20,
            "attrcode": "showrelate"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 1,
            "attrcode": "issystemattr"
          },
          {
            "itemtype": "refer",
            "label": "工单属性",
            "maxlength": 20,
            "attrcode": "pk_doc"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 1,
            "attrcode": "isprimarykey"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 20,
            "attrcode": "orderid"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 1,
            "attrcode": "isvisible"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 1,
            "attrcode": "visibleongrid"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 1,
            "attrcode": "visibleongrid"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 1,
            "attrcode": "visibleoncard"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 20,
            "attrcode": "sysext"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 20,
            "attrcode": "sysext2"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 20,
            "attrcode": "sysext3"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 20,
            "attrcode": "sysext4"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 20,
            "attrcode": "sysext5"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 20,
            "attrcode": "sysext6"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 20,
            "attrcode": "sysext7"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 20,
            "attrcode": "sysext8"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 20,
            "attrcode": "sysext9"
          },
          {
            "itemtype": "input",
            "label": "工单属性",
            "maxlength": 20,
            "attrcode": "sysext10"
          },
          {
            "itemtype": "datepicker",
            "col": 4,
            "label": "时间戳",
            "maxlength": 19,
            "attrcode": "ts"
          },
          {
            "itemtype": "label",
            "label": "dr",
            "maxlength": 3,
            "attrcode": "dr"
          }
        ],
        "moduletype": "table",
        "pagination": false,
        "code": "bodyvos",
        "name": "借用申请明细"
      },
      "childform1": {
        "items": [
        {
          "itemtype": "input",
          "label": "工单类型主键",
          "maxlength": 20,
          "attrcode": "pk_doct"
        },
        {
          "itemtype": "input",
          "label": "工单类型编码",
          "maxlength": 20,
          "attrcode": "doc_code"
        },
        {
          "itemtype": "input",
          "label": "工单类型名称",
          "maxlength": 20,
          "attrcode": "doc_name"
        },
        {
          "itemtype": "refer",
          "label": "工单分类主键",
          "maxlength": 20,
          "attrcode": "pk_doctype"
        },
        {
          "itemtype": "input",
          "label": "主表表名",
          "maxlength": 20,
          "attrcode": "doc_tablename"
        },
        {
          "itemtype": "input",
          "label": "子表表名",
          "maxlength": 20,
          "attrcode": "doc_subtablename"
        },
        {
          "itemtype": "select",
          "col": 4,
          "label": "单据结构",
          "maxlength": 1,
          "attrcode": "doc_structure",
          "options": [
            {
              "display": "单主表",
              "value": "single"
            },
            {
              "display": "主子表",
              "value": "multi"
            }
          ]
        },
        {
          "itemtype": "input",
          "label": "备注",
          "maxlength": 20,
          "attrcode": "remark"
        },
        {
          "itemtype": "input",
          "label": "是否发布元数据",
          "maxlength": 20,
          "attrcode": "ispubmetadata"
        },
        {
          "itemtype": "input",
          "label": "是否发布小应用",
          "maxlength": 20,
          "attrcode": "ispubapplet"
        }
      ],

        "moduletype": "form",
        "relationcode": "bodyvos",
        "areastatus": "edit",
        "code": "childform1",
        "name": "明细子表1"
      },
      "childform2": {
        "items": [
          {
            "itemtype": "refer",
            "col": "4",
            "visible": true,
            "label": "资产类型",
            "maxlength": "20",
            "refcode": "uapbd/refer/am/CategoryTreeRef/index.js",
            "attrcode": "pk_category"
          },
          {
            "itemtype": "input",
            "col": "4",
            "visible": true,
            "label": "资产详细描述",
            "maxlength": "80",
            "attrcode": "spec"
          },
          {
            "itemtype": "input",
            "col": "4",
            "visible": true,
            "label": "用途",
            "maxlength": "80",
            "attrcode": "model"
          },
          {
            "itemtype": "number",
            "col": "4",
            "visible": true,
            "label": "数量",
            "required": true,
            "attrcode": "quantity"
          },
          {
            "itemtype": "datepicker",
            "col": "4",
            "visible": true,
            "label": "借用日期",
            "maxlength": "19",
            "required": true,
            "attrcode": "expect_start_date"
          },
          {
            "itemtype": "datepicker",
            "col": "4",
            "visible": true,
            "label": "预计归还日期",
            "maxlength": "19",
            "attrcode": "expect_end_date"
          },
          {
            "itemtype": "select",
            "col": "4",
            "visible": true,
            "label": "借用情况",
            "attrcode": "borrow_condition",
            "options": [
              {
                "display": "未借用",
                "value": "0"
              },
              {
                "display": "已借用",
                "value": "1"
              },
              {
                "display": "不处理",
                "value": "2"
              }
            ]
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "申请单子表主键",
            "maxlength": "20",
            "attrcode": "pk_apply_b"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "集团",
            "maxlength": "20",
            "attrcode": "pk_group"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "资产组织最新版本",
            "maxlength": "20",
            "attrcode": "pk_org"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "行号",
            "maxlength": "30",
            "attrcode": "rowno"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "设备编码",
            "maxlength": "20",
            "attrcode": "pk_equip"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "卡片编号",
            "maxlength": "40",
            "disabled": true,
            "attrcode": "pk_equip.card_code"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "期初",
            "maxlength": "1",
            "disabled": true,
            "attrcode": "pk_equip.is_pre"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "设备名称",
            "maxlength": "200",
            "attrcode": "equip_name"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "物料编码",
            "maxlength": "20",
            "attrcode": "pk_material_v"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "物料编码版本",
            "maxlength": "20",
            "attrcode": "pk_material"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "序列号",
            "maxlength": "80",
            "attrcode": "serial_num"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "仓库",
            "maxlength": "20",
            "attrcode": "pk_warehouse"
          },
          {
            "itemtype": "datepicker",
            "col": "4",
            "label": "领用日期",
            "maxlength": "19",
            "attrcode": "start_date"
          },
          {
            "itemtype": "datepicker",
            "col": "4",
            "label": "实际归还日期",
            "maxlength": "19",
            "attrcode": "end_date"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "项目",
            "maxlength": "20",
            "attrcode": "pk_jobmngfil"
          },
          {
            "itemtype": "refer",
            "col": "4",
            "label": "资产组织",
            "maxlength": "20",
            "attrcode": "pk_org_v"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "技术要求",
            "maxlength": "200",
            "attrcode": "memo"
          },
          {
            "itemtype": "switch",
            "col": "4",
            "label": "关闭",
            "maxlength": "1",
            "disabled": true,
            "attrcode": "close_flag"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符1",
            "maxlength": "128",
            "attrcode": "pre_text1"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符2",
            "maxlength": "128",
            "attrcode": "pre_text2"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符3",
            "maxlength": "128",
            "attrcode": "pre_text3"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符4",
            "maxlength": "128",
            "attrcode": "pre_text4"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符5",
            "maxlength": "128",
            "attrcode": "pre_text5"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符6",
            "maxlength": "128",
            "attrcode": "pre_text6"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符7",
            "maxlength": "128",
            "attrcode": "pre_text7"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符8",
            "maxlength": "128",
            "attrcode": "pre_text8"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符9",
            "maxlength": "128",
            "attrcode": "pre_text9"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "预置字符10",
            "maxlength": "128",
            "attrcode": "pre_text10"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项1",
            "maxlength": "101",
            "attrcode": "def1"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项2",
            "maxlength": "101",
            "attrcode": "def2"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项3",
            "maxlength": "101",
            "attrcode": "def3"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项4",
            "maxlength": "101",
            "attrcode": "def4"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项5",
            "maxlength": "101",
            "attrcode": "def5"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项6",
            "maxlength": "101",
            "attrcode": "def6"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项7",
            "maxlength": "101",
            "attrcode": "def7"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项8",
            "maxlength": "101",
            "attrcode": "def8"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项9",
            "maxlength": "101",
            "attrcode": "def9"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项10",
            "maxlength": "101",
            "attrcode": "def10"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项11",
            "maxlength": "101",
            "attrcode": "def11"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项12",
            "maxlength": "101",
            "attrcode": "def12"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项13",
            "maxlength": "101",
            "attrcode": "def13"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项14",
            "maxlength": "101",
            "attrcode": "def14"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项15",
            "maxlength": "101",
            "attrcode": "def15"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项16",
            "maxlength": "101",
            "attrcode": "def16"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项17",
            "maxlength": "101",
            "attrcode": "def17"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项18",
            "maxlength": "101",
            "attrcode": "def18"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项19",
            "maxlength": "101",
            "attrcode": "def19"
          },
          {
            "itemtype": "input",
            "col": "4",
            "label": "自定义项20",
            "maxlength": "101",
            "attrcode": "def20"
          },
          {
            "itemtype": "datepicker",
            "col": "4",
            "label": "时间戳",
            "maxlength": "19",
            "attrcode": "ts"
          }
        ],
        "moduletype": "form",
        "relationcode": "bodyvos",
        "areastatus": "browse",
        "code": "childform2",
        "name": "明细子表2"
      },
      "head": {
        "items": [
        {
          "itemtype": "input",
          "label": "工单类型主键",
          "maxlength": 20,
          "visible":true,
          "attrcode": "pk_doct"
        },
        {
          "itemtype": "input",
          "label": "工单类型编码",
          "maxlength": 20,
          "visible":true,
          "attrcode": "doc_code"
        },
        {
          "itemtype": "input",
          "label": "工单类型名称",
          "maxlength": 20,
          "visible":true,
          "attrcode": "doc_name"
        },
        {
          "itemtype": "refer",
          "label": "工单分类主键",
          "maxlength": 20,
          "visible":true,
          "attrcode": "pk_doctype"
        },
        {
          "itemtype": "input",
          "label": "主表表名",
          "maxlength": 20,
          "visible":true,
          "attrcode": "doc_tablename"
        },
        {
          "itemtype": "input",
          "label": "子表表名",
          "maxlength": 20,
          "visible":true,
          "attrcode": "doc_subtablename"
        },
        {
          "itemtype": "select",
          "col": 4,
          "label": "单据结构",
          "maxlength": 1,
          "attrcode": "doc_structure",
          "options": [
            {
              "display": "单主表",
              "value": "single"
            },
            {
              "display": "主子表",
              "value": "multi"
            }
          ]
        },
        {
          "itemtype": "input",
          "label": "备注",
          "maxlength": 20,
          "attrcode": "remark"
        },
        {
          "itemtype": "input",
          "label": "是否发布元数据",
          "maxlength": 20,
          "attrcode": "ispubmetadata"
        },
        {
          "itemtype": "input",
          "label": "是否发布小应用",
          "maxlength": 20,
          "attrcode": "ispubapplet"
        }
      ],
        "moduletype": "form",
        "code": "head",
        "name": "BorrowApplyHeadVO"
      },
      "gridrelation": {
        "bodyvos": {
          "destBrowseAreaCode": "childform2",
          "destEditAreaCode": "childform1",
          "srcAreaCode": "bodyvos",
          "tabRelation": [
            "bodyvos"
          ]
        }
      },
      "formrelation": {
        "head": []
      }
    }
  },
  "success": true
};

export default woDetailsData;
