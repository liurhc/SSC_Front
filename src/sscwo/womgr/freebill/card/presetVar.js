/**
 * 全局变量设置文件
 */
import 'ssccommon/components/globalPresetVar';

//全局变量尽量配置在presetVar中，防止全局变量污染。也方便统一管理。
window.presetVar = {
    ...window.presetVar,
    // pageId: '201102611_2611_IWEB',
    // pageId: 'freebill_card',
    // pageCode: 'freebill_card',
    //表头（主表）变量配置
    head: {
        head1: 'head'
    },
    //表体（子表）变量配置
    body: {
        body1: 'freebill_detaill'
    },
    //搜索模板变量配置
    search: {}
};