import presetVar from '../presetVar';
const body1 = window.presetVar.body.body1;
const head1 = window.presetVar.head.head1;

/**
 * 表头合计处理
 */
export function headSumHandle(prop) {
    let busiitemfield = ['amount'];
    let busiitemfieldTotal = [0];
    let rows = prop.cardTable.getAllRows(body1, false);
    if (rows && rows[0] && rows.length > 0) {
        for (let m = 0; m < rows.length; m++)
            if (rows[m] && rows[m].status != 3) {
                for (let i = 0; i < busiitemfield.length; i++) {
                    let countmouny = rows[m].values[busiitemfield[i]].value;
                    if (countmouny == null || countmouny == '') {
                        countmouny = 0;
                    }
                    busiitemfieldTotal[i] += + countmouny;
                }

            }

    }

    for (let i = 0; i < busiitemfieldTotal.length; i++) {
        let key = busiitemfield[i];
        let scale = prop.form.getFormItemsValue(head1, key).scale; // 取模板精度
        scale = scale == null || scale == -1 ? 2 : scale;
        let display = busiitemfieldTotal[i].toFixed(scale);

        let changeHeadItem = {
            [busiitemfield[i]]: {
                value: busiitemfieldTotal[i],
                display: display,
                scale: scale
            }
        }
        prop.form.setFormItemsValue(head1, changeHeadItem);
    }
}

// 获取表体默认值
export function getBodyDefaultValue(props) {
    let defaultValue={};
    props.meta.getMeta()[body1].items.map((one)=>{
        if(one.initialvalue != null && one.initialvalue.value != null && one.initialvalue.value != ""){
            defaultValue[one.attrcode] = {display: one.initialvalue.display, value: one.initialvalue.value}
        } else if(one.attrcode == 'amount'){
            defaultValue[one.attrcode] = {display:null, value:''}
        }
    });
    return defaultValue;
}
