import afterEvent from './afterEvent';
import tableButtonClick from './tableButtonClick';
import tableExtendButtonClick from './tableExtendButtonClick';
import pageButtonClick from './pageButtonClick';

export { tableButtonClick, tableExtendButtonClick, pageButtonClick, afterEvent};
