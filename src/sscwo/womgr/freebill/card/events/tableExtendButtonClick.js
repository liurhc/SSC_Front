/**
 * 表格扩展按钮click事件
 */
import {headSumHandle, getBodyDefaultValue} from './commonEvent';

const areaBtnAction = window.presetVar.areaBtnAction;
const body1 = window.presetVar.body.body1;
const statusVar = window.presetVar.status;

function tableExtendButtonClick() {
    return {
        [body1 + areaBtnAction.del]: {
            actionCode: 'BodyDelete',
            afterClick: (record, index) => {
            	// 合计金额到表头
                headSumHandle.call(this, this.props);
            }
        },
        [body1 + areaBtnAction.edit]: { actionCode: 'BodyMore' },
        [body1 + areaBtnAction.insert]: { 
            actionCode: 'BodyInsert',
            afterClick: (record, index) => {
                // 初始化行默认值
                this.props.cardTable.setValByKeysAndIndex(body1, index + 1, getBodyDefaultValue(this.props));
                
                // 合计金额到表头
                headSumHandle.call(this, this.props);
            }
        },
        [body1 + areaBtnAction.copy]: {
            actionCode: 'BodyCopy',
            keyField: 'pk_doc',
            afterClick: (record, index) => {
                // 合计金额到表头
                headSumHandle.call(this, this.props);
            }
        }
    }
}

export default tableExtendButtonClick;