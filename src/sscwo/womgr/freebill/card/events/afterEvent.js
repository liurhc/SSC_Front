
import {base,promptBox} from 'nc-lightapp-front';
import {headSumHandle} from './commonEvent';
const head1 = window.presetVar.head.head1;
const body1 = window.presetVar.body.body1;


export default function afterEvent(props,moduleId,key,value, changedrows) {
    let multiLang = this.props.MutiInit.getIntl(7020); //this.moduleId
    console.log(multiLang && multiLang.get("702001GDFL-0033"), props,moduleId,key,value, 22, changedrows);
    switch(moduleId) {
        case 'freebill_detaill' :
            switch (key) {
                case 'pk_org': //组织
                case 'billdate': //单据日期
                case 'amount' :
                    // 金额输入为空自定赋值为0
                    if (value == null||value==undefined || value=='') {
                        props.cardTable.setValByKeyAndRowId(body1, changedrows[0].rowid, 'amount', {value: 0, display: ""});
                    }

                    // 合计金额到表头
                    headSumHandle.call(this, this.props);
                    break;
                    // debugger;
                    // let onlyBodyData = props.createGridAfterEventData('20521030', 'freebill_detaill', moduleId, key, changedrows)
                    // console.log('表体字段change后，表体数据结构', onlyBodyData);
                    // let headBodyData = props.createBodyAfterEventData('20521030', 'head', 'freebill_detaill', moduleId, key, changedrows)
                    // console.log('表体字段change后，表头表体数据结构', headBodyData);

                    // requestApi.changeByBody({
                    //     data: headBodyData,
                    //     success: (data)=> {
                    //         //设置表头数据
                    //         props.form.setAllFormValue({[presetVar.head.head1]: data.data.head[presetVar.head.head1]});
                    //         //设置表体数据
                    //         props.cardTable.setTableData(presetVar.body.body1, data.data.body[presetVar.body.body1]);

                    //     }
                    // })
                    // break;
            }
            break;
        case 'head' :
            switch(key) {
                case 'pk_org': //组织
                    if (changedrows.value != value.value && changedrows.value != null 
                            && changedrows.value != undefined && changedrows.value != "") {
                        // 提示是否确认修改组织，修改后将清空部分字段
                        promptBox({
                            color: 'warning',
                            title: multiLang && multiLang.get("702001GDFL-0034"),
                            content: multiLang && multiLang.get("702001GDFL-0035"),
                            noFooter: false, 
                            noCancelBtn: false,
                            beSureBtnName: multiLang && multiLang.get("7020-0001"),
                            cancelBtnName: multiLang && multiLang.get("7020-0002"),      
                            beSureBtnClick: ()=>{
                                // 组织修改后，清空表头组织关联的档案字段
                                let changeHeadItem = {};
                                let meta = props.meta.getMeta();
                                let filterAttr = ['pk_org', 'billmaker', 'pk_group','transtypepk'];
                                meta[head1].items.forEach((item) => {
                                    if (item.itemtype == 'refer' && !filterAttr.includes(item.attrcode)) {
                                        changeHeadItem[item.attrcode] = {value: null, display: null};
                                    }
                                });

                                props.form.setFormItemsValue(head1, changeHeadItem);

                                // 组织修改后，清空表体行
                                let data = props.cardTable.getAllRows(body1, false);
                                if (data && data.length > 0) {
                                    props.cardTable.setTableData(body1, {rows: []});
                                    props.form.setFormItemsValue(head1, {amount: {value: 0, display: null}});
                                }
                            },
                            cancelBtnClick: ()=>{
                                // 还原组织字段
                                let changeHeadItem = {};
                                changeHeadItem['pk_org'] = {value: changedrows.value, display: changedrows.display};
                                props.form.setFormItemsValue(head1, changeHeadItem);
                            },
                        })
                    }
                    break;
                case 'billdate': //单据日期
                case 'apply_org': //申请单位
                case 'org_currinfo'://汇率
                case 'apply_dept': //申请部门
                case 'pk_iobsclass': //收支项目
                case 'billmaker': //申请人
                case 'pk_currtype': //币种
            }
            break;
    }
};