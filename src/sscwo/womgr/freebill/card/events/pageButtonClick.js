/**
 * 页面（页面肩部）级别按钮click事件
 */
import { ajax, print, base, promptBox, toast, cacheTools } from 'nc-lightapp-front';
import requestApi from '../requestApi';


import { imageScan, imageView } from "sscrp/rppub/components/image";
import { getUrlParam } from "ssccommon/utils/urlUtils";
import { getMultiLang } from 'nc-lightapp-front'
import initTemplate from '../initTemplate';

const areaBtnAction = window.presetVar.areaBtnAction;
const pageButton = window.presetVar.pageButton;
const statusVar = window.presetVar.status;
const head1 = window.presetVar.head.head1;
const body1 = window.presetVar.body.body1;
const pageId = window.presetVar.pageId;


let billtype = window.presetVar.billtype;
// TOP NCCLOUD-60141 Add
function setButtonShowHide(props, pageStatus) {
    if (pageStatus == statusVar.browse) {
        props.button.setButtonVisible(
            [
                pageButton.pageAdd,
                pageButton.pageCopy,
                pageButton.pageEdit,
                pageButton.pageDel,
                pageButton.billSubmit,
                pageButton.linkVoucher,
                pageButton.pagePrint,
                pageButton.pageSave,
                pageButton.fileManager,
                pageButton.imageShow,
                pageButton.imageUpload,
                pageButton.pageClose,
                'LinkApprove'
            ],
            true);
        props.button.setButtonVisible(
            [
                body1 + areaBtnAction.add,
                pageButton.pageSave,
                pageButton.pageClose,
                pageButton.billRecall,
                pageButton.pageClose,
                pageButton.billSaveSubmit,
                body1 + areaBtnAction.del,
                body1 + areaBtnAction.copy,
                body1 + areaBtnAction.insert
            ],
            false);
    } else if (pageStatus == statusVar.add) {
        props.button.setButtonVisible(
            [

                pageButton.pageSave,
                pageButton.pageClose,
                pageButton.fileManager,
                body1 + areaBtnAction.add,
                body1 + areaBtnAction.del,
                body1 + areaBtnAction.copy,
                body1 + areaBtnAction.insert
            ],
            true);

        props.button.setButtonVisible(
            [
                pageButton.pageAdd,
                pageButton.pageCopy,
                pageButton.pageEdit,
                pageButton.pageDel,
                pageButton.billRecall,
                pageButton.billSubmit,
                pageButton.pagePrint,
                pageButton.imageShow,
                pageButton.imageUpload,
                pageButton.linkVoucher,
                'LinkApprove'
            ],
            false);
    } else {
        props.button.setButtonVisible(
            [
                pageButton.pageSave,
                pageButton.pageClose,
                pageButton.fileManager,
                body1 + areaBtnAction.add,
                body1 + areaBtnAction.del,
                body1 + areaBtnAction.copy,
                body1 + areaBtnAction.insert
            ],
            true);
        props.button.setButtonVisible(
            [
                pageButton.pageAdd,
                pageButton.pageCopy,
                pageButton.pageEdit,
                pageButton.pageDel,
                pageButton.billSubmit,
                pageButton.billRecall,
                pageButton.pagePrint,
                pageButton.imageShow,
                pageButton.imageUpload,
                pageButton.linkVoucher,
                'LinkApprove'
            ],
            false);
    }
}
// BTM

function pageButtonClick() {
    let multiLang = this.props.MutiInit.getIntl("7020");
    return {
        [pageButton.pageSave]: () => {
            let props = this.props;
            let pageId = window.presetVar.pageId;
            props.cardTable.filterEmptyRows(body1, ['pk_group', 'pk_doc', 'pk_org']);
            let allData = props.createMasterChildData(pageId, head1, body1);
            let rows = props.cardTable.checkTableRequired("freebill_detaill");//对表体数据进行必输项校验

            if (!rows) {
                return
            }
            // 获取模板中表体隐藏字段
            let filterEmptyData = allData;
            if (!window.presetVar.isSingle) {
                let template = props.meta.getMeta();
                let bodyItems = template.freebill_detaill.items;
                let hiddenItems = bodyItems.filter(item => item.visible !== true);
                let hiddenAttrs = hiddenItems.map(item => item.attrcode);
                // 过滤空行
                filterEmptyData = filterEmptyRows(allData, hiddenAttrs);
            }

            let pk_freeform = filterEmptyData.head.head.rows[0].values.pk_freeform.value;
            if (pk_freeform == null || pk_freeform == undefined || pk_freeform == "") {
                filterEmptyData.head.head.rows[0].values.pk_freeform.value = this.state.billId;
            }
            requestApi.save({
                status: window.presetVar.pageStatus || props.getUrlParam('status'),
                data: filterEmptyData,
                success: (data) => {
                    if (data.success) {
                        props.cardTable.closeModel(body1);
                        let billid = data.data.head.head.rows[0].values.billid.value;

                        props.addUrlParam({ status: statusVar.browse, c: props.getUrlParam('c'), billId: billid, transtype: props.form.getFormItemsValue(head1, 'transtype').value })
                        window.presetVar.pageStatus = statusVar.browse;
                        //设置表头数据
                        props.form.setAllFormValue({ [head1]: data.data.head[head1] });
                        if (!window.presetVar.isSingle) {
                            //设置表体数据
                            data.data.body && props.cardTable.setTableData(body1, data.data.body[body1]);
                            props.cardTable.setStatus(body1, statusVar.browse);
                            // TOP NCCLOUD-60141 Add
                        }
                        props.form.setFormStatus(head1, statusVar.browse);
                        setButtonShowHide(props, 'browse');
                        // BTM 
                    }
                }
            });
        },
        [pageButton.pageCopy]: () => {
            let props = this.props;
            let appcode = window.presetVar.appcode;
            //location.hash = `#status=${statusVar.add}&c=${appcode}&copyFromBillId=${props.getUrlParam('billId')}`;
            props.linkTo('/sscwo/womgr/freebill/card/index.html', { 
                c: appcode, 
                status: statusVar.add, 
                copyFromBillId: props.getUrlParam('billId') 
            });
            location.reload();
        },
        [pageButton.pageClose]: () => {//取消

            let props = this.props;
            let pagestatus = window.presetVar.pageStatus || props.getUrlParam("status");
            //在我的作业中iframe中单据的status修改时候不发生改变，单独处理一下
            let scene = props.getUrlParam("scene");
            if (scene == 'zycl') {
                query(scene);
            } else {

                if (pagestatus == statusVar.edit) {
                    query();
                } else {
                    window.onbeforeunload = null;
                    window.top.close();
                }

            }


            function query(scene) {
                let billdata = {};
                billdata.billid = props.getUrlParam("billId") || props.getUrlParam("id");
                requestApi.query({
                    data: billdata,
                    success: (data) => {

                        let billid = data.data.head.head.rows[0].values.billid.value;
                        if (scene) {
                            props.linkTo('/sscwo/womgr/freebill/card/index.html', { status: statusVar.browse, c: props.getUrlParam('c'), billId: billid, transtype: props.form.getFormItemsValue(head1, 'transtype').value, scene: scene });
                        } else {
                            props.linkTo('/sscwo/womgr/freebill/card/index.html', { status: statusVar.browse, c: props.getUrlParam('c'), billId: billid, transtype: props.form.getFormItemsValue(head1, 'transtype').value });
                        }

                        //设置表头数据
                        props.form.setAllFormValue({ [head1]: data.data.head[head1] });
                        //设置表体数据
                        data.data.body && props.cardTable.setTableData(body1, data.data.body[body1]);
                        props.form.setFormStatus(head1, statusVar.browse);
                        props.cardTable.setStatus(body1, statusVar.browse);
                        // TOP NCCLOUD-60141 Add
                        setButtonShowHide(props, 'browse');

                    }
                });
            }

        },
        [pageButton.pageDel]: () => {//删除单据
            let props = this.props;
            let appcode = window.presetVar.appcode;
            let pageId = window.presetVar.pageId;

            props.form.setFormStatus(head1, statusVar.browse);
            props.cardTable.setStatus(body1, statusVar.browse);
            let allData = props.createMasterChildData(pageId, head1, body1);
            requestApi.billRemove({
                data: allData,
                success: (data) => {
                    if (data.data) {
                        window.onbeforeunload = null;
                        window.top.close();

                    }
                }
            });
        },
        [pageButton.billInvalid]: () => {//单据作废
            let props = this.props;
            props.form.setFormStatus(head1, statusVar.browse);
            props.cardTable.setStatus(body1, statusVar.browse);
            let allData = props.createMasterChildData(pageId, head1, body1);
            requestApi.billInvalid({
                data: allData,
                success: (data) => {
                    //设置表头数据
                    props.form.setAllFormValue({ [head1]: data.data.head[head1] })
                    //设置表体数据
                    data.data.body && props.cardTable.setTableData(body1, data.data.body[body1]);


                }
            });
        },
        [pageButton.billEnable]: () => { //单据重启
            let props = this.props;
            props.form.setFormStatus(head1, statusVar.browse);
            props.cardTable.setStatus(body1, statusVar.browse);
            let allData = props.createMasterChildData(pageId, head1, body1);
            requestApi.billEnable({
                data: allData,
                success: (data) => {
                    //设置表头数据
                    props.form.setAllFormValue({ [head1]: data.data.head[head1] })
                    //设置表体数据
                    data.data.body && this.props.cardTable.setTableData(body1, data.data.body[body1]);

                }
            });
        },
        //目前工单没有单据关闭的按钮
        [pageButton.billClose]: () => {//单据关闭
            let props = this.props;
            props.form.setFormStatus(head1, statusVar.browse);
            props.cardTable.setStatus(body1, statusVar.browse);
            let allData = props.createMasterChildData(pageId, head1, body1);
            requestApi.billClose({
                data: allData,
                success: (data) => {
                    //设置表头数据
                    props.form.setAllFormValue({ [head1]: data.data.head[head1] })
                    //设置表体数据
                    data.data.body && props.cardTable.setTableData(body1, data.data.body[body1]);

                    location.hash = `#status=${statusVar.browse}&billId=${props.form.getFormItemsValue(presetVar.head.head1, 'billno').value}`;
                    initTemplate.call(this, props, false)
                }
            });
        },
        [pageButton.pagePrint]: () => { //打印单据
            let funcode = (this.props.getUrlParam("appcode") == undefined || this.props.getUrlParam("appcode") == '') ? this.props.getSearchParam("c") : this.props.getUrlParam("appcode");
            if (funcode && funcode.endsWith("SH")) {
                funcode = funcode.substr(0, funcode.length - 2)
            }
            print(
                'pdf',  //支持两类: 'html'为模板打印, 'pdf'为pdf打印
                '/nccloud/sscwo/freebill/FreeBillPrintAction.do', //后台服务url
                {
                    funcode: funcode,
                    nodekey: 'dbl_' + window.presetVar.billtype,     //模板节点标识
                    userjson: JSON.stringify({ "transtype": window.presetVar.transtype }),
                    oids: [this.props.getUrlParam("billId") || this.props.getUrlParam("id")]
                }
            )
        },
        [pageButton.pageEdit]: () => {
            let props = this.props;
            scene = props.getUrlParam('scene');
            //获取单据编号
            let djbh = props.form.getFormItemsValue("head", ['billno'])[0].value;
            let canEdit = true;
            //来源于审批中心
            if (scene == 'approve' || scene == 'approvesce') {
                //判断单据是否是当前用户待审批
                ajax({
                    url: '/nccloud/riart/message/list.do',
                    async: false,
                    data: {
                        billno: djbh,
                        isread: 'N'
                    },
                    success: result => {
                        if (result.data) {
                            if (result.data.total < 1) {
                                toast({ content: multiLang && multiLang.get("702001GDFL-0036"), color: 'warning' });
                                canEdit = false;
                            }
                        }
                    }
                });
            }
            //来源于我的作业
            if (scene == 'zycl') {
                //判断单据是否是当前用户待处理
                ajax({
                    url: '/nccloud/ssctp/sscbd/SSCTaskHandlePendingNumAction.do',
                    async: false,
                    data: {
                        billno: djbh
                    },
                    success: result => {
                        if (result.data) {
                            if (result.data.total < 1) {
                                toast({ content: multiLang && multiLang.get("702001GDFL-0037"), color: 'warning' });
                                canEdit = false;
                            }
                        }
                    }
                });
            }
            if (!canEdit) {
                return;
            }
            let pagecode = window.presetVar.pageCode;
            let appcode = window.presetVar.appcode;
            // props.setUrlParam({status:statusVar.edit});
            // 临时方案适配平台linkTo异常问题，审批、作业处理页面点击修改，去除linkTO add by wangwph
            // initTemplate.call(this,props,false)
            // debugger;
            props.form.setFormStatus(head1, statusVar.edit);
            props.form.setFormItemsDisabled(head1, { 'pk_org': true }); // 状态为修改的情况下，pk_org不允许修改
            props.cardTable.setStatus(body1, statusVar.edit);
            // TOP NCCLOUD-60141 Add
            setButtonShowHide(props, 'edit');
            // BTM 
            // TOP NCCLOUD-60141 MOD
            // if(sceneout == 'approve' || sceneout == 'zycl' || scene == 'zycx') {
            // let scene = getUrlParam(props, 'scene');
            let scene = props.getUrlParam('scene');
            // if(scene == 'approve' || scene == 'zycl' || scene == 'zycx') {
            // BTM 
            props.setUrlParam({ status: statusVar.edit });
            let t = window.presetVar.pageStatus || props.getUrlParam('status');
            console.log(t);
            props.button.setButtonsVisible({
                [pageButton.pageSave]: true,
                [pageButton.billSaveSubmit]: true,
                [pageButton.pageEdit]: false,
                ['mtapp_detail_Add']: true

            });

            let reqData = {
                uistate: 'add',
                tradetype: window.presetVar.transtype,
                userjson: "{'pagecode': '" + pagecode + "'}",
                appcode: appcode,
            };
            requestApi.loadData({
                props: props,
                meta: props.meta.getMeta(),
                data: reqData,
                success: (data) => {
                    window.presetVar.pageStatus = statusVar.edit;
                    requestApi.loadDataHandle(data.data, props);
                    requestApi.tableExtendColHandle(props);
                }
            });
        },
        [pageButton.imageUpload]: () => {//影像扫描
            // let props = this.props;
            // let allData = props.createMasterChildData(pageId, head1, body1);
            // let filterEmptyData = filterEmptyRows(allData);
            // let openbillid = props.getUrlParam('billId');
            // let transtype =  window.presetVar.transtype;
            // imageScan(filterEmptyData, openbillid, transtype, 'iweb');


            let props = this.props;
            // let openbillid = props.getUrlParam('billId');
            let billdata = props.createMasterChildData(pageId, head1, body1);

            var billInfoMap = {};

            //基础字段 单据pk,单据类型，交易类型，单据的组织
            billInfoMap.pk_billid = billdata.head.head.rows[0].values.pk_freeform.value;
            billInfoMap.pk_billtype = billdata.head.head.rows[0].values.billtype.value;
            billInfoMap.pk_tradetype = billdata.head.head.rows[0].values.transtype.value;
            billInfoMap.pk_org = billdata.head.head.rows[0].values.pk_org.value;
            console.log(billdata.head.head.rows[0].values);
            //影像所需 FieldMap
            billInfoMap.BillType = billdata.head.head.rows[0].values.transtype.value;
            billInfoMap.BillDate = billdata.head.head.rows[0].values.creationtime.value;
            billInfoMap.Busi_Serial_No = billdata.head.head.rows[0].values.pk_freeform.value;
            billInfoMap.pk_billtype = billdata.head.head.rows[0].values.billtype.value;
            billInfoMap.OrgNo = billdata.head.head.rows[0].values.pk_org.value;
            billInfoMap.BillCode = billdata.head.head.rows[0].values.billno.value;
            billInfoMap.OrgName = billdata.head.head.rows[0].values.pk_org.display;
            billInfoMap.Cash = billdata.head.head.rows[0].values.amount.value;

            console.log("billInfoMap" + JSON.stringify(billInfoMap));
            imageScan(billInfoMap, 'iweb');

        }
        ,
        [pageButton.imageShow]: () => {//影像查看
            // let props = this.props;
            // let showData = props.createMasterChildData(pageId, head1, body1);
            // let filtershowEmptyData = filterEmptyRows(showData);
            // let openShowbillid = props.getUrlParam('billId');
            // let transtype =  window.presetVar.transtype;
            // imageView(filtershowEmptyData, openShowbillid, transtype, 'iweb');

            let props = this.props;
            let billdata = props.createMasterChildData(pageId, head1, body1);
            let openbillid = billdata.head.head.rows[0].values.pk_freeform.value;
            var billInfoMap = {};

            //基础字段 单据pk,单据类型，交易类型，单据的组织
            billInfoMap.pk_billid = openbillid;
            billInfoMap.pk_billtype = billdata.head.head.rows[0].values.billtype.value;
            billInfoMap.pk_tradetype = billdata.head.head.rows[0].values.transtype.value;
            billInfoMap.pk_org = billdata.head.head.rows[0].values.pk_org.value;
            imageView(billInfoMap, 'iweb');
        }
        ,
        [pageButton.billSubmit]: () => {//提交
            let props = this.props;
            let appcode = window.presetVar.appcode;

            let allData = props.createMasterChildData(pageId, head1, body1);
            allData.status = window.presetVar.pageStatus || props.getUrlParam("status");
            allData.pageCode = window.presetVar.pageCode;
            allData.transType = props.form.getFormItemsValue(head1, 'transtype').value;


            requestApi.billSubmit({
                data: allData,
                success: (data) => {
                    window.presetVar.pageStatus = statusVar.browse;
                    // location.hash = `#status=${statusVar.browse}&c=${appcode}&billId=${props.form.getFormItemsValue(head1, 'pk_freeform').value}&transtype=${props.form.getFormItemsValue(head1, 'transtype').value}`;
                    if (data.data.workflow && (data.data.workflow == 'approveflow' || data.data.workflow == 'workflow')) {
                        this.setState({
                            compositedata: data.data,
                            compositedisplay: true
                        });
                        return;
                    }
                    window.assignUsers = null;
                    this.setState({
                        compositedisplay: false
                    });
                    props.linkTo(location.pathname, { status: statusVar.browse, c: appcode, billId: props.form.getFormItemsValue(head1, 'pk_freeform').value, transtype: props.form.getFormItemsValue(head1, 'transtype').value });

                    //设置表头数据
                    props.form.setAllFormValue({ [head1]: data.data.head[head1] })
                    //设置表体数据
                    data.data.body && this.props.cardTable.setTableData(body1, data.data.body[body1]);
                    //处理按钮状态
                    // dataHandle(data.data.head[head1],props);
                    // alert("提交成功");
                    props.button.setButtonVisible(
                        [
                            pageButton.pageCopy,   // 复制
                            pageButton.billRecall, // 收回
                            pageButton.imageUpload,// 影响扫描
                            pageButton.imageShow,  // 影响查看
                            pageButton.linkVoucher,  // 联查凭证
                            pageButton.pagePrint,
                            "LinkApprove"
                        ],
                        true);
                    props.button.setButtonVisible(
                        [
                            pageButton.pageSave,    // 保存
                            pageButton.pageClose,// 取消
                            pageButton.billSaveSubmit,// 保存提交
                            pageButton.billSubmit,  // 提交
                            pageButton.pageDel,     // 删除
                            pageButton.pageEdit     // 修改
                        ],
                        false);
                    props.form.setFormStatus(head1, statusVar.browse);
                    props.cardTable.setStatus(body1, statusVar.browse);
                }
            });
        },
        [pageButton.billSaveSubmit]: () => {//保存提交
            let props = this.props;
            // props.form.setFormStatus(head1, statusVar.browse);
            // props.cardTable.setStatus(body1, statusVar.browse);
            let appCode = window.presetVar.appcode;
            let allData = props.createMasterChildData(pageId, head1, body1);
            allData.status = window.presetVar.pageStatus || props.getUrlParam("status");
            allData.pageCode = window.presetVar.pageCode;
            allData.transType = props.form.getFormItemsValue(head1, 'transtype').value;
            // 获取模板中表体隐藏字段
            let filterEmptyData = allData;
            if (!window.presetVar.isSingle) {
                let template = props.meta.getMeta();
                let bodyItems = template.freebill_detaill.items;
                let hiddenItems = bodyItems.filter(item => item.visible !== true);
                let hiddenAttrs = hiddenItems.map(item => item.attrcode);
                // 过滤空行
                filterEmptyData = filterEmptyRows(allData, hiddenAttrs);
            }
            requestApi.billSubmit({
                data: filterEmptyData,
                success: (data) => {
                    window.presetVar.pageStatus = statusVar.browse;
                    if (data.data.workflow && (data.data.workflow == 'approveflow' || data.data.workflow == 'workflow')) {
                        //制单人指派场景
                        props.form.setAllFormValue({ [head1]: data.data.billdata.head[head1] })
                        props.form.setFormStatus(head1, statusVar.edit);

                        if (!window.presetVar.isSingle) {
                            //设置表体数据
                            data.data.billdata.body && this.props.cardTable.setTableData(body1, data.data.billdata.body[body1]);
                            props.cardTable.setStatus(body1, statusVar.edit);
                        }

                        let billid = data.data.billdata.head[head1].rows[0].values.billid.value;
                        props.addUrlParam({ status: statusVar.browse, c: props.getUrlParam('c'), billId: billid, transtype: props.form.getFormItemsValue(head1, 'transtype').value });
                        window.presetVar.pageStatus = statusVar.browse;
                        this.setState({
                            compositedata: data.data,
                            compositedisplay: true
                        });
                        return;
                    } else if (data.data.savedVO) {
                        //保存成功 提交失败场景
                        props.form.setAllFormValue({ [head1]: data.data.savedVO.head[head1] })
                        props.form.setFormStatus(head1, statusVar.browse);

                        if (!window.presetVar.isSingle) {
                            //设置表体数据
                            data.data.savedVO.body && this.props.cardTable.setTableData(body1, data.data.savedVO.body[body1]);
                            props.cardTable.setStatus(body1, statusVar.browse);
                        }

                        let billid = data.data.savedVO.head[head1].rows[0].values.billid.value;
                        props.addUrlParam({ status: statusVar.browse, c: props.getUrlParam('c'), billId: billid, transtype: props.form.getFormItemsValue(head1, 'transtype').value });
                        window.presetVar.pageStatus = statusVar.browse;
                        setButtonShowHide(props, statusVar.browse);
                        toast({ content: data.data.message, color: 'danger' });
                        return;
                    }

                    window.assignUsers = null;
                    this.setState({
                        compositedisplay: false
                    });
                    //设置表头数据
                    props.form.setAllFormValue({ [head1]: data.data.head[head1] });
                    //设置表体数据
                    data.data.body && props.cardTable.setTableData(body1, data.data.body[body1]);

                    //   location.hash = `#status=${statusVar.browse}&c=${appCode}&billId=${props.form.getFormItemsValue(head1, 'pk_freeform').value}&transtype=${props.form.getFormItemsValue(head1, 'transtype').value}`;
                    props.linkTo(location.pathname, { status: statusVar.browse, c: appCode, billId: props.form.getFormItemsValue(head1, 'pk_freeform').value, transtype: props.form.getFormItemsValue(head1, 'transtype').value });


                    //设置表头数据
                    props.form.setAllFormValue({ [head1]: data.data.head[head1] })
                    //设置表体数据
                    data.data.body && this.props.cardTable.setTableData(body1, data.data.body[body1]);
                    //处理按钮状态
                    // dataHandle(data.data.head[head1],props);
                    // alert("提交成功");
                    // props.button.setButtonsVisible({  // setButtonsVisible 函数有bug
                    //     [pageButton.pageCopy]:true,    
                    //     [pageButton.billRecall]: true,  // 收回
                    //     [pageButton.imageUpload]:true,  // 影响扫描
                    //     [pageButton.imageShow]:true,   // 影响查看
                    //     [pageButton.linkVoucher]:true,  // 联查凭证
                    //     [pageButton.pageSave]:false,   // 保存
                    //     [pageButton.pageCancel]:false, // 取消
                    //     [pageButton.billSaveSubmit]:false, // 保存提交
                    //     [pageButton.billSubmit]: false, // 提交
                    //     [pageButton.pageDel]: false,    // 删除
                    //     [pageButton.pageEdit]: false,   // 修改

                    // });
                    props.button.setButtonVisible(
                        [
                            pageButton.pageCopy,   // 复制
                            pageButton.billRecall, // 收回
                            pageButton.imageUpload,// 影响扫描
                            pageButton.imageShow,  // 影响查看
                            pageButton.linkVoucher,  // 联查凭证
                            pageButton.pagePrint,
                            "LinkApprove"
                        ],
                        true);
                    props.button.setButtonVisible(
                        [
                            pageButton.pageSave,    // 保存
                            pageButton.pageClose,// 取消
                            pageButton.billSaveSubmit,// 保存提交
                            pageButton.billSubmit,  // 提交
                            pageButton.pageDel,     // 删除
                            pageButton.pageEdit     // 修改
                        ],
                        false);
                    props.form.setFormStatus(head1, statusVar.browse);
                    props.cardTable.setStatus(body1, statusVar.browse);
                }
            });
        }
        ,
        [pageButton.billRecall]: () => {//收回
            let props = this.props;

            props.form.setFormStatus(head1, statusVar.browse);
            props.cardTable.setStatus(body1, statusVar.browse);
            let allData = props.createMasterChildData(pageId, head1, body1);
            requestApi.billRecall({
                data: allData,
                success: (data) => {
                    //alert("收回成功")
                    //设置表头数据
                    props.form.setAllFormValue({ [head1]: data.data.head[head1] })
                    //设置表体数据
                    data.data.body && this.props.cardTable.setTableData(body1, data.data.body[body1]);
                    //处理按钮状态
                    props.button.setButtonsVisible({
                        [pageButton.billRecall]: false,
                        [pageButton.billSubmit]: true,
                        [pageButton.pageDel]: true,
                        [pageButton.pageEdit]: true,
                    });
                }
            });
        }, [pageButton.fileManager]: () => {//附件管理
            let props = this.props;
            let billId = props.getUrlParam("billId") || props.getUrlParam("id") || props.getUrlParam('openbillid') || this.state.billId || "";
            // this.setState({ showUploader: !this.state.showUploader });
            if (billId == "" && !this.state.showUploader) {
                //去后台生成单据主键
                requestApi.generatBillId({
                    data: { billtype: window.presetVar.billtype },
                    success: (data) => {
                        billId = data.data['pk_bill'];
                        this.setState({ billId: billId, showUploader: !this.state.showUploader });
                        this.accessorybillid = billId;
                    },
                    error: (data) => {
                        alert(data.message);
                    }
                });
            } else {
                //this.setState({ billId: billId });
                this.setState({
                    billId: billId,
                    showUploader: !this.state.showUploader
                });
            }
        }, [pageButton.linkVoucher]: () => {//联查凭证
            let props = this.props;
            let pageId = window.presetVar.pageId;

            props.form.setFormStatus(head1, statusVar.browse);
            props.cardTable.setStatus(body1, statusVar.browse);
            let allData = props.createMasterChildData(pageId, head1, body1);

            let openbillid = "";
            let tradetype = "";
            let pk_group = "";
            let pk_org = "";
            let billdate = "";
            let billtype = "";
            allData.head[presetVar.head.head1].rows.map((row) => {
                for (let key in row.values) {
                    if (row.values[key].value || row.values[key].value == 0) {
                        if (key === "pk_freeform") {
                            openbillid = row.values[key].value;
                        }
                        if (key === "transtype") {
                            tradetype = row.values[key].value;
                        }
                        if (key === "pk_group") {
                            pk_group = row.values[key].value;
                        }
                        if (key === "pk_org") {
                            pk_org = row.values[key].value;
                        }
                        if (key === "billdate") {
                            billdate = row.values[key].value;
                        }
                        if (key === "billtype") {
                            billtype = row.values[key].value;
                        }
                    }
                }
                return row;
            });

            let queryData = [{
                pk_billtype: tradetype,
                pk_group: pk_group,
                pk_org: pk_org,
                relationID: openbillid
            }]

            let { openTo, getUrlParam, getSearchParam } = props;

            requestApi.linkVoucher({
                props: props,
                data: queryData,
                success: (res) => {
                    if (!res || !res.success) return
                    let { data: {
                        src, appcode, pagecode, des, pklist, url, cachekey,
                    } } = res

                    if (src === '_LinkVouchar2019') {// 联查
                        if (des) { //跳转到凭证界面
                            if (pklist) {
                                if (pklist.length == 1) { //单笔联查
                                    openTo(url, {
                                        status: 'browse',
                                        appcode,
                                        pagecode,
                                        id: pklist[0],
                                        n: multiLang && multiLang.get['7020PZ-001'],//'联查凭证'
                                        pagekey: 'link',
                                        backflag: 'noback'
                                    })
                                    return
                                } else { //多笔联查
                                    cacheTools.set(cachekey, pklist)
                                    openTo(url, {
                                        status: 'browse',
                                        appcode,
                                        pagecode,
                                        n: multiLang && multiLang.get['7020PZ-001'], // '凭证预览' 
                                        scene: appcode + src //多笔联查新加scene字段
                                    })
                                    return
                                }
                            }
                        } else { //跳转到会计平台 这里的appcode是业务组的小应用编码
                            cacheTools.set((getUrlParam('c') || getSearchParam('c')) + src, pklist);
                        }
                    } else if (src === '_Preview2019') {// 预览
                        //拼接预览数据
                        const viewData = {
                            messagevo: [{
                                pk_system: 'sscivm', //系统编码
                                busidate: billdate,//单据日期
                                relationid: openbillid,//单据主键
                                pk_org: pk_org,//组织主键 
                                pk_group: pk_group,//集团主键
                                pk_billtype: tradetype//交易类型
                            }],//支持多笔预览
                            desbilltype: ['C0'],//目标单据类型,C0为总账凭证,目前只支持总张凭证
                            srcbilltype: billtype.value || tradetype//业务组交易类型
                        }
                        cacheTools.set(appcode + src, viewData);
                    }
                    // window.top.environmentmodel='development'
                    //打开凭证节点
                    // url=url.slice(18);
                    openTo(url, {
                        status: 'browse',
                        appcode,
                        pagecode,
                        scene: appcode + src,
                        n: multiLang && multiLang.get['7020PZ-001'] // '凭证预览' 
                    });
                }
            });
        }, [pageButton.fileView]: () => {//附件查看

            alert(multiLang && multiLang.get("702001GDFL-0039"));
        }, [body1 + areaBtnAction.add]: {
            actionCode: 'Add'
        }, ['LinkApprove']: () => {
            let props = this.props;
            let pageId = window.presetVar.pageId;
            let billdata = props.createMasterChildData(pageId, head1, body1);
            let transtype = billdata.head.head.rows[0].values.transtype.value;
            let billId = billdata.head.head.rows[0].values.pk_freeform.value;
            this.setState({
                showLinkApprove: true,
                billId: billId,
                transtype: transtype
            });
        }
    }
}

/*
 * @method   if条件下为false   除去NaN、0、-0、false   剩余undefined、null、""
 * @author   add by yangguoqiang @18/03/19
 * @params
 *     condition     {any}
 * @return   {boolean}       返回ture/false
 * @demo     isWrong('')    因为后台返回数据不规范
 */
function isWrongFalse(param) {
    return typeof param === 'undefined' || param === null || param === '' || param === false;
}


/**
 * modify by qiufh@yonyou.com from the platform source
 * 32、移除没有输入内容，所有的空行
 * @param  tableId   meta的id号
 * @param  key       data的键值
 */
function filterEmptyRows(allData, keys) {
    let filterData;
    if (!window.presetVar.isSingle && allData.body.freebill_detaill.rows && allData.body.freebill_detaill.rows.length > 0) {
        allData.body.freebill_detaill.rows.forEach((item, index) => {
            let values = item.values;
            let tempArr = Object.keys(values).filter(item => item != 'numberindex');
            if (Array.isArray(keys)) {
                tempArr = tempArr.filter(function (val) {
                    return keys.every(function (key) {
                        return val !== key
                    })
                })
            }
            // flag 为true 说明每个字段  （要不然不是对象  TODO ? 应该不需要判断 略）   或者 没值
            let flag = tempArr.every(one => (isWrongFalse(values[one].value)))
            flag && (delete allData.body.freebill_detaill.rows[index])
        })
        allData.body.freebill_detaill.rows = allData.body.freebill_detaill.rows.filter(item => !!item)
        filterData = allData;
        return filterData;
    }
    return allData;
}

export default pageButtonClick;
