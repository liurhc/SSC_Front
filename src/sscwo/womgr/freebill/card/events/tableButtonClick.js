/**
 * 表格肩部按钮click事件
 */
import {ajax} from 'nc-lightapp-front';
import afterEvent from './afterEvent';
import {headSumHandle, getBodyDefaultValue} from './commonEvent';
const areaBtnAction = window.presetVar.areaBtnAction;

const tableShoulderBtnAction = window.presetVar.tableShoulderBtnAction;
const statusVar = window.presetVar.status;
const {body1, body2} = window.presetVar.body;

function tableButtonClick() {
    return {

        [body1 + areaBtnAction.add] : () => {
            let props = this.props;
            // 增行，并初始化默认值
            props.cardTable.addRow(body1, undefined, getBodyDefaultValue(props));

            // 合计金额到表头
            headSumHandle.call(this, props);

        }
    }
}

export default tableButtonClick;