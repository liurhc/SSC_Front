/**
 * 后台接口文件
 */
import requestApi from "ssccommon/components/requestApi";
import {setTableExtendCol} from 'ssccommon/components/bill';
import {tableExtendButtonClick} from './events';
import {ajax} from 'nc-lightapp-front';
import {toast} from 'nc-lightapp-front';

const presetVar = window.presetVar;
const statusVar = window.presetVar.status;
const areaBtnAction = window.presetVar.areaBtnAction;
const head1 = window.presetVar.head.head1;
const body1 = window.presetVar.body.body1;

let requestDomain = '';
let requestApiOverwrite = {
    ...requestApi,
    query: (opt) => {
        let pageCode = window.presetVar.pageCode;
        let transtype = window.presetVar.transtype;

        opt.data = {
            openbillid: opt.data.billid,
            tradetype: transtype,
            pk_doc: window.presetVar.pk_doc,
            userjson: "{'pagecode':" + pageCode + "}"
        }

        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscwo/freebill/FreeBillViewBillAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    pageInit: (opt) => {

        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscwo/freebill/FreeBillPageInitAction.do`,
            data: opt.data,
            async: false,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    generatBillId: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscwo/freebill/FreeBillGeneratBillIdAction.do`,
            data: opt.data,
            async: false,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    save: (opt) => {
        let pageCode = window.presetVar.pageCode;
        let transtype = window.presetVar.transtype;

        opt.data.pageid = pageCode;
        opt.data = {
            ...opt.data,
            uistate: opt.status,
            tradetype: transtype,
            pk_doc: window.presetVar.pk_doc,
            userjson: "{'pagecode':" + pageCode + "}"
        }

        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscwo/freebill/FreeBillSaveBillAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    billRemove: (opt) => {
        //数据适配开始
        let openbillid = "";
        let tradetype = "";
        opt.data.head[presetVar.head.head1].rows.map((row) => {
            for (let key in row.values) {
                if (row.values[key].value || row.values[key].value == 0) {
                    if (key === "pk_freeform") {
                        openbillid = row.values[key].value;
                    }
                    if (key === "transtype") {
                        tradetype = row.values[key].value;
                    }
                }
            }
            return row;
        });
        //数据适配结束
        let pageCode = window.presetVar.pageCode;

        opt.data = {
            openbillid: openbillid,
            tradetype: tradetype,
            userjson: "{'pagecode':" + pageCode + "}"
        }
        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscwo/freebill/FreeBillDeleteBillAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    linkVoucher: (opt) => {
        ajax({
            // url: `${requestDomain}/nccloud/sscwo/freebill/FreeBillHasDesBill.do`,
            url: `${requestDomain}/nccloud/sscwo/freebill/FreeBillVoucherLinkQueryAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })

    },
    loadData: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscwo/freebill/FreeBillDefDataAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    changeByHead: (opt) => {
        let pageCode = window.presetVar.pageCode;

        opt.data = {
            ...opt.data,
            pk_doc: window.presetVar.pk_doc,
            userjson: "{'pagecode':" + pageCode + "}"
        }
        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscwo/freebill/FreeBillValueChangedAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        });
    },
    changeByBody: (opt) => {
        let pageCode = window.presetVar.pageCode;

        opt.data = {
            ...opt.data,
            pk_doc: window.presetVar.pk_doc,
            userjson: "{'pagecode':" + pageCode + "}"
        }
        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscwo/freebill/FreeBillValueChangedAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        });
    },
    copy: (opt) => { //单据复制
        let pageCode = window.presetVar.pageCode;
        let transtype = window.presetVar.transtype;

        opt.data = {
            ...opt.data,
            pageid: pageCode,
            pk_doc: window.presetVar.pk_doc,
            userjson: "{'pagecode':" + pageCode + "}",
            tradetype: transtype,
        }
        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscwo/freebill/FreeBillCopyBillAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    billSubmit: (opt) => {

        let openbillid = "";
        opt.data.head[presetVar.head.head1].rows.map((row) => {
            for (let key in row.values) {
                if (row.values[key].value || row.values[key].value == 0) {
                    if (key === "pk_freeform")
                        openbillid = row.values[key].value;
                }
            }
            return row;
        });

        let pageCode = window.presetVar.pageCode;

        opt.data.pageid = pageCode;
        opt.data = {
            ...opt.data,
            pk_doc: window.presetVar.pk_doc,
            userjson: "{'pagecode':" + opt.data.pageCode +
                ",'openbillid':'" + openbillid +
                "','tradetype':" + opt.data.transType +
                ",'ntbCheck':'N'" +
                ",'accessorybillid':" + null +
                ",'fromssc':'N'" +
                ",'uistate':" + opt.data.status +
                (window.assignUsers ? (",'assignUsers':" + JSON.stringify(window.assignUsers)) : "") +
                "}"
        }
        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscwo/freebill/FreeBillSubmitAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            },
            error: (data) => {
                toast({content: data.message, color: 'danger'});
                opt.error(data);
            }
        });
    },

    billRecall: (opt) => {
        let openbillid = "";
        opt.data.head[presetVar.head.head1].rows.map((row) => {
            for (let key in row.values) {
                if (row.values[key].value || row.values[key].value == 0) {
                    if (key === "pk_freeform")
                        openbillid = row.values[key].value;
                }
            }
            return row;
        });
        //数据适配结束
        let pageCode = window.presetVar.pageCode;
        let transtype = window.presetVar.transtype;

        opt.data = {
            openbillid: openbillid,
            tradetype: transtype,
            pk_doc: window.presetVar.pk_doc,
            userjson: "{'pagecode':" + pageCode + "}"
        }
        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscwo/freebill/FreeBillRecallAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            },
            error: (data) => {
                toast({content: data.message, color: 'danger'});
                opt.error(data);
            }
        });
    },

    hasDesBill: (opt) => {
        ajax({
            url: `${requestDomain}/nccloud/sscwo/freebill/FreeBillHasDesBill.do`,
            data: opt.data,
            async: false,
            success: (data) => {
                if (data.success) {
                    data = data.data;
                    opt.success(data);
                }
            }
        })
    },

    metaDataHandle: function (meta, props) {
        let pageStatus = window.presetVar.pageStatus || props.getUrlParam("status"); // 状态：add/edit/browse

        // 1、模板获取
        //let meta = props.meta.getMeta();
        let headItems = meta[head1].items;
        // 在模板中加入2位精度
        // headItems.find((item) => (item.attrcode == 'amount')).scale = "2";
        if (!window.presetVar.isSingle) {
            headItems.find((item) => (item.attrcode == 'amount')).disabled = true;
        }
        // 表体
        // if(meta[body1] != undefined && meta[body1] != null) {
        //     let bodyItems = meta[body1].items;
        //     bodyItems.find((item) => (item.attrcode == 'amount')).scale = "2";
        // }

        // 表头
        // 根据编码规则配置，设置单据号是否可编辑
        let ruleSet = window.presetVar.billNoIsEditable; // false为不可编辑
        let tmplSet = headItems.find((e) => (e.attrcode == 'billno')).disabled ? true : false; // true为不可编辑
        // 当编码规则的配置和模板的配置都为可编辑的时候才可编辑
        if (ruleSet == true && tmplSet == false) {
            headItems.find((e) => (e.attrcode == 'billno')).disabled = false;
        } else {
            headItems.find((e) => (e.attrcode == 'billno')).disabled = true;
        }

        // 状态为修改的情况下，pk_org不允许修改
        if (pageStatus == presetVar.status.edit) {
            headItems.find((item) => (item.attrcode == 'pk_org')).disabled = true;
        }

        // 设置区域模板
        console.log(meta)
        props.meta.setMeta(meta);
    },

    tableExtendColHandle: function (props) {
        tableExtendColHandle.call(this, props);
    },

    loadDataHandle: function (data, props) {
        loadDataHandle.call(this, data, props);
    }
}

// 表格扩展列处理
// 策略：如果表体扩展列不存在，必须创建，然后根据状态切换进行扩展列显示和隐藏
function tableExtendColHandle(props) {
    let pageStatus = window.presetVar.pageStatus || props.getUrlParam('status'); //状态变量
    let hasTableExtCol = false; //是否存在扩展列
    let isShowTableExtCol = false; //是否显示表格扩展列
    if (!window.presetVar.isSingle) {
        // 判断最后一列是否为扩展列
        if (props.meta.getMeta()[body1].items[props.meta.getMeta()[body1].items.length - 1].attrcode == 'opr') {
            hasTableExtCol = true;
        }

        // 创建表格扩展列
        if (hasTableExtCol == false) {
            setTableExtendCol.bind(this)(props, props.meta.getMeta(), [{
                tableAreaId: body1,
                btnAreaId: body1,
                copyRowWithoutCols: [],
                buttonVisible: (record, index) => {
                    return [
                        body1 + areaBtnAction.del,
                        body1 + areaBtnAction.edit,
                        body1 + areaBtnAction.copy,
                        body1 + areaBtnAction.insert
                    ]
                },
                onButtonClick: tableExtendButtonClick.bind(this)
            }]);
        }

        // 根据页面状态进行显示隐藏扩展列
        switch (pageStatus) {
            case statusVar.browse:
                isShowTableExtCol = false;
                break;
            case statusVar.add:
                isShowTableExtCol = true;
                break;
            case statusVar.edit:
                isShowTableExtCol = true;
                break;
        }

        if (isShowTableExtCol) {
            props.button.setButtonVisible(
                [
                    body1 + areaBtnAction.del,
                    body1 + areaBtnAction.copy,
                    body1 + areaBtnAction.insert
                ],
                true);

        } else {
            props.button.setButtonVisible(
                [
                    body1 + areaBtnAction.del,
                    body1 + areaBtnAction.copy,
                    body1 + areaBtnAction.insert
                ],
                false);
        }
    }
}

// 单据过滤规则处理 data:单据默认数据
function loadDataHandle(data, props) {
    // 1、模板获取
    let meta = props.meta.getMeta();

    // 2、表头处理
    let headValues = data.head[head1].rows[0].values;
    meta[head1].items.forEach((item) => {
        let attrcode = item.attrcode;

        // 2.1、更新默认值
        if (headValues[attrcode] != null && headValues[attrcode] != undefined) {
            item.initialvalue = {
                value: headValues[attrcode].value,
                scale: headValues[attrcode].scale,
                display: headValues[attrcode].display = null ? headValues[attrcode].value : headValues[attrcode].display
            };
        }

        // 2.2、处理交叉校验规则和参照过滤
        filterAllRefParm(item, props, head1);
    });

    //适配多表头
    if (meta.formrelation) {
        meta.formrelation[presetVar.head.head1].forEach((item) => {
            if (meta[item]) {
                meta[item].items.forEach((key) => {
                    let attrcode = key.attrcode;
                    if (headValues[attrcode]) {
                        key.initialvalue = {
                            value: headValues[attrcode].value,
                            scale: headValues[attrcode].scale,
                            display: headValues[attrcode].display = null ? headValues[attrcode].value : headValues[attrcode].display
                        };
                        //处理交叉校验规则和参照过滤
                        filterAllRefParm(key, props, presetVar.head.head1);
                    }
                })
            }

        });
    }

    // 3、表体处理
    if (data.body != null && data.body != undefined && data.body != 'undefined') {
        let bodyValues = data.body[body1].rows[0].values;
        meta[body1].items.forEach((item) => {
            let attrcode = item.attrcode;

            // 3.1、更新默认值
            if (bodyValues[attrcode] != null && bodyValues[attrcode] != undefined) {
                item.initialvalue = {
                    value: bodyValues[attrcode].value,
                    display: bodyValues[attrcode].display = null ? bodyValues[attrcode].value : bodyValues[attrcode].display
                };


                // if (bodyValues[attrcode].scale != -1) {
                //     item.scale = bodyValues[attrcode].scale;
                // }
            }
            //模板中的金额增加默认值placeholder属性,9.20 平台下次出盘生效
            //       if(item.attrcode=='amount'){
            //         item.placeholder=6;
            //    }
            // 3.2、处理交叉校验规则和参照过滤
            filterAllRefParm(item, props, body1);
        });
        meta[body1] && meta[body1].items && meta[body1].items.forEach((item) => {
            let attrcode = item.attrcode;

            // 3.1、更新默认值
            if (bodyValues[attrcode] != null && bodyValues[attrcode] != undefined) {
                item.initialvalue = {
                    value: bodyValues[attrcode].value,
                    display: bodyValues[attrcode].display = null ? bodyValues[attrcode].value : bodyValues[attrcode].display
                };
            }
            // 3.2、处理交叉校验规则和参照过滤
            filterAllRefParm(item, props, body1);
        });
    }

    props.meta.setMeta(meta);
}

// 属性过滤规则处理
function filterAllRefParm(item, props, flag) {
    if (!item.visible) return;
    let PublicDefaultRefFilterPath = 'nccloud.web.sscwo.ref.sqlbuilder.PublicDefaultRefSqlBuilder';//公共 
    let OrgTreeRefFilterPath = 'nccloud.web.sscwo.ref.sqlbuilder.OrgTreeRefSqlBuilder';//组织(包含组织权限过滤规则)

    // 配置所属组织的参照过滤，根据组织权限参照过滤
    if (item.attrcode == 'pk_org') {
        item.queryCondition = () => {
            let pk_group = (flag == head1 ? (props.form.getAllFormValue(head1).rows[0].values['pk_group'].value) : (props.cardTable.getClickRowIndex(body1).record.values['pk_group'].value))
            return {
                pk_group: pk_group,
                TreeRefActionExt: OrgTreeRefFilterPath,
                isDataPowerEnable: 'Y'
            };
        }
    }
    // 配置部门的参照过滤，根据所属组织参照过滤
    else if (item.attrcode == 'dept') {
        item.queryCondition = () => {
            let deptItem = props.form.getAllFormValue(head1).rows[0].values['pk_org'];
            let deptVal = deptItem ? deptItem.value : null;
            return {
                pk_org: deptVal,
                isDataPowerEnable: 'Y'
            };
        };
    }
    // 配置人员的参照过滤，根据所属组织参照过滤
    else if (item.attrcode == 'psndoc') {
        item.queryCondition = () => {
            let orgItem = props.form.getAllFormValue(head1).rows[0].values['pk_org'];
            let deptItem = (flag == head1 ? (props.form.getAllFormValue(head1).rows[0].values['dept']) : (props.cardTable.getClickRowIndex(presetVar.body.body1).record.values['dept']));
            let orgVal = orgItem ? orgItem.value : null;
            let deptVal = deptItem ? deptItem.value : null;

            return {
                pk_org: orgVal,
                pk_dept: deptVal,
                isDataPowerEnable: 'Y'
            };
        };
    } else if (item.itemtype == 'refer') {
        item.queryCondition = () => {
            let orgItem = props.form.getAllFormValue(head1).rows[0].values['pk_org'];
            let orgVal = orgItem ? orgItem.value : null;

            return {
                pk_org: orgVal,
                isDataPowerEnable: 'Y'
            };
        };
    }

    // 处理交叉校验规则
    if (item.itemtype == 'refer') {
        let oldQueryCondition = item.queryCondition;
        let crossrule_itemkey = item.attrcode;
        item.queryCondition = (obj) => {
            window.presetVar.bodyChangeValuesAll;
            let crossrule_datasout = props.createMasterChildData(presetVar.pageId, presetVar.head.head1, presetVar.body.body1);
            let crossrule_tradetype = props.form.getAllFormValue(presetVar.head.head1).rows[0].values['transtype'].value
            let crossrule_org = props.form.getAllFormValue(presetVar.head.head1).rows[0].values['pk_org'].value
            let crossrule_datatypes = 'billcard'; // billcard:一主一子
            let crossrule_datas = crossrule_datasout;
            if (window.presetVar.bodyChangeValuesAll != null) {
                crossrule_datas.body[flag].rows = [window.presetVar.bodyChangeValuesAll];
            }

            if (oldQueryCondition == null || oldQueryCondition == undefined || oldQueryCondition == 'undefined') {
                let config = {
                    // pk_org:crossrule_org,
                    crossrule_datas: JSON.stringify(crossrule_datas),
                    crossrule_tradetype: crossrule_tradetype,
                    crossrule_org: crossrule_org,
                    crossrule_datatypes: crossrule_datatypes,
                    crossrule_area: flag,
                    crossrule_itemkey: crossrule_itemkey
                };

                if (obj.refType == 'grid') {
                    config.GridRefActionExt = PublicDefaultRefFilterPath;
                } else if (obj.refType == 'tree') {
                    config.TreeRefActionExt = PublicDefaultRefFilterPath;
                } else if (obj.refType == 'gridTree') {
                    config.GridRefActionExt = PublicDefaultRefFilterPath;
                }

                return config;
            }

            let oldData = oldQueryCondition(obj);
            if (oldData == null || oldData == undefined || oldData == 'undefined') {
                let config = {
                    // pk_org:crossrule_org,
                    crossrule_datas: JSON.stringify(crossrule_datas),
                    crossrule_tradetype: crossrule_tradetype,
                    crossrule_org: crossrule_org,
                    crossrule_datatypes: crossrule_datatypes,
                    crossrule_area: flag,
                    crossrule_itemkey: crossrule_itemkey
                };

                if (obj.refType == 'grid') {
                    config.GridRefActionExt = PublicDefaultRefFilterPath;
                } else if (obj.refType == 'tree') {
                    config.TreeRefActionExt = PublicDefaultRefFilterPath;
                } else if (obj.refType == 'gridTree') {
                    config.GridRefActionExt = PublicDefaultRefFilterPath;
                }

                return config;
            } else {
                let config = {
                    // pk_org:crossrule_org,
                    crossrule_datas: JSON.stringify(crossrule_datas),
                    crossrule_tradetype: crossrule_tradetype,
                    crossrule_org: crossrule_org,
                    crossrule_datatypes: crossrule_datatypes,
                    crossrule_area: flag,
                    crossrule_itemkey: crossrule_itemkey,
                    ...oldData
                };

                if (!config.hasOwnProperty('TreeRefActionExt') && !config.hasOwnProperty('GridRefActionExt')) {
                    if (obj.refType == 'grid') {
                        config.GridRefActionExt = PublicDefaultRefFilterPath;
                    } else if (obj.refType == 'tree') {
                        config.TreeRefActionExt = PublicDefaultRefFilterPath;
                    } else if (obj.refType == 'gridTree') {
                        config.GridRefActionExt = PublicDefaultRefFilterPath;
                    }
                }

                return config;
            }
        }
    }
}

function getBillVO(data) {
    let billVO = {};
    data.head[presetVar.head.head1].rows.map((row) => {
        billVO.parentVO = {};
        for (let key in row.values) {
            if (row.values[key].value) {
                billVO.parentVO[key] = row.values[key].value;
            }
        }
    })
    if (!window.presetVar.isSingle) {//解决单主表联查凭证报错
        billVO.childrenVO = [];
        data.body[presetVar.body.body1].rows.map((row) => {
            let oneRowVO = {};
            for (let key in row.values) {
                if (row.values[key].value) {
                    oneRowVO[key] = row.values[key].value;
                }
            }
            billVO.childrenVO.push(oneRowVO);
        })
    }

    return billVO;

}

export default requestApiOverwrite;
