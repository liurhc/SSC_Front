import React, {Component} from 'react';
import ReactDOM from 'react-dom';

import FreeBill from './FreeBill';

ReactDOM.render((<FreeBill />)
    , document.querySelector('#app'));