import "./presetVar";
import React, {Component} from "react";
import {createPage, high, getAttachments} from "nc-lightapp-front";
import {tableButtonClick, pageButtonClick, afterEvent} from "./events";
import initTemplate from "./initTemplate";
import {initData} from "./initTemplate";
import {
    BillStyle,
    BillHead,
    BillBody,
    Form,
    CardTable,
    ButtonGroup
} from "ssccommon/components/bill";

import "./index.less";

const {NCUploader, ApproveDetail, ApprovalTrans} = high;

const pageButton = window.presetVar.pageButton;

class FreeBill extends Component {
    constructor(props) {
        if (NODE_ENV == "development") {
            window.NODE_ENV = "development";
        }
        super();
        this.state = {
            showUploader: false, // 显示上传组件
            groupLists: 0, // 更多下的附件按钮附件数
            billId: "", // 用于上传组件绑定
            showLinkApprove: false, // 显示审批详情
            compositedisplay: false, // 显示ApprovalTrans
            compositedata: '', // ApprovalTrans  data绑定
            transtype: "", // ApprovalDetai  billtype绑定
            btn: [
                presetVar.body.body1 + presetVar.areaBtnAction.del,
                presetVar.body.body1 + presetVar.areaBtnAction.edit
            ],
            Title: "", // 节点标题
            disabledUploadBtn: false // 禁用上传组件按钮

        };
        let billid = props.getUrlParam("billId") || props.getUrlParam("id");
        if (billid) {
            getAttachments(billid).then(lists => {

                this.setState({
                    groupLists: lists
                }, () => {
                    initTemplate.call(this, props, true);
                })
            })
        } else {
            initTemplate.call(this, props, true);
        }
        let starttime = new Date().getTime();
        console.log("startinit" + Date());

        console.log(new Date().getTime() - starttime + "endinit");
    }

    componentDidMount() {
    }

    componentWillMount() {
        window.onbeforeunload = () => {
            let status = this.props.getUrlParam("status");
            if (status == 'edit') {
                return ''
                /* 国际化处理： 确定要离开吗？*/
            }
        }
    }

    // ApprovalTrans getResult事件
    getAssginUser = (value) => {
        window.assignUsers = value;
        pageButtonClick.call(this)[pageButton.billSubmit]();
    }

    // showLinkApprove 取消事件
    turnOff() {
        let pageButton = window.presetVar.pageButton;
        let head1 = window.presetVar.head.head1;
        let body1 = window.presetVar.body.body1;
        let areaBtnAction = window.presetVar.areaBtnAction;
        this.setState({
            compositedisplay: false
        });
        this.props.form.setFormStatus(window.presetVar.head.head1, 'browse');
        this.props.cardTable.setStatus(window.presetVar.body.body1, 'browse');
        this.props.button.setButtonVisible(
            [
                pageButton.pageAdd,
                pageButton.pageCopy,
                pageButton.pageEdit,
                pageButton.pageDel,
                pageButton.billSubmit,
                pageButton.linkVoucher,
                pageButton.pagePrint,
                pageButton.pageSave,
                pageButton.fileManager,
                pageButton.imageShow,
                pageButton.imageUpload,
                pageButton.pageClose,
                'LinkApprove'
            ],
            true);
        this.props.button.setButtonVisible(
            [
                body1 + areaBtnAction.add,
                pageButton.pageSave,
                pageButton.pageClose,
                pageButton.billRecall,
                pageButton.pageClose,
                pageButton.billSaveSubmit,
                body1 + areaBtnAction.del,
                body1 + areaBtnAction.copy,
                body1 + areaBtnAction.insert
            ],
            false);
    }


    render() {
        let multiLang = this.props.MutiInit.getIntl(7020); //this.moduleId
        let btnModalConfig = {
            [pageButton.pageDel]: {
                //  "702001GDFL-0017": "删除",
                //  "702001GDFL-0018": "确认删除该单据吗",
                title: multiLang && multiLang.get("702001GDFL-0017"),
                content: multiLang && multiLang.get("702001GDFL-0018"),
                beSureBtnClick: this.billRemove
            }
        };
        return (
            <BillStyle {...this.props}>
                {/*"702001GDFL-0015": "自定义工单",*/}
                <BillHead
                    title={this.state.Title}
                    // multiLang && multiLang.get("702001GDFL-0015")
                    refreshButtonEvent={initData.bind(this, this.props, true)}
                >
                    <ButtonGroup
                        areaId="head"
                        buttonEvent={pageButtonClick.bind(this)}
                        modalConfig={btnModalConfig}
                    />
                    <Form
                        areaId={window.presetVar.head.head1}
                        onAfterEvent={afterEvent.bind(this)}
                    />
                </BillHead>

                <BillBody>
                    {/*"702001GDFL-0016": "自定义工单明细"*/}
                    <CardTable
                        title={multiLang && multiLang.get("702001GDFL-0016")}
                        areaId="freebill_detaill"
                        totalItemCode="amount"
                        modelSave={pageButtonClick.call(this)["pageSave"]}
                        onAfterEvent={afterEvent.bind(this)}
                    >
                        <ButtonGroup
                            areaId="body_shoulder"
                            buttonEvent={tableButtonClick.bind(this)}
                        />
                    </CardTable>
                    {this.state.showUploader && (
                        <NCUploader
                            disableButton={this.state.disabledUploadBtn}
                            // billcode={this.state.billId}
                            // pk_billtypecode={window.presetVar.billtype}
                            billId={this.state.billId}
                            onHide={() => {
                                this.setState({showUploader: false});
                                getAttachments(this.state.billId).then(lists => {
                                    this.props.button.setButtonTitle('fileManager', multiLang && multiLang.get("702001GDFL-0039") + ' ' + lists + ' ');
                                })
                            }}
                            placement={"bottom"}
                            disableModify={this.props.form.getFormItemsValue("head", 'approvestatus')&&this.props.form.getFormItemsValue("head", 'approvestatus').value!="-1"}
                            getGroupList={this.getGroupList}
                        />
                    )}
                    <ApproveDetail
                        show={this.state.showLinkApprove}
                        close={() => this.setState({showLinkApprove: false})}
                        billtype={this.state.transtype}
                        billid={this.state.billId}
                    />
                    {this.state.compositedisplay ? <ApprovalTrans
                        title={multiLang && multiLang.get("702001GDFL-0040")}
                        data={this.state.compositedata}
                        display={this.state.compositedisplay}
                        getResult={this.getAssginUser}
                        cancel={this.turnOff.bind(this)}
                    /> : ""}
                </BillBody>
            </BillStyle>
        );
    }
}

function getUrlParam(pop) {
    if (!pop) return;
    let result;
    let queryString = window.location.search || window.location.hash;
    result = getParam(pop, queryString);
    if (!result) {
        result = getParam(pop, window.parent.location.hash || window.parent.location.search);
    }
    return result;
}

function getParam(pop, queryString) {
    if (!pop) return;
    let result;
    if (queryString.includes("?")) {
        queryString = queryString.split("?")[1];
    } else {
        queryString = queryString.substring(1);
    }
    if (queryString) {
        let paramsArr = queryString.split("&");
        if (paramsArr && paramsArr instanceof Array) {
            paramsArr.forEach(item => {
                if (
                    item.indexOf("=") != -1 &&
                    item.split("=") &&
                    item.split("=") instanceof Array
                ) {
                    if (item.split("=")[0] === pop) {
                        if (item.split("=")[1]) {
                            result = decodeURIComponent(item.split("=")[1]);
                        }
                    }
                }
            });
        }
    }
    return result;
}

let isSingle = getUrlParam("isSingle");
let pagecode = getUrlParam("p");
FreeBill = createPage({
    billinfo: {
        billtype: isSingle == "true" || isSingle == "Y" ? "form" : "card",
        pagecode: pagecode,
        headcode: "head",
        bodycode: "freebill_detaill"
    },
    mutiLangCode: "7020"
})(FreeBill);

export default FreeBill;
