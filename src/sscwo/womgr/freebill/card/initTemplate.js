/**
 * 页面初始化逻辑：模板及初始化数据的配置及加载
 */
import requestApi from './requestApi';
import { getBodyDefaultValue} from './events/commonEvent';
import {getMultiLang} from 'nc-lightapp-front'
import { beforeUpdatePage, updatePage } from 'ssccommon/utils/updatePage';

const head1 = window.presetVar.head.head1;
const body1 = window.presetVar.body.body1;
const pageButton = presetVar.pageButton;
const pageCode = window.presetVar.pageCode;
const statusVar = window.presetVar.status;
const areaBtnAction = window.presetVar.areaBtnAction;
import { toast } from "nc-lightapp-front";

let initPage = function (props) {
    // 浏览态不请求initpage
    let pageStatus = props.getUrlParam("status"); // 状态：add/edit/browse
    let appcode = props.getUrlParam("appcode"); // 小应用编码
    let appid = props.getSearchParam("ar"); // 小应用ID
    // let pagecode = props.getUrlParam("pagecode"); // 模板编码
    let pagecode;
    let transtype = props.getUrlParam("transtype"); // 交易类型
    let scene = props.getUrlParam('scene');

    // 适配部分代码使用c作为参数名称
    if (appcode == null || appcode == undefined || appcode == "") {
        appcode = props.getUrlParam("c");
    }
    // 从iframe url中获取不到appcode和status，则从工作台url中获取
    if (appcode == null || appcode == undefined || appcode == "") {
        appcode = props.getSearchParam("c");
        props.setUrlParam({c:appcode});
    }

    pagecode = props.getUrlParam("p");
        
    if(scene == 'approve'){
        pagecode='702002ZDYDJ_card_sh';
    }else if(scene == 'zycl'||scene == 'zycx'){
        if(!pagecode)pagecode='702002ZDYDJ_card_sh';
    }else if(scene == 'fip'){
        pagecode = '702002ZDYDJ_card_lc';
    }else if (pagecode == null || pagecode == undefined || pagecode == "") {
            pagecode = props.getSearchParam("p");
    }


    // 设置pageStatus默认值
    if (pageStatus == null || pageStatus == undefined || pageStatus == "") {
        pageStatus = statusVar.add;
        props.setUrlParam({status:pageStatus});
    }
    //工作项管理打开工单设置页面为浏览态
    if (appcode == '702002ZDYDJSH' && appid == '1001ZG10000000000Q8L'){
        pageStatus = statusVar.browse;
        props.setUrlParam({status:pageStatus});
    }

    // 全局参数赋值
    window.presetVar.pageStatus = pageStatus;
    window.presetVar.pageCode = pagecode;
    window.presetVar.pageId = pagecode;
    window.presetVar.appcode = appcode;
    window.presetVar.appid = appid;
    
    // 根据小应用编码或交易类型获取后台获取参数，交易类型为空则根据小应用编码获取参数
    requestApi.pageInit({
        data: {
            tradetype:transtype,
            appcode:appcode,
            pagecode:pagecode
        },
        success: (data) => {
            window.presetVar.isSingle = data.data.isSingle; // 是否单表
            window.presetVar.transtype = data.data.transtype;
            window.presetVar.billtype = data.data.billtype; // 单据类型
            window.presetVar.pageId = data.data.templetCode;
            window.presetVar.billNoIsEditable = data.data.billNoIsEditable; //单据编码是否可编辑（编码规则配置参数）
            window.presetVar.pk_doc = data.data.pk_doc; 
        }
    });
}

let initData = function (props, isRefresh = false) {
    let pageStatus = window.presetVar.pageStatus||props.getUrlParam("status");
    let appcode = props.getUrlParam("c");
    if (pageStatus == statusVar.browse) {

        setTimeout(function(){
            props.form.setFormStatus(head1, statusVar.browse);
            props.cardTable.setStatus(body1, statusVar.browse);
        }, );
        props.button.setButtonVisible(
            [
                pageButton.pageAdd,
                pageButton.pageCopy,
                pageButton.pageDel,
                pageButton.billSubmit,
                pageButton.linkVoucher,
                pageButton.pagePrint,
                pageButton.pageSave,
                pageButton.fileManager,
                pageButton.imageShow,
                pageButton.imageUpload,
                pageButton.pageClose,
                'LinkApprove'
            ],
            true);
        props.button.setButtonVisible(
            [
                body1 + areaBtnAction.add,
                pageButton.pageSave,
                pageButton.pageClose,
                pageButton.billRecall,
                pageButton.pageClose,
                pageButton.billSaveSubmit,
                pageButton.pageEdit,
            ],
            false);

        let that = this;
        let reqdata = {
            billid: props.getUrlParam("billId")||props.getUrlParam("id")
        }
        requestApi.query({
            data: reqdata,
            success: (data) => {
                props.form.setAllFormValue({[head1]: data.data.head[head1]});
                //设置表体数据
                data.data.body && props.cardTable.setTableData(body1, data.data.body[body1]);
                window.presetVar.billtype= data.data.head.head.rows[0].values.billtype.value;
                dataHandle.bind(this)(data.data.head[head1], props);
                // if(!window.presetVar.isSingle){
                    if (isRefresh)
                        getMultiLang({moduleId: 7020, domainName: 'sscwo',currentLocale: 'simpchn',callback: (json) => {
                            toast({title:  json['702001GDFL-0041']}) }});
                   
                // }
                 
            }
        });

    } else if (pageStatus == statusVar.add) {
        // setTimeout(function(){

        props.form.setFormStatus(head1, statusVar.add);
        props.cardTable.setStatus(body1, statusVar.edit);
        // }, );
        props.button.setButtonVisible(
            [
                pageButton.pageAdd,
                pageButton.pageCopy,
                pageButton.pageEdit,
                pageButton.pageDel,
                pageButton.billRecall,
                pageButton.billSubmit,
                pageButton.pagePrint,
                pageButton.imageShow,
                pageButton.imageUpload,
                pageButton.linkVoucher,
                'LinkApprove'
            ],
            false);

        props.button.setButtonVisible(
            [

                pageButton.pageSave,
                pageButton.pageClose,
                pageButton.fileManager,
                body1 + areaBtnAction.add,
            ],
            true);


        let data = props.createMasterChildData(props.meta.getMeta().code, 'head', 'freebill_detaill');


        let reqData = {
            uistate: 'add',
            tradetype: window.presetVar.transtype,
            // nodecode: 'dbl_zdydj',
            userjson: "{'pagecode': '" + window.presetVar.pageCode + "'}",
            appcode: appcode,
        };
        reqData = {
            ...reqData,
            ...data,
            pk_doc:window.presetVar.pk_doc,
        }
        if (window.presetVar.isSingle){
            reqData.body = null;
        }
        let copybiillid = props.getUrlParam("copyFromBillId");
        if (copybiillid) {
            let that = this;
            let allData = props.createMasterChildData(window.presetVar.pageId, head1, body1);
            allData.billid = copybiillid;
            requestApi.loadData({
                props: props,
                meta: props.meta.getMeta(),
                data: reqData,
                success: (data) => {
                    requestApi.loadDataHandle(data.data, props);
                }
            },(
                requestApi.copy({
                    data: allData,
                    success: (data) => {
                        props.form.setAllFormValue({[head1]: data.data.head[head1]});
                        //设置表体数据
                        data.data.body && props.cardTable.setTableData(body1, data.data.body[body1]);
                    }
                })
            ));
        } else {
            let starttime = new Date().getTime();
            console.log("startsetdata"+Date());
            requestApi.loadData({
                props: props,
                meta: props.meta.getMeta(),
                data: reqData,
                success: (data) => {
                    // 模板处理
                    console.log(new Date().getTime()-starttime+"endajax");
                    beforeUpdatePage(props);
                    props.form.setAllFormValue({[head1]: data.data.head[head1]}); // 设置表头数据
                    requestApi.loadDataHandle(data.data, props);
                    if(!window.presetVar.isSingle){
                        props.cardTable.addRow(body1, undefined, getBodyDefaultValue(props),false); 
                    }
                    updatePage(props, head1, body1);
                    console.log(new Date().getTime()-starttime+"endsetdata");
                }
            });
        }
    } else if (pageStatus == statusVar.edit) {
        props.form.setFormStatus(head1, statusVar.edit);
        props.cardTable.setStatus(body1, statusVar.edit);
        props.button.setButtonVisible(
            [
                pageButton.pageSave,
                pageButton.pageClose,
                pageButton.fileManager,
                body1 + areaBtnAction.add,
            ],
            true);
        props.button.setButtonVisible(
            [
                pageButton.pageAdd,
                pageButton.pageCopy,
                pageButton.pageEdit,
                pageButton.pageDel,
                pageButton.billSubmit,
                pageButton.billRecall,
                pageButton.pagePrint,
                pageButton.imageShow,
                pageButton.imageUpload,
                pageButton.linkVoucher,
                'LinkApprove'
            ],
            false);

        let that = this;
        let reqdata = {
            billid: props.getUrlParam("billId")||props.getUrlParam("id")
        }
        
        let data = props.createMasterChildData(props.meta.getMeta().code, 'head', 'freebill_detaill');
        let reqData = {
            uistate: 'add',
            tradetype: window.presetVar.transtype,
            // nodecode: 'dbl_zdydj',
            userjson: "{'pagecode': '" + window.presetVar.pageCode + "'}",
            appcode: appcode,
        };
        reqData = {
            ...reqData,
            ...data,
            pk_doc:window.presetVar.pk_doc,
        }
        if (!window.presetVar.isSingle) {
          //  props.cardTable.delRowsByIndex('freebill_detaill', 0);
        } else {
            reqData.body = null;
        }
        requestApi.loadData({
            props: props,
            meta: props.meta.getMeta(),
            data: reqData,
            success: (data) => {
                // 模板处理
                // pubUtils.setDefaultValue.call(this, data, 'head');
                requestApi.loadDataHandle(data.data, props);
            }
        },(
            requestApi.query({
                data: reqdata,
                success: (data) => {
                    props.form.setAllFormValue({[head1]: data.data.head[head1]}); // 设置表头数据
                    data.data.body && props.cardTable.setTableData(body1, data.data.body[body1]); // 设置表体数据
                }
            })
        ));

        
    }
    if (!window.presetVar.isSingle) {
        // setTimeout(() => {
            requestApi.tableExtendColHandle.call(this, props);
        // },);
    }
}

function dataHandle(head, props) {
    //处理数据和按钮状态
    if (head == null || head.rows[0] == null || head.rows[0].values == null)
        return;
    if(window.presetVar.pageCode=='702002ZDYDJ_card_lc'){
        props.button.setButtonVisible(
            [
                pageButton.pageAdd,
                pageButton.pageCopy,
                pageButton.pageEdit,
                pageButton.pageDel,
                pageButton.billSubmit,
                pageButton.billRecall,
                pageButton.pageSave,
                pageButton.billSaveSubmit,
                pageButton.pageClose,
                pageButton.imageUpload
            ],
            false);
        return;
    }
    let isBrowseStatus = statusVar.browse;
    let scene = props.getUrlParam('scene');//判断来源节点
    if (isBrowseStatus) {

        let spzt = head.rows[0].values.approvestatus.value;//审批状态
        let btnsVisibleObj = {};
        // 审批状态:0=审批未通过，1=审批通过，2=审批进行中，3=提交，-1=自由
        //生效状态： 0=未生效，1=已生效，
        let sxzt = head.rows[0].values.sxzt.value;
        if(sxzt=='0'){
           // 生效状态： 0=未生效

            props.button.setButtonVisible([pageButton.Edit], true);
            if (spzt == '-1') {
                props.button.setButtonVisible([pageButton.billSubmit,pageButton.pageEdit], true);

            } else {
                if (spzt == '3') {
                    props.button.setButtonVisible([pageButton.billRecall], true);
                    props.button.setButtonVisible([pageButton.billSubmit,pageButton.pageDel,pageButton.pageEdit], false);

                    // TOP NCCLOUD-87850 MOD
                    // if(scene == 'approve'||scene == 'zycx'){
                    //     props.button.setButtonVisible([pageButton.pageEdit], true);
                    // }
                    // if (scene == 'zycl') {
                    //     props.button.setButtonVisible([pageButton.pageEdit], false);
                    // }
                    if (scene == 'approve' || scene == 'zycx' || scene == 'zycl') {
                        props.button.setButtonVisible([pageButton.pageEdit], true);
                    }
                    // BTM NCCLOUD-87850
                }
                if (spzt == '2') {
                    props.button.setButtonVisible([pageButton.pageEdit], false);
                    props.button.setButtonVisible([pageButton.billSubmit,pageButton.pageDel], false);

                    if(scene == 'approve'||scene == 'zycl'||scene == 'zycx'){

                        props.button.setButtonVisible([pageButton.pageEdit], true);
                      

                    }
                }
                // 审批通过
                if (spzt == '1') {

                    props.button.setButtonVisible([pageButton.pageDel,pageButton.billSubmit,pageButton.billRecall,pageButton.pageEdit], false);
                    // props.button.setButtonVisible([pageButton.pageCopy], true);

                    if(scene == 'approve'||scene == 'zycl'||scene == 'zycx'){

                        props.button.setButtonVisible([pageButton.pageEdit], true);
                    }
                }
                if (spzt == '0') {
                    props.button.setButtonVisible([pageButton.billSubmit], false);
                    props.button.setButtonVisible([pageButton.pageDel], false);
                }


            }

            //未生效状态上传控件操作按钮启用
            this.setState({
                disabledUploadBtn: false
            })
        }else{
            // 生效状态： 1已生效
            props.button.setButtonVisible([pageButton.pageEdit, pageButton.pageDel, pageButton.billSubmit], false);

            if(spzt == '1'){

                props.button.setButtonVisible([pageButton.billRecall,pageButton.imageUpload], false);
               
            }
            //生效状态上传控件操作按钮禁用
            this.setState({
                disabledUploadBtn: true
            })
        }

    }

    if (scene == 'zycx' || scene == 'bzcx' || scene == 'fycx') {
        props.button.setButtonVisible([pageButton.pageCopy], false)
    }

}

export default function (props, reloadFlag) {

    let that = this;
    let filenum=0;
    filenum=this.state.groupLists;

    // 初始化页面参数
    initPage(props);

    // 初始化模板及数据
    if (reloadFlag == true) {
        let starttime = new Date().getTime();
        console.log("startuidom"+Date());
        let createUIDomPromise = new Promise((resolve, reject) => {
            props.createUIDom({
                appcode: window.presetVar.appcode,
                pagecode: window.presetVar.pageCode,//页面id
                appid: ''//注册按钮的id
            }, (data) => {
                resolve(data);
            })
        });
    
        let getMultiLangPromise = new Promise((resolve, reject) => {
            getMultiLang({
                moduleId: 7020, domainName: 'sscwo',currentLocale: 'simpchn',
                callback: (json) => {
                    resolve(json);
                }
            })
        });
        Promise.all([createUIDomPromise, getMultiLangPromise]).then((resultList) => {
                let data = resultList[0];
                let json = resultList[1];
                console.log(new Date().getTime()-starttime+"endlang");
                let button = data.button;
                beforeUpdatePage(props);
                // 找到更多下的附件按钮，增加附件数。
                if (button != null && button != undefined) {
                    props.button.setButtonTitle('fileManager',json['702001GDFL-0039'] + ' ' +filenum);
                }
                
                tplHandle.call(this, meta, button);

                let meta = data.template;

                requestApi.metaDataHandle(meta,props);

                // 设置按钮模板
                initData.call(that,props);

                that.setState({Title: that.props.getSearchParam('n')});
                updatePage(props, head1, body1);
        })
    

        function tplHandle(meta,button) {

            //按钮存入组件
            props.button.setButtons(button);
        }

        //hashchange 回调
        props.setHashChangeCallback(() => {
            requestApi.tableExtendColHandle.call(this, props);
        })

    } else {
        initData.call(that,props);
    }
}

export {
    initData
}
