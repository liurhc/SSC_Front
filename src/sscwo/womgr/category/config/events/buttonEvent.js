import {setTableExtendCol} from 'ssccommon/components/profile'
import currentVar from '../presetVar'

import {base} from 'nc-lightapp-front';

function buttonEvent() {
    let multiLang = this.props.MutiInit.getIntl(7020);
    return {


        [currentVar.pageButtons.Save]: {

            url: '/nccloud/sscwo/cpdoctype/save.do',
            beforeClick: () => {
                this.props.editTable.filterEmptyRows(currentVar.listArea, ['typecode', 'typename'], 'include');
                return true;
            },
            afterClick: () => {
                this.props.button.setButtonVisible([currentVar.pageButtons.Edit], true);
                this.props.button.setMainButton([currentVar.pageButtons.Add], true)
                this.props.button.setPopContent('Delete', multiLang && multiLang.get('702001GDFL-0032'))
            }

        },
        [currentVar.listButtons.Delete]: {

            url: '/nccloud/sscwo/cpdoctype/save.do',


        },
        [currentVar.pageButtons.Add]: {
            afterClick: () => {
                this.props.button.setButtonVisible([currentVar.pageButtons.Edit], false);
                this.props.button.setMainButton([currentVar.pageButtons.Add], false)
                this.props.button.setPopContent('Delete')
                /* 设置操作列上删除按钮的弹窗提示 */
            }
        },
        [currentVar.pageButtons.Cancel]: {
            afterClick: () => {
                this.props.button.setButtonVisible([currentVar.pageButtons.Edit], true);
                this.props.button.setMainButton([currentVar.pageButtons.Add], true)
                this.props.button.setPopContent('Delete', multiLang && multiLang.get('702001GDFL-0032'))
            }
        },
        [currentVar.pageButtons.Edit]: {
            afterClick: () => {
                this.props.button.setButtonVisible([currentVar.pageButtons.Edit], false);
                this.props.button.setMainButton([currentVar.pageButtons.Add], false)
                this.props.button.setPopContent('Delete')
                /* 设置操作列上删除按钮的弹窗提示 */
            }
        },

    }
}

export default buttonEvent