import currentVar from '../presetVar';
import requestApi from '../requestApi';
let queryData = function(){
    requestApi.query({
        data: {pk_doctype: this.queryKey.category},
        success: (data) => {
            if(data == null){
                this.props.editTable.setTableData(currentVar.listArea, { rows: [] });
            }else{
                this.props.editTable.setTableData(currentVar.listArea, data[currentVar.listArea]);
            }
        }
    })
}
//onCategoryRefChange工单分类没有用到该方法。
let onCategoryRefChange = function(e){
    this.setState({category: e});

    if((e || {}).refpk != null){
        this.queryKey.category=e.refpk;
    }else{
        this.queryKey.category=undefined;
    }
    queryData.call(this);
}
export default {queryData, onCategoryRefChange}