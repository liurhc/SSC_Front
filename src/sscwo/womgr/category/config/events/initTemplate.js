import {setTableExtendCol} from 'ssccommon/components/profile';
import fixedTemplet from './fixedTemplet';
import currentVar from '../presetVar';
import {query, buttonEvent} from './index';
import {getMultiLang} from 'nc-lightapp-front'

export default function (props) {
    let that = this;
    props.createUIDom(
        {
        pagecode: currentVar.pageId,//页面id
        appid: ' '//注册按钮的id
        },
        function (data) {
            //设置区域模板createButtonApp
            getMultiLang({moduleId: 7020, domainName: 'sscwo',currentLocale: 'simpchn',
                callback: (json) => {
                    props.meta.setMeta(data.template);
                    //设置按钮模板
                    // "702001GDFL-0032": "确认删除该信息吗"
                    let fixedTempletValue = fixedTemplet(props, json);
                    props.button.setButtons(fixedTempletValue.button);
                    props.button.setPopContent(currentVar.listButtons.Delete,json['702001GDFL-0032']);
                    // props.button.setButtonsVisible([currentVar.pageButtons.Save,currentVar.pageButtons.Cancel], false);
                    props.button.setButtonsVisible({'Save': false, 'Cancel': false, 'Add': true});
                    //设置表格的扩展按钮列
                    setTableExtendCol.call(that, props, data.template, [{
                        areaId: currentVar.listArea,
                        btnAreaId: currentVar.listButtonArea,
                        btnKeys: [currentVar.listButtons.Delete],
                        onButtonClick: buttonEvent.bind(that)
                    }]);
                    //获取表格的数据
                    query.queryData.call(that);
                }})

        }
    )
}
