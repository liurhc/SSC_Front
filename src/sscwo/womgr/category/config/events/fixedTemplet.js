import currentVar from '../presetVar'
export default function (props,json) {
    // "702001GDFL-0004": "新增",
    // "7020-0005": "保存",
    // "7020-0002": "取消",
    // "702001GDFL-0017": "删除",
    // "7020-0004": "修改",
    return ({button:[

            {
                "isenable": true,
                "iskeyfunc": false,
                "order": "0",

                "type": "buttongroup",
                "key":"buttongroup2",
                "title": 'buttongroup',
                "area": currentVar.pageButtonArea,
                "visible" : true,
                "children": [
                    {
                        btncolor: "button_main",
                        isenable: true,
                        order: "1",
                        "type": "general_btn",
                        "key": currentVar.pageButtons.Add,
                        "title": json['702001GDFL-0004'],
                        "area": currentVar.pageButtonArea,
                        "parentCode": "buttongroup2"
                    },
                    {
                        btncolor: "button_secondary",
                        isenable: true,
                        order: "2",

                        "type": "general_btn",
                        "key": currentVar.pageButtons.Edit,
                        "title": json['7020-0004'],
                        "area": currentVar.pageButtonArea,
                        "parentCode": "buttongroup2"
                    }
                ]

            },

            {
                btncolor: "button_main",
                "type": "general_btn",
                "key": currentVar.pageButtons.Save,
                "title": json['7020-0005'],
                "area": currentVar.pageButtonArea,
            },
            {
                btncolor: "button_secondary",
                "type": "general_btn",
                "key": currentVar.pageButtons.Cancel,
                "title": json['7020-0002'],
                "area": currentVar.pageButtonArea,
            },
            {
                "type": "button_main",
                "key": currentVar.listButtons.Delete,
                "title": json['702001GDFL-0017'],
                "area": currentVar.listButtonArea,
           
            }
            

        ]})
}