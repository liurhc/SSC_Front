import requestApi from "ssccommon/components/requestApi";

import {ajax } from 'nc-lightapp-front';

let requestDomain =  '';

let requestApiOverwrite = {
    ...requestApi,
    query:(opt) => {
        ajax({
            url: `${requestDomain}/nccloud/sscwo/cpdoctype/query.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },

    save: (opt) => {

        opt.data = {
            head: opt.data,
        }
        ajax({
            url: `${requestDomain}/nccloud/sscwo/cpdoctype/save.do`,
            data: opt.data,
            success: (data) => {
                if (data.success) {
                    data = data.data;
                    opt.success(data);
                }
            }
        })
    },

    del: (opt) => {
        ajax({
            url: `${requestDomain}/nccloud/sscwo/cpdoctype/del.do`,
            data: opt.data,
            success: (data) => {
                if (data.success) {
                    data = data.data;
                    opt.success(data);
                }
            }
        })
    }
}

export default  requestApiOverwrite;
