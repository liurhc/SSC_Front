import React, {Component} from 'react';
import {createPage} from 'nc-lightapp-front';
import {
    ProfileStyle,
    ProfileHead,
    ProfileBody,
    ButtonGroup,
    HeadCenterCustom,
    EmptyArea
} from 'ssccommon/components/profile';
import {EditTable} from 'ssccommon/components/table';
import currentVar from './presetVar'
import {initTemplate, listButtonClick, buttonEvent} from './events/index';


import './index.less';

class SettingwoList extends Component {

    constructor(props) {
        super();
        this.state = {
            category: {},
        }
        this.queryKey = {
            category: undefined
        }
        initTemplate.call(this, props);
    }

    componentDidMount() {
        window.onbeforeunload = () => {
            let statu = 'browse', btnsData = this.props.button.getButtons()
            for (let item of btnsData) {
                if (item.key === 'Save') {
                    statu = item.visible ? 'edit' : 'browse'
                    break
                }
            }
            if (statu === 'edit') return ''
        }
    }

    render() {
        const {table} = this.props;
        const {createSimpleTable} = table;
        let multiLang = this.props.MutiInit.getIntl(7020); //this.moduleId
        return (
            <div id="settingwoList">
                {/* 档案风格布局 */}
                <ProfileStyle
                    layout="singleTable"
                    {...this.props}
                >
                    {/*页面头*/}
                    {/*"702001GDFL-0001": "工单单据分类",*/}
                    <ProfileHead
                        title={multiLang && multiLang.get('702001GDFL-0001')}
                    >

                        <ButtonGroup
                            area={currentVar.pageButtonArea}
                            ctrlEditTableArea={[window.presetVar.listArea]}
                            buttonEvent={buttonEvent.bind(this)}
                        />
                    </ProfileHead>
                    {/*页面体*/}
                    <ProfileBody>
                        <EditTable
                            showCheck={false}
                            areaId={window.presetVar.listArea}
                        />
                    </ProfileBody>
                </ProfileStyle>
            </div>
        )
    }
}

SettingwoList = createPage({
    mutiLangCode: '7020'
})(SettingwoList);
export default SettingwoList;