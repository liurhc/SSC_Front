import 'ssccommon/components/globalPresetVar';

/**页面全局变量 */
let currentVar = {
    /**步骤调 */
    pagestep:'pagestep',
    pagesteparea:'pagesteparea',
    /**页面ID */
    pageId: '702001GDLX_card',
    listPageId: '702001GDLX_list',
    /**表头区域ID */
    headArea: 'cpdoc',
    body1Area: 'cpdocmaster',
    body2Area: 'cpdocchild',
    pageHeadButtonArea: 'pageHeadButtonArea',
    pageBody1ButtonArea: 'pageBody1ButtonArea',
    pageBody2ButtonArea: 'pageBody2ButtonArea',
    pageHeadButtons:{
        Edit: 'HeadEdit',
        Save: 'HeadSave',
        Cancel: 'HeadCancel',
        Send: 'HeadSend',
        Next: 'HeadNext'

    },
    pageBody1Buttons:{  
        Add: 'Body1Add',
        Edit: 'Body1Edit',
        Save: 'Body1Save',
        Cancel: 'Body1Cancel'
    },
    pageBody2Buttons:{  
        Add: 'Body2Add',
        Edit: 'Body2Edit',
        Save: 'Body2Save',
        Cancel: 'Body2Cancel'
    },
    listBody1ButtonArea: 'listBody1ButtonArea',
    listBody2ButtonArea: 'listBody2ButtonArea',
    listBody1Buttons: {
        Delete: 'Body1Delete'
    },
    listBody2Buttons: {
        Delete: 'Body2Delete'
    },
    page2Id: 'cpdoc',
    page2Area: 'cpdoc',
    page2ButtonArea: 'page2ButtonArea',
    page2Buttons:{
        Edit: 'Page2Edit',
        Send: 'Page2Send',

    },
    page3Id: 'cpdoc',
    page3Area: 'appletForm',
    page3bottomArea: 'cpdoc',
    page3ButtonArea: 'page3ButtonArea',
    page3Buttons:{
        Edit: 'Page3Edit',
        Send: 'Page3Send',
        Back:'Page3Back'
    },
    confirms:{
        page1SendConfirm: 'page1SendConfirm',
        page2SendConfirm: 'page2SendConfirm',
        page3SendConfirm: 'page3SendConfirm'
    }
}
window.presetVar = {
    ...window.presetVar,
    ...currentVar
};
export default currentVar
