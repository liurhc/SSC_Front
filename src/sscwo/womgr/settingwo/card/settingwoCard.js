import React, { Component } from "react";
import { createPage, base,promptBox } from "nc-lightapp-front";
const { NCStep, NCBackBtn } = base;
import currentVar from "./presetVar";
import { initTemplate, afterEvent, buttonEvent } from "./events/index";

import {
    ProfileStyle,
    ProfileHead,
    ProfileBody,
    BodyRight,
    ButtonGroup,
    EmptyArea
} from "ssccommon/components/profile";
import { EditTable } from "ssccommon/components/table";

import "./index.less";

class SettingwoCard extends Component {
    constructor(props) {
        super();

        this.state = {
            close: "show", // 没用的state
            pagestepmsg: "", //弹框内容，可以是字符串或dom
            body1Show: "hide", // 是否显示body1Area
            body2Show: "hide", // 是否显示body2Area
            page1Show: // 是否显示page1
                !props.getUrlParam("page") || props.getUrlParam("page") == "1"
                    ? "show"
                    : "hide",
            page2Show: props.getUrlParam("page") == "2" ? "show" : "hide",  // 是否显示page2
            page3Show: props.getUrlParam("page") == "3" ? "show" : "hide", // 是否显示page3
            page1StepDescription: "", // 步骤一描述
            page2StepDescription: "", // 步骤二描述
            page3StepDescription: "", // 步骤三描述
            page1StepStatus: "", // 步骤一状态
            page2StepStatus: "", // 步骤二状态
            page3StepStatus: "", // 步骤三状态
            //page1ConfirmMsg: '确定退出新增工单',
            page2ConfirmMsg: "", //弹框内容，可以是字符串或dom
            //page3ConfirmMsg: '小应用发布成功后，将不可修改',
            page3FormShow: "show", // 显示page3表单
            page3MsgShow: "hide" // 没用的state
        };

        initTemplate.call(this, props);
        
    }
    
    componentDidMount() {
        window.onbeforeunload = () => {
            let status = this.props.form.getFormStatus('cpdoc');  
            if(!status){
                status = this.props.form.getFormStatus('appletForm');
            }
            let status1 = this.props.editTable.getStatus(currentVar.body1Area);
            let status2 = this.props.editTable.getStatus(currentVar.body2Area);
            if (status == 'edit'||status1 == 'edit'||status2 == 'edit') {
                return ''; 
            }
        }
    }
   
    // 返回按钮点击
    backFun = () => {
        let multiLang = this.props.MutiInit.getIntl(7020);
       let ispubappletvalue=this.props.form.getAllFormValue('cpdoc').rows[0].values.ispubapplet.display;
       //如果已经发布完小应用了，点退出按钮无需提示。
        if(ispubappletvalue==(multiLang && multiLang.get("702001GDFL-0013"))){
            this.props.linkTo('/sscwo/womgr/settingwo/list/index.html',{
                pagecode: currentVar.listPageId
            })

        }else{
            promptBox({
                color: 'warning',    
                title: multiLang && multiLang.get('702001GDFL-0012'), // 弹框表头信息
                content: multiLang && multiLang.get('702001GDFL-0042'), //弹框内容，可以是字符串或dom
                beSureBtnClick: () => {
                    this.props.linkTo('/sscwo/womgr/settingwo/list/index.html',{
                        pagecode: currentVar.listPageId
                    })
                },
                    
            })

        }

        
    };
    render() {
        const {  form, modal } = this.props;
        // let { createModal } = modal;
        let { createForm } = form;
        let multiLang = this.props.MutiInit.getIntl("7020"); //this.moduleId
        let getForm = () => {
            return createForm(currentVar.headArea, {});
        };
        return (
            <div id="settingwoCard" className='nc-bill-card'>
                <div className="step-area nc-theme-area-bgc">
                    <div className="back-btn-wrap">
                        <NCBackBtn
                            onClick={() => {
                                this.backFun();
                            }}
                        />
                    </div>
                    {/*"702001GDFL-0004": "新增工单",*/}
                    {/*"702001GDFL-0005": "发布元数据",*/}
                    {/*"702001GDFL-0006": "发布小应用",*/}
                    <div className="step">
                        <NCStep.NCSteps current={1}>
                            <NCStep
                                title={multiLang && multiLang.get("702001GDFL-0004")}
                                description={this.state.page1StepDescription}
                                status={this.state.page1StepStatus}
                            />
                            <NCStep
                                title={multiLang && multiLang.get("702001GDFL-0005")}
                                description={this.state.page2StepDescription}
                                status={this.state.page2StepStatus}
                            />
                            <NCStep
                                title={multiLang && multiLang.get("702001GDFL-0006")}
                                description={this.state.page3StepDescription}
                                status={this.state.page3StepStatus}
                            />
                        </NCStep.NCSteps>
                    </div>
                </div>
                
                {this.state.page1Show == "show" ? (
                    <div className='nc-bill-bottom-area'>
                        <ProfileStyle layout="singleTable" {...this.props}>
                            {/*"702001GDFL-0004": "新增工单",*/}
                            <ProfileHead
                                title={multiLang && multiLang.get("702001GDFL-0004")}
                            >
                                <ButtonGroup
                                    area={currentVar.pageHeadButtonArea}
                                    ctrlFormArea={[currentVar.headArea]}
                                    buttonEvent={buttonEvent.bind(this)}
                                />
                            </ProfileHead>
                            <ProfileBody>
                                <BodyRight>
                                    <EmptyArea className="nc-theme-gray-area-bgc">{getForm()}</EmptyArea>
                                </BodyRight>
                            </ProfileBody>
                        </ProfileStyle>
                    </div>
                ) : null}
                {this.state.page2Show == "show" ? (
                    <div className='nc-bill-bottom-area'>
                        <ProfileStyle layout="singleTable" {...this.props}>
                            {/*"702001GDFL-0005": "发布元数据",*/}
                            <ProfileHead
                                title={multiLang && multiLang.get("702001GDFL-0005")}
                            >
                                <ButtonGroup
                                    area={currentVar.pageHeadButtonArea}
                                    ctrlFormArea={[currentVar.headArea]}
                                    buttonEvent={buttonEvent.bind(this)}
                                />
                            </ProfileHead>
                            <ProfileBody>
                                <BodyRight>
                                    <EmptyArea className="nc-theme-gray-area-bgc">{getForm()}</EmptyArea>

                                    <EditTable
                                        className={this.state.body1Show}
                                        areaId={currentVar.body1Area}
                                        // "702001GDFL-0008": "主表字段",
                                        title={multiLang && multiLang.get("702001GDFL-0008")}
                                        showCheck={false}
                                        adaptionHeight={false}
                                    >
                                        <ButtonGroup
                                            area={currentVar.pageBody1ButtonArea}
                                            ctrlEditTableArea={[currentVar.body1Area]}
                                            buttonEvent={buttonEvent.bind(this)}
                                        />
                                    </EditTable>
                                    <EditTable
                                        className={this.state.body2Show}
                                        areaId={currentVar.body2Area}
                                        // "702001GDFL-0009": "子表字段"
                                        title={multiLang && multiLang.get("702001GDFL-0009")}
                                        showCheck={false}
                                        onAfterEvent={afterEvent.bind(this)}
                                        adaptionHeight={false}
                                    >
                                        <ButtonGroup
                                            area={currentVar.pageBody2ButtonArea}
                                            ctrlEditTableArea={[currentVar.body2Area]}
                                            buttonEvent={buttonEvent.bind(this)}
                                        />
                                    </EditTable>
                                </BodyRight>
                            </ProfileBody>
                        </ProfileStyle>
                        <div>
                            {/*createModal(currentVar.confirms.page2SendConfirm, {
                                // "7020-0001": "确定",
                                // "702001GDFL-0013": "发布成功",
                                title: multiLang && multiLang.get("7020-0001"), // 弹框表头信息
                                content: this.state.page2ConfirmMsg, //弹框内容，可以是字符串或dom
                                beSureBtnClick: () => {
                                    requestApi.send({
                                        data: {
                                            pk_doc: this.props.getUrlParam("pk_doc"),
                                            re_publish: "true"
                                        },
                                        success: data => {
                                            NCMessage.create({
                                                content: multiLang && multiLang.get("702001GDFL-0013"),
                                                color: "success",
                                                position: "bottomRight"
                                            });
                                            this.props.linkTo(location.pathname, {
                                                page: "3",
                                                status: window.presetVar.status.edit,
                                                pk_doc: this.props.getUrlParam("pk_doc")
                                            });
                                            // pubUtils.setUrlParam('page',3);
                                            // pubUtils.setUrlParam('status', window.presetVar.status.edit);
                                            setTimeout(() => {
                                                initTemplate.call(this, this.props, true);
                                            });
                                        }
                                    });
                                }, //点击确定按钮事件
                                // "7020-0001": "确定",
                                // "7020-0003": "关闭",
                                userControl: false, // 点确定按钮后，是否自动关闭弹出框.true:手动关。false:自动关
                                rightBtnName: multiLang && multiLang.get("7020-0003"), //左侧按钮名称,默认关闭
                                leftBtnName: multiLang && multiLang.get("7020-0001") //右侧按钮名称， 默认确认
                            })*/}
                        </div>
                    </div>
                ) : null}
                {this.state.page3Show == "show" ? (
                    <div className='nc-bill-bottom-area'>
                        <ProfileStyle layout="singleTable" {...this.props}>
                            {/*"702001GDFL-0006": "发布小应用",*/}
                            <ProfileHead
                                title={multiLang && multiLang.get("702001GDFL-0006")}
                            >
                                <ButtonGroup
                                    area={currentVar.page3ButtonArea}
                                    buttonEvent={buttonEvent.bind(this)}
                                />
                            </ProfileHead>
                            <ProfileBody>
                                <BodyRight>
                                    <EmptyArea className="nc-theme-gray-area-bgc">
                                        <div className={this.state.page3FormShow}>
                                            {createForm(currentVar.page3Area, {})}
                                        </div>
                                        {/*"702001GDFL-0011": "工单信息",*/}
                                        <div className={`${this.state.page3FormShow} nc-theme-common-header-bgc nc-theme-title-font-c`} id="woinfor">
                                            {multiLang && multiLang.get("702001GDFL-0011")}
                                        </div>
                                        <div className={this.state.page3FormShow}>{getForm()}</div>
                                    </EmptyArea>
                                </BodyRight>
                            </ProfileBody>
                        </ProfileStyle>

                        <div>
                            {/*"702001GDFL-0012": "提示",*/}
                            {/* {createModal(currentVar.confirms.page3SendConfirm, {
                        title: multiLang && multiLang.get('702001GDFL-0012'),// 弹框表头信息
                        content: multiLang && multiLang.get("702001GDFL-0003"),//'小应用发布成功后，将不可修改', //弹框内容，可以是字符串或dom
                        beSureBtnClick: ()=>{
                            let data = ((this.props.form.getAllFormValue(currentVar.page3Area) || {}).rows[0] || {}).values || {};
                            requestApi.sendApp({
                                data:{
                                    pk_doc: this.props.getUrlParam("pk_doc"),
                                    app_name: data.app_name.value,
                                    menuItemCode: data.menuItemCode.value,
                                    menuItemName: data.menuItemName.value
                                },
                                // "702001GDFL-0013": "发布成功",
                                success:(data)=>{
                                    NCMessage.create({content: multiLang && multiLang.get('702001GDFL-0013'), color: 'success', position: 'bottomRight'});
                                    this.props.form.setFormStatus(currentVar.page3Area, window.presetVar.status.browse);
                                    this.props.button.setButtonVisible([currentVar.page3Buttons.Send], false);
                                }
                            })
                        }, //点击确定按钮事件
                        // "7020-0002": "取消",
                        // "702001GDFL-0006": "发布小应用",
                        userControl: false,  // 点确定按钮后，是否自动关闭弹出框.true:手动关。false:自动关
                        rightBtnName : multiLang && multiLang.get('7020-0002'), //左侧按钮名称,默认关闭
                        leftBtnName : multiLang && multiLang.get('702001GDFL-0006'), //右侧按钮名称， 默认确认
                    })} */}
                        </div>
                    </div>
                ) : null}
            </div>
        );
    }
}
SettingwoCard = createPage({
    mutiLangCode: "7020"
})(SettingwoCard);
export default SettingwoCard;
