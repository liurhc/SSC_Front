import requestApi from "ssccommon/components/requestApi";
import {ajax } from 'nc-lightapp-front';
let requestDomain =  '';

let requestApiOverwrite = {
    ...requestApi,
    //单据模板接口
    tpl: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscwo/cpdoc/gettemplate.do`,
            data: opt.data,
            success: opt.success
        });
    },
    loaddata:(opt) => {

        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/expenseaccount/ExpenseaccountDefDataAction.do?tradetype=2641&uistatus=add`,
            data: opt.data,
            success: (data) => {
                // data = data.data;
                // let newData = {}
                // newData.head = data.data.head.head;
                // newData.body = data.data.bodys[0].body;

                data.data.head['head'].rows.forEach((row) => {
                    for (let attrCode in row.values) {
                        let initialvalue = {
                            value : row.values[attrCode].value,
                            display: row.values[attrCode].display
                        };
                        opt.meta['head'].items.forEach((item) => {
                            if (item.attrcode == attrCode) {
                                item.initialvalue = initialvalue;
                            }
                        });
                    }
                });
                for(let body in data.data.bodys){
                    if(data.data.bodys[body] && data.data.bodys[body].rows){
                        data.data.bodys[body].rows.forEach((row) => {
                            for (let attrCode in row.values){
                                let initialvalue = {
                                    value: row.values[attrCode].value,
                                    display: row.values[attrCode].display
                                };
                                for (let table in opt.meta.gridrelation) {
                                    opt.meta[table].items.forEach((item) => {
                                        if (item.attrcode == attrCode){
                                            item.initialvalue = initialvalue;
                                        }
                                    });
                                }
                            }
                        });
                    }

                }
                opt.success(null);
            }
        })
    },
    save:(opt) => {

        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscwo/cpdoc/save.do`,
            data: opt.data,
            success: (data) => {

                data = data.data;
                opt.success(data);
            }
        })
    },

    save_cpdocmaster:(opt) => {
      opt.data = {
          head: opt.data,
      }
        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscwo/cpdocattribute/save.do`,
           data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },





    query_cpdocmaster:(opt) => {

        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscwo/cpdocattribute/query.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    delete_cpdocmaster:(opt) => {

        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscwo/cpdocattribute/del.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    save_cpdocchild:(opt) => {

        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscwo/cpdocattribute/save.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    query_cpdocchild:(opt) => {

        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscwo/cpdocattribute/query.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    delete_cpdocchild:(opt) => {

        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscwo/cpdocattribute/del.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    public_metadata:(opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscwo/cpdoc/publish.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },

    query_wo:(opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscwo/cpdoc/query.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    valueChange:(opt) => {

        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/expenseaccount/ExpenseaccountValueChangeAction.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    viewBill:(opt) =>{
        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscwo/cpdoc/query.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        });
    },
    queryapp:(opt) =>{
        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscwo/cpdoc/queryapp.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        });
    },
    viewHeadBody:(opt) =>{
        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscwo/cpdocattribute/query.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        });
    },
    send:(opt) =>{
        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscwo/cpdoc/publish.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        });
    },
    sendApp:(opt) =>{
        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscwo/cpdoc/publishapp.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        });
    }

}

export default  requestApiOverwrite;
