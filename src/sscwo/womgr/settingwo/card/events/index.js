import initTemplate from './initTemplate';
import afterEvent from './afterEvent';
import buttonEvent from './buttonEvent';
import dataModelChange from './dataModelChange';
import fixedTemplet from './fixedTemplet';


export { initTemplate,afterEvent,buttonEvent,dataModelChange,fixedTemplet};
