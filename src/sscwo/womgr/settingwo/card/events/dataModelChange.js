let changePage1Head = function(data,json){
    if(data == null || data.rows == null || data.rows.length == 0){
        return data;
    }
    for (let name in data.rows[0].values){
        if(name == 'ispubapplet' || name == 'ispubmetadata'){
            if(data.rows[0].values[name].value == true){
                data.rows[0].values[name] = {value:json['702001GDFL-0013'], display: json['702001GDFL-0013']};
            }else{
                data.rows[0].values[name] = {value:json['702001GDFL-0031'], display: json['702001GDFL-0031']};
            }
        }
        if(name == 'doc_structure'){
            if(data.rows[0].values[name].value == 'single'){
                data.rows[0].values[name] = {value:'single', display: json['702001GDFL-0051']};
            }else if(data.rows[0].values[name].value == 'multi'){
                data.rows[0].values[name] = {value:'multi', display: json['702001GDFL-0052']};
            }
        }
        if(name == 'doctype'){
            data.rows[0].values[name] = {
                value:data.rows[0].values[name].value, 
                display: (data.rows[0].values['typename'] || {}).value 
            };
        }
    }
    return data;
}
export default {changePage1Head}