import {setTableExtendCol} from 'ssccommon/components/profile'
import requestApi from '../requestApi'
import currentVar from '../presetVar'
import {fixedTemplet, dataModelChange} from './index'
import {getMultiLang} from 'nc-lightapp-front'
let pk_doc;
let initData=function(json){
    // getMultiLang({moduleId: 7020, domainName: 'sscwo',currentLocale: 'simpchn', callback: (json) => {
    let url = this.props.getUrlParam("status");
    let page = this.props.getUrlParam("page");
    let pk_doc1 = this.props.getUrlParam("pk_doc");
    if(pk_doc1){
       pk_doc =pk_doc1
    }
    if(page == 2){

       
        this.setState({page1StepStatus:'finish',page2StepStatus:'process', page3StepStatus:'wait',
                page1StepDescription:json['702001GDFL-0046'],page2StepDescription:json['702001GDFL-0047'], page3StepDescription: json['702001GDFL-0031'],
                page1Show:'hide', page2Show:'show', page3Show:'hide'});

        //保存完跳转到发布元数据界面。

        if(url == window.presetVar.status.browse || url == window.presetVar.status.edit){
            if(url == window.presetVar.status.browse){
                //设置按钮的初始可见性
                this.props.form.setFormStatus(currentVar.headArea, window.presetVar.status.browse);
                this.props.editTable.setStatus(currentVar.body1Area, window.presetVar.status.browse);
                this.props.editTable.setStatus(currentVar.body2Area, window.presetVar.status.browse);
                this.props.button.setButtonVisible(
                    [
                        currentVar.pageHeadButtons.Save,
                        currentVar.pageHeadButtons.Cancel,
                        currentVar.pageHeadButtons.Next,
                        currentVar.pageBody1Buttons.Add,
                        currentVar.pageBody1Buttons.Save,
                        currentVar.pageBody1Buttons.Cancel,
                        currentVar.pageBody2Buttons.Add,
                        currentVar.pageBody2Buttons.Save,
                        currentVar.pageBody2Buttons.Cancel,

                    ],
                    false);
                this.props.button.setButtonVisible(
                    [
                        currentVar.pageHeadButtons.Send,
                        currentVar.pageHeadButtons.Edit,
                        currentVar.pageBody1Buttons.Edit,
                        currentVar.pageBody2Buttons.Edit
                    ],
                    true);


            }else{
                //设置按钮的初始可见性
                this.props.button.setButtonVisible(
                    [
                        currentVar.pageHeadButtons.Send,
                        currentVar.pageHeadButtons.Next,
                        currentVar.pageHeadButtons.Edit,
                        currentVar.pageBody1Buttons.Edit,
                        currentVar.pageBody2Buttons.Edit,
                    ],
                    false);
                this.props.button.setButtonVisible(
                    [
                        currentVar.pageHeadButtons.Save,
                        currentVar.pageHeadButtons.Cancel,
                        currentVar.pageBody1Buttons.Add,
                        currentVar.pageBody1Buttons.Save,
                        currentVar.pageBody1Buttons.Cancel,
                        currentVar.pageBody2Buttons.Add,
                        currentVar.pageBody2Buttons.Save,
                        currentVar.pageBody2Buttons.Cancel,
                    ],
                    true);
                this.props.form.setFormStatus(currentVar.headArea, window.presetVar.status.edit);
                this.props.editTable.setStatus(currentVar.body1Area, window.presetVar.status.edit);
                this.props.editTable.setStatus(currentVar.body2Area, window.presetVar.status.edit);

                setTableExtendCol(this.props, this.props.meta.getMeta(), [{
                    areaId: currentVar.body1Area,
                    btnAreaId: currentVar.listBody1ButtonArea,
                    btnKeys: [currentVar.listBody1Buttons.Delete],
                    onButtonClick: ()=>{return {[currentVar.listBody1Buttons.Delete]:{ActionCode:'Delete'}}}
                }]);
                setTableExtendCol(this.props, this.props.meta.getMeta(), [{
                    areaId: currentVar.body2Area,
                    btnAreaId: currentVar.listBody2ButtonArea,
                    btnKeys: [currentVar.listBody2Buttons.Delete],
                    onButtonClick: ()=>{return {[currentVar.listBody2Buttons.Delete]:{ActionCode:'Delete'}}}
                }]);
            }
            requestApi.viewBill({
                data:{pk_doc: pk_doc},
                success:(data) =>{
                    if(data != null && data[currentVar.headArea]!=null){
                        // 表头返回的pk_doc
                        let headReturn_pk_doc = data[currentVar.headArea].rows[0].values.pk_doc.value;
                        // 表头返回的pk_doc
                        let headReturn_doc_structure = data[currentVar.headArea].rows[0].values.doc_structure.value;
                        // 表头返回的pk_doc
                        let headReturn_pk_doc_subtable = data[currentVar.headArea].rows[0].values.pk_doc_subtable.value;
                        this.props.form.setAllFormValue({[currentVar.headArea]:dataModelChange.changePage1Head(data[currentVar.headArea],json)});
                        if(data.cpdoc.rows[0].values.ispubapplet.value == json['702001GDFL-0013']){
                            this.setState({page2StepDescription:json['702001GDFL-0046'],page3StepDescription:json['702001GDFL-0046'],page3StepStatus:'finish'});
                            this.props.button.setButtonVisible(
                                [
                                    currentVar.pageHeadButtons.Edit,
                                    currentVar.pageHeadButtons.Send,
                                    currentVar.pageBody1Buttons.Edit,
                                    currentVar.pageBody2Buttons.Edit,

                                ],
                                false);
                            this.props.button.setButtonVisible(
                                [
                                    currentVar.pageHeadButtons.Next,


                                ],
                                true);
                        }else if(data.cpdoc.rows[0].values.ispubmetadata.value == json['702001GDFL-0013']){
                            this.setState({page3StepDescription: json['702001GDFL-0031'],page3StepStatus:'wait'});
                            this.props.button.setButtonVisible(
                                [
                                   
                                    currentVar.pageHeadButtons.Send,
                                    

                                ],
                                false);
                            this.props.button.setButtonVisible(
                                [
                                    currentVar.pageHeadButtons.Edit,
                                    currentVar.pageBody1Buttons.Edit,
                                    currentVar.pageBody2Buttons.Edit,
                                    currentVar.pageHeadButtons.Next,


                                ],
                                true);
                        }
                        requestApi.viewHeadBody({
                            data:{pk_doc: headReturn_pk_doc, grid_code:'cpdocmaster'},
                            success:(dataBody) =>{
                                if(dataBody != null && dataBody[currentVar.body1Area]!=null){
                                    if(dataBody.cpdocmaster){
                                        for(let i in dataBody.cpdocmaster.rows){
                                            if(i!='remove'&& dataBody.cpdocmaster.rows[i].values.issystemattr){
                                                if(dataBody.cpdocmaster.rows[i].values.issystemattr.value==true){
                                                    dataBody.cpdocmaster.rows[i].values['issystemattr'] = {value:true, display:json['702001GDFL-0044']};
                                                }else{
                                                    dataBody.cpdocmaster.rows[i].values['issystemattr'] = {value:false, display: json['702001GDFL-0045']};
                                                }
                                            }
                                        }

                                    }
                                    this.props.editTable.setTableData(currentVar.body1Area, dataBody[currentVar.body1Area]);
                                }
                            }
                        });
                        if(headReturn_doc_structure == 'single'){
                            this.setState({body1Show:'show',body2Show:'hide'});
                        }else{
                            this.setState({body1Show:'show',body2Show:'show'});
                            requestApi.viewHeadBody({
                                data:{pk_doc: headReturn_pk_doc_subtable, grid_code:'cpdocchild'},
                                success:(dataBody2) =>{
                                    if(dataBody2 != null && dataBody2[currentVar.body2Area]!=null){
                                        if(dataBody2.cpdocchild){
                                            for(let i in dataBody2.cpdocchild.rows){
                                                if(i!='remove'&& dataBody2.cpdocchild.rows[i].values.issystemattr){
                                                    if(dataBody2.cpdocchild.rows[i].values.issystemattr.value==true){
                                                        dataBody2.cpdocchild.rows[i].values['issystemattr'] = {value:true, display: json['702001GDFL-0044']};
                                                    }else{
                                                        dataBody2.cpdocchild.rows[i].values['issystemattr'] = {value:false, display: json['702001GDFL-0045']};
                                                    }
                                                }
                                            }

                                        }
                                        this.props.editTable.setTableData(currentVar.body2Area, dataBody2[currentVar.body2Area]);
                                    }
                                }
                            });
                        }
                        
                    }
                }
            });
        }
        else{
            //设置按钮的初始可见性
            this.props.button.setButtonVisible(
                [
                    currentVar.pageHeadButtons.Edit,
                    currentVar.pageHeadButtons.Send,
                    currentVar.pageHeadButtons.Next,
                    currentVar.pageBody1Buttons.Edit,
                    currentVar.pageBody2Buttons.Edit
                ],
                false);
            this.props.form.setFormStatus(currentVar.headArea, window.presetVar.status.add);
        }

       


       

    }
    else if(page == 3){
        this.setState({page1StepDescription:json['702001GDFL-0046'],page2StepDescription:json['702001GDFL-0046'], page3StepDescription:json['702001GDFL-0047'],
            page1StepStatus:'finish',page2StepStatus:'finish', page3StepStatus:'process',page1Show:'hide', page2Show:'hide', page3Show:'show'});

        requestApi.viewBill({
            data:{pk_doc: pk_doc},
            success:(data) =>{
                let isSendApp = false;
                if(data != null && data[currentVar.headArea]!=null){
                    if(data[currentVar.headArea].rows[0].values.ispubapplet.value == true){
                        isSendApp = true;
                    }
                }
                if(isSendApp == true){
                    this.setState({page3FormShow:'show', page3MsgShow:'show',page1StepDescription:json['702001GDFL-0046'],page2StepDescription:json['702001GDFL-0046'], page3StepDescription:json['702001GDFL-0046'],
                        page1StepStatus:'finish',page2StepStatus:'finish', page3StepStatus:'finish'});
                    this.props.button.setButtonVisible([currentVar.page3Buttons.Send], false);
                    this.props.form.setFormStatus(currentVar.page3Area, window.presetVar.status.browse);
                }else{
                    this.setState({page3FormShow:'show', page3MsgShow:'hide'});
                    this.props.button.setButtonVisible([currentVar.page3Buttons.Send], true);
                    this.props.form.setFormStatus(currentVar.page3Area, window.presetVar.status.add);
                }

                let formData
                let initdata = dataModelChange.changePage1Head(data[currentVar.headArea],json);            
               let curstatus=this.props.form.getFormStatus(currentVar.page3Area);            
                if(window.presetVar.status.browse==curstatus){

                    requestApi.queryapp({
                    data:{pk_doc: pk_doc},
                    success:(data) =>{
                      console.log(111,data)

                      
                        this.props.form.setAllFormValue({[currentVar.headArea]:initdata});
                let menuItemCode=data[0]
                let menuItemName=data[1]
                let app_name=data[2]
                 formData = {
                    areaType:'form',
                    rows:[{values:{menuItemCode:{value:menuItemCode},menuItemName:{value:menuItemName},app_name:{value:app_name}}}]
                    }
                    this.props.form.setAllFormValue({[currentVar.page3Area]:formData});
                    }
                    
                    })
                    }else{       
                       
                        this.props.form.setAllFormValue({[currentVar.headArea]:initdata});
                        let menuItemCode='17020B'+initdata.rows[0].values.doc_code.value;
                        let menuItemName=initdata.rows[0].values.doc_name.value;
                        let app_name=initdata.rows[0].values.doc_name.value;
                         formData = {
                            areaType:'form',
                            rows:[{values:{menuItemCode:{value:menuItemCode},menuItemName:{value:menuItemName},app_name:{value:app_name}}}]
                        }
                        this.props.form.setAllFormValue({[currentVar.page3Area]:formData});
                    }



                
               
                this.setState({page1Show:'hide', page2Show:'hide', page3Show:'show',});
            }
        });
    }else{
        this.setState({page1Show:'show', page2Show:'hide', page3Show:'hide',page1StepDescription:json['702001GDFL-0047'],page2StepDescription: json['702001GDFL-0031'], page3StepDescription: json['702001GDFL-0031'],
        page1StepStatus:'process',page2StepStatus:'wait', page3StepStatus:'wait'});
        if(url == window.presetVar.status.browse || url == window.presetVar.status.edit){
            if(url == window.presetVar.status.browse){
                //设置按钮的初始可见性
                this.props.button.setButtonVisible(
                    [
                        currentVar.pageHeadButtons.Save,
                        currentVar.pageHeadButtons.Next,
                        currentVar.pageHeadButtons.Cancel,
                        currentVar.pageBody1Buttons.Add, 
                        currentVar.pageBody1Buttons.Save, 
                        currentVar.pageBody1Buttons.Cancel, 
                        currentVar.pageBody2Buttons.Add, 
                        currentVar.pageBody2Buttons.Save, 
                        currentVar.pageBody2Buttons.Cancel,
                    ], 
                    false);
                this.props.button.setButtonVisible(
                    [
                        currentVar.pageHeadButtons.Send, 
                        currentVar.pageHeadButtons.Edit,
                        currentVar.pageBody1Buttons.Edit,
                        currentVar.pageBody2Buttons.Edit
                    ], 
                    true);
            }
            else{
                //设置按钮的初始可见性
                this.props.button.setButtonVisible(
                    [
                        currentVar.pageHeadButtons.Send,
                        currentVar.pageHeadButtons.Next,
                        currentVar.pageHeadButtons.Edit,
                        currentVar.pageBody1Buttons.Edit,
                        currentVar.pageBody2Buttons.Edit
                    ], 
                    false);
                this.props.button.setButtonVisible(
                    [
                        currentVar.pageHeadButtons.Save, 
                        currentVar.pageHeadButtons.Cancel,
                        currentVar.pageBody1Buttons.Add, 
                        currentVar.pageBody1Buttons.Save, 
                        currentVar.pageBody1Buttons.Cancel, 
                        currentVar.pageBody2Buttons.Add, 
                        currentVar.pageBody2Buttons.Save, 
                        currentVar.pageBody2Buttons.Cancel,
                    ], 
                    true);
                this.props.form.setFormStatus(currentVar.headArea, window.presetVar.status.edit);
                this.props.editTable.setStatus(currentVar.body1Area, window.presetVar.status.edit);
                this.props.editTable.setStatus(currentVar.body2Area, window.presetVar.status.edit);

                setTableExtendCol(this.props, this.props.meta.getMeta(), [{
                    areaId: currentVar.body1Area,
                    btnAreaId: currentVar.listBody1ButtonArea,
                    btnKeys: [currentVar.listBody1Buttons.Delete],
                    onButtonClick: ()=>{return {[currentVar.listBody1Buttons.Delete]:{ActionCode:'Delete'}}}
                }]);
                setTableExtendCol(this.props, this.props.meta.getMeta(), [{
                    areaId: currentVar.body2Area,
                    btnAreaId: currentVar.listBody2ButtonArea,
                    btnKeys: [currentVar.listBody2Buttons.Delete],
                    onButtonClick: ()=>{return {[currentVar.listBody2Buttons.Delete]:{ActionCode:'Delete'}}}
                }]);
            }
            requestApi.viewBill({
                data:{pk_doc: pk_doc},
                success:(data) =>{
                    if(data != null && data[currentVar.headArea]!=null){
                        if(data[currentVar.headArea].rows[0].values.ispubapplet.value == true){
                            this.setState({page2StepDescription:json['702001GDFL-0046'],page2StepStatus:'finish'});
                        }
                        if(data[currentVar.headArea].rows[0].values.ispubmetadata.value == true){
                            this.setState({page3StepDescription:json['702001GDFL-0046'],page3StepStatus:'finish'});
                        }
                        this.props.form.setAllFormValue({[currentVar.headArea]:dataModelChange.changePage1Head(data[currentVar.headArea],json)});
                        requestApi.viewHeadBody({
                            data:{pk_doc: data[currentVar.headArea].rows[0].values.pk_doc.value, grid_code:'cpdocmaster'},
                            success:(data) =>{
                                if(data != null && data[currentVar.body1Area]!=null){
                                    if(data.cpdocmaster){
                                        for(let i in data.cpdocmaster.rows){
                                           if(i!='remove'&&data.cpdocmaster.rows[i].values.issystemattr){
                                               if(data.cpdocmaster.rows[i].values.issystemattr.value==true){
                                                   data.cpdocmaster.rows[i].values['issystemattr'] = {value:true, display: json['702001GDFL-0044']};
                                               }else{
                                                   data.cpdocmaster.rows[i].values['issystemattr'] = {value:false, display: json['702001GDFL-0045']};
                                               }
                                           }
                                        }

                                    }
                                    this.props.editTable.setTableData(currentVar.body1Area, data[currentVar.body1Area]);
                                }
                            }
                        });
                        if(data[currentVar.headArea].rows[0].values.doc_structure.value == 'single'){
                            this.setState({body1Show:'show',body2Show:'hide'});
                        }else{
                            this.setState({body1Show:'show',body2Show:'show'});
                            requestApi.viewHeadBody({
                                data:{pk_doc: data[currentVar.headArea].rows[0].values.pk_doc_subtable.value, grid_code:'cpdocchild'},
                                success:(data) =>{
                                    if(data != null && data[currentVar.body2Area]!=null){
                                        if(data.cpdocchild){
                                            for(let i in data.cpdocchild.rows){
                                                if(i!='remove'&&data.cpdocchild.rows[i].values.issystemattr){
                                                    if(data.cpdocchild.rows[i].values.issystemattr.value==true){
                                                        data.cpdocchild.rows[i].values['issystemattr'] = {value:true, display: json['702001GDFL-0044']};
                                                    }else{
                                                        data.cpdocchild.rows[i].values['issystemattr'] = {value:false, display: json['702001GDFL-0045']};
                                                    }
                                                }
                                            }
                                        }
                                        this.props.editTable.setTableData(currentVar.body2Area, data[currentVar.body2Area]);
                                    }
                                }
                            });
                        }
                    }
                }
            });
        }
        else{
            //设置按钮的初始可见性
            this.props.button.setButtonVisible(
                [
                    currentVar.pageHeadButtons.Edit,
                    currentVar.pageHeadButtons.Next,
                    currentVar.pageHeadButtons.Send,
                    currentVar.pageBody1Buttons.Edit, 
                    currentVar.pageBody2Buttons.Edit
                ], 
                false);
            this.props.button.setButtonVisible(
                [
                    currentVar.pageHeadButtons.Cancel,
                    currentVar.pageHeadButtons.Save,

                ],
                true);
            this.props.form.setFormStatus(currentVar.headArea, window.presetVar.status.add);
        }
    }
    // }})
}
export default function (props, flag) {
    let that = this;
    if(flag == true){
        getMultiLang({moduleId: 7020, domainName: 'sscwo',currentLocale: 'simpchn',
                   callback: (json) => {
            initData.call(that,json);
        }})
    }else{
        let createUIDomPromise = new Promise((resolve, reject) => {
            props.createUIDom({
                pagecode: currentVar.pageId,//页面id
                appid: ' '//注册按钮的id
            }, (data) => {
                resolve(data);
            })
        });
    
        let getMultiLangPromise = new Promise((resolve, reject) => {
            getMultiLang({
                moduleId: 7020, domainName: 'sscwo',currentLocale: 'simpchn',
                callback: (json) => {
                    resolve(json);
                }
            })
        });
        Promise.all([createUIDomPromise, getMultiLangPromise]).then((resultList) => {
            let data = resultList[0];
            let json = resultList[1];
            
            let meta = data.template;
            let fixedTempletValue = fixedTemplet(props, json);
            meta[currentVar.page3Area] = fixedTempletValue[currentVar.page3Area];
            meta['formrelation'] = null;
            //设置区域模板
            props.meta.setMeta(meta);
            //设置按钮模板
            props.button.setButtons(fixedTempletValue.button);
            initData.call(that,json);
            })
        
    }
}