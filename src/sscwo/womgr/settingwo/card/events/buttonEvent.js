import {setTableExtendCol} from 'ssccommon/components/profile'
import pubUtils from 'ssccommon/utils/pubUtils';
import {initTemplate} from './index'
import currentVar from '../presetVar'
import requestApi from '../requestApi'
import {base,promptBox,toast} from 'nc-lightapp-front';

const {NCMessage} = base;
let flag;

function buttonEvent() {
    return {
       
        [currentVar.pagestep]:{
            click:(ncProps)=>{
                let multiLang = ncProps.MutiInit.getIntl(7020);

                let initdata=ncProps.form.getAllFormValue('cpdoc');
                if( initdata.rows[0].values.ispubapplet.value==(multiLang && multiLang.get("702001GDFL-0013"))){
                    ncProps.linkTo('/sscwo/womgr/settingwo/list/index.html',{
                        pagecode: currentVar.listPageId
                    })
                }else{
                    let multiLang = ncProps.MutiInit.getIntl(7020);
                    this.setState({pagestepmsg:multiLang && multiLang.get("702001GDFL-0042")});
                    //this.props.modal.show(currentVar.pagestep);
                    promptBox({
                    title: multiLang && multiLang.get("7020-0001"), // 弹框表头信息
                    content: this.state.pagestepmsg, //弹框内容，可以是字符串或dom
                    beSureBtnClick: () => {
                        NCMessage.create({
                            content: multiLang && multiLang.get("702001GDFL-0007"),
                            color: "success",
                            position: "bottomRight"
                        });
                        this.props.linkTo("/sscwo/womgr/settingwo/list/index.html", {
                            pagecode: currentVar.listPageId
                        });
                    }, //点击确定按钮事件
                    // "7020-0001": "确定",
                    // "7020-0002": "取消",
                    userControl: false, // 点确定按钮后，是否自动关闭弹出框.true:手动关。false:自动关
                    rightBtnName: multiLang && multiLang.get("7020-0002"), //左侧按钮名称,默认关闭
                    leftBtnName: multiLang && multiLang.get("7020-0001") //右侧按钮名称， 默认确认
                })
            }
        }},
        [currentVar.pageHeadButtons.Edit]:{
            ActionCode:'Edit',
         afterClick: (ncProps)=>{
            this.setState({'refresh':true});
            ncProps.form.setFormStatus('cpdoc', 'edit');
             ncProps.form.setFormItemsDisabled('cpdoc',{'doc_structure':true});
            let tt= ncProps.form.getAllFormValue('cpdoc');
            console.log(1211,tt)
             this.props.button.setButtonVisible([currentVar.pageHeadButtons.Next], false);
             this.props.button.setButtonVisible([currentVar.pageHeadButtons.Send], false);
             //增加一个业务情景，表头修改之后表体也修改
             flag=1;
           }

        },
        [currentVar.pageHeadButtons.Save]:{

            click: (ncProps)=>{
                let saveData={};
                saveData['cpdoc'] = ncProps.form.getAllFormValue('cpdoc');
               
                requestApi.save({
                        data:saveData,
                    success: data => {
                        ncProps.linkTo(location.pathname,{page:'2',status:window.presetVar.status.browse,pk_doc:data.cpdoc.rows[0].values.pk_doc.value,pk_doc_subtable:data.cpdoc.rows[0].values.pk_doc_subtable.value});

                        if((this.props.meta.getMeta()[currentVar.body1Area].items[this.props.meta.getMeta()[currentVar.body1Area].items.length-1] || {}).attrcode == 'opr'){
                            this.props.meta.getMeta()[currentVar.body1Area].items.pop();
                        }
                        if((this.props.meta.getMeta()[currentVar.body2Area].items[this.props.meta.getMeta()[currentVar.body2Area].items.length-1] || {}).attrcode == 'opr'){
                            this.props.meta.getMeta()[currentVar.body2Area].items.pop();
                        }
                        setTimeout(()=>{
                            initTemplate.call(this, this.props, true);
                        })
                        //恢复默认
                        flag=0;
                    }
                });
            },

            ActionCode:'Save',
            url: '/nccloud/sscwo/cpdoc/save.do',
            afterClick: (ncProps, data)=>{
                ncProps.linkTo(location.pathname,{page:'2',status:window.presetVar.status.browse,pk_doc:data.data.cpdoc.rows[0].values.pk_doc.value,pk_doc_subtable:data.data.cpdoc.rows[0].values.pk_doc_subtable.value});

                if((this.props.meta.getMeta()[currentVar.body1Area].items[this.props.meta.getMeta()[currentVar.body1Area].items.length-1] || {}).attrcode == 'opr'){
                    this.props.meta.getMeta()[currentVar.body1Area].items.pop();
                }
                if((this.props.meta.getMeta()[currentVar.body2Area].items[this.props.meta.getMeta()[currentVar.body2Area].items.length-1] || {}).attrcode == 'opr'){
                    this.props.meta.getMeta()[currentVar.body2Area].items.pop();
                }
                setTimeout(()=>{
                    initTemplate.call(this, this.props, true);
                })
                //恢复默认
                flag=0;
            }

        },

        [currentVar.pageHeadButtons.Cancel]:{
            ActionCode:'Cancel',
            click: ()=>{
                let multiLang = this.props.MutiInit.getIntl('7020'); 
                //let t=this.props.form.getAllFormValue('cpdoc').rows[0].values;
               let page= this.props.getUrlParam("page");
               //发布小应用和元数据页面都能获取到值
               if(page==undefined||page==1){
                   this.setState({page1ConfirmMsg:multiLang && multiLang.get("702001GDFL-0043")});
                   //this.props.modal.show(currentVar.confirms.page1SendConfirm);  
                   promptBox({
                                // "7020-0001": "确定",
                                // "702001GDFL-0007": "退回到列表状态",
                                title: multiLang && multiLang.get("7020-0001"), // 弹框表头信息
                                content: multiLang && multiLang.get("702001GDFL-0002"), //'确定退出新增工单', //弹框内容，可以是字符串或dom
                                beSureBtnClick: () => {
                                    NCMessage.create({
                                        content: multiLang && multiLang.get("702001GDFL-0007"),
                                        color: "success",
                                        position: "bottomRight"
                                    });
                                    this.props.linkTo("/sscwo/womgr/settingwo/list/index.html",
                                        {
                                            pagecode: currentVar.listPageId
                                        }
                                    );
                                }, //点击确定按钮事件
                                cancelBtnClick: () => {
                                    pubUtils.setUrlParam("page", 1);
                                    pubUtils.setUrlParam("status", window.presetVar.status.add);
                                    initTemplate.call(this, this.props, false);
                                },
                                userControl: false, // 点确定按钮后，是否自动关闭弹出框.true:手动关。false:自动关
                                // 7020-0001": "确定",
                                // 7020-0003": "关闭",
                                rightBtnName: multiLang && multiLang.get("7020-0003"), //左侧按钮名称,默认关闭
                                leftBtnName: multiLang && multiLang.get("7020-0001") //右侧按钮名称， 默认确认
                                
                            })
               };
               if(page==2){

                   this.props.form.setFormStatus(currentVar.headArea, window.presetVar.status.browse);
                   setTimeout(()=>{
                    initTemplate.call(this, this.props, true);
                })
                   if((this.props.meta.getMeta()[currentVar.body1Area].items[this.props.meta.getMeta()[currentVar.body1Area].items.length-1] || {}).attrcode == 'opr'){
                        this.props.meta.getMeta()[currentVar.body1Area].items.pop();
                    }
                    if((this.props.meta.getMeta()[currentVar.body2Area].items[this.props.meta.getMeta()[currentVar.body2Area].items.length-1] || {}).attrcode == 'opr'){
                        this.props.meta.getMeta()[currentVar.body2Area].items.pop();
                    }
                   flag=0;
               }

            }
        },
        [currentVar.pageBody1Buttons.Add]:{
            ActionCode:'Add',
            afterClick:(ncProps)=>{
                //新增属性的时候，属性值不可编辑。
                let masterdata= this.props.editTable.getAllData(currentVar.body1Area).rows;
                let index;
                let systemattr=[];
                for(index in masterdata) {
                    if(index==masterdata.length-1){
                            systemattr.push( masterdata[index].rowid);
                            let disabledArr2 = ['issystemattr'];

                        this.props.editTable.setValByKeyAndIndex(currentVar.body1Area,parseInt(index),'nullable',{value:'true',isEdit:false});
                        this.props.editTable.setEditableRowKeyByIndex(currentVar.body1Area,parseInt(index) , disabledArr2, false)


                    }
                }



            }

        },
        [currentVar.pageBody1Buttons.Edit]:{
            ActionCode:'Edit',
            afterClick: (ncProps, data)=>{
                setTableExtendCol(ncProps, this.props.meta.getMeta(), [{
                    areaId: currentVar.body1Area,
                    btnAreaId: currentVar.listBody1ButtonArea,
                    //btnKeys: [currentVar.listBody1Buttons.Delete],
                    buttonVisible:(record, index) => {
                        if (record.values.issystemattr.display=='是' || record.values.issystemattr.display=='en_i是') {

                        } else {
                            return [currentVar.listBody1Buttons.Delete]
                        }
                    },

                    onButtonClick: ()=>{return {[currentVar.listBody1Buttons.Delete]:{ActionCode:'Delete'}}}
                }]);
                this.props.button.setButtonVisible([currentVar.pageBody1Buttons.Add], true);

                let masterdata=ncProps.editTable.getAllData(currentVar.body1Area).rows;
                let index;
                let disabledArr = [];
                let disabledArr1 = [];
                let systemattr=[];
                for(index in masterdata) {
                    systemattr.push( masterdata[index].rowid);
                    if(index!='remove'&& masterdata[index].values.issystemattr.display=='是'){
                        disabledArr.push( masterdata[index].rowid);
                    }else{
                        if(index!='remove'){
                             disabledArr1.push( masterdata[index].rowid);
                       //     let disabledArr3 = ['issystemattr'];
                       //     ncProps.editTable.setEditableRowKeyByIndex(currentVar.body1Area,parseInt(index), disabledArr3, false)
                        }


                    }

                }


                setTimeout(function(){
                    let disabledArr3 = ['issystemattr'];
                    ncProps.editTable.setEditableRowKeyByRowId(currentVar.body1Area, disabledArr1, disabledArr3,false);
                    let disabledArr2 = ['name','type','defaultvalue','issystemattr','nullable'];
                    ncProps.editTable.setEditableRowKeyByRowId(currentVar.body1Area, disabledArr, disabledArr2,false);}, 500);

            }
        },
        [currentVar.pageBody1Buttons.Save]:{
            ActionCode:'Save',
            url: '/nccloud/sscwo/cpdocattribute/save.do',
            afterClick: ()=>{


                this.props.meta.getMeta()[currentVar.body1Area].items.pop();
                this.props.button.setButtonVisible([currentVar.pageBody1Buttons.Add], false);
                this.props.button.setButtonVisible([currentVar.pageHeadButtons.Send], true);
                //增加一个业务情景，表头修改之后表体也修改
                if(flag==1){
                    this.props.button.setButtonVisible([currentVar.pageHeadButtons.Edit], false);
                    this.props.button.setButtonVisible([currentVar.pageHeadButtons.Send], false);
                }else{
                    this.props.button.setButtonVisible([currentVar.pageHeadButtons.Edit], true);
                    this.props.button.setButtonVisible([currentVar.pageHeadButtons.Send], true);
                }
               // this.props.button.setButtonVisible([currentVar.pageHeadButtons.Edit], true);
                this.props.button.setButtonVisible([currentVar.pageHeadButtons.Next], false);
                let pkdoc=this.props.getUrlParam("pk_doc");
                requestApi.viewHeadBody({

                    data:{pk_doc: pkdoc, grid_code:'cpdocmaster'},
                    success:(data) =>{
                        if(data != null && data[currentVar.body1Area]!=null){
                            if(data.cpdocmaster){
                                for(let i in data.cpdocmaster.rows){
                                    if(i!='remove'&&data.cpdocmaster.rows[i].values.issystemattr){
                                        if(data.cpdocmaster.rows[i].values.issystemattr.value==true){
                                            data.cpdocmaster.rows[i].values['issystemattr'] = {value:true, display: '是'};
                                        }else{
                                            data.cpdocmaster.rows[i].values['issystemattr'] = {value:false, display: '否'};
                                        }
                                    }
                                }

                            }
                            this.props.editTable.setTableData(currentVar.body1Area, data[currentVar.body1Area]);
                        }
                    }
                });

            }
        },
        [currentVar.pageBody1Buttons.Cancel]:{
            ActionCode:'Cancel',
            afterClick: ()=>{
                this.props.meta.getMeta()[currentVar.body1Area].items.pop();
                this.props.button.setButtonVisible([currentVar.pageBody1Buttons.Add], false);
            }
        },
        [currentVar.pageBody2Buttons.Add]:{ActionCode:'Add',
            afterClick: (ncProps)=>{
                //新增属性的时候，属性值不可编辑。
                let masterdata=this.props.editTable.getAllData(currentVar.body2Area).rows;
                let index;
                let systemattr=[];
                for(index in masterdata) {
                    if(index==masterdata.length-1){
                        systemattr.push( masterdata[index].rowid);
                        let disabledArr2 = ['issystemattr'];

                        this.props.editTable.setValByKeyAndIndex(currentVar.body2Area,parseInt(index),'nullable',{value:'true',isEdit:false});
                        this.props.editTable.setEditableRowKeyByIndex(currentVar.body2Area,parseInt(index) , disabledArr2, false)


                    }
                }

            }
        },
        [currentVar.pageBody2Buttons.Edit]:{
            ActionCode:'Edit',
            afterClick: (ncProps, data)=>{
                setTableExtendCol(ncProps, this.props.meta.getMeta(), [{
                    areaId: currentVar.body2Area,
                    btnAreaId: currentVar.listBody2ButtonArea,
                    //btnKeys: [currentVar.listBody2Buttons.Delete],
                    buttonVisible:(record, index) => {
                        if (record.values.issystemattr.display=='是' || record.values.issystemattr.display == 'en_i是') {

                        } else {
                            return [currentVar.listBody2Buttons.Delete]
                        }
                    },
                    onButtonClick: ()=>{return {[currentVar.listBody2Buttons.Delete]:{ActionCode:'Delete'}}}
                }]);
                this.props.button.setButtonVisible([currentVar.pageBody2Buttons.Add], true);


                let childdata=ncProps.editTable.getAllData(currentVar.body2Area).rows;
                let index;
                let disabledArr = [];
                let disabledArr1 = [];
                for(index in childdata) {

                    if(index!='remove'&& childdata[index].values.issystemattr.display=='是'){
                        disabledArr.push( childdata[index].rowid)
                    }else{
                        if(index!='remove'){
                            disabledArr1.push( childdata[index].rowid)
                        }
                    }
                }




                setTimeout(function(){
                    let disabledArr3 = ['issystemattr'];
                    ncProps.editTable.setEditableRowKeyByRowId(currentVar.body2Area, disabledArr1, disabledArr3,false);
                    let disabledArr2 = ['name','type','defaultvalue','issystemattr','nullable'];
                    ncProps.editTable.setEditableRowKeyByRowId(currentVar.body2Area, disabledArr, disabledArr2,false);}, 500);

            }
        },
        [currentVar.pageBody2Buttons.Save]:{
            ActionCode:'Save',
            url: '/nccloud/sscwo/cpdocattribute/save.do',
            afterClick: (ncProps)=>{
                this.props.meta.getMeta()[currentVar.body2Area].items.pop();
                this.props.button.setButtonVisible([currentVar.pageBody2Buttons.Add], false);
                this.props.button.setButtonVisible([currentVar.pageHeadButtons.Send], true);
               
                if(flag==1){
                    this.props.button.setButtonVisible([currentVar.pageHeadButtons.Edit], false);
                    this.props.button.setButtonVisible([currentVar.pageHeadButtons.Send], false);
                }else{
                    this.props.button.setButtonVisible([currentVar.pageHeadButtons.Edit], true);
                    this.props.button.setButtonVisible([currentVar.pageHeadButtons.Send], true);
                }
                this.props.button.setButtonVisible([currentVar.pageHeadButtons.Next], false);

                 
                //let cpdocchilddata=ncProps.form.getAllFormValue('cpdoc').rows[0].values.pk_doc_subtable.value||this.props.getUrlParam("pk_doc_subtable");;
                                let cpdocchilddata=this.props.getUrlParam("pk_doc_subtable");

                requestApi.viewHeadBody({
                    data:{pk_doc: cpdocchilddata, grid_code:'cpdocchild'},
                    success:(data) =>{
                        if(data != null && data[currentVar.body2Area]!=null){
                            if(data.cpdocchild){
                                for(let i in data.cpdocchild.rows){
                                    if(i!='remove'&&data.cpdocchild.rows[i].values.issystemattr){
                                        if(data.cpdocchild.rows[i].values.issystemattr.value==true){
                                            data.cpdocchild.rows[i].values['issystemattr'] = {value:true, display: '是'};
                                        }else{
                                            data.cpdocchild.rows[i].values['issystemattr'] = {value:false, display: '否'};
                                        }
                                    }
                                }

                            }
                            this.props.editTable.setTableData(currentVar.body2Area, data[currentVar.body2Area]);
                        }
                    }
                });

            }
        },
        [currentVar.pageBody2Buttons.Cancel]:{
            ActionCode:'Cancel',
            afterClick: ()=>{
                this.props.meta.getMeta()[currentVar.body2Area].items.pop();
                this.props.button.setButtonVisible([currentVar.pageBody2Buttons.Add], false);
            }
        },
        [currentVar.page2Buttons.Edit]:{
            click: ()=>{
               // pubUtils.setUrlParam('page',1);
              //  pubUtils.setUrlParam('status', window.presetVar.status.edit);
                this.props.linkTo(location.pathname,{page:'1',status:window.presetVar.status.edit});
                setTimeout(()=>{
                    initTemplate.call(this, this.props, true);
                })
            }
        },
        [currentVar.pageHeadButtons.Send]:{
            click: ()=>{
                let pk_doc = this.props.getUrlParam("pk_doc");
                requestApi.send({
                    data:{pk_doc:pk_doc},
                    success:(data)=>{
                      
                        if(data!=null){
                            this.setState({page2ConfirmMsg: data});
                            //this.props.modal.show(currentVar.confirms.page2SendConfirm);
                            let multiLang = this.props.MutiInit.getIntl('7020'); 
                            promptBox({
                                color: 'warning',    
                                title: multiLang && multiLang.get("7020-0001"), // 弹框表头信息
                                content: this.state.page2ConfirmMsg, //弹框内容，可以是字符串或dom
                                beSureBtnClick: () => {
                                    requestApi.send({
                                        data: {
                                            pk_doc: this.props.getUrlParam("pk_doc"),
                                            re_publish: "true"
                                        },
                                        success: data => {
                                            NCMessage.create({
                                                content: multiLang && multiLang.get("702001GDFL-0013"),
                                                color: "success",
                                                position: "bottomRight"
                                            });
                                            this.props.linkTo(location.pathname, {
                                                page: "3",
                                                status: window.presetVar.status.edit,
                                                pk_doc: this.props.getUrlParam("pk_doc")
                                            });
                                            // pubUtils.setUrlParam('page',3);
                                            // pubUtils.setUrlParam('status', window.presetVar.status.edit);
                                            setTimeout(() => {
                                                initTemplate.call(this, this.props, true);
                                            });
                                        }
                                    });
                                },
                                beSureBtnName:  multiLang && multiLang.get('702001GDFL-0005'),        
                            })

                        }else{
                            
                            toast({ title: '发布成功' })
                           // pubUtils.setUrlParam('page',3);
                          //  pubUtils.setUrlParam('status', window.presetVar.status.edit);
                            this.props.linkTo(location.pathname,{page:'3',status:window.presetVar.status.edit,pk_doc:pk_doc});
                            setTimeout(()=>{
                                initTemplate.call(this, this.props, true);
                            })
                        }
                    }
                })
            }
        },
        [currentVar.page3Buttons.Edit]:{
            click: ()=>{
                this.props.form.setFormStatus(currentVar.page3Area, window.presetVar.status.Edit);
                this.props.button.setButtonVisible([currentVar.page3Buttons.Send], true);
                this.props.button.setButtonVisible([currentVar.page3Buttons.Edit], false);
            }
        },
        [currentVar.page3Buttons.Back]:{
            ActionCode:'Edit',
            click: ()=>{
                 
                  this.props.linkTo(location.pathname,{page:'2',status: window.presetVar.status.browse,pk_doc:this.props.getUrlParam("pk_doc")});             
                   this.props.button.setButtonVisible([currentVar.pageHeadButtons.Next], true);
                   this.props.button.setButtonVisible([currentVar.pageHeadButtons.Send], false);
                   initTemplate.call(this, this.props, true);
 
                }
            },
        [currentVar.pageHeadButtons.Next]:{
            ActionCode:'Edit',
            click: ()=>{
              //  pubUtils.setUrlParam('page',3);
              //  pubUtils.setUrlParam('status', window.presetVar.status.browse);
                this.props.linkTo(location.pathname,{page:'3',status:window.presetVar.status.browse,pk_doc:this.props.getUrlParam("pk_doc")});
                setTimeout(()=>{
                    initTemplate.call(this, this.props, true);
                })


            }
        },
        [currentVar.page3Buttons.Send]:{
            click: ()=>{
                // this.props.modal.show(currentVar.confirms.page3SendConfirm);
                let multiLang = this.props.MutiInit.getIntl('7020'); 
                promptBox({
                    color: 'warning',              
                    title: multiLang && multiLang.get('702001GDFL-0012'),                   
                    content: multiLang && multiLang.get("702001GDFL-0003"),
                    beSureBtnClick: ()=>{
                        let data = ((this.props.form.getAllFormValue(currentVar.page3Area) || {}).rows[0] || {}).values || {};
                        requestApi.sendApp({
                            data:{
                                pk_doc: this.props.getUrlParam("pk_doc"),
                                app_name: data.app_name.value,
                                menuItemCode: data.menuItemCode.value,
                                menuItemName: data.menuItemName.value
                            },
                            // "702001GDFL-0013": "发布成功",
                            success:(data)=>{
                                //NCMessage.create({content: multiLang && multiLang.get('702001GDFL-0013'), color: 'success', position: 'bottomRight'});
                                toast({ title: '发布成功' })
                                this.props.form.setFormStatus(currentVar.page3Area, window.presetVar.status.browse);
                                this.props.button.setButtonVisible([currentVar.page3Buttons.Send], false);
                             //  initTemplate.call(this, this.props);
                            }
                        })
                    },
                    beSureBtnName:  multiLang && multiLang.get('702001GDFL-0006'),        
                })
            }
        }
}
}
export default buttonEvent;