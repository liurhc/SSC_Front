import currentVar from '../presetVar'

export default function (props,json) {
    // "702001GDFL-0005": "发布元数据",
    // "7020-0004": "修改",button_secondary
    // "7020-0002": "取消",
    // "7020-0005": "保存",
    // "702001GDFL-0026": "下一步",
    // "702001GDFL-0004": "新增",
    // "702001GDFL-0017": "删除",
    // "702001GDFL-0028": "上一步"
    // "702001GDFL-0006": "发布小应用",
    // "702001GDFL-0027": "X",
    // "702001GDFL-0023": "菜单编码",
    // "702001GDFL-0024": "菜单名称",
    // "702001GDFL-0025": "应用名称",
        return ({
            button:[
                {
                    "type": "button_main",
                    "key": currentVar.pageHeadButtons.Send,
                    "title": json['702001GDFL-0005'],
                    "area": currentVar.pageHeadButtonArea,
                },
                {
                    "type": "button_secondary",
                    "key": currentVar.pageHeadButtons.Edit,
                    "title": json['7020-0004'],
                    "area": currentVar.pageHeadButtonArea,
                },
                {
                    "type": "button_main",
                    "key": currentVar.pageHeadButtons.Save,
                    "title": json['7020-0005'],
                    "area": currentVar.pageHeadButtonArea,
                },
                {
                    "key": currentVar.pageHeadButtons.Cancel,
                    "title": json['7020-0002'],
                    "area": currentVar.pageHeadButtonArea,
                },
                {
                    "type": "button_main",
                    "key": currentVar.pageHeadButtons.Next,
                    "title": json['702001GDFL-0026'],
                    "area": currentVar.pageHeadButtonArea,
                },
                {
                    "type": "button_main",
                    "key": currentVar.pageBody1Buttons.Add,
                    "title": json['702001GDFL-0004'],
                    "area": currentVar.pageBody1ButtonArea,
                },
                {
                    "type": "button_secondary",
                    "key": currentVar.pageBody1Buttons.Edit,
                    "title": json['7020-0004'],
                    "area": currentVar.pageBody1ButtonArea,
                },
                {
                    "type": "button_main",
                    "key": currentVar.pageBody1Buttons.Save,
                    "title": json['7020-0005'],
                    "area": currentVar.pageBody1ButtonArea,
                },
                {
                    "key": currentVar.pageBody1Buttons.Cancel,
                    "title": json['7020-0002'],
                    "area": currentVar.pageBody1ButtonArea,
                },
                {
                    "type": "button_main",
                    "key": currentVar.pageBody2Buttons.Add,
                    "title": json['702001GDFL-0004'],
                    "area": currentVar.pageBody2ButtonArea,
                },
                {
                    "type": "button_secondary",
                    "key": currentVar.pageBody2Buttons.Edit,
                    "title": json['7020-0004'],
                    "area": currentVar.pageBody2ButtonArea,
                },
                {
                    "type": "button_main",
                    "key": currentVar.pageBody2Buttons.Save,
                    "title": json['7020-0005'],
                    "area": currentVar.pageBody2ButtonArea,
                },
                {
                    "key": currentVar.pageBody2Buttons.Cancel,
                    "title": json['7020-0002'],
                    "area": currentVar.pageBody2ButtonArea,
                },
                {
                    "type": "button_main",
                    "key": currentVar.listBody1Buttons.Delete,
                    "title": json['702001GDFL-0017'],
                    "area": currentVar.listBody1ButtonArea,
                },
                {
                    "type": "button_main",
                    "key": currentVar.listBody2Buttons.Delete,
                    "title": json['702001GDFL-0017'],
                    "area": currentVar.listBody2ButtonArea,
                },

                {
                    "type": "button_main",
                    "key": currentVar.page2Buttons.Send,
                    "title": json['702001GDFL-0005'],
                    "area": currentVar.page2ButtonArea,
                },
                {
                    "type": "button_secondary",
                    "key": currentVar.page2Buttons.Edit,
                    "title": json['7020-0004'],
                    "area": currentVar.page2ButtonArea,
                },
                {
                    "type": "button_secondary",
                    "key": currentVar.page3Buttons.Back,
                    "title": json['702001GDFL-0028'],
                    "area": currentVar.page3ButtonArea,
                },
                {
                    "type": "button_main",
                    "key": currentVar.page3Buttons.Send,
                    "title": json['702001GDFL-0006'],
                    "area": currentVar.page3ButtonArea,
                },
                {

                    "key": currentVar.pagestep,
                    "title": json['702001GDFL-0027'],
                    "area": currentVar.pagesteparea,
                },
                // {
                //     "type": "button_main",
                //     "key": currentVar.page3Buttons.Edit,
                //     "title": "重新发布",
                //     "area": currentVar.page3ButtonArea,
                // }
            ],
            appletForm:{
                moduletype: 'form',
                items: [
                    {
                        "itemtype": "input",
                        "visible": true,
                        "label": json['702001GDFL-0023'],
                        "attrcode": "menuItemCode",
                        "col": "4",
                        "maxlength":"46",
                    },
                    {
                        "itemtype": "input",
                        "visible": true,
                        "label": json['702001GDFL-0024'],
                        "attrcode": "menuItemName",
                        "col": "4",
                        "maxlength":"42",
                    },
                    {
                        "itemtype": "input",
                        "visible": true,
                        "label": json['702001GDFL-0025'],
                        "attrcode": "app_name",
                        "col": "4",
                        "maxlength":"42",
                    }
                ],
                status: 'edit',
            }
        })
}

