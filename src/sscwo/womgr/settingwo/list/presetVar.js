import 'ssccommon/components/globalPresetVar';

/**页面全局变量 */
let currentVar = {
    /**页面ID */
    pageId: '702001GDLX_list',
    cardPageId: '702001GDLX_card',
    /**列表区域ID */
    listArea: 'cpdoc',
    /**列表按钮区域ID */
    listButtonArea: 'listButton',
    /**页面按钮区域ID */
    pageButtonArea: 'pageButton',
    pageButtons:{
        /**删除 */
        Add: 'Add',
    },
    /**列表按钮 */
    listButtons: {
        /**删除 */
        Delete: 'Delete',
        /**设值  */
        Set: 'Set',
        See:'See'
    },
}
window.presetVar = {
    ...window.presetVar,
    ...currentVar
};
export default currentVar