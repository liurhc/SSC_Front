import requestApi from '../requestApi'
import presetVar from '../presetVar'
import {query} from './index'

let onButtonClick = function(btnKey, record, index){
    let that = this;
    switch(btnKey){
        case presetVar.listButtons.Delete:
            requestApi.del({
                data:{pk_doc: record.pk_doc.value},
                success: ()=>{query.queryData.call(that)}
            })
        break;
        case presetVar.listButtons.Set:
            this.props.linkTo('/sscwo/womgr/settingwo/card/index.html', {
                status: window.presetVar.status.browse,
                pk_doc: record.pk_doc.value,
                pk_doc_subtable: record.pk_doc_subtable.value,
                page: 2,
                pagecode: presetVar.cardPageId
            })
        break;
        case presetVar.listButtons.See:
            this.props.linkTo('/sscwo/womgr/settingwo/card/index.html', {
                status: window.presetVar.status.browse,
                pk_doc: record.pk_doc.value,
                page: 2,
                pagecode: presetVar.cardPageId
            })
            break;
    }
}
export default {onButtonClick}