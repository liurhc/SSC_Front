import currentVar from '../presetVar'

export default function (props,json) {
    // "702001GDFL-0004": "新增",
    // "702001GDFL-0017": "删除",
    // "702001GDFL-0029": "设置"
    // "702001GDFL-0030": "查看"
    return ({
        button:[
            {
                "type": "button_main",
                "key": currentVar.pageButtons.Add,
                "title": json['702001GDFL-0004'],
                "area": currentVar.pageButtonArea,
            },
            {
                "type": "button_main",
                "key": currentVar.listButtons.Delete,
                "title": json['702001GDFL-0017'],
                "area": currentVar.listButtonArea,
            },
            {
                "type": "button_main",
                "key": currentVar.listButtons.Set,
                "title": json['702001GDFL-0029'],
                "area": currentVar.listButtonArea,
            },
            {
                "type": "button_main",
                "key": currentVar.listButtons.See,
                "title": json['702001GDFL-0030'],
                "area": currentVar.listButtonArea,
            }
        ]
    })
}