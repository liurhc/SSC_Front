import currentVar from '../presetVar';
import requestApi from '../requestApi';
import { debug } from 'util';
let queryData = function(){
    requestApi.query({
        data: {pk_doctype: this.queryKey.category},
        success: (data) => {
            if(data == null){
                this.props.table.setAllTableData(currentVar.listArea, { rows: [] });
            }else{
                this.props.table.setAllTableData(currentVar.listArea, data[currentVar.listArea]);
            }
        }
    })
}
let onCategoryRefChange = function(e){
    this.setState({category: e});
    
    if((e || {}).refpk != null){
        this.queryKey.category=e.refpk;
    }else{
        this.queryKey.category=undefined;
    }
    queryData.call(this);
}
export default {queryData, onCategoryRefChange}