import {setTableExtendCol} from 'ssccommon/components/profile';
import fixedTemplet from './fixedTemplet';
import currentVar from '../presetVar';
import {query, listButtonClick} from './index';
import {getMultiLang} from 'nc-lightapp-front'

export default function (props) {
    let that = this;
    let createUIDomPromise = new Promise((resolve, reject) => {
        props.createUIDom({
            pagecode: currentVar.pageId,//页面id
            appid: ' '//注册按钮的id
        }, (data) => {
            resolve(data);
        })
    });

    let getMultiLangPromise = new Promise((resolve, reject) => {
        getMultiLang({
            moduleId: 7020, domainName: 'sscwo',currentLocale: 'simpchn',
            callback: (json) => {
                resolve(json);
            }
        })
    });
    Promise.all([createUIDomPromise, getMultiLangPromise]).then((resultList) => {
        let data = resultList[0];
        let json = resultList[1];
		
        let fixedTempletValue = fixedTemplet(props, json);
        //设置区域模板createButtonApp
        props.meta.setMeta(data.template);
        //获取表格的数据
        query.queryData.call(that);
        //设置按钮模板
        props.button.setButtons(fixedTempletValue.button);
        props.button.setPopContent(currentVar.listButtons.Delete, json['702001GDFL-0032']);

        let getSendRenDer = (text, record, index)=>{
            // "702001GDFL-0013": "发布成功",
            // "702001GDFL-0031": "未发布"
            if(((record || {}).ispubapplet || {}).value == true){
                return (
                    <span><span className="list-send"></span>{json['702001GDFL-0013']}</span>
                )
            }else if(((record || {}).ispubapplet || {}).value == false){
                return (
                    <span><span className="list-not-send"></span>{json['702001GDFL-0031']}</span>
                )
            }
        }
        let getSendRenDer1 = (text, record, index)=>{
            if(((record || {}).ispubmetadata || {}).value == true){
                return (
                    <span><span className="list-send"></span>{json['702001GDFL-0013']}</span>
                )
            }else if(((record || {}).ispubmetadata || {}).value == false){
                return (
                    <span><span className="list-not-send"></span>{json['702001GDFL-0031']}</span>
                )
            }
        }
        props.table.setTableRender(currentVar.listArea, "ispubapplet", getSendRenDer);
        props.table.setTableRender(currentVar.listArea, "ispubmetadata", getSendRenDer1);
        // props.button.setPopContent(currentVar.listButtons.Delete,'确认要删除吗？');

        //设置表格的扩展按钮列
        setTableExtendCol.call(that, props, data.template, [{
            areaId: currentVar.listArea,
            btnAreaId: currentVar.listButtonArea,
            //btnKeys: [currentVar.listButtons.Delete, currentVar.listButtons.Set],
            buttonVisible: (record, index) => {
                if (record.ispubapplet.value==true&&record.ispubmetadata.value==true) {
                    return  [currentVar.listButtons.Delete, currentVar.listButtons.See]

                } else {
                    return  [currentVar.listButtons.Delete, currentVar.listButtons.Set]

                }

            },
            onButtonClick: (props, btnKey, record, index)=>{
                listButtonClick.onButtonClick.call(that, props, btnKey, record, index);
            }
        }]);
		})
}
