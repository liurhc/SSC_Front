import React, {Component} from 'react';
import {createPage} from 'nc-lightapp-front';
import {
    ProfileStyle,
    ProfileHead,
    ProfileBody,
    ButtonGroup,
    HeadCenterCustom,
    EmptyArea
} from 'ssccommon/components/profile';
import currentVar from './presetVar'
import {query, initTemplate, buttonClick} from './events/index';
import CategoryRef from '../../../refer/category/cpdoctype'


import './index.less';

class SettingwoList extends Component {

    constructor(props) {
        super();
        this.state = {
            category:{} // 工单分类
        }
        this.queryKey = {
            category:undefined // 工单分类
        }
        initTemplate.call(this, props); 
    }
    componentDidMount() {

    }
    render() {
        const {table} = this.props;
        const {createSimpleTable} = table;
        let multiLang = this.props.MutiInit.getIntl(7020); //this.moduleId
        return (
            <div id="settingwoList">
                {/* 档案风格布局 */}
                <ProfileStyle
                    layout="singleTable"
                    {...this.props}
                >
                    {/*页面头*/}
                    {/*"702001GDFL-0014": "工单单据类型管理",*/}
                    <ProfileHead
                        title={multiLang && multiLang.get('702001GDFL-0014')}
                    >
                        <HeadCenterCustom>
                            <div className={'category'}>
                            {CategoryRef({
                                fieldid:'cpdoctype',
                                onChange: (e)=>{query.onCategoryRefChange.call(this, e)},
                                value:this.state.category
                            })}
                            </div>
                        </HeadCenterCustom>
                        <ButtonGroup
                            area={currentVar.pageButtonArea}
                            buttonEvent={
                                {
                                    Add: {click:()=>{buttonClick.buttonClick.call(this, currentVar.pageButtons.Add)}}
                                }
                            }
                        />
                    </ProfileHead>
                    {/*页面体*/}
                    <ProfileBody>
                        <EmptyArea>
                            {createSimpleTable(currentVar.listArea, {
                                showCheck:false,
                                showIndex:true
                            })}
                        </EmptyArea>
                    </ProfileBody>
                </ProfileStyle>
            </div>
        )
    }
}
SettingwoList = createPage({
    mutiLangCode: '7020'
})(SettingwoList);
export default SettingwoList;