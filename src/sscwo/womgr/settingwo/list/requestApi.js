import requestApi from "ssccommon/components/requestApi";

import {ajax } from 'nc-lightapp-front';

let requestDomain =  '';

let requestApiOverwrite = {
    ...requestApi,
    //单据模板接口
    tpl: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/sscwo/cpdoc/gettemplate.do`,
            data: opt.data,
            success: opt.success
        });
    },
    query:(opt) => {
        ajax({
            url: `${requestDomain}/nccloud/sscwo/cpdoc/list.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    add:(opt) => {
        opt.data = {
            ... opt.data,
            userjson:"{'pagecode':"+window.pagecode+"}"
        }
        ajax({
            url: `${requestDomain}/nccloud/erm/billcontrast/BillContrastAddAction.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    save: (opt) => {
        opt.data.rows.forEach((row) => {
            delete row.values.ts;
        });
        opt.data = {
            head: opt.data,
        }
        ajax({
            url: `${requestDomain}/nccloud/sscwo/cpdoctype/save.do`,
            data: opt.data,
            success: (data) => {
                if (data.success) {
                    data = data.data;
                    opt.success(data);
                }
            }
        })
    },
    del: (opt) => {
        ajax({
            url: `${requestDomain}/nccloud/sscwo/cpdoc/del.do`,
            data: opt.data,
            success: (data) => {
                if (data.success) {
                    data = data.data;
                    opt.success(data);
                }
            }
        })
    }
}

export default  requestApiOverwrite;
