import { Component } from "react";
import { createPage, toast, base, getMultiLang, promptBox, createPageIcon } from "nc-lightapp-front";
import "./index.less";
import { initTemplate, requestApi } from "./events";
import echarts from "echarts";
const { NCBackBtn, NCAffix } = base;

let ASSESS = {
  // POOR: '差',
  // MEDIUM: '中',
  // EXCELLENT: '优',
  // GOOD: '良'
}

let SERIUS_LEVEL = {
  // QUALIFIED: '',
  // MORESLIGHT: '很轻微',
  // SLIGHT: '轻微',
  // GENERAL: '一般',
  // SERIOUS: '严重',
  // MORESERIOUS: '非常严重'
}

let echartsOptions = {}

// reportStatu add edit view

class QualityTaskCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      reportId: "",
      multiLang: {},
      showDelModal: false,
      showBackBtn: true,
      showCancelModel: false
    };
    initTemplate.call(this, props);
  }
  componentDidMount() {
    getMultiLang({
      moduleId: 7040,
      domainName: 'sscqm',
      currentLocale: 'zh-CN',
      callback: (multiLang) => { this.setState({multiLang}, () => {
        this.makeMultiLangEchartsptions()

        const {props: {
          getUrlParam, getSearchParam
        }} = this
        const reportStatu = getUrlParam('reportStatu') || getSearchParam('reportStatu')
        const reportId = getUrlParam('reportId') || getSearchParam('reportId')

        if (reportStatu === 'add') { //生成 add
          const {userId} = window.parent.GETBUSINESSINFO()
          requestApi.generateReport({
            taskid: reportId,
            userId,
            success: (data) => {
              // console.log('generateReport: ', data)
              this.setState({reportIdToCancelSave: data.data.extendAttribute1.extendAttribute1.rows[0].values.reportid.value})
              this.eChartsOptionSet(data);
              this.generateReport(data);
            }
          });
        } else if (reportStatu === 'edit') { // 修改 edit
          requestApi.getReportInfo({
            id: reportId,
            success: data => {
              this.setState({reportIdToCancelSave: data.data.extendAttribute1.extendAttribute1.rows[0].values.reportid.value})
              this.eChartsOptionSet(data);
              this.editReport(data);
            }
          });
        } else { // 预览 view
          requestApi.getReportInfo({
            id: reportId,
            success: data => {
              this.setState({reportIdToCancelSave: data.data.extendAttribute1.extendAttribute1.rows[0].values.reportid.value})
              this.eChartsOptionSet(data);
              this.viewReport(data);
            }
          });
        }

        ASSESS = {
          POOR: multiLang['7040-0145'],
          MEDIUM: multiLang['7040-0144'],
          EXCELLENT: multiLang['7040-0142'],
          GOOD: multiLang['7040-0143']
        }

        SERIUS_LEVEL = {
          QUALIFIED: '',
          MORESLIGHT: multiLang['7040-0148'],
          SLIGHT: multiLang['7040-0149'],
          GENERAL: multiLang['7040-0150'],
          SERIOUS: multiLang['7040-0151'],
          MORESERIOUS: multiLang['7040-0152']
        }

      }) }
    })
    // 暂时强制隐藏返回按钮
    this.props.button.setButtonVisible({close: false});

    window.onbeforeunload = () => {
      if (this.props.form.getFormStatus('extendAttribute1') !== 'browse') return ''
    }
  }
  // 处理echarts配置 多语
  makeMultiLangEchartsptions = () => {
    const {multiLang} = this.state

    echartsOptions = {
      "option1": {
        "title": {
          "text": multiLang['7040-0180'],
          "x": "center"
        },
        "tooltip": {
          "trigger": "item",
          "formatter": "{a} <br/>{b} : {c} ({d}%)"
        },
        "legend": {
          "orient": "vertical",
          "x": "left",
          "data": [multiLang['7040-0181'], multiLang['7040-0182']],
          "icon": "circle"
        },
        "toolbox": {
          "show": false
        },
        "calculable": true,
        "series": [
          {
            "name": multiLang['7040-0183'],
            "type": "pie",
            "radius": "55%",
            "center": ["50%", "60%"],
            "data": [
              { "value": 200, "name": multiLang['7040-0181'] },
              { "value": 1000, "name": multiLang['7040-0182'] }
            ],
            "itemStyle": {
              "normal": {
                "color": param => {
                  if(param.name === multiLang['7040-0181']) {
                    return '#3fa1ff'
                  } else {
                    return '#646464'
                  }
                }
              }
            }
          }
        ]
      },
      "option2": {
        "title": {
          "text": multiLang['7040-0184'],
          "x": "center"
        },
        "tooltip": {
          "trigger": "item",
          "formatter": "{a} <br/>{b} : {c} ({d}%)"
        },
        "legend": {
          "icon": "circle",
          "orient": "vertical",
          "x": "left",
          "data": [multiLang['7040-0131'], multiLang['7040-0132'], multiLang['7040-0133'], multiLang['7040-0134'], multiLang['7040-0135']]
        },
        "toolbox": {
          "show": false
        },
        "calculable": true,
        "series": [
          {
            "name": multiLang['7040-0184'],
            "type": "pie",
            "radius": "55%",
            "center": ["50%", "60%"],
            "data": [
              { "value": 0, "name": multiLang['7040-0131'] },
              { "value": 0, "name": multiLang['7040-0132'] },
              { "value": 0, "name": multiLang['7040-0133'] },
              { "value": 0, "name": multiLang['7040-0134'] },
              { "value": 0, "name": multiLang['7040-0135'] }
            ],
            "itemStyle": {
              "normal": {
                "color": param => {
                  if(param.name === multiLang['7040-0131']) {
                    return '#f2637b'
                  } else if (param.name === multiLang['7040-0132']) {
                    return '#ff7800'
                  } else if (param.name === multiLang['7040-0133']) {
                    return '#fbd444'
                  } else if (param.name === multiLang['7040-0134']) {
                    return '#9860e5'
                  } else {
                    return '#59bb76'
                  }
                }
              }
            }
          }
        ]
      },
      "option3": {
        "color": ['#59bb76'],
        "title": {
          "text": multiLang['7040-0185'],
          "x": "center"
        },
        "tooltip": {
          "trigger": "axis"
        },
        "legend": {
          "icon": "circle",
          "data": [
            multiLang['7040-0186']
          ],
          "x": "left"
        },
        "toolbox": {
          "show": false
        },
        "calculable": true,
        "xAxis": [
          {
            "type": "category",
            "data": [
              multiLang['7040-0141']
            ]
          }
        ],
        "yAxis": [
          {
            "type": "value"
          }
        ],
        "series": [
          {
            "name": multiLang['7040-0186'],
            "type": "bar",
            "data": [/* 90, 80, 60, 0 */],
            "barWidth": 60,
            "markLine" : {
              "data" : [
                {"yAxis": 0, "name":multiLang['7040-0145'], "label": {
                  formatter(params){return params.data.name}
                }},
                {"yAxis": 60, "name":multiLang['7040-0144'], "label": {
                  formatter(params){return params.data.name}
                }},
                {"yAxis": 80, "name":multiLang['7040-0143'], "label": {
                  formatter(params){return params.data.name}
                }},
                {"yAxis": 90, "name":multiLang['7040-0142'], "label": {
                  formatter(params){return params.data.name}
                }}
              ]
            },
            "label": {
              "normal": {
                "show": true,
                "position": 'top'
              }
            },
          }
        ]
      }
    }
  }
  // 转义
  reportdis = data => {
    const {multiLang} = this.state

    const formData = data.data.extendAttribute1.extendAttribute1.rows[0].values
    formData.assess.display = ASSESS[formData.assess.value]
    formData.report_status.display = formData.report_status.value === 'UNCHECK' ? multiLang['7040-0146'] : multiLang['7040-0147']

    const listData = data.data.qmQualityTaskTpl1.qmQualityTaskTpl1.rows
    let index = -1
    while (++index < listData.length) {
      listData[index].values.serious_level.display = SERIUS_LEVEL[listData[index].values.serious_level.value]
    }
  };
  // 预览报告
  viewReport = data => {
    const statu = data.data.extendAttribute1.extendAttribute1.rows[0].values.report_status.value

    this.props.button.setButtonVisible({
      save: false,
      close: false,
      cancel: false,
      edit: statu !== "CHECKED" ? true : false,
      del: statu !== "CHECKED" ? true : false,
      CancelExamine: statu === "CHECKED" ? true : false,
      Examine: statu === "UNCHECK" ? true : false,
      docbrowsing: true
    });

    const {getUrlParam, getSearchParam} = this.props
    this.setState({
      reportId: getUrlParam('reportId') || getSearchParam('reportId'),
      showBackBtn:true
    });

    this.props.form.setFormStatus('extendAttribute1', 'browse')

    this.reportdis(data);

    const {extendAttribute1, qmQualityTaskTpl1} = data.data
    this.props.form.setAllFormValue({
      extendAttribute1: {
        areacode: "extendAttribute1",
        rows: extendAttribute1.extendAttribute1.rows
      }
    });
    this.props.table.setAllTableData("qmQualityTaskTpl1", {
      areacode: "qmQualityTaskTpl1",
      rows: qmQualityTaskTpl1.qmQualityTaskTpl1.rows
    });
  };
  // 修改报告
  editReport = data => {
    this.props.button.setButtonVisible(["docbrowsing", "edit", "del", "Examine", "CancelExamine"], false);

    const {getUrlParam, getSearchParam} = this.props
    this.setState({
      reportId: getUrlParam('reportId') || getSearchParam('reportId')
    });

    this.reportdis(data);

    const {extendAttribute1, qmQualityTaskTpl1} = data.data
    this.props.form.setAllFormValue({
      extendAttribute1: {
        areacode: "extendAttribute1",
        rows: extendAttribute1.extendAttribute1.rows
      }
    });
    this.props.table.setAllTableData("qmQualityTaskTpl1", {
      areacode: "qmQualityTaskTpl1",
      rows: qmQualityTaskTpl1.qmQualityTaskTpl1.rows
    });

    this.props.form.setFormStatus("extendAttribute1", "edit");
    this.setState({showBackBtn: false})
  };
  //跳转到list
  pagetransition = (fromDel = false) => {
    this.props.linkTo(
      '/sscqm/qualityReport/qualityReport/list/index.html',
      {
        appcode: '70400106',
        pagecode: '70400106_list',
        fromDel: fromDel ? 'Y' : 'N'
      }
    )
  };
  //保存按钮事件
  saveButtonClick = (props, key) => {
    const {multiLang} = this.state

    let param = props.form.getAllFormValue("extendAttribute1");
    let tableparam = props.table.getAllTableData("qmQualityTaskTpl1");
    requestApi.reportSave({
      param,
      tableparam,
      success: ({data, success}) => {
        if (success) {
          const statu = data.extendAttribute1.extendAttribute1.rows[0].values.report_status.value

          props.form.setFormStatus("extendAttribute1", "browse");
          this.setState({showBackBtn: true})
          toast({
            duration: 3,
            color: "success",
            title: multiLang['7040-0153']
          });

          this.props.button.setButtonVisible({
            save: false,
            cancel: false,
            edit: statu !== "CHECKED" ? true : false,
            del: statu !== "CHECKED" ? true : false,
            CancelExamine: statu === "CHECKED" ? true : false,
            Examine: statu === "UNCHECK" ? true : false,
            docbrowsing: true
          });

          const formData = data.extendAttribute1.extendAttribute1.rows[0].values
          formData.assess.display = ASSESS[formData.assess.value]
          formData.report_status.display = formData.report_status.value === 'UNCHECK' ? multiLang['7040-0146'] : multiLang['7040-0147']

          const listData = data.qmQualityTaskTpl1.qmQualityTaskTpl1.rows
          let index = -1
          while (++index < listData.length) {
            listData[index].values.serious_level.display = SERIUS_LEVEL[listData[index].values.serious_level.value]
          }

          this.props.form.setAllFormValue({extendAttribute1: { rows: data.extendAttribute1.extendAttribute1.rows}})

          this.props.table.setAllTableData('qmQualityTaskTpl1', { rows: data.qmQualityTaskTpl1.qmQualityTaskTpl1.rows})

          const {props: {
            getUrlParam, getSearchParam
          }} = this
          const reportStatu = getUrlParam('reportStatu') || getSearchParam('reportStatu')
          if (reportStatu === 'add') { // 生成报告
            const values = data.extendAttribute1.extendAttribute1.rows[0]
            sessionStorage.setItem('qualityReportAdded', JSON.stringify(values))
          }
        }
      }
    });
  };
  delButtonClick = (props, key) => {
    let reportvalue = props.form.getFormItemsValue(
      "extendAttribute1",
      "report_status"
    ).value;

    const {multiLang} = this.state

    if (reportvalue !== "UNCHECK") {
      toast({
        color: "warning",
        duration: 3,
        title: multiLang['7040-0154']
      });
    } else {
      requestApi.delreport({
        id: props.form.getFormItemsValue(
          "extendAttribute1",
          "reportid"
        ).value,
        success: ({success}) => {
          if (success) {
            const {props: {
              getUrlParam, getSearchParam
            }} = this
            if (getUrlParam('reportStatu') === 'add' || getSearchParam('reportStatu') === 'add') {
              sessionStorage.setItem('qualityReportAdded', '')
            }
            this.pagetransition(true)
          }
        }
      });
    }
  };
  editButtonClick = (props, key) => {
    let reportvalue = props.form.getFormItemsValue(
      "extendAttribute1",
      "report_status"
    ).value;

    const {multiLang} = this.state

    if (reportvalue == multiLang['7040-0147']) {
      toast({
        color: "warning",
        title: multiLang['7040-0155']
      });
    } else {
      props.form.setFormStatus("extendAttribute1", "edit");
      this.setState({showBackBtn: false})

      props.button.setButtonVisible({
        save: true,
        cancel: true,
        del: false,
        edit: false,
        Examine: false,
        CancelExamine: false,
        docbrowsing: false
      });
    }
  };
  // 图表
  eChartsOptionSet = ({data: { extendAttribute1, qmQualityTaskTpl1 }}) => {
    const {multiLang} = this.state
    // console.log('chartsData: ', extendAttribute1, qmQualityTaskTpl1)
    let formValues = extendAttribute1.extendAttribute1.rows[0].values
    //图一
    let chart1 = echarts.init(document.getElementById("task-chart1"));

    let actual_sample_quantity = formValues.actual_sample_quantity.value;
    let unSampleQuantity = formValues.un_sample_quantity.value;

    echartsOptions.option1.series[0].data[0].value = actual_sample_quantity;
    echartsOptions.option1.series[0].data[1].value = unSampleQuantity;
    chart1.setOption(echartsOptions.option1, true);

    //图二
    let chart2 = echarts.init(document.getElementById("task-chart2"));

    let more_serious_problem_quantity =
      formValues.more_serious_problem_quantity.value;
    let serious_problem_quantity = formValues.serious_problem_quantity.value;
    let general_problem_quantity = formValues.general_problem_quantity.value;
    let slight_problem_quantity = formValues.slight_problem_quantity.value;
    let more_slight_problem_quantity = formValues.more_slight_problem_quantity.value;
    if (
      more_serious_problem_quantity == 0 &&
      serious_problem_quantity == 0 &&
      general_problem_quantity == 0 &&
      slight_problem_quantity == 0 &&
      more_slight_problem_quantity == 0
    ) {
      console.warn(multiLang['7040-0140']);
    } else {
      echartsOptions.option2.series[0].data[0].value = more_serious_problem_quantity;
      echartsOptions.option2.series[0].data[1].value = serious_problem_quantity;
      echartsOptions.option2.series[0].data[2].value = general_problem_quantity;
      echartsOptions.option2.series[0].data[3].value = slight_problem_quantity;
      echartsOptions.option2.series[0].data[4].value = more_slight_problem_quantity;
      chart2.setOption(echartsOptions.option2, true);
    }

    //图三
    let chart3 = echarts.init(document.getElementById("task-chart3"));
    let score = formValues.score.value;
    //降序排列
    echartsOptions.option3.series[0].data.push(score);
    chart3.setOption(echartsOptions.option3, true);
  };
  // 生成报告
  generateReport = data => {
    this.props.button.setButtonVisible(["docbrowsing", "edit", "del", "Examine", "CancelExamine"], false);

    const {getUrlParam, getSearchParam} = this.props
    this.setState({
      reportId: getUrlParam('reportId') || getSearchParam('reportId')
    });

    this.reportdis(data);

    const {extendAttribute1, qmQualityTaskTpl1} = data.data

    this.props.form.setAllFormValue({
      extendAttribute1: {
        areacode: "extendAttribute1",
        rows: extendAttribute1.extendAttribute1.rows
      }
    });
    this.props.table.setAllTableData("qmQualityTaskTpl1", {
      areacode: "qmQualityTaskTpl1",
      rows: qmQualityTaskTpl1.qmQualityTaskTpl1.rows
    });

    this.props.form.setFormStatus("extendAttribute1", "edit");
    this.setState({showBackBtn: false})
  };
  // 表单编辑后事件
  detailViewAfterEvent = (props, formid, key, text, values, record) => {
    const {multiLang} = this.state

    if (key == "score") {
      // 评价赋值
      let { value } = text;
      if (value < 60) { // 差
        props.form.setFormItemsValue("extendAttribute1", {
          assess: { value: "POOR", display: multiLang['7040-0145'] }
        });
      } else if (value < 80) { // 中
        props.form.setFormItemsValue("extendAttribute1", {
          assess: { value: "MEDIUM", display: multiLang['7040-0144'] }
        });
      } else if (value < 90) { // 良
        props.form.setFormItemsValue("extendAttribute1", {
          assess: { value: "GOOD", display: multiLang['7040-0143'] }
        });
      } else if (value >= 90) { // 优
        props.form.setFormItemsValue("extendAttribute1", {
          assess: { value: "EXCELLENT", display: multiLang['7040-0142'] }
        });
      }
      let chart3 = echarts.init(document.getElementById("task-chart3"));
      //降序排列
      echartsOptions.option3.series[0].data[0] = value
      chart3.setOption(echartsOptions.option3, true);
    }
  };
  docbrowsingButtonClick = (props, key) => {
    let {sscorgid: {value, display}, task_name, taskid} = props.form.getAllFormValue("extendAttribute1").rows[0].values
    let taskReportInfo = {
      pkOrg: {value, display},
      qmTask: {
        value: taskid.value,
        display: task_name.value
      }
    };

    sessionStorage.setItem("taskReportInfo", JSON.stringify(taskReportInfo));
    this.props.openTo(
      '/sscqm/billsExamine/billsExamine/config/index.html',
      {
        appcode: '70400105',
        pagecode: '70400105_list'
      }
    )
  };
  // 取消按钮点击
  cancelButtonClick = () => {
    requestApi.getReportInfo({
      id: this.state.reportIdToCancelSave,
      success: data => {
        this.eChartsOptionSet(data);
        this.viewReport(data);
      }
    });
  }
  // 审核 / 取消审核
  examineFunc = examine => {
    const reportId = this.props.form.getFormItemsValue(
      "extendAttribute1",
      "reportid"
    ).value
    const {userId} = window.parent.GETBUSINESSINFO()
    requestApi.examineApi({
      id: reportId,
      statu: examine ? "UNCHECK" : "CHECKED",
      userId,
      success: response => {
        if (response.success) {
          this.reportdis(response);
          const {extendAttribute1, qmQualityTaskTpl1} = response.data
          this.props.form.setAllFormValue({
            extendAttribute1: {
              areacode: "extendAttribute1",
              rows: extendAttribute1.extendAttribute1.rows
            }
          });
          this.props.table.setAllTableData("qmQualityTaskTpl1", {
            areacode: "qmQualityTaskTpl1",
            rows: qmQualityTaskTpl1.qmQualityTaskTpl1.rows
          });

          this.props.button.setButtonVisible({
            edit: !examine,
            del: !examine,
            CancelExamine: examine,
            Examine: !examine
          });
        }
      }
    })
  }

  render() {
    const {
      form,
      button: { createButtonApp },
      table: { createSimpleTable }
    } = this.props;

    const { detailViewAfterEvent, pagetransition, delButtonClick, cancelButtonClick } = this;
    const { createForm } = form;

    const {multiLang, showDelModal, showBackBtn, showCancelModel} = this.state

    return (
      <div id="quality-task">

        <NCAffix offsetTop={0}>
          <div id="quality-task-head">
            <div className="clearfix quality-head--wrap">
              <NCBackBtn
                style={{
                  float: 'left',
                  marginTop: '14px',
                  display: showBackBtn ? 'block' : 'none'
                }}
                onClick={() => { pagetransition() }}
              ></NCBackBtn>
              {/* {!showBackBtn && createPageIcon()} */}
              { createPageIcon()}
              <h1>{multiLang['7040-0139']}</h1>
              <div className="btn-right">
                {createButtonApp({
                  area: "QualityTaskHead1",
                  onButtonClick: (props, key) => {
                    switch (key) {
                      case "save":
                        this.saveButtonClick(props, "save");
                        break;
                      case "del":
                        this.setState({ showDelModal: true })
                        break;
                      case "edit":
                        this.editButtonClick(props);
                        break;
                      case "docbrowsing":
                        this.docbrowsingButtonClick(props, "docbrowsing");
                        break;
                      case "cancel":
                        this.setState({showCancelModel: true})
                        break;
                      case "Examine":
                        this.examineFunc(true)
                        break;
                      case "CancelExamine":
                        this.examineFunc(false)
                        break;
                    }
                  }
                })}
              </div>
            </div>
          </div>
        </NCAffix>

        <div id="quality-task-body">
          {createForm("extendAttribute1", {
            expandArr: ["extendAttribute2", "extendAttribute3"],
            onAfterEvent: detailViewAfterEvent
          })}

          <p className="item-task-title">{multiLang['7040-0156']}</p>
          {createSimpleTable("qmQualityTaskTpl1", {
            showIndex: true
          })}

          <p className="item-task-title">{multiLang['7040-0157']}</p>
          <div className="echarts-task-wrap">
            {/* 实际抽样情况展示 */}
            <div className="item-echarts-task" id="task-chart1" />
            {/* 稽核情况统计 */}
            <div className="item-echarts-task" id="task-chart2" />
            {/* 稽核结果评价 */}
            <div className="item-echarts-task" id="task-chart3" />
          </div>
        </div>

        {showDelModal && promptBox({
          color: 'warning',
          title: multiLang && multiLang['7040-0025'],
          content: multiLang && multiLang['7040-0191'],
          noFooter: false,
          noCancelBtn: false,
          beSureBtnClick: () => {
            this.setState({showDelModal: false})
            delButtonClick(this.props)
          },
          cancelBtnClick: () => { this.setState({ showDelModal: false }); },
          closeByClickBackDrop: true
        })}

        {showCancelModel && promptBox({
          color: 'warning',
          title: multiLang && multiLang['7040-0103'],
          content: multiLang && multiLang['7040-0190'],
          noFooter: false,
          noCancelBtn: false,
          beSureBtnClick: () => {
            this.setState({showCancelModel: false})
            cancelButtonClick()
          },
          cancelBtnClick: () => { this.setState({ showCancelModel: false }); },
          closeByClickBackDrop: true
        })}

      </div>
    );
  }
}

// localStorage.setItem("gzip", "0");

QualityTaskCard = createPage({})(QualityTaskCard);
export default QualityTaskCard;
