import { ajax } from "nc-lightapp-front";
export default {
  getReportInfo: ({ id, success }) => {
    ajax({
      url: `/nccloud/sscqm/checkreport/CheckReportQueryByIdAction.do`,
      method: "post",
      data: {id},
      success
    });
  },
  reportSave: ({ param, tableparam, success }) => {
    ajax({
      url: `/nccloud/sscqm/checkreport/CheckReportSaveAction.do`,
      method: "post",
      success,
      data: {
        extendAttribute1: {
          extendAttribute1: { rows: param.rows }
        },
        qmQualityTaskTpl1: {
          qmQualityTaskTpl1: { rows: tableparam.rows }
        }
      }
    });
  },
  delreport: ({ id, success }) => {
    ajax({
      url: `/nccloud/sscqm/checkreport/CheckReportDeleteAction.do`,
      method: "post",
      success,
      data: {id}
    });
  },
  generateReport: ({taskid, userId, success}) => {
    ajax({
      url: `/nccloud/sscqm/checkreport/CheckReportGeneratingAction.do`,
      method: "post",
      success,
      data: {
        taskid, userId
      }
    });
  },
  // 审核 & 取消审核
  examineApi: ({success, id, userId, statu}) => {
    const url = `/nccloud/sscqm/checkreport/CheckReportCard${statu === 'UNCHECK' ? '' : 'Un'}ApproveAction.do`
    ajax({
      url,
      method: "post",
      data: {id, userid: userId},
      success
    });
  },
};
