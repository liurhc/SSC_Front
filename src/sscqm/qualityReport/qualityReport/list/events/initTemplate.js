import { base, getMultiLang } from 'nc-lightapp-front';
let { NCPopconfirm} = base;

export default function(props) {

  const _this = this

  props.createUIDom({}, function({ button, template }) {
    // console.log('button: ', button)
    // console.log('template: ', template)
    getMultiLang({moduleId: 7040, domainName: 'sscqm',currentLocale: 'zh-CN', callback: (multiLang) => {

      props.button.setButtons(button);
      props.meta.setMeta(template);
    
      template["QualityTaskList"].items.push({
        label: multiLang['7040-0104'],
        attrcode: "opr",
        itemtype: "customer",
        visible: true,
        width: "210px",
        fixed: "right",
        render: (node, values, idx) => {
          return (
            <div>

              {values.report_status.value !== "CHECKED" && (
                <a
                  className="billnoa"
                  onClick={() => {
                    _this.editButtonClick(values);
                  }}
                >{multiLang['7040-0105']}</a>
              )}

              {values.report_status.value !== "CHECKED" && (
                <NCPopconfirm  placement="top" content={multiLang['7040-0191']} onClose={() => {
                  _this.delButtonClick(values, idx);
                }}>
                  <a className="billnoa">{multiLang['7040-0106']}</a>
                </NCPopconfirm>
              )}

              {values.report_status.value === "CHECKED" && (
                <a
                  className="billnoa"
                  onClick={() => {
                    _this.cancelcheckButtonClick(values, idx);
                  }}
                >{multiLang['7040-0107']}</a>
              )}

              {values.report_status.value === "UNCHECK" && (
                <a
                  className="billnoa"
                  onClick={() => {
                    _this.checkedButtonClick(values, idx);
                  }}
                >{multiLang['7040-0108']}</a>
              )}

            </div>
          );
        }
      });

      props.table.setTableRender(
        "QualityTaskList",
        "report_code",
        (values, text, index) => (
          <a
            onClick={() => {
              _this.increaseTaskView(text)
            }}
            className="billnoa"
          >
            {text && text.report_code ? text.report_code.value : ""}
          </a>
        )
      );

      // 稽核评价
      props.table.setTableRender(
        "QualityTaskList",
        "assess",
        (values, text, index) => {
          return <span>{assessStatu(text.assess.value, multiLang)}</span>;
        }
      );
      // 任务状态
      props.table.setTableRender(
        "QualityTaskList",
        "report_status",
        (values, text, index) => (
          <span>
            {text && text.report_status
              ? text.report_status.value === "UNCHECK"
                ? multiLang['7040-0109']
                : multiLang['7040-0110']
              : ""}
          </span>
        )
      );
      // 任务属性
      props.table.setTableRender(
        "QualityTaskList",
        "task_property",
        (values, text, index) => (
          <span>
            {text && text.task_property
              ? text.task_property.value === "DAILY"
                ? multiLang['7040-0111']
                : multiLang['7040-0112']
              : ""}
          </span>
        )
      );

      props.form.setFormStatus('increaseTaskForm', 'edit')

    }})

  });
}

function assessStatu(statu, multiLang) {
  let text = "";
  if (statu === "EXCELLENT") {
    text = multiLang['7040-0113'];
  } else if (statu === "GOOD") {
    text = multiLang['7040-0114'];
  } else if (statu === "MEDIUM") {
    text = multiLang['7040-0115'];
  } else if (statu === "POOR") {
    text = multiLang['7040-0116'];
  }
  return text;
}
