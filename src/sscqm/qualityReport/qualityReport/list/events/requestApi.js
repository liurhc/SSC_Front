import { ajax } from "nc-lightapp-front";

export default {
  //查询按钮接口
  getrepoetlist: ({ data, success }) => {
    ajax({
      url: `/nccloud/sscqm/checkreport/CheckReportQueryListAction.do`,
      method: "post",
      data,
      success
    });
  },
  delrepoetlist: ({id, success}) => {
    ajax({
      url: `/nccloud/sscqm/checkreport/CheckReportDeleteAction.do`,
      method: "post",
      success,
      data: {id}
    });
  },
  checkedrepoetlist: ({success, id, userId, statu}) => {
    const url = `/nccloud/sscqm/checkreport/CheckReport${statu === 'UNCHECK' ? '' : 'Un'}ApproveAction.do`

    ajax({
      url,
      method: "post",
      data: {id, userid: userId},
      success
    });
  },
  checkBeforeGenerate: ({taskid, userId, success}) => {
    ajax({
      url: `/nccloud/sscqm/checkreport/CheckReportBeforeGenerateAction.do`,
      method: "post",
      success,
      data: {
        taskid, userId
      }
    });
  }
};
