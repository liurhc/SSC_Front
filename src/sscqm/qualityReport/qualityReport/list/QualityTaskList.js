import { Component } from "react";
import { createPage, base, toast, getMultiLang, createPageIcon } from "nc-lightapp-front";
const { NCModal, NCButton, NCAffix } = base;
import "./index.less";
import { initTemplate, requestApi } from "./events";
import headImg from '../../../public/image/djwh5.svg'

class QualityTaskList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refpk: "",
      pkOrg: "",
      // 查询区信息
      isidvalidated: "",
      isid: "",
      isidvalidatedName: "",
      isidName: "",

      showIncreaseModal: false,
      multiLang: {}
    };
    initTemplate.call(this, props);
  }
  componentDidMount() {
    getMultiLang({moduleId: 7040, domainName: 'sscqm',currentLocale: 'zh-CN', callback: (multiLang) => {
      this.setState({multiLang})

      const fromDel = this.props.getUrlParam('fromDel')
      if (fromDel && fromDel === 'Y') {
        toast({
          color: "success",
          title: multiLang['7040-0039']
        });
      }
    }})
  }
  //查询按钮点击事件
  searchClick() {
    const param = {
      taskid: "",
      orgid: "",
      startdate: "",
      enddate: "",
      checkstatus: ""
    };
    const searchAreaData = this.props.search.getAllSearchData(
      "extendAttribute"
    );
    // debugger;
    for (let item of searchAreaData.conditions) {
      if (item.field === "isid") {
        param.taskid = item.value.firstvalue;
      } else if (item.field === "isidvalidated") {
        param.orgid = item.value.firstvalue;
      } else if (item.field === "rangepicker") {
        param.startdate = item.value.firstvalue;
        param.enddate = item.value.secondvalue;
      } else if (item.field === "shzt") {
        param.checkstatus = item.value.firstvalue;
      }
    }
    requestApi.getrepoetlist({
      data: param,
      success: data => {
        this.getrepoetlistsuccess(data);
      }
    });
  }
  //获取list成功之后插入表格
  getrepoetlistsuccess({data: {QualityTaskList}}) {
    this.props.table.setAllTableData("QualityTaskList", QualityTaskList);

    const length = QualityTaskList.rows.length
    const {multiLang} = this.state
    if (length > 0) {
      toast({
        content: multiLang['7040-0192'] + "，" + multiLang['7040-0193'] + length + multiLang['7040-0194']
      });
    } else {
      toast({content: multiLang['7040-0195'],color:'warning'})
    }
  }
  //操作列刪除
  delButtonClick = (text, idx) => {
    const {multiLang} = this.state
    if (text.report_status.value == "CHECKED") {
      toast({
        color: "warning",
        title: multiLang['7040-0118']
      });
    } else {
      requestApi.delrepoetlist({
        id: text.reportid.value,
        success: ({success}) => {
          if (success) {
            toast({
              duration: 3,
              color: "success",
              title: multiLang['7040-0119']
            });
            this.props.table.deleteTableRowsByIndex('QualityTaskList', idx)
          }
        }
      });
    }
  };
  //审核
  checkedButtonClick = (text, idx) => {
    const {userId} = window.parent.GETBUSINESSINFO()
    const {multiLang} = this.state
    
    requestApi.checkedrepoetlist({
      id: text.reportid.value,
      statu: "UNCHECK",
      userId,
      success: ({data, success}) => {
        if (success) {
          toast({
            duration: 3,
            color: "success",
            content: multiLang['7040-0177']
          });

          const {values} = data.QualityTaskList.rows[0]
          for (let attr in values) {
            this.props.table.setValByKeyAndIndex(
              'QualityTaskList', 
              idx, 
              attr, 
              values[attr]
            )
          }
        }
      }
    });
  };
  //取消审核
  cancelcheckButtonClick = (text, idx) => {
    const {userId} = window.parent.GETBUSINESSINFO()
    const {multiLang} = this.state

    requestApi.checkedrepoetlist({
      userId,
      id: text.reportid.value,
      statu: "CHECKED",
      success: ({data, success}) => {
        if (success) {
          toast({
            duration: 3,
            color: "success",
            content: multiLang['7040-0178']
          });
          const {values} = data.QualityTaskList.rows[0]
          for (let attr in values) {
            this.props.table.setValByKeyAndIndex(
              'QualityTaskList', 
              idx, 
              attr, 
              values[attr]
            )
          }
        }
      }
    });
  };
  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.isidvalidated !== this.state.isidvalidated) {
      const {
        search: { setSearchValByField }
      } = this.props;
      if (nextState.isid)
        setSearchValByField("extendAttribute", "isid", {
          value: undefined,
          display: undefined
        });
    }
    return true;
  }
  increaseTask = () => {
    this.setState({ showIncreaseModal: true });
    this.props.form.setFormStatus('increaseTaskForm', 'edit')
  }
  //查询区编辑后事件
  afterEvent(code, node, areaId) {
    const { refpk, refname } = node;
    if (code === "isidvalidated") {
      this.setState({
        isidvalidated: refpk || "",
        isidvalidatedName: refname || ""
      });
    } else if (code === "isid") {
      this.setState({ isid: refpk || "", isidName: refname || "" });
    }

    if (code !== "isidvalidated") return;

    const { getMeta, setMeta } = this.props.meta;
    let meta = getMeta();
    meta[areaId].items.map(one => {
      if (one.attrcode == "isid") {
        one.queryCondition = () => {
          return {
            orgid: refpk,
            GridRefActionExt: 'nccloud.web.sscqm.sscbd.ref.sqlbuilder.CheckTaskRefSqlBuilder'
          };
        };
        one.disabled = refpk ? false : true;
      }
    });
    setMeta(meta);
  }
  // 生成 表单编辑后事件
  increaseFormAfterEvent = (events, areaId, code, node) => {
    if (code === "isidvalidated") { // 共享服务中心
      this.props.form.setFormItemsDisabled(areaId, {
        isid: node.refpk ? false : true
      });
      if (!node.refpk || node.refpk !== this.state.pkOrg) {
        this.props.form.setFormItemsValue(areaId, {
          isid: { value: null, display: null }
        });
      }
      this.setState({ pkOrg: node.refpk || "" });
    } else { // 稽核任务
      this.setState({ pkOrg: "" });
    }
    if (code !== "isidvalidated") return;

    const { getMeta, setMeta } = this.props.meta;
    let meta = getMeta();
    meta[areaId].items.map(one => {
      if (one.attrcode == "isid") {
        one.queryCondition = () => {
          return {
            orgid: node.refpk,
            GridRefActionExt: 'nccloud.web.sscqm.sscbd.ref.sqlbuilder.CheckTaskRefSqlBuilder'
          };
        };
      }
    });
    setMeta(meta);
  };
  //操作列修改事件
  editButtonClick = text => {
    const {multiLang} = this.state

    if (text.report_status.value == "CHECKED") {
      toast({
        color: "warning",
        title: multiLang['7040-0120']
      });
    } else {
      this.saveReportVal()
      this.props.linkTo(
        '/sscqm/qualityReport/qualityReport/card/index.html', 
        {
          appcode: '70400106',
          pagecode: '70400106_card',
          reportStatu: 'edit',
          reportId: text.reportid.value
        }
      )
    }
  };
  // 预览报告
  increaseTaskView = text => {
    this.saveReportVal()
    this.props.linkTo(
      '/sscqm/qualityReport/qualityReport/card/index.html', 
      {
        appcode: '70400106',
        pagecode: '70400106_card',
        reportStatu: 'view',
        reportId: text.reportid.value
      }
    )
  };
  // 缓存查询区数据
  saveReportVal = () => {
    sessionStorage.setItem("reportval", JSON.stringify(this.props.search.getAllSearchData('extendAttribute').conditions))
  }
  // 生成报告
  increaseTaskSave = () => {
    const {
      form: { getAllFormValue, isCheckNow }
    } = this.props;
    if (!isCheckNow("increaseTaskForm")) return;

    const {userId} = window.parent.GETBUSINESSINFO()
    const reportId = getAllFormValue("increaseTaskForm").rows[0].values.isid.value

    requestApi.checkBeforeGenerate({
      taskid: reportId, 
      userId,
      success: response => {

        // debugger
        this.saveReportVal()

        this.props.linkTo(
          '/sscqm/qualityReport/qualityReport/card/index.html', 
          {
            appcode: '70400106',
            pagecode: '70400106_card',
            reportStatu: 'add',
            reportId
          }
        )

      }
    })
  };
  // 查询区成功挂载钩子函数
  renderCompleteEvent = () => {
    let reportInfo = sessionStorage.getItem("reportval");
    if (reportInfo) {
      reportInfo = JSON.parse(reportInfo);
      const param = {
        taskid: "",
        orgid: "",
        startdate: "",
        enddate: "",
        checkstatus: ""
      };
      for (let item of reportInfo) {
        if (item.field === "isid") {
          param.taskid = item.value.firstvalue;
        } else if (item.field === "isidvalidated") {
          param.orgid = item.value.firstvalue;
        } else if (item.field === "rangepicker") {
          param.startdate = item.value.firstvalue;
          param.enddate = item.value.secondvalue;
        } else if (item.field === "shzt") {
          param.checkstatus = item.value.firstvalue;
        }
      }

      this.props.search.setDisabledByField(
        "extendAttribute",
        "isid",
        false
      );

      this.props.search.setSearchValue("extendAttribute", reportInfo);

      requestApi.getrepoetlist({
        data: param,
        success: data => {
          if (sessionStorage.getItem('qualityReportAdded')) {
            const record = JSON.parse(sessionStorage.getItem('qualityReportAdded'))
            const rows = data.data.QualityTaskList.rows
            sessionStorage.setItem('qualityReportAdded', '')
            
            const recordCode = record.values.report_code.value
            let index = -1, onoff = false
            while(++index < rows.length) {
              const eachCode = rows[index].values.report_code.value
              if (recordCode === eachCode) {
                onoff = true
                break
              }
            }
            if (!onoff) {
              rows.unshift(record)
            }
          }

          this.getrepoetlistsuccess(data);
        }
      });
    }
  }

  render() {
    const {
      button: { createButtonApp },
      search: { NCCreateSearch },
      table: { createSimpleTable },
      form: { createForm }
    } = this.props;

    const {
      increaseTask,
      increaseTaskSave,
      increaseFormAfterEvent,
      state: { showIncreaseModal, multiLang },
      renderCompleteEvent,
      increaseTaskView
    } = this;

    return (
      <div id="quality-task">
        <NCAffix offsetTop={0}>
          <div id="quality-task-head--wrap">
            <div className="clearfix quality-task-head">
              {createPageIcon()}
              <h1>{multiLang['7040-0117']}</h1>
              <div className="btn-right clearfix">
                {createButtonApp({
                  area: "QualityTaskHead2",
                  onButtonClick: increaseTask
                })}
              </div>
            </div>
            {NCCreateSearch("extendAttribute", {
              hideSearchCondition: false,
              hideBtnArea: false,
              showAdvBtn: false,
              clickSearchBtn: this.searchClick.bind(this),
              onAfterEvent: this.afterEvent.bind(this),
              renderCompleteEvent
            })}
          </div>
        </NCAffix>
        {/*表体*/}
        <div id="quality-task-body">
          {createSimpleTable("QualityTaskList", {
            showIndex: true,
            // showCheck: true
            onRowDoubleClick: (record, idx, props, event) => increaseTaskView(record)
          })}
        </div>
        {/* 生成 */}
        <NCModal show={showIncreaseModal}>
          <NCModal.Header>
            {" "}
            <NCModal.Title>{multiLang['7040-0121']}</NCModal.Title>{" "}
          </NCModal.Header>
          <NCModal.Body>
            {createForm("increaseTaskForm", {
              onAfterEvent: increaseFormAfterEvent
            })}
          </NCModal.Body>
          <NCModal.Footer>
            <NCButton onClick={increaseTaskSave} colors="primary">
              {multiLang['7040-0122']}
            </NCButton>
            <NCButton
              onClick={() => {
                this.setState({ showIncreaseModal: false });
              }}
            >{multiLang['7040-0123']}</NCButton>
          </NCModal.Footer>
        </NCModal>
      </div>
    );
  }
}

// localStorage.setItem("gzip", "0");
// localStorage.setItem("ShuntServerInfo", "{}");

QualityTaskList = createPage({
  mutiLangCode: '7040'
})(QualityTaskList);
export default QualityTaskList;
