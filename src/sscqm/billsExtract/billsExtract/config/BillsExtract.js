import { Component } from "react";
import { createPage, toast, base, getMultiLang, promptBox, createPageIcon } from "nc-lightapp-front";
import "./index.less";
import "./index.css";
const { NCAffix } = base;
import { initTemplate, requestApi } from "./events";
import headImg from '../../../public/image/djwh5.svg'
class BillsExtract extends Component {
  constructor(props) {
    super(props);
    this.state = {
      qmTask: "", // 稽核任务pk
      sampleAmount: "0", // 抽取xx张 
      sampler: "", // 抽取人 用于渲染
      confirmer: "", // 确认人 用于渲染
      userId: "", // 抽取人pk 用于缓存
      pkOrg: "", // 财务共享服务中心pk缓存 用于区别pk变更
      currentTaskStatu: '', // 当前选中任务的状态
      showCheckModal: false,
      multiLang: {}
    };
    initTemplate.call(this, props);
  }
  componentDidMount() {
    const {userId} = window.parent.GETBUSINESSINFO()
    this.setState({userId})

    getMultiLang({moduleId: 7040, domainName: 'sscqm',currentLocale: 'zh-CN', callback: (multiLang) => {
      this.setState({multiLang})
    }})
  }
  // 单据抽样
  billExtract = (sampling = false) => {
    const { qmTask, multiLang } = this.state;
    if (sampling === "sampling") {
      const {userId, currentTaskStatu} = this.state
      if (currentTaskStatu === 'SAMPLED') {
        this.setState({showCheckModal: true})
        return false
      }
      requestApi.checkbillsSampling({
        taskid: qmTask,
        userId,
        success: ({ data, success }) => {
          if (success) {
            this.props.table.setAllTableData("billsExtractList", {
              areacode: "billsExtractList",
              rows: data.billsExtractList.rows
            });
            this.checkBillsCount(qmTask)

            const length = data.billsExtractList.rows.length
            if (length > 0) {
              toast({
                content: multiLang['7040-0196'] + "，" + multiLang['7040-0193'] + length + multiLang['7040-0194']
              });
            } else {
              toast({content: multiLang['7040-0197'], color:'warning'})
            }
          }
        }
      });
    } else {
      const searchAreaData = this.props.search.getAllSearchData('searchArea')
      const param = {taskid: "",orgs: "",billType: "",checkno: ""}
      for (let item of searchAreaData.conditions) {
        if (item.field === 'qmTask') {
          param.taskid = item.value.firstvalue
        } else if (item.field === 'orgs') {
          param.orgs = item.value.firstvalue
        } else if (item.field === 'billType') {
          param.billType = item.value.firstvalue
        } else if (item.field === 'checkno') {
          param.checkno = item.value.firstvalue
        }
      }
      requestApi.checkbills({
        data: param,
        success: ({success, data}) => {
          if (success) {
            this.props.table.setAllTableData("billsExtractList", {
              areacode: "billsExtractList",
              rows: data.billsExtractList.rows
            });
            const length = data.billsExtractList.rows.length
            if (length > 0) {
              toast({
                content: multiLang['7040-0192'] + "，" + multiLang['7040-0193'] + length + multiLang['7040-0194']
              });
            } else {
              toast({content: multiLang['7040-0195'],color:'warning'})
            }
          }
        }
      });
    }
  };
  // 查询抽取数量 & 当前任务状态 & 抽取人 & 确认人
  checkBillsCount = (taskid) => {
    requestApi.checkBillsCount({
      taskid,
      success: ({ data }) => {
        const [sampleAmount, statu, sampler, confirmer] = data.split('|')
        this.setState({sampleAmount, sampler, confirmer})
        // 不同任务状态设置按钮状态
        const {setButtonDisabled, setButtonVisible} = this.props.button
        this.setState({currentTaskStatu: statu})
        // 抽取
        if (statu === 'ENABLED' || statu === 'SAMPLED') { // 可抽取
          setButtonDisabled('Extract', false)
        } else { // 不可抽取 
          setButtonDisabled('Extract', true)
        }
        // 确认 & 取消确认
        if (statu === 'SAMPLED') { // 已抽取 显示确认
          setButtonVisible('SureExtract', true)
          setButtonVisible('CancelSureExtract', false)
        } else if (statu === 'CONFIRMED') { // 已确认 显示取消确认
          setButtonVisible('SureExtract', false)
          setButtonVisible('CancelSureExtract', true)
        } else { // 其他状态隐藏确认&取消确认
          setButtonVisible('SureExtract', false)
          setButtonVisible('CancelSureExtract', false)
        }
      }
    })
  }
  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.pkOrg !== this.state.pkOrg) {
      const {search: {
        setSearchValByField
      }} = this.props
      setSearchValByField('searchArea', 'billType', {value: undefined,display: undefined})
      setSearchValByField('searchArea', 'orgs', {value: undefined,display: undefined})
      if (nextState.qmTask) setSearchValByField('searchArea', 'qmTask', {value: undefined,display: undefined})
    }
    return true
  }
  // 列表双击事件
  onRowDoubleClick = record => this.openBillHandler(record)
  // 查询区编辑后事件
  onAfterEvent = (code, values, areaId) => {
    if (code === "qmTask") { // 暂存稽核任务
      this.setState({
        qmTask: values.refpk || "",
        // sampler: values.values && values.values.sampler ? values.values.sampler.display : '',
        // confirmer: values.values && values.values.confirmer ? decodeURIComponent(values.values.confirmer.display) : ''
      });
      // 已抽取数量
      if (values.refpk) {
        this.checkBillsCount(values.refpk)
      } else {
        this.setState({sampleAmount: 0})
        // 清空任务时
        const {setButtonDisabled, setButtonVisible} = this.props.button
        setButtonDisabled('Extract', true)
        setButtonVisible('SureExtract', false)
        setButtonVisible('CancelSureExtract', false)
      }
    }
    if (code === "pkOrg") {
      this.setState({pkOrg: values.refpk})
    }
    if (code !== "pkOrg") return;
    const { getMeta, setMeta } = this.props.meta;
    let meta = getMeta();
    meta[areaId].items.map(one => {
      if (one.attrcode === "qmTask") {
        one.queryCondition = () => {
          return {
            orgid: values.refpk, 
            GridRefActionExt: 'nccloud.web.sscqm.sscbd.ref.sqlbuilder.CheckTaskRefSqlBuilder'
          };
        };
        one.disabled = values.refpk ? false : true;
      }
      if (one.attrcode === "billType") {
        one.queryCondition = () => {
          return {
            orgid: values.refpk,
            GridRefActionExt: 'nccloud.web.sscqm.sscbd.ref.sqlbuilder.BilltypeRefSqlBuilder'
          };
        };
        one.disabled = values.refpk ? false : true;
      }
      if (one.attrcode === "orgs") {
        one.queryCondition = () => {
          return {
            sscid: values.refpk,
            GridRefActionExt: 'nccloud.web.sscqm.sscbd.ref.sqlbuilder.OrgRefSqlBuilder'
          };
        };
        one.disabled = values.refpk ? false : true;
      }
    });
    setMeta(meta);
  };
  // open bill
  openBillHandler = ({trade_type_code, bill_type_code, pk_bill}) => {
    const param = {
      billtypeCode: bill_type_code.value,
      transtypeCode: trade_type_code.value,
      billid: pk_bill.value
    }

    requestApi.openBill({
      param,
      success: ({data: { url, data }, success}) => {
        // debugger
        if (success) {
          this.props.specialOpenTo(
            url,
            {
              ...data,
              scene: 'zycx'
            }
          )
        }
      }
    });
  }
  // 确认 & 取消确认
  billsConfirm = onOff => {
    const { qmTask, userId } = this.state;
    const {setButtonDisabled, setButtonVisible} = this.props.button
    if (onOff) { // 确认
      requestApi.billsConfirm({
        taskid: qmTask,
        userId,
        success: response => {
          if (response.success) {
            setButtonDisabled('Extract', true)
            setButtonVisible('SureExtract', false)
            setButtonVisible('CancelSureExtract', true)
            this.setState({confirmer: window.parent.GETBUSINESSINFO().userName})
          }
        }
      })
    } else { // 取消确认
      requestApi.billsUnConfirm({
        taskid: qmTask,
        userId,
        success: response => {
          if (response.success) {
            setButtonDisabled('Extract', false)
            setButtonVisible('SureExtract', true)
            setButtonVisible('CancelSureExtract', false)
            this.setState({confirmer: ''})
          }
        }
      })
    }
  }
  // 已抽取状态 继续抽取
  specialExtract = () => {
    const {userId, qmTask} = this.state
    requestApi.checkbillsSampling({
      taskid: qmTask,
      userId,
      success: ({ data, success }) => {
        if (success) {
          this.props.table.setAllTableData("billsExtractList", {
            areacode: "billsExtractList",
            rows: data.billsExtractList.rows
          });
          this.checkBillsCount(qmTask)

          this.setState({showCheckModal: false})

          const length = data.billsExtractList.rows.length
          const {multiLang} = this.state
          if (length > 0) {
            toast({
              content: multiLang['7040-0196'] + "，" + multiLang['7040-0193'] + length + multiLang['7040-0194']
            });
          } else {
            toast({content: multiLang['7040-0197'], color:'warning'})
          }
        }
      }
    });
  }

  render() {
    const {
      search: { NCCreateSearch },
      table: { createSimpleTable },
      button: { createButtonApp }
    } = this.props;

    const { onAfterEvent, billExtract, billsConfirm, onRowDoubleClick, specialExtract } = this;

    const {
      // qmTask, // 稽核任务pk
      sampleAmount, // 抽取xx张 
      sampler, // 抽取人
      confirmer, // 确认人
      showCheckModal,
      multiLang
    } = this.state

    return (
      <div id="bill-extract">
        <NCAffix offsetTop={0}>
          <div className="header">
            {createPageIcon()}
            <span className="qm-title">{multiLang['7040-0070']}</span>
            <div className="qm-btns">
              {createButtonApp({
                onButtonClick: (props, key) => {
                  if (key === "Extract") { // 抽取
                    billExtract("sampling");
                  } else if (key === "SureExtract") { // 确定
                    billsConfirm(true)
                  } else { // 取消确定
                    billsConfirm(false)
                  }
                },
                area: "BillsExtractHead"
              })}
            </div>
          </div>
          <div className="qm-search-area">
            {NCCreateSearch("searchArea", {
              hideSearchCondition: false,
              hideBtnArea: false,
              showAdvBtn: false,
              clickSearchBtn: billExtract,
              onAfterEvent
            })}
          </div>
        </NCAffix>
        <div id="bill-extract-body">
          {createSimpleTable("billsExtractList", {
            showIndex: true,
            onRowDoubleClick
          })}
        </div>
        <div id="bill-info-fixed">
          <div className="bill-info-con">
            <span>
              {multiLang['7040-0072']}&nbsp;
              {sampleAmount}{multiLang['7040-0073']}
            </span>
            <span>
              {multiLang['7040-0074']}&nbsp;
              {sampler}
              &nbsp;&nbsp;&nbsp;{multiLang['7040-0075']}&nbsp;
              {confirmer}
            </span>
          </div>
        </div>

        {showCheckModal && promptBox({
          color: 'warning',
          title: multiLang && multiLang['7040-0040'],
          content: multiLang && multiLang['7040-0176'],
          noFooter: false,
          noCancelBtn: false,
          beSureBtnClick: () => {
            this.setState({showCheckModal: false})
            specialExtract()
          },
          cancelBtnClick: () => { this.setState({ showCheckModal: false }); },
          closeByClickBackDrop: true
        })}

      </div>
    );
  }
}

// localStorage.setItem("gzip", "0");
// localStorage.setItem("ShuntServerInfo", "{}");

BillsExtract = createPage({
  mutiLangCode: '7040'
})(BillsExtract);
export default BillsExtract;
