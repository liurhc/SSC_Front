import { getMultiLang } from "nc-lightapp-front";

export default function(props) {
  const _this = this
  
  props.createUIDom({}, function({ button, template }) {
    // console.log('tpl: ', template)

    props.button.setButtons(button);
    props.button.setButtonDisabled('Extract', true)
    props.button.setButtonVisible('SureExtract', false)
    props.button.setButtonVisible('CancelSureExtract', false)

    props.meta.setMeta(template);

    props.table.setTableRender(
      "billsExtractList",
      "billno",
      (text, record, index) => {
        return (
          <a
            className="billnoa"
            onClick={() => {
              _this.openBillHandler(record)
            }}
          >
            {record.billno.value}
          </a>
        );
      }
    );

    getMultiLang({moduleId: 7040, domainName: 'sscqm',currentLocale: 'zh-CN', callback: (multiLang) => {
      props.table.setTableRender(
        "billsExtractList",
        "check_result",
        (values, text, index) => {
          return (
            <span>
              {(() => {
                if (text.check_result.value === "PASS") {
                  return multiLang['7040-0068'];
                } else if (text.check_result.value === "TEMP") {
                  return "";
                } else {
                  return multiLang['7040-0069'];
                }
              })()}
            </span>
          );
        }
      );
    }})

    
  });

}
