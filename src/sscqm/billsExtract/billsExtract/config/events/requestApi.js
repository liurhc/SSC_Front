import { ajax } from "nc-lightapp-front";

export default {
  // 取得任务单据
  checkbills: ({data, success}) => {
    ajax({
      url: `/nccloud/sscqm/checkbillsample/CheckBillSampleQueryListAction.do`,
      method: "post",
      success,
      data
    });
  },
  // 查询抽取数量 & 当前任务状态 & 抽取人 & 确认人
  checkBillsCount: ({taskid, success}) => {
    ajax({
      url: `/nccloud/sscqm/checkbillsample/CheckBillSampleGetCountByTask.do`,
      method: "post",
      data: {
        taskid
      },
      success
    });
  },
  // 单据抽样
  checkbillsSampling: ({taskid, userId, success}) => {
    ajax({
      url: `/nccloud/sscqm/checkbillsample/CheckBillSampleAction.do`,
      method: "post",
      data: {
        taskid, userId
      },
      success
    });
  },
  // 打开单据
  openBill: ({ success, param }) => {
    ajax({
      url: `/nccloud/sscrp/rpbill/BrowseBillAction.do`,
      method: "post",
      data: param,
      success
    });
  },
  // 确认
  billsConfirm: ({taskid, success, userId}) => {
    ajax({
      url: `/nccloud/sscqm/checktask/CheckTaskConfirmAction.do`,
      method: "post",
      data: {
        taskid, userId
      },
      success
    });
  },
  // 取消确认
  billsUnConfirm: ({taskid, success, userId}) => {
    ajax({
      url: `/nccloud/sscqm/checktask/CheckTaskUnConfirmAction.do`,
      method: "post",
      data: {
        taskid, userId
      },
      success
    });
  },
};
