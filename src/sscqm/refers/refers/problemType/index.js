!(function(e, r) {
  "object" == typeof exports && "object" == typeof module
    ? (module.exports = r(require("nc-lightapp-front")))
    : "function" == typeof define && define.amd
      ? define(["nc-lightapp-front"], r)
      : "object" == typeof exports
        ? (exports["sscqm/public/common/problemType/index"] = r(
            require("nc-lightapp-front")
          ))
        : (e["sscqm/public/common/problemType/index"] = r(
            e["nc-lightapp-front"]
          ));
})(window, function(e) {
  return (function(e) {
    var r = {};
    function t(n) {
      if (r[n]) return r[n].exports;
      var o = (r[n] = {
        i: n,
        l: !1,
        exports: {}
      });
      return e[n].call(o.exports, o, o.exports, t), (o.l = !0), o.exports;
    }
    return (
      (t.m = e),
      (t.c = r),
      (t.d = function(e, r, n) {
        t.o(e, r) ||
          Object.defineProperty(e, r, {
            configurable: !1,
            enumerable: !0,
            get: n
          });
      }),
      (t.r = function(e) {
        Object.defineProperty(e, "__esModule", {
          value: !0
        });
      }),
      (t.n = function(e) {
        var r =
          e && e.__esModule
            ? function() {
                return e.default;
              }
            : function() {
                return e;
              };
        return t.d(r, "a", r), r;
      }),
      (t.o = function(e, r) {
        return Object.prototype.hasOwnProperty.call(e, r);
      }),
      (t.p = "../../../../"),
      t((t.s = 629))
    );
  })({
    0: function(r, t) {
      r.exports = e;
    },
    256: function(e, r, t) {
      "use strict";
      Object.defineProperty(r, "__esModule", {
        value: !0
      }),
        (r.default = function() {
          var e =
            arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
          return React.createElement(
            n,
            Object.assign(
              {
                multiLang: {
                  moduleId: '7040',
                  domainName: 'sscqm',
                  currentLocale: 'simpchn'
                },
                refType: "tree",
                refName: "refer-0004",
                placeholder: "refer-0004",
                refCode: "",
                queryTreeUrl: "/nccloud/sscqm/checkproblemtype/CheckProblemTypeTreeRefAction.do",
                treeConfig: {
                  name: ["refer-0001", "refer-0002"],
                  code: ["refcode", "refname"]
                },
                rootNode: {
                  refname: "refer-0004",
                  refpk: "root"
                },
                isMultiSelectedEnabled: !1
              },
              e
            )
          );
        });
      var n = t(0).high.Refer;
    },
    629: function(e, r, t) {
      e.exports = t(256);
    }
  });
});
