!function(e, t) {
    "object" == typeof exports && "object" == typeof module ? module.exports = t(require("nc-lightapp-front")) : "function" == typeof define && define.amd ? define(["nc-lightapp-front"], t) : "object" == typeof exports ? exports["sscqm/refers/refers/task/index"] = t(require("nc-lightapp-front")) : e["sscqm/refers/refers/task/index"] = t(e["nc-lightapp-front"])
}(window, function(e) {
    return function(e) {
        var t = {};
        function n(r) {
            if (t[r])
                return t[r].exports;
            var o = t[r] = {
                i: r,
                l: !1,
                exports: {}
            };
            return e[r].call(o.exports, o, o.exports, n),
            o.l = !0,
            o.exports
        }
        return n.m = e,
        n.c = t,
        n.d = function(e, t, r) {
            n.o(e, t) || Object.defineProperty(e, t, {
                enumerable: !0,
                get: r
            })
        }
        ,
        n.r = function(e) {
            "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, {
                value: "Module"
            }),
            Object.defineProperty(e, "__esModule", {
                value: !0
            })
        }
        ,
        n.t = function(e, t) {
            if (1 & t && (e = n(e)),
            8 & t)
                return e;
            if (4 & t && "object" == typeof e && e && e.__esModule)
                return e;
            var r = Object.create(null);
            if (n.r(r),
            Object.defineProperty(r, "default", {
                enumerable: !0,
                value: e
            }),
            2 & t && "string" != typeof e)
                for (var o in e)
                    n.d(r, o, function(t) {
                        return e[t]
                    }
                    .bind(null, o));
            return r
        }
        ,
        n.n = function(e) {
            var t = e && e.__esModule ? function() {
                return e.default
            }
            : function() {
                return e
            }
            ;
            return n.d(t, "a", t),
            t
        }
        ,
        n.o = function(e, t) {
            return Object.prototype.hasOwnProperty.call(e, t)
        }
        ,
        n.p = "../../../../",
        n(n.s = 8)
    }([function(t, n) {
        t.exports = e
    }
    , function(e, t) {
        e.exports = function(e) {
            return e.webpackPolyfill || (e.deprecate = function() {}
            ,
            e.paths = [],
            e.children || (e.children = []),
            Object.defineProperty(e, "loaded", {
                enumerable: !0,
                get: function() {
                    return e.l
                }
            }),
            Object.defineProperty(e, "id", {
                enumerable: !0,
                get: function() {
                    return e.i
                }
            }),
            e.webpackPolyfill = 1),
            e
        }
    }
    , , , , , , , function(e, t, n) {
        e.exports = n(9)
    }
    , function(e, t, n) {
        "use strict";
        (function(e) {
            var r, o, u, i = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(e) {
                return typeof e
            }
            : function(e) {
                return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
            }
            ;
            !function(f, c) {
                "object" == i(t) && "object" == i(e) ? e.exports = c(n(0)) : (o = [n(0)],
                void 0 === (u = "function" == typeof (r = c) ? r.apply(t, o) : r) || (e.exports = u))
            }(window, function(e) {
                return function(e) {
                    var t = {};
                    function n(r) {
                        if (t[r])
                            return t[r].exports;
                        var o = t[r] = {
                            i: r,
                            l: !1,
                            exports: {}
                        };
                        return e[r].call(o.exports, o, o.exports, n),
                        o.l = !0,
                        o.exports
                    }
                    return n.m = e,
                    n.c = t,
                    n.d = function(e, t, r) {
                        n.o(e, t) || Object.defineProperty(e, t, {
                            enumerable: !0,
                            get: r
                        })
                    }
                    ,
                    n.r = function(e) {
                        "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, {
                            value: "Module"
                        }),
                        Object.defineProperty(e, "__esModule", {
                            value: !0
                        })
                    }
                    ,
                    n.t = function(e, t) {
                        if (1 & t && (e = n(e)),
                        8 & t)
                            return e;
                        if (4 & t && "object" == (void 0 === e ? "undefined" : i(e)) && e && e.__esModule)
                            return e;
                        var r = Object.create(null);
                        if (n.r(r),
                        Object.defineProperty(r, "default", {
                            enumerable: !0,
                            value: e
                        }),
                        2 & t && "string" != typeof e)
                            for (var o in e)
                                n.d(r, o, function(t) {
                                    return e[t]
                                }
                                .bind(null, o));
                        return r
                    }
                    ,
                    n.n = function(e) {
                        var t = e && e.__esModule ? function() {
                            return e.default
                        }
                        : function() {
                            return e
                        }
                        ;
                        return n.d(t, "a", t),
                        t
                    }
                    ,
                    n.o = function(e, t) {
                        return Object.prototype.hasOwnProperty.call(e, t)
                    }
                    ,
                    n.p = "../../../../",
                    n(n.s = 813)
                }({
                    0: function(t, n) {
                        t.exports = e
                    },
                    812: function(e, t, n) {
                        Object.defineProperty(t, "__esModule", {
                            value: !0
                        }),
                        t.default = function() {
                            var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                            return React.createElement(r, Object.assign({
                                multiLang: {
                                    moduleId: "7040",
                                    domainName: "sscqm",
                                    currentLocale: "simpchn"
                                },
                                refType: "grid",
                                refName: "refer-0006",
                                refCode: "",
                                placeholder: "",
                                queryGridUrl: "/nccloud/sscqm/checktask/CheckTaskGridRefAction.do",
                                columnConfig: [{
                                    name: ["refer-0001", "refer-0002"],
                                    code: ["code", "name"]
                                }],
                                isMultiSelectedEnabled: !1
                            }, e))
                        }
                        ;
                        var r = n(0).high.Refer
                    },
                    813: function(e, t, n) {
                        e.exports = n(812)
                    }
                })
            })
        }
        ).call(this, n(1)(e))
    }
    ])
});
//# sourceMappingURL=index.js.map
