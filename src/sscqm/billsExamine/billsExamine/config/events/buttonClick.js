import requestApi from "./requestApi";
import {pageTo} from 'nc-lightapp-front'

// /nccloud/resources/erm/expenseaccount/expenseaccount/card/index.html#/?status=browse&scene=bzcx&id=1001AT10000000001CUR&appcode=201102BCLF&pagecode=201102BCLF_C&tradetype=2641

export default {
  // 单据稽核
  checkDetailsView(btnKey, text) {
    switch (btnKey) {
      case "Audit":
        const {
          cardPagination: { setCardPaginationId },
          table: { getAllTableData }
        } = this.props;

        const allTableData = getAllTableData("billsExamineList");
        // 列表数据缓存
        const tableDataCache = {};
        tableDataCache.allpks = allTableData.allpks;
        tableDataCache.rows = allTableData.rows.map(ele => ele.values);

        sessionStorage.setItem(
          "examineSearchInfo", 
          JSON.stringify(this.props.search.getAllSearchData('newSearchArea').conditions)
        )

        // 隐藏列表 显示详情
        this.setState(
          { showBillDetail: true, currentValues: text, tableDataCache },
          () => {
            const {trade_type_code, bill_type_code, pk_bill} = text
            const param = {
              billtypeCode: bill_type_code.value,
              transtypeCode: trade_type_code.value,
              billid: pk_bill.value
            }

            requestApi.openBill({
              param,
              success: ({data: { url, data }, success}) => {
                // debugger
                if (success) {
                  if (url.indexOf('#') > -1) {
                    if (url.indexOf('#/') > -1) {
                      if(url.indexOf('?') < 0) {
                          url += '?'
                      }
                    }
                  } else {
                    url += '#/?'
                  }

                  const start = url.indexOf('/nccloud/resources') > -1 ? '' : '/nccloud/resources'
                  let iframeUrl = `${start}${url}status=${data.status}&scene=zycx`

                  for (let attr in data) {
                    if (attr != 'status') {
                      iframeUrl += `&${attr}=${data[attr]}`
                    }
                  }
                  // iframeUrl=  iframeUrl.slice(18)
                  // console.log('iframeUrl: ', iframeUrl)

                  this.setState({iframeUrl}, () => {
                      pageTo.addUrlParam({
                          c: data.appcode,
                          p: data.pagecode
                      })
                      setCardPaginationId({
                        status: 1,
                        id: text.id.value
                      });
                  })
                }
              }
            });
          }
        );
        break;
      default:
        break;
    }
  }
};
