import { ajax } from "nc-lightapp-front";

export default {
  // 打开单据
  openBill: ({ success, param }) => {
    ajax({
      url: `/nccloud/sscrp/rpbill/BrowseBillAction.do`,
      method: "post",
      data: param,
      success
    });
  },
  // 列表查询
  checkbills: ({data, success}) => {
    ajax({
      url: `/nccloud/sscqm/checkbill/CheckBillQueryListAction.do`,
      method: "post",
      data,
      success
    });
  },
  // 稽核内容查询
  checkContentQry: ({taskid, success}) => {
    ajax({
      url: `/nccloud/sscqm/checkcontent/CheckContentListAction.do`,
      method: "post",
      data: {
        taskid
      },
      success
    });
  },
  // 问题查询
  checkProblemList: ({id, success}) => {
    ajax({
      url: `/nccloud/sscqm/checkbill/CheckBillInfoAction.do`,
      method: "post",
      data: {
        checkbillid: id
      },
      success
    });
  },
  // 保存
  saveTemp: ({data, success}) => {
    // method：save
    ajax({
      url: `/nccloud/sscqm/checkbill/CheckBillSaveAction.do`,
      method: "post",
      data,
      success
    });
  },
  // 稽核
  checkExamine: ({data, success}) => {
    // method：temp
    ajax({
      url: `/nccloud/sscqm/checkbill/CheckBillSaveAction.do`,
      method: "post",
      data,
      success
    });
  },
  // 无问题通过
  checkPass: ({data, success}) => {
    // method：pass
    ajax({
      url: `/nccloud/sscqm/checkbill/CheckBillSaveAction.do`,
      method: "post",
      data,
      success
    });
  },
  // 查询单据状态
  checkCurrentBillStatu: ({value, success}) => {
    ajax({
      url: `/nccloud/sscqm/checktask/CheckTaskGetStatusByIdAction.do`,
      method: "post",
      data: {taskid: value},
      success
    });
  },
};
