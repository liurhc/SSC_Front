import requestApi from './requestApi'
import initTemplate from './initTemplate'
import buttonClick from './buttonClick'
export {
    initTemplate,
    requestApi,
    buttonClick
}
