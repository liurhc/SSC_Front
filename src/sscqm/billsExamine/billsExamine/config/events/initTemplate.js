import buttonClick from "./buttonClick";
import { getMultiLang } from "nc-lightapp-front";

export default function(props) {
  const _this = this

  props.createUIDom({}, function({ button, template }) {
    // console.log('button: ', button)
    // console.log('template: ', template)
    getMultiLang({moduleId: 7040, domainName: 'sscqm',currentLocale: 'zh-CN', callback: (multiLang) => {
      
      template.billsExamineList.items.push({
        label: multiLang['7040-0104'],
        attrcode: "opr",
        visible: true,
        width: "85px",
        fixed: "right",
        render: (values, text, index) => {
          return props.button.createOprationButton(["Audit"], {
            area: "billsExamineList",
            onButtonClick: (props, btnKey) => {
              buttonClick.checkDetailsView.apply(_this, [btnKey, text]);
            }
          });
        }
      });
  
      props.meta.setMeta(template);
      props.button.setButtons(button);
  
      props.table.setTableRender(
        "billsExamineList",
        "check_status",
        (values, text, index) => {
          return (
            <span>
              {(() => {
                if (text.check_status.value === "UNCHECK") {
                  return multiLang['7040-0077'];
                } else if (text.check_status.value === "CHECKING") {
                  return multiLang['7040-0078'];
                } else {
                  return multiLang['7040-0079'];
                }
              })()}
            </span>
          );
        }
      );
  
      props.table.setTableRender(
        "billsExamineList",
        "check_result",
        (values, text, index) => {
          return (
            <span>
              {(() => {
                if (text.check_result.value === "PASS") {
                  return multiLang['7040-0080'];
                } else if (text.check_result.value === "PROBLEM") {
                  return multiLang['7040-0081'];
                } else {
                  return "";
                }
              })()}
            </span>
          );
        }
      );
  
      props.table.setTableRender(
        "billsExamineList",
        "severity",
        (values, text, index) => {
          return (
            <span>
              {(() => {
                if (!text.severity) return "";
                if (text.severity.value === "QUALIFIED") {
                  return multiLang['7040-0082'];
                } else if (text.severity.value === "MORESLIGHT") {
                  return multiLang['7040-0083'];
                } else if (text.severity.value === "SLIGHT") {
                  return multiLang['7040-0084'];
                } else if (text.severity.value === "GENERAL") {
                  return multiLang['7040-0085'];
                } else if (text.severity.value === "SERIOUS") {
                  return multiLang['7040-0086'];
                } else if (text.severity.value === "MORESERIOUS") {
                  return multiLang['7040-0087'];
                } else {
                  return "";
                }
              })()}
            </span>
          );
        }
      );

    }})

  });

}
