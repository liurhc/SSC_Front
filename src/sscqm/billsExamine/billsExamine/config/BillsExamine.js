import { Component } from "react";
import { createPage, toast, base, pageTo, getMultiLang, createPageIcon } from "nc-lightapp-front";
import "./index.less";
import { initTemplate, requestApi } from "./events";
const { NCModal, NCButton, NCBackBtn, NCFormControl, NCAffix } = base;
import buttonClick from "./events/buttonClick";

// 严重程度
let PROBLEM_TYPES = {
  // QUALIFIED: "合格",
  // MORESLIGHT: "很轻微",
  // SLIGHT: "轻微",
  // GENERAL: "一般",
  // SERIOUS: "严重",
  // MORESERIOUS: "非常严重"
};

// 严重程度映射
let SERIOUS_LEVEL = {
  QUALIFIED: 0,
  MORESLIGHT: 1,
  SLIGHT: 2,
  GENERAL: 3,
  SERIOUS: 4,
  MORESERIOUS: 5
}

const DATA_SOURCE = "sscqm.billsExamine.billsExamine.listDataSource";

class BillsExamine extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showBillDetail: false,
      pkOrg: "",
      qmTask: "",
      iframeUrl: "",
      currentValues: {}, // 当前打开单据values
      showCheckModal: false,
      checkContent: null, // 稽核内容
      problemList: [],
      currentProblemType: {},
      tableDataCache: {},
      nowState: "unChecked",
      currentValuesState: "",
      fromPage: "",
      multiLang: {},
      currentBillStatu: ''
    };

    initTemplate.call(this, props);
  }
  componentDidMount() {
    getMultiLang({moduleId: 7040, domainName: 'sscqm',currentLocale: 'zh-CN', callback: (multiLang) => {
      this.setState({multiLang})

      PROBLEM_TYPES = {
        QUALIFIED: multiLang['7040-0082'],
        MORESLIGHT: multiLang['7040-0083'],
        SLIGHT: multiLang['7040-0084'],
        GENERAL: multiLang['7040-0085'],
        SERIOUS: multiLang['7040-0086'],
        MORESERIOUS: multiLang['7040-0087']
      }
    }})

    window.onbeforeunload = () => {
      const {showBillDetail, nowState} = this.state
      if (showBillDetail && nowState === 'unChecked') return ''
    }
  }
  componentDidUpdate() {
    // iframe高度适配
    if (!this.state.showBillDetail) return true
    const billFrame = document.getElementById('bill-frame')
    if (billFrame) {
      let clientHeight = document.documentElement.clientHeight || document.body.clientHeight
      clientHeight -= 78
      billFrame.style.minHeight = `${clientHeight}px`
    }
    return true
  }
  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.pkOrg !== this.state.pkOrg) {
      const {
        search: { setSearchValByField }
      } = this.props;
      setSearchValByField("newSearchArea", "billType", {
        value: undefined,
        display: undefined
      });
      setSearchValByField("newSearchArea", "orgs", {
        value: undefined,
        display: undefined
      });
      if (nextState.qmTask)
        setSearchValByField("newSearchArea", "qmTask", {
          value: undefined,
          display: undefined
        });
    }
    return true;
  }
  // 查询区编辑后事件 稽核任务 财务组织 单据类型 参照过滤
  onAfterEvent = (code, node, areaId) => {
    if (code === "pkOrg") {
      this.setState({ pkOrg: node.refpk || "" });
    }

    if (code === "qmTask") {
      this.setState({
        qmTask: node.refpk || ""
      });
    }

    if (code !== "pkOrg") return;

    const { getMeta, setMeta } = this.props.meta;
    let meta = getMeta();
    meta[areaId].items.map(one => {
      if (one.attrcode == "qmTask") { // 稽核任务
        one.queryCondition = () => {
          return {
            orgid: node.refpk, 
            GridRefActionExt: 'nccloud.web.sscqm.sscbd.ref.sqlbuilder.CheckTaskRefSqlBuilder'
          };
        };
        one.disabled = node.refpk ? false : true;
      }
      if (one.attrcode === "billType") { // 单据类型
        one.queryCondition = () => ({
          orgid: node.refpk,
          GridRefActionExt: 'nccloud.web.sscqm.sscbd.ref.sqlbuilder.BilltypeRefSqlBuilder'
        });
        one.disabled = node.refpk ? false : true;
      }
      if (one.attrcode === "orgs") { // 财务组织
        one.queryCondition = () => {
          return {
            sscid: node.refpk,
            GridRefActionExt: 'nccloud.web.sscqm.sscbd.ref.sqlbuilder.OrgRefSqlBuilder'
          };
        };
        one.disabled = node.refpk ? false : true;
      }
    });
    setMeta(meta);
  };
  // 双击 -> 稽核功能
  onRowDoubleClick = record => buttonClick.checkDetailsView.apply(this, ['Audit', record])
  // 单据查询
  billExtract = () => {
    const {
      table: { setAllTableData }
    } = this.props;
    const searchAreaData = this.props.search.getAllSearchData("newSearchArea");
    const param = {
      taskid: "",
      orgs: "",
      billType: "",
      checkno: "",
      checkStatus: "",
      checkResult: ""
    };
    for (let item of searchAreaData.conditions) {
      if (item.field === "qmTask") {
        param.taskid = item.value.firstvalue;
      } else if (item.field === "orgs") {
        param.orgs = item.value.firstvalue;
      } else if (item.field === "billType") {
        param.billType = item.value.firstvalue;
      } else if (item.field === "checkno") {
        param.checkno = item.value.firstvalue;
      } else if (item.field === "checkStatus") {
        param.checkStatus = item.value.firstvalue;
      } else if (item.field === "checkResult") {
        param.checkResult = item.value.firstvalue;
      }
    }

    // return console.log('param: ', param)
    
    requestApi.checkbills({
      data: param,
      success: ({success, data}) => {
        // console.log('checkbills: ', data.billsExamineList.rows)
        // debugger
        if (success) {
          let idx = -1;
          let allpks = [];
          while (++idx < data.billsExamineList.rows.length) {
            allpks.push(data.billsExamineList.rows[idx].values.id.value)
          }

          setAllTableData("billsExamineList", {
            areacode: "billsExamineList",
            rows: data.billsExamineList.rows,
            allpks
          });

          const length = data.billsExamineList.rows.length
          const {multiLang} = this.state
          if (length > 0) {
            toast({
              content: multiLang['7040-0192'] + "，" + multiLang['7040-0193'] + length + multiLang['7040-0194']
            });
          } else {
            toast({content: multiLang['7040-0195'],color:'warning'})
          }
        }
      }
    });
  };
  // 报告跳转查询
  reviewBillList = param => {
    requestApi.checkbills({
      data: param,
      success: ({data, success}) => {
        if (success) {
          let idx = -1;
          let allpks = [];
          while (++idx < data.billsExamineList.rows.length) {
            allpks.push(data.billsExamineList.rows[idx].values.id.value)
          }

          this.props.table.setAllTableData("billsExamineList", {
            areacode: "billsExamineList",
            rows: data.billsExamineList.rows,
            allpks
          });

          this.props.search.setDisabledByField( 'newSearchArea', 'pkOrg', true )
          this.props.search.setDisabledByField( 'newSearchArea', 'checkno', false )
          this.props.search.setDisabledByField( 'newSearchArea', 'checkStatus', false )
          this.props.search.setDisabledByField( 'newSearchArea', 'checkResult', false )
          this.props.search.setDisabledByField( 'newSearchArea', 'orgs', false )
          this.props.search.setDisabledByField( 'newSearchArea', 'billType', false )
          // console.log('data.billsExamineList.rows: ', data.billsExamineList.rows)
        }
      }
    });
  }
  // 返回列表
  handleBack = () => {
    this.setState(
      {
        showBillDetail: false,
        nowState: "unChecked"
      },
      () => {
        if(sessionStorage.getItem("examineSearchInfo") === 'undefined') return
        
        const infos = JSON.parse(sessionStorage.getItem("examineSearchInfo"))

        let idx = -1
        while(++idx < infos.length) {
          const info = infos[idx]
          this.props.search.setSearchValByField("newSearchArea", info.field, {
            value: info.value.firstvalue,
            display: info.display
          });
        }

        setTimeout(() => {
          this.billExtract();
        }, 80)
      }
    );
  };
  // 打开稽核弹窗
  billCheckDetailView = () => {
    const { currentValues } = this.state;
    // 稽核内容查询
    requestApi.checkContentQry({
      taskid: currentValues.taskid.value,
      success: data => {
        this.setState({ checkContent: data.data });
      }
    });
    
    // 问题查询
    requestApi.checkProblemList({
      id: currentValues.id.value,
      success: data => {
        this.setState({
          problemList: data.data.billsExamineList.rows[0].values.checkProblems.value.map(ele => ({
            problemid: { value: ele.problemid },
            checkinfoid: { value: ele.id },
            problemName: { value: ele.problem_name },
            seriousLevel: { value: ele.serious_level }
          }))
        });

        this.props.form.setAllFormValue({
          extendAttribute: {
            rows: [
              {
                values: {
                  status: {
                    display: data.data.billsExamineList.rows[0].values.problem_desc.value,
                    value: data.data.billsExamineList.rows[0].values.problem_desc.value
                  },
                  state: {
                    display: data.data.billsExamineList.rows[0].values.improve_desc.value,
                    value: data.data.billsExamineList.rows[0].values.improve_desc.value
                  }
                }
              }
            ]
          }
        });
      }
    });

    this.setState({ showCheckModal: true }, () => {
      let checked =
        (currentValues.check_status &&
          currentValues.check_status.value === "CHECKED") ||
        this.state.nowState === "checked"
          ? true
          : false;
      // this.props.form.setFormItemsDisabled("extendAttribute", {
      //   state: checked,
      //   status: checked
      // });
      this.props.form.setFormStatus(
        "extendAttribute",
        checked ? "browse" : "edit"
      );
    });
  };
  makeSeriousName = problemList => {
    let currentSeriousLevel = -1
    let currentSeriousName = ''
    let index = -1
    while (++index < problemList.length) {
      if (SERIOUS_LEVEL[problemList[index].seriousLevel.value] > currentSeriousLevel) {
        currentSeriousLevel = SERIOUS_LEVEL[problemList[index].seriousLevel.value]
        currentSeriousName = problemList[index].seriousLevel.value
      }
    }
    return currentSeriousName
  }
  closeModal = () => {
    const { currentValues, currentValuesState } = this.state;
    currentValues.check_status.value =
      currentValuesState === "CHECKED"
        ? currentValuesState
        : currentValues.check_status.value;
    this.setState({
      showCheckModal: false,
      currentValues,
      currentValuesState: "",
      nowState: "unChecked"
    });
  }; 
  // 稽核问题参照编辑回调
  extendSearchAreaAfterEvent = (refCode, node, areaId) => {
    // debugger
    const values = node.values || {}
    values.refpk = node.refpk || ''
    values.refname = node.refname || ''
    values.refcode = node.refcode || ''
    // console.log('currentProblemType: ', values)
    this.setState({ currentProblemType: values });
  };
  // 删除问题
  delProblem = idx => {
    let { problemList } = this.state;
    problemList.splice(idx, 1);
    this.setState({ problemList });
  };
  // 稽核保存
  saveTemp = () => {
    const { currentValues, problemList, currentProblemType } = this.state;
    const {
      form: { getAllFormValue }
    } = this.props;
    const { state, status } = getAllFormValue(
      "extendAttribute"
    ).rows[0].values;

    if ((currentProblemType && currentProblemType.serious_level)) {
      let index = -1, onff = false
      while (++index < problemList.length) {
        if (problemList[index].problemid.value === currentProblemType.refpk) {
          onff = true
          continue
        }
      }
      if (!onff) {
        problemList.push({
          problemid: { value: currentProblemType.refpk },
          problemName: { value: currentProblemType.name.value },
          seriousLevel: { value: currentProblemType.serious_level.value },
          checkinfoid: { value: '' }
        });

        this.setState(
          {
            problemList,
            currentProblemType: {}
          },
          () => {
            this.props.search.clearSearchArea("extendSearchArea");
          }
        );
      }
    }

    const param = {
      checkbillid: currentValues.id.value || "",
      taskid: currentValues.taskid.value || "",
      problem_desc: status.value || "", // 问题描述
      improve_desc: state.value || "", // 整改说明
      checkProblems: problemList.map(ele => ({
        problemid: ele.problemid.value || "",
        problem_name: ele.problemName.value || "",
        serious_level: ele.seriousLevel.value || "",
        id:
          ele.checkinfoid && ele.checkinfoid.value ? ele.checkinfoid.value : "",
        checkbillid:
          ele.checkbillid && ele.checkbillid.value ? ele.checkbillid.value : "",
        taskid: ele.taskid && ele.taskid.value ? ele.taskid.value : ""
      })),
      ts: currentValues.ts.value || "",
      severity: this.makeSeriousName(problemList),
      checkResult: 'TEMP'
    };

    const {userId} = window.parent.GETBUSINESSINFO()
    param.processor = userId

    requestApi.saveTemp({
      data: param,
      success: ({data, success}) => {
        // debugger
        if (success) {
          toast({ title: this.state.multiLang['7040-0089'], duration: 3 });

          const { currentValues } = this.state;
          currentValues.check_status.value = "CHECKING";
          this.setState({
            showCheckModal: false,
            currentValues,
            currentValuesState: "",
            nowState: "unChecked"
          });
        }
      }
    });
  };
  // 添加问题
  addProblem = () => {
    const {
      state: { currentProblemType, problemList, multiLang },
      props: {
        search: { clearSearchArea }
      }
    } = this;
    if (currentProblemType && currentProblemType.serious_level) {
      // console.log('currentProblemType: ', currentProblemType)
      // debugger
      let index = -1, onff = false
      while (++index < problemList.length) {
        if (problemList[index].problemid.value === currentProblemType.refpk) {
          onff = true
          continue
        }
      }
      if (onff) return toast({ color: "warning", title: multiLang['7040-0188'] })
      
      problemList.push({
        problemid: { value: currentProblemType.refpk },
        problemName: { value: currentProblemType.name.value },
        seriousLevel: { value: currentProblemType.serious_level.value },
        checkinfoid: { value: '' }
      });
      this.setState(
        {
          problemList,
          currentProblemType: {}
        },
        () => {
          clearSearchArea("extendSearchArea");
        }
      );
    }
  };


  // 修改
  editExamine = () => {
    const { currentValues } = this.state;
    const currentValuesState = currentValues.check_status.value;
    currentValues.check_status.value = "unChecked";
    this.setState({ currentValues, currentValuesState, nowState: "unChecked" });
    // this.props.form.setFormItemsDisabled("extendAttribute", {
    //   state: false,
    //   status: false
    // });
    this.props.form.setFormStatus("extendAttribute", "edit");
  };
  // 稽核
  checkExamine = () => {
    const { currentValues, problemList, multiLang, currentProblemType } = this.state;

    if (problemList.length === 0 && !(currentProblemType && currentProblemType.serious_level))
      return toast({ color: "warning", title: multiLang['7040-0090'] });

    if ((currentProblemType && currentProblemType.serious_level)) {
      let index = -1, onff = false
      while (++index < problemList.length) {
        if (problemList[index].problemid.value === currentProblemType.refpk) {
          onff = true
          continue
        }
      }
      if (!onff) {
        problemList.push({
          problemid: { value: currentProblemType.refpk },
          problemName: { value: currentProblemType.name.value },
          seriousLevel: { value: currentProblemType.serious_level.value },
          checkinfoid: { value: '' }
        });

        this.setState(
          {
            problemList,
            currentProblemType: {}
          },
          () => {
            this.props.search.clearSearchArea("extendSearchArea");
          }
        );
      }
    }

    const {
      form: { getAllFormValue }
    } = this.props;
    const { state, status } = getAllFormValue(
      "extendAttribute"
    ).rows[0].values;

    const param = {
      checkbillid: currentValues.id.value || "",
      taskid: currentValues.taskid.value || "",
      problem_desc: status.value || "", // 问题描述
      improve_desc: state.value || "", // 整改说明
      checkProblems: problemList.map(ele => ({
        problemid: ele.problemid.value || "",
        problem_name: ele.problemName.value || "",
        serious_level: ele.seriousLevel.value || "",
        id:
          ele.checkinfoid && ele.checkinfoid.value ? ele.checkinfoid.value : "",
        checkbillid:
          ele.checkbillid && ele.checkbillid.value ? ele.checkbillid.value : "",
        taskid: ele.taskid && ele.taskid.value ? ele.taskid.value : ""
      })),
      ts: currentValues.ts.value || "",
      severity: this.makeSeriousName(problemList),
      checkResult: 'PROBLEM'
    };
    
    const {userId} = window.parent.GETBUSINESSINFO()
    param.processor = userId

    requestApi.checkExamine({
      data: param,
      success: ({data, success}) => {
        if (success) {
          toast({ title: multiLang['7040-0091'] });
          const { currentValues, currentValuesState } = this.state;
          currentValues.check_status.value = "CHECKED";
          this.setState({
            showCheckModal: false,
            currentValues,
            currentValuesState: "",
            nowState: "checked"
          });
        }
      }
    });
  };
  // 无问题通过
  checkPass = () => {
    const { currentValues, problemList, multiLang, currentProblemType } = this.state;

    if (problemList.length !== 0 || (currentProblemType && currentProblemType.serious_level))
      return toast({ color: "warning", title: multiLang['7040-0092'] });

    const {
      form: { getAllFormValue }
    } = this.props;
    const { state, status } = getAllFormValue(
      "extendAttribute"
    ).rows[0].values;

    const param = {
      checkbillid: currentValues.id.value || "",
      taskid: currentValues.taskid.value || "",
      problem_desc: status.value || "", // 问题描述
      improve_desc: state.value || "", // 整改说明
      ts: currentValues.ts.value || "",
      checkResult: 'PASS'
    };
    
    const {userId} = window.parent.GETBUSINESSINFO()
    param.processor = userId

    requestApi.checkPass({
      data: param,
      success: ({data, success}) => {
        if(success) {
          toast({ title: multiLang['7040-0093'] });
          this.closeModal();
        }
      }
    });
  };
  // 查询区挂载完成回调 
  renderCompleteEvent = () => {
    // 报告跳转
    let reportInfo = sessionStorage.getItem('taskReportInfo')
    if (reportInfo) {
      reportInfo = JSON.parse(reportInfo)
      this.setState({fromPage: 'report'}, () => {

        // console.log('reportInfo: ', reportInfo)

        this.props.search.setSearchValByField("newSearchArea", "pkOrg", reportInfo.pkOrg);
        this.props.search.setSearchValByField("newSearchArea", "qmTask", reportInfo.qmTask);

        const param = {
          taskid: `${reportInfo.qmTask.value}`,
          orgs: "",
          billType: "",
          checkno: "",
          checkStatus: "",
          checkResult: ""
        }
        this.reviewBillList(param);
        
        // this.setState({ pkOrg: reportInfo.pkOrg.value, qmTask: reportInfo.qmTask.value });
        const { getMeta, setMeta } = this.props.meta;
        let meta = getMeta();
        meta['newSearchArea'].items.map(one => {
          if (one.attrcode == "qmTask") { // 稽核任务
            one.queryCondition = () => {
              return {
                orgid: reportInfo.pkOrg.value, 
                GridRefActionExt: 'nccloud.web.sscqm.sscbd.ref.sqlbuilder.CheckTaskRefSqlBuilder'
              };
            };
          } else if (one.attrcode === "billType") { // 单据类型
            one.queryCondition = () => ({
              orgid: reportInfo.pkOrg.value,
              GridRefActionExt: 'nccloud.web.sscqm.sscbd.ref.sqlbuilder.BilltypeRefSqlBuilder'
            });
          } else if (one.attrcode === "orgs") { // 财务组织
            one.queryCondition = () => {
              return {
                sscid: reportInfo.pkOrg.value,
                GridRefActionExt: 'nccloud.web.sscqm.sscbd.ref.sqlbuilder.OrgRefSqlBuilder'
              };
            };
          }
        });
        setMeta(meta);
      })
    }
  }
  // 点击翻页器回调
  cardPaginationUpdate = (props, id) => {
    // debugger
    const {
      cardPagination: { setCardPaginationId }
    } = this.props;
    setCardPaginationId({
      status: 1,
      id
    });
    // console.log("翻页器更新： ", id);
    const { allpks, rows } = this.state.tableDataCache;
    const currentValues = rows[allpks.indexOf(id)];
    // console.log("当前缓存数据已更新： ", currentValues);
    // 缓存切换后当前行数据 并更新iframe层URL
    this.setState(
      {
        currentValues,
        nowState: "unChecked",
        currentValuesState: currentValues.check_status.value
      },
      () => {
        setTimeout(() => {
          const {trade_type_code, bill_type_code, pk_bill} = currentValues
          const param = {
            billtypeCode: bill_type_code.value,
            transtypeCode: trade_type_code.value,
            billid: pk_bill.value
          }

          let srcurl = this.state.iframeUrl

          requestApi.openBill({
            param,
            success: ({data: { url, data }, success}) => {
              // debugger
              if (success) {
                if (url.indexOf('#') > -1) {
                  if (url.indexOf('#/') > -1) {
                    if(url.indexOf('?') < 0) {
                        url += '?'
                    }
                  }
                } else {
                  url += '#/?'
                }

                const start = url.indexOf('/nccloud/resources') > -1 ? '' : '/nccloud/resources'
                let iframeUrl = `${start}${url}status=${data.status}&scene=zycx`

                for (let attr in data) {
                  if (attr != 'status') {
                    iframeUrl += `&${attr}=${data[attr]}`
                  }
                }
                // console.log('iframeUrl: ', iframeUrl)
                this.setState({iframeUrl}, () => {
                    pageTo.addUrlParam({
                        c: data.appcode,
                        p: data.pagecode
                    })
                    setCardPaginationId({
                      status: 1,
                      id: currentValues.id.value
                    });

                    let src = srcurl.substr(srcurl.indexOf("/nccloud"));
                    src = src.substr(0, src.indexOf("#"));
                    if(src == iframeUrl.substr(0, iframeUrl.indexOf("#"))){
                        document.getElementById("bill-frame").contentWindow.location.reload(true);
                    }
                })
              }
            }
          });
        }, 200)
      }
    );
  };
  // 查询当前单据状态 未确认不可稽核
  checkCurrentBillStatu = () => {
    const {multiLang, currentValues: {taskid: {value}}} = this.state;
    requestApi.checkCurrentBillStatu({
      value,
      success: ({success, data}) => {
        // console.log('checkCurrentBillStatu: ', res)
        if (success) {
          if (['SAVED', 'ENABLED', 'SAMPLED'].indexOf(data) > -1) { //不可稽核
            toast({ color: "warning", title: multiLang['7040-0198'] })
          } else { // 可稽核
            this.billCheckDetailView()
            this.setState({currentBillStatu: data})
          }
        }
      }
    })
  }

  render() {
    const {
      button: { createButtonApp },
      search: { NCCreateSearch },
      table: { createSimpleTable },
      cardPagination: { createCardPagination },
      form: { createForm }
    } = this.props;

    const {
      showBillDetail,
      iframeUrl,
      showCheckModal,
      checkContent,
      problemList,
      currentProblemType,
      currentValues,
      nowState,
      fromPage,
      multiLang,
      currentBillStatu
    } = this.state;

    const {
      billExtract,
      onAfterEvent,
      // billCheckDetailView,
      extendSearchAreaAfterEvent,
      addProblem,
      saveTemp,
      checkExamine,
      checkPass,
      editExamine,
      delProblem,
      closeModal,
      makeSeriousName,
      onRowDoubleClick,
      renderCompleteEvent,
      checkCurrentBillStatu
    } = this;

    // 列表
    const checkList = (
      <div id="bill-examine">
        <NCAffix offsetTop={0}>
          <div id="bill-examine-head--wrap" className="clearfix">
            <div className='bill-examine-head'>
              {createPageIcon()}
              <h1>{multiLang['7040-0088']}</h1>
            </div>
            {NCCreateSearch("newSearchArea", {
              hideSearchCondition: false,
              hideBtnArea: false,
              showAdvBtn: false,
              clickSearchBtn: billExtract,
              onAfterEvent,
              // hideBtnArea: fromPage === 'report' ? true : false,
              showClearBtn: fromPage === 'report' ? false : true,
              // renderCompleteEvent
            })}
          </div>
        </NCAffix>
        <div id="bill-examine-body">
          {createSimpleTable("billsExamineList", {
            showIndex: true,
            // showCheck: true,
            dataSource: DATA_SOURCE,
            onRowDoubleClick
          })}
        </div>
      </div>
    );

    // 单据稽核/打开单据
    const checkDetail = (
      <div id="bill-examine">
        <NCAffix offsetTop={0}>
          <div id="check-detail-head">
            <NCBackBtn onClick={this.handleBack} />
            {createPageIcon()}
            <h1>{multiLang['7040-0088']}</h1>
            <div className="detail-head-right clearfix">
              {/* 稽核操作 */}
              {createButtonApp({
                area: "BillCheckDetail",
                onButtonClick: checkCurrentBillStatu
              })}
              {/* 翻页器 */}
              {createCardPagination({
                handlePageInfoChange: this.cardPaginationUpdate,
                dataSource: DATA_SOURCE
              })}
            </div>
          </div>
        </NCAffix>
        <div id="check-detail-wrap">
          <iframe id="bill-frame" frameborder="0" src={iframeUrl}>
            {multiLang['7040-0094']}
          </iframe>
        </div>
      </div>
    );

    let currentSeriousName = makeSeriousName(problemList)

    return (
      <div>
        {showBillDetail ? checkDetail : checkList}

        <NCModal
          show={showCheckModal}
          onHide={closeModal}
        >
          <NCModal.Header closeButton={true}>
            <NCModal.Title>{multiLang['7040-0095']}</NCModal.Title>
          </NCModal.Header>
          <NCModal.Body>
            <div className='item-wrap clearfix'>
              <h1>{multiLang['7040-0096']}</h1>
              <p
                className={
                  (currentValues.check_status &&
                    currentValues.check_status.value === "CHECKED") ||
                  nowState === "checked"
                    ? "check-content check-content-view"
                    : "check-content"
                }
              >
                <ul className="clearfix">
                  {checkContent && checkContent.length > 0
                    ? checkContent.map(eachCon => {
                        return (
                          <li title={eachCon.refname}>
                            {eachCon.refname}
                          </li>
                        );
                      })
                    : ""}
                </ul>
              </p>
            </div>
            
            <div className='line-through--wrap'></div>
            
            <div className='item-wrap clearfix'>
              <h1>{multiLang['7040-0097']}</h1>
              <div
                className={
                  (currentValues.check_status &&
                    currentValues.check_status.value === "CHECKED") ||
                  nowState === "checked"
                    ? "problem-list-wrap problem-list-wrap-view"
                    : "problem-list-wrap"
                }
              >
                <ul className="problem-list">
                  {problemList.map((ele, idx) => (
                    <li>
                      <span className='name' title={ele.problemName.value}>{ele.problemName.value}</span>
                      <span className='yzcd'>{multiLang['7040-0098']}</span>
                      <span className='problem' title={PROBLEM_TYPES[ele.seriousLevel.value]}>{PROBLEM_TYPES[ele.seriousLevel.value]}</span>

                      <a
                        className="billnoa-examine"
                        onClick={() => {
                          delProblem(idx);
                        }}
                        style={{
                          display:
                            (currentValues.check_status &&
                              currentValues.check_status.value === "CHECKED") ||
                            nowState === "checked"
                              ? "none"
                              : "inline-block"
                        }}
                      >{multiLang['7040-0202']}</a>
                    </li>
                  ))}
                </ul>
                <div
                  id="extend-area"
                  style={{
                    display:
                      (currentValues.check_status &&
                        currentValues.check_status.value === "CHECKED") ||
                      nowState === "checked"
                        ? "none"
                        : "block"
                  }}
                >
                  {NCCreateSearch("extendSearchArea", {
                    hideSearchCondition: true,
                    hideBtnArea: true,
                    showAdvBtn: false,
                    onAfterEvent: extendSearchAreaAfterEvent
                  })}
                  <span className="extend-label">{multiLang['7040-0098']}</span>
                  <NCFormControl
                    disabled={true}
                    value={
                      currentProblemType && currentProblemType.serious_level
                        ? currentProblemType.serious_level.value
                          ? PROBLEM_TYPES[currentProblemType.serious_level.value]
                          : ""
                        : ""
                    }
                  />
                  
                  <a
                    className="billnoa-examine"
                    onClick={addProblem}
                  >{multiLang['7040-0201']}</a>
                </div>
              </div>
            </div>
            
            <div className='line-through--wrap'></div>
            
            <div className='item-wrap clearfix'>
              <h1>{multiLang['7040-0199']}</h1>
              <div className='current-serious-wrap'>
                { currentSeriousName ? PROBLEM_TYPES[currentSeriousName] : '' }
              </div>
            </div>

            <div className='line-through--wrap'></div>
            
            <div className='item-wrap clearfix'>
              <div
                className={
                  (currentValues.check_status &&
                    currentValues.check_status.value === "CHECKED") ||
                  nowState === "checked"
                    ? "extend-area-form extend-area-form-view"
                    : "extend-area-form"
                }
              >
                {createForm("extendAttribute", {})}
              </div>
            </div>
          </NCModal.Body>
          {
            (fromPage !== 'report' && (currentBillStatu !== 'REPORTED' && currentBillStatu !== 'CLOSED'))
            &&
            (
              <NCModal.Footer className='examine-check-modal--footer'>
                <NCButton
                  style={{
                    display:
                      (currentValues.check_status &&
                        currentValues.check_status.value === "CHECKED") ||
                      nowState === "checked"
                        ? "none"
                        : "inline-block"
                  }}
                  onClick={checkPass}
                  colors="primary"
                >{multiLang['7040-0099']}</NCButton>
                <NCButton
                  style={{
                    display:
                      (currentValues.check_status &&
                        currentValues.check_status.value === "CHECKED") ||
                      nowState === "checked"
                        ? "none"
                        : "inline-block"
                  }}
                  onClick={checkExamine}
                >{multiLang['7040-0100']}</NCButton>
                <NCButton
                  style={{
                    display:
                      (currentValues.check_status &&
                        currentValues.check_status.value === "CHECKED") ||
                      nowState === "checked"
                        ? "none"
                        : "inline-block"
                  }}
                  onClick={saveTemp}
                >{multiLang['7040-0200']}</NCButton>
                <NCButton
                  style={{
                    display:
                      (currentValues.check_status &&
                        currentValues.check_status.value === "CHECKED") ||
                      nowState === "checked"
                        ? fromPage === 'report' ? "none" : "inline-block"
                        : "none"
                  }}
                  onClick={editExamine}
                  colors="primary"
                >{multiLang['7040-0102']}</NCButton>
                <NCButton onClick={closeModal}>{multiLang['7040-0103']}</NCButton>
              </NCModal.Footer>
            )
          }
        </NCModal>
      </div>
    );
  }
}

// localStorage.setItem("gzip", "0");
// localStorage.setItem("ShuntServerInfo", "{}");

BillsExamine = createPage({
  mutiLangCode: '7040'
})(BillsExamine);
export default BillsExamine;
