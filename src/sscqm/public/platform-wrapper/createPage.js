import React, { Component } from 'react';
import  {createPage} from 'old-nc-lightapp-front';

let oldCreatePage = createPage;
export default ({ initTemplate, mutiLangCode, billinfo }) => (App) => {

    class extApp extends Component {
        constructor(props) {
            super(props);

            this.createUIDom = (...param) => {
                //todo
                props.createUIDom(...param);
            }

            this.specialOpenTo = (...param) => {
                //todo
                props.specialOpenTo(...param)
            }

            this.output = {
                createUIDom : this.createUIDom,
                specialOpenTo : this.specialOpenTo
            }
        }

        render() {
            return <App {...this.props}  {...this.output} />
        }
    }

    return oldCreatePage(
        { initTemplate, mutiLangCode, billinfo }
    )(extApp);

} 
