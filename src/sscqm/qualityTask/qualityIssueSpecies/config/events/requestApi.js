import { ajax } from "nc-lightapp-front";

export default {
  // 获取问题树
  checkoutTreeContents: opt => {
    ajax({
      url: `/nccloud/sscqm/checkproblemtype/CheckProblemTypeTreeAction.do`,
      method: "post",
      data: {
        pid: '', keyword: '',
        queryCondition: {
          pk_defdoclist: null, 
          DataPowerOperationCode:null, 
          isDataPowerEnable:false, 
          isShowUnit:false
        },
        pageInfo:{pageSize: 50, pageIndex: -1}
      },
      success: ({data: {rows}, success}) => {
        if(success) opt.success(rows);
      }
    });
  },
  // 添加问题类型
  addProblemType: ({data, success}) => {
    ajax({
      url: `/nccloud/sscqm/checkproblemtype/CheckProblemTypeSaveAction.do`,
      method: "post",
      data,
      success
    });
  },
  // 获取单条问题记录
  checkProblemTypes: ({id, success}) => {
    ajax({
      url: `/nccloud/sscqm/checkproblemtype/CheckProblemTypeQueryAction.do`,
      method: "post",
      data: {id},
      success
    });
  },
  // 启用问题
  startProblem: ({pks, success}) => {
    ajax({
      url: `/nccloud/sscqm/checkproblemtype/CheckProblemTypeStartAction.do`,
      method: "post",
      data: {ids: pks},
      success
    });
  },
  // 启用问题
  stopProblem: ({pks, success}) => {
    ajax({
      url: `/nccloud/sscqm/checkproblemtype/CheckProblemTypeStopAction.do`,
      method: "post",
      data: {ids: pks},
      success
    });
  },
  delProblemType: ({refpk, success}) => {
    ajax({
      url: `/nccloud/sscqm/checkproblemtype/CheckProblemTypeDeleteAction.do`,
      method: "post",
      data: {id: refpk},
      success
    });
  }
  // updateProblemType: ({data, success}) => {
  //   ajax({
  //     url: `/quality-api/checkproblemtypes/update`,
  //     method: "post",
  //     data,
  //     success
  //   });
  // }
};
