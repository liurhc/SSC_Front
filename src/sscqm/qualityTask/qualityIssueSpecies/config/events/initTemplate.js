export default function(props) {
  const _this = this;
  props.createUIDom({}, function({ button, template }) {
    props.button.setButtons(button);
    props.meta.setMeta(template);
    props.button.setButtonVisible(["SaveDetail", "CancelSaveDetail"], false);
    _this.initSyncTree();
    props.form.setFormStatus('detailView', 'edit')
  });
}
