import { Component } from "react";
import { createPage, toast, base, getMultiLang, promptBox } from "nc-lightapp-front";
import "./index.less";
const { NCCheckbox } = base;
import { initTemplate, requestApi } from "./events";
import {
  ProfileStyle,
  ProfileHead,
  ProfileBody,
  BodyLeft,
  BodyRight,
  HeadCenterCustom,
  ButtonGroup
} from "ssccommon/components/profile";

class QualitySpecies extends Component {
  constructor(props) {
    super(props);
    this.state = {
      parentTreeNode: null,
      checked: false,
      status: 'add',
      rootPk: '',
      multiLang: {},
      disabledTreeSearch: false,
      disabledCheck: false,
      showSureCancelModel: false,
      beforeAddSelectedNode: null,
      delPk: '',
      showDelModel: false
    };

    this.PROBLEM_TYPES = {
      // QUALIFIED: "合格",
      // MORESLIGHT: "很轻微",
      // SLIGHT: "轻微",
      // GENERAL: "一般",
      // SERIOUS: "严重",
      // MORESERIOUS: "非常严重"
    };
    
    initTemplate.call(this, props);
  }
  componentDidMount() {
    getMultiLang({moduleId: 7040, domainName: 'sscqm',currentLocale: 'zh-CN', callback: (multiLang) => {
      // console.log('multiLang: ', multiLang)
      this.setState({multiLang})

      this.PROBLEM_TYPES = {
        QUALIFIED: multiLang["7040-0011"],
        MORESLIGHT: multiLang["7040-0012"],
        SLIGHT: multiLang["7040-0013"],
        GENERAL: multiLang["7040-0014"],
        SERIOUS: multiLang["7040-0015"],
        MORESERIOUS: multiLang["7040-0016"]
      }
    }})

    window.onbeforeunload = () => {
      if (this.props.form.getFormStatus('detailView') === 'edit') return ''
    }
  }
  // 初始化数据
  initSyncTree = () => {
    const {multiLang} = this.state

    requestApi.checkoutTreeContents({
      success: rows => {
        const {
          syncTree: { setSyncTreeData, hideIcon, createTreeData }
        } = this.props;

        if(!this.state.checked) {
          rows = rows.filter(each => each.values.is_using.value === 'Y')
          let index = -1
          while(++index < rows.length) {
            rows[index].values.is_using.value = true
            rows[index].refname =  `${rows[index].refcode} ${rows[index].refname}`
          }
        } else {
          let index = -1
          while(++index < rows.length) {
            rows[index].values.is_using.value = rows[index].values.is_using.value === 'Y' ? true : false
            rows[index].refname =  `${rows[index].refcode} ${rows[index].refname}${!rows[index].values.is_using.value ? ' ' + multiLang['7040-0005'] : ''}`
          }
        }
        let treeData = createTreeData(rows)
        // console.log('initSyncTree: ', treeData)

        setSyncTreeData(
          "qualitySpeciesTree",
          treeData
        );
        if (treeData && treeData[0] && treeData[0].refpk) {
          this.setState({rootPk: treeData[0].refpk})
          hideIcon("qualitySpeciesTree", treeData[0].refpk, {
            delIcon:false,
            editIcon:false,
            addIcon:true
          })
        }
      }
    });
  };
  // 增
  clickAddIconEve = parentTreeNode => {
    this.setState({beforeAddSelectedNode: this.state.parentTreeNode}, () => {
      this.setState({ parentTreeNode, status: 'add' })
    })
    
    const {
      form: {setFormStatus, setAllFormValue, setFormItemsDisabled}
    } = this.props
    setFormStatus('detailView', 'edit')
    
    this.props.syncTree.setTreeEdit('qualitySpeciesTree', false)
    this.props.syncTree.setNodeDisable('qualitySpeciesTree', true)
    this.setState({disabledTreeSearch: true, disabledCheck: true})

    setFormItemsDisabled('detailView', {'is_using': true})
    setAllFormValue({
      'detailView': {
        rows:[{values: {
          // category: {display: '', value: ''},
          deduction_point: {display: '', value: ''},
          instructions: {display: '', value: ''},
          is_using: {display: true, value: true},
          orgid: {display: '', value: ''},
          code: {display: '', value: ''},
          name: {display: '', value: ''},
          name2: {display: '', value: ''},
          name3: {display: '', value: ''},
          name4: {display: '', value: ''},
          name5: {display: '', value: ''},
          name6: {display: '', value: ''},
          id: {display: '', value: ''},
          serious_level: {display: '', value: ''},
          parentid: {display: parentTreeNode.pid, value: parentTreeNode.pid}
        }}]
      }
    })
    this.props.button.setButtonVisible([ 'SaveDetail', 'CancelSaveDetail' ], true)
  };
  // 新增 / 修改 编辑后事件
  formEditAfterEvent = () => {
    const {getAllFormValue, setFormStatus, setFormItemsDisabled, setFormItemsValue, isCheckNow, getResidtxtLang} = this.props.form
    const {rows: [ details ]} = getAllFormValue('detailView')
    const {
      code, 
      name, 
      name2, 
      name3, 
      name4, 
      name5, 
      name6, 
      deduction_point, 
      serious_level, 
      // category, 
      instructions, 
      is_using, 
      /* orgName, */ 
      orgid, 
      // parentid
    } = details.values
    const {parentTreeNode, multiLang} = this.state

    if (
      !isCheckNow('detailView')
    )
      return toast({
        title: multiLang['7040-0004'],
        color: "warning"
      });
    
    if (isNaN( Number(deduction_point.value) ))
      return toast({
        title: multiLang['7040-0160'],
        color: "warning"
      });
    let deductionPoint = Number(deduction_point.value) > 0 ? -1 * Number(deduction_point.value) : Number(deduction_point.value)

    const data = {
      orgid: orgid.value,
      code: code.value,
      name: name.value,
      name2: name2 ? name2.value : '',
      name3: name3 ? name3.value : '',
      name4: name4 ? name4.value : '',
      name5: name5 ? name5.value : '',
      name6: name6 ? name6.value : '',
      parentid: parentTreeNode.refpk,
      // category: category.value,
      instructions: instructions.value,
      is_using: is_using.value ? 'Y' : 'N',
      serious_level: serious_level.value,
      deduction_point: deductionPoint
    };

    const _name = getResidtxtLang('detailView', 'name')

    if (this.state.status === 'add') { // 增加
      const { addNodeSuccess, setNodeSelected } = this.props.syncTree;
      requestApi.addProblemType({
        data,
        success: res => {
          const values = {}
          for (let attr in res.data) {
            values[attr] = {display: res.data[attr], scale: "-1", value: res.data[attr]}
          }

          let newNode = {
            isleaf: true,
            pid: parentTreeNode.refpk,
            refcode: code.value,
            refname: code.value + ' ' + details.values[_name].value,
            refpk: res.data.id,
            values
          }
          addNodeSuccess("qualitySpeciesTree", newNode);
          this.setState({parentTreeNode: newNode})
          setNodeSelected('qualitySpeciesTree', res.data.id)

          setFormStatus('detailView', 'browse')

          this.props.syncTree.setTreeEdit('qualitySpeciesTree', true)
          this.props.syncTree.setNodeDisable('qualitySpeciesTree', false)
          this.setState({disabledTreeSearch: false, disabledCheck: false})
    
          setFormItemsDisabled('detailView', {'is_using': false})
          this.props.button.setButtonVisible([ 'SaveDetail', 'CancelSaveDetail' ], false)
          setFormItemsValue('detailView',{'deduction_point':{value:deductionPoint,display:deductionPoint}})
        }
      });
    } else { // 修改
      data.id = parentTreeNode.refpk
      data.parentid = parentTreeNode.pid
      data.ts = parentTreeNode.values.ts.value

      requestApi.addProblemType({
        data,
        success: res => {
          const { editNodeSuccess } = this.props.syncTree
          let refname = is_using.value ? code.value + ' ' + details.values[_name].value : code.value + ' ' + details.values[_name].value + ' ' + multiLang['7040-0005']

          const values = {}
          for (let attr in res.data) {
            values[attr] = {display: res.data[attr], scale: "-1", value: res.data[attr]}
          }

          editNodeSuccess('qualitySpeciesTree', {
            isleaf: parentTreeNode.isleaf,
            pid: parentTreeNode.pid,
            refcode: code.value,
            refname,
            refpk: parentTreeNode.refpk,
            values
          })
          
          setFormStatus('detailView', 'browse')

          this.props.syncTree.setTreeEdit('qualitySpeciesTree', true)
          this.props.syncTree.setNodeDisable('qualitySpeciesTree', false)
          this.setState({disabledTreeSearch: false, disabledCheck: false})

          setFormItemsDisabled('detailView', {'is_using': false})
          this.props.button.setButtonVisible([ 'SaveDetail', 'CancelSaveDetail' ], false)

          this.props.syncTree.hideIcon("qualitySpeciesTree", this.state.rootPk, {
            delIcon:false,
            editIcon:false,
            addIcon:true
          })
          setFormItemsValue('detailView',{'deduction_point':{value:deductionPoint,display:deductionPoint}})
        }
      });
    }
  };
  // 节点选中
  onSelectEve = (id, node, onoff) => {
    const {form: {setAllFormValue, setFormStatus, setFormItemsDisabled}, button: {setButtonVisible} } = this.props
    // 设置右侧表单数据
    if (node.pid === '__root__') {
      this.setState({parentTreeNode: null})
      return
    }
    requestApi.checkProblemTypes({
      id,
      success: data => {
        // console.log('currentProblemType_ ', data.data)
        setFormStatus('detailView', 'browse')

        this.props.syncTree.setTreeEdit('qualitySpeciesTree', true)
        this.props.syncTree.setNodeDisable('qualitySpeciesTree', false)
        this.setState({disabledTreeSearch: false, disabledCheck: false})

        setFormItemsDisabled('detailView', {'is_using': false})
        const detailViews = {}
        for (let attr in data.data) {
          if (attr === 'serious_level') {
            detailViews[attr] = {
              display: this.PROBLEM_TYPES[data.data[attr]],
              value: data.data[attr]
            }
          } else if (attr === 'is_using') {
            detailViews[attr] = {
              display: data.data[attr] === 'N' ? false : true,
              value: data.data[attr] === 'N' ? false : true
            }
          } else {
            detailViews[attr] = {
              display: data.data[attr],
              value: data.data[attr]
            }
          }
        }
        setAllFormValue({
          'detailView': {
            rows:[{values: detailViews}]
          }
        })

        setButtonVisible([ 'SaveDetail', 'CancelSaveDetail' ], false)
      }
    });
    this.setState({parentTreeNode: node})
  };
  // 准备取消
  prToCancel = () => this.setState({showSureCancelModel: true})
  // 取消保存
  cancelSaveDetails = () => {
    const {
      form: { setFormStatus, setAllFormValue, setFormItemsDisabled },
      button: { setButtonVisible },
      syncTree: { setNodeSelected }
    } = this.props;

    const { parentTreeNode, status } = this.state
    if (parentTreeNode.pid === '__root__') {
      this.setState({parentTreeNode: null})
    } else {
      if (status !== 'add') {
        const { values } = parentTreeNode
        setAllFormValue({
          'detailView': {
            rows:[{values}]
          }
        })
      } else {
        setAllFormValue({
          'detailView': {
            rows:[{values: this.state.beforeAddSelectedNode ? this.state.beforeAddSelectedNode.values : {}}]
          }
        })
      }
    }
    
    setFormStatus('detailView', 'browse')

    this.props.syncTree.setTreeEdit('qualitySpeciesTree', true)
    this.props.syncTree.setNodeDisable('qualitySpeciesTree', false)
    this.setState({disabledTreeSearch: false, disabledCheck: false, showSureCancelModel: false})

    setFormItemsDisabled('detailView', {'is_using': status === 'add' ? true : false})
    setButtonVisible([ 'SaveDetail', 'CancelSaveDetail' ], false)
  }
  // 是否显示停用
  changeCheck = checked => {
    this.setState({ checked, parentTreeNode: null }, () => {
      this.initSyncTree()
    });
    this.props.button.setButtonVisible({
      SaveDetail: false,
      CancelSaveDetail: false
    })
    this.props.form.EmptyAllFormValue('detailView')
  }
  // 头部按钮组事件函数
  pageButtonClick = () => {
    return {
      SaveDetail: this.formEditAfterEvent, // 保存
      CancelSaveDetail: this.prToCancel, // 取消保存
    }
  }
  // 向下递归👇 找到所有子节点pk
  findChildsPk = (node, arr) => {
    arr.push(node.refpk)
    if (node.children && node.children.length > 0) {
      let index = -1
      while (++index < node.children.length) {
        this.findChildsPk(node.children[index], arr)
      }
    }
  }
  // 向上递归👆 寻找父节点pk
  findParentsPk = (pid, arr) => {
    // debugger
    const pNode = this.props.syncTree.getSyncTreeValue('qualitySpeciesTree', pid)
    // console.log('pNode  ', pNode)
    if (pNode.values.is_using.value || pNode.pid === '__root__') return // 找到第一个已启用节点返回
    arr.push(pNode.refpk)
    this.findParentsPk(pNode.pid, arr)
  }
  // 表单编辑后事件 启用/停用
  detailViewAfterEvent = (props, formid, key, text, values, record) => {
    const { parentTreeNode, multiLang } = this.state // 当前选中节点
    if (key !== 'is_using' || !parentTreeNode) return
    const {
      /* button: { setButtonVisible }, */
      syncTree: { delNodeSuceess/* , editNodeSuccess */, getSyncTreeValue, editNodeSuccess },
      /* form: { EmptyAllFormValue } */
    } = this.props
    // 启用/停用
    if (text.value) { // 启用
      /***
       * 显示停用时才会有启用操作
       * 启用该节点 向上递归启用至 第一个为已启用状态的父级节点 或至根节点
       * **/
      const arr = []
      arr.push(parentTreeNode.refpk)
      // 未启用父节点pk
      this.findParentsPk(parentTreeNode.pid, arr)
      // console.log('arr11: ', arr)
      const pks = arr.join(',')
      requestApi.startProblem({
        pks,
        success: response => {
          let index = -1 
          while (++index < arr.length) {
            let currentNode = getSyncTreeValue('qualitySpeciesTree', arr[index])
            currentNode.values.is_using.value = true
            // console.log(currentNode.refname)
            const _name = currentNode.refname.substr(currentNode.refcode.length)
            currentNode.refname = `${currentNode.refcode} ${_name.replace(multiLang['7040-0005'], '').trim()}`
            editNodeSuccess('qualitySpeciesTree', currentNode)
          }
          
          this.props.syncTree.hideIcon("qualitySpeciesTree", this.state.rootPk, {
            delIcon:false,
            editIcon:false,
            addIcon:true
          })
        }
      })
    } else { // 停用
      const arr = []
      this.findChildsPk(parentTreeNode, arr)
      const pks = arr.join(',')
      requestApi.stopProblem({
        pks,
        success: response => {
          if (this.state.checked) {
            /***
             * 停用该节点
             * 非叶子节点停用其所有子节点
             * ****/
            let index = -1 
            while (++index < arr.length) {
              let currentNode = getSyncTreeValue('qualitySpeciesTree', arr[index])
              if (currentNode.values.is_using.value) {
                currentNode.refname += currentNode.refname.indexOf(multiLang['7040-0005']) > -1 ? '' : ' '+multiLang['7040-0005']
              }
              currentNode.values.is_using.value = false
              editNodeSuccess('qualitySpeciesTree', currentNode)
            }
          } else {
            /***
             * 删除该节点及其所有子节点
             * 清空右侧表单
             * **/
            delNodeSuceess("qualitySpeciesTree", parentTreeNode.refpk);
            this.setState({parentTreeNode: null})
          }

          this.props.syncTree.hideIcon("qualitySpeciesTree", this.state.rootPk, {
            delIcon:false,
            editIcon:false,
            addIcon:true
          })
        }
      })
    }
  }
  // 改
  clickEditIconEve = parentTreeNode => {
    requestApi.checkProblemTypes({
      id: parentTreeNode.refpk,
      success: data => {
        parentTreeNode.values.ts.value = data.data.ts

        const {form: {setAllFormValue, setFormStatus, setFormItemsDisabled} } = this.props
        setFormStatus('detailView', 'edit')

        this.props.syncTree.setTreeEdit('qualitySpeciesTree', false)
        this.props.syncTree.setNodeDisable('qualitySpeciesTree', true)
        this.setState({disabledTreeSearch: true, disabledCheck: true})

        setFormItemsDisabled('detailView', {'is_using': true})

        const detailViews = {}
        for (let attr in data.data) {
          if (attr === 'serious_level') {
            detailViews[attr] = {
              display: this.PROBLEM_TYPES[data.data[attr]],
              value: data.data[attr]
            }
          } else if (attr === 'is_using') {
            detailViews[attr] = {
              display: data.data[attr] === 'N' ? false : true,
              value: data.data[attr] === 'N' ? false : true
            }
          } else {
            detailViews[attr] = {
              display: data.data[attr],
              value: data.data[attr]
            }
          }
        }
        setAllFormValue({
          'detailView': {
            rows:[{values: detailViews}]
          }
        })

        this.setState({ parentTreeNode, status: 'edit' })
        this.props.button.setButtonVisible([ 'SaveDetail', 'CancelSaveDetail' ], true)
      }
    });
  };
  // 删
  clickDelIconEve = ({refpk, key, pid, refname, isleaf, values: {is_using}}) => {
    const {multiLang} = this.state
    if (!isleaf) return toast({title: multiLang['7040-0021'], color: 'warning'})

    if (is_using.value) {
      return toast({
        title: multiLang && multiLang['7040-0189'],
        color: "warning"
      });
    }

    this.setState({showDelModel: true, delPk: refpk})
  };
  // 确定删除
  sureDelHandler = () => {
    requestApi.delProblemType({
      refpk: this.state.delPk,
      success: response => {
        if (response.success) {
          const { syncTree: {delNodeSuceess} } = this.props
          const {multiLang} = this.state
          delNodeSuceess('qualitySpeciesTree', this.state.delPk)
          toast({title: `${multiLang['7040-0158']}${multiLang['7040-0159']}。`, color: 'success'})
          this.setState({showDelModel: false})
          this.props.form.EmptyAllFormValue('detailView')
        }
      }
    })
  }

  render() {
    const {
      syncTree: { createSyncTree },
      form: { createForm }
    } = this.props;

    const {
      clickDelIconEve,
      clickEditIconEve,
      clickAddIconEve,
      onSelectEve,
      changeCheck,
      pageButtonClick,
      detailViewAfterEvent,
      cancelSaveDetails,
      sureDelHandler
    } = this;

    const {checked, parentTreeNode, multiLang, disabledTreeSearch, disabledCheck, showSureCancelModel, showDelModel} = this.state

    return (
      <ProfileStyle id="quality-species" layout="treeTable" {...this.props}>
        <ProfileHead title={multiLang['7040-0017']}>
          <HeadCenterCustom>
            <NCCheckbox
              onChange={changeCheck}
              checked={checked}
              disabled={disabledCheck}
            >{multiLang['7040-0022']}</NCCheckbox>
          </HeadCenterCustom>
          <ButtonGroup
            area={'detailViewBtn'}
            buttonEvent={pageButtonClick}
          />
        </ProfileHead>
        <ProfileBody>
          <BodyLeft>
            {createSyncTree({
              treeId: "qualitySpeciesTree",
              needSearch: true,
              needEdit: true,
              defaultExpandAll: true,
              showModal: false,
              clickAddIconEve,
              clickEditIconEve,
              clickDelIconEve,
              onSelectEve,
              disabledSearch: false,
              showLine: true,
              disabledSearch: disabledTreeSearch,
              selectedForInit: true
            })}
          </BodyLeft>
          <BodyRight>
            <div style={{paddingTop: '30px', display: !!parentTreeNode ? 'block' : 'none'}}>
              {createForm("detailView", {
                onAfterEvent: detailViewAfterEvent
              })}
            </div>
          </BodyRight>
        </ProfileBody>

        {showSureCancelModel && promptBox({
          color: 'warning',
          title: multiLang && multiLang['7040-0009'],
          content: multiLang && multiLang['7040-0190'],
          noFooter: false,
          noCancelBtn: false,
          beSureBtnClick: () => {
            this.setState({showSureCancelModel: false})
            cancelSaveDetails()
          },
          cancelBtnClick: () => { this.setState({ showSureCancelModel: false }); },
          closeByClickBackDrop: true
        })}

        {showDelModel && promptBox({
          color: 'warning',
          title: multiLang && multiLang['7040-0025'],
          content: multiLang && multiLang['7040-0191'],
          noFooter: false,
          noCancelBtn: false,
          beSureBtnClick: () => {
            this.setState({showDelModel: false})
            sureDelHandler()
          },
          cancelBtnClick: () => { this.setState({ showDelModel: false }); },
          closeByClickBackDrop: true
        })}

      </ProfileStyle>
    );
  }
}

// localStorage.setItem("gzip", "0");
// localStorage.setItem("ShuntServerInfo", "{}");

QualitySpecies = createPage({mutiLangCode: '7040'})(QualitySpecies);
export default QualitySpecies;
