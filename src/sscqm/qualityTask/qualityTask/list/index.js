import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import QualityTaskList from './QualityTaskList'
ReactDOM.render(<QualityTaskList />, document.querySelector('#app'));