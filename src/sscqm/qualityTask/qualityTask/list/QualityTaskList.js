import { Component } from "react";
import { createPage, toast, base, getMultiLang, promptBox, createPageIcon } from "nc-lightapp-front";
const { /* NCSelect, NCModal, NCButton, */ NCAffix } = base;
import "./index.less";
import { initTemplate, requestApi } from "./events";

class QualityTaskList extends Component {
  constructor(props) {
    super(props);
    this.pagecode = ''
    this.state = {
      refpk: "",
      selectedList: [],
      showSampledBillDiscontModal: false,
      sampledBillValues: {},
      multiLang: {}
    };
    initTemplate.call(this, props);
  }
  componentWillMount() {
    // 缓存pagecode
    const {props: {
      getUrlParam, getSearchParam
    }} = this
    this.pagecode = getUrlParam('p') || getSearchParam('p')
  }
  componentDidMount() {
    getMultiLang({moduleId: 7040, domainName: 'sscqm',currentLocale: 'zh-CN', callback: (multiLang) => {
      this.setState({multiLang})
    }})
  }
  //查询按钮点击事件
  searchClick() {
    const { conditions } = this.props.search.getAllSearchData(
      "extendAttribute"
    );
    const param = {
      orgid: "", // 共享中心
      likeStr: "", // 模糊查询
      beginDate: "", // 开始日期
      endDate: "", // 结束日期
      taskState: "", // 任务状态
      pagecode: this.pagecode
    };
    conditions.forEach(ele => {
      if (ele.field === "rangePicker") {
        param.beginDate = ele.value.firstvalue;
        param.endDate = ele.value.secondvalue;
      } else {
        param[ele.field] = ele.value.firstvalue;
      }
    });
    requestApi.checktasks({
      data: param,
      success: data => {
        const {data: {QualityTaskList: {rows}}} = data
        this.props.table.setAllTableData("QualityTaskList", {
          areacode: "QualityTaskList",
          rows
        });
      }
    });
  }
  // 删除
  delRule = (values, index) => {
    const {multiLang} = this.state
    requestApi.delTask({
      id: values.id.value,
      success: res => {
        if (res.success) {
          toast({
            duration: 3,
            title: multiLang['7040-0039']
          });
          this.props.table.deleteTableRowsByIndex("QualityTaskList", index);
        }
      }
    });
  };
  // 启用
  enableRule = (values, idx) => {
    requestApi.enableRule({
      id: values.id.value,
      success: data => {
        if (data.success) {
          this.props.table.setValByKeyAndIndex(
            'QualityTaskList', 
            idx, 
            'task_status', 
            {value: 'ENABLED', display: null, scale: -1}
          )
        }
      }
    });
  };
  // 停用
  discontRule = (values, idx) => {
    if (values.task_status.value === "SAMPLED") {
      this.setState({
        showSampledBillDiscontModal: true,
        sampledBillValues: values
      });
      return;
    }
    requestApi.discontRule({
      id: values.id.value,
      success: data => {
        if (data.success) {
          this.props.table.setValByKeyAndIndex(
            'QualityTaskList', 
            idx, 
            'task_status', 
            {value: 'SAVED', display: null, scale: -1}
          )
        }
      }
    });
  };
  // 已抽取单据停用
  sampledBill = () => {
    requestApi.discontRule({
      id: this.state.sampledBillValues.id.value,
      success: data => {
        if (data.success) {
          this.searchClick();
          this.setState({ showSampledBillDiscontModal: false });
        }
      }
    });
  };
  // 保存查询区信息
  saveSearchInfo = () => {
    const { conditions } = this.props.search.getAllSearchData(
      "extendAttribute"
    );
    // 缓存查询区信息 qualityTaskSearchConditions
    sessionStorage.setItem(
      "qualityTaskSearchConditions",
      JSON.stringify(conditions)
    );
  };
  // 新增稽核任务
  addTask = () => {
    // 当前行信息 & 进入卡片的状态
    if (sessionStorage.getItem("qualityTaskInfo")) {
      sessionStorage.setItem("qualityTaskInfo", "");
    }
    sessionStorage.setItem("taskCreaterInfo", JSON.stringify(window.parent.GETBUSINESSINFO()));
    // 列表查询信息存储
    this.saveSearchInfo();
    // 跳转
    this.props.linkTo('/sscqm/qualityTask/qualityTask/card/index.html', {appcode: '70400103', pagecode: '70400103_card'})
  };
  // 详情概览
  getDetailBrowse = text => {
    // qualityTaskInfo 选中行信息 页面状态
    sessionStorage.setItem(
      "qualityTaskInfo",
      JSON.stringify({ text, statu: "browse" })
    );
    // 任务状态 taskState
    sessionStorage.setItem("taskState", JSON.stringify(text.task_status));
    // 查询区信息
    this.saveSearchInfo();
    this.props.linkTo('/sscqm/qualityTask/qualityTask/card/index.html', {appcode: '70400103', pagecode: '70400103_card'})
  };
  // 稽核任务修改
  getDetailEdit = text => {
    sessionStorage.setItem(
      "qualityTaskInfo",
      JSON.stringify({ text, statu: "edit" })
    );
    sessionStorage.setItem("taskState", JSON.stringify(text.task_status));
    this.saveSearchInfo();
    this.props.linkTo('/sscqm/qualityTask/qualityTask/card/index.html', {appcode: '70400103', pagecode: '70400103_card'})
  };
  // 获取分配内容
  getAssigns = values => {
    requestApi.getAssign({
      id: values.id.value,
      success: res => {
        // console.log('getAssign, ', res)
        this.setState({ selectedList: res.data });
      }
    });
  };
  // 列表双击
  onRowDoubleClick = record => {
    this.getDetailBrowse(record);
  }
  // 查询区钩子函数
  renderCompleteEvent = () => {
    const {multiLang} = this.state
    // 查询区
    const searchInfo = sessionStorage.getItem("qualityTaskSearchConditions");
    if (searchInfo && searchInfo !== 'undefined') {
      const searchDetails = JSON.parse(searchInfo);
      if (searchDetails) {
        if (searchDetails.length > 0) {
          this.props.search.setSearchValue("extendAttribute", searchDetails);
        }
        // 列表
        const param = {
          orgid: "",
          likeStr: "",
          beginDate: "",
          endDate: "",
          taskState: "",
          pagecode: this.pagecode
        };
        searchDetails.forEach(ele => {
          if (ele.field === "rangePicker") {
            param.beginDate = ele.value.firstvalue;
            param.endDate = ele.value.secondvalue;
          } else {
            param[ele.field] = ele.value.firstvalue;
          }
        });
        requestApi.checktasks({
          data: param,
          success: data => {
            const {data: {QualityTaskList: {rows}}} = data

            if (sessionStorage.getItem('qualityTaskAddedList')) {
              const addedList = JSON.parse(sessionStorage.getItem('qualityTaskAddedList'))
              sessionStorage.setItem('qualityTaskAddedList', '')
              // console.log('rows: ', rows)
              // console.log('addedList: ', addedList)

              let index = -1
              while(++index < addedList.length) {
                let eachAddedCode = addedList[index].code.value
                let _index = -1, onoff = false
                while(++_index < rows.length) {
                  let eachSearchCode = rows[_index].values.code.value
                  if(eachAddedCode === eachSearchCode) {
                    onoff = true // 重复
                    continue
                  }
                }
                if (!onoff) { // 不重复
                  rows.unshift({
                    status: '0',
                    values: addedList[index]
                  })
                }
              }
            }

            this.props.table.setAllTableData("QualityTaskList", {
              areacode: "QualityTaskList",
              rows
            });
          }
        });
      }
    } else {
      if (sessionStorage.getItem('qualityTaskAddedList')) {
        const addedList = JSON.parse(sessionStorage.getItem('qualityTaskAddedList'))
        sessionStorage.setItem('qualityTaskAddedList', '')
        const rows = []

        let index = -1
        while(++index < addedList.length) {
          rows.unshift({
            status: '0',
            values: addedList[index]
          })
        }

        this.props.table.setAllTableData("QualityTaskList", {
          areacode: "QualityTaskList",
          rows
        });
      }
    }
    // 是否点击取消按钮返回
    const taskCardBtnClickInfo = sessionStorage.getItem('taskCardBtnClickInfo')
    if (taskCardBtnClickInfo) {
      const {btn} = JSON.parse(taskCardBtnClickInfo)
      if (btn === 'cancel') { // 取消新增或取消修改
        toast({
          duration: 3,
          color: "warning",
          title: multiLang['7040-0164']
        });
      } else if (btn === 'del') { // 取消新增或取消修改
        toast({
          duration: 3,
          title: multiLang['7040-0165']
        });
      }
      sessionStorage.setItem('taskCardBtnClickInfo', '')
    }
  }

  render() {
    const {
      /* form, */
      button: { createButtonApp },
      search: { NCCreateSearch },
      table: { createSimpleTable }
    } = this.props;

    const { showSampledBillDiscontModal, multiLang } = this.state;

    const { onRowDoubleClick, renderCompleteEvent } = this

    return (
      <div id="quality-task">
        <NCAffix offsetTop={0}>
          <div id="quality-task-head">
            <div className="clearfix head--title">
              {createPageIcon()}
              <h1>{multiLang['7040-0038']}</h1>
              <div className="btn-right clearfix">
                {createButtonApp({
                  area: "QualityTaskHead0",
                  onButtonClick: this.addTask
                })}
              </div>
            </div>
            {NCCreateSearch("extendAttribute", {
              hideSearchCondition: false,
              hideBtnArea: false,
              showAdvBtn: false,
              clickSearchBtn: this.searchClick.bind(this),
              renderCompleteEvent
            })}
          </div>
        </NCAffix>
        <div id="quality-task-body">
          {createSimpleTable("QualityTaskList", {
            showIndex: true,
            showCheck: false,
            onRowDoubleClick
          })}
        </div>

        {showSampledBillDiscontModal && promptBox({
          color: 'warning',
          title: multiLang && multiLang['7040-0040'],
          content: multiLang && multiLang['7040-0041'],
          noFooter: false,
          noCancelBtn: false,
          beSureBtnClick: () => {
            this.setState({showSampledBillDiscontModal: false})
            this.sampledBill()
          },
          cancelBtnClick: () => { this.setState({ showSampledBillDiscontModal: false }); },
          closeByClickBackDrop: true
        })}
        
      </div>
    );
  }
}

// localStorage.setItem("ShuntServerInfo", "{}");
// localStorage.setItem("gzip", "0");

QualityTaskList = createPage({
  mutiLangCode: '7040'
})(QualityTaskList);
export default QualityTaskList;
