import { ajax } from "nc-lightapp-front";

export default {
  // 查询按钮接口
  checktasks: ({data, success}) => {
    ajax({
      url: `/nccloud/sscqm/checktask/CheckTaskQueryListAction.do`,
      method: "post",
      data,
      success
    });
  },
  // 删除
  delTask: ({ id, success }) => {
    ajax({
      url: `/nccloud/sscqm/checktask/CheckTaskDeleteAction.do`,
      method: "post",
      data: {id},
      success
    });
  },
  // 启用
  enableRule: ({ id, success }) => {
    ajax({
      url: `/nccloud/sscqm/checktask/CheckTaskStartAction.do`,
      method: "post",
      data: {id},
      success
    });
  },
  // 停用
  discontRule: ({ id, success }) => {
    ajax({
      url: `/nccloud/sscqm/checktask/CheckTaskStopAction.do`,
      method: "post",
      data: {id},
      success
    });
  },
  // 分配参照点击确定接口
  checktask: (id, content, success) => {
    ajax({
      url: `/nccloud/sscqm/checktask/CheckTaskSaveTaskContentAction.do`,
      method: "post",
      data: {
        taskid: id, content
      },
      success
    });
  },
  // 获取分配内容
  getAssign: ({ id, success }) => {
    ajax({
      url: `/nccloud/sscqm/checkcontent/CheckContentGetTreeByTaskAction.do`,
      method: "post",
      data: {id},
      success
    });
  }
};
