import index from "../../../../refers/refers/assign/index";
import requestApi from "./requestApi";
import { base, toast, getMultiLang } from 'nc-lightapp-front';
let { NCPopconfirm} = base;

export default function(props) {
  const _this = this;
  props.createUIDom({}, function({ button, template }) {
    getMultiLang({moduleId: 7040, domainName: 'sscqm',currentLocale: 'zh-CN', callback: (multiLang) => {
      // console.log('init_multiLang: ', multiLang)
      // console.log('template: ', template)
      template.extendAttribute.items[3].isMultiSelectedEnabled = true

      props.button.setButtons(button);
      props.meta.setMeta(template);

      template.QualityTaskList.items.push({
        label: multiLang['7040-0023'],
        attrcode: "opr",
        itemtype: "customer",
        visible: true,
        width: "210px",
        fixed: "right",
        render: (node, values, idx) => {
          return (
            <div>
              {/* 保存/已启用状态可分配 */}
              <div
                className="checktask-wrap" /* style={{display: (values.task_status.value === 'SAVED' || values.task_status.value === 'ENABLED') ? 'display' : 'none'}} */
              >
                {index({
                  clickContainer: (
                    <a
                      onClick={() => {
                        _this.getAssigns(values);
                      }}
                      className="operation-btna"
                      style={{ cursor: "pointer" }}
                    >{multiLang['7040-0024']}</a>
                  ),
                  isMultiSelectedEnabled: true,
                  value: _this.state.selectedList,
                  onChange: val => {
                    // console.log('val ', val)
                    let array = [];
                    for (let item of val) {
                      array.push({
                        name: item.refname,
                        id: item.refpk,
                        parentid: item.pid
                      });
                    }
                    requestApi.checktask(values.id.value, array, response => {
                      if (response.success) {
                        toast({
                          duration: 3,
                          title: multiLang['7040-0166']
                        });
                      }
                    });
                  }
                })}
              </div>
              {/* 保存状态可删除 */}
              {values.task_status.value === "SAVED" && (
                <NCPopconfirm  placement="top" content={multiLang['7040-0191']} onClose={() => {
                  _this.delRule(values, idx);
                }}>
                  <a className="billnoa">{multiLang['7040-0025']}</a>
                </NCPopconfirm>
              )}
              {/* 未启用状态可修改 */}
              {values.task_status.value === "SAVED" && (
                <a
                  className="billnoa"
                  onClick={() => {
                    _this.getDetailEdit(values);
                  }}
                >{multiLang['7040-0026']}</a>
              )}
              {/* 保存状态可启用 */}
              {values.task_status.value === "SAVED" && (
                <a
                  className="billnoa"
                  onClick={() => {
                    _this.enableRule(values, idx);
                  }}
                >{multiLang['7040-0027']}</a>
              )}
              {/* 已启用/已抽取 可取消启用 */}
              {(values.task_status.value === "ENABLED" ||
                values.task_status.value === "SAMPLED") && (
                <a
                  className="billnoa"
                  onClick={() => {
                    _this.discontRule(values, idx);
                  }}
                >{multiLang['7040-0028']}</a>
              )}
            </div>
          );
        }
      });

      // 任务编码
      props.table.setTableRender(
        "QualityTaskList",
        "code",
        (values, text, index) => (
          <a
            onClick={() => {
              _this.getDetailBrowse(text);
            }}
            className="operation-btna"
          >
            {text && text.code ? text.code.value : ""}
          </a>
        )
      );
      // 任务状态
      props.table.setTableRender(
        "QualityTaskList",
        "task_status",
        (values, text, index) => {
          return <span>{translateTaskStatu(text.task_status.value, multiLang)}</span>;
        }
      );
      // 任务属性
      props.table.setTableRender(
        "QualityTaskList",
        "task_property",
        (values, text, index) => (
          <span>
            {text && text.task_property
              ? text.task_property.value === "DAILY"
                ? multiLang['7040-0029']
                : multiLang['7040-0030']
              : ""}
          </span>
        )
      );

    }})
      
  });
}

function translateTaskStatu(statu, multiLang) {
  let text = "";
  if (statu === "SAVED") {
    text = multiLang['7040-0031'];
  } else if (statu === "ENABLED") {
    text = multiLang['7040-0032']
  } else if (statu === "SAMPLED") {
    text = multiLang['7040-0033']
  } else if (statu === "CONFIRMED") {
    text = multiLang['7040-0034']
  } else if (statu === "CHECKED") {
    text = multiLang['7040-0035']
  } else if (statu === "REPORTED") {
    text = multiLang['7040-0036']
  } else if (statu === "CLOSED") {
    text = multiLang['7040-0037']
  }
  return text;
}
