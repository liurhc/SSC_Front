
const MULTI_SELECTED_ENABLED_ARRAY = ['orgs', 'trade_type', 'bill_type', 'workinggroup']

export default function(props) {

  const _this = this;
  props.createUIDom({}, function({ button, template }) {
    // console.log('template: ', template)
    // console.log('btn: ', button)

    // 参照多选
    for (let attr in template) {
      if (attr === 'samplingProportionForm' || attr.indexOf('qmQualityTaskRule') > -1) {
        let idx = -1
        while (++idx < template[attr].items.length) {
          if(MULTI_SELECTED_ENABLED_ARRAY.indexOf(template[attr].items[idx].attrcode) > -1) {
            template[attr].items[idx].isMultiSelectedEnabled = true
          } 
        }
      }
    }
    
    props.button.setButtons(button);
    props.button.setButtonsVisible(['edit','del'], false)

    // 财务组织参照过滤
    template["samplingProportionForm"].items.map(
      (one, index) => {
        if (one.attrcode === "orgs") {
          one.queryCondition = () => {
            return {
              sscorgid: _this.props.form.getFormItemsValue("extendAttribute", "sscorgid").value,
              queryIds: '',
              TreeRefActionExt: 'nccloud.web.sscqm.sscbd.ref.sqlbuilder.OrgRefSqlBuilder'
            };
          };
        }
      }
    );
    template["qmQualityTaskRule0"].items.map((one, index) => {
      if (one.attrcode === "orgs") {
        one.queryCondition = () => {
          return {
            sscorgid: _this.props.form.getFormItemsValue("extendAttribute", "sscorgid").value,
            queryIds: _this.props.form.getFormItemsValue("samplingProportionForm", "orgs").value,
            TreeRefActionExt: 'nccloud.web.sscqm.sscbd.ref.sqlbuilder.OrgRefSqlBuilder'
          };
        };
      }
    });

    // 交易类型参照过滤
    template["samplingProportionForm"].items.map(
      (one, index) => {
        if (one.attrcode === "trade_type") {
          one.queryCondition = () => {
            return {
              pk_billtype: _this.props.form.getFormItemsValue(
                "samplingProportionForm",
                "bill_type"
              ).value,
              queryIds: '',
              GridRefActionExt: 'nccloud.web.sscqm.sscbd.ref.sqlbuilder.TradetypeRefSqlBuilder'
            };
          };
        }
      }
    );
    template["qmQualityTaskRule0"].items.map((one, index) => {
      if (one.attrcode === "trade_type") {
        one.queryCondition = () => {
          return {
            pk_billtype: _this.props.form.getFormItemsValue(
              "qmQualityTaskRule0",
              "bill_type"
            ).value,
            queryIds: _this.props.form.getFormItemsValue(
              "samplingProportionForm",
              "trade_type"
            ).value,
            GridRefActionExt: 'nccloud.web.sscqm.sscbd.ref.sqlbuilder.TradetypeRefSqlBuilder'
          };
        };
      }
    });

    // 单据类型参照过滤
    template["samplingProportionForm"].items.map(
      (one, index) => {
        if (one.attrcode === "bill_type") {
          one.queryCondition = () => {
            return {
              sscorgid: _this.props.form.getFormItemsValue(
                "extendAttribute",
                "sscorgid"
              ).value,
              queryIds: '',
              GridRefActionExt: 'nccloud.web.sscqm.sscbd.ref.sqlbuilder.BilltypeRefSqlBuilder'
            };
          };
        }
      }
    );
    template["qmQualityTaskRule0"].items.map((one, index) => {
      if (one.attrcode === "bill_type") {
        one.queryCondition = () => {
          return {
            sscorgid: _this.props.form.getFormItemsValue("extendAttribute", "sscorgid")
            .value,
            queryIds: _this.props.form.getFormItemsValue(
              "samplingProportionForm",
              "bill_type"
            ).value,
            GridRefActionExt: 'nccloud.web.sscqm.sscbd.ref.sqlbuilder.BilltypeRefSqlBuilder'
          };
        };
      }
    });

    // 作业组参照过滤
    template["samplingProportionForm"].items.map(
      (one, index) => {
        if (one.attrcode === "workinggroup") {
          one.queryCondition = () => {
            return {
              sscorgid: _this.props.form.getFormItemsValue(
                "extendAttribute",
                "sscorgid"
              ).value,
              queryIds: '',
              TreeRefActionExt: 'nccloud.web.sscqm.sscbd.ref.sqlbuilder.WorkinggroupRefSqlBuilder'
            };
          };
        }
      }
    );
    template["qmQualityTaskRule0"].items.map((one, index) => {
      if (one.attrcode === "workinggroup") {
        one.queryCondition = () => {
          return {
            sscorgid: _this.props.form.getFormItemsValue("extendAttribute", "sscorgid")
            .value,
            queryIds: _this.props.form.getFormItemsValue(
              "samplingProportionForm",
              "workinggroup"
            ).value,
            TreeRefActionExt: 'nccloud.web.sscqm.sscbd.ref.sqlbuilder.WorkinggroupRefSqlBuilder'
          };
        };
      }
    });

    props.meta.setMeta(template);

    props.form.setFormStatus('samplingProportionForm', 'edit')
    props.form.setFormStatus('extendAttribute', 'edit')
    props.form.setFormStatus('qmQualityTaskRule0', 'edit')

    props.button.setButtonVisible({
      "add": false,
      "start": false,
      "stop": false
    })

  });

}
