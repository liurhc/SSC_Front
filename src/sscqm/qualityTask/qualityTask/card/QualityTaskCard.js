import { Component } from "react";
import { createPage, toast, base, getMultiLang, promptBox, createPageIcon } from "nc-lightapp-front";
const { NCTabsControl, NCModal, NCButton, NCBackBtn, NCAffix } = base;
import "./index.less";
import { initTemplate, requestApi } from "./events";
import assign from "../../../refers/refers/assign/index";

let TASK_STATES = {
  // SAVED: "保存",
  // ENABLED: "已启用",
  // SAMPLED: "已抽取",
  // CONFIRMED: "已确认",
  // CHECKED: "已稽核",
  // REPORTED: "已报告",
  // CLOSED: "已关闭"
};

let TASK_PROPERTYS = {
  // DAILY: "日常",
  // SPECIAL: "专项"
};

class QualityTaskCard extends Component {
  constructor(props) {
    super(props);
    this.num = 0;
    this.state = {
      selectTab: -1,
      tabsConfig: [],
      currentTaskId: 0,
      currentTs: '',
      showDelModal: false,
      canShowDelIcon: true,
      selectedList: [],
      showAssignBtn: false,
      showSampledBillDiscontModal: false,
      sampledBillValues: {},
      multiLang: {},
      showCancelModel: false,
      addAfterView: null,
      showBackBtn: true
    };
    initTemplate.call(this, props);
  }
  componentDidMount() {
    // 多语
    getMultiLang({moduleId: 7040, domainName: 'sscqm',currentLocale: 'zh-CN', callback: (multiLang) => {
      TASK_STATES = {
        SAVED: multiLang['7040-0044'],
        ENABLED: multiLang['7040-0045'],
        SAMPLED: multiLang['7040-0046'],
        CONFIRMED: multiLang['7040-0047'],
        CHECKED: multiLang['7040-0048'],
        REPORTED: multiLang['7040-0049'],
        CLOSED: multiLang['7040-0050']
      }

      TASK_PROPERTYS = {
        DAILY: multiLang['7040-0051'],
        SPECIAL: multiLang['7040-0052']
      }

      this.setState({ multiLang, selectTab: 0 }, () => {
        this.setState(
          {
            tabsConfig: [
              { key: 0, text: `${multiLang['7040-0054']}1`, area: "qmQualityTaskRule0" }
            ]
          },
          () => {
            const qualityTaskInfo = sessionStorage.getItem("qualityTaskInfo");
            if (qualityTaskInfo && JSON.parse(qualityTaskInfo).text.id) { // 修改 / 预览
              const detailInfo = JSON.parse(qualityTaskInfo);
              if (detailInfo.statu === "browse") { // 浏览
                this.setState({canShowDelIcon: false, showBackBtn: true})
                this.setPageBrowse(detailInfo.text);
              } else if (detailInfo.statu === "edit") { //修改
                this.setState({canShowDelIcon: true, showBackBtn: false})
                this.setPageEdit(detailInfo.text);
              }
            } else { // 新增
              this.setState({canShowDelIcon: true, showBackBtn: false})
              this.setAllFormItemsDisabled(true)

              let month = Number(new Date().getMonth()) + 1
              const date = `${new Date().getFullYear()}-${month < 10 ? '0' + month : month}-${new Date().getDate()} 00:00:00`
              this.props.form.setFormItemsValue('extendAttribute',{'start_time':{display: date,value: date}})
            }
          }
        );
      });
    }})

    window.onbeforeunload = () => {
      if (this.props.form.getFormStatus('extendAttribute') === 'edit') return ''
    }
  }
  // 详情概览
  setPageBrowse = text => {
    this.getFormAllVal(text, "browse");
  };
  getFormAllVal = (text, statu) => {
    const id = text.id.value;
    requestApi.getDetail({
      id,
      success: res => {
        // console.log('response: ', res)
        if (res.success) {
          // main
          this.setAllFormValue(res.data, statu);

          const {extendAttribute: {extendAttribute: {rows: [
            extValues
          ]}}} = res.data
          const {values: {
            ts, id
          }} = extValues
          this.setState({
            currentTaskId: id.value,
            currentTs: ts.value
          })

          this.props.form.setFormItemsDisabled("extendAttribute", {
            sscorgid: true
          });
          // 默认编辑态 statu值为browse进入浏览态
          if (statu === "browse") {
            const {
              form: { setFormStatus }
            } = this.props;
            setTimeout(() => {
              setFormStatus("extendAttribute", "browse");
              setFormStatus("samplingProportionForm", "browse");
            }, 200)

            const {task_status} = res.data.extendAttribute.extendAttribute.rows[0].values

            this.props.button.setButtonVisible({
              "save": false,
              "cancel": false,
              "edit": task_status.value === "SAVED" ? true : false,
              "del": task_status.value === "SAVED" ? true : false,
              "addRule": false,

              'add': true,
              'start': task_status.value === "SAVED" ? true : false,
              'stop': (task_status.value === 'ENABLED' || task_status.value === 'SAMPLED') ? true : false
            })

            this.setState({showAssignBtn: true})
          }
        }
      }
    });
  };
  setAllFormValue = (data, statu) => {
    const {multiLang} = this.state
    // 表头数据设置
    const extendValues = data.extendAttribute.extendAttribute.rows[0].values
    // 任务状态转义
    extendValues.task_status.display = TASK_STATES[extendValues.task_status.value]
    // 任务属性转义
    extendValues.task_property.display = TASK_PROPERTYS[extendValues.task_property.value]
    this.props.form.setAllFormValue({
      extendAttribute: {
        rows: [{ values: extendValues }]
      }
    });

    // 抽样
    const sampValues = data.samplingProportionForm.extendAttribute.rows[0].values
    this.props.form.setAllFormValue({
      samplingProportionForm: {
        rows: [{ values: sampValues }]
      }
    });

    // 分层
    const taskRuleKeys = Object.keys(data).filter(each => each!= 'extendAttribute' && each != 'samplingProportionForm')
    // 分层规则模板深拷贝
    if (taskRuleKeys.length > 1) {
      // tabs
      let index = -1;
      let tabsConfig = this.state.tabsConfig;
      let moreConfig = [];
      while (++index < taskRuleKeys.length - 1) {
        this.num++;
        const currentAreaCode = "qmQualityTaskRule" + this.num;
        const obj = {
          key: this.num,
          text: multiLang['7040-0054'] + (this.num + 1),
          area: currentAreaCode
        };
        tabsConfig.push(obj);
        moreConfig.push(obj);
      }
      this.setState({ tabsConfig });
      // meta
      let meta = this.props.meta.getMeta();
      let idx = -1;
      while (++idx < moreConfig.length) {
        let newArea = this.deepCopy(meta.qmQualityTaskRule0);
        newArea.code = moreConfig[idx].area;
        meta[moreConfig[idx].area] = newArea;
      }
      this.props.meta.setMeta(meta);
    }
    setTimeout(() => {
      // 分层规则数据设置
      if (Object.keys(data).length > 2) {
        // console.log('taskRuleKeys: ', Object.keys(data))
        // console.log('tabsConfig: ', this.state.tabsConfig)
        let _idx = -1;
        while (++_idx < this.state.tabsConfig.length) {
          const currentArea = this.state.tabsConfig[_idx].area;
          const values = data[currentArea].extendAttribute.rows[0].values
          this.props.form.setAllFormValue({
            [currentArea]: {
              rows: [{ values }]
            }
          });
          if (statu === "browse") {
            this.props.form.setFormStatus(currentArea, "browse");
          } else {
            this.props.form.setFormStatus(currentArea, "edit");
          }
        }
      } else {
        this.tabsConfigSplice(0)
      }
    }, 200)
  };
  deepCopy(p, c) {
    var c = c || {};
    for (var i in p) {
      if (p[i] && typeof p[i] === "object" ) {
        c[i] = Object.prototype.toString.call(p[i]) === "[object Array]" ? [] : {};
        this.deepCopy(p[i], c[i]);
      } else {
        c[i] = p[i];
      }
    }
    return c;
  }
  checkTab(selectTab) {
    this.setState({ selectTab });
  }
  // 抽样范围表单编辑后事件
  samplingProportionFormAfterEvent = (props, formid, key, text, values, record) => {
    const {tabsConfig, multiLang} = this.state
    const {form: {getFormItemsValue, setFormItemsValue}} = props

    if (key === 'orgs') { // 财务组织
      let onOff = false
      let index = -1
      while (++index < tabsConfig.length) {
        const eachInfo = getFormItemsValue(tabsConfig[index].area, "orgs")
        if (eachInfo && eachInfo.value) {
          setFormItemsValue(tabsConfig[index].area, {
            'orgs':{value: null, display: null}
          })
          if (!onOff) onOff = true
        }
      }

      if (onOff) {
        toast({
          color: "warning",
          title: multiLang['7040-0066'],
          duration: 3
        });
      }
    } else if (key === 'bill_type') { // 单据类型
      let onOff = false
      let index = -1
      while (++index < tabsConfig.length) {
        const eachInfo = getFormItemsValue(tabsConfig[index].area, "bill_type")
        if (eachInfo && eachInfo.value) {
          setFormItemsValue(tabsConfig[index].area, {
            'bill_type':{value: null, display: null}
          })
          if (!onOff) onOff = true
        }
      }

      if (onOff) {
        toast({
          color: "warning",
          title: multiLang['7040-0168'],
          duration: 3
        });
      }

      setFormItemsValue('samplingProportionForm', {
        'trade_type':{value: null, display: null}
      })
    }
  }
  // 分层规则表单编辑后事件
  eachRuleFormAfterEv = (props, formid, key, text, values, record) => {
    const {form: {setFormItemsValue}} = props

    if (key === 'bill_type') { // 单据类型
      setFormItemsValue(formid, {
        'trade_type':{value: null, display: null}
      })
    }
  }
  // 点击NCBackBtn回调
  goBackHandler = () => {
    sessionStorage.setItem('taskCardBtnClickInfo', JSON.stringify({
      btn: 'goBack'
    }))

    this.props.linkTo('/sscqm/qualityTask/qualityTask/list/index.html', {appcode: '70400103', pagecode: '70400103_list'})
  }
  // 修改
  setPageEdit = text => {
    this.getFormAllVal(text, "edit");
  };
  // 删除分层规则
  tabsConfigSplice = idx => {
    const { tabsConfig } = this.state;
    tabsConfig.splice(idx, 1);
    this.setState({ tabsConfig, selectTab: 0 });
  };
  //取消按钮事件
  cancelButtonClick = () => {
    const qualityTaskInfo = sessionStorage.getItem("qualityTaskInfo");
    let statu = "add";
    if (qualityTaskInfo) {
      statu = JSON.parse(qualityTaskInfo).statu;
    }

    if (statu != 'add') {
      const {task_status} = this.props.form.getAllFormValue("extendAttribute").rows[0].values

      this.props.button.setButtonVisible({
        start: task_status.value === "SAVED" ? true : false,
        stop: (task_status.value === 'ENABLED' || task_status.value === 'SAMPLED') ? true : false,
        add: true
      })
      this.setState({showAssignBtn: true})
    }

    if (statu === 'add') { // 取消新增
      sessionStorage.setItem('taskCardBtnClickInfo', JSON.stringify({
        btn: 'cancel'
      }))

      const {addAfterView} = this.state
      if (addAfterView) {
        const {multiLang} = this.state
        this.setState({ multiLang, selectTab: 0 }, () => {
          this.setState(
            {
              tabsConfig: [
                { key: 0, text: `${multiLang['7040-0054']}1`, area: "qmQualityTaskRule0" }
              ]
            },
            () => {
              const detailInfo = addAfterView
              // if (detailInfo.statu === "browse") { // 浏览
                this.setState({canShowDelIcon: false})
                this.setPageBrowse(detailInfo.text);
              // } else if (detailInfo.statu === "edit") { //修改
              //   this.setState({canShowDelIcon: true})
              //   this.setPageEdit(detailInfo.text);
              // }
              this.setState({showCancelModel: false, showBackBtn: true})
            }
          );
        });
      } else {
        window.onbeforeunload = () => {}
        this.props.linkTo('/sscqm/qualityTask/qualityTask/list/index.html', {appcode: '70400103', pagecode: '70400103_list'})
      }

      return
    } else { // 取消修改 卡片进入浏览态
      this.props.form.setFormItemsDisabled("extendAttribute", {
        sscorgid: true
      });
      const {
        form: { setFormStatus }
      } = this.props;
      // 表头
      setFormStatus("extendAttribute", "browse");
      // 抽样规则
      setFormStatus("samplingProportionForm", "browse");
      this.setState({showBackBtn: true})
      //分层规则
      let _idx = -1;
      while (++_idx < this.state.tabsConfig.length) {
        this.props.form.setFormStatus(this.state.tabsConfig[_idx].area, "browse");
      }
      // 按钮
      this.props.button.setButtonVisible({
        save: false,
        addRule: false,
        edit: true,
        del: true,
        cancel: false,
      })
      this.setState({canShowDelIcon: false})

      const {id} = this.props.form.getAllFormValue("extendAttribute").rows[0].values
      requestApi.getDetail({
        id: id.value,
        success: res => {
          if (res.success) {
            // main
            this.setAllFormValueAfterSave(res);
          }
        }
      });
    }
  }
  // 删除任务
  handledel(props) {
    const taskidValues = props.form.getFormItemsValue("extendAttribute","id")
    const taskid = (taskidValues && taskidValues.value) ? taskidValues.value : this.state.currentTaskId;
    requestApi.delTask({
      taskid,
      success: res => {
        if (res.success) {
          toast({
            duration: 3,
            title: this.state.multiLang['7040-0064']
          });
          sessionStorage.setItem('taskCardBtnClickInfo', JSON.stringify({
            btn: 'del'
          }))
          this.props.linkTo('/sscqm/qualityTask/qualityTask/list/index.html', {appcode: '70400103', pagecode: '70400103_list'})
        }
      }
    });
  }
  //新增按钮事件
  newIncreaseClick() {
    const {multiLang} = this.state

    let tabsConfig = this.state.tabsConfig;
    if (tabsConfig.length === 5) {
      toast({
        color: "warning",
        title: multiLang['7040-0056']
      });
      return;
    }
    this.num++;
    const currentAreaCode = "qmQualityTaskRule" + this.num;
    tabsConfig.push({
      key: this.num,
      text: multiLang['7040-0054'] + (this.num + 1),
      area: currentAreaCode
    });
    this.setState({ tabsConfig });
    let meta = this.props.meta.getMeta();
    //深拷贝
    //深拷贝一份分层规则1的模版，并让模版的moduleId和code相等
    let newArea = this.deepCopy(meta.qmQualityTaskRule0);
    newArea.code = currentAreaCode;
    meta[currentAreaCode] = newArea;
    for (let item of tabsConfig) {
      // 参照过滤
      meta[item.area].items.map((one, index) => {
        if (one.attrcode === "trade_type") {
          one.queryCondition = () => {
            return {
              pk_billtype: this.props.form.getFormItemsValue(
                item.area,
                "bill_type"
              ).value,
              queryIds: this.props.form.getFormItemsValue(
                "samplingProportionForm",
                "trade_type"
              ).value,
              GridRefActionExt: 'nccloud.web.sscqm.sscbd.ref.sqlbuilder.TradetypeRefSqlBuilder'
            };
          };
        }
        if (one.attrcode === "orgs") {
          one.queryCondition = () => {
            return {
              sscorgid: this.props.form.getFormItemsValue(
                "extendAttribute",
                "sscorgid"
              ).value,
              queryIds: this.props.form.getFormItemsValue("samplingProportionForm", "orgs").value,
              TreeRefActionExt: 'nccloud.web.sscqm.sscbd.ref.sqlbuilder.OrgRefSqlBuilder'
            };
          };
        }
        if (one.attrcode === "bill_type") {
          one.queryCondition = () => {
            return {
              sscorgid: this.props.form.getFormItemsValue(
                "extendAttribute",
                "sscorgid"
              ).value,
              queryIds: this.props.form.getFormItemsValue(
                "samplingProportionForm",
                "bill_type"
              ).value,
              GridRefActionExt: 'nccloud.web.sscqm.sscbd.ref.sqlbuilder.BilltypeRefSqlBuilder'
            };
          };
        }
        if (one.attrcode === "workinggroup") {
          one.queryCondition = () => {
            return {
              sscorgid: this.props.form.getFormItemsValue(
                "extendAttribute",
                "sscorgid"
              ).value,
              queryIds: this.props.form.getFormItemsValue(
                "samplingProportionForm",
                "workinggroup"
              ).value,
              TreeRefActionExt: 'nccloud.web.sscqm.sscbd.ref.sqlbuilder.WorkinggroupRefSqlBuilder'
            };
          };
        }
      });
    }
    this.props.meta.setMeta(meta);

    for (let item of tabsConfig) {
      this.props.form.setFormStatus(item.area, "edit");
    }
    this.setState({showBackBtn: false})
  }
  // 修改按钮回调事件
  handleEdit = () => {
    this.setState({canShowDelIcon: true, showAssignBtn: false})
    const {
      form: { setFormStatus }
    } = this.props;
    setFormStatus("extendAttribute", "edit");
    setFormStatus("samplingProportionForm", "edit");
    let index = -1;
    while (++index < this.state.tabsConfig.length) {
      setFormStatus(this.state.tabsConfig[index].area, "edit");
    }
    this.setState({showBackBtn: false})
    this.props.button.setButtonVisible({
      save: true,
      cancel: true,
      edit: false,
      del: false,
      addRule: true,
      start: false,
      stop: false,
      add: false
    });

    const qualityTaskInfo = sessionStorage.getItem('qualityTaskInfo') ? JSON.parse(sessionStorage.getItem('qualityTaskInfo')) : { text: {} }
    qualityTaskInfo.statu = 'edit'
    sessionStorage.setItem(
      "qualityTaskInfo",
      JSON.stringify(qualityTaskInfo)
    );
  };
  //保存按钮事件
  saveButtonClick(props, key) {
    const {
      form: { getAllFormValue, isCheckNow, getFormItemsValue }
    } = props;
    const { multiLang } = this.state
    const param = {}; // 入参
    // 必输项校验
    if (!isCheckNow("extendAttribute") || !isCheckNow("samplingProportionForm"))
      return;
    let _idx = -1;
    while (++_idx < this.state.tabsConfig.length) {
      if (!isCheckNow(this.state.tabsConfig[_idx].area)) return;
    }
    // 抽样比例 分层比例校验
    const sampleRateVal = Number(
      getFormItemsValue("extendAttribute", "sample_rate").value
    );
    if (isNaN(sampleRateVal))
      return toast({
        duration: 3,
        color: "warning",
        title: multiLang['7040-0058']
      });
    if (sampleRateVal > 100)
      return toast({
        duration: 3,
        color: "warning",
        title: multiLang['7040-0059']
      });
    if (sampleRateVal <= 0)
      return toast({
        duration: 3,
        color: "warning",
        title: multiLang['7040-0169']
      });
    _idx = -1;
    while (++_idx < this.state.tabsConfig.length) {
      const eachTotalRateVal = Number(
        getFormItemsValue(this.state.tabsConfig[_idx].area, "total_rate").value
      );
      if (isNaN(eachTotalRateVal))
        return toast({
          duration: 3,
          color: "warning",
          title: multiLang['7040-0060']
        });
      if (eachTotalRateVal > 100)
        return toast({
          duration: 3,
          color: "warning",
          title: multiLang['7040-0061']
        });
      if (eachTotalRateVal <= 0)
        return toast({
          duration: 3,
          color: "warning",
          title: multiLang['7040-0170']
        });
    }
    // 表头
    param.extendAttribute = {
      extendAttribute: { rows: getAllFormValue("extendAttribute").rows}
    }
    // 抽样范围
    param.samplingProportionForm = {
      extendAttribute: { rows: getAllFormValue("samplingProportionForm").rows}
    }
    // 分层规则
    if (this.state.tabsConfig.length > 0) {
      let taskRuleAreas = this.state.tabsConfig.map(ele => ele.area);
      taskRuleAreas.forEach((eachArea, _idx_) => {
        param[`qmQualityTaskRule${_idx_}`] = {
          extendAttribute: { rows: getAllFormValue(eachArea).rows }
        }
      });
    }
    // return console.log('save_param: ', param)

    requestApi.qualityTaskCardSave({
      data: param,
      success: data => {
        // console.log('response: ', data.data)
        if (!data.success) {
          toast({
            duration: 3,
            color: "danger",
            title: multiLang['7040-0062'],
            content: data.error.message
          });
          return;
        }
        this.setState({canShowDelIcon: false})
        this.savedSuccess(data);

        if (!sessionStorage.getItem("qualityTaskInfo")) { // 新增时 缓存已新增数据
          const list = sessionStorage.getItem('qualityTaskAddedList')
                      ? JSON.parse(sessionStorage.getItem('qualityTaskAddedList'))
                      : []
          list.push(data.data.extendAttribute.extendAttribute.rows[0].values)
          sessionStorage.setItem('qualityTaskAddedList', JSON.stringify(list))
        }
      }
    });
  }
  // 保存成功回调
  savedSuccess = data => {
    const {task_status} = data.data.extendAttribute.extendAttribute.rows[0].values
    const {
      form: { setFormStatus }
    } = this.props;
    setFormStatus("extendAttribute", "browse");
    setFormStatus("samplingProportionForm", "browse");
    let index = -1;
    while (++index < this.state.tabsConfig.length) {
      setFormStatus(this.state.tabsConfig[index].area, "browse");
    }
    this.setState({showBackBtn: true})
    this.props.button.setButtonVisible({
      save: false,
      cancel: false,
      edit: true,
      del: true,
      addRule: false,

      start: task_status.value === "SAVED" ? true : false,
      stop: (task_status.value === 'ENABLED' || task_status.value === 'SAMPLED') ? true : false,
      add: true
    });

    this.setState({showAssignBtn: true})

    toast({ duration: 3, title: this.state.multiLang['7040-0063'] });

    this.setAllFormValueAfterSave(data, false)
  };
  // 保存后刷新数据
  setAllFormValueAfterSave = ({ data }) => {
    // 表头数据设置
    const extendValues = data.extendAttribute.extendAttribute.rows[0].values
    // 任务状态转义
    extendValues.task_status.display = TASK_STATES[extendValues.task_status.value]
    // 任务属性转义
    extendValues.task_property.display = TASK_PROPERTYS[extendValues.task_property.value]
    this.props.form.setAllFormValue({
      extendAttribute: {
        rows: [{ values: extendValues }]
      }
    });

    // 抽样
    const sampValues = data.samplingProportionForm.extendAttribute.rows[0].values
    this.props.form.setAllFormValue({
      samplingProportionForm: {
        rows: [{ values: sampValues }]
      }
    });

    // 分层
    const keys = Object.keys(data).filter(e => e !== 'extendAttribute' && e !== 'samplingProportionForm')
    // debugger
    keys.sort((a, b) => {
      return Number(a[a.length - 1]) - Number(b[b.length - 1])
    })
    // debugger
    let _idx = -1;
    while (++_idx < this.state.tabsConfig.length) {
      const currentArea = keys[_idx].area;
      const values = data[keys[_idx]].extendAttribute.rows[0].values
      this.props.form.setAllFormValue({
        [currentArea]: {
          rows: [{ values }]
        }
      });
    }

    // sessionStorage.setItem("qualityTaskInfo", JSON.stringify({
    //   text: {},
    //   statu: 'edit'
    // }));
  };
  // 设置除 共享服务中心 外所有表单项置灰
  setAllFormItemsDisabled = disabled => {
    const {form: {
      EmptyAllFormValue,
      setFormItemsDisabled,
      setFormItemsValue
    }} = this.props
    // 表头
    setFormItemsDisabled('extendAttribute',{
      'code': disabled,
      'name': disabled,
      'task_property': disabled,
      'start_time': disabled,
      'sample_rate': disabled,
      'remark': disabled
    })
    // 抽样范围
    setFormItemsDisabled('samplingProportionForm',{
      'start_bill_date': disabled,
      'end_bill_date': disabled,
      'start_amount': disabled,
      'end_amount': disabled,
      'orgs': disabled,
      'bill_type': disabled,
      'trade_type': disabled,
      'workinggroup': disabled
    })
    // 分层规则
    let index = -1
    while (++index < this.state.tabsConfig.length) {
      setFormItemsDisabled(this.state.tabsConfig[index].area,{
        'start_bill_date': disabled,
        'end_bill_date': disabled,
        'start_amount': disabled,
        'end_amount': disabled,
        'orgs': disabled,
        'bill_type': disabled,
        'trade_type': disabled,
        'workinggroup': disabled,
        'total_rate': disabled
      })
    }
    // 清空表单数据并设置默认数据
    setFormItemsValue('extendAttribute',{
      'code':{value: null, display: null},
      'name':{value: null, display: null},
      'task_property':{value: null, display: null},
      // 'start_time':{value: null, display: null},
      'sample_rate':{value: null, display: null},
      'remark':{value: null, display: null}
    })
    EmptyAllFormValue('samplingProportionForm')
    index = -1
    while (++index < this.state.tabsConfig.length) {
      const cArea = this.state.tabsConfig[index].area
      EmptyAllFormValue(cArea)
      // 分层比例默认100
      if(disabled) {
        setFormItemsValue(
          cArea,
          {
            'total_rate': {
              value: '100',
              display: '100'
            }
          }
        )
      }
    }
    // 创建人默认赋值
    const creater = JSON.parse(sessionStorage.getItem("taskCreaterInfo"))
    setFormItemsValue(
      'extendAttribute',
      {
        'creater': {
          value: creater.userId,
          display: creater.userName
        }
      }
    )
  }
  // 表头编辑后事件
  extendAttributeFormAfterEv = (props, formid, key, text, values, record) => {
    if (key === 'sscorgid') {
      if (text && text.value) {
        this.setAllFormItemsDisabled(false)
      } else {
        this.setAllFormItemsDisabled(true)
      }
    }
  }
  // 获取分配内容
  getAssigns = () => {
    const {id: {value}} = this.props.form.getAllFormValue("extendAttribute").rows[0].values
    requestApi.getAssign({
      id: value,
      success: res => {
        this.setState({ selectedList: res.data });
      }
    });
  };
  // 确定分配
  sureAssign = val => {
    // console.log('val ', val)
    let array = [];
    for (let item of val) {
      array.push({
        name: item.refname,
        id: item.refpk,
        parentid: item.pid
      });
    }

    const {id: {value}} = this.props.form.getAllFormValue("extendAttribute").rows[0].values

    requestApi.checktask(value, array, response => {
      if (response.success) {
        toast({
          duration: 3,
          title: this.state.multiLang['7040-0171']
        });
      }
    });
  }
  // 启用
  enableRule = () => {
    const {id: {value}} = this.props.form.getAllFormValue("extendAttribute").rows[0].values

    requestApi.enableRule({
      id: value,
      success: ({data, success}) => {
        // console.log('enableRule Response: ', data)
        if (success) {
          this.setAllFormValueAfterSave({data})

          this.props.button.setButtonVisible({
            start: false,
            stop: true,
            edit: false,
            del: false
          })
        }
      }
    });
  };
  // 停用
  discontRule = () => {
    const {id, task_status} = this.props.form.getAllFormValue("extendAttribute").rows[0].values

    if (task_status.value === "SAMPLED") {
      this.setState({
        showSampledBillDiscontModal: true,
        sampledBillValues: {id: id.value}
      });
      return;
    }
    requestApi.discontRule({
      id: id.value,
      success: data => {
        // console.log('discontRule Res: ', data.data)
        if (data.success) {
          this.setAllFormValueAfterSave(data)

          this.props.button.setButtonVisible({
            start: true,
            stop: false,
            edit: true,
            del: true
          })
        }
      }
    });
  };
  // 已抽取单据停用
  sampledBill = () => {
    requestApi.discontRule({
      id: this.state.sampledBillValues.id,
      success: data => {
        if (data.success) {
          this.setAllFormValueAfterSave(data)

          this.props.button.setButtonVisible({
            start: true,
            stop: false
          })

          this.setState({ showSampledBillDiscontModal: false })
        }
      }
    });
  };
  // 卡片新增
  cardAddHandle = () => {
    if (sessionStorage.getItem("qualityTaskInfo")) {
      this.setState({addAfterView: JSON.parse(sessionStorage.getItem("qualityTaskInfo"))})
      sessionStorage.setItem("qualityTaskInfo", "");
    }

    sessionStorage.setItem("taskCreaterInfo", JSON.stringify(window.parent.GETBUSINESSINFO()));

    this.setState({canShowDelIcon: true})

    this.props.form.setFormStatus('samplingProportionForm', 'edit')
    this.props.form.setFormStatus('extendAttribute', 'edit')
    this.setState({showBackBtn: false})

    this.props.form.EmptyAllFormValue('extendAttribute')
    this.props.form.EmptyAllFormValue('samplingProportionForm')

    // 表头
    this.props.form.setFormItemsDisabled('extendAttribute',{
      'code': true,
      'name': true,
      'task_property': true,
      'start_time': true,
      'sample_rate': true,
      'remark': true,
      'sscorgid': false
    })
    // 抽样范围
    this.props.form.setFormItemsDisabled('samplingProportionForm',{
      'start_bill_date': true,
      'end_bill_date': true,
      'start_amount': true,
      'end_amount': true,
      'orgs': true,
      'bill_type': true,
      'trade_type': true,
      'workinggroup': true
    })

    this.setState({ selectTab: 0, showAssignBtn: false }, () => {
      this.setState(
        {
          tabsConfig: [
            { key: 0, text: `${this.state.multiLang['7040-0054']}1`, area: "qmQualityTaskRule0" }
          ]
        },
        () => {
          // 分层规则
          let index = -1
          while (++index < this.state.tabsConfig.length) {
            this.props.form.setFormItemsDisabled(this.state.tabsConfig[index].area,{
              'start_bill_date': true,
              'end_bill_date': true,
              'start_amount': true,
              'end_amount': true,
              'orgs': true,
              'bill_type': true,
              'trade_type': true,
              'workinggroup': true,
              'total_rate': true
            })
            this.props.form.setFormStatus(this.state.tabsConfig[index].area, 'edit')
            this.props.form.EmptyAllFormValue(this.state.tabsConfig[index].area)
          }

          let month = Number(new Date().getMonth()) + 1
          const date = `${new Date().getFullYear()}-${month < 10 ? '0' + month : month}-${new Date().getDate()} 00:00:00`
          this.props.form.setFormItemsValue('extendAttribute',{'start_time':{display: date,value: date}})
        }
      );
    });

    this.props.button.setButtonVisible({
      save: true,
      cancel: true,
      edit: false,
      del: false,
      addRule: true,
      start: false,
      stop: false,
      add: false
    });
  }

  render() {
    const {
      form,
      button: { createButtonApp }
    } = this.props;
    const { createForm } = form;

    const { cardAddHandle, tabsConfigSplice, samplingProportionFormAfterEvent, goBackHandler, extendAttributeFormAfterEv, getAssigns, sureAssign, enableRule, discontRule, sampledBill, eachRuleFormAfterEv, cancelButtonClick } = this;

    const { tabsConfig, showDelModal, canShowDelIcon, selectedList, showAssignBtn, showSampledBillDiscontModal, multiLang, showCancelModel, showBackBtn } = this.state;

    let getShowList = tab => {
      if (this.state.selectTab == tab) {
        return "show";
      } else {
        return "hide";
      }
    };

    let pageStatu = "add";
    if (sessionStorage.getItem("qualityTaskInfo")) {
      pageStatu = JSON.parse(sessionStorage.getItem("qualityTaskInfo")).statu;
    }

    return (
      <div id="quality-task">
        <NCAffix offsetTop={0}>
          <div id="quality-task-head">
            <div className="clearfix quality-head--wrap">
              <NCBackBtn
                style={{
                  float: 'left',
                  marginTop: '8px',
                  display: showBackBtn ? 'block' : 'none'
                }}
                onClick={ goBackHandler }
              ></NCBackBtn>
              {/* { !showBackBtn && createPageIcon()} */}
              { createPageIcon() }
              <h1>{multiLang['7040-0053']}</h1>
              <div className="btn-right clearfix">
                {createButtonApp({
                  area: "QualityTaskHead1",
                  onButtonClick: (props, key) => {
                    switch (key) {
                      case "save":
                        this.saveButtonClick(props, "save");
                        break;
                      case "edit":
                        this.handleEdit();
                        break;
                      case "del":
                        this.setState({ showDelModal: true })
                        break;
                      case "add":
                        cardAddHandle()
                        break;
                      case "start":
                        enableRule()
                        break;
                      case "stop":
                        discontRule()
                        break;
                      default: // 取消
                        // this.cancelButtonClick()
                        this.setState({showCancelModel: true})
                        break;
                    }
                  }
                })}
                {/* 分配 */}
                { showAssignBtn && assign({
                  clickContainer: (
                    <NCButton
                      onClick={getAssigns}
                    >{multiLang['7040-0174']}</NCButton>
                  ),
                  isMultiSelectedEnabled: true,
                  value: selectedList,
                  onChange: sureAssign
                })}
              </div>
            </div>
          </div>
        </NCAffix>
        <div id="quality-task-body">
          {createForm("extendAttribute", {
            onAfterEvent: extendAttributeFormAfterEv
          })}

          {/* 抽样范围 */}
          <div className="lightapp-component-TabLi">
            <div className="tab_title active">
              <p>{multiLang['7040-0067']}</p>
              <p />
            </div>
          </div>
          <div>{createForm("samplingProportionForm", {
            onAfterEvent: samplingProportionFormAfterEvent
          })}</div>

          {/* 分层规则 */}
          <div className="tabs-wrap">
            <NCTabsControl defaultKey={this.state.selectTab}>
              {tabsConfig.map((item, index) => {
                let idx = index;
                return (
                  <div
                    key={item.key}
                    clickFun={() => {
                      // this.checkTab(item.key);
                      this.checkTab(idx);
                    }}
                  >
                    {item.text}
                    {canShowDelIcon && pageStatu !== "browse" && idx === tabsConfig.length - 1 && (
                      <i
                        onClick={ev => {
                          ev = ev || window.event;
                          ev.cancelBubble = true;
                          ev.stopPropagation();
                          tabsConfigSplice(idx);
                        }}
                        className="iconfont icon-guanbi"
                      />
                    )}
                  </div>
                );
              })}
            </NCTabsControl>

            <div className="btn-right">
              {createButtonApp({
                area: "rule",
                onButtonClick: this.newIncreaseClick.bind(this)
              })}
            </div>
          </div>

          {tabsConfig.map((item, index) => {
            return (
              <div className={getShowList(index)} key={index}>
                {createForm(item.area, {
                  onAfterEvent: eachRuleFormAfterEv
                })}
              </div>
            );
          })}
        </div>

        {/* 删除 */}
        {showDelModal && promptBox({
          color: 'warning',
          title: multiLang && multiLang['7040-0106'],
          content: multiLang && multiLang['7040-0191'],
          noFooter: false,
          noCancelBtn: false,
          beSureBtnClick: () => {
            this.setState({showDelModal: false})
            this.handledel(this.props)
          },
          cancelBtnClick: () => { this.setState({ showDelModal: false }); },
          closeByClickBackDrop: true
        })}

        {/* 取消 */}
        {showCancelModel && promptBox({
          color: 'warning',
          title: multiLang && multiLang['7040-0043'],
          content: multiLang && multiLang['7040-0190'],
          noFooter: false,
          noCancelBtn: false,
          beSureBtnClick: () => {
            this.setState({showCancelModel: false})
            cancelButtonClick()
          },
          cancelBtnClick: () => { this.setState({ showCancelModel: false }); },
          closeByClickBackDrop: true
        })}

        {/* 重复抽取 */}
        {showSampledBillDiscontModal && promptBox({
          color: 'warning',
          title: multiLang && multiLang['7040-0040'],
          content: multiLang && multiLang['7040-0041'],
          noFooter: false,
          noCancelBtn: false,
          beSureBtnClick: () => {
            this.setState({showSampledBillDiscontModal: false})
            sampledBill()
          },
          cancelBtnClick: () => { this.setState({ showSampledBillDiscontModal: false }); },
          closeByClickBackDrop: true
        })}

      </div>
    );
  }
}

// localStorage.setItem("gzip", "0");
// localStorage.setItem("ShuntServerInfo", "{}");

QualityTaskCard = createPage({
  mutiLangCode: '7040'
})(QualityTaskCard);
export default QualityTaskCard;
