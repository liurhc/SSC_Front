export default function(props) {
  const _this = this
  props.createUIDom({}, function({button, template}) {
    props.button.setButtons(button);
    props.meta.setMeta(template);
    props.button.setButtonVisible(["Start", "Stop"], false);
    _this.initSyncTree();
  });
}
