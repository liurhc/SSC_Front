import { ajax } from "nc-lightapp-front";

export default {
  checkoutTreeContents: opt => {
    ajax({
      url: `/nccloud/sscqm/checkcontent/CheckContentTreeAction.do`,
      method: "post",
      data: {
        pid: '', keyword: '',
        queryCondition: {
          pk_defdoclist: null, 
          DataPowerOperationCode:null, 
          isDataPowerEnable:false, 
          isShowUnit:false
        },
        pageInfo:{pageSize: 50, pageIndex: -1}
      },
      success: ({data, success}) => {
        if (success) opt.success(data);
      }
    });
  },
  addContents: ({data, success}) => {
    ajax({
      url: `/nccloud/sscqm/checkcontent/CheckContentSaveAction.do`,
      method: "post",
      data,
      success
    });
  },
  getSingleContent: ({refpk, success}) => {
    ajax({
      url: `/nccloud/sscqm/checkcontent/CheckContentQueryAction.do`,
      method: "post",
      data: {id: refpk},
      success
    });
  },
  updateContents: ({data, success}) => {
    ajax({
      url: `/nccloud/sscqm/checkcontent/CheckContentSaveAction.do`,
      method: "post",
      data,
      success
    });
  },
  delContent: ({refpk, success}) => {
    ajax({
      url: `/nccloud/sscqm/checkcontent/CheckContentDeleteAction.do`,
      method: "post",
      data: {id: refpk},
      success
    });
  },
  // 启用
  checkConStart: ({success, refpk}) => {
    ajax({
      url: `/nccloud/sscqm/checkcontent/CheckContentStartAction.do`,
      method: "post",
      data: {ids: refpk},
      success
    });
  },
  // 停用
  checkConStop: ({success, refpk}) => {
    ajax({
      url: `/nccloud/sscqm/checkcontent/CheckContentStopAction.do`,
      method: "post",
      data: {ids: refpk},
      success
    });
  }
};
