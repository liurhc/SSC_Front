import { Component } from "react";
import { createPage, toast, base, getMultiLang, promptBox, createPageIcon } from "nc-lightapp-front";
const { NCModal, NCButton, NCCheckbox, NCAffix } = base;
import "./index.less";
import { initTemplate, requestApi } from "./events";

class QualityDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      parentTreeNode: null,
      showAddModal: false,
      showUpdateModal: false,
      checked: false,
      selectedNodePk: '',
      rootPk: '',
      multiLang: null,
      delNodePk: '',
      showDelModal: false
    };
    initTemplate.call(this, props);
  }
  componentDidMount() {
    getMultiLang({moduleId: 7040, domainName: 'sscqm',currentLocale: 'zh-CN', callback: (multiLang) => {
      // console.log('multiLang: ', multiLang)
      this.setState({multiLang})
    }})

    window.onbeforeunload = () => {
      const {showAddModal, showUpdateModal} = this.state
      if (showAddModal || showUpdateModal) return ''
    }
  }
  // 初始化数据
  initSyncTree = () => {
    const {multiLang} = this.state

    requestApi.checkoutTreeContents({
      success: data => {
        const {
          syncTree: { setSyncTreeData, hideIcon, createTreeData }
        } = this.props;

        let {rows} = data
        if(!this.state.checked) {
          rows = rows.filter(each => each.values.is_using.value === 'Y')
          let index = -1
          while(++index < rows.length) {
            rows[index].refname =  `${rows[index].refcode} ${rows[index].refname}`
          }
        } else {
          let index = -1
          while(++index < rows.length) {
            rows[index].refname =  `${rows[index].refcode} ${rows[index].refname}${rows[index].values.is_using.value === 'N' ? ` ${multiLang && multiLang['7040-0005']}` : ''}`
          }
        }

        let treeData = createTreeData(rows)
        // console.log('initSyncTree: ', treeData)

        setSyncTreeData("qualityDetailsTree", treeData);
        if (treeData && treeData[0] && treeData[0].refpk) {
          this.setState({rootPk: treeData[0].refpk})
          hideIcon("qualityDetailsTree", treeData[0].refpk, {
            delIcon: false,
            editIcon: false,
            addIcon: true
          });
        }
      }
    });
  };
  // 增
  clickAddIconEve = parentTreeNode => {
    this.setState({ parentTreeNode, showAddModal: true });
    this.props.button.setButtonVisible({
      Start: false,
      Stop: false
    })
    this.props.form.setFormStatus('addDetailForm', 'edit')
    this.props.syncTree.cancelSelectedNode('qualityDetailsTree')
  };
  // 新增 确定
  onAddAfterEvent = () => {
    const { getAllFormValue, isCheckNow, getResidtxtLang } = this.props.form;
    const { rows } = getAllFormValue("addDetailForm");
    const {
      values: { name, code, name2, name3, name4, name5, name6 }
    } = rows[0];

    const {multiLang} = this.state

    if(!isCheckNow('addDetailForm')) {
      toast({ title: multiLang && multiLang['7040-0004'], color: "warning" });
      return;
    }

    const {
      parentTreeNode: { refpk }
    } = this.state;

    const data = {
      id: "",
      code: code.value,
      name: name.value,
      parentid: refpk,
      instructions: "",
      is_using: 'Y',
      name2: name2 ? name2.value : '',
      name3: name3 ? name3.value : '',
      name4: name4 ? name4.value : '',
      name5: name5 ? name5.value : '',
      name6: name6 ? name6.value : ''
    };

    const { addNodeSuccess } = this.props.syncTree;

    requestApi.addContents({
      data,
      success: data => {
        this.setState({ showAddModal: false });

        const values = {}
        for (let attr in data.data) {
          values[attr] = {display: data.data[attr], scale: "-1", value: data.data[attr]}
        }

        const attr = getResidtxtLang('addDetailForm', 'name')

        addNodeSuccess("qualityDetailsTree", {
          isleaf: true,
          pid: refpk,
          refcode: code.value,
          refname: code.value + ' ' + rows[0].values[attr].value,
          refpk: data.data.id,
          values
        });
      }
    });
  };
  // 改
  clickEditIconEve = parentTreeNode => {
    this.props.button.setButtonVisible({
      Start: false,
      Stop: false
    })
    requestApi.getSingleContent({
      refpk: parentTreeNode.refpk,
      success: data => {
        parentTreeNode.values.ts.value = data.data.ts
        this.setState({ parentTreeNode, showUpdateModal: true }, () => {
          const { setAllFormValue, setFormStatus } = this.props.form;
          const {multiLang} = this.state

          let refnameArray = parentTreeNode.refname.substr(data.data.code.length)
          let refName = refnameArray.replace(multiLang && multiLang['7040-0005'], '').trim()

          const name2 = data.data.name2 || ''
          const name3 = data.data.name3 || ''
          const name4 = data.data.name4 || ''
          const name5 = data.data.name5 || ''
          const name6 = data.data.name6 || ''

          setAllFormValue({
            updateDetailForm: {
              rows: [
                {
                  values: {
                    name: {
                      display: refName,
                      value: refName
                    },
                    name2: {
                      display: name2,
                      value: name2
                    },
                    name3: {
                      display: name3,
                      value: name3
                    },
                    name4: {
                      display: name4,
                      value: name4
                    },
                    name5: {
                      display: name5,
                      value: name5
                    },
                    name6: {
                      display: name6,
                      value: name6
                    },
                    code: {
                      display: data.data.code,
                      value: data.data.code
                    }
                  }
                }
              ]
            }
          });
          setFormStatus('updateDetailForm', 'edit')
          this.props.syncTree.cancelSelectedNode('qualityDetailsTree')
        });
      }
    });
  };
  // 修改 确定
  onUpdateAfterEvent = () => {
    const { getAllFormValue, isCheckNow, getResidtxtLang } = this.props.form;
    const { rows } = getAllFormValue("updateDetailForm");
    const {
      values: { name, code, name2, name3, name4, name5, name6 }
    } = rows[0];
    const {multiLang} = this.state

    if (!isCheckNow('updateDetailForm')) {
      toast({ title: multiLang && multiLang['7040-0004'], color: "warning" });
      return;
    }

    const {
      parentTreeNode: { refpk, pid, isleaf/* , refcode */, values: {
        ts, 
        is_using
      } },
      checked
    } = this.state;

    const data = {
      // sscorgid: "",
      code: code.value,
      id: refpk,
      name: name.value,
      parentid: pid,
      // instructions: "",
      is_using: is_using.value,
      ts: ts.value,
      name2: name2 ? name2.value : '',
      name3: name3 ? name3.value : '',
      name4: name4 ? name4.value : '',
      name5: name5 ? name5.value : '',
      name6: name6 ? name6.value : ''
    };

    const { editNodeSuccess } = this.props.syncTree;
    // console.log('param: ', data)
    requestApi.updateContents({
      data,
      success: data => {
        // console.log('response: ', data)
        this.setState({ showUpdateModal: false });

        const values = {}
        for (let attr in data.data) {
          values[attr] = {display: data.data[attr], scale: "-1", value: data.data[attr]}
        }

        const attr = getResidtxtLang('updateDetailForm', 'name')
        const _name = rows[0].values[attr].value
        
        if (checked) {
          editNodeSuccess("qualityDetailsTree", {
            isleaf,
            pid,
            refcode: code.value,
            refname: is_using.value === 'Y' ? code.value + ' ' + _name : code.value + ' ' + _name + ` ${multiLang && multiLang['7040-0005']}`,
            refpk,
            is_using: is_using.value,
            values
          });
        } else {
          editNodeSuccess("qualityDetailsTree", {
            isleaf,
            pid,
            refcode: code.value,
            refname: code.value + ' ' + _name,
            refpk,
            is_using: is_using.value,
            values
          });
        }

        this.props.syncTree.hideIcon("qualityDetailsTree", this.state.rootPk, {
          delIcon: false,
          editIcon: false,
          addIcon: true
        });
      }
    });
  };
  // 删
  clickDelIconEve = ({
    refpk, key, pid, refname, isleaf, values: { is_using }
  }) => {
    const {multiLang} = this.state
    if (!isleaf) {
      return toast({
        title: multiLang && multiLang['7040-0003'],
        color: "warning"
      });
    }
    if (is_using.value === 'Y') {
      return toast({
        title: multiLang && multiLang['7040-0189'],
        color: "warning"
      });
    }
    this.props.button.setButtonVisible({
      Start: false,
      Stop: false
    })

    this.setState({delNodePk: refpk, showDelModal: true})
  };
  // 确定删除
  onDelAfterEvent = () => {
    requestApi.delContent({
      refpk: this.state.delNodePk,
      success: response => {
        if (response.success) {
          const {
            syncTree: { delNodeSuceess }
          } = this.props;
          delNodeSuceess("qualityDetailsTree", this.state.delNodePk);
          const {multiLang} = this.state
          toast({
            title: `${multiLang && multiLang['7040-0158']}${multiLang && multiLang['7040-0159']}。`,
            color: "success"
          });
        }
      }
    });
  }
  // 显示停用状态切换
  changeCheck = checked => {
    this.setState({ checked }, () => {
      this.initSyncTree()
    });
    this.props.button.setButtonVisible({
      Start: false,
      Stop: false
    })
  };
  //选择节点回调
  onSelectEve = (refpk, node, isleaf, events) => {
    // console.log('当前选中节点数据  ', node)
    const {setButtonVisible} = this.props.button
    if (node.pid === '__root__') {
      setButtonVisible({
        Start: false,
        Stop: false
      })
      return
    }
    this.setState({selectedNodePk: refpk, parentTreeNode: node})
    if (node.values.is_using.value === 'Y') {
      setButtonVisible({
        Start: false,
        Stop: true
      })
    } else {
      setButtonVisible({
        Start: true,
        Stop: false
      })
    }
  };
  // 向上递归 寻找父节点pk
  findParentsPk = (pid, arr) => {
    const pNode = this.props.syncTree.getSyncTreeValue('qualityDetailsTree', pid)
    if (pNode.values.is_using.value === 'Y' || pNode.pid === '__root__') return // 找到第一个已启用节点返回
    arr.push(pid)
    // console.log(`启用 ${pNode.refname}`)
    this.findParentsPk(pNode.pid, arr)
  }
  // 向上递归 启用父节点
  recursionPNode = pid => {
    const pNode = this.props.syncTree.getSyncTreeValue('qualityDetailsTree', pid)
    if (pNode.values.is_using.value === 'Y' || pNode.pid === '__root__') return // 找到第一个已启用节点返回
    
    pNode.values.is_using.value = 'Y'

    const refNameArr = pNode.refname.substr(pNode.refcode.length)
    const refname = `${pNode.refcode} ${refNameArr.replace('(停)', '').trim()}`
    pNode.refname = refname

    this.props.syncTree.editNodeSuccess('qualityDetailsTree', pNode)
    // console.log(`启用 ${pNode.refname}`)
    this.recursionPNode(pNode.pid)
  }
  // 向下递归👇 找到所有子节点pk
  findChildsPk = (node, arr) => {
    arr.push(node.refpk)
    if (node.children && node.children.length > 0) {
      let index = -1
      while (++index < node.children.length) {
        this.findChildsPk(node.children[index], arr)
      }
    }
  }
  // 启用 / 停用
  changeConStatu = (props, key) => {
    const {selectedNodePk, parentTreeNode, multiLang} = this.state
    if (key === 'Start') {  // 启用
      const arr = []
      // 当前节点 及 所有子节点pk
      arr.push(selectedNodePk)
      // 未启用父节点pk
      this.findParentsPk(parentTreeNode.pid, arr)
      requestApi.checkConStart({
        refpk: arr.join(','),
        success: (response) => {
          if (response.success) {
            // 显示全部时才有启用操作
            this.props.button.setButtonVisible({ // 右上按钮
              Start: false,
              Stop: true
            })
            // 向上递归 启用父节点
            this.recursionPNode(parentTreeNode.pid)
            // 启用自己
            parentTreeNode.values.is_using.value = 'Y'

            const refNameArr = parentTreeNode.refname.substr(parentTreeNode.refcode.length)
            const refname = `${parentTreeNode.refcode} ${refNameArr.replace(multiLang && multiLang['7040-0005'], '').trim()}`
            parentTreeNode.refname = refname

            this.props.syncTree.editNodeSuccess('qualityDetailsTree', parentTreeNode)
            this.setState({parentTreeNode})

            this.props.syncTree.hideIcon("qualityDetailsTree", this.state.rootPk, {
              delIcon: false,
              editIcon: false,
              addIcon: true
            });
          }
        }
      })
    } else if (key === 'Stop') { // 停用
      const arr = []
      this.findChildsPk(parentTreeNode, arr)
      const pks = arr.join(',')
      requestApi.checkConStop({
        refpk: pks,
        success: (response) => {
          if (response.success) {
            if (this.state.checked) { // 显示全部时
              this.props.button.setButtonVisible({
                Start: true,
                Stop: false
              })
              // 展示全部树节点时 停用后不删除 改变当前停用节点的数据
              this.makeChildsStop(parentTreeNode)
              this.setState({parentTreeNode: this.props.syncTree.getSelectNode('qualityDetailsTree')})
            } else { // 只显示启用
              this.props.button.setButtonVisible({
                Start: false,
                Stop: false
              })
              this.props.syncTree.delNodeSuceess("qualityDetailsTree", selectedNodePk);
            }

            this.props.syncTree.hideIcon("qualityDetailsTree", this.state.rootPk, {
              delIcon: false,
              editIcon: false,
              addIcon: true
            });
          }
        }
      })
    }
  };
  // 向下递归 停用所有子节点
  makeChildsStop = parentTreeNode => {
    const {multiLang} = this.state
    parentTreeNode.values.is_using.value = 'N'
    if (parentTreeNode.refname.indexOf(multiLang && multiLang['7040-0005']) == -1) parentTreeNode.refname = `${parentTreeNode.refname} ${multiLang && multiLang['7040-0005']}`
    this.props.syncTree.editNodeSuccess('qualityDetailsTree', parentTreeNode)
    if (!parentTreeNode.isleaf && parentTreeNode.children && parentTreeNode.children.length > 0) { // 非叶子节点 改变所有子节点状态为停用
      let index = -1
      while(++index < parentTreeNode.children.length) {
        const childOne = parentTreeNode.children[index]
        this.makeChildsStop(childOne)
      }
    }
  }

  render() {
    const {
      syncTree: { createSyncTree },
      form: { createForm },
      button: { createButtonApp }
    } = this.props;

    const { showAddModal, checked, showUpdateModal, multiLang, showDelModal } = this.state;

    const {
      onAddAfterEvent,
      changeCheck,
      onUpdateAfterEvent,
      onSelectEve,
      changeConStatu,
      clickAddIconEve,
      clickEditIconEve,
      clickDelIconEve,
      onDelAfterEvent
    } = this;

    return (
      <div id="quality-details">
        <NCAffix offsetTop={0}>
          <div id="quality-details-head" className="clearfix">
            {createPageIcon()}
            <h1>{multiLang && multiLang['7040-0001']}</h1>
            <NCCheckbox onChange={changeCheck} checked={checked}>
              {multiLang && multiLang['7040-0006']}
            </NCCheckbox>
            {createButtonApp({
              onButtonClick: changeConStatu,
              area: "detailViewBtn"
            })}
          </div>
        </NCAffix>
        <div id="quality-details-body">
          <div className="body-tree">
            {createSyncTree({
              treeId: "qualityDetailsTree",
              needSearch: true,
              disabledSearch: false,
              needEdit: true,
              defaultExpandAll: true,
              showModal: false,
              clickAddIconEve,
              clickEditIconEve,
              clickDelIconEve,
              showLine: true,
              onSelectEve,
              selectedForInit: true
            })}
          </div>
        </div>

        <NCModal className="operate-note" show={showAddModal}>
          <NCModal.Header>
            {" "}
            <NCModal.Title>{multiLang && multiLang['7040-0007']}</NCModal.Title>{" "}
          </NCModal.Header>
          <NCModal.Body>{createForm("addDetailForm", {})}</NCModal.Body>
          <NCModal.Footer>
            <NCButton colors='primary' onClick={onAddAfterEvent}>{multiLang && multiLang['7040-0008']}</NCButton>
            <NCButton
              onClick={() => {
                this.setState({ showAddModal: false });
              }}
            >{multiLang && multiLang['7040-0009']}</NCButton>
          </NCModal.Footer>
        </NCModal>

        <NCModal className="operate-note" show={showUpdateModal}>
          <NCModal.Header>
            {" "}
            <NCModal.Title>{multiLang && multiLang['7040-0010']}</NCModal.Title>{" "}
          </NCModal.Header>
          <NCModal.Body>{createForm("updateDetailForm", {})}</NCModal.Body>
          <NCModal.Footer>
            <NCButton colors='primary' onClick={onUpdateAfterEvent}>{multiLang && multiLang['7040-0008']}</NCButton>
            <NCButton
              onClick={() => {
                this.setState({ showUpdateModal: false });
              }}
            >{multiLang && multiLang['7040-0009']}</NCButton>
          </NCModal.Footer>
        </NCModal>

        {showDelModal && promptBox({
          color: 'warning',
          title: multiLang && multiLang['7040-0025'],
          content: multiLang && multiLang['7040-0191'],
          noFooter: false,
          noCancelBtn: false,
          beSureBtnClick: () => {
            this.setState({showDelModal: false})
            onDelAfterEvent()
          },
          cancelBtnClick: () => { this.setState({ showDelModal: false }); },
          closeByClickBackDrop: true
        })}

      </div>
    );
  }
}

// localStorage.setItem("gzip", "0");
// localStorage.setItem("ShuntServerInfo", "{}");

QualityDetails = createPage({
  mutiLangCode: '7040'
})(QualityDetails);
export default QualityDetails;
