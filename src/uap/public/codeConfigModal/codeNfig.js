import CodeConfigModal from "./index.js";
export default function codeNfig(
  newProps,
  params = { data: {}, url: "" },
  call
) {
  let callback = (json, bool, init) => {
    newProps.modal.show("code-config", {
      title: "打印条码",
      size: "lg",
      noFooter: true,
      content: (
        <CodeConfigModal
          params={params}
          onCancelInfoSave={() => {
            closeModal(newProps);
          }}
          onEnsureInfoSave={info => {
            closeModal(newProps);
            if (call && typeof call === "function") {
              call(info);
            }
          }}
        />
      ),
      closeModalEve: closeModal(newProps)
    });
  };

  callback();
}

const closeModal = props => {
  props.modal.close("code-config");
};
