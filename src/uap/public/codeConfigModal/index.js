import React, { Component, Fragment } from 'react';
import ReactDOM from 'react-dom';
import { createPage, base, ajax, toast } from 'nc-lightapp-front';
import './index.less';
import initTemplate from './lifeCycle/initTemplate';
import afterEvent from './events/afterEvent';
import beforEvent from './events/beforEvent';

const { NCButton } = base;

class CodeConfigModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			info: {
				barcodeRule: null,
				barcodeLabel: null,
				iprintcount: 0,
				vis: false,
				templateID: null
			},
			materialinfo: {}
		};
	}

	componentDidMount() {
		this.getData();
	}

	getData = () => {
		const that = this;
		const { url = '/nccloud/barcode/init/barcodeinitaction.do', data = {} } = this.props.params;
		if (Object.prototype.toString.call(data).slice(8, -1) === 'Object' && Object.keys(data).length) {
			this.setState({
				materialinfo: data
			});
			ajax({
				method: 'post',
				url: url,
				data: data,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						that.renderData(data);
					}
				},
				error: (res) => {
					alert(res.message);
				}
			});
		}
	};

	renderData = (data) => {
		const initForm = {
			rows: [
				{
					values: {
						iprintcount: {
							display: data.printcount.iprintcount,
							value: data.printcount.iprintcount
						},
						barcodeRule: {
							display: data.barcodeRule.refname,
							value: data.barcodeRule.refcode
						},
						barcodeLabel: {
							display: data.barcodeLabel.refname,
							value: data.barcodeLabel.refcode
						}
					}
				}
			]
		};
		this.modifyMeta();
		this.props.form.setAllFormValue({ codeControl: initForm });
		let info = {
			barcodeRule: data.barcodeRule.refpk,
			barcodeLabel: data.barcodeLabel.refpk,
			iprintcount: data.printcount.iprintcount
		};
		this.setState({
			info
		});
	};

	modifyMeta = () => {
		let meta = this.props.meta.getMeta();
		meta['codeControl'].items.map((item) => {});
		this.props.meta.setMeta(meta);
	};

	ensureInfoSave = () => {
		if (this.props.onEnsureInfoSave && typeof this.props.onEnsureInfoSave === 'function') {
			this.props.onEnsureInfoSave(this.state.info);
		}
	};

	cancelInfoSave = () => {
		if (this.props.onCancelInfoSave && typeof this.props.onCancelInfoSave === 'function') {
			this.props.onCancelInfoSave();
		}
	};

	render() {
		const { form } = this.props;
		let { createForm } = form;
		return (
			<div className='code-config-contain'>
				{createForm('codeControl', {
					onAfterEvent: afterEvent.bind(this),
					onBeforeEvent: beforEvent.bind(this)
				})}
				<p className='code-tips tips-one'>物料条码规则、条形码标签为空时，则默认规则、默认标签进行打印</p>
				<p className='code-tips tips-two'>打印份数为空时，按照条形码标签计算打印份数；不为空时，则按打印份数打印</p>
				<footer className='code-config-modal'>
					<NCButton onClick={this.ensureInfoSave}>确定</NCButton>
					<NCButton onClick={this.cancelInfoSave}>取消</NCButton>
				</footer>
			</div>
		);
	}
}

CodeConfigModal = createPage({
	initTemplate: initTemplate
})(CodeConfigModal);

export default CodeConfigModal;
