export default function afterEvent(props, moduleId, key, value, oldValue) {
	let meta = props.meta.getMeta();
	meta.codeControl.items.map((item) => {
		if ([ 'barcodeLabel', 'barcodeRule' ].includes(item.attrcode)) {
			item.queryCondition = {
				pk_org: this.state.materialinfo.pk_org
			}
		}
	});
	props.meta.setMeta(meta);
	return true;
}
