import { base, ajax, toast } from 'nc-lightapp-front';
export default function(props) {
	let meta = {};
	let callback = (json, status, inlt) => {
		meta = {
			codeControl: {
				moduletype: 'form',
				items: [
					{
						attrcode: 'barcodeRule',
						placeholder: '',
						value: '',
						leftspace: 0,
						rightspace: 0,
						required: false,
						visible: true,
						itemtype: 'refer',
						refcode: 'uap/refer/riart/barCodeRuleDefaultTableTreeRef/index',
						label: '物料条形码规则：'
					},
					{
						attrcode: 'barcodeLabel',
						placeholder: '',
						value: '',
						leftspace: 0,
						rightspace: 0,
						required: false,
						visible: true,
						itemtype: 'refer',
						refcode: 'uap/refer/riart/barcodeLabelTableRef/index',
						label: '条形码标签：'
					},
					{
						attrcode: 'iprintcount',
						placeholder: '',
						value: '',
						leftspace: 0,
						rightspace: 0,
						required: false,
						visible: true,
						itemtype: 'number',
						label: '打印份数：'
					}
					// {
					//   attrcode: 'vis',
					//   placeholder: '',
					//   leftspace: 0,
					//   rightspace: 0,
					//   required: false,
					//   visible: true,
					//   itemtype: 'switch',
					//   label: '显示条码清单：',
					// }
				],
				status: 'edit'
			}
		};
		props.meta.setMeta(meta);
	};
	callback();
}
