import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {  toast,createPage, ajax, high, base} from 'nc-lightapp-front';
// import ajax from '../../../api/ajax.js';
import UserRefer from '../../../refer/riart/userRefer';
const { NCLoading, NCButton, NCTable, NCInput, NCCheckbox, NCSelect, NCIcon, NCPopconfirm, NCDatePicker, NCBreadcrumb, NCFormControl, NCModal, NCAffix, NCBackBtn } = base;
const NCBreadcrumbItem = NCBreadcrumb.NCBreadcrumbItem;
//import { createPage, ajax, high, base } from 'src';
const { ApproveComment, ApproveProcess } = high;

import './index.less';
import '../index.less';


//const ajax = jQuery.ajax;

const cloneObj = function (obj) {
    var o;
    if (typeof obj == "object") {
        if (obj === null) {
            o = null;
        } else {
            if (obj instanceof Array) {
                o = [];
                for (var i = 0, len = obj.length; i < len; i++) {
                    o.push(cloneObj(obj[i]));
                }
            } else {
                o = {};
                for (var j in obj) {
                    o[j] = cloneObj(obj[j]);
                }
            }
        }
    } else {
        o = obj;
    }
    return o;
};
// const cloneObj = function(obj){
// return jQuery.extend(true, {}, obj, {});
// };
const convertFormData = function (obj, formName) {
    let _obj = {};
    for (let k in obj) {
        _obj[k] = { "value": obj[k] }
    }
    let ret = {};
    ret[formName] = {
        "areacode": formName,
        "rows": [
            {
                "status": "0",
                "values": _obj
            }
        ]
    }
    return ret;
};

Date.prototype.format = function (format) {
    var date = {
        "M+": this.getMonth() + 1,
        "d+": this.getDate(),
        "h+": this.getHours(),
        "m+": this.getMinutes(),
        "s+": this.getSeconds(),
        "q+": Math.floor((this.getMonth() + 3) / 3),
        "S+": this.getMilliseconds()
    };
    if (/(y+)/i.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length));
    }
    for (var k in date) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length == 1
                ? date[k] : ("00" + date[k]).substr(("" + date[k]).length));
        }
    }
    return format;
};

Date.prototype.DateAdd = function (strInterval, Number) {
    var dtTmp = this;
    switch (strInterval) {
        case 's': return new Date(Date.parse(dtTmp) + (1000 * Number));
        case 'n': return new Date(Date.parse(dtTmp) + (60000 * Number));
        case 'h': return new Date(Date.parse(dtTmp) + (3600000 * Number));
        case 'd': return new Date(Date.parse(dtTmp) + (86400000 * Number));
        case 'w': return new Date(Date.parse(dtTmp) + ((86400000 * 7) * Number));
        case 'q': return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) + Number * 3, dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds());
        case 'm': return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) + Number, dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds());
        case 'y': return new Date((dtTmp.getFullYear() + Number), dtTmp.getMonth(), dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds());
    }
}

let getParams = function() {
    let url = location.search; //获取url中"?"符后的字串   
    if(!url || url=="")url = location.hash; //获取url中"#"符后的字串   
    console.log('url------', decodeURI(url));
    url = decodeURI(url);
    let params = new Object();
    if (url.indexOf("?") == 0 || url.indexOf("#") == 0) {
        let str = url.substr(3);
        let strs = str.split("&");
        for (let i = 0; i < strs.length; i++) {
            params[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
        }
    }
    return params;
};
const urlParams = getParams();
console.log('urlParams', urlParams)

var pageMsgType = "approve";


class ApproveDetailBox extends Component {
    constructor(props) {
        super(props);
        this.props = props;
        let _this = this;
        let binds = "markRead approvePass approveRefuse approveReject " +
            "detailDoApprove detailCancelApprove suggestChange openResumeModal closeResumeModal"
       
        binds.split(" ").forEach(function (item) {
            _this[item] = _this[item].bind(_this);
        });
        this.state = {
            json: {},
            inlt: null,
            approveType: "approve",
            showResumeModal: false,
            skipCodes: {},
            resumeDetail: {},
            resumeCode: "",
            confirmOrCancle: null,
            suggestion: "",
            message: {},
            approveData: null,
            wf_task_val: "",
            Children: [],
            stepData:null,
            tabledata:[],
            enableActions:[],
            frameHeight: '500px',
            billtype:"",
            billid:"",
            currTime: Date.now(),
            isLoaded: false
        }
    }

    componentWillMount() {
        let callback = (json, status, inlt) => { // json 多语json格式参数； status： 是否请求到json多语，可用来判断多语资源是否请求到并进行一些别的操作； inlt： 可用来进行占位符的一些操作
            if (status) {
                this.setState({ json, inlt });// 保存json和inlt到页面state中并刷新页面
            } else {
                console.log('未加载到多语资源');   // 未请求到多语资源的后续操作
            }

        }
        this.props.MultiInit.getMultiLang({ 'moduleId': '1016-10160501-detail', 'domainName': 'uap', callback });
    }

    //输入意见输入框
    suggestChange(val) {
        this.setState({
            suggestion: val
        });
    }


    // approveCancel(e) {
    //     if(!confirm(this.state.json['10160501-000003'])){return;}/* 国际化处理： 已经审批完成,确定收回?*/
    //     console.log(this.state.json['10160501-000004'] + this.state.message.id);/* 国际化处理： 发送审批取消请求*/
    //     console.log(e);
    //     let _this = this;
    //     let msg = _this.state.message;

    //     ajax({
    //         url: '/nccloud/workflow/approvalcenter/unApproveAction.do',
    //         data: { "billtype": msg.billtype, "billid": msg.billid, "pk_checkflow": msg.pk_detail },
    //         method: 'POST',
    //         success: (res) => {
    //             if (res.data && (res.data == "200" || res.data.code == "200")) {
    //                 toast({ color: 'success' });
    //             }
    //             else {
    //                 toast({ color: 'danger', content: res.data.error});
    //             }
    //         }
    //     });
    // }

    markRead() {
        let _this = this;
        ajax({
            url: '/nccloud/riart/message/setRead.do',
            data: { "pk_message": _this.state.message.id, msgType: pageMsgType, isread: "Y" },
            method: 'POST',
            success: (res) => {
                //let data = res.data;
                //_this.markIsread();
            }
        });
    }

    openResumeModal() {
        console.log("openResumeModal");
        this.setState({
            showResumeModal: true
        });
    }

    closeResumeModal() {
        console.log("closeResumeModal");
        this.setState({
            approveType: "approve",
            skipCodes: {},
            showResumeModal: false
        });
    }

    approvePass() {
        console.log(this.state.json['10160501-000005'] + this.state.message.id);/* 国际化处理： 发送审批通过请求*/
        let _this = this;
        let msg = _this.state.message;
        //this.markIsread();
        let _data = { "billtype": msg.billtype, "billid": msg.billid, "pk_checkflow": msg.pk_detail, "skipCodes": _this.state.skipCodes["approve"] || [] };
        if(_this.state.confirmOrCancle){ 
            _data["confirmOrCancleAction"] = _this.state.confirmOrCancle;
        }
        if (this.state.showApproveDetailModal) {
            _data["checknote"] = this.state.suggestion;
        }

        ajax({
            url: '/nccloud/workflow/approvalcenter/approvePassAction.do',
            data: _data,
            method: 'POST',
            success: (res) => {
                if (res.data && (res.data == "200" || res.data.code == "200")) {
                    toast({ color: 'success' });
                    _this.markRead();
                }
                else if(res.data.code && res.data.code.length == 3 && res.data.code.slice(0,1) == "3") {
                    let _state = _this.state;
                    _state.skipCodes["approve"] = res.data.skipCodes;
                    _state.resumeDetail = res.data.detail;
                    _state.resumeCode = res.data.code;
                    _this.setState(_state);
                    _this.openResumeModal();
                }
                else {
                    toast({ color: 'danger', content: res.data.error});
                }
            }
        });
    }

    approveRefuse() {
        console.log(this.state.json['10160501-000006'] + this.state.message.id);/* 国际化处理： 发送审批拒绝请求*/
        let _this = this;
        let msg = _this.state.message;
        //this.markIsread();
        let _data = { "billtype": msg.billtype, "billid": msg.billid, "pk_checkflow": msg.pk_detail };

        _data["check_note"] = this.state.suggestion;
        _data["checknote"] = this.state.suggestion;

        ajax({
            url: '/nccloud/workflow/approvalcenter/approveNoPassAction.do',
            data: _data,
            method: 'POST',
            success: (res) => {
                if (res.data && (res.data == "200" || res.data.code == "200")) {
                    toast({ color: 'success' });
                    _this.markRead();
                }
                else {
                    toast({ color: 'danger', content: res.data.error});
                }
            }
        });
    }

    approveReject() {
        console.log(this.state.json['10160501-000007'] + this.state.message.id);/* 国际化处理： 发送审批驳回请求*/
        let _this = this;
        let msg = _this.state.message;
        //this.markIsread();
        let _data = { "billtype": msg.billtype, "billid": msg.billid, "pk_checkflow": msg.pk_detail, "pk_wf_task": this.state.wf_task_val };
        //if (this.state.showApproveDetailModal) {
        _data["checknote"] = this.state.suggestion;
        //}

        ajax({
            url: '/nccloud/workflow/approvalcenter/approveRejectAction.do',
            data: _data,
            method: 'POST',
            success: (res) => {
                if (res.data && (res.data == "200" || res.data.code == "200")) {
                    toast({ color: 'success' });
                    _this.markRead();
                }
                else {
                    toast({ color: 'danger', content: res.data.error});
                }
            }
        });
    }

    detailDoApprove() {
        console.log("detailDoApprove");
        //return null;
        if (this.state.approveType == "approve") {
            //
            this.approvePass();
        }
        else if (this.state.approveType == "Noapprove") {
            //
            this.approveRefuse();
        }
        else if (this.state.approveType == "reject") {
            //
            this.approveReject();
        }
    }

    detailCancelApprove() {
        console.log("detailCancelApprove");
        if(this.state.resumeCode == "315" || this.state.resumeCode == "314"){
            this.setState({
                confirmOrCancle:2
            })
        }
        this.approvePass();
    
    }

    //重置iframe内页面样式
    resetIframeStyle = () => {
        let $frame = document.querySelector("#detailIframe");
        let frameWin = $frame.contentWindow;
        let $body = frameWin.document.body;
        $body.style.backgroundColor = '#fff';
        $body.style.padding = 0;
        // $body.style.overflowX = 'hidden';
        if(frameWin.document.querySelector("#app")) {
            let $app = frameWin.document.querySelector("#app");
            $app.style.padding = 0;
            $app.style.margin = 0;
            // $app.style.overflow = 'auto';
        }
        setTimeout(() => {
            this.setState({
                frameHeight: `${$body.scrollHeight + 50}px`
            })
        }, 1000);
    }
  

    componentDidMount() {
        let _this = this;
        let params = { "pk_message": urlParams["pk_message"] };
        console.log('params', params)
        this.begServiceCurrDate();
        ajax({
            url: '/nccloud/riart/message/list.do',
            data: params,
            method: 'POST',
            success: (res) => {
                let dt = res.data.items.map(function (item, idx, origin) {
                    let _item = item;
                    _item.id = _item.pk_message;
                    return _item;
                });
                let msg = dt[0];
                let _detail = JSON.parse(msg.detail);
                let _enableActions = _detail["enableActions"];
                _this.setState({
                    message: msg,
                    enableActions: _enableActions
                }, () => {
                    console.log('message,',msg)
                });
                ajax({
                    url: '/nccloud/riart/message/getApproveUrl.do',
                    data: { "pk_message": msg.pk_message, "billtype": msg.billtype, "billid": msg.billid, "appcode": (msg.funccode || "").replace(/(^[\s\n\t]+|[\s\n\t]+$)/g, "") },
                    method: 'POST',
                    success: (res) => {
                        console.log(res.data);
                        let finalUrl = "/";
                        let url = (res.data && res.data.pageurl) || "no permission";
                        if(url == "no permission"){
                            toast({ color: 'warning', content:this.state.json['10160501-000008'] });/* 国际化处理： 没有审批应用权限或配置错误，请联系管理员。*/
                            return;
                        }
                        if(url == "message not found"){
                            toast({ color: 'warning', content:this.state.json['10160501-000009'] });/* 国际化处理： 没有找到该消息。*/
                            return;
                        }
                        if (url) {
                            let _appcode = res.data.appcode || urlParams["c"];
                            let _pagecode = res.data.pagecode || urlParams["p"];
                            // if(url.indexOf("#")>=0){
                            //     let us = url.split("#");
                            //     us[0] = us[0] + (url.indexOf("?")>=0 ? "&" : "?" ) + "status=browse&id=" + msg.billid;
                            //     finalUrl = us.join("#");
                            // }
                            // else {
                            //     finalUrl = url + (url.indexOf("?")>=0 ? "&" : "?" ) + "status=browse&id=" + msg.billid;
                            // }
                            if(url.indexOf("?")==-1){
                                finalUrl = url + "?scene=approvesce&status=browse&id=" + msg.billid+"&appcode="+_appcode+"&pagecode="+_pagecode;
                            }
                            else if(url.indexOf("#")>=0){
                                finalUrl = url + (url.split("#")[1].length>0 ? "&" : "") + "scene=approvesce&status=browse&id=" + msg.billid+"&appcode="+_appcode+"&pagecode="+_pagecode;
                            }
                            else {
                                finalUrl = url + "#scene=approvesce&status=browse&id=" + msg.billid+"&appcode="+_appcode+"&pagecode="+_pagecode;
                            }
                        }
                        // debugger
                        // finalUrl=finalUrl.slice(18);
                        // _this.props.openTo(finalUrl, {appcode: "10160501", pagecode: "10160501APPROVE"});
                        this.setState({
                        //  approveDetailModalUrl: res.data ? res.data : "/uap/dc/transtypeManage/list/index.html"
                            approveDetailModalUrl: finalUrl ? finalUrl : ""
                        });
                    }
                });
                console.log('msg', msg);
                ajax({
                    url: '/nccloud/workflow/approvalcenter/workNoteHistory.do',
                    data: { "billtype": msg.billtype, "billid": msg.billid },
                    method: 'POST',
                    success: (res) => {
               
                        if (res.data.rows.rows && res.data.rows.rows.length > 0) {
                
                 
                                 
                                _this.setState({
                                    approveData:res.data.rows,
                                    isLoaded: true
                                // tabledata:_tabledata
                                })

              
                        }
                    }
                });
            }
        });
    }

    handleSuccess() {
        this.props.linkTo("/uap/msgcenter/message/main/index.html?msgType=" + 'approve');
    }

    begServiceCurrDate = () => {//请求服务器当前时间
		const that = this;
		ajax({
			url: '/nccloud/workflow/approvalcenter/queryServerTimeAction.do',
			data: {},
			method: 'post',
			loading: false,
			success: (res) => {
				if(res && res.data) {
					that.setState({
						currTime: res.data
					}, () => {
						this.render();
					})
				}
			}
		})
    }
    
    getElement= () => {
        return document.getElementById('loadingBox');
    }

    canBackControl = () => {
        let $frame = document.querySelector("#detailIframe");
        let frameWin = $frame.contentWindow;
        if(frameWin.document.querySelector(".nc-bill-form-area .form-component-item-wrapper") && frameWin.document.querySelector(".nc-bill-form-area .form-component-item-wrapper").className.includes('edit')) {
            return false;
        }
        return true;
    }

    render() {
        let _this = this;
      
        const { form } = this.props;
        //let content = { __html: this.state.message.content };
        let resumeHtml = { __html: this.state.resumeDetail ? this.state.resumeDetail.html : "" };
        let bold = { "font-size": "24px", "font-weight": "bold" };
        let normal = { "font-size": "24px" }

        let ApproveProcessData = _this.state.approveData;
        const { currTime } = this.state;

        let billid = _this.state.message.billid;
        let billtype = _this.state.message.billtype;
        let _detail = {};
        let submitRejectBillMode = null;
        let rejectHistory = {"isReject":false, "rejectReason":null};
        try {
            _detail = JSON.parse(_this.state.message.detail);
            submitRejectBillMode = _detail && _detail["SubmitRejectBillMode"];
            if (_detail && _detail["rejectHistory"]) {
                rejectHistory = _detail && _detail["rejectHistory"];
                // console.log('test_rejectHistory', rejectHistory)
            }
        } catch (e) {}
        let detail = _this.state.message.detail && JSON.parse(_this.state.message.detail);
        // debugger
        let checknote = decodeURI(urlParams['checknote']);
        let hintMessage = detail && detail.clientinfo && detail.clientinfo.hintMessage;
        console.log('rejectHistory',rejectHistory)


        return (
            <div className="approve-datail-main">
                <div className="detail-goback">
                    <NCBackBtn
                        onClick={()=>{
                            let res = this.canBackControl();
                            if(!res) {
                                alert('请保存您的信息修改。');
                                return false;
                            }
                            this.props.linkTo("/uap/msgcenter/message/main/index.html?msgType=" + 'approve');
                        }}
                    >{this.state.json['10160501-000010']}</NCBackBtn> {/* 国际化处理： 返回*/}
                </div>
                {ApproveProcessData ? <div className="approve-bill-detail">
					<div className="steps">
                        {
                           ApproveProcessData && Object.keys(ApproveProcessData).length &&  <ApproveProcess
                                className="outer-steps"
                                data={ApproveProcessData}
                                currTime={currTime}
                            >
                            </ApproveProcess>
                        }
					</div>
					<div className="comment">
						<ApproveComment
							className="outer-comment"
                            billtype={billtype}
                            hintMessage={hintMessage}
                            rejectHistory={rejectHistory}
                            billid={billid}
                            submitRejectBillMode={submitRejectBillMode}
                            checknote = {checknote}
                            onHandleSuccess={this.handleSuccess.bind(this)}
						/>
					</div>
				</div>: !this.state.isLoaded ? <div id="loadingBox"><NCLoading showBackDrop={true} loadingType="rotate" container={this.getElement} show={!this.state.isLoaded}/></div>:<div style={{padding:'15px',backgroundColor:'#fff'}}>
                    <img src="../../../public/img/empty_approve_detail.png" style={{width: '165px',margin:'0 auto',display:'block'}}/>
                </div>}
	           	

                {/* <NCAffix>
                    <ApproveProcessDemo 
                        message={_this.state.message}  
                        pageMsgType={urlParams["pageMsgType"]}  
                        stepData={_this.state.stepData} 
                        tabledata= {_this.state.tabledata} 
                        enableActions={_this.state.enableActions} 
                        approvePass={this.approvePass} 
                    />      
                </NCAffix> */}
                    {/* {_this.state.message.pk_message ? _this.createApprove() : ""} */}
            
                {/* <div className="card-container" style={{ height: this.state.frameHeight }}> */}
                <div className="card-container">
                    <iframe src={_this.state.approveDetailModalUrl} style={{ width: "100%" }} id="detailIframe" scrolling="auto"
                        onLoad={() => {
                            this.resetIframeStyle();
                        }}></iframe>
                </div>
              
                <NCModal
                    className="form-modal"
                    size={this.state.resumeDetail && this.state.resumeDetail.size ? this.state.resumeDetail.size : "lg"}
                    show={this.state.showResumeModal}
                    backdrop={true}
                    onHide={this.closeResumeModal}>
                    <NCModal.Header closeButton>
                        <NCModal.Title > {this.state.json['10160501-000011']} </NCModal.Title>{/* 国际化处理： 审批警告*/}
                    </NCModal.Header >

                    <NCModal.Body >
                        {this.state.resumeDetail && this.state.resumeDetail.html ? <div dangerouslySetInnerHTML={resumeHtml}></div> : ""}
                        {this.state.resumeDetail && this.state.resumeDetail.url ? <iframe src={this.state.resumeDetail.url} style={{width: "1000px"}}></iframe> : ""}
                    </NCModal.Body>
                    <NCModal.Footer>
                        {this.state.resumeCode == "313" || this.state.resumeCode == "316" ? "" :
                            <NCButton colors="primary" onClick={this.detailDoApprove}> {this.state.json['10160501-000012']} </NCButton>}{/* 国际化处理： 确定*/}
                        {this.state.resumeCode != "314" && this.state.resumeCode != "315"  ? "" :
                            <NCButton colors="primary" onClick={this.detailCancelApprove}> {this.state.json['10160501-000014']} </NCButton>}{/* 国际化处理： 取消*/}
                        <NCButton className="btn-transparent" onClick={this.closeResumeModal}> {this.state.json['10160501-000013']} </NCButton>{/* 国际化处理： 关闭*/}
                    </NCModal.Footer>
                </NCModal>
            </div>
        );
    }
}



let ApproveDetailPage = createPage({
    //initTemplate: initTemplate,
})(ApproveDetailBox);

ReactDOM.render(
    <ApproveDetailPage />
    , document.querySelector("#app"));

