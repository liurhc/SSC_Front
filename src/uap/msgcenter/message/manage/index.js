import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, base, ajax, high, toast } from 'nc-lightapp-front';
// import UserRefer from '../../../refer/riart/userRefer';
import UserRefer from '../../../refer/riart/userDefaultRefer';
import moment from "moment/moment";

import { buttonClick, initTemplate, pageInfoClick } from './events';

const { NCButton, NCSelect, NCModal, NCTable, NCRow, NCCol, NCTree, NCBreadcrumb, 
        NCFormControl, NCTabs,NCForm,NCRadio, NCDatePicker, NCRangePickerClient, NCPagination, NCCheckbox } = base;
const NCOption = NCSelect.NCOption;
const { NCRangePicker } = NCDatePicker;
const Tree = NCTree;
const TreeNode = Tree.NCTreeNode;
const NCBreadcrumbItem = NCBreadcrumb.NCBreadcrumbItem;
const NCTabPane = NCTabs.NCTabPane;
const NCFormItem = NCForm.NCFormItem;

import './index.less';

let tableId = 'message';

const cloneObj = function (obj) {
    var o;
    if (typeof obj == "object") {
        if (obj === null) {
            o = null;
        } else {
            if (obj instanceof Array) {
                o = [];
                for (var i = 0, len = obj.length; i < len; i++) {
                    o.push(cloneObj(obj[i]));
                }
            } else {
                o = {};
                for (var j in obj) {
                    o[j] = cloneObj(obj[j]);
                }
            }
        }
    } else {
        o = obj;
    }
    return o;
};


Date.prototype.format = function (format) {
    var date = {
        "M+": this.getMonth() + 1,
        "d+": this.getDate(),
        "h+": this.getHours(),
        "m+": this.getMinutes(),
        "s+": this.getSeconds(),
        "q+": Math.floor((this.getMonth() + 3) / 3),
        "S+": this.getMilliseconds()
    };
    if (/(y+)/i.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length));
    }
    for (var k in date) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length == 1
                ? date[k] : ("00" + date[k]).substr(("" + date[k]).length));
        }
    }
    return format;
};

Date.prototype.DateAdd = function (strInterval, Number) {
    var dtTmp = this;
    switch (strInterval) {
        case 's': return new Date(Date.parse(dtTmp) + (1000 * Number));
        case 'n': return new Date(Date.parse(dtTmp) + (60000 * Number));
        case 'h': return new Date(Date.parse(dtTmp) + (3600000 * Number));
        case 'd': return new Date(Date.parse(dtTmp) + (86400000 * Number));
        case 'w': return new Date(Date.parse(dtTmp) + ((86400000 * 7) * Number));
        case 'q': return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) + Number * 3, dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds());
        case 'm': return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) + Number, dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds());
        case 'y': return new Date((dtTmp.getFullYear() + Number), dtTmp.getMonth(), dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds());
    }
}


let getParams = function() {
    let url = location.search; //获取url中"?"符后的字串   
    if(!url || url=="")url = location.hash; //获取url中"#"符后的字串   
    let params = new Object();
    if (url.indexOf("?") == 0 || url.indexOf("#") == 0) {
        let str = url.substr(1);
        let strs = str.split("&");
        for (let i = 0; i < strs.length; i++) {
            params[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
        }
    }
    return params;
};
const urlParams = getParams();
let loading = false;


let vosToGrid = function(tableId, _objs){
    let ret = {
        areacode: tableId,
        rows: []
    };

    let obj;
    let _it;
    _objs.forEach(function(item, idx, origin){
        obj = cloneObj(item);
        _it = {status: "0", values: {
        }};
        for (var j in obj) {
            _it.values[j] = {value: obj[j]};
        }
        ret.rows.push(_it);
    });

    return ret;
};

let gridToVo = function(obj){
    let vo = {};
    for (var j in obj) {
        vo[j] = obj[j].value;
    }
    return vo;
};




let msgtypeNames = {
    "nc": '00220601-000012',/* 国际化处理： 系统*/
    "email": '00220601-000013',/* 国际化处理： 邮件*/
    "sms": '00220601-000014'/* 国际化处理： 短信*/
};


class MessageList extends Component {
    constructor(props) {
        super(props);
        this.props = props;
        let { form, button, table, search ,syncTree } = this.props;
        let { setSyncTreeData } = syncTree;
        this.setSyncTreeData = setSyncTreeData;   //设置树根节点数据方法
        let _this = this;
        let binds = "msgsourcetypeChange changeMsgType changeSt setSubject setTime setSender setReceiver initData refreshList changePage scrollBottom preResend resend showMoreReceivers";
        binds.split(" ").forEach(function (item) {
            _this[item] = _this[item].bind(_this);
        });
        this.state = {
            json: {},
            inlt: null,
            totalPage: 1, 
            params: {
                msgType: "approve",
                startTime: "",
                endTime: "", 
                msgsourcetype: "", 
                subject: "", 
                sender: {},
                receiver: {},
                msgtypes: [],
                page: 1,
                perPage: 10
            }
        }
    }

    componentWillMount() {
        let callback = (json, status, inlt) => { // json 多语json格式参数； status： 是否请求到json多语，可用来判断多语资源是否请求到并进行一些别的操作； inlt： 可用来进行占位符的一些操作
            if (status) {
                this.setState({ json, inlt });// 保存json和inlt到页面state中并刷新页面
            } else {
                console.log('未加载到多语资源');   // 未请求到多语资源的后续操作
            }

        }
        this.props.MultiInit.getMultiLang({ 'moduleId': '0022-00220601', 'domainName': 'uap', callback });
    }

    initData () {
        let _this = this;
        _this.refreshList(_this.state.params);
    }

    refreshList(params, more){
        let _this = this;
        if(!params)params = _this.state.params;
        let _params = cloneObj(params);
        let samePs = "msgType msgsourcetype subject startTime endTime";
        samePs.split(" ").forEach(function (item, idx, origin) {
            _params[item] = _this.state.params[item];
        });
        _params.senderId = "";
        if (this.state.params.sender && this.state.params.sender.refpk && this.state.params.sender.refpk.length > 0) {
            _params.senderId = this.state.params.sender.refpk;
        }
        _params.receiverId = "";
        if (this.state.params.receiver && this.state.params.receiver.refpk && this.state.params.receiver.refpk.length > 0) {
            _params.receiverId = this.state.params.receiver.refpk;
        }
        ajax({
            url:'/nccloud/riart/message/managelist.do',
            data: _params,
            success: (res)=>{
                let dt = res.data.items;
                let _perPage = parseInt(_this.state.params.perPage);
                let _total = parseInt(res.data.total);
                let _totalPage = Math.ceil(_total / _perPage);
                if (_totalPage == _params.page && dt.length > 0) {
                    _this.setState({ loadingText: this.state.json['00220601-000015'] });/* 国际化处理： 到底了*/
                }
                else {
                    _this.setState({ loadingText: this.state.json['00220601-000016'] });/* 国际化处理： 滚动加载更多*/
                }
                let _msgs = _this.state.messages;
                _this.setState({
                    messages: []
                });
                let newMsgs = more ? [..._msgs, ...dt] : dt;
                newMsgs = dt;
                _this.setState({
                    messages: newMsgs,
                    total: _total,
                    totalPage: _totalPage
                });
                newMsgs = newMsgs.map(function(item, idx, origin){
                    item.msgtypeName = _this.state.json[msgtypeNames[item.msgtype]] || item.msgtype;
                    item.senderNameCode = item.senderName + "(" + item.senderCode + ")";
                    item.receiverNames = item.receivers.map(function(item, idx, origin){
                        return item.user_name + "(" + (item.user_code || "") + ")";
                    }).join(",");
                    if(item.resendtimes == null || item.resendtimes==""){item.resendtimes=0}
                    return item;
                });
                _this.props.table.setAllTableData(tableId, vosToGrid(tableId, newMsgs));
                _this.scrollBottom();
            }
        });
    }


    showMoreReceivers(obj) {
        console.log(obj);
        let _this = this;
        let vos = vosToGrid("receiversTable", obj.receivers.value);
        // _this.props.modal.show('showMoreModal', {
        //     title: '所有收件人',
        //     // content: obj.receiverNames.value,
        //     content: _this.props.table.createSimpleTable("receiversTable", {showCheck:false, showIndex:true, onRowClick:function(a,b,obj,idx){
        //         // console.log("main onRowClick");
        //         //console.log(arguments);
        //         // _this.preResend(obj.pk_message.value, idx);
        //         // _this.showMoreReceivers(obj);
        //     }}),
        //     // beSureBtnClick: () => {
        //     //     _this.resend(pk_message, idx);
        //     // }
        // });
        _this.props.table.setAllTableData("receiversTable", vos);
        _this.setState({showMore: true});
        
    }

    preResend (pk_message, idx) {
        let _this = this;
        _this.props.modal.show('resendModal', {
            title: this.state.json['00220601-000017'],/* 国际化处理： 确认重发*/
            content: this.state.json['00220601-000018'],/* 国际化处理： 确定重发吗？*/
            beSureBtnClick: () => {
                _this.resend(pk_message, idx);
            }
        });
    }

    resend (pk_message, idx) {
        let _this = this;
        ajax({
            url:'/nccloud/riart/message/resend.do',
            data: {pk_message: pk_message, msgType: _this.state.params.msgType},
            success: (res)=>{
                let _msgs = _this.state.messages;
                _this.setState({
                    messages: []
                });
                _msgs[idx].resendtimes = parseInt(_msgs[idx].resendtimes) + 1;
                _this.setState({
                    messages: _msgs
                });
                _msgs = _msgs.map(function(item, idx, origin){
                    item.msgtypeName = _this.state.json[msgtypeNames[item.msgtype]] || item.msgtype;
                    if(item.resendtimes == null || item.resendtimes==""){item.resendtimes=0}
                    return item;
                });
                _this.props.table.setAllTableData(tableId, vosToGrid(tableId, _msgs));
                _this.scrollBottom();
                toast({ color: 'success' });
            }
        });
    }

    componentDidMount () {
        this.initData();
        this.scrollBottom();
    }

    msgsourcetypeChange = value => {
        this.setParam("msgsourcetype", value, true);
    };
    changeSt = (value, selected) => {
        console.log(value);
        let _params = this.state.params;
        if(selected){
            _params.msgtypes.push(value);
        }
        else{
            let idx = _params.msgtypes.indexOf(value);
            if(idx>=0){
                _params.msgtypes.splice(idx,1);
            }
        }
        this.setState({params: _params});
    }
    setParam = (_param, value, refresh) => {
        let _this = this;
        let _state = _this.state;
        if (_state.params[_param] == value) return;
        _state.params[_param] = value;
        _state.params.page = 1;
        _this.setState(_state);
        if (refresh) _this.refreshList(_state.params);
    };
    changeMsgType = value => {
        this.setParam("msgType", value, true);
    }
    setSubject = value => {
        this.setParam("subject", value, false);
    };
    setTime = (startTime, endTime) => {
        let _this = this;
        let _state = _this.state;
        if (_state.params.startTime == startTime && _state.params.endTime == endTime) return;
        _state.params.startTime = startTime;
        _state.params.endTime = endTime;
        _state.params.page = 1;
        _this.setState(_state);
        _this.refreshList(_this.state.params);
    };
    setDateRange = value => {
        if (value.length == 2) {
            this.setTime(value[0].format("YYYY-MM-DD 00:00:00"), value[1].format("YYYY-MM-DD 23:59:59"));
        }
    };
    changeDateRange = value => {
        if (value.length == 2) {
            this.setTime(value[0] + " 00:00:00", value[1] + " 23:59:59");
        }
    };
    setSender = value => {
        let _this = this;
        let _state = _this.state;
        if (_state.params.sender == value) return;
        if (_state.params.sender && value && _state.params.sender.refpk == value.refpk) return;
        _state.params.sender = value;
        _this.setState(_state);
    };
    setReceiver = value => {
        let _this = this;
        let _state = _this.state;
        if (_state.params.receiver == value) return;
        if (_state.params.receiver && value && _state.params.receiver.refpk == value.refpk) return;
        _state.params.receiver = value;
        _this.setState(_state);
    };

    changePage(page, scroll) {
        let _this = this;
        let _params = _this.state.params;
        _params.page = page;
        _this.setState({params: _params});
        if(!scroll){
            _this.refreshList(_params);
        }else{
            _this.refreshList(_params, true);
        }
    }

    //滚动到底部加载更多
    scrollBottom = () => {
        return;
        let page = this.state.params.page;
        let _this = this;
        window.onscroll = function () {
            let scrollTop = document.documentElement.scrollTop || document.body.scrollTop;  //已滚动高度
            let domHeight = document.body.clientHeight; //DOM高度
            let winHeight = document.documentElement.clientHeight; //窗口高度
            if (winHeight + scrollTop >= domHeight) {
                if(!loading){
                    console.log("loading: " + loading + "   page: " + page + " _this.state.totalPage:" + _this.state.totalPage);
                    loading = true;
                    if (_this.state.totalPage > page){
                        page++;
                        _this.changePage(page, true);
                    }
                }
            }
        }
    };


    render() {
        const {table, button, search, form, editTable, syncTree, asyncTree, modal, DragWidthCom } = this.props;
        let {createEditTable} = editTable;
        const {createForm} = form;
        const {createSimpleTable} = table;
        const {createButton} = button;
        const {NCCreateSearch} = search;
        const {activeKey, showSearch, orderdateVal} = this.state;
        let {createSyncTree, setSyncTreeData} = syncTree;
        let {createAsyncTree} = asyncTree;
        let { createModal } = modal;
        let _this = this;

        let params = _this.state.params;
        let dpvs = null;
        if (_this.state.params.startTime.slice(0, 10) && _this.state.params.endTime.slice(0, 10)) {
            dpvs = [moment(_this.state.params.startTime.slice(0, 10)), moment(_this.state.params.endTime.slice(0, 10))];
        }

        let meta = _this.props.meta.getMeta();

        let commitTimeRender = () => {
            return <div>
                <span className="label">{this.state.json['00220601-000023']} : </span>{/* 国际化处理： 提交时间*/}
                <div className="tag-content">
                    <a href="javascript:;" 
                        className={params.startTime == "" && params.endTime == "" ? 'selected-item item': 'item'}
                        onClick={function () { _this.setTime("", ""); }}>
                        {this.state.json['00220601-000024']}{/* 国际化处理： 全部*/}
                    </a>
                    <a href="javascript:;" 
                        className={params.startTime == (new Date()).DateAdd('d', -3).format("yyyy-MM-dd 00:00:00") && params.endTime == ""  ? 'selected-item item': 'item'}
                        onClick={function () { _this.setTime((new Date()).DateAdd('d', -3).format("yyyy-MM-dd 00:00:00"), ""); }}>
                        {this.state.json['00220601-000025']}{/* 国际化处理： 最近三天*/}
                    </a>
                    <a href="javascript:;" 
                        className={params.startTime == (new Date()).DateAdd('d', -7).format("yyyy-MM-dd 00:00:00") && params.endTime == ""  ? 'selected-item item': 'item'}
                        // style={params.startTime == (new Date()).DateAdd('d', -7).format("yyyy-MM-dd 00:00:00") && params.endTime == "" ? selectedStyle : {}}
                        onClick={function () { _this.setTime((new Date()).DateAdd('d', -7).format("yyyy-MM-dd 00:00:00"), ""); }}>
                        {this.state.json['00220601-000026']}{/* 国际化处理： 最近一周*/}
                    </a>
                    <a href="javascript:;" className="item"
                        className={params.startTime == (new Date()).DateAdd('m', -1).format("yyyy-MM-dd 00:00:00") && params.endTime == ""  ? 'selected-item item': 'item'}
                        // style={params.startTime == (new Date()).DateAdd('m', -1).format("yyyy-MM-dd 00:00:00") && params.endTime == "" ? selectedStyle : {}}
                        onClick={function () { _this.setTime((new Date()).DateAdd('m', -1).format("yyyy-MM-dd 00:00:00"), ""); }}>
                        {this.state.json['00220601-000027']}{/* 国际化处理： 最近一月*/}
                    </a>
                    {/* <a href="javascript:;" className="item"
                        style={params.startTime == "" && params.endTime == (new Date()).DateAdd('m', -1).format("yyyy-MM-dd 00:00:00") ? selectedStyle : {}}
                        onClick={function () { _this.setTime("", (new Date()).DateAdd('m', -1).format("yyyy-MM-dd 00:00:00")); }}>
                        更早
                    </a> */}
                </div>
                <div className="form-item-content">
                    <div class="item search-item">
                        <NCRangePickerClient
                            format={"YYYY-MM-DD"}
                            onSelect={_this.setDateRange.bind(this)}
                            onChange={_this.changeDateRange.bind(this)}
                            showClear={true}
                            showOk={true}
                            className={'range-fixed'}
                            value={dpvs}
                            // defaultValue={this.state.value}
                            placeholder={this.state.json['00220601-000019']}/* 国际化处理： 请搜索提交时间*/
                            dateInputPlaceholder={[this.state.json['00220601-000020'], this.state.json['00220601-000021']]}/* 国际化处理： 开始,结束*/
                        />
                    </div>
                </div>
            </div>
        }

        
        if(meta[tableId] && meta[tableId].items[meta[tableId].items.length-1].label == this.state.json['00220601-000011']){/* 国际化处理： 操作*/
            meta[tableId].items.splice(meta[tableId].items.length-1, 1);
        }
        let event = {
            label: this.state.json['00220601-000011'],/* 国际化处理： 操作*/
            attrcode: 'opr',
            itemtype: 'message',
            visible: true || ["approve", "todo"].indexOf(_this.state.params.msgType)==-1,
            render: (text, record, index) => {
            let buttonAry = ["approve", "todo"].indexOf(_this.state.params.msgType)==-1 ? [ "showmore", "resend" ] : [ "showmore" ];
            return _this.props.button.createOprationButton(buttonAry, {
                area: "opr",
                buttonLimit: 3,
                onButtonClick: (props, key) => {
                    // console.log(key);
                    // console.log(props, key, text, record, index);
                    if(key == "resend"){
                        _this.preResend(record.pk_message.value, index);
                    }
                    else if(key == "showmore"){
                        _this.showMoreReceivers(record);
                    }
                }
            });
        }
        };

        if(meta[tableId] && meta[tableId].items[meta[tableId].items.length-1].label != this.state.json['00220601-000011']){/* 国际化处理： 操作*/
            if(this.state.json['00220601-000011'])meta[tableId].items.push(event);
            // _this.props.meta.setMeta(meta);
        }


        return (
            <div className="message-wrap nc-single-table">
                <div className="nc-singleTable-header-area">
                    <div className="header-title-search-area">
                        <h2 className="title-search-detail">{this.state.json['00220601-000029']}</h2>{/* 国际化处理： 消息管理中心*/}
                    </div>
                    <div className="header-button-area">
                        {/* {this.props.button.createButtonApp({
                            area: 'page_header',
                            buttonLimit: 7, 
                            onButtonClick: buttonClick.bind(this), 
                            popContainer: document.querySelector('.header-button-area')
                        })} */}
                    </div>
                </div>
                <div className="message-search-container nc-singleTable-search-area" style={{"margin-top": "8px", "margin-bottom": "8px"}}>
                    <div className="search-line inline">
                        <span className="label">{this.state.json['00220601-000030']} : </span>{/* 国际化处理： 消息类型*/}
                        <NCSelect
                            style={{ width: 200, display: "inline-block", "max-width": 300}}
                            value={_this.state.params.msgType}
                            onChange={_this.changeMsgType}
                            showClear={false}
                            >
                            <NCOption key={"approve"}>{this.state.json['00220601-000031']}</NCOption>{/* 国际化处理： 审批消息*/}
                            <NCOption key={"todo"}>{this.state.json['00220601-000032']}</NCOption>{/* 国际化处理： 业务消息*/}
                            <NCOption key={"prealert"}>{this.state.json['00220601-000033']}</NCOption>{/* 国际化处理： 预警消息*/}
                            <NCOption key={"notice"}>{this.state.json['00220601-000034']}</NCOption>{/* 国际化处理： 通知消息*/}
                        </NCSelect>
                    </div>
                    <div className="search-line inline">
                        <span className="label">{this.state.json['00220601-000005']} : </span>{/* 国际化处理： 消息发送类型*/}
                        <NCCheckbox checked={_this.state.params.msgtypes.indexOf("nc")>=0} onChange={function(e){
                            _this.changeSt("nc", e);
                        }}>{this.state.json['00220601-000012']}</NCCheckbox>{/* 国际化处理： 系统*/}
                        <NCCheckbox checked={_this.state.params.msgtypes.indexOf("email")>=0} onChange={function(e){
                            _this.changeSt("email", e);
                        }}>{this.state.json['00220601-000013']}</NCCheckbox>{/* 国际化处理： 邮件*/}
                        <NCCheckbox checked={_this.state.params.msgtypes.indexOf("sms")>=0} onChange={function(e){
                            _this.changeSt("sms", e);
                        }}>{this.state.json['00220601-000014']}</NCCheckbox>{/* 国际化处理： 短信*/}
                    </div>
                    <div className="search-line">
                        {commitTimeRender()}
                    </div>
                    <div className="search-line">
                            <span className="label">{this.state.json['00220601-000035']} : </span>{/* 国际化处理： 更多条件*/}
                            <div className="form-item-content">
                                <div class="item refer-item">
                                    <UserRefer
                                        value={params.sender}
                                        onChange={_this.setSender}
                                        isMultiSelectedEnabled={false}
                                        queryCondition={{
                                            showSysAdmin: true,
                                            showGroupAdmin: true
                                        }}
                                        placeholder={this.state.json['00220601-000003']}/* 国际化处理： 发送人*/
                                    />
                                </div>
                                <div class="item refer-item">
                                    <UserRefer
                                        value={params.receiver}
                                        onChange={_this.setReceiver}
                                        isMultiSelectedEnabled={false}
                                        queryCondition={{
                                            showSysAdmin: true,
                                            showGroupAdmin: true
                                        }}
                                        placeholder={this.state.json['00220601-000022']}/* 国际化处理： 接收人*/
                                    />
                                </div>
                                <div class="item">
                                    <NCFormControl
                                        className="search-item"
                                        value={params.subject}
                                        onChange={_this.setSubject}
                                        placeholder={this.state.json['00220601-000002']}/* 国际化处理： 标题*/
                                    />
                                </div>
                                <NCButton colors="primary" onClick={function () { _this.refreshList() }}>{this.state.json['00220601-000036']}</NCButton>{/* 国际化处理： 确定*/}
                            </div>
                        </div>
                </div>
                <div className="message-list-container"> 
                    <div className="list-header">
                        <div className="header-layout" style={{width: 140}}>
                                    {_this.state.total == 0 ? "" : <span class="total">{this.state.inlt&&this.state.inlt.get('10160501-000037',{'total':_this.state.total})}</span>}{/* 国际化处理： 共,条*/}
                        </div>
                    </div>
                </div>
                <div className="nc-singleTable-table-area">
                    {/* <div className ="nc-bill-table-area"> */}
                        {createSimpleTable(tableId, {showCheck:false, showIndex:true, onRowClick:function(a,b,obj,idx){
                            // console.log("main onRowClick");
                            //console.log(arguments);
                            // _this.preResend(obj.pk_message.value, idx);
                            // _this.showMoreReceivers(obj);
                        }})}
                    {/* </div> */}
                </div>
                {/* {this.state.total > 0 ? 
                <div className="loading-container">
                    <span className="icon iconfont icon-gundongxiangxia"></span>
                    <span>{this.state.loadingText}</span>
                </div> : ''} */}
                <div>
                    <NCPagination
                        first
                        last
                        prev
                        next
                        boundaryLinks
                        items={_this.state.totalPage}
                        maxButtons={5}
                        activePage={_this.state.params.page}
                        onSelect={_this.changePage} />
                </div>
                {createModal('resendModal', {size: "lg"})}
                {/* {createModal('showMoreModal')} */}
                <NCModal
                        show={_this.state.showMore}
                        size="lg"
                        onHide={function(){
                            _this.setState({showMore: false});
                        }}
                    >
                    <NCModal.Header closeButton>
                        <NCModal.Title > {this.state.json['00220601-000039']} </NCModal.Title>{/* 国际化处理： 所有收件人*/}
                    </NCModal.Header >
    
                    <NCModal.Body >
                        {_this.props.table.createSimpleTable("receiversTable", {showCheck:false, showIndex:true, onRowClick:function(a,b,obj,idx){
                            // console.log("main onRowClick");
                            //console.log(arguments);
                            // _this.preResend(obj.pk_message.value, idx);
                            // _this.showMoreReceivers(obj);
                        }})}
                    </NCModal.Body>
                    
                </NCModal>
            </div>
            
        )
    }
}

 
MessageList = createPage({
    initTemplate: initTemplate,
})(MessageList);

ReactDOM.render(
    <MessageList />
    , document.querySelector("#app"));

