import { createPage, ajax, base, toast } from 'nc-lightapp-front';
let { NCPopconfirm, NCMessage } = base;
const { NCButton, NCSelect, NCOption, NCModal, NCTable, NCRow, NCCol, NCTree, NCBreadcrumb,
  NCFormControl, NCTabs, NCForm } = base;
let tableId = 'message';
export default function (props) {
  let callback = (json, status, inlt) => { // json 多语json格式参数； status： 是否请求到json多语，可用来判断多语资源是否请求到并进行一些别的操作； inlt： 可用来进行占位符的一些操作
    if (status) {

      let _this = this;
      let buttons = {
        listbuttons: [
          {
            "id": "0001A41000000006J5B1",
            "type": "button_main",
            "key": "showmore",
            "title": json['00220601-000000'],/* 国际化处理： 查看接收人*/
            "area": "opr",
            "children": []
          },
          {
            "id": "0001A41000000006J5B2",
            "type": "message",
            "key": "resend",
            "title": json['00220601-000001'],/* 国际化处理： 重发*/
            "area": "opr",
            "children": []
          }
        ]
      };
      props.button.setButtons(buttons.listbuttons);
      let res =
        {
          "data": {
            'message': {
              "code": 'message',
              "items": [
                {
                  "itemtype": "input",
                  "required": false,
                  "label": json['00220601-000002'],/* 国际化处理： 标题*/
                  "attrcode": "subject",
                  "maxlength": "50",
                  "visible": true,
                  "metapath": "subject"
                },
                {
                  "itemtype": "input",
                  "required": false,
                  "visible": true,
                  "label": json['00220601-000003'],/* 国际化处理： 发送人*/
                  "attrcode": "senderNameCode",
                  "maxlength": "100",
                  "metapath": "senderNameCode"
                },
                {
                  "itemtype": "input",
                  "required": false,
                  "visible": true,
                  "label": json['00220601-000004'],/* 国际化处理： 发送时间*/
                  "attrcode": "sendtime",
                  "maxlength": "100",
                  "metapath": "sendtime",
                  // "width": '100'
                },
                {
                  "itemtype": "input",
                  "required": false,
                  "visible": true,
                  "label": json['00220601-000005'],/* 国际化处理： 消息发送类型*/
                  "attrcode": "msgtypeName",
                  "maxlength": "100",
                  "metapath": "msgtypeName",
                  // "width": '100'
                },
                {
                  "itemtype": "input",
                  "required": false,
                  "visible": true,
                  "label": json['00220601-000006'],/* 国际化处理： 消息来源类型*/
                  "attrcode": "msgstype",
                  "maxlength": "100",
                  "metapath": "msgstype",
                  // "width": '100'
                },
                {
                  "itemtype": "input",
                  "required": false,
                  "visible": true,
                  "label": json['00220601-000007'],/* 国际化处理： 收件人*/
                  "attrcode": "receiverNames",
                  "maxlength": "100",
                  "metapath": "receiverNames",
                  // "width": '100'
                },
                {
                  "itemtype": "input",
                  "required": false,
                  "visible": true,
                  "label": json['00220601-000008'],/* 国际化处理： 重发次数*/
                  "attrcode": "resendtimes",
                  "maxlength": "100",
                  "metapath": "resendtimes",
                  // "width": '100'
                }
              ],
              "moduletype": "table",
              "name": tableId,
              "pagination": false,
              showIndex: true,
              status: "browse"
            },
            'receiversTable': {
              "code": 'receiversTable',
              "items": [
                {
                  "itemtype": "input",
                  "required": false,
                  "label": json['00220601-000009'],/* 国际化处理： 编码*/
                  "attrcode": "user_code",
                  "maxlength": "50",
                  "visible": true,
                  "metapath": "user_code"
                },
                {
                  "itemtype": "input",
                  "required": false,
                  "visible": true,
                  "label": json['00220601-000010'],/* 国际化处理： 名称*/
                  "attrcode": "user_name",
                  "maxlength": "100",
                  "metapath": "user_name"
                }
              ],
              "moduletype": "table",
              "name": "receiversTable",
              "pagination": false,
              showIndex: true,
              status: "browse"
            },
            "code": "messagegrid"
          }

        }
      //添加表格操作列
      let event = {
        label: json['00220601-000011'],/* 国际化处理： 操作*/
        attrcode: 'opr',
        itemtype: 'message',
        visible: true,
        render: (text, record, index) => {
          let buttonAry = ["resend"];
          return props.button.createOprationButton(buttonAry, {
            area: "opr",
            buttonLimit: 3,
            onButtonClick: (props, key) => {
              console.log(key);
              console.log(props, key, text, record, index);
            }
          });
        }
      };

      let meta = res.data;
      // meta[tableId].items.push(event);
      props.meta.setMeta(meta);
    }
  }

  props.MultiInit.getMultiLang({ moduleId: '0022-00220601', domainName: 'uap', callback });
}
