let t0 = parseFloat((new Date()).getTime())/1000;

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, base, ajax, high, toast } from 'nc-lightapp-front';
// import ajax from '../../../api/ajax.js';
import UserRefer from '../../../refer/riart/userRefer';
const { NCButton, NCPagination, NCTable, NCOption, NCInput, NCCheckbox, NCSelect, NCIcon, NCPopconfirm, NCPopover, NCDatePicker, NCBreadcrumb, NCFormControl, NCModal, NCDropdown, NCMenu } = base;
const NCBreadcrumbItem = NCBreadcrumb.NCBreadcrumbItem;
const { NCRangePicker } = NCDatePicker;
const { Item } = NCMenu;
const { ToastModal, ApproveComment, ApproveProcess } = high;
import Transfer from '../../../public/excomponents/Transfer';
import moment from "moment/moment";
import './index.less';
import '../index.less';
import { SSL_OP_NO_TLSv1_2 } from 'constants';

//if(window.location.href.indexOf("localhost") >=0)sessionStorage.setItem('gzip', 0);
//const ajax = jQuery.ajax;



let t1 = parseFloat((new Date()).getTime())/1000;
let t2 = parseFloat((new Date()).getTime())/1000;
let t3 = parseFloat((new Date()).getTime())/1000;
let t4 = parseFloat((new Date()).getTime())/1000;
let t5 = null;

class MessageList extends Component {
    constructor(props) {
        super(props);
        this.props = props;
        let { form, button, table, search ,syncTree } = this.props;
        let { setSyncTreeData } = syncTree;
        this.setSyncTreeData = setSyncTreeData;   //设置树根节点数据方法
        let _this = this;
        this.state = {
            anything: null
        }
    }

    componentDidMount () {
        let _this = this;
        t2 = parseFloat((new Date()).getTime())/1000;
        ajax({
            url: '/nccloud/riart/message/summary.do',
            data: {},
            method: 'POST',
            success: (res) => {
                t3 = parseFloat((new Date()).getTime())/1000;
                _this.setState({anything: true});
            }
        });
    }

    render () {
        t4 = parseFloat((new Date()).getTime())/1000;
        if(t5==null)t5 = parseFloat((new Date()).getTime())/1000;
        return <div>
            <p>页面加载时间分析(单位：秒)</p>
            <p>进入页面到引入组件花费的时间：{t1-t0}</p>
            <p>进入页面到第一次渲染页面结束花费的时间：{t5-t0}</p>
            <p>进入页面到开始执行组件代码花费的时间：{t2-t0}</p>
            <p>ajax花费时间：{t3-t2}</p>
            <p>ajax结束后渲染页面结束的总时间(整个页面加载完成总耗时)：{t4-t0}</p>
        </div>;
    }
}

let MessageListPage = createPage({
    //initTemplate: initTemplate,
})(MessageList);

ReactDOM.render(
    <MessageListPage />
    , document.querySelector("#app"));

