import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, base, ajax, high, toast, LocalToDongbaTime, DongbaToLocalTime } from 'nc-lightapp-front';
// import ajax from '../../../api/ajax.js';
import { initTemplate } from './events/initTemplate.js';
import UserRefer from '../../../refer/riart/userRefer';
import Utils from '../../../public/utils';
import AddUserRefer from '../../../refer/riart/userReferWithDeptAndOrg';
const { NCButton, NCPagination, NCCheckbox, NCSelect, NCPopover, NCDatePicker, NCRangePickerClient, NCFormControl, NCModal, NCDropdown, NCMenu } = base;
const NCOption = NCSelect.NCOption;
// const NCBreadcrumbItem = NCBreadcrumb.NCBreadcrumbItem;
// const { NCRangePicker } = NCDatePicker;
const { Item } = NCMenu;
const { ApproveDetail, ApproveBtns, ApprovalTrans } = high;
import BatchApprove from './batchApprove.js';
import moment from "moment/moment";
import './index.less';
import '../index.less';


//if (window.location.href.indexOf("localhost") >= 0) sessionStorage.setItem('gzip', 0);
//const ajax = jQuery.ajax;

const specialWorkflowtypes = [4, 5];
let domHeight = document.body.clientHeight;
const cloneObj = function (obj) {
    var o;
    if (typeof obj == "object") {
        if (obj === null) {
            o = null;
        } else {
            if (obj instanceof Array) {
                o = [];
                for (var i = 0, len = obj.length; i < len; i++) {
                    o.push(cloneObj(obj[i]));
                }
            } else {
                o = {};
                for (var j in obj) {
                    o[j] = cloneObj(obj[j]);
                }
            }
        }
    } else {
        o = obj;
    }
    return o;
};
// const cloneObj = function(obj){
// return jQuery.extend(true, {}, obj, {});
// };
const convertFormData = function (obj, formName) {
    let _obj = {};
    for (let k in obj) {
        _obj[k] = { "value": obj[k] }
    }
    let ret = {};
    ret[formName] = {
        "areacode": formName,
        "rows": [
            {
                "status": "0",
                "values": _obj
            }
        ]
    }
    return ret;
};

Date.prototype.format = function (format) {
    var date = {
        "M+": this.getMonth() + 1,
        "d+": this.getDate(),
        "h+": this.getHours(),
        "m+": this.getMinutes(),
        "s+": this.getSeconds(),
        "q+": Math.floor((this.getMonth() + 3) / 3),
        "S+": this.getMilliseconds()
    };
    if (/(y+)/i.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length));
    }
    for (var k in date) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length == 1
                ? date[k] : ("00" + date[k]).substr(("" + date[k]).length));
        }
    }
    return format;
};

Date.prototype.DateAdd = function (strInterval, Number) {
    var dtTmp = this;
    switch (strInterval) {
        case 's': return new Date(Date.parse(dtTmp) + (1000 * Number));
        case 'n': return new Date(Date.parse(dtTmp) + (60000 * Number));
        case 'h': return new Date(Date.parse(dtTmp) + (3600000 * Number));
        case 'd': return new Date(Date.parse(dtTmp) + (86400000 * Number));
        case 'w': return new Date(Date.parse(dtTmp) + ((86400000 * 7) * Number));
        case 'q': return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) + Number * 3, dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds());
        case 'm': return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) + Number, dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds());
        case 'y': return new Date((dtTmp.getFullYear() + Number), dtTmp.getMonth(), dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds());
    }
}

let getParams = function () {
    let url = location.search; //获取url中"?"符后的字串   
    if (!url || url == "") url = location.hash; //获取url中"#"符后的字串   
    let params = new Object();
    if (url.indexOf("?") == 0 || url.indexOf("#") == 0) {
        let str = url.substr(1);
        if (url.indexOf("#/?") == 0) str = url.substr(3);
        let strs = str.split("&");
        for (let i = 0; i < strs.length; i++) {
            params[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
        }
    }
    return params;
};
const urlParams = getParams();

// const approveActions = [
//     //审批内所有的操作
//     { display: this.state.json['10160501-000000'], value: 'approve' }, // 批准/* 国际化处理： 批准*/
//     { display: this.state.json['10160501-000001'], value: 'Noapprove' }, // 不批准/* 国际化处理： 不批准*/
//     { display: this.state.json['10160501-000002'], value: 'reject' } // 驳回/* 国际化处理： 驳回*/
// ];

var pageMsgType = urlParams["msgType"] || "approve";//从小应用跳转过来的页面类型
let curPageType = urlParams["curPageType"] || pageMsgType;//当前选中的页面类型
pageMsgType = curPageType || pageMsgType;
var isTest = false;
var readTxt = '10160501-000025';/* 国际化处理： 已审批*/
var unreadTxt = '10160501-000026';/* 国际化处理： 未审批*/
let changePageMsgType = function (newPageMsgType) {
    pageMsgType = newPageMsgType;
    curPageType = newPageMsgType;
    if (pageMsgType == "approve") {
        readTxt = '10160501-000025';/* 国际化处理： 已审批*/
        unreadTxt = '10160501-000026';/* 国际化处理： 未审批*/
    }
    if (pageMsgType == "todo" || pageMsgType == "prealert") {
        readTxt = '10160501-000027';/* 国际化处理： 已处理*/
        unreadTxt = '10160501-000028';/* 国际化处理： 未处理*/
    }
    if (pageMsgType == "notice") {
        readTxt = '10160501-000029';/* 国际化处理： 已读*/
        unreadTxt = '10160501-000030';/* 国际化处理： 未读*/
    }
}
changePageMsgType(pageMsgType);
var readStateChanged = false;
let loading = false;
var initParam = "";
if (urlParams["ipm"] == "recent3") {
    initParam = "recent3";
}
if (urlParams["ipm"] == "recent7") {
    initParam = "recent7";
}
if (urlParams["ipm"] == "recent30") {
    initParam = "recent30";
}

class MessageCountBox extends Component {
    constructor(props) {
        super(props);
        this.props = props;
        this.state = {
            currentPage: "approve",
            json: this.props.json,
            inlt: this.props.inlt
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.pageType !== this.state.currentPage) {
            this.setState({
                currentPage: nextProps.pageType
            });
        }
        if (JSON.stringify(nextProps.json) != "{}") {
            this.setState({ json: nextProps.json, inlt: nextProps.inlt });
        }
    }

    render() {
        let { currentPage } = this.state;
        let { counts } = this.props;
        let tabList = [
            { title: this.state.json['10160501-000021'], key: 'approve' },/* 国际化处理： 审批中心*/
            { title: this.state.json['10160501-000022'], key: 'todo' },/* 国际化处理： 业务中心*/
            { title: this.state.json['10160501-000023'], key: 'prealert' },/* 国际化处理： 预警中心*/
            { title: this.state.json['10160501-000024'], key: 'notice' },/* 国际化处理： 通知中心*/
        ]
        //根据不同页面类型将对应页签移到首位，只有从其他页面跳转过来时才重新排序，切换tab页签不进行排序
        tabList.map((item, index, array) => {
            if (urlParams["msgType"] === item.key) {
                let curItem = array.splice(index, 1);
                array.unshift(curItem[0]);
                return array;
            }
        });

        return (
            <div className="message-tab-container">
                {
                    tabList.map(item =>
                        <div className={item.key === curPageType ? 'tab-item active' : 'tab-item'} onClick={() => this.props.onPageChange(item.key)}>
                            {item.title}
                            <span className="count">{counts[item.key]}</span>
                        </div>
                    )
                }
            </div>
        );
    }
}

const isreadType = { "all": "", "isread": "Y", "unread": "N" };

class MessageSearchBox extends Component {
    constructor(props) {
        super(props);
        this.props = props;
        this.state = this.props;
        this.state.json = this.props.json;
        this.state.inlt = this.props.inlt;
        this.state.billtypes = [];
        this.state.msgsourcetypes = [];
        // this.state.params = { isread: "unread", startTime: (pageMsgType == "todo" ? "" : (new Date()).DateAdd('m', -1).format("yyyy-MM-dd 00:00:00") ), endTime: "", msgsourcetype: "", billtype: "", billno: "", subject: "" };
        this.state.params = cloneObj(this.props.listParams);
        this.state.params.isread = this.props.listParams.isread == "N" ? "unread" : "isread";
        this.state.showMoreSearch = true;//是否显示高级查询
        this.state.showMoreBilltype = false;//是否显示更多单据类型
        this.state.moreBtnShow = false;//是否显示单据类型更多按钮
        this.state.dateRangePlaceholder = this.state.json['10160501-000031'];/* 国际化处理： 请搜索提交时间*/
        let _this = this;
        let binds = "billtypeChange msgsourcetypeChange setIsread setBillno setSubject setTime setSender refreshList";
        binds.split(" ").forEach(function (item) {
            _this[item] = _this[item].bind(_this);
        });
    }

    componentWillReceiveProps(nextProps) {
        if (JSON.stringify(nextProps.json) != "{}") {
            this.setState({ json: nextProps.json, inlt: nextProps.inlt });
        }
    }
    setParam = (_param, value, refresh) => {
        let _this = this;
        let _state = _this.state;
        if (_state.params[_param] == value) return;
        _state.params[_param] = value;
        _this.setState(_state);
        if (refresh) _this.refreshList();
    };
    billtypeChange = value => {
        this.setParam("billtype", value, true);
    };
    msgsourcetypeChange = value => {
        this.setParam("msgsourcetype", value, true);
    };
    setIsread = value => {
        this.setParam("isread", value, true);
    };
    setBillno = value => {
        this.setParam("billno", value, false);
    };
    setSubject = value => {
        this.setParam("subject", value, false);
    };
    setTime = (startTime, endTime) => {
        let _this = this;
        let _state = _this.state;
        if (_state.params.startTime == startTime && _state.params.endTime == endTime) return;
        _state.params.startTime = startTime;
        _state.params.endTime = endTime;
        _this.setState(_state);
        _this.refreshList();
    };
    setDateRange = value => {
        if (value.length == 2) {
            this.setTime(value[0].format("YYYY-MM-DD 00:00:00"), value[1].format("YYYY-MM-DD 23:59:59"));
        }
    };
    changeDateRange = value => {
        if (value.length == 2) {
            this.setTime(value[0] + " 00:00:00", value[1] + " 23:59:59");
        }
    };
    setSender = value => {
        let _this = this;
        let _state = _this.state;
        if (_state.params.sender == value) return;
        if (_state.params.sender && value && _state.params.sender.refpk == value.refpk) return;
        _state.params.sender = value;
        _this.setState(_state);
        // _this.refreshList();
    };

    refreshList() {
        let _this = this;
        let _listParams = cloneObj(this.props.listParams);
        let samePs = "billtype msgsourcetype billno subject startTime endTime"
        samePs.split(" ").forEach(function (item, idx, origin) {
            _listParams[item] = _this.state.params[item];
        });
        _listParams.isread = isreadType[this.state.params.isread];
        _listParams.senderId = "";
        _listParams.page = 1;
        if (this.state.params.sender && this.state.params.sender.refpk && this.state.params.sender.refpk.length > 0) {
            _listParams.senderId = this.state.params.sender.refpk;
        }
        // return;//todo
        this.props.refreshList(_listParams);
    }

    //计算是否显示单据类型更多按钮
    billTypeBtnShow = () => {
        let $billTypeArea = document.querySelector("#billTypeArea");
        let billTypeWidthArr = [...$billTypeArea.querySelectorAll(".tag-content .item")].map(item => item.offsetWidth);
        let billTypeAreaWidth = $billTypeArea.offsetWidth - $billTypeArea.querySelector(".label").offsetWidth - $billTypeArea.querySelector(".more-right").offsetWidth;
        let billTypeWidth = billTypeWidthArr.reduce((prev, cur) => prev + cur);
        if (billTypeWidth >= billTypeAreaWidth) {
            this.setState({
                moreBtnShow: true
            });
        }
    }

    getMyBilltypes() {
        let _this = this;
        if (curPageType == "approve" || curPageType == "todo") {
            ajax({
                url: '/nccloud/riart/message/getMyBilltypes.do',
                data: { msgType: curPageType },
                method: 'POST',
                success: (res) => {
                    let retData = res.data;
                    if (!retData.names) retData.names = {};

                    // let _data = ["all", "isread", "unread"];
                    let _data = ["unread"];
                    let billtypes = {};
                    for (let i = 0; i < _data.length; i++) {
                        billtypes[_data[i]] = retData.number[_data[i]].map(function (item, idx, origin) { return { name: retData.names[item.billtype] || (item.billtype == null || item.billtype == "" ? this.state.json['10160501-000032'] : item.billtype), id: item.billtype, count: item.count }; });/* 国际化处理： 其他*/
                    }

                    _this.setState({
                        billtypes: billtypes
                    }, () => {
                        _this.billTypeBtnShow();
                    });
                }
            });
        }
    }

    getMyMsgstypes() {
        // let _this = this;
        // if (pageMsgType == "todo" || pageMsgType == "notice" || isTest) {
        //     ajax({
        //         url: '/nccloud/riart/message/getMyMsgstypes.do',
        //         // data: { msgType: "approve" },
        //         data: { msgType: isTest ? "approve" : pageMsgType },
        //         method: 'POST',
        //         success: (res) => {
        //             let retData = res.data;
        //             if (!retData.names) retData.names = {};

        //             // let _data = ["all", "isread", "unread"];
        //             let _data = ["unread"];
        //             let msgsourcetypes = {};
        //             for (let i = 0; i < _data.length; i++) {
        //                 msgsourcetypes[_data[i]] = retData.number[_data[i]].map(function (item, idx, origin) { return { name: retData.names[item.msgsourcetype] || item.msgsourcetype, id: item.msgsourcetype, count: item.count }; });
        //             }
        //             _this.setState({ msgsourcetypes: msgsourcetypes });
        //         }
        //     });
        // }
    }

    componentDidMount() {
        let _this = this;
        _this.getMyBilltypes();
        _this.getMyMsgstypes();
    }

    render() {
        let _this = this;
        if (_this.props.needRefresh) {
            let flag = false;
            let _ps = _this.state.params;
            let _isread = "all";
            if(_this.props.listParams.isread == "N"){
                _isread = "unread";
            }
            if(_this.props.listParams.isread == "Y"){
                _isread = "isread";
            }
            if(_ps.isread != _isread){
                _ps.isread = _isread;
                flag = true;
            }
            if (_ps.sender && _ps.sender.refpk && _ps.sender.refpk.length > 0 && (_this.props.listParams.senderId == null || _this.props.listParams.senderId.length == 0)) {
                _ps.sender = null;
                flag = true;
            }
            
            let samePs = "billtype msgsourcetype billno subject startTime endTime";
            let sameParams = samePs.split(" ");
            for(let i=0;i<sameParams.length;i++){
                if(_ps[sameParams[i]] != _this.props.listParams[sameParams[i]]){
                    _ps[sameParams[i]] = _this.props.listParams[sameParams[i]];
                    flag = true;
                }
            }
            if(flag)_this.setState({params: _ps});
            _this.getMyBilltypes();
            _this.getMyMsgstypes();
            _this.props.countRefreshed();
        }
        // let billtypeOptions = [];
        // if(_billtypes){
        //     for(let i=0; i<_billtypes.length; i++){
        //         billtypeOptions.push(<NCOption value={_billtypes[i].id}>{_billtypes[i].name}({_billtypes[i].count})</NCOption>);
        //     }
        // }
        let ps = _this.state.params;

        //let _billtypes = _this.state.billtypes[_this.state.params.isread];
        let _billtypes = _this.state.billtypes["unread"];
        let billtypeOptions = [<span className={['point', 'item', ps.billtype == "" ? 'selected-search-item' : ''].join(' ')} onClick={function () { _this.billtypeChange(""); }}>{this.state.json['10160501-000063']}</span>];/* 国际化处理： 全部*/
        if (_billtypes) {
            for (let i = 0; i < _billtypes.length; i++) {
                billtypeOptions.push(<span className={['point', 'item', ps.billtype == _billtypes[i].id ? 'selected-search-item' : ''].join(' ')} onClick={function () { _this.billtypeChange(_billtypes[i].id); }}>{_billtypes[i].name} <span className="count">{_billtypes[i].count}</span></span>);
            }
        }

        // let _msgsourcetypes = _this.state.msgsourcetypes[_this.state.params.isread];
        let _msgsourcetypes = _this.state.msgsourcetypes["unread"];
        let msgsourcetypeOptions = [<a href="javascript:;" className={ps.msgsourcetype == "" ? 'selected-search-item item' : 'item'} onClick={function () { _this.msgsourcetypeChange(""); }}>{this.state.json['10160501-000063']}</a>];/* 国际化处理： 全部*/
        if (_msgsourcetypes) {
            for (let i = 0; i < _msgsourcetypes.length; i++) {
                msgsourcetypeOptions.push(<a href="javascript:;" className={ps.msgsourcetype == _msgsourcetypes[i].id ? 'selected-search-item item' : 'item'} onClick={function () { _this.msgsourcetypeChange(_msgsourcetypes[i].id); }}>{_msgsourcetypes[i].name} <span className="count">{_msgsourcetypes[i].count}</span></a>);
            }
        }

        let dpvs = null;
        if (_this.state.params.startTime.slice(0, 10) && _this.state.params.endTime.slice(0, 10)) {
            dpvs = [moment(_this.state.params.startTime.slice(0, 10)), moment(_this.state.params.endTime.slice(0, 10))];
        }
        let commitTimeRender = () => {
            return <div>
                <span className="label">{this.state.json['10160501-000064']} : </span>{/* 国际化处理： 提交时间*/}
                <div className="tag-content">
                    <span
                        className={['point', 'item', ps.startTime == "" && ps.endTime == "" ? 'selected-search-item' : ''].join(' ')}
                        onClick={function () { _this.setTime("", ""); }}>
                        {this.state.json['10160501-000063']/* 国际化处理： 全部*/}
                    </span>
                    <span
                        className={['point', 'item', ps.startTime == (new Date()).DateAdd('d', -3).format("yyyy-MM-dd 00:00:00") && ps.endTime == "" ? 'selected-search-item' : ''].join(' ')}
                        onClick={function () { _this.setTime((new Date()).DateAdd('d', -3).format("yyyy-MM-dd 00:00:00"), ""); }}>
                        {this.state.json['10160501-000065']/* 国际化处理： 最近三天*/}
                    </span>
                    <span
                        className={['point', 'item', ps.startTime == (new Date()).DateAdd('d', -7).format("yyyy-MM-dd 00:00:00") && ps.endTime == "" ? 'selected-search-item' : ''].join(' ')}
                        // style={ps.startTime == (new Date()).DateAdd('d', -7).format("yyyy-MM-dd 00:00:00") && ps.endTime == "" ? selectedStyle : {}}
                        onClick={function () { _this.setTime((new Date()).DateAdd('d', -7).format("yyyy-MM-dd 00:00:00"), ""); }}>
                        {this.state.json['10160501-000066']/* 国际化处理： 最近一周*/}
                    </span>
                    <span
                        className={['point', 'item', ps.startTime == (new Date()).DateAdd('m', -1).format("yyyy-MM-dd 00:00:00") && ps.endTime == "" ? 'selected-search-item' : ''].join(' ')}
                        // style={ps.startTime == (new Date()).DateAdd('m', -1).format("yyyy-MM-dd 00:00:00") && ps.endTime == "" ? selectedStyle : {}}
                        onClick={function () { _this.setTime((new Date()).DateAdd('m', -1).format("yyyy-MM-dd 00:00:00"), ""); }}>
                        {this.state.json['10160501-000067']/* 国际化处理： 最近一月*/}
                    </span>
                    {/* <a href="javascript:;" className="item"
                        style={ps.startTime == "" && ps.endTime == (new Date()).DateAdd('m', -1).format("yyyy-MM-dd 00:00:00") ? selectedStyle : {}}
                        onClick={function () { _this.setTime("", (new Date()).DateAdd('m', -1).format("yyyy-MM-dd 00:00:00")); }}>
                        更早
                    </a> */}
                </div>
                <div className="form-item-content">
                    <div class="item search-item">
                        <NCRangePickerClient
                            format={"YYYY-MM-DD"}
                            onSelect={_this.setDateRange.bind(this)}
                            onChange={_this.changeDateRange.bind(this)}
                            showClear={true}
                            showOk={true}
                            className={'range-fixed'}
                            value={dpvs}
                            // defaultValue={this.state.value}
                            placeholder={this.state.dateRangePlaceholder}
                            dateInputPlaceholder={[this.state.json['10160501-000033'], this.state.json['10160501-000034']]}/* 国际化处理： 开始,结束*/
                        />
                    </div>
                </div>
            </div>
        }

        return (
            <div className="message-search-container">
                <div className="search-line">
                    <span className="label">{this.state.json['10160501-000069']} : </span>{/* 国际化处理： 处理状态*/}
                    <div className="tag-content">
                        <span className={['item', 'point', ps.isread == 'all' ? 'selected-search-item' : ''].join(' ')}
                            onClick={function () { _this.setIsread("all"); }}>{this.state.json['10160501-000063']}</span>{/* 国际化处理： 全部*/}
                        <span className={['item', 'point', ps.isread == 'unread' ? 'selected-search-item' : ''].join(' ')}
                            onClick={function () { _this.setIsread("unread"); }}>{this.state.json[unreadTxt]}</span>
                        <span className={['item', 'point', ps.isread == 'isread' ? 'selected-search-item' : ''].join(' ')}
                            onClick={function () { _this.setIsread("isread"); }}>{this.state.json[readTxt]}</span>
                    </div>
                    {/* 将提交时间和处理状态放在同一行 */}
                    {pageMsgType != "approve" && pageMsgType != "todo" ? "" :
                        <div style={{ display: "inline-block", marginLeft: "100px" }}>
                            <div>
                                {commitTimeRender()}
                            </div>
                        </div>
                    }

                </div>
                {pageMsgType == "approve" || pageMsgType == "todo" ? "" :
                    <div className="search-line">
                        {commitTimeRender()}
                    </div>
                }
                {pageMsgType != "approve" && pageMsgType != "todo" ? "" :
                    <div id="billTypeArea" className="search-line table">
                        <span className="label layout">{this.state.json['10160501-000020']} : </span>{/* 国际化处理： 单据类型*/}
                        <div className="layout">
                            <div className={`${!this.state.moreBtnShow ? "tag-content" : !this.state.showMoreBilltype ? "tag-content multi" : "tag-content"}`}>
                                {billtypeOptions}
                            </div>
                        </div>
                        <p className="more-right layout">
                            {
                                this.state.moreBtnShow ?
                                    <span className="point" className="more-text" onClick={() => {
                                        this.setState({
                                            showMoreBilltype: !this.state.showMoreBilltype
                                        })

                                    }}>{this.state.json['10160501-000070']} <i className={`icon iconfont ${this.state.showMoreBilltype ? "icon-hangcaozuoxiangshang1" : "icon-icon-hangcaozuoxiala1"}`}></i></span> : ''/* 国际化处理： 更多*/
                            }
                            <span className="point" onClick={() => {
                                this.setState({
                                    showMoreSearch: !this.state.showMoreSearch
                                })
                            }}>{this.state.showMoreSearch ? this.state.json['10160501-000035'] : this.state.json['10160501-000036']}</span>{/* 国际化处理： 收起,高级*/}
                        </p>
                    </div>
                }
                {/* {(pageMsgType != "todo") && !isTest ? "" :
                    <div className="search-line">
                        <span className="label">单据类型 : </span>
                        <div className="tag-content">
                            {msgsourcetypeOptions}
                        </div>
                    </div>
                } */}
                {/* {(pageMsgType != "notice") ? "" :
                    <div className="search-line">
                        <span className="label">通知类型 : </span>
                        <div className="tag-content">
                            {msgsourcetypeOptions}
                        </div>
                    </div>
                } */}
                {(pageMsgType != "approve" && pageMsgType != "todo") && !isTest ? "" :
                    this.state.showMoreSearch ?
                        <div className="search-line">
                            <span className="label">{this.state.json['10160501-000072']} : </span>{/* 国际化处理： 更多条件*/}
                            <div className="form-item-content">
                                <div class="item">
                                    <NCFormControl
                                        className="search-item"
                                        value={ps.billno}
                                        onChange={_this.setBillno}
                                        placeholder={this.state.json['10160501-000037']}/* 国际化处理： 单据编号*/
                                    />
                                </div>
                                <div class="item refer-item">
                                    <UserRefer
                                        value={ps.sender}
                                        onChange={_this.setSender}
                                        queryCondition={{
                                            GridRefActionExt: "nccloud.web.riart.billtype.ref.action.ItfApproveCenterUserRef",
                                            isMutiGroup: false,
                                            isShowGroupAdmin: false,
                                            isShowSysAdmin: false,
                                            isAuthFilter: false,
                                            isAllUserVisible: false,
                                            isShareUserVisible: false,
                                            isSelfVisible: false,
                                            isNeedNCUser: false,
                                            adminoption: 'USE'
                                        }}
                                        isMultiSelectedEnabled={false}
                                        placeholder={this.state.json['10160501-000038']}/* 国际化处理： 发送人*/
                                    />
                                </div>
                                <div class="item">
                                    <NCFormControl
                                        className="search-item"
                                        value={ps.subject}
                                        onChange={_this.setSubject}
                                        placeholder={this.state.json['10160501-000039']}/* 国际化处理： 标题*/
                                    />
                                </div>
                                <NCButton colors="primary" onClick={function () { _this.refreshList() }}>{this.state.json['10160501-000093']}</NCButton>{/* 国际化处理： 查询*/}
                            </div>
                        </div> : ''
                }
            </div>
        );
    }
}



class MessageBox extends Component {
    constructor(props) {
        super(props);
        this.props = props;
        let _this = this;
        let binds = "cancelAssginModal markRead markUnRead handleSelect hideCurrentItem markIsread markIsunread processCancel approveCancel approvePass approveRefuse approveReject " +
            "openResumeModal closeResumeModal openApproveModal closeApproveModal closeApproveDetailModal detailDoApprove detailCancelApprove openApproveDetailModal " +
            "openDetailPage openWorkflowDetailPage openApproveDetailPage showRejectData openNoPassModal openRejectModal suggestChange defaultSgtChange" +
            " openAddApproveModal openTransferModal setAddApprover setTransferApprover workflowPassOrAssgin approvePassOrAssgin"
        binds.split(" ").forEach(function (item) {
            if (_this[item]) { } else {
                console.log(item)
            }
            _this[item] = _this[item].bind(_this);
        });
        this.state = {
            json: this.props.json,
            inlt: this.props.inlt,
            showApproveModal: false,
            showApproveDetailModal: false,
            sadm: false,
            approveModalDropup: true,
            approveDetailModalUrl: "",
            showResumeModal: false,
            selectOne: props.selectOne,
            approveType: "approve",
            // defaultSgt: this.props.json['10160501-000040'],/* 国际化处理： 同意*/
            defaultSgt: "",
            suggestion: "",
            message: props.message,
            skipCodes: {},
            resumeType: null,
            resumeDetail: {},
            resumeCode: "",
            confirmOrCancle: null,
            addApproverUsers: [],
            transferApproverUser: null,
            isAssgin: false,
            oprType: "1",
            assginTreeValue: {

            },
            approveData: [

            ],
            wf_task_val: "",
            Children: [],
            noticeShowMore: false,//通知中心查看更多flag
            approveShowMore: false, //审批中心查看更多flag
            assgindisplay: false,
            assgindata: null

        }
    }

    componentWillReceiveProps(nextProps) {
        if (JSON.stringify(nextProps.json) != "{}") {
            this.setState({ json: nextProps.json, inlt: nextProps.inlt });
        }
    }

    dealAssginUsedr = (value) => {

        this.state.assgindata = value;

        this.approvePass();
        this.setState({
            assgindisplay: false,
            assgindata: null
        })

    }


    handleSelect(e) {
        this.state.selectOne(this.props.message.id);
    }

    processCancel(e) {
        console.log(this.state.json['10160501-000041'] + this.props.message.id);/* 国际化处理： 发送取消处理请求*/
        console.log(e);
        let _this = this;
        let _state = _this.state;
        let msg = _this.state.message;
        _this.setState({ approveType: "unApprove" });
        let _data = { "billtype": msg.billtype, "billid": msg.billid, "pk_checkflow": msg.pk_detail, "workflowtype": msg.workflowtype, "skipCodes": _this.state.skipCodes["unApprove"] || [] };
        if (_this.state.confirmOrCancle) {
            _data["confirmOrCancleAction"] = _this.state.confirmOrCancle;
        }
        ajax({
            url: '/nccloud/workflow/approvalcenter/uNWorkflowSingalPassAction.do',
            data: _data,
            method: 'POST',
            success: (res) => {
                if (res.data && (res.data == "200" || res.data.code == "200")) {
                    toast({ color: 'success' });
                    _this.setState({ skipCodes: {}, approveType: "approve" });
                    _this.markIsread();//为了刷新未读数量。
                    _this.hideCurrentItem();//隐藏当前操作条目
                }
                else if (res.data.code && res.data.code == "321") {
                    // _this.openWorkflowDetailPage();
                    _this.props.openTo(res.data.detail.url, { appcode: res.data.detail.appcode, pagecode: res.data.detail.pagecode, c: res.data.detail.appcode, p: res.data.detail.pagecode, status: "browse" });
                }
                else if (res.data.code && res.data.code.length == 3 && res.data.code.slice(0, 1) == "3") {
                    _state.skipCodes["unApprove"] = res.data.skipCodes;
                    _state.resumeDetail = res.data.detail;
                    _state.resumeCode = res.data.code;
                    _state.approveType = "unApprove";
                    _this.setState(_state);
                    _this.openResumeModal();
                }
                else {
                    toast({ color: 'danger', content: res.data.error });
                }
            }
        });
    }

    approveCancel(e) {
        // if (!confirm("已经审批完成,确定收回?")) { return; }
        console.log(this.state.json['10160501-000004'] + this.props.message.id);/* 国际化处理： 发送审批取消请求*/
        console.log(e);
        let _this = this;
        let _state = _this.state;
        let msg = _this.state.message;
        _this.setState({ approveType: "unApprove" });
        if (parseInt(msg.workflowtype) == 6) {
            _this.processCancel(e);
        } else {
            let _data = { "billtype": msg.billtype, "billid": msg.billid, "pk_checkflow": msg.pk_detail, "skipCodes": _this.state.skipCodes["unApprove"] || [] };
            if (_this.state.confirmOrCancle) {
                _data["confirmOrCancleAction"] = _this.state.confirmOrCancle;
            }
            ajax({
                url: '/nccloud/workflow/approvalcenter/unApproveAction.do',
                data: _data,
                method: 'POST',
                success: (res) => {
                    if (res.data && (res.data == "200" || res.data.code == "200")) {
                        toast({ color: 'success' });
                        _this.setState({ skipCodes: {}, approveType: "approve" });
                        _this.markIsread();//为了刷新未读数量。
                        _this.hideCurrentItem();//隐藏当前操作条目
                    }
                    else if (res.data.code && res.data.code == "321") {
                        // _this.openWorkflowDetailPage();
                        _this.props.openTo(res.data.detail.url, { appcode: res.data.detail.appcode, pagecode: res.data.detail.pagecode, c: res.data.detail.appcode, p: res.data.detail.pagecode, status: "browse" });
                    }
                    else if (res.data.code && res.data.code.length == 3 && res.data.code.slice(0, 1) == "3") {
                        _state.skipCodes["unApprove"] = res.data.skipCodes;
                        _state.resumeDetail = res.data.detail;
                        _state.resumeCode = res.data.code;
                        _state.approveType = "unApprove";
                        _this.setState(_state);
                        _this.openResumeModal();
                    }
                    else {
                        toast({ color: 'danger', content: res.data.error });
                    }
                }
            });
        }
    }

    markRead() {
        let _this = this;
        ajax({
            url: '/nccloud/riart/message/setRead.do',
            data: { "pk_message": _this.state.message.id, msgType: pageMsgType, isread: "Y" },
            method: 'POST',
            success: (res) => {
                //let data = res.data;
                _this.markIsread();
            }
        });
    }

    markUnRead() {
        let _this = this;
        ajax({
            url: '/nccloud/riart/message/setRead.do',
            data: { "pk_message": _this.state.message.id, msgType: pageMsgType, isread: "N" },
            method: 'POST',
            success: (res) => {
                //let data = res.data;
                _this.markIsunread();
            }
        });
    }

    workflowPassOrAssgin() {
        this.approvePassOrAssgin();
    }

    approvePassOrAssgin() {
        let _this = this;
        let msg = _this.state.message;
        debugger;
        if (msg.workflowtype == "4" || msg.workflowtype == "5") {
            ajax({
                url: '/nccloud/workflow/approvalcenter/workflowSingalEnableQueryAction.do',
                data: { "billtype": msg.billtype, "billid": msg.billid, "pk_checkflow": msg.pk_detail },
                method: 'POST',
                success: (res) => {
                    debugger;
                    if (res.data && res.data.content) {
                        //    toast({ color: 'success', content: '获取指派人员成功' });
                        _this.state.isAssgin = true;

                        _this.setState({
                            assgindata: res.data,
                            assgindisplay: true,
                        })
                        // _this.setState({
                        //     assginTreeValue : {
                        //         leftTreeData : res.data.leftdata,
                        //         rightTreeData : []
                        //     },
                        //     approveType: "assgin",
                        //     showApproveModal: true
                        // });
                    } else if (res.data && (res.data == "500" || res.data.code == "500")) {
                        toast({ color: 'danger', content: res.data.error });
                    } else {
                        _this.approvePass()
                    }
                }
            });
        } else {
            ajax({
                url: '/nccloud/workflow/approvalcenter/queryApproveAssginInfo.do',
                data: { "billtype": msg.billtype, "billid": msg.billid, "pk_checkflow": msg.pk_detail, "workflow_type": msg.workflowtype, },
                method: 'POST',
                success: (res) => {
                    debugger;
                    if (res.data.code != "411" && res.data) {
                        //    toast({ color: 'success', content: '获取指派人员成功' });
                        _this.state.isAssgin = true;

                        _this.setState({
                            assgindata: res.data,
                            assgindisplay: true,
                        })
                        // _this.setState({
                        //     assginTreeValue : {
                        //         leftTreeData : res.data.leftdata,
                        //         rightTreeData : []
                        //     },
                        //     approveType: "assgin",
                        //     showApproveModal: true
                        // });
                    } else if (res.data && (res.data == "500" || res.data.code == "500")) {
                        toast({ color: 'danger', content: res.data.error });
                    } else {
                        _this.approvePass()
                    }
                }
            });
        }

    }


    approvePass() {

        console.log(this.state.json['10160501-000005'] + this.props.message.id);/* 国际化处理： 发送审批通过请求*/
        let _this = this;
        let msg = _this.state.message;
        //this.markIsread();
        let _data = { "billtype": msg.billtype, "billid": msg.billid, "pk_checkflow": msg.pk_detail, "skipCodes": _this.state.skipCodes["approve"] || [], "assgininfo": this.state.assgindata, "isAssgin": this.state.isAssgin };
        if (_this.state.confirmOrCancle) {
            _data["confirmOrCancleAction"] = _this.state.confirmOrCancle;
        }
        if (this.state.showApproveDetailModal) {
            _data["checknote"] = this.state.suggestion;
            _data["check_note"] = this.state.suggestion;
        }
        else {
            _data["checknote"] = this.state.defaultSgt;
            _data["check_note"] = this.state.defaultSgt;
        }

        ajax({
            url: '/nccloud/workflow/approvalcenter/approvePassAction.do',
            data: _data,
            method: 'POST',
            success: (res) => {
                let _state = _this.state;
                if (res.data && (res.data == "200" || res.data.code == "200")) {
                    _state.skipCodes = {};
                    _state.resumeDetail = {};
                    _state.resumeCode = "";
                    _this.setState(_state);
                    toast({ color: 'success', content: _this.state.json['10160501-000094'] });/* 国际化处理： 审批完成*/
                    _this.closeApproveDetailModal();
                    _this.closeApproveModal();
                    _this.closeResumeModal();
                    // _this.markRead();
                    _this.markIsread();
                    _this.hideCurrentItem();//隐藏当前操作条目
                }
                else if (res.data.code && res.data.code == "321") {
                    // _this.openWorkflowDetailPage();
                    _this.props.openTo(res.data.detail.url, { appcode: res.data.detail.appcode, pagecode: res.data.detail.pagecode, c: res.data.detail.appcode, p: res.data.detail.pagecode, status: "browse" });
                }
                else if (res.data.code && res.data.code.length == 3 && res.data.code.slice(0, 1) == "3") {
                    _state.skipCodes["approve"] = res.data.skipCodes;
                    _state.resumeDetail = res.data.detail;
                    _state.resumeCode = res.data.code;
                    _this.setState(_state);
                    _this.openResumeModal();
                }
                else {
                    toast({ color: 'danger', content: res.data.error });
                }
            }
        });
    }

    approveRefuse() {
        console.log(this.state.json['10160501-000006'] + this.props.message.id);/* 国际化处理： 发送审批拒绝请求*/
        debugger;
        let _this = this;
        let msg = _this.state.message;
        //this.markIsread();
        let _data = { "billtype": msg.billtype, "billid": msg.billid, "pk_checkflow": msg.pk_detail, "skipCodes": _this.state.skipCodes["Noapprove"] || [] };
        //if (this.state.showApproveDetailModal) {
        _data["checknote"] = this.state.suggestion;
        _data["check_note"] = this.state.suggestion;
        //}

        if(_this.state.isAssgin){
            let assginUser = [];
            if(_this.state.assginTreeValue.rightTreeData && _this.state.assginTreeValue.rightTreeData.length>0){
                for(let i = 0;i<_this.state.assginTreeValue.rightTreeData.length;i++){
                    assginUser.push(_this.state.assginTreeValue.rightTreeData[i].refpk)
                }
            }else{
                toast({ color: 'warning', content: _this.state.json['10160501-000097'] });/* 国际化处理： 请选择指派人员！*/
                return;
            }
            _data["isAssgin"] = _this.state.isAssgin;
            _data["assginUsers"] = assginUser;
        }

        //return null;
        ajax({
            url: '/nccloud/workflow/approvalcenter/approveNoPassAction.do',
            data: _data,
            method: 'POST',
            success: (res) => {
                if (res.data && (res.data == "200" || res.data.code == "200")) {
                    toast({ color: 'success' });
                    _this.closeApproveDetailModal();
                    _this.closeApproveModal();
                    _this.markIsread();
                    _this.hideCurrentItem();//隐藏当前操作条目
                }
                else if (res.data && (res.data == "355" || res.data.code == "355")){
                    _this.setState({"isAssgin": true});
                    _this.openApproveModal();
                    toast({ color: 'danger', content: res.data.error });
                }
                else {
                    toast({ color: 'danger', content: res.data.error });
                    _this.closeApproveModal();
                }
            }
        });
    }

    approveReject() {
        console.log(this.state.json['10160501-000007'] + this.props.message.id);/* 国际化处理： 发送审批驳回请求*/
        let _this = this;
        let msg = _this.state.message;
        //this.markIsread();
        let _data = { "billtype": msg.billtype, "billid": msg.billid, "pk_checkflow": msg.pk_detail, "pk_wf_task": this.state.wf_task_val, "skipCodes": _this.state.skipCodes["reject"] || [] };
        //if (this.state.showApproveDetailModal) {
        _data["checknote"] = this.state.suggestion;
        _data["check_note"] = this.state.suggestion;
        //}

        ajax({
            url: '/nccloud/workflow/approvalcenter/approveRejectAction.do',
            data: _data,
            method: 'POST',
            success: (res) => {
                if (res.data && (res.data == "200" || res.data.code == "200")) {
                    toast({ color: 'success', content: this.state.json['10160501-000042'] });/* 国际化处理： 驳回成功*/
                    _this.closeApproveDetailModal();
                    _this.closeApproveModal();
                    _this.markIsread();
                    _this.hideCurrentItem();//隐藏当前操作条目
                }
                else {
                    toast({ color: 'danger', content: res.data.error });
                }
            }
        });
    }

    hideCurrentItem() {
        this.props.hideCurrentItem(this.props.idx);
    }

    markIsread(e) {
        this.props.markIsread(this.props.message.id);
        let msg = this.state.message;
        msg.isread = "Y";
        this.setState({ message: msg });
    }

    markIsunread(e) {
        this.props.markIsread(this.props.message.id, true);
        let msg = this.state.message;
        msg.isread = "N";
        this.setState({ message: msg });
    }

    openResumeModal() {
        console.log("openResumeModal");
        this.setState({
            showResumeModal: true
        });
    }

    closeResumeModal() {
        console.log("closeResumeModal");
        this.setState({
            approveType: "approve",
            skipCodes: {},
            showResumeModal: false
        });
    }

    closeApproveModal() {
        console.log("closeApproveModal");
        this.setState({
            approveType: "approve",
            // isAssgin: false,
            showApproveModal: false
        });
    }

    openApproveModal() {
        console.log("openApproveModal");
        this.setState({
            showApproveModal: true
        });
    }

    closeApproveDetailModal() {
        console.log("closeApproveDetailModal");
        this.setState({
            showApproveDetailModal: false
        });
    }

    detailDoApprove() {
        console.log("detailDoApprove");
        //return null;
        if (this.state.resumeCode == "315" || this.state.resumeCode == "314") {
            this.setState({
                confirmOrCancle: 1
            })
        }
        if (this.state.approveType == "approve") {
            //
            this.approvePass();
        }
        else if (this.state.approveType == "Noapprove") {
            //
            this.approveRefuse();
        }
        else if (this.state.approveType == "reject") {
            //
            this.approveReject();
        }
        else if (this.state.approveType == "addapprove") {
            this.addapprove();
        }
        else if (this.state.approveType == "transfer") {
            this.transferApprove()
        }
        else if (this.state.approveType == "unApprove") {
            this.approveCancel()
        }

        //改了指派组件之后这块不会被用到
        // else if (this.state.approveType == "assgin") {
        //     //
        //     this.approveAssgin();
        // }



    }

    detailCancelApprove() {
        console.log("detailCancelApprove");
        if (this.state.resumeCode == "315" || this.state.resumeCode == "314") {
            this.setState({
                confirmOrCancle: 2
            })
        }
        if (this.state.approveType == "unApprove") {
            this.approveCancel()
        }
        else {this.approvePass();}

    }


    // approveAssgin(){

    //     let _this = this;
    //     console.log("发送审批通过请求" + this.props.message.id);
    //     let msg = _this.state.message;
    //     //this.markIsread();
    //     let assginUser = [];
    //     if(_this.state.assginTreeValue.rightTreeData && _this.state.assginTreeValue.rightTreeData.length>0){
    //         for(let i = 0;i<_this.state.assginTreeValue.rightTreeData.length;i++){


    //             assginUser.push(_this.state.assginTreeValue.rightTreeData[i].refpk)
    //         }
    //     }else{
    //         toast({ color: 'warning', content: '请选择指派人员！' });
    //         return;
    //     }
    //     let _data = { "billtype": msg.billtype, "billid": msg.billid, "pk_checkflow": msg.pk_detail, "skipCodes": _this.state.skipCodes["approve"] || [],"isAssgin":_this.state.isAssgin,"assginUsers":assginUser};

    //     if (this.state.showApproveDetailModal) {
    //         _data["checknote"] = this.state.suggestion;
    //         _data["check_note"] = this.state.suggestion;
    //     }
    //     else {
    //         _data["checknote"] = this.state.defaultSgt;
    //         _data["check_note"] = this.state.defaultSgt;
    //     }

    //     ajax({
    //         url: '/nccloud/workflow/approvalcenter/approvePassAction.do',
    //         data: _data,
    //         method: 'POST',
    //         success: (res) => {
    //             if (res.data && (res.data == "200" || res.data.code == "200")) {
    //                 toast({ color: 'success' });
    //                 _this.closeApproveDetailModal();
    //                 _this.closeApproveModal();
    //                 _this.closeResumeModal();
    //                 _this.markRead();
    //             }
    //             else if (res.data.code && res.data.code.length == 3 && res.data.code.slice(0, 1) == "3") {
    //                 let _state = _this.state;
    //                 _state.skipCodes["approve"] = res.data.skipCodes;
    //                 _state.resumeDetail = res.data.detail;
    //                 _state.resumeCode = res.data.code;
    //                 _this.setState(_state);
    //                 _this.openResumeModal();
    //             }
    //             else {
    //                 toast({ color: 'danger', content: res.data.error});
    //             }
    //         }
    //     });

    // }


    transferApprove() {
        debugger;
        let _this = this;
        let value = _this.state.transferApproverUser;
        let addTransferUsers = [];

        if (value) {
            let cdata = value.refpk;
            addTransferUsers.push(cdata)

        }


        if (addTransferUsers == null || addTransferUsers.length == 0) {
            toast({ color: 'warning', content: this.state.json['10160501-000043'] });/* 国际化处理： 请选择改派用户*/
            return;
        }

        // if (value) {
        //     toast({ color: 'danger', content: '改派人员不能为空'});
        //     return;
        // } else {
        //     addTransferUsers.push(value.refpk);

        // }


        //*
        ajax({
            url: '/nccloud/workflow/approvalcenter/approveTransfer.do',
            data: { "pk_checkflow": _this.state.message.pk_detail, "pk_message": _this.state.message.pk_message, "billtype": _this.state.message.billtype, "billid": _this.state.message.billid, "trans_user": addTransferUsers, "checknote": _this.state.suggestion, "check_note": _this.state.suggestion },
            method: 'POST',
            success: (res) => {
                if (res.data) {
                    if (res.data && (res.data == "200" || res.data.code == "200")) {
                        toast({ color: 'success' });
                        _this.closeApproveDetailModal();
                        _this.closeApproveModal();
                        _this.markRead();
                    }
                }
            }
        });

    }

    addapprove() {
        let _this = this;
        let value = _this.state.addApproverUsers;
        let addApproveUsers = [];
        debugger;
        if (value && value.length > 0) {
            for (let i = 0; i < value.length; i++) {
                let cdata = value[i].refpk;
                addApproveUsers.push(cdata)
            }
        }

        if (!addApproveUsers || addApproveUsers.length == 0) {
            toast({ color: 'warning', content: this.state.json['10160501-000044'] });/* 国际化处理： 请选择加签用户*/
            return;
        }

        //*
        ajax({
            url: '/nccloud/workflow/approvalcenter/addApprove.do',
            data: { "pk_checkflow": _this.state.message.pk_detail, "pk_message": _this.state.message.pk_message, "billtype": _this.state.message.billtype, "billid": _this.state.message.billid, "approve_users": addApproveUsers, "checknote": _this.state.suggestion, "check_note": _this.state.suggestion },
            method: 'POST',
            success: (res) => {
                if (res.data) {
                    if (res.data && (res.data == "200" || res.data.code == "200")) {
                        toast({ color: 'success' });
                        _this.closeApproveDetailModal();
                        _this.closeApproveModal();
                    }
                }
            }
        });
    }


    openDetailPage() {
        let _this = this;
        let _appcode = _this.state.message.funccode == null ? "" : _this.state.message.funccode.replace(/(^\s*)|(\s*$)/g, "");
        let _pagecode = "";
        if (_this.state.message.params != null && _this.state.message.params.indexOf("pagecode=") >= 0) {
            try {
                _pagecode = _this.state.message.params.split("pagecode=")[1].split("&")[0];
            }
            catch (e) { }
        }
        if (pageMsgType == "todo" || pageMsgType == "notice") {
            let url = "/nccloud/riart/message/getTodoUrl.do";
            if (pageMsgType == "notice") url = "/nccloud/riart/message/getNoticeUrl.do";
            ajax({
                url: url,
                async: false,
                data: { "pk_message": _this.state.message.pk_message, "billid": _this.state.message.billid, "billtype": _this.state.message.billtype, "appcode": _this.state.message.funccode == null ? "" : _this.state.message.funccode.replace(/(^\s*)|(\s*$)/g, ""), "params": _this.state.message.params },
                method: 'POST',
                success: (res) => {
                    if (res.data) {
                        let url = res.data;
                        if (url == "no permission") {
                            toast({ color: 'warning', content: this.state.json['10160501-000008'] });/* 国际化处理： 没有审批应用权限或配置错误，请联系管理员。*/
                            return;
                        }
                        let finalUrl = url;
                        let msg = _this.state.message;
                        // if(pageMsgType == "notice"){
                        if (url.indexOf("?") == -1) {
                            finalUrl = url + "?scene=" + pageMsgType;
                        }
                        else if (url.indexOf("#") >= 0) {
                            finalUrl = url + (url.split("#")[1].length > 0 ? "&" : "") + "scene=" + pageMsgType;
                        }
                        else {
                            finalUrl = url + "#scene=" + pageMsgType;
                        }
                        // }
                        if (res.data) {
                            //2018-09-10 老梁需求确认：新窗口打开
                            _this.props.openTo(finalUrl, { appcode: _appcode, pagecode: _pagecode, c: _appcode, p: _pagecode });
                            // _this.props.openTo(res.data, {appcode: "10160502", pagecode: "10160502TODO"});
                        }
                    }
                }
            });
        }
    }

    openWorkflowDetailPage() {
        let _this = this;
        let _appcode = _this.state.message.funccode == null ? "" : _this.state.message.funccode.replace(/(^\s*)|(\s*$)/g, "");
        let _pagecode = "";
        if (_this.state.message.params != null && _this.state.message.params.indexOf("pagecode=") >= 0) {
            try {
                _pagecode = _this.state.message.params.split("pagecode=")[1].split("&")[0];
            }
            catch (e) { }
        }
        let msg = this.state.message;
        ajax({
            url: '/nccloud/riart/message/getApproveUrl.do',
            data: { "pk_message": msg.pk_message, "billtype": msg.billtype, "billid": msg.billid, "appcode": msg.funccode == null ? "" : msg.funccode.replace(/(^\s*)|(\s*$)/g, "") },
            method: 'POST',
            success: (res) => {
                let finalUrl = "/";
                debugger
                let url = (res.data && res.data.pageurl) || "no permission";
                if (url == "no permission") {
                    toast({ color: 'warning', content: this.state.json['10160501-000008'] });/* 国际化处理： 没有审批应用权限或配置错误，请联系管理员。*/
                    return;
                }
                if (url == "message not found") {
                    toast({ color: 'warning', content: this.state.json['10160501-000009'] });/* 国际化处理： 没有找到该消息。*/
                    _this.hideCurrentItem();//隐藏当前操作条目
                    return;
                }
                if (url) {
                    // if(url.indexOf("#")>=0){
                    //     let us = url.split("#");
                    //     us[0] = us[0] + (url.indexOf("?")>=0 ? "&" : "?" ) + "status=browse&id=" + msg.billid;
                    //     finalUrl = us.join("#");
                    // }
                    // else {
                    //     finalUrl = url + (url.indexOf("?")>=0 ? "&" : "?" ) + "status=browse&id=" + msg.billid;
                    // }
                    if (url.indexOf("?") == -1) {
                        finalUrl = url + "?scene=approvesce&status=browse&id=" + msg.billid;
                    }
                    else if (url.indexOf("#") >= 0) {
                        finalUrl = url + (url.split("#")[1].length > 0 ? "&" : "") + "scene=approvesce&status=browse&id=" + msg.billid;
                    }
                    else {
                        finalUrl = url + "#scene=approvesce&status=browse&id=" + msg.billid;
                    }
                }
                // _this.props.openTo(finalUrl, {appcode: "10160501", pagecode: "10160501APPROVE", c: _appcode, p: _pagecode});
                _appcode = res.data.appcode || _appcode;
                _pagecode = res.data.pagecode || _pagecode;
                // _this.props.openTo(finalUrl, { appcode: _appcode, pagecode: _pagecode, c: _appcode, p: _pagecode, checknote: this.state.defaultSgt });
                // 2019-01-27 敬礼改动，为了统计打开节点的日志。
                _this.props.openTo(finalUrl, { appcode: "10160501", pagecode: "10160501APPROVE", c: _appcode, p: _pagecode, checknote: this.state.defaultSgt });
            }
        });

    }

    openApproveDetailPage() {
        let _this = this;
        let msg = _this.state.message;
        let _appcode = _this.state.message.funccode == null ? "" : _this.state.message.funccode.replace(/(^\s*)|(\s*$)/g, "");
        let _pagecode = "";
        if (_this.state.message.params != null && _this.state.message.params.indexOf("pagecode=") >= 0) {
            try {
                _pagecode = _this.state.message.params.split("pagecode=")[1].split("&")[0];
            }
            catch (e) { }
        }
        // if(_appcode == null || _appcode == "" || _pagecode == null || _pagecode == ""){
        ajax({
            url: '/nccloud/riart/message/getApproveUrl.do',
            data: { "pk_message": msg.pk_message, "billtype": msg.billtype, "billid": msg.billid, "appcode": (msg.funccode || "").replace(/(^[\s\n\t]+|[\s\n\t]+$)/g, "") },
            method: 'POST',
            success: (res) => {
                console.log(res.data);
                let url = (res.data && res.data.pageurl) || "no permission";
                if (url == "no permission") {
                    toast({ color: 'warning', content: this.state.json['10160501-000008'] });/* 国际化处理： 没有审批应用权限或配置错误，请联系管理员。*/
                    return;
                }
                if (url == "message not found") {
                    toast({ color: 'warning', content: this.state.json['10160501-000009'] });/* 国际化处理： 没有找到该消息。*/
                    return;
                }
                _appcode = res.data.appcode || _appcode;
                _pagecode = res.data.pagecode || _pagecode;
                // _this.props.openTo(finalUrl, {appcode: "10160501", pagecode: "10160501APPROVE"});
                this.props.linkTo("/uap/msgcenter/message/approveDetail/index.html?pk_message=" + this.state.message.pk_message + "&pageMsgType=" + pageMsgType, { appcode: "10160501", pagecode: "10160501APPROVE", c: _appcode, p: _pagecode, checknote: this.state.defaultSgt });
            }
        });
        return;
        // }
        // console.log('defaultSgt', this.state.defaultSgt)
        // this.props.linkTo("/uap/msgcenter/message/approveDetail/index.html?pk_message=" + this.state.message.pk_message+"&pageMsgType="+pageMsgType, {appcode: "10160501", pagecode: "10160501APPROVE",c:_appcode,p:_pagecode, checknote:this.state.defaultSgt});
        // this.props.openTo("/uap/msgcenter/message/approveDetail/index.html?pk_message=" + this.state.message.pk_message+"&pageMsgType="+pageMsgType, {appcode: "10160501", pagecode: "10160501APPROVE", approveCenterBillCode: {appcode:'', pagecode: ''}});
    }

    openApproveDetailModal() {
        console.log("openApproveDetailModal");
        this.setState({
            showApproveDetailModal: true
        });
        let _this = this;
        //this.createApprove();
        if (this.state.approveDetailModalUrl != "") {
            return;
        }

        //*
        ajax({
            url: '/nccloud/riart/message/getApproveUrl.do',
            data: { "pk_message": _this.state.message.pk_message, "billtype": _this.state.message.billtype, "billid": _this.state.message.billid, "appcode": _this.state.message.funccode == null ? "" : _this.state.message.funccode.replace(/(^\s*)|(\s*$)/g, "") },
            method: 'POST',
            success: (res) => {
                let url = (res.data && res.data.pageurl) || "no permission";
                if (url == "no permission") {
                    toast({ color: 'warning', content: this.state.json['10160501-000008'] });/* 国际化处理： 没有审批应用权限或配置错误，请联系管理员。*/
                    return;
                }
                if (url == "message not found") {
                    toast({ color: 'warning', content: this.state.json['10160501-000009'] });/* 国际化处理： 没有找到该消息。*/
                    _this.hideCurrentItem();//隐藏当前操作条目
                    return;
                }
                if (res.data) {
                    this.setState({
                        approveDetailModalUrl: res.data
                    });
                }
            }
        });
    }


    //审批按钮操作事件
    approveRadio = (val) => {
        this.setState({
            approveType: val
        });
    };
    //输入意见输入框
    suggestChange(val) {
        this.setState({
            suggestion: val
        });
    }

    //输入意见输入框
    defaultSgtChange(val) {
        this.setState({
            defaultSgt: val
        });
    }

    // createApprove = () => {
    //     //审批流程
    //     let _this = this;
    //     let { approveDetail } = this.props;

    //     let suggestion = "";
    //     let billID = this.state.message.billid;

    //     let _state = this.state;
    //     if (_state.approveData.length == 0) {
    //         //todo ajax
    //     }

    //     return approveDetail.create("approve_" + this.state.message.id, {
    //         data: _this.state.approveData,
    //         approveType: _this.state.approveType,
    //         suggestion: _this.state.suggestion,
    //         approveList: approveActions,
    //         needInput: true,
    //         approveRadio: this.approveRadio.bind(this),
    //         suggestChange: this.suggestChange.bind(this),
    //         billID
    //     });
    // };

    componentDidMount() {
        // let _this = this;
        // setTimeout(function () {
        //     let formData = convertFormData(_this.props.message, "msgBoxTemplate");
        //     _this.props.form.setAllFormValue(formData);
        // }, 5000);
    }


    openRejectModal() {
        console.log("openRejectModal");
        this.setState({
            approveType: "reject",
            showApproveModal: true
        });
    }

    openNoPassModal() {
        console.log("openNoPassModal");
        this.setState({
            approveType: "Noapprove",
            showApproveModal: true
        });
    }

    openAddApproveModal() {
        console.log("openAddApproveModal");
        this.setState({
            approveType: "addapprove",
            showApproveModal: true
        });
    }


    openTransferModal() {
        console.log("openTransferModal");
        this.setState({
            approveType: "transfer",
            showApproveModal: true
        });
    }


    setAddApprover = value => {
        debugger;
        let _this = this;
        let _state = _this.state;
        _state.addApproverUsers = value;

        //alert( _state.addApproverUsers);
        _this.setState(_state);
        // _this.refreshList();
    };



    setTransferApprover = value => {
        let _this = this;
        let _state = _this.state;
        _state.transferApproverUser = value;

        //alert( _state.addApproverUsers);
        _this.setState(_state);
        // _this.refreshList();
    };


    showRejectData() {

        let _this = this;
        if (this.state.approveType != "reject") return;
        let msg = _this.state.message;
        if (_this.state.Children.length > 0) return;
        //请求数据
        debugger;
        ajax({
            url: '/nccloud/workflow/approvalcenter/worklownoteRejectQueryAction.do',
            data: { "billtype": msg.billtype, "billid": msg.billid, "workflow_type": msg.workflowtype },
            method: 'POST',
            success: (res) => {
                let children = [];
                if (res.data != null && res.data.rows.rows && res.data.rows.rows.length > 0) {
                    for (let i = 0; i < res.data.rows.rows.length; i++) {
                        let cdata = res.data.rows.rows[i].values;
                        children.push(<NCOption key={cdata.pk_wf_task.value}>{cdata.checkname.value}审批</NCOption>);/* 国际化处理： 审批*/
                    }
                }

                this.setState({
                    Children: children
                });
            }
        });
    }


    handleRejectChange = (e) => {
        this.setState({ wf_task_val: e });
    }

    cancelAssginModal() {
        this.setState({
            assgindisplay: false
        })
    }



    downloadAtta(pk_doc, filename) {
        // ajax({
        //     url: '/nccloud/riart/message/download.do',
        //     data: { "pk_doc": pk_doc, "filename": filename, "name": filename },
        //     method: 'POST',
        //     success: (res) => {
        //     }
        // });
        let data = { "pk_doc": pk_doc, "filename": filename, "name": filename };
        // let form = new formData();
        let $form = document.createElement('form');
        $form.target = "";
        $form.method = "POST";
        $form.action = `${location.origin}/nccloud/riart/message/download.do`;
        $form.type = 'hidden';
        for (let key in data) {
            let input = document.createElement('input');
            input.type = 'hidden';
            input.name = key;
            input.value = data[key];
            $form.appendChild(input);
        }
        document.body.appendChild($form);
        $form.submit();
        document.body.removeChild($form);
    }

    render() {
        let _this = this;
        const { form, modal } = this.props;
        let { createModal } = modal;
        let content = { __html: this.state.message.content };
        let resumeHtml = { __html: this.state.resumeDetail ? this.state.resumeDetail.html : "" };
        //  this.state.assginTreeValue.rightTreeData = [];

        // if (this.state.message.isread == "Y") {
        //     console.log(this.state.message);
        // }
        let _detail = {};
        let enableActions = [];
        let hintMessage;
        let hintContent;
        let moreMenu = "";
        let rejectHistory = {"isReject":false, "rejectReason":null};
        let submitRejectBillMode = null;
        try {
            _detail = JSON.parse(this.props.message.detail);
            if (_detail && _detail["rejectHistory"]) {
                rejectHistory = _detail && _detail["rejectHistory"];
            }
        } catch (e) {}
        if (pageMsgType == "approve") {
            try {
                _detail = JSON.parse(this.props.message.detail);
                if (_detail) {
                    enableActions = _detail && _detail["enableActions"];
                    hintMessage = _detail && _detail.clientinfo && _detail.clientinfo.hintMessage;
                    hintContent = <div style={{ maxWidth: '380px' }}>{hintMessage}</div>;
                }
            } catch (e) {}
            try {
                _detail = JSON.parse(this.props.message.detail);
                if (_detail) {
                    submitRejectBillMode = _detail && _detail["SubmitRejectBillMode"];
                }
            } catch (e) {}
            /* 国际化处理： 不批准*/
            /* 国际化处理： 加签*/
            /* 国际化处理： 改派*/
            moreMenu = (<NCMenu className="more-dropdown">
                {(enableActions || []).filter(function (item, idx, origin) { return item == "nopass"; }).length > 0 ?
                    <Item key="nopass"><span onClick={this.openNoPassModal}>{this.state.json['10160501-000001']}</span></Item> : ""}
                {(enableActions || []).filter(function (item, idx, origin) { return item == "addapprover"; }).length > 0 ?
                    <Item key="addapprover"><span onClick={this.openAddApproveModal}>{this.state.json['10160501-000046']}</span></Item> : ""}
                {(enableActions || []).filter(function (item, idx, origin) { return item == "transfer"; }).length > 0 ?
                    <Item key="transfer"><span onClick={this.openTransferModal}>{this.state.json['10160501-000048']}</span></Item> : ""}
            </NCMenu>);
        }

        let modalTitle = "";
        let modalBody = "";
        // if (this.state.approveType == "assgin") {
        //     modalTitle = "选择指派";
        //     modalBody =
        //         (<NCModal.Body >
        //             <div className="body-content">
        //                 <div className="content-inner">
        //                     <div className="label" style={{ width: 90 }}>选择指派人员</div>
        //                     <div className="content" style={{ width: 200 }}>

        //                     {/* <CompositeTransfer data={data}
        //                             display = {false}
        //                             getResult = {this.getAssginUsedr}
        //                          /> */}
        //                         <Transfer 
        //                             TransferId={'org_transferid'} 
        //                             leftTreeData={this.state.assginTreeValue.leftTreeData} 
        //                             rightTreeData={this.state.assginTreeValue.rightTreeData} 
        //                             value={this.state.assginTreeValue.rightTreeData} 
        //                             oprType={this.state.oprType} 
        //                         /> 
        //                     </div>
        //                 </div>
        //                 <div className="content-inner">
        //                     <div className="label">输入批语</div>
        //                     <div className="content">
        //                         <NCFormControl
        //                             className="demo-input"
        //                             value={this.state.suggestion}
        //                             onChange={this.suggestChange}
        //                         />
        //                     </div>
        //                 </div>
        //             </div>
        //         </NCModal.Body>);
        // }
        // else



        if (this.state.approveType == "Noapprove" && this.state.isAssgin) {
            modalTitle = this.state.json['10160501-000095'];/* 国际化处理： 选择指派*/
            modalBody =
                (<NCModal.Body >
                    <div className="body-content">
                        <div className="content-inner">
                            <div className="label" style={{ width: 90 }}>{this.state.json['10160501-000096']}</div>/* 国际化处理： 选择指派人员*/
                            <div className="content" style={{ width: 200 }}>

                            {/* <CompositeTransfer data={data}
                                    display = {false}
                                    getResult = {this.getAssginUsedr}
                                 /> */}
                                <Transfer 
                                    TransferId={'org_transferid'} 
                                    leftTreeData={this.state.assginTreeValue.leftTreeData} 
                                    rightTreeData={this.state.assginTreeValue.rightTreeData} 
                                    value={this.state.assginTreeValue.rightTreeData} 
                                    oprType={this.state.oprType} 
                                /> 
                            </div>
                        </div>
                        <div className="content-inner">
                        {/* 国际化处理： 输入批语*/}
                            <div className="label">{this.state.json['10160501-000073']}</div>
                            <div className="content">
                                <NCFormControl
                                    className="demo-input"
                                    value={this.state.suggestion}
                                    onChange={this.suggestChange}
                                />
                            </div>
                        </div>
                    </div>
                </NCModal.Body>);
        }
        else if (this.state.approveType == "Noapprove") {
            modalTitle = this.state.json['10160501-000001'];/* 国际化处理： 不批准*/
            modalBody = (
                <NCModal.Body >
                    <div className="body-content">
                        <div className="content-inner">
                            {/* 国际化处理： 输入批语*/}
                            <div className="label">{this.state.json['10160501-000073']}</div>
                            <div className="content">
                                <NCFormControl
                                    className="demo-input"
                                    value={this.state.suggestion}
                                    onChange={this.suggestChange}
                                />
                            </div>
                        </div>
                    </div>
                </NCModal.Body>);
        }
        else if (this.state.approveType == "reject") {
            modalTitle = this.state.json['10160501-000002'];/* 国际化处理： 驳回*/
            modalBody =
                (<NCModal.Body >
                    <div className="body-content">
                        <div className="content-inner">
                            {/* 国际化处理： 选择驳回环节*/}
                            <div className="label" style={{ width: 90 }}>{this.state.json['10160501-000074']}</div>
                            <div className="content">
                                <NCSelect
                                    defaultValue={this.state.json['10160501-000045']}/* 国际化处理： 请选择驳回环节*/
                                    style={{ width: 200 }}
                                    onChange={this.handleRejectChange}
                                >
                                    {this.state.Children}
                                </NCSelect>
                            </div>
                        </div>
                        <div className="content-inner">
                            {/* 国际化处理： 输入批语*/}
                            <div className="label">{this.state.json['10160501-000073']}</div>
                            <div className="content">
                                <NCFormControl
                                    className="demo-input"
                                    value={this.state.suggestion}
                                    onChange={this.suggestChange}
                                />
                            </div>
                        </div>
                    </div>
                </NCModal.Body>);
        }
        else if (this.state.approveType == "addapprove") {
            modalTitle = this.state.json['10160501-000046'];/* 国际化处理： 加签*/
            modalBody = (<NCModal.Body >
                <div className="body-content">
                    <div className="content-inner">
                        <div className="label" style={{ width: 90 }}>{this.state.json['10160501-000075']}</div>{/* 国际化处理： 选择加签人*/}
                        <div className="content">
                            <div style={{ width: 200 }}>
                            <AddUserRefer
                                    value={_this.state.addApproverUsers}
                                    queryCondition={{
                                        GridRefActionExt: "nccloud.web.riart.billtype.ref.action.ItfApproveCenterUserRef",
                                        isMutiGroup: false,
                                        isShowGroupAdmin: false,
                                        isShowSysAdmin: false,
                                        isAuthFilter: false,
                                        isAllUserVisible: false,
                                        isShareUserVisible: false,
                                        isSelfVisible: false,
                                        isNeedNCUser: false,
                                        adminoption: 'USE'
                                    }}
                                    onChange={_this.setAddApprover}
                                    isMultiSelectedEnabled={true}
                                    placeholder={this.state.json['10160501-000047']}/* 国际化处理： 加签人*/
                                />
                            </div>
                        </div>
                    </div>
                    <div className="content-inner">
                        <div className="label">{this.state.json['10160501-000073']}</div>{/* 国际化处理： 输入批语*/}
                        <div className="content">
                            <NCFormControl
                                className="demo-input"
                                value={this.state.suggestion}
                                onChange={this.suggestChange}
                            />
                        </div>
                    </div>
                </div>
            </NCModal.Body>);
        }
        else if (this.state.approveType == "transfer") {
            modalTitle = this.state.json['10160501-000048'];/* 国际化处理： 改派*/
            modalBody =
                (<NCModal.Body >
                    <div className="body-content">
                        <div className="content-inner">
                            <div className="label" style={{ width: 90 }}>{this.state.json['10160501-000076']}</div>{/* 国际化处理： 选择改派人员*/}
                            <div className="content">
                                <div style={{ width: 200 }}>
                                    <UserRefer
                                        value={_this.state.transferApproverUser}
                                        queryCondition={{
                                            isMutiGroup: false,
                                            isShowGroupAdmin: false,
                                            isShowSysAdmin: false,
                                            isAuthFilter: false,
                                            isAllUserVisible: false,
                                            isShareUserVisible: false,
                                            isSelfVisible: false,
                                            isNeedNCUser: false,
                                            adminoption: 'USE'
                                        }}
                                        onChange={_this.setTransferApprover}
                                        isMultiSelectedEnabled={false}
                                        placeholder={this.state.json['10160501-000049']}/* 国际化处理： 改派人*/
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="content-inner">
                            <div className="label">{this.state.json['10160501-000073']}</div>{/* 国际化处理： 输入批语*/}
                            <div className="content">
                                <NCFormControl
                                    className="demo-input"
                                    value={this.state.suggestion}
                                    onChange={this.suggestChange}
                                />
                            </div>
                        </div>
                    </div>
                </NCModal.Body>);
        }
        return (
            <div className={`${(pageMsgType == "notice" || pageMsgType == "prealert") && this.state.message.isread == "N" ? "list-item noread" : "list-item"}`}
                onClick={(pageMsgType == "notice" || pageMsgType == "prealert") && this.state.message.isread == "N" ? this.markRead : () => { }}>


                {this.state.assgindisplay ? <ApprovalTrans
                    data={this.state.assgindata}
                    // hideNote={true}
                    display={this.state.assgindisplay}
                    cancel={this.cancelAssginModal}
                    getResult={this.dealAssginUsedr} /> : ''}


                <div className="item-header approve">
                    {pageMsgType != "todo" ?
                        <span className="table-layout checkbox-item">
                            {
                                pageMsgType === 'approve' && this.state.message.canbatch == "N" ?
                                    <NCPopover
                                        placement="top"
                                        trigger="hover"
                                        content={this.state.json['10160501-000050']}/* 国际化处理： 该消息不能批量审批*/
                                    >
                                        <span>
                                            <NCCheckbox
                                                checked={this.state.message.checked}
                                                disabled={this.state.message.canbatch == "N"}
                                                onChange={this.handleSelect} >
                                            </NCCheckbox>
                                        </span>
                                    </NCPopover>
                                    :
                                    <NCCheckbox
                                        checked={this.state.message.checked}
                                        disabled={this.state.message.canbatch == "N"}
                                        onChange={this.handleSelect} >
                                    </NCCheckbox>
                            }
                        </span> : ''
                    }

                    {/* <p className={pageMsgType !== "approve" && this.state.message.isread == "Y" ? "item-title link" : "item-title"}
                        onClick={pageMsgType !== "approve" ? this.openDetailPage : {}}>{this.state.message.subject}</p> */}
                    {pageMsgType == "todo" || pageMsgType == "approve" ?
                        <div className={`item-title link ${pageMsgType == "approve" ? (this.state.message.isread == "Y" ? "" : "approve") : ""}`} title={this.state.message.subject} onClick={pageMsgType == "todo" ? this.openDetailPage : this.openApproveDetailPage}><p className="approve-title">{this.state.message.subject}</p></div>
                        :
                        <div className="item-title" title={this.state.message.subject}><p className="notice-title">{this.state.message.subject}</p>
                            {pageMsgType == "notice" || pageMsgType == "prealert" ?
                                this.state.message.isread == "Y" ?
                                    <span className="read-tag isread">{this.state.json['10160501-000029']}</span> : <span className="read-tag">{this.state.json['10160501-000030']}</span>/* 国际化处理： 已读,未读*/
                                : ''}
                        </div>
                    }
                    {pageMsgType == "approve" ?
                        <span>
                            <span className="item-text">{this.state.json['10160501-000077']}: {this.state.message.senderName}</span>{/* 国际化处理： 提交*/}
                            <span className="item-text">{this.state.message.sendtime}</span>
                        </span> : ''
                    }
                    <div className="table-layout right-area">
                        {pageMsgType != "approve" ?
                            <span className="right">
                                <span className="item-text">{this.state.json['10160501-000077']}: {this.state.message.senderName}</span>{/* 国际化处理： 提交*/}
                                <span className="item-text">{this.state.message.sendtime}</span>
                                {pageMsgType == "notice" && this.state.message.funccode && this.state.message.funccode.length > 0 ? <NCButton onClick={this.openDetailPage}>{this.state.json['10160501-000078']}</NCButton> : ""}        {/* 国际化处理： 详情*/}
                            </span>
                            :
                            (this.state.message.isread == "Y" ?
                                (
                                    specialWorkflowtypes.indexOf(parseInt(this.state.message.workflowtype)) > -1 ?
                                        <span className="btn-content btn-content-first" style={{ textAlign: 'center' }}>
                                            <NCButton onClick={() => this.props.modal.show('processCancel')}>{this.state.json['10160501-000079']}</NCButton>{/* 国际化处理： 撤销*/}
                                            <NCButton onClick={function () { _this.setState({ sadm: true }); }}>{this.state.json['10160501-000081']}</NCButton>{/* 国际化处理： 单据详情改审批详情*/}
                                        </span>
                                        :
                                        <span className="btn-content" style={{ textAlign: 'center' }}>
                                            <NCButton onClick={() => this.props.modal.show('approveCancel')}>{this.state.json['10160501-000056']}</NCButton>{/* 国际化处理： 取消审批*/}
                                            {/* <NCButton onClick={this.openApproveDetailPage}>审批详情</NCButton> */}
                                            <NCButton onClick={function () { _this.setState({ sadm: true }); }}>{this.state.json['10160501-000081']}</NCButton>{/* 国际化处理： 审批详情*/}
                                        </span>
                                )
                                :
                                (specialWorkflowtypes.indexOf(parseInt(this.state.message.workflowtype)) > -1 ?
                                    <div className="btn-area right">
                                        {hintMessage ? <NCPopover placement="bottom" content={hintContent} trigger="hover">
                                            <span className="iconfont icon-yujing"></span>
                                        </NCPopover> : ''}
                                        
                                        {rejectHistory.isReject ? <NCPopover placement="bottom" content={rejectHistory.rejectReason} trigger="hover">
                                            <span className="iconfont icon-xiaoxi"></span>
                                        </NCPopover> : ''}
                                        {true || (enableActions || []).filter(function (item, idx, origin) { return item == "pass"; }).length > 0 || 6 == parseInt(this.state.message.workflowtype) ?
                                            <div className="comment-input">
                                                <NCFormControl
                                                    className="demo-input"
                                                    placeholder="0/200"
                                                    value={this.state.defaultSgt}
                                                    onChange={this.defaultSgtChange}
                                                />
                                            </div> : ""}
                                        {parseInt(this.state.message.workflowtype) == 6 ? "" :
                                            <div className="btn-content btn-content-first">
                                                {(enableActions || []).length > 0 ?
                                                    <NCButton colors="primary" onClick={this.workflowPassOrAssgin}>{enableActions[0]}</NCButton>
                                                    : ""}
                                            </div>
                                        }
                                        <div className="btn-content">
                                            {(enableActions || []).filter(function (item, idx, origin) { return item == "pass"; }).length > 0 ?
                                                <NCButton colors="primary" onClick={this.approvePassOrAssgin}>{this.state.json['10160501-000000']}</NCButton> : ""}{/* 国际化处理： 批准*/}
                                            {(enableActions || []).filter(function (item, idx, origin) { return item == "reject"; }).length > 0 ?
                                                // <NCButton onClick={this.openRejectModal}>驳回</NCButton> 
                                                <ApproveBtns billMessage={{ billtype: this.state.message.billtype, billid: this.state.message.billid, pk_detail: this.state.message.pk_detail }}
                                                    comment={_this.state.defaultSgt}
                                                    btns={['reject']}
                                                    submitRejectBillMode={submitRejectBillMode}
                                                    onHandleSuccess={function () {
                                                        _this.closeResumeModal();
                                                        // _this.markRead();
                                                        _this.markIsread();
                                                        _this.hideCurrentItem();//隐藏当前操作条目
                                                    }}
                                                />
                                                : ""}
                                            {(
                                                specialWorkflowtypes.indexOf(parseInt(this.state.message.workflowtype)) > -1 ?
                                                    <NCButton onClick={function () { _this.setState({ sadm: true }); }}>{this.state.json['10160501-000081']}</NCButton>/* 国际化处理： 单据详情改为审批详情*/
                                                    :
                                                    // <NCButton onClick={this.openApproveDetailPage}>审批详情</NCButton>
                                                    <NCButton onClick={function () { _this.setState({ sadm: true }); }}>{this.state.json['10160501-000081']}</NCButton>/* 国际化处理： 审批详情*/
                                            )}

                                            {enableActions && enableActions.length > 1 && (enableActions.filter(el => ["nopass", "addapprover", "transfer"].includes(el))).length > 0 ?
                                                <NCDropdown overlay={moreMenu} animation="slide-up">
                                                    {/* <NCButton>{this.state.json['10160501-000070']} <i className="icon iconfont icon-hangcaozuoxiala1" style={{ fontSize: '12px' }} onClick={function () { }}></i></NCButton>国际化处理： 更多 */}
                                                    <NCButton>更多 <i className="icon iconfont icon-hangcaozuoxiala1" style={{ fontSize: '12px' }} onClick={function () { }}></i></NCButton>{/* 国际化处理： 更多*/}
                                                </NCDropdown> : ''}
                                        </div>
                                    </div>
                                    :
                                    <div className="btn-area right">
                                        {rejectHistory.isReject ? <NCPopover placement="bottom" content={rejectHistory.rejectReason} trigger="hover">
                                            <span className="iconfont icon-xiaoxi"></span>
                                        </NCPopover> : ''}
                                        {true || (enableActions || []).filter(function (item, idx, origin) { return item == "pass"; }).length > 0 ?
                                            <div className="comment-input">
                                                <NCFormControl
                                                    className="demo-input"
                                                    placeholder="0/200"
                                                    value={this.state.defaultSgt}
                                                    onChange={this.defaultSgtChange}
                                                />
                                            </div> : ""}
                                        <div className="btn-content">
                                            {hintMessage ? <NCPopover placement="bottom" content={hintContent} trigger="hover">
                                                <span className="iconfont icon-yujing"></span>
                                            </NCPopover> : ''}
                                            {(enableActions || []).filter(function (item, idx, origin) { return item == "pass"; }).length > 0 ?
                                                <NCButton colors="primary" onClick={this.approvePassOrAssgin}>{this.state.json['10160501-000000']}</NCButton> : ""}{/* 国际化处理： 批准*/}
                                            {(enableActions || []).filter(function (item, idx, origin) { return item == "reject"; }).length > 0 ?
                                                // <NCButton onClick={this.openRejectModal}>驳回</NCButton> 
                                                <ApproveBtns billMessage={{ billtype: this.state.message.billtype, billid: this.state.message.billid, pk_detail: this.state.message.pk_detail }}
                                                    comment={_this.state.defaultSgt}
                                                    btns={['reject']}
                                                    submitRejectBillMode={submitRejectBillMode}
                                                    onHandleSuccess={function () {
                                                        _this.closeResumeModal();
                                                        // _this.markRead();
                                                        _this.markIsread();
                                                        _this.hideCurrentItem();//隐藏当前操作条目
                                                    }}
                                                />
                                                : ""}
                                            {(
                                                specialWorkflowtypes.indexOf(parseInt(this.state.message.workflowtype)) > -1 ?
                                                    <NCButton onClick={this.openWorkflowDetailPage}>{this.state.json['10160501-000080']}</NCButton>/* 国际化处理： 单据详情*/
                                                    :
                                                    // <NCButton onClick={this.openApproveDetailPage}>审批详情</NCButton>
                                                    <NCButton onClick={function () { _this.setState({ sadm: true }); }}>{this.state.json['10160501-000081']}</NCButton>/* 国际化处理： 审批详情*/
                                            )}

                                            {enableActions && enableActions.length > 1 && (enableActions.filter(el => ["nopass", "addapprover", "transfer"].includes(el))).length > 0 ?
                                                <NCDropdown overlay={moreMenu} animation="slide-up">
                                                    <NCButton>{this.state.json['10160501-000070']} <i className="icon iconfont icon-hangcaozuoxiala1" style={{ fontSize: '12px' }} onClick={function () { }}></i></NCButton>{/* 国际化处理： 更多*/}
                                                </NCDropdown> : ''}
                                        </div>
                                    </div>)
                            )
                        }
                    </div>
                </div>
                <div className="item-content">
                    <div className={pageMsgType == "approve" ? "content-approve" : "content-other"} style={pageMsgType == "prealert" ? { paddingLeft: 20 } : {}}>
                        <div className={`clearfix ${pageMsgType === "notice" && !this.state.noticeShowMore ? 'notice-con' : pageMsgType === "approve" && !this.state.approveShowMore ? 'approve-con' : ''}`} dangerouslySetInnerHTML={content}></div>
                        {/* {console.log('len', Utils.getStrNum(content.__html, 'labeltext'))} */}
                        <div style={{ display: Utils.getStrNum(content.__html, 'labeltext') > 6 ? 'inline-block' : 'none' }}>
                            {
                                pageMsgType == "notice" ?
                                    <span className="show-more" onClick={() => this.setState({ noticeShowMore: !this.state.noticeShowMore })}>{this.state.noticeShowMore ? this.state.json['10160501-000051'] : this.state.json['10160501-000052']}</span> /* 国际化处理： [收起],[查看全部]*/
                                    : pageMsgType == "approve" ?
                                        <span className="show-more" onClick={() => this.setState({ approveShowMore: !this.state.approveShowMore })}>{this.state.approveShowMore ? this.state.json['10160501-000035'] : this.state.json['10160501-000053']}</span> /* 国际化处理： 收起,查看全部*/
                                        : ''
                            }
                        </div>
                        {true || pageMsgType != "approve" ?
                            <div className="right-area">
                                {/* {pageMsgType == "prealert" ? 
                                        <span>
                                            <span className="item"><i className="icon iconfont icon-yewuchuli"></i> 业务处理</span>
                                            <span className="item"><i className="icon iconfont icon-shituqiehuan"></i> 查看详情</span>
                                        </span> : ''
                                    } */}
                                {pageMsgType == "notice" && this.state.message.isread == "N" ? <span className="item" onClick={_this.markRead}><i className="icon iconfont icon-yidu"></i> {this.state.json['10160501-000084']}</span> : ''}{/* 国际化处理： 标记已读*/}
                                {pageMsgType == "notice" && this.state.message.isread == "Y" ? <span className="item" onClick={_this.markUnRead}><i className="icon iconfont icon-yidu"></i> {this.state.json['10160501-000085']}</span> : ''}{/* 国际化处理： 标记未读*/}
                                {false && pageMsgType == "prealert" ? <span className="item"><i className="icon iconfont icon-yidu"></i> {this.state.json['10160501-000082']}</span> : ''}{/* 国际化处理： 业务处理*/}
                                {this.state.message.attachments.length > 0 ?

                                    <NCPopover
                                        placement="top"
                                        trigger="hover"
                                        content={this.state.message.attachments[0].filename}
                                    >
                                        <span className="item" onClick={() => _this.downloadAtta(_this.state.message.attachments[0].pk_file, _this.state.message.attachments[0].filename)}>
                                            <i className="icon iconfont icon-fujianshenpi"></i> {this.state.json['10160501-000086']}({this.state.message.attachments.length}){/* 国际化处理： 附件*/}
                                        </span>
                                    </NCPopover>

                                    : ''}
                            </div> : ''
                        }
                    </div>
                </div>

                {_this.state.sadm ?
                    <ApproveDetail
                        show={_this.state.sadm}
                        close={function () { _this.setState({ sadm: false }); }}
                        billtype={_this.state.message.billtype}
                        billid={_this.state.message.billid}
                    />
                    : ""}

                {/* <span style={{float:'right'}}>{this.state.message.isread == "Y" ? "已读" : "未读"}</span> */}
                {/* {form.createForm("msgBoxTemplate", {})} */}

                {/* <div id={this.props.message.pk_message+"approve"}>{this.props.message.pk_message+"approve"}</div> */}
                {createModal('processCancel', {
                    className: "junior",
                    title: this.state.json['10160501-000054'],/* 国际化处理： 取消处理*/
                    content: this.state.json['10160501-000055'],/* 国际化处理： 已经处理完成，确定收回？*/
                    beSureBtnClick: _this.processCancel
                })}
                {createModal('approveCancel', {
                    className: "junior",
                    title: this.state.json['10160501-000056'],/* 国际化处理： 取消审批*/
                    content: this.state.json['10160501-000057'],/* 国际化处理： 已经审批完成，确定收回？*/
                    beSureBtnClick: _this.approveCancel
                })}
                <NCModal
                    className="form-modal"
                    size="xlg"
                    show={this.state.showApproveDetailModal}
                    backdrop={true}
                    onHide={this.closeApproveDetailModal}>
                    <NCModal.Header closeButton>
                        <NCModal.Title > {this.state.json['10160501-000081']} </NCModal.Title>{/* 国际化处理： 审批详情*/}
                    </NCModal.Header >

                    <NCModal.Body >
                        {/* {this.createApprove()} */}
                        <div>
                            {/* iframe src={this.state.approveDetailModalUrl} */}
                            <iframe src={this.state.approveDetailModalUrl} style={{ width: "100%" }}></iframe>
                            {/* <iframe src="/uap/imp/msgTempType/main/index.html"></iframe> */}
                        </div>
                    </NCModal.Body>
                    <NCModal.Footer>
                        <NCButton colors="primary" onClick={this.detailDoApprove}> {this.state.json['10160501-000012']} </NCButton>{/* 国际化处理： 确定*/}
                        <NCButton className="btn-transparent" onClick={this.closeApproveDetailModal}> {this.state.json['10160501-000087']} </NCButton>{/* 国际化处理： 取消*/}
                    </NCModal.Footer>
                </NCModal>

                <NCModal
                    className="form-modal"
                    size={this.state.resumeDetail && this.state.resumeDetail.size ? this.state.resumeDetail.size : "lg"}
                    show={this.state.showResumeModal}
                    backdrop={true}
                    onHide={this.closeResumeModal}>
                    <NCModal.Header closeButton>
                        <NCModal.Title > {this.state.json['10160501-000011']} </NCModal.Title>{/* 国际化处理： 审批警告*/}
                    </NCModal.Header >

                    <NCModal.Body >
                        {this.state.resumeDetail && this.state.resumeDetail.html ? <div dangerouslySetInnerHTML={resumeHtml}></div> : ""}
                        {this.state.resumeDetail && this.state.resumeDetail.url ? <iframe src={this.state.resumeDetail.url} style={{ width: "100%", height: (domHeight - 150) + "px" }}></iframe> : ""}
                    </NCModal.Body>
                    <NCModal.Footer>
                        {this.state.resumeCode == "313" || this.state.resumeCode == "316" ? "" :
                            <NCButton colors="primary" onClick={this.detailDoApprove}> {this.state.json['10160501-000012']} </NCButton>}{/* 国际化处理： 确定*/}
                        {this.state.resumeCode != "314" && this.state.resumeCode != "315" ? "" :
                            <NCButton colors="primary" onClick={this.detailCancelApprove}> {this.state.json['10160501-000087']} </NCButton>}{/* 国际化处理： 取消*/}
                        <NCButton className="btn-transparent" onClick={this.closeResumeModal}> {this.state.json['10160501-000013']} </NCButton>{/* 国际化处理： 关闭*/}
                    </NCModal.Footer>
                </NCModal>

                <NCModal
                    className="form-modal"
                    onShow={this.showRejectData}
                    show={this.state.showApproveModal}
                    backdrop={true}
                    onHide={this.closeApproveModal}>
                    <NCModal.Header closeButton>
                        <NCModal.Title > {modalTitle} </NCModal.Title>
                    </NCModal.Header >
                    {modalBody}
                    <NCModal.Footer>
                        <NCButton colors="primary" onClick={this.detailDoApprove}> {this.state.json['10160501-000012']}</NCButton>{/* 国际化处理： 确定*/}
                        <NCButton className="btn-transparent" onClick={this.closeApproveModal}> {this.state.json['10160501-000013']} </NCButton>{/* 国际化处理： 关闭*/}
                    </NCModal.Footer>
                </NCModal>

            </div>
        );
        // <ApprovalTrans
        // data={this.state.assgindata1111}
        // display = {true}
        // getResult = {this.dealAssginUsedr} />
    }
}



MessageBox = createPage({
    initTemplate: initTemplate,
})(MessageBox);


class MessageList extends Component {
    constructor(props) {
        super(props);
        this.props = props;
        let _this = this;
        let binds = "refreshSummary countRefreshed closeBatchModal changeBatch doBatch getCanBatch selectOne selectAll batchApprove batchSetRead batchSetUnread markIsread markIsreads changePage refreshList hideCurrentItem"; // batchTurnDown
        binds.split(" ").forEach(function (item) {
            _this[item] = _this[item].bind(_this);
        });
        let _startTime = "";
        let _isread = "N";
        if (initParam == "recent3") {
            _startTime = (new Date()).DateAdd('d', -3).format("yyyy-MM-dd 00:00:00");
            _isread = "Y";
        }
        else if (initParam == "recent7") {
            _startTime = (new Date()).DateAdd('d', -7).format("yyyy-MM-dd 00:00:00");
            _isread = "Y";
        }
        else if (initParam == "recent30") {
            _startTime = (new Date()).DateAdd('m', -1).format("yyyy-MM-dd 00:00:00");
            _isread = "Y";
        }
        else {
            _startTime = (pageMsgType == "todo" ? "" : (new Date()).DateAdd('m', -1).format("yyyy-MM-dd 00:00:00"));
        }
        this.state = {
            json: {},
            inlt: null,
            conditionClass: 'unfixed',
            messages: [],
            showLoadingIcon: false,
            selectedAll: false,
            needRefresh: false,
            totalPage: 10,
            unreadCounts: {
                approve: 0,
                todo: 0,
                prealert: 0,
                notice: 0
            },
            listParams: {
                msgType: pageMsgType,
                isread: _isread,
                startTime: _startTime,
                endTime: "", msgsourcetype: "", billtype: "", billno: "", subject: "",
                page: 1,
                perPage: 10
            },
            showBatchModal: false,
            batchAction: "approve",
            pageType: "approve",
            loadingText: '10160501-000058'/* 国际化处理： 滚动加载更多*/
        }
    }



    refreshList(params, more) {
        let _this = this;
        this.setState({ listParams: params });
        this.setState({ "needRefresh": true });
        ajax({
            url: '/nccloud/riart/message/list.do',
            data: params,
            method: 'POST',
            success: (res) => {
                loading = false;
                //let data = res.data;
                let list = res.data.items;
                let _perPage = parseInt(_this.state.listParams.perPage);
                let _total = parseInt(res.data.total);
                let _totalPage = Math.ceil(_total / _perPage);
                let dt = list.map(function (item, idx, origin) {
                    let _item = item;
                    _item.checked = false;
                    let mmt = moment(item.sendtime);
                    _item.sendtime = DongbaToLocalTime(mmt).format('YYYY-MM-DD HH:mm:ss');
                    if (pageMsgType == "prealert" || pageMsgType == "notice") _item.canbatch = "Y";//todo
                    _item.id = _item.pk_message;
                    return _item;
                });
                //是否最后一页
                if (_totalPage == params.page && list.length > 0) {
                    _this.setState({ loadingText: '10160501-000059' });/* 国际化处理： 到底了*/
                }
                else {
                    _this.setState({ loadingText: '10160501-000058' });/* 国际化处理： 滚动加载更多*/
                }
                let _msgs = _this.state.messages;
                _this.setState({ messages: [] });
                _this.setState({
                    messages: more ? [..._msgs, ...dt] : dt,
                    total: _total,
                    showLoadingIcon: false,
                    totalPage: _totalPage,
                    selectedAll: false
                });
                _this.scrollBottom();
                readStateChanged = false;
            }
        });
    }

    changePage(page, scroll) {
        let _this = this;
        let _listParams = _this.state.listParams;
        _listParams.page = page;
        //_this.setState({listParams: _listParams});
        if (!scroll) {
            _this.refreshList(_listParams);
        } else {
            this.loadMoreData(_listParams);
        }
    }

    componentWillMount() {
        let pageType = urlParams["msgType"] || "approve";
        this.setState({
            pageType
        });

        let callback = (json, status, inlt) => { // json 多语json格式参数； status： 是否请求到json多语，可用来判断多语资源是否请求到并进行一些别的操作； inlt： 可用来进行占位符的一些操作
            if (status) {
                this.setState({ json, inlt });// 保存json和inlt到页面state中并刷新页面
            } else {
                console.log('未加载到多语资源');   // 未请求到多语资源的后续操作
            }

        }
        this.props.MultiInit.getMultiLang({ 'moduleId': '1016-10160501', 'domainName': 'uap', callback });

    }

    componentDidMount() {
        let _this = this;
        _this.refreshList(_this.state.listParams);
        this.scrollBottom();
        this.refreshSummary();
    }

    refreshSummary() {
        let _this = this;
        ajax({
            url: '/nccloud/riart/message/summary.do',
            data: {},
            method: 'POST',
            success: (res) => {
                let retVal = { counts: res.data };

                _this.setState({
                    unreadCounts: retVal.counts
                });
            }
        });
    }

    getCanBatch() {
        let msgs = this.getSelected().filter(function (item, idx, origin) {
            return item.canbatch == "N";
        });
        return msgs.length > 0;
    }

    getSelected() {
        let _state = this.state;
        let msgs = _state.messages.filter(function (item, idx, origin) {
            return item.checked;
        });
        return msgs;
    }

    getSelectedIds() {
        let ids = this.getSelected().map(function (item, idx, origin) {
            return item.id;
        });
        return ids;
    }

    selectOne(id) {
        let _state = this.state;
        _state.messages.forEach(function (item, idx, origin) {
            if (item.id == id) {
                if (item.checked) {
                    _state.selectedAll = false;
                }
                item.checked = !item.checked;
            }
        });
        this.setState({ messages: _state.messages });
    }

    selectAll(e) {
        let _state = this.state;
        _state.messages.forEach(function (item, idx, origin) {
            if (item.canbatch != "N") item.checked = e;
        });
        this.setState({ messages: _state.messages, selectedAll: e });
    }

    batchApprove() {

        //不再使用
        return;

        let _this = this;
        let ids = this.getSelected();
        let messages = _this.getSelected();
        let isOver = false;
        let errormsg = "";
        let messagepks = [];


        if (!messages || messages.length == 0) {
            toast({ color: 'warning', content: this.state.json['10160501-000060'] });/* 国际化处理： 选择批量数据*/
            return;
        }

        for (let i = 0; i < messages.length; i++) {
            messagepks.push(messages[i].pk_message)

        }

        ajax({
            url: '/nccloud/workflow/approvalcenter/batchApproveAction.do',
            data: params,
            method: 'POST',
            async: false,
            success: (res) => {
                debugger;
                if ((res.data == "200" || res.data.code == "200")) {
                    toast({ color: 'success' });
                    //    _this.closeApproveDetailModal();
                    _this.closeBatchModal();
                    //    _this.markRead();
                }
                else {
                    toast({ color: 'danger', content: res.data.errormsg });
                    //是一个数组
                    toast({ color: 'danger', content: res.data.errormsgpks });
                    _this.markIsreads(res.data.errormsgpks);

                }
            }
        });

    }

    closeBatchModal() {
        this.setState({ showBatchModal: false });
    }

    changeBatch(type) {
        let _this = this;
        let ids = this.getSelectedIds();
        console.log(ids);
        // if(type == "batchApprove"){
        //     //todo
        //     //this.batchApprove();
        //     _this.setState({showBatchModal: true, batchAction: "approve"});
        // }
        // else if(type == "batchReject"){
        //     //todo
        //     _this.setState({showBatchModal: true, batchAction: "rejecttofirst"});
        // }
        // else if(type == "batchRefuse"){
        //     //todo
        //     _this.setState({showBatchModal: true, batchAction: "nopass"});
        // }
        _this.setState({ showBatchModal: true, batchAction: "approve" });
    }

    doBatch(params) {
        debugger;
        console.log(params);
        let _this = this;
        let ids = this.getSelectedIds();
        console.log(ids);
        params.messagepks = ids;

        ajax({
            url: '/nccloud/workflow/approvalcenter/batchApproveAction.do',
            data: params,
            method: 'POST',
            async: false,
            success: (res) => {
                debugger;
                if (res.data && (res.data == "200" || res.data.code == "200")) {
                    toast({ color: 'success' });
                    //    _this.closeApproveDetailModal();
                    _this.closeBatchModal();
                    //    _this.markRead();
                    let ids = _this.getSelectedIds();
                    if (params.actiontype == "rejecttofirst") {
                        _this.hideCurrentItems(ids);
                    }
                    else {
                        _this.markIsreads(ids);
                    }

                } else if (res.data && res.data.errormsg) {
                    toast({ color: 'danger', content: res.data.errormsg });
                    //是一个数组
                    //   toast({ color: 'danger', content: res.data.errormsgpks});
                    let _ids = ids;
                    if (res.data.errormsgpks) {
                        _ids = ids.filter(function (item, idx, origin) { return res.data.errormsgpks.indexOf(item) < 0; });
                    }
                    _this.markIsreads(_ids);
                } else {
                    toast({ color: 'danger', content: this.state.json['10160501-000061'] });/* 国际化处理： 业务数据异常，请刷新尝试*/
                }
            }
        });

    }

    batchSetReadAction(unread) {
        let _this = this;
        let ids = this.getSelectedIds();
        console.log(ids);
        ajax({
            url: '/nccloud/riart/message/setRead.do',
            data: { "pk_messages": ids, msgType: pageMsgType, isread: unread ? "N" : "Y" },
            method: 'POST',
            success: (res) => {
                //let data = res.data;
                _this.markIsreads(ids, unread);
            },
            error: (res) => {
                toast({ content: res.message, color: 'danger' });
            }
        });
    }

    batchSetRead() {
        this.batchSetReadAction();
    }

    batchSetUnread() {
        this.batchSetReadAction(true);
    }

    markIsread(id, unread) {
        let _state = this.state;
        _state.messages.forEach(function (item, idx, origin) {
            if (item.id == id) {
                item.isread = unread ? "N" : "Y";
            }
        });
        this.setState({ messages: _state.messages });
        this.refreshSummary();
        this.setState({ "needRefresh": true });
        readStateChanged = true;
    }





    markIsreads(ids, unread) {
        let _this = this;
        let _state = this.state;
        _state.messages.forEach(function (item, idx, origin) {
            ids.forEach(function (_item, _idx, _origin) {
                if (item.id == _item) {
                    item.isread = unread ? "N" : "Y";
                }
            });
        });
        this.setState({ messages: _state.messages });
        this.refreshSummary();
        this.setState({ "needRefresh": true });
        readStateChanged = true;
    }

    //滚动到底部加载更多
    scrollBottom = () => {
        let page = this.state.listParams.page;
        let _this = this;
        let isFixed = false;
        window.onscroll = function () {
            let scrollTop = document.documentElement.scrollTop || document.body.scrollTop;  //已滚动高度
            // let domHeight = document.body.clientHeight; //DOM高度
            let domHeight = document.getElementsByClassName('message-wrap')[0].clientHeight;
            let winHeight = document.documentElement.clientHeight; //窗口高度
            let sumH = winHeight + scrollTop;
            console.log('domHeight, winHeight, sumH', domHeight, winHeight, sumH);
            if (sumH >= domHeight + 50) {
                _this.setState({
                    showLoadingIcon: true
                });
                if (!loading) {
                    console.log("loading: " + loading + "   page: " + page + " _this.state.totalPage:" + _this.state.totalPage);
                    loading = true;
                    if (_this.state.totalPage > page) {
                        page++;
                        _this.changePage(page, true);
                    }
                }
            } else if (sumH < domHeight + 100) {
                _this.setState({
                    showLoadingIcon: false
                });
            }
            let h = this.document.documentElement.scrollTop || this.document.body.scrollTop;
            if (h > 120) {
                if (!isFixed) {
                    // console.log('fixed')
                    _this.setState({
                        conditionClass: 'fixed'
                    });
                    isFixed = true;
                }
            } else {
                if (isFixed) {
                    // console.log('move')
                    _this.setState({
                        conditionClass: 'unfixed'
                    });
                    isFixed = false;
                }
            }
        }
    };

    //滚动分页加载数据
    loadMoreData = (params) => {
        this.refreshList(params, true);
    }

    countRefreshed() {
        this.setState({ "needRefresh": false });
    }

    //隐藏当前操作条目
    hideCurrentItem(index) {
        let { messages } = this.state;
        let newMessages = cloneObj(messages);
        newMessages[index] && newMessages.splice(index, 1);
        console.log(this.state.json['10160501-000062'], newMessages, index);/* 国际化处理： 删除啦*/
        this.setState({ messages: [] });
        this.setState({
            messages: newMessages
        });
    }

    //隐藏当前操作条目
    hideCurrentItems(ids) {
        let _this = this;
        let { messages } = this.state;
        let newMessages = messages.filter(function (item, idx, origin) { return (ids.indexOf(item.pk_message) < 0); });;
        console.log(this.state.json['10160501-000062'], newMessages, ids);/* 国际化处理： 删除啦*/
        this.setState({ messages: [] });
        setTimeout(function(){
            _this.setState({
                messages: newMessages
            });
            _this.refreshSummary();
            _this.setState({ "needRefresh": true });
        }, 500);
        
    }

    /*
    batchTurnDown(){
        let ids = this.getSelectedIds();
        console.log(ids);
    }
    //*/

    render() {
        let _this = this;
        if (!_this.state.selectedAll && _this.state.messages.length > 0 && _this.state.messages.length == _this.getSelectedIds().length) {
            _this.setState({ selectedAll: true });
        }

        const { form, approveDetail } = this.props;
        // let msgs = [];
        let _state = this.state;
        // if (_state && _state.messages) {
        //     msgs = _state.messages.map(function (item, idx, origin) {
        //         return <MessageBox 
        //                 message={item} 
        //                 selectOne={_this.selectOne} 
        //                 form={form} 
        //                 markIsread={_this.markIsread} 
        //                 approveDetail={approveDetail} 
        //                 />;
        //     });
        // }
        return (


            <div className="message-wrap" style={{ height: 'auto' }}>
                <BatchApprove
                    json={this.state.json}
                    inlt={this.state.inlt}
                    title={this.state.json['10160501-000089']} /* 国际化处理： 批量审批*/
                    display={_this.state.showBatchModal}
                    defaultSelected={_this.state.batchAction}
                    returnResult={_this.doBatch}
                    cancel={function () { _this.setState({ showBatchModal: false }); }}
                />
                <MessageCountBox
                    json={_this.state.json}
                    inlt={_this.state.inlt}
                    counts={_this.state.unreadCounts}
                    pageType={this.state.pageType}
                    onPageChange={(type) => {
                        let loc = `${location.pathname}?msgType=${urlParams["msgType"] || "approve"}&curPageType=${type}`;

                        // this.setState({
                        //     pageType: type
                        // });

                        let hashP = {
                            "approve": { appcode: "10160501", pagecode: "10160501APPROVE" },
                            "todo": { appcode: "10160502", pagecode: "10160502TODO" },
                            "prealert": { appcode: "10160503", pagecode: "10160503PREALERT" },
                            "notice": { appcode: "10160504", pagecode: "10160504NOTICE" }
                        };
                        try {
                            this.props.linkTo(loc, hashP[type]);
                        } catch (e) { }
                        // location.href = loc;
                        changePageMsgType(type);
                        let _startTime = (type == "todo" ? "" : (new Date()).DateAdd('m', -1).format("yyyy-MM-dd 00:00:00"));
                        let _isread = "N";
                        let _listParams = {
                            msgType: type,
                            isread: _isread,
                            startTime: _startTime,
                            endTime: "", msgsourcetype: "", billtype: "", billno: "", subject: "",
                            page: 1,
                            perPage: 10
                        };
                        _this.setState({ pageType: type, listParams: _listParams, needRefresh: true });
                        _this.refreshList(_listParams);
                    }}
                />
                <MessageSearchBox
                    json={_this.state.json}
                    inlt={_this.state.inlt}
                    needRefresh={_this.state.needRefresh}
                    countRefreshed={_this.countRefreshed}
                    listParams={_this.state.listParams}
                    refreshList={_this.refreshList}
                />
                <div className={"message-list-container " + (pageMsgType == "todo" ? "todo" : "")}>
                    {pageMsgType != "todo" ?
                        <div className={`list-header ${_state.conditionClass}`}>
                            <div className="header-layout" style={{ width: 140 }}>
                                <NCCheckbox className="list-checkbox" type="checkbox" onChange={this.selectAll} checked={this.state.selectedAll} >{this.state.json['10160501-000090']}</NCCheckbox>{/* 国际化处理： 全选*/}
                                {this.state.total == 0 ? "" : <span class="total">{this.state.inlt && this.state.inlt.get('10160501-000088', { 'total': this.state.total })}</span>}
                            </div>
                            <div className="header-layout">
                                {/* {pageMsgType != "approve" ? "" :
                                    <NCButton
                                        className="btn-white"
                                        onClick={this.batchApprove}
                                        disabled={this.getCanBatch()}
                                    >批量审批</NCButton>
                                } */}
                                {pageMsgType != "approve" || _this.state.listParams.isread == "Y" ? "" :
                                    <NCButton
                                        className="btn-white"
                                        onClick={function () { _this.changeBatch("batchApprove"); }}
                                        disabled={_this.getSelectedIds().length == 0}
                                    >{this.state.json['10160501-000091']}</NCButton>/* 国际化处理： 批量处理*/
                                }
                                {/* {pageMsgType != "approve" || _this.state.listParams.isread == "Y" ? "" :
                                    <NCSelect
                                            defaultValue={"batchApprove"}
                                            style={{ width: 100}}
                                            disabled={_this.getSelectedIds().length==0}
                                            // onChange={_this.changeBatch}
                                            onSelect={_this.changeBatch}
                                        >
                                        <NCOption key={"batchApprove"}>批量审批</NCOption>
                                        <NCOption key={"batchReject"}>批量驳回到制单人</NCOption>
                                        <NCOption key={"batchRefuse"}>批量拒绝</NCOption>
                                    </NCSelect>
                                } */}
                                {(pageMsgType != "notice" && pageMsgType != "prealert") && !isTest ? "" :
                                    <NCButton
                                        className="btn-white"
                                        onClick={this.batchSetRead}
                                        // disabled={this.getCanBatch()}
                                        disabled={_this.getSelectedIds().length == 0}
                                    >{this.state.json['10160501-000092']}{this.state.json[readTxt]}</NCButton>/* 国际化处理： 标记为*/
                                }
                                {(pageMsgType != "notice" && pageMsgType != "prealert") && !isTest ? "" :
                                    <NCButton
                                        className="btn-white"
                                        onClick={this.batchSetUnread}
                                        // disabled={this.getCanBatch()}
                                        disabled={_this.getSelectedIds().length == 0}
                                    >{this.state.json['10160501-000092']}{this.state.json[unreadTxt]}</NCButton>/* 国际化处理： 标记为*/
                                }
                            </div>
                        </div> : ''
                    }
                    <div className="message-list-box">
                        {_state.messages && _state.messages.map((item, idx, origin) => {
                            return <MessageBox
                                json={_this.state.json}
                                inlt={_this.state.inlt}
                                message={item}
                                selectOne={_this.selectOne}
                                form={form}
                                idx={idx}
                                markIsread={_this.markIsread}
                                approveDetail={approveDetail}
                                hideCurrentItem={_this.hideCurrentItem}
                            />;
                        })
                        }
                    </div>
                    {this.state.total > 0 && this.state.showLoadingIcon ?
                        <div className="loading-container">
                            <span className="icon iconfont icon-gundongxiangxia"></span>
                            <span>{this.state.json[this.state.loadingText]}</span>
                        </div> : ''}
                </div>
                {true || _this.state.totalPage < 2 ? "" :
                    <div>
                        <NCPagination
                            first
                            last
                            prev
                            next
                            boundaryLinks
                            items={_this.state.totalPage}
                            maxButtons={5}
                            activePage={_this.state.listParams.page}
                            onSelect={_this.changePage} />
                    </div>
                }
            </div>


        );
        // <ApprovalTrans
        // data={this.state.assgindata}
        // display = {this.state}
        // getResult = {this.dealAssginUsedr} />
    }
}


let MessageListPage = createPage({
    //initTemplate: initTemplate,
})(MessageList);

ReactDOM.render(<MessageListPage />, document.querySelector("#app"));

