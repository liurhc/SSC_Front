﻿import { ajax } from 'nc-lightapp-front'; 

export default function (props) {
	let callback = (json, status, inlt) => { // json 多语json格式参数； status： 是否请求到json多语，可用来判断多语资源是否请求到并进行一些别的操作； inlt： 可用来进行占位符的一些操作
		if (status) {
			let meta = {
				"msgBoxTemplate": {
					"items": [
						{
							attrcode: 'pk_detail',
							label: '单据编码',
							itemtype: 'input',
							col: 4,
							leftspace: 0,
							rightspace: 0,
							visible:true,
						},
						{
							attrcode: 'detail',
							label: '单据类型',
							itemtype: 'input',
							col: 4,
							leftspace: 0,
							rightspace: 0,
							visible:true,
						},
						{
							attrcode: 'destination',
							label: 'destination',
							itemtype: 'input',
							col: 4,
							leftspace: 0,
							rightspace: 0,
							visible:true,
						},
						{
							attrcode: 'msgsourcetype',
							label: 'msgsourcetype',
							itemtype: 'input',
							col: 4,
							leftspace: 0,
							rightspace: 0,
							visible:true,
						},
						{
							attrcode: 'sender',
							label: 'sender',
							itemtype: 'input',
							col: 4,
							leftspace: 0,
							rightspace: 0,
							visible:true,
						}
					],
					showindex: true,
					status: "browse"
					/*
					,
					"moduletype": "table",
					"orgid": null,
					"pagination": true,
					"sysid": null,
					"templatecode": "单据接口定义",
					"templatename": "单据接口定义",
					"tenantid": null
					*/
				}
			};
	
	/*
	//添加表格操作列
	let event = {

		label: '操作',
		attrcode: 'opr',
		render(text, record, index) {
			debugger;
			let recordVal = record.values;
			return (
				<div className="currency-opr-col">
					<span
						className="currency-opr-del"
						onClick={() => {
							console.log(index)
							props.editTable.delRow('10140curtpgrid', index)
						}}>删除</span>
				</div>
			);
		}
	};

	meta['10140curtpgrid'].items.push(event)
	//*/

		props.meta.setMeta(meta);
		}
		else {
			console.log('未加载到多语资源')   // 未请求到多语资源的后续操作
		}
	}
	props.MultiInit.getMultiLang({ moduleId: '1016-10160501', domainName: 'uap', callback });
}
