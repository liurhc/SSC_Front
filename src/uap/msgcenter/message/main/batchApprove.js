import React, { Component } from 'react';
import { base, high } from 'nc-lightapp-front';
const {NCModal, NCRadio, NCButton,NCTextArea } = base;
const { CommentList } = high;
// import CommentList from '../../approvalComponents/ApproveComment/CommentList'

export default class BatchApprove extends Component {
    constructor(props){
        super(props);
        this.state = {
            json: this.props.json,
            inlt: this.props.inlt,
            showModal: this.props.display,
            selectedValue: this.props.defaultSelected || 'approve',
            textValue: ''
        };
        this.cancel = this.cancel.bind(this);
        this.confirm = this.confirm.bind(this);
        this.changeRadio = this.changeRadio.bind(this);
        this.onTextChange = this.onTextChange.bind(this);
        this.changeCommentList = this.changeCommentList.bind(this);
    }
    componentWillReceiveProps(nextProps){
        this.setState({
            showModal: nextProps.display,
            selectedValue: nextProps.defaultSelected || 'approve',
            textValue: ''
        });
        if(JSON.stringify(nextProps.json) != "{}"){
            this.setState({json: nextProps.json, inlt: nextProps.inlt});
        }
    }
    changeRadio(value){
        this.setState({
            selectedValue: value
        });
    }
    onTextChange(value){
        if(value != this.state.textValue){
            this.setState({
                textValue: value
            });
        }
    }
    changeCommentList(obj){
        this.setState({
            textValue: obj.note
        });
    }
    cancel(){
        this.props.cancel();
    }
    confirm(){
        this.props.returnResult({
            actiontype: this.state.selectedValue,
            check_note: this.state.textValue
        });
    }
    render(){
        return (
            <NCModal show={this.state.showModal} size='lg'>
                <NCModal.Header>{this.props.title}</NCModal.Header>
                <NCModal.Body>
                    <div>
                        <NCRadio.NCRadioGroup style={{marginLeft:30}} selectedValue={this.state.selectedValue} onChange={this.changeRadio}>
                            <NCRadio value="approve">{this.state.json['10160501-000000']}</NCRadio>{/* 国际化处理： 批准*/}
                            <NCRadio value="rejecttofirst">{this.state.json['10160501-000017']}</NCRadio>{/* 国际化处理： 驳回至制单人*/}
                            <NCRadio value="nopass">{this.state.json['10160501-000001']}</NCRadio>{/* 国际化处理： 不批准*/}
                        </NCRadio.NCRadioGroup>
                        <div className="approve-comment">
                            <div className="approve-comment-text">
                                <NCTextArea 
                                    placeholder={this.state.json['10160501-000016']}/* 国际化处理： 审批意见*/
                                    showMax={true}
                                    max={200}
                                    value={this.state.textValue} 
                                    onChange={this.onTextChange} 
                                    className="comment-text"
                                />
                                <CommentList onChange={this.changeCommentList}/>
                            </div>
                        </div>
                    </div>
                </NCModal.Body>
                <NCModal.Footer>
                        <NCButton onClick={this.confirm} colors="primary">{this.state.json['10160501-000018']}</NCButton>{/* 国际化处理： 确定*/}
                        <NCButton onClick={this.cancel} style={{ marginRight: 15 }}>{this.state.json['10160501-000087']}</NCButton>{/* 国际化处理： 取消*/}
                </NCModal.Footer>
            </NCModal>
        );
    }

}
