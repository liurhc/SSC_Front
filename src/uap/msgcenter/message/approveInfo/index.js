/**
 * Created by renyjk on 2018/6/1.
 */
import React,{Component} from 'react';
import ReactDOM from 'react-dom';
//import { createPage, base, ajax } from 'nc-lightapp-front';
import { createPage, ajax, high, base, toast } from 'nc-lightapp-front';
const { ApproveComment, ApproveProcess } = high;
import UserRefer from '../../../refer/riart/userRefer';
const { NCPagination, NCTable, NCOption, NCInput, NCCheckbox, NCSelect, NCIcon, NCPopconfirm, NCDatePicker, NCBreadcrumb, NCFormControl, NCModal,NCButton } = base;
const NCBreadcrumbItem = NCBreadcrumb.NCBreadcrumbItem;
const { NCRangePicker } = NCDatePicker;
import Transfer from '../../../public/excomponents/Transfer';
import moment from "moment/moment";

import './index.less';
import '../index.less';
let getParams = function() {
    let url = location.search; //获取url中"?"符后的字串   
    let params = new Object();
    if (url.indexOf("?") != -1) {
        let str = url.substr(1);
        let strs = str.split("&");
        for (let i = 0; i < strs.length; i++) {
            params[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
        }
    }
    return params;
};
const urlParams = getParams();



let commentList =[{text:"批转假条"},{text:"同意审批"},{text:"通过批准"},{text:"审批驳回"}];

let billData = {
}

// let tableData= {
//     rows: []
// }

let tableColumns =  [

	{ title: '序号', dataIndex: 'ordernum', key: 'ordernum', width: 100 },
	{ title: '发送人', dataIndex: 'sendername', key: 'sendername', width: 200 },
	{ title: '发送日期', dataIndex: 'senddate', key: 'senddate', width: 200 },
	{ title: '审批人', dataIndex: 'approver', key: 'approver', width: 200 },
	{ title: '审批日期', dataIndex: 'dealdate', key: 'dealdate', width: 200 },
	{ title: '批语', dataIndex: 'checknote', key: 'h', width: 200 }
]


export default class ApproveProcessDemo extends Component {
    constructor (props){
		super (props);
        this.state={
			showApproveDetailModal: false,
		//	stepData1:stepData1||[],
            commentList: commentList || [],
            billData: billData.data || [],
          //  tableData: tableData || null,
			tableColumns: tableColumns || [],
			approveType: "approve",
			showApproveModal: false,
			defaultSgt: "同意",
			resumeDetail: {},
			skipCodes: {},
			resumeCode: "",
			showResumeModal: false,
			assginTreeValue:{
			},
			approveDetailModalUrl: "",
			transferApproverUser: null,
			oprType:"1",
			suggestion: "",
			wf_task_val: "",
			Children: [],
			message: {},
			showRecord: false
		}
		let _this = this;
		let binds = "approvePass closeApproveModal openRejectModal openNoPassModal openAddApproveModal"+
		" openTransferModal openResumeModal closeApproveDetailModal closeResumeModal"+
		" detailDoApprove approveAssgin transferApprove markRead markIsread addapprove"+
		" approveRefuse approveReject approvePassOrAssgin suggestChange showRejectData handleRejectChange"
        binds.split(" ").forEach(function (item) {
            _this[item] = _this[item].bind(_this);
        });
		

	}
	
//和main页面的重复  好冗余的

approvePass() {
	console.log("发送审批通过请求" + this.props.message.id);
	let _this = this;
	let msg = _this.state.message;
	//this.markIsread();
	let _data = { "billtype": msg.billtype, "billid": msg.billid, "pk_checkflow": msg.pk_detail, "skipCodes": _this.state.skipCodes["approve"] || [] };
	if (this.state.showApproveDetailModal) {
		_data["checknote"] = this.state.suggestion;
	}
	else {
		_data["checknote"] = this.state.defaultSgt;
	}

	ajax({
		url: '/nccloud/workflow/approvalcenter/approvePassAction.do',
		data: _data,
		method: 'POST',
		success: (res) => {
			if (res.data && (res.data == "200" || res.data.code == "200")) {
				toast({ color: 'success', content: '审批完成' });
				_this.state.skipCodes["approve"] = res.data.skipCodes;
				_this.closeApproveDetailModal();
				_this.closeApproveModal();
				_this.closeResumeModal();
				_this.markRead();
			}
			else if (res.data.code && res.data.code.length == 3 && res.data.code.slice(0, 1) == "3") {
				let _state = _this.state;
				_state.skipCodes["approve"] = [];
				_state.resumeDetail = res.data.detail;
				_state.resumeCode = res.data.code;
				_this.setState(_state);
				_this.openResumeModal();
			}
			else {
				toast({ color: 'danger', content: res.data.error});
			}
		}
	});
}

closeApproveModal() {
	console.log("closeApproveModal");
	this.setState({
		approveType: "approve",
		showApproveModal: false
	});
}


openRejectModal() {
	console.log("openRejectModal");
	this.setState({
		approveType: "reject",
		showApproveModal: true
	});
}

openNoPassModal() {
	console.log("openNoPassModal");
	this.setState({
		approveType: "Noapprove",
		showApproveModal: true
	});
}

openAddApproveModal() {
	console.log("openAddApproveModal");
	this.setState({
		approveType: "addapprove",
		showApproveModal: true
	});
}


openTransferModal() {
	console.log("openTransferModal");
	this.setState({
		approveType: "transfer",
		showApproveModal: true
	});
}

openResumeModal() {
	console.log("openResumeModal");
	this.setState({
		showResumeModal: true
	});
}


closeApproveDetailModal() {
	console.log("closeApproveDetailModal");
	this.setState({
		showApproveDetailModal: false
	});
}


closeResumeModal() {
	console.log("closeResumeModal");
	this.setState({
		approveType: "approve",
		skipCodes: {},
		showResumeModal: false
	});
}



detailDoApprove() {
	console.log("detailDoApprove");
	//return null;
	if (this.state.approveType == "approve") {
		//
		this.approvePass();
	}
	else if (this.state.approveType == "Noapprove") {
		//
		this.approveRefuse();
	}
	else if (this.state.approveType == "reject") {
		//
		this.approveReject();
	}
	else if (this.state.approveType == "addapprove") {
		this.addapprove();
	}
	else if (this.state.approveType == "transfer") {
		this.transferApprove()
	} else if (this.state.approveType == "assgin") {
		//
		this.approveAssgin();
	}
}


approveAssgin(){

	let _this = this;
	console.log("发送审批通过请求" + this.props.message.id);
	let msg = _this.state.message;
	//this.markIsread();
	let assginUser = [];
	if(_this.state.assginTreeValue.rightTreeData && _this.state.assginTreeValue.rightTreeData.length>0){
		for(let i = 0;i<_this.state.assginTreeValue.rightTreeData.length;i++){
			debugger;

			assginUser.push(_this.state.assginTreeValue.rightTreeData[i].refpk)
		}
	}else{
		toast({ color: 'warning', content: '请选择指派人员！' });
		return;
	}
	let _data = { "billtype": msg.billtype, "billid": msg.billid, "pk_checkflow": msg.pk_detail, "skipCodes": _this.state.skipCodes["approve"] || [],"isAssgin":_this.state.isAssgin,"assginUsers":assginUser};
   debugger;
	if (this.state.showApproveDetailModal) {
		_data["checknote"] = this.state.suggestion;
	}
	else {
		_data["checknote"] = this.state.defaultSgt;
	}

	ajax({
		url: '/nccloud/workflow/approvalcenter/approvePassAction.do',
		data: _data,
		method: 'POST',
		success: (res) => {
			if (res.data && (res.data == "200" || res.data.code == "200")) {
				toast({ color: 'success', content: '操作成功' });
				_this.closeApproveDetailModal();
				_this.closeApproveModal();
				_this.closeResumeModal();
				_this.markRead();
			}
			else if (res.data.code && res.data.code.length == 3 && res.data.code.slice(0, 1) == "3") {
				let _state = _this.state;
				_state.skipCodes["approve"] = res.data.skipCodes;
				_state.resumeDetail = res.data.detail;
				_state.resumeCode = res.data.code;
				_this.setState(_state);
				_this.openResumeModal();
			}
			else {
				toast({ color: 'danger', content: res.data.error});
			}
		}
	});

}

transferApprove() {
	debugger;
	let _this = this;
	let value = _this.state.transferApproverUser;
	let addTransferUsers = [];

	if (value) {
		toast({ color: 'danger', content: '改派人员不能为空' });
		return;
	} else {
		addTransferUsers.push(value.refpk);

	}


	//*
	ajax({
		url: '/nccloud/workflow/approvalcenter/approveTransfer.do',
		data: { "pk_checkflow": _this.state.message.pk_detail, "pk_message": _this.state.message.pk_message, "billtype": _this.state.message.billtype, "billid": _this.state.message.billid, "trans_user": addTransferUsers, "check_note": _this.state.suggestion },
		method: 'POST',
		success: (res) => {
			if (res.data) {
				if (res.data && (res.data == "200" || res.data.code == "200")) {
					toast({ color: 'success',content: '操作成功' });
					_this.closeApproveDetailModal();
					_this.closeApproveModal();
					_this.markRead();
				}
			}
		}
	});

}

markRead() {
	let _this = this;
	ajax({
		url: '/nccloud/riart/message/setRead.do',
		data: { "pk_message": _this.state.message.id, msgType: _this.props.pageMsgType, isread: "Y" },
		method: 'POST',
		success: (res) => {
			//let data = res.data;
			_this.markIsread();
		}
	});
}


markIsread(e) {
	this.props.markIsread(this.props.message.id);
	let msg = this.state.message;
	msg.isread = "Y";
	this.setState({ message: msg });
}


addapprove() {
	let _this = this;
	let value = _this.state.addApproverUsers;
	let addApproveUsers = [];
	if (addApproveUsers || addApproveUsers.length == 0) {
		toast({ color: 'warning', content: '请选择加签用户' });
		return;
	}
	debugger;
	if (value && value.length > 0) {
		for (let i = 0; i < value.length; i++) {
			let cdata = value[i].refpk;
			addApproveUsers.push(cdata)
		}
	}
	//*
	ajax({
		url: '/nccloud/workflow/approvalcenter/addApprove.do',
		data: { "pk_checkflow": _this.state.message.pk_detail, "pk_message": _this.state.message.pk_message, "billtype": _this.state.message.billtype, "billid": _this.state.message.billid, "approve_users": addApproveUsers, "check_note": _this.state.suggestion },
		method: 'POST',
		success: (res) => {
			if (res.data) {
				if (res.data && (res.data == "200" || res.data.code == "200")) {
					toast({ color: 'success',content: '操作成功' });
					_this.closeApproveDetailModal();
					_this.closeApproveModal();
				}
			}
		}
	});
}



approveRefuse() {
	console.log("发送审批拒绝请求" + this.props.message.id);
	let _this = this;
	let msg = _this.state.message;
	//this.markIsread();
	let _data = { "billtype": msg.billtype, "billid": msg.billid, "pk_checkflow": msg.pk_detail, "skipCodes": _this.state.skipCodes["Noapprove"] || [] };
	//if (this.state.showApproveDetailModal) {
	_data["check_note"] = this.state.suggestion;
	//}

	//return null;
	ajax({
		url: '/nccloud/workflow/approvalcenter/approveNoPassAction.do',
		data: _data,
		method: 'POST',
		success: (res) => {
			if (res.data && (res.data == "200" || res.data.code == "200")) {
				toast({ color: 'success',content: '操作成功' });
				_this.closeApproveDetailModal();
				_this.closeApproveModal();
				_this.markRead();
			}
			else {
				toast({ color: 'danger', content: res.data.error});
			}
		}
	});
}

approveReject() {
	debugger;
	console.log("发送审批驳回请求" + this.props.message.id);
	let _this = this;
	let msg = _this.state.message;
	//this.markIsread();
	let _data = { "billtype": msg.billtype, "billid": msg.billid, "pk_checkflow": msg.pk_detail, "pk_wf_task": this.state.wf_task_val, "skipCodes": _this.state.skipCodes["reject"] || [] };
	//if (this.state.showApproveDetailModal) {
	_data["checknote"] = this.state.suggestion;
	//}

	ajax({
		url: '/nccloud/workflow/approvalcenter/approveRejectAction.do',
		data: _data,
		method: 'POST',
		success: (res) => {
			if (res.data && (res.data == "200" || res.data.code == "200")) {
				toast({ color: 'success',content: '操作成功' });
				_this.closeApproveDetailModal();
				_this.closeApproveModal();
				_this.markRead();
			}
			else {
				toast({ color: 'danger', content: res.data.error});
			}
		}
	});
}



approvePassOrAssgin(){
    let _this = this;
    let msg = _this.state.message;

    ajax({
        url: '/nccloud/workflow/approvalcenter/isCanAssignQuery.do',
        data: { "billtype": msg.billtype, "billid": msg.billid, "pk_checkflow": msg.pk_detail},
        method: 'POST',
        success: (res) => {
            debugger;
            if (res.data && (res.data == "410" || res.data.code == "410")) {
				toast({ color: 'success', content: '获取指派人员成功' });
                _this.state.isAssgin = true;
                debugger;

                _this.setState({
                    assginTreeValue : {
                        leftTreeData : res.data.leftdata,
                        rightTreeData : null
                    },
                    approveType: "assgin",
                    showApproveModal: true
                });
            }else if(res.data && (res.data == "500" || res.data.code == "500")){
                    toast({ color: 'danger', content: res.data.error});
            }else{
                _this.approvePass()
            }
        }
    });
  }


    //输入意见输入框
    suggestChange(val) {
        this.setState({
            suggestion: val
        });
    }

	
    showRejectData() {
		let _this = this;
		debugger;
        if (_this.state.approveType != "reject") return;
        let msg = _this.state.message;
        if (_this.state.Children.length > 0) return;
        //请求数据
        ajax({
            url: '/nccloud/workflow/approvalcenter/worklownoteHistoryQuery.do',
            data: { "billtype": msg.billtype, "billid": msg.billid },
            method: 'POST',
            success: (res) => {
                let children = [];
                if (res.data.rows.rows && res.data.rows.rows.length > 0) {
                    for (let i = 0; i < res.data.rows.rows.length; i++) {
                        let cdata = res.data.rows.rows[i].values;
                        children.push(<NCOption key={cdata.pk_wf_task.value}>{cdata.checkname.value}审批</NCOption>);
                    }
                }

                this.setState({
                    Children: children
                });
            }
        });
    }


    handleRejectChange = (e) => {
        this.setState({ wf_task_val: e });
    }

//后面有时间抽出来

    render() {
		this.state.message = this.props.message;
//------------

let modalTitle = "";
let modalBody = "";
if (this.state.approveType == "assgin") {
	modalTitle = "选择指派";
	modalBody =
		(<NCModal.Body >
			<div className="body-content">
				<div className="content-inner">
					<div className="label" style={{ width: 90 }}>选择指派人员</div>
					<div className="content" style={{ width: 200 }}>
						<Transfer
							TransferId={'org_transferid'}
							leftTreeData={this.state.assginTreeValue.leftTreeData}
							value={this.state.assginTreeValue}
							oprType={this.state.oprType}
						/>
					</div>
				</div>
				<div className="content-inner">
					<div className="label">输入批语</div>
					<div className="content">
						<NCFormControl
							className="demo-input"
							value={this.state.suggestion}
							onChange={this.suggestChange}
						/>
					</div>
				</div>
			</div>
		</NCModal.Body>);
}
else if (this.state.approveType == "Noapprove") {
	modalTitle = "不批准";
	modalBody = (
		<NCModal.Body >
			<div className="body-content">
				<div className="content-inner">
					<div className="label">输入批语</div>
					<div className="content">
						<NCFormControl
							className="demo-input"
							value={this.state.suggestion}
							onChange={this.suggestChange}
						/>
					</div>
				</div>
			</div>
		</NCModal.Body>);
}
else if (this.state.approveType == "reject") {
	debugger;
	modalTitle = "驳回";
	modalBody =
		(<NCModal.Body >
			<div className="body-content">
				<div className="content-inner">
					<div className="label" style={{ width: 90 }}>选择驳回环节</div>
					<div className="content">
						<NCSelect
							defaultValue="请选择驳回环节"
							style={{ width: 200 }}
							onChange={this.handleRejectChange}
						>
							{this.state.Children}
						</NCSelect>
					</div>
				</div>
				<div className="content-inner">
					<div className="label">输入批语</div>
					<div className="content">
						<NCFormControl
							className="demo-input"
							value={this.state.suggestion}
							onChange={this.suggestChange}
						/>
					</div>
				</div>
			</div>
		</NCModal.Body>);
}
else if (this.state.approveType == "addapprove") {
	modalTitle = "加签";
	modalBody = (<NCModal.Body >
		<div className="body-content">
			<div className="content-inner">
				<div className="label" style={{ width: 90 }}>选择加签人</div>
				<div className="content">
					<div style={{ width: 200 }}>
						<UserRefer
							value={_this.state.addApproverUsers}
							onChange={_this.setAddApprover}
							isMultiSelectedEnabled={true}
							placeholder="加签人"
							queryCondition={{
								isMutiGroup:false,
								isShowGroupAdmin:false,
								isShowSysAdmin:false,
								isAuthFilter:false,
								isAllUserVisible:false,
								isShareUserVisible:false,
								isSelfVisible:false,
								isNeedNCUser:false,
								adminoption:'USE' 
								}}
						/>
					</div>
				</div>
			</div>
			<div className="content-inner">
				<div className="label">输入批语</div>
				<div className="content">
					<NCFormControl
						className="demo-input"
						value={this.state.suggestion}
						onChange={this.suggestChange}
					/>
				</div>
			</div>
		</div>
	</NCModal.Body>);
}
else if (this.state.approveType == "transfer") {
	modalTitle = "改派";
	modalBody =
		(<NCModal.Body >
			<div className="body-content">
				<div className="content-inner">
					<div className="label" style={{ width: 90 }}>选择改派人员</div>
					<div className="content">
						<div style={{ width: 200 }}>
							<UserRefer
								value={_this.state.transferApproverUser}
								onChange={_this.setTransferApprover}
								isMultiSelectedEnabled={false}
								placeholder="加签人"
								queryCondition={{
									isMutiGroup:false,
									isShowGroupAdmin:false,
									isShowSysAdmin:false,
									isAuthFilter:false,
									isAllUserVisible:false,
									isShareUserVisible:false,
									isSelfVisible:false,
									isNeedNCUser:false,
									adminoption:'USE' 
									}}
							/>
						</div>
					</div>
				</div>
				<div className="content-inner">
					<div className="label">输入批语</div>
					<div className="content">
						<NCFormControl
							className="demo-input"
							value={this.state.suggestion}
							onChange={this.suggestChange}
						/>
					</div>
				</div>
			</div>
		</NCModal.Body>);
}








//-----------

		const {  commentList,billData, tableColumns } = this.state;
		const {stepData,tabledata} = this.props;

		if(this.props.tabledata!=null && this.props.tabledata.length>0){
		
		}else{
		
			this.props.tabledata = [];
		}

		if(this.props.enableActions!=null && this.props.enableActions.length>0){
		
		}else{
		
			this.props.enableActions = [];
		}
		let _this = this;
		let enableActions = this.props.enableActions;

        return (
            <div id="finance-reva-revecontract-card" className="nc-bill-extCard"> 
               <div className="approve-bill-detail">
                    <div className="steps">
                        <ApproveProcess 
                            className="outer-steps" 
                            data={stepData}
                        >
                        </ApproveProcess>
                    </div>
                    <div className="comment">
                        <ApproveComment
                            className="outer-comment"
                            commentList = {commentList}
                            dagreData = {billData}
                            tableColumns = {tableColumns}
							tableData = {tabledata}
							bottomBtns = {(comment,track)=>{
								return <span>
    					 	{(enableActions || []).filter(function (item, idx, origin) { return item == "pass"; }).length > 0 ?
									<NCButton colors="primary" onClick={()=>{
										this.approvePassOrAssgin();
									}}>批准</NCButton> : ""}
                            {(enableActions || []).filter(function (item, idx, origin) { return item == "nopass"; }).length > 0 ?
                                	<NCButton onClick={()=>{
										this.openNoPassModal();	
									}}>不批准</NCButton> : ""}
                            {(enableActions || []).filter(function (item, idx, origin) { return item == "reject"; }).length > 0 ?
                               	<NCButton onClick={()=>{
									this.openRejectModal()
								}}>驳回</NCButton>: ""}
                            {(enableActions || []).filter(function (item, idx, origin) { return item == "addapprover"; }).length > 0 ?
                              	<NCButton onClick={()=>{
									this.openAddApproveModal()
								}}>加签</NCButton> : ""}
                            {/* {(enableActions || []).filter(function (item, idx, origin) { return item == "assign"; }).length > 0 ?
                                <NCButton colors="primary" onClick={this.openAssignModal}>指派</NCButton> : ""} */}
                            {(enableActions || []).filter(function (item, idx, origin) { return item == "transfer"; }).length > 0 ?
                                	<NCButton onClick={()=>{
									this.openTransferModal()
									}}>改派</NCButton> : ""}



									{/* <NCButton onClick={()=>{
										console.log(comment,track)
									}}>批准</NCButton> */}
									{/* <NCButton>驳回</NCButton>
									<NCButton>不批准</NCButton> */}
									<NCButton>附件</NCButton>
								</span>
							}}
							onChangeOpinions = {(data)=>{
								console.log(data)
								return [{text:"批转假条1"},{text:"同意审批2"},{text:"通过批准3"},{text:"审批驳回4"}]
							}}
                        />
                    </div>
                </div>
                {/* <div className="approve-bill-record">
					<div className="record-header">
						<span onClick={() => {
							this.setState({
								showRecord: !this.state.showRecord
							})
						}}>审批记录</span>
						<i className={`icon iconfont ${this.state.showRecord ? "icon-jiantouxia1" : "icon-jiantouyou"}`}></i>
					</div>
					<div className={`record-content ${!this.state.showRecord ? "hide" : ""}`}>
						<NCTable 
							columns={tableColumns} 
							data={this.props.tabledata}
						></NCTable>
					</div>
                </div> */}


	
				<NCModal
                    className="form-modal"
                    size="xlg"
                    show={this.state.showApproveDetailModal}
                    backdrop={true}
                    onHide={this.closeApproveDetailModal}>
                    <NCModal.Header closeButton>
                        <NCModal.Title > 审批详情 </NCModal.Title>
                    </NCModal.Header >

                    <NCModal.Body >
                        {/* {this.createApprove()} */}
                        <div>
                            {/* iframe src={this.state.approveDetailModalUrl} */}
                            <iframe src={this.state.approveDetailModalUrl} style={{ width: "100%" }}></iframe>
                            {/* <iframe src="/uap/imp/msgTempType/main/index.html"></iframe> */}
                        </div>
                    </NCModal.Body>
                    <NCModal.Footer>
                        <NCButton colors="primary" onClick={this.detailDoApprove}> 确定 </NCButton>
                        <NCButton className="btn-transparent" onClick={this.closeApproveDetailModal}> 取消 </NCButton>
                    </NCModal.Footer>
                </NCModal>

                <NCModal
                    className="form-modal"
                    size={this.state.resumeDetail && this.state.resumeDetail.size ? this.state.resumeDetail.size : "lg"}
                    show={this.state.showResumeModal}
                    backdrop={true}
                    onHide={this.closeResumeModal}>
                    <NCModal.Header closeButton>
                        <NCModal.Title > 审批警告 </NCModal.Title>
                    </NCModal.Header >

                    <NCModal.Body >
                        {this.state.resumeDetail && this.state.resumeDetail.html ? <div dangerouslySetInnerHTML={resumeHtml}></div> : ""}
                        {this.state.resumeDetail && this.state.resumeDetail.url ? <iframe src={this.state.resumeDetail.url} style={{ width: "100%" }}></iframe> : ""}
                    </NCModal.Body>
                    <NCModal.Footer>
                        {this.state.resumeCode == "313" ? "" :
                            <NCButton colors="primary" onClick={this.detailDoApprove}> 确定 </NCButton>}
                            <NCButton className="btn-transparent" onClick={this.closeResumeModal}> 关闭 </NCButton>
                    </NCModal.Footer>
                </NCModal>

                <NCModal
                    className="form-modal"
                    onShow={this.showRejectData}
                    show={this.state.showApproveModal}
                    backdrop={true}
                    onHide={this.closeApproveModal}>
                    <NCModal.Header closeButton>
                        <NCModal.Title > {modalTitle} </NCModal.Title>
                    </NCModal.Header >
                    {modalBody}
                    <NCModal.Footer>
                        <NCButton colors="primary" onClick={this.detailDoApprove}> 确定</NCButton>
                        <NCButton className="btn-transparent" onClick={this.closeApproveModal}> 关闭 </NCButton>
                    </NCModal.Footer>
                </NCModal>
            </div>
        )
    }
}




