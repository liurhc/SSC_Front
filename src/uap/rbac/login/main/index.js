import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {cacheTools, ajax, Cipher } from 'platform-login'; // add Cipher by bbqin
import './index.less';
import './psw.less';
import {Modal} from "./modal";
import CreateAlert from "./alert";
import RSAUtils from "../rsa/security.js";
import {Select,Button,Step,CheckBox } from 'tinper-bee';
import {getStoreLang,InitMultiLang,getMultiLang,getStoreBc} from "./lang"
const NCSelect = Select;
const NCButton = Button;
const NCCheckbox = CheckBox;
const NCStep = Step;
const NCOption = NCSelect.Option;
const NCSteps = NCStep.Steps;
// 增加加密操作 bbqin
const { opaqueEncrypt } = Cipher;
class Main extends Component {
    constructor(props) {
        super(props);
        let userid = this.getCookie("userloginid");
        if(userid==""||userid=="null"||userid==undefined||userid=="undefined"){
            this.state = {
                showChangePsw: false,
                step:0,
                forgetpsw:false,
                mobileFindPsw:false,
                forgetstep:0,
                mobileFindPswStep:0,
                checked:false,
                selectTypeChangePsw:false
            }
        }else{
            this.state = {
                showChangePsw: false,
                step:0,
                forgetpsw:false,
                mobileFindPsw:false,
                forgetstep:0,
                mobileFindPswStep:0,
                checked:true,
                selectTypeChangePsw:false
            } 
        }
           
    }
    
    componentDidMount() { 
        String.prototype.insert = function (index, item) {
            var temp = [];
            for (var i = 0; i < this.length; i++) {
                temp.push(this[i]);
            }
            temp.splice(index, 0, item);
            return temp.join("")
        };       
        let userid = this.getCookie("userloginid");
        if(userid==""||userid=="null"||userid==undefined||userid=="undefined"){

        }else{
                document.getElementById('username').value=userid;
        }
        if(this.props.data.codeVerfiy)
            this.randImg();
    }
    getCookie = (sName) => {
        var sRE = "(?:; )?" + sName + "=([^;]*);?";
        var oRE = new RegExp(sRE);
    
        if (oRE.test(document.cookie)) {
            return RegExp["$1"];
        } else
            return null;
    };
    setCookie = (sName, sValue) => {
        var date=new Date(); 
        date.setTime(date.getTime()+365*24*60*60*1000);
        var sCookie = sName + "=" + sValue+";expires="+date.toGMTString()+";path=/";        
        document.cookie = sCookie;
    };
    changeCheck=()=> {
        this.setState({checked:!this.state.checked});
        if(!this.state.checked==false)
            this.setCookie("userloginid","");
    }
    initOption = (type,data) =>{
        let body=[];
        data.map((option,i) => (
            body.push(<NCOption className={"login-"+type+"-option"} value={option.code}>{option.name}</NCOption>)
        ));
        return body;
    }
    onSelect = (type,data) =>{
        if(type==="bc"){
            //this.loginprops.busiCenterCode = data;
            this.setCookie("busiCenterCode",data);
            window.location.reload();
        }
        if(type==="lang"){
            this.setCookie("langCode",data);
            window.location.reload();
        }           
    }

    onSubmit = ()=>{        
        this.loginprops.timezone = new Date().toString().substring(25,33).insert(6,":");
        this.loginprops.userCode = document.getElementById('username').value;
        this.loginprops.userPWD = document.getElementById('password').value;
        if(this.loginprops.codeVerfiy){
            this.loginprops.rangImg = document.getElementById("rand").value;
        }
        if(!this.loginprops.exponent){
            document.getElementById('login-error-msg').innerText = getMultiLang("00043","安全日志数据源异常，请联系环境管理员处理");
            document.getElementById('login-error-msg').title = getMultiLang("00043","安全日志数据源异常，请联系环境管理员处理");
        }
        var key = RSAUtils.getKeyPair(this.loginprops.exponent, '', this.loginprops.modulus);
		this.loginprops.userPWD = RSAUtils.encryptedString(key, this.loginprops.userPWD);
        if(this.state.checked==true){
            this.setCookie("userloginid",this.loginprops.userCode);
        }
        var _this = this;
        // add  by bbqin
        function uuidv4() {
            return 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                var r = (Math.random() * 16) | 0,
                    v = c == 'x' ? r : (r & 0x3) | 0x8;
                return v.toString(16);
            });
        }
        let aeskeyCahe = uuidv4();
        let aeskey = RSAUtils.encryptedString(key, aeskeyCahe);
        this.loginprops.aeskey = aeskey;
        localStorage.setItem('cowboy', opaqueEncrypt(aeskeyCahe));
        // 重置 aes 请求 --bbqin
        localStorage.setItem('rockin', false);
        // ------ end ------

        ajax({
            url: '/nccloud/riart/login/verfiy.do',
            data:_this.loginprops,        
            success: function (res) {
                let { success, data } = res;
                if (success) {
                    let rslCode = data.rslCode;
                
                    // 发一个同步ajax --bbqin
                    ajax({
                        type: 'post',
                        url: '/nccloud/platform/aes/switch.do',
                        data: { 
                            sysParamJson: {
                                busiaction: '查询请求aes加密开关' 
                            }
                        },
                        async: false,
                        success: function(asd) { // aesSwitchData
                            asd = typeof asd === 'string' ? JSON.parse(asd) : asd;
                            if (asd.data) {
                                if (asd.data.success || asd.success) {
                                    if (asd.data) {
                                        localStorage.setItem('rockin', true);
                                    } else {
                                        localStorage.setItem('rockin', false);
                                    }
                                }
                            }
                        }
                    });
                    if(rslCode=="0"){
                        window.location = "/"+data.index;
                        //window.location = "/uap/rbac/login/test/index.html";
                    }
                    if(_this.props.codeVerfiy)
                        _this.randImg();
                    if(rslCode=="1"){
                        document.getElementById('login-error-msg').innerText=data.rslMsg;    
                        document.getElementById('login-error-msg').title = data.rslMsg;
                    }
                    if(rslCode=="2"){
                        _this.onChangePsw();
                        _this.loginprops.dsName = data.dsName;
                        document.getElementById("tips").innerText=data.rslMsg;
                        document.getElementById("newpsw").focus();
                        
                    }
                    if(rslCode=="3"){
                        CreateAlert(data.rslMsg,function(){
                            _this.loginprops.notips = 1;
                            _this.onSubmit();
                        });
                    }
                    if(rslCode=="4"){                        
                        let chllid = data.chllid;
                        try{                            
                            let snid = sn(_this.loginprops.userCode,true); 
                            let signdata = sign(chllid,_this.loginprops.userCode,true,true);
                            _this.loginprops.p_sn = snid;
                            _this.loginprops.p_signdata = signdata;
                            _this.onSubmit();
                        }catch(e){
                            alert(e);
                            return;
                        }   
                    }
                    if(rslCode=="5"){                        
                        CreateAlert(data.rslMsg,function(){
                            _this.loginprops.forcelogin = 1;
                            _this.onSubmit();
                        },getMultiLang);
                    }  
                    if(rslCode=="-1"){
                       
                        document.getElementById('login-error-msg').innerText = data.msg;
                        document.getElementById('login-error-msg').title = data.rslMsg;
                    }                  
                }
            }
        })
    }

    onChangePsw = () =>{
        this.setState({ showChangePsw: true });
    }
    forgetPsw = () =>{
        this.closeSelectTypeChangePsw();
        this.setState({ forgetpsw: true });
    }  
    close= () => {
        this.setState({
            showChangePsw: false,
            step:0
        });
    }
    onNext = () =>{
        this.setState({
            step:this.state.step+1
        });
    }
    onBefore = () =>{
        this.setState({
            step:this.state.step-1<0?0:this.state.step-1
        });
    }
    toClose = (time) =>{
        this.onNext();
        let self = this;
        setTimeout(() => {
            self.close();
          }, time);
    }
    closeForgetPsw= () => {
        this.setState({
            forgetpsw: false,
            forgetstep:0
        });
        this.clickForgetPwdBtn();
    }
    onNextForgetPsw = () =>{
        document.getElementById("psw-error-msg").innerText="";
        this.setState({
            forgetstep:this.state.forgetstep+1
        });
    }
    onBeforeForgetPsw = () =>{
        this.setState({
            forgetstep:this.state.forgetstep-1<0?0:this.state.forgetstep-1
        });
    }
    toGetResetCode = () =>{
        let op = {};
        op.bc = this.loginprops.busiCenterCode;
        op.userCode = document.getElementById("pwduser").value;
        op.contact = document.getElementById("pwdemail").value;
        op.type = '1';
        let _this = this;
        op.langCode = this.loginprops.langCode;
        // 重置 aes 请求 --bbqin
        localStorage.setItem('rockin', false);
        
        ajax({
            url: '/nccloud/riart/login/forpsw.do',
            data:op,        
            success: function (res) {
                let { success, data } = res;
                if (success) {
                   if(data.status=="0"){
                    _this.loginprops.fkey = data.content;
                    _this.onNextForgetPsw();
                    _this.clearText();
                   }else{
                    document.getElementById("psw-error-msg").innerText = data.content;
                   }
                }
            }
        })
    }
    clearText = () =>{
        document.getElementById("pwd1").value = "";
        document.getElementById("pwd2").value = "";
        document.getElementById("pwdverfiy").value = "";
    }
    toVerfiyAndReset = (time) =>{
        document.getElementById("psw-error-msg").innerText="";
        if(document.getElementById("pwd1").value!=document.getElementById("pwd2").value){
            document.getElementById("psw-error-msg").innerText = getMultiLang("00003","两次密码不一致");
            return;
        }
        let _this = this;
        let op = {};
        op.key = this.loginprops.fkey;
        op.identifyCode = document.getElementById("pwdverfiy").value;
        var key = RSAUtils.getKeyPair(this.loginprops.exponent, '', this.loginprops.modulus);
        op.psw = RSAUtils.encryptedString(key, document.getElementById("pwd1").value);
        op.langCode = this.loginprops.langCode;
        op.type = "1";
        ajax({
            url: '/nccloud/riart/login/toreset.do',
            data:op,        
            success: function (res) {
                console.log(res);
                let { success, data } = res;
                if (success) {
                   if(data.status=="0"){
                    _this.onNextForgetPsw();
                        setTimeout(() => {
                            _this.closeForgetPsw();
                            _this.closeSelectTypeChangePsw();
                      }, "5000");
                   }else{
                        document.getElementById("psw-error-msg").innerText = data.content;
                   }
                }
            }
        })
    }
    onEnter = (e) =>{
        if(e.which==13){
            if(e.target.id=="username"){
                this.refs.password.focus();
            }
            if(e.target.id=="password"){
                if(this.props.data.codeVerfiy)
                    this.refs.rand.focus()
                else
                    this.onSubmit();
            }
            if(e.target.id=="rand"){
                this.onSubmit();
            }
        }else if(e.target.id=="username"){
            let userCode = document.getElementById('username').value;
            if(userCode==""){
                document.getElementById('password').value="";
            }
        }
    }
    onEnterResetPwd = (e) => {
        if(e.which==13){
            let id = e.target.id;
            if(id == 'newpsw') {
                document.getElementById("newpsw1").focus();
            }else if(id == 'newpsw1') {
                document.getElementById('reset-psd-confirm').focus();
            }
        }
    }
    onReset = () => {
        
        let self = this;
        document.getElementById("psw-error-msg").innerText = "";
        let op = {};
        op.lodpsw = this.loginprops.userPWD
        op.newpsw = document.getElementById("newpsw").value;
        let newpsw1 = document.getElementById("newpsw1").value;
        if(op.newpsw!=newpsw1){
            document.getElementById("psw-error-msg").innerText = getMultiLang("00003","两次密码不一致");
            return;
        }
        let key = RSAUtils.getKeyPair(this.loginprops.exponent, '', this.loginprops.modulus);
        //op.lodpsw = RSAUtils.encryptedString(key,op.lodpsw);
        op.dsName = this.loginprops.dsName;
        op.userCode = this.loginprops.userCode;
        op.newpsw = RSAUtils.encryptedString(key,op.newpsw);
        op.langCode = this.loginprops.langCode;
        ajax({
            url: '/nccloud/login/psw/reset.do',
            data:op,        
            success: function (res) {
                console.log(res);
                let { success, data } = res;
                if (success) {
                    if(data=="1"){
                        document.getElementById("psw-error-msg").innerText = getMultiLang("00030","修改失败请联系管理员！");
                    }else if(data=="0"){
                        self.toClose("5000");
                        self.loginprops.userPWD = document.getElementById('password').value = "";                        
                    }else{
                        document.getElementById("psw-error-msg").innerText = data;
                    }
                }
            }
        })
    }
    hideVersionCheck = () =>{
        document.getElementById("checkVersion").style.display="none";
    }
    showVersionTips = () =>{
        let arr = navigator.userAgent.split(' '); 
        let chromeVersion = '';
        for(let i=0;i < arr.length;i++){
            if(/chrome/i.test(arr[i]))
            chromeVersion = arr[i]
        }
        if(chromeVersion){
            let version = Number(chromeVersion.split('/')[1].split('.')[0]);
            if(version&&version>43){
                return;
            }
        }
        let i=0;
        let myVal=setInterval(function(){
            document.getElementById("checkVersion").style.top=i+"px";
            if(i==60){
                clearInterval(myVal);
            }
            i++;  
        },8 );
    }
    clearMsg = () =>{
        document.getElementById("login-error-msg").innerText="";
    }
    randImg = ()=>{
        document.getElementById("rand").value="";
        ajax({
            url: '/nccloud/riart/login/rand.do',      
            success: function (res) {
                let { success, data } = res;
                if (success) {         
                    document.getElementById("randImg").src="data:image/png;base64,"+data;
                }
            }
        })
    }
    clickForgetPwdBtn = () =>{
        this.setState({selectTypeChangePsw:true})
    }
    closeSelectTypeChangePsw = () =>{
        this.setState({selectTypeChangePsw:false})
    }
    // 手机找回密码 start
    mobileFindPsw = () =>{
        this.closeSelectTypeChangePsw();
        this.setState({ mobileFindPsw: true });
    }
    closeMobileFindPsw= () => {
        this.setState({
            mobileFindPsw: false,
            mobileFindPswStep:0
        });
        this.clickForgetPwdBtn();
    }
    mobileFindPswRandImg = () =>{
        document.getElementById("mobilepicvercode").value="";
        this.picVerCodeKey = new Date().getTime();
        document.getElementById("mobilefindpwdrandImg").src = this.yhtRandImgUrl + this.picVerCodeKey;
    }
    toMobileFindPwdSendCode = () =>{
        let op = {};
        op.bc = this.loginprops.busiCenterCode;
        op.userCode = document.getElementById("mobilepwduser").value;
        op.contact = document.getElementById("mobilepwdnum").value;
        op.type = '2';
        op.picVerCode = document.getElementById("mobilepicvercode").value;
        op.picVerCodeKey = this.picVerCodeKey + "";
        let _this = this;
        op.langCode = this.loginprops.langCode;
	// 重置 aes 请求 --bbqin
        localStorage.setItem('rockin', false);
        ajax({
            url: '/nccloud/riart/login/forpsw.do',
            data:op,        
            success: function (res) {
                let { success, data } = res;
                if (success) {
                   if(data.status=="0"){
                    _this.loginprops.fkey = data.content;
                    _this.onNextMobileFindPsw();
                    _this.onMobileFindPwdOneStepClearText();
                   }else{
                    document.getElementById("mobile-find-psw-error-msg").innerText = data.content;
                    // 如果是验证码错误，则刷新验证码
                    if(data.status=="0481") {
                        _this.mobileFindPswRandImg();
                    }
                   }
                }
            }
        })
    }
    onMobileFindPwdOneStepClearText = () =>{
        document.getElementById("m-find-pwd-pwd1").value = "";
        document.getElementById("m-find-pwd-pwd2").value = "";
        document.getElementById("m-find-pwd-pwdverfiy").value = "";
    }
    onNextMobileFindPsw = () =>{
        document.getElementById("mobile-find-psw-error-msg").innerText="";
        this.setState({
            mobileFindPswStep:this.state.mobileFindPswStep+1
        });
    }
    onBeforeMobileFindPsw = () =>{
        this.setState({
            mobileFindPswStep:this.state.mobileFindPswStep-1<0?0:this.state.mobileFindPswStep-1
        });
    }
    toMobileVerfiyAndReset = (time) =>{
        document.getElementById("mobile-find-psw-error-msg").innerText="";
        if(document.getElementById("m-find-pwd-pwd1").value!=document.getElementById("m-find-pwd-pwd2").value){
            document.getElementById("mobile-find-psw-error-msg").innerText = getMultiLang("00003","两次密码不一致");
            return;
        }
        let _this = this;
        let op = {};
        op.key = this.loginprops.fkey;
        op.identifyCode = document.getElementById("m-find-pwd-pwdverfiy").value;
        var key = RSAUtils.getKeyPair(this.loginprops.exponent, '', this.loginprops.modulus);
        op.psw = RSAUtils.encryptedString(key, document.getElementById("m-find-pwd-pwd1").value);
        op.langCode = this.loginprops.langCode;
        op.type = "2";
        ajax({
            url: '/nccloud/riart/login/toreset.do',
            data:op,        
            success: function (res) {
                console.log(res);
                let { success, data } = res;
                if (success) {
                   if(data.status=="0"){
                    _this.onNextMobileFindPsw();
                        setTimeout(() => {
                            _this.closeMobileFindPsw();
                            _this.closeSelectTypeChangePsw();
                      }, "5000");
                   }else{
                        document.getElementById("mobile-find-psw-error-msg").innerText = data.content;
                   }
                }
            }
        })
    }
    // 手机找回密码 end  
    render() {
        let {data,busiCenterCode,langCode} = this.props;   
        data.busiCenterCode = busiCenterCode;
        data.langCode = langCode;
        let {bc,lang,modulus,exponent,codeVerfiy,yhtRandImgUrl} = data;
        this.loginprops =data;
        this.yhtRandImgUrl = yhtRandImgUrl;
        this.picVerCodeKey= new Date().getTime();
        let m_top = codeVerfiy?"45px":"55px";
        let m_bottom = codeVerfiy?"30px":"60px";
        let img2=codeVerfiy?<div className="randDiv"><input ref="rand" onKeyDown={this.onEnter} id="rand" placeholder={getMultiLang("00031","验证码")}/><img onClick={this.randImg} id="randImg"/></div>:"";
        let img3="../../../public/img/lang/"+langCode+"/login-center.png";
        return (
            <div className="login-main">
                <div className="login-top">
                    <div className="login-logo">
                        <div className="logo-img"><img src="../../../public/img/logo.png"/></div>                                          
                    </div>
                    <div className="login-lang">
                        <NCSelect id="busicenter" onSelect={this.onSelect.bind(this.value,"lang")} defaultValue={langCode} className="langcode">
                            {this.initOption("lang",lang)}
                        </NCSelect>
                    </div>
                </div>
                <div id="checkVersion" >
			        <i  class="iconfont attention icon-zhuyi1"></i>		
		            &nbsp;{getMultiLang("00001","建议使用Chrome谷歌浏览器43+,以获得最佳体验。")}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		            <i onClick={this.hideVersionCheck} class="iconfont close icon-guanbi"></i>
                </div>
                <div className="login-content">
                    <div className="content-left"></div>
                    <div className="content-center"><img src={img3}/></div>    
                    <div className="content-right">
                        <div className="login-panel-out">
                            <div  className="login-panel" style={{paddingTop:m_top,paddingBottom:m_bottom}}>
                                <NCSelect field="nc-select_combobox" fieldname="下拉框" id="busicenter" onSelect={this.onSelect.bind(this.value,"bc")} defaultValue={busiCenterCode}>
                                        {this.initOption("bc",bc)}
                                </NCSelect>
                                <input style={{display:"none"}} type="text" name="fakeusernameremembered"/>
                                <input style={{display:"none"}} type="password" name="fakepasswordremembered"/>
                                <input name="fakeusernameremembered" field="username"  onFocus={this.clearMsg} fieldname="用户名" id="username" onKeyDown={this.onEnter}    className="u-form-control text" type="text" placeholder="请输入OA账号"/>
                                
                                <input name="fakepasswordremembered" field="password" onFocus={this.clearMsg} ref="password" fieldname="密码" id="password" onKeyDown={this.onEnter}   className="u-form-control text" type="password" placeholder={getMultiLang("00004","密码")}/>
                                
                                {img2}
                                <div className="login-error2-msg" id="login-error-msg"></div>
                                <div className="login-btn">
                                    <NCButton field="loginBtn" fieldname="登陆" id="loginBtn" onClick = {this.onSubmit} className="btn" colors="danger">{getMultiLang("00007","登录")}</NCButton>
                                </div>
                                <div className="panel-oper">
                                <NCCheckbox field="remember" fieldname="记住账号"  onChange={this.changeCheck} checked={this.state.checked} className="inbox">{getMultiLang("00005","记住账号")}</NCCheckbox>
                                <a field="forget" fieldname="忘记密码" onClick={this.clickForgetPwdBtn} className="oper-ps" href="#">{getMultiLang("00006","忘记密码")}?</a>
                                </div>
                            </div>
                        </div>
                        
                    </div>                
                </div>
                
                <div className="login-bottom">Copyright &#169;{getMultiLang("00008","2018用友网络科技股份有限公司版权所有")}</div>
                <Modal visible={this.state.showChangePsw}  step={this.state.step}>
                    <div className="login-tabs">
                    <Modal.Title onClose={this.close}>
                    {getMultiLang("00009","重置密码")}
                    </Modal.Title>
                    <Modal.Content className="reset-psw">
                        <div id="tips" className="tips">{getMultiLang("00010","为保证账户安全,首次登陆需修改初始密码,重新设置密码")}</div>
                        <div className="psw-error-msg psw-reset-msg" id="psw-error-msg"></div>                        
                        <div className="psw-p">
                            <div className="psw-p-label">{getMultiLang("00011","新密码")}</div>
                            <div className="psw-p-input">
                                <input className="u-form-control text" id="newpsw" type="password" placeholder={getMultiLang("00012","请输入")} onKeyDown={this.onEnterResetPwd}/>
                            </div>
                        </div>
                        <div className="psw-p">
                            <div className="psw-p-label">{getMultiLang("00013","确认新密码")}</div>
                            <div className="psw-p-input">
                                <input className="u-form-control text" id="newpsw1" type="password" placeholder={getMultiLang("00012","请输入")} onKeyDown={this.onEnterResetPwd}/>
                            </div>
                        </div>
                    </Modal.Content>
                    <Modal.Foot>
                        <NCButton className="modalCencel" onClick={this.close}>{getMultiLang("00014","取消")}</NCButton>
                        <NCButton className="modalOK" onClick={this.onReset} id="reset-psd-confirm">{getMultiLang("00015","确认")}</NCButton>
                    </Modal.Foot>
                    </div>
                    <div className="login-tabs">
                    <Modal.Title onClose={this.close}>
                    {getMultiLang("00009","重置密码")}
                    </Modal.Title>
                    <Modal.Content>
                        <div className="psw-reset1">{getMultiLang("00016","密码修改成功！请重新登录。")}</div>
                        <div className="psw-reset2">{getMultiLang("00017","5s后自动关闭")}</div>         
                    </Modal.Content>
                    <Modal.Foot>                        
                        <NCButton className="modalOK" onClick={this.close}>{getMultiLang("00018","关闭")}</NCButton>
                    </Modal.Foot>
                    </div>                    
                </Modal>
                <Modal visible={this.state.selectTypeChangePsw}  step={this.state.step}>
                    <div className="login-tabs">
                    <Modal.Title onClose={this.closeSelectTypeChangePsw}>
                    {getMultiLang("00033","选择找回密码方式")}
                    </Modal.Title>
                    <div className="select-change-pwd">
                        <NCButton field="emailFindBtn" fieldname="邮箱找回密码" id="emailFindBtn" onClick = {this.forgetPsw} className="btn" colors="danger">{getMultiLang("00034","邮箱找回密码")}</NCButton>
                        <NCButton field="mobileFindBtn" fieldname="手机找回密码" id="mobileFindBtn" onClick = {this.mobileFindPsw} className="btn mobile-btn" colors="danger">{getMultiLang("00035","手机找回密码")}</NCButton>
                    </div>
                    </div>                  
                </Modal>
                <Modal visible={this.state.mobileFindPsw}  step={this.state.mobileFindPswStep}>
                    <div className="login-tabs">
                        <Modal.Title onClose={this.closeMobileFindPsw}>
                        {getMultiLang("00035","手机找回密码")}
                        </Modal.Title>
                        <Modal.Content className="reset-psw">
                        <NCSteps className="forget-step" current={this.state.forgetstep} size="small">
                            <NCStep title={getMultiLang("00019","填写信息")} />
                            <NCStep title={getMultiLang("00009","重置密码")} />
                            <NCStep title={getMultiLang("00020","修改成功")} />
                        </NCSteps>
                        <div className="tips">{getMultiLang("00036","输入要找回密码的用户和用户绑定的手机号")}</div>
                        <div className="tips">{getMultiLang("00037","点击”下一步”将向手机号发送验证码")}</div>
                        <div className="psw-error-msg" id="mobile-find-psw-error-msg"></div>   
                        <div className="psw-p">
                            <div className="psw-p-label">{getMultiLang("00002","用户名")}</div>
                            <div className="psw-p-input">
                                <input className="u-form-control text" id="mobilepwduser" type="text" placeholder={getMultiLang("00012","请输入")}/>
                            </div>
                        </div>
                        <div className="psw-p">
                            <div className="psw-p-label">{getMultiLang("00038","手机号")}</div>
                            <div className="psw-p-input">
                                <input className="u-form-control text" id="mobilepwdnum" type="text" placeholder={getMultiLang("00012","请输入")}/>
                            </div>
                        </div>
                        <div className="psw-p">
                            <div className="psw-p-label">{getMultiLang("00039","验证码")}</div>
                            <div className="psw-p-input">
                                <div className="mobile-find-pwd-randDiv">
                                   <input id="mobilepicvercode" type="text" placeholder={getMultiLang("00012","请输入")}/>
                                   <img onClick={this.mobileFindPswRandImg} id="mobilefindpwdrandImg" src={this.yhtRandImgUrl + this.picVerCodeKey}/>
                                </div>
                            </div>
                        </div>
                        </Modal.Content>
                        <Modal.Foot>
                            <NCButton className="modalCencel" onClick={this.closeMobileFindPsw}>{getMultiLang("00014","取消")}</NCButton>
                            <NCButton className="modalOK" onClick={this.toMobileFindPwdSendCode}>{getMultiLang("00024","下一步")}</NCButton>
                        </Modal.Foot>
                        
                    </div>
                    <div className="login-tabs">
                        <Modal.Title onClose={this.closeMobileFindPsw}>
                        {getMultiLang("00006","忘记密码")}
                        </Modal.Title>
                        <Modal.Content className="reset-psw">
                        <NCSteps className="forget-step" current={this.state.mobileFindPswStep} size="small">
                            <NCStep title={getMultiLang("00019","填写信息")} />
                            <NCStep title={getMultiLang("00009","重置密码")} />
                            <NCStep title={getMultiLang("00020","修改成功")} />
                        </NCSteps>
                        <div className="tips">{getMultiLang("00040","输入绑定手机收到的验证码并设置新密码")}</div>
                        <div className="tips">{getMultiLang("00026","点击”下一步”确认修改成功")}</div>
                        <div className="psw-error-msg" id="mobile-find-psw-error-msg"></div>   
                        <div className="psw-p">
                            <div className="psw-p-label">{getMultiLang("00041","手机验证码")}</div>
                            <div className="psw-p-input">
                                <input className="u-form-control text" name="pwdverfiy" id="m-find-pwd-pwdverfiy" type="text" placeholder={getMultiLang("00012","请输入")}/>
                            </div>
                        </div>
                        <div className="psw-p">
                            <div className="psw-p-label">{getMultiLang("00011","新密码")}</div>
                            <div className="psw-p-input">
                                <input className="u-form-control text" name="pwd1" id="m-find-pwd-pwd1" type="password" placeholder={getMultiLang("00012","请输入")}/>
                            </div>
                        </div>
                        <div className="psw-p">
                            <div className="psw-p-label">{getMultiLang("00013","确认新密码")}</div>
                            <div className="psw-p-input">
                                <input className="u-form-control text" name="pwd2" id="m-find-pwd-pwd2" type="password" placeholder={getMultiLang("00012","请输入")}/>
                            </div>
                        </div>
                        </Modal.Content>
                        <Modal.Foot>
                            <NCButton className="modalCencel" onClick={this.closeMobileFindPsw}>{getMultiLang("00014","取消")}</NCButton>
                            <NCButton className="modalCencel" onClick={this.onBeforeMobileFindPsw}>{getMultiLang("00028","上一步")}</NCButton>
                            <NCButton className="modalOK" onClick={this.toMobileVerfiyAndReset.bind("5000")}>{getMultiLang("00024","下一步")}</NCButton>
                        </Modal.Foot>
                    </div>
                    <div className="login-tabs">
                        <Modal.Title onClose={this.closeMobileFindPsw}>
                        {getMultiLang("00006","忘记密码")}
                        </Modal.Title>
                        <Modal.Content className="reset-psw">
                        <NCSteps className="forget-step" current={this.state.forgetstep} size="small">
                            <NCStep title={getMultiLang("00019","填写信息")} />
                            <NCStep title={getMultiLang("00009","重置密码")} />
                            <NCStep title={getMultiLang("00020","修改成功")} />
                        </NCSteps>
                            <div className="psw-forget1">{getMultiLang("00029","密码重置成功！")}</div>
                            <div className="psw-forget2">{getMultiLang("00017","5秒后自动关闭")}</div>
                        </Modal.Content>
                        <Modal.Foot>
                            <NCButton className="modalOK" onClick={this.closeMobileFindPsw}>{getMultiLang("00018","关闭")}</NCButton>
                        </Modal.Foot>
                    </div>
                    
                </Modal>
                <Modal visible={this.state.forgetpsw}  step={this.state.forgetstep}>
                    <div className="login-tabs">
                        <Modal.Title onClose={this.closeForgetPsw}>
                        {getMultiLang("00006","忘记密码")}
                        </Modal.Title>
                        <Modal.Content className="reset-psw">
                        <NCSteps className="forget-step" current={this.state.forgetstep} size="small">
                            <NCStep title={getMultiLang("00019","填写信息")} />
                            <NCStep title={getMultiLang("00009","重置密码")} />
                            <NCStep title={getMultiLang("00020","修改成功")} />
                        </NCSteps>
                        <div className="tips">{getMultiLang("00021","输入要找回密码的用户和用户绑定的邮箱")}</div>
                        <div className="tips">{getMultiLang("00022","点击”下一步”将向邮箱发送验证码")}</div>
                        <div className="psw-error-msg" id="psw-error-msg"></div>   
                        <div className="psw-p">
                            <div className="psw-p-label">{getMultiLang("00002","用户名")}</div>
                            <div className="psw-p-input">
                                <input className="u-form-control text" id="pwduser" type="text" placeholder={getMultiLang("00012","请输入")}/>
                            </div>
                        </div>
                        <div className="psw-p">
                            <div className="psw-p-label">{getMultiLang("00023","邮箱")}</div>
                            <div className="psw-p-input">
                                <input className="u-form-control text" id="pwdemail" type="text" placeholder={getMultiLang("00012","请输入")}/>
                            </div>
                        </div>
                        </Modal.Content>
                        <Modal.Foot>
                            <NCButton className="modalCencel" onClick={this.closeForgetPsw}>{getMultiLang("00014","取消")}</NCButton>
                            <NCButton className="modalOK" onClick={this.toGetResetCode}>{getMultiLang("00024","下一步")}</NCButton>
                        </Modal.Foot>
                        
                    </div>
                    <div className="login-tabs">
                        <Modal.Title onClose={this.closeForgetPsw}>
                        {getMultiLang("00006","忘记密码")}
                        </Modal.Title>
                        <Modal.Content className="reset-psw">
                        <NCSteps className="forget-step" current={this.state.forgetstep} size="small">
                            <NCStep title={getMultiLang("00019","填写信息")} />
                            <NCStep title={getMultiLang("00009","重置密码")} />
                            <NCStep title={getMultiLang("00020","修改成功")} />
                        </NCSteps>
                        <div className="tips">{getMultiLang("00025","输入绑定邮箱收到的验证码并设置新密码")}</div>
                        <div className="tips">{getMultiLang("00026","点击”下一步”确认修改成功")}</div>
                        <div className="psw-error-msg" id="psw-error-msg"></div>   
                        <div className="psw-p">
                            <div className="psw-p-label">{getMultiLang("00027","邮箱验证码")}</div>
                            <div className="psw-p-input">
                                <input className="u-form-control text" name="pwdverfiy" id="pwdverfiy" type="text" placeholder={getMultiLang("00012","请输入")}/>
                            </div>
                        </div>
                        <div className="psw-p">
                            <div className="psw-p-label">{getMultiLang("00011","新密码")}</div>
                            <div className="psw-p-input">
                                <input className="u-form-control text" name="pwd1" id="pwd1" type="password" placeholder={getMultiLang("00012","请输入")}/>
                            </div>
                        </div>
                        <div className="psw-p">
                            <div className="psw-p-label">{getMultiLang("00013","确认新密码")}</div>
                            <div className="psw-p-input">
                                <input className="u-form-control text" name="pwd2" id="pwd2" type="password" placeholder={getMultiLang("00012","请输入")}/>
                            </div>
                        </div>
                        </Modal.Content>
                        <Modal.Foot>
                            <NCButton className="modalCencel" onClick={this.closeForgetPsw}>{getMultiLang("00014","取消")}</NCButton>
                            <NCButton className="modalCencel" onClick={this.onBeforeForgetPsw}>{getMultiLang("00028","上一步")}</NCButton>
                            <NCButton className="modalOK" onClick={this.toVerfiyAndReset.bind("5000")}>{getMultiLang("00024","下一步")}</NCButton>
                        </Modal.Foot>
                    </div>
                    <div className="login-tabs">
                        <Modal.Title onClose={this.closeForgetPsw}>
                        {getMultiLang("00006","忘记密码")}
                        </Modal.Title>
                        <Modal.Content className="reset-psw">
                        <NCSteps className="forget-step" current={this.state.forgetstep} size="small">
                            <NCStep title={getMultiLang("00019","填写信息")} />
                            <NCStep title={getMultiLang("00009","重置密码")} />
                            <NCStep title={getMultiLang("00020","修改成功")} />
                        </NCSteps>
                            <div className="psw-forget1">{getMultiLang("00029","密码重置成功！")}</div>
                            <div className="psw-forget2">{getMultiLang("00017","5秒后自动关闭")}</div>
                        </Modal.Content>
                        <Modal.Foot>
                            <NCButton className="modalOK" onClick={this.closeForgetPsw}>{getMultiLang("00018","关闭")}</NCButton>
                        </Modal.Foot>
                    </div>
                    
                </Modal>{this.showVersionTips()}
            </div>            
        )
    }
}

// 重置 aes 请求 --bbqin
localStorage.setItem('rockin', false);


const getData = () => { 
    try{
        cacheTools.clear();
    }catch(e){

    }
    let data ={};
    data.bcCode = getStoreBc();
    document.getElementsByTagName("TITLE")[0].text = getMultiLang("00042"," NC Cloud 服务大型企业数字化转型");
    ajax({
        url: '/nccloud/riart/login/init.do',
        data:data,
        success: function (res) {
            document.querySelector('#login_div').setAttribute("class","bg2");
            let { success, data } = res;
            if (success) {                
                ReactDOM.render(<Main data={data}  busiCenterCode={(data.bcCode==undefined||data.bcCode=='')?(data.bc.length==0?"":data.bc[0].code):data.bcCode} langCode={getStoreLang(data.lang.length==0?"":data.lang[0].code)}/>, document.querySelector('#login_div'));
            }
        }
    })
}
InitMultiLang("login-001",getData);

