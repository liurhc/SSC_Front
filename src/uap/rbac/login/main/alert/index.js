import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './index.less';
class Alert extends Component {

    onOk = () =>{       
        this.onCancel(); 
        this.props.onOk();
    }
    onCancel = () =>{
        document.querySelector('#login_div').removeChild(document.querySelector("#alertid"));
    }
    render() {

        const {title,getLang} = this.props;
        return <div className="login-alert-content">
        			        	

            <div className="login-alert-panel"><i  class="iconfont attention icon-zhuyi1"></i>	{title}</div>
            <div className="login-alert-bottom"><button className="u-button btn alert-ok" onClick={this.onOk}>{getLang("00032","确定")}</button><button className="u-button btn alert-cancel" onClick={this.onCancel}>{getLang("00014","取消")}</button></div>
        </div>
    }
}
export default function CreateAlert(title,onOk,getLang){
    let tr = document.createElement('div');
    tr.className="login-alert";
    tr.id="alertid";
    ReactDOM.render(<Alert title={title} onOk={onOk} getLang={getLang}></Alert>,tr);
    document.querySelector('#login_div').appendChild(tr);
}