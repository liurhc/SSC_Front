import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, promptBox, toast } from 'nc-lightapp-front';
import './index.less';
const { NCSelect } = base;
const NCOption = NCSelect.NCOption;

class ThirdPartyLogin extends Component {
    constructor(props) {
        super(props);
        let url = window.location.href;
        let baseurl = window.location.host;
        let redirect_uri = decodeURIComponent(decodeURIComponent(url.substring(url.indexOf("redirect_uri=")+13)))
        // if(redirect_uri.indexOf("http")==0 && redirect_uri.indexOf(baseurl)==-1){
        //          toast({content:"单点登录地址不一致",color:'error'});
        //          }
        this.state = {
            loginprops:{},
            redirect_uri,
            accesstoken:props.getUrlParam('accesstoken'),
            billid:props.getUrlParam('billid'),
            billtype:props.getUrlParam('billtype'),
            pk_task:props.getUrlParam('pk_task'),
            connecttype:props.getUrlParam('connecttype'),
            busicenter:'',
            langcode:'',
            usercode:'',
        }
    }
    componentWillMount() {
        String.prototype.insert = function (index, item) {
            var temp = [];
            for (var i = 0; i < this.length; i++) {
                temp.push(this[i]);
            }
            temp.splice(index, 0, item);
            return temp.join("")
        }; 
        let callback = (json, status, inlt) => { // json 多语json格式参数； status： 是否请求到json多语，可用来判断多语资源是否请求到并进行一些别的操作； inlt： 可用来进行占位符的一些操作
            if (status) {
                this.setState({ json, inlt });// 保存json和inlt到页面state中并刷新页面
            } else {
                console.log(未加载到多语资源);   // 未请求到多语资源的后续操作/* 国际化处理： 未加载到多语资源,未加载到多语资源*/
            }
        }
        this.props.MultiInit.getMultiLang({ 'moduleId': 'login-001', 'domainName': 'uap', callback });
    }
    componentDidMount() {
        this.onSubmit();
    }
    initOption = (data) =>{
        let body=[];
        data.map((option,i) => (
            body.push(<NCOption value={option.code}>{option.name}</NCOption>)
        ));
        return body;
    }
    handleChange = (value) => {
        this.setState({
            busicenter:value
        });
    };
    onSubmit = ()=>{
        let that = this;
        ajax({
            url: '/nccloud/baseapp/thirdPartyLogin/thirdPartyVerfiy.do',
            data:{
                firstlogin:"Y",
                token:that.state.accesstoken,
                pk_task:that.state.pk_task,
                connecttype:that.state.connecttype,
                timezone: new Date().toString().substring(25,33).insert(6,":")
            },
            success: function (res) {
                let { success, data } = res;
                if (success) {
                    let rslCode = data.rslCode;
                    if(rslCode=="0"){//登录成功
                        if(that.state.connecttype){
                            data.redir? (setTimeout(function(){window.location = data.redir},1500)): toast({content:"联查业务单据失败 :"+data.errmsg,color:'danger'});
                        }else{
                            window.location = that.state.redirect_uri;
                        }
                        
                    }
                    if(rslCode=="5"){//已登录,是否强制登陆   
                        if(that.state.connecttype){
                            data.redir? (setTimeout(function(){window.location = data.redir},1500)): toast({content:"联查业务单据失败 :"+data.errmsg,color:'danger'});
                        }else{
                            promptBox({
                                color: 'warning',
                                title: "确定登录",/* 国际化处理： 确认删除*/
                                content: "该用户已在线，是否强制登录？",/* 国际化处理： 是否要删除所选数据？*/
                                beSureBtnClick: () => {
                                    ajax({
                                        url: '/nccloud/baseapp/thirdPartyLogin/thirdPartyVerfiy.do',
                                        data:{
                                            firstlogin:'N',
                                            usercode:that.state.usercode,
                                            langcode:that.state.langcode,
                                            busicenter:that.state.busicenter,
                                            timezone: new Date().toString().substring(25,33).insert(6,":"),
                                            forcelogin:"Y"
                                        },
                                        success: function (res) {
                                            window.location = that.state.redirect_uri;
                                        }
                                    })
                                }
                            })
                        }               
                        
                    }
                    if(rslCode=="6"){//token错误或者过期
                        toast({content:data.rslMsg,color:'danger'});
                    }
                    if(rslCode=="7"){//没有可登录的系统
                        toast({content:data.rslMsg,color:'danger'});
                    }
                    if(rslCode=="8"){//多账套
                        that.setState({
                            usercode:data.usercode,
                            langcode:data.langcode,
                            busicenter:data.rslMsg[0].code
                        });
                        that.props.modal.show('busi',{
                            content:<div>
                                        <NCSelect 
                                            defaultValue={data.rslMsg[0].code} 
                                            onChange={that.handleChange.bind(this)}
                                            style={{ width: 200, marginRight: 2 }}>
                                            {that.initOption(data.rslMsg)}
                                        </NCSelect>
                                    </div>,
                            hideRightBtn:true,
                            beSureBtnClick: () => {
                                ajax({
                                    url: '/nccloud/baseapp/thirdPartyLogin/thirdPartyVerfiy.do',
                                    data:{
                                        firstlogin:'N',
                                        usercode:that.state.usercode,
                                        langcode:that.state.langcode,
                                        busicenter:that.state.busicenter,
                                        timezone: new Date().toString().substring(25,33).insert(6,":"),
                                    },  
                                    success: function (res) {
                                        window.location = that.state.redirect_uri;
                                    }
                                })
                            }
                        })
                    }
                    if(rslCode=="-1"){//登录异常
                        toast({content:data.rslMsg,color:'danger'});
                    }                 
                }
            }
        })
    }

	render() {
        let { modal, data } = this.props;
        let { createModal } = modal;
        this.loginprops = data;
		return (
            <div id= "login_div" className="content-right">
                {createModal('busi', {title:"请选择登录账套"})}
            </div>
		);
	}
}

export default ThirdPartyLogin = createPage({

})(ThirdPartyLogin);
ReactDOM.render(<ThirdPartyLogin />, document.querySelector('#app'));
