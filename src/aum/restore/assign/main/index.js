import { RenderRouter } from 'nc-lightapp-front';
import routes from './router';
import ampub from 'ampub';

const { utils } = ampub;
const { initMultiLangByModule } = utils.multiLangUtils;

(function main(routers, htmlTagid) {
	let moduleIds = { aum: [ '452001512A' ], ampub: [ 'common' ] };
	initMultiLangByModule(moduleIds, () => {
		RenderRouter(routers, htmlTagid);
	});
})(routes, 'app');
