/**
 * 领用归还常量
 */
export const sameConfig = {
	pkField: 'pk_assign',
	dataSource: 'aum.restore.assign.main',
	cardRouter: '/card',
	listRouter: '/list',
	title: '452001512A-000000' /* 国际化处理： 领用归还*/,
	printFilename: '452001512A-000001' /* 国际化处理： 领用归还*/
};
