import initTemplate from './initTemplate';
import buttonClick, { getAssginUsedr, turnOff } from './buttonClick';
import searchBtnClick from './searchBtnClick';
import pageInfoClick from './pageInfoClick';
import { rowSelected, setBatchBtnsEnable } from './rowSelected';
import afterEvent from './afterEvent';
import doubleClick from './doubleClick';

export {
	initTemplate,
	buttonClick,
	searchBtnClick,
	pageInfoClick,
	rowSelected,
	setBatchBtnsEnable,
	afterEvent,
	doubleClick,
	getAssginUsedr,
	turnOff
};
