import { ajax } from 'nc-lightapp-front';
import { pageConfig } from '../constants';
import { setBatchBtnsEnable } from './rowSelected';
import ampub from 'ampub';

const { components, utils } = ampub;
const { getContext, loginContextKeys } = components.LoginContext;
const { showMessage } = utils.msgUtils;
const { setListValue, setQueryInfoCache, listConst } = utils.listUtils;

const { pagecode, searchId, tableId, bill_type } = pageConfig;
export default function searchBtnClick(props, searchVal, type, queryInfo, isRefresh) {
	if (!searchVal) {
		return;
	}
	// 交易类型
	let transi_type = getContext(loginContextKeys.transtype);
	if (!queryInfo) {
		queryInfo = props.search.getQueryInfo(searchId);
	}

	let pageInfo = props.table.getTablePageInfo(tableId);
	if (queryInfo) {
		queryInfo.pageInfo = pageInfo;
		queryInfo.pagecode = pagecode;
		queryInfo.billtype = bill_type;
		queryInfo.transtype = transi_type;
	}

	// 缓存查询条件
	setQueryInfoCache.call(this, queryInfo, isRefresh, type);
	queryList.call(this, queryInfo, props, isRefresh);
}
function queryList(queryInfo, props, isRefresh) {
	ajax({
		url: pageConfig.url.query,
		data: queryInfo,
		success: (res) => {
			setListValue.call(
				this,
				props,
				res,
				tableId,
				isRefresh ? listConst.SETVALUETYPE.refresh : listConst.SETVALUETYPE.query
			);
			setBatchBtnsEnable.call(this, props, tableId);
		},
		error: (res) => {
			if (res && res.message) {
				showMessage.call(this, props, { content: res.message, color: 'warning' });
			}
		}
	});
}
