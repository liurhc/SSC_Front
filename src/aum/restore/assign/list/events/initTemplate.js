import { ajax } from 'nc-lightapp-front';
import { pageConfig } from '../constants';
import { setBatchBtnsEnable } from './rowSelected';
import { linkToCard } from './buttonClick';
import ampub from 'ampub';

const { components, commonConst, utils } = ampub;
const { defRefCondition, commonRefCondition } = components.refInit;
const { openApprove } = components.queryAboutUtils;
const { getContext, loginContextKeys, loginContext } = components.LoginContext;
const { IBusiRoleConst } = commonConst.CommonKeys;
const { UISTATE } = commonConst.StatusUtils;
const { getMultiLangByID } = utils.multiLangUtils;
const { showMessage } = utils.msgUtils;
const { createOprationColumn } = utils.listUtils;
const { getScriptListReturn } = components.ScriptReturnUtils;
const { AssetOrgMultiRefFilter } = components.assetOrgMultiRefFilter;

const { pagecode, cardPageId, tableId, searchId, node_code, formId, pkField, dataSource } = pageConfig;

export default function initTemplate(props) {
	props.createUIDom(
		{
			pagecode: pagecode //页面id
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button, () => {
						// 行删除时悬浮框提示
						props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001'));
					});
				}
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta, () => {
						// 设置批量按钮可用性
						setBatchBtnsEnable.call(this, props, tableId);
					});
				}
			}
		}
	);
}

function modifierMeta(props, meta) {
	let pk_group = getContext(loginContextKeys.groupId);
	meta[searchId].items = meta[searchId].items.map((item, key) => {
		item.col = '3';
		return item;
	});
	//分页控件显示标识
	meta[tableId].pagination = true;
	meta[tableId].showindex = true;
	meta[tableId].showcheck = true;
	//人员根据组织进行过滤
	meta[searchId].items.forEach((item) => {
		// 资产组织过滤
		if (item.attrcode == 'pk_org') {
			item.queryCondition = () => {
				return { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter' };
			};
		} else if (item.attrcode == 'pk_recorder' || item.attrcode == 'pk_taker') {
			AssetOrgMultiRefFilter.call(this, props, searchId, item, 'pk_org');
			// 如果user_name的参照需要根据前面选中参照的值进行过滤
			item.queryCondition = () => {
				// 添加过滤方法
				let pk_org = getSearchValue.call(this, props, 'pk_org'); // 获取前面选中参照的值
				let filter = { isShowUnit: true, busifuncode: IBusiRoleConst.ASSETORG };
				if (pk_org) {
					filter['pk_org'] = pk_org;
				}
				return filter; // 根据pk_org过滤
			};
		} else if (
			item.attrcode == 'bodyvos.pk_equip.pk_usedept' ||
			item.attrcode == 'bodyvos.pk_equip.pk_user' ||
			item.attrcode == 'bodyvos.pk_usedept_after' ||
			item.attrcode == 'bodyvos.pk_user_after'
		) {
			let orgField = 'bodyvos.pk_equip.pk_usedunit';
			if (item.attrcode == 'bodyvos.pk_usedept_after' || item.attrcode == 'bodyvos.pk_user_after') {
				orgField = 'bodyvos.pk_unit_used_after';
			}
			AssetOrgMultiRefFilter.call(this, props, searchId, item, orgField);
			//（使用部门）
			item.queryCondition = () => {
				let pk_unit_used = getSearchValue.call(this, props, orgField); // 使用权
				let filter = { isShowUnit: true, busifuncode: IBusiRoleConst.ASSETORG };
				if (pk_unit_used) {
					filter['pk_org'] = pk_unit_used;
				}
				return filter; // 根据pk_org过滤
			};
			if (item.attrcode == 'bodyvos.pk_equip.pk_usedept' || item.attrcode == 'bodyvos.pk_usedept_after') {
				// 添加【执行时包含下级】默认勾选
				item.isRunWithChildren = true;
				item.defaultRunWithChildren = true;
			}
		} else if (item.attrcode == 'bodyvos.pk_equip.pk_mandept' || item.attrcode == 'bodyvos.pk_equip.pk_manager') {
			AssetOrgMultiRefFilter.call(this, props, searchId, item, 'bodyvos.pk_equip.pk_ownerorg');
			//（管理部门）
			item.queryCondition = () => {
				let pk_unit_used = getSearchValue.call(this, props, 'bodyvos.pk_equip.pk_ownerorg'); // 使用管理组织
				let filter = { isShowUnit: true, busifuncode: IBusiRoleConst.ASSETORG };
				if (pk_unit_used) {
					filter['pk_org'] = pk_unit_used;
				}
				return filter; // 根据pk_org过滤
			};
			if (item.attrcode == 'bodyvos.pk_equip.pk_mandept') {
				// 添加【执行时包含下级】默认勾选
				item.isRunWithChildren = true;
				item.defaultRunWithChildren = true;
			}
		} else if (item.attrcode == 'bodyvos.pk_warehouse_in') {
			AssetOrgMultiRefFilter.call(this, props, searchId, item, 'bodyvos.pk_icorg_after');
			item.queryCondition = () => {
				let pk_org = getSearchValue.call(this, props, 'bodyvos.pk_icorg_after'); //变动后库存组织,
				let filter = { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.StoreDocSqlBuilder' };
				if (pk_org) {
					filter['pk_org'] = pk_org;
				}
				return filter;
			};
		} else {
			let bodyDefPrefix = 'bodyvos.def';
			defRefCondition.call(this, props, item, searchId, pk_group, true, 'pk_org', bodyDefPrefix);
			defRefCondition.call(this, props, item, searchId, pk_group, true);
		}
		// 添加【执行时包含下级】默认勾选
		commonRefCondition.call(this, props, item);
	});
	//给单据号列添加超链接
	meta[tableId].items = meta[tableId].items.map((item, key) => {
		item.width = 150;
		if (item.attrcode == 'bill_code') {
			item.render = (text, record, index) => {
				return (
					<div class="simple-table-td" field="bill_code" fieldname={getMultiLangByID('452001512A-000002')}>
						<span
							className="code-detail-link"
							onClick={() => {
								linkToCard.call(this, props, record);
							}}
						>
							{record.bill_code && record.bill_code.value}
						</span>
					</div>
				);
			};
		}
		return item;
	});
	//添加操作列
	let opr = createOprationColumn.call(this, props, { tableButtonClick });
	meta[tableId].items.push(opr);

	return meta;
}

function tableButtonClick(props, key, text, record, index, tableId) {
	switch (key) {
		case 'Delete':
			let paramInfoMap = {};
			paramInfoMap[record.pk_assign.value] = record.ts.value;
			let params = [ { id: record.pk_assign.value, index: index } ];
			ajax({
				url: pageConfig.url.delete,
				data: {
					paramInfoMap: paramInfoMap,
					dataType: 'listData',
					OperatorType: 'DELETE',
					pageid: cardPageId
				},

				success: (res) => {
					getScriptListReturn.call(this, params, res, props, pkField, formId, tableId, true, dataSource);
				},
				error: (res) => {
					showMessage.call(this, props, { color: 'danger', content: res.message });
				}
			});
			break;
		case 'Edit':
			ajax({
				url: pageConfig.url.assignEdit,
				data: {
					pk: record.pk_assign.value,
					resourceCode: node_code
				},
				success: (res) => {
					if (res.data) {
						showMessage.call(this, props, { color: 'warning', content: res.data });
					} else {
						linkToCard.call(this, props, record, UISTATE.edit);
					}
				},
				error: (res) => {
					if (res && res.message) {
						showMessage.call(this, props, { color: 'warning', content: res.message });
					}
				}
			});
			break;
		case 'Commit':
			unOrCommitAction.call(this, props, record, index, 'SAVE', 'commit');
			break;
		case 'UnCommit':
			unOrCommitAction.call(this, props, record, index, 'UNSAVE');
			break;
		case 'QueryAboutBillFlow':
			this.setState({
				pk_bill: record.pk_assign.value,
				transi_type: record.transi_type.value
			});
			openApprove(this);
			break;
	}
}

function unOrCommitAction(props, record, index, operatorType, commitType) {
	let paramInfoMap = {};
	let id = record.pk_assign.value;
	let ts = record.ts.value;
	paramInfoMap[id] = ts;
	let params = [ { id: record.pk_assign.value, index: index } ];
	let data = {
		paramInfoMap: paramInfoMap,
		dataType: 'listData',
		OperatorType: operatorType,
		pageid: cardPageId
	};
	if (commitType) {
		data.commitType = commitType;
	}

	ajax({
		url: pageConfig.url.commit,
		data: data,
		success: (res) => {
			getScriptListReturn.call(this, params, res, props, pkField, formId, tableId, false, dataSource);
		},
		error: (res) => {
			showMessage.call(this, props, { color: 'danger', content: res.message });
		}
	});
}
//获取查询条件的值
function getSearchValue(props, field) {
	let data = props.search.getSearchValByField(searchId, field);
	let value = '';
	if (data && data.value) {
		value = data.value.firstvalue;
	}
	if (value && value.split(',').length == 1) {
		return value;
	} else {
		return '';
	}
}
