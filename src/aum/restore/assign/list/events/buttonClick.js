import { ajax, print, output } from 'nc-lightapp-front';
import { pageConfig } from '../constants';
import searchBtnClick from './searchBtnClick';
import ampub from 'ampub';

const { components, commonConst, utils } = ampub;
const { UISTATE } = commonConst.StatusUtils;
const { showConfirm, MsgConst, showMessage } = utils.msgUtils;
const { batchRefresh } = utils.listUtils;
const { getScriptListReturn } = components.ScriptReturnUtils;

const {
	tableId,
	cardRouter,
	cardPageId,
	pkField,
	printNodekey,
	formId,
	commitOpr,
	commitType,
	unCommitOpr,
	printFilename,
	dataSource
} = pageConfig;

export default function buttonClick(props, id) {
	switch (id) {
		case 'Add':
			add.call(this, props);
			break;
		case 'Delete':
			delConfirm.call(this, props);
			break;
		case 'Commit':
			unOrCommitAction.call(this, props, commitOpr, commitType);
			break;
		case 'UnCommit':
			unOrCommitAction.call(this, props, unCommitOpr);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'QueryAboutBusiness':
			openBillTrack.call(this, props);
			break;
		case 'Refresh':
			refreshAction.call(this, props);
			break;
		case 'Attachment':
			attachmentClick.call(this, props);
			break;
		default:
			break;
	}
}
/**
 * 删除弹框提示
 * @param {*} props 
 */
function delConfirm(props) {
	let checkedRows = props.table.getCheckedRows(tableId);
	if (checkedRows.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseDelete });
		return;
	}
	showConfirm.call(this, props, { type: MsgConst.Type.DelSelect, beSureBtnClick: deleteAction });
}
/**
 * 删除处理
 * @param {*} props 
 */
function deleteAction(props) {
	let data = props.table.getCheckedRows(tableId);
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values.pk_assign.value;
		let ts = v.data.values.ts.value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	ajax({
		url: pageConfig.url.delete,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: cardPageId
		},
		success: (res) => {
			getScriptListReturn.call(this, params, res, props, 'pk_assign', formId, tableId, true);
		},
		error: (res) => {
			if (res && res.message) {
				showMessage.call(this, props, { content: res.message, color: 'warning' });
			}
		}
	});
}

/**
 * 刷新处理
 * @param {*} props 
 */
function refreshAction(props) {
	batchRefresh.call(this, props, searchBtnClick);
}

/**
 * 提交、收回处理
 * @param {*} props 
 * @param {*} operatorType 
 * @param {*} commitType 
 */
function unOrCommitAction(props, operatorType, commitType, content) {
	let selectdata = props.table.getCheckedRows(tableId);

	let paramInfoMap = {};
	let params = selectdata.map((v) => {
		let id = v.data.values[pkField].value;
		let ts = v.data.values.ts.value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	let data = {
		paramInfoMap: paramInfoMap,
		dataType: 'listData',
		OperatorType: operatorType,
		pageid: cardPageId,
		content
	};
	if (commitType) {
		data.commitType = commitType;
	}

	ajax({
		url: pageConfig.url.commit,
		data: data,
		success: (res) => {
			if (content) {
				this.setState({
					compositedisplay: false
				});
			}
			getScriptListReturn.call(this, params, res, props, pkField, formId, tableId, false, dataSource);
		},
		error: (res) => {
			if (res && res.message) {
				showMessage.call(this, props, { content: res.message, color: 'warning' });
			}
		}
	});
}

/**
 * 打印
 * @param {*} props 
 */
export function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		pageConfig.url.printUrl, // 后台打印服务url
		printData
	);
}

/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });
		return;
	}
	output({
		url: pageConfig.url.printUrl,
		data: printData
	});
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let checkedRows = props.table.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		return false;
	}
	let pks = [];
	checkedRows.map((item) => {
		pks.push(item.data.values[pkField].value);
	});
	let printData = {
		filename: printFilename, // 文件名称
		nodekey: printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

/**
 * 附件
 * @param {*} props 
 */
function attachmentClick(props) {
	let checkedrows = props.table.getCheckedRows(tableId);
	// billId 是单据主键
	let billId = checkedrows[0].data.values.pk_assign.value;
	// billNo 是单据号
	let billNo = checkedrows[0].data.values.bill_code.value;
	props.ncUploader.show('warcontract-uploader', {
		billId: 'pam/assign/' + billId,
		billNo
	});
}

/**
 * 跳转到卡片
 * @param {*} props 
 * @param {*} record 
 * @param {*} status 
 */
export function linkToCard(props, record = {}, status = UISTATE.browse) {
	props.pushTo(cardRouter, {
		pagecode: cardPageId,
		status,
		id: record[pkField] ? record[pkField].value : ''
	});
}

/**
 * 列表新增按钮事件
 * @param {*} props 
 */
function add(props) {
	linkToCard(props, {}, UISTATE.add);
}

//单据追溯
function openBillTrack(props) {
	let checkedRows = props.table.getCheckedRows(tableId);
	this.setState({
		show: true,
		pk_bill: checkedRows[0].data.values.pk_assign.value
	});
}

//提交及指派 回调，注意保存提交时候需要调用保存提交的方法
export function getAssginUsedr(content) {
	unOrCommitAction.call(this, this.props, 'SAVE', 'commit', content); //原提交方法添加参数content
}
//取消 指派
export function turnOff() {
	this.setState({
		compositedisplay: false
	});
}
