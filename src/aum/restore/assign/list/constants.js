/**
 * 列表常量
 */
import { sameConfig } from '../const';

export const pageConfig = {
	...sameConfig,
	tableId: 'list_head',
	node_code: '4520016010',
	printNodekey: null,
	searchId: 'searchArea',
	pagecode: '452001512A_list',
	cardPageId: '452001512A_card',
	appid: '452001512A',
	commitOpr: 'SAVE',
	commitType: 'commit',
	unCommitOpr: 'UNSAVE',
	bill_type: '4A16',
	formId: 'card_head',
	url: {
		delete: '/nccloud/aum/assign_return/delete.do',
		commit: '/nccloud/aum/assign_return/commit.do',
		printUrl: '/nccloud/aum/assign_return/printCard.do',
		query: '/nccloud/aum/assign_return/query.do',
		assignEdit: '/nccloud/aum/assign_return/assignEdit.do',
		querypage: '/nccloud/aum/assign_return/querypage.do'
	}
};
