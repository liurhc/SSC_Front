import React, { Component } from 'react';
import { createPage, high, base, createPageIcon } from 'nc-lightapp-front';
import {
	initTemplate,
	buttonClick,
	searchBtnClick,
	pageInfoClick,
	rowSelected,
	afterEvent,
	doubleClick,
	getAssginUsedr,
	turnOff,
	setBatchBtnsEnable
} from './events';
import { pageConfig } from './constants';
import ampub from 'ampub';

const { components, utils } = ampub;
const { ApprovalTrans } = components;
const { getMultiLangByID } = utils.multiLangUtils;

const { NCAffix } = base;
const { tableId, searchId, bill_type, dataSource, pkField, title } = pageConfig;

class RestoreAssignList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			show: false,
			pk_bill: '',
			transi_type: '',
			showApprove: false,
			compositedisplay: false,
			compositedata: {}
		};
		initTemplate.call(this, props);
	}

	render() {
		let { table, search, ncmodal, ncUploader } = this.props;
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		const { createModal } = ncmodal;
		const { BillTrack, ApproveDetail } = high;

		let { createNCUploader } = ncUploader;

		return (
			<div className="nc-bill-list">
				<NCAffix>
					<div className="nc-bill-header-area">
						<div className="header-title-search-area">
							{createPageIcon()}
							<h2 className="title-search-detail">{getMultiLangByID(title)}</h2>
						</div>
						<div className="header-button-area">
							{this.props.button.createButtonApp({
								area: 'list_head',
								onButtonClick: buttonClick.bind(this),
								buttonLimit: 3
							})}
						</div>
					</div>
				</NCAffix>
				<div className="nc-bill-search-area">
					{NCCreateSearch(searchId, {
						onAfterEvent: afterEvent.bind(this),
						clickSearchBtn: searchBtnClick.bind(this),
						defaultConditionsNum: 3
					})}
				</div>
				<div className="nc-bill-table-area">
					{createSimpleTable(tableId, {
						handlePageInfoChange: pageInfoClick.bind(this),
						showIndex: true,
						showCheck: true,
						onRowDoubleClick: doubleClick,
						onSelected: rowSelected.bind(this),
						onSelectedAll: rowSelected.bind(this),
						dataSource: dataSource,
						pkname: pkField,
						componentInitFinished: () => {
							//缓存数据赋值成功的钩子函数
							//若初始化数据后需要对数据做修改，可以在这里处理
							setBatchBtnsEnable.call(this, this.props, tableId);
						}
					})}
				</div>
				<BillTrack
					show={this.state.show}
					close={() => {
						this.setState({ show: false });
					}}
					pk={this.state.pk_bill} //单据id
					type={bill_type} //单据类型
				/>
				{/* 审批详情 */}
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transi_type}
					billid={this.state.pk_bill}
				/>
				{/* 附件区 */}
				{createNCUploader('warcontract-uploader', {})}
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002')} /* 国际化处理： 指派*/
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={getAssginUsedr.bind(this)}
						cancel={turnOff.bind(this)}
					/>
				) : (
					''
				)}
			</div>
		);
	}
}

RestoreAssignList = createPage({})(RestoreAssignList);

export default RestoreAssignList;
