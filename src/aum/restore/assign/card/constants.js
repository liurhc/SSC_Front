import { sameConfig } from '../const';

export const pageConfig = {
	...sameConfig,
	formId: 'card_head',
	tableId: 'card_body_bodyvos',
	tableIdEdit: 'card_body_bodyvos_edit',
	pagecode: '452001512A_card',
	list_pagecode: '452001512A_list',
	node_code: '4520016010',
	bill_type: '4A16',
	appid: '452001512A',
	printUrl: '/nccloud/aum/assign_return/printCard.do',
	printNodekey: null,
	// 编辑态按钮
	editBtns: [ 'Save', 'SaveCommit', 'Cancel', 'OpenCard', 'AddLine', 'DelLine', 'BatchAlter' ],
	browseBtns: [
		//'EditGroup',
		'Add',
		'Edit',
		'Delete',
		'Commit',
		'UnCommit',
		'Attachment',
		'QueryAboutBillFlow',
		'QueryAboutBusiness',
		'More',
		'OpenCard',
		'Print',
		'Refresh'
	],
	url: {
		assignEdit: '/nccloud/aum/assign_return/assignEdit.do',
		commit: '/nccloud/aum/assign_return/commit.do',
		query_card: '/nccloud/aum/assign_return/query_card.do',
		assignBatchAlter: '/nccloud/aum/assign_return/assignBatchAlter.do',
		query_card: '/nccloud/aum/assign_return/query_card.do',
		setDefaultValue: '/nccloud/aum/assign_return/setDefaultValue.do',
		insert: '/nccloud/aum/assign_return/insert.do',
		update: '/nccloud/aum/assign_return/update.do',
		delete: '/nccloud/aum/assign_return/delete.do',
		printUrl: '/nccloud/aum/assign_return/printCard.do',
		headAfterEdit: '/nccloud/aum/assign_return/headAfterEdit.do',
		bodyAfterEdit: '/nccloud/aum/assign_return/bodyAfterEdit.do'
	}
};
