import { pageConfig } from '../constants';
import ampub from 'ampub';

const { components, utils } = ampub;
const { orgChangeEvent } = components.OrgChangeEvent;
const { commonHeadAfterEvent, commonBodyAfterEvent } = utils.cardUtils;

const { formId, tableId, pagecode } = pageConfig;

/**
 * 表头编辑后事件处理
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} oldValue 
 */
export function headAfterEvent(props, moduleId, key, value, oldValue) {
	if (moduleId == formId) {
		if ('pk_org_v' == key) {
			let callback = () => {
				//如果资产组织有值，默认表体增加一行。
				let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v').value;
				if (pk_org_v) {
					props.cardTable.addRow(tableId, undefined, undefined, false);
				}
				let config = {
					afterEditUrl: pageConfig.url.headAfterEdit,
					pagecode,
					key,
					value,
					oldValue
				};
				commonHeadAfterEvent.call(this, props, config);
			};
			orgChangeEvent.call(
				this,
				props,
				pagecode,
				formId,
				tableId,
				key,
				value,
				oldValue,
				[],
				[ 'AddLine', 'BatchAlter' ],
				callback
			);
		}
	}
}

/**
 * 表体编辑后事件处理
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedrows 
 * @param {*} index 
 * @param {*} record 
 * @param {*} type 
 * @param {*} method 
 */
export function bodyAfterEvent(props, moduleId, key, value, changedRows, index, record, type, method) {
	if (moduleId == tableId) {
		let config = {
			afterEditUrl: pageConfig.url.bodyAfterEdit,
			pagecode,
			key,
			record,
			changedRows,
			index,
			formId,
			tableId,
			moduleId,
			// 修改[设备，使用权，库存组织，使用人]的时候
			keys: [ 'pk_equip', 'pk_unit_used_after_v', 'pk_icorg_after_v', 'pk_user_after' ]
		};
		commonBodyAfterEvent.call(this, props, config);
	}
}
