import { pageConfig } from '../constants';
import { init } from './buttonClick';
import ampub from 'ampub';

const { components, commonConst, utils } = ampub;
const { defRefCondition } = components.refInit;
const { openEquipCardByPk } = components.queryAboutUtils;
const { getContext, loginContextKeys, loginContext } = components.LoginContext;
const { IBusiRoleConst } = commonConst.CommonKeys;
const { getMultiLangByID } = utils.multiLangUtils;
const { afterModifyCardMeta } = utils.cardUtils;

const { pagecode, formId, tableId } = pageConfig;
export default function(props) {
	props.createUIDom(
		{
			pagecode: pagecode //页面id
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
				if (data.button) {
					let buttons = data.button;
					props.button.setButtons(buttons);
				}
				if (data.template) {
					let meta = data.template;
					modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta, () => {
						init.call(this, props);
					});
				}
			}
		}
	);
}

function modifierMeta(props, meta) {
	let status = props.getUrlParam('status');
	meta[formId].status = status;
	meta[tableId].status = status;
	let pk_group = getContext(loginContextKeys.groupId);

	//给表头设置人员参照
	meta[formId].items.map((item) => {
		// 资产组织过滤
		if (item.attrcode == 'pk_org_v') {
			item.queryCondition = () => {
				return { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgVidRefFilter' };
			};
		} else if (item.attrcode == 'pk_recorder' || item.attrcode == 'pk_taker') {
			item.queryCondition = () => {
				//应该取值pk_org
				let pk_org = props.form.getFormItemsValue(formId, 'pk_org');
				return { pk_org: pk_org.value, isShowUnit: true, busifuncode: IBusiRoleConst.ASSETORG };
			};
		} else {
			defRefCondition.call(this, props, item, formId, pk_group);
		}
	});

	//表体设备卡片参照设置
	meta[tableId].items.map((item) => {
		// 设备参照过滤
		setQueryCondition.call(this, props, item, pk_group);
		if (item.attrcode == 'pk_equip') {
			// 给资产编码列添加超链接
			item.renderStatus = 'browse';
			item.render = (text, record, index) => {
				return (
					<div class="card-table-browse">
						<span
							className="code-detail-link"
							onClick={() => {
								openEquipCardByPk(props, record.values.pk_equip.value);
							}}
						>
							{record.values.pk_equip && record.values.pk_equip.display}
						</span>
					</div>
				);
			};
		}
	});

	let porCol = {
		attrcode: 'opr',
		label: getMultiLangByID('amcommon-000000') /* 国际化处理： 操作*/,
		visible: true,
		className: 'table-opr',
		width: 150,
		fixed: 'right',
		itemtype: 'customer',
		render(text, record, index) {
			let btnAry = props.getUrlParam('status') === 'browse' ? [ 'OpenCard' ] : [ 'OpenCard', 'DelLine' ];

			return props.button.createOprationButton(btnAry, {
				area: 'card_body_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => {
					switch (key) {
						case 'DelLine':
							props.cardTable.delRowsByIndex(tableId, index);
							break;
						case 'OpenCard':
							let status = props.cardTable.getStatus(tableId);
							if (status == 'edit') {
								props.cardTable.openModel(tableId, 'edit', record, index);
							} else {
								props.cardTable.toggleRowView(tableId, record);
							}
							break;
					}
				}
			});
		}
	};
	meta[tableId].items.push(porCol);
	// 卡片模板修改后公共处理
	meta = afterModifyCardMeta.call(this, props, meta, { tableId });

	return meta;
}
// 设备参照
function setQueryCondition(props, item, pk_group) {
	if (item.attrcode == 'pk_equip') {
		// 设备卡片参照设置
		item.isMultiSelectedEnabled = true;
		item.queryCondition = () => {
			let pk_org = props.form.getFormItemsValue(formId, 'pk_org');
			let bill_type = props.form.getFormItemsValue(formId, 'bill_type');
			let transi_type = props.form.getFormItemsValue(formId, 'transi_type');
			//设备卡片根据表头的主组织过滤，必须是已借用的
			return {
				bill_type: bill_type.value,
				transi_type: transi_type.value,
				pk_org: pk_org.value,
				GridRefActionExt: 'nccloud.web.aum.restore.assign.refcondition.PK_EQUIPSqlBuilder'
			};
		};
	} else {
		defRefCondition.call(this, props, item, formId, pk_group);
	}
}
