import React, { Component } from 'react';
import { createPage, base, high } from 'nc-lightapp-front';
import {
	modelSave,
	rowSelected,
	initTemplate,
	headAfterEvent,
	bodyAfterEvent,
	buttonClick,
	pageInfoClick,
	bodyBeforeEvent,
	backToList,
	getAssginUsedr,
	turnOff
} from './events';
import { pageConfig } from './constants';
import ampub from 'ampub';

const { components, utils } = ampub;
const { getContext, loginContextKeys } = components.LoginContext;
const { getMultiLangByID } = utils.multiLangUtils;
const { createCardTitleArea, createCardPaginationArea, getCommonTableHeadLeft } = utils.cardUtils;
const { closeBrowserUtils } = utils;
const { ApprovalTrans } = components;

const { NCAffix } = base;
const { title, formId, tableId, pagecode, bill_type, dataSource } = pageConfig;

class RestoreAssignCard extends Component {
	constructor(props) {
		super(props);
		this.state = {
			show: false,
			pk_bill: '',
			showApprove: false,
			compositedisplay: false,
			compositedata: {}
		};
		closeBrowserUtils.call(this, props, { form: [ formId ], cardTable: [ tableId ] });
		initTemplate.call(this, props);
	}
	componentDidMount() {}

	//获取列表肩部信息
	getTableHeadLeft = () => {
		return getCommonTableHeadLeft.call(this, this.props, { tableId });
	};

	getTableHead = () => {
		return (
			<div className="shoulder-definition-area">
				<div className="definition-icons">
					{this.props.button.createButtonApp({
						area: 'card_body',
						buttonLimit: 3,
						onButtonClick: buttonClick.bind(this)
					})}

					{this.props.cardTable.createBrowseIcons(tableId, {
						iconArr: [ 'close', 'open', 'max', 'setCol' ],
						maxDestAreaId: 'nc-bill-card'
					})}
				</div>
			</div>
		);
	};

	render() {
		let { cardTable, form, ncmodal, ncUploader } = this.props;
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createModal } = ncmodal;
		const { BillTrack, ApproveDetail } = high;

		let { createNCUploader } = ncUploader;

		return (
			<div id="nc-bill-card" className="nc-bill-card">
				<div className="nc-bill-top-area">
					<NCAffix>
						<div className="nc-bill-header-area">
							<div className="header-title-search-area">
								{createCardTitleArea.call(this, this.props, {
									title: getMultiLangByID(title),
									formId,
									backBtnClick: backToList
								})}
							</div>
							<div className="header-button-area">
								{this.props.button.createButtonApp({
									area: 'card_head',
									buttonLimit: 3,
									onButtonClick: buttonClick.bind(this),
									popContainer: document.querySelector('.header-button-area')
								})}
							</div>
							{createCardPaginationArea.call(this, this.props, {
								formId,
								dataSource,
								pageInfoClick
							})}
						</div>
					</NCAffix>
					<div className="nc-bill-form-area">
						{createForm(formId, { onAfterEvent: headAfterEvent.bind(this) })}
					</div>
				</div>
				<div className="nc-bill-bottom-area">
					<div className="nc-bill-table-area">
						{createCardTable(tableId, {
							tableHeadLeft: this.getTableHeadLeft.bind(this),
							tableHead: this.getTableHead.bind(this),
							modelSave: modelSave.bind(this),
							showIndex: true,
							showCheck: true,
							isAddRow: true,
							onAfterEvent: bodyAfterEvent.bind(this),
							onBeforeEvent: bodyBeforeEvent.bind(this),
							selectedChange: rowSelected.bind(this)
						})}
					</div>
				</div>
				<BillTrack
					show={this.state.show}
					close={() => {
						this.setState({ show: false });
					}}
					pk={this.state.pk_bill} //单据id
					type={bill_type} //单据类型
				/>
				{/* 审批详情 */}
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={getContext(loginContextKeys.transtype)}
					billid={this.state.pk_bill}
				/>
				{/* 附件区 */}
				{createNCUploader('warcontract-uploader', {})}
				{/* 确认取消框 */}
				{createModal(`${pagecode}-confirm`, { color: 'warning' })}
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002')} /* 国际化处理： 指派*/
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={getAssginUsedr.bind(this)}
						cancel={turnOff.bind(this)}
					/>
				) : (
					''
				)}
			</div>
		);
	}
}

RestoreAssignCard = createPage({
	billinfo: {
		billtype: 'card',
		pagecode: pagecode,
		headcode: formId,
		bodycode: tableId
	}
})(RestoreAssignCard);

export default RestoreAssignCard;
