/**
 * 借用归还常量
 */
export const sameConfig = {
	pkField: 'pk_borrow',
	dataSource: 'aum.restore.borrow.main',
	cardRouter: '/card',
	listRouter: '/list',
	title: '452001508A-000000' /* 国际化处理： 借用归还*/,
	printFilename: '452001508A-000001' /* 国际化处理： 借用归还*/
};
