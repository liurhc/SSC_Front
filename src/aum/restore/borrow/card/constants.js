/**
 * 借用归还卡片态常量
 */
import { sameConfig } from '../const';
export const pageConfig = {
	...sameConfig,
	formId: 'card_head',
	tableId: 'card_body_bodyvos',
	tableIdEdit: 'card_body_bodyvos_edit',
	list_pagecode: '452001508A_list',
	pagecode: '452001508A_card',
	node_code: '4520012010',
	bill_type: '4A17',
	appid: '452001508A',
	printNodekey: null,
	// 编辑态按钮
	editBtns: [ 'Save', 'SaveCommit', 'Cancel', 'OpenCard', 'AddLine', 'DelLine', 'BatchAlter' ],
	browseBtns: [
		//'EditGroup',
		'Add',
		'Edit',
		'Delete',
		'Commit',
		'UnCommit',
		'Attachment',
		'QueryAboutBillFlow',
		'QueryAboutBusiness',
		'More',
		'OpenCard',
		'Print',
		'Refresh'
	],
	url: {
		borrowEdit: '/nccloud/aum/borrow_return/borrowEdit.do',
		commit: '/nccloud/aum/borrow_return/commit.do',
		query_card: '/nccloud/aum/borrow_return/query_card.do',
		borrowBatchAlter: '/nccloud/aum/borrow_return/borrowBatchAlter.do',
		query_card: '/nccloud/aum/borrow_return/query_card.do',
		setDefaultValue: '/nccloud/aum/borrow_return/setDefaultValue.do',
		insert: '/nccloud/aum/borrow_return/insert.do',
		update: '/nccloud/aum/borrow_return/update.do',
		delete: '/nccloud/aum/borrow_return/delete.do',
		printUrl: '/nccloud/aum/borrow_return/printCard.do',
		headAfterEdit: '/nccloud/aum/borrow_return/headAfterEdit.do',
		bodyAfterEdit: '/nccloud/aum/borrow_return/bodyAfterEdit.do'
	}
};
