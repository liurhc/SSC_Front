import { pageConfig } from '../constants';
import ampub from 'ampub';

const { commonConst, utils } = ampub;
const { UISTATE } = commonConst.StatusUtils;
const { setHeadAreaData } = utils.cardUtils;

let { editBtns, browseBtns, formId, tableId } = pageConfig;
/**
 * 设置按钮的可见性
 * @param {*} props 
 * @param {*} status 
 */
export function setBtnVisibleByOper(props, status) {
	switch (status) {
		case UISTATE.add:
			props.form.setFormItemsDisabled(formId, {
				pk_org_v: false,
				pk_org: false
			});
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, UISTATE.edit);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(browseBtns, false);
			props.button.setButtonVisible(editBtns, true);
			break;
		case UISTATE.edit:
			props.form.setFormItemsDisabled(formId, {
				pk_org_v: true,
				pk_org: true
			});
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, status);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(browseBtns, false);
			props.button.setButtonVisible(editBtns, true);
			break;
		case UISTATE.browse:
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, status);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(editBtns, false);
			props.button.setButtonVisible(browseBtns, true);
			break;
		case UISTATE.blank:
			props.form.setFormStatus(formId, UISTATE.browse);
			props.cardTable.setStatus(tableId, UISTATE.browse);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(editBtns, false);
			props.button.setButtonVisible(browseBtns, false);
			props.button.setButtonVisible([ 'Add' ], true);
			break;
		default:
			break;
	}
	setHeadAreaData.call(this, props, { status });
}
