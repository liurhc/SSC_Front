import { pageConfig } from '../constants';
import ampub from 'ampub';

const { commonConst } = ampub;
const { IBusiRoleConst } = commonConst.CommonKeys;

const { tableId, formId, tableIdEdit } = pageConfig;

export default function bodyBeforeEvent(props, moduleId, key, value, index, record) {
	refCondtion.call(this, props, moduleId, key, value, index, record);
	if (key == 'pk_warehouse_in') {
		// 库存组织为空时，归还仓库不可编辑，否则可编辑
		if (record.values.pk_icorg_after_v.value) {
			props.cardTable.setEditableByIndex(tableId, index, 'pk_warehouse_in', true);
		} else {
			props.cardTable.setEditableByIndex(tableId, index, 'pk_warehouse_in', false);
		}
	}
	return true;
}
//表体字段参照处理
function refCondtion(props, moduleId, key, value, index, record) {
	let meta = props.meta.getMeta();
	meta[moduleId].items.map((item) => {
		setQueryCondition.call(this, props, item, record);
	});
	meta[tableIdEdit].items.map((item) => {
		setQueryCondition.call(this, props, item, record);
	});
	props.meta.setMeta(meta);
}
// 设备参照
function setQueryCondition(props, item, record) {
	// 设置使用权的参照
	if (item.attrcode == 'pk_unit_used_after_v') {
		item.queryCondition = () => {
			let pk_org = props.form.getFormItemsValue(formId, 'pk_org');
			return {
				pk_org: pk_org.value,
				TreeRefActionExt: 'nccloud.web.aum.restore.borrow.refcondition.PK_UNIT_USED_AFTER_VSqlBuilder'
			};
		};
	}
	// 设置使用部门的参照
	if (item.attrcode == 'pk_usedept_after_v') {
		item.queryCondition = () => {
			let pk_unit_used_after = record.values && record.values.pk_unit_used_after;
			return {
				pk_org: pk_unit_used_after && pk_unit_used_after.value,
				busifuncode: IBusiRoleConst.ASSETORG
			};
		};
	}
	// 设置使用人的参照
	if (item.attrcode == 'pk_user_after') {
		item.queryCondition = () => {
			let pk_org = record.values && record.values.pk_unit_used_after;
			return {
				pk_org: pk_org && pk_org.value,
				busifuncode: IBusiRoleConst.ASSETORG
			};
		};
	}
	// 库存组织参照
	if (item.attrcode == 'pk_icorg_after_v') {
		item.queryCondition = () => {
			let pk_group = props.form.getFormItemsValue(formId, 'pk_group');
			return {
				pk_group: pk_group.value
			};
		};
	}
	// 归还仓库参照
	if (item.attrcode == 'pk_warehouse_in') {
		item.queryCondition = () => {
			return {
				pk_org: (record.values.pk_icorg_after || {}).value,
				GridRefActionExt: 'nccloud.web.ampub.common.refCodition.StoreDocSqlBuilder'
			};
		};
	}
	// 项目参照
	if (item.attrcode == 'pk_jobmngfil') {
		item.queryCondition = () => {
			let pk_org = props.form.getFormItemsValue(formId, 'pk_org');
			let pk_group = props.form.getFormItemsValue(formId, 'pk_group');
			return {
				pk_org: pk_org.value,
				pk_group: pk_group.value
			};
		};
	}
	// 设置归还位置的参照设置
	if (item.attrcode == 'pk_location_after') {
		item.queryCondition = () => {
			let pk_org = props.form.getFormItemsValue(formId, 'pk_org');
			return {
				pk_org: pk_org.value
			};
		};
	}
}
