import { loadDataByPk, clearData } from './buttonClick';
export default function pageInfoClick(props, pk) {
	// 更新参数
	props.setUrlParam({ id: pk });
	//清空数据
	clearData.call(this, props);
	loadDataByPk.call(this, props, pk);
}
