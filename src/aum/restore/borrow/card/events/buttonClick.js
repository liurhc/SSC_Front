import { ajax, print, cardCache, output } from 'nc-lightapp-front';
import { pageConfig } from '../constants';
import { setBtnVisibleByOper } from './setBtnVisible';
import ampub from 'ampub';

const { components, commonConst, utils } = ampub;
const { openApprove } = components.queryAboutUtils;
const { getContext, loginContextKeys } = components.LoginContext;
const { UISTATE } = commonConst.StatusUtils;
const { showConfirm, MsgConst, showMessage } = utils.msgUtils;
const { setBillFlowBtnsVisible, setCardValue } = utils.cardUtils;
const { beforeSaveValidator } = components.saveValidatorsUtil;
const { getScriptCardReturnData } = components.ScriptReturnUtils;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

const {
	tableId,
	formId,
	pagecode,
	dataSource,
	pkField,
	printNodekey,
	node_code,
	bill_type,
	listRouter,
	printFilename
} = pageConfig;
export default function(props, id) {
	switch (id) {
		case 'AddLine':
			addLineClick.call(this, props);
			break;
		case 'DelLine':
			delLineClick.call(this, props);
			break;
		case 'BatchAlter':
			batchAlter.call(this, props);
			break;
		case 'Add':
			addClick.call(this, props);
			break;
		case 'Save':
			doSave.call(this, props);
			break;
		case 'Edit':
			editClick.call(this, props);
			break;
		case 'Cancel':
			cancelConfirm.call(this, props);
			break;
		case 'Delete':
			deleteConfirm.call(this, props);
			break;
		case 'Commit':
			this.commitType = 'commit';
			unOrCmmitClick.call(this, props, 'SAVE', 'commit');
			break;
		case 'UnCommit':
			unOrCmmitClick.call(this, props, 'UNSAVE');
			break;
		case 'SaveCommit':
			this.commitType = 'saveCommit';
			unOrCmmitClick.call(this, props, 'SAVE', 'saveCommit');
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'QueryAboutBusiness':
			openBillTrack.call(this, props);
			break;
		case 'QueryAboutBillFlow':
			queryAboutBillFlow.call(this, props);
			break;
		case 'Refresh':
			refreshCard.call(this, props);
			break;
		case 'Attachment':
			attachmentClick.call(this, props);
			break;
		default:
			break;
	}
}

/**
 * 新增点击事件处理
 * @param {*} props 
 */
function addClick(props) {
	props.cardTable.closeExpandedRow(tableId);
	clearData.call(this, props);
	setDefaultValue.call(this, props);
	toggleShow.call(this, props, UISTATE.add);
	// 设置字段的可编辑性
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	if (!pk_org_v || !pk_org_v.value) {
		props.initMetaByPkorg('pk_org_v');
		props.button.setButtonDisabled([ 'AddLine' ], true);
	}
}

/**
 * 修改点击事件处理
 * @param {*} props 
 */
function editClick(props) {
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	if (!pk) {
		pk = props.getUrlParam('id');
	}
	if (!pk) {
		return;
	}
	props.cardTable.closeExpandedRow(tableId);
	let data = {
		pk,
		resourceCode: node_code
	};
	ajax({
		url: pageConfig.url.borrowEdit,
		data,
		success: (res) => {
			if (res.data) {
				showMessage.call(this, props, { color: 'warning', content: res.data });
			} else {
				toggleShow.call(this, props, UISTATE.edit);
			}
		},
		error: (res) => {
			if (res && res.message) {
				showMessage.call(this, props, { color: 'warning', content: res.message });
			}
		}
	});
}
/**
 * 删行按钮，批量删除
 * @param {*} props 
 * @param {*} tableId 
 */
function delLineClick(props) {
	//选中的数据
	let checkedRows = props.cardTable.getCheckedRows(tableId);
	let checkedIndex = [];
	checkedRows.map((item) => {
		checkedIndex.push(item.index);
	});
	props.cardTable.delRowsByIndex(tableId, checkedIndex);
}
/**
 * 取消保存按钮点击事件
 * @param {*} props 
 * @param {*} formId 
 * @param {*} tableId 
 * @param {*} bill_type 
 */
function cancelClick(props) {
	props.form.cancel(formId);
	props.cardTable.resetTableData(tableId);
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	// 加载编辑前数据，如果编辑前没有数据，则加载当前列表最后一条数据，如果还没有，显示为空不处理
	if (!pk) {
		pk = cardCache.getCurrentLastId(dataSource);
	}
	if (pk) {
		props.setUrlParam({ status: UISTATE.browse });
		loadDataByPk.call(this, props, pk);
	} else {
		setValue.call(this, props, null);
		toggleShow.call(this, props, UISTATE.blank);
	}
}

/**
 * 通过单据id获取缓存单据信息，没有缓存则重新查询
 * @param {*} props 
 * @param {*} pk 
 */
export function loadDataByPk(props, pk) {
	let cachData = cardCache.getCacheById(pk, dataSource);
	if (cachData) {
		setValue.call(this, props, cachData);
	} else {
		getDataById.call(this, props, pk);
	}
}

/**
 * 提交、收回处理
 * @param {*} props 
 * @param {*} OperatorType 
 * @param {*} commitType 
 */
function unOrCmmitClick(props, OperatorType, commitType, content) {
	if (commitType == 'saveCommit') {
		let pass = beforeSaveValidator.call(this, props, formId, tableId, []);
		if (!pass) {
			return;
		}
		if (content) {
			doCmmit.call(this, props, OperatorType, commitType, content);
		} else {
			//取主子表数据
			let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
			props.validateToSave(cardData, () => {
				doCmmit.call(this, props, OperatorType, commitType, content);
			});
		}
	} else {
		doCmmit.call(this, props, OperatorType, commitType, content);
	}
}

/**
 * 提交、收回处理
 * @param {*} props 
 * @param {*} OperatorType 
 * @param {*} commitType 
 * @param {*} content 
 * @param {*} cardData 
 */
function doCmmit(props, OperatorType, commitType, content) {
	//取主子表数据
	let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	//单据主键
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	//向后台传递数据
	let paramInfoMap = {}; //键值对  pk = ts
	paramInfoMap[pk] = ''; //卡片页面ts为空也可以 CardData 里有ts
	let obj = {
		dataType: 'cardData',
		OperatorType: OperatorType,
		pageid: pagecode,
		paramInfoMap: paramInfoMap,
		content
	};
	if (commitType) {
		obj.commitType = commitType;
	}
	cardData.userjson = JSON.stringify(obj);
	//提交
	ajax({
		url: pageConfig.url.commit,
		data: cardData,
		success: (res) => {
			if (content) {
				this.setState({
					compositedisplay: false
				});
			}

			let callBack = () => {
				//设置状态
				toggleShow.call(this, props, UISTATE.browse);
			};

			getScriptCardReturnData.call(
				this,
				res,
				props,
				formId,
				tableId,
				pkField,
				dataSource,
				setValue4Commit,
				false,
				callBack,
				pagecode
			);
		}
	});
}

/**
 * 打印
 * @param {*} props 
 */
export function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		pageConfig.url.printUrl, // 后台打印服务url
		printData
	);
}

/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });
		return;
	}
	output({
		url: pageConfig.url.printUrl,
		data: printData
	});
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let pk = props.form.getFormItemsValue(formId, pkField);
	if (!pk || !pk.value) {
		return false;
	}
	let pks = [ pk.value ];
	let printData = {
		filename: getMultiLangByID(printFilename), // 文件名称
		nodekey: printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

//卡片界面刷新
function refreshCard(props) {
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	getDataById.call(this, props, pk, true);
}

/**
 * 表体批改处理
 * @param {*} props 
 */
function batchAlter(props) {
	// 获取表体行有效数据的行数
	let visibleRows = props.cardTable.getVisibleRows(tableId);
	// 如果没有数据或只有一条数据，直接返回
	if (visibleRows.length < 2) {
		return;
	}

	// 获取原始数据
	let changeData = props.cardTable.getTableItemData(tableId);

	// 获取卡片的数据
	let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);

	ajax({
		url: pageConfig.url.borrowBatchAlter,
		data: {
			card: cardData,
			batchChangeIndex: changeData.batchChangeIndex, // 原始数据行号
			batchChangeKey: changeData.batchChangeKey, // 原始数据key
			batchChangeValue: changeData.batchChangeValue, // 原始数据value
			// 原始数据所在的vo，对应聚合VO中的tableCodes，一主一子可为空，一主多子要传对应的值
			tableCode: ''
		},
		success: (res) => {
			let { success, data } = res;
			if (success) {
				//把数据设置到界面上
				setValue.call(this, props, data);
			}
		},
		error: (res) => {
			if (res && res.message) {
				showMessage.call(this, props, { color: 'warning', content: res.message });
			}
		}
	});
}

/**
 * 附件
 * @param {*} props 
 */
function attachmentClick(props) {
	// billNo 是单据主键
	let billId = props.form.getFormItemsValue(formId, 'pk_borrow').value;
	props.ncUploader.show('warcontract-uploader', {
		billId: 'pam/borrow/' + billId
	});
}

/**
 * 通过主键获取单据数据
 * @param {*} props 
 * @param {*} pk 
 * @param {*} status 
 */
export function getDataById(props, pk, isRefresh = false, isListEdit = false) {
	ajax({
		url: pageConfig.url.query_card,
		data: {
			pk: pk,
			pagecode: pagecode
		},
		success: (res) => {
			if (res.data) {
				cardCache.updateCache(pkField, pk, res.data, formId, dataSource);
			} else {
				cardCache.deleteCacheById(pkField, pk, dataSource);
				// 没数据，提示数据已被删除
				showMessage.call(this, props, { type: MsgConst.Type.DataDeleted });
			}
			if (isRefresh) {
				showMessage.call(this, props, { type: MsgConst.Type.RefreshSuccess });
			}
			//返回的数据设置到界面上
			setValue.call(this, props, res.data, isListEdit);
		}
	});
}

/**
 * 设置表头、表体数据
 * @param {*} props 
 * @param {*} data 
 */
export function setValue(props, data, isListEdit = false, isSave = false) {
	let status = props.getUrlParam('status');
	if (isListEdit || isSave) {
		props.form.setFormStatus(formId, UISTATE.browse);
	} else {
		//根据status设置按钮的显示
		setBtnVisibleByOper.call(this, props, status);
	}
	setCardValue.call(this, props, data, formId, tableId);
	if (isListEdit || !isSave) {
		//根据status设置按钮的显示
		setBtnVisibleByOper.call(this, props, status);
	}
	// 根据单据状态设置按钮可见性
	setBillFlowBtnsVisible.call(this, props, formId, pkField);
}

/**
 * 模板加载后初期化处理
 */
export function init(props) {
	//根据状态和id值加载页面数据
	let status = props.getUrlParam('status');
	if (status != UISTATE.add) {
		let pk = props.getUrlParam('id');
		if (pk) {
			getDataById.call(this, props, pk, false, true);
		}
	} else {
		setDefaultValue.call(this, props);
	}

	if (status === UISTATE.add) {
		// 设置字段的可编辑性
		let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
		if (!pk_org_v || !pk_org_v.value) {
			props.initMetaByPkorg('pk_org_v');
			props.button.setButtonDisabled([ 'AddLine' ], true);
		}
	}
}

/**
 * 新增设置默认值
 * @param {*} props 
 */
function setDefaultValue(props) {
	// 公共默认值处理
	// 集团
	let groupId = getContext(loginContextKeys.groupId);
	let groupName = getContext(loginContextKeys.groupName);
	// 交易类型
	let transi_type = getContext(loginContextKeys.transtype);
	let pk_transitype = getContext(loginContextKeys.pk_transtype);
	// 默认主组织
	let pk_org = getContext(loginContextKeys.pk_org);
	let org_Name = getContext(loginContextKeys.org_Name);
	let pk_org_v = getContext(loginContextKeys.pk_org_v);
	let org_v_Name = getContext(loginContextKeys.org_v_Name);
	props.form.setFormItemsValue(formId, {
		pk_group: { value: groupId, display: groupName },
		pk_org: { value: pk_org, display: org_Name },
		pk_org_v: { value: pk_org_v, display: org_v_Name },
		bill_type: { value: bill_type },
		transi_type: { value: transi_type },
		pk_transitype: { value: pk_transitype }
	});
	//设置单据默认值
	let data = props.createMasterChildData(pagecode, formId, tableId);
	data.body = null;
	ajax({
		url: pageConfig.url.setDefaultValue,
		data,
		success: (res) => {
			//把数据设置到界面上
			setValue.call(this, props, res.data);
			//如果资产组织有值，新增时默认表体增加一行。
			let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v').value;
			if (pk_org_v) {
				props.cardTable.addRow(tableId, undefined, undefined, false);
			} else {
				// 初始进来的时候，资产组织没值时，批改不可用
				props.button.setButtonDisabled([ 'BatchAlter' ], true);
			}
			// 设置表体删除按钮的可用性
			setDelLineBtnDisable.call(this, props, tableId);
		},
		error: (res) => {
			if (res && res.message) {
				showMessage.call(this, props, { color: 'warning', content: res.message });
			}
		}
	});
}

/**
 * 页面状态切换处理
 * @param {*} props 
 * @param {*} status 
 */
export function toggleShow(props, status) {
	props.setUrlParam({ status });
	setBtnVisibleByOper.call(this, props, status);
	// 根据单据状态设置按钮可见性
	setBillFlowBtnsVisible.call(this, props, formId, pkField);
}

/**
 * 保存处理
 * @param {*} props 
 */
export function doSave(props, doCloseModel = false) {
	//校验表头必输项
	let pass = beforeSaveValidator.call(this, props, formId, tableId, []);
	if (!pass) {
		return;
	}
	let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);

	let status = props.getUrlParam('status');
	let url = pageConfig.url.insert;
	if (UISTATE.edit === status) {
		//修改保存
		url = pageConfig.url.update;
	}

	//调用Ajax保存数据
	props.validateToSave(cardData, () => {
		ajax({
			url: url,
			data: cardData,
			success: (res) => {
				if (res.success) {
					// 关闭侧拉
					if (doCloseModel) {
						props.cardTable.closeModel(tableId);
					}
					afterSave.call(this, props, res.data, status);
				}
			}
		});
	});
}

/**
 * 保存后处理
 * @param {*} props 
 * @param {*} data 
 * @param {*} oldstatus 
 */
function afterSave(props, data, oldstatus) {
	props.setUrlParam({ status: UISTATE.browse });
	let pk = '';
	if (data && data.head && data.head[formId] && data.head[formId].rows && data.head[formId].rows[0].values) {
		pk = data.head[formId].rows[0].values[pkField].value;
	}
	// 页面设值
	setValue.call(this, props, data, false, true);
	// 保存成功后处理缓存
	let cacheData = props.createMasterChildData(pagecode, formId, tableId);
	if (oldstatus == UISTATE.add) {
		cardCache.addCache(pk, cacheData, formId, dataSource);
	} else {
		cardCache.updateCache(pkField, pk, cacheData, formId, dataSource);
	}
	//设置按钮的显示
	toggleShow.call(this, props, UISTATE.browse);
	showMessage.call(this, props, { type: MsgConst.Type.SaveSuccess });
}

/**
 * 删除弹框提示
 * @param {*} props 
 */
function deleteConfirm(props) {
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	if (!pk) {
		return;
	}
	showConfirm.call(this, props, { type: MsgConst.Type.Delete, beSureBtnClick: delReal });
}
/**
 * 删除处理
 * @param {*} props 
 */
function delReal(props) {
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let ts = props.form.getFormItemsValue(formId, 'ts').value;
	let paramInfoMap = {};
	paramInfoMap[pk] = ts;
	ajax({
		url: pageConfig.url.delete,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: pagecode
		},
		success: (res) => {
			getScriptCardReturnData.call(
				this,
				res,
				props,
				formId,
				tableId,
				pkField,
				dataSource,
				setValue4Commit,
				true,
				loadDataByPk4Commit
			);
		},
		error: (res) => {
			showMessage.call(this, props, { color: 'warning', content: res.message });
		}
	});
}

/**
 * 整单保存处理
 * @param {*} props 
 */
export function modelSave(props) {
	doSave.call(this, props, true);
}

/**
 * 清除表头、表体数据
 * @param {*} props 
 */
export function clearData(props) {
	// 清空数据
	props.form.EmptyAllFormValue(formId);
	// 清除表体数据
	props.cardTable.setTableData(tableId, { rows: [] }, null, false);
}

/**
 * 设置删行按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setDelLineBtnDisable(props, moduleId) {
	let num = props.cardTable.getNumberOfRows(moduleId);
	let checkedRows = [];
	if (num > 0) {
		checkedRows = props.cardTable.getCheckedRows(moduleId);
	}
	props.button.setButtonDisabled([ 'DelLine' ], !(checkedRows && checkedRows.length > 0));
}

/**
 * 表体加行
 * @param {*} props 
 */
export function addLineClick(props) {
	props.cardTable.addRow(tableId);
}
/**
 * 取消提示框
 * @param {} props 
 */
function cancelConfirm(props) {
	showConfirm.call(this, props, { type: MsgConst.Type.Cancel, beSureBtnClick: cancelClick });
}

//单据追溯
function openBillTrack(props) {
	this.setState({
		show: true,
		pk_bill: props.form.getFormItemsValue(formId, pkField).value
	});
}

//返回列表界面
export function backToList(props) {
	props.pushTo(listRouter, { pagecode: pagecode.replace('card', 'list') });
}
//提交及指派 回调，注意保存提交时候需要调用保存提交的方法
export function getAssginUsedr(content) {
	if (this.commitType == 'commit') {
		unOrCmmitClick.call(this, this.props, 'SAVE', 'commit', content); //原提交方法添加参数content
	} else {
		unOrCmmitClick.call(this, this.props, 'SAVE', 'saveCommit', content);
	}
}
//取消 指派
export function turnOff() {
	this.setState({
		compositedisplay: false
	});
}

/**
 * 提交收回后刷新值
 * @param {*} data 
 */
function setValue4Commit(data) {
	setValue.call(this, this.props, data);
}

/**
 * 删除后加载新数据
 * @param {*} pk 
 */
function loadDataByPk4Commit(pk) {
	// 如果获取到的pk存在，加载新数据，不存在，画面值清空。
	if (pk) {
		loadDataByPk.call(this, this.props, pk);
	} else {
		setValue.call(this, this.props, null);
	}
}

/**
 * 审批详情处理
 * @param {*} props 
 */
function queryAboutBillFlow(props) {
	this.setState({
		pk_bill: props.form.getFormItemsValue(formId, pkField).value
	});
	openApprove(this);
}
