/**
 * 列表常量
 */
import { sameConfig } from '../const';
export const pageConfig = {
	...sameConfig,
	tableId: 'list_head',
	node_code: '4520012010',
	printNodekey: null,
	searchId: 'searchArea',
	pagecode: '452001508A_list',
	cardPageId: '452001508A_card',
	appid: '452001508A',
	commitOpr: 'SAVE',
	commitType: 'commit',
	unCommitOpr: 'UNSAVE',
	bill_type: '4A17',
	formId: 'card_head',
	url: {
		delete: '/nccloud/aum/borrow_return/delete.do',
		commit: '/nccloud/aum/borrow_return/commit.do',
		printUrl: '/nccloud/aum/borrow_return/printCard.do',
		query: '/nccloud/aum/borrow_return/query.do',
		borrowEdit: '/nccloud/aum/borrow_return/borrowEdit.do',
		querypage: '/nccloud/aum/borrow_return/querypage.do'
	}
};
