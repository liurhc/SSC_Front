import { ajax } from 'nc-lightapp-front';
import { pageConfig } from '../constants';
import ampub from 'ampub';

const { utils } = ampub;
const { setListValue } = utils.listUtils;

const { pagecode } = pageConfig;

export default function(props, config, pks) {
	let data = {
		allpks: pks,
		pagecode: pagecode
	};
	let url = pageConfig.url.querypage;
	//得到数据渲染到页面
	ajax({
		url,
		data,
		success: function(res) {
			setListValue.call(this, props, res);
		}
	});
}
