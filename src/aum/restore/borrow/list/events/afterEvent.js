import { pageConfig } from '../constants';
import ampub from 'ampub';

const { components } = ampub;
const { isMultiCorpRefHandler } = components.assetOrgMultiRefFilter;

const { searchId } = pageConfig;

export default function afterEvent(key, value) {
	if (key === 'pk_org') {
		isMultiCorpRefHandler.call(this, this.props, value, searchId, [ 'pk_recorder', 'pk_taker' ]);
	} else if (key === 'bodyvos.pk_equip.pk_usedunit') {
		isMultiCorpRefHandler.call(this, this.props, value, searchId, [
			'bodyvos.pk_equip.pk_usedept',
			'bodyvos.pk_equip.pk_user'
		]);
	} else if (key === 'bodyvos.pk_icorg_after') {
		isMultiCorpRefHandler.call(this, this.props, value, searchId, [ 'bodyvos.pk_warehouse_in' ]);
	} else if (key === 'bodyvos.pk_unit_used_after') {
		isMultiCorpRefHandler.call(this, this.props, value, searchId, [
			'bodyvos.pk_usedept_after',
			'bodyvos.pk_user_after'
		]);
	}
}
