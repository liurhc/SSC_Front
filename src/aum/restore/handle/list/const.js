// 小应用主键
export const pageConfig = {
	appid: '0001Z910000000007CNT',
	// 小应用名称
	title: '452001504A-000001' /* 国际化处理：归还处理*/,
	// 表格区域id
	tableId: 'list_head',
	// 表格节点编码
	pagecode: '452001504A_list',
	appcode: '452001504A',
	// 组织类型
	orgRefCode: 'uapbd/refer/org/AssetOrgGridRef/index.js',
	//归还人参照
	takerRefCode: 'uapbd/refer/psninfo/PsndocTreeGridRef/index.js',
	//领用单单据类型
	billtype_assign: '4A14',
	billname_assign: '452001504A-000002' /* 国际化处理：领用单*/,
	//借用单单据类型
	billtype_borrow: '4A15',
	billname_borrow: '452001504A-000003' /* 国际化处理：借用单*/,
	//设备卡片
	billtype_equip: '4A00',
	billname_equip: '452001504A-000004' /* 国际化处理：资产卡片*/
};

export const url = {
	//查询按钮url
	queryUrl: '/nccloud/aum/restore_hanle/query.do',
	//确认归还按钮url
	confirmUrl: '/nccloud/aum/restore_hanle/confirm.do',
	//编辑后事件url
	afterEventUrl: '/nccloud/aum/restore_hanle/afterEvent.do',
	//批改url
	batchAlterUrl: '/nccloud/aum/restore_hanle/batchAlter.do'
};

//行选中时，删除和确认归还按钮才可用
export const rowSelected = (props, moudleId) => {
	let checkedRows = props.editTable.getCheckedRows(moudleId);
	let batchBtns = [ 'DeleteLine', 'ConfirmReturn' ];
	props.button.setButtonDisabled(batchBtns, !(checkedRows && checkedRows.length > 0));
};
