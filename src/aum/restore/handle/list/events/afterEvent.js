import { ajax } from 'nc-lightapp-front';
import { url, pageConfig } from '../const';
import ampub from 'ampub';

const { utils } = ampub;
const { showMessage } = utils.msgUtils;
const { getMultiLangByID } = utils.multiLangUtils;

const { tableId, pagecode } = pageConfig;
export default function(props, moduleId, key, value, changedrows, index, record) {
	if (moduleId == tableId && key == 'pk_equip') {
		equipAfterEdit.call(this, props, key, value, index, changedrows);
	} else if (
		moduleId == tableId &&
		(key == 'pk_icorg_out' || //库存组织
		key == 'pk_unit_used_before_v' || //使用权
		key == 'pk_unit_used_before' ||
		key == 'pk_usedept_before_v' || //使用部门
			key == 'pk_usedept_before')
	) {
		if (key == 'pk_icorg_out') {
			//修改库存组织，把仓库值清空
			props.editTable.setValByKeyAndIndex(tableId, index, 'pk_warehouse_out', { display: null, value: null });
			//设置仓库字段为可编辑
			props.editTable.setEditableRowKeyByIndex(tableId, index, 'pk_warehouse_out', true);
		}
		bodyEditAfterEvent.call(this, props, key, value, index, changedrows, record);
	}
}

function createBodyAfterEventData(props, key, changedrows, index) {
	//先拿到选择的数据
	let rows = getVisibleRowsData.call(this, props, tableId);
	const data = {
		pageid: pagecode,
		model: {
			areaType: 'table',
			areacode: tableId,
			rows: rows
		}
	};

	return {
		attrcode: key,
		changedrows: changedrows,
		grid: data,
		index: index
	};
}

/**
 * 设备卡片的编辑后事件，要带出对应的领用单或者借用单，或者设备卡片数据
 * @param {*} props 
 * @param {*} key 
 * @param {*} value 
 * @param {*} index 
 * @param {*} changedrows 
 */
function equipAfterEdit(props, key, value, index, changedrows) {
	//先增行，再传后台进行数据处理
	let { afterEventUrl } = url;
	let {
		billtype_assign,
		billname_assign,
		billtype_borrow,
		billname_borrow,
		billtype_equip,
		billname_equip
	} = pageConfig;
	//如果清空某行的设备卡片，直接清空该行的其它值，返回
	let nullFlag = isClearPKEquip.call(this, props, changedrows, value, index);
	if (nullFlag) {
		return;
	}
	let number = parseInt(index);
	let changedIndexs = [];
	//先校验选择的设备是否和界面上已有的数据重复
	let flag = validatorEquipRepeat.call(this, props, value, index);
	if (!flag) {
		//把设备卡片的字段值清空
		props.editTable.setValByKeyAndIndex(tableId, index, 'pk_equip', { display: null, value: null });
		props.editTable.setValByKeyAndIndex(tableId, index, 'pk_equip.equip_name', { display: null, value: null });
		props.editTable.setValByKeyAndIndex(tableId, index, 'pk_equip.spec', { display: null, value: null });
		props.editTable.setValByKeyAndIndex(tableId, index, 'pk_equip.model', { display: null, value: null });
		return;
	}

	value.map((val, i) => {
		if (i == 0) {
			//由于多选之后，当前行的值是所选值得拼接，所以要重新赋值
			props.editTable.setValByKeyAndIndex(tableId, index, 'pk_equip', {
				value: val.refpk,
				display: val.refname
			});
		} else {
			let pk_equip = {
				value: val.refpk,
				display: val.refname
			};
			let config = { pk_equip };
			props.editTable.addRow(tableId, number, false, config);
		}
		// 保存新加的行号
		changedIndexs.push(number);
		number = number + 1;
	});

	let ajaxdata = createBodyAfterEventData(props, key, changedrows, index);
	//新增功能---差异化处理
	let newRecord = ajaxdata.grid.model.rows.filter((row, i) => {
		if (changedIndexs.includes(i)) {
			return row;
		}
	});
	ajaxdata.grid.model = {
		...ajaxdata.grid.model,
		//传入后台的数据是新加的行和编辑的当前行
		rows: newRecord
	};
	ajaxdata.index = 0;
	ajax({
		url: afterEventUrl,
		data: ajaxdata,
		success: (res) => {
			//向界面界面设置数据
			let { data } = res;
			if (data && data[tableId] && data[tableId].rows) {
				props.beforeUpdatePage();
				let changedRows = [];
				//把单据类型字段设值未单据名称
				data[tableId].rows.map((row, i) => {
					let bill_type = row.values.bill_type;
					if (bill_type && bill_type.value == billtype_assign) {
						row.values.bill_type = { value: getMultiLangByID(billname_assign) };
					} else if (bill_type && bill_type.value == billtype_borrow) {
						row.values.bill_type = { value: getMultiLangByID(billname_borrow) };
					} else if (bill_type && bill_type.value == billtype_equip) {
						row.values.bill_type = { value: getMultiLangByID(billname_equip) };
					}
					// 构造添加的行，用来更新数据
					changedRows.push({ index: changedIndexs[i], data: row });
				});
				//更新数据
				props.editTable.updateDataByIndexs(tableId, changedRows);

				props.editTable.setStatus(tableId, 'edit');
				props.updatePage(tableId, false);
			}
		}
	});
}

function isClearPKEquip(props, changeRows, value, index) {
	let retValue = false;
	let oldValue = changeRows[0].oldvalue.value;
	let newValue = changeRows[0].newvalue.value;
	if (oldValue && (oldValue != '' || oldValue != null) && (newValue == '' || newValue == null)) {
		//把设备卡片清空了
		retValue = true;
		let meta = props.meta.getMeta();
		meta[tableId].items.map((item) => {
			props.editTable.setValByKeyAndIndex(tableId, index, item.attrcode, { display: null, value: null });
		});
	}
	return retValue;
}
/**
 * 获取表体的数据，去除已经被删除的数据
 * @param {*} props 
 * @param {*} tableId 
 */
export function getVisibleRowsData(props, tableId) {
	let rows = props.editTable.getAllRows(tableId, true);
	return rows.filter((item) => item.status != 3);
}

/**
 * 校验选择的设备卡片是否和界面上的已有的重复
 * @param {*} props 
 * @param {*} value 
 * @param {*} index 
 */
function validatorEquipRepeat(props, value, index) {
	let exsitRows = getVisibleRowsData.call(this, props, tableId);
	let repeatNum = [];
	value.map((val) => {
		let repeat = exsitRows.findIndex((row, i) => {
			return val.refpk == row.values.pk_equip.value && i != index;
		});
		if (repeat != -1) {
			repeatNum.push(repeat);
		}
	});
	if (repeatNum && repeatNum.length > 0) {
		let rowNums = '';
		repeatNum.map((num, i) => {
			rowNums = rowNums + (num + 1);
			if (i < repeatNum.length - 1) {
				rowNums = rowNums + ',';
			}
		});
		showMessage.call(this, props, {
			content: getMultiLangByID('452001504A-000005', { rowNums }),
			color: 'warning'
		}); /* 国际化处理： 选择的资产卡片和第{rowNums}行重复，请重新选择！*/
		return false;
	}
	return true;
}

function bodyEditAfterEvent(props, key, value, index, changedrows, record) {
	let { tableId } = pageConfig;
	let { afterEventUrl } = url;
	let ajaxdata = createBodyAfterEventData(props, key, changedrows, index);
	//新增功能---差异化处理
	let newRecord = JSON.parse(JSON.stringify(record || {}));
	ajaxdata.grid.model = {
		...ajaxdata.grid.model,
		rows: [ newRecord ]
	};
	// 缓存编辑的第几行
	let tempIndex = ajaxdata.index;
	ajaxdata.index = 0; //修改编辑行为0

	ajax({
		url: afterEventUrl,
		data: ajaxdata,
		success: (res) => {
			let { data } = res;

			if (data && data[tableId] && data[tableId].rows) {
				props.beforeUpdatePage();
				// 表体编辑单行，更新单行数据
				props.editTable.updateDataByIndexs(tableId, [ { index: tempIndex, data: data[tableId].rows[0] } ]);
				props.editTable.setStatus(tableId, 'edit');
				props.updatePage(tableId, false);
			}
		}
	});
}
