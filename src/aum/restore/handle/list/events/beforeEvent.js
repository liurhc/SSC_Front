import { pageConfig } from '../const';
const { tableId } = pageConfig;
export default function beforeEvent(props, moduleId, item, index, value, record) {
	//编辑仓库之前先判断库存组织是否可以编辑
	if ('pk_warehouse_out' == item.attrcode) {
		let pk_icorg_out = record.values.pk_icorg_out;
		if (pk_icorg_out.value) {
			props.editTable.setEditableRowKeyByIndex(tableId, index, 'pk_warehouse_out', true);
		} else {
			props.editTable.setEditableRowKeyByIndex(tableId, index, 'pk_warehouse_out', false);
		}
	}
	return true;
}
