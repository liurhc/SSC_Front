import { ajax } from 'nc-lightapp-front';
import { url, pageConfig, rowSelected } from '../const';
import ampub from 'ampub';

const { components, utils } = ampub;
const { MsgConst, showMessage } = utils.msgUtils;
const { getContext, loginContextKeys } = components.LoginContext;
const { getMultiLangByID } = utils.multiLangUtils;

/**
 * 查询按钮
 * @param {*} props 
 * @param {*} tableId 
 */
export default function(props, tableId, pk_taker, isRefresh) {
	let mainorg = this.state.mainorg;
	let pk_org = null;

	if (mainorg && mainorg.refpk) {
		pk_org = mainorg.refpk;
	} else if ('' == mainorg) {
		pk_org = getContext.call(this, loginContextKeys.pk_org);
	}

	if (!pk_taker) {
		pk_taker = null;
		if (isRefresh) {
			//刷新时
			let takervalue = this.state.taker;
			if (takervalue && takervalue.refpk) {
				pk_taker = takervalue.refpk;
			}
		}
	}
	//判断组织和人员不能为空
	if (null == pk_org || null == pk_taker || '' == pk_taker) {
		//清空表体的值
		props.editTable.setTableData(tableId, { rows: [] });
		props.editTable.setStatus(tableId, 'edit');
		return;
	}
	let data = {
		pk_org,
		pk_taker
	};

	let { queryUrl } = url;
	let {
		billtype_assign,
		billname_assign,
		billtype_borrow,
		billname_borrow,
		billtype_equip,
		billname_equip
	} = pageConfig;
	ajax({
		url: queryUrl,
		data: data,
		success: (res) => {
			let { data } = res;
			if (data && data[tableId] && data[tableId].rows) {
				//把单据类型字段设值未单据名称
				data[tableId].rows.map((row) => {
					let bill_type = row.values.bill_type;
					if (bill_type && bill_type.value == billtype_assign) {
						row.values.bill_type = { value: getMultiLangByID(billname_assign) };
					} else if (bill_type && bill_type.value == billtype_borrow) {
						row.values.bill_type = { value: getMultiLangByID(billname_borrow) };
					} else if (bill_type && bill_type.value == billtype_equip) {
						row.values.bill_type = { value: getMultiLangByID(billname_equip) };
					}
				});
				props.editTable.setTableData(tableId, data[tableId]);
				props.editTable.setStatus(tableId, 'edit');
			} else {
				props.editTable.setTableData(tableId, { rows: [] });
			}

			rowSelected.call(this, props, tableId);
			if (isRefresh) {
				//刷新成功，弹框提示
				showMessage.call(this, props, { type: MsgConst.Type.RefreshSuccess });
			}
		}
	});
}
