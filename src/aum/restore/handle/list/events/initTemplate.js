import { pageConfig, rowSelected } from '../const';
import { getVisibleRowsData } from './afterEvent';
import ampub from 'ampub';

const { components, commonConst, utils } = ampub;
const { getContext, loginContextKeys, loginContext } = components.LoginContext;
const { IBusiRoleConst } = commonConst.CommonKeys;
const { getMultiLangByID } = utils.multiLangUtils;

/**
 * 初始化模板
 * @param {*} props 
 */
export default function(props) {
	let { pagecode, tableId } = pageConfig;
	props.createUIDom(
		{
			//页面编码
			pagecode
		},
		(data) => {
			if (data) {
				if (data.context) {
					//初始化上下文变量
					loginContext.call(this, data.context);
				}
				if (data.button) {
					props.button.setButtons(data.button, () => {
						//设置批量按钮可用性
						rowSelected.call(this, props, tableId);
					});
					// 行删除时悬浮框提示
					props.button.setPopContent('DelLine', getMultiLangByID('msgUtils-000001') /* 国际化处理： 确定要删除吗？*/);
				}
				if (data.template) {
					let meta = data.template;
					modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta);
				}
			}
		}
	);
}

/**
 * 修改模板
 * @param {*} props 
 * @param {*} meta 
 */
function modifierMeta(props, meta) {
	const { tableId } = pageConfig;
	setMetaRefCondtion.call(this, props, meta);
	// 添加表格操作列
	let oprCol = {
		label: getMultiLangByID('amcommon-000000') /* 国际化处理： 操作*/,
		visible: true,
		attrcode: 'opr',
		itemtype: 'customer',
		className: 'table-opr',
		// 锁定在右边
		fixed: 'right',
		width: '110px',
		render: (text, record, index) => {
			let buttonAry = [ 'DelLine' ];
			return props.button.createOprationButton(buttonAry, {
				area: 'list_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => {
					props.editTable.delRow(tableId, index);
				}
			});
		}
	};
	meta[tableId].items.push(oprCol);

	return meta;
}

function getTableEditRow(props, tableId, key) {
	let allRows = getVisibleRowsData.call(this, props, tableId);
	let data = {
		index: -1,
		data: {}
	};
	if (allRows.length == 0) {
		return data;
	}
	if (typeof key == 'string') {
		for (let i = 0; i < allRows.length; i++) {
			let values = allRows[i].values;
			if (values[key].isEdit) {
				data = {
					data: allRows[i],
					index: i
				};
				break;
			}
		}
	} else {
		for (let i = 0; i < allRows.length; i++) {
			let values = allRows[i].values;
			let keys = Object.keys(values);
			for (let j = 0; j < keys.length; j++) {
				if (values[keys[j]].isEdit) {
					data = {
						data: allRows[i],
						index: i
					};
					break;
				}
			}
		}
	}
	return data;
}

function setMetaRefCondtion(props, meta) {
	const { tableId } = pageConfig;
	//处理归还人的参照
	meta[tableId].items.map((item) => {
		if (item.attrcode == 'pk_taker' || item.attrcode == 'pk_recorder') {
			item.queryCondition = () => {
				let editRow = getTableEditRow.call(this, props, tableId, item.attrcode);
				let pk_org = props.editTable.getValByKeyAndIndex(tableId, editRow.index, 'pk_org').value;
				return {
					pk_org: pk_org,
					busifuncode: IBusiRoleConst.ASSETORG
				};
			};
		} else if (item.attrcode == 'pk_equip') {
			//设备卡片设置支持多选
			item.isMultiSelectedEnabled = true;
			//设备卡片的编辑前事件
			item.queryCondition = () => {
				let taker = this.state.taker;
				let pk_org =
					this.state.mainorg == ''
						? getContext.call(this, loginContextKeys.pk_org)
						: this.state.mainorg.refpk;
				let pk_taker = null;
				if (taker && taker.refpk) {
					pk_taker = taker.refpk;
				}
				let borrow_return_transitype = getContext.call(this, 'borrow_return_transitype');
				let assign_return_transitype = getContext.call(this, 'assign_return_transitype');
				let pk_group = getContext.call(this, loginContextKeys.groupId);
				//设备卡片根据表头的主组织过滤，必须是已借用的
				return {
					pk_org: pk_org,
					pk_taker: pk_taker,
					borrow_return_transitype,
					assign_return_transitype,
					pk_group,
					GridRefActionExt: 'nccloud.web.aum.restore.handle.action.PK_Equip_SqlBuilder'
				};
			};
		} else if (item.attrcode == 'pk_warehouse_out') {
			item.isShowUnit = false;
			item.queryCondition = () => {
				//仓库根据库存组织过滤
				let editRow = getTableEditRow.call(this, props, tableId, item.attrcode);
				let pk_icorg_out = props.editTable.getValByKeyAndIndex(tableId, editRow.index, 'pk_icorg_out').value;

				return {
					pk_org: pk_icorg_out,
					GridRefActionExt: 'nccloud.web.ampub.common.refCodition.StoreDocSqlBuilder'
				};
			};
		} else if (item.attrcode == 'pk_unit_used_before_v') {
			//使用权的参照范围，根据使用管理组织过滤委托的使用权
			item.queryCondition = () => {
				let editRow = getTableEditRow.call(this, props, tableId, item.attrcode);
				//取当前界面的使用管理组织
				let pk_usedorg = props.editTable.getValByKeyAndIndex(tableId, editRow.index, 'pk_org');

				return {
					pk_org: pk_usedorg.value,
					TreeRefActionExt: 'nccloud.web.ampub.common.refCodition.UsedUnitSqlBuilder'
				};
			};
		} else if (item.attrcode == 'pk_usedept_before_v') {
			item.queryCondition = () => {
				//归还后使用部门根据归还后使用权过滤
				let editRow = getTableEditRow.call(this, props, tableId, item.attrcode);
				let pk_org = props.editTable.getValByKeyAndIndex(tableId, editRow.index, 'pk_unit_used_before').value;
				return {
					pk_org: pk_org,
					busifuncode: IBusiRoleConst.ASSETORG
				};
			};
		} else if (item.attrcode == 'pk_user_before') {
			//归还后使用人根据
			item.queryCondition = () => {
				let editRow = getTableEditRow.call(this, props, tableId, item.attrcode);
				let pk_org = props.editTable.getValByKeyAndIndex(tableId, editRow.index, 'pk_unit_used_before').value;
				let pk_dept = props.editTable.getValByKeyAndIndex(tableId, editRow.index, 'pk_usedept_before').value;
				return {
					pk_org: pk_org,
					pk_dept: pk_dept,
					busifuncode: IBusiRoleConst.ASSETORG
				};
			};
		} else if (item.attrcode == 'pk_location_before') {
			let pk_org =
				this.state.mainorg == '' ? getContext.call(this, loginContextKeys.pk_org) : this.state.mainorg.refpk;
			item.queryCondition = () => {
				return { pk_org: pk_org };
			};
		}
	});
}
