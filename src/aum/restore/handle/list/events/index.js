import initTemplate from './initTemplate';
import buttonClick from './buttonClick';
import afterEvent from './afterEvent';
import searchBtnClick from './searchBtnClick';
import beforeEvent from './beforeEvent';

export { initTemplate, buttonClick, searchBtnClick, afterEvent, beforeEvent };
