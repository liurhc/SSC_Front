import { ajax } from 'nc-lightapp-front';
import { url, pageConfig, rowSelected } from '../const';
import searchBtnClick from './searchBtnClick';
import { getVisibleRowsData } from './afterEvent';
import ampub from 'ampub';

const { components, utils } = ampub;
const { showConfirm, MsgConst, showMessage } = utils.msgUtils;
const { getContext, loginContextKeys } = components.LoginContext;
const { getMultiLangByID } = utils.multiLangUtils;

const { tableId } = pageConfig;
/**
 * 按钮点击事件
 * @param {*} props 
 * @param {*} id 
 * @param {*} tableId 
 */
export default function buttonClick(props, id) {
	switch (id) {
		case 'Search':
			//查询按钮
			searchBtnClick.call(this, props, tableId);
			break;
		case 'AddLine':
			addLine.call(this, props, tableId);
			break;
		case 'DeleteLine':
			batchDel.call(this, props, tableId);
			break;
		case 'ConfirmReturn':
			//确认归还按钮
			confirmReturnAction.call(this, props, tableId);
			break;
		case 'BatchAlter':
			//批改按钮
			batcheAlterAction.call(this, props, tableId);
			break;
		case 'Refresh':
			refreshAction.call(this, props, tableId);
	}
}

function addLine(props, tableId) {
	//判断主组织不能为空
	let mainorg = this.state.mainorg;
	if ('' == mainorg) {
		mainorg = {
			refpk: getContext.call(this, loginContextKeys.pk_org),
			refname: getContext.call(this, loginContextKeys.org_Name)
		};
	}
	let isOrgNull = true;
	if (mainorg && mainorg.refpk) {
		let pk_org = mainorg.refpk;
		if (!pk_org || pk_org == null || pk_org == '') {
			isOrgNull = false;
		}
	} else {
		isOrgNull = false;
	}
	if (!isOrgNull) {
		showMessage.call(this, props, {
			content: getMultiLangByID('452001504A-000006') /* 国际化处理： 资产组织不能为空！*/,
			color: 'warning'
		});
		return;
	}
	let index = props.editTable.getNumberOfRows(tableId);
	props.editTable.addRow(tableId, index);
}
/**
 * 批量删除按钮，只在前台删除，后台并不直接删除
 * @param {*} props 
 * @param {*} tableId 
 */
function batchDel(props, tableId) {
	let checkedRows = props.editTable.getCheckedRows(tableId);
	if (checkedRows.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseDelete });
		return;
	} else {
		showConfirm.call(this, props, { type: MsgConst.Type.DelSelect, beSureBtnClick: deleteAction });
	}
}

function deleteAction(props) {
	let checkedRows = props.editTable.getCheckedRows(tableId);
	let indexArr = [];
	checkedRows.map((row) => {
		indexArr.push(row.index);
	});
	props.editTable.deleteTableRowsByIndex(tableId, indexArr);
	rowSelected.call(this, props, tableId);
}

/**
 * 确认归还按钮
 * @param {*} props 
 * @param {*} tableId 
 */
function confirmReturnAction(props, tableId) {
	let { confirmUrl } = url;
	let {
		pagecode,
		billtype_assign,
		billname_assign,
		billtype_borrow,
		billname_borrow,
		billtype_equip,
		billname_equip
	} = pageConfig;

	//确认归还前校验数据
	let enable = validatorBeforeConfirmReturn.call(this, props, tableId);
	if (!enable) {
		return;
	}
	//先拿到选择的数据
	let checkedRows = props.editTable.getCheckedRows(tableId);
	let vos = [];
	checkedRows.map((row) => {
		let bill_type = row.data.values.bill_type;
		if (bill_type && bill_type.value == getMultiLangByID(billname_assign)) {
			row.data.values.bill_type = { value: billtype_assign };
		} else if (bill_type && bill_type.value == getMultiLangByID(billname_borrow)) {
			row.data.values.bill_type = { value: billtype_borrow };
		} else if (bill_type && bill_type.value == getMultiLangByID(billname_equip)) {
			row.data.values.bill_type = { value: billtype_equip };
		}
		vos.push(row.data);
	});
	//取得下游交易类型参数
	let borrow_return_transitype = getContext.call(this, 'borrow_return_transitype');
	let assign_return_transitype = getContext.call(this, 'assign_return_transitype');

	let transitypeString = JSON.stringify({
		borrow_return_transitype: borrow_return_transitype,
		assign_return_transitype: assign_return_transitype
	});
	const data = {
		pageid: pagecode,
		model: {
			areaType: 'table',
			areacode: tableId,
			rows: vos
		},
		userjson: transitypeString
	};

	ajax({
		url: confirmUrl,
		data,
		success: (res) => {
			let { data } = res;

			if (data.pk_equips) {
				let equipData = data.pk_equips;
				//返回的是生成下游单据的设备卡片的主键，删除已生成下游单据的设备卡片
				let rowDatas = getVisibleRowsData.call(this, props, tableId);
				let delLine = [];
				rowDatas.map((row, i) => {
					let pk_equip = row.values.pk_equip.value;
					let del = equipData.find((item) => {
						return item == pk_equip;
					});
					if (del) {
						delLine.push(i);
					}
				});
				props.editTable.deleteTableRowsByIndex(tableId, delLine);
				rowSelected.call(this, props, tableId);
			}

			if (data && data.errorMsg) {
				showMessage.call(this, props, { content: data.errorMsg, color: 'warning' });
			}
		}
	});
}

function validatorBeforeConfirmReturn(props, tableId) {
	let checkedRows = props.editTable.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length == 0) {
		showMessage.call(this, props, {
			content: getMultiLangByID('452001504A-000007') /* 国际化处理： 请先选择数据！*/,
			color: 'warning'
		});
		return false;
	}
	//实际归还日期必输
	let isNull = true;
	checkedRows.map((row) => {
		let data = row.data;
		let return_date = data.values.return_date;
		if (!return_date || return_date.value == null || return_date.value == '') {
			showMessage.call(this, props, {
				content: getMultiLangByID('452001504A-000008') /* 国际化处理： 归还日期数据不能为空!*/,
				color: 'warning'
			});
			isNull = false;
		}
	});
	return isNull;
}

/**
 * 批改按钮动作
 * @param {*} props 
 * @param {*} tableId 
 */
function batcheAlterAction(props, tableId) {
	let { batchAlterUrl } = url;
	let { pagecode } = pageConfig;
	//取得字段数据
	let changeData = props.cardTable.getTableItemData(tableId);
	//获取界面的数据
	let rows = getVisibleRowsData.call(this, props, tableId);
	let gridData = {
		pageid: pagecode,
		model: {
			areaType: 'table',
			areacode: tableId,
			rows: rows
		}
	};
	let data = {
		gridData: gridData,
		batchChangeIndex: changeData.batchChangeIndex, // 原始数据行号
		batchChangeKey: changeData.batchChangeKey, // 原始数据key
		batchChangeValue: changeData.batchChangeValue, // 原始数据value
		tableCode: ''
	};
	ajax({
		url: batchAlterUrl,
		data: data,
		success: (res) => {
			let { data } = res;

			if (data && data[tableId] && data[tableId].rows) {
				props.editTable.setTableData(tableId, data[tableId]);
				props.editTable.setStatus(tableId, 'edit');
			}
		}
	});
}

/**
 * 刷新按钮功能
 * @param {*} props 
 * @param {*} tableId 
 */
function refreshAction(props, tableId) {
	searchBtnClick.call(this, props, tableId, undefined, true);
}
