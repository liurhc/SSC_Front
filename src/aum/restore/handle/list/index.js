import React, { Component } from 'react';
import { createPage, base, createPageIcon } from 'nc-lightapp-front';
import { buttonClick, initTemplate, afterEvent, beforeEvent } from './events';
import { pageConfig, rowSelected } from './const';
import { searchBtnClick } from './events';
import ampub from 'ampub';

const { components, utils, commonConst } = ampub;
const { AMRefer } = components;
const { getContext, loginContextKeys } = components.LoginContext;
const { getMultiLangByID, initMultiLangByModule } = utils.multiLangUtils;
const { IBusiRoleConst } = commonConst.CommonKeys;
const { closeBrowserUtils } = utils;

const { NCAffix } = base;

class RestoreList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			mainorg: '',
			taker: {}
		};
		closeBrowserUtils.call(this, props, { editTable: [ tableId ] });
		initTemplate.call(this, props);
	}

	// 组织切换
	mainorgChange = (value = {}) => {
		//清空人员的值
		this.setState({ mainorg: value, taker: {} });
		//清空表体的值
		this.props.editTable.setTableData(tableId, { rows: [] });
		this.props.editTable.setStatus(tableId, 'edit');
	};

	// 归还人切换事件
	takerChange = (value = {}) => {
		this.setState({ taker: value });
		let { tableId } = pageConfig;
		let pk_taker = null == value ? null : value.refpk;
		searchBtnClick.call(this, this.props, tableId, pk_taker);
	};
	render() {
		const { editTable, button, ncmodal } = this.props;
		const { createEditTable } = editTable;
		const { createModal } = ncmodal;
		const { createButtonApp } = button;
		const { tableId, title, orgRefCode, takerRefCode } = pageConfig;
		let mainorgobj = {
			queryCondition: () => {
				//权限过滤
				return { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter' };
			},
			onChange: (value) => {
				this.mainorgChange(value);
			},
			refcode: orgRefCode,
			defaultValue:
				this.state.mainorg == ''
					? {
							refpk: getContext.call(this, loginContextKeys.pk_org),
							refname: getContext.call(this, loginContextKeys.org_Name)
						}
					: this.state.mainorg
		};
		let takerRefer = {
			queryCondition: () => {
				let pk_org =
					this.state.mainorg == ''
						? getContext.call(this, loginContextKeys.pk_org)
						: this.state.mainorg.refpk;
				return { pk_org: pk_org, busifuncode: IBusiRoleConst.ASSETORG };
			},
			onChange: (value) => {
				this.takerChange(value);
			},
			placeholder: getMultiLangByID('452001504A-000000') /* 国际化处理： 归还人*/,
			refcode: takerRefCode,
			defaultValue: this.state.taker
		};

		return (
			<div className="nc-single-table">
				<NCAffix>
					{/* 头部 header */}
					<div className="nc-bill-header-area">
						{/* 标题 title */}
						<div className="header-title-search-area">
							{createPageIcon()}
							<h2 className="title-search-detail">{getMultiLangByID(title)}</h2>
						</div>
					</div>
				</NCAffix>
				<div className="nc-singleTable-header-area">
					<div className="header-title-search-area">
						{/* 主组织 main-org */}
						{<AMRefer className="ref title-search-detail" config={mainorgobj} />}
						{/*归还人*/}
						{<AMRefer className="ref title-search-detail" config={takerRefer} />}
					</div>
					{/* 按钮区 btn-area */}
					<div className="header-button-area">
						{createButtonApp({
							area: 'list_head',
							onButtonClick: buttonClick.bind(this),
							buttonLimit: 3
						})}
					</div>
				</div>

				{/* 列表区 table-area */}
				<div className="nc-singleTable-table-area">
					{createEditTable(tableId, {
						onAfterEvent: afterEvent.bind(this),
						onBeforeEvent: beforeEvent.bind(this),
						showCheck: true,
						showIndex: true,
						onSelected: () => {
							rowSelected.call(this, this.props, tableId);
						},
						onSelectedAll: () => {
							rowSelected.call(this, this.props, tableId);
						}
					})}
				</div>
			</div>
		);
	}
}

const { pagecode, tableId } = pageConfig;
RestoreList = createPage({
	billinfo: {
		billtype: 'grid',
		pagecode: pagecode,
		bodycode: tableId
	}
})(RestoreList);

initMultiLangByModule({ aum: [ '452001504A' ], ampub: [ 'common' ] }, () => {
	ReactDOM.render(<RestoreList />, document.querySelector('#app'));
});
