//主子表列表

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, high, createPageIcon } from 'nc-lightapp-front';
const { NCTabs, NCAffix } = base;

const NCTabPane = NCTabs.NCTabPane;
import {
	buttonClick,
	initTemplate,
	searchBtnClick,
	pageInfoClick,
	tableModelConfirm,
	doubleClick,
	rowSelected,
	afterEvent,
	commitAction,
	setBatchBtnsEnable
} from './events';
import { pageConfig } from './const';
const { AREA_LIST, BUTTON_LIST, dataSource, title } = pageConfig;
import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { ApprovalTrans } = components;

class List extends Component {
	constructor(props) {
		super(props);
		this.searchcode = AREA_LIST.searchcode;
		this.tableId = AREA_LIST.tableId;
		this.transi_type = '';
		this.state = {
			bill_type: AREA_LIST.bill_type,
			show: false,
			showApprove: false,
			pk_apply: '',
			transi_type: '',
			compositedisplay: false,
			compositedata: {}
		};
		initTemplate.call(this, props);
	}
	componentDidMount() {} //提交及指派 回调，注意保存提交时候需要调用保存提交的方法

	getAssginUsedr = (content) => {
		commitAction.call(this, this.props, 'SAVE', 'commit', content); //原提交方法添加参数content
	};
	//取消 指派
	turnOff = () => {
		this.props.table.selectAllRows(this.tableId, false);
		setBatchBtnsEnable.call(this, this.props, this.tableId);
		this.setState({
			compositedisplay: false
		});
	};

	render() {
		let { table, search, ncUploader } = this.props;
		let { createNCUploader } = ncUploader;
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		const { BillTrack, ApproveDetail } = high;
		return (
			<div className="nc-bill-list">
				{createNCUploader('warcontract-uploader', {})}
				<NCAffix>
					<div className="nc-bill-header-area">
						<div className="header-title-search-area">
							{createPageIcon()}
							<h2 className="title-search-detail">{getMultiLangByID(title)}</h2>
						</div>
						<div className="header-button-area">
							{this.props.button.createButtonApp({
								area: BUTTON_LIST.list_head,
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
						</div>
					</div>
				</NCAffix>
				<div className="nc-bill-search-area">
					{NCCreateSearch(this.searchcode, {
						clickSearchBtn: searchBtnClick.bind(this),
						onAfterEvent: afterEvent.bind(this),
						dataSource: dataSource,
						pkname: 'pk_apply'
					})}
				</div>

				<div className="nc-bill-table-area">
					{createSimpleTable(this.tableId, {
						handlePageInfoChange: pageInfoClick,
						tableModelConfirm: tableModelConfirm,
						showIndex: true,
						showCheck: true,
						onRowDoubleClick: doubleClick.bind(this),
						onSelected: rowSelected.bind(this),
						onSelectedAll: rowSelected.bind(this),
						dataSource: dataSource,
						pkname: 'pk_apply',
						componentInitFinished: () => {
							//缓存数据赋值成功的钩子函数
							//若初始化数据后需要对数据做修改，可以在这里处理
							setBatchBtnsEnable.call(this, this.props, this.tableId);
						}
					})}
				</div>
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transi_type}
					billid={this.state.pk_apply}
				/>
				<BillTrack
					show={this.state.show}
					close={() => {
						this.setState({ show: false });
					}}
					pk={this.state.pk_apply} //单据id
					type={this.state.bill_type} //单据类型
				/>
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002')}
						/* 国际化处理： 指派*/
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				) : (
					''
				)}
			</div>
		);
	}
}

List = createPage({})(List);
// ReactDOM.render(<List />, document.querySelector('#app'));
export default List;
