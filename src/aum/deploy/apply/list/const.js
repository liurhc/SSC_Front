export const pageConfig = {
	AREA_CARD: {
		appid: '0001Z910000000006G9L',
		appcode: '452003004A',
		pagecode: '452003004A_card',
		formId: 'card_head',
		tableId: 'bodyvos',
		bodyAfterEditUrl: '/nccloud/aum/deploy_apply/bodyAfterEdit.do'
	},
	AREA_LIST: {
		appid: '0001Z910000000006G9L',
		appcode: '452003004A',
		pagecode: '452003004A_list',
		searchcode: 'searchArea',
		searchoid: '1001Z91000000000RYAX',
		tableId: 'list_head',
		bill_type: '4A40',
		transi_type: '4A40-01'
	},
	BUTTON_LIST: {
		list_head: 'list_head',
		list_inner: 'list_inner'
	},
	EditOpr_url: '/nccloud/aum/deploy_apply/apply_edit.do',
	node_code: '4520008015',
	printFilename: '452003004A-000000' /* 国际化处理： 调入申请*/,
	title: '452003004A-000000' /* 国际化处理： 调入申请*/,
	SEARCHPARAMS: '452003004A_list',

	printListUrl: '/nccloud/aum/deploy_apply/printCard.do', //列表打印url
	copydata_url: '/nccloud/aum/deploy_apply/apply_copy.do', //列表复制url

	printFuncode: '452003004A',
	printNodekey: null,

	dataSource: 'aum.deploy.apply.main',
	cardRouter: '/card'
};
