import ampub from 'ampub';
const { components } = ampub;
const { assetOrgMultiRefFilter } = components;
const { isMultiCorpRefHandler } = assetOrgMultiRefFilter;
import { pageConfig } from '../const';
const { AREA_LIST } = pageConfig;

export default function afterEvent(key, value) {
	if (key === 'pk_org') {
		isMultiCorpRefHandler.call(this, this.props, value, AREA_LIST.searchcode, [ 'pk_deployout_org', 'pk_applier' ]);
	}
	if (key === 'pk_deployout_org') {
		isMultiCorpRefHandler.call(this, this.props, value, AREA_LIST.searchcode, [ 'pk_deployin_ownerunit' ]);
	}
}
