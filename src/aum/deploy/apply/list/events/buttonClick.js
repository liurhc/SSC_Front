import { ajax, toast, print, cacheTools, output } from 'nc-lightapp-front';

import { pageConfig } from '../const';
const {
	AREA_LIST,
	BUTTON_LIST,
	printFilename,
	printNodekey,
	printListUrl,
	cardRouter,
	AREA_CARD,
	dataSource
} = pageConfig;
import searchBtnClick from './searchBtnClick';

import ampub from 'ampub';
const { utils, components } = ampub;
const { ScriptReturnUtils, queryAboutUtils, queryVocherUtils } = components;
const { getScriptListReturn } = ScriptReturnUtils;
const { openListBillTrack, openReportEquip, LinkReportConst } = queryAboutUtils;
const { queryAboutVoucher } = queryVocherUtils;
const { multiLangUtils, msgUtils, listUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { setBillFlowBtnsEnable, batchRefresh } = listUtils;
const { showConfirm, MsgConst, showMessage } = msgUtils;

import pageInfoClick from './pageInfoClick';
let tableId = AREA_LIST.tableId;
let formId = 'card_head';
let searchcode = AREA_LIST.searchcode;
let pagecode = AREA_LIST.pagecode;
export default function buttonClick(props, id) {
	let _this = this;
	switch (id) {
		case 'Add':
			props.pushTo(cardRouter, {
				pagecode: AREA_CARD.pagecode,
				status: 'add'
			});
			break;
		case 'Refresh':
			refreshAction.call(this, props);
			break;
		case 'Delete':
			delConfirm.call(this, props);
			break;
		case 'Commit':
			commitAction.call(this, props, 'SAVE', 'commit');
			break;
		case 'UnCommit':
			commitAction.call(this, props, 'UNSAVE', '');
			break;
		case 'QueryAboutBusiness':
			openListBillTrack(_this, props, tableId, 'pk_apply');
			break;
		case 'QueryAboutVoucher':
			queryAboutVoucher.call(this, props, tableId, 'pk_apply', AREA_LIST.appcode, true);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'Attachment':
			attachmentClick.call(this, props);
			break;
		case 'Copy':
			copy.call(this, props);
			break;
		case 'QueryAboutEquipReport':
			QueryAboutEquipReport.call(this, props);
		default:
			break;
	}
}

function QueryAboutEquipReport(props) {
	let checkData = props.table.getCheckedRows(tableId);
	let rowData;
	if (checkData.length == 0) {
		toast({ content: getMultiLangByID('452003004A-000010') /* 国际化处理：请先选中需要联查的数据 */, color: 'warning' });
		return;
	}
	rowData = checkData[0].data;

	let pk_deployout_org = rowData.values.pk_deployout_org.value;

	//根据当前选中表体行的“设备类别、设备名称、物料编码”查询设备台帐中货主管理组织
	//或者使用管理组织为调出组织，且卡片的状态分类=闲置类的设备卡片。

	let map = {};
	let conditions = [];
	let mapValue = {};
	mapValue['pk_deployout_org'] = pk_deployout_org;
	map['card_status'] = 3;
	map['pk_used_status.status_type'] = 2;
	mapValue['card_status'] = 3;
	mapValue['pk_used_status.status_type'] = 2;
	conditions.push({
		field: 'card_status',
		value: { firstvalue: 3, secondvalue: '' },
		oprtype: '=',
		display: '',
		isIncludeSub: false,
		refurl: ''
	});
	conditions.push({
		field: 'pk_used_status.status_type',
		value: { firstvalue: 2, secondvalue: '' },
		oprtype: '=',
		display: '',
		isIncludeSub: false,
		refurl: ''
	});
	if (pk_deployout_org) {
		map['pk_ownerorg'] = pk_deployout_org;
		map['pk_usedorg'] = pk_deployout_org;
		mapValue['pk_ownerorg'] = pk_deployout_org;
		mapValue['pk_usedorg'] = pk_deployout_org;
	}

	let querycondition = {
		logic: 'and',
		conditions: conditions
	};

	cacheTools.set(AREA_LIST.bill_type + `condition`, map);
	mapValue[LinkReportConst.GETSQLACTION] = 'nccloud.pub.aum.report.insurance.InsuranceSqlBuilder';
	// openReportEquip.call(this, props, BILLTYPE, BILLTYPE + 'condition', querycondition);
	openReportEquip.call(
		this,
		props,
		LinkReportConst.REPORT,
		AREA_LIST.bill_type + `condition`,
		querycondition,
		mapValue
	);
}

//列表复制
function copy(props) {
	let datas = props.table.getCheckedRows(tableId);
	let pk_apply;
	if (datas && datas.length > 0) {
		pk_apply = datas[0].data.values.pk_apply.value;
	}
	if (pk_apply) {
		props.pushTo(cardRouter, {
			pagecode: AREA_CARD.pagecode,
			status: 'copyAdd',
			id: pk_apply
		});
	} else {
		showMessage.call(this, props, { type: MsgConst.Type.DataDeleted });
		/* 国际化处理： 数据已经被删除*/
	}
}

/**
 * 附件上传
 * @param {*} props 
 */
function attachmentClick(props) {
	let checkedrows = props.table.getCheckedRows(tableId);
	// billNo 是单据编码
	let billNo = checkedrows[0].data.values['bill_code'].value;
	// billNo 是单据主键
	let billId = checkedrows[0].data.values['pk_apply'].value;
	props.ncUploader.show('warcontract-uploader', {
		billId: 'pam/deploy_apply/' + billId,
		billNo
	});
}

/**
 * 打印
 * @param {*} props 
 */
export function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	let { pageConfig } = props;
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		/* 国际化处理： 请选择需要打印的数据*/
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		printListUrl, // 后台打印服务url
		printData
	);
}

/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput }); /* 国际化处理： 请选择需要输出的数据*/
		return;
	}
	output({
		url: printListUrl,
		data: printData
	});
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let checkedRows = props.table.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length == 0) {
		return false;
	}
	let pks = [];
	checkedRows.map((item) => {
		pks.push(item.data.values['pk_apply'].value);
	});
	let printData = {
		filename: getMultiLangByID(printFilename), // 文件名称
		nodekey: printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

/**
 * 
 * @param {*} props 
 * @param {*} action 动作
 * @param {*} type 提交或保存提交 传入  commit，saveCommit 否则不传入
 */
export function commitAction(props, action, type, content) {
	//提交的进行校验
	if (action == 'SAVE' && type == 'commit') {
		let flag = beforeCommitValidator.call(this, props);
		if (!flag) {
			return;
		}
	}
	let data = props.table.getCheckedRows(tableId);
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values.pk_apply.value;
		let ts = v.data.values.ts.value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	ajax({
		url: '/nccloud/aum/deploy_apply/commit.do',
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: action,
			pageid: '452003004A_card',
			commitType: type, //提交
			content: content
		},
		success: (res) => {
			if (content) {
				this.setState({
					compositedisplay: false
				});
			}
			getScriptListReturn.call(this, params, res, props, 'pk_apply', formId, tableId, false, dataSource);
		}
	});
}

function beforeCommitValidator(props) {
	let data = props.table.getCheckedRows(tableId);
	let content = '';
	let flag = true;
	data.map((v) => {
		let deploy_type = v.data.values.deploy_type.value;
		let pk_deployout_org_v = v.data.values.pk_deployout_org_v.value;
		let index = v.index;
		let num = index + 1;
		if (!deploy_type || deploy_type == undefined || deploy_type == '') {
			flag = false;
			content +=
				'<div>' +
				getMultiLangByID('452003004A-000016', {
					num
				}) +
				'</div>' /* 国际化处理：<div>第{num}行调拨类型不能为空不能为空；</div> */;
		}
		if (!pk_deployout_org_v || pk_deployout_org_v == undefined || pk_deployout_org_v == '') {
			flag = false;
			content +=
				'<div>' +
				getMultiLangByID('452003004A-000017', {
					num
				}) +
				'</div>' /* 国际化处理：<div>第{num}行调出组织不能为空；</div> */;
		}
	});

	if (!flag) {
		toast({
			content,
			color: 'danger',
			isNode: true
		});
	}
	return flag;
}

/**
 * 删除弹框处理
 * @param {*} props 
 */
function delConfirm(props) {
	let checkedRows = props.table.getCheckedRows(tableId);
	if (checkedRows.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseDelete });
		/* 国际化处理： 请选择需要删除的数据*/
		return;
	}
	showConfirm.call(this, props, { type: MsgConst.Type.DelSelect, beSureBtnClick: deleteAction });
}

const deleteAction = (props) => {
	let data = props.table.getCheckedRows(tableId);
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values.pk_apply.value;
		let ts = v.data.values.ts.value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	ajax({
		url: '/nccloud/aum/deploy/apply_delete.do',
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: '452003004A_card'
		},
		success: (res) => {
			getScriptListReturn(params, res, props, 'pk_apply', formId, tableId, true);
		}
	});
};
/**
 * 刷新
 * @param {*} props 
 */
function refreshAction(props) {
	batchRefresh.call(this, props, searchBtnClick);
}

/**
 * 设置批量按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBatchBtnsEnable(props, moduleId) {
	setBillFlowBtnsEnable.call(this, props, { tableId: moduleId });
}
