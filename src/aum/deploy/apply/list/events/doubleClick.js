import { pageConfig } from '../const';
const { AREA_CARD, cardRouter } = pageConfig;

export default function doubleClick(record, index, props) {
	props.pushTo(cardRouter, {
		pagecode: AREA_CARD.pagecode,
		status: 'browse',
		id: record.pk_apply.value
	});
}
