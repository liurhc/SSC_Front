import { ajax, toast, cacheTools } from 'nc-lightapp-front';
import { pageConfig } from '../const';
const { AREA_LIST, SEARCHPARAMS } = pageConfig;
import ampub from 'ampub';
const { utils, components } = ampub;
const { listUtils } = utils;
const { setQueryInfoCache, setListValue, listConst } = listUtils;
const { ApprovalTrans, LoginContext } = components;
const { getContext, loginContextKeys } = LoginContext;
import { setBatchBtnsEnable } from './buttonClick';

//点击查询，获取查询区数据
export default function clickSearchBtn(props, searchVal, type, queryInfo, isRefresh) {
	if (!searchVal) {
		return;
	}
	let pageInfo = props.table.getTablePageInfo(AREA_LIST.tableId);
	if (!queryInfo) {
		queryInfo = props.search.getQueryInfo(AREA_LIST.searchcode);
	}
	queryInfo.pagecode = AREA_LIST.pagecode;
	queryInfo.pageInfo = pageInfo;
	queryInfo.billtype = AREA_LIST.bill_type;
	queryInfo.transtype = this.transi_type;
	// 缓存查询条件
	setQueryInfoCache.call(this, queryInfo, isRefresh, type);
	ajax({
		url: '/nccloud/aum/deploy/apply_query.do',
		data: queryInfo,
		success: (res) => {
			setListValue.call(
				this,
				props,
				res,
				AREA_LIST.tableId,
				isRefresh ? listConst.SETVALUETYPE.refresh : listConst.SETVALUETYPE.query
			);
			setBatchBtnsEnable.call(this, props, AREA_LIST.tableId);
		}
	});
}
