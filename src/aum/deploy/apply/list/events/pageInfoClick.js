import { ajax, toast } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import ampub from 'ampub';
const { utils } = ampub;
const { listUtils } = utils;
const { setListValue } = listUtils;
import { setBatchBtnsEnable } from './buttonClick';
const { AREA_LIST, BUTTON_LIST } = pageConfig;
const searchcode = AREA_LIST.searchcode;
const tableId = AREA_LIST.tableId;
const pagecode = AREA_LIST.pagecode;
export default function(props, config, pks) {
	// 后台还没更新，暂不可用
	let data = {
		allpks: pks,
		pagecode: pagecode
	};
	//得到数据渲染到页面
	ajax({
		url: '/nccloud/aum/deploy_apply/querypagegridbypks.do',
		data: data,
		success: function(res) {
			let { success } = res;
			if (success) {
				setListValue.call(this, props, res);
				setBatchBtnsEnable.call(this, props, tableId);
			}
		}
	});
}
