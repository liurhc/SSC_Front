import { ajax, toast } from 'nc-lightapp-front';

import { pageConfig } from '../const';
const { AREA_LIST, EditOpr_url, node_code, AREA_CARD, cardRouter, dataSource } = pageConfig;

import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { ScriptReturnUtils, queryAboutUtils } = components;
const { getScriptListReturn } = ScriptReturnUtils;
const { openListApprove } = queryAboutUtils;
const { StatusUtils } = commonConst;
const { UISTATE } = StatusUtils;
const { msgUtils, multiLangUtils } = utils;
const { MsgConst, showMessage } = msgUtils;
const { getMultiLangByID } = multiLangUtils;

let formId = AREA_CARD.formId;
let tableId = AREA_LIST.tableId;

/**
 * 表格操作按钮
 * @param {*} props 
 * @param {*} key 
 * @param {*} text 
 * @param {*} record 
 * @param {*} index 
 */
export default function(props, key, text, record, index) {
	switch (key) {
		case 'Edit':
			ajax({
				url: EditOpr_url,
				data: {
					pk: record.pk_apply.value,
					resourceCode: node_code
				},
				success: (res) => {
					if (res.data) {
						toast({ color: 'warning', content: res.data });
					} else {
						props.pushTo(cardRouter, {
							pagecode: AREA_CARD.pagecode,
							status: 'edit',
							id: record.pk_apply.value
						});
					}
				}
			});
			break;
		case 'Revise':
			ajax({
				url: EditOpr_url,
				data: {
					pk: record.pk_apply.value,
					resourceCode: node_code
				},
				success: (res) => {
					if (res.data) {
						toast({ color: 'warning', content: res.data });
					} else {
						props.pushTo(cardRouter, {
							pagecode: AREA_CARD.pagecode,
							status: 'Revise',
							id: record.pk_apply.value
						});
					}
				}
			});
			break;
		case 'Delete':
			let paramInfoMap = {};
			let params = [ { id: record.pk_apply.value, index: index } ];
			paramInfoMap[record.pk_apply.value] = record.ts.value;
			ajax({
				url: '/nccloud/aum/deploy/apply_delete.do',
				data: {
					paramInfoMap: paramInfoMap,
					dataType: 'listData',
					OperatorType: 'DELETE',
					pageid: AREA_CARD.pagecode
				},
				success: (res) => {
					getScriptListReturn.call(this, params, res, props, 'pk_apply', formId, tableId, true);
				}
			});
			break;
		case 'Commit':
			commitClick.call(this, 'SAVE', props, tableId, index, record);
			break;
		case 'UnCommit':
			commitClick.call(this, 'UNSAVE', props, tableId, index, record);
			break;
		case 'QueryAboutBillFlow':
			openListApprove(this, record, 'pk_apply');
			break;
		case 'Copy':
			copy.call(this, props, key, text, record, index);
			break;
	}
}

//列表行复制
function copy(props, key, text, record, index) {
	let pk_apply = record.pk_apply.value;
	if (pk_apply) {
		props.pushTo(cardRouter, {
			pagecode: AREA_CARD.pagecode,
			status: 'copyAdd',
			id: pk_apply
		});
	} else {
		showMessage.call(this, props, { type: MsgConst.Type.DataDeleted });
		/* 国际化处理： 数据已经被删除*/
	}
}

/**
 * 提交 收回 动作
 * @param {*} props 
 * @param {*} tableId 
 * @param {*} index 
 * @param {*} record 
 */
export function commitClick(OperatorType, props, tableId, index, record) {
	if (OperatorType == 'SAVE') {
		let flag = beforeCommitValidator.call(this, props, index, record, OperatorType);
		if (!flag) {
			return;
		}
	}

	let paramInfoMap = {};
	paramInfoMap[record.pk_apply.value] = record.ts.value;
	let params = [ { id: record.pk_apply.value, index: index } ];
	ajax({
		url: '/nccloud/aum/deploy_apply/commit.do',
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: OperatorType,
			pageid: '452003004A_card',
			commitType: 'commit'
		},
		success: (res) => {
			getScriptListReturn.call(this, params, res, props, 'pk_apply', formId, tableId, false, dataSource);
		}
	});
}

function beforeCommitValidator(props, index, record) {
	let content = '';
	let flag = true;
	let deploy_type = record.deploy_type.value;
	let pk_deployout_org_v = record.pk_deployout_org_v.value;
	//提交额外走的校验
	if (!deploy_type || deploy_type == undefined || deploy_type == '') {
		flag = false;
		content += '<div>' + getMultiLangByID('452003004A-000004') + '</div>' /* 国际化处理： <div>调拨类型不能为空</div> */;
	}
	if (!pk_deployout_org_v || pk_deployout_org_v == undefined || pk_deployout_org_v == '') {
		flag = false;
		content += '<div>' + getMultiLangByID('452003004A-000014') + '</div>' /* 国际化处理： <div>调处组织不能为空</div> */;
	}
	if (!flag) {
		toast({
			content,
			color: 'danger',
			isNode: true
		});
	}
	return flag;
}
