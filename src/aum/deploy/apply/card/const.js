export const pageConfig = {
	title: '452003004A-000000' /* 国际化处理： 调入申请*/,
	AREA_CARD: {
		appid: '0001Z910000000006G9L',
		appcode: '452003004A',
		pagecode: '452003004A_card',
		formId: 'card_head',
		tableId: 'bodyvos',
		bodyAfterEditUrl: '/nccloud/aum/deploy_apply/bodyAfterEdit.do'
	},
	AREA_LIST: {
		appid: '0001Z910000000006G9L',
		appcode: '452003004A',
		pagecode: '452003004A_list',
		searchcode: 'searchArea',
		searchoid: '1001Z91000000000RYAX',
		tableId: 'list_head',
		bill_type: '4A40',
		transi_type: '4A40-01'
	},
	EditOpr_url: '/nccloud/aum/deploy_apply/apply_edit.do',
	node_code: '4520008015',
	printFilename: '452003004A-000000' /* 国际化处理： 调入申请*/,
	BILLTYPE: '4A40',

	printCardUrl: '/nccloud/aum/deploy_apply/printCard.do', //卡片打印url

	copydata_url: '/nccloud/aum/deploy_apply/apply_copy.do', //卡片复制url

	printFuncode: '452003004A',
	printNodekey: null,

	dataSource: 'aum.deploy.apply.main',
	cardRouter: '/card',
	BatchAlter_url: '/nccloud/aum/deploy_apply/batchAlter.do' //批改
};
