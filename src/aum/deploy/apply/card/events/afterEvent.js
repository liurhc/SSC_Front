import { ajax, toast } from 'nc-lightapp-front';
import { pageConfig } from '../const';
const { AREA_CARD } = pageConfig;

import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils, cardUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { commonHeadAfterEvent, commonBodyAfterEvent, updateHeadData, updateBodyData } = cardUtils;
const { OrgChangeEvent } = components;

const { orgChangeEvent } = OrgChangeEvent;
export default function afterEvent(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	if (moduleId == AREA_CARD.formId) {
		headAfterEvent.call(this, props, moduleId, key, value, changedRows, index, record, type, eventType);
	} else if (moduleId == AREA_CARD.tableId) {
		bodyAfterEvent.call(this, props, moduleId, key, value, changedRows, index, record, type, eventType);
	}
}
/**
 * 表头编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} oldValue 
 * @param {*} val 
 */
export function headAfterEvent(props, moduleId, key, value, oldValue, val) {
	//表头部分
	if (key === 'pk_org_v') {
		let fields = [
			'pk_unit_used_v',
			'pk_unit_used',
			'pk_usedept_v',
			'pk_user',
			'pk_applier',
			'pk_deployin_ownerunit_v',
			'pk_deployout_org_v',
			'pk_deployin_ownerunit',
			'pk_deployout_org'
		];
		let callback = () => {
			if (value.value != undefined) {
				// buttonClick.call(this, this.props, 'AddLine');
				props.cardTable.addRow(this.tableId, undefined, undefined, false);
				props.form.setFormItemsDisabled(AREA_CARD.formId, {
					deploy_type: false,
					pk_deployout_org_v: false,
					pk_applier: false,
					pk_deployin_ownerunit: true,
					pk_user: true
				});
				let deploy_type = props.form.getFormItemsValue(AREA_CARD.formId, [ 'deploy_type' ])[0].value;
				if (deploy_type && deploy_type == '1') {
					props.form.setFormItemsDisabled(AREA_CARD.formId, { pk_deployin_ownerunit_v: false });
				} else {
					props.form.setFormItemsValue(AREA_CARD.formId, {
						pk_deployin_ownerunit_v: { value: '', display: '' }
					});
					props.form.setFormItemsDisabled(AREA_CARD.formId, { pk_deployin_ownerunit_v: true });
				}
			}
		};
		//组织切换
		orgChangeEvent.call(
			this,
			props,
			AREA_CARD.pagecode,
			AREA_CARD.formId,
			AREA_CARD.tableId,
			key,
			value,
			oldValue,
			fields,
			[ 'AddLine', 'BatchAlter' ],
			callback
		);
	}

	if (key === 'deploy_type') {
		//调拨类型
		if (value.value != undefined) {
			if (value.value === '1') {
				//所有权调拨 value 1  调入货主 pk_deployin_ownerunit  可编辑 否则 不可编辑
				props.form.setFormItemsDisabled(AREA_CARD.formId, { pk_deployin_ownerunit_v: false });
			} else {
				props.form.setFormItemsValue(AREA_CARD.formId, { pk_deployin_ownerunit_v: { value: '', display: '' } });
				props.form.setFormItemsDisabled(AREA_CARD.formId, { pk_deployin_ownerunit_v: true });
			}
		}
	}

	if (key === 'pk_deployout_org_v') {
		//调出组织
		if (value.value != undefined) {
			let pk_org_v = props.form.getFormItemsValue(AREA_CARD.formId, [ 'pk_org_v' ])[0].value;
			let rows = props.cardTable.getColValue(AREA_CARD.tableId, 'pk_equip', false, false);
			let state = true;
			rows.map((v) => {
				if (v && v.value && v.value != '') {
					this.pk_deployout_org_v = oldValue;
					props.modal.show('onAfterEvent');
					state = false;
					return;
				}
			});
			if (value.value === pk_org_v && state == true) {
				props.form.setFormItemsValue(AREA_CARD.formId, { pk_deployout_org_v: { value: '', display: '' } });
				toast({ content: getMultiLangByID('452003004A-000001') /* 国际化处理： 调出组织与资产组织不能相同！*/, color: 'danger' });
				return;
			}
		} else {
			let rows = props.cardTable.getColValue(AREA_CARD.tableId, 'pk_equip', false, false);
			rows.map((v) => {
				if (v && v.value && v.value != '') {
					this.pk_deployout_org_v = oldValue;
					props.modal.show('onAfterEvent');
					return;
				}
			});
		}
	}

	if (key === 'pk_unit_used_v') {
		//使用权
		if (value.value != undefined) {
			props.form.setFormItemsDisabled(AREA_CARD.formId, { pk_usedept_v: false, pk_user: false });
		} else {
			props.form.setFormItemsDisabled(AREA_CARD.formId, { pk_usedept_v: true, pk_user: true });
		}
	}
	//表头部分
}
/**
 * 表体编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedRows 
 * @param {*} index 
 * @param {*} record 
 * @param {*} type 
 * @param {*} eventType 
 */
function bodyAfterEvent(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	let data = props.createBodyAfterEventData(
		AREA_CARD.pagecode,
		AREA_CARD.formId,
		AREA_CARD.tableId,
		moduleId,
		key,
		changedRows,
		index
	);
	//表体部分
	if (key === 'pk_equip') {
		//设备编码
		let pk_equip = props.cardTable.getValByKeyAndIndex(AREA_CARD.tableId, index, key);
		if (pk_equip && pk_equip.value) {
			let config = {
				afterEditUrl: AREA_CARD.bodyAfterEditUrl,
				pagecode: AREA_CARD.pagecode,
				key,
				record,
				changedRows,
				index,
				keys: [ 'pk_equip' ]
			};
			commonBodyAfterEvent.call(this, props, config);
			// // 处理多选，带出联动项
			// ajax({
			// 	url: AREA_CARD.bodyAfterEditUrl,
			// 	data,
			// 	success: (res) => {
			// 		let { success, data } = res;
			// 		if (success) {
			// 			if (res.data) {
			// 				if (res.data.head && res.data.head[AREA_CARD.formId]) {
			// 					this.props.form.setAllFormValue({
			// 						[AREA_CARD.formId]: res.data.head[AREA_CARD.formId]
			// 					});
			// 				}
			// 				if (res.data.body && res.data.body[AREA_CARD.tableId]) {
			// 					this.props.cardTable.setTableData(AREA_CARD.tableId, res.data.body[AREA_CARD.tableId]);
			// 				}
			// 			}
			// 		}
			// 	}
			// });
		} else {
			// 清空联动项
			let meta = props.meta.getMeta();
			meta[AREA_CARD.tableId].items.map((item) => {
				if (
					item.attrcode == 'equip_name' ||
					item.attrcode == 'pk_category' ||
					item.attrcode == 'spec' ||
					item.attrcode == 'model'
				) {
					props.cardTable.setValByKeyAndIndex(AREA_CARD.tableId, index, item.attrcode, {
						value: null,
						display: null
					});
				}
			});
		}
	}
	if (key === 'pk_category') {
		let config = {
			afterEditUrl: AREA_CARD.bodyAfterEditUrl,
			pagecode: AREA_CARD.pagecode,
			key,
			record,
			changedRows,
			index,
			keys: [ 'pk_category' ]
		};
		commonBodyAfterEvent.call(this, props, config);
	}
	//数量
	if (key === 'quantity') {
		if (value <= 0) {
			props.cardTable.setValByKeyAndIndex(AREA_CARD.tableId, index, 'quantity', {
				value: null,
				display: null
			});
			toast({ content: getMultiLangByID('452003004A-000002') /* 国际化处理： 数量应为大于0整数！*/, color: 'danger' });
		}
	}

	//表体部分
}
