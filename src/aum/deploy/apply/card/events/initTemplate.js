import { pageConfig } from '../const';
const { AREA_CARD, BILLTYPE } = pageConfig;
import tableButtonClick from './tableButtonClick';
import { init, getdata, copydata } from './buttonClick';

import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { multiLangUtils, cardUtils } = utils;
const { queryAboutUtils, refInit, LoginContext } = components;
const { openEquipCardByPk } = queryAboutUtils;
const { defRefCondition } = refInit;
const { afterModifyCardMeta } = cardUtils;
const { getMultiLangByID } = multiLangUtils;
const { loginContext, getContext, loginContextKeys } = LoginContext;
const { CommonKeys } = commonConst;
const { IBusiRoleConst } = CommonKeys;

const formId = AREA_CARD.formId;
const tableId = AREA_CARD.tableId;
const pageId = AREA_CARD.pagecode;

export default function(props) {
	let _this = this;
	props.createUIDom(
		{
			pagecode: pageId
		},
		function(data) {
			if (data) {
				let status = props.getUrlParam('status');
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.template) {
					let meta = data.template;
					modifierMeta.call(_this, props, meta);
					props.meta.setMeta(meta, () => {
						initAfter.call(_this, props);
					});
				}
			}
		}
	);
}
function initAfter(props) {
	if (this.nowUIStatus != 'add') {
		if (this.nowUIStatus == 'copyAdd') {
			copydata.call(this, this.state.pk_apply);
		} else {
			let pk = this.props.getUrlParam('id');
			if (pk && pk != 'undefined') {
				getdata.call(this, pk);
			}
		}
	} else {
		init.call(this, props);
	}
}

//添加操作列、设置参照过滤条件
function modifierMeta(props, meta) {
	let _this = this;
	let transi_type = _this.transi_type;
	let status = props.getUrlParam('status');
	let pk_group = getContext(loginContextKeys.groupId);
	meta[formId].status = status;
	meta[tableId].status = status;

	meta[tableId].items.push({
		attrcode: 'opr',
		label: getMultiLangByID('amcommon-000000') /* 国际化处理： 操作*/,
		visible: true,
		itemtype: 'customer',
		className: 'table-opr',
		width: 130,
		fixed: 'right',
		render(text, record, index) {
			let status = props.cardTable.getStatus(tableId);
			var bill_status = props.form.getFormItemsValue(formId, 'bill_status').value;
			let isClose = record.values.close_flag.value;
			let buttonArys;

			if (bill_status === '3' || bill_status === '6') {
				//审批通过 或者 关闭状态
				if (isClose) {
					//关闭状态
					buttonArys = [ 'OpenCard', 'OpenLine' ];
				} else {
					buttonArys = [ 'OpenCard', 'CloseLine' ];
				}
			} else {
				buttonArys = [ 'OpenCard' ];
			}
			let buttonAry = status === 'browse' ? buttonArys : [ 'OpenCard', 'DelLine' ];

			return props.button.createOprationButton(buttonAry, {
				area: 'card_body_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => {
					tableButtonClick.call(_this, props, key, text, record, index);
				}
			});
		}
	});
	//表头参照过滤
	meta[formId].items.map((item) => {
		if (item.attrcode == 'pk_deployin_ownerunit_v') {
			//调入货主
			item.queryCondition = () => {
				let data = props.form.getFormItemsValue(formId, 'pk_org').value; // 调用相应组件的取值API
				return {
					pk_org: data,
					TreeRefActionExt: 'nccloud.web.aum.deploy.apply.refcondition.PK_DEPLOYIN_OWNERUNIT_VSqlBuilder'
				}; // 根据pk_org过滤
			};
		} else if (item.attrcode == 'pk_unit_used_v') {
			//使用权
			item.queryCondition = () => {
				let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value; // 主组织
				return {
					pk_org,
					TreeRefActionExt: 'nccloud.web.aum.deploy.apply.refcondition.PK_UNIT_USED_VSqlBuilder'
				}; // 根据pk_org过滤
			};
		} else if (item.attrcode == 'pk_usedept_v') {
			//使用部门
			item.queryCondition = () => {
				let pk_unit_used = props.form.getFormItemsValue(formId, 'pk_unit_used').value; // 使用权
				return { pk_org: pk_unit_used }; // 根据pk_org过滤
			};
		} else if (item.attrcode == 'pk_user') {
			//使用人
			item.queryCondition = () => {
				let pk_unit_used = props.form.getFormItemsValue(formId, 'pk_unit_used'); // 使用权
				let pk_usedept = props.form.getFormItemsValue(formId, 'pk_usedept'); // 使用部门
				if (pk_unit_used) {
					pk_unit_used = pk_unit_used.value;
				}
				if (pk_usedept) {
					pk_usedept = pk_usedept.value;
				}
				return { pk_org: pk_unit_used, pk_dept: pk_usedept, busifuncode: IBusiRoleConst.ASSETORG }; // 根据pk_org过滤
			};
		} else if (item.attrcode == 'pk_applier') {
			//申请人
			item.queryCondition = () => {
				let pk_org = props.form.getFormItemsValue(formId, 'pk_org'); // 资产组织
				if (pk_org) {
					pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
				}
				return { pk_org, busifuncode: IBusiRoleConst.ASSETORG, isShowUnit: true }; // 根据pk_org过滤
			};
		} else if (item.attrcode == 'pk_org_v' || item.attrcode == 'pk_deployout_org_v') {
			item.queryCondition = () => {
				return { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgVidRefFilter' };
			};
		} else if (item.attrcode == 'pk_deployin_ownerunit_v') {
			item.queryCondition = () => {
				return { TreeRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgVidRefFilter' };
			};
		} else {
			defRefCondition.call(this, props, item, formId, pk_group);
		}
	});

	//表体参照过滤
	meta[tableId].items.map((item) => {
		if (item.attrcode == 'pk_equip') {
			item.isMultiSelectedEnabled = true;

			//给资产编码列添加超链接
			item.renderStatus = 'browse';
			item.render = (text, record, index) => {
				return (
					<div class="card-table-browse">
						<span
							className="code-detail-link"
							onClick={() => {
								openEquipCardByPk(props, record.values.pk_equip.value);
							}}
						>
							{record.values.pk_equip && record.values.pk_equip.display}
						</span>
					</div>
				);
			};

			item.queryCondition = () => {
				// 添加过滤方法
				let data = props.form.getFormItemsValue(formId, [ 'pk_deployout_org' ])[0].value || ''; // 获取前面选中参照的值
				let deploy_type = props.form.getFormItemsValue(formId, [ 'deploy_type' ])[0].value || '';
				return {
					pk_deployout_org: data,
					deploy_type: deploy_type,
					transi_type: transi_type,
					bill_type: BILLTYPE,
					GridRefActionExt: 'nccloud.web.aum.deploy.apply.refcondition.PK_EQUIPSqlBuilder'
				}; // 根据pk_org过滤
			};
		}

		if (item.attrcode == 'pk_jobmngfil') {
			//项目
			item.queryCondition = () => {
				let pk_group = '';
				let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value; //资产组织
				if (props.initInfo) {
					pk_group = props.initInfo.pk_group;
				}
				return { pk_org, pk_group };
			};
		} else if (item.attrcode == 'pk_material_v') {
			//物料
			item.queryCondition = () => {
				let pk_group = '';
				let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value; //资产组织
				if (props.initInfo) {
					pk_group = props.initInfo.pk_group;
				}
				return { pk_org, pk_group };
			};
		} else if (item.attrcode == 'pk_category') {
			//类别
			item.onlyLeafCanSelect = true;
			item.queryCondition = () => {
				return { pk_group, m_bNotLeafSelectedEnabled: false };
			};
		} else {
			defRefCondition.call(this, props, item, formId, pk_group);
		}
	});

	// 卡片模板修改后公共处理
	meta = afterModifyCardMeta.call(this, props, meta);

	return meta;
}
