import { ajax, output, toast, print, cacheTools, cardCache } from 'nc-lightapp-front';

import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { ScriptReturnUtils, LoginContext, queryAboutUtils, queryVocherUtils, saveValidatorsUtil } = components;
const { getScriptCardReturnData } = ScriptReturnUtils;
const { openEquipCard, openBillTrack, openApprove } = queryAboutUtils;
const { queryAboutVoucher } = queryVocherUtils;
const { beforeSaveValidator } = saveValidatorsUtil;
const { multiLangUtils, cardUtils, msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { setCardValue, setHeadAreaData } = cardUtils;
const { getContext, loginContextKeys } = LoginContext;
const { StatusUtils, ButtonConst, TransferConst } = commonConst;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { UISTATE } = StatusUtils;
const { openReportEquip, LinkReportConst } = queryAboutUtils;
import { pageConfig } from '../const';
import { headAfterEvent } from './afterEvent';

const {
	AREA_CARD,
	printFuncode,
	printNodekey,
	printFilename,
	printCardUrl,
	EditOpr_url,
	node_code,
	copydata_url,
	BILLTYPE,
	AREA_LIST,
	dataSource,
	BatchAlter_url
} = pageConfig;
export default function(props, id) {
	let _this = this;
	let pk = props.form.getFormItemsValue(this.formId, 'pk_apply').value;
	switch (id) {
		case 'Add':
			this.old_pk = pk;
			props.form.EmptyAllFormValue(this.formId);
			props.cardTable.setTableData(this.tableId, { rows: [] });
			init.call(this, props);
			break;
		case 'Edit':
			ajax({
				url: EditOpr_url,
				data: {
					pk: pk,
					resourceCode: node_code
				},
				success: (res) => {
					if (res.data) {
						toast({ color: 'danger', content: res.data });
					} else {
						props.cardTable.closeExpandedRow(this.tableId);
						toggleShow.call(this, UISTATE.edit);
					}
				}
			});
			break;
		case 'Revise': //修订
			this.old_pk = pk;
			toggleShow.call(this, 'Revise');
			break;
		case 'Delete':
			deleteConfirm.call(this, props);
			break;
		case 'Save':
			saveClick.call(this);
			break;
		case 'Commit':
			commitClick.call(this, 'SAVE', 'commit');
			break;
		case 'SaveCommit':
			commitClick.call(this, 'SAVE', 'saveCommit');
			break;
		case 'UnCommit':
			commitClick.call(this, 'UNSAVE', '');
			break;
		case 'QueryAboutBusiness':
			openBillTrack(_this);
			break;
		case 'QueryAboutBillFlow':
			_this.setState({
				showApprove: true,
				transi_type: props.form.getFormItemsValue(this.formId, 'transi_type').value
			});
			break;
		case 'QueryAboutCard':
			openEquipCard(_this, props, this.tableId);
			break;
		case 'QueryAboutVoucher':
			queryAboutVoucher.call(this, props, this.formId, 'pk_apply', AREA_CARD.appcode); // tableId为区域编码，第三个参数为主键编码，appcode为应用编码
			break;
		case 'Cancel':
			cancelConfirm.call(this, props, pk);
			break;
		case 'AddLine':
			props.cardTable.addRow(this.tableId);
			setBtnsEnable.call(this, props, this.tableId);
			break;
		case 'Refresh':
			let pk_apply = props.form.getFormItemsValue(this.formId, 'pk_apply').value;
			getdata.call(this, pk_apply, true);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'DelLine':
			delLineClick.call(this, props);
			break;
		case 'BatchAlter':
			BatchAlter.call(this, props, this.tableId);
			break;
		case 'Attachment':
			attachmentClick.call(this, props);
			break;
		case 'Copy':
			this.old_pk = pk;
			copycard.call(this, props);
			break;
		case 'QueryAboutEquipReport':
			QueryAboutEquipReport.call(this, props);
		default:
			break;
	}
}
/**
 * 删除弹框提示
 * @param {*} props 
 */
function deleteConfirm(props) {
	showConfirm.call(this, props, { type: MsgConst.Type.Delete, beSureBtnClick: delConfirm });
}

function QueryAboutEquipReport(props) {
	let checkData = props.cardTable.getCheckedRows(this.tableId);
	let rowData;
	if (checkData.length == 0) {
		// checkData = props.cardTable.getVisibleRows(this.tableId);
		rowData = null;
	} else {
		rowData = checkData[0].data;
	}
	let equip_name;
	let pk_category;
	let pk_material_v;
	if (rowData) {
		equip_name = rowData.values.equip_name.value;
		pk_category = rowData.values.pk_category.value;
		pk_material_v = rowData.values.pk_material_v.value;
	} else {
		equip_name = null;
		pk_category = null;
		pk_material_v = null;
	}

	//根据当前选中表体行的“设备类别、设备名称、物料编码”查询设备台帐中货主管理组织
	//或者使用管理组织为调出组织，且卡片的状态分类=闲置类的设备卡片。
	let pk_deployout_org = props.form.getFormItemsValue(this.formId, 'pk_deployout_org').value;

	let map = {};
	let mapValue = {};
	mapValue['pk_deployout_org'] = pk_deployout_org;
	let conditions = [];
	map['card_status'] = 3;
	map['pk_used_status.status_type'] = 2;
	mapValue['card_status'] = 3;
	mapValue['pk_used_status.status_type'] = 2;

	conditions.push({
		field: 'card_status',
		value: { firstvalue: 3, secondvalue: '' },
		oprtype: '=',
		display: '',
		isIncludeSub: false,
		refurl: ''
	});
	conditions.push({
		field: 'pk_used_status.status_type',
		value: { firstvalue: 2, secondvalue: '' },
		oprtype: '=',
		display: '',
		isIncludeSub: false,
		refurl: ''
	});
	if (pk_deployout_org) {
		map['pk_ownerorg'] = pk_deployout_org;
		map['pk_usedorg'] = pk_deployout_org;
		mapValue['pk_ownerorg'] = pk_deployout_org;
		mapValue['pk_usedorg'] = pk_deployout_org;
	}
	if (equip_name) {
		map['equip_name'] = equip_name;
		mapValue['equip_name'] = equip_name;
	}
	if (pk_category) {
		map['pk_category'] = pk_category;
		mapValue['pk_category'] = pk_category;
	}
	if (pk_material_v) {
		map['pk_material_v'] = pk_material_v;
		mapValue['pk_material_v'] = pk_material_v;
	}
	let querycondition = {
		logic: 'and',
		conditions: conditions
	};
	cacheTools.set(BILLTYPE + `condition`, map);
	mapValue[LinkReportConst.GETSQLACTION] = 'nccloud.pub.aum.report.insurance.InsuranceSqlBuilder';
	openReportEquip.call(this, props, LinkReportConst.REPORT, BILLTYPE + `condition`, querycondition, mapValue);
}

/**
 * 取消
 * @param {*} props 
 */
export function cancelConfirm(props, pk) {
	let cancelClick = () => {
		// 恢复字段的可编辑性
		let pk_org_v = props.form.getFormItemsValue(this.formId, 'pk_org_v');
		if (!pk_org_v || !pk_org_v.value) {
			props.resMetaAfterPkorgEdit();
		}
		// 加载编辑前数据，如果编辑前没有数据，则加载当前列表最后一条数据，如果还没有，显示为空不处理
		if (!pk) {
			pk = cardCache.getCurrentLastId(dataSource);
		}
		this.nowUIStatus = 'browse';
		if (this.old_pk && this.old_pk != '') {
			getdata.call(this, this.old_pk);
		} else {
			getdata.call(this, pk);
		}
	};
	showConfirm.call(this, props, {
		type: MsgConst.Type.Cancel,
		beSureBtnClick: cancelClick
	});
}

//卡片界面复制
function copycard(props) {
	let pk_apply = props.form.getFormItemsValue(this.formId, 'pk_apply').value;
	let data = { pk: pk_apply, pagecode: AREA_CARD.pagecode };
	ajax({
		url: copydata_url,
		data,
		success: (res) => {
			if (res.data) {
				setCardValue.call(this, this.props, res.data);
				toggleShow.call(this, 'copyAdd');
			} else {
				showMessage.call(this, props, { type: MsgConst.Type.DataDeleted });
				/* 国际化处理： 数据已经被删除*/
			}
		}
	});
}

/**
 * 附件上传
 * @param {*} props 
 */
function attachmentClick(props) {
	let billId = props.form.getFormItemsValue(this.formId, 'pk_apply').value;
	props.ncUploader.show('warcontract-uploader', {
		billId: 'pam/deploy_apply/' + billId
	});
}

/**
 * 批改
 * @param {*} props 
 */
function BatchAlter(props, tableId) {
	// 获取原始数据
	let changeData = props.cardTable.getTableItemData(tableId);

	let cardData = props.createMasterChildDataSimple(AREA_CARD.pagecode, AREA_CARD.formId, tableId);
	// 资产名称不可批改（改为前台控制，多语文本类型走后台报错）
	if (changeData.batchChangeKey != 'equip_name') {
		ajax({
			url: BatchAlter_url,
			data: {
				card: cardData,
				batchChangeIndex: changeData.batchChangeIndex, // 原始数据行号
				batchChangeKey: changeData.batchChangeKey, // 原始数据key
				batchChangeValue: changeData.batchChangeValue, // 原始数据value // 原始数据所在的vo，对应聚合VO中的tableCodes，一主一子可为空，一主多子要传对应的值
				tableCode: ''
			},
			async: false,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					setCardValue.call(this, props, data);
				}
			}
		});
	}
}
/**
 * 删行
 * @param {*} props 
 */
function delLineClick(props) {
	//选中的数据
	let checkedRows = props.cardTable.getCheckedRows(this.tableId);
	let checkedIndex = [];
	checkedRows.map((item) => {
		checkedIndex.push(item.index);
	});
	props.cardTable.delRowsByIndex(this.tableId, checkedIndex);
	setBatchBtnsEnable.call(this, props, this.tableId);
	setBtnsEnable.call(this, props, this.tableId);
}

/**
 * 打印
 * @param {*} props 
 */
export function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	let { pageConfig } = props;
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		/* 国际化处理： 请选择需要打印的数据*/
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		printCardUrl, // 后台打印服务url
		printData
	);
}

/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });
		/* 国际化处理： 请选择需要输出的数据*/
		return;
	}
	output({
		url: printCardUrl,
		data: printData
	});
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let pk = props.form.getFormItemsValue(this.formId, 'pk_apply');
	if (!pk || !pk.value) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		/* 国际化处理： 请选择需要打印的数据*/
		return;
	}

	let pks = [ pk.value ];
	let printData = {
		filename: getMultiLangByID(printFilename), // 文件名称
		nodekey: printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

/**
 * 设置批量按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBatchBtnsEnable(props, moduleId) {
	let checkedRows = props.cardTable.getCheckedRows(moduleId);
	let batchBtns = [ 'QueryAboutCard', 'DelLine' ];
	props.button.setButtonDisabled(batchBtns, !(checkedRows && checkedRows.length > 0));
}

/**
 * 设置批改按钮的状态
 * @param {} props 
 * @param {*} moduleId 
 */
export function setBtnsEnable(props, moduleId) {}

/**
 * 页面初始化数据
 */
export function init(props) {
	setDefaultValue.call(this);
	let pk_org_v = props.form.getFormItemsValue(this.formId, 'pk_org_v');
	props.form.setFormItemsDisabled(this.formId, { pk_org_v: false, pk_org: false });
	if (pk_org_v && pk_org_v.value && pk_org_v.value != null && pk_org_v.value != '') {
		headAfterEvent.call(this, props, this.formId, 'pk_org_v', pk_org_v, { value: '' }, pk_org_v.value);
	} else {
		props.initMetaByPkorg('pk_org_v');
		this.props.button.setButtonDisabled([ 'AddLine', 'DelLine', 'BatchAlter' ], true);
	}
}

export function setDefaultValue(isNoOrg = false) {
	this.props.form.setFormItemsValue(this.formId, {
		bill_status: { value: '0', display: getMultiLangByID('statusUtils-000000') /* 国际化处理： 自由态*/ },
		bill_type: { value: BILLTYPE, display: BILLTYPE },
		transi_type: { value: getContext(loginContextKeys.transtype), display: getContext(loginContextKeys.transtype) },
		pk_transitype: { value: getContext(loginContextKeys.pk_transtype) },
		pk_group: { value: getContext(loginContextKeys.groupId), display: getContext(loginContextKeys.groupName) }
	});
	if (!isNoOrg) {
		this.props.form.setFormItemsValue(this.formId, {
			pk_org_v: {
				value: getContext(loginContextKeys.pk_org_v),
				display: getContext(loginContextKeys.org_v_Name)
			},
			pk_org: { value: getContext(loginContextKeys.pk_org), display: getContext(loginContextKeys.org_Name) }
		});
	}
	toggleShow.call(this, 'add');
}

//切换页面状态
export function toggleShow(status) {
	if (!status) {
		status = this.nowUIStatus;
	} else {
		this.nowUIStatus = status;
	}
	// 更新参数
	let pkVal = this.props.form.getFormItemsValue(AREA_CARD.formId, 'pk_apply');
	this.props.setUrlParam({ status, id: pkVal ? pkVal.value : '' });

	let flag = status === 'browse' ? false : true;

	let editButtons = [ 'Save', 'Cancel', 'AddLine', 'DelLine', 'BatchAlter', 'SaveCommit', 'OpenCard' ];
	let borwseButtons = [
		'Edit',
		'Add',
		'Back',
		'Delete',
		'Commit',
		'UnCommit',
		'Print',
		'PrintG',
		'Preview',
		'Output',
		'OpenCard',
		'Attachment',
		'QueryAbout',
		'Copy',
		'QueryAboutGroup',
		'QueryAboutCard',
		'QueryAboutBusiness',
		'QueryAboutVoucher',
		'QueryAboutBillFlow',
		'Revise',
		'Refresh'
	];
	//按钮的显示状态
	if (status === 'edit' || status === 'add' || status === 'copyAdd') {
		this.props.button.setButtonVisible(borwseButtons, false);
		this.props.button.setButtonVisible(editButtons, true);
		this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false);
		if (status === 'edit' || status === 'copyAdd') {
			this.props.form.setFormItemsDisabled(this.formId, { pk_org_v: true });
			let deploy_type = this.props.form.getFormItemsValue(AREA_CARD.formId, 'deploy_type');
			if (deploy_type.value === '1') {
				//所有权调拨 value 1  调入货主 pk_deployin_ownerunit  可编辑 否则 不可编辑
				this.props.form.setFormItemsDisabled(AREA_CARD.formId, { pk_deployin_ownerunit_v: false });
			} else {
				this.props.form.setFormItemsValue(AREA_CARD.formId, {
					pk_deployin_ownerunit_v: { value: '', display: '' }
				});
				this.props.form.setFormItemsDisabled(AREA_CARD.formId, { pk_deployin_ownerunit_v: true });
			}
			setBatchBtnsEnable.call(this, this.props, this.tableId);
			setBtnsEnable.call(this, this.props, this.tableId);
		} else {
			//新增设置按钮的禁用状态
			this.props.button.setButtonDisabled([ 'QueryAboutCard', 'DelLine' ], true);
		}
	} else if (status == 'Revise') {
		this.props.button.setButtonVisible(borwseButtons, false);
		this.props.button.setButtonVisible(editButtons, true);
		this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false);
		this.props.form.setFormItemsDisabled(this.formId, {
			deploy_type: false,
			pk_deployin_ownerunit: false,
			pk_deployout_org_v: false
		});
		this.props.form.setFormItemsDisabled(this.formId, {
			pk_org_v: true,
			pk_org: true,
			bill_code: true,
			pk_unit_used_v: true,
			pk_user: true,
			pk_applier: true,
			memo: true
		});
		let deploy_type = this.props.form.getFormItemsValue(AREA_CARD.formId, [ 'deploy_type' ])[0].value;
		if (deploy_type && deploy_type == '1') {
			this.props.form.setFormItemsDisabled(AREA_CARD.formId, { pk_deployin_ownerunit_v: false });
		} else {
			this.props.form.setFormItemsValue(AREA_CARD.formId, {
				pk_deployin_ownerunit_v: { value: '', display: '' }
			});
			this.props.form.setFormItemsDisabled(AREA_CARD.formId, { pk_deployin_ownerunit_v: true });
		}
		this.props.button.setButtonVisible(
			[ 'QueryAbout', 'QueryAboutGroup', 'QueryAboutCard', 'QueryAboutBusiness', 'QueryAboutVoucher' ],
			true
		);
		this.props.button.setButtonVisible([ 'SaveCommit' ], false);
	} else {
		this.props.button.setButtonVisible(editButtons, false);
		this.props.button.setButtonVisible(borwseButtons, true);
		if (this.props.form.getFormItemsValue(this.formId, [ 'bill_status' ])[0] != undefined) {
			let bill_status = this.props.form.getFormItemsValue(this.formId, [ 'bill_status' ])[0].value;
			if (bill_status === '0') {
				//自由态
				this.props.button.setButtonVisible([ 'UnCommit', 'Revise' ], false); //不显示收回
			} else if (bill_status === '1') {
				//已提交
				this.props.button.setButtonVisible([ 'Commit', 'Delete', 'Edit' ], false); //不显示提交、修改、删除
				this.props.button.setButtonVisible([ 'Revise' ], true);
			} else if (bill_status === '2') {
				//审批中
				this.props.button.setButtonVisible([ 'Commit', 'Delete', 'Edit', 'UnCommit' ], false); //不显示提交、修改、删除、收回
			} else if (bill_status === '3') {
				//审批通过
				this.props.button.setButtonVisible([ 'Commit', 'Delete', 'Edit', 'Revise' ], false); //不显示提交、修改、删除
			} else if (bill_status === '4') {
				//审批未通过
				this.props.button.setButtonVisible([ 'UnCommit', 'Revise', 'Commit' ], false); //不显示收回
				// this.props.button.setButtonVisible(
				// 	[ 'Commit', 'Delete', 'Edit', 'UnCommit', 'QueryAboutBillFlow', 'Revise' ],
				// 	false
				// ); //不显示提交、修改、删除、收回
			} else if (bill_status === '6') {
				//关闭
				this.props.button.setButtonVisible([ 'Commit', 'Delete', 'Edit', 'UnCommit', 'Revise' ], false); //不显示提交、修改、删除、收回
			} else {
				//新增时取消过来的
				this.props.button.setButtonVisible([ 'Commit', 'Delete', 'Edit', 'UnCommit', 'Revise' ], false); //不显示提交、修改、删除、收回
			}
		} else {
			this.props.button.setButtonVisible([ 'Commit', 'Delete', 'Edit', 'UnCommit', 'Revise' ], false); //不显示提交、修改、删除、收回
		}
		this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', true);
	}
	this.props.form.setFormStatus(this.formId, status);
	this.props.cardTable.setStatus(this.tableId, status == 'add' ? 'edit' : status);
	if (status == 'copyAdd' || status == 'Revise') {
		this.props.form.setFormStatus(this.formId, 'edit');
		this.props.cardTable.setStatus(this.tableId, 'edit');
	}
	let bill_code = this.props.form.getFormItemsValue(this.formId, [ 'bill_code' ])[0].value;
	setHeadAreaData.call(this, this.props, { bill_code, status });
}

export function setEmptyData() {
	this.props.form.EmptyAllFormValue(this.formId);
	this.props.cardTable.setTableData(this.tableId, { rows: [] });
	toggleShow.call(this, 'browse');
	let borwseButtons = [
		'Edit',
		'Back',
		'Delete',
		'Commit',
		'UnCommit',
		'Print',
		'PrintG',
		'Preview',
		'Output',
		'OpenCard',
		'Attachment',
		'QueryAbout',
		'Copy',
		'QueryAboutGroup',
		'QueryAboutCard',
		'QueryAboutBusiness',
		'QueryAboutVoucher',
		'QueryAboutBillFlow',
		'Revise',
		'Refresh'
	];
	this.props.button.setButtonVisible(borwseButtons, false);
}

//通过单据id查询单据信息
export function getdata(pk, isRefresh = false) {
	if (pk == null || pk == '') {
		setEmptyData.call(this);
		return;
	}
	let cachData = cardCache.getCacheById(pk, dataSource);
	if (cachData && !isRefresh) {
		setCardValue.call(this, this.props, cachData);
		toggleShow.call(this);
		this.setState({
			pk_apply: pk
		});
	} else {
		let data = { pk, pagecode: AREA_CARD.pagecode };
		ajax({
			url: '/nccloud/aum/deploy/apply_querycard.do',
			data,
			success: (res) => {
				if (res.data) {
					setCardValue.call(this, this.props, res.data);
					toggleShow.call(this, this.nowUIStatus);
					this.setState({
						pk_apply: pk
					});
					if (isRefresh) {
						showMessage.call(this, this.props, { type: MsgConst.Type.RefreshSuccess });
						/* 国际化处理： 刷新成功*/
					}
					cardCache.updateCache('pk_apply', pk, res.data, this.formId, dataSource);
				} else {
					this.props.form.cancel(this.formId);
					cardCache.deleteCacheById('pk_apply', pk, dataSource);
					setEmptyData.call(this);
					showMessage.call(this, this.props, { type: MsgConst.Type.DataDeleted });
					/* 国际化处理： 数据已删除*/
				}
			}
		});
	}
	// this.props.setUrlParam({ id: pk });
}

//复制数据--列表界面点击复制
export function copydata(pk) {
	let data = { pk, pagecode: AREA_CARD.pagecode };
	ajax({
		url: copydata_url,
		data,
		success: (res) => {
			if (res.data) {
				setCardValue.call(this, this.props, res.data);
				toggleShow.call(this, 'copyAdd');
				// putValueToView.call(this, res.data);
			} else {
				showMessage.call(this, this.props, { type: MsgConst.Type.DataDeleted });
				/* 国际化处理： 数据已经被删除*/
				toggleShow.call(this, 'add');
				setDefaultValue.call(this);
			}
		}
	});
}

//设置数据到界面展示
export function putValueToView(data) {
	toggleShow.call(this, 'copyAdd');
	if (data.head) {
		this.props.form.setAllFormValue({ [this.formId]: data.head[this.formId] });
	}
	if (data.body) {
		this.props.cardTable.setTableData(this.tableId, data.body[this.tableId]);
	}
}

/**
 * 业务校验方法
 */
export function businessValidator(isCommit) {
	//资产编码
	let pk_equip = this.props.cardTable.getColValue(this.tableId, 'pk_equip', false, false);
	//资产名称
	let equip_name = this.props.cardTable.getColValue(this.tableId, 'equip_name', false, false);
	//资产类别
	let pk_category = this.props.cardTable.getColValue(this.tableId, 'pk_category', false, false);
	let flag = true;
	let content = '';
	pk_equip.map((e, index) => {
		if (!pk_equip[index].value && !equip_name[index].value && !pk_category[index].value) {
			flag = false;
			let num = index + 1;
			content +=
				'<div>' +
				getMultiLangByID('452003004A-000005', {
					num
				}) +
				'</div>' /* 国际化处理：<div>第{num}行资产编码、资产名称、资产类别至少有一项不能为空；</div> */;
		}
	});
	let deploy_type = this.props.form.getFormItemsValue(this.formId, 'deploy_type').value;
	let pk_deployout_org_v = this.props.form.getFormItemsValue(this.formId, 'pk_deployout_org_v').value;
	//提交额外走的校验
	if (isCommit && (!deploy_type || deploy_type == undefined || deploy_type == '')) {
		flag = false;
		content += '<div>' + getMultiLangByID('452003004A-000004') + '</div>' /* 国际化处理： <div>调拨类型不能为空</div> */;
	}
	if (isCommit && (!pk_deployout_org_v || pk_deployout_org_v == undefined || pk_deployout_org_v == '')) {
		flag = false;
		content += '<div>' + getMultiLangByID('452003004A-000014') + '</div>' /* 国际化处理： <div>调处组织不能为空</div> */;
	}
	if (!flag) {
		toast({
			content,
			color: 'danger',
			isNode: true
		});
	}
	return flag;
}

//保存单据
export function saveClick() {
	// 保存前校验
	let pass = beforeSaveValidator.call(this, this.props, this.formId, this.tableId, []);
	if (!pass) {
		return;
	}
	//业务校验方法
	if (!businessValidator.call(this)) {
		return;
	}
	let saveStatus = UISTATE.add;
	let CardData = this.props.createMasterChildDataSimple(this.pagecode, this.formId, this.tableId);
	let url = '/nccloud/aum/deploy/apply_save.do'; //新增保存
	if (this.nowUIStatus === UISTATE.edit || this.nowUIStatus === 'Revise') {
		saveStatus = this.nowUIStatus;
		url = '/nccloud/aum/deploy/apply_update.do'; //修改保存
	}
	this.props.validateToSave(CardData, () => {
		ajax({
			url: url,
			data: CardData,
			success: (res) => {
				let pk_apply = null;
				if (res.success) {
					if (res.data) {
						this.props.form.setFormStatus(this.formId, UISTATE.browse);
						setCardValue.call(this, this.props, res.data);
						pk_apply = res.data.head[this.formId].rows[0].values.pk_apply.value;
						this.setState({
							pk_apply: pk_apply
						});
					}
					let cachData = this.props.createMasterChildData(this.pagecode, this.formId, this.tableId);
					// 保存成功后处理缓存
					if (saveStatus == UISTATE.add) {
						cardCache.addCache(pk_apply, cachData, this.formId, dataSource);
					} else {
						cardCache.updateCache('pk_apply', pk_apply, cachData, this.formId, dataSource);
					}
					this.old_pk = pk_apply;
					toggleShow.call(this, UISTATE.browse);
					showMessage.call(this, this.props, { type: MsgConst.Type.SaveSuccess });
					/* 国际化处理： 保存成功*/
				}
			}
		});
	});
}

//删除单据
export function delConfirm() {
	let pkField = 'pk_apply';
	let pk = this.props.form.getFormItemsValue(this.formId, pkField).value;
	let paramInfoMap = {};
	paramInfoMap[pk] = this.props.form.getFormItemsValue(this.formId, 'ts').value;
	ajax({
		url: '/nccloud/aum/deploy/apply_delete.do',
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: '452003004A_card'
		},
		success: (res) => {
			let { success, data } = res;
			if (res) {
				getScriptCardReturnData.call(
					this,
					res,
					this.props,
					this.formId,
					this.tableId,
					pkField,
					dataSource,
					null,
					true,
					getdata,
					this.pagecode
				);
			}
		}
	});
}

export function modelSave(props) {
	// props.cardTable.closeModel(this.tableId);
	saveClick.call(this);
}

//提交  保存提交 收回
export function commitClick(operator, commitType, content) {
	//提交 保存提交都走校验
	if (operator === 'SAVE') {
		// 保存前校验
		let pass = beforeSaveValidator.call(this, this.props, this.formId, this.tableId, []);
		if (!pass) {
			return;
		}
		//业务校验方法
		if (!businessValidator.call(this, true)) {
			return;
		}
	}
	let pk = this.props.form.getFormItemsValue(this.formId, 'pk_apply').value;
	let CardData = this.props.createMasterChildDataSimple(this.pagecode, this.formId, this.tableId);
	let paramInfoMap = {};
	paramInfoMap[pk] = ''; //卡片页面ts为空也可以 CardData 里有ts
	let obj = {
		dataType: 'cardData',
		OperatorType: operator,
		commitType: commitType,
		pageid: this.pagecode,
		paramInfoMap: paramInfoMap,
		content: content
	};
	CardData.userjson = JSON.stringify(obj);
	if (commitType === 'saveCommit') {
		this.props.validateToSave(CardData, () => {
			ajax({
				url: '/nccloud/aum/deploy_apply/commit.do',
				data: CardData,
				success: (res) => {
					if (res.success) {
						if (content) {
							this.setState({
								compositedisplay: false
							});
						}
						let callback = (pk_apply, data) => {
							toggleShow.call(this, UISTATE.browse);
							this.setState({ pk_apply: pk_apply });
							this.old_pk = pk_apply;
						};
						let setValue = (data) => {
							this.props.form.setFormStatus(this.formId, UISTATE.browse);
							setCardValue.call(this, this.props, data);
						};
						getScriptCardReturnData.call(
							this,
							res,
							this.props,
							this.formId,
							this.tableId,
							'pk_apply',
							dataSource,
							setValue,
							false,
							callback,
							this.pagecode
						);
					}
				}
			});
		});
	} else {
		ajax({
			url: '/nccloud/aum/deploy_apply/commit.do',
			data: CardData,
			success: (res) => {
				if (res.success) {
					if (content) {
						this.setState({
							compositedisplay: false
						});
					}
					let callback = () => {
						toggleShow.call(this, UISTATE.browse);
					};
					getScriptCardReturnData.call(
						this,
						res,
						this.props,
						this.formId,
						this.tableId,
						'pk_apply',
						dataSource,
						null,
						false,
						callback,
						this.pagecode
					);
				}
			}
		});
	}
}

/**
 * 切换调出组织
 */
export function changeEdeployout() {
	let rows = this.props.cardTable.getVisibleRows(AREA_CARD.tableId);
	let indexs = [];
	let rowids = rows.map((v, index) => {
		//取出带设备的表行
		let rowid = v.rowid;
		let pk_equip = v.values.pk_equip.value;
		if (pk_equip && pk_equip != '') {
			indexs.push(index);
		}
	});
	this.props.cardTable.delRowsByIndex(AREA_CARD.tableId, indexs);
	let pk_org_v = this.props.form.getFormItemsValue(AREA_CARD.formId, [ 'pk_org_v' ])[0].value;
	let value = this.props.form.getFormItemsValue(AREA_CARD.formId, [ 'pk_deployout_org_v' ])[0];
	if (value.value === pk_org_v) {
		this.props.form.setFormItemsValue(AREA_CARD.formId, { pk_deployout_org_v: { value: '', display: '' } });
		toast({ content: getMultiLangByID('452003004A-000001') /* 国际化处理：调出组织与资产组织不能相同！ */, color: 'danger' });
		return;
	}
}

export function cancelBtnClick() {
	this.props.form.setFormItemsValue(AREA_CARD.formId, { pk_deployout_org_v: this.pk_deployout_org_v });
}
