import { toast } from 'nc-lightapp-front';
import { pageConfig } from '../const';
const { AREA_CARD, BILLTYPE } = pageConfig;
const formId = AREA_CARD.formId;
const tableId = AREA_CARD.tableId;

import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils, cardUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { afterModifyCardMeta } = cardUtils;
const { LoginContext } = components;
const { loginContext, getContext, loginContextKeys } = LoginContext;
export default function bodyBeforeEvent(props, moduleId, key, value, index, record) {
	let editFlag = true;
	refCondtion.call(this, props, moduleId, key, value, index, record);
	editFlag = isEdit.call(this, props, moduleId, key, value, index, record);
	return editFlag;
}
//设备有值，物料编码、设备名称、设备类别、规格、型号、数量不允许编辑
function isEdit(props, moduleId, key, value, index, record) {
	if (key == 'pk_equip') {
		let pk_deployout_org_v = props.form.getFormItemsValue(formId, 'pk_deployout_org_v'); // 调出组织
		if (!pk_deployout_org_v || !pk_deployout_org_v.value || pk_deployout_org_v.value == '') {
			toast({ content: getMultiLangByID('452003004A-000003') /* 国际化处理： 请先录入调出组织 */, color: 'warning' });
			return false;
		}
	} else if (
		key == 'equip_name' ||
		key == 'pk_category' ||
		key == 'pk_material_v' ||
		key == 'model' ||
		key == 'spec' ||
		key == 'quantity'
	) {
		let pk_equip = props.cardTable.getValByKeyAndIndex(moduleId, index, 'pk_equip').value;
		if (pk_equip) {
			return false;
		} else {
			return true;
		}
	}
	return true;
}
//表体字段参照处理
function refCondtion(props, moduleId, key, value, index, record) {
	let meta = props.meta.getMeta();
	const transi_type = this.transi_type;
	const bill_type = BILLTYPE;
	meta[tableId].items.map((item) => {
		if (item.attrcode == 'pk_equip') {
			let pk_deployout_org = props.form.getFormItemsValue(formId, 'pk_deployout_org').value; // 调出组织
			let deploy_type = props.form.getFormItemsValue(formId, 'deploy_type').value; // 调拨类型
			if (!deploy_type) {
				deploy_type = '1'; //默认所有权调拨
			}
			item.queryCondition = () => {
				return {
					pk_org: pk_deployout_org,
					pk_deployout_org,
					deploy_type,
					transi_type,
					bill_type,
					GridRefActionExt: 'nccloud.web.aum.deploy.apply.refcondition.PK_EQUIPSqlBuilder'
				};
			};
		}
	});
	props.meta.setMeta(meta);
}
