import { ajax, toast, cardCache } from 'nc-lightapp-front';
import { pageConfig } from '../const';
const { AREA_CARD, dataSource } = pageConfig;

import { setBtnsEnable, toggleShow, setBatchBtnsEnable } from './buttonClick';
const formId = AREA_CARD.formId;
const tableId = AREA_CARD.tableId;
const pageId = AREA_CARD.pagecode;

import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

/**
 * 表格操作按钮
 * @param {*} props 
 * @param {*} key 
 * @param {*} text 
 * @param {*} record 
 * @param {*} index 
 */
export default function(props, key, text, record, index) {
	let status = props.getUrlParam('status');
	switch (key) {
		case 'OpenCard':
			let status = props.cardTable.getStatus(tableId);
			if (status == 'edit') {
				props.cardTable.openModel(tableId, 'edit', record, index);
			} else if (status == 'browse') {
				props.cardTable.toggleRowView(tableId, record);
			}
			break;
		case 'DelLine':
			props.cardTable.delRowsByIndex(tableId, index);
			setBatchBtnsEnable.call(this, props, tableId);
			setBtnsEnable.call(this, props, tableId);
			break;
		case 'OpenLine':
			openLine.call(this, props, record);
			break;
		case 'CloseLine':
			closeLine.call(this, props, record);
			break;
		default:
			break;
	}
}

function openLine(props, record) {
	let CardData = props.createMasterChildData(pageId, formId, tableId);
	let rows = [ record.values.pk_apply_b.value ];
	CardData.rows = rows;
	CardData.pagecode = pageId;
	let url = '/nccloud/aum/deploy_apply/openLine.do'; //打开
	ajax({
		url: url,
		data: CardData,
		success: (res) => {
			if (res.success) {
				if (res.data) {
					if (res.data.head && res.data.head[formId]) {
						props.form.setAllFormValue({ [formId]: res.data.head[formId] });
					}
					if (res.data.body && res.data.body[tableId]) {
						props.cardTable.setTableData(tableId, res.data.body[tableId]);
					}
					let pk = props.form.getFormItemsValue(formId, 'pk_apply').value;
					cardCache.updateCache('pk_apply', pk, res.data, formId, dataSource);
				}
				toast({ content: getMultiLangByID('452003004A-000006') /* 国际化处理：打开成功 */, color: 'success' });
				toggleShow.call(this);
			}
		}
	});
}

function closeLine(props, record) {
	let CardData = props.createMasterChildData(pageId, formId, tableId);
	let rows = [ record.values.pk_apply_b.value ];
	CardData.rows = rows;
	CardData.pagecode = pageId;
	let url = '/nccloud/aum/deploy_apply/closeLine.do'; //打开
	ajax({
		url: url,
		data: CardData,
		success: (res) => {
			if (res.success) {
				if (res.data) {
					if (res.data.head && res.data.head[formId]) {
						props.form.setAllFormValue({ [formId]: res.data.head[formId] });
					}
					if (res.data.body && res.data.body[tableId]) {
						props.cardTable.setTableData(tableId, res.data.body[tableId]);
					}
					let pk = props.form.getFormItemsValue(formId, 'pk_apply').value;
					cardCache.updateCache('pk_apply', pk, res.data, formId, dataSource);
				}
				toast({ content: getMultiLangByID('452003004A-000007') /* 国际化处理：关闭成功 */, color: 'success' });
				toggleShow.call(this);
			}
		}
	});
}
