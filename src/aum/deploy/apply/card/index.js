//主子表卡片
import React, { Component } from 'react';
import { createPage, base, high } from 'nc-lightapp-front';
const { NCAffix } = base;
import {
	buttonClick,
	initTemplate,
	afterEvent,
	pageInfoClick,
	bodyBeforeEvent,
	rowSelected,
	modelSave,
	changeEdeployout,
	cancelBtnClick,
	commitClick
} from './events';

import { pageConfig } from './const';
const { AREA_CARD, BILLTYPE, title, dataSource } = pageConfig;

import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils, cardUtils, closeBrowserUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { createCardTitleArea, createCardPaginationArea, getCommonTableHeadLeft } = cardUtils;
const { ApprovalTrans, LoginContext } = components;
const { getContext, loginContextKeys } = LoginContext;

class Card extends Component {
	constructor(props) {
		super(props);
		this.formId = AREA_CARD.formId;
		this.tableId = AREA_CARD.tableId;
		this.pagecode = AREA_CARD.pagecode;
		this.pk_deployout_org_v = {};
		this.state = {
			bill_type: BILLTYPE,
			pk_org: '',
			bill_code: '',
			show: false,
			showApprove: false,
			pk_apply: props.getUrlParam('id'),
			transi_type: '',
			compositedisplay: false,
			compositedata: {}
		};
		this.nowUIStatus = this.props.getUrlParam('status');
		this.old_pk = ''; //记录旧的pk 方便取消时候恢复
		closeBrowserUtils.call(this, props, { form: [ this.formId ], cardTable: [ this.tableId ] });
		initTemplate.call(this, props);
		if (getContext(loginContextKeys.transtype)) {
			this.transi_type = getContext(loginContextKeys.transtype);
		}
	}
	componentDidMount() {}
	//返回列表界面
	backToList = () => {
		this.props.pushTo('/list', {
			pagecode: AREA_CARD.pagecode
		});
	};

	//提交及指派 回调，注意保存提交时候需要调用保存提交的方法
	getAssginUsedr = (content) => {
		commitClick.call(this, 'SAVE', 'commit', content);
	};
	//取消 指派
	turnOff = () => {
		this.setState({
			compositedisplay: false
		});
	};
	//获取列表肩部信息
	getTableHeadLeft = () => {
		return getCommonTableHeadLeft.call(this, this.props);
	};

	//获取列表肩部信息
	getTableHead = () => {
		return (
			<div className="shoulder-definition-area">
				<div className="definition-icons">
					{this.props.button.createButtonApp({
						area: 'card_body',
						buttonLimit: 3,
						onButtonClick: buttonClick.bind(this),
						popContainer: document.querySelector('.nc-bill-table-area')
					})}
				</div>
			</div>
		);
	};

	render() {
		let { cardTable, form, ncmodal, ncUploader } = this.props;
		let { createNCUploader } = ncUploader;
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createModal } = ncmodal;
		const { BillTrack, ApproveDetail } = high;
		let formId = AREA_CARD.formId;
		return (
			<div className="nc-bill-card">
				{createNCUploader('warcontract-uploader', {})}
				<div className="nc-bill-top-area">
					<NCAffix>
						<div className="nc-bill-header-area">
							{/* 标题 title */}
							{createCardTitleArea.call(this, this.props, {
								title: getMultiLangByID(title),
								formId,
								backBtnClick: this.backToList
							})}

							<div className="header-button-area">
								{this.props.button.createButtonApp({
									area: 'card_head',
									buttonLimit: 3,
									onButtonClick: buttonClick.bind(this),
									popContainer: document.querySelector('.header-button-area')
								})}
							</div>
							{createCardPaginationArea.call(this, this.props, {
								formId,
								dataSource,
								pageInfoClick
							})}
						</div>
					</NCAffix>
					<div className="nc-bill-form-area">
						{createForm(this.formId, {
							onAfterEvent: afterEvent.bind(this)
						})}
					</div>
				</div>

				<div className="nc-bill-bottom-area">
					<div className="nc-bill-table-area">
						{createCardTable(this.tableId, {
							tableHeadLeft: this.getTableHeadLeft.bind(this),
							tableHead: this.getTableHead.bind(this),
							modelSave: modelSave.bind(this),
							onAfterEvent: afterEvent.bind(this),
							onBeforeEvent: bodyBeforeEvent.bind(this),
							showIndex: true,
							showCheck: true,
							isAddRow: true,
							onSelected: rowSelected.bind(this),
							onSelectedAll: rowSelected.bind(this)
						})}
					</div>
				</div>
				{/* 确认取消框 */}
				{createModal(`${this.pagecode}-confirm`, { color: 'warning' })}
				<BillTrack
					show={this.state.show}
					close={() => {
						this.setState({ show: false });
					}}
					pk={this.state.pk_apply} //单据id
					type={this.state.bill_type} //单据类型
				/>
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transi_type}
					billid={this.state.pk_apply}
				/>
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002')}
						/* 国际化处理： 指派*/
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				) : (
					''
				)}
				{createModal('onAfterEvent', {
					title: getMultiLangByID('452003004A-000008') /* 国际化处理：注意 */,
					content: getMultiLangByID('452003004A-000009') /* 国际化处理：是否确定进行修改，该操作会清空带资产编码的数据！ */,
					beSureBtnClick: changeEdeployout.bind(this),
					cancelBtnClick: cancelBtnClick.bind(this), //取消按钮事件回调
					closeModalEve: cancelBtnClick.bind(this) //关闭按钮事件回调
				})}
			</div>
		);
	}
}

Card = createPage({
	billinfo: {
		billtype: 'card',
		pagecode: AREA_CARD.pagecode,
		headcode: AREA_CARD.formId,
		bodycode: AREA_CARD.tableId
	}
})(Card);

// ReactDOM.render(<Card />, document.querySelector('#app'));
export default Card;
