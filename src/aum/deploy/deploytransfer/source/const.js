export const pageConfig = {
	//主子结构页面编码
	pageCode: '452003004A_transfer',
	appCode: '452003004A',
	//小应用主键
	appId: '0001Z910000000004NSJ',
	//表头列表code
	headCode: 'head',
	//表体列表code
	bodyCode: 'bodyvos',
	//主子拉平页面编码
	mainPageCode: '452003004A_transfermain',
	//主子拉平显示面板code
	mainCode: 'transfermain',
	//查询区域code
	searchCode: 'search',
	//查询区域模板主键
	search_templetid: '1001Z91000000000RYAX',
	//主表主键字段
	headPkField: 'pk_apply',
	//子表主键字段
	bodyPkField: 'pk_apply_b',
	//缓存命名空间
	dataSourceTransfer: 'aum.deploy.deploytransfer.source',
	//下游卡片路由
	cardRouter: '/card',
	// 单据类型
	billType: '4A40',
	// 上游卡片页面路由，超链接用
	sourceUrl: '/aum/deploy/apply/main/#/card',
	// 上游卡片页面的pagecode
	sourcePageCode: '452003004A_card',
	outNm: '452003004A-000012' /* 国际化处理： 生成所有权调出*/,
	usedOutNm: '452003004A-000013' /* 国际化处理： 生成使用权调出*/,
	// 查询url
	url: {
		srcApplyQuery: '/nccloud/aum/deploy/srcapply_query.do',
		usedSrcApplyQuery: '/nccloud/aum/deploy/usedsrcapply_query.do'
	}
};
