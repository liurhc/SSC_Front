import { pageConfig } from '../const';

import ampub from 'ampub';
const { utils, commonConst } = ampub;
const { transferUtils } = utils;
const { doTansferSearch } = transferUtils;

const { CommonKeys } = commonConst;
const { TransferConst } = CommonKeys;

const { headCode, bodyCode, searchCode, headPkField, bodyPkField, billType, pageCode, appCode } = pageConfig;
//点击查询，获取查询区数据
export default function clickSearchBtn(props, searchVal) {
	let dest_billtype = props.getUrlParam(TransferConst.dest_billtype);
	let url = pageConfig.url.srcApplyQuery;
	if (dest_billtype == '4A47') {
		url = pageConfig.url.usedSrcApplyQuery;
	}

	doTansferSearch(url, props, searchCode, billType, pageCode, headCode, bodyCode, headPkField, bodyPkField, appCode);
}
