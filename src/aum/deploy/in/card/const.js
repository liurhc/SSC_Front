export const formId = 'card_head';

export const tableId = 'card_body_bodyvos';

export const pagecode = '452003016A_card';

export const list_pagecode = '452003016A_list';

export const appid = '0001Z9100000000014YF';

export const appcode = '452003016A';

export const node_code = '4520028015';

export const dataSource = 'aum.deploy.in.main';

export const pkField = 'pk_redeploy_in';

export const cardRouter = '/card';

export const listRouter = '/list';

export const card_commit_url = '/nccloud/aum/deployin/commit.do';

export const card_print_url = '/nccloud/aum/deployin/printCard.do';

export const list_url = '/aum/deploy/in/list/index.html';

export const deleteUrl = '/nccloud/aum/deploy/delete.do';

export const editUrl = '/nccloud/aum/deployin/in_edit.do';

export const batchalter_url = '/nccloud/aum/deployin/batchalter.do';

export const queryUrl = '/nccloud/aum/deployin/querycard.do';

export const updateUrl = '/nccloud/aum/deployin/update.do';

export const fileName = '452003016A-000006';

export const billtype = '4A34';

// 财务数据字段
export const fiMoneyFields = [
	'origin_value',
	'accu_dep',
	'new_value',
	'net_money',
	'pre_devaluate',
	'dep_amount',
	'salvage'
];
