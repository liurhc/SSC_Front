//主子表卡片
import React, { Component } from 'react';
import { createPage, base, high } from 'nc-lightapp-front';
import { buttonClick, initTemplate, afterEvent, pageInfoClick } from './events';
import { delConfirm, modelSave, cancel, getDataByPk, linkToList, commitClick } from './events/buttonClick';
import bodyBeforeEvent from '../../in/card/events/bodyBeforeEvent';
import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { multiLangUtils, cardUtils, closeBrowserUtils, msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { createCardTitleArea, createCardPaginationArea, getCommonTableHeadLeft, setHeadAreaData } = cardUtils;
const { ApprovalTrans, LoginContext } = components;
const { getContext, loginContextKeys } = LoginContext;
const { CommonKeys, StatusUtils } = commonConst;
const { TransferConst } = CommonKeys;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { UISTATE } = StatusUtils;

const { NCAffix, NCStep, NCBackBtn } = base;
import { formId, tableId, pagecode, dataSource, pkField, billtype } from './const';

class Card extends Component {
	constructor(props) {
		super(props);
		this.state = {
			bill_type: billtype,
			transi_type: '',
			bill_code: '',
			step: 0,
			show: false,
			showApprove: false,
			pk_redeploy_in: '',
			compositedisplay: false,
			compositedata: {}
		};

		closeBrowserUtils.call(this, props, { form: [ formId ], cardTable: [ tableId ] });
		initTemplate.call(this, props);
		this.nowUIStatus = this.props.getUrlParam('status');
	}
	componentDidMount() {}

	//指派方法
	getAssginUsedr = (content) => {
		commitClick.call(this, this.props, 'SAVE', 'commit', content);
	};
	//取消 指派
	turnOff = () => {
		this.setState({
			compositedisplay: false
		});
	};

	//返回列表界面
	backToList = () => {
		linkToList.call(this, this.props);
	};

	getButtonNames = (codeId) => {
		if (codeId === 'edit' || codeId === 'add' || codeId === 'delete') {
			return 'main-button';
		} else {
			return 'secondary-button';
		}
	};

	//获取列表肩部信息
	getTableHeadLeft = () => {
		return getCommonTableHeadLeft.call(this, this.props, { tableId: tableId });
		// let { cardTable } = this.props;
		// let status = cardTable.getStatus(tableId) || UISTATE.browse;
		// let rowCount = 0;
		// let allRows = cardTable.getAllRows(tableId);
		// if (allRows) {
		// 	rowCount = allRows.length;
		// }
		// return (
		// 	<div className="shoulder-definition-area">
		// 		<div className="definition-search">
		// 			{status == UISTATE.browse && (
		// 				<div>
		// 					<span className="definition-search-title">总计 :</span>
		// 					<span className="count">{rowCount}</span>
		// 					<span className="definition-search-title">行</span>
		// 				</div>
		// 			)}
		// 		</div>
		// 	</div>
		// );
	};
	//获取列表肩部信息
	getTableHead = () => {
		let { button } = this.props;
		let { createButtonApp } = button;
		return (
			<div className="shoulder-definition-area">
				<div className="definition-icons">
					{/* 表体页肩按钮 */}
					{createButtonApp({
						area: 'card_body',
						buttonLimit: 3,
						onButtonClick: buttonClick.bind(this),
						popContainer: document.querySelector('.nc-bill-table-area')
					})}
					{/*表体页肩图标*/}
					{this.props.cardTable.createBrowseIcons(tableId, {
						iconArr: [ 'close', 'open', 'max', 'setCol' ],
						maxDestAreaId: 'nc-bill-card'
					})}
				</div>
			</div>
		);
	};
	//切换页面状态
	toggleShow = (status) => {
		if (!status) {
			status = this.props.getUrlParam('status');
		}
		let pkVal = this.props.form.getFormItemsValue(formId, pkField);
		if (pkVal) {
			pkVal = pkVal.value;
		} else {
			pkVal = '';
		}
		let data = {
			status: status,
			id: pkVal
		};
		this.props.setUrlParam(data);
		let editflag = false;
		let areaStatus = 'browse';
		if (
			status == UISTATE.add ||
			status == UISTATE.edit ||
			status == UISTATE.transferAdd ||
			status == UISTATE.transferEdit ||
			status == 'equipAdd'
		) {
			areaStatus = 'edit';
			editflag = true;
		}
		//设置表单表格的可编辑性
		this.props.form.setFormStatus(formId, areaStatus);
		this.props.cardTable.setStatus(tableId, areaStatus);
		//按钮的显示状态
		// if (status == UISTATE.edit) {
		this.props.button.setButtonVisible(
			[ 'Print', 'QueryAbout', 'Edit', 'Delete', 'Commit', 'UnCommit', 'Attachment', 'Refresh' ],
			!editflag
		);
		this.props.button.setButtonVisible(
			[ 'Save', 'Cancel', 'SaveCommit', 'AddLine', 'DelLine', 'BatchAlter' ],
			editflag
		);
		// this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false);
		// } else if(status == UISTATE.browse){
		// this.props.button.setButtonVisible(
		// 	[ 'Save', 'SaveCommit', 'Cancel', 'AddLine', 'DelLine', 'BatchAlter' ],
		// 	false
		// );
		// this.props.button.setButtonVisible(
		// 	[ 'Edit', 'Commit', 'UnCommit', 'Print', 'Attachment', 'Delete', 'Refresh', 'QueryAbout' ],
		// 	true
		// );
		// this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', true);
		let bill_status = this.props.form.getFormItemsValue(formId, [ 'bill_status' ]);
		if (bill_status[0] != undefined) {
			let bill_status = this.props.form.getFormItemsValue(formId, [ 'bill_status' ])[0].value;
			if (bill_status === '0') {
				//自由态
				this.props.button.setButtonVisible([ 'UnCommit' ], false); //不显示收回
			} else if (bill_status === '1') {
				//已提交
				this.props.button.setButtonVisible([ 'Commit', 'Delete', 'Edit' ], false); //不显示提交、修改、删除
			} else if (bill_status === '2') {
				//审批中
				this.props.button.setButtonVisible([ 'Commit', 'Delete', 'Edit', 'UnCommit' ], false); //不显示提交、修改、删除、收回
			} else if (bill_status === '3') {
				//审批通过
				this.props.button.setButtonVisible([ 'Commit', 'Delete', 'Edit' ], false); //不显示提交、修改、删除
			} else if (bill_status === '4') {
				//审批未通过
				this.props.button.setButtonVisible([ 'Commit', 'UnCommit' ], false); //不显示提交、收回
			} else if (bill_status === '6') {
				//关闭
				this.props.button.setButtonVisible([ 'Commit', 'Delete', 'Edit', 'UnCommit' ], false); //不显示提交、修改、删除、收回
			}
		}
		// }
		setHeadAreaData.call(this, this.props, { status });
	};
	// 进度条
	createSteps = () => {
		return (
			<NCStep.NCSteps current={this.state.step}>
				<NCStep title={getMultiLangByID('452003008A-000007')} />
				<NCStep title={getMultiLangByID('452003008A-000008')} />
				<NCStep title={getMultiLangByID('452003008A-000009')} />
				<NCStep title={getMultiLangByID('452003008A-000010')} />
				<NCStep title={getMultiLangByID('452003008A-000011')} />
			</NCStep.NCSteps>
		);
	};
	render() {
		let { cardTable, form, button, ncmodal, cardPagination, ncUploader } = this.props;
		const { createCardPagination } = cardPagination;
		let { createNCUploader } = ncUploader;
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createButtonApp } = button;
		let { createModal } = ncmodal;
		let status = this.props.getUrlParam('status');
		const { BillTrack, ApproveDetail } = high;
		return (
			<div className="nc-bill-card">
				<div className="nc-bill-top-area">
					<NCAffix>
						<div className="nc-bill-header-area">
							{/* 标题 title */}
							{createCardTitleArea.call(this, this.props, {
								title: getMultiLangByID('452003016A-000006'),
								formId,
								backBtnClick: this.backToList
							})}
							{/* <div className="header-title-search-area">
								{status == 'browse' ? (
									<NCBackBtn style={{ margin: 9 }} onClick={this.backToList} />) : (''
									)}
								<h2 className="title-search-detail">
									所有权调入{status == 'browse' ? `：${this.state.bill_code}` : ''}
								</h2>
							</div> */}
							<div className="header-button-area">
								{createButtonApp({
									area: 'card_head',
									buttonLimit: 3,
									onButtonClick: buttonClick.bind(this),
									popContainer: document.querySelector('.nc-bill-table-area')
								})}
							</div>
							{createCardPaginationArea.call(this, this.props, {
								formId,
								dataSource,
								pageInfoClick
							})}
							{/* {status == 'browse' ? (
								<div className="header-cardPagination-area">
									{createCardPagination({ handlePageInfoChange: pageInfoClick.bind(this) })}
								</div>
							) : (
									''
								)} */}
						</div>
					</NCAffix>
					{status == 'browse' ? (
						<div
							className="nc-bill-form-area browse-steps-area"
							style={{ paddingTop: 20, paddingRight: 40, paddingBottom: 0, paddingLeft: 40 }}
						>
							{' '}
							{this.createSteps()}
						</div>
					) : (
						''
					)}
					<div className="nc-bill-form-area">
						{createForm(formId, {
							onAfterEvent: afterEvent.bind(this),
							pkname: pkField,
							dataSource: dataSource
						})}
					</div>
				</div>
				<div className="nc-bill-bottom-area">
					<div className="nc-bill-table-area">
						{/* {this.getTableHead()} */}
						{createCardTable(tableId, {
							tableHeadLeft: this.getTableHeadLeft.bind(this),
							tableHead: this.getTableHead.bind(this),
							showIndex: true,
							hideAdd: true,
							hideDel: true,
							modelSave: modelSave.bind(this),
							onBeforeEvent: bodyBeforeEvent.bind(this),
							onAfterEvent: afterEvent.bind(this),
							pkname: pkField,
							dataSource: dataSource
						})}
					</div>
				</div>
				{/*单据追溯*/}
				<BillTrack
					show={this.state.show}
					close={() => {
						this.setState({ show: false });
					}}
					pk={this.state.pk_redeploy_in} //单据id
					type={this.state.bill_type} //单据类型
				/>
				{/* 审批详情 */}
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transi_type}
					billid={this.state.pk_redeploy_in}
				/>
				{createNCUploader('warcontract-uploader', {
					billid: this.state.pk_redeploy_out,
					billtype: this.state.code
				})}
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002')}
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				) : (
					''
				)}
			</div>
		);
	}
}

Card = createPage({
	billinfo: {
		billtype: 'card',
		pagecode: pagecode,
		headcode: formId,
		bodycode: tableId
	}
})(Card);

export default Card;
// ReactDOM.render(<Card />, document.querySelector('#app'));
