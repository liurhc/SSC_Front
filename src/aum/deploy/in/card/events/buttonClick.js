import { ajax, toast, print, cardCache, output } from 'nc-lightapp-front';

import ampub from 'ampub';
const { utils, components, commonConst } = ampub;

const { ScriptReturnUtils, queryAboutUtils, saveValidatorsUtil } = components;
const { getScriptCardReturnData } = ScriptReturnUtils;
const { openBillTrack } = queryAboutUtils;
const { beforeSaveValidator } = saveValidatorsUtil;
const { multiLangUtils, cardUtils, msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { setCardValue, setHeadAreaData } = cardUtils;
const { StatusUtils } = commonConst;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { UISTATE } = StatusUtils;

import {
	formId,
	tableId,
	pagecode,
	card_commit_url,
	fileName,
	card_print_url,
	editUrl,
	batchalter_url,
	node_code,
	deleteUrl,
	pkField,
	dataSource,
	updateUrl,
	listRouter,
	queryUrl,
	list_pagecode,
	fiMoneyFields
} from '../const';

export default function(props, id) {
	switch (id) {
		case 'Edit':
			edit.call(this, props);
			break;
		case 'Commit':
			commitClick.call(this, props, 'SAVE', 'commit');
			break;
		case 'UnCommit':
			commitClick.call(this, props, 'UNSAVE', '');
			break;
		case 'SaveCommit':
			commitClick.call(this, props, 'SAVE', 'saveCommit');
			break;
		case 'Delete':
			deleteConfirm.call(this, props);
			break;
		case 'Save':
			saveClick.call(this, props);
			break;
		case 'QueryAboutBusiness':
			let pk_redeploy_in = props.form.getFormItemsValue(formId, pkField);
			if (pk_redeploy_in && pk_redeploy_in.value) this.setState({ pk_redeploy_in: pk_redeploy_in.value });
			openBillTrack(this);
			break;
		case 'QueryAboutBillFlow':
			queryaboutbillflow.call(this, props);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'OutPut':
			outputTemp.call(this, props);
			break;
		case 'Cancel':
			cancelConfirm.call(this, props);
			break;
		case 'AddLine':
			props.cardTable.addRow(tableId);
			break;
		case 'Refresh':
			refresh.call(this, props);
			break;
		case 'Attachment':
			AttachmentClick.call(this, props);
			break;
		case 'BatchAlter':
			BatchAlter.call(this, props);
			break;
		default:
			break;
	}
}

/**
 * 修改
 */

export function edit(props) {
	let pk = this.props.form.getFormItemsValue(formId, pkField).value;
	if (!pk) {
		// toast({ color: 'warning', content: '该单据已被删除' });
		let config = {
			content: getMultiLangByID('452003016A-000002'),
			color: 'warning'
		};
		showMessage.call(this, props, config);
		/* 国际化处理： 当前环境并不支持SVG*/
		return;
	}
	ajax({
		url: editUrl,
		data: {
			pk: pk,
			resourceCode: node_code
		},
		success: (res) => {
			if (res.data) {
				toast({ color: 'warning', content: res.data });
			} else {
				props.cardTable.closeExpandedRow(tableId);
				this.toggleShow(UISTATE.edit);
				//去掉进度条
				this.setState({ step: -1 });
			}
		}
	});
}

/**
 * 保存按钮
 */
export function saveClick(props) {
	// 保存前校验
	let pass = beforeSaveValidator.call(this, props, formId, tableId, []);
	if (!pass) {
		return;
	}
	let pk_ownerorg_out_v = props.form.getFormItemsValue(formId, 'pk_ownerorg_out_v').value; //调出管理组织
	let pk_ownerorg_in_v = props.form.getFormItemsValue(formId, 'pk_ownerorg_in_v').value; //调入管理组织
	if (pk_ownerorg_out_v === pk_ownerorg_in_v) {
		let config = {
			content: getMultiLangByID('452003016A-000000'),
			color: 'warning'
		};
		showMessage.call(this, props, config);
		// toast({ content: getMultiLangByID("452003016A-000000"), color: 'warning' });
		/* 国际化处理： 当前环境并不支持SVG*/
		return;
	}
	// let CardData = props.createMasterChildData(pagecode, formId, tableId);
	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	props.validateToSave(CardData, () => {
		ajax({
			url: updateUrl, //修改保存
			data: CardData,
			success: (res) => {
				if (res.success) {
					setCardValue.call(this, props, res.data, formId, tableId);
					// 保存成功后处理缓存
					let CardData = props.createMasterChildData(pagecode, formId, tableId);
					if (status == UISTATE.add) {
						cardCache.addCache(pk, CardData, formId, dataSource);
					} else {
						cardCache.updateCache(pkField, pk, CardData, formId, dataSource);
					}
					this.toggleShow(UISTATE.browse);
					let step = res.data.head[formId].rows[0].values.step.value;
					let pk_redeploy_in = res.data.head[formId].rows[0].values[pkField].value;
					this.setState({ step, pk_redeploy_in });
					let pk = props.form.getFormItemsValue(formId, pkField).value;
					// toast({ content: '保存成功', color: 'success' });
					let config = {
						type: 'SaveSuccess'
					};
					showMessage.call(this, props, config);
					props.cardTable.closeModel(tableId);
				}
			}
		});
	});
}

/**
 * 删除按钮
 */
export function delConfirm() {
	ajax({
		url: deleteUrl,
		data: {
			id: this.props.getUrlParam('id'),
			ts: this.props.form.getFormItemsValue(formId, 'ts').value
		},
		success: (res) => {
			let { success, data } = res;
			if (success) {
				let callback = (newpk) => {
					loadDataByPk.call(this, this.props, newpk);
				};
				getScriptCardReturnData.call(
					this,
					res,
					this.props,
					formId,
					tableId,
					pkField,
					dataSource,
					null,
					true,
					callback,
					pagecode
				);
			}
		}
	});
}

/**
 * 通过单据id获取缓存单据信息，没有缓存则重新查询
 * @param {*} props 
 * @param {*} pk 
 */
export function loadDataByPk(props, pk) {
	let cachData = cardCache.getCacheById(pk, dataSource);
	if (cachData) {
		setValue.call(this, props, cachData);
	} else {
		getDataByPk.call(this, props, pk);
	}
}

/**
 * 设置界面值
 * @param {*} props 
 * @param {*} data 
 */
export function setValue(props, data) {
	if (!data) {
		return;
	}
	setCardValue.call(this, props, data, formId, tableId);
	this.toggleShow(UISTATE.browse);
	let step = data.head[formId].rows[0].values.step.value;
	this.setState({ step });
}

/**
 * 整单保存
 * 
 */
export function modelSave(props) {
	saveClick.call(this, props);
}

//提交
export function commitClick(props, OperatorType, commitType, content) {
	if (commitType === 'saveCommit' || commitType === 'commit') {
		this.props.cardTable.filterEmptyRows(tableId);
		// 保存前校验
		let pass = beforeSaveValidator.call(this, props, formId, tableId, []);
		if (!pass) {
			return;
		}
	}
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let paramInfoMap = {};
	paramInfoMap[pk] = ''; //卡片页面ts为空也可以 CardData 里有ts
	let obj = {
		dataType: 'cardData',
		OperatorType: OperatorType,
		commitType: commitType,
		pageid: pagecode,
		paramInfoMap: paramInfoMap,
		content: content
	};
	CardData.userjson = JSON.stringify(obj);
	ajax({
		url: card_commit_url,
		data: CardData,
		success: (res) => {
			if (content) {
				this.setState({
					compositedisplay: false
				});
			}

			let callback = () => {
				this.toggleShow(UISTATE.browse);
				let step = res.data.cardVos[0].head[formId].rows[0].values.step.value;
				let pk_redeploy_in = res.data.cardVos[0].head[formId].rows[0].values[pkField].value;
				this.setState({ step, pk_redeploy_in });
			};
			getScriptCardReturnData.call(
				this,
				res,
				props,
				formId,
				tableId,
				pkField,
				dataSource,
				null,
				false,
				callback,
				pagecode
			);
		},
		error: (res) => {
			if (res && res.message) {
				if (content) {
					this.setState({
						compositedisplay: false
					});
				}
				toast({ content: res.message, color: 'danger' });
			}
		}
	});
}

/**
* 打印
* @param {*} props 
*/
export function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	// let { pageConfig } = props;
	if (!printData) {
		// toast({ content: '请选择需要打印的数据', color: 'warning' });
		let config = {
			type: 'ChoosePrint'
		};
		showMessage.call(this, props, config);
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		card_print_url, // 后台打印服务url
		printData
	);
}

/**
* 输出
* @param {*} props 
*/
export function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		// toast({ content: '请选择需要输出的数据', color: 'warning' });
		let config = {
			type: 'ChooseOutput'
		};
		showMessage.call(this, props, config);
		return;
	}
	// this.setState({ printData }, () => {
	//     this.refs.printOutput.open();
	// });
	output({
		url: card_print_url,
		data: printData
	});
}

/* 获取打印数据
* @param {*} props 
* @param {*} outputType 
*/
function getPrintData(props, outputType = 'print') {
	let pk_redeploy_in = props.form.getFormItemsValue(formId, 'pk_redeploy_in');
	if (!pk_redeploy_in || !pk_redeploy_in.value) {
		return false;
	}
	let pks = [ pk_redeploy_in.value ];
	let printData = {
		filename: getMultiLangByID(fileName), // 文件名称
		// appcode: appcode, // 功能节点编码，即模板编码
		nodekey: null, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

/**
 * 取消
 */
export function cancel() {
	// 加载编辑前数据，如果编辑前没有数据，则加载当前列表最后一条数据，如果还没有，显示为空不处理
	let data = {
		status: UISTATE.browse
	};
	this.props.setUrlParam(data);
	this.toggleShow(UISTATE.browse);
	let pk = this.props.form.getFormItemsValue(formId, pkField).value;
	if (!pk) {
		pk = cardCache.getCurrentLastId(dataSource);
	}
	if (pk) {
		loadDataByPk.call(this, this.props, pk);
	}
}

/**
* 刷新
* @param {*} props 
*/
export function refresh(props) {
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let callback = (data) => {
		if (data) {
			// toast({ content: '刷新成功', color: 'success' });
			let config = {
				type: 'RefreshSuccess'
			};
			showMessage.call(this, props, config);
		} else {
			setNoDataButtonvisiable.call(this);
		}
	};
	// 查询到最新单据数据
	getDataByPk.call(this, props, pk, callback);
}

/**
 * 通过单据id查询单据信息
 * @param {*} props 
 * @param {*} pk 
 */
export function getDataByPk(props, pk, callback) {
	let pkvalue = pk;
	ajax({
		url: queryUrl,
		data: {
			pagecode: pagecode,
			pk: pk
		},
		success: (res) => {
			if (res.data) {
				setCardValue.call(this, props, res.data, formId, tableId);
				status = this.props.getUrlParam('status');
				this.toggleShow(status);
				let step = res.data.head[formId].rows[0].values.step.value;
				let pk_redeploy_in = res.data.head[formId].rows[0].values[pkField].value;
				this.setState({ step, pk_redeploy_in });
			}
			if ((!res.data || typeof res.data) == 'undefined' || res.data == null) {
				props.form.EmptyAllFormValue(formId);
				let nulldata = {
					rows: []
				};
				props.cardTable.setTableData(tableId, nulldata);
				cardCache.deleteCacheById(pkField, pkvalue, dataSource);
				this.setState({ step: 0, pk_redeploy_in: '' });
				let status = this.nowUIStatus;
				setHeadAreaData.call(this, this.props, { status, bill_code: '' });
				// toast({ content: '此数据已删除！', color: 'warning' });
				this.toggleShow(status);
				let config = {
					content: getMultiLangByID('452003016A-000003'),
					color: 'warning'
				};
				showMessage.call(this, props, config);
				/* 国际化处理： 当前环境并不支持SVG*/
			}
			let pk = props.form.getFormItemsValue(formId, pkField).value;
			cardCache.updateCache(pkField, pk, res.data, formId, dataSource);
			typeof callback == 'function' && callback(res.data);
		}
	});
}

/**
 * 審批詳情
 */

export function queryaboutbillflow(props) {
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	if (!pk) {
		// toast({ color: 'warning', content: '该单据已被修改' });
		let config = {
			content: getMultiLangByID('452003016A-000004'),
			color: 'warning'
		};
		showMessage.call(this, props, config);
		/* 国际化处理： 当前环境并不支持SVG*/
		return;
	}
	let transi_type = props.form.getFormItemsValue(formId, 'transi_type').value;
	this.setState({
		pk_redeploy_in: pk,
		showApprove: true,
		transi_type: transi_type
	});
}

/**
 * 附件
 */

export function AttachmentClick(props) {
	let billId = props.form.getFormItemsValue(formId, 'pk_redeploy_in').value;
	props.ncUploader.show('warcontract-uploader', {
		billId: 'pam/redeploy_in/' + billId
	});
}

/**
  * 批改 调入使用权，调入管理部门，调入库存组织，调入使用部门，调入责任人，调入管理人，调入位置，调入仓库
  */
export function BatchAlter(props) {
	// 获取原始数据
	let changeData = props.cardTable.getTableItemData(tableId);
	// 如果需要批改的字段需要特殊的处理，则调用后端的批改操作
	// let cardData = props.createMasterChildData(pagecode, formId, tableId);
	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);

	ajax({
		url: batchalter_url,
		data: {
			card: CardData,
			batchChangeIndex: changeData.batchChangeIndex, // 原始数据行号
			batchChangeKey: changeData.batchChangeKey, // 原始数据key
			batchChangeValue: changeData.batchChangeValue, // 原始数据value
			// 原始数据所在的vo，对应聚合VO中的tableCodes，一主一子可为空，一主多子要传对应的值
			tableCode: ''
		},
		async: false,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				//把数据设置到界面上
				if (data.body) {
					data.body[tableId].rows.forEach((row) => {
						fiMoneyFields.forEach((field) => {
							delete row.values[field];
						});
					});
					setCardValue.call(this, props, res.data, formId, tableId);
				}
			}
		}
	});
}

/**
 * 返回
 */

export function linkToList(props) {
	props.pushTo(listRouter, { pagecode: list_pagecode });
}

/**
 * 调用公共弹窗
 * @param {*} props 
 */
function deleteConfirm(props) {
	showConfirm.call(this, props, { type: MsgConst.Type.Delete, beSureBtnClick: delConfirm });
}

function cancelConfirm(props) {
	showConfirm.call(this, props, { type: MsgConst.Type.Cancel, beSureBtnClick: cancel });
}

/**
 * 设置新增取消没有数据时按钮可见
 * @param {*} props 
 */
export function setNoDataButtonvisiable() {
	let buttonArray = [
		'Edit',
		'Commit',
		'UnCommit',
		'Attachment',
		'QueryAbout',
		'Print',
		'Refresh',
		'Save',
		'SaveCommit',
		'Cancel'
	];
	this.props.button.setButtonVisible(buttonArray, false);
}
