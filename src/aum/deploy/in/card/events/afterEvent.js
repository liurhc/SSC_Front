import { ajax, toast } from 'nc-lightapp-front';
import * as CONSTANTS from '../const';

import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils, cardUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { commonHeadAfterEvent, commonBodyAfterEvent, updateHeadData, updateBodyData } = cardUtils;
const { OrgChangeEvent } = components;

const { orgChangeEvent } = OrgChangeEvent;

const { formId, tableId, pagecode } = CONSTANTS;
const bodyurl = '/nccloud/aum/deployin/bodyAfterEditAction.do';

export default function afterEvent(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	if (moduleId == formId) {
		headAfterEvent.call(this, props, moduleId, key, value, changedRows, index, record, type, eventType);
	} else if (moduleId == tableId) {
		bodyAfterEvent.call(this, props, moduleId, key, value, changedRows, index, record, type, eventType);
	}
}

/**
 * 表头编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedRows 
 * @param {*} index 
 * @param {*} record 
 * @param {*} type 
 * @param {*} eventType 
 */
function headAfterEvent(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	switch (key) {
		case 'pk_ownerorg_in_v':
			if (value) {
				let pk_ownerorg_out_v = props.form.getFormItemsValue(formId, 'pk_ownerorg_out_v').value; //调出管理组织
				if (value.value != changedRows.value) {
					props.form.setFormItemsValue(formId, { pk_ownerunit_in_v: { value: null, display: null } });
					props.form.setFormItemsValue(formId, { pk_ownerunit_in: { value: null, display: null } });
					props.cardTable.setColValue(tableId, 'pk_mandept_after', { display: null, value: null });
					props.cardTable.setColValue(tableId, 'pk_mandept_after_v', { display: null, value: null });
					props.cardTable.setColValue(tableId, 'pk_manager_after', { display: null, value: null });
				}
				// if (value.value === pk_ownerorg_out_v) {
				// 	// toast({ content: '调入货主管理组织不能与调出货主管理组织相同', color: 'warning' });
				// 	let config = {
				// 		content: getMultiLangByID("452003016A-000000"), color: 'warning'
				// 	}
				// 	showMessage.call(this, props, config)

				// }
			}
			break;
		case 'pk_usedorg_in_v':
			if (value) {
				if (value.value != changedRows.value) {
					props.cardTable.setColValue(tableId, 'pk_user_after', { display: null, value: null });
					props.cardTable.setColValue(tableId, 'pk_usedept_after_v', { display: null, value: null });
					props.cardTable.setColValue(tableId, 'pk_usedept_after', { display: null, value: null });
					props.cardTable.setColValue(tableId, 'pk_usedunit_in_v', { display: null, value: null });
					props.cardTable.setColValue(tableId, 'pk_usedunit_in', { display: null, value: null });
				}
			}
			break;
	}
}

/**
 * 表体编辑后事件
 * @param {} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedRows 
 * @param {*} index 
 * @param {*} record 
 * @param {*} type 
 * @param {*} eventType 
 */
function bodyAfterEvent(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	switch (key) {
		case 'pk_usedunit_in_v':
			if (value) {
				//调入使用权变化清空使用部门，使用人
				if (changedRows && changedRows[0].oldvalue.value != changedRows[0].newvalue.value) {
					this.props.cardTable.setValByKeyAndIndex(tableId, index, 'pk_usedept_after_v', {
						value: null,
						display: null
					});
					this.props.cardTable.setValByKeyAndIndex(tableId, index, 'pk_user_after', {
						value: null,
						display: null
					});
					// this.props.cardTable.setValByKeyAndIndex(tableId, index, 'pk_manager_after', {value:null,display:null});
				}
			} else {
				this.props.cardTable.setValByKeyAndIndex(tableId, index, 'pk_usedept_after_v', {
					value: null,
					display: null
				});
				this.props.cardTable.setValByKeyAndIndex(tableId, index, 'pk_user_after', {
					value: null,
					display: null
				});
			}
			break;
		case 'pk_user_after':
			//调入使用人带出使用部门
			if (value) {
				// let data = props.createBodyAfterEventData(pagecode, formId, tableId, moduleId, key, changedRows, index);
				// ajax({
				// 	url: '/nccloud/aum/deployin/bodyAfterEditAction.do',
				// 	data,
				// 	success: (res) => {
				// 		if (res.data && res.data.body) {
				// 			let pk_usedept_after = res.data.body[tableId].rows[index].values['pk_usedept_after']
				// 			let pk_usedept_after_v = res.data.body[tableId].rows[index].values['pk_usedept_after_v']
				// 			this.props.cardTable.setValByKeyAndIndex(tableId, index, 'pk_usedept_after', { value: pk_usedept_after.value, display: pk_usedept_after.display });
				// 			this.props.cardTable.setValByKeyAndIndex(tableId, index, 'pk_usedept_after_v', { value: pk_usedept_after_v.value, display: pk_usedept_after_v.display });
				// 		}
				// 	}
				// });
				let config = {
					afterEditUrl: bodyurl,
					pagecode,
					moduleId,
					tableId,
					key,
					record,
					changedRows,
					index
				};
				commonBodyAfterEvent.call(this, props, config);
			}
			break;
		case 'pk_icorg_in_v':
			if (value) {
				//调入库存组织清空调入仓库
				if (changedRows && changedRows[0].oldvalue.value != changedRows[0].newvalue.value) {
					this.props.cardTable.setValByKeyAndIndex(tableId, index, 'pk_warehouse_in', {
						value: null,
						display: null
					});
				}
			} else {
				this.props.cardTable.setValByKeyAndIndex(tableId, index, 'pk_warehouse_in', {
					value: null,
					display: null
				});
			}
			break;
		case 'pk_manager_after':
			if (value) {
				//调入管理人带出管理部门
				// let data = props.createBodyAfterEventData(pagecode, formId, tableId, moduleId, key, changedRows, index);
				// ajax({
				// 	url: '/nccloud/aum/deployin/bodyAfterEditAction.do',
				// 	data,
				// 	success: (res) => {
				// 		if (res.data && res.data.body) {
				// 			let pk_mandept_after = res.data.body[tableId].rows[index].values['pk_mandept_after']
				// 			let pk_mandept_after_v = res.data.body[tableId].rows[index].values['pk_mandept_after_v']
				// 			this.props.cardTable.setValByKeyAndIndex(tableId, index, 'pk_mandept_after', { value: pk_mandept_after.value, display: pk_mandept_after.display });
				// 			this.props.cardTable.setValByKeyAndIndex(tableId, index, 'pk_mandept_after_v', { value: pk_mandept_after_v.value, display: pk_mandept_after_v.display });
				// 		}
				// 	}
				// });
				let config = {
					afterEditUrl: bodyurl,
					pagecode,
					moduleId,
					tableId,
					key,
					record,
					changedRows,
					index
				};
				commonBodyAfterEvent.call(this, props, config);
			}
			break;
	}
}
