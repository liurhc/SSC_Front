import { searchId } from '../const';
import ampub from 'ampub';
const { components } = ampub;
const { assetOrgMultiRefFilter } = components;
const { isMultiCorpRefHandler } = assetOrgMultiRefFilter;

/**
 * 编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 编辑字段
 * @param {*} value 编辑字段的值
 * @param {*} changedRows 旧值
 * @param {*} index 
 * @param {*} record 
 * @param {*} type 
 * @param {*} eventType 
 */
export default function afterEvent(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	if (moduleId == searchId) {
		searchAfterEvent.call(this, props, moduleId, key, value, changedRows, index, record, type, eventType);
	}
}

/**
 * 查询区编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedRows 
 * @param {*} index 
 * @param {*} record 
 * @param {*} type 
 * @param {*} eventType 
 */
function searchAfterEvent(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	switch (key) {
		case 'pk_ownerorg_out':
			isMultiCorpRefHandler.call(this, props, value, searchId, [ 'pk_ownerunit_out' ]);
			break;
		case 'pk_ownerorg_in':
			isMultiCorpRefHandler.call(this, props, value, searchId, [ 'pk_ownerunit_in' ]);
			break;
	}
}
