//查询区编码
export const searchId = 'searchArea';

//列表表体区编码
export const tableId = 'list_head';

//卡片态表头区域
export const formId = 'card_head'

//列表页面编码
export const pagecode = '452003016A_list';


//所有权调入 应用编码
export const appid = '0001Z9100000000014YF';

//查询模板id
export const oid = '1001Z91000000000UR9K';

//卡片地址
export const card_url = '/aum/deploy/in/card/index.html';

//提交地址
export const card_commit_url = '/nccloud/aum/deployin/commit.do';

//卡片的pagecode
export const card_pagecode = '452003016A_card';

export const appcode = '452003016A';

//主键
export const pkField = 'pk_redeploy_in';

export const dataSource = 'aum.deploy.in.main';

export const cardRouter = '/card';

//打印URL地址
export const list_print_url = '/nccloud/aum/deployin/printList.do';

export const card_print_url = '/nccloud/aum/deployin/printCard.do';

export const editUrl = '/nccloud/aum/deployin/in_edit.do';

export const queryUrl = '/nccloud/aum/deployin/query.do';

export const queryPageUrl = '/nccloud/aum/deployin/querypagegridbypks.do';

export const deleteUrl = '/nccloud/aum/deployin/delete.do';

export const  billtype = '4A34';

export const  transitype = '4A34-01';

export const  node_code = '4520028015'

export const fileName = '452003016A-000006'


