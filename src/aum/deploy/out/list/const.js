//查询区编码
export const searchId = 'searchArea';

//列表表体区编码
export const tableId = 'list_head';

//列表页面编码
export const pagecode = '452003008A_list';

//所有权调出 应用编码
export const appcode = '452003008A';

//查询模板id
export const oid = '1001Z91000000000UR9B';

export const node_code = '4520028005';

export const fileName = "452003008A-000005";

export const printFuncode = '4520028005';
export const printNodekey = null;
export const bill_type = '4A33';

export const card_pagecode = '452003008A_card';
export const formId = 'card_head';
export const dataSource = 'aum.deploy.out.main';
export const transferDataSource = 'aum.deploy.deploytransfer.source';
export const pkField = 'pk_redeploy_out';
export const cardRouter = '/card';
export const transferRouter = '/transfer';

export const reqUrl = {
    queryUrl:'/nccloud/aum/deploy/query.do',
    queryPageUrl:'/nccloud/aum/deploy/querypagegridbypks.do',
    editUrl:'/nccloud/aum/deploy/out_edit.do',
    printListUrl:'/nccloud/aum/deploy/out_printlist.do',
    printCardUrl:'/nccloud/aum/deploy/out_printcard.do',
    deleteUrl:'/nccloud/aum/deploy/delete.do'
}