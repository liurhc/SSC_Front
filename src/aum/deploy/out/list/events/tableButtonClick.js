import { ajax, toast } from 'nc-lightapp-front';
import { tableId, node_code, card_pagecode, formId, reqUrl, pkField, dataSource } from '../const';
import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { ScriptReturnUtils, queryAboutUtils } = components;
const { getScriptListReturn } = ScriptReturnUtils;
const { openListApprove } = queryAboutUtils;
const { StatusUtils } = commonConst;
const { UISTATE } = StatusUtils;
const { msgUtils } = utils;
const { MsgConst, showMessage } = msgUtils;
import { linkToCard } from './buttonClick';

export default function tableButtonClick(props, key, text, record, index) {
	switch (key) {
		case 'Edit':
			edit.call(this, props, record);
			break;
		case 'Delete':
			delRow(props, tableId, index, record);
			break;
		case 'Commit':
			commitClick.call(this, 'SAVE', props, tableId, index, record);
			break;
		case 'UnCommit':
			commitClick.call(this, 'UNSAVE', props, tableId, index, record);
			break;
		case 'QueryAboutBillFlow':
			let pk = record.pk_redeploy_out.value;
			this.setState({
				pk_redeploy_out: pk,
				showApprove: true,
				transi_type: record.transi_type.value
			});
			break;
		default:
			break;
	}
}

/**
 * 修改
 */

function edit(props, record) {
	ajax({
		url: reqUrl.editUrl,
		data: {
			pk: record.pk_redeploy_out.value,
			resourceCode: node_code
		},
		success: (res) => {
			if (res.data) {
				toast({ color: 'warning', content: res.data });
			} else {
				linkToCard.call(this, props, record, UISTATE.edit);
			}
		}
	});
}

export function commitClick(OperatorType, props, tableId, index, record) {
	let paramInfoMap = {};
	paramInfoMap[record.pk_redeploy_out.value] = record.ts.value;
	let params = [ { id: record.pk_redeploy_out.value, index: index } ];
	ajax({
		url: '/nccloud/aum/deploy/commit.do',
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: OperatorType,
			pageid: card_pagecode,
			commitType: 'commit'
		},
		success: (res) => {
			getScriptListReturn.call(this, params, res, props, pkField, formId, tableId, false, dataSource);
		}
	});
}

/**
 * 直接删除数据
 * @param {*} record 
 */
const delRow = (props, tableId, index, record) => {
	let paramInfoMap = {};
	paramInfoMap[record.pk_redeploy_out.value] = record.ts.value;
	let params = [ { id: record.pk_redeploy_out.value, index: index } ];
	ajax({
		url: reqUrl.deleteUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: card_pagecode
		},
		success: (res) => {
			getScriptListReturn(params, res, props, 'pk_redeploy_out', formId, tableId, true, dataSource);
		}
	});
};
