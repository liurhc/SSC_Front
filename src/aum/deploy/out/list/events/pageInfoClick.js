import { ajax } from 'nc-lightapp-front';
import { pagecode, tableId, reqUrl } from '../const';

export default function(props, config, pks) {
	let data = {
		allpks: pks,
		pagecode: pagecode
	};
	//得到数据渲染到页面
	ajax({
		url: reqUrl.queryPageUrl,
		data: data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					props.table.setAllTableData(tableId, data[tableId]);
				} else {
					props.table.setAllTableData(tableId, { rows: [] });
				}
			}
		}
	});
}
