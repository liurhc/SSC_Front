import { tableId, pagecode, searchId, bill_type } from '../const';
import tableButtonClick from './tableButtonClick';
import { setBatchBtnsEnable, linkToCard } from './buttonClick';

import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { multiLangUtils, transferUtils } = utils;
const { refInit, LoginContext, EquipQueryCondition } = components;
const { defRefCondition } = refInit;
const { StatusUtils } = commonConst;
const { loginContext, getContext, loginContextKeys } = LoginContext;
const { getMultiLangByID } = multiLangUtils;
const { UISTATE, BILLSTATUS } = StatusUtils;
const { initCriteriaChangedHandlers, filterByOrg } = EquipQueryCondition;
const { conactAddButton } = transferUtils;

export default function(props) {
	let _this = this;
	props.createUIDom(
		{
			pagecode: pagecode //页面id
			// appcode:appcode//注册按钮的id
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}

				if (data.button) {
					let button = data.button;
					props.button.setButtons(button, () => {});
					// 行删除时悬浮框提示
					props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001'));
					let transi_type = getContext(loginContextKeys.transtype);
					conactAddButton(props, bill_type, transi_type, tableId);
				}
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta, _this);
					props.meta.setMeta(meta, () => {
						setBatchBtnsEnable.call(this, props, tableId);
					});
				}
			}
		}
	);
}

function modifierMeta(props, meta, _this) {
	meta[tableId].pagination = true;
	meta[tableId].items = meta[tableId].items.map((item, key) => {
		item.width = 150;
		if (item.attrcode == 'bill_code') {
			item.render = (text, record, index) => {
				return (
					<div class="simple-table-td" field="bill_code" fieldname={getMultiLangByID('452003008A-000006')}>
						<span
							className="code-detail-link"
							onClick={() => {
								linkToCard.call(this, props, record, UISTATE.browse);
							}}
						>
							{record.bill_code && record.bill_code.value}
						</span>
					</div>
				);
			};
		}
		return item;
	});
	//添加操作列
	let buttonCol = {
		attrcode: 'opr',
		label: getMultiLangByID('amcommon-000000'),
		className: 'table-opr',
		itemtype: 'customer',
		width: 200,
		fixed: 'right',

		visible: true,
		render: (text, record, index) => {
			let bill_status = record.bill_status.value;
			let buttonAry;
			if (bill_status === BILLSTATUS.free_check) {
				//自由态
				buttonAry = [ 'Commit', 'Edit', 'Delete' ];
			} else if (bill_status === BILLSTATUS.un_check) {
				//已提交
				buttonAry = [ 'UnCommit', 'QueryAboutBillFlow' ];
			} else if (bill_status === BILLSTATUS.check_going) {
				//审批中
				buttonAry = [ 'QueryAboutBillFlow' ];
			} else if (bill_status === BILLSTATUS.check_pass) {
				//审批通过
				buttonAry = [ 'UnCommit', 'QueryAboutBillFlow' ];
			} else if (bill_status === BILLSTATUS.check_nopass) {
				//审批未通过
				buttonAry = [ 'Edit', 'Delete', 'QueryAboutBillFlow' ];
			} else if (bill_status === BILLSTATUS.close) {
				//关闭
			} else {
			}
			return props.button.createOprationButton(buttonAry, {
				area: 'list_inner',
				buttonLimit: 3,
				// setButtonsDisabled :(props,tableId) => {setBatchBtnsEnable.call(this,props,tableId)},
				onButtonClick: (props, key) => {
					tableButtonClick.call(_this, props, key, text, record, index);
				}
			});
		}
	};
	meta[searchId].items.map((item) => {
		if (item.attrcode == 'pk_ownerorg_out') {
			item.queryCondition = () => {
				return { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter' };
			};
		} else if (item.attrcode == 'pk_ownerunit_out') {
			AssetUnitOrgMultiRefFilter.call(this, props, searchId, item, 'pk_ownerorg_out');
			item.queryCondition = () => {
				let pk_ownerorg_out = getSearchValue.call(this, props, 'pk_ownerorg_out');
				return {
					pk_ownerorg_out,
					TreeRefActionExt: 'nccloud.web.aum.deploy.out.refcondition.PK_OWNERUNIT_OUT_VSqlBuilder'
					// busifuncode: IBusiRoleConst.ASSETORG
				};
			};
		} else if (item.attrcode == 'pk_ownerunit_in') {
			AssetUnitOrgMultiRefFilter.call(this, props, searchId, item, 'pk_ownerorg_in');
			item.queryCondition = () => {
				let pk_ownerorg_in = getSearchValue.call(this, props, 'pk_ownerorg_in');
				return {
					pk_ownerorg_in,
					TreeRefActionExt: 'nccloud.web.aum.deploy.in.refcondition.PK_OWNERUNIT_IN_VSqlBuilder'
					// busifuncode: IBusiRoleConst.ASSETORG
				};
			};
		} else {
			let bodyDefPrefix = 'bodyvos.def';
			let pk_group = getContext(loginContextKeys.pk_group); //集团供自定义项参照试用
			defRefCondition.call(this, props, item, searchId, pk_group, true, 'pk_org', bodyDefPrefix);
			defRefCondition.call(this, props, item, searchId, pk_group, true);
		}
	});
	meta[tableId].items.push(buttonCol);
	initCriteriaChangedHandlers.call(this, props, meta, searchId);
	filterByOrg.call(
		this,
		props,
		meta,
		searchId,
		[ 'bodyvos.pk_equip.pk_mandept', 'bodyvos.pk_equip.pk_user', 'bodyvos.pk_equip.pk_manager' ],
		'pk_ownerorg_out',
		true
	);
	return meta;
}

//获取查询条件的值
function getSearchValue(props, field) {
	let data = props.search.getSearchValByField(searchId, field);
	if (data) {
		return data.value.firstvalue;
	}
}

/**
 *  给查询参照增加业务单元过滤
 *  初始化没有编辑组织，直接编辑需要业务单元过滤的字段，则显示所有有权限的业务单元
 * @param {*} props 
 * @param {*} searchId 查询区域id
 * @param {*} item 操作的字段
 * @param {*}key 过滤参照项的key，一般为‘pk_ownerorg’
 */
export function AssetUnitOrgMultiRefFilter(props, searchId, item, key) {
	item.isShowUnit = true;
	item.unitProps = {
		multiLang: {
			domainName: 'uapbd',
			// currentLocale: 'simpchn',
			moduleId: 'refer_uapbd'
		},
		refType: 'grid',
		refName: 'refer-000180' /*国际化处理：资产组织*/,
		placeholder: 'refer-000180' /*国际化处理：资产组织*/,
		refcode: 'uapbd/refer/org/AssetOrgGridRef/index',
		queryGridUrl: '/nccloud/uapbd/ref/AssetOrgGridRef.do',
		isMultiSelectedEnabled: false,
		columnConfig: [
			{
				name: [ 'refer-000002', 'refer-000003' ] /*国际化处理：编码，名称*/,
				code: [ 'code', 'name' ]
			}
		],
		isShowDisabledData: false,
		key: 'pk_ownerorg'
	};
	item.unitCondition = () => {
		let pk_org = props.search.getSearchValByField(searchId, key).value.firstvalue; // 调用相应组件的取值API
		return {
			pk_org,
			GridRefActionExt: 'nccloud.web.ampub.common.refCodition.UnitSqlBuilder'
		};
	};
}
