import { ajax, toast } from 'nc-lightapp-front';

import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils, cardUtils, msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { commonHeadAfterEvent, commonBodyAfterEvent, setCardValue, updateBodyData } = cardUtils;
const { OrgChangeEvent } = components;

const { orgChangeEvent } = OrgChangeEvent;
const { showMessage } = msgUtils;

import { formId, tableId, pagecode, TRANSFERINFO } from '../const';
const headurl = '/nccloud/aum/deploy/headafterevent.do';
const bodyurl = '/nccloud/aum/deploy/bodyafterevent.do';

/**
 * 编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 编辑字段
 * @param {*} value 编辑字段的值
 * @param {*} changedRows 旧值
 * @param {*} index 
 * @param {*} record 
 * @param {*} type 
 * @param {*} eventType 
 */
export default function afterEvent(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	if (moduleId == formId) {
		headAfterEvent.call(this, props, moduleId, key, value, changedRows, index, record, type, eventType);
	} else if (moduleId == tableId) {
		bodyAfterEvent.call(this, props, moduleId, key, value, changedRows, index, record, type, eventType);
	}
	//来源是转单的操作需要同步右边数据到左边
	if (this.srctype == TRANSFERINFO.srctype) {
		this.synTransferData();
	}
}

/**
 * 表头编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} oldValue
 * @param {*} index 
 * @param {*} record 
 * @param {*} type 
 * @param {*} eventType 
 */
function headAfterEvent(props, moduleId, key, value, oldValue, index, record, type, eventType) {
	// let data = props.createHeadAfterEventData(pagecode, formId, tableId, moduleId, key, value);
	switch (key) {
		case 'pk_ownerorg_out_v':
			if (value) {
				let callback = () => {
					// data = props.createHeadAfterEventData(pagecode, formId, tableId, moduleId, key, value);
					// ajax({
					// 	url: '/nccloud/aum/deploy/headafterevent.do',
					// 	data,
					// 	success: (res) => {
					// 		if (res.data && res.data.head) {
					// 			setCardValue.call(this, props, res.data, formId, tableId);

					if (value.value) {
						this.props.cardTable.addRow(tableId, undefined, undefined, false);
					}
					let config = {
						afterEditUrl: headurl,
						pagecode,
						key,
						value,
						oldValue
					};
					commonHeadAfterEvent.call(this, props, config);
					// }
					// }
					// });
				};

				let fields = [];
				let tableButtons = [ 'AddLine', 'BatchAlter' ];
				orgChangeEvent.call(
					this,
					props,
					pagecode,
					formId,
					tableId,
					key,
					value,
					oldValue,
					fields,
					tableButtons,
					callback
				);
			}

			break;

		case 'pk_ownerorg_in_v':
			if (value) {
				let config = {
					afterEditUrl: headurl,
					pagecode,
					key,
					value,
					oldValue
				};
				commonHeadAfterEvent.call(this, props, config);

				// let data = props.createHeadAfterEventData(pagecode, formId, tableId, moduleId, key, value);
				// ajax({
				// 	url: '/nccloud/aum/deploy/headafterevent.do',
				// 	data,
				// 	success: (res) => {
				// 		if (res.data && res.data.body) {
				// 			setCardValue.call(this, props, res.data, formId, tableId);
				// 		}
				// 	}
				// });
			}
			break;
		case 'pk_ownerunit_out_v':
			if (value) {
				let pk_ownerunit_out_v = value.value;
				let pk_ownerunit_in_v = props.form.getFormItemsValue(formId, 'pk_ownerunit_in_v').value;
				let pk_ownerorg_in_v = props.form.getFormItemsValue(formId, 'pk_ownerorg_in_v').value;
				let pk_ownerorg_out_v = props.form.getFormItemsValue(formId, 'pk_ownerorg_out_v').value;
				if (pk_ownerorg_in_v == pk_ownerorg_out_v && pk_ownerunit_in_v == pk_ownerunit_out_v) {
					// toast({ content: '调出货主管理组织+调出货主不能与调入货主管理组织+调入货主相同', color: 'warning' });
					let config = {
						content: getMultiLangByID('452003008A-000000'),
						color: 'warning'
					};
					showMessage.call(this, props, config);
					/* 国际化处理： 当前环境并不支持SVG*/
				}
				let rows = props.cardTable.getVisibleRows(tableId);
				let indexs = [];
				rows.map((row, index) => {
					indexs.push(index);
				});
				props.cardTable.setEditableByIndex(tableId, indexs, 'pk_equip', true);
			}
			break;
		case 'pk_ownerunit_in_v':
			//编辑后涉及表体
			if (value) {
				let data = props.createHeadAfterEventData(pagecode, formId, tableId, moduleId, key, value);
				ajax({
					url: '/nccloud/aum/deploy/headafterevent.do',
					data,
					success: (res) => {
						if (res.data && res.data.body) {
							// props.cardTable.setTableData(tableId, res.data.body[tableId]);
							setCardValue.call(this, props, res.data, formId, tableId);
							let pk_ownerunit_in_v = value.value;
							let pk_ownerunit_out_v = props.form.getFormItemsValue(formId, 'pk_ownerunit_out_v').value;
							let pk_ownerorg_in_v = props.form.getFormItemsValue(formId, 'pk_ownerorg_in_v').value;
							let pk_ownerorg_out_v = props.form.getFormItemsValue(formId, 'pk_ownerorg_out_v').value;
							if (pk_ownerorg_in_v == pk_ownerorg_out_v && pk_ownerunit_in_v == pk_ownerunit_out_v) {
								// toast({ content: '调出货主管理组织+调出货主不能与调入货主管理组织+调入货主相同', color: 'warning' });
								let config = {
									content: getMultiLangByID('452003008A-000000'),
									color: 'warning'
								};
								showMessage.call(this, props, config);
								/* 国际化处理： 当前环境并不支持SVG*/
							}
						}
					}
				});
			}
			break;
		case 'pk_recorder':
			// let data = props.createHeadAfterEventData(pagecode, formId, tableId, moduleId, key, value);
			// ajax({
			// 	url: '/nccloud/aum/deploy/headafterevent.do',
			// 	data,
			// 	success: (res) => {
			// 		if (res.data && res.data.head) {
			// 			// props.form.setAllFormValue({ [formId]: res.data.head[formId] });
			// 			setCardValue.call(this, props, res.data, formId, tableId);
			// 		}
			// 	}
			// });
			let config = {
				afterEditUrl: headurl,
				pagecode,
				key,
				value,
				oldValue
			};
			commonHeadAfterEvent.call(this, props, config);
			break;
		default:
			break;
	}
}

/**
 * 表体编辑后事件
 * @param {} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedRows 
 * @param {*} index 
 * @param {*} record 
 * @param {*} type 
 * @param {*} eventType 
 */
function bodyAfterEvent(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	switch (key) {
		case 'pk_equip':
			if ((value instanceof Array && value.length > 0) || value.refcode) {
				// let data = props.createBodyAfterEventData(pagecode, formId, tableId, moduleId, key, changedRows, index);
				// ajax({
				// 	url: '/nccloud/aum/deploy/bodyafterevent.do',
				// 	data,
				// 	success: (res) => {
				// 		if (res.data && res.data.body) {
				// 			props.cardTable.setTableData(tableId, res.data.body[tableId]);
				// 		}
				// 	}
				// });
				let config = {
					afterEditUrl: bodyurl,
					pagecode,
					moduleId,
					tableId,
					key,
					record,
					changedRows,
					index
				};
				commonBodyAfterEvent.call(this, props, config);
			} else {
				// 清空联动项
				let meta = props.meta.getMeta();
				meta[tableId].items.map((item) => {
					let clearfield = [
						'pk_category',
						'pk_usedunit_out_v',
						'pk_usedunit_out',
						'pk_mandept_before_v',
						'pk_mandept_before',
						'pk_icorg_out_v',
						'pk_icorg_out',
						'pk_faorg_out_v',
						'pk_faorg_out'
					];
					if (item.attrcode.startsWith('equip') || clearfield.indexOf(item.attrcode) != -1) {
						props.cardTable.setValByKeyAndIndex(tableId, index, item.attrcode, {
							value: null,
							display: null
						});
					}
				});
			}
			break;
		default:
			break;
	}
}
