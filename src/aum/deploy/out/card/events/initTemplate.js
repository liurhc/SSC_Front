import { toast } from 'nc-lightapp-front';

import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { multiLangUtils, cardUtils, msgUtils, transferUtils } = utils;
const { queryAboutUtils, refInit, LoginContext } = components;
const { openEquipCardByPk } = queryAboutUtils;
const { defRefCondition } = refInit;
const { afterModifyCardMeta } = cardUtils;
const { getMultiLangByID } = multiLangUtils;
const { loginContext, getContext, loginContextKeys } = LoginContext;
const { conactAddButton } = transferUtils;
const { CommonKeys, StatusUtils } = commonConst;
const { IBusiRoleConst, TransferConst, linkQueryConst } = CommonKeys;
const { UISTATE } = StatusUtils;
const { showMessage } = msgUtils;

import { formId, tableId, pagecode, TRANSFERINFO, bill_type } from '../const';
import tableButtonClick from './tableButtonClick';
import { setDefaultValue, getDataByPk } from './buttonClick';
import afterEvent from './afterEvent';

export default function(props) {
	let _this = this;
	let pageId = pagecode;
	let status = props.getUrlParam('status');

	//来源是转单的加载不同模板
	if (TRANSFERINFO.srctype == props.getUrlParam(TransferConst.srctype)) {
		pageId = TRANSFERINFO.pagecode;
		//转单下游界面的卡片区域编码必须与原卡片界面一致
		//formId = TRANSFERINFO.formcode;
		//tableId = TRANSFERINFO.tablecode
		//appId = TRANSFERINFO.appid;
	}
	props.createUIDom(
		{
			pagecode: pageId //页面id
		},
		function(data) {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button, () => {
						if (TRANSFERINFO.srctype != props.getUrlParam(TransferConst.srctype)) {
							//不是转单下游时需要处理拉单按钮
							let billtype = bill_type;
							let transtype = getContext(loginContextKeys.transtype);
							let scene = props.getUrlParam(linkQueryConst.SCENE);
							if (scene != linkQueryConst.SCENETYPE.approvesce) {
								conactAddButton(props, billtype, transtype, formId);
							}
							// if (status == UISTATE.edit || status == UISTATE.add) {
							// 	props.button.setButtonVisible(ButtonConst.AddTransferGroup, false);
							// }
							// let scene = props.getUrlParam(linkQueryConst.SCENE);
							// if (scene == linkQueryConst.SCENETYPE.approvesce) {
							// 	props.button.setButtonVisible(ButtonConst.AddTransferGroup, false);
							// }
						} else {
							//设置组织不可编辑
							props.form.setFormItemsDisabled(formId, { pk_org: false, pk_org_v: false });
						}
					});
				}
				if (data.template) {
					let meta = data.template;
					modifierMeta(props, meta, _this);
					props.meta.setMeta(meta, () => {
						loadData.call(this, props, _this);
						if (status == UISTATE.add) {
							setDefaultValue.call(this, props);
							let pk_ownerorg_out_v = props.form.getFormItemsValue(formId, 'pk_ownerorg_out_v');
							if (pk_ownerorg_out_v.value) {
								props.cardTable.addRow(tableId, undefined, undefined, false);
							} else {
								props.initMetaByPkorg('pk_ownerorg_out_v');
								props.form.setFormItemsDisabled(formId, { pk_ownerorg_out_v: false });
								props.button.setButtonDisabled([ 'AddLine', 'BatchAlter' ], true);
								// props.button.setButtonDisabled(['Save', 'SaveCommit'], true);
							}
							props.button.setButtonDisabled([ 'DelLine' ], true);
							afterEvent.call(_this, props, formId, 'pk_recorder');
						} else if (status == UISTATE.edit) {
							props.form.setFormItemsDisabled(formId, { pk_ownerorg_out_v: true });
						}
					});
				}
			}
		}
	);
}
function modifierMeta(props, meta, _this) {
	let status = props.getUrlParam('status');
	let transi_type = getContext(loginContextKeys.transtype);
	meta[formId].status = status;
	meta[tableId].status = status;
	meta[tableId].items = meta[tableId].items.map((item, key) => {
		item.width = 150;
		return item;
	});

	let porCol = {
		attrcode: 'opr',
		label: getMultiLangByID('amcommon-000000'),
		visible: true,
		className: 'table-opr',
		itemtype: 'customer',
		width: 130,
		fixed: 'right',
		render(text, record, index) {
			let status = props.cardTable.getStatus(tableId);
			let buttonAry = status === 'browse' ? [ 'OpenCard' ] : [ 'OpenCard', 'DelLine' ];

			return props.button.createOprationButton(buttonAry, {
				area: 'card_body_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => {
					tableButtonClick.call(this, props, key, text, record, index);
				}
			});
		}
	};
	let pk_group = getContext(loginContextKeys.pk_group); //集团供自定义项参照试用
	meta[tableId].items.push(porCol);
	//表头参照过滤
	meta[formId].items.map((item) => {
		//调出货主管理组织
		if (item.attrcode == 'pk_ownerorg_out_v') {
			item.queryCondition = () => {
				return { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgVidRefFilter' };
			};
		} else if (item.attrcode == 'pk_ownerunit_in_v') {
			//调入货主
			item.queryCondition = () => {
				let pk_ownerunit_in = props.form.getFormItemsValue(formId, 'pk_ownerunit_in'); // 调入货主
				let pk_ownerorg_in = props.form.getFormItemsValue(formId, 'pk_ownerorg_in'); //调入货主管理组织
				if (pk_ownerunit_in) {
					pk_ownerunit_in = props.form.getFormItemsValue(formId, 'pk_ownerunit_in').value;
				}
				if (pk_ownerorg_in) {
					pk_ownerorg_in = props.form.getFormItemsValue(formId, 'pk_ownerorg_in').value;
				}
				return {
					pk_ownerunit_in,
					pk_ownerorg_in,
					TreeRefActionExt: 'nccloud.web.aum.deploy.in.refcondition.PK_OWNERUNIT_IN_VSqlBuilder'
				};
			};
		} else if (item.attrcode == 'pk_ownerunit_out_v') {
			//调出货主
			item.queryCondition = () => {
				let pk_ownerunit_out = props.form.getFormItemsValue(formId, 'pk_ownerunit_out'); // 调入货主
				let pk_ownerorg_out = props.form.getFormItemsValue(formId, 'pk_ownerorg_out'); //调出货主管理组织
				if (pk_ownerunit_out) {
					pk_ownerunit_out = props.form.getFormItemsValue(formId, 'pk_ownerunit_out').value;
				}
				if (pk_ownerorg_out) {
					pk_ownerorg_out = props.form.getFormItemsValue(formId, 'pk_ownerorg_out').value;
				} else {
					props.form.set;
					// toast({ content: '请先录入调出货主管理组织', color: 'warning' });
					let config = {
						content: getMultiLangByID('452003008A-000002'),
						color: 'warning'
					};
					showMessage.call(this, props, config);
					/* 国际化处理： 当前环境并不支持SVG*/
				}
				return {
					transi_type,
					bill_type,
					pk_ownerorg_out,
					pk_ownerunit_out,
					TreeRefActionExt: 'nccloud.web.aum.deploy.out.refcondition.PK_OWNERUNIT_OUT_VSqlBuilder'
				};
			};
		} else if (item.attrcode == 'pk_recorder') {
			//经办人
			item.queryCondition = () => {
				let pk_ownerorg_out = props.form.getFormItemsValue(formId, 'pk_ownerorg_out');
				if (pk_ownerorg_out) {
					pk_ownerorg_out = props.form.getFormItemsValue(formId, 'pk_ownerorg_out').value;
				}
				return {
					pk_org: pk_ownerorg_out,
					busifuncode: IBusiRoleConst.ASSETORG
				};
			};
		} else {
			//表头自定义项参照处理

			defRefCondition.call(this, props, item, formId, pk_group);
		}
	});

	//表体参照过滤
	meta[tableId].items.map((item) => {
		//设备编码
		if (item.attrcode == 'pk_equip') {
			item.isMultiSelectedEnabled = true;
			item.renderStatus = UISTATE.browse;
			item.render = (text, record, index) => {
				return (
					<div class="card-table-browse">
						<span
							className="code-detail-link"
							onClick={() => {
								openEquipCardByPk.call(this, props, record.values[item.attrcode].value);
							}}
						>
							{record.values[item.attrcode] && record.values[item.attrcode].display}
						</span>
					</div>
				);
			};
			item.queryCondition = () => {
				let pk_ownerorg_out = props.form.getFormItemsValue(formId, 'pk_ownerorg_out').value; // 调出货主管理组织
				let pk_ownerunit_out = props.form.getFormItemsValue(formId, 'pk_ownerunit_out'); //调出货主
				if (pk_ownerunit_out) {
					pk_ownerunit_out = props.form.getFormItemsValue(formId, 'pk_ownerunit_out').value;
				}
				return {
					pk_org: pk_ownerorg_out, //主组织
					pk_ownerunit_out,
					transi_type: transi_type,
					bill_type: bill_type,
					GridRefActionExt: 'nccloud.web.aum.deploy.out.refcondition.PK_EQUIPSqlBuilder'
				};
			};
		} else {
			//表体自定义项参照处理

			defRefCondition.call(this, props, item, formId, pk_group);
		}
	});
	let config = { tableId };
	meta = afterModifyCardMeta.call(this, props, meta, config);
	return meta;
}

function loadData(props, _this) {
	let status = props.getUrlParam('status');
	if (status == UISTATE.add) {
	} else if (status == UISTATE.browse || status == UISTATE.edit) {
		let pk = _this.props.getUrlParam('id');
		if (pk) {
			getDataByPk.call(_this, props, pk);
		}
	} else if (status == UISTATE.transferAdd) {
		//转单
		_this.getDataBySrcId(props);
	} else if (status == 'equipAdd') {
		//设备树
		let pk = _this.props.getUrlParam('id');
		if (pk) {
			_this.getDataByEquipId(pk);
		}
	}
	_this.toggleShow(status);
}
