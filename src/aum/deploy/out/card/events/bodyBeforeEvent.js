import { formId, tableId } from '../const';
import { toast } from 'nc-lightapp-front';
import ampub from 'ampub';
const { utils, commonConst } = ampub;
const { multiLangUtils, msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { CommonKeys } = commonConst;
const { IBusiRoleConst } = CommonKeys;
const { showMessage } = msgUtils;

export default function bodyBeforeEvent(props, moduleId, key, value, index, record) {
	let editFlag = true;
	// refCondtion.call(this, props, moduleId, key, value, index, record);
	editFlag = isEdit.call(this, props, moduleId, key, value, index, record);
	return editFlag;
}
//设备有值，物料编码、设备名称、设备类别、规格、型号、数量不允许编辑
function isEdit(props, moduleId, key, value, index, record) {
	if (key == 'pk_equip') {
		let pk_ownerunit_out = props.form.getFormItemsValue(formId, 'pk_ownerunit_out').value; // 调出货主
		let pk_ownerorg_out = props.form.getFormItemsValue(formId, 'pk_ownerorg_out').value; //调出货主管理组织
		if (!pk_ownerunit_out) {
			// toast({ content: '请先录入调出货主', color: 'warning' });
			let config = {
				content: getMultiLangByID('452003008A-000001'),
				color: 'warning'
			};
			showMessage.call(this, props, config);
			/* 国际化处理： 当前环境并不支持SVG*/
			return false;
		}
		if (!pk_ownerorg_out) {
			// toast({ content: '请先录入调出货主管理组织', color: 'warning' });
			let config = {
				content: getMultiLangByID('452003008A-000002'),
				color: 'warning'
			};
			showMessage.call(this, props, config);
			/* 国际化处理： 当前环境并不支持SVG*/
			return false;
		}
		//TODO 有权限组织判断
	}
	return true;
}
