import { formId } from '../const';

import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils, msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { showMessage } = msgUtils;

//表头货主控制
export default function formBeforeEvent(props, moduleId, key, value, index, record) {
	let editFlag = true;
	editFlag = isEdit.call(this, props, moduleId, key, value, index, record);
	return editFlag;
}

function isEdit(props, moduleId, key, value, index, record) {
	switch (key) {
		case 'pk_ownerunit_out_v':
			let pk_ownerorg_out_v = props.form.getFormItemsValue(formId, 'pk_ownerorg_out_v').value;
			if (!pk_ownerorg_out_v) {
				props.form.setFormItemsDisabled(formId, { pk_ownerunit_out_v: false });
				// toast({ content: '请先填写调出管理组织', color: 'warning' });
				let config = {
					content: getMultiLangByID('452003008A-000003'),
					color: 'warning'
				};
				showMessage.call(this, props, config);
				return false;
			}
			return true;

		case 'pk_ownerunit_in_v':
			let pk_ownerorg_in_v = props.form.getFormItemsValue(formId, 'pk_ownerorg_in_v').value;
			if (!pk_ownerorg_in_v) {
				props.form.setFormItemsDisabled(formId, { pk_ownerunit_out_v: false });
				// toast({ content: '请先填写调入管理组织', color: 'warning' });
				let config = {
					content: getMultiLangByID('452003008A-000004'),
					color: 'warning'
				};
				showMessage.call(this, props, config);
				/* 国际化处理： 当前环境并不支持SVG*/
				return false;
			}
			return true;
		//不是特殊字段都是可以编辑
		default:
			return true;
	}
}
