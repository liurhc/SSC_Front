
import { loadDataByPk } from './buttonClick';

export default function (props, pk) {
    // 更新参数
	props.setUrlParam({ id: pk });
	loadDataByPk.call(this, props, pk);
}
