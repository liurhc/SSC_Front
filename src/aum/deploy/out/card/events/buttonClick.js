import { ajax, toast, print, cardCache, output } from 'nc-lightapp-front';
import {
	formId,
	tableId,
	fileName,
	pagecode,
	list_pagecode,
	reqUrl,
	printNodekey,
	TRANSFERINFO,
	listRouter,
	pkField,
	dataSource,
	transferRouter,
	node_code,
	bill_type,
	transferDataSource,
	fiMoneyFields
} from '../const';

import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { multiLangUtils, cardUtils, msgUtils, transferUtils } = utils;
const { ScriptReturnUtils, LoginContext, queryAboutUtils, saveValidatorsUtil } = components;

const { getScriptCardReturnData } = ScriptReturnUtils;
const { openBillTrack } = queryAboutUtils;
const { beforeSaveValidator } = saveValidatorsUtil;
const { pushToTransferPage } = transferUtils;
const { getMultiLangByID } = multiLangUtils;
const { setCardValue, setHeadAreaData } = cardUtils;
const { getContext, loginContextKeys, setContext } = LoginContext;

const { StatusUtils, CommonKeys } = commonConst;
const { ButtonConst, TransferConst } = CommonKeys;

const { showConfirm, MsgConst, showMessage } = msgUtils;
const { UISTATE } = StatusUtils;

import afterEvent from './afterEvent';

export default function(props, id) {
	switch (id) {
		case 'Add':
			add.call(this, props);
			break;
		case 'Edit':
			edit.call(this, props);
			break;
		case 'Delete':
			deleteConfirm.call(this, props);
			break;
		case 'Save':
			saveClick.call(this, props);
			break;
		case 'Cancel':
			cancelConfirm.call(this, props);
			break;
		case 'AddLine':
			props.cardTable.addRow(tableId);
			setBatchAlterDisabled.call(this, props);
			break;
		case 'DelLine':
			delline.call(this, props);
			setBatchAlterDisabled.call(this, props);
			setBodyBtnsEnable.call(this, props, tableId);
			break;
		case 'Commit':
			commit.call(this, props, 'SAVE', 'commit');
			break;
		case 'SaveCommit':
			commit.call(this, props, 'SAVE', 'saveCommit');
			break;
		case 'UnCommit':
			commit.call(this, props, 'UNSAVE', '');
			break;
		case 'GetFinanceData':
			getFianceData.call(this, props);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'QueryAboutBusiness':
			let pk_redeploy_out = props.form.getFormItemsValue(formId, pkField);
			if (pk_redeploy_out && pk_redeploy_out.value) this.setState({ pk_redeploy_out: pk_redeploy_out.value });
			openBillTrack(this);
			break;
		case 'QueryAboutBillFlow':
			queryaboutbillflow.call(this, props);
			break;
		case 'Refresh':
			refresh.call(this, props);
			break;
		case 'BatchAlter':
			BatchAlter.call(this, props, this.tableId);
			break;
		case 'Attachment':
			AttachmentClick.call(this, props);
			break;
		default:
			//默认是动态加载的拉单按钮，拉单按钮的code为“单据类型+交易类型”
			let srcinfo = id.split('+');
			//清掉上游缓存(退出转单和从上游返回下游然后再到上游的时候)
			props.transferTable.deleteCache(transferDataSource);
			pushToTransferPage.call(
				this,
				transferRouter, //上游查询页面的路由
				srcinfo[0], // 上游的bill_type
				srcinfo[1], // 上游的transtype
				bill_type, // 下游的bill_type
				getContext(loginContextKeys.transtype), // 下游的transtype
				'452003008A_transfercard' // 下游转单页面的pagecode
			);
			break;
	}
}

/**
 * 新增
 */
function add(props) {
	let pk_redeploy_out = props.form.getFormItemsValue(formId, pkField);
	if (pk_redeploy_out && pk_redeploy_out.value) {
		setContext.call(this, pkField, pk_redeploy_out, dataSource);
	}
	props.form.EmptyAllFormValue(formId);
	props.cardTable.setTableData(tableId, { rows: [] });
	setDefaultValue.call(this, props);
	this.setState({ step: -1 });
	let status = props.getUrlParam('status');
	this.nowUIStatus = UISTATE.add;
	if (status == UISTATE.edit || status == UISTATE.add) {
		props.button.setButtonVisible(ButtonConst.AddTransferGroup, false);
	}
	let pk_ownerorg_out_v = props.form.getFormItemsValue(formId, 'pk_ownerorg_out_v');
	if (pk_ownerorg_out_v && pk_ownerorg_out_v.value) {
		props.form.setFormItemsDisabled(formId, { pk_ownerorg_out_v: false });
		props.cardTable.addRow(tableId, undefined, undefined, false);
		afterEvent.call(this, props, formId, 'pk_recorder');
	} else {
		props.initMetaByPkorg('pk_ownerorg_out_v');
	}
	this.toggleShow(UISTATE.add);
}

/**
 * 修改
 */
function edit(props) {
	let pk = this.props.form.getFormItemsValue(formId, pkField).value;
	ajax({
		url: reqUrl.editUrl,
		data: {
			pk: pk,
			resourceCode: node_code
		},
		success: (res) => {
			if (res.data) {
				toast({ color: 'warning', content: res.data });
			} else {
				props.cardTable.closeExpandedRow(tableId);
				this.toggleShow(UISTATE.edit);
				if (this.nowUIStatus == UISTATE.edit || this.nowUIStatus == UISTATE.add) {
					props.button.setButtonVisible(ButtonConst.AddTransferGroup, false);
					props.form.setFormItemsDisabled(formId, { pk_ownerorg_out_v: true });
				}
				//去掉进度条
				this.setState({ step: -1 });
			}
		}
	});
}

/**
 * 取消
 */
export function cancel() {
	// 加载编辑前数据，如果编辑前没有数据，则加载当前列表最后一条数据，如果还没有，显示为空不处理
	let pk = getContext.call(this, pkField, dataSource);
	if (pk) {
		pk = pk.value;
	}
	let pk_org_v = this.props.form.getFormItemsValue(this.formId, 'pk_ownerorg_out_v');
	if (!pk_org_v || !pk_org_v.value) {
		this.props.resMetaAfterPkorgEdit();
	}
	//查询完清空单页缓存的主键
	setContext.call(this, pkField, null, dataSource);
	if (!pk) {
		pk = this.props.form.getFormItemsValue(formId, pkField).value;
	}
	if (!pk) {
		pk = cardCache.getCurrentLastId(dataSource);
	}
	if (pk) {
		loadDataByPk.call(this, this.props, pk);
	}
	this.toggleShow(UISTATE.browse);
	this.props.button.setButtonVisible(ButtonConst.AddTransferGroup, true);
	if (!pk) {
		this.props.form.EmptyAllFormValue(formId);
		this.props.cardTable.setTableData(tableId, { rows: [] });
		//新增取消步骤条为初始状态
		this.setState({ step: 0, pk_redeploy_out: '' });
		setNoDataButtonvisiable.call(this);
	}
}

/**
 * 保存按钮
 */
export function saveClick(props) {
	// 保存前校验
	let pass = beforeSaveValidator.call(this, props, formId, tableId, []);
	if (!pass) {
		return;
	}
	// let CardData = props.createMasterChildData(pagecode, formId, tableId);
	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let url = reqUrl.addUrl; //新增保存
	let status = this.nowUIStatus;
	if (status === 'edit') {
		url = reqUrl.updateUrl; //修改保存
	}
	props.validateToSave(CardData, () => {
		ajax({
			url: url,
			data: CardData,
			success: (res) => {
				if (res.success) {
					setCardValue.call(this, props, res.data, formId, tableId);
					let pk = props.form.getFormItemsValue(formId, pkField).value;
					// 保存成功后处理缓存
					let CardData = props.createMasterChildData(pagecode, formId, tableId);
					if (status == UISTATE.add) {
						cardCache.addCache(pk, CardData, formId, dataSource);
					} else {
						cardCache.updateCache(pkField, pk, CardData, formId, dataSource);
					}
					this.toggleShow(UISTATE.browse);
					let step = res.data.head[formId].rows[0].values.step.value;
					let pk_redeploy_out = res.data.head[formId].rows[0].values[pkField].value;
					// 	//保存的单据都处于自由态
					this.setState({ step, pk_redeploy_out });
					// toast({ content: '保存成功', color: 'success' });
					let config = {
						type: 'SaveSuccess'
					};
					showMessage.call(this, props, config);
					props.cardTable.closeModel(tableId);
				}
			}
		});
	});
}

/**
 * 删除按钮
 */
export function delConfirm() {
	let paramInfoMap = {};
	let pk = this.props.form.getFormItemsValue(formId, pkField).value;
	paramInfoMap[pk] = this.props.form.getFormItemsValue(this.formId, 'ts').value;
	ajax({
		url: reqUrl.deleteUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: pagecode
		},
		success: (res) => {
			let { success, data } = res;
			if (success) {
				let callback = (newpk) => {
					loadDataByPk.call(this, this.props, newpk);
				};
				getScriptCardReturnData.call(
					this,
					res,
					this.props,
					formId,
					tableId,
					pkField,
					dataSource,
					null,
					true,
					callback
				);
			}
		}
	});
}

/**
 * 通过单据id获取缓存单据信息，没有缓存则重新查询
 * @param {*} props 
 * @param {*} pk 
 */
export function loadDataByPk(props, pk) {
	if (pk == null || pk == '') {
		this.props.form.EmptyAllFormValue(formId);
		this.props.cardTable.setTableData(tableId, { rows: [] });
		//删除没有数据初始化为最初状态
		this.setState({ step: 0, pk_redeploy_out: '' });
		setNoDataButtonvisiable.call(this);
		let status = this.nowUIStatus;
		setHeadAreaData.call(this, this.props, { status, bill_code: '' });
	} else {
		let cachData = cardCache.getCacheById(pk, dataSource);
		if (cachData) {
			setValue.call(this, props, cachData);
		} else {
			getDataByPk.call(this, props, pk);
		}
	}
}

/**
 * 设置界面值
 * @param {*} props 
 * @param {*} data 
 */
export function setValue(props, data) {
	if (!data) {
		return;
	}
	setCardValue.call(this, props, data, formId, tableId);
	this.toggleShow(UISTATE.browse);
	let step = data.head[formId].rows[0].values.step.value;
	this.setState({ step: step });
}

/**
 * 整单保存
 */
export function modelSave(props) {
	saveClick.call(this, props);
}

/**
 * 删行
 */
export function delline(props) {
	let checkedRows = props.cardTable.getCheckedRows(tableId);
	let checkedIndex = [];
	checkedRows.map((item) => {
		checkedIndex.push(item.index);
	});
	props.cardTable.delRowsByIndex(tableId, checkedIndex);
}

/**
 * 提交
 */
export function commit(props, OperatorType, commitType, content) {
	if (commitType === 'saveCommit') {
		this.props.cardTable.filterEmptyRows(tableId);
		// 保存前校验
		let pass = beforeSaveValidator.call(this, props, formId, tableId, []);
		if (!pass) {
			return;
		}
	}
	if (commitType === 'commit') {
		this.props.cardTable.filterEmptyRows(tableId);
		// 保存前校验
		let pass = beforeSaveValidator.call(this, props, formId, tableId, []);
		if (!pass) {
			return;
		}
	}

	let pk = this.props.form.getFormItemsValue(this.formId, pkField).value;
	// let CardData = this.props.createMasterChildData(pagecode, formId, tableId);
	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let paramInfoMap = {};
	paramInfoMap[pk] = ''; //卡片页面ts为空也可以 CardData 里有ts
	let obj = {
		dataType: 'cardData',
		OperatorType: OperatorType,
		commitType: commitType,
		pageid: pagecode,
		paramInfoMap: paramInfoMap,
		content: content
	};
	CardData.userjson = JSON.stringify(obj);
	if (commitType === 'saveCommit') {
		if (content) {
			ajax({
				url: reqUrl.commiyUrl,
				data: CardData,
				success: (res) => {
					if (content) {
						this.setState({
							compositedisplay: false
						});
					}
					let srctype = props.getUrlParam(TransferConst.srctype);
					let callback = (pk, data) => {
						//判断是转单的保存提交还是正常保存提交
						if (srctype == TRANSFERINFO.srctype) {
							setTransferData.call(this, props);
							let step = res.data.cardVos[0].head[formId].rows[0].values.step.value;
							let pk_redeploy_out = res.data.cardVos[0].head[formId].rows[0].values[pkField].value;
							this.setState({ step, pk_redeploy_out });
							// let transferbodypk = 'pk_bill_b_src';//来源单据表体主键
							// let bodypkvalue = props.cardTable.getColValue(this.tableId, transferbodypk);//获取表体数据
							// bodypkvalue = bodypkvalue.map((pk) => {
							// 	return pk.value;
							// })
							// props.transferTable.setSavedTransferTableDataPk(bodypkvalue);
						} else {
							this.toggleShow(UISTATE.browse);
							let step = data.head[formId].rows[0].values.step.value;
							let pk_redeploy_out = data.head[formId].rows[0].values[pkField].value;
							this.setState({ step, pk_redeploy_out });
						}
					};
					if (srctype == TRANSFERINFO.srctype) {
						// let setvalue = (data) => {
						// 	setTransferData.call(this, data)
						// }
						//转单保存提交回刷
						getScriptCardReturnData.call(
							this,
							res,
							props,
							formId,
							tableId,
							pkField,
							dataSource,
							null,
							false,
							callback,
							pagecode
						);
					} else {
						getScriptCardReturnData.call(
							this,
							res,
							props,
							formId,
							tableId,
							pkField,
							dataSource,
							null,
							false,
							callback,
							pagecode
						);
					}
				},
				error: (res) => {
					if (res && res.message) {
						if (content) {
							this.setState({
								compositedisplay: false
							});
						}
						toast({ content: res.message, color: 'danger' });
					}
				}
			});
		} else {
			props.validateToSave(CardData, () => {
				ajax({
					url: reqUrl.commiyUrl,
					data: CardData,
					success: (res) => {
						if (content) {
							this.setState({
								compositedisplay: false
							});
						}
						let srctype = props.getUrlParam(TransferConst.srctype);
						let callback = (pk, data) => {
							if (srctype == TRANSFERINFO.srctype) {
								setTransferData.call(this, props);
								let step = res.data.cardVos[0].head[formId].rows[0].values.step.value;
								let pk_redeploy_out = res.data.cardVos[0].head[formId].rows[0].values[pkField].value;
								this.setState({ step, pk_redeploy_out });
								// let transferbodypk = 'pk_bill_b_src';//来源单据表体主键
								// let bodypkvalue = props.cardTable.getColValue(this.tableId, transferbodypk);//获取表体数据
								// bodypkvalue = bodypkvalue.map((pk) => {
								// 	return pk.value;
								// })
								// props.transferTable.setSavedTransferTableDataPk(bodypkvalue)
							} else {
								this.toggleShow(UISTATE.browse);
								let step = data.head[formId].rows[0].values.step.value;
								let pk_redeploy_out = data.head[formId].rows[0].values[pkField].value;
								this.setState({ step, pk_redeploy_out });
							}
						};
						if (srctype == TRANSFERINFO.srctype) {
							//转单保存提交回刷
							// let setvalue = (data) => {
							// 	setTransferData.call(this, data)
							// }
							getScriptCardReturnData.call(
								this,
								res,
								props,
								formId,
								tableId,
								pkField,
								dataSource,
								null,
								false,
								callback,
								pagecode
							);
						} else {
							getScriptCardReturnData.call(
								this,
								res,
								props,
								formId,
								tableId,
								pkField,
								dataSource,
								null,
								false,
								callback,
								pagecode
							);
						}
					},
					error: (res) => {
						if (res && res.message) {
							if (content) {
								this.setState({
									compositedisplay: false
								});
							}
							toast({ content: res.message, color: 'danger' });
						}
					}
				});
			});
		}
	} else {
		ajax({
			url: reqUrl.commiyUrl,
			data: CardData,
			success: (res) => {
				let { data } = res;
				if (content) {
					this.setState({
						compositedisplay: false
					});
				}
				let srctype = props.getUrlParam(TransferConst.srctype);
				let callback = (pk, data) => {
					if (srctype == TRANSFERINFO.srctype) {
						setTransferData.call(this, props);
						let step = res.data.cardVos[0].head[formId].rows[0].values.step.value;
						let pk_redeploy_out = res.data.cardVos[0].head[formId].rows[0].values[pkField].value;
						this.setState({ step, pk_redeploy_out });
						// let transferbodypk = 'pk_bill_b_src';//来源单据表体主键
						// let bodypkvalue = props.cardTable.getColValue(this.tableId, transferbodypk);//获取表体数据
						// bodypkvalue = bodypkvalue.map((pk) => {
						// 	return pk.value;
						// })
						// props.transferTable.setSavedTransferTableDataPk(bodypkvalue)
					} else {
						this.toggleShow(UISTATE.browse);
						let step = res.data.cardVos[0].head[formId].rows[0].values.step.value;
						let pk_redeploy_out = res.data.cardVos[0].head[formId].rows[0].values[pkField].value;
						this.setState({ step, pk_redeploy_out });
					}
				};
				if (srctype == TRANSFERINFO.srctype) {
					// let setvalue = (data) => {
					// 	setTransferData.call(this, data)
					// }
					//转单保存提交回刷
					getScriptCardReturnData.call(
						this,
						res,
						props,
						formId,
						tableId,
						pkField,
						dataSource,
						null,
						false,
						callback,
						pagecode
					);
				} else {
					getScriptCardReturnData.call(
						this,
						res,
						props,
						formId,
						tableId,
						pkField,
						dataSource,
						null,
						false,
						callback,
						pagecode
					);
				}
			},
			error: (res) => {
				if (res && res.message) {
					if (content) {
						this.setState({
							compositedisplay: false
						});
					}
					toast({ content: res.message, color: 'danger' });
				}
			}
		});
	}
}

/**
* 取财务数据
* @param {*} props 
*/
function getFianceData(props) {
	props.cardTable.filterEmptyRows(tableId);
	// let CardData = props.createMasterChildData(pagecode, formId, tableId);
	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	//如果没有表体数据直接返回
	let bodyvos = props.cardTable.getVisibleRows(tableId);
	if (!bodyvos || bodyvos.length == 0) {
		return;
	}
	ajax({
		url: reqUrl.getFianceDataUrl,
		data: CardData,
		success: (res) => {
			if (res.success) {
				setCardValue.call(this, props, res.data, formId, tableId);
			}
		}
	});
}

/**
 * 打印
 * @param {*} props 
 */
function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	// let { pageConfig } = props;
	if (!printData) {
		// toast({ content: '请选择需要打印的数据', color: 'warning' });
		let config = {
			type: 'ChoosePrint'
		};
		showMessage.call(this, props, config);
		return;
	}

	// let pks = [pk_redeploy_out.value];
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		reqUrl.printCardUrl, // 后台打印服务url
		printData
	);
}

/**
 * 输出
 * @param {*} props 
 */
function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		// toast({ content: '请选择需要输出的数据', color: 'warning' });
		let config = {
			type: 'ChooseOutput'
		};
		showMessage.call(this, props, config);
		return;
	}
	output({
		url: reqUrl.printCardUrl,
		data: printData
	});
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let pk_redeploy_out = props.form.getFormItemsValue(formId, 'pk_redeploy_out');
	if (!pk_redeploy_out || !pk_redeploy_out.value) {
		return false;
	}
	let pks = [ pk_redeploy_out.value ];
	let printData = {
		filename: getMultiLangByID(fileName), // 文件名称
		nodekey: printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

/**
* 刷新
* @param {*} props 
*/
export function refresh(props) {
	let pk = props.form.getFormItemsValue(formId, 'pk_redeploy_out').value;
	if (!pk) {
		return;
	}
	let callback = (data) => {
		if (data) {
			// toast({ content: '刷新成功', color: 'success' });
			let config = {
				type: 'RefreshSuccess'
			};
			showMessage.call(this, props, config);
		} else {
			setNoDataButtonvisiable.call(this);
		}
	};
	// 查询到最新单据数据
	getDataByPk.call(this, props, pk, callback);
}

/**
 * 通过单据id查询单据信息
 * @param {*} props 
 * @param {*} pk 
 */
export function getDataByPk(props, pk, callback) {
	ajax({
		url: reqUrl.queryUrl,
		data: {
			pagecode: pagecode,
			pk: pk
		},
		success: (res) => {
			if (res.data) {
				setCardValue.call(this, props, res.data, formId, tableId);
				this.toggleShow();
				let step = res.data.head[formId].rows[0].values.step.value;
				let pk_redeploy_out = this.props.form.getFormItemsValue(formId, pkField).value;
				this.setState({ step: step, pk_redeploy_out: pk_redeploy_out });
			}
			if (!res.data || typeof res.data == 'undefined' || res.data == null) {
				props.form.EmptyAllFormValue(formId);
				let nulldata = {
					rows: []
				};
				props.cardTable.setTableData(tableId, nulldata);
				cardCache.deleteCacheById(pkField, pk, dataSource);
				//没有数据刷新表头的单号
				this.setState({ step: 0, pk_redeploy_in: '' });
				let status = this.nowUIStatus;
				setHeadAreaData.call(this, this.props, { status, bill_code: '' });
				// toast({ content: '此数据已删除！', color: 'warning' });
				let config = {
					content: getMultiLangByID('452003016A-000003'),
					color: 'warning'
				};
				showMessage.call(this, props, config);
			}
			cardCache.updateCache(pkField, pk, res.data, formId, dataSource);
			typeof callback == 'function' && callback(res.data);
		}
	});
}

/**
 * 審批詳情
 */

export function queryaboutbillflow(props) {
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let transtype = props.form.getFormItemsValue(formId, 'transi_type').value;
	this.setState({
		pk_redeploy_out: pk,
		showApprove: true,
		transi_type: transtype
	});
}

/**
 * 附件
 */

export function AttachmentClick(props) {
	let billId = props.form.getFormItemsValue(formId, 'pk_redeploy_out').value;
	props.ncUploader.show('warcontract-uploader', {
		billId: 'pam/redeploy_out/' + billId
	});
}

/**
 * 批改
 */
export function BatchAlter(props) {
	// 获取表体行有效数据的行数
	let visibleRows = props.cardTable.getVisibleRows(tableId);
	// 如果没有数据或只有一条数据，直接返回
	if (visibleRows.length < 2) {
		return;
	}
	// 获取原始数据
	let changeData = props.cardTable.getTableItemData(tableId);

	// let cardData = props.createMasterChildData(pagecode, formId, tableId);
	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	ajax({
		url: reqUrl.batchUrl,
		data: {
			card: CardData,
			batchChangeIndex: changeData.batchChangeIndex, // 原始数据行号
			batchChangeKey: changeData.batchChangeKey, // 原始数据key
			batchChangeValue: changeData.batchChangeValue, // 原始数据value
			// 原始数据所在的vo，对应聚合VO中的tableCodes，一主一子可为空，一主多子要传对应的值
			tableCode: ''
		},
		async: false,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				//把数据设置到界面上
				if (data.body) {
					// Promise.resolve(true).then(() => {
					// 	// props.cardTable.setTableData(tableId, data.body[tableId]);
					// });
					data.body[tableId].rows.forEach((row) => {
						fiMoneyFields.forEach((field) => {
							delete row.values[field];
						});
					});
					setCardValue.call(this, props, res.data, formId, tableId);
				}
			}
		}
	});
}

/**
 * 返回
 */

export function linkToList(props) {
	props.pushTo(listRouter, { pagecode: list_pagecode });
}

/**
* 返回上游
*/

export function linkToTransfer(props, type) {
	if (type == 'cancel') {
		ToTransfer.call(this, props);
	} else {
		//判断当前单据是否存在没有处理的
		let count = props.transferTable.getTransformFormAmount(TRANSFERINFO.leftarea); //转单数据的数量
		let status = true;
		for (let index = 0; index < count; index++) {
			status = props.transferTable.getTransformFormStatus(TRANSFERINFO.leftarea, index);
			if (!status) {
				break;
			}
		}
		if (!status) {
			let backConfirm = () => {
				ToTransfer.call(this, props);
			};
			showConfirm.call(this, props, { type: MsgConst.Type.Back, beSureBtnClick: backConfirm });
			// props.ncmodal.show(`${pagecode}-confirm`, {
			// 	title: '注意',
			// 	content: '当前存在未处理完成的单据，是否放弃转单？',
			// 	beSureBtnClick: () => {
			// ToTransfer.call(this, props);
			// }
			// });
		} else {
			ToTransfer.call(this, props);
		}
	}
}

/**
 * 跳转转单页面
 * @param {*} props 
 */
export function ToTransfer(props) {
	let billtype = bill_type;
	let transiType = getContext(loginContextKeys.transtype);
	ajax({
		url: '/nccloud/ampub/common/querySrcActionInfo.do',
		data: {
			billtype,
			transiType
		},
		success: (res) => {
			let info = JSON.parse(res.data);
			let id = info[0].actionCode;
			let srcinfo = id.split('+');
			pushToTransferPage.call(
				this,
				transferRouter, //上游查询页面的路由
				srcinfo[0], // 上游的bill_type
				srcinfo[1], // 上游的transtype
				bill_type, // 下游的bill_type
				getContext(loginContextKeys.transtype), // 下游的transtype
				'452003008A_transfercard' // 下游转单页面的pagecode
			);
		}
	});
}

/**
 * 设置普通新增单据默认值
 */

export function setDefaultValue(props) {
	// 集团
	let groupId = getContext(loginContextKeys.groupId);
	let groupName = getContext(loginContextKeys.groupName);
	// 交易类型
	let transi_type = getContext(loginContextKeys.transtype);
	let pk_transitype = getContext(loginContextKeys.pk_transtype);
	// 默认主组织
	let pk_org = getContext(loginContextKeys.pk_org);
	let org_Name = getContext(loginContextKeys.org_Name);
	let pk_org_v = getContext(loginContextKeys.pk_org_v);
	let org_v_Name = getContext(loginContextKeys.org_v_Name);
	//业务日期
	let businessDate = getContext(loginContextKeys.businessDate);
	props.form.setFormItemsValue(formId, {
		pk_group: { value: groupId, display: groupName },
		pk_org: { value: pk_org, display: org_Name },
		pk_org_v: { value: pk_org_v, display: org_v_Name },
		//主组织为调出货主管理组织
		pk_ownerorg_out_v: { value: pk_org_v, display: org_v_Name },
		pk_ownerorg_out: { value: pk_org, display: org_Name },
		bill_type: { value: bill_type },
		transi_type: { value: transi_type },
		pk_transitype: { value: pk_transitype },
		bill_status: { value: '0', display: getMultiLangByID('statusUtils-000000') },
		bill_date: { value: businessDate }
	});
}

/**
 * 设置资产树链接过来的默认值
 * @param {*} props 
 */
export function setDefaultTreeValue(props) {
	// 集团
	let groupId = getContext(loginContextKeys.groupId);
	let groupName = getContext(loginContextKeys.groupName);
	// 交易类型
	let transi_type = getContext(loginContextKeys.transtype);
	let pk_transitype = getContext(loginContextKeys.pk_transtype);
	//业务日期
	let businessDate = getContext(loginContextKeys.businessDate);
	//资产树的组织默认值从设备中取
	props.form.setFormItemsValue(formId, {
		pk_group: { value: groupId, display: groupName },
		bill_type: { value: bill_type },
		transi_type: { value: transi_type },
		pk_transitype: { value: pk_transitype },
		bill_status: { value: '0', display: getMultiLangByID('statusUtils-000000') },
		bill_date: { value: businessDate }
	});
}

/**
 * 设置新增取消没有数据时按钮可见
 * @param {*} props 
 */
export function setNoDataButtonvisiable() {
	let buttonArray = [ 'Edit', 'Delete', 'Commit', 'UnCommit', 'Attachment', 'QueryAbout', 'Print', 'Refresh' ];
	this.props.button.setButtonVisible(buttonArray, false);
}

/**
 * 设置表体按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBodyBtnsEnable(props, moduleId) {
	let num = props.cardTable.getNumberOfRows(moduleId);
	let checkedRows = [];
	if (num > 0) {
		checkedRows = props.cardTable.getCheckedRows(moduleId);
	}
	this.props.button.setButtonDisabled([ 'DelLine' ], !(checkedRows && checkedRows.length > 0));
}

/**
 * 设置批改,删行按钮的可见性
 */
export function setBatchAlterDisabled(props) {
	let rownum = props.cardTable.getNumberOfRows(tableId, false); //获取当前行数
	if (rownum > 0) {
		let checkedRows = props.cardTable.getCheckedRows(tableId); //获取当前选中行数
		props.button.setButtonDisabled('DelLine', !(checkedRows && checkedRows.length > 0));
	} else {
		props.button.setButtonDisabled('DelLine', true);
	}
}

/**
 * 转单保存提交往页面赋值
 * @param {*} data 
 */
export function setTransferData(props) {
	// this.putHeadFormValue(data.head[this.formId]);
	// this.putTableValue(data.body[this.tableId]);
	this.toggleShow(UISTATE.transferBorwse);
	this.synTransferData();
	//回刷加载下一条数据前，先清空缓存数组中是否存在该条数据的pk
	let headpk = 'pk_redeploy_out';
	let headpkvalue = this.props.form.getFormItemsValue(this.formId, headpk).value;
	this.setState({ pk_redeploy_out: headpkvalue });
	if (this.transferEditId.indexOf(headpkvalue) != -1) {
		//移除缓存数组中记录保存的id
		let index = this.transferEditId.indexOf(headpkvalue);
		this.transferEditId.splice(index, 1);
	}
	let transferbodypk = 'pk_bill_b_src'; //来源单据表体主键
	let bodypkvalue = props.cardTable.getColValue(this.tableId, transferbodypk); //获取表体数据
	bodypkvalue = bodypkvalue.map((pk) => {
		return pk.value;
	});
	props.transferTable.setSavedTransferTableDataPk(bodypkvalue);
	this.props.transferTable.setTransformFormStatus(TRANSFERINFO.leftarea, {
		status: true,
		childId: 'pk_redeploy_out_b',
		onChange: (current, next, currentIndex) => {
			//props.transferTable.setTransferListValueByIndex(TRANSFERINFO.leftarea,next,currentIndex);
			this.transferIndex = currentIndex + 1;
			//保存成功提示并设置页面跳转状态
		}
	});
}

/**
 * 调用公共弹窗
 * @param {*} props 
 */
function deleteConfirm(props) {
	showConfirm.call(this, props, { type: MsgConst.Type.Delete, beSureBtnClick: delConfirm });
}

function cancelConfirm(props) {
	showConfirm.call(this, props, { type: MsgConst.Type.Cancel, beSureBtnClick: cancel });
}
