//主子表卡片
import React, { Component } from 'react';
import { createPage, ajax, base, high } from 'nc-lightapp-front';
import { buttonClick, initTemplate, afterEvent, pageInfoClick, transferBtnClick, rowSelected } from './events';
import {
	delConfirm,
	modelSave,
	cancel,
	linkToList,
	linkToTransfer,
	commit,
	setDefaultTreeValue
} from './events/buttonClick';

import formBeforeEvent from './events/formBeforeEvent';
import bodyBeforeEvent from './events/bodyBeforeEvent';
import * as CONSTANTS from './const';
import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { multiLangUtils, cardUtils, closeBrowserUtils, msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const {
	createCardTitleArea,
	createCardPaginationArea,
	getCommonTableHeadLeft,
	setHeadAreaData,
	setCardValue
} = cardUtils;
const { ApprovalTrans, LoginContext } = components;
const { getContext, loginContextKeys } = LoginContext;
const { CommonKeys, StatusUtils } = commonConst;
const { TransferConst, ButtonConst } = CommonKeys;

const { UISTATE } = StatusUtils;

import './index.less';

const { NCAffix, NCStep, NCBackBtn } = base;
const {
	formId,
	tableId,
	pagecode,
	TRANSFERINFO,
	editButtons,
	browseButtons,
	reqUrl,
	pkField,
	dataSource,
	transferDataSource,
	bill_type
} = CONSTANTS;

class Card extends Component {
	constructor(props) {
		super(props);
		this.state = {
			bill_type: bill_type,
			transi_type: '',
			bill_code: '',
			show: false,
			showApprove: false,
			pk_redeploy_out: '',
			step: 0,
			compositedisplay: false,
			compositedata: {}
		};

		closeBrowserUtils.call(this, props, { form: [ formId ], cardTable: [ tableId ] });
		initTemplate.call(this, props);
		this.pageid = pagecode;
		this.srctype = props.getUrlParam(TransferConst.srctype);
		if (this.srctype == TRANSFERINFO.srctype) {
			this.pageid = TRANSFERINFO.pagecode;
		}
		this.leftarea = TRANSFERINFO.leftarea;
		this.transferIndex = 0; //转单时左侧选择的数据序号
		this.transferEditId = new Array(); //转单后已处理单据又点修改的主键存储
		this.nowUIStatus = props.getUrlParam('status'); //存储界面当前状态
		this.formId = formId;
		this.tableId = tableId;
	}
	componentDidMount() {}

	//指派方法
	getAssginUsedr = (content) => {
		commit.call(this, this.props, 'SAVE', 'commit', content);
	};
	//取消 指派
	turnOff = () => {
		this.setState({
			compositedisplay: false
		});
	};

	//根据页面状态设置控件属性
	toggleShow = (status) => {
		if (!status) {
			status = this.props.getUrlParam('status');
		}
		let pkVal = this.props.form.getFormItemsValue(formId, pkField);
		if (pkVal) {
			pkVal = pkVal.value;
		} else {
			pkVal = '';
		}
		let data = {
			status: status,
			id: pkVal
		};
		this.props.setUrlParam(data);
		let editflag = false;
		let areaStatus = 'browse';
		if (
			status == UISTATE.add ||
			status == UISTATE.edit ||
			status == UISTATE.transferAdd ||
			status == UISTATE.transferEdit ||
			status == 'equipAdd'
		) {
			areaStatus = 'edit';
			editflag = true;
		}
		//设置表单和表格的可编辑性
		this.props.form.setFormStatus(formId, areaStatus);
		this.props.cardTable.setStatus(tableId, areaStatus);
		//设置按钮的可见性
		this.props.button.setButtonVisible(editButtons, editflag);
		this.props.button.setButtonVisible(browseButtons, !editflag);
		this.props.button.setButtonVisible(ButtonConst.AddTransferGroup, !editflag);
		let bill_status = this.props.form.getFormItemsValue(formId, [ 'bill_status' ]);

		if (bill_status[0] != undefined) {
			let bill_status = this.props.form.getFormItemsValue(formId, [ 'bill_status' ])[0].value;
			if (bill_status === '0') {
				//自由态
				this.props.button.setButtonVisible([ 'UnCommit' ], false); //不显示收回
			} else if (bill_status === '1') {
				//已提交
				this.props.button.setButtonVisible([ 'Commit', 'Delete', 'Edit' ], false); //不显示提交、修改、删除
			} else if (bill_status === '2') {
				//审批中
				this.props.button.setButtonVisible([ 'Commit', 'Delete', 'Edit', 'UnCommit' ], false); //不显示提交、修改、删除、收回
			} else if (bill_status === '3') {
				//审批通过
				this.props.button.setButtonVisible([ 'Commit', 'Delete', 'Edit' ], false); //不显示提交、修改、删除
			} else if (bill_status === '4') {
				//审批未通过
				this.props.button.setButtonVisible([ 'Commit', 'UnCommit' ], false); //不显示提交、收回;
			} else if (bill_status === '6') {
				//关闭
				this.props.button.setButtonVisible([ 'Commit', 'Delete', 'Edit', 'UnCommit' ], false); //不显示提交、修改、删除、收回
			} else {
				//新增时取消过来的
				this.props.button.setButtonVisible([ 'Commit', 'Delete', 'Edit', 'UnCommit' ], false); //不显示提交、修改、删除、收回
			}
		}
		if (status == UISTATE.transferBorwse) {
			//转单新增保存后不需要新增按钮
			this.props.button.setButtonVisible([ 'Add' ], false);
		}
		if (status == UISTATE.transferAdd || status == UISTATE.transferEdit) {
			//退出转单
			this.props.button.setButtonVisible([ 'Quit' ], true);
		}
		this.nowUIStatus = status;
		setHeadAreaData.call(this, this.props, { status });
	};

	//设置form表单head区域的信息
	putHeadFormValue = (formData) => {
		this.props.form.setAllFormValue({
			[formId]: formData
		});
		if (formData.rows[0].values.bill_code) {
			let bill_code = formData.rows[0].values.bill_code.value;
			this.setState({ bill_code: bill_code });
		}
	};
	//设置表体区域的信息
	putTableValue = (tableData) => {
		let timer = null;
		timer = setTimeout(() => {
			this.props.cardTable.setTableData(tableId, tableData);
			timer = null;
		}, 100);
		//设置详细信息
	};

	//设置转单右侧界面数据
	putTransferRightValue = (record, index, status) => {
		this.transferIndex = index;
		//点击转单缩略图的钩子函数 status:转单处理状态
		this.putHeadFormValue(record.head[formId]);
		this.putTableValue(record.body[tableId]);
		if (status) {
			let pk_redeploy_out = this.props.form.getFormItemsValue(formId, pkField).value;
			if (this.transferEditId.indexOf(pk_redeploy_out.value) == -1) {
				this.toggleShow(UISTATE.transferBorwse);
			} else {
				this.toggleShow(UISTATE.transferEdit);
			}
		} else {
			this.toggleShow(UISTATE.transferAdd);
		}
	};
	//同步单据信息到转单控件
	synTransferData = () => {
		//重取界面现有数据赋值到转单中
		let cardData = this.props.createMasterChildData(this.pageid, formId, tableId);
		this.props.transferTable.setTransferListValueByIndex(TRANSFERINFO.leftarea, cardData, this.transferIndex);
	};

	//根据上游来源单据信息获取下游单据信息
	getDataBySrcId = (props) => {
		let pks = props.transferTable.getTransferTableSelectedId();
		let transitype = getContext(loginContextKeys.transtype);
		ajax({
			url: reqUrl.scriptURL,
			data: {
				pks: pks,
				pageid: this.pageid,
				srcbilltype: '4A40',
				srctranstype: '4A40-01',
				nextbilltype: this.state.bill_type,
				nexttranstype: transitype
			},
			success: (res) => {
				if (res.data) {
					this.props.transferTable.setTransferListValue(this.leftarea, res.data);
				}
				//设置组织不可编辑
				props.form.setFormItemsDisabled(formId, { pk_org: false, pk_org_v: false });
			}
		});
	};
	selfButtonClick = (props, id) => {
		buttonClick.call(this, props, id);
	};

	//资产树链接调所有权调出
	getDataByEquipId = (pk) => {
		let data = { pk, pagecode };
		ajax({
			url: reqUrl.equipTreeLinkUrl,
			data,
			success: (res) => {
				setCardValue.call(this, this.props, res.data, formId, tableId);
				setDefaultTreeValue.call(this, this.props);
				this.toggleShow(UISTATE.add);
			}
		});
	};

	//返回列表界面
	backToList = () => {
		linkToList.call(this, this.props);
	};

	//返回上游界面
	backToTransfer = () => {
		linkToTransfer.call(this, this.props);
	};

	getButtonNames = (codeId) => {
		if (codeId === 'edit' || codeId === 'add' || codeId === 'delete') {
			return 'main-button';
		} else {
			return 'secondary-button';
		}
	};

	//获取列表肩部信息
	getTableHeadLeft = () => {
		return getCommonTableHeadLeft.call(this, this.props, { tableId: tableId });
	};
	//获取列表肩部信息
	getTableHead = () => {
		let { button } = this.props;
		let { createButtonApp } = button;
		let status = this.props.getUrlParam('status');
		return (
			<div className="shoulder-definition-area">
				<div className="definition-icons">
					{/* 表体页肩按钮 */}
					{createButtonApp({
						area: 'card_body',
						buttonLimit: 3,
						onButtonClick: buttonClick.bind(this),
						popContainer: document.querySelector('.nc-bill-table-area')
					})}
				</div>
			</div>
		);
	};

	//进度条
	createSteps = () => {
		return (
			<NCStep.NCSteps current={this.state.step}>
				<NCStep title={getMultiLangByID('452003008A-000007')} />
				<NCStep title={getMultiLangByID('452003008A-000008')} />
				<NCStep title={getMultiLangByID('452003008A-000009')} />
				<NCStep title={getMultiLangByID('452003008A-000010')} />
				<NCStep title={getMultiLangByID('452003008A-000011')} />
			</NCStep.NCSteps>
		);
	};
	render() {
		let { cardTable, form, button, ncmodal, cardPagination, transferTable, ncUploader } = this.props;
		const { createCardPagination } = cardPagination;
		let { createNCUploader } = ncUploader;
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createButtonApp } = button;
		let { createModal } = ncmodal;
		let status = this.nowUIStatus;
		let srctype = this.srctype;
		const { createTransferList } = transferTable;
		const { BillTrack, ApproveDetail } = high;
		let leftarea = this.leftarea;
		return (
			<div>
				{srctype == TRANSFERINFO.srctype ? (
					<div className="nc-bill-transferList" style={{ backgroundColor: '#fff' }}>
						<div className="nc-bill-top-area">
							<NCAffix>
								<div className="nc-bill-header-area">
									{/* 标题 title */}
									{createCardTitleArea.call(this, this.props, {
										title: getMultiLangByID('452003008A-000005'),
										formId,
										showBackBtn: true,
										backBtnClick: this.backToTransfer
									})}
									<div className="header-button-area">
										{this.props.button.createButtonApp({
											area: 'card_head',
											buttonLimit: 3,
											onButtonClick: transferBtnClick.bind(this)
										})}
									</div>
								</div>
							</NCAffix>
						</div>
						<div className="nc-bill-transferList-content">
							{createTransferList({
								headcode: formId,
								pkname: pkField,
								dataSource: transferDataSource,
								transferListId: leftarea, //转单列表id
								onTransferItemSelected: (record, status, index) => {
									//自动被选中触发（刚打开节点/保存某一条）
									this.transferIndex = index;
									//点击转单缩略图的钩子函数 status:转单处理状态
									this.putHeadFormValue(record.head[formId]);
									this.putTableValue(record.body[tableId]);
									if (status) {
										let pk_redeploy_out = this.props.form.getFormItemsValue(
											formId,
											'pk_redeploy_out'
										).value;
										if (this.transferEditId.indexOf(pk_redeploy_out) == -1) {
											this.toggleShow(UISTATE.transferBorwse);
										} else {
											this.toggleShow(UISTATE.transferEdit);
										}
									} else {
										this.toggleShow(UISTATE.transferAdd);
									}
								},
								onTransferItemClick: (record, index, status) => {
									this.putTransferRightValue(record, index, status);
								}
							})}
							<div className="nc-bill-card transferList-content-right">
								{status == 'transferBorwse' ? (
									<div className="nc-bill-form-area transfer-borwse"> {this.createSteps()}</div>
								) : (
									''
								)}
								<div className="nc-bill-form-area">
									{createForm(formId, {
										onAfterEvent: afterEvent.bind(this),
										onBeforeEvent: formBeforeEvent.bind(this),
										pkname: pkField,
										dataSource: dataSource
									})}
								</div>
								<div className="nc-bill-bottom-area">
									<div className="nc-bill-table-area">
										{createCardTable(tableId, {
											tableHeadLeft: this.getTableHeadLeft.bind(this),
											tableHead: this.getTableHead.bind(this),
											showCheck: true,
											showIndex: true,
											isAddRow: true,
											modelSave: modelSave.bind(this),
											onAfterEvent: afterEvent.bind(this),
											onBeforeEvent: bodyBeforeEvent.bind(this),
											selectedChange: rowSelected.bind(this),
											pkname: pkField,
											dataSource: dataSource
										})}
									</div>
								</div>
							</div>
						</div>
					</div>
				) : (
					<div className="nc-bill-card">
						<div className="nc-bill-top-area">
							<NCAffix>
								<div className="nc-bill-header-area">
									{/* 标题 title */}
									{createCardTitleArea.call(this, this.props, {
										title: getMultiLangByID('452003008A-000005'),
										formId,
										backBtnClick: this.backToList
									})}

									<div className="header-button-area">
										{createButtonApp({
											area: 'card_head',
											buttonLimit: 3,
											onButtonClick: buttonClick.bind(this),
											popContainer: document.querySelector('.nc-bill-table-area')
										})}
									</div>
									{createCardPaginationArea.call(this, this.props, {
										formId,
										dataSource,
										pageInfoClick
									})}
								</div>
							</NCAffix>
							{status == 'browse' ? (
								<div className="nc-bill-form-area borwse"> {this.createSteps()}</div>
							) : (
								''
							)}
							<div className="nc-bill-form-area">
								{createForm(formId, {
									onAfterEvent: afterEvent.bind(this),
									onBeforeEvent: formBeforeEvent.bind(this),
									pkname: pkField,
									dataSource: dataSource
								})}
							</div>
						</div>
						<div className="nc-bill-bottom-area">
							<div className="nc-bill-table-area">
								{createCardTable(tableId, {
									tableHeadLeft: this.getTableHeadLeft.bind(this),
									tableHead: this.getTableHead.bind(this),
									showIndex: true,
									showCheck: true,
									isAddRow: true,
									modelSave: modelSave.bind(this),
									selectedChange: rowSelected.bind(this),
									onAfterEvent: afterEvent.bind(this),
									onBeforeEvent: bodyBeforeEvent.bind(this)
								})}
							</div>
						</div>
					</div>
				)}
				{createModal(`${pagecode}-confirm`, { color: 'warning' })}
				{createNCUploader('warcontract-uploader', {
					billid: this.state.pk_redeploy_out,
					billtype: this.state.bill_code
				})}
				{/* 单据追溯 */}
				<BillTrack
					show={this.state.show}
					close={() => {
						this.setState({ show: false });
					}}
					pk={this.state.pk_redeploy_out} //单据id
					type={this.state.bill_type} //单据类型
				/>
				{/* 审批详情 */}
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transi_type}
					billid={this.state.pk_redeploy_out}
				/>
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('common-000002')}
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				) : (
					''
				)}
			</div>
		);
	}
}

Card = createPage({
	billinfo: {
		billtype: 'card',
		pagecode: pagecode,
		headcode: formId,
		bodycode: tableId
	}
})(Card);
export default Card;
// ReactDOM.render(<Card />, document.querySelector('#app'));
