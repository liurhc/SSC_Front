export const formId = 'card_head';

export const tableId = 'card_body_bodyvos';

export const pagecode = '452003008A_card';

export const list_pagecode = '452003008A_list';

export const appcode = '452003008A';

export const node_code = '4520028005';

export const listRouter = '/list';

export const transferRouter = '/transfer';

export const bill_type = '4A33';

export const fileName = '452003008A-000005';
//转单下游相关信息
export const TRANSFERINFO = {
	pagecode: '452003008A_transfercard',
	appid: '0001Z9100000000014WY',
	leftarea: 'leftarea',
	formcode: 'head',
	tablecode: 'bodyvos',
	srctype: 'transfer'
};

/**
 * 编辑态按钮
 */
export const editButtons = [ 'Save', 'SaveCommit', 'GetFinanceData', 'Cancel', 'AddLine', 'DelLine', 'BatchAlter' ];

/**
 * 浏览态按钮
 */
export const browseButtons = [
	'Add',
	'Edit',
	'Delete',
	'Commit',
	'Attachment',
	'Print',
	'QueryAbout',
	'UnCommit',
	'Refresh'
];

export const clearField = [
	'pk_usedunit_out_v',
	'pk_usedunit_out',
	'pk_mandept_before_v',
	'pk_mandept_before',
	'pk_icorg_out_v',
	'pk_icorg_out',
	'pk_faorg_out_v',
	'pk_faorg_out'
];

export const printFuncode = '4520028005';

export const printNodekey = null;

export const pkField = 'pk_redeploy_out';

export const dataSource = 'aum.deploy.out.main';

export const transferDataSource = 'aum.deploy.deploytransfer.source';

export const reqUrl = {
	addUrl: '/nccloud/aum/deploy/save.do',
	updateUrl: '/nccloud/aum/deploy/update.do',
	deleteUrl: '/nccloud/aum/deploy/delete.do',
	editUrl: '/nccloud/aum/deploy/out_edit.do',
	queryUrl: '/nccloud/aum/deploy/querycard.do',
	equipTreeLinkUrl: '/nccloud/aum/deploy/queryequipCard.do',
	scriptURL: '/nccloud/aum/deploy/changevo_srcids.do',
	commiyUrl: '/nccloud/aum/deploy/commit.do',
	printCardUrl: '/nccloud/aum/deploy/out_printcard.do',
	getFianceDataUrl: '/nccloud/aum/deploy/getFianceData.do',
	list_url: '/aum/deploy/out/list/index.html',
	batchUrl: '/nccloud/aum/deploy/batchalter.do'
};

// 财务数据字段
export const fiMoneyFields = [
	'origin_value',
	'accu_dep',
	'new_value',
	'net_money',
	'pre_devaluate',
	'dep_amount',
	'salvage'
];
