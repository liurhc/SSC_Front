import { formId, tableId } from '../const';
import { toast } from 'nc-lightapp-front';
import ampub from 'ampub';
const { utils, commonConst } = ampub;
const { multiLangUtils, msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { CommonKeys } = commonConst;
const { IBusiRoleConst } = CommonKeys;
const { showMessage } = msgUtils;

export default function bodyBeforeEvent(props, moduleId, key, value, index, record) {
	let editFlag = true;
	editFlag = isEdit.call(this, props, moduleId, key, value, index, record);
	return editFlag;
}
//设备有值，物料编码、设备名称、设备类别、规格、型号、数量不允许编辑
function isEdit(props, moduleId, key, value, index, record) {
	if (key == 'pk_equip') {
		let pk_usedorg_out = props.form.getFormItemsValue(formId, 'pk_usedorg_out').value; // 调出使用管理组织
		if (!pk_usedorg_out) {
			// toast({ content: '请先录入调出使用管理组织', color: 'warning' });
			toast({ content: getMultiLangByID('452003012A-000002'), color: 'warning' });
			/* 国际化处理： 当前环境并不支持SVG*/
			return false;
		}
	} else if (
		key == 'equip_name' ||
		key == 'pk_category' ||
		key == 'pk_material_v' ||
		key == 'model' ||
		key == 'spec' ||
		key == 'quantity'
	) {
		let pk_equip = props.cardTable.getValByKeyAndIndex(moduleId, index, 'pk_equip').value;
		if (pk_equip) {
			return false;
		}
	}
	return true;
}
