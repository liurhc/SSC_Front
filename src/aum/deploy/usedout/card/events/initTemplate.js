import * as CONSTANTS from '../const';
import tableButtonClick from './tableButtonClick';
import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { multiLangUtils, cardUtils, transferUtils } = utils;
const { queryAboutUtils, refInit, LoginContext } = components;
const { openEquipCardByPk } = queryAboutUtils;
const { defRefCondition } = refInit;
const { afterModifyCardMeta } = cardUtils;
const { getMultiLangByID } = multiLangUtils;
const { loginContext, getContext, loginContextKeys } = LoginContext;
const { conactAddButton } = transferUtils;
const { CommonKeys, StatusUtils } = commonConst;
const { IBusiRoleConst, TransferConst, linkQueryConst } = CommonKeys;
const { UISTATE } = StatusUtils;

import { setDefaultValue, getDataByPk } from './buttonClick';
import afterEvent from './afterEvent';

let { formId, tableId, pagecode, TRANSFERINFO, billtype } = CONSTANTS;
export default function(props) {
	let _this = this;
	let pageId = pagecode;
	let status = props.getUrlParam('status');
	//来源是转单的加载不同模板props.getUrlParam(TansferConst.srctype);
	if (TRANSFERINFO.srctype == props.getUrlParam(TransferConst.srctype)) {
		pageId = TRANSFERINFO.pagecode;
		//转单下游界面的卡片区域编码必须与原卡片界面一致
		//formId = TRANSFERINFO.formcode;
		//tableId = TRANSFERINFO.tablecode
		//appId = TRANSFERINFO.appid;
	}
	props.createUIDom(
		{
			pagecode: pageId //页面id
		},
		function(data) {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button, () => {
						let status = props.getUrlParam('status');
						if (TRANSFERINFO.srctype != props.getUrlParam(TransferConst.srctype)) {
							//不是转单下游时需要处理拉单按钮
							let transi_type = getContext(loginContextKeys.transtype);
							let bill_type = billtype;
							let scene = props.getUrlParam(linkQueryConst.SCENE);
							if (scene != linkQueryConst.SCENETYPE.approvesce) {
								conactAddButton(props, bill_type, transi_type, formId);
							}
							// if (status == UISTATE.edit || status == UISTATE.add) {
							// 	props.button.setButtonVisible(ButtonConst.AddTransferGroup, false);
							// }
							// let scene = props.getUrlParam(linkQueryConst.SCENE);
							// if (scene == linkQueryConst.SCENETYPE.approvesce) {
							// 	props.button.setButtonVisible(ButtonConst.AddTransferGroup, false);
							// }
						} else {
							//设置组织不可编辑
							props.form.setFormItemsDisabled(formId, { pk_org: false, pk_org_v: false });
						}
					});

					let editButtons = [
						'SaveGroup',
						'Cancel',
						'LineGroup',
						'BatchAlter',
						'OpenCard',
						'GetFinanceData'
					];
					let borwseButtons = [ 'EditGroup', 'Commit', 'UnCommit', 'Attachment', 'OpenCard' ];
					//按钮的显示状态
					if (status == 'edit' || status == 'add') {
						props.button.setButtonVisible(borwseButtons, false);
						props.button.setButtonVisible(editButtons, true);
					} else {
						props.button.setButtonVisible(editButtons, false);
						props.button.setButtonVisible(borwseButtons, true);
					}
				}
				if (data.template) {
					let meta = data.template;
					modifierMeta(props, meta, _this);
					props.meta.setMeta(meta, () => {
						loadData.call(this, props, _this);
						if (status == UISTATE.add) {
							setDefaultValue.call(this, props); //新增初始化默认值
							let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
							if (pk_org_v.value) {
								props.cardTable.addRow(tableId, undefined, undefined, false);
							} else {
								props.initMetaByPkorg('pk_org_v');
								props.button.setButtonDisabled([ 'AddLine' ], true);
								// props.button.setButtonDisabled(['Save','SaveCommit'],true);
							}
							props.button.setButtonDisabled([ 'DelLine' ], true);
							afterEvent.call(_this, props, formId, 'pk_recorder');
						} else if (status == UISTATE.edit) {
							props.form.setFormItemsDisabled(formId, { pk_org_v: true });
						}
					});
				}
			}
		}
	);
}
function modifierMeta(props, meta, _this) {
	let status = props.getUrlParam('status');
	let transi_type = getContext(loginContextKeys.transtype);
	meta[formId].status = status;
	meta[tableId].status = status;

	meta[tableId].items = meta[tableId].items.map((item, key) => {
		item.width = 150;
		return item;
	});

	let porCol = {
		attrcode: 'opr',
		label: getMultiLangByID('amcommon-000000'),
		visible: true,
		itemtype: 'customer',
		className: 'table-opr',
		width: 130,
		fixed: 'right',
		render(text, record, index) {
			let status = props.cardTable.getStatus(tableId);
			let buttonAry = status === 'browse' ? [ 'OpenCard' ] : [ 'OpenCard', 'DelLine' ];
			return props.button.createOprationButton(buttonAry, {
				area: 'card_body_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => {
					tableButtonClick.call(this, props, key, text, record, index);
				}
			});
		}
	};
	meta[tableId].items.push(porCol);
	//表头参照
	let pk_group = getContext(loginContextKeys.pk_group); //集团供自定义项参照试用
	meta[formId].items.map((item) => {
		//资产组织
		if (item.attrcode == 'pk_org_v') {
			item.queryCondition = () => {
				return { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgVidRefFilter' };
			};
		} else if (item.attrcode == 'pk_recorder') {
			//经办人
			item.queryCondition = () => {
				let pk_org = props.form.getFormItemsValue(formId, 'pk_org');
				if (pk_org) {
					pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
				}
				return {
					pk_org: pk_org,
					busifuncode: IBusiRoleConst.ASSETORG
				};
			};
		} else {
			//表头自定义项参照处理

			defRefCondition.call(this, props, item, formId, pk_group);
		}
	});
	//表体参照
	meta[tableId].items.map((item) => {
		//设备编码
		if (item.attrcode == 'pk_equip') {
			item.isMultiSelectedEnabled = true;
			item.renderStatus = UISTATE.browse;
			item.render = (text, record, index) => {
				return (
					<div class="card-table-browse">
						<span
							className="code-detail-link"
							onClick={() => {
								openEquipCardByPk.call(this, props, record.values[item.attrcode].value);
							}}
						>
							{record.values[item.attrcode] && record.values[item.attrcode].display}
						</span>
					</div>
				);
			};
			item.queryCondition = () => {
				let pk_org = props.form.getFormItemsValue(formId, 'pk_org'); // 资产组织
				let pk_usedorg_out = props.form.getFormItemsValue(formId, 'pk_usedorg_out'); //调出货主
				if (pk_org) {
					pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
				}
				if (pk_usedorg_out) {
					pk_usedorg_out = props.form.getFormItemsValue(formId, 'pk_usedorg_out').value;
				}
				return {
					pk_org: pk_org, //主组织
					pk_usedorg_out,
					bill_type: billtype,
					transi_type: transi_type,
					GridRefActionExt: 'nccloud.web.aum.deploy.usedout.refcondition.PK_EQUIPSqlBuilder'
				};
			};
		} else {
			//表体自定义项参照处理

			defRefCondition.call(this, props, item, formId, pk_group);
		}
	});

	let config = { tableId };
	meta = afterModifyCardMeta.call(this, props, meta, config);
	return meta;
}

function loadData(props, _this) {
	let status = props.getUrlParam('status');
	if (status == UISTATE.add) {
	} else if (status == UISTATE.browse || status == UISTATE.edit) {
		let pk = props.getUrlParam('id');
		if (pk) {
			getDataByPk.call(_this, props, pk);
		}
	} else if (status == UISTATE.transferAdd) {
		//转单
		_this.getDataBySrcId(props);
	} else if (status == 'equipAdd') {
		let pk = props.getUrlParam('id');
		if (pk) {
			_this.getDataByEquipId(pk);
		}
	}
	_this.toggleShow(status);
}
