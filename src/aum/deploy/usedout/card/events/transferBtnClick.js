import { ajax, toast, cardCache } from 'nc-lightapp-front';
import { TRANSFERINFO, pkField, reqUrl, pagecode, formId, tableId, dataSource } from '../const';
import { linkToList, linkToTransfer } from './buttonClick';

import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { saveValidatorsUtil } = components;
const { beforeSaveValidator } = saveValidatorsUtil;
const { cardUtils, msgUtils } = utils;
const { setCardValue } = cardUtils;
const { StatusUtils } = commonConst;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { UISTATE } = StatusUtils;

let headpk = 'pk_redeploy_out';
let bodypk = 'pk_redeploy_out_b';
let transferheadpk = 'pk_bill_src'; //来源单据表头主键
let transferbodypk = 'pk_bill_b_src'; //来源单据表体主键

//转单按钮事件处理
export default function(props, id) {
	//保存
	if (id === 'Save') {
		//表头必输项校验
		let pass = beforeSaveValidator.call(this, props, formId, tableId, []);
		if (!pass) {
			return;
		}
		this.props.cardTable.filterEmptyRows(this.tableId);
		// let cardData = this.props.createMasterChildData(this.pageid, this.formId, this.tableId);
		let cardData = this.props.createMasterChildDataSimple(this.pageid, this.formId, this.tableId);
		let headpkvalue = props.form.getFormItemsValue(this.formId, headpk).value; //获取表头主键
		let url = '/nccloud/aum/usedout/save.do'; //新增保存
		if (headpkvalue) {
			url = '/nccloud/aum/usedout/update.do'; //修改保存
		}
		//调用Ajax保存数据
		ajax({
			url: url,
			data: cardData,
			success: (res) => {
				let pk_borrow = null;
				let bill_code = '';
				if (res.success) {
					if (res.data) {
						// this.putHeadFormValue(res.data.head[this.formId]);
						// this.putTableValue(res.data.body[this.tableId]);
						setCardValue.call(this, props, res.data, formId, tableId);
						this.toggleShow(UISTATE.transferBorwse);
						let step = res.data.head[formId].rows[0].values.step.value;
						let pk_redeploy_out = res.data.head[formId].rows[0].values[pkField].value;
						this.setState({ step, pk_redeploy_out });
						this.synTransferData();
						//判断新增保存还是修改保存，新增增加缓存，修改是更新缓存
						let headpkvalue = props.form.getFormItemsValue(this.formId, headpk).value; //获取表头主键
						this.setState({ pk_redeploy_out: headpkvalue });
						let cardData = this.props.createMasterChildData(this.pageid, this.formId, this.tableId);
						if (url.indexOf('save.do') >= 0) {
							cardCache.addCache(headpkvalue, cardData, this.formId, dataSource);
						} else {
							if (this.transferEditId.indexOf(headpkvalue) != -1) {
								//移除缓存数组中记录保存的id
								let index = this.transferEditId.indexOf(headpkvalue);
								this.transferEditId.splice(index, 1);
							}
							cardCache.updateCache(pkField, headpkvalue, cardData, this.formId, dataSource);
						}
						let bodypkvalue = props.cardTable.getColValue(this.tableId, transferbodypk); //获取表体数据
						bodypkvalue = bodypkvalue.map((pk) => {
							return pk.value;
						});
						this.props.transferTable.setSavedTransferTableDataPk(bodypkvalue);
						//转单缓存处理完成后加载下一条数据
						props.transferTable.setTransformFormStatus(TRANSFERINFO.leftarea, {
							status: true,
							childId: 'pk_redeploy_out_b',
							onChange: (current, next, currentIndex) => {
								//props.transferTable.setTransferListValueByIndex(TRANSFERINFO.leftarea,next,currentIndex);
								this.transferIndex = currentIndex + 1;
								// this.toggleShow(UISTATE.transferBorwse);
								//保存成功提示并设置页面跳转状态
								// toast({ content: '保存成功', color: 'success' });
								let config = {
									type: 'SaveSuccess'
								};
								showMessage.call(this, props, config);
							}
						});
					}
				}
			}
		});
	} else if (id === 'Cancel') {
		//取消
		// props.ncmodal.show(`${pagecode}-confirm`, {
		// 	title: '取消',
		// 	content: '确定要取消吗？',
		// 	beSureBtnClick: () => {
		let cancelClick = () => {
			let headpkvalue = props.form.getFormItemsValue(this.formId, headpk).value;
			if (headpkvalue && this.transferEditId.indexOf(headpkvalue)) {
				//转单已处理后又点保存取消
				this.toggleShow(UISTATE.transferBorwse);
				this.synTransferData(); //同步取消后的数据到转单界面
			} else {
				let count = props.transferTable.getTransformFormAmount(TRANSFERINFO.leftarea); //转单数据的数量
				if (count == 1) {
					linkToTransfer.call(this, props);
				} else {
					props.transferTable.setTransformFormStatus(TRANSFERINFO.leftarea, {
						status: false,
						childId: bodypk,
						onChange: (current, next) => {}
					});
				}
			}
		};
		showConfirm.call(this, props, { type: MsgConst.Type.Cancel, beSureBtnClick: cancelClick });
		// 	}
		// });
	} else if (id === 'Delete') {
		// props.ncmodal.show(`${pagecode}-confirm`, {
		// 	title: '删除',
		// 	content: '确定要删除所选数据吗？',
		// 	beSureBtnClick: () => {
		let delConfirm = () => {
			let paramInfoMap = {};
			let pk = this.props.form.getFormItemsValue(formId, pkField).value;
			// let ts = this.props.form.getFormItemsValue(formId, 'ts').value
			paramInfoMap[pk] = this.props.form.getFormItemsValue(this.formId, 'ts').value;
			ajax({
				url: reqUrl.deleteUrl,
				data: {
					paramInfoMap: paramInfoMap,
					dataType: 'listData',
					OperatorType: 'DELETE',
					pageid: pagecode
				},
				success: (res) => {
					let { success, data } = res;
					if (success) {
						if (data.success === 'allsuccess') {
							// 清除缓存
							cardCache.deleteCacheById(pkField, pk, dataSource);
							toast({ content: data.successMsg, color: 'success' });
							// 加载下一条左侧数据，删除已删除数据左侧区域
							let count = props.transferTable.getTransformFormAmount(TRANSFERINFO.leftarea); //转单数据的数量
							if (count == 1) {
								linkToTransfer.call(this, props);
							} else {
								props.transferTable.setTransformFormStatus(TRANSFERINFO.leftarea, {
									status: false,
									childId: bodypk,
									onChange: (current, next) => {
										// toast({ color: 'success', content: '删除成功' });
										let config = {
											type: 'DeleteSuccess'
										};
										showMessage.call(this, props, config);
									}
								});
							}
						} else {
							toast({ content: data.errorMsg, color: 'danger' });
						}
					}
				}
			});
		};
		showConfirm.call(this, props, { type: MsgConst.Type.Delete, beSureBtnClick: delConfirm });
		// })
	} else if (id === 'Quit') {
		//判断当前单据是否存在没有处理的
		let count = props.transferTable.getTransformFormAmount(TRANSFERINFO.leftarea); //转单数据的数量
		let status = true;
		for (let index = 0; index < count; index++) {
			status = props.transferTable.getTransformFormStatus(TRANSFERINFO.leftarea, index);
			if (!status) {
				break;
			}
		}
		if (!status) {
			// props.ncmodal.show(`${pagecode}-confirm`, {
			// 	title: '注意',
			// 	content: '当前存在未处理完成的单据，是否放弃转单？',
			// 	beSureBtnClick: () => {
			let quitConfirm = () => {
				linkToList.call(this, props);
			};
			showConfirm.call(this, props, { type: MsgConst.Type.Quit, beSureBtnClick: quitConfirm });
			// })
		} else {
			linkToList.call(this, props);
		}
	} else if (id == 'Edit') {
		let headpkvalue = props.form.getFormItemsValue(this.formId, headpk).value;
		if (this.transferEditId.indexOf(headpkvalue) == -1) {
			this.transferEditId.push(headpkvalue);
		}
		let data = {
			status: UISTATE.transferEdit
		};
		this.props.setUrlParam(data);
		this.toggleShow(UISTATE.transferEdit);
	} else {
		this.selfButtonClick(props, id);
	}
}
