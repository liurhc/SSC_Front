import buttonClick from './buttonClick';
import initTemplate from './initTemplate';
import afterEvent from './afterEvent';
import pageInfoClick from './pageInfoClick';
import transferBtnClick from './transferBtnClick';
import rowSelected from './rowSelected';
import bodyBeforeEvent from './bodyBeforeEvent';


export { buttonClick, afterEvent,initTemplate,pageInfoClick,transferBtnClick,rowSelected,bodyBeforeEvent};
