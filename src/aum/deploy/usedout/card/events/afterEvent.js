import { ajax } from 'nc-lightapp-front';
import * as CONSTANTS from '../const';

import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils, cardUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { commonHeadAfterEvent, commonBodyAfterEvent, updateHeadData, updateBodyData } = cardUtils;
const { OrgChangeEvent } = components;

const { orgChangeEvent } = OrgChangeEvent;

const { formId, tableId, pagecode, TRANSFERINFO, fiMoneyFields } = CONSTANTS;
const headurl = '/nccloud/aum/usedout/headafterevent.do';
const bodyurl = '/nccloud/aum/usedout/bodyafterevent.do';

/**
 * 编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedRows 
 * @param {*} index 
 * @param {*} record 
 * @param {*} type 
 * @param {*} eventType 
 */
export default function(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	if (moduleId == formId) {
		headAfterEvent.call(this, props, moduleId, key, value, changedRows, index, record, type, eventType);
	} else if (moduleId == tableId) {
		bodyAfterEvent.call(this, props, moduleId, key, value, changedRows, index, record, type, eventType);
	}
	//来源是转单的操作需要同步右边数据到左边
	if (this.srctype == TRANSFERINFO.srctype) {
		this.synTransferData();
	}
}

/**
 * 表头编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} oldValue 
 * @param {*} val 
 */
function headAfterEvent(props, moduleId, key, value, oldValue, val) {
	// let data = props.createHeadAfterEventData(pagecode, formId, tableId, moduleId, key, value);
	switch (key) {
		case 'pk_org_v':
			if (value) {
				let callback = () => {
					// data = props.createHeadAfterEventData(pagecode, formId, tableId, moduleId, key, value);
					// ajax({
					// 	url: '/nccloud/aum/usedout/headafterevent.do',
					// 	data,
					// 	success: (res) => {
					// 		if (res.data && res.data.head) {
					// 			props.form.setAllFormValue({ [formId]: res.data.head[formId] });
					if (value.value) {
						this.props.cardTable.addRow(tableId, undefined, undefined, false);
					}
					// 		}
					// 	}
					// });
				};
				let config = {
					afterEditUrl: headurl,
					pagecode,
					key,
					value,
					oldValue
				};
				commonHeadAfterEvent.call(this, props, config);
				let fields = [ 'pk_recorder', 'memo' ];
				let tableButtons = [ 'AddLine', 'BatchAlter' ];
				orgChangeEvent.call(
					this,
					props,
					pagecode,
					formId,
					tableId,
					key,
					value,
					oldValue,
					fields,
					tableButtons,
					callback
				);
			}
			break;
		case 'pk_usedorg_in_v':
			if (value) {
				//调入使用管理组织编辑后需要给表体赋值
				let data = props.createHeadAfterEventData(pagecode, formId, tableId, moduleId, key, value);
				ajax({
					url: headurl,
					data,
					success: (res) => {
						if (res.data && res.data.body) {
							res.data.body[tableId].rows.forEach((row) => {
								fiMoneyFields.forEach((field) => {
									delete row.values[field];
								});
							});
							this.props.cardTable.updateDataByRowId(tableId, res.data.body[tableId], false);
						}
					}
				});
			}
			break;
		case 'pk_recorder':
			// let data = props.createHeadAfterEventData(pagecode, formId, tableId, moduleId, key, value);
			// ajax({
			// 	url: '/nccloud/aum/usedout/headafterevent.do',
			// 	data,
			// 	success: (res) => {
			// 		if (res.data && res.data.head) {
			// 			props.form.setAllFormValue({ [formId]: res.data.head[formId] });
			// 		}
			// 	}
			// });
			let config = {
				afterEditUrl: headurl,
				pagecode,
				key,
				value,
				oldValue
			};
			commonHeadAfterEvent.call(this, props, config);
			break;
		default:
			break;
	}
}

/**
 * 表体编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedRows 
 * @param {*} index 
 * @param {*} record 
 * @param {*} type 
 * @param {*} eventType 
 */
function bodyAfterEvent(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	if (key === 'pk_equip') {
		if ((value instanceof Array && value.length > 0) || value.refcode) {
			// let data = props.createBodyAfterEventData(pagecode, formId, tableId, moduleId, key, changedRows, index);
			// ajax({
			// 	url: '/nccloud/aum/usedout/bodyafterevent.do',
			// 	data,
			// 	success: (res) => {
			// 		if (res.data && res.data.body) {
			// 			this.props.cardTable.setTableData(tableId, res.data.body[tableId]);
			// 		}
			// 	}
			// });
			let config = {
				afterEditUrl: bodyurl,
				pagecode,
				moduleId,
				tableId,
				key,
				record,
				changedRows,
				index
			};
			commonBodyAfterEvent.call(this, props, config);
		} else {
			// 清空联动项
			let meta = props.meta.getMeta();
			meta[tableId].items.map((item) => {
				let clearfield = [
					'pk_category',
					'pk_usedunit_out_v',
					'pk_usedunit_out',
					'pk_mandept_before_v',
					'pk_mandept_before',
					'pk_icorg_out_v',
					'pk_icorg_out',
					'pk_faorg_out_v',
					'pk_faorg_out'
				];
				if (item.attrcode.startsWith('equip') || clearfield.indexOf(item.attrcode) != -1) {
					props.cardTable.setValByKeyAndIndex(tableId, index, item.attrcode, { value: null, display: null });
				}
			});
		}
	}
}
