import * as CONSTANTS from '../const';
import {setBatchAlterDisabled} from './buttonClick';
const {pagecode,formId,tableId} = CONSTANTS;

export default function tableButtonClick(props, key, text, record, index) {
    switch (key) {
        // 表格操作按钮
        case 'OpenCard':
            let status = props.cardTable.getStatus(tableId);
            if (status == 'edit') {
                props.cardTable.setClickRowIndex(tableId, { record, index });
                props.cardTable.openModel(tableId, 'edit', record, index);
            } else if (status == 'browse') {
                props.cardTable.toggleRowView(tableId, record)
            }
            break;
        case 'DelLine':
            props.cardTable.delRowsByIndex(tableId, index);
            setBatchAlterDisabled.call(this,props);
            break;
        default:
            break;
    }

}