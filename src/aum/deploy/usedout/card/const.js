export const formId = 'card_head';

export const searchId = 'searchArea';

export const tableId = 'card_body_bodyvos';

export const pagecode = '452003012A_card';

export const list_pagecode = '452003012A_list';

export const card_pagecode = '452003012A_card';

export const appid = '0001Z9100000000014YE';

export const appcode = '452003012A';

export const node_code = '4520028010';

export const dataSource = 'aum.deploy.usedout.main';

export const transferDataSource = 'aum.deploy.deploytransfer.source';

export const listRouter = '/list';

export const transferRouter = '/transfer';

export const pkField = 'pk_redeploy_out';

export const fileName = '452003012A-000001';

//转单下游相关信息
export const TRANSFERINFO = {
	pagecode: '452003012A_transfercard',
	appid: '0001Z9100000000014YE',
	leftarea: 'leftarea',
	formcode: 'head',
	tablecode: 'bodyvos',
	srctype: 'transfer'
};

export const billtype = '4A47';

export const reqUrl = {
	getFianceDataUrl: '/nccloud/aum/usedout/getFianceData.do',
	printCardUrl: '/nccloud/aum/usedout/out_printcard.do',
	deleteUrl: '/nccloud/aum/usedout/delete.do',
	editUrl: '/nccloud/aum/usedout/out_edit.do',
	list_url: '/aum/deploy/usedout/list/index.html',
	commitUrl: '/nccloud/aum/usedout/commitAction.do',
	queryUrl: '/nccloud/aum/usedout/querycard.do',
	addUrl: '/nccloud/aum/usedout/save.do',
	updateUrl: '/nccloud/aum/usedout/update.do',
	batchUrl: '/nccloud/aum/usedout/batchalter.do'
};

// 财务数据字段
export const fiMoneyFields = [
	'origin_value',
	'accu_dep',
	'new_value',
	'net_money',
	'pre_devaluate',
	'dep_amount',
	'salvage'
];
