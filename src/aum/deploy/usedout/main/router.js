import { asyncComponent } from 'nc-lightapp-front';

const List = asyncComponent(() => import(/* webpackChunkName: "aum/deploy/usedout/list/list" */ /* webpackMode: "eager" */ '../list'));
const Card = asyncComponent(() => import(/* webpackChunkName: "aum/deploy/usedout/card/card" */ /* webpackMode: "eager" */ '../card'));
const Source = asyncComponent(() => import(/* webpackChunkName: "aum/deploy/deploytransfer/source/source" */ /* webpackMode: "eager" */ '../../deploytransfer/source'));

const routes = [
	{
		path: '/',
		component: List,
		exact: true
	},
	{
		path: '/list',
		component: List
	},
	{
		path: '/card',
		component: Card
	},
	{
		path: '/transfer',
		component: Source
	}
];

export default routes;
