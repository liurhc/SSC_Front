//主子表列表

import React, { Component } from 'react';
import { createPage, high, base, createPageIcon } from 'nc-lightapp-front';
import {
	buttonClick,
	initTemplate,
	searchBtnClick,
	pageInfoClick,
	doubleClick,
	rowSelected,
	afterEvent
} from './events';
import { tableId, searchId, pagecode, dataSource, pkField, bill_type } from './const';
import { commit, setBatchBtnsEnable } from '../list/events/buttonClick';
import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { ApprovalTrans } = components;
const { NCAffix } = base;

class List extends Component {
	constructor(props) {
		super(props);
		this.state = {
			bill_type: bill_type,
			transi_type: '',
			show: false,
			showApprove: false,
			pk_redeploy_out: '',
			compositedisplay: false,
			compositedata: {}
		};
		initTemplate.call(this, props);
	}

	//指派方法
	getAssginUsedr = (content) => {
		commit.call(this, this.props, 'SAVE', content); //原提交方法添加参数content
	};
	//取消 指派
	turnOff = () => {
		this.props.table.selectAllRows(tableId, false);
		rowSelected.call(this, this.props, tableId);
		this.setState({
			compositedisplay: false
		});
	};

	render() {
		let { table, button, search, ncmodal, ncUploader } = this.props;
		let { createNCUploader } = ncUploader;
		let { createModal } = ncmodal;
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { createButtonApp } = button;
		const { ApproveDetail, BillTrack } = high;
		return (
			<div className="nc-bill-list">
				<NCAffix>
					<div className="nc-bill-header-area">
						<div className="header-title-search-area">
							{createPageIcon()}
							<h2 className="title-search-detail">{getMultiLangByID('452003012A-000001')}</h2>
						</div>
						<div className="header-button-area">
							{createButtonApp({
								area: 'list_head',
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
						</div>
					</div>
				</NCAffix>
				<div className="nc-bill-search-area">
					{NCCreateSearch(searchId, {
						clickSearchBtn: searchBtnClick.bind(this),
						dataSource: dataSource,
						onAfterEvent: afterEvent.bind(this)
					})}
				</div>
				<div className="nc-bill-table-area">
					{createSimpleTable(tableId, {
						handlePageInfoChange: pageInfoClick.bind(this),
						showIndex: true,
						showCheck: true,
						onRowDoubleClick: doubleClick.bind(this),
						onSelected: rowSelected.bind(this),
						onSelectedAll: rowSelected.bind(this),
						pkname: pkField,
						dataSource: dataSource,
						componentInitFinished: () => {
							//缓存数据赋值成功的钩子函数
							//若初始化数据后需要对数据做修改，可以在这里处理
							setBatchBtnsEnable.call(this, this.props, tableId);
						}
					})}
				</div>
				{createNCUploader('warcontract-uploader', {})}
				{createModal(`${pagecode}-confirm`, { color: 'warning' })}
				{/* 审批详情 */}
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transi_type}
					billid={this.state.pk_redeploy_out}
				/>
				{/* 单据追溯 */}
				<BillTrack
					show={this.state.show}
					close={() => {
						this.setState({ show: false });
					}}
					pk={this.state.pk_redeploy_out} //单据id
					type={this.state.bill_type} //单据类型
				/>
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002')}
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				) : (
					''
				)}
			</div>
		);
	}
}

List = createPage({})(List);
export default List;
// ReactDOM.render(<List />, document.querySelector('#app'));
