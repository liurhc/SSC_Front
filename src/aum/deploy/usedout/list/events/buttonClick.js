import {
	tableId,
	formId,
	bill_type,
	pagecode,
	dataSource,
	fileName,
	pkField,
	card_pagecode,
	reqUrl,
	cardRouter,
	transferRouter,
	transferDataSource
} from '../const';
import { ajax, toast, print, output } from 'nc-lightapp-front';
import { searchBtnClick } from './index';

import ampub from 'ampub';
const { utils, components } = ampub;
const { ScriptReturnUtils, queryAboutUtils, LoginContext } = components;
const { getScriptListReturn } = ScriptReturnUtils;
const { openListBillTrack } = queryAboutUtils;
const { multiLangUtils, msgUtils, listUtils, transferUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { setBillFlowBtnsEnable, batchRefresh } = listUtils;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { pushToTransferPage } = transferUtils;
const { getContext, loginContextKeys } = LoginContext;

export default function buttonClick(props, id) {
	switch (id) {
		case 'Add':
			add.call(this, props);
			break;
		case 'Refresh':
			refreshAction.call(this, props);
			break;
		case 'Delete':
			deleteAction.call(this, props);
			break;
		case 'Commit':
			commit.call(this, props, 'SAVE');
			break;
		case 'UnCommit':
			commit.call(this, props, 'UNSAVE');
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'QueryAboutBusiness':
			openListBillTrack(this, props, tableId, pkField);
			break;
		case 'Attachment':
			AttachmentClick.call(this, props);
			break;
		default:
			//默认是动态加载的拉单按钮，拉单按钮的code为“单据类型+交易类型”
			let srcinfo = id.split('+');
			//清掉上游缓存(退出转单和从上游返回下游然后再到上游的时候)
			props.transferTable.deleteCache(transferDataSource);
			pushToTransferPage.call(
				this,
				transferRouter, //上游查询页面的路由
				srcinfo[0], // 上游的bill_type
				srcinfo[1], // 上游的transtype
				bill_type, // 下游的bill_type
				getContext(loginContextKeys.transtype), // 下游的transtype
				'452003008A_transfercard' // 下游转单页面的pagecode
			);
			break;
	}
}

/**
 * 新增
 */

export function add(props) {
	linkToCard.call(this, props, undefined, 'add');
}

/**
 * 跳转卡片
 * @param {*} props 
 * @param {*} record 
 */
export function linkToCard(props, record = {}, status = 'browse') {
	props.pushTo(cardRouter, {
		pagecode: card_pagecode,
		status,
		id: record[pkField] ? record[pkField].value : ''
	});
}

/**
 * 删除按钮
 * @param {*} props 
 */
function deleteAction(props) {
	let checkedRows = props.table.getCheckedRows(tableId);
	if (checkedRows.length == 0) {
		// toast({ content: '请先选中需要删除的数据', color: 'warning' });
		let config = {
			type: ChooseDelete
		};
		showMessage.call(this, props, config);
		return;
	}
	showConfirm.call(this, props, { type: MsgConst.Type.DelSelect, beSureBtnClick: batchDel });
	// props.ncmodal.show(`${pagecode}-confirm`, {
	// 	title: '提示',
	// 	content: '是否确认要删除？',
	// 	beSureBtnClick: () => {
	// 		batchDel.call(this, props, checkedRows);
	// 	}
	// });
}

/**
 * 批量删除
 */
function batchDel(props) {
	let data = props.table.getCheckedRows(tableId);
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values.pk_redeploy_out.value;
		let ts = v.data.values.ts.value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});

	ajax({
		url: reqUrl.deleteUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: card_pagecode
		},
		success: (res) => {
			getScriptListReturn(params, res, props, 'pk_redeploy_out', formId, tableId, true, dataSource);
		}
	});
}

function refreshAction(props) {
	batchRefresh.call(this, props, searchBtnClick);
}

/**
 * 提交
 * @param {*} props 
 */
export function commit(props, OperatorType, content) {
	let data = props.table.getCheckedRows(tableId);
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values[pkField].value;
		let ts = v.data.values.ts.value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	let commitType = '';
	if (OperatorType === 'SAVE') {
		//提交传这个
		commitType = 'commit';
	}
	ajax({
		url: reqUrl.commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: OperatorType,
			pageid: card_pagecode,
			commitType: commitType, //提交
			content: content
		},
		success: (res) => {
			if (content) {
				this.setState({
					compositedisplay: false
				});
			}
			getScriptListReturn.call(this, params, res, props, pkField, formId, tableId, false, dataSource);
		}
	});
}

/**
 * 打印
 * @param {*} props 
 */
function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	// let { pageConfig } = props;
	if (!printData) {
		// toast({ content: '请选择需要打印的数据', color: 'warning' });
		let config = {
			type: ChoosePrint
		};
		showMessage.call(this, props, config);
		return;
	}

	// let pks = [pk_redeploy_out.value];
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		reqUrl.printCardUrl, // 后台打印服务url
		printData
	);
}

/**
 * 输出
 * @param {*} props 
 */
function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		// toast({ content: '请选择需要输出的数据', color: 'warning' });
		let config = {
			type: ChooseOutput
		};
		showMessage.call(this, props, config);
		return;
	}
	output({
		url: reqUrl.printCardUrl,
		data: printData
	});
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let checkedRows = props.table.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length == 0) {
		// toast({ content: '请选择需要打印的数据', color: 'warning' });
		let config = {
			type: ChoosePrint
		};
		showMessage.call(this, props, config);
		return false;
	}
	let pks = [];
	checkedRows.map((item) => {
		pks.push(item.data.values['pk_redeploy_out'].value);
	});
	let printData = {
		filename: getMultiLangByID(fileName), // 文件名称
		nodekey: '', // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

/**
 * 附件
 */

export function AttachmentClick(props) {
	let checkedrows = props.table.getCheckedRows(tableId);
	// billNo 是单据编码
	let billNo = checkedrows[0].data.values['bill_code'].value;
	// billNo 是单据主键
	let billId = checkedrows[0].data.values['pk_redeploy_out'].value;
	props.ncUploader.show('warcontract-uploader', {
		billId: 'pam/redeploy_out/' + billId,
		billNo
	});
}

/**
* 设置批量按钮是否可用
* @param {*} props 
* @param {*} moduleId 
*/
export function setBatchBtnsEnable(props, moduleId) {
	setBillFlowBtnsEnable.call(this, props, moduleId);
}
