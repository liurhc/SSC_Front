import { ajax, base, toast } from 'nc-lightapp-front';
import * as CONSTANTS from '../const';
const { tableId, card_pagecode, node_code, formId, reqUrl, dataSource } = CONSTANTS;
import { linkToCard } from './buttonClick';
import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { ScriptReturnUtils, queryAboutUtils } = components;
const { getScriptListReturn } = ScriptReturnUtils;
const { openListApprove } = queryAboutUtils;
const { StatusUtils } = commonConst;
const { UISTATE } = StatusUtils;
const { msgUtils } = utils;
const { MsgConst, showMessage } = msgUtils;
export default function tableButtonClick(props, key, text, record, index) {
	switch (key) {
		// 表格操作按钮
		case 'Edit':
			edit.call(this, props, record);
			break;
		case 'Delete':
			delRow(props, tableId, index, record);
			break;
		case 'Commit':
			commit.call(this, 'SAVE', props, tableId, index, record);
			break;
		case 'UnCommit':
			commit.call(this, 'UNSAVE', props, tableId, index, record);
			break;
		case 'QueryAboutBillFlow':
			let pk = record.pk_redeploy_out.value;
			this.setState({
				pk_redeploy_out: pk,
				showApprove: true,
				transi_type: record.transi_type.value
			});
			break;
		default:
			break;
	}
}

export function edit(props, record) {
	ajax({
		url: reqUrl.editUrl,
		data: {
			pk: record.pk_redeploy_out.value,
			resourceCode: node_code
		},
		success: (res) => {
			if (res.data) {
				toast({ color: 'warning', content: res.data });
			} else {
				linkToCard.call(this, props, record, UISTATE.edit);
			}
		}
	});
}

/**
 * 提交
 * @param {*} props 
 */
export function commit(OperatorType, props, tableId, index, record) {
	let paramInfoMap = {};
	paramInfoMap[record.pk_redeploy_out.value] = record.ts.value;
	let params = [ { id: record.pk_redeploy_out.value, index: index } ];
	ajax({
		url: reqUrl.commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: OperatorType,
			pageid: card_pagecode,
			commitType: 'commit' //提交
			//commitType : 'saveCommit' //保存提交
		},
		success: (res) => {
			getScriptListReturn.call(this, params, res, props, 'pk_redeploy_out', formId, tableId, false, dataSource);
		}
	});
}

/**
 * 直接删除数据
 * @param {*} record 
 */
const delRow = (props, tableId, index, record) => {
	let paramInfoMap = {};
	paramInfoMap[record.pk_redeploy_out.value] = record.ts.value;
	let params = [ { id: record.pk_redeploy_out.value, index: index } ];
	ajax({
		url: reqUrl.deleteUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: card_pagecode
		},
		success: (res) => {
			getScriptListReturn(params, res, props, 'pk_redeploy_out', formId, tableId, true, dataSource);
		}
	});
};
