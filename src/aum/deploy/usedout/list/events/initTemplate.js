import * as CONSTANTS from '../const';
import tableButtonClick from './tableButtonClick';
import { setBatchBtnsEnable, linkToCard } from './buttonClick';

import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { multiLangUtils, transferUtils } = utils;
const { assetOrgMultiRefFilter, refInit, LoginContext, EquipQueryCondition } = components;
const { AssetOrgMultiRefFilter } = assetOrgMultiRefFilter;
const { defRefCondition } = refInit;
const { CommonKeys, StatusUtils } = commonConst;
const { IBusiRoleConst } = CommonKeys;
const { loginContext, getContext, loginContextKeys } = LoginContext;
const { getMultiLangByID } = multiLangUtils;
const { UISTATE, BILLSTATUS } = StatusUtils;
const { initCriteriaChangedHandlers, filterByOrg } = EquipQueryCondition;
const { conactAddButton } = transferUtils;

const { tableId, pagecode, searchId, bill_type } = CONSTANTS;

export default function(props) {
	let _this = this;
	props.createUIDom(
		{
			pagecode
			//页面id
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button, () => {});
					// 行删除时悬浮框提示
					props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001'));
					let transi_type = getContext(loginContextKeys.transtype);
					conactAddButton(props, bill_type, transi_type, tableId);
				}
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta, _this);
					props.meta.setMeta(meta, () => {
						setBatchBtnsEnable.call(this, props, tableId);
					});
				}
			}
		}
	);
}

function modifierMeta(props, meta, _this) {
	meta[tableId].pagination = true;
	meta[tableId].items = meta[tableId].items.map((item, key) => {
		item.width = 145;
		if (item.attrcode == 'bill_code') {
			item.render = (text, record, index) => {
				return (
					<div class="simple-table-td" field="bill_code" fieldname={getMultiLangByID('452003012A-000000')}>
						<span
							className="code-detail-link"
							onClick={() => {
								linkToCard.call(this, props, record, UISTATE.browse);
							}}
						>
							{record.bill_code && record.bill_code.value}
						</span>
					</div>
				);
			};
		}
		return item;
	});
	//添加操作列
	let buttonCol = {
		attrcode: 'opr',
		label: getMultiLangByID('amcommon-000000'),
		className: 'table-opr',
		itemtype: 'customer',
		width: 200,
		visible: true,
		render: (text, record, index) => {
			let { createOprationButton } = props.button;
			let bill_status = record.bill_status.value;
			let buttonAry;
			if (bill_status === BILLSTATUS.free_check) {
				//自由态
				buttonAry = [ 'Commit', 'Edit', 'Delete' ];
			} else if (bill_status === BILLSTATUS.un_check) {
				//已提交
				buttonAry = [ 'UnCommit', 'QueryAboutBillFlow' ];
			} else if (bill_status === BILLSTATUS.check_going) {
				//审批中
				buttonAry = [ 'QueryAboutBillFlow' ];
			} else if (bill_status === BILLSTATUS.check_pass) {
				//审批通过
				buttonAry = [ 'UnCommit', 'QueryAboutBillFlow' ];
			} else if (bill_status === BILLSTATUS.check_nopass) {
				//审批未通过
				buttonAry = [ 'Edit', 'Delete', 'QueryAboutBillFlow' ];
			} else if (bill_status === BILLSTATUS.close) {
				//关闭
			} else {
			}
			return createOprationButton(buttonAry, {
				area: 'list_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => {
					tableButtonClick.call(_this, props, key, text, record, index);
				}
			});
		}
	};
	meta[tableId].items.push(buttonCol);
	meta[searchId].items.map((item) => {
		if (item.attrcode == 'pk_usedorg_out') {
			item.queryCondition = () => {
				return { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter' };
			};
		} else if (item.attrcode == 'pk_recorder') {
			AssetOrgMultiRefFilter.call(this, props, searchId, item, 'pk_usedorg_out');
			item.queryCondition = () => {
				let pk_org = getSearchValue.call(this, props, 'pk_usedorg_out');
				if (pk_org) {
					return {
						pk_org: pk_org,
						busifuncode: IBusiRoleConst.ASSETORG
					};
				} else {
					return {
						busifuncode: IBusiRoleConst.ASSETORG
					};
				}
			};
		} else if (item.attrcode == 'bodyvos.pk_usedept_after') {
			AssetOrgMultiRefFilter.call(this, props, searchId, item, 'bodyvos.pk_usedunit_in');
			item.queryCondition = () => {
				let pk_usedunit_in = getSearchValue.call(this, props, 'bodyvos.pk_usedunit_in');
				if (pk_usedunit_in) {
					return {
						pk_org: pk_usedunit_in,
						busifuncode: IBusiRoleConst.ASSETORG
					};
				} else {
					return {
						busifuncode: IBusiRoleConst.ASSETORG
					};
				}
			};
		} else if (item.attrcode == 'bodyvos.pk_user_after') {
			AssetOrgMultiRefFilter.call(this, props, searchId, item, 'bodyvos.pk_usedunit_in');
			item.queryCondition = () => {
				let pk_usedunit_in = getSearchValue.call(this, props, 'bodyvos.pk_usedunit_in');
				if (pk_usedunit_in) {
					return {
						pk_org: pk_usedunit_in,
						busifuncode: IBusiRoleConst.ASSETORG
					};
				} else {
					return {
						busifuncode: IBusiRoleConst.ASSETORG
					};
				}
			};
		} else if (item.attrcode == 'bodyvos.pk_warehouse_in') {
			AssetOrgMultiRefFilter.call(this, props, searchId, item, 'bodyvos.pk_icorg_in');
			item.queryCondition = () => {
				let pk_org = getSearchValue.call(this, props, 'bodyvos.pk_icorg_in');
				if (pk_org) {
					return {
						pk_org: pk_org,
						busifuncode: IBusiRoleConst.ASSETORG
					};
				} else {
					return {
						busifuncode: IBusiRoleConst.ASSETORG
					};
				}
			};
		} else {
			let bodyDefPrefix = 'bodyvos.def';
			let pk_group = getContext(loginContextKeys.pk_group); //集团供自定义项参照试用
			defRefCondition.call(this, props, item, searchId, pk_group, true, 'pk_org', bodyDefPrefix);
			defRefCondition.call(this, props, item, searchId, pk_group, true);
		}
	});

	initCriteriaChangedHandlers.call(this, props, meta, searchId, 'bodyvos.pk_equip');
	filterByOrg.call(
		this,
		props,
		meta,
		searchId,
		[ 'bodyvos.pk_equip.pk_mandept', 'bodyvos.pk_equip.pk_user', 'bodyvos.pk_equip.pk_manager' ],
		'pk_org',
		true
	);
	return meta;
}

//获取查询条件的值
function getSearchValue(props, field) {
	let data = props.search.getSearchValByField(searchId, field);
	if (data && data.value && data.value.firstvalue) {
		let firstvalue = data.value.firstvalue;
		if (firstvalue.split(',').length == 1) {
			return firstvalue;
		}
	}
}
