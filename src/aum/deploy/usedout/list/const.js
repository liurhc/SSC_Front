//查询区编码
export const searchId = 'searchArea';

//列表表体区编码
export const tableId = 'list_head';

//卡片表体区域编码
export const formId = 'card_head';

//列表页面编码
export const pagecode = '452003012A_list';

export const card_pagecode = '452003012A_card';

//所有权调出 应用编码
export const appid = '0001Z9100000000014YE';

export const appcode = '452003012A';
//查询模板id
export const oid = '1001Z91000000000B26I';

export const transi_type = '4A47-01';
export const bill_type = '4A47';

//功能节点
export const node_code = '4520028010';

export const fileName = '452003012A-000001';

export const dataSource = 'aum.deploy.usedout.main';
export const transferDataSource = 'aum.deploy.deploytransfer.source';
export const pkField = 'pk_redeploy_out';
export const cardRouter = '/card';
export const transferRouter = '/transfer';

export const reqUrl = {
    queryUrl:'/nccloud/aum/usedout/query.do',
    queryPageUrl:'/nccloud/aum/usedout/querypagegridbypks.do',
    editUrl:'/nccloud/aum/usedout/out_edit.do',
    commitUrl:'/nccloud/aum/usedout/commitAction.do',
    printListUrl:'/nccloud/aum/usedout/out_printlist.do',
    printCardUrl:'/nccloud/aum/usedout/out_printcard.do',
    deleteUrl:'/nccloud/aum/usedout/delete.do'
}


