export const formId = 'card_head';

export const tableId = 'card_body_bodyvos';

export const pagecode = '452003020A_card';

export const list_pagecode = '452003020A_list';

export const appid = '0001Z9100000000014YG';

export const appcode = '452003020A';

export const node_code = '4520028020';

//主键
export const pkField = 'pk_redeploy_in';

export const dataSource = 'aum.deploy.usedin.main';

export const listRouter = '/list';

// 打印模板所在NC节点及标识
export const printFuncode = '';
export const printNodekey = null;

export const card_commit_url = '/nccloud/aum/usedin/commit.do';

export const card_print_url = '/nccloud/aum/usedin/printCard.do';

export const list_url = '/aum/deploy/usedin/list/index.html';

export const editUrl = '/nccloud/aum/usedin/in_edit.do';

export const deleteUrl = '/nccloud/aum/usedin/delete.do';

export const queryUrl = '/nccloud/aum/usedin/querycard.do';

export const updateUrl = '/nccloud/aum/usedin/update.do';

export const batchalter_url = '/nccloud/aum/usedin/batchalter.do';

export const fileName = '452003020A-000001';

export const billtype = '4A48';

// 财务数据字段
export const fiMoneyFields = [
	'origin_value',
	'accu_dep',
	'new_value',
	'net_money',
	'pre_devaluate',
	'dep_amount',
	'salvage'
];
