import * as CONSTANTS from '../const';
let { formId, tableId, pagecode, appcode } = CONSTANTS;
import tableButtonClick from './tableButtonClick';
import { getDataByPk, setNoDataButtonvisiable } from './buttonClick';

import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { multiLangUtils, cardUtils, msgUtils } = utils;
const { queryAboutUtils, refInit, LoginContext } = components;
const { openEquipCardByPk } = queryAboutUtils;
const { defRefCondition } = refInit;
const { afterModifyCardMeta } = cardUtils;
const { getMultiLangByID } = multiLangUtils;
const { loginContext, getContext, loginContextKeys } = LoginContext;
const { CommonKeys, StatusUtils } = commonConst;
const { IBusiRoleConst } = CommonKeys;
const { UISTATE } = StatusUtils;

export default function(props) {
	let _this = this;
	props.createUIDom(
		{
			pagecode //页面id
		},
		function(data) {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					let status = props.getUrlParam('status');
					//按钮的显示状态
					if (status == 'edit') {
						props.button.setButtonVisible([ 'Edit', 'Commit' ], false);
						props.button.setButtonVisible([ 'Save', 'Cancel', 'BatchAlter' ], true);
					} else {
						props.button.setButtonVisible([ 'Save', 'Cancel', 'BatchAlter' ], false);
						props.button.setButtonVisible([ 'Edit', 'Delete', 'Commit' ], true);
					}
				}
				if (data.template) {
					let meta = data.template;
					modifierMeta(props, meta);
					props.meta.setMeta(meta, () => {
						loadData.call(this, props, _this);
					});
				}
			}
		}
	);
}
function modifierMeta(props, meta) {
	let status = props.getUrlParam('status');
	meta[formId].status = status;
	meta[tableId].status = status;
	meta[tableId].items = meta[tableId].items.map((item, key) => {
		item.width = 150;
		return item;
	});

	let porCol = {
		attrcode: 'opr',
		label: getMultiLangByID('amcommon-000000'),
		visible: true,
		itemtype: 'customer',
		className: 'table-opr',
		width: 130,
		fixed: 'right',
		render(text, record, index) {
			let status = props.cardTable.getStatus(tableId);
			let buttonAry = status === 'browse' ? [ 'OpenCard' ] : [ 'OpenCard', 'DelLine' ];

			return props.button.createOprationButton(buttonAry, {
				area: 'card_body_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => {
					tableButtonClick.call(this, props, key, text, record, index);
				}
			});
		}
	};
	meta[tableId].items.push(porCol);
	let pk_group = getContext(loginContextKeys.pk_group); //集团供自定义项参照试用
	//表头参照
	meta[formId].items.map((item) => {
		//经办人
		if (item.attrcode == 'pk_recorder') {
			item.queryCondition = () => {
				let pk_org = props.form.getFormItemsValue(formId, 'pk_org');
				if (pk_org) {
					pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
				}
				return {
					pk_org: pk_org,
					busifuncode: IBusiRoleConst.ASSETORG
				};
			};
		} else {
			//表头自定义项参照处理

			defRefCondition.call(this, props, item, formId, pk_group);
		}
	});

	//表体参照
	meta[tableId].items.map((item) => {
		//调入使用权
		if (item.attrcode == 'pk_usedunit_in_v') {
			item.queryCondition = () => {
				let pk_org = props.form.getFormItemsValue(formId, 'pk_org'); //资产组织
				let pk_usedorg_in = props.form.getFormItemsValue(formId, 'pk_usedorg_in'); //调入使用组织
				if (pk_usedorg_in) {
					pk_usedorg_in = props.form.getFormItemsValue(formId, 'pk_usedorg_in').value;
				}
				if (pk_org) {
					pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
				}

				return {
					pk_org: pk_usedorg_in,
					pk_usedorg_in: pk_usedorg_in,
					TreeRefActionExt: 'nccloud.web.aum.deploy.usedin.refcondition.PK_USEDUNITSqlBuilder'
				};
			};
		} else if (item.attrcode == 'pk_jobmngfil') {
			// 项目
			item.queryCondition = () => {
				let pk_org = props.form.getFormItemsValue(formId, 'pk_org'); // 调出货主管理组织
				if (pk_org) {
					pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
				}
				return {
					pk_org
				};
			};
		} else if (item.attrcode == 'pk_location_after') {
			//调入位置
			item.queryCondition = () => {
				let pk_usedorg_in = props.form.getFormItemsValue(formId, 'pk_usedorg_in'); //调入使用管理组织
				if (pk_usedorg_in) {
					pk_usedorg_in = props.form.getFormItemsValue(formId, 'pk_usedorg_in').value;
				}
				return {
					pk_org: pk_usedorg_in, //主组织
					pk_usedorg_in
				};
			};
		} else if (item.attrcode == 'pk_user_after') {
			// 调入责任人
			// let pk_usedunit_in = props.cardTable.getValByKeyAndIndex(tableId, index, 'pk_usedunit_in');//调入使用权
			// if (pk_usedunit_in) {
			// 	pk_usedunit_in = props.cardTable.getValByKeyAndIndex(tableId, index, 'pk_usedunit_in').value;
			// }
			item.queryCondition = () => {
				let pk_usedorg_in = props.form.getFormItemsValue(formId, 'pk_usedorg_in'); // 调入货主使用组
				// let rowData = props.cardTable.getClickRowIndex(tableId); //当前行的数据
				let index = props.cardTable.getCurrentIndex(tableId);
				let rowData = props.cardTable.getDataByIndex(tableId, index);
				let pk_usedunit_in = '';
				if (rowData && rowData.values) {
					pk_usedunit_in = rowData.values['pk_usedunit_in'].value;
				}
				if (pk_usedorg_in) {
					pk_usedorg_in = props.form.getFormItemsValue(formId, 'pk_usedorg_in').value;
				}
				return {
					pk_org: pk_usedunit_in, //主组织
					pk_usedunit_in,
					busifuncode: IBusiRoleConst.ASSETORG
				};
			};
		} else if (item.attrcode == 'pk_warehouse_in') {
			//调入仓库
			// let pkOrgICIn = props.cardTable.getValByKeyAndIndex(tableId, index, 'pk_icorg_in'); //调入库存组织
			// if (pkOrgICIn) {
			// 	pkOrgICIn = props.cardTable.getValByKeyAndIndex(tableId, index, 'pk_icorg_in').value;
			// }
			item.queryCondition = () => {
				let index = props.cardTable.getCurrentIndex(tableId);
				let rowData = props.cardTable.getDataByIndex(tableId, index);
				let pkOrgICIn = '';
				if (rowData && rowData.values) {
					pkOrgICIn = rowData.values['pk_icorg_in'].value;
				}
				return {
					pk_org: pkOrgICIn, //主组织
					pkOrgICIn,
					GridRefActionExt: 'nccloud.web.aum.deploy.in.refcondition.PK_WAREHOUSE_INSqlBuilder'
				};
			};
		} else if (item.attrcode == 'pk_usedept_after_v') {
			// 调入使用部门
			item.queryCondition = () => {
				let pk_ownerorg_in = props.form.getFormItemsValue(formId, 'pk_ownerorg_in'); // 调入货主管理组织
				if (pk_ownerorg_in) {
					pk_ownerorg_in = props.form.getFormItemsValue(formId, 'pk_ownerorg_in').value;
				}
				let index = props.cardTable.getCurrentIndex(tableId);
				let rowData = props.cardTable.getDataByIndex(tableId, index);
				let pk_usedunit_in = '';
				if (rowData && rowData.values) {
					pk_usedunit_in = rowData.values['pk_usedunit_in'].value;
				}
				// let pk_usedunit_in = props.cardTable.getValByKeyAndIndex(tableId, index, 'pk_usedunit_in'); //调入使用管理组织
				// if (pk_usedunit_in) {
				// 	pk_usedunit_in = props.cardTable.getValByKeyAndIndex(tableId, index, 'pk_usedunit_in').value;
				// }
				return {
					// pk_org: pk_ownerorg_in,//主组织
					pk_org: pk_usedunit_in, //主组织
					pk_usedunit_in,
					busifuncode: IBusiRoleConst.ASSETORG
				};
			};
		} else if (item.attrcode == 'pk_equip') {
			//通过设备编码查找设备
			item.renderStatus = UISTATE.browse;
			item.render = (text, record, index) => {
				return (
					<div class="card-table-browse">
						<span
							className="code-detail-link"
							onClick={() => {
								openEquipCardByPk.call(this, props, record.values[item.attrcode].value);
							}}
						>
							{record.values[item.attrcode] && record.values[item.attrcode].display}
						</span>
					</div>
				);
			};
		} else {
			//表体自定义项参照处理

			defRefCondition.call(this, props, item, formId, pk_group);
		}
	});
	let config = { tableId };
	meta = afterModifyCardMeta.call(this, props, meta, config);
	return meta;
}

function loadData(props, _this) {
	let status = props.getUrlParam('status');
	if (status != 'add') {
		let pk = props.getUrlParam('id');
		if (pk && pk != 'undefined') {
			let callback = (data) => {
				if (!data) {
					setNoDataButtonvisiable.call(_this);
				}
			};
			getDataByPk.call(_this, props, pk, callback);
		}
	}
}
