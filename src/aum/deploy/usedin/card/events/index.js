import buttonClick from './buttonClick';
import initTemplate from './initTemplate';
import afterEvent from './afterEvent';
import pageInfoClick from './pageInfoClick';
import bodyBeforeEvent from './bodyBeforeEvent';

export { buttonClick, afterEvent,initTemplate,pageInfoClick,bodyBeforeEvent};
