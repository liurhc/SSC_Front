import { loadDataByPk } from './buttonClick';
/**
 * 分页查询
 * @param {*} props 
 * @param {*} pk 
 */
export default function(props, pk) {
	// 更新参数
	props.setUrlParam({ id: pk });
	loadDataByPk.call(this, props, pk);
}
