import { toast } from 'nc-lightapp-front';
import { formId, tableId } from '../const';

import ampub from 'ampub';
const { utils, commonConst } = ampub;
const { multiLangUtils, msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { CommonKeys } = commonConst;
const { IBusiRoleConst } = CommonKeys;
const { showMessage } = msgUtils;

export default function bodyBeforeEvent(props, moduleId, key, value, index, record) {
	let editFlag = true;
	editFlag = isEdit.call(this, props, moduleId, key, value, index, record);
	refCondtion.call(this, props, moduleId, key, value, index, record);
	return editFlag;
}

function isEdit(props, moduleId, key, value, index, record) {
	if (key == 'pk_warehouse_in') {
		let pkOrgICIn = props.cardTable.getValByKeyAndIndex(tableId, index, 'pk_icorg_in').value;
		if (!pkOrgICIn) {
			// toast({ content: '请先填调入库存组织组织', color: 'warning' });
			toast({ content: getMultiLangByID('452003016A-000001'), color: 'warning' });
			/* 国际化处理： 当前环境并不支持SVG*/
			return false;
		}
	}
	return true;
}

//表体字段参照处理
function refCondtion(props, moduleId, key, value, index, record) {
	let meta = props.meta.getMeta();
	meta[tableId].items.map((item) => {
		//调入使用部门
		if (item.attrcode == 'pk_usedept_after_v') {
			let pk_usedunit_in = props.cardTable.getValByKeyAndIndex(tableId, index, 'pk_usedunit_in'); //调入使用权
			if (pk_usedunit_in) {
				pk_usedunit_in = props.cardTable.getValByKeyAndIndex(tableId, index, 'pk_usedunit_in').value;
			}
			item.queryCondition = () => {
				return {
					pk_org: pk_usedunit_in,
					busifuncode: IBusiRoleConst.ASSETORG
				};
			};
		}

		// 调入责任人
		if (item.attrcode == 'pk_user_after') {
			let pk_ownerorg_in = props.form.getFormItemsValue(formId, 'pk_ownerorg_in'); // 调入货主管理组
			let pk_usedunit_in = props.cardTable.getValByKeyAndIndex(tableId, index, 'pk_usedunit_in'); //调入使用权
			if (pk_usedunit_in) {
				pk_usedunit_in = props.cardTable.getValByKeyAndIndex(tableId, index, 'pk_usedunit_in').value;
			}
			if (pk_ownerorg_in) {
				pk_ownerorg_in = props.form.getFormItemsValue(formId, 'pk_ownerorg_in').value;
			}
			item.queryCondition = () => {
				return {
					pk_org: pk_usedunit_in, //调入使用权
					pk_usedunit_in,
					busifuncode: IBusiRoleConst.ASSETORG
				};
			};
		}

		//调入仓库
		if (item.attrcode == 'pk_warehouse_in') {
			let pkOrgICIn = props.cardTable.getValByKeyAndIndex(tableId, index, 'pk_icorg_in'); //调入库存组织
			if (pkOrgICIn) {
				pkOrgICIn = props.cardTable.getValByKeyAndIndex(tableId, index, 'pk_icorg_in').value;
			}
			item.queryCondition = () => {
				return {
					pk_org: pkOrgICIn, //主组织
					pkOrgICIn,
					GridRefActionExt: 'nccloud.web.aum.deploy.usedin.refcondition.PK_WAREHOUSE_INSqlBuilder'
				};
			};
		}
	});
	props.meta.setMeta(meta);
}
