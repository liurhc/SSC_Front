import { ajax, base, toast } from 'nc-lightapp-front';
import * as CONSTANTS from '../const';
import { linkToCard } from './buttonClick';
const { tableId, node_code, formId, card_pagecode, editUrl, list_commit_url, pkField, dataSource } = CONSTANTS;
import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { ScriptReturnUtils, queryAboutUtils } = components;
const { getScriptListReturn } = ScriptReturnUtils;
const { openListApprove } = queryAboutUtils;
const { StatusUtils } = commonConst;
const { UISTATE } = StatusUtils;
const { msgUtils } = utils;
const { MsgConst, showMessage } = msgUtils;
export default function tableButtonClick(props, key, text, record, index) {
	switch (key) {
		// 表格操作按钮
		case 'Edit':
			edit.call(this, props, record);
			break;
		case 'Delete':
			delRow(props, tableId, index, record);
			break;
		case 'Commit':
			commitClick.call(this, 'SAVE', props, tableId, index, record);
			break;
		case 'UnCommit':
			commitClick.call(this, 'UNSAVE', props, tableId, index, record);
			break;
		case 'QueryAboutBillFlow':
			let pk = record.pk_redeploy_in.value;
			this.setState({
				pk_redeploy_in: pk,
				showApprove: true,
				transi_type: record.transi_type.value
			});
			break;
		default:
			break;
	}
}

/**
 * 修改
 * @param {*} props 
 */
export function edit(props, record) {
	ajax({
		url: editUrl,
		data: {
			pk: record.pk_redeploy_in.value,
			resourceCode: node_code
		},
		success: (res) => {
			if (res.data) {
				toast({ color: 'warning', content: res.data });
			} else {
				linkToCard.call(this, props, record, UISTATE.edit);
			}
		}
	});
}

/**
 * 直接删除数据
 * @param {*} record 
 */
const delRow = (props, tableId, index, record) => {
	let paramInfoMap = {};
	paramInfoMap[record.pk_redeploy_in.value] = record.ts.value;
	let params = [ { id: record.pk_redeploy_in.value, index: index } ];
	ajax({
		url: '/nccloud/aum/usedin/delete.do',
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: card_pagecode
		},
		success: (res) => {
			getScriptListReturn(params, res, props, pkField, formId, tableId, true, dataSource);
		}
	});
};

//提交
export function commitClick(OperatorType, props, tableId, index, record) {
	let paramInfoMap = {};
	paramInfoMap[record.pk_redeploy_in.value] = record.ts.value;
	let params = [ { id: record.pk_redeploy_in.value, index: index } ];
	ajax({
		url: list_commit_url,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: OperatorType,
			pageid: card_pagecode,
			commitType: 'commit'
		},
		success: (res) => {
			getScriptListReturn.call(this, params, res, props, pkField, formId, tableId, false, dataSource);
		}
	});
}
