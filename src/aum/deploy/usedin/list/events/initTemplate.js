import * as CONSTANTS from '../const';
import tableButtonClick from './tableButtonClick';
const { tableId, pagecode, searchId } = CONSTANTS;
import { setBatchBtnsEnable, linkToCard } from './buttonClick';

import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { multiLangUtils } = utils;
const { assetOrgMultiRefFilter, refInit, LoginContext, EquipQueryCondition } = components;
const { AssetOrgMultiRefFilter } = assetOrgMultiRefFilter;
const { defRefCondition } = refInit;
const { CommonKeys, StatusUtils } = commonConst;
const { IBusiRoleConst } = CommonKeys;
const { loginContext, getContext, loginContextKeys } = LoginContext;
const { getMultiLangByID } = multiLangUtils;
const { UISTATE, BILLSTATUS } = StatusUtils;
const { filterByOrg } = EquipQueryCondition;

export default function(props) {
	let _this = this;
	props.createUIDom(
		{
			pagecode: pagecode //页面id
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button, () => {
						// setBatchBtnsEnable.call(this,props,tableId)
					});
					// 行删除时悬浮框提示
					props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001'));
				}
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta, _this);
					props.meta.setMeta(meta, () => {
						setBatchBtnsEnable.call(this, props, tableId);
					});
				}
			}
		}
	);
}

function modifierMeta(props, meta, _this) {
	meta[tableId].pagination = true;
	meta[tableId].items = meta[tableId].items.map((item, key) => {
		item.width = 145;
		if (item.attrcode == 'bill_code') {
			item.render = (text, record, index) => {
				return (
					<div class="simple-table-td" field="bill_code" fieldname={getMultiLangByID('452003020A-000002')}>
						<span
							className="code-detail-link"
							onClick={() => {
								linkToCard.call(this, props, record, UISTATE.browse);
							}}
						>
							{record.bill_code && record.bill_code.value}
						</span>
					</div>
				);
			};
		}
		return item;
	});
	//添加操作列
	let buttonCol = {
		attrcode: 'opr',
		label: getMultiLangByID('amcommon-000000'),
		className: 'table-opr',
		itemtype: 'customer',
		width: 200,
		fixed: 'right',
		visible: true,
		render: (text, record, index) => {
			let bill_status = record.bill_status.value;
			let buttonAry;
			if (bill_status === BILLSTATUS.free_check) {
				//自由态
				buttonAry = [ 'Commit', 'Edit', 'Delete' ];
			} else if (bill_status === BILLSTATUS.un_check) {
				//已提交
				buttonAry = [ 'UnCommit', 'QueryAboutBillFlow' ];
			} else if (bill_status === BILLSTATUS.check_going) {
				//审批中
				buttonAry = [ 'QueryAboutBillFlow' ];
			} else if (bill_status === BILLSTATUS.check_pass) {
				//审批通过
				buttonAry = [ 'UnCommit', 'QueryAboutBillFlow' ];
			} else if (bill_status === BILLSTATUS.check_nopass) {
				//审批未通过
				buttonAry = [ 'Edit', 'Delete', 'QueryAboutBillFlow' ];
			} else if (bill_status === BILLSTATUS.close) {
				//关闭
			} else {
			}
			return props.button.createOprationButton(buttonAry, {
				area: 'list_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => {
					tableButtonClick.call(_this, props, key, text, record, index);
				}
			});
		}
	};
	meta[tableId].items.push(buttonCol);
	meta[searchId].items.map((item) => {
		if (item.attrcode == 'pk_usedorg_in') {
			item.queryCondition = () => {
				return { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter' };
			};
		} else if (item.attrcode == 'pk_recorder') {
			AssetOrgMultiRefFilter.call(this, props, searchId, item, 'pk_usedorg_out');
			item.queryCondition = () => {
				let pk_org = getSearchValue.call(this, props, 'pk_usedorg_out');
				if (pk_org) {
					return {
						pk_org: pk_org,
						busifuncode: IBusiRoleConst.ASSETORG
					};
				} else {
					return {
						busifuncode: IBusiRoleConst.ASSETORG
					};
				}
			};
			// }
			// else if (item.attrcode == 'bodyvos.pk_usedunit_in') {
			// 	//调入使用权
			// 	item.queryCondition = () => {
			// 		let pk_usedorg_in = getSearchValue.call(this, props, 'pk_usedorg_in');
			// 		return {
			// 			pk_usedorg_in,
			// 			TreeRefActionExt: 'nccloud.web.aum.deploy.in.refcondition.PK_USEDUNIT_IN_VSqlBuilder'
			// 		};
			// 	};
		} else if (item.attrcode == 'bodyvos.pk_usedept_after') {
			// 调入使用部门
			AssetOrgMultiRefFilter.call(this, props, searchId, item, 'bodyvos.pk_usedunit_in');
			item.queryCondition = () => {
				let pk_usedunit_in = getSearchValue.call(this, props, 'bodyvos.pk_usedunit_in');
				if (pk_usedunit_in) {
					return {
						pk_org: pk_usedunit_in, //主组织
						busifuncode: IBusiRoleConst.ASSETORG
					};
				} else {
					return {
						busifuncode: IBusiRoleConst.ASSETORG
					};
				}
			};
		} else if (item.attrcode == 'bodyvos.pk_user_after') {
			//调入使用人
			AssetOrgMultiRefFilter.call(this, props, searchId, item, 'bodyvos.pk_usedunit_in');
			item.queryCondition = () => {
				let pk_usedunit_in = getSearchValue.call(this, props, 'bodyvos.pk_usedunit_in');
				if (pk_usedunit_in) {
					return {
						pk_org: pk_usedunit_in, //主组织
						busifuncode: IBusiRoleConst.ASSETORG
					};
				} else {
					return {
						busifuncode: IBusiRoleConst.ASSETORG
					};
				}
			};
		} else if (item.attrcode == 'bodyvos.pk_warehouse_in') {
			AssetOrgMultiRefFilter.call(this, props, searchId, item, 'bodyvos.pk_icorg_in');
			item.queryCondition = () => {
				let pkOrgICIn = getSearchValue.call(this, props, 'bodyvos.pk_icorg_in');
				if (pkOrgICIn) {
					return {
						pk_org: pkOrgICIn, //主组织
						pkOrgICIn,
						GridRefActionExt: 'nccloud.web.aum.deploy.in.refcondition.PK_WAREHOUSE_INSqlBuilder'
					};
				} else {
					return {
						GridRefActionExt: 'nccloud.web.aum.deploy.in.refcondition.PK_WAREHOUSE_INSqlBuilder'
					};
				}
			};
		} else {
			let bodyDefPrefix = 'bodyvos.def';
			let pk_group = getContext(loginContextKeys.pk_group); //集团供自定义项参照试用
			defRefCondition.call(this, props, item, searchId, pk_group, true, 'pk_org', bodyDefPrefix);
			defRefCondition.call(this, props, item, searchId, pk_group, true);
		}
	});
	filterByOrg.call(
		this,
		props,
		meta,
		searchId,
		[ 'bodyvos.pk_equip.pk_mandept', 'bodyvos.pk_equip.pk_user', 'bodyvos.pk_equip.pk_manager' ],
		'pk_usedorg_in',
		true
	);
	return meta;
}

//获取查询条件的值
function getSearchValue(props, field) {
	let data = props.search.getSearchValByField(searchId, field);
	if (data && data.value && data.value.firstvalue) {
		let firstvalue = data.value.firstvalue;
		if (firstvalue.split(',').length == 1) {
			return firstvalue;
		}
	}
}
