import { ajax } from 'nc-lightapp-front';
import * as CONSTANTS from '../const';

const { pagecode, tableId,queryPageUrl } = CONSTANTS

export default (props, config, pks) => {
	let data = {
		"allpks": pks,
		"pagecode": pagecode
	};
	//得到数据渲染到页面
	ajax({
		url: queryPageUrl,
		data: data,
		success: function (res) {
			let { success, data } = res;
			if (success) {
				if (data) {
					props.table.setAllTableData(tableId, data[tableId]);
				} else {
					props.table.setAllTableData(tableId, { rows: [] });
				}
			}
		}
	});
}
