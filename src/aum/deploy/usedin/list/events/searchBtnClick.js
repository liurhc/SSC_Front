import { ajax, toast } from 'nc-lightapp-front';
import ampub from 'ampub';
const { utils, components } = ampub;
const { listUtils } = utils;
const { setQueryInfoCache, setListValue, listConst } = listUtils;
const { ApprovalTrans, LoginContext } = components;
const { getContext, loginContextKeys } = LoginContext;
import { setBatchBtnsEnable } from './buttonClick';
import * as CONSTANTS from '../const';

const { pagecode, queryUrl, tableId, bill_type, searchId } = CONSTANTS;

//点击查询，获取查询区数据
export default function(props, searchVal, type, queryInfo, isRefresh) {
	if (!searchVal) {
		return;
	}
	//分页信息
	let pageInfo = props.table.getTablePageInfo(tableId);
	// let data = props.search.getQueryInfo(searchId, true);
	let transi_type = getContext(loginContextKeys.transtype);
	if (!queryInfo) {
		queryInfo = props.search.getQueryInfo(searchId);
	}
	// 重新查询，需要重置分页到第一页
	// pageInfo.pageIndex = 0;
	queryInfo.pagecode = pagecode;
	queryInfo.pageInfo = pageInfo;
	queryInfo.billtype = bill_type;
	queryInfo.transtype = transi_type;
	// 缓存查询条件
	setQueryInfoCache.call(this, queryInfo, isRefresh, type);
	ajax({
		url: queryUrl,
		data: queryInfo,
		success: (res) => {
			setListValue.call(
				this,
				props,
				res,
				tableId,
				isRefresh ? listConst.SETVALUETYPE.refresh : listConst.SETVALUETYPE.query
			);
			setBatchBtnsEnable.call(this, props, tableId);
		}
	});
}
