import {
	tableId,
	pagecode,
	dataSource,
	cardRouter,
	list_commit_url,
	pkField,
	card_print_url,
	formId,
	fileName,
	card_pagecode,
	deleteUrl
} from '../const';
import { ajax, toast, print, output } from 'nc-lightapp-front';

import ampub from 'ampub';
const { utils, components } = ampub;
const { ScriptReturnUtils, queryAboutUtils, queryVocherUtils, LoginContext } = components;
const { getScriptListReturn } = ScriptReturnUtils;
const { openListBillTrack, openReportEquip, LinkReportConst } = queryAboutUtils;
const { queryAboutVoucher } = queryVocherUtils;
const { multiLangUtils, msgUtils, listUtils, transferUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { setBillFlowBtnsEnable, batchRefresh } = listUtils;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { pushToTransferPage } = transferUtils;
const { getContext, loginContextKeys } = LoginContext;

import { searchBtnClick } from './index';

export default function buttonClick(props, id) {
	switch (id) {
		case 'Refresh':
			refresh.call(this, props);
			break;
		case 'Delete':
			delConfirm.call(this, props);
			break;
		case 'Commit':
			commit.call(this, props, 'SAVE');
			break;
		case 'UnCommit':
			commit.call(this, props, 'UNSAVE');
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'OutPut':
			outputTemp.call(this, props);
			break;
		case 'QueryAboutBusiness':
			openListBillTrack(this, props, tableId, pkField);
			break;
		case 'Attachment':
			AttachmentClick.call(this, props);
		default:
			break;
	}
}

//批量删除
export function delConfirm(props) {
	let checkedRows = props.table.getCheckedRows(tableId);
	if (checkedRows.length == 0) {
		// toast({ content: '请先选中需要删除的数据', color: 'warning' });
		let config = {
			type: ChooseDelete
		};
		showMessage.call(this, props, config);
		return;
	}
	showConfirm.call(this, props, { type: MsgConst.Type.DelSelect, beSureBtnClick: deleteAction });
}

function deleteAction(props) {
	let data = props.table.getCheckedRows(tableId);
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values.pk_redeploy_out.value;
		let ts = v.data.values.ts.value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	ajax({
		url: deleteUrl,
		data: params,
		success: (res) => {
			getScriptListReturn(params, res, props, 'pk_redeploy_in', formId, tableId, true, dataSource);
		}
	});
}

/**
 * 跳转卡片
 * @param {*} props 
 * @param {*} record 
 */
export function linkToCard(props, record = {}, status = 'browse') {
	props.pushTo(cardRouter, {
		pagecode: card_pagecode,
		status,
		id: record[pkField] ? record[pkField].value : ''
	});
}

/**
 * 提交
 * @param {*} props 
 */
export function commit(props, OperatorType, content) {
	let data = props.table.getCheckedRows(tableId);
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values[pkField].value;
		let ts = v.data.values.ts.value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	let commitType = '';
	if (OperatorType === 'SAVE') {
		//提交传这个
		commitType = 'commit';
	}
	ajax({
		url: list_commit_url,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: OperatorType,
			pageid: card_pagecode,
			commitType: commitType, //提交
			content: content
		},
		success: (res) => {
			if (content) {
				this.setState({
					compositedisplay: false
				});
			}
			getScriptListReturn.call(this, params, res, props, pkField, formId, tableId, false, dataSource);
		}
	});
}

/**
 * 打印
 * @param {*} props 
 */
export function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	// let { pageConfig } = props;
	if (!printData) {
		// toast({ content: '请选择需要打印的数据', color: 'warning' });
		let config = {
			type: ChoosePrint
		};
		showMessage.call(this, props, config);
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		card_print_url, // 后台打印服务url
		printData
	);
}

/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		// toast({ content: '请选择需要输出的数据', color: 'warning' });
		let config = {
			type: ChooseOutput
		};
		showMessage.call(this, props, config);
		return;
	}
	output({
		url: card_print_url,
		data: printData
	});
}

/* 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	// let { pageConfig } = props;
	// let { pagecode, tableId, pkField, printFuncode, printNodekey } = pageConfig;
	let checkedRows = props.table.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length == 0) {
		// toast({ content: '请选择需要打印的数据', color: 'warning' });
		let config = {
			type: ChoosePrint
		};
		showMessage.call(this, props, config);
		return false;
	}
	let pks = [];
	checkedRows.map((item) => {
		pks.push(item.data.values[pkField].value);
	});
	let printData = {
		filename: getMultiLangByID(fileName), // 文件名称
		nodekey: null, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

/**
* 刷新
* @param {*} props 
*/
export function refresh(props) {
	batchRefresh.call(this, props, searchBtnClick);
}

/**
 * 附件
 */

export function AttachmentClick(props) {
	let checkedrows = props.table.getCheckedRows(tableId);
	// billNo 是单据编码
	let billNo = checkedrows[0].data.values['bill_code'].value;
	// billNo 是单据主键
	let billId = checkedrows[0].data.values['pk_redeploy_in'].value;
	props.ncUploader.show('warcontract-uploader', {
		billId: 'pam/redeploy_in/' + billId,
		billNo
	});
}

/**
* 设置批量按钮是否可用
* @param {*} props 
* @param {*} moduleId 
*/
export function setBatchBtnsEnable(props, moduleId) {
	setBillFlowBtnsEnable.call(this, props, moduleId);
}
