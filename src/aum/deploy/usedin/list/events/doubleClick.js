import { linkToCard } from './buttonClick';
import ampub from 'ampub';
const { commonConst } = ampub;
const { StatusUtils } = commonConst;
const { UISTATE } = StatusUtils;

export default function doubleClick(record, index, props, e) {
	linkToCard.call(this, props, record, UISTATE.browse);
}
