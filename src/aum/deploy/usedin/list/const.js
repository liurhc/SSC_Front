//查询区编码
export const searchId = 'searchArea';

//列表表体区编码
export const tableId = 'list_head';

//卡片表体去编码
export const formId = 'card_head';

//列表页面编码
export const pagecode = '452003020A_list';

//卡片页面编码
export const card_pagecode = '452003020A_card'

//所有权调出 应用编码
export const appid = '0001Z9100000000014YG';

//查询模板id
export const oid = '1001Z91000000000BFB1';

export const appcode = '452003020A';

export const list_commit_url = '/nccloud/aum/usedin/commit.do';

export const bill_type = '4A48';

//主键
export const pkField = 'pk_redeploy_in';

export const dataSource = 'aum.deploy.usedin.main';

export const cardRouter = '/card';

// 打印模板所在NC节点及标识
export const printFuncode = '';
export const printNodekey = null;

export const list_print_url = '/nccloud/aum/usedin/printList.do';

export const card_print_url = '/nccloud/aum/usedin/printCard.do';

export const editUrl = '/nccloud/aum/usedin/in_edit.do';

export const deleteUrl ='/nccloud/aum/usedin/delete.do';

export const queryUrl = '/nccloud/aum/usedin/query.do';

export const queryPageUrl = '/nccloud/aum/usedin/querypagegridbypks.do';

//节点编码
export const  node_code ='4520028020';

export const fileName = '452003020A-000001';




