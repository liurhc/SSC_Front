//主子表列表
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast, createPageIcon } from 'nc-lightapp-front';
import { initTemplate, searchBtnClick, initTemplateFull, searchAfterEvent, buttonClick } from './events';
import { headcode, searchcode, bodycode, maincode, pageConfig, appcode } from './const';
import './index.less';
import ampub from 'ampub';

const { components, commonConst, utils } = ampub;
const { getContext, setContext } = components.LoginContext;
const { TransferConst } = commonConst.CommonKeys;
const { UISTATE } = commonConst.StatusUtils;
const { getMultiLangByID } = utils.multiLangUtils;

const { NCToggleViewBtn, NCAffix } = base;
const { dataSource, sourceInfoField } = pageConfig;
let { initSourceUrl } = pageConfig.url;

class BorrowApplyTransfer extends Component {
	constructor(props) {
		super(props);
		this.headTableId = headcode;
		this.state = {
			expand: true
		};
		let transferAppCode = props.getSearchParam('c');
		if (!transferAppCode) {
			transferAppCode = '452000508A';
		}
		let nextBillType = '4A15';
		let sourceBillType = '4A38';
		let defaultSourceTranstype = '4A38-01';
		let data = { transferAppCode, billtype: nextBillType };
		//根据转单应用编码和其注册的参数信息初始化上游信息
		ajax({
			url: initSourceUrl,
			data,
			success: (res) => {
				let sourceInfo = {
					appcode: appcode,
					billtype: sourceBillType,
					transtype: defaultSourceTranstype,
					nextAppCode: '',
					transferAppCode: transferAppCode,
					busitypes: new Array()
				};
				if (res.data) {
					let srcAppInfo = res.data;
					if (srcAppInfo.length > 0) {
						if (srcAppInfo.length == 1) {
							sourceInfo.billtype = sourceBillType;
							sourceInfo.transtype = srcAppInfo[0].src_transtype;
							sourceInfo.busitypes = srcAppInfo[0].busitypes;
							sourceInfo.nextAppCode = srcAppInfo[0].next_appcode;
						}
					} else {
						toast({
							color: 'warning',
							title: getMultiLangByID('452000508A-000004'),
							content: getMultiLangByID('452000508A-000003')
						});
					}
				} else {
					toast({
						color: 'warning',
						title: getMultiLangByID('452000508A-000004'),
						content: getMultiLangByID('452000508A-000003')
					});
				}
				setContext(sourceInfoField, sourceInfo, dataSource);
				initTemplate.call(this, props); //加载主子模板
				initTemplateFull.call(this, props); //加载主子拉平模板
			}
		});
	}
	componentDidMount() {}

	render() {
		const { transferTable, form, button, search } = this.props;
		const { NCCreateSearch } = search;
		const { createTransferTable, createTransferList } = transferTable;
		const { createButtonApp } = button;
		let selectedShow = transferTable.getSelectedListDisplay(this.headTableId);
		let btnCreateNextName = getMultiLangByID('452000508A-000002'); /*生成借用单*/
		return (
			<div id="transferDemo">
				{!selectedShow ? (
					<div>
						<div className="nc-bill-list">
							<NCAffix>
								<div className="nc-bill-header-area">
									<div className="header-title-search-area">
										{createPageIcon()}
										<h2 className="title-search-detail">
											{getMultiLangByID('452000508A-000001') /*请选择借用申请单*/}
										</h2>
									</div>
									<div className="header-button-area">
										<NCToggleViewBtn
											expand={this.state.expand}
											onClick={() => {
												if (!this.props.meta.getMeta()[maincode]) {
													initTemplateFull(this.props); //加载主子拉平模板
												}
												this.props.transferTable.changeViewType(this.headTableId);
												let expandtemp = !this.state.expand;
												this.setState({ expand: expandtemp });
											}}
										/>
									</div>
								</div>
							</NCAffix>
							<div className="nc-bill-search-area">
								{NCCreateSearch(
									searchcode, //模块id
									{
										onAfterEvent: searchAfterEvent.bind(this),
										clickSearchBtn: searchBtnClick.bind(this)
									}
								)}
							</div>
						</div>
					</div>
				) : (
					''
				)}

				<div className="nc-bill-transferTable-area">
					{createTransferTable({
						searchAreaCode: searchcode,
						tableType: 'full', //选填 表格默认显示的类型 full:主子拉平 nest:主子表
						headTableId: this.headTableId, //表格组件id
						bodyTableId: bodycode, //子表模板id
						fullTableId: maincode, //主子拉平模板id
						transferBtnText: btnCreateNextName, //转单按钮显示文字
						containerSelector: '#transferDemo', //容器的选择器 必须唯一,用于设置底部已选区域宽度
						dataSource: dataSource,
						componentInitFinished: () => {
							//缓存数据赋值成功的钩子函数
							//若初始化数据后需要对数据做修改，可以在这里处理
						},
						selectArea: () => {
							//已选列表自定义区域
							return (
								<div className="header-button-area">
									{createButtonApp({
										area: 'bottom',
										buttonLimit: 3,
										onButtonClick: buttonClick.bind(this),
										popContainer: document.querySelector('.header-button-area')
									})}
								</div>
							);
						},
						onTransferBtnClick: (ids) => {
							//清空缓存
							// let { deleteCache } = this.props.transferTable;
							// deleteCache(dataSource);
							let sourceInfo = getContext(sourceInfoField, dataSource);
							if (sourceInfo) {
								//点击转单按钮钩子函数
								this.props.pushTo('/card', {
									appcode: sourceInfo.nextAppCode,
									transferAppCode: sourceInfo.transferAppCode,
									pagecode: '452000512A_transfercard',
									status: UISTATE.transferAdd,
									type: TransferConst.type
								});
							} else {
								//点击转单按钮钩子函数
								this.props.pushTo('/card', {
									appcode: nextAppCode,
									transferAppCode: '452000508A',
									pagecode: '452000512A_transfercard',
									status: UISTATE.transferAdd,
									type: TransferConst.type
								});
							}
						},
						onChangeViewClick: () => {
							//点击切换视图钩子函数
							if (!this.props.meta.getMeta()[maincode]) {
								initTemplateFull(this.props); //加载主子拉平模板
							}
							this.props.transferTable.changeViewType(this.headTableId);
						},
						onCheckedChange: (flag, record, index, bodys) => {
							//勾选的回调函数
							if (flag) {
								this.props.button.setButtonDisabled([ 'Close' ], false);
							} else {
								let selectData = this.props.transferTable.getTransferTableSelectedValue();
								if (selectData && selectData.head && selectData.head.length > 0) {
									this.props.button.setButtonDisabled([ 'Close' ], false);
								} else {
									this.props.button.setButtonDisabled([ 'Close' ], true);
								}
							}
						},
						onClearAll: () => {
							this.props.button.setButtonDisabled([ 'Close' ], true);
						}
					})}
				</div>
			</div>
		);
	}
}

BorrowApplyTransfer = createPage({})(BorrowApplyTransfer);

export default BorrowApplyTransfer;
