import { pagecode, appcode, headcode, searchcode, pageConfig } from '../const';
import ampub from 'ampub';

const { components, commonConst } = ampub;
const { defRefCondition, commonRefCondition } = components.refInit;
const { getContext, loginContextKeys, loginContext } = components.LoginContext;
const { IBusiRoleConst } = commonConst.CommonKeys;
const { UISTATE } = commonConst.StatusUtils;
const { AssetOrgMultiRefFilter } = components.assetOrgMultiRefFilter;

let sourceAppCode = appcode;
let { sourceInfoField, dataSource, sourcePageCode } = pageConfig;
let { sourceUrl } = pageConfig.url;

export default function(props) {
	let sourceInfo = getContext(sourceInfoField, dataSource);
	if (sourceInfo.appcode) {
		sourceAppCode = sourceInfo.appcode;
	}
	props.createUIDom(
		{
			pagecode: pagecode, //借用申请 转单页面编码
			appcode: sourceAppCode //借用申请应用编码
		},
		function(data) {
			if (data) {
				if (data.context) {
					loginContext.call(this, data.context);
				}
				if (data.button) {
					let button = data.button;
					button[0].isenable = false; //默认不可用
					props.button.setButtons(button);
				}
				if (data.template) {
					let meta = data.template;
					meta = addLink(props, meta);
					meta = addCondition(props, meta);
					props.meta.addMeta(meta);
				}
			}
		}
	);
}
//增加查询相关条件控制
function addCondition(props, meta) {
	let pk_group = getContext(loginContextKeys.groupId);
	//查询区（借用人，借用部门）参照过滤
	meta[searchcode].items.map((item) => {
		if (item.attrcode == 'pk_org') {
			item.queryCondition = () => {
				return { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter' };
			};
		} else if (item.attrcode == 'pk_material' || item.attrcode == 'pk_jobmngfil') {
			//（项目、物料）
			item.queryCondition = () => {
				let pk_org = getSearchValue.call(this, props, 'pk_org'); // 主组织
				return { pk_org }; // 根据主组织过滤
			};
		} else if (item.attrcode == 'pk_usedept') {
			//组织多选参照过滤
			AssetOrgMultiRefFilter.call(this, props, searchcode, item, 'pk_org');
			//使用部门 使用人(领用部门 领用人)
			item.queryCondition = () => {
				let pk_unit_used = getSearchValue.call(this, props, 'pk_org'); //主组织
				let filter = { busifuncode: IBusiRoleConst.ASSETORG };
				if (pk_unit_used) {
					filter['pk_org'] = pk_unit_used;
				}
				return filter; // 根据pk_org过滤
			};
		} else if (item.attrcode == 'pk_user') {
			AssetOrgMultiRefFilter.call(this, props, searchcode, item, 'pk_org');
			item.queryCondition = () => {
				let pk_unit_used = getSearchValue.call(this, props, 'pk_org'); //主组织
				let filter = { busifuncode: IBusiRoleConst.ASSETORG };
				if (pk_unit_used) {
					filter['pk_org'] = pk_unit_used;
				}
				return filter; // 根据pk_org过滤
			};
		} else if (item.attrcode == 'pk_applier') {
			AssetOrgMultiRefFilter.call(this, props, searchcode, item, 'pk_org');
			//申请人
			item.queryCondition = () => {
				let pk_unit_used = getSearchValue.call(this, props, 'pk_org'); //主组织
				let filter = { busifuncode: IBusiRoleConst.ASSETORG };
				if (pk_unit_used) {
					filter['pk_org'] = pk_unit_used;
				}
				return filter; // 根据pk_org过滤
			};
		} else {
			let bodyDefPrefix = 'bodyvos.def';
			defRefCondition.call(this, props, item, searchcode, pk_group, true, 'pk_org', bodyDefPrefix);
			defRefCondition.call(this, props, item, searchcode, pk_group, true);
		}
		commonRefCondition.call(this, props, item);
	});
	return meta;
}
//获取查询条件的值
function getSearchValue(props, field) {
	let data = props.search.getSearchValByField(searchcode, field);
	if (data && data.value && data.value.firstvalue && 20 == data.value.firstvalue.length) {
		return data.value.firstvalue;
	}
}
//添加超链接
function addLink(props, meta) {
	meta[headcode].items = meta[headcode].items.map((item, key) => {
		if (item.attrcode == 'bill_code') {
			item.render = (text, record, index) => {
				return (
					<span
						className="code-detail-link"
						onClick={() => {
							props.openTo(sourceUrl, {
								pagecode: sourcePageCode,
								status: UISTATE.browse,
								appcode: sourceAppCode,
								id: record.pk_apply.value
							});
						}}
					>
						{record.bill_code && record.bill_code.value}
					</span>
				);
			};
		}
		return item;
	});
	return meta;
}
