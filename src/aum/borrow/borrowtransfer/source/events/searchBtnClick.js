import { ajax, toast } from 'nc-lightapp-front';
import {
	pageConfig,
	headcode,
	bodycode,
	pagecode,
	searchcode,
	search_templetid,
	head_pkfield,
	body_pkfield,
	appcode
} from '../const';
import ampub from 'ampub';

const { components, utils } = ampub;
const { getContext } = components.LoginContext;
const { MsgConst, showMessage } = utils.msgUtils;

let { sourceInfoField, dataSource } = pageConfig;
//点击查询，获取查询区数据
export default function clickSearchBtn(props, searchVal) {
	let data = props.search.getQueryInfo(searchcode, true);
	let sourceInfo = getContext(sourceInfoField, dataSource);
	let billtype = sourceInfo.billtype;
	let transtype = sourceInfo.transtype;
	let sourceAppcode = sourceInfo.appcode;
	if (!sourceAppcode) {
		sourceAppcode = appcode;
	}
	let metaData = props.meta.getMeta();
	if (data) {
		data.billtype = billtype;
		data.transtype = transtype;
		data.pagecode = pagecode;
		data.appcode = sourceAppcode;
		data.userdefObj = { busiTypes: sourceInfo.busitypes };
	}
	ajax({
		url: '/nccloud/aum/borrow/srcapply_query.do',
		data,
		success: (res) => {
			if (res.data) {
				props.transferTable.setTransferTableValue(headcode, bodycode, res.data, head_pkfield, body_pkfield);
				showMessage.call(this, props, { type: MsgConst.Type.QuerySuccess });
			} else {
				props.transferTable.setTransferTableValue(headcode, bodycode, [], head_pkfield, body_pkfield);
				showMessage.call(this, props, { type: MsgConst.Type.QueryNoData });
			}
		}
	});
}
