import { maincode, mainpagecode, searchcode, appcode, pageConfig } from '../const';
import ampub from 'ampub';

const { components, commonConst } = ampub;
const { UISTATE } = commonConst.StatusUtils;
const { getContext } = components.LoginContext;

let { sourcePageCode, dataSource, sourceInfoField } = pageConfig;
let sourceAppCode = appcode;
let { sourceUrl } = pageConfig.url;

//主子拉平模板
export default function initTemplateFull(props) {
	let sourceInfo = getContext(sourceInfoField, dataSource);
	if (sourceInfo.appcode) {
		sourceAppCode = sourceInfo.appcode;
	}
	props.createUIDom(
		{
			pagecode: mainpagecode, //页面id
			appcode: appcode //借用申请页面编码
		},
		function(data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = addLink(props, meta);
					props.meta.addMeta(meta);
				}
			}
		}
	);
}

//添加超链接
function addLink(props, meta) {
	meta[maincode].items = meta[maincode].items.map((item, key) => {
		if (item.attrcode == 'bill_code') {
			item.render = (text, record, index) => {
				return (
					<span
						className="code-detail-link"
						onClick={() => {
							props.openTo(sourceUrl, {
								pagecode: sourcePageCode,
								status: UISTATE.browse,
								appcode: sourceAppCode,
								id: record.pk_apply.value
							});
						}}
					>
						{record.bill_code && record.bill_code.value}
					</span>
				);
			};
		}
		return item;
	});
	return meta;
}
