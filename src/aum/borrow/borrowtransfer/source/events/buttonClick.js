import { ajax, toast } from 'nc-lightapp-front';
import ampub from 'ampub';

const { utils } = ampub;
const { getMultiLangByID } = utils.multiLangUtils;
const { MsgConst, showMessage } = utils.msgUtils;

/**
 * 按钮点击
 * @param {*} props 
 * @param {*} id 
 */
export default function buttonClick(props, id) {
	switch (id) {
		case 'Close': //不发放
			let selectData = props.transferTable.getTransferTableSelectedValue();
			if (selectData && selectData.head && selectData.head.length > 0) {
				let backGroudData = getPkTs(selectData);
				let bodyPkArray = getBodyPks(selectData);
				ajax({
					url: '/nccloud/aum/borrow/close.do',
					data: {
						pks: backGroudData
					},
					success: (res) => {
						props.transferTable.deleteItems(bodyPkArray);
						let title = getMultiLangByID('452000508A-000000'); /*不发放成功*/
						toast({ title, color: 'success' });
					}
				});
			} else {
				showMessage.call(this, props, { type: MsgConst.Type.ChooseOne }); /*选择一行数据*/
			}

			break;
	}
}

//根据返回的数据获取后台传输数据
function getPkTs(data) {
	if (data && data.head) {
		let allData = data.head;
		let pkTsArray = new Array(); //[{head : {pk:'',ts:''},body:[{pk:'',ts:''}]}] 后台可接收的数据格式
		for (let i = 0; i < allData.length; i++) {
			//根据获取的数据格式组装
			let singleData = { head: { pk: '', ts: '' }, bodys: [ { pk: '', ts: '' } ] };
			singleData.head.pk = allData[i].head.head.rows[0].values.pk_apply.value;
			singleData.head.ts = allData[i].head.head.rows[0].values.ts.value;
			let bodyRowDatas = allData[i].body.bodyvos.rows;
			if (bodyRowDatas) {
				let bodydata = bodyRowDatas.map(function(item) {
					let bodyRowData = { pk: item.values.pk_apply_b.value, ts: item.values.ts.value };
					return bodyRowData;
				});
				singleData.bodys = bodydata;
			}
			//数组中表头一致的合并成一条数据
			if (pkTsArray && pkTsArray.length > 0) {
				let isExist = false;
				for (let j = 0; j < pkTsArray.length; j++) {
					if (singleData.head.pk === pkTsArray[j].head.pk) {
						isExist = true;
						pkTsArray[j].body.concat(singleData.body);
						break;
					}
				}
				if (!isExist) {
					pkTsArray.push(singleData);
				}
			} else {
				pkTsArray.push(singleData);
			}
		}
		return pkTsArray;
	}
}
//获取所有表体主键
function getBodyPks(data) {
	if (data && data.head) {
		let allData = data.head;
		let bodyPkArray = new Array(); //所有表体主键
		for (let i = 0; i < allData.length; i++) {
			let bodyRowDatas = allData[i].body.bodyvos.rows;
			if (bodyRowDatas) {
				let singleBodyPks = bodyRowDatas.map(function(item) {
					return item.values.pk_apply_b.value;
				});
				bodyPkArray = bodyPkArray.concat(singleBodyPks);
			}
		}
		return bodyPkArray;
	}
}
