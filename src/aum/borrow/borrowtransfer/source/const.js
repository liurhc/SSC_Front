
/**
 * 主子结构页面编码
 */
export const pagecode = '452000504A_transferlist';

/**
 * 小应用主键
*/
export const appcode = '452000504A';

/**
 * 表头列表code
*/
export const headcode='head';

/**
 * 表体列表code
*/
export const bodycode='bodyvos';
/**
 * 主子拉平页面编码
 */
export const mainpagecode = '452000504A_transfermain';
/**
 * 主子拉平显示面板code
*/
export const maincode='transfermain';

/**
 * 查询区域code
*/
export const searchcode='searchArea';
/**
 * 查询区域模板主键
*/
export const search_templetid='1001Z91000000000RY9D';

/**
 * 主表主键字段
*/
export const head_pkfield='pk_apply';

/**
 * 子表主键字段
*/
export const body_pkfield='pk_apply_b';

export const pageConfig = {
    //转单缓存标识(备注：需要与下游保持一致)
    dataSource: 'aum.borrow.borrowtransfer.apply',
    sourcePageCode:'452000504A_card',
    //缓存上游信息的字段
    sourceInfoField:'sourceInfo',
    url :{
        sourceUrl:'/aum/borrow/apply/main/#/card',
        initSourceUrl:'/nccloud/ampub/common/querySrcAppInfo.do'
    }
}