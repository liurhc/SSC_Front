import { loadDataByPk } from './buttonClick';

export default function(props, pk) {
	if (!pk || pk == 'null') {
		return;
	}
	props.setUrlParam({ id: pk });
	loadDataByPk.call(this, props, pk);
}
