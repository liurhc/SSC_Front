import { ajax, print, toast, cardCache, output } from 'nc-lightapp-front';
import { headAfterEvent } from './afterEvent';
import { pageConfig } from '../const';
import { appConfig } from '../../const';
import ampub from 'ampub';

const { components, commonConst, utils } = ampub;
const { getContext, loginContextKeys } = components.LoginContext;
const { UISTATE, BILLSTATUS } = commonConst.StatusUtils;
const { getMultiLangByID } = utils.multiLangUtils;
const { showConfirm, MsgConst, showMessage } = utils.msgUtils;
const { beforeSaveValidator } = components.saveValidatorsUtil;
const { getScriptCardReturnData } = components.ScriptReturnUtils;
const { setCardValue, setHeadAreaData, setBillFlowBtnsVisible } = utils.cardUtils;

const { formId, tableId, pagecode, editButtons, browseButtons } = pageConfig;
const {
	dataSource,
	reqUrl,
	printNodekey,
	pkField,
	funcode,
	bill_type,
	listRouter,
	printFileName,
	pagecode_list
} = appConfig;

export default function(props, id) {
	switch (id) {
		//新增按钮
		case 'Add':
			add.call(this, props);
			break;
		//修改按钮
		case 'Edit':
			edit.call(this, props);
			break;
		//删除按钮
		case 'Delete':
			delConfirm.call(this, props);
			break;
		//保存按钮
		case 'Save':
			saveClick.call(this, props);
			break;
		//取消按钮
		case 'Cancel':
			// cancel.call(this, props);
			cancelConfirm.call(this, props);
			break;
		case 'AddLine':
			addLine.call(this, props, true);
			break;
		case 'DelLine':
			delLine.call(this, props);
			break;
		case 'Commit':
			commitAction.call(this, props, 'SAVE');
			break;
		case 'UnCommit':
			commitAction.call(this, props, 'UNSAVE');
			break;
		case 'SaveCommit':
			savecommit.call(this, props);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'Attachment':
			attachmentClick.call(this, props);
			break;
		case 'Copy':
			let pk_apply = props.form.getFormItemsValue(pageConfig.formId, 'pk_apply').value;
			copy.call(this, props, pk_apply);
			break;
		case 'Refresh':
			refresh.call(this, props);
			break;
		case 'BatchAlter':
			props.cardTable.batchChangeTableData(tableId);
			break;
		case 'QueryAboutBusiness':
			let pk1 = props.form.getFormItemsValue(formId, 'pk_apply').value;
			this.setState({
				show: true,
				pk_apply: pk1
			});
			break;
		case 'QueryAboutBillFlow':
			let pk = props.form.getFormItemsValue(formId, 'pk_apply').value;
			let transtype = props.form.getFormItemsValue(formId, 'transi_type').value;
			this.setState({
				showApprove: true,
				pk_apply: pk,
				transtype
			});
			break;
		default:
			break;
	}
}

/**
 * 新增
 * @param {*} props 
 */
export function add(props) {
	// 清空数据
	props.form.EmptyAllFormValue(formId);
	props.cardTable.setTableData(tableId, { rows: [] });
	setDefaultValue.call(this, props);
	setStatus.call(this, props, UISTATE.add);
	// 设字段的可编辑性
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	if (pk_org_v && pk_org_v.value) {
		headAfterEvent.call(this, props, formId, 'pk_org_v', pk_org_v, null);
	} else {
		props.initMetaByPkorg('pk_org_v');
		setBodyBtnsEnable.call(this, props, tableId);
	}
}

/**
 * 设置默认值
 * @param {*} props 
 */
function setDefaultValue(props) {
	// 公共默认值处理
	// 集团
	let groupId = getContext(loginContextKeys.groupId);
	let groupName = getContext(loginContextKeys.groupName);
	// 交易类型
	let transi_type = getContext(loginContextKeys.transtype);
	let pk_transitype = getContext(loginContextKeys.pk_transtype);
	// 默认主组织
	let pk_org = getContext(loginContextKeys.pk_org);
	let org_Name = getContext(loginContextKeys.org_Name);
	let pk_org_v = getContext(loginContextKeys.pk_org_v);
	let org_v_Name = getContext(loginContextKeys.org_v_Name);
	props.form.setFormItemsValue(formId, {
		pk_group: { value: groupId, display: groupName },
		pk_org: { value: pk_org, display: org_Name },
		pk_org_v: { value: pk_org_v, display: org_v_Name },
		bill_type: { value: bill_type },
		transi_type: { value: transi_type },
		pk_transitype: { value: pk_transitype },
		complete_flag: { value: false },
		bill_status: { value: BILLSTATUS.free_check, display: getMultiLangByID('statusUtils-000000') /*'自由态' */ }
	});
}

/**
 * 修改
 */
function edit(props) {
	let pk = props.form.getFormItemsValue(formId, 'pk_apply').value;
	ajax({
		url: reqUrl.edit,
		data: {
			pk,
			resourceCode: funcode
		},
		success: (res) => {
			if (res.data) {
				toast({ color: 'warning', content: res.data });
			} else {
				props.cardTable.closeExpandedRow(tableId);
				setStatus.call(this, props, UISTATE.edit);
			}
		}
	});
}

/**
* 删除
* @param {*} props 
*/
function delConfirm(props) {
	showConfirm.call(this, props, {
		type: MsgConst.Type.Delete,
		beSureBtnClick: delReal
	});
}

//删除单据
function delReal(props) {
	let paramInfoMap = {};
	let id = props.form.getFormItemsValue(formId, pkField).value;
	let ts = props.form.getFormItemsValue(formId, 'ts').value;
	paramInfoMap[id] = ts;
	ajax({
		url: reqUrl.delete,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: pagecode
		},
		success: (res) => {
			if (res.data.success === 'allsuccess') {
				// 清除缓存
				cardCache.deleteCacheById(pkField, id, dataSource);
				toast({ content: res.data.successMsg, color: 'success' });
				// 加载下一条数据
				let newpk = cardCache.getNextId(id, dataSource);
				loadDataByPk.call(this, props, newpk);
			} else {
				toast({ content: res.data.errorMsg, color: 'warning' });
			}
		}
	});
}

/**
* 新增行
* @param {*} props 
*/
export function addLine(props, isFocus = false) {
	props.cardTable.addRow(tableId, undefined, { quantity: { value: '1' } }, isFocus);
	setBodyBtnsEnable.call(this, props, tableId);
}

// 删除行
export function delLine(props) {
	let checkedRows = props.cardTable.getCheckedRows(tableId);
	let checkedIndex = [];
	checkedRows.map((item) => {
		checkedIndex.push(item.index);
	});
	props.cardTable.delRowsByIndex(tableId, checkedIndex);
	setBodyBtnsEnable.call(this, props, tableId);
}

//保存单据
export function saveClick(props) {
	let flag = validateBeforeSave.call(this, props);
	if (!flag) {
		return;
	}
	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let url = reqUrl.insert; //新增保存
	let status = props.form.getFormStatus(formId);
	if (status === UISTATE.edit) {
		url = reqUrl.update; //修改保存
	}
	props.cardTable.closeModel(tableId);
	//保存前执行验证公式
	props.validateToSave(CardData, () => {
		ajax({
			url: url,
			data: CardData,
			success: (res) => {
				if (res.success) {
					if (res.data) {
						afterSave.call(this, props, res.data, status);
					}
				}
			}
		});
	});
}

/**
* 保存前校验
* @param {*} props 
*/
function validateBeforeSave(props) {
	let pass = beforeSaveValidator.call(this, props, formId, tableId, [ 'quantity', 'close_flag', 'borrow_quantity' ]);
	if (!pass) {
		return false;
	}
	// 表体必输校验
	let allRows = props.cardTable.getVisibleRows(tableId);
	if (!allRows || allRows.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.BodyNotNull });
		return false;
	} else {
		let errs1 = []; //检验借用归还日期，必须大于借用日期
		let errs2 = []; //检验借用日期必须晚于当前日期
		allRows.map((row, index) => {
			let expect_start_date = row.values['expect_start_date'];
			let expect_end_date = row.values['expect_end_date'];
			if (expect_start_date && expect_start_date.value) {
				let start = new Date(expect_start_date.value).getTime();
				let curr = new Date(getContext(loginContextKeys.businessDate).substring(0, 10) + ' 00:00:00').getTime();
				if (start < curr) {
					errs2.push(index + 1);
				}
				if (expect_end_date && expect_end_date.value) {
					let end = new Date(expect_end_date.value).getTime();
					if (start > end) {
						errs1.push(index + 1);
					}
				}
			}
		});
		let errMsg = '';
		if (errs1 && errs1.length > 0) {
			errMsg =
				'<div>' +
				getMultiLangByID('452000504A-000000', {
					index: [ ...errs1 ].join('、')
				}) +
				'</div>' /*第{index}行【借用日期】不能晚于【预计归还日期】*/;
		}
		if (errs2 && errs2.length > 0) {
			errMsg =
				errMsg +
				'<div>' +
				getMultiLangByID('452000504A-000001', {
					index: [ ...errs2 ].join('、')
				}) +
				'</div>' /*第{index}行【借用日期】不能早于预【当前日期】*/;
		}
		if (errMsg) {
			toast({ content: errMsg, color: 'danger', isNode: true });
			return false;
		}
	}
	let flag = props.cardTable.checkTableRequired(tableId);
	if (!flag) {
		return false;
	}
	return true;
}

/**
 * 刷新
 * @param {*} props 
 */
function refresh(props) {
	let pk = props.form.getFormItemsValue(formId, pkField);
	if (!pk || !pk.value) {
		return;
	}
	// 查询到最新单据数据
	getDataByPk.call(this, props, pk.value, 'browse', true);
}

/**
 * 复制
 * @param {*} props 
 */
export function copy(props, pk_apply) {
	let data = { pk: pk_apply, pagecode };
	ajax({
		url: reqUrl.copy,
		data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (res.data) {
					setValue.call(this, props, data);
					setStatus.call(this, props, UISTATE.copyAdd);
				} else {
					showMessage.call(this, props, { type: MsgConst.Type.DataDeleted });
				}
			}
		}
	});
}

//附件
function attachmentClick(props) {
	let billId = props.form.getFormItemsValue(formId, pkField).value;
	props.ncUploader.show('uploder', {
		billId: 'pam/borrow_apply/' + billId
	});
}

/**
 * 修改
 */
function edit(props) {
	let pk = props.form.getFormItemsValue(formId, 'pk_apply').value;
	ajax({
		url: reqUrl.edit,
		data: {
			pk,
			resourceCode: funcode
		},
		success: (res) => {
			if (res.data) {
				toast({ color: 'warning', content: res.data });
			} else {
				props.cardTable.closeExpandedRow(tableId);
				setStatus.call(this, props, 'edit');
			}
		}
	});
}

/**
* 取消
* @param {*} props 
*/
export function cancelConfirm(props) {
	showConfirm.call(this, props, {
		type: MsgConst.Type.Cancel,
		beSureBtnClick: (props) => {
			cancel.call(this, props);
		}
	});
}

/**
 * 取消
 */
function cancel(props) {
	let pk_apply = props.form.getFormItemsValue(pageConfig.formId, 'pk_apply').value;
	if (pk_apply) {
		props.form.cancel(formId);
		props.cardTable.resetTableData(tableId);
		loadDataByPk.call(this, props, pk_apply);
	} else {
		//新增取消如果没有选择过组织需要调用此方法恢复字段的可编辑性
		let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
		if (!pk_org_v || !pk_org_v.value) {
			props.resMetaAfterPkorgEdit();
		}
		let pk = cardCache.getCurrentLastId(dataSource);
		if (pk) {
			loadDataByPk.call(this, props, pk);
		} else {
			props.form.EmptyAllFormValue(formId);
			props.cardTable.setTableData(tableId, { rows: [] });
		}
	}
	setStatus.call(this, props, 'browse');
}

/**
 * 通过单据id获取缓存单据信息，没有缓存则重新查询
 * @param {*} props 
 * @param {*} pk 
 */
export function loadDataByPk(props, pk) {
	if (!pk || pk == 'null') {
		setValue.call(this, props, undefined);
		return;
	}
	let cachData = cardCache.getCacheById(pk, dataSource);
	if (cachData) {
		setValue.call(this, props, cachData);
	} else {
		getDataByPk.call(this, props, pk);
	}
}

/**
 * 打印
 */
function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		reqUrl.printCardUrl, // 后台打印服务url
		printData
	);
}

/**
 * 输出
 * @param {*} props 
 */
function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });
		return;
	}
	output({
		url: reqUrl.printCardUrl,
		data: printData
	});
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let pk = props.form.getFormItemsValue(formId, 'pk_apply');
	if (!pk || !pk.value) {
		return false;
	}
	let pks = [ pk.value ];
	let printData = {
		filename: printFileName, // 文件名称
		nodekey: printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

/**
 * 保存提交
 * @param {*} props 
 */
export function savecommit(props, content) {
	let flag = validateBeforeSave.call(this, props);
	if (!flag) {
		return;
	}
	let pk = props.form.getFormItemsValue(formId, 'pk_apply').value;
	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let paramInfoMap = {};
	paramInfoMap[pk] = ''; //卡片页面ts为空也可以 CardData 里有ts
	let obj = {
		dataType: 'cardData',
		OperatorType: 'SAVE',
		commitType: 'saveCommit',
		pageid: pagecode,
		paramInfoMap: paramInfoMap,
		content: content
	};
	CardData.userjson = JSON.stringify(obj);
	//保存提交前执行校验公式
	props.validateToSave(CardData, () => {
		ajax({
			url: reqUrl.commitOrUn,
			data: CardData,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if (content) {
						this.setState({
							compositedisplay: false
						});
					}
					let setCommonValue = (data) => {
						setValue.call(this, props, data);
					};
					let callback = () => {
						setStatus.call(this, props, UISTATE.browse);
					};
					getScriptCardReturnData.call(
						this,
						res,
						props,
						formId,
						tableId,
						pkField,
						dataSource,
						setCommonValue,
						false,
						callback,
						pagecode
					);
				}
			},
			error: (res) => {
				if (res && res.message) {
					if (content) {
						this.setState({
							compositedisplay: false
						});
					}
					toast({ color: 'danger', content: res.message });
				}
			}
		});
	});
}

/**
 * 提交
 */
export function commitAction(props, OperatorType, content) {
	let commitType = '';
	if (OperatorType == 'SAVE') {
		commitType = 'commit';
	}
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let paramInfoMap = {};
	paramInfoMap[pk] = ''; //卡片页面ts为空也可以 CardData 里有ts
	let obj = {
		dataType: 'cardData',
		OperatorType: OperatorType,
		commitType: commitType,
		pageid: pagecode,
		paramInfoMap: paramInfoMap,
		content: content
	};
	CardData.userjson = JSON.stringify(obj);
	ajax({
		url: reqUrl.commitOrUn,
		data: CardData,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (content) {
					this.setState({
						compositedisplay: false
					});
				}
				let setCommonValue = (data) => {
					setValue.call(this, props, data);
				};
				let callback = () => {
					setStatus.call(this, props, UISTATE.browse);
				};
				getScriptCardReturnData.call(
					this,
					res,
					props,
					formId,
					tableId,
					pkField,
					dataSource,
					setCommonValue,
					false,
					callback,
					pagecode
				);
			}
		},
		error: (res) => {
			if (res && res.message) {
				if (content) {
					this.setState({
						compositedisplay: false
					});
				}
				toast({ color: 'danger', content: res.message });
			}
		}
	});
}

//通过单据id查询单据信息
export function getDataByPk(props, pk, status, isRefresh = false) {
	ajax({
		url: reqUrl.queryCardData,
		data: { pk, pagecode },
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					setValue.call(this, props, data);
					cardCache.updateCache('pk_apply', pk, data, formId, dataSource);
					if (isRefresh) {
						showMessage.call(this, props, { type: MsgConst.Type.RefreshSuccess });
					}
				} else {
					cardCache.deleteCacheById(pkField, pk, dataSource);
					setValue.call(this, props, undefined);
					showMessage.call(this, props, { type: MsgConst.Type.DataDeleted });
				}
			}
		}
	});
}

/**
* 保存后
* @param {*} props 
*/
function afterSave(props, data, oldstatus) {
	setValue.call(this, props, data);
	setStatus.call(this, props, UISTATE.browse);
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	// 保存成功后处理缓存
	let CardData = props.createMasterChildData(pagecode, formId, tableId);
	if (oldstatus == UISTATE.add) {
		cardCache.addCache(pk, CardData, formId, dataSource);
	} else {
		cardCache.updateCache(pkField, pk, CardData, formId, dataSource);
	}
	showMessage.call(this, props, { type: MsgConst.Type.SaveSuccess });
	let showdata = { status: UISTATE.browse, bill_code: '' };
	setHeadAreaData.call(this, this.props, showdata);
}

/**
 * 设置界面值
 * @param {*} props 
 * @param {*} data 
 */
export function setValue(props, data) {
	let status = props.form.getFormStatus(formId) || UISTATE.browse;
	setCardValue.call(this, props, data);
	if (status == UISTATE.browse) {
		setBrowseBtnsVisible.call(this, props);
	} else {
		setBodyBtnsEnable.call(this, props, tableId);
	}
}

/**
 * 设置界面状态
 * @param {*} props 
 * @param {*} status 
 */
export function setStatus(props, status = UISTATE.browse) {
	// 更新参数
	let pkVal = props.form.getFormItemsValue(formId, pkField);
	props.setUrlParam({ status, id: pkVal ? pkVal.value : '' });
	switch (status) {
		case UISTATE.add:
			props.form.setFormItemsDisabled(formId, {
				pk_org_v: false,
				pk_org: false
			});
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, UISTATE.edit);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(browseButtons, false);
			props.button.setButtonVisible(editButtons, true);
			setBodyBtnsEnable.call(this, props, tableId);
			break;
		case UISTATE.copyAdd:
			props.form.setFormItemsDisabled(formId, {
				pk_org_v: true,
				pk_org: true
			});
			props.form.setFormStatus(formId, UISTATE.add);
			props.cardTable.setStatus(tableId, UISTATE.edit);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(browseButtons, false);
			props.button.setButtonVisible(editButtons, true);
			setBodyBtnsEnable.call(this, props, tableId);
			break;
		case UISTATE.edit:
			props.form.setFormItemsDisabled(formId, {
				pk_org_v: true,
				pk_org: true
			});
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, status);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(browseButtons, false);
			props.button.setButtonVisible(editButtons, true);
			setBodyBtnsEnable.call(this, props, tableId);
			break;
		case UISTATE.browse:
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, status);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(editButtons, false);
			setBrowseBtnsVisible.call(this, props);
			break;
		default:
			break;
	}
	setHeadAreaData.call(this, props, { status });
}

/**
 * 浏览态设置按钮显示隐藏
 * @param {*} props 
 */
function setBrowseBtnsVisible(props) {
	let pkVal = props.form.getFormItemsValue(formId, pkField);
	if (!pkVal || !pkVal.value) {
		props.button.setButtonVisible(browseButtons, false);
		props.button.setButtonVisible('Add', true);
		return;
	}
	props.button.setButtonVisible(browseButtons, true);
	setBillFlowBtnsVisible.call(this, props, formId, pkField);
}

/**
 * 设置表体按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBodyBtnsEnable(props, moduleId) {
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	if (!pk_org_v || !pk_org_v.value) {
		props.button.setButtonDisabled([ 'AddLine', 'DelLine', 'BatchAlter' ], true);
		return;
	}
	props.button.setButtonDisabled([ 'AddLine', 'BatchAlter' ], false);
	let checkedRows = [];
	let num = props.cardTable.getNumberOfRows(moduleId, false);
	if (num > 0) {
		checkedRows = props.cardTable.getCheckedRows(moduleId);
	}
	props.button.setButtonDisabled([ 'DelLine' ], !(checkedRows && checkedRows.length > 0));
}

/**
* 返回
* @param {*} props 
*/
export function backToList(props) {
	props.pushTo(listRouter, { pagecode: pagecode_list });
}
