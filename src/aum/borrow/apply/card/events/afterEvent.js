import { ajax } from 'nc-lightapp-front';
import { setValue, addLine } from './buttonClick';
import { pageConfig } from '../const';
import { appConfig } from '../../const';
import ampub from 'ampub';

const { components, utils } = ampub;
const { orgChangeEvent } = components.OrgChangeEvent;
const { commonHeadAfterEvent, commonBodyAfterEvent } = utils.cardUtils;
const { formId, tableId, pagecode, fields } = pageConfig;
const { reqUrl } = appConfig;

/**
 * 表头编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} oldValue 
 */
export function headAfterEvent(props, moduleId, key, value, oldValue) {
	//表头编辑后事件取值
	let data = props.createHeadAfterEventData(pagecode, formId, tableId, moduleId, key, value);
	//资产组织版本
	if (key == 'pk_org_v') {
		//组织切换确定后发送后台请求
		let callback = () => {
			if (value.value) {
				// 若组织切换后有值则自动增行
				addLine.call(this, props);
			}
			data = props.createHeadAfterEventData(pagecode, formId, tableId, moduleId, key, value);
			dealData4AfterEvent.call(this, props, data);
		};
		let tableBtns = [ 'AddLine', 'BatchAlter' ];
		//组织切换
		orgChangeEvent.call(this, props, pagecode, formId, tableId, key, value, oldValue, fields, tableBtns, callback);
	}
	//借用人
	let config = {
		afterEditUrl: reqUrl.headAfterEditUrl,
		pagecode,
		key,
		value,
		oldValue,
		keys: [ 'pk_user' ]
	};
	commonHeadAfterEvent.call(this, props, config);
}

/**
 * 表体编辑后事件
 * 
 */
export function bodyAfterEvent(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	// 平台的自动增行不会附默认值，这里调用自己的增行,转单不自动增行
	if (this.srctype != 'transfer') {
		let num = props.cardTable.getNumberOfRows(tableId);
		if (num == index + 1) {
			addLine.call(this, props);
		}
	}
	if (!isEqual(changedRows)) {
		let config = {
			afterEditUrl: reqUrl.bodyAfterEditUrl,
			pagecode,
			key,
			record,
			changedRows,
			index,
			keys: [ 'pk_equip', 'pk_category' ]
		};
		commonBodyAfterEvent.call(this, props, config);
	}
}

function isEqual(changedrows) {
	if (changedrows) {
		if (Object.prototype.toString.call(changedrows) === '[object Array]') {
			return false;
		}
		let newValue = changedrows.newvalue;
		let oldValue = changedrows.oldvalue;
		if (!oldValue) {
			oldValue = '';
		}
		if (!newValue) {
			newValue = '';
		}
		if (newValue == oldValue) {
			return true;
		}
		return false;
	}
}

/**
 * 调后台处理编辑后事件的数据 
 * @param {*} props 
 * @param {*} data 
 */
function dealData4AfterEvent(props, data, url = reqUrl.headAfterEditUrl) {
	ajax({
		url,
		data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				setValue.call(this, props, data);
			}
		}
	});
}
