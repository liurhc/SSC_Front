import { pageConfig } from '../const';
import { appConfig } from '../../const';
import tableButtonClick from './tableButtonClick';
import ampub from 'ampub';

const { components, commonConst, utils } = ampub;
const { defRefCondition } = components.refInit;
const { openEquipCardByPk } = components.queryAboutUtils;
const { getContext, loginContextKeys, loginContext } = components.LoginContext;
const { IBusiRoleConst } = commonConst.CommonKeys;
const { BILLSTATUS } = commonConst.StatusUtils;
const { createOprationColumn } = utils.cardUtils;

const { formId, tableId, pagecode } = pageConfig;
const { bill_type } = appConfig;

export default function(props) {
	let _this = this;
	props.createUIDom(
		{
			pagecode: pagecode //页面id
		},
		function(data) {
			if (data) {
				if (data.context) {
					loginContext(data.context);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.template) {
					let meta = data.template;
					modifierMeta.call(_this, props, meta);
					props.meta.setMeta(meta, () => {
						_this.afterInit(this, props);
					});
				}
			}
		}
	);
}

/**
 * 处理meta，增加操作列
 * @param {*} props 
 * @param {*} meta 
 */
function modifierMeta(props, meta) {
	//参照过滤
	meta = filterRef.call(this, props, meta);
	// 添加表格操作列
	let oprCol = createOprationColumn.call(this, props, {
		width: '180',
		tableButtonClick,
		getInnerBtns: getInnerBtns.bind(this)
	});
	meta[tableId].items.push(oprCol);

	return meta;
}

/**
 * 获取操作列按钮
 * @param {*} props 
 * @param {*} text 
 * @param {*} record 
 * @param {*} index 
 */
function getInnerBtns(props, text, record, index) {
	let status = props.cardTable.getStatus(tableId);
	let close = record.values.close_flag.value;
	let buttonAry = status === 'browse' ? [ 'OpenCard' ] : [ 'OpenCard', 'DelLine' ];
	if (status === 'browse' && !close) {
		let bill_status = props.form.getFormItemsValue(formId, 'bill_status').value;
		if (BILLSTATUS.check_pass == bill_status) {
			buttonAry = [ 'OpenCard', 'CloseLine' ];
		}
	} else if (status === 'browse' && close) {
		buttonAry = [ 'OpenCard', 'OpenLine' ];
	}
	return buttonAry;
}

/**
 * 参照过滤
 * @param {*} props 
 * @param {*} meta 
 */
function filterRef(props, meta) {
	const transi_type = getContext(loginContextKeys.transtype);
	const pk_group = getContext(loginContextKeys.groupId);
	//表头部门参照过滤
	meta[formId].items.map((item) => {
		if (item.attrcode == 'pk_org_v') {
			//组织
			item.queryCondition = () => {
				return { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgVidRefFilter' };
			};
		} else if (item.attrcode == 'pk_unit_used_v') {
			//使用权
			item.queryCondition = () => {
				let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value; // 调用相应组件的取值API
				return {
					pk_org_unitused: pk_org,
					TreeRefActionExt: 'nccloud.web.aum.assign.apply.refcondition.PK_UNIT_USED_VSqlBuilder',
					DataPowerOperationCode: 'AMUsedunit'
				}; // 根据pk_org过滤
			};
		} else if (item.attrcode == 'pk_usedept_v') {
			//部门
			item.queryCondition = () => {
				let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value; // 调用相应组件的取值API
				return {
					pk_org,
					busifuncode: IBusiRoleConst.ASSETORG,
					DataPowerOperationCode: 'AMUsedept'
				}; // 根据pk_org过滤
			};
		} else if (item.attrcode == 'pk_user') {
			//使用人
			item.isShowUnit = false;
			item.queryCondition = () => {
				let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value; // 调用相应组件的取值API
				let pk_usedept = props.form.getFormItemsValue(formId, 'pk_usedept').value; // 使用部门
				return { pk_org, pk_dept: pk_usedept, busifuncode: IBusiRoleConst.ASSETORG }; // 根据pk_org过滤
			};
		} else if (item.attrcode == 'pk_applier') {
			//申请人
			item.isShowUnit = false;
			item.queryCondition = () => {
				let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
				return {
					pk_org,
					busifuncode: IBusiRoleConst.ASSETORG,
					isTreelazyLoad: false
				};
			};
		} else {
			defRefCondition.call(this, props, item, formId, pk_group);
		}
	});
	//表体参照过滤
	meta[tableId].items.map((item) => {
		//设备卡片
		if (item.attrcode == 'pk_equip') {
			item.isMultiSelectedEnabled = true; //设置可以多选
			//设置超链接
			if (item.attrcode == 'pk_equip') {
				//给资产编码列添加超链接
				item.renderStatus = 'browse';
				item.render = (text, record, index) => {
					return (
						<div class="card-table-browse">
							<span
								className="code-detail-link"
								onClick={() => {
									openEquipCardByPk(props, record.values.pk_equip.value);
								}}
							>
								{record.values.pk_equip && record.values.pk_equip.display}
							</span>
						</div>
					);
				};
			}
			item.queryCondition = () => {
				let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
				return {
					pk_org: pk_org,
					transi_type,
					bill_type,
					GridRefActionExt: 'nccloud.web.aum.assign.apply.refcondition.PK_EQUIPSqlBuilder'
				}; // 根据pk_org过滤
			};
		} else if (item.attrcode == 'pk_category') {
			//设备类别
			item.isMultiSelectedEnabled = true;
			item.queryCondition = () => {
				return { pk_group };
			};
		} else if (item.attrcode == 'pk_material_v') {
			//物料
			//TODO 多选
			item.queryCondition = () => {
				let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
				return { pk_org };
			};
		} else if (item.attrcode == 'pk_jobmngfil') {
			//项目
			item.queryCondition = () => {
				let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
				return { pk_org };
			};
		} else {
			defRefCondition.call(this, props, item, formId, pk_group);
		}
	});
	return meta;
}
