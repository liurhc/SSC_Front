import { ajax, toast, cardCache } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import { appConfig } from '../../const';
import { setStatus } from './buttonClick';
import ampub from 'ampub';

const { utils } = ampub;
const { getMultiLangByID } = utils.multiLangUtils;

const { pkField, reqUrl, dataSource } = appConfig;
const { tableId, pagecode, formId } = pageConfig;

export default function tableButtonClick(props, key, text, record, index) {
	switch (key) {
		// 表格操作按钮
		case 'OpenCard':
			let status = props.cardTable.getStatus(tableId);
			if (status == 'edit') {
				props.cardTable.openModel(tableId, 'edit', record, index);
			} else if (status == 'browse') {
				props.cardTable.toggleRowView(tableId, record);
			}
			break;
		case 'DelLine':
			props.cardTable.delRowsByIndex(tableId, index);
			break;
		case 'OpenLine':
			openLine.call(this, props, record, index, tableId);
			break;
		case 'CloseLine':
			closeLine.call(this, props, record, index, tableId);
			break;
		default:
			break;
	}
}

/**
 * 关闭
 * @param {*} props 
 * @param {*} record 
 * @param {*} index 
 */
function closeLine(props, record, index, tableId) {
	let pk_apply = props.form.getFormItemsValue(formId, pkField).value;
	let CardData = props.createMasterChildData(pagecode, formId, tableId);
	let rows = [ record.values.pk_apply_b.value ];
	CardData.rows = rows;
	CardData.pagecode = pagecode;
	ajax({
		url: reqUrl.closeUrl,
		data: CardData,
		success: (res) => {
			if (res.data) {
				if (res.data.head) {
					props.form.setAllFormValue({ [formId]: res.data.head[formId] });
				}
				if (res.data.body) {
					props.cardTable.setTableData(tableId, res.data.body[tableId]);
				}
				setStatus.call(this, props);
				toast({ content: getMultiLangByID('452000504A-000005') /* 国际化处理：打开成功 */, color: 'success' });
			}
			cardCache.updateCache(pkField, pk_apply, res.data, formId, dataSource);
		}
	});
}

/**
 * 打开
 * @param {*} props 
 * @param {*} record 
 * @param {*} index 
 */
function openLine(props, record, index, tableId) {
	let CardData = props.createMasterChildData(pagecode, formId, tableId);
	let rows = [ record.values.pk_apply_b.value ];
	let pk_apply = props.form.getFormItemsValue(formId, pkField).value;
	CardData.rows = rows;
	CardData.pagecode = pagecode;
	ajax({
		url: reqUrl.openUrl,
		data: CardData,
		success: (res) => {
			if (res.data) {
				if (res.data.head) {
					props.form.setAllFormValue({ [formId]: res.data.head[formId] });
				}
				if (res.data.body) {
					props.cardTable.setTableData(tableId, res.data.body[tableId]);
				}
				setStatus.call(this, props);
				toast({ content: getMultiLangByID('452000504A-000004') /* 国际化处理：打开成功 */, color: 'success' });
			}
			cardCache.updateCache(pkField, pk_apply, res.data, formId, dataSource);
		}
	});
}
