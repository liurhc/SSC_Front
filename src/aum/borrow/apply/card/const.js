//页面常量配置
export const pageConfig = {
	//表头区域编码
	formId: 'card_head',
	//表体区域编码
	tableId: 'bodyvos',
	//页面编码
	pagecode: '452000504A_card',
	//编辑态按钮
	editButtons: [ 'Save', 'SaveCommit', 'Cancel', 'AddLine', 'DelLine', 'BatchAlter' ],
	//浏览态按钮
	browseButtons: [
		'Add',
		'Edit',
		'Delete',
		'Copy',
		'Commit',
		'UnCommit',
		'QueryAboutBillFlow',
		'Attachment',
		'Print',
		'Refresh'
	],

	//组织切换事件需要清空的表头字段
	fields: [ 'pk_unit_used_v', 'pk_unit_used', 'pk_usedept_v', 'pk_usedept', 'pk_user', 'pk_applier', 'reason' ]
};
