//主子表卡片
import React, { Component } from 'react';
import { createPage, base, high } from 'nc-lightapp-front';
import {
	buttonClick,
	initTemplate,
	headAfterEvent,
	bodyAfterEvent,
	bodyBeforeEvent,
	pageInfoClick,
	rowSelected
} from './events';
import { saveClick, copy, setStatus, loadDataByPk, commitAction, savecommit, backToList } from './events/buttonClick';
import { pageConfig } from './const';
import { appConfig } from '../const';
import ampub from 'ampub';

const { components, commonConst, utils } = ampub;
const { UISTATE } = commonConst.StatusUtils;
const { getMultiLangByID } = utils.multiLangUtils;
const { createCardTitleArea, createCardPaginationArea } = utils.cardUtils;
const { closeBrowserUtils } = utils;
const { ApprovalTrans } = components;

const { tableId, formId, pagecode } = pageConfig;
const { dataSource, bill_type, pkField, title } = appConfig;
const { NCAffix } = base;
const { BillTrack, ApproveDetail } = high;

class BorrowApplyCard extends Component {
	constructor(props) {
		super(props);
		this.state = {
			transi_type: '4A38-01',
			pk_apply: '',
			show: false,
			showApprove: false,
			compositedisplay: false,
			compositedata: {}
		};
		//编辑态，浏览器刷新或者关闭弹出提示框
		closeBrowserUtils.call(this, props, { form: [ formId ], cardTable: [ tableId ] });
		initTemplate.call(this, props);
	}
	componentDidMount() {}

	afterInit = () => {
		let { props } = this;
		let status = props.getUrlParam('status') || UISTATE.browse;
		let pk_apply = props.getUrlParam('id');
		if (status == UISTATE.add) {
			buttonClick.call(this, props, 'Add');
		} else if (status == UISTATE.copyAdd) {
			copy.call(this, props, pk_apply);
		} else {
			if (pk_apply && pk_apply != 'undefined') {
				loadDataByPk.call(this, props, pk_apply, status);
			}
			setStatus.call(this, props, status);
			// if (status == UISTATE.edit) {
			// 	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
			// 	if (!pk_org_v || !pk_org_v.value) {
			// 		props.initMetaByPkorg('pk_org_v');
			// 	}
			// }
		}
	};

	//提交及指派 回调
	getAssginUsedr = (value) => {
		let pk = this.props.form.getFormItemsValue(formId, pkField).value;
		if (pk) {
			commitAction.call(this, this.props, 'SAVE', value);
		} else {
			savecommit.call(this, this.props, value);
		}
	};
	//取消 指派
	turnOff = () => {
		this.setState({
			compositedisplay: false
		});
	};

	//获取列表肩部信息
	getTableHeadLeft = () => {
		let { cardTable } = this.props;
		let status = cardTable.getStatus(tableId) || UISTATE.browse;
		let rowCount = 0;
		let allRows = cardTable.getVisibleRows(tableId);
		if (allRows) {
			rowCount = allRows.length;
		}
		let str1 = getMultiLangByID('cardUtils-000000', { num: rowCount }); /*总计{rowCount}行*/
		let messages1 = [];
		if (str1) {
			messages1 = str1.split(rowCount);
		}
		let applycount = 0;
		allRows.map((item) => {
			applycount += parseInt(item.values.quantity.value);
		});
		let str2 = getMultiLangByID('452000504A-000002', { num: applycount }); /*申请数量{rowCount}个*/
		let messages2 = [];
		if (str2) {
			messages2 = str2.split(applycount);
		}
		return (
			<div className="shoulder-definition-area">
				<div className="definition-search">
					{status == 'browse' && (
						<div>
							<span className="definition-search-title">{messages1[0]}</span>
							<span className="count">{rowCount}</span>
							<span>{messages1[1]}</span>
							<span> {messages2[0]}</span>
							<span className="count">{applycount}</span>
							<span>{messages2[1]}</span>
						</div>
					)}
				</div>
			</div>
		);
	};

	//获取列表肩部信息
	getTableHead = () => {
		let { button } = this.props;
		let { createButtonApp } = button;
		let status = this.props.form.getFormStatus(formId);
		return (
			<div className="shoulder-definition-area">
				<div className="definition-icons">
					{createButtonApp({
						area: 'card_body',
						buttonLimit: 3,
						onButtonClick: buttonClick.bind(this),
						popContainer: document.querySelector('.nc-bill-table-area')
					})}
				</div>
			</div>
		);
	};

	render() {
		let { cardTable, form, button, ncUploader } = this.props;
		let { createNCUploader } = ncUploader;
		// let buttons = this.props.button.getButtons();
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createButtonApp } = button;
		return (
			<div id="nc-bill-card" className="nc-bill-card">
				<div className="nc-bill-top-area">
					<NCAffix>
						<div className="nc-bill-header-area">
							{/* 标题 title */}
							{createCardTitleArea.call(this, this.props, {
								title: getMultiLangByID(title),
								formId,
								backBtnClick: backToList.bind(this)
							})}
							<div className="header-button-area">
								{createButtonApp({
									area: 'card_head',
									onButtonClick: buttonClick.bind(this),
									popContainer: document.querySelector('.header-button-area')
								})}
							</div>
							{createCardPaginationArea.call(this, this.props, {
								formId,
								dataSource,
								pageInfoClick
							})}
						</div>
					</NCAffix>
					<div className="nc-bill-form-area">
						{createForm(formId, {
							onAfterEvent: headAfterEvent.bind(this)
						})}
					</div>
				</div>
				<div className="nc-bill-table-area">
					{createCardTable(tableId, {
						tableHeadLeft: this.getTableHeadLeft.bind(this),
						tableHead: this.getTableHead.bind(this),
						modelSave: saveClick.bind(this),
						onAfterEvent: bodyAfterEvent.bind(this),
						selectedChange: rowSelected.bind(this),
						showIndex: true,
						showCheck: true
					})}
				</div>
				<BillTrack
					show={this.state.show}
					close={() => {
						this.setState({ show: false });
					}}
					pk={this.state.pk_apply} //单据id
					type={bill_type} //单据类型
				/>
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transi_type}
					billid={this.state.pk_apply}
				/>
				{/* 提交及指派 */}
				{this.state.compositedisplay && (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002')} /**指派 */
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				)}
				{/* 附件 */}
				{createNCUploader('uploder', {})}
			</div>
		);
	}
}

BorrowApplyCard = createPage({
	billinfo: {
		billtype: 'card',
		pagecode: pagecode,
		headcode: formId,
		bodycode: tableId
	}
})(BorrowApplyCard);

// ReactDOM.render(<Card />, document.querySelector("#app"));
export default BorrowApplyCard;
