//小应用常量
export const appConfig = {
	title: '452000504A-000003',
	//卡片页面路由
	cardRouter: '/card',
	//列表界面路由
	listRouter: '/list',
	//缓存数据命名空间
	dataSource: 'aum.borrow.apply.main',

	bill_type: '4A38',
	//借用申请功能节点
	funcode: '4520008005',
	//主键字段
	pkField: 'pk_apply',
	//小应用编码
	appcode: '452000504A',
	//卡片页面编码
	pagecode_card: '452000504A_card',
	//列表页面编码
	pagecode_list: '452000504A_list',
	// 打印模板编码可标识
	printNodekey: null,
	printFileName: '452000504A-000003',

	//请求url
	reqUrl: {
		//打印卡片
		printCardUrl: '/nccloud/aum/borrow/apply_printcard.do',
		// 表头编辑后事件
		headAfterEditUrl: '/nccloud/aum/borrow/apply_headAfterEditEvent.do',
		// 表体编辑后事件
		bodyAfterEditUrl: '/nccloud/aum/borrow/apply_bodyAfterEditEvent.do',
		//列表打印
		printListUrl: '/nccloud/aum/borrow/apply_printList.do',
		//修改
		edit: '/nccloud/aum/borrow/apply_edit.do',
		//复制
		copy: '/nccloud/aum/borrow/apply_copy.do',
		//提交收回
		commitOrUn: '/nccloud/aum/borrow/apply_commit.do',
		//查询卡片数据
		queryCardData: '/nccloud/aum/borrow/apply_querycard.do',
		//删除
		delete: '/nccloud/aum/borrow/apply_delete.do',
		//新增
		insert: '/nccloud/aum/borrow/apply_save.do',
		//修改
		update: '/nccloud/aum/borrow/apply_update.do',
		//打开
		openUrl: '/nccloud/aum/borrow/openLine.do',
		//关闭
		closeUrl: '/nccloud/aum/borrow/closeLine.do'
	}
};
