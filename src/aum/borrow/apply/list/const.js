//列表页面配置
export const pageConfig = {
    batchBtns: ['Delete', 'Commit', 'copy', 'UnCommit', 'Attachment', 'QueryAbout', 'Print', 'Preview', 'Output'],
    tableId: 'list_head',
    formId: 'card_head',
    searchId: 'searchArea',
    pagecode: "452000504A_list"
}