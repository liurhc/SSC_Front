//主子表列表

import React, { Component } from 'react';
import { createPage, high, createPageIcon } from 'nc-lightapp-front';
import {
	buttonClick,
	initTemplate,
	searchBtnClick,
	pageInfoClick,
	doubleClick,
	afterEvent,
	rowSelected
} from './events';
import { pageConfig } from './const';
import { appConfig } from '../const';
import { commitAction, setBatchBtnsEnable } from './events/buttonClick';
import ampub from 'ampub';

const { components, utils } = ampub;
const { getMultiLangByID } = utils.multiLangUtils;
const { ApprovalTrans } = components;

const { dataSource, bill_type, title, pkField } = appConfig;
const { tableId, searchId, pagecode } = pageConfig;
const { ApproveDetail } = high;

class BorrowAppayList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			transi_type: '4A38-01',
			pk_apply: '',
			show: false,
			showApprove: false,
			compositedisplay: false,
			compositedata: {}
		};
		initTemplate.call(this, props);
	}

	//提交及指派 回调
	getAssginUsedr = (value) => {
		commitAction.call(this, 'SAVE', this.props, value);
	};

	//取消 指派
	turnOff = () => {
		this.setState({
			compositedisplay: false
		});
	};

	render() {
		let { table, button, search, ncUploader } = this.props;
		let { createNCUploader } = ncUploader;
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { createButtonApp } = button;
		return (
			<div className="nc-bill-list">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						{createPageIcon()}
						<h2 className="title-search-detail">{getMultiLangByID(title)}</h2>
					</div>
					<div className="header-button-area">
						{createButtonApp({
							area: 'list_head',
							buttonLimit: 3,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				<div className="nc-bill-search-area">
					{NCCreateSearch(searchId, {
						onAfterEvent: afterEvent.bind(this),
						dataSource: dataSource,
						clickSearchBtn: searchBtnClick.bind(this),
						componentInitFinished: () => {}
					})}
				</div>
				<div className="nc-bill-table-area">
					{createSimpleTable(tableId, {
						handlePageInfoChange: pageInfoClick,
						onSelected: rowSelected.bind(this),
						onSelectedAll: rowSelected.bind(this),
						showIndex: true,
						showCheck: true,
						onRowDoubleClick: doubleClick.bind(this),
						dataSource: dataSource,
						pkname: pkField,
						componentInitFinished: () => {
							//缓存数据赋值成功的钩子函数
							//若初始化数据后需要对数据做修改，可以在这里处理
							setBatchBtnsEnable.call(this, this.props, tableId);
						}
					})}
				</div>
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transi_type}
					billid={this.state.pk_apply}
				/>
				{/* 提交及指派 */}
				{this.state.compositedisplay && (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002')} /**指派 */
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				)}
				{/* 附件 */}
				{createNCUploader('uploader', {})}
			</div>
		);
	}
}

BorrowAppayList = createPage({})(BorrowAppayList);

// ReactDOM.render(<List />, document.querySelector('#app'));
export default BorrowAppayList;
