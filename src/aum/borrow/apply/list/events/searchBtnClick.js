import { ajax, toast } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import { appConfig } from '../../const';
import { setBatchBtnsEnable } from './buttonClick';
import ampub from 'ampub';

const { components, utils } = ampub;
const { getContext, loginContextKeys } = components.LoginContext;
const { setQueryInfoCache, listConst, setListValue } = utils.listUtils;

const { tableId, searchId, pagecode } = pageConfig;
const { bill_type } = appConfig;

//点击查询，获取查询区数据
export default function(props, searchVal, type, queryInfo, isRefresh) {
	if (!searchVal) {
		return;
	}
	let transtype = getContext(loginContextKeys.transtype);
	if (!queryInfo) {
		queryInfo = props.search.getQueryInfo(searchId);
	}
	let pageInfo = props.table.getTablePageInfo(tableId);

	queryInfo.pagecode = pagecode;
	queryInfo.pageInfo = pageInfo;
	queryInfo.billtype = bill_type;
	queryInfo.transtype = transtype;

	// 缓存查询条件
	setQueryInfoCache.call(this, queryInfo, isRefresh, type);

	ajax({
		url: '/nccloud/aum/borrow/apply_query.do',
		data: queryInfo,
		success: (res) => {
			setListValue.call(
				this,
				props,
				res,
				tableId,
				isRefresh ? listConst.SETVALUETYPE.refresh : listConst.SETVALUETYPE.query
			);

			setBatchBtnsEnable.call(this, props, tableId);
		}
	});
}
