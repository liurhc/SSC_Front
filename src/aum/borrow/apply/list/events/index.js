import searchBtnClick from './searchBtnClick';
import initTemplate from './initTemplate';
import pageInfoClick from './pageInfoClick';
import buttonClick from './buttonClick';
import doubleClick from './doubleClick';
import afterEvent from './afterEvent';
import rowSelected from './rowSelected';
export { searchBtnClick, pageInfoClick, initTemplate, buttonClick, doubleClick, afterEvent, rowSelected };