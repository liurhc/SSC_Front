import { setBatchBtnsEnable } from './buttonClick';

/**
 * 行选中事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} record 
 * @param {*} index 
 * @param {*} status 
 */
export default function (props, moduleId, record, index, status) {
    setBatchBtnsEnable.call(this, props, moduleId);
}
