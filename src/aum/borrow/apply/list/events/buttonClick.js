import { ajax, print, toast, output, promptBox } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import { appConfig } from '../../const';
import searchBtnClick from './searchBtnClick';
import ampub from 'ampub';

const { components, commonConst, utils } = ampub;
const { UISTATE } = commonConst.StatusUtils;
const { showConfirm, MsgConst, showMessage } = utils.msgUtils;
const { batchRefresh, setBillFlowBtnsEnable } = utils.listUtils;
const { getScriptListReturn } = components.ScriptReturnUtils;

const { formId, tableId, pagecode, searchId, batchBtns } = pageConfig;
const { pagecode_card, reqUrl, printFileName, printNodekey, dataSource } = appConfig;

export default function(props, id) {
	switch (id) {
		case 'Add':
			props.pushTo('/card', {
				pagecode: '452000504A_card',
				status: 'add'
			});
			break;
		case 'Delete':
			deleteAction.call(this, props);
			break;
		case 'Commit':
			commitAction.call(this, 'SAVE', props);
			break;
		case 'UnCommit':
			commitAction.call(this, 'UNSAVE', props);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'Attachment':
			attachmentClick.call(this, props);
			break;
		case 'Copy':
			copy.call(this, props);
			break;
		case 'Refresh':
			batchRefresh.call(this, props, searchBtnClick);
			break;
		default:
			break;
	}
}

/**
 * 刷新
 * @param {*} props 
 */
function refresh(props) {
	let searchVal = props.search.getAllSearchData(searchId);
	searchBtnClick.call(this, props, searchVal);
}

//附件
function attachmentClick(props) {
	let checkedrows = props.table.getCheckedRows(tableId);
	if (!checkedrows || !checkedrows[0] || !checkedrows[0].data) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOne });
	}
	// billNo 是单据编码
	let billNo = checkedrows[0].data.values['bill_code'].value;
	// billNo 是单据主键
	let billId = checkedrows[0].data.values['pk_apply'].value;
	props.ncUploader.show('uploader', {
		billId: 'pam/borrow_apply/' + billId,
		billNo
	});
}

/**
 * 删除按钮
 * @param {*} props 
 */
function deleteAction(props) {
	showConfirm.call(this, props, {
		type: MsgConst.Type.DelSelect,
		beSureBtnClick: batchDel
	});
}

/**
 * 批量删除
 */
function batchDel(props) {
	let checkdata = props.table.getCheckedRows(tableId);
	let paramInfoMap = {};
	let params = checkdata.map((v) => {
		let id = v.data.values.pk_apply.value;
		let ts = v.data.values.ts.value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	ajax({
		url: '/nccloud/aum/borrow/apply_delete.do',
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: pagecode_card
		},
		success: (res) => {
			getScriptListReturn(params, res, props, 'pk_apply', formId, tableId, true, dataSource);
			params.map((item) => {
				props.table.deleteCacheId(tableId, item.id);
			});
		}
	});
}

/**
 * 提交
 */
export function commitAction(OperatorType, props, content) {
	let checkdata = props.table.getCheckedRows(tableId);
	let paramInfoMap = {};
	let params = checkdata.map((v) => {
		let id = v.data.values.pk_apply.value;
		let ts = v.data.values.ts.value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	let commitType = '';
	if (OperatorType === 'SAVE') {
		//提交传这个
		commitType = 'commit';
	}
	ajax({
		url: '/nccloud/aum/borrow/apply_commit.do',
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: OperatorType,
			pageid: pagecode_card,
			commitType: commitType, //提交
			content: content
		},
		success: (res) => {
			if (content) {
				this.setState({
					compositedisplay: false
				});
			}
			getScriptListReturn.call(this, params, res, props, 'pk_apply', formId, tableId, false, dataSource);
		}
	});
}

/**
 * 打印
 */
function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		reqUrl.printCardUrl, // 后台打印服务url
		printData
	);
}

/**
 * 输出
 * @param {*} props 
 */
function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });
		return;
	}
	output({
		url: reqUrl.printCardUrl,
		data: printData
	});
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let checkedRows = props.table.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		return false;
	}
	let pks = [];
	checkedRows.map((item) => {
		pks.push(item.data.values['pk_apply'].value);
	});
	let printData = {
		filename: printFileName, // 文件名称
		nodekey: printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

/**
 * 复制
 * @param {*} props 
 */
function copy(props) {
	let datas = props.table.getCheckedRows(tableId);
	let pk_apply;
	if (datas && datas.length > 0) {
		pk_apply = datas[0].data.values.pk_apply.value;
	}
	if (pk_apply) {
		props.pushTo('/card', {
			pagecode: pagecode_card,
			status: UISTATE.copyAdd,
			id: pk_apply
		});
	} else {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseCopy });
	}
}

//设置按钮状态
export function setBatchBtnsEnable(props, moduleId) {
	setBillFlowBtnsEnable.call(this, props, { tableId: moduleId });
}
