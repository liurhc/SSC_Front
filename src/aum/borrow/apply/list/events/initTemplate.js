import { pageConfig } from '../const';
import tableButtonClick from './tableButtonClick';
import { setBatchBtnsEnable } from './buttonClick';
import ampub from 'ampub';

const { components, commonConst, utils } = ampub;
const { defRefCondition, commonRefCondition } = components.refInit;
const { getContext, loginContextKeys, loginContext } = components.LoginContext;
const { IBusiRoleConst } = commonConst.CommonKeys;
const { BILLSTATUS } = commonConst.StatusUtils;
const { getMultiLangByID } = utils.multiLangUtils;
const { createOprationColumn } = utils.listUtils;
const { AssetOrgMultiRefFilter } = components.assetOrgMultiRefFilter;

const { searchId, tableId, pagecode } = pageConfig;

export default function(props) {
	let _this = this;
	props.createUIDom(
		{
			pagecode //页面id
		},
		function(data) {
			if (data) {
				if (data.context) {
					loginContext(data.context);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					// 行删除时悬浮框提示
					props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001')); /*'确认要删除吗？' */
				}
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(_this, props, meta);
					props.meta.setMeta(meta, () => {
						setBatchBtnsEnable.call(_this, props, tableId);
					});
				}
			}
		}
	);
}

function modifierMeta(props, meta) {
	//分页控件显示标识
	meta[tableId].pagination = true;
	let pk_group = getContext(loginContextKeys.groupId);
	//查询区（借用人，借用部门）参照过滤
	meta[searchId].items.map((item) => {
		if (item.attrcode == 'pk_org') {
			item.queryCondition = () => {
				return { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter' };
			};
		} else if (item.attrcode == 'pk_unit_used' || item.attrcode == 'bodyvos.pk_category') {
			// //（使用权、设备类别）
			// item.queryCondition = () => {
			// 	let pk_group = props.initInfo.pk_group; // 集团
			// 	return { pk_group }; // 根据pk_group过滤
			// };
		} else if (item.attrcode == 'pk_material' || item.attrcode == 'pk_jobmngfil') {
			//（项目、物料）
			item.queryCondition = () => {
				let pk_org = getSearchValue.call(this, props, 'pk_org'); // 主组织
				return { pk_org }; // 根据主组织过滤
			};
		} else if (item.attrcode == 'pk_usedept') {
			//组织多选参照过滤
			AssetOrgMultiRefFilter.call(this, props, searchId, item, 'pk_org');
			//使用部门 使用人(领用部门 领用人)
			item.queryCondition = () => {
				let pk_unit_used = getSearchValue.call(this, props, 'pk_org'); //主组织
				let filter = { busifuncode: IBusiRoleConst.ASSETORG };
				if (pk_unit_used) {
					filter['pk_org'] = pk_unit_used;
				}
				return filter; // 根据pk_org过滤
			};
		} else if (item.attrcode == 'pk_user') {
			AssetOrgMultiRefFilter.call(this, props, searchId, item, 'pk_org');
			item.queryCondition = () => {
				let pk_unit_used = getSearchValue.call(this, props, 'pk_org'); //主组织
				let filter = { busifuncode: IBusiRoleConst.ASSETORG };
				if (pk_unit_used) {
					filter['pk_org'] = pk_unit_used;
				}
				return filter; // 根据pk_org过滤
			};
		} else if (item.attrcode == 'pk_applier') {
			AssetOrgMultiRefFilter.call(this, props, searchId, item, 'pk_org');
			//申请人
			item.queryCondition = () => {
				let pk_unit_used = getSearchValue.call(this, props, 'pk_org'); //主组织
				let filter = { busifuncode: IBusiRoleConst.ASSETORG };
				if (pk_unit_used) {
					filter['pk_org'] = pk_unit_used;
				}
				return filter; // 根据pk_org过滤
			};
		} else {
			let bodyDefPrefix = 'bodyvos.def';
			defRefCondition.call(this, props, item, searchId, pk_group, true, 'pk_org', bodyDefPrefix);
			defRefCondition.call(this, props, item, searchId, pk_group, true);
		}
		// 添加【执行时包含下级】默认勾选
		commonRefCondition.call(this, props, item);
	});
	if (meta[tableId] && meta[tableId].items && meta[tableId].items.length > 0) {
		// 修改列渲染样式
		meta[tableId].items = meta[tableId].items.map((item, key) => {
			item.width = 150;
			//单据号添加下划线超链接
			if (item.attrcode == 'bill_code') {
				item.render = (text, record, index) => {
					return (
						<div class="simple-table-td" field="bill_code" fieldname="借用申请单号">
							<span
								className="code-detail-link"
								onClick={() => {
									props.pushTo('/card', {
										pagecode: '452000504A_card',
										status: 'browse',
										id: record.pk_apply.value
									});
								}}
							>
								{record.bill_code && record.bill_code.value}
							</span>
						</div>
					);
				};
			}
			return item;
		});
		// 添加表格操作列
		// 添加表格操作列
		let oprCol = createOprationColumn.call(this, props, {
			tableButtonClick: tableButtonClick.bind(this),
			getInnerBtns: getInnerBtns.bind(this)
		});
		meta[tableId].items.push(oprCol);
	}
	return meta;
}

//获取查询条件的值
function getSearchValue(props, field) {
	let data = props.search.getSearchValByField(searchId, field);
	if (data && data.value && data.value.firstvalue && 20 == data.value.firstvalue.length) {
		return data.value.firstvalue;
	}
}

/**设备卡片列表态操作列按钮 */
function getInnerBtns(props, text, record, index) {
	let buttonAry;
	if (record) {
		let bill_status = record.bill_status.value;
		if (BILLSTATUS.free_check == bill_status) {
			//自由态
			buttonAry = [ 'Commit', 'Edit', 'Delete', 'Copy' ];
		} else if (BILLSTATUS.check_nopass == bill_status) {
			//审批不通过
			buttonAry = [ 'Commit', 'Edit', 'Delete', 'Copy', 'QueryAboutBillFlow' ];
		} else if (BILLSTATUS.un_check == bill_status) {
			//提交态
			buttonAry = [ 'UnCommit', 'Copy', 'QueryAboutBillFlow' ];
		} else if (BILLSTATUS.check_pass == bill_status) {
			//审批通过态
			buttonAry = [ 'UnCommit', 'Copy', 'QueryAboutBillFlow' ];
		} else if (BILLSTATUS.check_going == bill_status) {
			//审核中
			buttonAry = [ 'Copy', 'QueryAboutBillFlow' ];
		} else if (BILLSTATUS.close == bill_status) {
			//关闭
			buttonAry = [ 'Copy', 'QueryAboutBillFlow' ];
		}
	} else {
		buttonAry = [];
	}
	return buttonAry;
}
