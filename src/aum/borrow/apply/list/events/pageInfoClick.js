import { ajax, toast } from 'nc-lightapp-front';
import { pageConfig } from '../const';

const { tableId, pagecode } = pageConfig;

export default function(props, config, pks) {
	let data = {
		allpks: pks,
		pagecode: pagecode
	};
	//得到数据渲染到页面
	let that = this;
	ajax({
		url: '/nccloud/aum/borrow/querypagegridbypks.do',
		data: data,
		success: function(res) {
			let { success, data } = res;
			if (success) {
				if (data) {
					props.table.setAllTableData(tableId, data[tableId]);
				} else {
					props.table.setAllTableData(tableId, { rows: [] });
				}
			}
		}
	});
}
