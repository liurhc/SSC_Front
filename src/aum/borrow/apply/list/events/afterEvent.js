import { pageConfig } from '../const';
import ampub from 'ampub';

const { components } = ampub;
const { isMultiCorpRefHandler } = components.assetOrgMultiRefFilter;
const { searchId } = pageConfig;

export default function afterEvent(key, value) {
	if (key === 'pk_org') {
		isMultiCorpRefHandler.call(this, this.props, value, searchId, [ 'pk_usedept', 'pk_applier', 'pk_user' ]);
	}
}
