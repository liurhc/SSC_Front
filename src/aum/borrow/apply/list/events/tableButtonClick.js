import { ajax, toast } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import { appConfig } from '../../const';
import ampub from 'ampub';

const { components, commonConst, utils } = ampub;
const { UISTATE } = commonConst.StatusUtils;
const { MsgConst, showMessage } = utils.msgUtils;
const { getScriptListReturn } = components.ScriptReturnUtils;

const { funcode, pagecode_card, dataSource } = appConfig;
const { formId, tableId } = pageConfig;
export default function tableButtonClick(props, key, text, record, index) {
	switch (key) {
		// 表格操作按钮
		case 'Edit':
			ajax({
				url: '/nccloud/aum/borrow/apply_edit.do',
				data: {
					pk: record.pk_apply.value,
					resourceCode: funcode
				},
				success: (res) => {
					if (res.data) {
						toast({ color: 'warning', content: res.data });
					} else {
						props.pushTo('/card', {
							pagecode: '452000504A_card',
							status: 'edit',
							id: record.pk_apply.value
						});
					}
				}
			});
			break;
		case 'Delete':
			let paramInfoMap = {};
			let id = record.pk_apply.value;
			paramInfoMap[id] = record.ts.value;
			let params = [ { id, index } ];
			ajax({
				url: '/nccloud/aum/borrow/apply_delete.do',
				data: {
					paramInfoMap: paramInfoMap,
					dataType: 'listData',
					OperatorType: 'DELETE',
					pageid: pagecode_card
				},
				success: (res) => {
					getScriptListReturn.call(this, params, res, props, 'pk_apply', formId, tableId, true, dataSource);
					props.table.deleteCacheId(tableId, id);
				}
			});
			break;
		case 'Commit':
			commit.call(this, 'SAVE', props, record, index);
			break;
		case 'UnCommit':
			commit.call(this, 'UNSAVE', props, record, index);
			break;
		case 'Copy':
			copy.call(this, props, key, text, record, index);
			break;
		case 'QueryAboutBillFlow':
			let pk_apply = record.pk_apply.value;
			let transi_type = record.transi_type.value;
			this.setState({
				showApprove: true,
				pk_apply,
				transi_type
			});
			break;
		default:
			break;
	}
}

/**
 * 提交
 */
function commit(OperatorType, props, record, index) {
	let paramInfoMap = {};
	let id = record.pk_apply.value;
	let ts = record.ts.value;
	paramInfoMap[id] = ts;
	let params = [
		{
			id,
			index
		}
	];
	let commitType = '';
	if (OperatorType === 'SAVE') {
		//提交传这个
		commitType = 'commit';
	}
	ajax({
		url: '/nccloud/aum/borrow/apply_commit.do',
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: OperatorType,
			pageid: pagecode_card,
			commitType: commitType //提交
			//commitType : 'saveCommit' //保存提交
		},
		success: (res) => {
			getScriptListReturn.call(this, params, res, props, 'pk_apply', formId, tableId, false, dataSource);
		}
	});
}

//列表行复制
function copy(props, key, text, record, index) {
	let pk_apply = record.pk_apply.value;
	if (pk_apply) {
		props.pushTo('/card', {
			pagecode: pagecode_card,
			status: UISTATE.copyAdd,
			id: pk_apply
		});
	} else {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseCopy });
	}
}
