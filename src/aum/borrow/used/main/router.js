import { asyncComponent } from 'nc-lightapp-front';

const List = asyncComponent(() =>
	import(/* webpackChunkName: "aum/borrow/used/list/list" */ /* webpackMode: "eager" */ '../list')
);
const Card = asyncComponent(() =>
	import(/* webpackChunkName: "aum/borrow/used/card/card" */ /* webpackMode: "eager" */ '../card')
);
//借用申请转单
const BorrowApplyTransfer = asyncComponent(() =>
	import(/* webpackChunkName: "aum/borrow/borrowtransfer/source/source" */ /* webpackMode: "eager" */ '../../borrowtransfer/source')
);

const routes = [
	{
		path: '/',
		component: List,
		exact: true
	},
	{
		path: '/list',
		component: List
	},
	{
		path: '/card',
		component: Card
	},
	{
		path: '/transfer',
		component: BorrowApplyTransfer
	}
];

export default routes;
