// 页面配置
export const pageConfig = {
	// 应用编码
	appcode: '452000512A',
	// 应用名称(国际化处理 借用单维护)
	title: '452000512A-000000',
	// 查询区域id
	searchAreaId: 'searchArea',
	// 表格区域id
	tableId: 'list_head',
	// 列表页面编码
	pagecode: '452000512A_list',
	// 卡片页面编码
	cardPageId: '452000512A_card',
	// 主键字段
	pkField: 'pk_borrow',
	// 单据类型
	bill_type: '4A15',
	// 文件名称(国际化处理 借用单维护)
	printFilename: '452000512A-000000',
	// 请求链接
	url: {
		queryPageUrl: '/nccloud/aum/borrow/used_querypagegridbypks.do',
		queryUrl: '/nccloud/aum/borrow/used_query.do',
		printUrl: '/nccloud/aum/borrow/used_printCard.do',
		commitUrl: '/nccloud/aum/borrow/used_commit.do',
		unCommitUrl: '/nccloud/aum/borrow/used_unCommit.do',
		editUrl: '/nccloud/aum/borrow/used_edit.do'
	},
	// 打印模板所在NC节点及标识
	printFuncode: '4520032005',
	printNodekey: null,
	// 权限资源编码
	resourceCode: '4520012005',
	cardRouter: '/card',
	dataSource: 'aum.borrow.used.main'
};
