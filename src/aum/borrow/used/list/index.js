import React, { Component } from 'react';
import { createPage, base, high, createPageIcon } from 'nc-lightapp-front';
import {
	searchBtnClick,
	initTemplate,
	pageInfoClick,
	buttonClick,
	doubleClick,
	rowSelected,
	afterEvent,
	setBatchBtnsEnable,
	commit
} from './events';
import { pageConfig } from './const';
import ampub from 'ampub';

const { components, utils } = ampub;
const { ApprovalTrans } = components;
const { getMultiLangByID } = utils.multiLangUtils;

const { NCAffix } = base;
const { BillTrack, ApproveDetail } = high;
const { title, searchAreaId, tableId, pagecode, url, bill_type, dataSource, pkField } = pageConfig;

/**
 * 主子表列表页面入口文件
 */
class MasterChildList extends Component {
	constructor(props) {
		super(props);
		this.state = {
			pk_bill: '',
			showApprove: false,
			compositedisplay: false,
			compositedata: {}
		};
		initTemplate.call(this, props);
	}

	//提交及指派 回调，注意保存提交时候需要调用保存提交的方法
	getAssginUsedr = (content) => {
		commit.call(this, this.props, content); //原提交方法添加参数content
	};
	//取消 指派
	turnOff = () => {
		this.props.table.selectAllRows(tableId, false);
		setBatchBtnsEnable.call(this, this.props, tableId);
		this.setState({
			compositedisplay: false
		});
	};

	render() {
		const { table, search, button, ncUploader } = this.props;
		const { createButtonApp } = button;
		const { NCCreateSearch } = search;
		const { createSimpleTable } = table;
		let { createNCUploader } = ncUploader;

		let { showBillTrack, pk_bill, transi_type, showApprove, compositedata, compositedisplay } = this.state; // 需要输出的数据

		return (
			<div className="nc-bill-list">
				<NCAffix>
					{/* 头部 header */}
					<div className="nc-bill-header-area">
						{/* 标题 title 国际化处理  借用单维护SVG*/}
						<div className="header-title-search-area">
							{createPageIcon()}
							<h2 className="title-search-detail">{getMultiLangByID(title)}</h2>
						</div>
						{/* 按钮区 btn-area */}
						<div className="header-button-area">
							{createButtonApp({
								area: 'list_head',
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
						</div>
					</div>
				</NCAffix>
				{/* 查询区 search-area */}
				<div className="nc-bill-search-area">
					{NCCreateSearch(searchAreaId, {
						clickSearchBtn: searchBtnClick.bind(this),
						onAfterEvent: afterEvent.bind(this),
						dataSource: dataSource,
						componentInitFinished: () => {
							//缓存数据赋值成功的钩子函数
							//若初始化数据后需要对数据做修改，可以在这里处理
						}
					})}
				</div>
				{/* 列表区 table-area */}
				<div className="nc-bill-table-area">
					{createSimpleTable(tableId, {
						pkname: pkField,
						showCheck: true,
						showIndex: true,
						handlePageInfoChange: pageInfoClick.bind(this),
						onRowDoubleClick: doubleClick.bind(this),
						onSelected: rowSelected.bind(this),
						onSelectedAll: rowSelected.bind(this),
						dataSource: dataSource,
						componentInitFinished: () => {
							//缓存数据赋值成功的钩子函数
							//若初始化数据后需要对数据做修改，可以在这里处理
							setBatchBtnsEnable.call(this, this.props, tableId);
						}
					})}
				</div>
				{/*审批详情区*/}
				<ApproveDetail
					show={showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={transi_type}
					billid={this.state[pkField]}
				/>
				{/* 单据追溯框 */}
				<BillTrack
					show={showBillTrack}
					close={() => {
						this.setState({
							showBillTrack: false
						});
					}}
					pk={pk_bill}
					type={bill_type}
				/>
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002')}
						data={compositedata}
						display={compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				) : (
					''
				)}
				{/* 附件管理框 */}
				{createNCUploader(`${pagecode}-uploader`, {})}
			</div>
		);
	}
}

const MasterChildListBase = createPage({})(MasterChildList);

// 渲染页面
// ReactDOM.render(<MasterChildListBase pageConfig={pageConfig} />, document.querySelector('#app'));

export default MasterChildListBase;
