import { ajax } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import { setBatchBtnsEnable } from './buttonClick';
import ampub from 'ampub';

const { utils } = ampub;
const { setListValue } = utils.listUtils;

const { tableId, pagecode, url } = pageConfig;

/**
 * 分页处理
 * @param {*} props 
 */
export default (props, config, pks) => {
	let data = {
		allpks: pks,
		pagecode: pagecode
	};
	//得到数据渲染到页面
	ajax({
		url: url.queryPageUrl,
		data,
		success: (res) => {
			let { success } = res;
			if (success) {
				setListValue.call(this, props, res);
				setBatchBtnsEnable.call(this, props, tableId);
			}
		}
	});
};
