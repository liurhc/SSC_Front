import { pageConfig } from '../const';
import ampub from 'ampub';

const { components } = ampub;
const { isMultiCorpRefHandler } = components.assetOrgMultiRefFilter;

const { searchAreaId } = pageConfig;
/**
 * 列表查询区编辑后事件
 * @param props
 * @param key
 * @param value
 */
export default function(key, value) {
	//根据资产组织是否多选，以进行后续相关字段的业务单元过滤
	if (key === 'pk_org') {
		isMultiCorpRefHandler.call(this, this.props, value, searchAreaId, [ 'pk_dept', 'pk_taker' ]);
	}
}
