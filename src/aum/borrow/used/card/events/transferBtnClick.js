import { ajax } from 'nc-lightapp-front';
import { pageConfig, TRANSFERINFO } from '../const';
import buttonClick, { setValue, setStatus, edit, cancel, setBtnsVisible, validateBeforeSave } from './buttonClick';
import ampub from 'ampub';

const { commonConst, utils } = ampub;
const { UISTATE } = commonConst.StatusUtils;
const { showConfirm, MsgConst, showMessage } = utils.msgUtils;

const { formId, tableId, pkField, pkFieldB, url } = pageConfig;

export default function(props, id) {
	switch (id) {
		case 'Save':
			transferSave.call(this, props);
			break;
		case 'Cancel':
			transferCancelConfirm.call(this, props);
			break;
		case 'Delete':
			transferDeleteConfirm.call(this, props);
			break;
		case 'Quit':
			transferQuitConfirm.call(this, props);
			break;
		case 'Edit':
			transferEdit.call(this, props);
			break;
		default:
			buttonClick.call(this, props, id);
			break;
	}
}

/**
 * 转单下游删除确认
 * @param {*} props 
 */
function transferDeleteConfirm(props) {
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	if (!pk) {
		return;
	}
	showConfirm.call(this, props, { type: MsgConst.Type.Delete, beSureBtnClick: transferDelte });
}

/**
* 转单下游删除
* @param {*} props 
*/
function transferDelte(props) {
	let pagecode = this.pagecode;
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let ts = props.form.getFormItemsValue(formId, 'ts').value;
	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let paramInfoMap = {
		[pk]: ts
	};
	let obj = {
		dataType: 'cardData',
		OperatorType: 'DELETE',
		commitType: 'commit',
		pageid: pagecode,
		paramInfoMap: paramInfoMap
	};
	CardData.userjson = JSON.stringify(obj);
	ajax({
		url: url.commitUrl,
		data: CardData,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data.success === 'allsuccess') {
					//缓存处理
					let count = props.transferTable.getTransformFormAmount(TRANSFERINFO.leftarea); //转单数据的数量
					if (count == 1) {
						props.pushTo(TRANSFERINFO.transferRouter, {
							appcode: this.transferAppCode,
							pagecode: this.transferAppCode + 'src'
						});
					} else {
						props.transferTable.setTransformFormStatus(TRANSFERINFO.leftarea, {
							status: false,
							childId: pkFieldB,
							onChange: (current, next) => {
								showMessage.call(this, props, { type: MsgConst.Type.DeleteSuccess });
							}
						});
					}
					deleteSrcData.call(this, props);
				}
			}
		}
	});
}

/**
 * 转单下游保存
 * @param {*} props 
 */
export function transferSave(props) {
	let pagecode = this.pagecode;
	//必输项校验和过滤无效行
	let flag = validateBeforeSave.call(this, props);
	if (!flag) {
		return;
	}
	let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let reqUrl = url.insertUrl; //新增保存
	let oldstatus = props.form.getFormStatus(formId);
	if (oldstatus == UISTATE.edit) {
		reqUrl = url.updateUrl; //修改保存
	}
	// 保存前执行验证公式
	props.validateToSave(cardData, () => {
		ajax({
			url: reqUrl,
			data: cardData,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					// 性能优化，关闭表单和表格渲染
					props.beforeUpdatePage();
					setStatus.call(this, props, UISTATE.transferBorwse);
					setValue.call(this, props, data);
					//同步保存后的数据到转单界面
					this.synTransferData();
					let pk = props.form.getFormItemsValue(formId, pkField).value;
					// 若存在于转单已处理点击修改的数组中，则移除
					let index = this.transferEditId.indexOf(pk);
					if (index != -1) {
						this.transferEditId.splice(index, 1);
					}
					deleteSrcData.call(this, props);
					props.transferTable.setTransformFormStatus(TRANSFERINFO.leftarea, {
						status: true,
						childId: pkFieldB,
						onChange: (current, next, currentIndex) => {
							this.transferIndex = currentIndex + 1;
							showMessage.call(this, props, { type: MsgConst.Type.SaveSuccess });
						}
					});
					setBtnsVisible.call(this, props);
					// 性能优化，表单和表格统一渲染
					props.updatePage(formId, tableId);
				}
			}
		});
	});
}

/**
 * 转单下游取消确认
 * @param {*} props 
 */
function transferCancelConfirm(props) {
	showConfirm.call(this, props, { type: MsgConst.Type.Cancel, beSureBtnClick: transferCancel });
}

/**
 * 转单下游取消
 * @param {*} props 
 */
function transferCancel(props) {
	let headpkvalue = props.form.getFormItemsValue(formId, pkField).value;
	let index = this.transferEditId.indexOf(headpkvalue);
	if (headpkvalue && index != -1) {
		//转单已处理后又点保存取消
		this.transferEditId.splice(index, 1);
		props.form.cancel(formId);
		props.cardTable.resetTableData(tableId);
		setStatus.call(this, props, UISTATE.transferBorwse);
		// 设置按钮显隐性
		setBtnsVisible.call(this, props);
		this.synTransferData(); //同步取消后的数据到转单界面
	} else {
		let count = props.transferTable.getTransformFormAmount(TRANSFERINFO.leftarea); //转单数据的数量
		if (count == 1) {
			props.pushTo(TRANSFERINFO.transferRouter, {
				appcode: this.transferAppCode,
				pagecode: this.transferAppCode + 'src'
			});
		} else {
			props.transferTable.setTransformFormStatus(TRANSFERINFO.leftarea, {
				status: false,
				childId: pkFieldB,
				onChange: (current, next) => {}
			});
		}
	}
}

/**
 * 退出转单确认
 * @param {*} props 
 */
function transferQuitConfirm(props) {
	//判断当前单据是否存在没有处理的
	let count = props.transferTable.getTransformFormAmount(TRANSFERINFO.leftarea); //转单数据的数量
	let status = true;
	for (let index = 0; index < count; index++) {
		status = props.transferTable.getTransformFormStatus(TRANSFERINFO.leftarea, index);
		if (!status) {
			break;
		}
	}
	if (!status) {
		showConfirm.call(this, props, { type: MsgConst.Type.Quit, beSureBtnClick: transferquit });
	} else {
		props.pushTo(TRANSFERINFO.transferRouter, {
			appcode: this.transferAppCode,
			pagecode: this.transferAppCode + 'src'
		});
	}
}

/**
 * 退出转单
 * @param {} props 
 */
function transferquit(props) {
	props.pushTo(TRANSFERINFO.transferRouter, {
		appcode: this.transferAppCode,
		pagecode: this.transferAppCode + 'src'
	});
}

/**
 * 转单下游修改
 * @param {} props 
 */
function transferEdit(props) {
	let headpkValue = props.form.getFormItemsValue(formId, pkField).value;
	if (this.transferEditId.indexOf(headpkValue) == -1) {
		this.transferEditId.push(headpkValue);
	}
	edit.call(this, props);
}

/**
 * 保存/保存提交/删除成功去掉上游数据
 * @param {} props 
 */
export function deleteSrcData(props) {
	let pkvalues = props.cardTable.getColValue(tableId, 'pk_bill_b_src'); //表体的pk、数组
	pkvalues = pkvalues.map((e) => {
		return e.value;
	});
	props.transferTable.setSavedTransferTableDataPk(pkvalues);
}
