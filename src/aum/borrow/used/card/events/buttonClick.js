import { ajax, print, output, cardCache, cacheTools } from 'nc-lightapp-front';
import { pageConfig, TRANSFERINFO } from '../const';
import { deleteSrcData } from './transferBtnClick';
import { headAfterEvent } from './afterEvent';
import ampub from 'ampub';

const { components, commonConst, utils } = ampub;
const { openReportEquip } = components.queryAboutUtils;
const { getContext, loginContextKeys } = components.LoginContext;
const { UISTATE } = commonConst.StatusUtils;
const { getMultiLangByID } = utils.multiLangUtils;
const { showConfirm, MsgConst, showMessage } = utils.msgUtils;
const { setBillFlowBtnsVisible, setCardValue, setHeadAreaData } = utils.cardUtils;
const { beforeSaveValidator } = components.saveValidatorsUtil;
const { getScriptCardReturnData } = components.ScriptReturnUtils;

const {
	formId,
	tableId,
	pagecode,
	printFilename,
	editBtns,
	browseBtns,
	pkField,
	pkFieldB,
	bill_type,
	url,
	printNodekey,
	listRouter,
	resourceCode,
	dataSource,
	transferDataSource,
	notFilterFields,
	sourceInfoField
} = pageConfig;

/**
 * 按钮点击
 * @param {*} props 
 * @param {*} id 
 */
export default function(props, id) {
	switch (id) {
		case 'Add':
			add.call(this, props);
			break;
		case 'Edit':
			edit.call(this, props);
			break;
		case 'Delete':
			delConfirm.call(this, props);
			break;
		case 'Save':
			save.call(this, props);
			break;
		case 'Cancel':
			cancelConfirm.call(this, props);
			break;
		case 'Refresh':
			refresh.call(this, props);
			break;
		case 'AddLine':
			addLine.call(this, props, true);
			break;
		case 'DelLine':
			delLine.call(this, props);
			break;
		case 'BatchAlter':
			batchAlter.call(this, props);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'Commit':
			commit.call(this, props);
			break;
		case 'SaveCommit':
			saveCommit.call(this, props);
			break;
		case 'UnCommit':
			unCommit.call(this, props);
			break;
		case 'QueryAboutBusiness':
			queryAboutBusiness.call(this, props);
			break;
		case 'QueryAboutEquipReport':
			queryAboutEquipReport.call(this, props);
			break;
		case 'QueryAboutBillFlow':
			queryAboutBillFlow.call(this, props);
			break;
		case 'Attachment':
			attachment.call(this, props);
			break;
		default:
			break;
	}
}

/**
* 新增
* @param {*} props 
*/
export function add(props) {
	props.cardTable.closeExpandedRow(tableId);
	setStatus.call(this, props, UISTATE.add);
	// 设置按钮显隐性
	setBtnsVisible.call(this, props);
	// 清空数据
	setValue.call(this, props, undefined);
	// 设置默认值
	setDefaultValue.call(this, props);
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	//设置默认组织，调用后台编辑后事件
	if (pk_org_v && pk_org_v.value) {
		headAfterEvent.call(this, props, formId, 'pk_org_v', pk_org_v, { value: '' }, pk_org_v.value);
	} else {
		props.initMetaByPkorg('pk_org_v');
		props.form.setFormItemsDisabled(formId, {
			pk_org_v: false,
			pk_org: false
		});
		setBodyBtnsEnable.call(this, props, tableId);
	}
}

/**
 * 新增设置默认值
 * @param {*} props 
 */
function setDefaultValue(props) {
	// 交易类型
	let transi_type = getContext(loginContextKeys.transtype);
	let pk_transitype = getContext(loginContextKeys.pk_transtype);
	// 默认主组织
	let pk_org = getContext(loginContextKeys.pk_org);
	let org_Name = getContext(loginContextKeys.org_Name);
	let pk_org_v = getContext(loginContextKeys.pk_org_v);
	let org_v_Name = getContext(loginContextKeys.org_v_Name);
	props.form.setFormItemsValue(formId, {
		pk_org: { value: pk_org, display: org_Name },
		pk_org_v: { value: pk_org_v, display: org_v_Name },
		bill_type: { value: bill_type },
		transi_type: { value: transi_type },
		pk_transitype: { value: pk_transitype }
	});
}

/**
* 修改
* @param {*} props 
*/
export function edit(props, validatePermission = true) {
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	if (!pk) {
		pk = props.getUrlParam('id');
	}
	if (!pk) {
		return;
	}
	let editAction = (status = UISTATE.edit) => {
		// 关闭所有展开行
		props.cardTable.closeExpandedRow(tableId);
		// 设置修改态
		setStatus.call(this, props, status);
		// 设置按钮显隐性
		setBtnsVisible.call(this, props);
		// 设置组织字段不可编辑
		props.form.setFormItemsDisabled(formId, {
			pk_org_v: true,
			pk_org: true
		});
	};

	if (validatePermission) {
		let data = {
			pk,
			resourceCode
		};
		ajax({
			url: url.editUrl,
			data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if (data) {
						showMessage.call(this, props, { color: 'warning', content: data });
					} else {
						if (this.srctype == TRANSFERINFO.srctype) {
							editAction(UISTATE.transferEdit);
						} else {
							editAction();
						}
					}
				}
			}
		});
	} else {
		editAction();
	}
}

/**
* 删除确认
* @param {*} props 
*/
export function delConfirm(props) {
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	if (!pk) {
		return;
	}
	showConfirm.call(this, props, { type: MsgConst.Type.Delete, beSureBtnClick: delReal });
}

/**
 * 删除
 * @param {*} props 
 * @param {*} operatorType 
 * @param {*} commitType 
 */
function delReal(props) {
	let pagecode = this.pagecode;
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let ts = props.form.getFormItemsValue(formId, 'ts').value;
	let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let paramInfoMap = {
		[pk]: ts
	};
	let obj = {
		dataType: 'cardData',
		OperatorType: 'DELETE',
		commitType: 'commit',
		pageid: pagecode,
		paramInfoMap: paramInfoMap
	};
	cardData.userjson = JSON.stringify(obj);
	ajax({
		url: url.commitUrl,
		data: cardData,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				let callback = (newpk) => {
					// 更新参数
					props.setUrlParam({ id: newpk });
					let loadCallback = () => {
						setBtnsVisible.call(this, props);
					};
					// 加载下一条数据
					loadDataByPk.call(this, props, newpk, loadCallback);
				};
				getScriptCardReturnData.call(
					this,
					res,
					props,
					formId,
					tableId,
					pkField,
					dataSource,
					undefined,
					true,
					callback,
					pagecode
				);
			}
		}
	});
}

/**
* 返回
* @param {*} props 
*/

export function backToList(props) {
	if (this.srctype == TRANSFERINFO.srctype) {
		//判断当前单据是否存在没有处理的
		let count = props.transferTable.getTransformFormAmount(TRANSFERINFO.leftarea); //转单数据的数量
		let isCompleted = true;
		for (let index = 0; index < count; index++) {
			isCompleted = props.transferTable.getTransformFormStatus(TRANSFERINFO.leftarea, index);
			if (!isCompleted) {
				break;
			}
		}
		if (!isCompleted) {
			showConfirm.call(this, props, {
				type: MsgConst.Type.Back,
				beSureBtnClick: transferBackSureClick
			});
		} else {
			transferBackSureClick.call(this, props);
		}
	} else {
		props.pushTo(listRouter, { pagecode: pagecode.replace('card', 'list') });
	}
}

/**
 * 转单下游点击返回
 */
function transferBackSureClick(props) {
	props.pushTo(TRANSFERINFO.transferRouter, {
		appcode: this.transferAppCode,
		pagecode: this.transferAppCode + 'src'
	});
}

/**
* 保存
* @param {*} props 
*/
export function save(props) {
	let pagecode = this.pagecode;
	// 保存前校验
	let flag = validateBeforeSave.call(this, props);
	if (!flag) {
		return;
	}
	let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let reqUrl = url.insertUrl; //新增保存
	let oldstatus = props.form.getFormStatus(formId);
	if (oldstatus == UISTATE.edit) {
		reqUrl = url.updateUrl; //修改保存
	}
	// 保存前执行验证公式
	props.validateToSave(cardData, () => {
		ajax({
			url: reqUrl,
			data: cardData,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					// 性能优化，关闭表单和表格渲染
					props.beforeUpdatePage();
					// 设置状态
					setStatus.call(this, props, UISTATE.browse);
					// 设置值
					setValue.call(this, props, data);
					showMessage.call(this, props, { type: MsgConst.Type.SaveSuccess });
					let pk = props.form.getFormItemsValue(formId, pkField).value;
					// 保存成功后处理缓存
					let cachData = props.createMasterChildData(pagecode, formId, tableId);
					if (oldstatus == UISTATE.add) {
						cardCache.addCache(pk, cachData, formId, dataSource);
					} else {
						cardCache.updateCache(pkField, pk, cachData, formId, dataSource);
					}
					// 设置按钮显隐性
					setBtnsVisible.call(this, props);
					// 性能优化，表单和表格统一渲染
					props.updatePage(formId, tableId);
				}
			}
		});
	});
}

export function validateBeforeSave(props) {
	// 必输项校验和过滤空行
	let flag = beforeSaveValidator.call(this, props, formId, tableId, notFilterFields);
	if (!flag) {
		return false;
	}
	let take_date = props.form.getFormItemsValue(formId, 'take_date');
	let take_date_time = new Date(take_date.value).getTime();
	let allRows = props.cardTable.getVisibleRows(tableId);
	let errs = [];
	// 检验预计归还日期必须晚于表头借用日期
	if (take_date) {
		allRows.map((row, index) => {
			let expect_end_date = row.values['expect_end_date'];
			if (expect_end_date && expect_end_date.value) {
				let end_date_time = new Date(expect_end_date.value).getTime();
				if (take_date_time > end_date_time) {
					errs.push(index + 1);
				}
			}
		});
		let errMsg = '';
		let newErr = '';
		if (errs && errs.length > 0) {
			errs.map((item) => {
				newErr = newErr + ' [' + item + '] ';
			});
			errMsg = getMultiLangByID('452000512A-000001', {
				index: newErr
			}) /*第{index}行【预计归还日期】不能早于表头的【借用日期】*/;
		}
		if (errMsg) {
			showMessage.call(this, props, { color: 'danger', content: errMsg });
			return false;
		}
	}
	return true;
}

/**
* 取消
* @param {*} props 
*/
function cancelConfirm(props) {
	showConfirm.call(this, props, { type: MsgConst.Type.Cancel, beSureBtnClick: cancel });
}

/**
* 取消
* @param {*} props 
*/
export function cancel(props) {
	// 恢复字段的可编辑性
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	if (!pk_org_v || !pk_org_v.value) {
		props.resMetaAfterPkorgEdit();
	}
	setStatus.call(this, props, UISTATE.browse);
	// 加载编辑前数据，如果编辑前没有数据，则加载当前列表最后一条数据，如果还没有，显示为空不处理
	let pk = cardCache.getCurrentId(dataSource);
	if (!pk) {
		pk = cardCache.getCurrentLastId(dataSource);
	}
	let callback = () => {
		// 设置按钮显隐性
		setBtnsVisible.call(this, props);
	};
	loadDataByPk.call(this, props, pk, callback);
}

/**
* 刷新
* @param {*} props 
*/
export function refresh(props) {
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	if (!pk) {
		return;
	}
	let callback = (data) => {
		// 设置按钮显隐性
		setBtnsVisible.call(this, props);
		if (data) {
			showMessage.call(this, props, { type: MsgConst.Type.RefreshSuccess });
		}
	};
	// 查询到最新单据数据
	getDataByPk.call(this, props, pk, callback);
}

/**
* 新增行
* @param {*} props 
*/
export function addLine(props, isFocus = false) {
	//设置新增行表体默认值
	let pk_usedept_after_v = props.form.getFormItemsValue(formId, 'pk_dept_v');
	let pk_usedept_after = props.form.getFormItemsValue(formId, 'pk_dept');
	let pk_user_after = props.form.getFormItemsValue(formId, 'pk_taker');
	let pk_unit_used_after = props.form.getFormItemsValue(formId, 'pk_unit_used');
	let pk_unit_used_after_v = props.form.getFormItemsValue(formId, 'pk_unit_used_v');
	let lineNum = props.cardTable.getNumberOfRows(tableId);
	props.cardTable.addRow(
		tableId,
		lineNum,
		{ pk_usedept_after_v, pk_usedept_after, pk_user_after, pk_unit_used_after, pk_unit_used_after_v },
		isFocus
	);
	setBodyBtnsEnable.call(this, props, tableId);
}

/**
* 删除行
* @param {*} props 
*/
export function delLine(props) {
	let checkedRows = props.cardTable.getCheckedRows(tableId);
	let checkedIndex = [];
	checkedRows.map((item) => {
		checkedIndex.push(item.index);
	});
	props.cardTable.delRowsByIndex(tableId, checkedIndex);
	setBodyBtnsEnable.call(this, props, tableId);
}

/**
 * 设置界面状态
 * @param {*} props 
 * @param {*} status 
 */
export function setStatus(props, status = UISTATE.browse) {
	if (status == UISTATE.add || status == UISTATE.transferAdd) {
		props.form.setFormStatus(formId, 'add');
		props.cardTable.setStatus(tableId, UISTATE.edit);
		setBodyBtnsEnable.call(this, props, tableId);
	} else if (status == UISTATE.edit || status == UISTATE.transferEdit) {
		props.form.setFormStatus(formId, 'edit');
		props.cardTable.setStatus(tableId, 'edit');
		setBodyBtnsEnable.call(this, props, tableId);
	} else if (status == UISTATE.browse || status == UISTATE.transferBorwse) {
		props.form.setFormStatus(formId, 'browse');
		props.cardTable.setStatus(tableId, 'browse');
	}
	setHeadAreaData.call(this, props, { status });
}

/**
 * 通过单据id获取缓存单据信息，没有缓存则重新查询
 * @param {*} props 
 * @param {*} pk 
 */
export function loadDataByPk(props, pk, callback) {
	if (!pk || pk == 'null') {
		setValue.call(this, props, undefined);
		typeof callback == 'function' && callback();
	} else {
		let cachData = cardCache.getCacheById(pk, dataSource);
		if (cachData) {
			setValue.call(this, props, cachData);
			typeof callback == 'function' && callback(cachData);
		} else {
			getDataByPk.call(this, props, pk, callback);
		}
	}
}

//根据上游来源单据信息获取下游单据信息
export function getDataBySrcId(props) {
	let pagecode = this.pagecode;
	const transi_type = getContext(loginContextKeys.transtype);
	let pks = props.transferTable.getTransferTableSelectedId();
	let sourceInfo = getContext(sourceInfoField, transferDataSource);
	ajax({
		url: url.getDataBySrcUrl,
		data: {
			pks: pks,
			pageid: pagecode,
			srcbilltype: sourceInfo.billtype,
			srctranstype: sourceInfo.transtype,
			nextbilltype: bill_type,
			nexttranstype: transi_type
		},
		success: (res) => {
			if (res.data) {
				props.transferTable.setTransferListValue(TRANSFERINFO.leftarea, res.data);
				//设置组织不可编辑
				props.form.setFormItemsDisabled(formId, { pk_org: true, pk_org_v: true });
			}
		}
	});
}

/**
 * 通过单据id查询单据信息
 * @param {*} props 
 * @param {*} pk 
 */
export function getDataByPk(props, pk, callback) {
	ajax({
		url: url.queryCardUrl,
		data: {
			pagecode,
			pk
		},
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					// 性能优化，关闭表单和表格渲染
					props.beforeUpdatePage();
					setValue.call(this, props, data);
					cardCache.updateCache(pkField, pk, data, formId, dataSource);
					// 性能优化，表单和表格统一渲染
					props.updatePage(formId, tableId);
				} else {
					// 未查到数据，赋空值
					setValue.call(this, props, undefined);
					cardCache.deleteCacheById(pkField, pk, dataSource);
					showMessage.call(this, props, { type: MsgConst.Type.DataDeleted });
				}
				typeof callback == 'function' && callback(data);
			}
		}
	});
}

/**
 * 设置界面值
 * @param {*} props 
 * @param {*} data 
 */
export function setValue(props, data) {
	setCardValue.call(this, props, data);
}

/**
 * 设置按钮显示隐藏
 * @param {*} props 
 */
export function setBtnsVisible(props) {
	let status = props.form.getFormStatus(formId) || UISTATE.browse;
	let btnObj = {};
	if (status != UISTATE.browse) {
		browseBtns.map((item) => {
			btnObj[item] = false;
		});
		editBtns.map((item) => {
			btnObj[item] = true;
		});
	} else {
		editBtns.map((item) => {
			btnObj[item] = false;
		});
		let pkVal = props.form.getFormItemsValue(formId, pkField);
		if (!pkVal || !pkVal.value) {
			browseBtns.map((item) => {
				btnObj[item] = false;
			});
			btnObj['Add'] = true;
		} else {
			browseBtns.map((item) => {
				btnObj[item] = true;
			});
		}
	}
	// 转单页面，退出转单按钮始终显示
	if (this.srctype == TRANSFERINFO.srctype) {
		btnObj['Quit'] = true;
	}
	props.button.setButtonVisible(btnObj);
	setBillFlowBtnsVisible.call(this, props, formId, pkField);
}

/**
 * 设置表体按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBodyBtnsEnable(props, moduleId) {
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	if (!pk_org_v || !pk_org_v.value) {
		props.button.setButtonDisabled([ 'AddLine', 'DelLine', 'BatchAlter' ], true);
	} else {
		props.button.setButtonDisabled([ 'AddLine', 'BatchAlter' ], false);
		let checkedRows = [];
		let num = props.cardTable.getNumberOfRows(moduleId, false);
		if (num > 0) {
			checkedRows = props.cardTable.getCheckedRows(moduleId);
		}
		props.button.setButtonDisabled([ 'DelLine' ], !(checkedRows && checkedRows.length > 0));
	}
}

/**
 * 打印
 * @param {*} props 
 */
export function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		url.printUrl, // 后台打印服务url
		printData
	);
}

/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });
		return;
	}
	output({
		url: url.printUrl,
		data: printData
	});
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let pk = props.form.getFormItemsValue(formId, pkField);
	if (!pk || !pk.value) {
		return false;
	}
	let pks = [ pk.value ];
	let printData = {
		filename: getMultiLangByID(printFilename) /*国际化处理 借用单维护 svg */,
		nodekey: printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

/**
 * 保存提交
 * @param {*} props 
 * @param {*} operatorType 
 * @param {*} commitType 
 */
export function saveCommit(props, content) {
	let pagecode = this.pagecode;
	//必输项校验和过滤无效行
	let flag = validateBeforeSave.call(this, props);
	if (!flag) {
		return;
	}

	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let ts = props.form.getFormItemsValue(formId, 'ts').value;
	let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let paramInfoMap = {
		[pk]: ts
	};
	let obj = {
		dataType: 'cardData',
		OperatorType: 'SAVE',
		commitType: 'saveCommit',
		pageid: pagecode,
		paramInfoMap: paramInfoMap,
		content: content
	};
	cardData.userjson = JSON.stringify(obj);
	// 调用Ajax保存数据
	let saveCommitAction = () => {
		ajax({
			url: url.commitUrl,
			data: cardData,
			success: (res) => {
				let { success } = res;
				if (success) {
					// 性能优化，关闭表单和表格渲染
					props.beforeUpdatePage();
					if (content) {
						this.setState({
							compositedisplay: false
						});
					}
					if (this.srctype == 'transfer') {
						//转单提交后处理
						transferCommitAfter.call(this, props, res);
					} else {
						let setCommonValue = (data) => {
							setStatus.call(this, props, UISTATE.browse);
							setValue.call(this, props, data);
						};
						let callback = () => {
							setBtnsVisible.call(this, props);
						};
						// 数据回刷和缓存处理
						getScriptCardReturnData.call(
							this,
							res,
							props,
							formId,
							tableId,
							pkField,
							dataSource,
							setCommonValue,
							false,
							callback,
							pagecode
						);
					}
					// 性能优化，表单和表格统一渲染
					props.updatePage(formId, tableId);
				}
			}
		});
	};

	if (content) {
		saveCommitAction();
	} else {
		// 保存前执行验证公式
		props.validateToSave(cardData, saveCommitAction);
	}
}

/**
 * 转单页面提交后处理
 * @param {*} props 
 * @param {*} res 
 */
function transferCommitAfter(props, res) {
	let pagecode = this.pagecode;
	let setCommonValue = (data) => {
		setStatus.call(this, props, UISTATE.transferBorwse);
		setValue.call(this, props, data);
	};
	let callback = () => {
		this.synTransferData();
		//清楚上游缓存数据
		deleteSrcData.call(this, props);
		let headpkvalue = props.form.getFormItemsValue(formId, pkField);
		let index = this.transferEditId.indexOf(headpkvalue.value);
		if (index != -1) {
			this.transferEditId.splice(index, 1);
		}
		props.transferTable.setTransformFormStatus(TRANSFERINFO.leftarea, {
			status: true,
			childId: pkFieldB,
			onChange: (current, next, currentIndex) => {
				this.transferIndex = currentIndex + 1;
			}
		});
		setBtnsVisible.call(this, props);
	};
	getScriptCardReturnData.call(
		this,
		res,
		props,
		formId,
		tableId,
		pkField,
		transferDataSource,
		setCommonValue,
		false,
		callback,
		pagecode
	);
}

/**
 * 提交
 * @param {*} props 
 * @param {*} operatorType 
 * @param {*} commitType 
 */
export function commit(props, content) {
	runScript.call(this, props, 'SAVE', 'commit', content);
}

/**
 * 收回
 * @param {*} props 
 * @param {*} operatorType 
 * @param {*} commitType 
 */
function unCommit(props) {
	runScript.call(this, props, 'UNSAVE', 'commit');
}

/**
 * 提交
 * @param {*} props 
 * @param {*} operatorType 
 * @param {*} commitType 
 */
function runScript(props, operatorType, commitType, content) {
	let pagecode = this.pagecode;
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let ts = props.form.getFormItemsValue(formId, 'ts').value;
	let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let paramInfoMap = {
		[pk]: ts
	};
	let obj = {
		dataType: 'cardData',
		OperatorType: operatorType,
		commitType: commitType,
		pageid: pagecode,
		paramInfoMap: paramInfoMap,
		content: content
	};
	cardData.userjson = JSON.stringify(obj);
	ajax({
		url: url.commitUrl,
		data: cardData,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				// 性能优化，关闭表单和表格渲染
				props.beforeUpdatePage();
				if (content) {
					this.setState({
						compositedisplay: false
					});
				}
				let setCommonValue = (data) => {
					setValue.call(this, props, data);
				};
				let callback = () => {
					setBtnsVisible.call(this, props);
				};
				getScriptCardReturnData.call(
					this,
					res,
					props,
					formId,
					tableId,
					pkField,
					dataSource,
					setCommonValue,
					false,
					callback,
					pagecode
				);
				if (this.srctype == TRANSFERINFO.srctype) {
					this.synTransferData();
				}
				// 性能优化，表单和表格统一渲染
				props.updatePage(formId, tableId);
			}
		}
	});
}

/**
 * 单据追溯
 * @param {*} props 
 */
function queryAboutBusiness(props) {
	let pkVal = props.form.getFormItemsValue(formId, pkField);
	if (pkVal && pkVal.value) {
		this.setState({
			pk_bill: pkVal.value,
			showBillTrack: true
		});
	}
}

/**
 * 附件管理
 * @param {*} props 
 */
function attachment(props) {
	let pagecode = this.pagecode;
	// billId 是单据主键
	let billId = props.form.getFormItemsValue(formId, pkField).value;
	props.ncUploader.show(`${pagecode}-uploader`, {
		billId: 'pam/borrow/' + billId
	});
}

/**
 * 批改操作
 * @param {*} props 
 */
function batchAlter(props) {
	let pagecode = this.pagecode;
	// 获取表体行有效数据的行数
	let visibleRows = props.cardTable.getVisibleRows(tableId);
	// 如果没有数据或只有一条数据，直接返回
	if (visibleRows.length < 2) {
		return;
	}
	// 获取原始数据
	let changeData = props.cardTable.getTableItemData(tableId);

	// 获取卡片的数据
	let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	ajax({
		url: url.batchAlterUrl,
		data: {
			card: cardData,
			batchChangeIndex: changeData.batchChangeIndex, // 原始数据行号
			batchChangeKey: changeData.batchChangeKey, // 原始数据key
			batchChangeValue: changeData.batchChangeValue, // 原始数据value
			// 原始数据所在的vo，对应聚合VO中的tableCodes，一主一子可为空，一主多子要传对应的值
			tableCode: ''
		},
		success: (res) => {
			let { success, data } = res;
			if (success) {
				// 性能优化，关闭表单和表格渲染
				props.beforeUpdatePage();
				//把数据设置到界面上
				setValue.call(this, props, data);
				// 性能优化，表单和表格统一渲染
				props.updatePage(formId, tableId);
			}
		}
	});
}

/**
 * 联查台账
 * @param {*} props 
 */
function queryAboutEquipReport(props) {
	let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
	let visibleRows = props.cardTable.getVisibleRows(tableId);
	let equip_categories = [];
	visibleRows.map((item) => {
		let pkCategoryValue = item.values['pk_category'].value;
		if (pkCategoryValue != null) {
			equip_categories.push(pkCategoryValue);
		}
	});
	//去除重复的设备类别
	let new_equip_categories = Array.from(new Set(equip_categories));
	let obj = {};
	let querycondition = {};
	if (new_equip_categories.length == 0) {
		// 条件传组织、资产状态
		obj = {
			pk_usedorg: pk_org,
			// 状态类为闲置类
			'pk_used_status.status_type': '2'
		};
		querycondition = {
			logic: 'and',
			conditions: [
				{
					field: 'pk_usedorg',
					value: { firstvalue: pk_org, secondvalue: '' },
					oprtype: '=',
					display: '',
					isIncludeSub: false,
					refurl: ''
				},
				{
					field: 'pk_used_status.status_type',
					value: { firstvalue: '2', secondvalue: '' },
					oprtype: '=',
					display: '',
					isIncludeSub: false,
					refurl: ''
				}
			]
		};
	} else {
		// 条件传组织、资产类别、资产状态
		let pk_categorys = new_equip_categories.join(',');
		obj = {
			pk_usedorg: pk_org,
			pk_category: pk_categorys,
			// 状态类为闲置类
			'pk_used_status.status_type': '2'
		};
		querycondition = {
			logic: 'and',
			conditions: [
				{
					field: 'pk_usedorg',
					value: { firstvalue: pk_org, secondvalue: '' },
					oprtype: '=',
					display: '',
					isIncludeSub: false,
					refurl: ''
				},
				{
					field: 'pk_category',
					value: { firstvalue: pk_categorys, secondvalue: '' },
					oprtype: '=',
					display: '',
					isIncludeSub: false,
					refurl: ''
				},
				{
					field: 'pk_used_status.status_type',
					value: { firstvalue: '2', secondvalue: '' },
					oprtype: '=',
					display: '',
					isIncludeSub: false,
					refurl: ''
				}
			]
		};
	}
	cacheTools.set(`${bill_type}+condition`, obj);
	openReportEquip(props, bill_type, `${bill_type}+condition`, querycondition);
}

/**
 * 审批详情
 * @param props
 */
function queryAboutBillFlow(props) {
	let pkVal = props.form.getFormItemsValue(formId, pkField).value;
	let transi_type = props.form.getFormItemsValue(formId, 'transi_type').value;
	this.setState({
		showApprove: true,
		transi_type,
		pk_bill: pkVal
	});
}
