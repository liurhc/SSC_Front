import { setBodyBtnsEnable } from './buttonClick';
import ampub from 'ampub';

const { commonConst } = ampub;
const { UISTATE } = commonConst.StatusUtils;

/**
 * 表格操作按钮
 * @param {*} props 
 * @param {*} key 
 * @param {*} text 
 * @param {*} record 
 * @param {*} index 
 */
export default function(props, key, text, record, index, tableId) {
	switch (key) {
		// 展开
		case 'OpenCard':
			openCard.call(this, props, record, index, tableId);
			break;
		// 删行
		case 'DelLine':
			delLine.call(this, props, index, tableId);
			break;
		default:
			break;
	}
}

/**
 * 表格行展开
 * @param {*} props 
 * @param {*} record 
 * @param {*} index 
 */
function openCard(props, record, index, tableId) {
	let status = props.cardTable.getStatus(tableId);
	if (status == UISTATE.edit) {
		props.cardTable.setClickRowIndex(tableId, { record, index });
		props.cardTable.openModel(tableId, status, record, index);
	} else if (status == UISTATE.browse) {
		props.cardTable.toggleRowView(tableId, record);
	}
}

/**
 * 表格行删除
 * @param {*} props 
 * @param {*} index 
 */
function delLine(props, index, tableId) {
	props.cardTable.delRowsByIndex(tableId, index, tableId);
	setBodyBtnsEnable.call(this, props, tableId);
}
