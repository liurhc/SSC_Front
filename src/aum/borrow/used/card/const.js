// 页面配置
export const pageConfig = {
	// 应用编码
	appcode: '452000512A',
	// 应用名称(国际化处理 借用单维护)
	title: '452000512A-000000',
	// 表单区域id
	formId: 'card_head',
	// 表格区域id
	tableId: 'bodyvos',
	// 页面编码
	pagecode: '452000512A_card',
	// 编辑态按钮
	editBtns: [
		'SaveGroup',
		'Cancel',
		'OpenCard',
		'AddLine',
		'DelLine',
		'BatchAlter',
		'QueryAboutEquipReport' //联查台账
	],
	// 浏览态按钮
	browseBtns: [
		'Add',
		'Edit',
		'Delete',
		'Commit',
		'UnCommit',
		'Attachment',
		'OpenCard',
		'QueryAboutBusiness', //单据追溯
		'QueryAboutEquipReport', //资产台账
		'QueryAboutBillFlow', //审批详情
		'Print',
		'Output',
		'Refresh'
	],
	// 主表主键字段
	pkField: 'pk_borrow',
	// 子表主键字段
	pkFieldB: 'pk_borrow_b',
	// 单据类型
	bill_type: '4A15',
	// 交易类型
	transi_type: '4A15-01',
	// 文件名称(国际化处理 借用单维护)
	printFilename: '452000512A-000000',
	// 请求链接
	url: {
		queryCardUrl: '/nccloud/aum/borrow/used_query_card.do',
		insertUrl: '/nccloud/aum/borrow/used_insert.do',
		updateUrl: '/nccloud/aum/borrow/used_update.do',
		editUrl: '/nccloud/aum/borrow/used_edit.do',
		printUrl: '/nccloud/aum/borrow/used_printCard.do',
		commitUrl: '/nccloud/aum/borrow/used_commit.do',
		unCommitUrl: '/nccloud/aum/borrow/used_unCommit.do',
		headAfterEditUrl: '/nccloud/aum/borrow/used_headAfterEdit.do',
		bodyAfterEditUrl: '/nccloud/aum/borrow/used_bodyAfterEdit.do',
		batchAlterUrl: '/nccloud/aum/borrow/used_batchAlter.do',
		getDataBySrcUrl: '/nccloud/aum/borrow/used_changevo_srcids.do'
	},
	// 过滤空行相关无效字段
	notFilterFields: [
		'pk_unit_used_after',
		'pk_unit_used_after_v',
		'pk_usedept_after',
		'pk_usedept_after_v',
		'pk_user_after'
	],
	// 组织切换清空字段
	clearFields: [
		'pk_recorder',
		'bill_status',
		'pk_taker',
		'pk_dept_v',
		'pk_dept',
		'memo',
		'take_date',
		'pk_unit_used',
		'pk_unit_used_v'
	],
	// 打印模板所在NC节点及标识
	printFuncode: '4520032005',
	printNodekey: null,
	//NC节点标识
	resourceCode: '4520012005',
	listUrl: '/aum/borrow/used/list/index.html',
	//权限资源代码
	dataSource: 'aum.borrow.used.main',
	// 缓存上游信息的字段
	sourceInfoField: 'sourceInfo',
	//转单缓存标识（需要与上游转单保持一致）
	transferDataSource: 'aum.borrow.borrowtransfer.apply',
	listRouter: '/list'
};

//转单下游相关信息
export const TRANSFERINFO = {
	pagecode: '452000512A_transfercard',
	leftarea: 'leftarea',
	srctype: 'transfer',
	resourceCode: '4520012005',
	transferRouter: '/transfer'
};
