import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { SimpleReport } from 'nc-report';
const searchAreaId = 'light_report';
import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils, reportDrillUtils } = utils;
const { RefFilterForSearchArea, assetOrgMultiRefFilter } = components;
const { initMultiLangByModule } = multiLangUtils;
const { refCommonFilterForSearchArea, appPermissionOrgRefFilter, commonRefFilter } = RefFilterForSearchArea;
const { reportDrillToBill } = reportDrillUtils;
const { AssetOrgMultiRefFilter } = assetOrgMultiRefFilter;

export default class ReportBase extends Component {
	constructor(props) {
		super(props);
	}

	disposeSearch(meta, props) {
		meta[searchAreaId].items.map((item, key) => {
			// 查询区公共过滤
			refCommonFilterForSearchArea.call(this, props, item, searchAreaId);
			//按使用权过滤
			if (
				item.attrcode.includes('pk_usedept') ||
				item.attrcode.includes('pk_user') ||
				item.attrcode.includes('pk_mandept') ||
				item.attrcode.includes('pk_manager')
			) {
				AssetOrgMultiRefFilter.call(this, props, searchAreaId, item, 'pk_usedunit');
			}
			// 组织权限过滤
			if (
				item.attrcode.endsWith('pk_org') ||
				item.attrcode.endsWith('pk_usedorg') ||
				item.attrcode.endsWith('pk_ownerorg')
			) {
				appPermissionOrgRefFilter.call(this, item);
			}
			let filterByOrgFields = [
				'pam_reduce_b.pk_jobmngfil',
				'pam_reduce.pk_recorder',
				'pk_material',
				'pk_supplier',
				'pk_location'
			];
			commonRefFilter.call(this, props, item, searchAreaId, filterByOrgFields, 'pam_reduce.pk_org');
		});
		return meta;
	}
	/**
   * 
   * @param  items: 查询区查询数据，如需拓展查询区参数，请返回与items相同格式的查询数据
   */
	expandSearchVal(items) {
		return items;
	}

	/**
   * searchId: 查询区需要的参数
   * 'vname': 需要附默认值的字段
   * {value: '111'}: 显示值，区间为[]，具体可以对照平台查询区修改
   * 'like': 为oprtype字段值
   */
	setDefaultVal(searchId, props) {
		//查询区默认显示字段值
		// props.search.setSearchValByField(searchId, 'vname', { value: '111' }, 'like');
	}

	/**
	 * 报表联查实现接口
	 */
	setConnectionSearch(transSaveObject, obj, data, props, url, urlParams, sessonKey) {
		sessionStorage.setItem(sessonKey, transSaveObject);
		let fieldName = data.fldName;
		let transi_type = null;
		let primary_key = null;
		if (fieldName == 'bill_code') {
			//联查处置单
			transi_type = 'transi_type';
			primary_key = 'pk_reduce';
		} else if ((fieldName = 'this.pk_equip.equip_code')) {
			//联查资产卡片
			transi_type = 'this.pk_equip.transi_type';
			primary_key = 'pk_equip';
		}

		reportDrillToBill.call(this, props, data, primary_key, transi_type, transSaveObject);
	}

	render() {
		return (
			<div className="table">
				<SimpleReport
					showAdvBtn={true}
					setDefaultVal={this.setDefaultVal.bind(this)}
					expandSearchVal={this.expandSearchVal.bind(this)}
					disposeSearch={this.disposeSearch.bind(this)}
					setConnectionSearch={this.setConnectionSearch.bind(this)}
				/>
			</div>
		);
	}
}

let moduleIds = { ampub: [ 'common' ] };
initMultiLangByModule(moduleIds, () => {
	// 显示页面
	ReactDOM.render(<ReportBase />, document.querySelector('#app'));
});
