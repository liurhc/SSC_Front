import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { SimpleReport } from 'nc-report';
import ampub from 'ampub';

const { components, commonConst, utils } = ampub;
const { initCriteriaChangedHandlers } = components.EquipQueryCondition;
const { IBusiRoleConst } = commonConst.CommonKeys;
const { initMultiLangByModule } = utils.multiLangUtils;
const { reportDrillToBill } = utils.reportDrillUtils;
const { AssetOrgMultiRefFilter } = components.assetOrgMultiRefFilter;

const searchId = 'light_report';

export default class AssignReturnReport extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	disposeSearch(meta, props) {
		//设备卡片相关的初始化过滤
		initCriteriaChangedHandlers.call(this, props, meta, searchId);

		let org_code = 'nvl(pam_assign.pk_org,pam_assign_re.pk_org)';
		// 参照过滤
		let items = meta[searchId].items;
		items.forEach((item) => {
			if (
				item.attrcode == 'pam_assign.pk_recorder' || //领用人
				item.attrcode == 'pam_assign.pk_dept' || //领用部门
				item.attrcode == 'pam_assign_re.pk_taker' || //归还人
				item.attrcode == 'pam_assign_re.pk_recorder' //归还经办人
			) {
				//界面展示字段控制
				AssetOrgMultiRefFilter.call(this, props, searchId, item, org_code);

				// 如果user_name的参照需要根据前面选中参照的值进行过滤
				item.queryCondition = () => {
					let pk_org = getSearchValue.call(this, props, searchId, org_code); //主组织
					let filter = { busifuncode: IBusiRoleConst.ASSETORG };
					if (pk_org) {
						filter['pk_org'] = pk_org;
					}
					return filter; // 根据pk_org过滤
				};
			} else if (item.attrcode == org_code) {
				item.queryCondition = () => {
					return { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter' };
				};
			} else if (item.attrcode == 'nvl(pam_assign_b.pk_jobmngfil,pam_assign_re.pk_jobmngfil)') {
				//项目字段过滤，根据主组织过滤
				item.queryCondition = () => {
					let pk_org = getSearchValue.call(this, props, searchId, org_code);
					let filter = {};
					//主组织
					if (pk_org) {
						filter['pk_org'] = pk_org;
					}
					return filter; // 根据pk_org过滤
				};
			} else if (item.attrcode == 'nvl(pam_assign_b.pk_usedept_after,pam_assign_re.pk_usedept_after)') {
				let usedunit_code = 'nvl(pam_assign_b.pk_unit_used_after,pam_assign_re.pk_unit_used_after)';
				// 如果user_name的参照需要根据前面选中参照的值进行过滤
				item.queryCondition = () => {
					let pk_org = getSearchValue.call(this, props, searchId, usedunit_code); //主组织
					let filter = {};
					if (pk_org) {
						filter['pk_org'] = pk_org;
					}
					return filter;
				};
			}
		});
		return meta; // 处理后的过滤参照返回给查询区模板
	}

	/**
   * 
   * @param  items: 查询区查询数据，如需拓展查询区参数，请返回与items相同格式的查询数据
   */
	expandSearchVal(items) {
		return items;
	}
	/**
	 * 报表联查实现接口
	 */
	setConnectionSearch(transSaveObject, obj, data, props, url, urlParams, sessonKey) {
		sessionStorage.setItem(sessonKey, transSaveObject);
		let fieldName = data.fldName;
		let bill_type = null;
		let primary_key = null;
		if (fieldName == 'bill_code') {
			//联查领用单
			bill_type = 'bill_type';
			primary_key = 'pk_assign';
		} else if (fieldName == 'bill_code_re') {
			//联查领用归还单
			bill_type = 'bill_type_re';
			primary_key = 'pk_assign_re';
		} else if (fieldName == 'this.pk_equip.equip_code') {
			bill_type = 'this.pk_equip.bill_type';
			primary_key = 'pk_equip';
		}

		reportDrillToBill.call(this, props, data, primary_key, bill_type, transSaveObject);
	}

	render() {
		return (
			<div className="table">
				<SimpleReport
					expandSearchVal={this.expandSearchVal.bind(this)}
					disposeSearch={this.disposeSearch.bind(this)}
					showAdvBtn={true}
					setConnectionSearch={this.setConnectionSearch.bind(this)}
				/>
			</div>
		);
	}
}

initMultiLangByModule({ ampub: [ 'common' ] }, () => {
	ReactDOM.render(<AssignReturnReport />, document.querySelector('#app'));
});

//获取查询条件的值
function getSearchValue(props, searchId, org) {
	let data = props.search.getSearchValByField(searchId, org);
	if (data && data.value && data.value.firstvalue) {
		let tempValue = data.value.firstvalue.split(',');
		if (tempValue.length == 1) {
			return data.value.firstvalue;
		}
	}
}
