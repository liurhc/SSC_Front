import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { SimpleReport } from 'nc-report';
import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils, reportDrillUtils } = utils;
const { EquipQueryCondition } = components;
const { initMultiLangByModule } = multiLangUtils;
const { initCriteriaChangedHandlers } = EquipQueryCondition;
const { reportDrillToBill } = reportDrillUtils;

export default class InsuranceReport extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	disposeSearch(meta, props) {
		// let org_code = 'pam_insurance.pk_org';
		// // 参照过滤
		// let items = meta['light_report'].items;
		// items.forEach((item) => {
		// 	if (item.attrcode == org_code) {
		// 		item.isMultiSelectedEnabled = true;
		// 		item.queryCondition = () => {
		// 			return { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter' };
		// 		};
		// 	}
		// });
		initCriteriaChangedHandlers.call(this, props, meta, 'light_report', 'pam_equip');
		return meta; // 处理后的过滤参照返回给查询区模板
	}

	/**
	 * 报表联查实现接口
	 */
	setConnectionSearch(transSaveObject, obj, data, props, url, urlParams, sessonKey) {
		sessionStorage.setItem(sessonKey, transSaveObject);
		let fieldName = data.fldName;
		let bill_type = null;
		let primary_key = null;
		if (fieldName == 'bill_code') {
			//投保单号->资产投保
			bill_type = 'bill_type';
			primary_key = 'pk_insurance';
		} else if (fieldName == 'bill_code_compensation') {
			//联查索赔单
			bill_type = 'this.pk_compensation.bill_type';
			primary_key = 'pk_compensation';
		} else if (fieldName == 'this.pk_equip.equip_code') {
			//设备卡片
			bill_type = 'this.pk_equip.bill_type';
			primary_key = 'pk_equip';
		}

		reportDrillToBill.call(this, props, data, primary_key, bill_type, transSaveObject);
	}

	render() {
		return (
			<div className="table">
				<SimpleReport
					disposeSearch={this.disposeSearch.bind(this)}
					showAdvBtn={true}
					setConnectionSearch={this.setConnectionSearch.bind(this)}
				/>
			</div>
		);
	}
}
initMultiLangByModule({ ampub: [ 'common' ] }, () => {
	ReactDOM.render(<InsuranceReport />, document.querySelector('#app'));
});
