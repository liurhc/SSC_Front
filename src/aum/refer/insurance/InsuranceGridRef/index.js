import { high } from 'nc-lightapp-front';

const { Refer } = high;

/**
 * 资产投保
 * @param {*} props 
 */
export default function(props = {}) {
	let conf = {
		multiLang: {
			domainName: 'aum',
			// currentLocale: 'simpchn',
			moduleId: 'refer_aum'
		},
		refName: 'refer_aum-000003' /*国际化处理：资产投保 */,
		refCode: 'aum.ref.InsuranceGridRef',
		refType: 'grid',
		queryGridUrl: '/nccloud/aum/ref/InsuranceGridRef.do',
		isMultiSelectedEnabled: false, //是否多选
		isHasDisabledData: false, //是否有停用属性
		columnConfig: [
			{ name: [ 'refer_aum-000000', 'refer_aum-000001' ], code: [ 'refcode', 'refname' ] }
		] /*国际化处理：'编码', '名称' */
	};
	return <Refer {...Object.assign(conf, props)} />;
}
