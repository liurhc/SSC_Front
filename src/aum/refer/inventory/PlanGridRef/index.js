import { high } from 'nc-lightapp-front';

const { Refer } = high;

/**
 * 盘点计划
 * @param {*} props 
 */
export default function PlanGridRef(props = {}) {
	let conf = {
		multiLang: {
			domainName: 'aum',
			// currentLocale: 'simpchn',
			moduleId: 'refer_aum'
		},
		refType: 'grid',
		refName: 'refer_aum-000004' /*国际化处理：盘点计划 */,
		refCode: 'aum.ref.PlanGridRef',
		queryGridUrl: '/nccloud/aum/ref/PlanGridRef.do',
		isMultiSelectedEnabled: false, //是否多选
		isHasDisabledData: false, //是否有停用属性
		columnConfig: [
			{
				name: [ 'refer_aum-000000', 'refer_aum-000001', 'refer_aum-000005' ],
				code: [ 'refcode', 'refname', 'start_date' ]
			}
		] /*国际化处理：'编码', '名称',盘点开始时间 */
	};
	return <Refer {...Object.assign(conf, props)} />;
}
