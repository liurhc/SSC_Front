import { high } from 'nc-lightapp-front';

const { Refer } = high;

/**
 * 变动项目
 * @param {*} props 
 */
export default function AlterItemGridRef(props = {}) {
	let conf = {
		multiLang: {
			domainName: 'aum',
			// currentLocale: 'simpchn',
			moduleId: 'refer_aum'
		},
		refType: 'grid',
		refName: 'refer_aum-000002' /*国际化处理：变动项目*/,
		refCode: 'aum.ref.AlterItemGridRef',
		queryGridUrl: '/nccloud/aum/ref/AlterItemGridRef.do',
		isMultiSelectedEnabled: false, //是否多选
		isHasDisabledData: false, //是否有停用属性
		columnConfig: [
			{ name: [ 'refer_aum-000000', 'refer_aum-000001' ], code: [ 'refcode', 'refname' ] }
		] /*国际化处理：'编码', '名称' */
	};
	return <Refer {...Object.assign(conf, props)} />;
}
