import React, { Component } from 'react';
import { createPage } from 'nc-lightapp-front';
import MasterChildListBase from '../../transfer-base/source';

// 页面配置
const pageConfig = {
	// 应用主键
	appid: '0001Z9100000000012LS',
	// 应用编码
	appcode: '452004008A',
	// 应用名称(国际化处理 待报废资产处理')
	title: '452004004A-000003',
	// 查询区域id
	searchAreaId: 'searchArea',
	// 表格区域id
	tableId: 'list_head',
	// 页面编码
	pagecode: '452004008A_list',
	// 主键字段
	pkField: 'pk_equip',
	// 下游单据类型
	bill_type: '4A19',
	// 浏览态按钮
	browseBtns: [ '4A19+4A19-01' ],
	// 查询模板id
	quertTempletId: '0001Z9100000000063WP',
	// 请求链接
	url: {
		queryUrl: '/nccloud/aum/disusedtransfer/queryList.do'
	},
	transferToAppcode: '452004004A',
	transferToCardRouter: '/card',
	transferToPagecode: '452004004A_card',
	dataSource: 'aum.reduce.disused.transfer',
	statusFilter: 'nccloud.web.aum.reduce.disusedtransfer.refcondition.PK_STATUSSqlBuilder'
};

const MasterChildList = createPage({})(MasterChildListBase);

export default class List extends Component {
	render() {
		return <MasterChildList pageConfig={pageConfig} />;
	}
}
