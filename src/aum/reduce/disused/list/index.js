import React, { Component } from 'react';
import { createPage } from 'nc-lightapp-front';
import MasterChildListBase, { baseConfig } from '../../reduce-base/list-base';

// 页面配置
const pageConfig = {
	...baseConfig,
	// 应用主键
	appid: '0001Z9100000000012LR',
	// 应用编码
	appcode: '452004004A',
	// 应用名称(国际化处理 资产报废)
	title: '452004004A-000000',
	// 页面编码
	pagecode: '452004004A_list',
	// 单据类型
	bill_type: '4A19',
	// 请求链接
	url: {
		queryUrl: '/nccloud/aum/disused/query.do',
		queryPageUrl: '/nccloud/aum/disused/querypagegridbypks.do',
		editUrl: '/nccloud/aum/disused/edit.do',
		printUrl: '/nccloud/aum/disused/printCard.do',
		commitUrl: '/nccloud/aum/disused/commit.do',
		deleteUrl: '/nccloud/aum/disused/commit.do',
		unCommitUrl: '/nccloud/aum/disused/unCommit.do'
	},
	refFilter: {
		// 组织权限过滤处理类
		orgRefFilter: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter',
		// 仓库过滤处理类
		wareHouseRefFilter: 'nccloud.web.ampub.common.refCodition.StoreDocSqlBuilder'
	},
	// 输出文件名称(国际化处理 资产报废)
	printFilename: '452004004A-000000',
	// 节点标记
	nodeFlag: 'disused',
	// 权限资源编码
	resourceCode: '4520032010',
	dataSource: 'aum.reduce.disused.main'
};

const MasterChildList = createPage({})(MasterChildListBase);

export default class List extends Component {
	render() {
		return <MasterChildList pageConfig={pageConfig} />;
	}
}
