import React, { Component } from 'react';
import { createPage } from 'nc-lightapp-front';
import MasterChildCardBase, { baseConfig } from '../../reduce-base/card-base';

// 页面配置
const pageConfig = {
	...baseConfig,
	// 应用主键
	appid: '0001Z9100000000012LR',
	// 应用编码
	appcode: '452004004A',
	// 应用名称(国际化处理 资产报废)
	title: '452004004A-000000',
	// 页面编码
	pagecode: '452004004A_card',
	// 单据类型
	bill_type: '4A19',
	// 编辑态按钮
	editBtns: [ 'Save', 'SaveCommit', 'Cancel', 'GetFinanceData', 'OpenCard', 'AddLine', 'DelLine', 'BatchAlter' ],
	// 浏览态按钮
	browseBtns: [
		'Add',
		'Edit',
		'Delete',
		'Commit',
		'UnCommit',
		'Attachment',
		'OpenCard',
		'QueryAboutBusiness',
		'QueryAboutBillFlow',
		'ReceiptShow',
		'ReceiptScan',
		'Print',
		'Output',
		'Refresh'
	],
	// 请求链接
	url: {
		transferEquipUrl: '/nccloud/aum/disused/transferEquip.do',
		queryCardUrl: '/nccloud/aum/disused/queryCard.do',
		insertUrl: '/nccloud/aum/disused/insert.do',
		updateUrl: '/nccloud/aum/disused/update.do',
		editUrl: '/nccloud/aum/disused/edit.do',
		printUrl: '/nccloud/aum/disused/printCard.do',
		commitUrl: '/nccloud/aum/disused/commit.do',
		unCommitUrl: '/nccloud/aum/disused/unCommit.do',
		deleteUrl: '/nccloud/aum/disused/commit.do',
		headAfterEditUrl: '/nccloud/aum/disused/headAfterEdit.do',
		bodyAfterEditUrl: '/nccloud/aum/disused/bodyAfterEdit.do',
		batchAlterUrl: '/nccloud/aum/disused/batchAlter.do',
		getFianceDataUrl: '/nccloud/aum/disused/getFianceData.do'
	},
	refFilter: {
		// 组织权限过滤处理类
		orgRefFilter: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgVidRefFilter',
		// 资产过滤处理类
		equipRefFilter: 'nccloud.web.aum.reduce.disused.refcondition.PK_EQUIPSqlBuilder',
		// 仓库过滤处理类
		wareHouseRefFilter: 'nccloud.web.ampub.common.refCodition.StoreDocSqlBuilder'
	},
	// 输出文件名称(国际化处理 资产报废)
	printFilename: '452004004A-000000',
	// 权限资源编码
	resourceCode: '4520032010',
	dataSource: 'aum.reduce.disused.main',
	transferDataSource: 'aum.reduce.disused.transfer',
	transferFromAppcode: '452004008A',
	// 节点标记
	nodeFlag: 'disused',
	// 审批中表格可编辑字段
	editableFieldsInApprove: [ 'confirm_flag' ],
	// 编辑后事件字段
	editAfterEventFields: [ 'pk_equip', 'confirm_flag', 'pk_icorg_v' ],
	// 组织切换清空字段
	clearFields: [ 'pk_recorder' ]
};

const MasterChildCard = createPage({
	billinfo: {
		billtype: 'card',
		pagecode: pageConfig.pagecode,
		headcode: pageConfig.formId,
		bodycode: pageConfig.tableId
	}
})(MasterChildCardBase);

export default class Card extends Component {
	render() {
		return <MasterChildCard pageConfig={pageConfig} />;
	}
}
