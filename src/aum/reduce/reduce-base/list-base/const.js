// 页面配置
export const pageConfig = {
	// 查询区域id
	searchAreaId: 'searchArea',
	// 表格区域id
	tableId: 'list_head',
	// 应用主键
	pkField: 'pk_reduce',
	// 打印模板节点标识
	printNodekey: null,
	cardRouter: '/card'
};
