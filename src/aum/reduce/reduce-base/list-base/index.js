import React, { Component } from 'react';
import { base, high, createPageIcon } from 'nc-lightapp-front';
import ampub from 'ampub';

const { components, utils } = ampub;

const { ApprovalTrans, queryVocherSrcUtils } = components;

const { queryVoucherSrc } = queryVocherSrcUtils;

const { multiLangUtils } = utils;

const { getMultiLangByID } = multiLangUtils;

import {
	searchBtnClick,
	initTemplate,
	pageInfoClick,
	buttonClick,
	doubleClick,
	afterEvent,
	rowSelected,
	commit,
	setBatchBtnsEnable
} from './events';
import { pageConfig } from './const';

const { NCAffix } = base;
const { BillTrack, ApproveDetail } = high;

/**
 * 主子表列表页面入口文件
 */
class MasterChildListBase extends Component {
	constructor(props) {
		super(props);
		this.state = {
			pk_bill: '',
			transi_type: '',
			showApprove: false,
			showBillTrack: false,
			compositedata: {},
			compositedisplay: false
		};
		initTemplate.call(this, props);
	}

	componentDidMount() {
		let { pageConfig = {} } = this.props;
		let { tableId, pkField, pagecode, nodeFlag } = pageConfig;
		if (nodeFlag == 'sale') {
			//凭证联查来源单据
			queryVoucherSrc(this.props, tableId, pkField, pagecode);
		}
	}

	// 提交及指派回调
	getAssginUsedr = (content) => {
		commit.call(this, this.props, content);
	};

	// 取消指派
	turnOff = () => {
		this.setState({
			compositedisplay: false
		});
	};

	render() {
		let { pageConfig = {} } = this.props;
		let { title, searchAreaId, tableId, pagecode, pkField, bill_type, dataSource } = pageConfig;
		const { table, search, button, ncUploader } = this.props;
		const { createSimpleTable } = table;
		const { NCCreateSearch } = search;
		const { createButtonApp } = button;
		let { createNCUploader } = ncUploader;

		let { showBillTrack, pk_bill, transi_type, showApprove, compositedata, compositedisplay } = this.state; // 需要输出的数据

		return (
			<div className="nc-bill-list">
				<NCAffix>
					{/* 头部 header */}
					<div className="nc-bill-header-area">
						{/* 标题 title */}
						<div className="header-title-search-area">
							{createPageIcon()}
							<h2 className="title-search-detail">{getMultiLangByID(title)}</h2>
						</div>
						{/* 按钮区 btn-area */}
						<div className="header-button-area">
							{createButtonApp({
								area: 'list_head',
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
						</div>
					</div>
				</NCAffix>
				{/* 查询区 search-area */}
				<div className="nc-bill-search-area">
					{NCCreateSearch(searchAreaId, {
						onAfterEvent: afterEvent.bind(this),
						clickSearchBtn: searchBtnClick.bind(this),
						dataSource: dataSource,
						componentInitFinished: () => {
							//缓存数据赋值成功的钩子函数
							//若初始化数据后需要对数据做修改，可以在这里处理
						}
					})}
				</div>
				{/* 列表区 table-area */}
				<div className="nc-bill-table-area">
					{createSimpleTable(tableId, {
						showCheck: true,
						showIndex: true,
						handlePageInfoChange: pageInfoClick.bind(this),
						onRowDoubleClick: doubleClick.bind(this),
						onSelected: rowSelected.bind(this),
						onSelectedAll: rowSelected.bind(this),
						dataSource: dataSource,
						pkname: pkField,
						componentInitFinished: () => {
							//缓存数据赋值成功的钩子函数
							//若初始化数据后需要对数据做修改，可以在这里处理
							setBatchBtnsEnable.call(this, this.props, tableId);
						}
					})}
				</div>
				{/* 单据追溯框 */}
				<BillTrack
					show={showBillTrack}
					close={() => {
						this.setState({
							showBillTrack: false
						});
					}}
					pk={pk_bill}
					type={bill_type}
				/>
				{/* 审批详情 */}
				<ApproveDetail
					show={showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={transi_type}
					billid={pk_bill}
				/>
				{/* 提交及指派 */}
				{compositedisplay && (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002')}
						data={compositedata}
						display={compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				)}
				{/* 附件管理框 */}
				{createNCUploader(`${pagecode}-uploader`, {})}
			</div>
		);
	}
}

export default MasterChildListBase;

export const baseConfig = pageConfig;
