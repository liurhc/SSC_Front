import ampub from 'ampub';

const { components } = ampub;

const { assetOrgMultiRefFilter } = components;

const { isMultiCorpRefHandler } = assetOrgMultiRefFilter;

/**
 * 查询区编辑后事件
 * @param {*} key 
 * @param {*} value 
 */
export default function afterEvent(key, value) {
	let { pageConfig = {} } = this.props;
	let { searchAreaId } = pageConfig;
	if (key == 'pk_org') {
		// 根据组织字段是否多选控制其他参照是否有业务单元过滤的参照框。
		isMultiCorpRefHandler.call(this, this.props, value, searchAreaId, [ 'pk_usedept', 'pk_manager', 'pk_user' ]);
	} else if (key == 'bodyvos.pk_icorg') {
		// 根据库存组织字段是否多选控制入库字段是否有业务单元过滤的参照框。
		isMultiCorpRefHandler.call(this, this.props, value, searchAreaId, [ 'bodyvos.pk_warehouse_in' ]);
	}
}
