import { ajax } from 'nc-lightapp-front';
import ampub from 'ampub';

const { utils } = ampub;

const { listUtils } = utils;

const { setListValue } = listUtils;

import { setBatchBtnsEnable } from './buttonClick';

/**
 * 分页处理
 * @param {*} props 
 */
export default function(props, config, pks) {
	let { pageConfig = {} } = props;
	let { tableId, pagecode, url } = pageConfig;
	let data = {
		allpks: pks,
		pagecode
	};
	//得到数据渲染到页面
	ajax({
		url: url.queryPageUrl,
		data,
		success: (res) => {
			setListValue.call(this, props, res);
			setBatchBtnsEnable.call(this, props, tableId);
		}
	});
}
