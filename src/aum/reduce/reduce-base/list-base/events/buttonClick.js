import { ajax, print, output } from 'nc-lightapp-front';
import ampub from 'ampub';

const { components, utils } = ampub;

const { ScriptReturnUtils, queryVocherUtils } = components;

const { getScriptListReturn } = ScriptReturnUtils;
const { queryAboutVoucher } = queryVocherUtils;

const { listUtils, msgUtils, multiLangUtils } = utils;

const { setBillFlowBtnsEnable, batchRefresh } = listUtils;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { getMultiLangByID } = multiLangUtils;

import searchBtnClick from './searchBtnClick';

/**
 * 按钮点击
 * @param {*} props 
 * @param {*} id 
 */
export default function(props, id) {
	switch (id) {
		case 'Add':
			add.call(this, props);
			break;
		case 'Delete':
			delConfirm.call(this, props);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'Commit':
			commit.call(this, props);
			break;
		case 'UnCommit':
			unCommit.call(this, props);
			break;
		case 'Refresh':
			refresh.call(this, props);
			break;
		case 'QueryAboutBusiness':
			queryAboutBusiness.call(this, props);
			break;
		case 'QueryAboutVoucher':
			queryAboutVoucherSelf.call(this, props);
			break;
		case 'Attachment':
			attachment.call(this, props);
			break;
		default:
			break;
	}
}

/**
 * 新增
 * @param {*} props 
 */
export function add(props) {
	linkToCard.call(this, props, undefined, 'add');
}

/**
 * 批量删除
 * @param {*} props 
 */
export function delConfirm(props) {
	let { pageConfig = {} } = props;
	let { tableId } = pageConfig;
	let checkedRows = props.table.getCheckedRows(tableId);
	if (checkedRows.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseDelete });
		return;
	}
	showConfirm.call(this, props, { type: MsgConst.Type.DelSelect, beSureBtnClick: batchDel });
}

/**
 * 跳转卡片
 * @param {*} props 
 * @param {*} record 
 */
export function linkToCard(props, record = {}, status = 'browse') {
	let { pageConfig = {} } = props;
	let { pkField, cardRouter, pagecode } = pageConfig;
	props.pushTo(cardRouter, {
		pagecode: pagecode.replace('list', 'card'),
		status,
		id: record[pkField] ? record[pkField].value : ''
	});
}

/**
 * 设置批量按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBatchBtnsEnable(props, moduleId) {
	setBillFlowBtnsEnable.call(this, props, moduleId);
}

/**
 * 打印
 * @param {*} props 
 */
export function printTemp(props) {
	let { pageConfig = {} } = props;
	let { url } = pageConfig;
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		url.printUrl, // 后台打印服务url
		printData
	);
}

/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let { pageConfig = {} } = props;
	let { url } = pageConfig;
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });
		return;
	}
	output({
		url: url.printUrl,
		data: printData
	});
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let { pageConfig = {} } = props;
	let { tableId, pkField, printNodekey, printFilename } = pageConfig;
	let checkedRows = props.table.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length == 0) {
		return false;
	}
	let pks = [];
	checkedRows.map((item) => {
		pks.push(item.data.values[pkField].value);
	});
	let printData = {
		filename: getMultiLangByID(printFilename) /*国际化处理  资产处置/资产报废 svg */,
		nodekey: printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

/**
 * 删除
 * 
 * @param {*} props 
 */
function batchDel(props) {
	let { pageConfig = {} } = props;
	let { tableId, pagecode, pkField, url, dataSource } = pageConfig;
	let data = props.table.getCheckedRows(tableId);
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values[pkField].value;
		let ts = v.data.values['ts'].value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	ajax({
		url: url.deleteUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: pagecode.replace('list', 'card'),
			commitType: 'commit' //提交
		},
		success: (res) => {
			getScriptListReturn.call(
				this,
				params,
				res,
				props,
				pkField,
				tableId.replace('list', 'card'),
				tableId,
				true,
				dataSource
			);
		}
	});
}

/**
 * 提交
 * 
 * @param {*} props 
 */
export function commit(props, content) {
	runScript.call(this, props, 'SAVE', content);
}

/**
 * 收回
 * 
 * @param {*} props 
 */
function unCommit(props) {
	runScript.call(this, props, 'UNSAVE');
}

/**
 * 提交
 * 
 * @param {*} props 
 */
function runScript(props, actionType, content) {
	let { pageConfig = {} } = props;
	let { tableId, pagecode, pkField, url, dataSource } = pageConfig;
	let data = props.table.getCheckedRows(tableId);
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values[pkField].value;
		let ts = v.data.values['ts'].value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	ajax({
		url: url.commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: actionType,
			pageid: pagecode.replace('list', 'card'),
			commitType: 'commit', //提交
			content
		},
		success: (res) => {
			if (content) {
				this.setState({
					compositedisplay: false
				});
			}
			getScriptListReturn.call(
				this,
				params,
				res,
				props,
				pkField,
				tableId.replace('list', 'card'),
				tableId,
				false,
				dataSource
			);
		}
	});
}

/**
* 刷新
* @param {*} props 
*/
export function refresh(props) {
	batchRefresh.call(this, props, searchBtnClick);
}

/**
 * 单据追溯
 * @param {*} props 
 */
function queryAboutBusiness(props) {
	let { pageConfig = {} } = props;
	let { tableId, pkField } = pageConfig;
	let data = props.table.getCheckedRows(tableId);
	if (!data || data.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOne });
		return;
	}
	let pkVal = data[0].data.values[pkField];
	if (pkVal && pkVal.value) {
		this.setState({
			pk_bill: pkVal.value,
			showBillTrack: true
		});
	}
}

function queryAboutVoucherSelf(props) {
	let { pageConfig = {} } = props;
	let { tableId, pkField, appcode } = pageConfig;
	queryAboutVoucher.call(this, props, tableId, pkField, appcode, true);
}

/**
 * 附件管理
 * @param {*} props 
 */
function attachment(props) {
	let { pageConfig = {} } = props;
	let { tableId, pagecode, pkField } = pageConfig;
	let checkedrows = props.table.getCheckedRows(tableId);
	if (!checkedrows || checkedrows.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOne });
		return;
	}
	// billNo 是单据编码
	let billNo = checkedrows[0].data.values['bill_code'].value;
	// billId 是单据主键
	let billId = checkedrows[0].data.values[pkField].value;
	props.ncUploader.show(`${pagecode}-uploader`, {
		billId: 'pam/reduce/' + billId,
		billNo
	});
}
