import ampub from 'ampub';

const { components, utils } = ampub;

const { LoginContext, RefFilterForSearchArea } = components;

const { refCommonFilterForSearchArea, appPermissionOrgRefFilter, commonRefFilter } = RefFilterForSearchArea;
const { loginContext } = LoginContext;

const { listUtils, multiLangUtils } = utils;

const { createOprationColumn } = listUtils;
const { getMultiLangByID } = multiLangUtils;

import tableButtonClick from './tableButtonClick';
import { linkToCard, setBatchBtnsEnable } from './buttonClick';

/**
 * 模板初始化
 * @param {*} props 
 */
export default function(props) {
	let { pageConfig = {} } = props;
	let { tableId, pagecode } = pageConfig;
	props.createUIDom(
		{
			// 页面编码
			pagecode
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
				if (data.button) {
					// 设置按钮
					let button = data.button;
					props.button.setButtons(button, () => {
						setBatchBtnsEnable.call(this, props, tableId);
					});
					// 行删除时悬浮框提示
					props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001')); /* 国际化处理：确定要删除吗？ SVG*/
				}
				if (data.template) {
					// 修改模板
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta, () => {
						afterInit.call(this, props);
					});
				}
			}
		}
	);
}

/**
 * 模板初始化以后
 * @param {*} props 
 */
function afterInit(props) {
	let { pageConfig = {} } = props;
	let { tableId } = pageConfig;
	setBatchBtnsEnable.call(this, props, tableId);
}

/**
 * 修改模板
 * @param {*} props 
 * @param {*} meta 
 */
function modifierMeta(props, meta) {
	let { pageConfig = {} } = props;
	let { searchAreaId, tableId, refFilter } = pageConfig;
	// 参照查询条件过滤
	if (meta[searchAreaId] && meta[searchAreaId].items && meta[searchAreaId].items.length > 0) {
		meta[searchAreaId].items.map((item, key) => {
			let filterByPkOrg = [ 'pk_recorder', 'bodyvos.pk_jobmngfil' ];
			// 查询区公共过滤
			refCommonFilterForSearchArea.call(this, props, item, searchAreaId);
			// 组织权限过滤
			if (
				item.attrcode.endsWith('pk_org') ||
				item.attrcode.endsWith('pk_usedorg') ||
				item.attrcode.endsWith('pk_ownerorg')
			) {
				appPermissionOrgRefFilter.call(this, item);
			}
			if (filterByPkOrg.includes(item.attrcode)) {
				// 经办人、项目根据资产组织过滤
				commonRefFilter.call(this, props, item, searchAreaId, filterByPkOrg, 'pk_org');
			} else if (item.attrcode == 'bodyvos.pk_warehouse_in') {
				commonRefFilter.call(
					this,
					props,
					item,
					searchAreaId,
					'bodyvos.pk_warehouse_in',
					'bodyvos.pk_icorg',
					'pk_org',
					{
						'bodyvos.pk_warehouse_in': { GridRefActionExt: refFilter.wareHouseRefFilter }
					}
				);
			}
		});
	}
	if (meta[tableId] && meta[tableId].items && meta[tableId].items.length > 0) {
		// 修改列渲染样式
		meta[tableId].items.map((item, key) => {
			if (item.attrcode == 'bill_code') {
				item.render = (text, record, index) => {
					return (
						<div class="simple-table-td" field="bill_code" fieldname="bill_code">
							<span
								className="code-detail-link"
								onClick={() => {
									linkToCard.call(this, props, record, 'browse');
								}}
							>
								{record && record.bill_code && record.bill_code.value}
							</span>
						</div>
					);
				};
			}
			return item;
		});
		// 添加表格操作列
		let oprCol = createOprationColumn.call(this, props, { tableButtonClick });
		meta[tableId].items.push(oprCol);
	}

	return meta;
}
