import { ajax } from 'nc-lightapp-front';
import ampub from 'ampub';

const { components, utils, commonConst } = ampub;

const { ScriptReturnUtils } = components;

const { getScriptListReturn } = ScriptReturnUtils;

const { msgUtils } = utils;

const { showMessage } = msgUtils;

const { StatusUtils } = commonConst;

const { UISTATE } = StatusUtils;

import { linkToCard } from './buttonClick';

/**
 * 表格操作按钮
 * @param {*} props 
 * @param {*} key 
 * @param {*} text 
 * @param {*} record 
 * @param {*} index 
 */
export default function(props, key, text, record, index) {
	switch (key) {
		case 'Edit':
			edit.call(this, props, record, index);
			break;
		case 'Delete':
			singleDel.call(this, props, record, index);
			break;
		case 'Commit':
			commit.call(this, props, record, index);
			break;
		case 'UnCommit':
			unCommit.call(this, props, record, index);
			break;
		case 'QueryAboutBillFlow':
			queryAboutBillFlow.call(this, props, record, index);
			break;
		default:
			break;
	}
}

/**
 * 表格行修改
 * @param {*} props 
 * @param {*} record 
 */
function edit(props, record) {
	let { pageConfig = {} } = props;
	let { pkField, url, resourceCode } = pageConfig;
	let pk = record[pkField].value;
	let data = {
		pk,
		resourceCode
	};
	ajax({
		url: url.editUrl,
		data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					showMessage.call(this, props, { color: 'warning', content: data });
				} else {
					linkToCard.call(this, props, record, UISTATE.edit);
				}
			}
		}
	});
}

/**
 * 删除
 * 
 * @param {*} props 
 */
function singleDel(props, record, index) {
	let { pageConfig = {} } = props;
	let { tableId, pagecode, pkField, url, dataSource } = pageConfig;
	let id = record[pkField].value;
	let ts = record['ts'].value;
	let params = [
		{
			id,
			index
		}
	];
	let paramInfoMap = {
		[id]: ts
	};

	ajax({
		url: url.deleteUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: pagecode.replace('list', 'card'),
			commitType: 'commit' //提交
		},
		success: (res) => {
			getScriptListReturn.call(
				this,
				params,
				res,
				props,
				pkField,
				tableId.replace('list', 'card'),
				tableId,
				true,
				dataSource
			);
		}
	});
}

/**
 * 提交
 * 
 * @param {*} props 
 */
function commit(props, record, index) {
	runScript.call(this, props, 'SAVE', record, index);
}

/**
 * 提交
 * 
 * @param {*} props 
 */
function unCommit(props, record, index) {
	runScript.call(this, props, 'UNSAVE', record, index);
}

/**
 * 提交
 * 
 * @param {*} props 
 */
function runScript(props, actionType, record, index) {
	let { pageConfig = {} } = props;
	let { tableId, pagecode, pkField, url, dataSource } = pageConfig;
	let id = record[pkField].value;
	let ts = record['ts'].value;
	let params = [
		{
			id,
			index
		}
	];
	let paramInfoMap = {
		[id]: ts
	};

	ajax({
		url: url.commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: actionType,
			pageid: pagecode.replace('list', 'card'),
			commitType: 'commit' //提交
		},
		success: (res) => {
			getScriptListReturn.call(
				this,
				params,
				res,
				props,
				pkField,
				tableId.replace('list', 'card'),
				tableId,
				false,
				dataSource
			);
		}
	});
}

/**
 * 审批详情
 * 
 * @param {*} props 
 * @param {*} record 
 * @param {*} index 
 */
function queryAboutBillFlow(props, record, index) {
	let { pageConfig = {} } = props;
	let { pkField } = pageConfig;
	let id = record[pkField].value;
	let transi_type = record['transi_type'].value;
	this.setState({
		pk_bill: id,
		transi_type,
		showApprove: true
	});
}
