import React, { Component } from 'react';
import { base, high } from 'nc-lightapp-front';
import ampub from 'ampub';

const { components, utils, commonConst } = ampub;

const { ApprovalTrans } = components;

const { multiLangUtils, cardUtils, closeBrowserUtils } = utils;

const { getMultiLangByID } = multiLangUtils;
const { createCardTitleArea, createCardPaginationArea, getCommonTableHeadLeft } = cardUtils;

const { StatusUtils, CommonKeys } = commonConst;

const { UISTATE } = StatusUtils;
const { TransferConst } = CommonKeys;

import {
	buttonClick,
	initTemplate,
	headAfterEvent,
	bodyAfterEvent,
	pageInfoClick,
	save,
	backToList,
	rowSelected,
	commit,
	saveCommit
} from './events';
import { pageConfig } from './const';

const { NCAffix } = base;
const { BillTrack, ApproveDetail } = high;

/**
 * 主子表卡片页面入口文件
 */
class MasterChildCardBase extends Component {
	constructor(props) {
		super(props);
		this.state = {
			pk_bill: '',
			transi_type: '',
			showApprove: false,
			showBillTrack: false,
			compositedata: {},
			compositedisplay: false,
			billStateFlag: false
		};
		if (
			props.getUrlParam(TransferConst.srctype) == TransferConst.type ||
			props.getUrlParam('srctype') == TransferConst.type
		) {
			this.isTransfer = true;
		} else {
			this.isTransfer = false;
		}
		const { pageConfig = {} } = props;
		const { formId, tableId } = pageConfig;
		closeBrowserUtils.call(this, props, { form: [ formId ], cardTable: [ tableId ] });
		initTemplate.call(this, props);
	}

	// 提交及指派回调
	getAssginUsedr = (content) => {
		let { pageConfig = {} } = this.props;
		let { formId } = pageConfig;
		let status = this.props.form.getFormStatus(formId) || UISTATE.browse;
		if (status == UISTATE.browse) {
			commit.call(this, this.props, content);
		} else {
			saveCommit.call(this, this.props, content);
		}
	};

	// 取消指派
	turnOff = () => {
		this.setState({
			compositedisplay: false
		});
	};

	//获取列表合计行信息
	getTableHeadLeft = () => {
		return getCommonTableHeadLeft.call(this, this.props);
	};

	//获取列表肩部信息
	getTableHead = () => {
		let { pageConfig = {}, button } = this.props;
		let { tableId } = pageConfig;
		let { createButtonApp } = button;

		return (
			<div className="shoulder-definition-area">
				<div className="definition-icons">
					{createButtonApp({
						area: 'card_body',
						buttonLimit: 3,
						onButtonClick: (props, id) => {
							buttonClick.call(this, props, id, tableId);
						},
						popContainer: document.querySelector('.header-button-area')
					})}
				</div>
			</div>
		);
	};

	render() {
		let { pageConfig = {} } = this.props;
		let { title, formId, tableId, pagecode, bill_type, dataSource } = pageConfig;
		const { cardTable, form, button, ncUploader } = this.props;
		const { createCardTable } = cardTable;
		const { createForm } = form;
		const { createButtonApp } = button;
		const { createNCUploader } = ncUploader;

		let {
			pk_bill,
			transi_type,
			showApprove,
			showBillTrack,
			compositedata,
			compositedisplay,
			billStateFlag
		} = this.state;

		return (
			<div className="nc-bill-card">
				<div className="nc-bill-top-area">
					<NCAffix>
						{/* 头部 header */}
						<div className="nc-bill-header-area">
							{/* 标题 title */}
							<div className="header-title-search-area">
								{createCardTitleArea.call(this, this.props, {
									title: getMultiLangByID(title),
									formId,
									backBtnClick: backToList
								})}
							</div>
							{/* 按钮区 btn-area */}
							<div className="header-button-area">
								{createButtonApp({
									area: 'card_head',
									buttonLimit: 3,
									onButtonClick: buttonClick.bind(this),
									popContainer: document.querySelector('.header-button-area')
								})}
							</div>
							{/* 翻页区 待处置报废跳转过来的页面无翻页按钮 */}
							{!(this.props.getUrlParam(TransferConst.srctype) == TransferConst.type) &&
								createCardPaginationArea.call(this, this.props, {
									formId,
									dataSource,
									pageInfoClick
								})}
						</div>
					</NCAffix>
					{/* 表单区 form-area */}
					<div className="nc-bill-form-area">
						{createForm(formId, {
							onAfterEvent: headAfterEvent.bind(this)
						})}
					</div>
				</div>
				<div className="nc-bill-bottom-area">
					{/* 列表区 table-area */}
					<div className="nc-bill-table-area">
						{createCardTable(tableId, {
							tableHeadLeft: this.getTableHeadLeft.bind(this),
							tableHead: this.getTableHead.bind(this),
							modelSave: save.bind(this),
							onAfterEvent: bodyAfterEvent.bind(this),
							showCheck: true,
							showIndex: true,
							selectedChange: rowSelected.bind(this),
							hideAdd: billStateFlag,
							hideDel: billStateFlag
						})}
					</div>
				</div>
				{/* 单据追溯框 */}
				<BillTrack
					show={showBillTrack}
					close={() => {
						this.setState({
							showBillTrack: false
						});
					}}
					pk={pk_bill}
					type={bill_type}
				/>
				{/* 审批详情 */}
				<ApproveDetail
					show={showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={transi_type}
					billid={pk_bill}
				/>
				{/* 提交及指派 */}
				{compositedisplay && (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002')}
						data={compositedata}
						display={compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				)}
				{/* 附件管理框 */}
				{createNCUploader(`${pagecode}-uploader`, {})}
			</div>
		);
	}
}

export default MasterChildCardBase;

export const baseConfig = pageConfig;
