import ampub from 'ampub';

const { components, utils, commonConst } = ampub;

const { OrgChangeEvent } = components;

const { orgChangeEvent } = OrgChangeEvent;

const { cardUtils } = utils;

const { commonBodyAfterEvent, commonHeadAfterEvent } = cardUtils;

const { StatusUtils } = commonConst;

const { BILLSTATUS } = StatusUtils;

import { setBodyBtnsEnable, addLine } from './buttonClick';

/**
 * 表头编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} oldValue 
 * @param {*} val 
 */
export function headAfterEvent(props, moduleId, key, value, oldValue, val) {
	let { pageConfig = {} } = props;
	let { pagecode, formId, tableId, clearFields, url } = pageConfig;
	if (key == 'pk_org_v') {
		//组织切换确定后发送后台请求
		let callback = () => {
			if (value.value) {
				// 若组织切换后有值则自动增行
				addLine.call(this, props);
				let config = {
					afterEditUrl: url.headAfterEditUrl,
					pagecode,
					key,
					value,
					oldValue
				};
				commonHeadAfterEvent.call(this, props, config);
			}
			setBodyBtnsEnable.call(this, props, tableId);
		};
		//组织切换
		orgChangeEvent.call(this, props, pagecode, formId, tableId, key, value, oldValue, clearFields, false, callback);
	} else {
		let config = {
			afterEditUrl: url.headAfterEditUrl,
			pagecode,
			key,
			value,
			oldValue,
			keys: [ 'pk_fiorg_v' ]
		};
		commonHeadAfterEvent.call(this, props, config);
	}
}

/**
 * 表体编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedRows 
 * @param {*} index 
 * @param {*} record 
 * @param {*} type 
 * @param {*} eventType 
 */
export function bodyAfterEvent(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	let { pageConfig = {} } = props;
	let { pagecode, formId, tableId, editAfterEventFields, url } = pageConfig;
	let billStatusVal = props.form.getFormItemsValue(formId, 'bill_status').value;
	// 平台的自动增行不会附默认值，这里调用自己的增行,非转单且为自由态才增行
	if (this.isTransfer == false && billStatusVal == BILLSTATUS.free_check) {
		let num = props.cardTable.getNumberOfRows(tableId);
		if (num == index + 1) {
			addLine.call(this, props);
		}
	}
	let config = {
		afterEditUrl: url.bodyAfterEditUrl,
		pagecode,
		key,
		record,
		changedRows,
		index,
		keys: editAfterEventFields
	};
	commonBodyAfterEvent.call(this, props, config);
}
