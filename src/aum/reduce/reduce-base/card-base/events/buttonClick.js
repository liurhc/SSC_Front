import { ajax, print, output, cardCache } from 'nc-lightapp-front';
import ampub from 'ampub';

const { components, utils, commonConst } = ampub;

const { ScriptReturnUtils, queryVocherUtils, saveValidatorsUtil, LoginContext, imageHandle } = components;

const { getScriptCardReturnData } = ScriptReturnUtils;
const { queryAboutVoucher } = queryVocherUtils;
const { beforeSaveValidator } = saveValidatorsUtil;
const { getContext, loginContextKeys } = LoginContext;
const { amImageScan, amImageView } = imageHandle;

const { cardUtils, msgUtils, multiLangUtils } = utils;

const { setBillFlowBtnsVisible, setCardValue, setHeadAreaData } = cardUtils;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { getMultiLangByID } = multiLangUtils;

const { StatusUtils, CommonKeys } = commonConst;

const { UISTATE, BILLSTATUS } = StatusUtils;
const { TransferConst } = CommonKeys;

import { headAfterEvent } from './afterEvent';

/**
 * 按钮点击
 * @param {*} props 
 * @param {*} id 
 */
export default function(props, id) {
	switch (id) {
		case 'Add':
			add.call(this, props);
			break;
		case 'Edit':
			edit.call(this, props);
			break;
		case 'Delete':
			delConfirm.call(this, props);
			break;
		case 'Save':
			save.call(this, props);
			break;
		case 'Cancel':
			cancelConfirm.call(this, props);
			break;
		case 'Refresh':
			refresh.call(this, props);
			break;
		case 'AddLine':
			addLine.call(this, props, true);
			break;
		case 'DelLine':
			delLine.call(this, props);
			break;
		case 'BatchAlter':
			batchAlter.call(this, props);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'Commit':
			commit.call(this, props);
			break;
		case 'SaveCommit':
			saveCommit.call(this, props);
			break;
		case 'UnCommit':
			unCommit.call(this, props);
			break;
		case 'GetFinanceData':
			getFianceData.call(this, props);
			break;
		case 'QueryAboutBusiness':
			queryAboutBusiness.call(this, props);
			break;
		case 'QueryAboutBillFlow':
			queryAboutBillFlow.call(this, props);
			break;
		case 'QueryAboutVoucher':
			queryAboutVoucherSelf.call(this, props);
			break;
		case 'Attachment':
			attachment.call(this, props);
			break;
		case 'ReceiptScan':
			receiptScan.call(this, props);
			break;
		case 'ReceiptShow':
			receiptShow.call(this, props);
		default:
			break;
	}
}

/**
* 新增
* @param {*} props 
*/
export function add(props) {
	let { pageConfig = {} } = props;
	let { formId, tableId } = pageConfig;
	// 关闭所有展开行
	props.cardTable.closeExpandedRow(tableId);
	// 设置新增态
	setStatus.call(this, props, UISTATE.add);
	// 设置按钮显隐性
	setBtnsVisible.call(this, props);
	// 设置组织字段可编辑
	props.form.setFormItemsDisabled(formId, {
		pk_org_v: false,
		pk_org: false
	});
	// 清空数据
	setValue.call(this, props, undefined);
	// 设置默认值
	setDefaultValue.call(this, props);
	// 设置字段的可编辑性
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	if (pk_org_v && pk_org_v.value) {
		headAfterEvent.call(this, props, formId, 'pk_org_v', pk_org_v, { value: '' }, pk_org_v.value);
	} else {
		props.initMetaByPkorg('pk_org_v');
		setBodyBtnsEnable.call(this, props, tableId);
	}
}

/**
 * 新增设置默认值
 * @param {*} props 
 */
function setDefaultValue(props) {
	let { pageConfig = {} } = props;
	let { formId, bill_type } = pageConfig;
	// 模板默认值处理

	// 公共默认值处理
	// 集团
	let groupId = getContext(loginContextKeys.groupId);
	let groupName = getContext(loginContextKeys.groupName);
	// 交易类型
	let transi_type = getContext(loginContextKeys.transtype);
	let pk_transitype = getContext(loginContextKeys.pk_transtype);
	// 默认主组织
	let pk_org = getContext(loginContextKeys.pk_org);
	let org_Name = getContext(loginContextKeys.org_Name);
	let pk_org_v = getContext(loginContextKeys.pk_org_v);
	let org_v_Name = getContext(loginContextKeys.org_v_Name);
	props.form.setFormItemsValue(formId, {
		pk_group: { value: groupId, display: groupName },
		pk_org: { value: pk_org, display: org_Name },
		pk_org_v: { value: pk_org_v, display: org_v_Name },
		bill_type: { value: bill_type },
		transi_type: { value: transi_type },
		pk_transitype: { value: pk_transitype }
	});
}

/**
* 修改
* @param {*} props 
*/
export function edit(props, validatePermission = true) {
	let { pageConfig = {} } = props;
	let { formId, tableId, pkField, url, resourceCode } = pageConfig;
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	if (!pk || pk == 'undefined' || pk == 'null') {
		pk = props.getUrlParam('id');
	}
	if (!pk || pk == 'undefined' || pk == 'null') {
		return;
	}
	let editAction = () => {
		let billStatusVal = props.form.getFormItemsValue(formId, 'bill_status').value;
		// 单据状态为已提交或审批中
		if (billStatusVal == BILLSTATUS.un_check || billStatusVal == BILLSTATUS.check_going) {
			// 隐藏侧拉框增行与删行按钮，取消自动增行
			this.state.billStateFlag = true;
		} else {
			this.state.billStateFlag = false;
		}
		// 关闭所有展开行
		props.cardTable.closeExpandedRow(tableId);
		// 设置修改态
		setStatus.call(this, props, UISTATE.edit);
		// 设置按钮显隐性
		setBtnsVisible.call(this, props);
		// 设置组织字段不可编辑
		props.form.setFormItemsDisabled(formId, {
			pk_org_v: true,
			pk_org: true
		});
	};

	if (validatePermission) {
		let data = {
			pk,
			resourceCode
		};
		ajax({
			url: url.editUrl,
			data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if (data) {
						showMessage.call(this, props, { color: 'warning', content: data });
					} else {
						editAction();
					}
				}
			}
		});
	} else {
		editAction();
	}
}

/**
* 删除确认
* @param {*} props 
*/
export function delConfirm(props) {
	let { pageConfig = {} } = props;
	let { formId, pkField } = pageConfig;
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	if (!pk) {
		return;
	}
	showConfirm.call(this, props, { type: MsgConst.Type.Delete, beSureBtnClick: singleDel });
}

/**
 * 删除
 * @param {*} props 
 * @param {*} operatorType 
 * @param {*} commitType 
 */
function singleDel(props) {
	let { pageConfig = {} } = props;
	let { formId, tableId, pagecode, pkField, url, dataSource, transferListRouter, transferFromAppcode } = pageConfig;
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let ts = props.form.getFormItemsValue(formId, 'ts').value;
	let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let paramInfoMap = {
		[pk]: ts
	};
	let obj = {
		dataType: 'cardData',
		OperatorType: 'DELETE',
		commitType: 'commit',
		pageid: pagecode,
		paramInfoMap: paramInfoMap
	};
	cardData.userjson = JSON.stringify(obj);
	ajax({
		url: url.deleteUrl,
		data: cardData,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (props.getUrlParam(TransferConst.srctype) == TransferConst.type) {
					// 待处置待报废删除跳转到待处理页面
					if (data.success === 'allsuccess') {
						props.pushTo(transferListRouter, {
							appcode: transferFromAppcode,
							pagecode: transferFromAppcode + '_list'
						});
					} else {
						showMessage.call(this, props, {
							color: 'danger',
							content: res.data.errorMsg
						});
					}
				} else {
					// 普通删除
					let callback = (newpk) => {
						// 更新参数
						props.setUrlParam({ id: newpk });
						// 加载下一条数据
						let loadCallback = () => {
							setBtnsVisible.call(this, props);
						};
						loadDataByPk.call(this, props, newpk, loadCallback);
					};
					getScriptCardReturnData.call(
						this,
						res,
						props,
						formId,
						tableId,
						pkField,
						dataSource,
						undefined,
						true,
						callback
					);
				}
			}
		}
	});
}

/**
* 返回
* @param {*} props 
*/
export function backToList(props) {
	let { pageConfig = {} } = props;
	let { pagecode, listRouter, transferListRouter, transferFromAppcode } = pageConfig;
	if (props.getUrlParam(TransferConst.srctype) == 'transfer') {
		// 从待处置/待报废跳转过来
		props.pushTo(transferListRouter, {
			appcode: transferFromAppcode,
			pagecode: transferFromAppcode + '_list'
		});
	} else {
		props.pushTo(listRouter, { pagecode: pagecode.replace('card', 'list') });
	}
}

/**
* 保存
* @param {*} props 
*/
export function save(props) {
	let { pageConfig = {}, cardPagination } = props;
	let { formId, tableId, pagecode, pkField, url, dataSource } = pageConfig;
	// 必输项校验和过滤空行
	let flag = validateBeforeSave.call(this, props);
	if (!flag) {
		return;
	}
	let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let reqUrl = url.insertUrl; //新增保存
	let oldstatus = props.form.getFormStatus(formId);
	if (oldstatus == UISTATE.edit) {
		reqUrl = url.updateUrl; //修改保存
	}
	// 保存前执行验证公式
	props.validateToSave(cardData, () => {
		// 调用Ajax保存数据
		ajax({
			url: reqUrl,
			data: cardData,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					// 性能优化，关闭表单和表格渲染
					props.beforeUpdatePage();
					setStatus.call(this, props, UISTATE.browse);
					setValue.call(this, props, data);
					showMessage.call(this, props, { type: MsgConst.Type.SaveSuccess });
					let pk = props.form.getFormItemsValue(formId, pkField).value;
					if (props.getUrlParam(TransferConst.srctype) == TransferConst.type) {
						// 从待处置待报废跳转过来，保存成功后处理缓存
						afterTransferSave.call(this, props);
					} else {
						let cachData = props.createMasterChildData(pagecode, formId, tableId);
						// 普通单据保存成功后处理缓存
						if (oldstatus == UISTATE.add) {
							cardCache.addCache(pk, cachData, formId, dataSource);
						} else {
							cardCache.updateCache(pkField, pk, cachData, formId, dataSource);
						}
					}
					// 恢复字段的编辑性（已提交和审核中状态点击修改仅有是否报废/处置收入、处置支出为可编辑）
					let oldBillStatus = cardData.head[formId].rows[0].values['bill_status'].value;
					if (oldBillStatus == BILLSTATUS.un_check || oldBillStatus == BILLSTATUS.check_going) {
						props.resMetaAfterPkorgEdit();
					}
					// 设置按钮显隐性
					setBtnsVisible.call(this, props);
					// 性能优化，表单和表格统一渲染
					props.updatePage(formId, tableId);
				}
			}
		});
	});
}

/**
* 保存前校验
* @param {*} props 
*/
function validateBeforeSave(props) {
	let { pageConfig = {} } = props;
	let { formId, tableId, nodeFlag } = pageConfig;
	// 必输项校验和清除无效行
	let flag = beforeSaveValidator.call(this, props, formId, tableId);
	if (!flag) {
		return false;
	}
	if (nodeFlag == 'disused') {
		let allRows = props.cardTable.getVisibleRows(tableId);
		// 确认标记校验
		let confirmFlag = false;
		allRows.map((item) => {
			if (item.values['confirm_flag'].value == true) {
				confirmFlag = true;
			}
		});
		if (!confirmFlag) {
			showMessage.call(this, props, {
				color: 'warning',
				content: getMultiLangByID('452004004A-000005')
			}); /*国际化处理  单据没有需要报废的设备！svg*/
			return false;
		}
	}
	return true;
}

/**
 * 转单保存后缓存处理
 * @param {*} props 
 */
function afterTransferSave(props) {
	let { pageConfig = {} } = props;
	let { tableId, transferDataSource } = pageConfig;
	let allRows = props.cardTable.getAllRows(tableId);
	let pkEquipValue = [];
	allRows.map((item) => {
		pkEquipValue.push(item.values['pk_equip'].value);
	});
	pkEquipValue.map((item) => {
		cardCache.deleteCacheById('pk_equip', item, transferDataSource);
	});
}

/**
* 取消
* @param {*} props 
*/
function cancelConfirm(props) {
	showConfirm.call(this, props, { type: MsgConst.Type.Cancel, beSureBtnClick: cancel });
}

/**
* 取消
* @param {*} props 
*/
function cancel(props) {
	let { pageConfig = {} } = props;
	let { formId, tableId, pkField, transferListRouter, dataSource, transferFromAppcode } = pageConfig;
	if (props.getUrlParam(TransferConst.srctype) == 'transfer') {
		// 待报废/待处置跳转过来
		let pk = props.form.getFormItemsValue(formId, pkField).value;
		if (!pk) {
			props.pushTo(transferListRouter, {
				appcode: transferFromAppcode,
				pagecode: transferFromAppcode + '_list'
			});
		} else {
			props.form.cancel(formId);
			props.cardTable.resetTableData(tableId);
			setStatus.call(this, props, UISTATE.browse);
			setBtnsVisible.call(this, props);
		}
	} else {
		// 恢复字段的可编辑性
		let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
		let billStatusVal = props.form.getFormItemsValue(formId, 'bill_status').value;
		if (
			!pk_org_v ||
			!pk_org_v.value ||
			billStatusVal == BILLSTATUS.un_check ||
			billStatusVal == BILLSTATUS.check_going
		) {
			props.resMetaAfterPkorgEdit();
		}
		setStatus.call(this, props, UISTATE.browse);
		// 加载编辑前数据，如果编辑前没有数据，则加载当前列表最后一条数据，如果还没有，显示为空不处理
		let pk = cardCache.getCurrentId(dataSource);
		if (!pk) {
			pk = cardCache.getCurrentLastId(dataSource);
		}
		let callback = () => {
			setBtnsVisible.call(this, props);
		};
		loadDataByPk.call(this, props, pk, callback);
	}
}

/**
* 刷新
* @param {*} props 
*/
function refresh(props) {
	let { pageConfig = {} } = props;
	let { formId, pkField } = pageConfig;
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	if (!pk) {
		return;
	}
	let callback = (data) => {
		// 设置按钮显隐性
		setBtnsVisible.call(this, props);
		if (data) {
			showMessage.call(this, props, { type: MsgConst.Type.RefreshSuccess });
		}
	};
	// 查询到最新单据数据
	getDataByPk.call(this, props, pk, callback);
}

/**
* 新增行
* @param {*} props 
*/
export function addLine(props, isFocus = false) {
	let { pageConfig = {} } = props;
	let { tableId } = pageConfig;
	props.cardTable.addRow(tableId, undefined, undefined, isFocus);
	setBodyBtnsEnable.call(this, props, tableId);
}

/**
* 删除行
* @param {*} props 
*/
function delLine(props) {
	let { pageConfig = {} } = props;
	let { tableId } = pageConfig;
	let checkedRows = props.cardTable.getCheckedRows(tableId);
	let checkedIndex = [];
	checkedRows.map((item) => {
		checkedIndex.push(item.index);
	});
	props.cardTable.delRowsByIndex(tableId, checkedIndex);
	setBodyBtnsEnable.call(this, props, tableId);
}

/**
 * 批改操作
 * @param {*} props 
 */
function batchAlter(props) {
	let { pageConfig = {} } = props;
	let { formId, tableId, pagecode, url } = pageConfig;
	let num = props.cardTable.getNumberOfRows(tableId);
	if (num <= 1) {
		return;
	}
	// 获取原始数据
	let changeData = props.cardTable.getTableItemData(tableId);
	// 如果需要批改的字段需要特殊的处理，则调用后端的批改操作
	let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	ajax({
		url: url.batchAlterUrl,
		data: {
			card: cardData,
			batchChangeIndex: changeData.batchChangeIndex, // 原始数据行号
			batchChangeKey: changeData.batchChangeKey, // 原始数据key
			batchChangeValue: changeData.batchChangeValue, // 原始数据value
			// 原始数据所在的vo，对应聚合VO中的tableCodes，一主一子可为空，一主多子要传对应的值
			tableCode: ''
		},
		success: (res) => {
			let { success, data } = res;
			if (success) {
				// 性能优化，关闭表单和表格渲染
				props.beforeUpdatePage();
				// 将数据设置到页面上
				setValue.call(this, props, data);
				// 性能优化，表单和表格统一渲染
				props.updatePage(formId, tableId);
			}
		}
	});
}

/**
 * 设置界面状态
 * @param {*} props 
 * @param {*} status 
 */
export function setStatus(props, status = UISTATE.browse) {
	let { pageConfig = {} } = props;
	let { formId, tableId, editableFieldsInApprove } = pageConfig;
	let billStatusVal = props.form.getFormItemsValue(formId, 'bill_status').value;
	switch (status) {
		case UISTATE.add:
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, UISTATE.edit);
			setBodyBtnsEnable.call(this, props, tableId);
			break;
		case UISTATE.edit:
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, status);
			setBodyBtnsEnable.call(this, props, tableId);
			// 单据状态为已提交或审批中
			if (billStatusVal == BILLSTATUS.un_check || billStatusVal == BILLSTATUS.check_going) {
				// 设置是否报废为可编辑，其余字段不可编辑
				props.initMetaByPkorg('pk_org_v');
				props.form.setFormItemsDisabled(formId, {
					pk_org_v: true
				});
				props.cardTable.setColEditableByKey(tableId, editableFieldsInApprove, false);
			}
			break;
		case UISTATE.browse:
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, status);
			break;
		default:
			break;
	}
	setHeadAreaData.call(this, props, { status });
}

/**
 * 通过单据id获取缓存单据信息，没有缓存则重新查询
 * @param {*} props 
 * @param {*} pk 
 */
export function loadDataByPk(props, pk, callback) {
	let { pageConfig = {} } = props;
	let { dataSource } = pageConfig;
	if (!pk || pk == 'null') {
		setValue.call(this, props, undefined);
		typeof callback == 'function' && callback();
	} else {
		let cachData = cardCache.getCacheById(pk, dataSource);
		if (cachData) {
			setValue.call(this, props, cachData);
			typeof callback == 'function' && callback(cachData);
		} else {
			getDataByPk.call(this, props, pk, callback);
		}
	}
}

/**
 * 通过单据id查询单据信息
 * @param {*} props 
 * @param {*} pk 
 */
function getDataByPk(props, pk, callback) {
	let { pageConfig = {} } = props;
	let { formId, pagecode, pkField, url, dataSource, tableId } = pageConfig;
	ajax({
		url: url.queryCardUrl,
		data: {
			pagecode,
			pk
		},
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					// 性能优化，关闭表单和表格渲染
					props.beforeUpdatePage();
					setValue.call(this, props, data);
					cardCache.updateCache(pkField, pk, data, formId, dataSource);
					// 性能优化，表单和表格统一渲染
					props.updatePage(formId, tableId);
				} else {
					// 未查到数据，赋空值
					setValue.call(this, props, undefined);
					cardCache.deleteCacheById(pkField, pk, dataSource);
					showMessage.call(this, props, { type: MsgConst.Type.DataDeleted });
				}
				typeof callback == 'function' && callback(data);
			}
		}
	});
}

/**
 * 设置界面值
 * @param {*} props 
 * @param {*} data 
 */
export function setValue(props, data) {
	setCardValue.call(this, props, data);
}

/**
 * 设置按钮显示隐藏
 * @param {*} props 
 */
export function setBtnsVisible(props) {
	let { pageConfig = {} } = props;
	let { formId, editBtns, browseBtns, pkField } = pageConfig;
	let status = props.form.getFormStatus(formId) || UISTATE.browse;
	let billStatusVal = props.form.getFormItemsValue(formId, 'bill_status').value;
	let pkVal = props.form.getFormItemsValue(formId, pkField);
	let btnObj = {};
	if (status != UISTATE.browse) {
		browseBtns.map((item) => {
			btnObj[item] = false;
		});
		editBtns.map((item) => {
			btnObj[item] = true;
		});
	} else {
		editBtns.map((item) => {
			btnObj[item] = false;
		});
		if (!pkVal || !pkVal.value) {
			browseBtns.map((item) => {
				btnObj[item] = false;
			});
			btnObj['Add'] = true;
		} else {
			browseBtns.map((item) => {
				btnObj[item] = true;
			});
		}
	}
	// 待处置、报废页面传过来，隐藏新增按钮
	if (props.getUrlParam(TransferConst.srctype) == 'transfer') {
		btnObj['Add'] = false;
	}
	props.button.setButtonVisible(btnObj);
	setBillFlowBtnsVisible.call(this, props, formId, pkField);
	// 单据状态为已提交过审批中需要特殊处理
	if (billStatusVal == BILLSTATUS.un_check || billStatusVal == BILLSTATUS.check_going) {
		if (status == UISTATE.edit) {
			props.button.setButtonVisible('SaveCommit', false);
		} else if (status == UISTATE.browse && pkVal.value) {
			props.button.setButtonVisible('Edit', true);
		}
	}
}

/**
 * 设置表体按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBodyBtnsEnable(props, moduleId) {
	let { pageConfig = {} } = props;
	let { formId } = pageConfig;
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	let bill_status = props.form.getFormItemsValue(formId, 'bill_status');
	if (
		!pk_org_v ||
		!pk_org_v.value ||
		(bill_status && (bill_status.value == BILLSTATUS.un_check || bill_status.value == BILLSTATUS.check_going))
	) {
		props.button.setButtonDisabled([ 'AddLine', 'DelLine', 'BatchAlter' ], true);
	} else {
		props.button.setButtonDisabled([ 'AddLine', 'BatchAlter' ], false);
		let checkedRows = [];
		let num = props.cardTable.getNumberOfRows(moduleId, false);
		if (num > 0) {
			checkedRows = props.cardTable.getCheckedRows(moduleId);
		}
		props.button.setButtonDisabled([ 'DelLine' ], !(checkedRows && checkedRows.length > 0));
	}
}

/**
 * 打印
 * @param {*} props 
 */
function printTemp(props) {
	let { pageConfig = {} } = props;
	let { url } = pageConfig;
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		url.printUrl, // 后台打印服务url
		printData
	);
}

/**
 * 输出
 * @param {*} props 
 */
function outputTemp(props) {
	let { pageConfig = {} } = props;
	let { url } = pageConfig;
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });
		return;
	}
	output({
		url: url.printUrl,
		data: printData
	});
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let { pageConfig = {} } = props;
	let { formId, pkField, printFilename, printNodekey } = pageConfig;
	let pk = props.form.getFormItemsValue(formId, pkField);
	if (!pk || !pk.value) {
		return false;
	}
	let pks = [ pk.value ];
	let printData = {
		filename: getMultiLangByID(printFilename) /*国际化处理  资产处置/资产报废 svg */,
		nodekey: printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

/**
 * 保存提交
 * @param {*} props 
 * @param {*} operatorType 
 * @param {*} commitType 
 */
export function saveCommit(props, content) {
	let { pageConfig = {} } = props;
	let { formId, tableId, pagecode, pkField, url, dataSource } = pageConfig;
	// 必输项校验和过滤空行
	let flag = validateBeforeSave.call(this, props);
	if (!flag) {
		return;
	}

	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let ts = props.form.getFormItemsValue(formId, 'ts').value;
	let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let paramInfoMap = {
		[pk]: ts
	};
	let obj = {
		dataType: 'cardData',
		OperatorType: 'SAVE',
		commitType: 'saveCommit',
		pageid: pagecode,
		paramInfoMap: paramInfoMap,
		content
	};
	cardData.userjson = JSON.stringify(obj);
	let saveCommitAction = () => {
		// 调用Ajax保存提交数据
		ajax({
			url: url.commitUrl,
			data: cardData,
			success: (res) => {
				let { success } = res;
				if (success) {
					// 性能优化，关闭表单和表格渲染
					props.beforeUpdatePage();
					if (content) {
						this.setState({
							compositedisplay: false
						});
					}
					let setCommonValue = (data) => {
						// 设置浏览态
						setStatus.call(this, props, UISTATE.browse);
						// 设置值
						setValue.call(this, props, data);
					};
					let callback = () => {
						// 设置按钮显隐性
						setBtnsVisible.call(this, props);
					};
					getScriptCardReturnData.call(
						this,
						res,
						props,
						formId,
						tableId,
						pkField,
						dataSource,
						setCommonValue,
						false,
						callback,
						pagecode
					);
					// 待处置待报废页面跳转过来处理缓存
					if (props.getUrlParam(TransferConst.srctype) == TransferConst.type) {
						afterTransferSave.call(this, props);
					}
					// 性能优化，表单和表格统一渲染
					props.updatePage(formId, tableId);
				}
			}
		});
	};
	if (content) {
		saveCommitAction();
	} else {
		// 保存前执行验证公式
		props.validateToSave(cardData, saveCommitAction);
	}
}

/**
 * 提交
 * @param {*} props 
 * @param {*} operatorType 
 * @param {*} commitType 
 */
export function commit(props, content) {
	runScript.call(this, props, 'SAVE', 'commit', content);
}

/**
 * 收回
 * @param {*} props 
 * @param {*} operatorType 
 * @param {*} commitType 
 */
function unCommit(props) {
	runScript.call(this, props, 'UNSAVE', 'commit');
}

/**
 * 提交
 * @param {*} props 
 * @param {*} operatorType 
 * @param {*} commitType 
 */
function runScript(props, operatorType, commitType, content) {
	let { pageConfig = {} } = props;
	let { formId, tableId, pagecode, pkField, url, dataSource } = pageConfig;
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let ts = props.form.getFormItemsValue(formId, 'ts').value;
	let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let paramInfoMap = {
		[pk]: ts
	};
	let obj = {
		dataType: 'cardData',
		OperatorType: operatorType,
		commitType: commitType,
		pageid: pagecode,
		paramInfoMap: paramInfoMap,
		content
	};
	cardData.userjson = JSON.stringify(obj);
	ajax({
		url: url.commitUrl,
		data: cardData,
		success: (res) => {
			let { success } = res;
			if (success) {
				// 性能优化，关闭表单和表格渲染
				props.beforeUpdatePage();
				if (content) {
					this.setState({
						compositedisplay: false
					});
				}
				let setCommonValue = (data) => {
					setValue.call(this, props, data);
				};
				let callback = () => {
					setBtnsVisible.call(this, props);
				};
				getScriptCardReturnData.call(
					this,
					res,
					props,
					formId,
					tableId,
					pkField,
					dataSource,
					setCommonValue,
					false,
					callback,
					pagecode
				);
				// 性能优化，表单和表格统一渲染
				props.updatePage(formId, tableId);
			}
		}
	});
}

/**
* 取财务数据
* 
* @param {*} props 
*/
function getFianceData(props) {
	let { pageConfig = {} } = props;
	let { formId, tableId, pagecode, url } = pageConfig;
	props.cardTable.filterEmptyRows(tableId);
	let pk_equips = [];
	let allRows = props.cardTable.getAllRows(tableId);
	allRows.map((item) => {
		let pk_equip = item.values.pk_equip.value;
		if (pk_equip) {
			pk_equips.push(pk_equip);
		}
	});

	if (pk_equips.length == 0) {
		showMessage.call(this, props, {
			color: 'warning',
			content: getMultiLangByID('452004004A-000002')
		}); /*国际化处理 表体没有选择的设备 svg */
		return;
	}
	let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	ajax({
		url: url.getFianceDataUrl,
		data: cardData,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				// 性能优化，关闭表单和表格渲染
				props.beforeUpdatePage();
				setValue.call(this, props, data);
				// 性能优化，表单和表格统一渲染
				props.updatePage(formId, tableId);
			}
		}
	});
}

/**
 * 单据追溯
 * @param {*} props 
 */
function queryAboutBusiness(props) {
	let { pageConfig = {} } = props;
	let { formId, pkField } = pageConfig;
	let pkVal = props.form.getFormItemsValue(formId, pkField);
	if (pkVal && pkVal.value) {
		this.setState({
			pk_bill: pkVal.value,
			showBillTrack: true
		});
	}
}

/**
 * 审批详情
 * 
 * @param {*} props 
 */
function queryAboutBillFlow(props) {
	let { pageConfig = {} } = props;
	let { formId, pkField } = pageConfig;
	let pkVal = props.form.getFormItemsValue(formId, pkField);
	let transi_type = props.form.getFormItemsValue(formId, 'transi_type');
	if (pkVal && pkVal.value) {
		this.setState({
			pk_bill: pkVal.value,
			transi_type: transi_type.value,
			showApprove: true
		});
	}
}

/**
 * 联查凭证
 * @param {*} props 
 */
function queryAboutVoucherSelf(props) {
	let { pageConfig = {} } = props;
	let { formId, pkField, appcode } = pageConfig;
	queryAboutVoucher.call(this, props, formId, pkField, appcode, false);
}

/**
 * 附件管理
 * @param {*} props 
 */
function attachment(props) {
	let { pageConfig = {} } = props;
	let { formId, pagecode, pkField } = pageConfig;
	// billId 是单据主键
	let billId = props.form.getFormItemsValue(formId, pkField).value;
	props.ncUploader.show(`${pagecode}-uploader`, {
		billId: 'pam/reduce/' + billId
	});
}

/**
 * 影像扫描
 * @param {*} props 
 */
function receiptScan(props) {
	let { pageConfig } = props;
	let { pagecode, pkField, formId, tableId } = pageConfig;
	let imageData = {
		pagecode,
		formId,
		tableId,
		pkField
	};
	amImageScan(props, imageData);
}

/**
 * 影像查看
 * @param {*} props 
 */
function receiptShow(props) {
	let { pageConfig } = props;
	let { pagecode, pkField, formId, tableId } = pageConfig;
	let imageData = {
		pagecode,
		formId,
		tableId,
		pkField
	};
	amImageView(props, imageData);
}
