import { ajax } from 'nc-lightapp-front';
import ampub from 'ampub';

const { components, utils, commonConst } = ampub;

const { queryAboutUtils, refInit, LoginContext } = components;

const { openEquipCardByPk } = queryAboutUtils;
const { defRefCondition } = refInit;
const { loginContext, getContext, loginContextKeys } = LoginContext;

const { cardUtils } = utils;

const { createOprationColumn, afterModifyCardMeta } = cardUtils;

const { StatusUtils, CommonKeys } = commonConst;

const { UISTATE, BILLSTATUS } = StatusUtils;
const { IBusiRoleConst, TransferConst } = CommonKeys;

import { add, edit, setStatus, setValue, loadDataByPk, setBtnsVisible } from './buttonClick';
import tableButtonClick from './tableButtonClick';

/**
 * 初始化模板
 * @param {*} props 
 */
export default function(props) {
	let { pageConfig = {} } = props;
	let { pagecode } = pageConfig;
	props.createUIDom(
		{
			// 页面编码
			pagecode
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta, () => {
						afterInit.call(this, props);
					});
				}
			}
		}
	);
}

/**
 * 模板初始化以后
 * @param {*} props 
 */
function afterInit(props) {
	let { pageConfig = {} } = props;
	let { formId, pagecode, bill_type, url, transferDataSource } = pageConfig;
	let status = props.getUrlParam('status') || UISTATE.browse;
	//从资产树跳转过来
	let srctype = props.getUrlParam('srctype');
	//从待报废/处置跳转过来
	let type = props.getUrlParam('type');
	if (status == UISTATE.add) {
		if (srctype == TransferConst.type || type == TransferConst.type) {
			// 设置新增态
			setStatus.call(this, props, UISTATE.add);
			// 设置按钮显隐性
			setBtnsVisible.call(this, props);
			// 交易类型编码
			const transi_type = getContext(loginContextKeys.transtype);
			let transferInfo = getContext(`${bill_type}+${transi_type}`, transferDataSource);
			let pks = [];
			let userdefObj = {};
			if (transferInfo) {
				pks = transferInfo.pks;
				// 待报废/处置生成报废单取传过来的pk_org
				userdefObj = { pk_org: transferInfo.pk_org };
			} else {
				let pk = props.getUrlParam('equipid');
				let ts = props.getUrlParam('equipts');
				if (pk) {
					pks = [];
					pks.push({ head: { pk, ts } });
				}
				// 其他生成报废/处置单不传pk_org
				userdefObj = { pk_org: '' };
			}

			if (pks && pks.length > 0) {
				ajax({
					url: url.transferEquipUrl,
					data: {
						pageid: pagecode,
						pks,
						srcbilltype: '4A00',
						srctranstype: '4A00-01',
						nextbilltype: bill_type,
						nexttranstype: transi_type,
						userdefObj
					},
					success: (res) => {
						let { success, data } = res;
						if (success) {
							setValue.call(this, props, data);
							if (transferInfo) {
								// 待报废待处置生成报废单、处置单设置组织不可编辑
								props.form.setFormItemsDisabled(formId, {
									pk_org_v: true,
									pk_org: true
								});
							}
						}
					}
				});
			}
		} else {
			add.call(this, props);
		}
	} else if (status == UISTATE.edit) {
		setStatus.call(this, props, UISTATE.browse);
		// 设置按钮显隐性
		setBtnsVisible.call(this, props);
		let pk = props.getUrlParam('id');
		if (pk && pk != 'undefined' && pk != 'null') {
			props.button.setButtonVisible('Add', false);
			let callback = () => {
				edit.call(this, props, false);
			};
			loadDataByPk.call(this, props, pk, callback);
		}
	} else {
		setStatus.call(this, props, status);
		// 设置按钮显隐性
		setBtnsVisible.call(this, props);
		let pk = props.getUrlParam('id');
		if (pk && pk != 'undefined' && pk != 'null') {
			let callback = () => {
				setBtnsVisible.call(this, props);
			};
			loadDataByPk.call(this, props, pk, callback);
		}
	}
}

/**
 * 修改模板
 * @param {*} props 
 * @param {*} meta 
 */
function modifierMeta(props, meta) {
	let { pageConfig = {} } = props;
	let { formId, bill_type, tableId, refFilter } = pageConfig;
	const pk_group = getContext(loginContextKeys.groupId);
	const transi_type = getContext(loginContextKeys.transtype);
	// 表头参照过滤
	if (meta[formId] && meta[formId].items && meta[formId].items.length > 0) {
		meta[formId].items.map((item) => {
			if (
				item.itemtype == 'refer' &&
				item.attrcode != 'pk_org_v' &&
				item.attrcode != 'pk_org' &&
				item.attrcode != 'pk_fiorg_v' &&
				item.attrcode != 'pk_fiorg'
			) {
				item.queryCondition = () => {
					let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
					return { pk_org }; // 根据pk_org过滤
				};
			}
			// 组织权限过滤
			if (item.attrcode == 'pk_org_v') {
				item.queryCondition = () => {
					return { GridRefActionExt: refFilter.orgRefFilter };
				};
			} else if (item.attrcode == 'pk_recorder') {
				//经办人和借用部门根据pk_org和设置了权限的业务单元过滤
				item.queryCondition = () => {
					let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
					return {
						pk_org,
						busifuncode: IBusiRoleConst.ASSETORG
					};
				};
			} else {
				//自定义项的过滤
				defRefCondition.call(this, props, item, formId, pk_group);
			}
		});
	}
	// 表体参照过滤
	if (meta[tableId] && meta[tableId].items && meta[tableId].items.length > 0) {
		meta[tableId].items.map((item) => {
			if (
				item.itemtype == 'refer' &&
				// 库存组织不过滤
				item.itemtype != 'pk_icorg_v' &&
				item.itemtype != 'pk_icorg'
			) {
				item.queryCondition = () => {
					let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
					return { pk_org }; // 根据pk_org过滤
				};
			}
			// 资产卡片多选
			if (item.attrcode == 'pk_equip') {
				item.isMultiSelectedEnabled = true;
				//给资产编码列添加超链接
				item.renderStatus = UISTATE.browse;
				item.render = (text, record, index) => {
					return (
						<div class="card-table-browse">
							<span
								className="code-detail-link"
								onClick={() => {
									openEquipCardByPk.call(this, props, record.values[item.attrcode].value);
								}}
							>
								{record.values[item.attrcode] && record.values[item.attrcode].display}
							</span>
						</div>
					);
				};
				//设置设备的查询条件
				item.queryCondition = () => {
					let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value; // 调用相应组件的取值API
					return {
						pk_org,
						transi_type,
						bill_type,
						GridRefActionExt: refFilter.equipRefFilter
					};
				};
			} else if (item.attrcode == 'pk_user_after' || item.attrcode == 'pk_usedept_after_v') {
				//使用人，使用部门参照当前使用权可见的人或者部门
				item.queryCondition = () => {
					// let rowData = props.cardTable.getClickRowIndex(tableId);
					let index = props.cardTable.getCurrentIndex(tableId);
					let rowData = props.cardTable.getDataByIndex(tableId, index);
					let pk_unit_used_after = '';
					if (rowData && rowData.values) {
						pk_unit_used_after = rowData.values['pk_unit_used_after'].value;
					}
					return {
						pk_org: pk_unit_used_after,
						busifuncode: IBusiRoleConst.ASSETORG,
						isTreelazyLoad: false
					};
				};
			} else if (item.attrcode == 'pk_warehouse_in') {
				//报废入库参照库存组织
				item.queryCondition = () => {
					// let rowData = props.cardTable.getClickRowIndex(tableId);
					let index = props.cardTable.getCurrentIndex(tableId);
					let rowData = props.cardTable.getDataByIndex(tableId, index);
					let pk_icorg = '';
					if (rowData && rowData.values) {
						pk_icorg = rowData.values['pk_icorg'].value;
					}
					return {
						pk_org: pk_icorg,
						GridRefActionExt: refFilter.wareHouseRefFilter
					};
				};
			} else {
				//自定义项的过滤
				defRefCondition.call(this, props, item, formId, pk_group);
			}
		});
		// 添加表格操作列
		let oprCol = createOprationColumn.call(this, props, { getInnerBtns, tableButtonClick });
		meta[tableId].items.push(oprCol);
	}

	// 卡片模板修改后公共处理
	meta = afterModifyCardMeta.call(this, props, meta);

	return meta;
}

/**
 * 操作列根据不同状态显示不同按钮
 * @param {*} props 
 * @param {*} text 
 * @param {*} record 
 * @param {*} index 
 */
function getInnerBtns(props, text, record, index) {
	let { pageConfig = {} } = props;
	let { formId } = pageConfig;
	let status = props.form.getFormStatus(formId) || UISTATE.browse;
	let billStatusVal = props.form.getFormItemsValue(formId, 'bill_status').value;
	let buttonAry = [];
	if (status == UISTATE.add || status == UISTATE.edit) {
		buttonAry = [ 'OpenCard', 'CopyLine', 'DelLine' ];
		if (billStatusVal == BILLSTATUS.un_check || billStatusVal == BILLSTATUS.check_going) {
			buttonAry = [ 'OpenCard' ];
		}
	} else {
		buttonAry = [ 'OpenCard' ];
	}
	return buttonAry;
}
