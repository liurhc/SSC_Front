import { loadDataByPk, setBtnsVisible } from './buttonClick';

/**
 * 分页查询
 * @param {*} props 
 * @param {*} pk 
 */
export default function(props, pk) {
	if (!pk || pk == 'undefined' || pk == 'null') {
		return;
	}
	// 更新参数
	props.setUrlParam({ id: pk });
	let callback = () => {
		setBtnsVisible.call(this, props);
	};
	loadDataByPk.call(this, props, pk, callback);
}
