// 页面配置
export const pageConfig = {
	// 表单区域id
	formId: 'card_head',
	// 表格区域id
	tableId: 'bodyvos',
	// 主键字段
	pkField: 'pk_reduce',
	// 打印模板节点标识
	printNodekey: null,
	listRouter: '/list',
	transferListRouter: '/transfer'
};
