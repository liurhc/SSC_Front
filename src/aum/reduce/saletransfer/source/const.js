export const pageConfig = {
	// 主子结构页面编码
	pageCode: '452004504A_transfer',
	appCode: '452004504A',
	//表头列表code
	headCode: 'head',
	//表体列表code
	bodyCode: 'bodyvos',
	//主子拉平页面编码
	mainPageCode: '452004504A_main',
	//主子拉平显示面板code
	mainCode: 'transfermain',
	//查询区域code
	searchCode: 'search',
	//主表主键字段
	headPkField: 'pk_reduce',
	//子表主键字段
	bodyPkField: 'pk_reduce_b',
	// 单据类型
	billType: '4A18',
	// 交易类型
	transType: '4A18-01',
	//缓存命名空间
	dataSourceTransfer: 'aum.sale.saletransfer.source',
	//下游卡片路由
	cardRouter: '/card',
	// 上游卡片页面路由，超链接用
	sourceUrl: '/aum/reduce/sale/main/#/card',
	// 上游卡片页面的pagecode
	sourcePageCode: '452004504A_card',
	srccompensation_query: '/nccloud/aum/sale/srcsale_query.do',
	payAbleNm: '452004004A-000007' /* 国际化处理：生成应付单*/,
	receivAbleNm: '452004004A-000008' /* 国际化处理：生成应收单*/
};
