import React from 'react';
import MsgInfo from './MsgInfo';

export function createMsgInfo(props, config = {}) {
	return (
		<MsgInfo
			getThis={(_this) => {
				this.myMsgInfo = _this;
			}}
			rootprops={props}
			config={config}
		/>
	);
}
