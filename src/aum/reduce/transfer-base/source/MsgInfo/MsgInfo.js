import React, { Component } from 'react';
import ampub from 'ampub';

const { utils } = ampub;

const { multiLangUtils } = utils;

const { getMultiLangByID } = multiLangUtils;

export default class MsgInfo extends Component {
	constructor(props) {
		super(props);
		//传出this
		this.props.getThis(this);
		this.state = {
			totalNum: 0
		};
	}

	render() {
		let { totalNum } = this.state;
		let str = getMultiLangByID('452004004A-000006', { num: totalNum }) /*国际化处理  已选 : x 条 svg */;

		return (
			<div>
				<p>{str}</p>
			</div>
		);
	}
}
