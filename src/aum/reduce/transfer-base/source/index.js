import React, { Component } from 'react';
import { base, createPageIcon } from 'nc-lightapp-front';
import ampub from 'ampub';

const { utils } = ampub;

const { multiLangUtils } = utils;

const { getMultiLangByID } = multiLangUtils;

import { searchBtnClick, initTemplate, buttonClick, rowSelected } from './events';
import { createMsgInfo } from './MsgInfo';

import './index.less';

const { NCAffix } = base;

/**
 * 主子表列表页面入口文件
 */
class MasterChildListBase extends Component {
	constructor(props) {
		super(props);
		// this.len = 0; //页尾数据
		initTemplate.call(this, props);
	}

	render() {
		const { table, search, button, ncmodal, pageConfig = {} } = this.props;
		const { createModal } = ncmodal;
		const { createButtonApp } = button;
		const { NCCreateSearch } = search;
		const { createSimpleTable } = table;
		const { title, searchAreaId, tableId, pagecode, pkField, dataSource } = pageConfig;

		return (
			<div className="nc-bill-list transfer-base-area">
				<NCAffix>
					{/* 头部 header */}
					<div className="nc-bill-header-area">
						{/* 标题 title */}
						<div className="header-title-search-area">
							{createPageIcon()}
							<h2 className="title-search-detail">{getMultiLangByID(title)}</h2>
						</div>
						{/* 按钮区 btn-area */}
						<div className="header-button-area">
							{/* {createButtonApp({
								area: 'list_head',
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})} */}
						</div>
					</div>
				</NCAffix>
				{/* 查询区 search-area */}
				<div className="nc-bill-search-area">
					{NCCreateSearch(searchAreaId, {
						clickSearchBtn: searchBtnClick.bind(this),
						dataSource: dataSource,
						componentInitFinished: () => {
							//缓存数据赋值成功的钩子函数
							//若初始化数据后需要对数据做修改，可以在这里处理
						}
					})}
				</div>
				{/* 列表区 table-area */}
				<div className="nc-bill-table-area">
					{createSimpleTable(tableId, {
						showCheck: true,
						showIndex: true,
						dataSource: dataSource,
						onSelected: rowSelected.bind(this),
						onSelectedAll: rowSelected.bind(this),
						pkname: pkField,
						// selectedChange: (props, moduleId, newVal) => {
						// 	setMsgInfoData.call(this, { newVal });
						// },
						componentInitFinished: () => {
							//缓存数据赋值成功的钩子函数
							//若初始化数据后需要对数据做修改，可以在这里处理

							// 根据是否有选中行确认按钮的可用性
							rowSelected.call(this, this.props);
						}
					})}
				</div>
				<div className="msg-info">
					{createMsgInfo.call(this, this.props)}
					{createButtonApp({
						area: 'list_head',
						buttonLimit: 3,
						onButtonClick: buttonClick.bind(this),
						popContainer: document.querySelector('.header-button-area')
					})}
				</div>
			</div>
		);
	}
}

export default MasterChildListBase;
