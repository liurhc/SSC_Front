import { ajax } from 'nc-lightapp-front';
import ampub from 'ampub';

const { components, utils } = ampub;

const { LoginContext } = components;

const { getContext, loginContextKeys } = LoginContext;

const { listUtils } = utils;

const { setListValue, setQueryInfoCache, listConst } = listUtils;

import { setMsgInfoData } from '../MsgInfo';

/**
 * 点击查询，获取查询区数据
 * @param {*} props 
 * @param {*} searchVal 
 */
export default function(props, searchVal, type, queryInfo, isRefresh) {
	if (!searchVal) {
		return;
	}
	let { pageConfig = {} } = props;
	let { searchAreaId, tableId, pagecode, bill_type } = pageConfig;
	// cacheTools.set(`${pagecode}_searchParams`, searchVal);

	const transferTransi_type = getContext(loginContextKeys.transtype);

	queryInfo = props.search.getQueryInfo(searchAreaId);
	// 页面信息
	queryInfo.pagecode = pagecode;
	queryInfo.billtype = bill_type;
	queryInfo.userdefObj = {
		transi_type: transferTransi_type
	};

	//缓存查询条件
	setQueryInfoCache.call(this, queryInfo, isRefresh, type);

	// 条件查询
	ajax({
		url: pageConfig.url.queryUrl,
		data: queryInfo,
		success: (res) => {
			setListValue.call(
				this,
				props,
				res,
				tableId,
				isRefresh ? listConst.SETVALUETYPE.refresh : listConst.SETVALUETYPE.query
			);
			setMsgInfoData.call(this, { newVal: 0 });
		}
	});
}
