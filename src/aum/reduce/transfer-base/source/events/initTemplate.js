import { ajax } from 'nc-lightapp-front';
import ampub from 'ampub';

const { components } = ampub;

const { LoginContext, queryAboutUtils, RefFilterForSearchArea } = components;

const { loginContext, getContext, loginContextKeys } = LoginContext;
const { openEquipCardByPk } = queryAboutUtils;

const { refCommonFilterForSearchArea, appPermissionOrgRefFilter, commonRefFilter } = RefFilterForSearchArea;

/**
 * 模板初始化
 * @param {*} props 
 */
export default function(props) {
	let { pageConfig = {} } = props;
	let { pagecode } = pageConfig;
	props.createUIDom(
		{
			// 页面编码
			pagecode
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta, () => {
						afterInit.call(this, props);
					});
				}
			}
		}
	);
}

/**
 * 模板初始化以后
 * @param {*} props 
 */
function afterInit(props) {
	// let { pageConfig = {} } = props;
	// let { searchAreaId, pagecode } = pageConfig;
	// let searchVal = cacheTools.get(`${pagecode}_searchParams`);
	// if (searchVal) {
	// 	props.search.setSearchValue(searchAreaId, searchVal.conditions);
	// 	searchBtnClick.call(this, props, searchVal);
	// }

	// 设置默认查询条件;
	let { pageConfig = {} } = props;
	let { searchAreaId, url, bill_type } = pageConfig;
	let defaultStatusUrl = url.queryDefaultStatusUrl;
	const transferTransi_type = getContext(loginContextKeys.transtype);
	let queryInfo = {};
	queryInfo.billtype = bill_type;
	queryInfo.userdefObj = {
		transi_type: transferTransi_type
	};
	if (defaultStatusUrl) {
		ajax({
			url: defaultStatusUrl,
			data: queryInfo,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					let pks = [];
					let displays = [];
					data.map((item) => {
						pks.push(item.pk_status);
						displays.push(item.status_name);
					});
					let statusPk = pks.join(',');
					let statusDisplay = displays.join(',');
					let statusvalue = props.search.getSearchValByField(searchAreaId, 'pk_used_status');
					//判断当前查询区资产状态是否有值
					if (!statusvalue.value.firstvalue) {
						props.search.setSearchValByField(searchAreaId, 'pk_used_status', {
							value: statusPk,
							display: statusDisplay
						});
					}
				}
			}
		});
	}
}

/**
 * 修改模板
 * @param {*} props 
 * @param {*} meta 
 */
function modifierMeta(props, meta) {
	const transferTransi_type = getContext(loginContextKeys.transtype);
	let { pageConfig = {} } = props;
	let { searchAreaId, bill_type, statusFilter, tableId, browseBtns } = pageConfig;
	props.button.setButtonDisabled(`${browseBtns[0]}`, true);
	// 参照查询条件过滤
	meta[searchAreaId].items.map((item, key) => {
		refCommonFilterForSearchArea.call(this, props, item, searchAreaId);
		// 组织权限过滤
		if (
			item.attrcode.endsWith('pk_org') ||
			item.attrcode.endsWith('pk_usedorg') ||
			item.attrcode.endsWith('pk_ownerorg')
		) {
			appPermissionOrgRefFilter.call(this, item);
		}
		if (item.attrcode == 'pk_mandept' || item.attrcode == 'pk_manager') {
			commonRefFilter.call(this, props, item, searchAreaId, [ 'pk_mandept', 'pk_manager' ], 'pk_ownerorg');
		} else if (item.attrcode == 'pk_usedept' || item.attrcode == 'pk_user') {
			commonRefFilter.call(this, props, item, searchAreaId, [ 'pk_usedept', 'pk_user' ], 'pk_usedunit');
		} else if (item.attrcode == 'pk_jobmngfil') {
			commonRefFilter.call(this, props, item, searchAreaId, 'pk_jobmngfil', 'pk_org');
		} else if (item.attrcode == 'pk_used_status') {
			item.queryCondition = () => {
				return {
					bill_type: bill_type,
					transi_type: transferTransi_type,
					GridRefActionExt: statusFilter
				};
			};
		}
	});

	//修改列渲染样式
	meta[tableId].items.map((item, key) => {
		if (item.attrcode == 'equip_code') {
			//给资产编码列添加超链接
			item.render = (text, record, index) => {
				return (
					<div class="simple-table-td" field="equip_code" fieldname="equip_code">
						<span
							className="code-detail-link"
							onClick={() => {
								openEquipCardByPk.call(this, props, record.pk_equip.value);
							}}
						>
							{record && record.equip_code && record.equip_code.value}
						</span>
					</div>
				);
			};
		}
	});

	return meta;
}
