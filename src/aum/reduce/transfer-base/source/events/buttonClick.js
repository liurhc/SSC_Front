import ampub from 'ampub';

const { components, utils, commonConst } = ampub;

const { LoginContext } = components;

const { setContext, getContext } = LoginContext;

const { listUtils } = utils;

const { listConst } = listUtils;

const { CommonKeys } = commonConst;
const { TransferConst } = CommonKeys;

import { setMsgInfoData } from '../MsgInfo';

/**
 * 按钮点击
 * @param {*} props 
 * @param {*} id 
 */
export default function(props, id) {
	let { pageConfig = {} } = props;
	let { browseBtns } = pageConfig;
	switch (id) {
		case browseBtns[0]:
			pushBill.call(this, props);
			break;
		default:
			break;
	}
}

/**
 * 推单
 * @param {*} props 
 */
export function pushBill(props) {
	let { pageConfig = {} } = props;
	let { tableId } = pageConfig;
	let checkedRows = props.table.getCheckedRows(tableId);
	linkToCard.call(this, props, checkedRows);
}

/**
 * 跳转卡片
 * @param {*} props 
 * @param {*} checkedRows 
 */
export function linkToCard(props, checkedRows) {
	let { pageConfig = {} } = props;
	let {
		browseBtns,
		transferToAppcode,
		transferToCardRouter,
		transferToPagecode,
		dataSource,
		searchAreaId
	} = pageConfig;
	let pks = [];
	let queryInfo = getContext(listConst.QUERYINFOKEY.queryInfo, listConst.QUERYINFOKEY.dataSource);
	let pk_org = props.search.getSearchValByField(searchAreaId, 'pk_org').value.firstvalue;
	if (queryInfo) {
		// 若有缓存，取缓存的数据，防止从卡片返回到待处理界面选择组织未点击查询生成错误数据
		let conditions = queryInfo.querycondition.conditions;
		conditions.map((item) => {
			if (item.field == 'pk_org') {
				pk_org = item.value.firstvalue;
			}
		});
	}
	checkedRows.map((item) => {
		let values = item.data.values;
		let pk = values.pk_equip.value;
		let ts = values.ts.value;
		pks.push({ head: { pk, ts } });
	});
	let transferInfo = {
		pk_org,
		pks
	};
	setContext(`${browseBtns[0]}`, transferInfo, dataSource);
	// cacheTools.set(`${browseBtns[0]}`, pks);
	props.pushTo(transferToCardRouter, {
		appcode: transferToAppcode,
		pagecode: transferToPagecode,
		status: 'add',
		type: TransferConst.type
	});
}

/**
 * 行选中事件
 * @param {*} props 
 */
export function rowSelected(props) {
	let { pageConfig = {} } = props;
	let { tableId, browseBtns } = pageConfig;
	let checkedRows = props.table.getCheckedRows(tableId);
	if (checkedRows && checkedRows.length > 0) {
		// 刷新下方区域合计信息
		setMsgInfoData.call(this, { newVal: checkedRows.length });
		props.button.setButtonDisabled([ `${browseBtns[0]}` ], false);
	} else {
		setMsgInfoData.call(this, { newVal: 0 });
		props.button.setButtonDisabled(`${browseBtns[0]}`, true);
	}
}
