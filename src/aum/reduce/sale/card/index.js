import React, { Component } from 'react';
import { createPage } from 'nc-lightapp-front';
import MasterChildCardBase, { baseConfig } from '../../reduce-base/card-base';

// 页面配置
const pageConfig = {
	...baseConfig,
	// 应用主键
	appid: '0001Z9100000000012LU',
	// 应用编码
	appcode: '452004504A',
	// 应用名称(国际化处理 资产处置)
	title: '452004004A-000001',
	// 页面编码
	pagecode: '452004504A_card',
	// 编辑态按钮
	editBtns: [ 'Save', 'SaveCommit', 'Cancel', 'GetFinanceData', 'OpenCard', 'AddLine', 'DelLine', 'BatchAlter' ],
	// 浏览态按钮
	browseBtns: [
		'Add',
		'Edit',
		'Delete',
		'Commit',
		'UnCommit',
		'Attachment',
		'OpenCard',
		'QueryAboutVoucher',
		'QueryAboutBusiness',
		'QueryAboutBillFlow',
		'Print',
		'Output',
		'Refresh'
	],
	// 单据类型
	bill_type: '4A18',
	// 请求链接
	url: {
		transferEquipUrl: '/nccloud/aum/sale/transferEquip.do',
		queryCardUrl: '/nccloud/aum/sale/queryCard.do',
		insertUrl: '/nccloud/aum/sale/insert.do',
		updateUrl: '/nccloud/aum/sale/update.do',
		editUrl: '/nccloud/aum/sale/edit.do',
		printUrl: '/nccloud/aum/sale/printCard.do',
		commitUrl: '/nccloud/aum/sale/commit.do',
		unCommitUrl: '/nccloud/aum/sale/unCommit.do',
		deleteUrl: '/nccloud/aum/sale/commit.do',
		headAfterEditUrl: '/nccloud/aum/sale/headAfterEdit.do',
		bodyAfterEditUrl: '/nccloud/aum/sale/bodyAfterEdit.do',
		batchAlterUrl: '/nccloud/aum/sale/batchAlter.do',
		getFianceDataUrl: '/nccloud/aum/sale/getFianceData.do'
	},
	refFilter: {
		// 组织权限过滤处理类
		orgRefFilter: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgVidRefFilter',
		// 资产过滤处理类
		equipRefFilter: 'nccloud.web.aum.reduce.sale.refcondition.PK_EQUIPSqlBuilder'
	},
	// 输出文件名称(国际化处理 资产处置)
	printFilename: '452004004A-000001',
	// 权限资源编码
	resourceCode: '4520032005',
	dataSource: 'aum.reduce.sale.main',
	transferDataSource: 'aum.reduce.sale.transfer',
	transferFromAppcode: '452004508A',
	// 节点标记
	nodeFlag: 'sale',
	// 审批中表格可编辑字段
	editableFieldsInApprove: [ 'purgefee', 'purgerevenue' ],
	// 编辑后事件字段
	editAfterEventFields: [ 'pk_equip', 'purgerevenue', 'purgefee' ],
	// 组织切换清空字段
	clearFields: [ 'pk_recorder', 'pk_fiorg', 'pk_fiorg_v', 'pk_raorg', 'pk_raorg_v' ]
};

const MasterChildCard = createPage({
	billinfo: {
		billtype: 'card',
		pagecode: pageConfig.pagecode,
		headcode: pageConfig.formId,
		bodycode: pageConfig.tableId
	}
})(MasterChildCardBase);

export default class Card extends Component {
	render() {
		return <MasterChildCard pageConfig={pageConfig} />;
	}
}
