import React, { Component } from 'react';
import { createPage } from 'nc-lightapp-front';
import MasterChildListBase, { baseConfig } from '../../reduce-base/list-base';

// 页面配置
const pageConfig = {
	...baseConfig,
	// 应用主键
	appid: '0001Z9100000000012LU',
	// 应用编码
	appcode: '452004504A',
	// 应用名称(国际化处理 资产处置)
	title: '452004004A-000001',
	// 页面编码
	pagecode: '452004504A_list',
	// 单据类型
	bill_type: '4A18',
	// 请求链接
	url: {
		queryUrl: '/nccloud/aum/sale/query.do',
		queryPageUrl: '/nccloud/aum/sale/querypagegridbypks.do',
		editUrl: '/nccloud/aum/sale/edit.do',
		printUrl: '/nccloud/aum/sale/printCard.do',
		commitUrl: '/nccloud/aum/sale/commit.do',
		unCommitUrl: '/nccloud/aum/sale/unCommit.do',
		deleteUrl: '/nccloud/aum/sale/commit.do'
	},
	refFilter: {
		// 组织权限过滤处理类
		orgRefFilter: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter'
	},
	// 输出文件名称(国际化处理 资产处置)
	printFilename: '452004004A-000001',
	// 节点标记
	nodeFlag: 'sale',
	// 权限资源编码
	resourceCode: '4520032005',
	dataSource: 'aum.reduce.sale.main'
};

const MasterChildList = createPage({})(MasterChildListBase);

export default class List extends Component {
	render() {
		return <MasterChildList pageConfig={pageConfig} />;
	}
}
