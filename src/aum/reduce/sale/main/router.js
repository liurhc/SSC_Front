import { asyncComponent } from 'nc-lightapp-front';

const List = asyncComponent(() =>
	import(/* webpackChunkName: "aum/reduce/sale/list/list" */ /* webpackMode: "eager" */ '../list')
);
const Card = asyncComponent(() =>
	import(/* webpackChunkName: "aum/reduce/sale/card/card" */ /* webpackMode: "eager" */ '../card')
);
const Transfer = asyncComponent(() =>
	import(/* webpackChunkName: "aum/reduce/sale/transfer/transfer" */ /* webpackMode: "eager" */ '../transfer')
);

const routes = [
	{
		path: '/',
		component: List,
		exact: true
	},
	{
		path: '/list',
		component: List
	},
	{
		path: '/card',
		component: Card
	},
	{
		path: '/transfer',
		component: Transfer
	}
];

export default routes;
