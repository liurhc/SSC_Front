import React, { Component } from 'react';
import { createPage } from 'nc-lightapp-front';
import MasterChildListBase from '../../transfer-base/source';

// 页面配置
const pageConfig = {
	// 应用主键
	appid: '0001Z9100000000012LV',
	// 应用编码
	appcode: '452004508A',
	// 应用名称(国际化处理 待处置资产处理')
	title: '452004004A-000004',
	// 查询区域id
	searchAreaId: 'searchArea',
	// 表格区域id
	tableId: 'list_head',
	// 页面编码
	pagecode: '452004508A_list',
	// 主键字段
	pkField: 'pk_equip',
	// 下游单据类型
	bill_type: '4A18',
	// 浏览态按钮
	browseBtns: [ '4A18+4A18-01' ],
	// 请求链接
	url: {
		queryUrl: '/nccloud/aum/saletransfer/queryList.do',
		queryDefaultStatusUrl: '/nccloud/aum/saletransfer/queryStatus.do'
	},
	transferToAppcode: '452004504A',
	transferToCardRouter: '/card',
	transferToPagecode: '452004504A_card',
	dataSource: 'aum.reduce.sale.transfer',
	statusFilter: 'nccloud.web.aum.reduce.saletransfer.refcondition.PK_STATUSSqlBuilder'
};

const MasterChildList = createPage({})(MasterChildListBase);

export default class List extends Component {
	render() {
		return <MasterChildList pageConfig={pageConfig} />;
	}
}
