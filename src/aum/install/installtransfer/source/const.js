export const pageConfig = {
	// 主子结构页面编码
	pageCode: '452002004A_transfer',
	appCode: '452002004A',
	//表头列表code
	headCode: 'head',
	//表体列表code
	bodyCode: 'bodyvos',
	//主子拉平页面编码
	mainPageCode: '452002004A_main',
	//主子拉平显示面板code
	mainCode: 'transfermain',
	//查询区域code
	searchCode: 'search',
	//主表主键字段
	headPkField: 'pk_bill',
	//子表主键字段
	bodyPkField: 'pk_bill_b',
	// 单据类型
	billType: '4A24',
	// 交易类型
	transType: '4A24-01',
	//缓存命名空间
	dataSourceTransfer: 'aum.install.installtransfer.source',
	//下游卡片路由
	cardRouter: '/card',
	// 上游卡片页面路由，超链接用
	sourceUrl: '/aum/install/install/main/#/card',
	// 上游卡片页面的pagecode
	sourcePageCode: '452002004A_card',
	srccompensation_query: '/nccloud/aum/install/srcinstall_query.do'
};
