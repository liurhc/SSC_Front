import { pageConfig } from '../const';

import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { multiLangUtils, transferUtils } = utils;
const { assetOrgMultiRefFilter, refInit, LoginContext } = components;
const { AssetOrgMultiRefFilter } = assetOrgMultiRefFilter;
const { defRefCondition, commonRefCondition } = refInit;
const { CommonKeys } = commonConst;
const { IBusiRoleConst, TransferConst } = CommonKeys;
const { loginContext, getContext, loginContextKeys } = LoginContext;
const { getMultiLangByID } = multiLangUtils;
const { addLinkToTemplate, commonMetaHandle } = transferUtils;

const { pageCode, appCode, searchCode, headCode, bodyCode, sourceUrl, sourcePageCode, headPkField } = pageConfig;

export default function(props) {
	let query_appcode = appCode;
	let appcodeCache = getContext(TransferConst.query_appcode, TransferConst.dataSource);
	if (appcodeCache) {
		query_appcode = appcodeCache;
	}
	props.createUIDom(
		{
			pagecode: pageCode, //页面id
			appcode: query_appcode //注册按钮的id
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta, query_appcode);
					props.meta.setMeta(meta);
				}
			}
		}
	);
}
function modifierMeta(props, meta, sourceAppCode) {
	meta = refCondtion(props, meta);
	meta = addLinkToTemplate(props, meta, headCode, bodyCode, sourceUrl, sourcePageCode, sourceAppCode, headPkField);
	// 模板公共处理调用
	commonMetaHandle.call(this, props, meta, searchCode);
	return meta;
}
//查询中参照字段的过滤条件添加
function refCondtion(props, meta) {
	let pk_group = getContext(loginContextKeys.groupId);
	meta[searchCode].items.map((item) => {
		// 资产组织过滤
		if (item.attrcode == 'pk_org') {
			const src_appcode = props.getUrlParam('src_appcode');
			const src_tradetype = props.getUrlParam('src_tradetype');
			item.queryCondition = () => {
				return {
					src_appcode,
					src_tradetype,
					GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter'
				};
			};
		} else if (item.attrcode == 'bodyvos.pk_equip.pk_usedept' || item.attrcode == 'bodyvos.pk_equip.pk_user') {
			AssetOrgMultiRefFilter.call(this, props, searchCode, item, 'bodyvos.pk_equip.pk_usedunit');
			//（使用部门）
			item.queryCondition = () => {
				let pk_unit_used = getSearchValue.call(this, props, 'bodyvos.pk_equip.pk_usedunit'); // 使用权
				let filter = { isShowUnit: true, busifuncode: IBusiRoleConst.ASSETORG };
				if (pk_unit_used) {
					filter['pk_org'] = pk_unit_used;
				}
				return filter; // 根据pk_org过滤
			};
		} else if (item.attrcode == 'bodyvos.pk_equip.pk_mandept' || item.attrcode == 'bodyvos.pk_equip.pk_manager') {
			AssetOrgMultiRefFilter.call(this, props, searchCode, item, 'bodyvos.pk_equip.pk_ownerorg');
			//（管理部门）
			item.queryCondition = () => {
				let pk_unit_used = getSearchValue.call(this, props, 'bodyvos.pk_equip.pk_ownerorg'); // 管理组织
				let filter = { isShowUnit: true, busifuncode: IBusiRoleConst.ASSETORG };
				if (pk_unit_used) {
					filter['pk_org'] = pk_unit_used;
				}
				return filter; // 根据pk_org过滤
			};
		} else {
			let bodyDefPrefix = 'bodyvos.def';
			defRefCondition.call(this, props, item, searchCode, pk_group, true, 'pk_org', bodyDefPrefix);
			defRefCondition.call(this, props, item, searchCode, pk_group, true);
		}
	});
	return meta;
}
//获取查询条件的值
function getSearchValue(props, field) {
	let data = props.search.getSearchValByField(searchCode, field);
	let value = '';
	if (data && data.value) {
		value = data.value.firstvalue;
	}
	if (value && value.split(',').length == 1) {
		return value;
	} else {
		return '';
	}
}
