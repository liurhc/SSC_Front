import { pageConfig } from '../const';

import ampub from 'ampub';
const { utils } = ampub;
const { transferUtils } = utils;
const { doTansferSearch } = transferUtils;

const {
	headCode,
	bodyCode,
	searchCode,
	headPkField,
	bodyPkField,
	srccompensation_query,
	billType,
	pageCode,
	appCode
} = pageConfig;

//点击查询，获取查询区数据
export default function clickSearchBtn(props, searchVal) {
	if (!searchVal) {
		return;
	}
	doTansferSearch(
		srccompensation_query,
		props,
		searchCode,
		billType,
		pageCode,
		headCode,
		bodyCode,
		headPkField,
		bodyPkField,
		appCode
	);
}
