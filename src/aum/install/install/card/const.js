// 页面配置
export const pageConfig = {
	title: '452002004A-000000' /* 国际化处理： 安装调试*/,
	printFilename: '452002004A-000000' /* 国际化处理： 安装调试*/,
	tableId: 'list_head',
	pageId: '452002004A_list',
	appid: '0001Z910000000000LO3', //小应用id
	appcode: '452002004A', //小应用code
	searchId: 'searchArea', //查询区
	oid: '1001Z91000000000HJ6B', //查询模板id
	bill_type: '4A24',
	transi_type: '4A24-01',
	// form节点编码
	form_tableId: 'bodyvos',
	form_pageId: '452002004A_list',
	card_pageId: '452002004A_card',
	formId: 'card_head',
	//url
	list_url: '/aum/install/install/list/index.html', //列表url
	card_url: '/aum/install/install/card/index.html', //卡片url
	printListUrl: '/nccloud/aum/install/printCard.do', //列表打印url
	printCardUrl: '/nccloud/aum/install/printCard.do', //卡片打印url

	EditOpr_url: '/nccloud/aum/install/install_edit.do',
	BatchAlter_url: '/nccloud/aum/install/batchAlter.do', //

	node_code: '4520020005',
	printFuncode: '452002004A',
	printNodekey: null,

	//操作 action url
	list_delete: '/nccloud/aum/install/delete.do',
	list_query: '/nccloud/aum/install/query.do',
	list_querybypks: '/nccloud/aum/install/querypagegridbypks.do',
	card_query: '/nccloud/aum/install/querycard.do',
	card_addsave: '/nccloud/aum/install/save.do',
	card_updatesave: '/nccloud/aum/install/update.do',
	card_approve: '/nccloud/aum/install/approve.do',
	card_commit: '/nccloud/aum/install/commit.do',
	card_apportion: '/nccloud/aum/install/apportion.do', //分摊
	bodyAfterEditUrl: '/nccloud/aum/install/bodyAfterEdit.do', //表体编辑后事件
	headAfterEditUrl: '/nccloud/aum/install/headAfterEdit.do', //表头编辑后事件

	dataSource: 'aum.install.install.main',
	cardRouter: '/card',

	//精度处理
	precision_url: '/nccloud/aum/install/precision.do',
	//资产组织切换事件处理
	orgchangeevent_url: '/nccloud/aum/install/orgchangeevent.do',
	card_billCodeGenBack: '/nccloud/ampub/common/billCodeGenBack.do',
	//操作 action url
	// 编辑态按钮
	editBtns: [ 'save', 'cancel', 'add', 'delete' ],
	// 浏览态按钮
	browseBtns: [ 'add', 'edit', 'delete' ],
	// 所有按钮颜色
	btnColors: {
		add: 'main-button',
		edit: 'main-button',
		delete: 'secondary-button',
		save: 'main-button',
		cancel: 'secondary-button'
	}
};
