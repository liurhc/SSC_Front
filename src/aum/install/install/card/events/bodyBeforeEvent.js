import { toast } from 'nc-lightapp-front';

export default function bodyBeforeEvent(props, moduleId, key, value, index, record) {
	let editFlag = true;
	refCondtion.call(this, props, moduleId, key, value, index, record);
	editFlag = isEdit.call(this, props, moduleId, key, value, index, record);
	return editFlag;
}
//设备有值，物料编码、设备名称、设备类别、规格、型号、数量不允许编辑
function isEdit(props, moduleId, key, value, index, record) {
	if (key == 'pk_equip') {
		let pk_srcdist_b = props.cardTable.getValByKeyAndIndex(moduleId, index, 'pk_srcdist_b').value; // 调出组织
		if (pk_srcdist_b) {
			return false;
		}
	} else if (key == 'pk_srcdist_b') {
		let pk_equip = props.cardTable.getValByKeyAndIndex(moduleId, index, 'pk_equip').value;
		if (pk_equip) {
			return false;
		}
	}
	return true;
}
//表体字段参照处理
function refCondtion(props, moduleId, key, value, index, record) {
	let meta = props.meta.getMeta();
	props.meta.setMeta(meta);
}
