import { ajax, toast, print, cardCache, output } from 'nc-lightapp-front';
import { pageConfig } from '../const';
const {
	EditOpr_url,
	node_code,
	appcode,
	formId,
	printNodekey,
	printFilename,
	printCardUrl,
	card_pageId,
	BatchAlter_url,
	dataSource,
	bill_type,
	card_query,
	card_addsave,
	card_updatesave,
	list_delete,
	card_commit,
	card_apportion
} = pageConfig;
import { headAfterEvent, bodyAfterEvent } from './afterEvent';

import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { ScriptReturnUtils, LoginContext, queryAboutUtils, queryVocherUtils, saveValidatorsUtil } = components;
const { getScriptCardReturnData } = ScriptReturnUtils;
const { openEquipCard, openBillTrack } = queryAboutUtils;
const { queryAboutVoucher } = queryVocherUtils;
const { beforeSaveValidator } = saveValidatorsUtil;
const { multiLangUtils, cardUtils, msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { setCardValue, setHeadAreaData } = cardUtils;
const { getContext, loginContextKeys } = LoginContext;
const { StatusUtils } = commonConst;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { UISTATE } = StatusUtils;

export default function(props, id) {
	let _this = this;
	let pk = props.form.getFormItemsValue(this.formId, 'pk_bill').value;
	switch (id) {
		case 'Add':
			this.old_pk = pk;
			props.form.EmptyAllFormValue(this.formId);
			props.cardTable.setTableData(this.tableId, { rows: [] });
			init.call(this, props);
			// props.button.setButtonVisible([ 'AddLine' ], false);
			break;
		case 'Edit':
			ajax({
				url: EditOpr_url,
				data: {
					pk: pk,
					resourceCode: node_code
				},
				success: (res) => {
					if (res.data) {
						toast({ color: 'warning', content: res.data });
					} else {
						props.cardTable.closeExpandedRow(this.tableId);
						toggleShow.call(this, 'edit');
					}
				},
				error: (res) => {
					if (res && res.message) {
						toast({ color: 'danger', content: res.message });
					}
				}
			});
			break;
		case 'Delete':
			deleteConfirm.call(this, props);
			break;
		case 'Save':
			saveClick.call(this);
			break;
		case 'Commit':
			commitClick.call(this, 'SAVE', 'commit');
			break;
		case 'SaveCommit':
			commitClick.call(this, 'SAVE', 'saveCommit');
			break;
		case 'UnCommit':
			commitClick.call(this, 'UNSAVE', '');
			break;
		case 'Apportion':
			apportionClick.call(this); //分摊
			break;
		case 'QueryAboutBusiness':
			openBillTrack(_this);
			break;
		case 'QueryAboutBillFlow':
			_this.setState({
				showApprove: true,
				transi_type: props.form.getFormItemsValue(formId, 'transi_type').value
			});
			break;
		case 'QueryAboutCard':
			openEquipCard(_this, props, this.tableId);
			break;
		case 'QueryAboutVoucher':
			queryAboutVoucher.call(this, props, formId, 'pk_bill', appcode); // tableId为区域编码，第三个参数为主键编码，appcode为应用编码
			break;
		case 'Cancel':
			cancelConfirm.call(this, props, pk);
			break;
		case 'AddLine':
			props.cardTable.addRow(this.tableId);
			setApportionBtnsEnable(props, this.tableId);
			break;
		case 'Refresh':
			let pk_bill = props.form.getFormItemsValue(formId, 'pk_bill').value;
			getdata.call(this, pk_bill, true);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'DelLine':
			delLineClick.call(this, props);
			break;
		case 'BatchAlter':
			BatchAlter.call(this, props, this.tableId);
			break;
		case 'Attachment':
			attachmentClick.call(this, props);
			break;
		default:
			break;
	}
}
/**
 * 删除弹框提示
 * @param {*} props 
 */
function deleteConfirm(props) {
	showConfirm.call(this, props, { type: MsgConst.Type.Delete, beSureBtnClick: delConfirm });
}

/**
 * 取消
 * @param {*} props 
 */
export function cancelConfirm(props, pk) {
	let cancelClick = () => {
		// 恢复字段的可编辑性
		let pk_org_v = props.form.getFormItemsValue(this.formId, 'pk_org_v');
		if (!pk_org_v || !pk_org_v.value) {
			props.resMetaAfterPkorgEdit();
		}
		// 加载编辑前数据，如果编辑前没有数据，则加载当前列表最后一条数据，如果还没有，显示为空不处理
		if (!pk) {
			pk = cardCache.getCurrentLastId(dataSource);
		}
		props.form.cancel(this.formId);
		props.cardTable.resetTableData(this.tableId);
		this.nowUIStatus = 'browse';
		if (this.old_pk && this.old_pk != '') {
			getdata.call(this, this.old_pk);
		} else {
			getdata.call(this, pk);
		}
	};
	showConfirm.call(this, props, {
		type: MsgConst.Type.Cancel,
		beSureBtnClick: cancelClick
	});
}

/**
 * 附件上传
 * @param {*} props 
 */
function attachmentClick(props) {
	let billId = props.form.getFormItemsValue(this.formId, 'pk_bill').value;
	props.ncUploader.show('warcontract-uploader', {
		billId: 'pam/bill/' + billId
	});
}

/**
 * 批改
 * @param {*} props 
 */
function BatchAlter(props, tableId) {
	// 获取原始数据
	let changeData = props.cardTable.getTableItemData(tableId);
	let cardData = props.createMasterChildDataSimple(card_pageId, formId, tableId);
	ajax({
		url: BatchAlter_url,
		data: {
			card: cardData,
			batchChangeIndex: changeData.batchChangeIndex, // 原始数据行号
			batchChangeKey: changeData.batchChangeKey, // 原始数据key
			batchChangeValue: changeData.batchChangeValue, // 原始数据value // 原始数据所在的vo，对应聚合VO中的tableCodes，一主一子可为空，一主多子要传对应的值
			tableCode: ''
		},
		async: false,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				// Promise.resolve(true).then(() => {
				setCardValue.call(this, props, data);
				// });
			}
		},
		error: (res) => {
			if (res && res.message) {
				toast({ color: 'danger', content: res.message });
			}
		}
	});
}

/**
 * 删行
 * @param {*} props 
 */
function delLineClick(props) {
	//选中的数据
	let checkedRows = props.cardTable.getCheckedRows(this.tableId);
	let checkedIndex = [];
	checkedRows.map((item) => {
		checkedIndex.push(item.index);
	});
	props.cardTable.delRowsByIndex(this.tableId, checkedIndex);
	bodyAfterEvent.call(this, props, this.tableId, 'money_org', { value: '0.00' }, [], 1);
	setBatchBtnsEnable.call(this, props, this.tableId);
	setApportionBtnsEnable.call(this, props, this.tableId);
}

/**
 * 打印
 * @param {*} props 
 */
export function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	let { pageConfig } = props;
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		/* 国际化处理： 请选择需要打印的数据*/
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		printCardUrl, // 后台打印服务url
		printData
	);
}

/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput }); /* 国际化处理： 请选择需要输出的数据*/
		return;
	}
	output({
		url: printCardUrl,
		data: printData
	});
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let pk = props.form.getFormItemsValue(this.formId, 'pk_bill');
	if (!pk || !pk.value) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		/* 国际化处理： 请选择需要打印的数据*/
		return;
	}

	let pks = [ pk.value ];
	let printData = {
		filename: getMultiLangByID(printFilename), // 文件名称
		nodekey: printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

/**
 * 设置批量按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBatchBtnsEnable(props, moduleId) {
	let checkedRows = props.cardTable.getCheckedRows(moduleId);
	let batchBtns = [ 'QueryAboutCard', 'DelLine' ];
	props.button.setButtonDisabled(batchBtns, !(checkedRows && checkedRows.length > 0));
}
/**
 * 设置分摊按钮、批改按钮的状态
 * @param {} props 
 * @param {*} moduleId 
 */
export function setApportionBtnsEnable(props, moduleId) {
	let rows = props.cardTable.getNumberOfRows(moduleId);
	props.button.setButtonDisabled([ 'Apportion' ], !(rows && rows > 0));
}
/**
 * 页面初始化数据
 */
export function init(props) {
	setDefaultValue.call(this);
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	props.form.setFormItemsDisabled(this.formId, { pk_org_v: false, pk_org: false });
	if (pk_org_v && pk_org_v.value && pk_org_v.value != null && pk_org_v.value != '') {
		headAfterEvent.call(this, props, formId, 'pk_org_v', pk_org_v, { value: '' }, pk_org_v.value);
	} else {
		props.initMetaByPkorg('pk_org_v');
		this.props.button.setButtonDisabled([ 'AddLine', 'DelLine', 'Apportion', 'BatchAlter' ], true);
	}
}

export function setDefaultValue(isNoOrg = false) {
	this.props.form.setFormItemsValue(formId, {
		bill_status: { value: '0', display: getMultiLangByID('statusUtils-000000') /* 国际化处理： 自由态*/ },
		bill_type: { value: bill_type, display: bill_type },
		transi_type: { value: getContext(loginContextKeys.transtype), display: getContext(loginContextKeys.transtype) },
		pk_transitype: { value: getContext(loginContextKeys.pk_transtype) },
		pk_group: { value: getContext(loginContextKeys.groupId), display: getContext(loginContextKeys.groupName) }
	});
	if (!isNoOrg) {
		this.props.form.setFormItemsValue(formId, {
			pk_org_v: {
				value: getContext(loginContextKeys.pk_org_v),
				display: getContext(loginContextKeys.org_v_Name)
			},
			pk_org: { value: getContext(loginContextKeys.pk_org), display: getContext(loginContextKeys.org_Name) }
		});
	}
	toggleShow.call(this, 'add');
}

//切换页面状态
export function toggleShow(status) {
	if (!status) {
		status = this.nowUIStatus;
	} else {
		this.nowUIStatus = status;
	}
	// 更新参数
	let pkVal = this.props.form.getFormItemsValue(formId, 'pk_bill');
	this.props.setUrlParam({ status, id: pkVal ? pkVal.value : '' });

	let flag = status === 'browse' ? false : true;
	let editButtons = [ 'Save', 'Cancel', 'AddLine', 'DelLine', 'BatchAlter', 'SaveCommit', 'Apportion', 'OpenCard' ];
	let borwseButtons = [
		'Edit',
		'Add',
		'Back',
		'Delete',
		'Commit',
		'UnCommit',
		'Print',
		'PrintG',
		'Preview',
		'Output',
		'OpenCard',
		'Attachment',
		'QueryAbout',
		'QueryAboutGroup',
		'QueryAboutCard',
		'QueryAboutBusiness',
		'QueryAboutBillFlow',
		'Refresh'
	];

	//按钮的显示状态
	if (status === 'edit' || status === 'add') {
		this.props.button.setButtonVisible(borwseButtons, false);
		this.props.button.setButtonVisible(editButtons, true);
		this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false);
		if (status === 'edit') {
			this.props.form.setFormItemsDisabled(formId, { pk_org_v: true });
		} else {
			//新增设置按钮的禁用状态
			this.props.button.setButtonDisabled([ 'Apportion', 'QueryAboutCard', 'DelLine' ], true);
		}
		setBatchBtnsEnable.call(this, this.props, this.tableId);
		setApportionBtnsEnable.call(this, this.props, this.tableId);
	} else {
		this.props.button.setButtonVisible(editButtons, false);
		this.props.button.setButtonVisible(borwseButtons, true);
		if (this.props.form.getFormItemsValue(this.formId, [ 'bill_status' ])[0] != undefined) {
			let bill_status = this.props.form.getFormItemsValue(this.formId, [ 'bill_status' ])[0].value;
			if (bill_status === '0') {
				//自由态
				this.props.button.setButtonVisible([ 'UnCommit' ], false); //不显示收回
			} else if (bill_status === '1') {
				//已提交
				this.props.button.setButtonVisible([ 'Commit', 'Delete', 'Edit' ], false); //不显示提交、修改、删除
			} else if (bill_status === '2') {
				//审批中
				this.props.button.setButtonVisible([ 'Commit', 'Delete', 'Edit', 'UnCommit' ], false); //不显示提交、修改、删除、收回
			} else if (bill_status === '3') {
				//审批通过
				this.props.button.setButtonVisible([ 'Commit', 'Delete', 'Edit' ], false); //不显示提交、修改、删除
			} else if (bill_status === '4') {
				//审批未通过
				this.props.button.setButtonVisible([ 'UnCommit', 'Commit' ], false); //不显示收回
			} else if (bill_status === '6') {
				//关闭
				this.props.button.setButtonVisible([ 'Commit', 'Delete', 'Edit', 'UnCommit' ], false); //不显示提交、修改、删除、收回
			} else {
				//新增时取消过来的
				this.props.button.setButtonVisible([ 'Commit', 'Delete', 'Edit', 'UnCommit' ], false); //不显示提交、修改、删除、收回
			}
			if (bill_status === '3') {
				//审批通过
				this.props.button.setButtonVisible([ 'QueryAboutVoucher' ], true);
			} else {
				this.props.button.setButtonVisible([ 'QueryAboutVoucher' ], false);
			}
		} else {
			this.props.button.setButtonVisible([ 'Commit', 'Delete', 'Edit', 'UnCommit' ], false); //不显示提交、修改、删除、收回
		}
		this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', true);
	}

	this.props.form.setFormStatus(this.formId, status);
	this.props.cardTable.setStatus(this.tableId, status == 'add' ? 'edit' : status);
	let bill_code = this.props.form.getFormItemsValue(this.formId, [ 'bill_code' ])[0].value;
	setHeadAreaData.call(this, this.props, { bill_code, status });
}

export function setEmptyData() {
	this.props.form.EmptyAllFormValue(this.formId);
	this.props.cardTable.setTableData(this.tableId, { rows: [] });
	toggleShow.call(this, 'browse');
	let borwseButtons = [
		'Edit',
		'Back',
		'Delete',
		'Commit',
		'UnCommit',
		'Print',
		'PrintG',
		'Preview',
		'Output',
		'OpenCard',
		'Attachment',
		'QueryAbout',
		'QueryAboutGroup',
		'QueryAboutCard',
		'QueryAboutBusiness',
		'QueryAboutVoucher',
		'QueryAboutBillFlow',
		'Refresh'
	];
	this.props.button.setButtonVisible(borwseButtons, false);
}

//通过单据id查询单据信息
export function getdata(pk, isRefresh) {
	if (pk == null || pk == '') {
		setEmptyData.call(this);
		return;
	}
	let cachData = cardCache.getCacheById(pk, dataSource);
	if (cachData && !isRefresh) {
		// setValue.call(this, cachData);
		setCardValue.call(this, this.props, cachData);
		toggleShow.call(this);
		this.setState({ pk_bill: pk });
	} else {
		let data = { pk, pagecode: card_pageId };
		ajax({
			url: card_query,
			data,
			success: (res) => {
				if (res.data) {
					setCardValue.call(this, this.props, res.data);
					toggleShow.call(this);
					this.setState({ pk_bill: pk });
					cardCache.updateCache('pk_bill', pk, res.data, formId, dataSource);
					if (isRefresh) {
						showMessage.call(this, this.props, { type: MsgConst.Type.RefreshSuccess });
						/* 国际化处理： 刷新成功*/
					}
				} else {
					this.props.form.cancel(this.formId);
					cardCache.deleteCacheById('pk_bill', pk, dataSource);
					setEmptyData.call(this);
					showMessage.call(this, this.props, { type: MsgConst.Type.DataDeleted });
					/* 国际化处理： 数据已删除*/
				}
			},
			error: (res) => {
				if (res && res.message) {
					toast({ content: res.message, color: 'danger' });
				}
			}
		});
	}
	this.props.setUrlParam({ id: pk });
}

export function setValue(data, status = 'browse') {
	if (this.nowUIStatus) {
		status = this.nowUIStatus;
	}
	toggleShow.call(this, status);
	if (data && data.head) {
		this.props.form.setAllFormValue({ [this.formId]: data.head[this.formId] });
		let bill_code = data.head[this.formId].rows[0].values.bill_code.value;
		this.setState({ bill_code });
	}
	if (data && data.body) {
		this.props.cardTable.setTableData(this.tableId, data.body[this.tableId]);
	}
}
//浮点型加法
function FloatAdd(arg1, arg2) {
	var r1, r2, m;
	try {
		r1 = arg1.toString().split('.')[1].length;
	} catch (e) {
		r1 = 0;
	}
	try {
		r2 = arg2.toString().split('.')[1].length;
	} catch (e) {
		r2 = 0;
	}
	m = Math.pow(10, Math.max(r1, r2));
	return (arg1 * m + arg2 * m) / m;
}

//浮点数乘法运算
function FloatMul(arg1, arg2) {
	var m = 0,
		s1 = arg1.toString(),
		s2 = arg2.toString();
	try {
		m += s1.split('.')[1].length;
	} catch (e) {}
	try {
		m += s2.split('.')[1].length;
	} catch (e) {}
	return Number(s1.replace('.', '')) * Number(s2.replace('.', '')) / Math.pow(10, m);
}

/**
 * 业务校验方法
 */
function businessValidator() {
	//必须输入资产编码或资产来源号
	//资产编码
	let pk_equip = this.props.cardTable.getColValue(this.tableId, 'pk_equip', false, false);
	//资产来源号
	let pk_srcdist_b = this.props.cardTable.getColValue(this.tableId, 'pk_srcdist_b', false, false);

	let flag = true;
	let content = '';
	pk_equip.map((e, index) => {
		if (!pk_equip[index].value && !pk_srcdist_b[index].value) {
			flag = false;
			let num = index + 1;
			content +=
				'<div>' +
				getMultiLangByID('452002004A-000002', { num }) +
				'</div>' /* 国际化处理：<div>第{num}行必须输入资产编码或资产来源；</div> */;
		}
	});
	//必须输入资产编码或资产来源号

	//调试费用表体和表头金额对比
	let money_total_org = this.props.form.getFormItemsValue(this.formId, 'money_total_org');
	let money_org = this.props.cardTable.getColValue(this.tableId, 'money_org', false, false);
	let money_orgs = parseFloat(0);
	money_org.map((e) => {
		let newMoney = e.value == null || e.value == '' ? 0 : e.value;
		let scale = e.scale == null || e.scale == '' ? Math.pow(10, 2) : Math.pow(10, e.scale);
		money_orgs = FloatAdd(FloatMul(money_orgs, scale), FloatMul(newMoney, scale)) / scale;
	});
	if (parseFloat(money_total_org.value) != parseFloat(money_orgs)) {
		flag = false;
		content += '<div>' + getMultiLangByID('452002004A-000003') + '</div>';
		/* 国际化处理： <div>表体调试费用和表头调试费用总额不一致；</div>*/
	}
	//调试费用表体和表头金额对比
	if (!flag) {
		toast({
			content,
			color: 'danger',
			isNode: true
		});
	}
	return flag;
}

//保存单据
export function saveClick() {
	// 保存前必输项校验
	let pass = beforeSaveValidator.call(this, this.props, this.formId, this.tableId, []);
	if (!pass) {
		return;
	}
	//业务校验方法
	if (!businessValidator.call(this)) {
		return;
	}
	let saveStatus = this.nowUIStatus;
	let url = card_addsave; //新增保存
	if (saveStatus === UISTATE.edit) {
		url = card_updatesave; //修改保存
	} else {
	}
	let CardData = this.props.createMasterChildDataSimple(card_pageId, this.formId, this.tableId);
	CardData.bill_type = bill_type;
	this.props.validateToSave(CardData, () => {
		ajax({
			url: url,
			data: CardData,
			success: (res) => {
				let pk_bill = null;
				if (res.success) {
					if (res.data) {
						this.nowUIStatus = UISTATE.browse;
						this.props.form.setFormStatus(this.formId, UISTATE.browse);
						setCardValue.call(this, this.props, res.data);
						pk_bill = res.data.head[this.formId].rows[0].values.pk_bill.value;
						this.setState({ pk_bill: pk_bill });
						// 保存成功后处理缓存
						let cachData = this.props.createMasterChildData(card_pageId, this.formId, this.tableId);
						if (saveStatus == UISTATE.add) {
							cardCache.addCache(pk_bill, cachData, this.formId, dataSource);
						} else {
							cardCache.updateCache('pk_bill', pk_bill, cachData, this.formId, dataSource);
						}
						toggleShow.call(this);
					}
					showMessage.call(this, this.props, { type: MsgConst.Type.SaveSuccess });
					/* 国际化处理： 保存成功*/
				}
			},
			error: (res) => {
				if (res && res.message) {
					toast({ content: res.message, color: 'danger' });
				}
			}
		});
	});
}

//分摊
export function apportionClick() {
	// 清除空表体白行
	this.props.cardTable.filterEmptyRows(this.tableId, [ 'dr', 'rowno', 'pseudocolumn' ]);
	let CardData = this.props.createMasterChildDataSimple(card_pageId, this.formId, this.tableId);
	if (CardData.body[this.tableId].rows.length == 0) {
		toast({ content: getMultiLangByID('452002004A-000008'), color: 'danger' });
		return;
	}
	CardData.bill_type = bill_type;
	ajax({
		url: card_apportion,
		data: CardData,
		success: (res) => {
			if (res.success) {
				setCardValue.call(this, this.props, res.data);
				toast({ content: getMultiLangByID('452002004A-000004'), color: 'success' });
				/* 国际化处理： 分摊成功*/
			}
		},
		error: (res) => {
			if (res && res.message) {
				toast({ content: res.message, color: 'danger' });
			}
		}
	});
}

//提交
export function commitClick(OperatorType, commitType, content) {
	if (commitType === 'saveCommit') {
		// 保存前校验
		let pass = beforeSaveValidator.call(this, this.props, this.formId, this.tableId, []);
		if (!pass) {
			return;
		}
		//业务校验方法
		if (!businessValidator.call(this)) {
			return;
		}
	}
	let Status = this.nowUIStatus;
	let pk = this.props.form.getFormItemsValue(this.formId, 'pk_bill').value;
	// let CardData = this.props.createMasterChildData(card_pageId, this.formId, this.tableId);
	let CardData = this.props.createMasterChildDataSimple(card_pageId, this.formId, this.tableId);
	let paramInfoMap = {};
	paramInfoMap[pk] = ''; //卡片页面ts为空也可以 CardData 里有ts
	let obj = {
		dataType: 'cardData',
		OperatorType: OperatorType,
		commitType: commitType,
		pageid: card_pageId,
		paramInfoMap: paramInfoMap,
		content: content
	};
	CardData.userjson = JSON.stringify(obj);

	if (commitType === 'saveCommit') {
		this.props.validateToSave(CardData, () => {
			ajax({
				url: card_commit,
				data: CardData,
				success: (res) => {
					if (content) {
						this.setState({
							compositedisplay: false
						});
					}
					let callback = (pk_bill, data) => {
						toggleShow.call(this, 'browse');
						this.setState({ pk_bill: pk_bill });
					};
					let setValue = (data) => {
						this.props.form.setFormStatus(this.formId, UISTATE.browse);
						setCardValue.call(this, this.props, data);
					};
					getScriptCardReturnData.call(
						this,
						res,
						this.props,
						this.formId,
						this.tableId,
						'pk_bill',
						dataSource,
						setValue,
						false,
						callback,
						card_pageId
					);
					// if (res.data.success === approveConst.ALLSUCCESS) {
					// 	toggleShow.call(this, 'browse');
					// }
				},
				error: (res) => {
					if (res && res.message) {
						if (content) {
							this.setState({
								compositedisplay: false
							});
						}
						toast({ content: res.message, color: 'warning' });
					}
				}
			});
		});
	} else {
		ajax({
			url: card_commit,
			data: CardData,
			success: (res) => {
				if (content) {
					this.setState({
						compositedisplay: false
					});
				}
				//成功后回调
				let callback = () => {
					toggleShow.call(this, 'browse');
				};

				getScriptCardReturnData.call(
					this,
					res,
					this.props,
					this.formId,
					this.tableId,
					'pk_bill',
					dataSource,
					null,
					false,
					callback,
					card_pageId
				);
				// if (res.data.success === approveConst.ALLSUCCESS) {
				// 	toggleShow.call(this, 'browse');
				// }
			},
			error: (res) => {
				if (res && res.message) {
					if (content) {
						this.setState({
							compositedisplay: false
						});
					}
					toast({ content: res.message, color: 'warning' });
				}
			}
		});
	}
}

//删除单据
export function delConfirm() {
	let pkField = 'pk_bill';
	let paramInfoMap = {};
	let pk = this.props.form.getFormItemsValue(this.formId, pkField).value;
	paramInfoMap[pk] = this.props.form.getFormItemsValue(this.formId, 'ts').value;
	ajax({
		url: list_delete,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: card_pageId
		},
		success: (res) => {
			let { success, data } = res;
			if (res) {
				getScriptCardReturnData.call(
					this,
					res,
					this.props,
					this.formId,
					this.tableId,
					'pk_bill',
					dataSource,
					null,
					true,
					getdata,
					card_pageId
				);
			}
		},
		error: (res) => {
			if (res && res.message) {
				toast({ content: res.message, color: 'warning' });
			}
		}
	});
}

export function modelSave(props) {
	// props.cardTable.closeModel(this.tableId);
	saveClick.call(this);
}
