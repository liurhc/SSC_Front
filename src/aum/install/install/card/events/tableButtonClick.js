import { setApportionBtnsEnable, setBatchBtnsEnable } from './buttonClick';
import { bodyAfterEvent } from './afterEvent';
/**
 * 表格操作按钮
 * @param {*} props 
 * @param {*} key 
 * @param {*} text 
 * @param {*} record 
 * @param {*} index 
 */
export default function(tableId, props, key, text, record, index, status) {
	switch (key) {
		// 表格操作按钮
		case 'DelLine':
			delRow.call(this, props, tableId, index, record);
			break; // 表格操作按钮
		case 'OpenCard':
			OpenCard.call(this, props, tableId, status, record, index);
			break;
		default:
			break;
	}
}
/**
 * 删除数据行
 */
function delRow(props, tableId, index, record) {
	let money_org = props.cardTable.getValByKeyAndIndex(tableId, index, 'money_org').value;
	props.cardTable.delRowsByIndex(tableId, index);
	if (money_org && money_org != 0 && money_org != '0.00') {
		bodyAfterEvent.call(this, props, tableId, 'money_org', { value: '0.00' }, [], 1);
	}
	setApportionBtnsEnable.call(this, props, tableId);
	setBatchBtnsEnable.call(this, props, tableId);
}
/**
 * 卡片态展开事件处理
 * @param {*} record 
 */
function OpenCard(props, tableId, status, record, index) {
	//浏览态展开卡片
	if (status === 'browse') {
		props.cardTable.toggleRowView(tableId, record);
	} else {
		//编辑态展开侧栏
		props.cardTable.openModel(tableId, 'edit', record, index);
	}
}
