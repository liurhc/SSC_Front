import buttonClick, { modelSave, getdata, setDefaultValue, commitClick, delConfirm } from './buttonClick';
import initTemplate from './initTemplate';
import afterEvent from './afterEvent';
import pageInfoClick from './pageInfoClick';
import rowSelected from './rowSelected';
import bodyBeforeEvent from './bodyBeforeEvent';
export {
	buttonClick,
	afterEvent,
	initTemplate,
	pageInfoClick,
	rowSelected,
	bodyBeforeEvent,
	modelSave,
	getdata,
	setDefaultValue,
	commitClick,
	delConfirm
};
