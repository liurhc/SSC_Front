import { pageConfig } from '../const';
import tableButtonClick from './tableButtonClick';
import { init } from './buttonClick';
const { formId, bill_type, form_tableId, card_pageId } = pageConfig;

import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils, cardUtils, closeBrowserUtils } = utils;
const { queryAboutUtils, refInit, LoginContext } = components;
const { openEquipCardByPk } = queryAboutUtils;
const { defRefCondition } = refInit;
const { afterModifyCardMeta } = cardUtils;
const { getMultiLangByID } = multiLangUtils;
const { loginContext, getContext, loginContextKeys } = LoginContext;

const tableId = form_tableId;

export default function(props) {
	let _this = this;
	props.createUIDom(
		{
			pagecode: card_pageId
		},
		function(data) {
			if (data) {
				let status = props.getUrlParam('status');
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
					_this.transi_type = getContext(loginContextKeys.transtype);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.template) {
					let meta = data.template;
					meta = filter.call(_this, props, meta);
					meta = modifierMeta.call(_this, props, meta);
					props.meta.setMeta(meta, () => {
						if (status === 'add') {
							init.call(_this, props);
						}
					});
				}
			}
		}
	);
}

//参照过滤
function filter(props, meta) {
	let pk_group = getContext(loginContextKeys.groupId);
	let transi_type = this.transi_type;
	meta[formId].items.map((item) => {
		if (item['attrcode'] === 'pk_cusmandoc' || item['attrcode'] === 'pk_raorg_v') {
			//调试公司  利润中心
			item.queryCondition = () => {
				// 添加过滤方法
				let data = props.form.getFormItemsValue(formId, [ 'pk_org' ])[0].value || ''; // 获取前面选中参照的值
				return { pk_org: data }; // 根据pk_org过滤
			};
		} else if (item.attrcode == 'pk_org_v') {
			item.queryCondition = () => {
				return { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgVidRefFilter' };
			};
		} else if (item.attrcode == 'pk_fiorg_v') {
			item.queryCondition = () => {
				return { TreeRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgVidRefFilter' };
			};
		} else {
			defRefCondition.call(this, props, item, formId, pk_group);
		}
	});

	meta[tableId].items.map((item) => {
		if (item['attrcode'] === 'pk_equip') {
			item.isMultiSelectedEnabled = true;
			item.renderStatus = 'browse';
			item.render = (text, record, index) => {
				return (
					<div class="card-table-browse">
						<span
							className="code-detail-link"
							onClick={() => {
								openEquipCardByPk(props, record.values.pk_equip.value);
							}}
						>
							{record.values.pk_equip && record.values.pk_equip.display}
						</span>
					</div>
				);
			};

			item.queryCondition = () => {
				// 添加过滤方法
				let data = props.form.getFormItemsValue(formId, [ 'pk_org' ])[0].value || ''; // 获取前面选中参照的值
				return {
					pk_org: data,
					transi_type: transi_type,
					bill_type: bill_type,
					GridRefActionExt: 'nccloud.web.aum.install.refcondition.PK_EQUIPSqlBuilder'
				}; // 根据pk_org过滤
			};
		} else if (item.attrcode == 'pk_srcdist_b') {
			item.isMultiSelectedEnabled = true;
			//设备来源
			item.queryCondition = () => {
				let data = props.form.getFormItemsValue(formId, 'pk_org').value; // 调用相应组件的取值API
				return {
					pk_org: data,
					GridRefActionExt: 'nccloud.web.aum.install.refcondition.PK_SRCDIST_BSqlBuilder'
				}; // 根据pk_org过滤
			};
		} else if (item.attrcode == 'pk_jobmngfil') {
			//项目
			item.queryCondition = () => {
				let data = props.form.getFormItemsValue(formId, 'pk_org').value; // pk_org
				return { pk_org: data, pk_group: pk_group }; // 根据pk_org过滤
			};
		} else {
			defRefCondition.call(this, props, item, formId, pk_group);
		}
	});
	// 卡片模板修改后公共处理
	meta = afterModifyCardMeta.call(this, props, meta);

	return meta;
}

function modifierMeta(props, meta) {
	let _this = this;
	let status = props.getUrlParam('status');
	meta[formId].status = status;
	meta[tableId].status = status;

	let porCol = {
		attrcode: 'opr',
		label: getMultiLangByID('amcommon-000000') /* 国际化处理： 操作*/,
		visible: true,
		className: 'table-opr',
		itemtype: 'customer',
		width: 130,
		fixed: 'right',
		render(text, record, index) {
			let status = props.cardTable.getStatus(tableId);
			let buttonAry = status === 'browse' ? [ 'OpenCard' ] : [ 'OpenCard', 'DelLine' ];
			return props.button.createOprationButton(buttonAry, {
				area: 'card_body_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) =>
					tableButtonClick.call(_this, tableId, props, key, text, record, index, status)
			});
		}
	};
	meta[tableId].items.push(porCol);

	return meta;
}
