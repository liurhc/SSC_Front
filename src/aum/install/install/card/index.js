//主子表卡片
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast, high, cardCache } from 'nc-lightapp-front';
const { NCAffix, NCBackBtn } = base;
import {
	buttonClick,
	initTemplate,
	afterEvent,
	pageInfoClick,
	rowSelected,
	bodyBeforeEvent,
	modelSave,
	getdata,
	setDefaultValue,
	commitClick,
	delConfirm
} from './events';
import { pageConfig } from './const';
const { pageId, card_pageId, formId, bill_type, form_tableId, searchId, title, dataSource } = pageConfig;
import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils, cardUtils, closeBrowserUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { createCardTitleArea, createCardPaginationArea, getCommonTableHeadLeft } = cardUtils;
const { ApprovalTrans } = components;

class Card extends Component {
	constructor(props) {
		super(props);
		this.formId = formId;
		this.searchId = searchId;
		this.tableId = form_tableId;
		this.transi_type = '';
		this.state = {
			bill_type: bill_type,
			pk_org: '',
			bill_code: '',
			show: false,
			showApprove: false,
			pk_bill: props.getUrlParam('id'),
			transi_type: '',
			compositedisplay: false,
			compositedata: {}
		};
		this.nowUIStatus = this.props.getUrlParam('status');
		this.old_pk = ''; //记录旧的pk 方便取消时候恢复
		closeBrowserUtils.call(this, props, { form: [ pageConfig.formId ], cardTable: [ pageConfig.tableId ] });
		initTemplate.call(this, props);
	}
	componentDidMount() {
		if (this.nowUIStatus != 'add') {
			let pk = this.props.getUrlParam('id');
			if (pk && pk != 'undefined') {
				getdata.call(this, pk);
			}
		} else {
			// setDefaultValue.call(this);
		}
	}
	//返回列表界面
	backToList = () => {
		this.props.pushTo('/list', { pagecode: pageId });
	};
	//提交及指派 回调
	getAssginUsedr = (value) => {
		commitClick.call(this, 'SAVE', 'commit', value);
	};
	//取消 指派
	turnOff = () => {
		this.setState({
			compositedisplay: false
		});
	};

	//获取列表肩部信息
	getTableHeadLeft = () => {
		return getCommonTableHeadLeft.call(this, this.props);
	};

	//获取列表肩部信息
	getTableHead = () => {
		return (
			<div className="shoulder-definition-area">
				<div className="definition-icons">
					{this.props.button.createButtonApp({
						area: 'card_body',
						buttonLimit: 3,
						onButtonClick: buttonClick.bind(this)
					})}
				</div>
			</div>
		);
	};
	render() {
		let { cardTable, form, button, ncmodal, cardPagination, ncUploader } = this.props;
		let { createNCUploader } = ncUploader;
		const { createCardPagination } = cardPagination;
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createButtonApp } = button;
		let { createModal } = ncmodal;
		let status = form.getFormStatus(formId) || 'browse';
		const { BillTrack, ApproveDetail } = high;
		return (
			<div className="nc-bill-card">
				{createNCUploader('warcontract-uploader', {})}
				<div className="nc-bill-top-area">
					<NCAffix>
						<div className="nc-bill-header-area">
							{/* 标题 title */}
							{createCardTitleArea.call(this, this.props, {
								title: getMultiLangByID(title),
								formId,
								backBtnClick: this.backToList
							})}

							<div className="header-button-area">
								{createButtonApp({
									area: 'card_head',
									onButtonClick: buttonClick.bind(this)
								})}
							</div>

							{createCardPaginationArea.call(this, this.props, {
								formId,
								dataSource,
								pageInfoClick
							})}
						</div>
					</NCAffix>
					<div className="nc-bill-form-area">
						{createForm(this.formId, {
							onAfterEvent: afterEvent.bind(this)
						})}
					</div>
				</div>

				<div className="nc-bill-bottom-area">
					<div className="nc-bill-table-area">
						{createCardTable(this.tableId, {
							tableHeadLeft: this.getTableHeadLeft.bind(this),
							tableHead: this.getTableHead.bind(this),
							modelSave: modelSave.bind(this),
							onAfterEvent: afterEvent.bind(this),
							onBeforeEvent: bodyBeforeEvent.bind(this),
							showCheck: true,
							showIndex: true,
							isAddRow: true,
							onSelected: rowSelected.bind(this),
							onSelectedAll: rowSelected.bind(this)
						})}
					</div>
				</div>
				{/* 单据追溯 */}
				<BillTrack
					show={this.state.show}
					close={() => {
						this.setState({ show: false });
					}}
					pk={this.state.pk_bill} //单据id
					type={this.state.bill_type} //单据类型
				/>
				{/* 审批详情 */}
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transi_type}
					billid={this.state.pk_bill}
				/>
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002')}
						/* 国际化处理： 指派*/
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				) : (
					''
				)}
				{createModal(`${card_pageId}-confirm`, { color: 'warning' })}
				{/* 确认取消框 */}
			</div>
		);
	}
}

Card = createPage({
	billinfo: {
		// 单据类型
		billtype: 'card',
		// 节点对应的模板编码
		pagecode: card_pageId,
		// 表头(表单)区域编码
		headcode: formId,
		// 表体(表格)区域编码
		bodycode: form_tableId
	}
})(Card);

// ReactDOM.render(<Card />, document.querySelector('#app'));
export default Card;
