//主子表列表

import React, { Component } from 'react';
import { createPage, high, base, createPageIcon } from 'nc-lightapp-front';

import {
	buttonClick,
	initTemplate,
	searchBtnClick,
	pageInfoClick,
	tableModelConfirm,
	doubleClick,
	rowSelected,
	afterEvent,
	commitAction,
	setBatchBtnsEnable
} from './events';
import { pageConfig } from './const';
const { bill_type, tableId, searchId, dataSource, title, pageId } = pageConfig;

import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { ApprovalTrans, queryVocherSrcUtils } = components;
const { queryVoucherSrc } = queryVocherSrcUtils;

const { NCAffix } = base;
class List extends Component {
	constructor(props) {
		super(props);
		this.searchId = searchId;
		this.tableId = tableId;
		this.transi_type = '';
		this.state = {
			bill_type: bill_type,
			show: false,
			showApprove: false,
			pk_bill: '',
			transi_type: '',
			compositedisplay: false,
			compositedata: {}
		};
		initTemplate.call(this, props);
	}
	componentDidMount() {
		//凭证联查来源单据
		queryVoucherSrc(this.props, tableId, 'pk_bill', pageId);
		// this.getData();
	}
	//提交及指派 回调
	getAssginUsedr = (value) => {
		commitAction.call(this, 'SAVE', this.props, value);
	};
	//取消 指派
	turnOff = () => {
		this.props.table.selectAllRows(this.tableId, false);
		setBatchBtnsEnable.call(this, this.props, this.tableId);
		this.setState({
			compositedisplay: false
		});
	};

	render() {
		let { table, search, ncmodal, ncUploader } = this.props;
		let { createNCUploader } = ncUploader;
		let { createModal } = ncmodal;
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		const { BillTrack, ApproveDetail } = high;

		return (
			<div className="nc-bill-list">
				{createNCUploader('warcontract-uploader', {})}
				<NCAffix>
					<div className="nc-bill-header-area">
						<div className="header-title-search-area">
							{createPageIcon()}
							<h2 className="title-search-detail">{getMultiLangByID(title)}</h2>
						</div>
						<div className="header-button-area">
							{this.props.button.createButtonApp({
								area: 'list_head',
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
						</div>
					</div>
				</NCAffix>
				<div className="nc-bill-search-area">
					{NCCreateSearch(this.searchId, {
						onAfterEvent: afterEvent.bind(this),
						clickSearchBtn: searchBtnClick.bind(this),
						dataSource: dataSource,
						pkname: 'pk_bill'
					})}
				</div>

				<div className="nc-bill-table-area">
					{createSimpleTable(this.tableId, {
						handlePageInfoChange: pageInfoClick,
						tableModelConfirm: tableModelConfirm,
						showIndex: true,
						showCheck: true,
						onRowDoubleClick: doubleClick.bind(this),
						onSelected: rowSelected.bind(this),
						onSelectedAll: rowSelected.bind(this),
						dataSource: dataSource,
						pkname: 'pk_bill',
						componentInitFinished: () => {
							//缓存数据赋值成功的钩子函数
							//若初始化数据后需要对数据做修改，可以在这里处理
							setBatchBtnsEnable.call(this, this.props, tableId);
						}
					})}
				</div>
				{/* 审批详情 */}
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transi_type}
					billid={this.state.pk_bill}
				/>
				{/* 单据追溯 */}
				<BillTrack
					show={this.state.show}
					close={() => {
						this.setState({ show: false });
					}}
					pk={this.state.pk_bill} //单据id
					type={this.state.bill_type} //单据类型
				/>
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002')}
						/* 国际化处理： 指派*/
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				) : (
					''
				)}
				{createModal(`${tableId}-del-confirm`, { color: 'warning' })}
			</div>
		);
	}
}

List = createPage({})(List);

// ReactDOM.render(<List />, document.querySelector('#app'));
export default List;
