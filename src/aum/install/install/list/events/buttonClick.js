import { ajax, toast, print, output } from 'nc-lightapp-front';
import { pageConfig } from '../const';
const {
	appcode,
	printFilename,
	printNodekey,
	printListUrl,
	formId,
	tableId,
	pageId,
	dataSource,
	card_pageId,
	list_delete,
	card_commit,
	cardRouter
} = pageConfig;
import pageInfoClick from './pageInfoClick';
import searchBtnClick from './searchBtnClick';

import ampub from 'ampub';
const { utils, components } = ampub;
const { ScriptReturnUtils, queryAboutUtils, queryVocherUtils } = components;
const { getScriptListReturn } = ScriptReturnUtils;
const { openListBillTrack } = queryAboutUtils;
const { queryAboutVoucher } = queryVocherUtils;
const { multiLangUtils, msgUtils, listUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { setBillFlowBtnsEnable, batchRefresh } = listUtils;
const { showConfirm, MsgConst, showMessage } = msgUtils;

export default function buttonClick(props, id) {
	let _this = this;
	switch (id) {
		case 'Add':
			props.pushTo(cardRouter, {
				pagecode: card_pageId,
				status: 'add'
			});
			break;
		case 'Refresh':
			refreshAction.call(this, props);
			break;
		case 'Delete':
			delConfirm.call(this, props);
			break;
		case 'Commit':
			commitAction.call(this, 'SAVE', props);
			break;
		case 'UnCommit':
			commitAction.call(this, 'UNSAVE', props);
			break;
		case 'QueryAboutBusiness':
			openListBillTrack(_this, props, tableId, 'pk_bill');
			break;
		case 'QueryAboutVoucher':
			queryAboutVoucher.call(this, props, tableId, 'pk_bill', appcode, true);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'Attachment':
			attachmentClick.call(this, props);
			break;
		default:
			break;
	}
}

/**
 * 附件上传
 * @param {*} props 
 */
function attachmentClick(props) {
	let checkedrows = props.table.getCheckedRows(tableId);
	// billNo 是单据编码
	let billNo = checkedrows[0].data.values['bill_code'].value;
	// billNo 是单据主键
	let billId = checkedrows[0].data.values['pk_bill'].value;
	props.ncUploader.show('warcontract-uploader', {
		billId: 'pam/bill/' + billId,
		billNo
	});
}

/**
 * 打印
 * @param {*} props 
 */
export function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	let { pageConfig } = props;
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		/* 国际化处理： 请选择需要打印的数据*/
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		printListUrl, // 后台打印服务url
		printData
	);
}

/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });
		/* 国际化处理： 请选择需要输出的数据*/
		return;
	}
	output({
		url: printListUrl,
		data: printData
	});
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let checkedRows = props.table.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length == 0) {
		return false;
	}
	let pks = [];
	checkedRows.map((item) => {
		pks.push(item.data.values['pk_bill'].value);
	});
	let printData = {
		filename: getMultiLangByID(printFilename), // 文件名称
		nodekey: printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

export function delConfirm(props) {
	let checkedRows = props.table.getCheckedRows(tableId);
	if (checkedRows.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseDelete });
		/* 国际化处理： 请选择需要删除的数据*/
		return;
	}
	showConfirm.call(this, props, { type: MsgConst.Type.DelSelect, beSureBtnClick: deleteAction });
}

export function deleteAction(props, data) {
	data = props.table.getCheckedRows(tableId);
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values.pk_bill.value;
		let ts = v.data.values.ts.value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	ajax({
		url: list_delete,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: card_pageId
		},
		success: (res) => {
			getScriptListReturn.call(this, params, res, props, 'pk_bill', formId, tableId, true);
		},
		error: (res) => {
			if (res && res.message) {
				toast({ content: res.message, color: 'danger' });
			}
		}
	});
}

/**
 * 提交 、收回 
 */
export function commitAction(OperatorType, props, content) {
	let data = props.table.getCheckedRows(tableId);
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values.pk_bill.value;
		let ts = v.data.values.ts.value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	let commitType = '';
	if (OperatorType === 'SAVE') {
		//提交传这个
		commitType = 'commit';
	}
	ajax({
		url: card_commit,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: OperatorType,
			pageid: card_pageId,
			commitType: commitType, //提交
			content: content
		},
		success: (res) => {
			if (content) {
				this.setState({
					compositedisplay: false
				});
			}
			getScriptListReturn.call(this, params, res, props, 'pk_bill', formId, tableId, false, dataSource);
		},
		error: (res) => {
			if (res && res.message) {
				toast({ content: res.message, color: 'danger' });
			}
		}
	});
}
/**
 * 刷新
 * @param {*} props 
 */
function refreshAction(props) {
	batchRefresh.call(this, props, searchBtnClick);
}

/**
 * 设置批量按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBatchBtnsEnable(props, moduleId) {
	setBillFlowBtnsEnable.call(this, props, { tableId: moduleId });
}
