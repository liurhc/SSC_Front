import { ajax } from 'nc-lightapp-front';

import { pageConfig } from '../const';
const { tableId, pageId, list_querybypks } = pageConfig;
import ampub from 'ampub';
const { utils } = ampub;
const { listUtils } = utils;
const { setListValue } = listUtils;

import { setBatchBtnsEnable } from './buttonClick';
export default function(props, config, pks) {
	// 后台还没更新，暂不可用
	let data = {
		allpks: pks,
		pagecode: pageId
	};
	//得到数据渲染到页面
	ajax({
		url: list_querybypks,
		data: data,
		success: function(res) {
			let { success } = res;
			if (success) {
				setListValue.call(this, props, res);
				setBatchBtnsEnable.call(this, props, tableId);
			}
		}
	});
}
