import { ajax, toast } from 'nc-lightapp-front';

import { pageConfig } from '../const';
const {
	node_code,
	EditOpr_url,
	formId,
	card_pageId,
	tableId,
	list_delete,
	card_commit,
	cardRouter,
	dataSource
} = pageConfig;

import pageInfoClick from './pageInfoClick';
import ampub from 'ampub';
const { utils, components } = ampub;
const { ScriptReturnUtils } = components;
const { getScriptListReturn } = ScriptReturnUtils;
/**
 * 表格操作按钮
 * @param {*} props 
 * @param {*} key 
 * @param {*} text 
 * @param {*} record 
 * @param {*} index 
 */
export default function(tableId, props, key, text, record, index) {
	switch (key) {
		// 表格操作按钮
		case 'Delete':
			delRow.call(this, props, tableId, index, record);
			break; // 表格操作按钮
		case 'Edit':
			edit.call(this, record, props);
			break;
		case 'Commit':
			commitClick.call(this, 'SAVE', props, tableId, index, record);
			break;
		case 'UnCommit':
			commitClick.call(this, 'UNSAVE', props, tableId, index, record);
			break;
		case 'QueryAboutBillFlow':
			let pk = record.pk_bill.value;
			this.setState({
				pk_bill: pk,
				showApprove: true,
				transi_type: record.transi_type.value
			});
			break;
		default:
			break;
	}
}

/**
 * 提交 收回 动作
 * @param {*} props 
 * @param {*} tableId 
 * @param {*} index 
 * @param {*} record 
 */
function commitClick(OperatorType, props, tableId, index, record) {
	let paramInfoMap = {};
	paramInfoMap[record.pk_bill.value] = record.ts.value;
	let params = [ { id: record.pk_bill.value, index: index } ];
	ajax({
		url: card_commit,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: OperatorType,
			pageid: card_pageId,
			commitType: 'commit'
		},
		success: (res) => {
			getScriptListReturn.call(this, params, res, props, 'pk_bill', formId, tableId, false, dataSource);
		},
		error: (res) => {
			if (res && res.message) {
				toast({ content: res.message, color: 'warning' });
			}
		}
	});
}

//刷新
function refreshAction(props) {
	var pks = props.table.getPks(tableId);
	pageInfoClick(props, '', pks);
}

/**
 * 直接删除数据
 * @param {*} record 
 */
function delRow(props, tableId, index, record) {
	let paramInfoMap = {};
	paramInfoMap[record.pk_bill.value] = record.ts.value;
	let params = [ { id: record.pk_bill.value, index: index } ];
	ajax({
		url: list_delete,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: card_pageId
		},
		success: (res) => {
			getScriptListReturn.call(this, params, res, props, 'pk_bill', formId, tableId, true);
		}
	});
}
/**
 * 修改数据
 * @param {*} props 
 * @param {*} tableId 
 * @param {*} index 
 */
function edit(record, props) {
	ajax({
		url: EditOpr_url,
		data: {
			pk: record.pk_bill.value,
			resourceCode: node_code
		},
		success: (res) => {
			if (res.data) {
				toast({ color: 'warning', content: res.data });
			} else {
				props.pushTo(cardRouter, {
					pagecode: card_pageId,
					status: 'edit',
					id: record.pk_bill.value
				});
			}
		},
		error: (res) => {
			if (res && res.message) {
				toast({ color: 'warning', content: res.message });
			}
		}
	});
}
