import tableButtonClick from './tableButtonClick';
import { pageConfig } from '../const';
const { card_pageId, tableId, pageId, searchId, cardRouter } = pageConfig;
import { setBatchBtnsEnable } from './buttonClick';

import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { multiLangUtils } = utils;
const { assetOrgMultiRefFilter, refInit, LoginContext } = components;
const { AssetOrgMultiRefFilter } = assetOrgMultiRefFilter;
const { defRefCondition, commonRefCondition } = refInit;
const { CommonKeys } = commonConst;
const { IBusiRoleConst } = CommonKeys;
const { loginContext, getContext, loginContextKeys } = LoginContext;
const { getMultiLangByID } = multiLangUtils;

export default function(props) {
	let _this = this;
	props.createUIDom(
		{
			pagecode: pageId
		},
		function(data) {
			if (data) {
				if (data.context) {
					//初始化上下文变量
					loginContext.call(_this, data.context);
					_this.transi_type = getContext(loginContextKeys.transtype);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button, () => {
						setBatchBtnsEnable.call(this, props, tableId);
						props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001') /* 国际化处理： 确定要删除吗？*/);
					});
				}
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(_this, props, meta);
					props.meta.setMeta(meta, () => {
						afterInit.call(_this, props);
					});
				}
			}
		}
	);
}

/**
 * 模板初始化以后
 * @param {*} props
 */
function afterInit(props) {
	setBatchBtnsEnable.call(this, props, tableId);
}

function modifierMeta(props, meta) {
	meta[searchId].items = meta[searchId].items.map((item, key) => {
		item.col = '3';
		return item;
	});
	meta[tableId].pagination = true;
	meta[tableId].items = meta[tableId].items.map((item, key) => {
		item.width = 150;
		if (item.attrcode == 'bill_code') {
			item.render = (text, record, index) => {
				return (
					<div
						class="simple-table-td"
						field="bill_code"
						fieldname={getMultiLangByID('452002004A-000005')} /* 国际化处理： 安装调试单号*/
					>
						<span
							className="code-detail-link"
							onClick={() => {
								props.pushTo(cardRouter, {
									pagecode: card_pageId,
									status: 'browse',
									id: record.pk_bill.value
								});
							}}
						>
							{record && record.bill_code && record.bill_code.value}
						</span>
					</div>
				);
			};
		}
		return item;
	});
	//添加操作列
	meta[tableId].items.push({
		attrcode: 'opr',
		label: getMultiLangByID('amcommon-000000') /* 国际化处理： 操作*/,
		width: 200,
		fixed: 'right',
		className: 'table-opr',
		itemtype: 'customer',
		visible: true,
		render: (text, record, index) => {
			var bill_status = record.bill_status.value;
			let buttonAry;
			if (bill_status === '0') {
				//自由态
				buttonAry = [ 'Commit', 'Edit', 'Delete' ];
			} else if (bill_status === '1') {
				//已提交
				buttonAry = [ 'UnCommit', 'QueryAboutBillFlow' ];
			} else if (bill_status === '2') {
				//审批中
				buttonAry = [ 'QueryAboutBillFlow' ];
			} else if (bill_status === '3') {
				//审批通过
				buttonAry = [ 'UnCommit', 'QueryAboutBillFlow' ];
			} else if (bill_status === '4') {
				//审批未通过
				buttonAry = [ 'Edit', 'Delete', 'QueryAboutBillFlow' ];
			} else if (bill_status === '6') {
				//关闭
			} else {
			}
			return props.button.createOprationButton(buttonAry, {
				area: 'list_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => tableButtonClick.call(this, tableId, props, key, text, record, index)
			});
		}
	});
	let pk_group = getContext(loginContextKeys.groupId);
	meta[searchId].items.map((item) => {
		if (item.attrcode == 'pk_org') {
			item.queryCondition = () => {
				return { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter' };
			};
		} else if (item.attrcode == 'pk_cusmandoc') {
			//组织多选参照过滤
			AssetOrgMultiRefFilter.call(this, props, searchId, item, 'pk_org');
			getQueryCondition.call(this, props, item, 'pk_org');
		} else if (item.attrcode == 'billmaker') {
			//组织多选参照过滤
			AssetOrgMultiRefFilter.call(this, props, searchId, item, 'pk_org');
			getQueryCondition.call(this, props, item, 'pk_org');
		} else if (item.attrcode == 'bodyvos.pk_equip.pk_mandept' || item.attrcode == 'bodyvos.pk_equip.pk_manager') {
			AssetOrgMultiRefFilter.call(this, props, searchId, item, 'bodyvos.pk_equip.pk_ownerorg');
			// 管理部门、管理人根据货主管理组织过滤
			item.queryCondition = () => {
				let pk_ownerorg = getSearchValue.call(this, props, 'bodyvos.pk_equip.pk_ownerorg');
				if (pk_ownerorg) {
					return { pk_org: pk_ownerorg, isShowUnit: true, busifuncode: IBusiRoleConst.ASSETORG };
				} else {
					return { isShowUnit: true, busifuncode: IBusiRoleConst.ASSETORG };
				}
			};
			if (item.attrcode == 'bodyvos.pk_equip.pk_mandept') {
				// 添加【执行时包含下级】默认勾选
				item.isRunWithChildren = true;
				item.defaultRunWithChildren = true;
			}
		} else if (item.attrcode == 'bodyvos.pk_equip.pk_usedept' || item.attrcode == 'bodyvos.pk_equip.pk_user') {
			AssetOrgMultiRefFilter.call(this, props, searchId, item, 'bodyvos.pk_equip.pk_usedunit');
			// 使用部门、使用人根据使用权过滤
			item.queryCondition = () => {
				let pk_usedunit = getSearchValue.call(this, props, 'bodyvos.pk_equip.pk_usedunit');
				if (pk_usedunit) {
					return { pk_org: pk_usedunit, isShowUnit: true, busifuncode: IBusiRoleConst.ASSETORG };
				} else {
					return { isShowUnit: true, busifuncode: IBusiRoleConst.ASSETORG };
				}
			};
			if (item.attrcode == 'bodyvos.pk_equip.pk_usedept') {
				// 添加【执行时包含下级】默认勾选
				item.isRunWithChildren = true;
				item.defaultRunWithChildren = true;
			}
		} else {
			let bodyDefPrefix = 'bodyvos.def';
			defRefCondition.call(this, props, item, searchId, pk_group, true, 'pk_org', bodyDefPrefix);
			defRefCondition.call(this, props, item, searchId, pk_group, true);
		}
		// 添加【执行时包含下级】默认勾选
		commonRefCondition.call(this, props, item);
	});

	return meta;
}

//获取查询条件的值
function getSearchValue(props, field) {
	let data = props.search.getSearchValByField(searchId, field);
	if (data && data.value && data.value.firstvalue && 20 == data.value.firstvalue.length) {
		let firstvalue = data.value.firstvalue;
		if (firstvalue.split(',').length == 1) {
			return firstvalue;
		}
	}
}

/**
 * 获取字段的过滤条件，针对是部门和人员等需要组织多选参照的字段
 */
function getQueryCondition(props, item, org) {
	item.queryCondition = () => {
		let pk_org = getSearchValue.call(this, props, org); //主组织
		let filter = { busifuncode: IBusiRoleConst.ASSETORG };
		if (pk_org) {
			filter['pk_org'] = pk_org;
		}
		return filter; // 根据pk_org过滤
	};
}
