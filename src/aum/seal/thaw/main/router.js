import { asyncComponent } from 'nc-lightapp-front';

const List = asyncComponent(() =>
	import(/* webpackChunkName: "aum/seal/thaw/list/list" */ /* webpackMode: "eager" */ '../list')
);
const Card = asyncComponent(() =>
	import(/* webpackChunkName: "aum/seal/thaw/card/card" */ /* webpackMode: "eager" */ '../card')
);
const transfer = asyncComponent(() =>
	import(/* webpackChunkName: "aum/seal/freezetransfer/source/source" */ /* webpackMode: "eager" */ '../../freezetransfer/source')
);
const routes = [
	{
		path: '/',
		component: List,
		exact: true
	},
	{
		path: '/list',
		component: List
	},
	{
		path: '/card',
		component: Card
	},
	{
		path: '/transfer',
		component: transfer
	}
];

export default routes;
