//主子表卡片
import React, { Component } from 'react';
import { createPage, ajax, base, toast, high } from 'nc-lightapp-front';
const { NCAffix, NCBackBtn } = base;
import {
	buttonClick,
	initTemplate,
	afterEvent,
	pageInfoClick,
	transferBtnClick,
	rowSelected,
	bodyBeforeEvent,
	saveClick,
	toggleShow,
	putHeadFormValue,
	putTableValue,
	commitClick,
	pushToTransfer,
	transferSave
} from './events';

import { pageConfig } from './const';
const {
	card_pageId,
	formId,
	bill_type,
	form_tableId,
	form_pageId,
	searchId,
	TRANSFERINFO,
	listRouter,
	dataSourceTransfer,
	appcode,
	title,
	dataSource
} = pageConfig;

import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { multiLangUtils, cardUtils, closeBrowserUtils, msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { createCardTitleArea, createCardPaginationArea, getCommonTableHeadLeft } = cardUtils;
const { ApprovalTrans, LoginContext } = components;
const { getContext } = LoginContext;
const { CommonKeys, StatusUtils } = commonConst;
const { TransferConst } = CommonKeys;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { UISTATE } = StatusUtils;

class Card extends Component {
	constructor(props) {
		super(props);
		this.formId = formId;
		this.searchId = searchId;
		this.tableId = form_tableId;
		this.transi_type = '';
		this.state = {
			bill_type: bill_type,
			pk_org: '',
			bill_code: '',
			show: false,
			showApprove: false,
			pk_thaw: props.getUrlParam('id'),
			transi_type: '',
			compositedisplay: false,
			compositedata: {}
		};
		//转单需要信息
		this.srctype = props.getUrlParam(TransferConst.srctype);
		if (this.srctype == TRANSFERINFO.srctype) {
			this.pageid = TRANSFERINFO.pagecode;
		}
		this.leftarea = TRANSFERINFO.leftarea;
		this.transferIndex = 0; //转单时左侧选择的数据序号
		this.transferEditId = new Array(); //转单后已处理单据又点修改的主键存储
		this.nowUIStatus = props.getUrlParam('status'); //存储界面当前状态
		this.old_pk = ''; //记录旧的pk 方便取消时候恢复
		closeBrowserUtils.call(this, props, {
			form: [ this.formId ],
			cardTable: [ this.tableId ]
		});
		initTemplate.call(this, props);
	}
	componentDidMount() {}

	//返回列表界面
	backToList = () => {
		let status = this.nowUIStatus;
		if (status == UISTATE.transferAdd || status == UISTATE.transferBorwse || status == UISTATE.transferEdit) {
			//判断当前单据是否存在没有处理的
			let count = this.props.transferTable.getTransformFormAmount(TRANSFERINFO.leftarea); //转单数据的数量
			let isHas = true;
			for (let index = 0; index < count; index++) {
				isHas = this.props.transferTable.getTransformFormStatus(TRANSFERINFO.leftarea, index);
				if (!isHas) {
					break;
				}
			}
			if (!isHas) {
				showConfirm.call(this, this.props, {
					type: MsgConst.Type.Back,
					beSureBtnClick: pushToTransfer.bind(this)
				});
			} else {
				//转过来的回去转单
				pushToTransfer.call(this);
			}
		} else {
			this.props.pushTo(listRouter, {
				pagecode: form_pageId
			});
		}
	};

	//提交及指派 回调
	getAssginUsedr = (value) => {
		commitClick.call(this, 'SAVE', 'commit', value);
	};
	//取消 指派
	turnOff = () => {
		this.setState({
			compositedisplay: false
		});
	};
	modelSave = (props) => {
		// props.cardTable.closeModel(this.tableId);
		saveClick.call(this);
	};

	//获取列表肩部信息
	getTableHeadLeft = () => {
		return getCommonTableHeadLeft.call(this, this.props);
	};

	//获取列表肩部信息
	getTableHead = () => {
		return (
			<div className="shoulder-definition-area">
				<div className="definition-icons">
					{this.props.button.createButtonApp({
						area: 'card_body',
						buttonLimit: 3,
						onButtonClick: buttonClick.bind(this),
						popContainer: document.querySelector('.header-button-area')
					})}
				</div>
			</div>
		);
	};
	//----------------转单处理---start-----------
	//根据上游来源单据信息获取下游单据信息
	getDataBySrcId = (props) => {
		let pks = props.transferTable.getTransferTableSelectedId();
		ajax({
			url: '/nccloud/aum/thaw/changevo_srcids.do',
			data: {
				appcode: appcode,
				appcode: appcode,
				pks: pks,
				pageid: this.pageid,
				srcbilltype: getContext(TransferConst.src_billtype_cache, TransferConst.dataSource),
				srctranstype: getContext(TransferConst.src_transtype_cache, TransferConst.dataSource),
				nextbilltype: '4A10',
				nexttranstype: this.transi_type
			},
			success: (res) => {
				if (res.data) {
					this.props.transferTable.setTransferListValue(this.leftarea, res.data);
				} else {
					showMessage.call(this, props, { type: MsgConst.Type.DataDeleted });
					/* 国际化处理： 数据已删除*/
				}
				toggleShow.call(this);
				//设置组织不可编辑
				props.form.setFormItemsDisabled(this.formId, { pk_org: true, pk_org_v: true });
			}
		});
	};

	//设置转单右侧界面数据
	putTransferRightValue = (record, index, status) => {
		this.transferIndex = index;
		//点击转单缩略图的钩子函数 status:转单处理状态
		if (status) {
			let pk_thaw = this.props.form.getFormItemsValue(this.formId, 'pk_thaw');
			if (this.transferEditId.indexOf(pk_thaw.value) == -1) {
				toggleShow.call(this, UISTATE.transferBorwse);
			} else {
				toggleShow.call(this, UISTATE.transferEdit);
			}
		} else {
			toggleShow.call(this, UISTATE.transferAdd);
		}
		putHeadFormValue.call(this, record.head[this.formId]);
		putTableValue.call(this, record.body[this.tableId]);
	};
	//同步单据信息到转单控件
	synTransferData = () => {
		//重取界面现有数据赋值到转单中
		let cardData = this.props.createMasterChildData(this.pageid, this.formId, this.tableId);
		this.props.transferTable.setTransferListValueByIndex(TRANSFERINFO.leftarea, cardData, this.transferIndex);
	};
	//转单公用原单据按钮功能
	selfButtonClick = (props, id) => {
		buttonClick.call(this, props, id);
	};
	//----------------转单处理---end-----------

	render() {
		let { cardTable, form, button, ncmodal, cardPagination, transferTable, ncUploader } = this.props;
		let { createNCUploader } = ncUploader;
		const { createCardPagination } = cardPagination;
		let buttons = this.props.button.getButtons();
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createButtonApp } = button;
		let { createModal } = ncmodal;
		let status = this.nowUIStatus;
		let srctype = this.srctype;
		const { createTransferList } = transferTable;
		let leftarea = this.leftarea;
		const { BillTrack, ApproveDetail, PrintOutput } = high;
		let statusNew = form.getFormStatus(formId) || UISTATE.browse;
		return (
			<div>
				{srctype == TRANSFERINFO.srctype ? (
					<div className="nc-bill-transferList" style={{ backgroundColor: '#fff' }}>
						<div className="nc-bill-top-area">
							<NCAffix>
								<div className="nc-bill-header-area">
									<div className="header-title-search-area">
										<NCBackBtn
											className="title-search-detail"
											onClick={() => {
												this.backToList.call(this, this.props);
											}}
										/>

										<h2 className="title-search-detail">{getMultiLangByID(title)}</h2>
									</div>
									<div className="header-button-area">
										{createButtonApp({
											area: 'card_head',
											buttonLimit: 3,
											onButtonClick: transferBtnClick.bind(this)
										})}
									</div>
								</div>
							</NCAffix>
						</div>
						<div className="nc-bill-transferList-content">
							{createTransferList({
								dataSource: dataSourceTransfer,
								pkname: 'pk_freeze_b_src',
								headcode: this.formId,
								transferListId: leftarea, //转单列表id
								onTransferItemSelected: (record, status, index) => {
									this.putTransferRightValue(record, index, status);
								},
								onTransferItemClick: (record, index, status) => {
									this.putTransferRightValue(record, index, status);
								}
							})}
							<div className="nc-bill-card transferList-content-right">
								<div className="nc-bill-form-area">
									{createForm(this.formId, {
										onAfterEvent: afterEvent.bind(this)
									})}
								</div>
								<div className="nc-bill-bottom-area">
									<div className="nc-bill-table-area">
										{/* {this.getTableHead()} */}
										{createCardTable(this.tableId, {
											tableHeadLeft: this.getTableHeadLeft.bind(this),
											tableHead: this.getTableHead.bind(this),
											modelSave: transferSave.bind(this),
											showIndex: true,
											showCheck: true,
											isAddRow: true,
											onAfterEvent: afterEvent.bind(this),
											onBeforeEvent: bodyBeforeEvent.bind(this),
											onSelected: rowSelected.bind(this),
											onSelectedAll: rowSelected.bind(this)
										})}
									</div>
								</div>
							</div>
						</div>
					</div>
				) : (
					<div className="nc-bill-card">
						<div className="nc-bill-top-area">
							<NCAffix>
								<div className="nc-bill-header-area">
									{/* 标题 title */}
									{createCardTitleArea.call(this, this.props, {
										title: getMultiLangByID(title),
										formId,
										backBtnClick: this.backToList
									})}
									<div className="header-button-area">
										{this.props.button.createButtonApp({
											area: 'card_head',
											buttonLimit: 3,
											onButtonClick: buttonClick.bind(this),
											popContainer: document.querySelector('.header-button-area')
										})}
									</div>
									{createCardPaginationArea.call(this, this.props, {
										formId,
										dataSource,
										pageInfoClick
									})}
								</div>
							</NCAffix>
							<div className="nc-bill-form-area">
								{createForm(this.formId, {
									onAfterEvent: afterEvent.bind(this)
								})}
							</div>
						</div>

						<div className="nc-bill-bottom-area">
							<div className="nc-bill-table-area">
								{/* {this.getTableHead()} */}
								{createCardTable(this.tableId, {
									tableHeadLeft: this.getTableHeadLeft.bind(this),
									tableHead: this.getTableHead.bind(this),
									modelSave: this.modelSave.bind(this),
									showIndex: true,
									showCheck: true,
									isAddRow: true,
									onAfterEvent: afterEvent.bind(this),
									onBeforeEvent: bodyBeforeEvent.bind(this),
									onSelected: rowSelected.bind(this),
									onSelectedAll: rowSelected.bind(this)
								})}
							</div>
						</div>
					</div>
				)}
				{/* 确认取消框 */}
				{createModal(`${card_pageId}-confirm`, { color: 'warning' })}
				{createNCUploader('warcontract-uploader', {})}
				<BillTrack
					show={this.state.show}
					close={() => {
						this.setState({ show: false });
					}}
					pk={this.state.pk_thaw} //单据id
					type={this.state.bill_type} //单据类型
				/>
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transi_type}
					billid={this.state.pk_thaw}
				/>
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002')}
						/* 国际化处理： 指派*/
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				) : (
					''
				)}
			</div>
		);
	}
}

Card = createPage({
	billinfo: {
		// 单据类型
		billtype: 'card',
		// 节点对应的模板编码
		pagecode: card_pageId,
		// 表头(表单)区域编码
		headcode: formId,
		// 表体(表格)区域编码
		bodycode: form_tableId
	}
})(Card);
export default Card;
// ReactDOM.render(<Card />, document.querySelector('#app'));
