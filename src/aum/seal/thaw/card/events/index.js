import buttonClick, {
	saveClick,
	getdata,
	setDefaultValue,
	toggleShow,
	delConfirm,
	putHeadFormValue,
	putTableValue,
	commitClick
} from './buttonClick';
import initTemplate from './initTemplate';
import afterEvent from './afterEvent';
import pageInfoClick from './pageInfoClick';
import transferBtnClick, { pushToTransfer, transferSave } from './transferBtnClick';
import rowSelected from './rowSelected';
import bodyBeforeEvent from './bodyBeforeEvent';
export {
	transferBtnClick,
	pageInfoClick,
	initTemplate,
	buttonClick,
	afterEvent,
	rowSelected,
	bodyBeforeEvent,
	saveClick,
	getdata,
	setDefaultValue,
	toggleShow,
	delConfirm,
	putHeadFormValue,
	putTableValue,
	commitClick,
	pushToTransfer,
	transferSave
};
