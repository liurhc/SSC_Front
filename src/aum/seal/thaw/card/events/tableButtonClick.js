import { setBtnsEnable, setBatchBtnsEnable } from './buttonClick';

/**
 * 表格操作按钮
 * @param {*} props 
 * @param {*} key 
 * @param {*} text 
 * @param {*} record 
 * @param {*} index 
 */
export default function(tableId, props, key, text, record, index, status) {
	switch (key) {
		// 表格操作按钮
		case 'DelLine':
			delRow.call(this, props, tableId, index, record);
			break; // 表格操作按钮
		case 'OpenCard':
			OpenCard.call(this, props, tableId, status, record, index);
			break;
		default:
			break;
	}
}
/**
 * 删除数据行
 */
function delRow(props, tableId, index, record) {
	props.cardTable.delRowsByIndex(tableId, index);
	setBatchBtnsEnable.call(this, props, tableId);
	setBtnsEnable.call(this, props, tableId);
}
/**
 * 卡片态展开事件处理
 */
function OpenCard(props, tableId, status, record, index) {
	//浏览态展开卡片
	if (status === 'browse') {
		props.cardTable.toggleRowView(tableId, record);
	} else {
		//编辑态展开侧栏
		props.cardTable.openModel(tableId, 'edit', record, index);
	}
}
