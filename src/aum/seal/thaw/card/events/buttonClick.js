import { ajax, toast, print, cardCache, output } from 'nc-lightapp-front';
import { pageConfig } from '../const';
const {
	node_code,
	EditOpr_url,
	printFilename,
	printNodekey,
	printCardUrl,
	card_pageId,
	formId,
	bill_type,
	card_query,
	card_addsave,
	card_updatesave,
	list_delete,
	card_commit,
	dataSource,
	sourceRouter,
	BatchAlter_url,
	TRANSFERINFO,
	dataSourceTransfer
} = pageConfig;
import { headAfterEvent } from './afterEvent';

import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { ScriptReturnUtils, LoginContext, queryAboutUtils, saveValidatorsUtil } = components;
const { getScriptCardReturnData } = ScriptReturnUtils;
const { openEquipCard, openBillTrack } = queryAboutUtils;

const { beforeSaveValidator } = saveValidatorsUtil;
const { multiLangUtils, cardUtils, msgUtils, transferUtils } = utils;
const { pushToTransferPage } = transferUtils;
const { getMultiLangByID } = multiLangUtils;
const { setCardValue, setHeadAreaData } = cardUtils;
const { getContext, loginContextKeys } = LoginContext;
const { StatusUtils, CommonKeys } = commonConst;
const { ButtonConst, TransferConst } = CommonKeys;

const { showConfirm, MsgConst, showMessage } = msgUtils;
const { UISTATE } = StatusUtils;

export default function(props, id) {
	let _this = this;
	let pk = props.form.getFormItemsValue(this.formId, 'pk_thaw').value;
	switch (id) {
		case 'Add':
			this.old_pk = pk;
			props.form.EmptyAllFormValue(this.formId);
			props.cardTable.setTableData(this.tableId, { rows: [] });
			init.call(this, props);
			break;
		case 'Edit':
			ajax({
				url: EditOpr_url,
				data: {
					pk: pk,
					resourceCode: node_code
				},
				success: (res) => {
					if (res.data) {
						toast({ color: 'warning', content: res.data });
					} else {
						props.cardTable.closeExpandedRow(this.tableId);
						toggleShow.call(this, 'edit');
					}
				},
				error: (res) => {
					if (res && res.message) {
						toast({ color: 'warning', content: res.message });
					}
				}
			});
			break;
		case 'Delete':
			deleteConfirm.call(this, props);
			break;
		case 'Save':
			saveClick.call(this);
			break;
		case 'Commit':
			commitClick.call(this, 'SAVE', 'commit');
			break;
		case 'SaveCommit':
			commitClick.call(this, 'SAVE', 'saveCommit');
			break;
		case 'QueryAboutBusiness':
			openBillTrack(_this);
			break;
		case 'QueryAboutBillFlow':
			_this.setState({
				showApprove: true,
				transi_type: props.form.getFormItemsValue(this.formId, 'transi_type').value
			});
			break;
		case 'QueryAboutCard':
			openEquipCard(_this, props, this.tableId);
			break;
		case 'UnCommit':
			commitClick.call(this, 'UNSAVE', '');
			break;
		case 'Cancel':
			cancelConfirm.call(this, props, pk);
			break;
		case 'AddLine':
			props.cardTable.addRow(this.tableId);
			setBtnsEnable.call(this, props, this.tableId);
			break;
		case 'Refresh':
			getdata.call(this, pk, true);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'DelLine':
			delLineClick.call(this, props);
			break;
		case 'BatchAlter':
			BatchAlter.call(this, props, this.tableId);
			break;
		case 'Attachment':
			attachmentClick.call(this, props);
			break;
		default:
			//默认是动态加载的拉单按钮，拉单按钮的code为“单据类型+交易类型”
			let index = id.indexOf('+');
			let srcinfo = new Array(2);
			if (index > 0) {
				srcinfo = id.split('+');
			} else {
				srcinfo[0] = id;
			}
			//清掉上游缓存(退出转单和从上游返回下游然后再到上游的时候)
			props.transferTable.deleteCache(dataSourceTransfer);
			pushToTransferPage.call(
				this,
				sourceRouter,
				srcinfo[0],
				srcinfo[1],
				bill_type,
				this.transi_type,
				'452002508A_transfercard'
			);
			break;
	}
}

/**
 * 删除弹框提示
 * @param {*} props 
 */
function deleteConfirm(props) {
	showConfirm.call(this, props, { type: MsgConst.Type.Delete, beSureBtnClick: delConfirm });
}

/**
 * 取消
 * @param {*} props 
 */
export function cancelConfirm(props, pk) {
	let cancelClick = () => {
		// 恢复字段的可编辑性
		let pk_org_v = props.form.getFormItemsValue(this.formId, 'pk_org_v');
		if (!pk_org_v || !pk_org_v.value) {
			props.resMetaAfterPkorgEdit();
		}
		// 加载编辑前数据，如果编辑前没有数据，则加载当前列表最后一条数据，如果还没有，显示为空不处理
		if (!pk) {
			pk = cardCache.getCurrentLastId(dataSource);
		}
		this.nowUIStatus = 'browse';
		if (this.old_pk && this.old_pk != '') {
			getdata.call(this, this.old_pk);
		} else {
			getdata.call(this, pk);
		}
	};
	showConfirm.call(this, props, {
		type: MsgConst.Type.Cancel,
		beSureBtnClick: cancelClick
	});
}

/**
 * 附件上传
 * @param {*} props 
 */
function attachmentClick(props) {
	let billId = props.form.getFormItemsValue(this.formId, 'pk_thaw').value;
	props.ncUploader.show('warcontract-uploader', {
		billId: 'pam/thaw/' + billId
	});
}

/**
 * 批改
 * @param {*} props 
 */
function BatchAlter(props, tableId) {
	// 获取原始数据
	let changeData = props.cardTable.getTableItemData(tableId);
	let cardData = props.createMasterChildDataSimple(card_pageId, formId, tableId);
	ajax({
		url: BatchAlter_url,
		data: {
			card: cardData,
			batchChangeIndex: changeData.batchChangeIndex, // 原始数据行号
			batchChangeKey: changeData.batchChangeKey, // 原始数据key
			batchChangeValue: changeData.batchChangeValue, // 原始数据value // 原始数据所在的vo，对应聚合VO中的tableCodes，一主一子可为空，一主多子要传对应的值
			tableCode: ''
		},
		async: false,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				setCardValue.call(this, props, data);
			}
		},
		error: (res) => {
			if (res && res.message) {
				toast({ color: 'danger', content: res.message });
			}
		}
	});
}

/**
 * 删行
 * @param {*} props 
 */
function delLineClick(props) {
	//选中的数据
	let checkedRows = props.cardTable.getCheckedRows(this.tableId);
	let checkedIndex = [];
	checkedRows.map((item) => {
		checkedIndex.push(item.index);
	});
	props.cardTable.delRowsByIndex(this.tableId, checkedIndex);
	setBatchBtnsEnable.call(this, props, this.tableId);
	setBtnsEnable.call(this, props, this.tableId);
}

/**
 * 打印
 * @param {*} props 
 */
export function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint }); /* 国际化处理： 请选择需要打印的数据*/
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		printCardUrl, // 后台打印服务url
		printData
	);
}

/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });
		/* 国际化处理： 请选择需要输出的数据*/
		return;
	}
	output({
		url: printCardUrl,
		data: printData
	});
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let pk = props.form.getFormItemsValue(this.formId, 'pk_thaw');
	if (!pk || !pk.value) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint }); /* 国际化处理： 请选择需要打印的数据*/
		return;
	}

	let pks = [ pk.value ];
	let printData = {
		filename: getMultiLangByID(printFilename), // 文件名称
		nodekey: printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

/**
 * 设置批量按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBatchBtnsEnable(props, moduleId) {
	let checkedRows = props.cardTable.getCheckedRows(moduleId);
	let batchBtns = [ 'QueryAboutCard', 'DelLine' ];
	props.button.setButtonDisabled(batchBtns, !(checkedRows && checkedRows.length > 0));
}
/**
 * 设置批改按钮的状态
 * @param {} props 
 * @param {*} moduleId 
 */
export function setBtnsEnable(props, moduleId) {}

/**
 * 页面初始化数据
 */
export function init(props) {
	setDefaultValue.call(this);
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	props.form.setFormItemsDisabled(this.formId, { pk_org_v: false, pk_org: false });
	if (pk_org_v && pk_org_v.value && pk_org_v.value != null && pk_org_v.value != '') {
		headAfterEvent.call(this, props, this.formId, 'pk_org_v', pk_org_v, { value: '' }, pk_org_v.value);
	} else {
		props.initMetaByPkorg('pk_org_v');
		props.button.setButtonDisabled([ 'AddLine', 'DelLine', 'BatchAlter' ], true);
	}
}

export function setDefaultValue(isNoOrg = false) {
	this.props.form.setFormItemsValue(formId, {
		bill_status: { value: '0', display: getMultiLangByID('statusUtils-000000') /* 国际化处理： 自由态*/ },
		bill_type: { value: bill_type, display: bill_type },
		transi_type: { value: getContext(loginContextKeys.transtype), display: getContext(loginContextKeys.transtype) },
		pk_transitype: { value: getContext(loginContextKeys.pk_transtype) },
		pk_group: { value: getContext(loginContextKeys.groupId), display: getContext(loginContextKeys.groupName) },
		bill_date: {
			value: getContext(loginContextKeys.businessDate),
			display: getContext(loginContextKeys.businessDate)
		}
	});
	if (!isNoOrg) {
		this.props.form.setFormItemsValue(formId, {
			pk_org_v: {
				value: getContext(loginContextKeys.pk_org_v),
				display: getContext(loginContextKeys.org_v_Name)
			},
			pk_org: { value: getContext(loginContextKeys.pk_org), display: getContext(loginContextKeys.org_Name) }
		});
	}
	toggleShow.call(this, 'add');
}

//根据页面状态设置控件属性
export function toggleShow(status) {
	if (!status) {
		status = this.nowUIStatus;
	} else {
		this.nowUIStatus = status;
	}
	// 更新参数
	let pkVal = this.props.form.getFormItemsValue(formId, 'pk_thaw');
	this.props.setUrlParam({ status, id: pkVal ? pkVal.value : '' });

	let editButtons = [ 'Save', 'Cancel', 'AddLine', 'DelLine', 'BatchAlter', 'SaveCommit', 'OpenCard' ];
	let borwseButtons = [
		'Edit',
		'Add',
		'Back',
		'Delete',
		'Commit',
		'UnCommit',
		'Print',
		'PrintGroup',
		'Preview',
		'Output',
		'OpenCard',
		'Attachment',
		'QueryAbout',
		'QueryAboutGroup',
		'QueryAboutCard',
		'QueryAboutBusiness',
		'QueryAboutBillFlow',
		'Refresh'
	];
	let editflag = false;
	let areaStatus = 'browse';
	if (
		status == UISTATE.add ||
		status == UISTATE.edit ||
		status == UISTATE.transferAdd ||
		status == UISTATE.transferEdit
	) {
		areaStatus = 'edit';
		editflag = true;
	}
	//设置表单和表格的可编辑性
	this.props.form.setFormStatus(this.formId, areaStatus);
	this.props.cardTable.setStatus(this.tableId, areaStatus);

	if (editflag) {
		//设置按钮的可见性
		this.props.button.setButtonVisible(borwseButtons, !editflag);
		this.props.button.setButtonVisible(editButtons, editflag);

		this.props.button.setButtonVisible(ButtonConst.AddTransferGroup, false);
	} else {
		//设置按钮的可见性
		this.props.button.setButtonVisible(editButtons, editflag);
		this.props.button.setButtonVisible(borwseButtons, !editflag);
		this.props.button.setButtonVisible(ButtonConst.AddTransferGroup, true);
	}
	if (status == UISTATE.transferBorwse) {
		//转单新增保存后不需要新增按钮
		this.props.button.setButtonVisible([ 'Add' ], false);
	}
	if (status == UISTATE.transferAdd || status == UISTATE.transferEdit) {
		//退出转单
		this.props.button.setButtonVisible([ 'Quit' ], true);
	}
	//处理完正常按钮后 进行处理浏览态不同单据状态下的隐藏 按钮
	if (status === UISTATE.browse || status === UISTATE.transferBorwse) {
		if (this.props.form.getFormItemsValue(this.formId, [ 'bill_status' ])[0] != undefined) {
			let bill_status = this.props.form.getFormItemsValue(this.formId, [ 'bill_status' ])[0].value;
			if (bill_status === '0') {
				//自由态
				this.props.button.setButtonVisible([ 'UnCommit' ], false); //不显示收回
			} else if (bill_status === '1') {
				//已提交
				this.props.button.setButtonVisible([ 'Commit', 'Delete', 'Edit' ], false); //不显示提交、修改、删除
			} else if (bill_status === '2') {
				//审批中
				this.props.button.setButtonVisible([ 'Commit', 'Delete', 'Edit', 'UnCommit' ], false); //不显示提交、修改、删除、收回
			} else if (bill_status === '3') {
				//审批通过
				this.props.button.setButtonVisible([ 'Commit', 'Delete', 'Edit' ], false); //不显示提交、修改、删除
			} else if (bill_status === '4') {
				//审批未通过
				this.props.button.setButtonVisible([ 'UnCommit', 'Commit' ], false); //不显示收回
				// this.props.button.setButtonVisible(
				// 	[ 'Commit', 'Delete', 'Edit', 'UnCommit', 'QueryAboutBillFlow' ],
				// 	false
				// ); //不显示提交、修改、删除、收回
			} else if (bill_status === '6') {
				//关闭
				this.props.button.setButtonVisible([ 'Commit', 'Delete', 'Edit', 'UnCommit' ], false); //不显示提交、修改、删除、收回
			} else {
				//新增时取消过来的
				this.props.button.setButtonVisible([ 'Commit', 'Delete', 'Edit', 'UnCommit' ], false); //不显示提交、修改、删除、收回
			}
		} else {
			this.props.button.setButtonVisible([ 'Commit', 'Delete', 'Edit', 'UnCommit' ], false); //不显示提交、修改、删除、收回
		}
	} else {
		if (status === UISTATE.edit) {
			this.props.form.setFormItemsDisabled(this.formId, { pk_org_v: true, pk_org: true });
			setBatchBtnsEnable.call(this, this.props, this.tableId);
			setBtnsEnable.call(this, this.props, this.tableId);
		} else {
			//新增设置按钮的禁用状态
			this.props.button.setButtonDisabled([ 'QueryAboutCard', 'DelLine' ], true);
		}
	}
	let bill_code = this.props.form.getFormItemsValue(this.formId, [ 'bill_code' ])[0].value;
	setHeadAreaData.call(this, this.props, { bill_code, status });
}
//界面状态由编辑态取消到对应status
export function cancel(status) {
	this.props.form.cancel(this.formId);
	this.props.cardTable.resetTableData(this.tableId);
	toggleShow.call(this, status);
}

export function setEmptyData() {
	this.props.form.EmptyAllFormValue(this.formId);
	this.props.cardTable.setTableData(this.tableId, { rows: [] });
	toggleShow.call(this, 'browse');
	let borwseButtons = [
		'Edit',
		'Back',
		'Delete',
		'Commit',
		'UnCommit',
		'Print',
		'PrintG',
		'Preview',
		'Output',
		'OpenCard',
		'Attachment',
		'QueryAbout',
		'QueryAboutGroup',
		'QueryAboutCard',
		'QueryAboutBusiness',
		'QueryAboutBillFlow',
		'Refresh'
	];
	this.props.button.setButtonVisible(borwseButtons, false);
}

//通过单据id查询单据信息
export function getdata(pk, isRefresh = false) {
	if (pk == null || pk == '') {
		setEmptyData.call(this);
		return;
	}
	let cachData = cardCache.getCacheById(pk, dataSource);
	if (cachData && !isRefresh) {
		setCardValue.call(this, this.props, cachData);
		toggleShow.call(this);
	} else {
		let data = { pk, pagecode: card_pageId };
		ajax({
			url: card_query,
			data,
			success: (res) => {
				if (res.data) {
					setCardValue.call(this, this.props, res.data);
					toggleShow.call(this);
					if (isRefresh) {
						showMessage.call(this, this.props, { type: MsgConst.Type.RefreshSuccess });
						/* 国际化处理： 刷新成功*/
					}
					cardCache.updateCache('pk_thaw', pk, res.data, formId, dataSource);
				} else {
					this.props.form.cancel(this.formId);
					cardCache.deleteCacheById('pk_thaw', pk, dataSource);
					setEmptyData.call(this);
					showMessage.call(this, this.props, { type: MsgConst.Type.DataDeleted });
					/* 国际化处理： 数据已删除*/
				}
			},
			error: (res) => {
				if (res && res.message) {
					toast({ content: res.message, color: 'danger' });
				}
			}
		});
	}
}
function CompareDate(d1, d2) {
	return new Date(d1) > new Date(d2);
}
/**
 * 业务校验方法
 */
export function businessValidator() {
	//启封日期
	let bill_date = this.props.form.getFormItemsValue(this.formId, 'bill_date').value;
	//封存日期
	let ft_date = this.props.cardTable.getColValue(this.tableId, 'ft_date', false, false);
	let flag = true;
	let content = '';
	ft_date.map((e, index) => {
		if (e.value && CompareDate(e.value.substring(0, 10), bill_date.substring(0, 10))) {
			flag = false;
			let num = index + 1;
			content +=
				'<div>' +
				getMultiLangByID('452002508A-000004', { num }) +
				'</div>' /* 国际化处理：<div>启封日期不能早于第{num}行封存日期</div> */;
		}
	});
	if (!flag) {
		toast({
			content,
			color: 'danger',
			isNode: true
		});
	}
	return flag;
}

//保存单据
export function saveClick() {
	// 保存前校验
	let pass = beforeSaveValidator.call(this, this.props, this.formId, this.tableId, []);
	if (!pass) {
		return;
	}
	//业务校验方法
	if (!businessValidator.call(this)) {
		return;
	}
	let CardData = this.props.createMasterChildDataSimple(card_pageId, this.formId, this.tableId);
	CardData.bill_type = bill_type;
	let url = card_addsave; //新增保存
	let saveStatus = UISTATE.add;
	if (this.nowUIStatus === UISTATE.edit) {
		saveStatus = this.nowUIStatus;
		url = card_updatesave; //修改保存
	}
	this.props.validateToSave(CardData, () => {
		ajax({
			url: url,
			data: CardData,
			success: (res) => {
				let pk_thaw = null;
				if (res.success) {
					if (res.data) {
						this.props.form.setFormStatus(this.formId, UISTATE.browse);
						setCardValue.call(this, this.props, res.data);
						pk_thaw = res.data.head[this.formId].rows[0].values.pk_thaw.value;
						this.setState({ pk_thaw: pk_thaw });
						// 保存成功后处理缓存
						let cachData = this.props.createMasterChildData(card_pageId, this.formId, this.tableId);
						if (saveStatus == UISTATE.add) {
							cardCache.addCache(pk_thaw, cachData, this.formId, dataSource);
						} else {
							cardCache.updateCache('pk_thaw', pk_thaw, cachData, this.formId, dataSource);
						}
						toggleShow.call(this, UISTATE.browse);
						showMessage.call(this, this.props, { type: MsgConst.Type.SaveSuccess });
						/* 国际化处理： 保存成功*/
					}
				}
			},
			error: (res) => {
				if (res && res.message) {
					toast({ content: res.message, color: 'danger' });
				}
			}
		});
	});
}

//----------------界面设值--------start------
//设置form表单head区域的信息
export function putHeadFormValue(formData) {
	this.props.form.setAllFormValue({
		[this.formId]: formData
	});
}
//设置表体区域的信息
export function putTableValue(tableData) {
	this.props.cardTable.setTableData(this.tableId, tableData);
}

//----------------界面设值--------end------

//提交
export function commitClick(OperatorType, commitType, content) {
	if (commitType === 'saveCommit') {
		// 保存前校验
		let pass = beforeSaveValidator.call(this, this.props, this.formId, this.tableId, []);
		if (!pass) {
			return;
		}
		//业务校验方法
		if (!businessValidator.call(this)) {
			return;
		}
	}
	let pk = this.props.form.getFormItemsValue(this.formId, 'pk_thaw').value;
	let CardData = this.props.createMasterChildDataSimple(card_pageId, this.formId, this.tableId);
	let paramInfoMap = {};
	paramInfoMap[pk] = ''; //卡片页面ts为空也可以 CardData 里有ts
	let obj = {
		dataType: 'cardData',
		OperatorType: OperatorType,
		commitType: commitType,
		pageid: card_pageId,
		paramInfoMap: paramInfoMap,
		content: content
	};
	CardData.userjson = JSON.stringify(obj);
	if (commitType === 'saveCommit') {
		this.props.validateToSave(CardData, () => {
			ajax({
				url: card_commit,
				data: CardData,
				success: (res) => {
					if (res.success) {
						if (content) {
							this.setState({
								compositedisplay: false
							});
						}
						//来源是转单的特殊处理
						if (TRANSFERINFO.srctype == this.props.getUrlParam(TransferConst.srctype)) {
							transferCommitAfter.call(this, this.props, res);
						} else {
							let callback = (pk_thaw, data) => {
								toggleShow.call(this, 'browse');
								this.setState({ pk_thaw: pk_thaw });
							};
							let setValue = (data) => {
								this.props.form.setFormStatus(this.formId, UISTATE.browse);
								setCardValue.call(this, this.props, data);
							};
							getScriptCardReturnData.call(
								this,
								res,
								this.props,
								this.formId,
								this.tableId,
								'pk_thaw',
								dataSource,
								setValue,
								false,
								callback,
								card_pageId
							);
						}
					}
				},
				error: (res) => {
					if (res && res.message) {
						toast({ content: res.message, color: 'danger' });
					}
				}
			});
		});
	} else {
		ajax({
			url: card_commit,
			data: CardData,
			success: (res) => {
				if (res.success) {
					if (content) {
						this.setState({
							compositedisplay: false
						});
					}
					if (TRANSFERINFO.srctype == this.props.getUrlParam(TransferConst.srctype)) {
						transferCommitAfter.call(this, this.props, res);
					} else {
						let callback = () => {
							toggleShow.call(this, 'browse');
						};
						getScriptCardReturnData.call(
							this,
							res,
							this.props,
							this.formId,
							this.tableId,
							'pk_thaw',
							dataSource,
							null,
							false,
							callback,
							card_pageId
						);
					}
				}
			},
			error: (res) => {
				if (res && res.message) {
					toast({ content: res.message, color: 'danger' });
				}
			}
		});
	}
}

/**
 * 转单后的处理
 * @param {} props 
 */
export function transferCommitAfter(props, res) {
	let bodypk = 'pk_thaw_b';
	let callback = (pk_thaw, data) => {
		toggleShow.call(this, UISTATE.transferBorwse);
		this.synTransferData();
		this.setState({ pk_thaw: pk_thaw });

		let pkvalues = props.cardTable.getColValue(this.tableId, 'pk_freeze_b_src'); //表体的上游pk、数组
		pkvalues = pkvalues.map((e) => {
			return e.value;
		});
		//转单上游缓存移除对应的pk
		// cardCache.deleteCacheById(headpk, pk, dataSourceTransfer);
		props.transferTable.setSavedTransferTableDataPk(pkvalues);

		let headpkvalue = props.form.getFormItemsValue(this.formId, 'pk_thaw');
		let index = this.transferEditId.indexOf(headpkvalue.value);
		if (index != -1) {
			this.transferEditId.splice(index, 1);
		}

		props.transferTable.setTransformFormStatus(TRANSFERINFO.leftarea, {
			status: true,
			childId: bodypk,
			onChange: (current, next, currentIndex) => {
				this.transferIndex = currentIndex + 1;
			}
		});
	};
	let setValue = (data) => {
		this.props.form.setFormStatus(this.formId, UISTATE.browse);
		setCardValue.call(this, this.props, data);
	};
	getScriptCardReturnData.call(
		this,
		res,
		this.props,
		this.formId,
		this.tableId,
		'pk_thaw',
		dataSource,
		setValue,
		false,
		callback,
		card_pageId
	);
}

export function setValue(data, status = 'browse') {
	if (this.nowUIStatus) {
		status = this.nowUIStatus;
	}
	if (data && data.head) {
		this.props.form.setAllFormValue({ [this.formId]: data.head[this.formId] });
	}
	if (data && data.body) {
		this.props.cardTable.setTableData(this.tableId, data.body[this.tableId]);
	}
	toggleShow.call(this, status);
}
//删除单据
export function delConfirm() {
	let pkField = 'pk_thaw';
	let pk = this.props.form.getFormItemsValue(this.formId, pkField).value;
	let paramInfoMap = {};
	paramInfoMap[pk] = this.props.form.getFormItemsValue(this.formId, 'ts').value;
	ajax({
		url: list_delete,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: card_pageId
		},
		success: (res) => {
			if (res) {
				getScriptCardReturnData.call(
					this,
					res,
					this.props,
					this.formId,
					this.tableId,
					pkField,
					dataSource,
					setValue,
					true,
					getdata,
					card_pageId
				);
			}
		},
		error: (res) => {
			if (res && res.message) {
				toast({ content: res.message, color: 'danger' });
			}
		}
	});
}
