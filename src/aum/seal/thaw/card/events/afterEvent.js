import { ajax } from 'nc-lightapp-front';
import { pageConfig } from '../const';
const { form_tableId, card_pageId, formId, TRANSFERINFO, bodyAfterEditUrl } = pageConfig;

import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils, cardUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { commonHeadAfterEvent, commonBodyAfterEvent, updateHeadData, updateBodyData } = cardUtils;
const { OrgChangeEvent } = components;

const { orgChangeEvent } = OrgChangeEvent;
export default function afterEvent(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	if (moduleId == formId) {
		headAfterEvent.call(this, props, moduleId, key, value, changedRows, index, record, type, eventType);
	} else if (moduleId == form_tableId) {
		bodyAfterEvent.call(this, props, moduleId, key, value, changedRows, index, record, type, eventType);
	}

	//来源是转单的操作需要同步右边数据到左边
	if (this.srctype == TRANSFERINFO.srctype) {
		this.synTransferData();
	}
}

/**
 * 表头编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} oldValue 
 * @param {*} val 
 */
export function headAfterEvent(props, moduleId, key, value, oldValue, val) {
	if (key === 'pk_org_v') {
		//清空表头和主组织相关字段-和原nc保持一致
		let fields = [ 'pk_recorder' ];
		let callback = () => {
			if (value.value != undefined) {
				props.cardTable.addRow(this.tableId, undefined, undefined, false);
			}
		};
		//组织切换
		orgChangeEvent.call(
			this,
			props,
			card_pageId,
			formId,
			form_tableId,
			key,
			value,
			oldValue,
			fields,
			[ 'AddLine', 'BatchAlter' ],
			callback
		);
	}
}

/**
 * 表体编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedRows 
 * @param {*} index 
 * @param {*} record 
 * @param {*} type 
 * @param {*} eventType 
 */
function bodyAfterEvent(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	let data = props.createBodyAfterEventData(card_pageId, formId, form_tableId, moduleId, key, changedRows, index);
	if (key === 'pk_equip') {
		let pk_equip = props.cardTable.getValByKeyAndIndex(form_tableId, index, key);
		if (pk_equip && pk_equip.value) {
			// 处理多选，带出联动项
			let config = {
				afterEditUrl: bodyAfterEditUrl,
				pagecode: form_tableId,
				key,
				record,
				changedRows,
				index,
				keys: [ 'pk_equip' ]
			};
			commonBodyAfterEvent.call(this, props, config);
			// ajax({
			// 	url: bodyAfterEditUrl,
			// 	data,
			// 	success: (res) => {
			// 		let { success, data } = res;
			// 		if (success) {
			// 			if (res.data) {
			// 				if (res.data.head && res.data.head[formId]) {
			// 					this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
			// 				}
			// 				if (res.data.body && res.data.body[form_tableId]) {
			// 					this.props.cardTable.setTableData(form_tableId, res.data.body[form_tableId]);
			// 				}
			// 			}
			// 		}
			// 	}
			// });
		} else {
			// 清空联动项
			let meta = props.meta.getMeta();
			meta[form_tableId].items.map((item) => {
				if (
					item.attrcode.startsWith(key) ||
					item.attrcode.endsWith('_before') ||
					item.attrcode.endsWith('_after')
				) {
					props.cardTable.setValByKeyAndIndex(form_tableId, index, item.attrcode, {
						value: null,
						display: null
					});
				}
			});
		}
	}
}
