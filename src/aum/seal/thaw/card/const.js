// 页面配置
export const pageConfig = {
	/**
 * 资产启封常量
 */
	dataSource: 'aum.seal.thaw.main',
	cardRouter: '/card',
	listRouter: '/list',
	sourceRouter: '/transfer',
	dataSourceTransfer: 'aum.seal.freezetransfer.source',
	printFilename: '452002508A-000000' /* 国际化处理： 资产启封*/,
	title: '452002508A-000000' /* 国际化处理： 资产启封*/,
	tableId: 'list_head',
	pageId: '452002508A_list',
	appid: '0001Z910000000002SB7', //小应用id
	appcode: '452002508A', //小应用id
	searchId: 'searchArea', //查询区
	oid: '1001Z910000000004FT7', //查询模板id
	bill_type: '4A10',
	transi_type: '4A10-01',
	// form节点编码
	form_tableId: 'bodyvos',
	form_pageId: '452002508A_list',
	card_pageId: '452002508A_card',
	formId: 'card_head',
	pk_group: '0001S210000000000AYW',

	//url
	list_url: '/aum/seal/thaw/list/index.html', //列表url
	card_url: '/aum/seal/thaw/card/index.html', //卡片url

	printListUrl: '/nccloud/aum/thaw/printCard.do', //列表打印url
	printCardUrl: '/nccloud/aum/thaw/printCard.do', //卡片打印url

	bodyAfterEditUrl: '/nccloud/aum/thaw/bodyAfterEdit.do', //表体编辑后事件url

	EditOpr_url: '/nccloud/aum/thaw/thaw_edit.do',
	node_code: '4520024035',

	printFuncode: '452002508A',
	printNodekey: null,

	//操作 action url
	list_delete: '/nccloud/aum/thaw/delete.do',
	list_query: '/nccloud/aum/thaw/query.do',
	list_querybypks: '/nccloud/aum/thaw/querypagegridbypks.do',

	card_query: '/nccloud/aum/thaw/querycard.do',
	card_addsave: '/nccloud/aum/thaw/save.do',
	card_updatesave: '/nccloud/aum/thaw/update.do',
	card_commit: '/nccloud/aum/thaw/commit.do',

	//   card_rollbackBillCode : '/nccloud/aum/common/rollbackBillCode.do',
	card_billCodeGenBack: '/nccloud/ampub/common/billCodeGenBack.do',

	//精度处理
	precision_url: '/nccloud/aum/thaw/precision.do',

	//资产组织切换事件处理
	orgchangeevent_url: '/nccloud/aum/thaw/orgchangeevent.do',
	BatchAlter_url: '/nccloud/aum/thaw/batchAlter.do',

	//转单下游相关信息
	TRANSFERINFO: {
		pagecode: '452002508A_transfercard',
		appid: '0001Z910000000002SB7',
		appcode: '452002508A',
		leftarea: 'leftarea',
		srctype: 'transfer'
	}
};
