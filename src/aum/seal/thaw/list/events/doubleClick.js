import { pageConfig } from '../const';
const { card_url, card_pageId, cardRouter } = pageConfig;

export default function doubleClick(record, index, props) {
	props.pushTo(cardRouter, {
		pagecode: card_pageId,
		status: 'browse',
		id: record.pk_thaw.value
	});
}
