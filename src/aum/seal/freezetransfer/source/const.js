export const pageConfig = {
	//主子结构页面编码
	pageCode: '452002504A_transferlist',

	pageCodeCard: '452002504A_card',
	//小应用主键
	appId: '0001Z910000000002TUX',
	//小应用主键
	appCode: '452002504A',
	//表头列表code
	headCode: 'head',
	//表体列表code
	bodyCode: 'bodyvos',
	//主子拉平页面编码
	mainPageCode: '452002504A_transfermain',
	//主子拉平显示面板code
	mainCode: 'transfermain',
	//查询区域code
	searchCode: 'search',
	//查询区域模板主键
	search_templetid: '1001Z910000000009R39',
	//主表主键字段
	headPkField: 'pk_freeze',
	//子表主键字段
	bodyPkField: 'pk_freeze_b',
	//单据类型
	billType: '4A09',
	//交易类型
	transType: '4A09-01',
	//缓存命名空间
	dataSourceTransfer: 'aum.seal.freezetransfer.source',
	//下游卡片路由
	cardRouter: '/card',
	// 上游卡片页面路由，超链接用
	sourceUrl: '/aum/seal/freeze/main/#/card',
	// 上游卡片页面的pagecode
	sourcePageCode: '452002504A_card',
	srcfreeze_query: '/nccloud/aum/freeze/srcfreeze_query.do'
};
