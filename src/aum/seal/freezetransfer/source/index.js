//主子表列表

import React, { Component } from 'react';
import { createPage, base } from 'nc-lightapp-front';
import { initTemplate, searchBtnClick, initTemplateFull, afterEvent } from './events';
import { pageConfig } from './const';

import ampub from 'ampub';
const { utils } = ampub;
const { transferUtils, multiLangUtils } = utils;
const { queryAppCodeByTransType, pushToTransferCard, addReturnBtn } = transferUtils;
const { initMultiLangByModule, getMultiLangByID } = multiLangUtils;

const { headCode, searchCode, bodyCode, mainCode, dataSourceTransfer, cardRouter, billType } = pageConfig;
const { NCToggleViewBtn, NCAffix } = base;

class FreezeTransfer extends Component {
	constructor(props) {
		super(props);
		this.headTableId = headCode;
		this.state = {
			expand: true
		};
	}
	componentWillMount() {
		initMultiLangByModule({ aum: [ '452002504A' ], ampub: [ 'common' ] }, () => {
			queryAppCodeByTransType.call(this, this.props, billType, initTemplate);
		});
	}

	render() {
		const { transferTable, search } = this.props;
		const { NCCreateSearch } = search;
		const { createTransferTable } = transferTable;
		let selectedShow = transferTable.getSelectedListDisplay(this.headTableId);

		return (
			<div id="transferDemo">
				{!selectedShow ? (
					<div>
						<div className="nc-bill-list">
							<NCAffix>
								<div className="nc-bill-header-area">
									<div className="header-title-search-area">{addReturnBtn.call(this)}</div>
									<div className="header-button-area">
										<NCToggleViewBtn
											expand={this.state.expand}
											onClick={() => {
												if (!this.props.meta.getMeta()[mainCode]) {
													initTemplateFull(this.props); //加载主子拉平模板
												}
												this.props.transferTable.changeViewType(this.headTableId);
												let expandtemp = !this.state.expand;
												this.setState({ expand: expandtemp });
											}}
										/>
									</div>
								</div>
							</NCAffix>
							<div className="nc-bill-search-area">
								{NCCreateSearch(
									searchCode, //模块id
									{
										clickSearchBtn: searchBtnClick.bind(this),
										onAfterEvent: afterEvent.bind(this)
									}
								)}
							</div>
						</div>
					</div>
				) : (
					''
				)}

				<div className="nc-bill-transferTable-area">
					{createTransferTable({
						searchAreaCode: searchCode,
						dataSource: dataSourceTransfer,
						componentInitFinished: () => {
							//缓存数据赋值成功的钩子函数
							//若初始化数据后需要对数据做修改，可以在这里处理
						},
						tableType: 'nest', //选填 表格默认显示的类型 full:主子拉平 nest:主子表
						headTableId: this.headTableId, //表格组件id
						bodyTableId: bodyCode, //子表模板id
						fullTableId: mainCode, //主子拉平模板id
						transferBtnText: getMultiLangByID('452002504A-000000') /* 国际化处理：生成资产启封*/, //转单按钮显示文字
						containerSelector: '#transferDemo', //容器的选择器 必须唯一,用于设置底部已选区域宽度
						selectArea: () => {
							//已选列表自定义区域
							return <span />;
						},
						onTransferBtnClick: (ids) => {
							//点击转单按钮钩子函数
							pushToTransferCard.call(this, cardRouter, billType, dataSourceTransfer);
						},
						onChangeViewClick: () => {
							//点击切换视图钩子函数
							if (!this.props.meta.getMeta()[mainCode]) {
								initTemplateFull.call(this, this.props); //加载主子拉平模板
							}
							this.props.transferTable.changeViewType(this.headTableId);
							this.setState({ expand: !this.state.expand });
						}
					})}
				</div>
			</div>
		);
	}
}

FreezeTransfer = createPage({})(FreezeTransfer);

export default FreezeTransfer;
