//主子表列表

import React, { Component } from 'react';
import { createPage, high, base, createPageIcon } from 'nc-lightapp-front';
import {
	buttonClick,
	initTemplate,
	searchBtnClick,
	pageInfoClick,
	doubleClick,
	rowSelected,
	afterEvent
} from './events';
import { pageConfig } from './const.js';
import { commitAction, setBatchBtnsEnable } from './events/index';
import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { ApprovalTrans, LoginContext } = components;
const { getContext, loginContextKeys } = LoginContext;

const { NCAffix } = base;
const tableId = pageConfig.areas.list.tableId;

class List extends Component {
	constructor(props) {
		super(props);
		this.state = {
			show: false,
			showApprove: false,
			pk_freeze: '',
			compositedisplay: false,
			compositedata: {}
		};
		initTemplate.call(this, props);
	}

	//提交及指派 回调
	getAssginUsedr = (value) => {
		commitAction.call(this, 'SAVE', this.props, value);
	};

	//取消 指派
	turnOff = () => {
		this.props.table.selectAllRows(tableId, false);
		setBatchBtnsEnable.call(this, this.props, tableId);
		this.setState({
			compositedisplay: false
		});
	};

	render() {
		let { table, button, search, ncUploader } = this.props;
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { createButtonApp } = button;
		const { BillTrack, ApproveDetail } = high;
		let { createNCUploader } = ncUploader;

		return (
			<div className="nc-bill-list">
				<NCAffix>
					{/* 头部 header */}
					<div className="nc-bill-header-area">
						{/* 标题 title */}
						<div className="header-title-search-area">
							{createPageIcon()}
							<h2 className="title-search-detail">{getMultiLangByID(pageConfig.appname)}</h2>
						</div>
						{/* 按钮区 btn-area */}
						<div className="header-button-area">
							{createButtonApp({
								area: 'list_head',
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
						</div>
					</div>
				</NCAffix>
				{/* 查询区 search-area */}
				<div className="nc-bill-search-area">
					{NCCreateSearch(pageConfig.areas.list.searchId, {
						clickSearchBtn: searchBtnClick.bind(this),
						onAfterEvent: afterEvent.bind(this),
						dataSource: pageConfig.dataSource,
						componentInitFinished: () => {
							//缓存数据赋值成功的钩子函数
							//若初始化数据后需要对数据做修改，可以在这里处理
						}
					})}
				</div>
				{/* 列表区 table-area */}
				<div className="nc-bill-table-area">
					{createSimpleTable(tableId, {
						handlePageInfoChange: pageInfoClick,
						// tableModelConfirm: tableModelConfirm,
						showIndex: true,
						showCheck: true,
						onRowDoubleClick: doubleClick.bind(this),
						onSelected: rowSelected.bind(this),
						onSelectedAll: rowSelected.bind(this),
						dataSource: pageConfig.dataSource,
						pkname: pageConfig.vo_name.pk_freeze,
						componentInitFinished: () => {
							setBatchBtnsEnable.call(this, this.props, pageConfig.areas.list.tableId);
							//缓存数据赋值成功的钩子函数
							//若初始化数据后需要对数据做修改，可以在这里处理
						}
					})}
				</div>
				{/* 审批详情 */}
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={getContext(loginContextKeys.transtype)}
					billid={this.state.pk_freeze}
				/>
				{/* 单据追溯 */}
				<BillTrack
					show={this.state.show}
					close={() => {
						this.setState({ show: false });
					}}
					pk={this.state.pk_freeze} //单据id
					type={pageConfig.bill_type} //单据类型
				/>
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002')}
						/* 国际化处理： 指派*/
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				) : (
					''
				)}
				{/* 附件管理框 */}
				{createNCUploader('warcontract-uploader', {})}
			</div>
		);
	}
}

const MasterChildListBase = createPage({})(List);

export default MasterChildListBase;
