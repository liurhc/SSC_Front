import { appConfig } from './../const';

// 页面级配置
const pageConfig = {
	...appConfig,
	setValueType: {
		refresh: 'refresh',
		query: 'query'
	},
	btnUsability: {
		Multi: [
			'Delete',
			'True',
			'Commit',
			'UnCommit',
			'Attachment',
			'QueryAboutVoucher',
			'QueryAboutBusiness',
			'QueryAboutBillFlow'
		],
		Nothing: [
			'Delete',
			'Commit',
			'UnCommit',
			'Attachment',
			'QueryAboutVoucher',
			'QueryAboutBusiness',
			'QueryAboutBillFlow',
			'Print',
			'Preview',
			'Output'
		],
		free: [
			'Delete',
			'Commit',
			'Attachment',
			'QueryAboutVoucher',
			'QueryAboutBusiness',
			'QueryAboutBillFlow',
			'Print',
			'Preview',
			'Output'
		],
		Pass: [
			'UnCommit',
			'Attachment',
			'QueryAboutVoucher',
			'QueryAboutBusiness',
			'QueryAboutBillFlow',
			'Print',
			'Preview',
			'Output'
		],
		UnCheck: [
			'UnCommit',
			'Attachment',
			'QueryAboutVoucher',
			'QueryAboutBusiness',
			'QueryAboutBillFlow',
			'Print',
			'Preview',
			'Output'
		],
		Checking: [
			'UnCommit',
			'Attachment',
			'QueryAboutVoucher',
			'QueryAboutBusiness',
			'QueryAboutBillFlow',
			'Print',
			'Preview',
			'Output'
		]
	},
	listButtonVisible: {
		headBtns: {
			Save: true,
			SaveCommit: true,
			Cancel: true,
			Add: true,
			Edit: true,
			Delete: true,
			Commit: true,
			UnCommit: true,
			File: true,
			Print: true,
			Preview: true,
			OutPut: true
		},
		innerBtns: {
			// 自由态_0
			freeAudit: [ 'Commit', 'Edit', 'Delete' ],
			// 已提交(未审核)_1
			unAudit: [ 'UnCommit', 'QueryAboutBillFlow' ],
			// 审核中_2
			auditing: [ 'QueryAboutBillFlow' ],
			// 审核通过_3
			auditPass: [ 'UnCommit', 'QueryAboutBillFlow' ],
			// 审核不通过_4
			auditNoPass: [ 'Edit', 'Delete', 'QueryAboutBillFlow' ]
		}
	}
};

export { pageConfig };
