import { pageConfig } from './../const.js';

export default function doubleClick(record, index, props) {
	this.props.pushTo('/card', {
		pagecode: pageConfig.pagecode.card_pagecode,
		status: pageConfig.ui_status.browse,
		id: record[pageConfig.vo_name.pk_freeze].value
	});
}
