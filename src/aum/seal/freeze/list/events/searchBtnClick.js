import { ajax } from 'nc-lightapp-front';
import { pageConfig } from '../const.js';
import ampub from 'ampub';
const { utils, components } = ampub;
const { listUtils } = utils;
const { setQueryInfoCache, setListValue, listConst } = listUtils;
const { ApprovalTrans, LoginContext } = components;
const { getContext, loginContextKeys } = LoginContext;

import { setBatchBtnsEnable } from './index';

const list_areas = pageConfig.areas.list;

/**
 * 查询按钮点击事件
 * 
 * @param {*} props 
 * @param {*} searchVal 
 */
export default (props, searchVal, type, queryInfo, isRefresh) => {
	if (!searchVal) {
		return;
	}
	if (!queryInfo) {
		queryInfo = props.search.getQueryInfo(list_areas.searchId);
	}
	let pageInfo = props.table.getTablePageInfo(list_areas.tableId);
	queryInfo.pagecode = pageConfig.pagecode.list_pagecode;
	queryInfo.pageInfo = pageInfo;
	queryInfo.billtype = pageConfig.bill_type;
	queryInfo.transtype = getContext(loginContextKeys.transtype);
	// 缓存查询条件
	setQueryInfoCache.call(this, queryInfo, isRefresh, type);
	ajax({
		url: pageConfig.url.queryUrl,
		data: queryInfo,
		success: (res) => {
			setListValue.call(
				this,
				props,
				res,
				list_areas.tableId,
				isRefresh ? pageConfig.setValueType.refresh : pageConfig.setValueType.query
			);
			setBatchBtnsEnable.call(this, props, list_areas.tableId);
		}
	});
};
