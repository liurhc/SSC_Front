import { ajax } from 'nc-lightapp-front';
import { pageConfig } from './../const.js';
import ampub from 'ampub';
const { utils } = ampub;
const { listUtils } = utils;
const { setListValue } = listUtils;

export default function(props, config, pks) {
	let data = {
		allpks: pks,
		pagecode: pageConfig.pagecode.list_pagecode
	};
	//得到数据渲染到页面
	ajax({
		url: pageConfig.url.queryPageUrl,
		data: data,
		success: function(res) {
			setListValue.call(this, props, res);
		}
	});
}
