import { ajax, print, output } from 'nc-lightapp-front';
import { pageConfig } from '../const.js';
import { searchBtnClick } from './index';

import ampub from 'ampub';
const { utils, components } = ampub;
const { ScriptReturnUtils, queryAboutUtils } = components;
const { getScriptListReturn } = ScriptReturnUtils;
const { openListBillTrack } = queryAboutUtils;
const { multiLangUtils, msgUtils, listUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { setBillFlowBtnsEnable, batchRefresh } = listUtils;
const { showConfirm, MsgConst, showMessage } = msgUtils;

const pk_freeze = pageConfig.vo_name.pk_freeze;
const url = pageConfig.url;
const card_pagecode = pageConfig.pagecode.card_pagecode;
const list_areas = pageConfig.areas.list;
const dataSource = pageConfig.dataSource;

export default function(props, id) {
	switch (id) {
		case 'Add':
			addAction.call(this);
			break;
		case 'Refresh':
			batchRefresh.call(this, props, searchBtnClick);
			break;
		case 'Delete':
			delConfirm.call(this, props);
			break;
		case 'Commit':
			commitAction.call(this, 'SAVE', props);
			break;
		case 'UnCommit':
			commitAction.call(this, 'UNSAVE', props);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'QueryAboutBusiness':
			openListBillTrack(this, this.props, list_areas.tableId, [ pk_freeze ]);
			break;
		case 'Attachment':
			attachmentClick.call(this, props);
			break;
		default:
			break;
	}
}

/**
 * 新增
 */
function addAction() {
	this.props.pushTo('/card', {
		pagecode: card_pagecode,
		status: pageConfig.ui_status.add
	});
}

/**
 * 附件上传
 * 
 * @param {*} props 
 */
function attachmentClick(props) {
	let checkedrows = props.table.getCheckedRows(list_areas.tableId);
	// billNo 是单据编码
	let billNo = checkedrows[0].data.values[pageConfig.vo_name.bill_code].value;
	// billNo 是单据主键
	let billId = checkedrows[0].data.values[pk_freeze].value;
	props.ncUploader.show('warcontract-uploader', { billId: 'pam/freeze/' + billId, billNo });
}

/**
 * 打印
 * 
 * @param {*} props 
 */
export function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		/* 国际化处理： 请选择需要打印的数据*/
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		pageConfig.url.printCardUrl, // 后台打印服务url
		printData
	);
}

/**
 * 输出
 * 
 * @param {*} props 
 */
export function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });
		/* 国际化处理： 请选择需要输出的数据*/
		return;
	}
	output({
		url: pageConfig.url.printCardUrl,
		data: printData
	});
}

/**
 * 获取打印数据
 * 
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let checkedRows = props.table.getCheckedRows(list_areas.tableId);
	if (!checkedRows || checkedRows.length == 0) {
		return false;
	}
	let pks = [];
	checkedRows.map((item) => {
		pks.push(item.data.values[pk_freeze].value);
	});
	let printData = {
		filename: getMultiLangByID(pageConfig.appname), // 文件名称
		nodekey: pageConfig.printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

/**
 * 删除提示
 * 
 * @param {*} props 
 */
function delConfirm(props) {
	let checkedRows = props.table.getCheckedRows(list_areas.tableId);
	if (checkedRows.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseDelete });
		/* 国际化处理： 请选择需要删除的数据*/
		return;
	}
	showConfirm.call(this, props, { type: MsgConst.Type.DelSelect, beSureBtnClick: deleteAction });
}

/**
 * 删除
 * 
 * @param {*} props 
 */
function deleteAction(props, data) {
	data = props.table.getCheckedRows(list_areas.tableId);
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values[pk_freeze].value;
		let ts = v.data.values.ts.value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	ajax({
		url: url.deleteUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: card_pagecode
		},
		success: (res) => {
			getScriptListReturn.call(
				this,
				params,
				res,
				props,
				pk_freeze,
				pageConfig.areas.card.card_head,
				list_areas.tableId,
				true
			);
		}
	});
}

/**
 * 提交收回按钮
 * 
 * @param {*} OperatorType 
 * @param {*} props 
 * @param {*} content 
 */
export function commitAction(OperatorType, props, content) {
	let data = props.table.getCheckedRows(list_areas.tableId);
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values[pk_freeze].value;
		let ts = v.data.values.ts.value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	let commitType = '';
	if (OperatorType === 'SAVE') {
		//提交传这个
		commitType = 'commit';
	}
	ajax({
		url: url.commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: OperatorType,
			pageid: card_pagecode,
			commitType: commitType, //提交
			content: content
		},
		success: (res) => {
			if (content) {
				this.setState({
					compositedisplay: false
				});
			}
			getScriptListReturn.call(
				this,
				params,
				res,
				props,
				pk_freeze,
				pageConfig.areas.card.card_head,
				list_areas.tableId,
				false,
				dataSource
			);
		}
	});
}

/**
 * 批量设置按钮是否可用
 * 
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBatchBtnsEnable(props, moduleId) {
	setBillFlowBtnsEnable.call(this, props);
}
