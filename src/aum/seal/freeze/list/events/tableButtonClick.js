import { ajax, toast } from 'nc-lightapp-front';
import { pageConfig } from './../const.js';

import ampub from 'ampub';
const { components, commonConst } = ampub;
const { ScriptReturnUtils, queryAboutUtils } = components;
const { getScriptListReturn } = ScriptReturnUtils;
const { openListApprove } = queryAboutUtils;
const { StatusUtils } = commonConst;
const { UISTATE } = StatusUtils;

const pk_freeze = pageConfig.vo_name.pk_freeze;
const url = pageConfig.url;
const cardAreas = pageConfig.areas.card;
const listAreas = pageConfig.areas.list;
const dataSource = pageConfig.dataSource;

export default function tableButtonClick(props, key, text, record, index) {
	switch (key) {
		// 表格操作按钮
		case 'Edit':
			// 修改
			editAction.call(this, record);
			break;
		case 'Delete':
			// 删除
			delRow.call(this, props, index, record);
			break;
		case 'Commit':
			// 提交
			commitClick.call(this, 'SAVE', props, index, record);
			break;
		case 'UnCommit':
			// 收回
			commitClick.call(this, 'UNSAVE', props, index, record);
			break;
		case 'QueryAboutBillFlow':
			// 审批详情
			openListApprove(this, record, pk_freeze);
			break;
		default:
			break;
	}
}

/**
 * 修改
 * 功能：权限校验
 * 
 * @param {*} record 
 */
function editAction(record) {
	ajax({
		url: url.editUrl,
		data: {
			pk: record[pk_freeze].value,
			resourceCode: pageConfig.node_code
		},
		success: (res) => {
			if (res.data) {
				toast({ color: 'warning', content: res.data });
			} else {
				this.props.pushTo('/card', {
					pagecode: pageConfig.pagecode.card_pagecode,
					status: UISTATE.edit,
					id: record[pk_freeze].value
				});
			}
		}
	});
}

/**
 * 删除
 * 注：默认带pop提示 
 * 
 * @param {*} props 
 * @param {*} index 
 * @param {*} record 
 */
function delRow(props, index, record) {
	let paramInfoMap = {};
	paramInfoMap[record[pk_freeze].value] = record.ts.value;
	let params = [ { id: record[pk_freeze].value, index: index } ];
	ajax({
		url: url.deleteUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: pageConfig.pagecode.card_pagecode
		},
		success: (res) => {
			if (res.success) {
				getScriptListReturn.call(
					this,
					params,
					res,
					props,
					pk_freeze,
					cardAreas.card_head,
					listAreas.tableId,
					true
				);
			}
		}
	});
}

/**
 * 提交 收回 动作
 * 
 * @param {*} OperatorType 
 * @param {*} props 
 * @param {*} index 
 * @param {*} record 
 */
function commitClick(OperatorType, props, index, record) {
	let paramInfoMap = {};
	paramInfoMap[record[pk_freeze].value] = record.ts.value;
	let params = [ { id: record[pk_freeze].value, index: index } ];
	ajax({
		url: url.commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: OperatorType,
			pageid: pageConfig.pagecode.card_pagecode,
			commitType: 'commit'
		},
		success: (res) => {
			getScriptListReturn.call(
				this,
				params,
				res,
				props,
				pk_freeze,
				cardAreas.card_head,
				listAreas.tableId,
				false,
				dataSource
			);
		}
	});
}
