import { pageConfig } from './../const.js';
import tableButtonClick from './tableButtonClick';
import { setBatchBtnsEnable } from './index';

import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { multiLangUtils } = utils;
const { assetOrgMultiRefFilter, refInit, LoginContext } = components;
const { AssetOrgMultiRefFilter } = assetOrgMultiRefFilter;
const { defRefCondition, commonRefCondition } = refInit;
const { CommonKeys } = commonConst;
const { IBusiRoleConst } = CommonKeys;
const { loginContext, getContext, loginContextKeys } = LoginContext;
const { getMultiLangByID } = multiLangUtils;

const vo_name = pageConfig.vo_name;

export default function(props) {
	props.createUIDom(
		{
			pagecode: pageConfig.pagecode.list_pagecode //页面编码
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
				if (data.template) {
					let meta = data.template;
					modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta, () => {});
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button, () => {
						setBatchBtnsEnable.call(this, props, pageConfig.areas.list.tableId);
						props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001') /* 国际化处理： 确定要删除吗？*/);
					});
				}
			}
		}
	);
}

function modifierMeta(props, meta) {
	//分页控件显示标识
	meta[pageConfig.areas.list.tableId].pagination = true;
	// 参照查询条件过滤
	filterRefer.call(this, props, meta);
	// 单据号添加下划线超链接
	addBillCodeLink.call(this, meta);
	// 添加列表操作列
	addOperationColumn.call(this, props, meta);
}

//获取查询条件的值
function getSearchValue(props, field) {
	let data = props.search.getSearchValByField(pageConfig.areas.list.searchId, field);
	if (data && data.value && data.value.firstvalue && 20 == data.value.firstvalue.length) {
		let firstvalue = data.value.firstvalue;
		if (firstvalue.split(',').length == 1) {
			return firstvalue;
		}
	}
}

function filterRefer(props, meta) {
	meta[pageConfig.areas.list.searchId].items.map((item, key) => {
		if (item.attrcode == 'bodyvos.pk_equip.pk_mandept' || item.attrcode == 'bodyvos.pk_equip.pk_manager') {
			AssetOrgMultiRefFilter.call(
				this,
				props,
				pageConfig.areas.list.searchId,
				item,
				'bodyvos.pk_equip.pk_ownerorg'
			);
			// 管理部门、管理人根据货主管理组织过滤
			item.queryCondition = () => {
				let pk_ownerorg = getSearchValue.call(this, props, 'bodyvos.pk_equip.pk_ownerorg');
				if (pk_ownerorg) {
					return { pk_org: pk_ownerorg, isShowUnit: true, busifuncode: IBusiRoleConst.ASSETORG };
				} else {
					return { isShowUnit: true, busifuncode: IBusiRoleConst.ASSETORG };
				}
			};
			if (item.attrcode == 'bodyvos.pk_equip.pk_mandept') {
				// 添加【执行时包含下级】默认勾选
				item.isRunWithChildren = true;
				item.defaultRunWithChildren = true;
			}
		} else if (item.attrcode == 'bodyvos.pk_equip.pk_usedept' || item.attrcode == 'bodyvos.pk_equip.pk_user') {
			AssetOrgMultiRefFilter.call(
				this,
				props,
				pageConfig.areas.list.searchId,
				item,
				'bodyvos.pk_equip.pk_usedunit'
			);
			// 使用部门、使用人根据使用权过滤
			item.queryCondition = () => {
				let pk_usedunit = getSearchValue.call(this, props, 'bodyvos.pk_equip.pk_usedunit');
				if (pk_usedunit) {
					return { pk_org: pk_usedunit, isShowUnit: true, busifuncode: IBusiRoleConst.ASSETORG };
				} else {
					return { isShowUnit: true, busifuncode: IBusiRoleConst.ASSETORG };
				}
			};
			if (item.attrcode == 'bodyvos.pk_equip.pk_usedept') {
				// 添加【执行时包含下级】默认勾选
				item.isRunWithChildren = true;
				item.defaultRunWithChildren = true;
			}
		} else if (item.attrcode == vo_name.pk_org) {
			item.queryCondition = () => {
				return { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter' };
			};
		} else if (item.attrcode == vo_name.pk_recorder) {
			AssetOrgMultiRefFilter.call(this, props, pageConfig.areas.list.searchId, item, vo_name.pk_org);
			//经办人
			item.queryCondition = () => {
				let pk_org = getSearchValue.call(this, props, vo_name.pk_org); // 资产组织
				if (pk_org) {
					return { pk_org: pk_org, busifuncode: IBusiRoleConst.ASSETORG, isShowUnit: true }; // 根据pk_org过滤
				} else {
					return { busifuncode: IBusiRoleConst.ASSETORG, isShowUnit: true }; // 根据pk_org过滤
				}
			};
		} else if (item.attrcode == vo_name.bodyvos_pk_warehouse_in) {
			AssetOrgMultiRefFilter.call(this, props, pageConfig.areas.list.searchId, item, vo_name.bodyvos_pk_icorg);
			// 库存组织
			item.queryCondition = () => {
				let pk_org = getSearchValue.call(this, props, vo_name.bodyvos_pk_icorg);
				if (pk_org) {
					return {
						pk_org: pk_org,
						GridRefActionExt: 'nccloud.web.aum.seal.freeze.refcondition.PK_WAREHOUSE_INSqlBuilder'
					}; // 根据pk_org过滤
				} else {
					return {
						GridRefActionExt: 'nccloud.web.aum.seal.freeze.refcondition.PK_WAREHOUSE_INSqlBuilder'
					}; // 根据pk_org过滤
				}
			};
		} else if (
			item.attrcode == 'bodyvos.pk_equip.pk_mandept' ||
			item.attrcode == 'bodyvos.pk_equip.pk_mandept_v' ||
			item.attrcode == 'bodyvos.pk_equip.pk_usedept' ||
			item.attrcode == 'bodyvos.pk_equip.pk_usedept_v'
		) {
			item.isRunWithChildren = true;
			item.defaultRunWithChildren = true;
		} else if (item.attrcode == 'bodyvos.pk_jobmngfil') {
			AssetOrgMultiRefFilter.call(this, props, pageConfig.areas.list.searchId, item, 'pk_org');
			//（项目）
			item.queryCondition = () => {
				let pk_org = getSearchValue.call(this, props, 'pk_org'); // 资产组织
				if (pk_org) {
					return { pk_org: pk_org, isShowUnit: true }; // 根据pk_org过滤
				} else {
					return { isShowUnit: true }; // 根据pk_org过滤
				}
			};
		} else {
			let bodyDefPrefix = 'bodyvos.def';
			defRefCondition.call(
				this,
				props,
				item,
				pageConfig.areas.list.searchId,
				getContext(loginContextKeys.groupId),
				true,
				vo_name.pk_org,
				bodyDefPrefix
			);
			defRefCondition.call(
				this,
				props,
				item,
				pageConfig.areas.list.searchId,
				getContext(loginContextKeys.groupId),
				true
			);
		}
		commonRefCondition.call(this, props, item);
	});
}

function addOperationColumn(props, meta) {
	meta[pageConfig.areas.list.tableId].items.push({
		attrcode: 'opr',
		label: getMultiLangByID('amcommon-000000') /* 国际化处理： 操作*/,
		width: 200,
		itemtype: 'customer',
		fixed: 'right',
		className: 'table-opr',
		visible: true,
		render: (text, record, index) => {
			let bill_status = record.bill_status.value;
			let buttonAry = [];
			// 自由态
			bill_status == 0 && (buttonAry = pageConfig.listButtonVisible.innerBtns.freeAudit);
			// 已提交(未审核)
			bill_status == 1 && (buttonAry = pageConfig.listButtonVisible.innerBtns.unAudit);
			// 审核中
			bill_status == 2 && (buttonAry = pageConfig.listButtonVisible.innerBtns.auditing);
			// 审核通过
			bill_status == 3 && (buttonAry = pageConfig.listButtonVisible.innerBtns.auditPass);
			// 审核不通过
			bill_status == 4 && (buttonAry = pageConfig.listButtonVisible.innerBtns.auditNoPass);
			return props.button.createOprationButton(buttonAry, {
				area: 'list_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => {
					tableButtonClick.call(this, props, key, text, record, index);
				}
			});
		}
	});
}

function addBillCodeLink(meta) {
	meta[pageConfig.areas.list.tableId].items = meta[pageConfig.areas.list.tableId].items.map((item, key) => {
		item.width = 150;
		//单据号添加下划线超链接
		if (item.attrcode == vo_name.bill_code) {
			item.render = (text, record, index) => {
				return (
					<div
						class="simple-table-td"
						field="bill_code"
						fieldname={getMultiLangByID('452002504A-000003') /* 国际化处理：封存单号 */}
					>
						<span
							className="code-detail-link"
							onClick={() => {
								this.props.pushTo('/card', {
									pagecode: pageConfig.pagecode.card_pagecode,
									status: pageConfig.ui_status.browse,
									id: record[vo_name.pk_freeze].value
								});
							}}
						>
							{record && record[vo_name.bill_code] && record[vo_name.bill_code].value}
						</span>
					</div>
				);
			};
		}
		return item;
	});
}
