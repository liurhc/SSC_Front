import ampub from 'ampub';
const { components } = ampub;
const { assetOrgMultiRefFilter } = components;
const { isMultiCorpRefHandler } = assetOrgMultiRefFilter;
import { pageConfig } from '../const.js';
export default function afterEvent(key, value) {
	if (key == 'bodyvos.pk_icorg') {
		// 根据库存组织字段是否多选控制入库字段是否有业务单元过滤的参照框。
		isMultiCorpRefHandler.call(this, this.props, value, pageConfig.areas.list.searchId, [
			'bodyvos.pk_warehouse_in'
		]);
	}
}
