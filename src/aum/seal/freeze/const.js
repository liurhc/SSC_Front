import ampub from 'ampub';
const { commonConst } = ampub;
const { StatusUtils } = commonConst;
const { UISTATE, BILLSTATUS } = StatusUtils;
// 应用级配置
const appConfig = {
	// 小应用名称
	appname: '452002504A-000001' /* 国际化处理：资产封存 */,
	// 小应用编码
	appcode: '452002504A',
	// 原nc节点编码
	node_code: '4520024030',
	// 单据类型：资产封存
	bill_type: '4A09',
	// 打印模板节点标识
	printNodekey: null,
	// 数据源
	dataSource: 'aum.seal.freeze.main',
	// 界面状态
	ui_status: {
		...UISTATE
	},
	// 单据状态
	bill_status: {
		...BILLSTATUS
	},
	// 页面编码
	pagecode: {
		list_pagecode: '452002504A_list',
		// 卡片页面编码
		card_pagecode: '452002504A_card'
	},
	url: {
		// 新增保存
		addSave: '/nccloud/aum/freeze/freeze_save.do',
		// 列表打印
		printCardUrl: '/nccloud/aum/freeze/printCard.do',
		// 表体编辑后事件
		bodyAfterEditUrl: '/nccloud/aum/freeze/bodyAfterEdit.do',
		// 表头编辑后事件
		headAfterEditUrl: '/nccloud/aum/freeze/headAfterEdit.do',
		// 修改保存
		editSave: '/nccloud/aum/freeze/freeze_update.do',
		// 修改
		edit: '/nccloud/aum/freeze/freeze_edit.do',
		// 删除
		deleteUrl: '/nccloud/aum/freeze/freeze_delete.do',
		// 提交
		commitUrl: '/nccloud/aum/freeze/commit.do',
		// 高级查询
		queryUrl: '/nccloud/aum/freeze/freeze_query.do',
		// 根据pks查询（分页查询）
		queryPageUrl: '/nccloud/aum/freeze/querypagegridbypks.do',
		// 修改(权限校验)
		editUrl: '/nccloud/aum/freeze/freeze_edit.do',
		// 根据pk查询（一个单据）
		queryCardUrl: '/nccloud/aum/freeze/freeze_querycard.do'
	},
	// 页面区域
	areas: {
		list: {
			tableId: 'list_head',
			searchId: 'searchArea'
		},
		card: {
			// 表头信息
			card_head: 'card_head',
			// 详细信息
			bodyvos: 'bodyvos',
			// 侧拉编辑_详细信息
			bodyvos_edit: 'bodyvos_edit',
			// 向下展开_详细信息
			bodyvos_browse: 'bodyvos_browse',
			// 操作信息
			card_tailinfo: 'card_tailinfo'
		}
	},
	// VO中的字段名称
	vo_name: {
		pk_freeze: 'pk_freeze',
		bill_code: 'bill_code',
		pk_org: 'pk_org',
		pk_org_v: 'pk_org_v',
		pk_equip: 'pk_equip',
		pk_warehouse_in: 'pk_warehouse_in',
		equip_name: 'equip_name',
		pk_category: 'pk_category',
		pk_material_v: 'pk_material_v',
		model: 'model',
		spec: 'spec',
		quantity: 'quantity',
		pk_icorg: 'pk_icorg',
		pk_recorder: 'pk_recorder',
		bodyvos_pk_warehouse_in: 'bodyvos.pk_warehouse_in',
		bodyvos_pk_icorg: 'bodyvos.pk_icorg',
		bill_date: 'bill_date',
		ft_date: 'ft_date'
	}
};

export { appConfig };
