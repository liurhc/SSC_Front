//主子表卡片
import React, { Component } from 'react';
import { createPage, base, high } from 'nc-lightapp-front';
import {
	commitAction,
	rowSelected,
	buttonClick,
	getDataByPk,
	didMountSetDefaultValue,
	modelSave,
	initTemplate,
	afterEvent,
	pageInfoClick,
	bodyBeforeEvent,
	setStatus
} from './events';
import { pageConfig } from './const';
import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils, cardUtils, closeBrowserUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { createCardTitleArea, createCardPaginationArea, getCommonTableHeadLeft } = cardUtils;
const { ApprovalTrans, LoginContext } = components;
const { getContext, loginContextKeys } = LoginContext;

const card_pagecode = pageConfig.pagecode.card_pagecode;
const { NCAffix } = base;

class Card extends Component {
	constructor(props) {
		super(props);
		this.state = {
			show: false,
			showApprove: false,
			pk_freeze: props.getUrlParam('id'),
			totalcount: 0,
			compositedisplay: false,
			compositedata: {}
		};
		closeBrowserUtils.call(this, props, {
			form: [ pageConfig.areas.card.card_head ],
			cardTable: [ pageConfig.areas.card.bodyvos ]
		});
		initTemplate.call(this, props);
		this.nowUIStatus = props.getUrlParam('status'); //存储界面当前状态
	}
	componentDidMount() {
		let status = this.nowUIStatus;
		if (status != pageConfig.ui_status.add) {
			let pk = this.props.getUrlParam('id');
			if (pk && pk != 'undefined') {
				getDataByPk.call(this, this.props, pk, () => {
					setStatus.call(this, status);
				});
			}
		} else {
			didMountSetDefaultValue.call(this);
		}
	}

	//提交及指派 回调
	getAssginUsedr = (value) => {
		commitAction.call(this, this.props, 'SAVE', 'commit', value);
	};
	//取消 指派
	turnOff = () => {
		this.setState({
			compositedisplay: false
		});
	};

	//获取列表肩部信息
	getTableHead = () => {
		let { button } = this.props;
		let { createButtonApp } = button;
		return (
			<div className="shoulder-definition-area">
				<div className="definition-icons">
					{createButtonApp({
						area: 'card_body',
						buttonLimit: 3,
						onButtonClick: buttonClick.bind(this),
						popContainer: document.querySelector('.nc-bill-table-area')
					})}

					{this.props.cardTable.createBrowseIcons(pageConfig.areas.card.bodyvos, {
						iconArr: [ 'close', 'open', 'max', 'setCol' ],
						maxDestAreaId: card_pagecode
					})}
				</div>
			</div>
		);
	};

	backToList = () => {
		this.props.pushTo('/list', { pagecode: card_pagecode.replace('card', 'list') });
	};

	//获取列表肩部信息
	getTableHead = () => {
		let { button } = this.props;
		let { createButtonApp } = button;

		return (
			<div className="shoulder-definition-area">
				<div className="definition-icons">
					{createButtonApp({
						area: 'card_body',
						buttonLimit: 3,
						onButtonClick: (props, id) => {
							buttonClick.call(this, props, id, pageConfig.areas.card.bodyvos);
						},
						ignoreHotkeyCode: [ 'DelLine' ], // 忽略快捷键的按钮key code
						popContainer: document.querySelector('.header-button-area')
					})}
				</div>
			</div>
		);
	};

	//获取列表肩部信息
	getTableHeadLeft = () => {
		return getCommonTableHeadLeft.call(this, this.props);
	};

	render() {
		let { cardTable, form, button, ncmodal, cardPagination, ncUploader } = this.props;
		let { createNCUploader } = ncUploader;
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createButtonApp } = button;
		let { createModal } = ncmodal;
		const { BillTrack, ApproveDetail } = high;
		return (
			<div id={card_pagecode}>
				<div className="nc-bill-card">
					<div className="nc-bill-top-area">
						<NCAffix>
							<div className="nc-bill-header-area">
								{/* 标题 title */}
								{createCardTitleArea.call(this, this.props, {
									title: getMultiLangByID(pageConfig.appname),
									formId: pageConfig.areas.card.card_head,
									backBtnClick: this.backToList
								})}
								<div className="header-button-area">
									{createButtonApp({
										area: 'card_head',
										onButtonClick: buttonClick.bind(this)
									})}
								</div>
								{createCardPaginationArea.call(this, this.props, {
									formId: pageConfig.areas.card.card_head,
									dataSource: pageConfig.dataSource,
									pageInfoClick: pageInfoClick.bind(this)
								})}
							</div>
						</NCAffix>
						<div className="nc-bill-form-area">
							{createForm(pageConfig.areas.card.card_head, {
								onAfterEvent: afterEvent.bind(this)
							})}
						</div>
					</div>
					<div className="nc-bill-bottom-area">
						<div className="nc-bill-table-area">
							{createCardTable(pageConfig.areas.card.bodyvos, {
								tableHeadLeft: this.getTableHeadLeft.bind(this),
								tableHead: this.getTableHead.bind(this),
								showIndex: true,
								showCheck: true,
								isAddRow: true,
								modelSave: modelSave.bind(this),
								onAfterEvent: afterEvent.bind(this),
								onBeforeEvent: bodyBeforeEvent.bind(this),
								selectedChange: rowSelected.bind(this)
							})}
						</div>
					</div>
				</div>
				<BillTrack
					show={this.state.show}
					close={() => {
						this.setState({ show: false });
					}}
					pk={this.state.pk_freeze} //单据id
					type={pageConfig.bill_type} //单据类型
				/>
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={getContext(loginContextKeys.transtype)}
					billid={this.state.pk_freeze}
				/>
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002')}
						/* 国际化处理： 指派*/
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				) : (
					''
				)}
				{createNCUploader('warcontract-uploader', {})}
			</div>
		);
	}
}

const MasterChildCardBase = createPage({
	billinfo: {
		// 单据类型
		billtype: pageConfig.billtype,
		// 节点对应的模板编码
		pagecode: card_pagecode,
		// 表头(表单)区域编码
		headcode: pageConfig.areas.card.card_head,
		// 表体(表格)区域编码
		bodycode: pageConfig.areas.card.bodyvos
	}
})(Card);

export default MasterChildCardBase;
