import { pageConfig } from '../const';
import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { multiLangUtils, cardUtils } = utils;
const { queryAboutUtils, refInit, LoginContext } = components;
const { openEquipCardByPk } = queryAboutUtils;
const { defRefCondition } = refInit;
const { afterModifyCardMeta } = cardUtils;
const { getMultiLangByID } = multiLangUtils;
const { loginContext, getContext, loginContextKeys } = LoginContext;
const { CommonKeys } = commonConst;
const { IBusiRoleConst } = CommonKeys;

import { addAction } from './index';

export default function(props) {
	props.createUIDom(
		{
			pagecode: pageConfig.pagecode.card_pagecode //页面编码
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.template) {
					let meta = data.template;
					modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta, () => {
						afterInit.call(this, props);
					});
				}
			}
		}
	);
}

/**
 * 模板初始化以后
 * @param {*} props 
 */
function afterInit(props) {
	let status = props.getUrlParam('status');
	if (status == pageConfig.ui_status.add) {
		addAction.call(this);
	} else if ((status = pageConfig.ui_status.edit)) {
		this.props.form.setFormItemsDisabled(pageConfig.areas.card.card_head, { pk_org_v: true });
	}
}

function modifierMeta(props, meta) {
	let status = props.getUrlParam('status');
	meta[pageConfig.areas.card.card_head].status = status;
	meta[pageConfig.areas.card.bodyvos].status = status;
	// 添加操作列
	addOperationColumn.call(this, props, meta);
	// 参照过滤
	meta = filterRefer.call(this, props, meta);
	return meta;
}

/**
 * 添加卡片界面列表操作列
 * @param {*} props 
 * @param {*} meta 
 */
function addOperationColumn(props, meta) {
	meta[pageConfig.areas.card.bodyvos].items.push({
		attrcode: 'opr',
		label: getMultiLangByID('amcommon-000000') /* 国际化处理： 操作*/,
		visible: true,
		itemtype: 'customer',
		className: 'table-opr',
		width: 150,
		fixed: 'right',
		render(text, record, index) {
			let status = props.cardTable.getStatus(pageConfig.areas.card.bodyvos);
			let buttonAry = status === 'browse' ? [ 'OpenCard' ] : [ 'OpenCard', 'DelLine' ];
			return props.button.createOprationButton(buttonAry, {
				area: 'card_body_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) =>
					tableButtonClick(this, pageConfig.areas.card.bodyvos, props, key, text, record, index, status)
			});
		}
	});
}

function filterRefer(props, meta) {
	// 表头组织过滤
	meta[pageConfig.areas.card.card_head].items.map((item) => {
		// 表头	主组织过滤
		if (item.attrcode == 'pk_org_v') {
			item.queryCondition = () => {
				return { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgVidRefFilter' };
			};
		} else if (item.attrcode == 'pk_org') {
			item.disabled = true;
			item.queryCondition = () => {
				return { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter' };
			};
		} else if (item.attrcode == 'pk_recorder') {
			// 表头 经办人根据组织过滤
			item.queryCondition = () => {
				let pk_org = props.form.getFormItemsValue(pageConfig.areas.card.card_head, 'pk_org').value; // 资产组织
				return { pk_org, busifuncode: IBusiRoleConst.ASSETORG, isShowUnit: true }; // 根据pk_org过滤
			};
		} else {
			defRefCondition.call(
				this,
				props,
				item,
				pageConfig.areas.card.card_head,
				getContext(loginContextKeys.groupId)
			);
		}
	});

	//表体参照过滤
	meta[pageConfig.areas.card.bodyvos].items.map((item) => {
		// 参照根据组织过滤
		if (item.itemtype == 'refer' && item.attrcode != pageConfig.vo_name.pk_equip) {
			item.queryCondition = () => {
				let pk_org = props.form.getFormItemsValue(pageConfig.areas.card.card_head, pageConfig.vo_name.pk_org)
					.value;
				return { pk_org }; // 根据pk_org过滤
			};
		} else if (item.attrcode == pageConfig.vo_name.pk_equip) {
			// 设备根据组织过滤
			// 设备支持多选
			item.isMultiSelectedEnabled = true;
			//给资产编码列添加超链接
			item.renderStatus = pageConfig.ui_status.browse;
			item.render = (text, record, index) => {
				return (
					<div class="card-table-browse">
						<span
							className="code-detail-link"
							onClick={() => {
								openEquipCardByPk.call(this, props, record.values[item.attrcode].value);
							}}
						>
							{record.values[item.attrcode] && record.values[item.attrcode].display}
						</span>
					</div>
				);
			};
		} else {
			defRefCondition.call(
				this,
				props,
				item,
				pageConfig.areas.card.card_head,
				getContext(loginContextKeys.groupId)
			);
		}
	});
	// 卡片模板修改后公共处理
	meta = afterModifyCardMeta.call(this, props, meta);
	return meta;
}
/**
 * 操作列按钮事件处理
 * @param {*} bodyvos 
 * @param {*} props 
 * @param {*} key 
 * @param {*} text 
 * @param {*} record 
 * @param {*} index 
 */
const tableButtonClick = (e, bodyvos, props, key, text, record, index, status) => {
	switch (key) {
		// 表格操作按钮
		case 'OpenCard':
			let status = props.cardTable.getStatus(pageConfig.areas.card.bodyvos);
			if (status == pageConfig.ui_status.edit) {
				props.cardTable.openModel(pageConfig.areas.card.bodyvos, pageConfig.ui_status.edit, record, index);
			} else if (status == pageConfig.ui_status.browse) {
				props.cardTable.toggleRowView(pageConfig.areas.card.bodyvos, record);
			}
			break;
		case 'DelLine':
			props.cardTable.delRowsByIndex(pageConfig.areas.card.bodyvos, index);
			break;
		default:
			break;
	}
};
