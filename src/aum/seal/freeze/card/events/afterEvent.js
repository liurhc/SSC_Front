import { ajax } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import { setValue } from './buttonClick';

import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils, cardUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { commonHeadAfterEvent, commonBodyAfterEvent, updateHeadData, updateBodyData } = cardUtils;
const { OrgChangeEvent } = components;

const { orgChangeEvent } = OrgChangeEvent;

export default function afterEvent(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	// 表头编辑后事件
	if (moduleId == pageConfig.areas.card.card_head) {
		headAfterEvent.call(this, props, moduleId, key, value, changedRows, index, record, type, eventType);
	}
	// 表体编辑后事件
	if (moduleId == pageConfig.areas.card.bodyvos) {
		bodyAfterEvent.call(this, props, moduleId, key, value, changedRows, index, record, type, eventType);
	}
}

/**
 * 表头编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} oldValue 
 * @param {*} val 
 */
export function headAfterEvent(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	//表头编辑后事件取值
	let data = props.createHeadAfterEventData(
		pageConfig.pagecode.card_pagecode,
		pageConfig.areas.card.card_head,
		pageConfig.areas.card.bodyvos,
		moduleId,
		key,
		value
	);
	if (key === 'pk_org_v') {
		/* 
		 * 组织切换后: 组织为空： 不增行;组织不为空：默认增行,不聚焦
		 */
		let callback1 = () => {
			if (value.value != undefined) {
				props.cardTable.addRow(pageConfig.areas.card.bodyvos, undefined, undefined, false);
			}
		};
		let callback = () => {
			if (value.value != undefined) {
				props.cardTable.addRow(pageConfig.areas.card.bodyvos, undefined, undefined, false);
			}
			data = props.createHeadAfterEventData(
				pageConfig.pagecode.card_pagecode,
				pageConfig.areas.card.card_head,
				pageConfig.areas.card.bodyvos,
				moduleId,
				key,
				value
			);
			let config = {
				afterEditUrl: pageConfig.url.headAfterEditUrl,
				pagecode: pageConfig.pagecode.card_pagecode,
				key,
				value,
				oldValue: changedRows,
				keys: [ 'pk_org_v' ],
				callback: null
			};
			commonHeadAfterEvent.call(this, props, config);

			// dealData4AfterEvent.call(this, props, data);
		};
		//组织切换
		orgChangeEvent.call(
			this,
			props,
			pageConfig.pagecode.card_pagecode,
			pageConfig.areas.card.card_head,
			pageConfig.areas.card.bodyvos,
			key,
			value,
			changedRows,
			[ 'pk_recorder' ],
			[ 'AddLine', 'BatchAlter' ],
			callback
		);
	} else {
		// dealData4AfterEvent.call(this, props, data);
		let config = {
			afterEditUrl: pageConfig.url.headAfterEditUrl,
			pagecode: pageConfig.pagecode.card_pagecode,
			key,
			value,
			oldValue: changedRows
		};
		commonHeadAfterEvent.call(this, props, config);
	}
}

/**
 * 表体编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedRows 
 * @param {*} index 
 * @param {*} record 
 * @param {*} type 
 * @param {*} eventType 
 */
function bodyAfterEvent(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	//使用createBodyAfterEventData 初始化为后台平台可以使用的格式
	let data = props.createBodyAfterEventData(
		pageConfig.pagecode.card_pagecode,
		pageConfig.areas.card.card_head,
		pageConfig.areas.card.bodyvos,
		moduleId,
		key,
		changedRows,
		index
	);
	if (key === 'pk_equip') {
		let pk_equip = props.cardTable.getValByKeyAndIndex(pageConfig.areas.card.bodyvos, index, key);
		if (pk_equip && pk_equip.value) {
			// 处理多选，带出联动项
			let config = {
				afterEditUrl: pageConfig.url.bodyAfterEditUrl,
				pagecode: pageConfig.pagecode.card_pagecode,
				key,
				record,
				changedRows,
				index,
				keys: [ 'pk_equip' ]
			};
			commonBodyAfterEvent.call(this, props, config);

			// ajax({
			// 	url: pageConfig.url.bodyAfterEditUrl,
			// 	data,
			// 	async: false,
			// 	success: (res) => {
			// 		let { success, data } = res;
			// 		if (success) {
			// 			setValue.call(this, data);
			// 		}
			// 	}
			// });
		} else {
			// 清空联动项
			let meta = props.meta.getMeta();
			meta[pageConfig.areas.card.bodyvos].items.map((item) => {
				if (
					item.attrcode.startsWith(key) ||
					item.attrcode.endsWith('_before') ||
					item.attrcode.endsWith('_after')
				) {
					props.cardTable.setValByKeyAndIndex(pageConfig.areas.card.bodyvos, index, item.attrcode, {});
				}
			});
		}
	}
	if (key === 'pk_icorg_v') {
		let pk_icorg_v = props.cardTable.getValByKeyAndIndex(pageConfig.areas.card.bodyvos, index, key);
		let config = {
			afterEditUrl: pageConfig.url.bodyAfterEditUrl,
			pagecode: pageConfig.pagecode.card_pagecode,
			key,
			record,
			changedRows,
			index,
			keys: [ 'pk_icorg_v' ]
		};
		commonBodyAfterEvent.call(this, props, config);
		// 处理多选，带出联动项
		// ajax({
		// 	url: pageConfig.url.bodyAfterEditUrl,
		// 	data,
		// 	async: false,
		// 	success: (res) => {
		// 		let { success, data } = res;
		// 		if (success) {
		// 			setValue.call(this, data);
		// 		}
		// 	}
		// });
	}
}

/**
 * 调后台处理编辑后事件的数据 
 * @param {*} props 
 * @param {*} data 
 */
function dealData4AfterEvent(props, data, url = pageConfig.url.headAfterEditUrl) {
	ajax({
		url,
		data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				setValue.call(this, data);
			}
		}
	});
}
