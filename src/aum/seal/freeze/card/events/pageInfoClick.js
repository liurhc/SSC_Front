import { ajax } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import { setValue, toggleShow } from './index';

export default function(props, pk) {
	let _this = this;
	let data = {
		pk: pk,
		pagecode: pageConfig.pagecode.card_pagecode
	};
	ajax({
		url: pageConfig.url.queryCardUrl,
		data: data,
		success: (res) => {
			this.props.setUrlParam({ id: pk });
			setValue.call(this, res.data);
			toggleShow.call(this);
		}
	});
}
