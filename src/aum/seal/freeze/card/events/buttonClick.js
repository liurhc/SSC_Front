import { ajax, toast, print, cardCache, output } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import { headAfterEvent } from './index';

import ampub from 'ampub';
const { utils, components } = ampub;
const { ScriptReturnUtils, LoginContext, queryAboutUtils, saveValidatorsUtil } = components;
const { getScriptCardReturnData } = ScriptReturnUtils;
const { openEquipCard, openBillTrack, openApprove } = queryAboutUtils;
const { beforeSaveValidator } = saveValidatorsUtil;
const { multiLangUtils, cardUtils, msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { setCardValue, setHeadAreaData } = cardUtils;
const { getContext, loginContextKeys } = LoginContext;
const { showConfirm, MsgConst, showMessage } = msgUtils;

const card_head = pageConfig.areas.card.card_head;
const bodyvos = pageConfig.areas.card.bodyvos;
const dataSource = pageConfig.dataSource;

export default function(props, id) {
	switch (id) {
		case 'Add':
			addAction.call(this);
			break;
		case 'Edit':
			editAction.call(this);
			break;
		case 'Delete':
			delConfirm.call(this);
			break;
		case 'Back':
			props.pushTo('/list', { pagecode: pagecode.replace('card', 'list') });
			break;
		case 'Save':
			saveAction.call(this);
			break;
		case 'Cancel':
			cancelConfirm.call(this, props);
			break;
		case 'Refresh':
			refresh.call(this, props);
			break;
		case 'Commit':
			commitAction.call(this, props, 'SAVE', 'Commit');
			break;
		case 'UnCommit':
			commitAction.call(this, props, 'UNSAVE', '');
			break;
		case 'SaveCommit':
			commitAction.call(this, props, 'SAVE', 'saveCommit');
			break;
		case 'QueryAboutBusiness':
			openBillTrack(this);
			break;
		case 'QueryAboutBillFlow':
			openApprove(this);
			break;
		case 'QueryAboutCard':
			openEquipCard(this, this.props, pageConfig.areas.card.bodyvos);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'AddLine':
			props.cardTable.addRow(pageConfig.areas.card.bodyvos);
			break;
		case 'DelLine':
			delLineClick.call(this, props);
			break;
		case 'BatchAlter':
			BatchAlter.call(this, props);
			break;
		case 'Attachment':
			attachmentClick.call(this, props);
			break;
		default:
			break;
	}
}

/**
 * 附件上传
 * @param {*} props 
 */
function attachmentClick(props) {
	let billId = props.form.getFormItemsValue(pageConfig.areas.card.card_head, 'pk_freeze').value;
	props.ncUploader.show('warcontract-uploader', { billId: 'pam/freeze/' + billId });
}

function saveAction() {
	// 保存前校验
	let pass = beforeSaveValidator.call(this, this.props, card_head, bodyvos, []);
	if (!pass) {
		return;
	}
	pass = localBeforeSaveValidator.call(this, pass);
	if (!pass) {
		return;
	}
	let CardData = this.props.createMasterChildDataSimple(pageConfig.pagecode.card_pagecode, card_head, bodyvos);
	let url = pageConfig.url.addSave; //新增保存
	if (this.props.getUrlParam('status') === 'edit') {
		url = pageConfig.url.editSave; //修改保存
	}
	this.props.validateToSave(CardData, () => {
		ajax({
			url: url,
			data: CardData,
			success: (res) => {
				if (res.success) {
					if (res.data) {
						let pk_freeze = res.data.head[card_head].rows[0].values.pk_freeze.value;
						setValue.call(this, res.data);
						let cacheData = this.props.createMasterChildData(
							pageConfig.pagecode.card_pagecode,
							card_head,
							bodyvos
						);
						// 保存成功后处理缓存
						if (this.props.getUrlParam('status') == pageConfig.ui_status.add) {
							cardCache.addCache(pk_freeze, cacheData, card_head, dataSource);
						} else {
							cardCache.updateCache('pk_freeze', pk_freeze, cacheData, card_head, dataSource);
						}
						this.props.setUrlParam({
							status: pageConfig.ui_status.browse,
							id: pk_freeze
						});
						this.setState({ pk_freeze: pk_freeze });
						toggleShow.call(this, pageConfig.ui_status.browse);
						showMessage.call(this, this.props, { type: MsgConst.Type.SaveSuccess });
						/* 国际化处理： 保存成功*/
					}
				}
			}
		});
	});
}

function localBeforeSaveValidator(pass) {
	let bill_date = new Date(
		this.props.form.getFormItemsValue(pageConfig.areas.card.card_head, pageConfig.vo_name.bill_date).value
	);
	let errs = [];
	let bodyvos = this.props.cardTable.getAllRows(pageConfig.areas.card.bodyvos);
	bodyvos.map((item, index) => {
		let ft_date = item.values[pageConfig.vo_name.ft_date].value;
		if (ft_date) {
			ft_date = new Date(ft_date);
			if (ft_date < bill_date) {
				errs.push(index + 1);
			}
		}
	});
	let errMsg = '';
	if (errs && errs.length > 0) {
		errMsg =
			'<div>' +
			getMultiLangByID('452002504A-000002', {
				index: errs.join('、')
			}) +
			'</div>' /* 国际化处理：第{index}行预计启封日期不能早于封存日期 */;
	}
	if (errMsg) {
		toast({ content: errMsg, color: 'danger', isNode: true });
		pass = false;
	}
	return pass;
}
export function commitAction(props, OperatorType, commitType, content) {
	if (commitType === 'saveCommit') {
		// 保存前校验
		let pass = beforeSaveValidator.call(this, this.props, card_head, bodyvos, []);
		if (!pass) {
			return;
		}
	}
	let pk = this.props.getUrlParam('id');
	let CardData = this.props.createMasterChildDataSimple(pageConfig.pagecode.card_pagecode, card_head, bodyvos);
	CardData.bill_type = pageConfig.bill_type;
	let paramInfoMap = {};
	paramInfoMap[pk] = ''; //卡片页面ts为空也可以 CardData 里有ts
	let obj = {
		dataType: 'cardData',
		OperatorType: OperatorType,
		commitType: commitType,
		pageid: pageConfig.pagecode.card_pagecode,
		paramInfoMap: paramInfoMap,
		content: content
	};
	CardData.userjson = JSON.stringify(obj);
	if (content) {
		ajax({
			url: '/nccloud/aum/freeze/commit.do',
			data: CardData,
			success: (res) => {
				if (res.success) {
					if (content) {
						this.setState({
							compositedisplay: false
						});
					}
					getScriptCardReturnData.call(
						this,
						res,
						this.props,
						card_head,
						bodyvos,
						pageConfig.vo_name.pk_freeze,
						dataSource,
						setValue,
						false,
						(pk_freeze, data) => {
							toggleShow.call(this, pageConfig.ui_status.browse);
							this.props.setUrlParam({ id: pk_freeze, status: pageConfig.ui_status.browse });
							this.setState({ pk_freeze: pk_freeze });
						},
						pageConfig.pagecode.card_pagecode
					);
				}
			}
		});
	} else {
		// 保存提交需要走验证公式
		this.props.validateToSave(CardData, () => {
			ajax({
				url: '/nccloud/aum/freeze/commit.do',
				data: CardData,
				success: (res) => {
					if (res.success) {
						if (content) {
							this.setState({
								compositedisplay: false
							});
						}
						getScriptCardReturnData.call(
							this,
							res,
							props,
							card_head,
							bodyvos,
							pageConfig.vo_name.pk_freeze,
							dataSource,
							setValue,
							false,
							(pk_freeze, data) => {
								toggleShow.call(this, pageConfig.ui_status.browse);
								this.props.setUrlParam({ id: pk_freeze, status: pageConfig.ui_status.browse });
								this.setState({ pk_freeze: pk_freeze });
							},
							pageConfig.pagecode.card_pagecode
						);
					}
				}
			});
		});
	}
}
function cancelConfirm(props) {
	let cancelClick = () => {
		// 恢复字段的可编辑性
		let pk_org_v = props.form.getFormItemsValue(pageConfig.areas.card.card_head, 'pk_org_v');
		if (!pk_org_v || !pk_org_v.value) {
			props.resMetaAfterPkorgEdit();
		}
		let pk = cardCache.getCurrentLastId(dataSource);
		this.props.setUrlParam({ status: pageConfig.ui_status.browse });
		if (!pk || pk == 'null') {
			// 清空所有数据
			// setValue.call(this, null);
			setEmptyData.call(this);
			setStatus.call(this, pageConfig.ui_status.browse, pageConfig.btnConfig.visible.browseButtons.noData);
		}
		loadDataByPk.call(this, this.props, pk, (cachData) => {
			if (cachData) {
				let btnConfig = getBtnConfig.call(this, cachData);
				setStatus.call(this, pageConfig.ui_status.browse, btnConfig);
			}
		});
		setStatus.call(this);
	};
	showConfirm.call(this, this.props, { type: MsgConst.Type.Cancel, beSureBtnClick: cancelClick });
}

function cancelAction() {
	// 恢复字段的可编辑性
	let pk_org_v = props.form.getFormItemsValue(pageConfig.areas.card.card_head, 'pk_org_v');
	if (!pk_org_v || !pk_org_v.value) {
		props.resMetaAfterPkorgEdit();
	}
	let pk = cardCache.getCurrentLastId(dataSource);
	this.props.setUrlParam({ status: pageConfig.ui_status.browse });
	loadDataByPk.call(this, this.props, pk, (cachData) => {
		if (cachData) {
			let btnConfig = getBtnConfig.call(this, cachData);
			setStatus.call(this, pageConfig.ui_status.browse, btnConfig);
		} else {
			// 清空所有数据
			setValue.call(this, undefined);
			setStatus.call(this, pageConfig.ui_status.browse, pageConfig.btnConfig.visible.browseButtons.noData);
		}
	});
	setStatus.call(this);
}

function getBtnConfig(data) {
	let bill_status = data.head[pageConfig.areas.card.card_head].rows[0].values.bill_status.value;
	if (bill_status == '0') {
		return pageConfig.btnConfig.visible.browseButtons.free;
	} else if (bill_status == '1') {
		return pageConfig.btnConfig.visible.browseButtons.unAudit;
	} else if (bill_status == '2') {
		return pageConfig.btnConfig.visible.browseButtons.auditing;
	} else if (bill_status == '3') {
		return pageConfig.btnConfig.visible.browseButtons.auditPass;
	}
}

function editAction() {
	let pk = this.props.form.getFormItemsValue(card_head, 'pk_freeze').value;
	ajax({
		url: pageConfig.url.edit,
		data: {
			pk,
			resourceCode: pageConfig.node_code
		},
		success: (res) => {
			if (res.data) {
				toast({ color: 'warning', content: res.data });
			} else {
				setStatus.call(this, pageConfig.ui_status.edit);
				this.props.cardTable.closeExpandedRow(pageConfig.areas.card.bodyvos);
				this.props.setUrlParam({ status: pageConfig.ui_status.edit });
				toggleShow.call(this, pageConfig.ui_status.edit);
				// 设置资产组织不可编辑
				this.props.form.setFormItemsDisabled(card_head, { pk_org_v: true });
			}
		}
	});
}
function deleteAction() {
	let paramInfoMap = {};
	let pk = this.props.getUrlParam('id');
	paramInfoMap[pk] = this.props.form.getFormItemsValue(card_head, 'ts').value;
	ajax({
		url: '/nccloud/aum/freeze/freeze_delete.do',
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: pageConfig.pagecode.card_pagecode
		},
		success: (res) => {
			getScriptCardReturnData.call(
				this,
				res,
				this.props,
				card_head,
				pageConfig.areas.card.bodyvos,
				'pk_freeze',
				dataSource,
				setValue,
				true,
				getdata,
				pageConfig.pagecode.card_pagecode
			);
		}
	});
}
function getdata(pk) {
	loadDataByPk.call(this, this.props, pk, (cachData) => {
		if (cachData) {
			let btnConfig = getBtnConfig.call(this, cachData);
			setStatus.call(this, pageConfig.ui_status.browse, btnConfig);
		} else {
			// 清空所有数据
			setValue.call(this, undefined);
			setStatus.call(this, pageConfig.ui_status.browse, pageConfig.btnConfig.visible.browseButtons.noData);
		}
	});
}

function delConfirm() {
	let pk = this.props.form.getFormItemsValue(card_head, 'pk_freeze').value;
	if (!pk) {
		return;
	}
	showConfirm.call(this, this.props, { type: MsgConst.Type.Delete, beSureBtnClick: deleteAction });
}

/**
 * 通过单据id获取缓存单据信息，没有缓存则重新查询
 * @param {*} props 
 * @param {*} pk 
 */
export function loadDataByPk(props, pk, callBack) {
	if (!pk || pk == 'null') {
		callBack && typeof callBack == 'function' && callBack.call(this);
		return null;
	}
	let cachData = cardCache.getCacheById(pk, dataSource);
	if (cachData) {
		setValue.call(this, cachData);
		callBack && typeof callBack == 'function' && callBack.call(this, cachData);
	} else {
		getDataByPk.call(this, props, pk);
	}
}

/**
* 刷新
* @param {*} props 
*/
export function refresh(props) {
	let pk = props.getUrlParam('id');
	if (!pk) {
		return;
	}
	// 查询到最新单据数据
	getDataByPk.call(this, props, pk, () => {
		showMessage.call(this, this.props, { type: MsgConst.Type.RefreshSuccess });
		/* 国际化处理： 刷新成功*/
	});
}

export function addAction() {
	// 关闭展开行
	this.props.cardTable.closeExpandedRow(bodyvos);
	// 设置新增态
	setStatus.call(this, pageConfig.ui_status.add);
	setTimeout(() => {
		// 清空所有数据
		this.props.form.EmptyAllFormValue(card_head, card_head, false);
		this.props.cardTable.setTableData(bodyvos, { rows: [] });
		// 设置默认值
		setDefaultValue.call(this);
		let status = this.props.getUrlParam('status');
		if (status == pageConfig.ui_status.browse) {
			this.props.setUrlParam({ status: pageConfig.ui_status.add, id: [] });
		}
		// 设置字段的可编辑性
		let pk_org_v = this.props.form.getFormItemsValue(card_head, 'pk_org_v');
		if (pk_org_v && pk_org_v.value) {
			headAfterEvent.call(this, this.props, card_head, 'pk_org_v', pk_org_v, { value: '' }, pk_org_v.value);
		} else {
			this.props.initMetaByPkorg('pk_org_v');
			setBodyBtnsEnable.call(this, this.props, bodyvos);
		}
		this.props.form.setFormItemsDisabled(card_head, {
			pk_org_v: false,
			pk_org: false
		});
	});
}

/**
 * 设置界面状态
 * @param {*} props 
 * @param {*} status 
 */
export function setStatus(status = pageConfig.ui_status.browse, btnConfig) {
	switch (status) {
		case pageConfig.ui_status.add:
			// 设置表头状态
			this.props.form.setFormStatus(card_head, status);
			// 设置表体状态
			this.props.cardTable.setStatus(bodyvos, pageConfig.ui_status.edit);
			// 设置表头按钮可见性
			this.props.button.setButtonVisible(pageConfig.btnConfig.visible.editButtons);
			// 设置表体按钮可用性
			setBodyBtnsEnable.call(this);
			break;
		case pageConfig.ui_status.edit:
			// 设置表头状态
			this.props.form.setFormStatus(card_head, status);
			// 设置表体状态
			this.props.cardTable.setStatus(bodyvos, status);
			// 设置表头按钮可见性
			this.props.button.setButtonVisible(pageConfig.btnConfig.visible.editButtons);
			// 设置表体按钮可用性
			setBodyBtnsEnable.call(this);
			break;
		case pageConfig.ui_status.browse:
			// 设置表头状态
			this.props.form.setFormStatus(card_head, status);
			// 设置表体状态
			this.props.cardTable.setStatus(bodyvos, status);
			// 设置表头按钮可见性
			this.props.button.setButtonVisible(btnConfig);
			// 设置表体按钮可用性
			setBodyBtnsEnable.call(this);
			break;
		default:
			break;
	}
	setHeadAreaData.call(this, this.props, { status });
}

/**
 * 设置表体按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBodyBtnsEnable() {
	let pk_org_v = this.props.form.getFormItemsValue(card_head, 'pk_org_v');
	if (!pk_org_v || !pk_org_v.value) {
		this.props.button.setButtonDisabled([ 'AddLine', 'DelLine', 'BatchAlter' ], true);
	} else {
		this.props.button.setButtonDisabled([ 'AddLine', 'BatchAlter' ], false);
		let checkedRows = [];
		let num = this.props.cardTable.getNumberOfRows(bodyvos, false);
		if (num > 0) {
			checkedRows = this.props.cardTable.getCheckedRows(bodyvos);
		}
		this.props.button.setButtonDisabled([ 'DelLine' ], !(checkedRows && checkedRows.length > 0));
	}
}

/**
 * 新增设置默认值
 * @param {*} props 
 */
export function setDefaultValue() {
	// 模板默认值处理

	// 公共默认值处理
	// 集团
	let groupId = getContext(loginContextKeys.groupId);
	let groupName = getContext(loginContextKeys.groupName);
	// 交易类型
	let transi_type = getContext(loginContextKeys.transtype);
	let pk_transitype = getContext(loginContextKeys.pk_transtype);
	// 默认主组织
	let pk_org = getContext(loginContextKeys.pk_org);
	let org_Name = getContext(loginContextKeys.org_Name);
	let pk_org_v = getContext(loginContextKeys.pk_org_v);
	let org_v_Name = getContext(loginContextKeys.org_v_Name);
	let bill_date = getContext(loginContextKeys.businessDate);
	this.props.form.setFormItemsValue(card_head, {
		pk_group: { value: groupId, display: groupName },
		pk_org: { value: pk_org, display: org_Name },
		pk_org_v: { value: pk_org_v, display: org_v_Name },
		bill_status: { value: '0', display: getMultiLangByID('statusUtils-000000') /* 国际化处理： 自由态*/ },
		bill_type: { value: pageConfig.bill_type },
		transi_type: { value: transi_type },
		pk_transitype: { value: pk_transitype },
		bill_date: { value: bill_date }
	});
}

/**
 * 批改
 * @param {*} props 
 */
function BatchAlter(props) {
	// 获取原始数据
	let changeData = props.cardTable.getTableItemData(pageConfig.areas.card.bodyvos);
	// 如果需要批改的字段需要特殊的处理，则调用后端的批改操作
	let cardData = props.createMasterChildDataSimple(
		pageConfig.pagecode.card_pagecode,
		card_head,
		pageConfig.areas.card.bodyvos
	);
	ajax({
		url: '/nccloud/aum/freeze/batchAlter.do',
		data: {
			card: cardData,
			batchChangeIndex: changeData.batchChangeIndex, // 原始数据行号
			batchChangeKey: changeData.batchChangeKey, // 原始数据key
			batchChangeValue: changeData.batchChangeValue, // 原始数据value
			// 原始数据所在的vo，对应聚合VO中的tableCodes，一主一子可为空，一主多子要传对应的值
			tableCode: ''
		},
		success: (res) => {
			let { success, data } = res;
			if (success) {
				//把数据设置到界面上
				setValue.call(this, data);
			}
		}
	});
}

/**
 * 删行
 * @param {*} props 
 */
function delLineClick(props) {
	//选中的数据
	let checkedRows = props.cardTable.getCheckedRows(pageConfig.areas.card.bodyvos);
	let checkedIndex = [];
	checkedRows.map((item) => {
		checkedIndex.push(item.index);
	});
	props.cardTable.delRowsByIndex(pageConfig.areas.card.bodyvos, checkedIndex);
}

/**
 * 打印
 * @param {*} props 
 */
export function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		/* 国际化处理： 请选择需要打印的数据*/
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		pageConfig.url.printCardUrl, // 后台打印服务url
		printData
	);
}

/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput }); /* 国际化处理： 请选择需要输出的数据*/
		return;
	}
	output({
		url: pageConfig.url.printCardUrl,
		data: printData
	});
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let pk = props.form.getFormItemsValue(card_head, 'pk_freeze');
	if (!pk || !pk.value) {
		return;
	}
	let pks = [ pk.value ];
	let printData = {
		filename: getMultiLangByID(pageConfig.appname), // 文件名称
		nodekey: pageConfig.printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

/**
 * 设置界面值
 * @param {*} props 
 * @param {*} data 
 */
export function setValue(data) {
	setCardValue.call(this, this.props, data);
}

export function setEmptyData() {
	this.props.form.EmptyAllFormValue(card_head, card_head, false);
	this.props.cardTable.setTableData(bodyvos, { rows: [] });
	toggleShow.call(this, 'browse');
	let borwseButtons = [
		'Edit',
		'Back',
		'Delete',
		'Commit',
		'UnCommit',
		'Print',
		'PrintG',
		'Preview',
		'Output',
		'Attachment',
		'QueryAbout',
		'QueryAboutGroup',
		'QueryAboutCard',
		'QueryAboutBusiness',
		'QueryAboutBillFlow',
		'Refresh'
	];
	this.props.button.setButtonVisible(borwseButtons, false);
}
/**
 * 通过单据id查询单据信息
 * @param {*} props 
 * @param {*} pk 
 */
export function getDataByPk(props, pk, callBack) {
	ajax({
		url: '/nccloud/aum/freeze/freeze_querycard.do',
		data: { pk, pagecode: pageConfig.pagecode.card_pagecode },
		success: (res) => {
			if (res.data) {
				setValue.call(this, res.data);
				toggleShow.call(this);
				callBack && typeof callBack == 'function' && callBack.call(this);
			} else {
				cardCache.deleteCacheById('pk_freeze', pk, dataSource);
				props.form.cancel(card_head);
				setEmptyData.call(this);
				let bill_code = '';
				setHeadAreaData.call(this, this.props, { bill_code });
				showMessage.call(this, this.props, { type: MsgConst.Type.DataDeleted });
			}
		}
	});
}

export function toggleShow(status) {
	if (!status) {
		status = this.props.getUrlParam('status');
	}
	if (status != pageConfig.ui_status.add && !this.props.form.getFormItemsValue(card_head, [ 'pk_freeze' ])[0]) {
		this.props.button.setButtonVisible(pageConfig.btnConfig.visible.browseButtons.noData);
		this.nowUIStatus = status;
		return;
	}
	let areaStatus = 'browse';
	let bill_status = null;
	if (this.props.form.getFormItemsValue(card_head, [ 'bill_status' ])[0]) {
		bill_status = this.props.form.getFormItemsValue(card_head, [ 'bill_status' ])[0].value;
	}
	if (status == pageConfig.ui_status.add || status == pageConfig.ui_status.edit) {
		areaStatus = 'edit';
		this.props.button.setButtonVisible(pageConfig.btnConfig.visible.editButtons);
	} else if (status == pageConfig.ui_status.browse) {
		bill_status === '0' && this.props.button.setButtonVisible(pageConfig.btnConfig.visible.browseButtons.free);
		bill_status === '1' && this.props.button.setButtonVisible(pageConfig.btnConfig.visible.browseButtons.unAudit);
		bill_status === '2' && this.props.button.setButtonVisible(pageConfig.btnConfig.visible.browseButtons.auditing);
		bill_status === '3' && this.props.button.setButtonVisible(pageConfig.btnConfig.visible.browseButtons.auditPass);
		bill_status === '4' &&
			this.props.button.setButtonVisible(pageConfig.btnConfig.visible.browseButtons.auditNoPass);
		setStatus.call(this);
	}
	//设置表单和表格的可编辑性
	this.props.form.setFormStatus(card_head, areaStatus);
	this.props.cardTable.setStatus(pageConfig.areas.card.bodyvos, areaStatus);
	this.nowUIStatus = status;
}

export function didMountSetDefaultValue() {
	this.props.form.setFormItemsValue(card_head, {
		bill_status: { value: '0', display: getMultiLangByID('statusUtils-000000') /* 国际化处理： 自由态*/ }
	});
	toggleShow.call(this);
}

// 整单保存
export function modelSave() {
	this.props.cardTable.closeModel(pageConfig.areas.card.bodyvos);
	saveAction.call(this);
}

export function setInterfaceData(data) {
	this.props.form.EmptyAllFormValue(card_head);
	this.props.cardTable.setTableData(pageConfig.areas.card.bodyvos, { rows: [] });
	if (!data) {
		return;
	}

	this.props.form.setAllFormValue({
		[card_head]: data.head[card_head]
	});
	let bill_code = null;
	data.head[card_head].rows[0].values.bill_code && (bill_code = data.head[card_head].rows[0].values.bill_code.value);
	this.setState({ bill_code });

	this.props.cardTable.setTableData(pageConfig.areas.card.bodyvos, data.body[pageConfig.areas.card.bodyvos]);
	let totalcount = this.props.cardTable.getNumberOfRows(pageConfig.areas.card.bodyvos);
	this.setState({ totalcount });
}
