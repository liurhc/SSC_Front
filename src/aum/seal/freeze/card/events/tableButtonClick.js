import { pageConfig } from './../const';

const tableId = pageConfig.areas.card.bodyvos;
const edit_ui_status = pageConfig.ui_status.edit;

export default function tableButtonClick(props, key, text, record, index) {
	switch (key) {
		// 表格操作按钮
		case 'OpenCard':
			let status = props.cardTable.getStatus(tableId);
			if (status == edit_ui_status) {
				props.cardTable.openModel(tableId, edit_ui_status, record, index);
				e.stopPropagation();
			} else if (status == 'browse') {
				props.cardTable.toggleRowView(tableId, record);
			}
			break;
		case 'DelLine':
			props.cardTable.delRowsByIndex(tableId, index);
			props.stopPropagation();
			break;
		default:
			break;
	}
}
