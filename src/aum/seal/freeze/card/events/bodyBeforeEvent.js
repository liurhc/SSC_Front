import { toast } from 'nc-lightapp-front';
import { pageConfig } from '../const';

import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils, cardUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { afterModifyCardMeta } = cardUtils;
const { LoginContext } = components;
const { loginContext, getContext, loginContextKeys } = LoginContext;

export default function bodyBeforeEvent(props, moduleId, key, value, index, record) {
	let editFlag = true;
	refCondtion.call(this, props, moduleId, key, value, index, record);
	editFlag = isEdit.call(this, props, moduleId, key, value, index, record);
	return editFlag;
}

//设备有值，物料编码、设备名称、设备类别、规格、型号、数量不允许编辑
function isEdit(props, moduleId, key, value, index, record) {
	if (key == pageConfig.vo_name.pk_equip) {
		let pk_org = props.form.getFormItemsValue(pageConfig.areas.card.card_head, pageConfig.vo_name.pk_org).value; // 调出组织
		let pk_org_v = props.form.getFormItemsValue(pageConfig.areas.card.card_head, pageConfig.vo_name.pk_org_v).value; // 调出组织
		if (!pk_org) {
			toast({ content: getMultiLangByID('452002504A-000004') /*请先录入资产组织*/, color: 'warning' });
			return false;
		}
		//TODO 有权限组织判断
	} else if (
		key == pageConfig.vo_name.equip_name ||
		key == pageConfig.vo_name.pk_category ||
		key == pageConfig.vo_name.pk_material_v ||
		key == pageConfig.vo_name.model ||
		key == pageConfig.vo_name.spec ||
		key == pageConfig.vo_name.quantity
	) {
		let pk_equip = props.cardTable.getValByKeyAndIndex(moduleId, index, pageConfig.vo_name.pk_equip).value;
		if (pk_equip) {
			return false;
		}
	}
	return true;
}

//表体字段参照处理
function refCondtion(props, moduleId, key, value, index, record) {
	let meta = props.meta.getMeta();
	meta[pageConfig.areas.card.bodyvos].items.map((item) => {
		if (item.attrcode == pageConfig.vo_name.pk_warehouse_in) {
			item.queryCondition = () => {
				let pk_icorg = props.cardTable.getValByKeyAndIndex(
					pageConfig.areas.card.bodyvos,
					index,
					pageConfig.vo_name.pk_icorg
				).value;
				return {
					pk_org: pk_icorg,
					GridRefActionExt: 'nccloud.web.aum.seal.freeze.refcondition.PK_WAREHOUSE_INSqlBuilder'
				};
			};
		} else if (item.attrcode == pageConfig.vo_name.pk_equip) {
			item.queryCondition = () => {
				let pk_org = props.form.getFormItemsValue(pageConfig.areas.card.card_head, pageConfig.vo_name.pk_org)
					.value; // 资产组织
				return {
					pk_org,
					transi_type: getContext(loginContextKeys.transtype),
					bill_type: pageConfig.bill_type,
					GridRefActionExt: 'nccloud.web.aum.seal.freeze.refcondition.PK_EQUIPSqlBuilder'
				};
			};
		}
	});
	meta = afterModifyCardMeta.call(this, props, meta);
	props.meta.setMeta(meta);
}
