//资产领用常量
export const appConfig = {
	node_code: '4520008010', //原NC功能权限资源管理节点注册的资源号，用作权限校验使用
	pkField: 'pk_apply',
	billType: '4A37',
	dataSource: 'aum.assign.appy.main',
	printUrl: '/nccloud/aum/assign/apply_printCard.do',
	printFuncode: '452001004A', // 打印模板应变编码标识
	printNodekey: null, //打印模板nodekey，默认为空
	title: '452001004A-000000' /* 国际化处理： 领用申请*/
};
