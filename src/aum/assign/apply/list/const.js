export {appConfig,filedConst} from '../const';

//列表界面常量
export const pageConfig ={
    pagecode:'452001004A_list',//页面编码
    searchcode:'searchArea',//查询区域编码
    tableId:'list_head',//列表区域编码

    list_head:'list_head',//右上角按钮区域
    list_inner:'list_inner',//列表内按钮区域

    cardRouter:'/card',//卡片跳转路径
    cardFormId:'card_head',//卡片表头区域编码
    cardPageCode : '452001004A_card',//卡片页面编码

    url:{
        apply_delete:'/nccloud/aum/assign/apply_delete.do',
        commitOrUn:'/nccloud/aum/assign/commitOrUn.do',
        apply_query:'/nccloud/aum/assign/apply_query.do',
        querypagegridbypks:'/nccloud/aum/assign/querypagegridbypks.do'
    }
}