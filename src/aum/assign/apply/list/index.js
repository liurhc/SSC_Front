import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, high, createPageIcon } from 'nc-lightapp-front';
import {
	rowSelected,
	buttonClick,
	initTemplate,
	searchBtnClick,
	pageInfoClick,
	tableModelConfirm,
	doubleClick,
	searchAfterEvent
} from './events';
import { commit, setBatchBtnsEnable } from './events/buttonClick';
import { pageConfig } from './const';
import { appConfig } from '../const';
import ampub from 'ampub';

const { components, utils } = ampub;
const { getMultiLangByID } = utils.multiLangUtils;
const { ApprovalTrans } = components;

const { NCAffix } = base;
let { dataSource, pkField, printUrl, title } = appConfig;

const { ApproveDetail } = high; //审批详情
class List extends Component {
	constructor(props) {
		super(props);
		this.searchcode = pageConfig.searchcode;
		this.tableId = pageConfig.tableId;
		this.state = {
			showApprove: false,
			transtype: '4A37-01', //默认值，实际取列表界面上数据信息，用于联查审批详情
			pk_bill: '',
			compositedisplay: false,
			compositedata: {}
		};
		initTemplate.call(this, props);
	}
	componentDidMount() {}

	//提交及指派 回调
	getAssginUsedr = (value) => {
		commit.call(this, 'SAVE', this.props, value);
	};
	//取消 指派
	turnOff = () => {
		this.props.table.selectAllRows(this.tableId, false);
		this.setState({
			compositedisplay: false
		});
	};

	render() {
		let { table, button, search, ncUploader, ncmodal } = this.props;
		const { createModal } = ncmodal;
		let { createSimpleTable } = table;
		let { createNCUploader } = ncUploader;
		let { NCCreateSearch } = search;
		let { createButton } = button;

		return (
			<div className="nc-bill-list">
				<NCAffix>
					<div className="nc-bill-header-area">
						<div className="header-title-search-area">
							{createPageIcon()}
							<h2 className="title-search-detail">{getMultiLangByID(title)}</h2>
						</div>
						<div className="header-button-area">
							{this.props.button.createButtonApp({
								area: pageConfig.list_head,
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
						</div>
					</div>
				</NCAffix>
				<div className="nc-bill-search-area">
					{NCCreateSearch(this.searchcode, {
						clickSearchBtn: searchBtnClick.bind(this),
						onAfterEvent: searchAfterEvent.bind(this)
					})}
				</div>

				<div className="nc-bill-table-area">
					{createSimpleTable(this.tableId, {
						handlePageInfoChange: pageInfoClick,
						showIndex: true,
						showCheck: true,
						onRowDoubleClick: doubleClick.bind(this),
						onSelected: rowSelected.bind(this),
						onSelectedAll: rowSelected.bind(this),
						dataSource: dataSource,
						pkname: pkField,
						componentInitFinished: () => {
							//缓存数据赋值成功的钩子函数
							setBatchBtnsEnable(this.props, this.tableId);
						}
					})}
				</div>
				{/* 审批详情 */}
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transtype}
					billid={this.state.pk_bill}
				/>
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002')}
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				) : (
					''
				)}
				{createNCUploader('assign-uploader', {})}
			</div>
		);
	}
}

List = createPage({})(List);
export default List;
