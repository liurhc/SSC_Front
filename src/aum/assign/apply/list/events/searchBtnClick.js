import { ajax } from 'nc-lightapp-front';
import { appConfig } from '../../const';
import { pageConfig } from '../const';
import { setBatchBtnsEnable } from './buttonClick';
import ampub from 'ampub';

const { components, utils } = ampub;
const { getContext, loginContextKeys } = components.LoginContext;
const { setQueryInfoCache, setListValue, listConst } = utils.listUtils;

let { billType } = appConfig;
let { apply_query } = pageConfig.url;

//点击查询，获取查询区数据
export default function(props, data, type, queryInfo, isRefresh) {
	if (!data) {
		return;
	}
	let transtype = getContext(loginContextKeys.transtype);
	if (!queryInfo) {
		queryInfo = props.search.getQueryInfo(pageConfig.searchcode);
	}
	let pageInfo = props.table.getTablePageInfo(pageConfig.tableId);
	if (queryInfo) {
		queryInfo.pagecode = pageConfig.pagecode;
		queryInfo.pageInfo = pageInfo;
		queryInfo.billtype = billType;
		queryInfo.transtype = transtype;
	}
	// 缓存查询条件
	setQueryInfoCache.call(this, queryInfo, isRefresh, type);
	props.button.setButtonDisabled([ 'Refresh' ], false);
	ajax({
		url: apply_query,
		data: queryInfo,
		success: (res) => {
			setListValue.call(
				this,
				props,
				res,
				pageConfig.tableId,
				isRefresh ? listConst.SETVALUETYPE.refresh : listConst.SETVALUETYPE.query
			);
			setBatchBtnsEnable.call(this, props, pageConfig.tableId);
		}
	});
}
