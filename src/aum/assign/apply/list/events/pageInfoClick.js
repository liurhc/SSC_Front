import { ajax } from 'nc-lightapp-front';
import {pageConfig} from '../const';

const {pagecode,tableId,searchcode} = pageConfig;
const {querypagegridbypks} = pageConfig.url;

export default function(props, config, pks) {
	let pageInfo = props.table.getTablePageInfo(tableId);
    let searchVal = props.search.getAllSearchData(searchcode);
    let data = {
        "allpks": pks,
        "pagecode": pagecode
    };
	//得到数据渲染到页面
	ajax({
		url: querypagegridbypks,
		data: data,
		success: function(res) {
			let { success, data } = res;
			if (success) {
				if (data) {
					props.table.setAllTableData(tableId, data[tableId]);
				} else {
					props.table.setAllTableData(tableId, { rows: [] });
				}
			}
		}
	});
}
