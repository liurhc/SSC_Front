import { pageConfig } from '../const';
import ampub from 'ampub';

const { components } = ampub;
const { isMultiCorpRefHandler } = components.assetOrgMultiRefFilter;

const { searchcode } = pageConfig;

export default function(key, value) {
	if (key === 'pk_org') {
		isMultiCorpRefHandler.call(this, this.props, value, searchcode, [
			'pk_material',
			'pk_jobmngfil',
			'pk_usedept',
			'pk_user',
			'pk_applier'
		]);
	}
}
