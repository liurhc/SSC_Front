import { tableButtonClick } from './';
import { pageConfig } from '../const';
import { linkToCard, setBatchBtnsEnable } from './buttonClick';
import ampub from 'ampub';

const { components, commonConst, utils } = ampub;
const { defRefCondition, commonRefCondition } = components.refInit;
const { loginContext, getContext, loginContextKeys } = components.LoginContext;
const { IBusiRoleConst } = commonConst.CommonKeys;
const { BILLSTATUS } = commonConst.StatusUtils;
const { getMultiLangByID } = utils.multiLangUtils;
const { listConst } = utils.listUtils;
const { AssetOrgMultiRefFilter } = components.assetOrgMultiRefFilter;

let { searchcode, tableId, pagecode } = pageConfig;

export default function(props) {
	props.createUIDom(
		{
			pagecode: pagecode //页面编码
		},
		(data) => {
			if (data) {
				if (data.context) {
					//初始化上下文变量
					loginContext.call(this, data.context);
				}
				if (data.button) {
					props.button.setButtons(data.button);
					// 行删除时悬浮框提示
					props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001') /* 国际化处理： 确定要删除吗？*/);
				}
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta, () => {
						setBatchBtnsEnable.call(this, props, tableId);
						let isQueryed = getContext(listConst.QUERYINFOKEY.isQueryed, listConst.QUERYINFOKEY.dataSource);
						if (!isQueryed) {
							props.button.setButtonDisabled([ 'Refresh' ], true);
						}
					});
				}
			}
		}
	);
}
function modifierMeta(props, meta) {
	meta[tableId].items = meta[tableId].items.map((item, key) => {
		if (item.attrcode == 'bill_code') {
			item.render = (text, record, index) => {
				return (
					<span
						className="code-detail-link"
						onClick={() => {
							linkToCard.call(this, props, record);
						}}
					>
						{record.bill_code && record.bill_code.value}
					</span>
				);
			};
		}
		return item;
	});
	let label = getMultiLangByID('amcommon-000000'); /*操作*/
	//添加操作列
	meta[tableId].items.push({
		attrcode: 'opr',
		label,
		width: 200,
		className: 'table-opr',
		fixed: 'right',
		visible: true,
		render: (text, record, index) => {
			let buttonAry = [];
			if (record.bill_status) {
				//有单据状态值时
				//取得单据状态字段
				let bill_status = record.bill_status.value;
				buttonAry = getBtnAry(bill_status);
			}
			return props.button.createOprationButton(buttonAry, {
				area: pageConfig.list_inner,
				buttonLimit: 3,
				onButtonClick: (props, key) => {
					tableButtonClick.call(this, props, key, text, record, index);
				}
			});
		}
	});
	meta = refcondition.call(this, meta, props);
	return meta;
}
//查询区参照过滤
function refcondition(meta, props) {
	let pk_group = getContext(loginContextKeys.groupId);
	//查询区参照过滤
	meta[searchcode].items.map((item) => {
		if (item.attrcode == 'pk_org') {
			item.queryCondition = () => {
				return { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter' };
			};
		}
		if (item.attrcode == 'pk_unit_used' || item.attrcode == 'bodyvos.pk_category') {
			//（设备类别）
			item.queryCondition = () => {
				return { pk_group }; // 根据pk_group过滤
			};
		} else if (item.attrcode == 'pk_material' || item.attrcode == 'pk_jobmngfil') {
			//组织多选参照过滤
			AssetOrgMultiRefFilter.call(this, props, searchcode, item);
			//（项目、物料）
			item.queryCondition = () => {
				let pk_org = getSearchValueOrg.call(this, props, 'pk_org'); // 主组织
				return { pk_org: pk_org }; // 根据主组织过滤
			};
		} else if (item.attrcode == 'pk_usedept' || item.attrcode == 'pk_user') {
			//组织多选参照过滤
			AssetOrgMultiRefFilter.call(this, props, searchcode, item);
			//使用部门 使用人(领用部门 领用人)
			item.queryCondition = () => {
				let pk_org = getSearchValueOrg.call(this, props, 'pk_org'); //主组织
				if (pk_org) {
					return { pk_org: pk_org, isShowUnit: false, busifuncode: IBusiRoleConst.ASSETORG }; // 有pk_org过滤
				} else {
					return { isShowUnit: true, busifuncode: IBusiRoleConst.ASSETORG }; // 多选时无pk_org过滤
				}
			};
		} else if (item.attrcode == 'pk_applier') {
			//组织多选参照过滤
			AssetOrgMultiRefFilter.call(this, props, searchcode, item);
			//申请人
			item.queryCondition = () => {
				let pk_org = getSearchValueOrg.call(this, props, 'pk_org'); //主组织
				if (pk_org) {
					return { pk_org: pk_org, isShowUnit: false, busifuncode: IBusiRoleConst.ASSETORG }; // 根据pk_org过滤
				} else {
					return { isShowUnit: true, busifuncode: IBusiRoleConst.ASSETORG }; // 根据pk_org过滤
				}
			};
		} else {
			let bodyDefPrefix = 'bodyvos.def';
			defRefCondition.call(this, props, item, searchcode, pk_group, true, 'pk_org', bodyDefPrefix);
			defRefCondition.call(this, props, item, searchcode, pk_group, true);
		}
		// 添加【执行时包含下级】默认勾选
		commonRefCondition.call(this, props, item);
	});
	return meta;
}
/**
 * 根据状态获得操作列显示哪些按钮
 * @param {*} bill_status 
 */
function getBtnAry(bill_status) {
	let buttonAry = [];
	if (BILLSTATUS.free_check == bill_status) {
		//自由态
		buttonAry = [ 'Commit', 'Edit', 'Delete', 'Copy' ];
	} else if (BILLSTATUS.check_nopass == bill_status) {
		//审批不通过
		buttonAry = [ 'Edit', 'Delete', 'Copy', 'QueryAboutBillFlow' ];
	} else if (BILLSTATUS.un_check == bill_status) {
		//提交态
		buttonAry = [ 'UnCommit', 'Copy', 'QueryAboutBillFlow' ];
	} else if (BILLSTATUS.check_pass == bill_status) {
		//审批通过态
		buttonAry = [ 'UnCommit', 'Copy', 'QueryAboutBillFlow' ];
	} else if (BILLSTATUS.check_going == bill_status) {
		//审核中
		buttonAry = [ 'Copy', 'QueryAboutBillFlow' ];
	} else if (BILLSTATUS.close == bill_status) {
		//关闭
		buttonAry = [ 'Copy', 'QueryAboutBillFlow' ];
	}
	return buttonAry;
}
//获取查询条件的值
function getSearchValueOrg(props, field) {
	let data = props.search.getSearchValByField(searchcode, field);
	if (data && data.value && data.value.firstvalue && 20 == data.value.firstvalue.length) {
		return data.value.firstvalue;
	}
}
