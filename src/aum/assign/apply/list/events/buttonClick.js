import { ajax, print } from 'nc-lightapp-front';
import searchBtnClick from './searchBtnClick';
import { pageConfig } from '../const';
import { appConfig } from '../../const';
import ampub from 'ampub';

const { components, commonConst, utils } = ampub;
const { getScriptListReturn } = components.ScriptReturnUtils;
const { UISTATE } = commonConst.StatusUtils;
const { showConfirm, MsgConst, showMessage } = utils.msgUtils;
const { batchRefresh } = utils.listUtils;

let { tableId, searchcode, pagecode, cardRouter, cardPageCode, cardFormId } = pageConfig;
let { apply_delete, commitOrUn, apply_query } = pageConfig.url;
let { pkField, printUrl, dataSource } = appConfig;

const batchBtns = [ 'Delete', 'Copy', 'Commit', 'UnCommit', 'Attachment', 'QueryAbout', 'Print', 'Preview', 'Output' ];

export default function buttonClick(props, id) {
	switch (id) {
		case 'Add':
			linkToCard.call(this, props, {}, UISTATE.add);
			break;
		case 'Refresh':
			refreshAction(props);
			break;
		case 'Delete':
			delConfirm.call(this, props);
			break;
		case 'Copy':
			copy.call(this, props);
			break;
		case 'Commit':
			commit.call(this, 'SAVE', props);
			break;
		case 'UnCommit':
			commit.call(this, 'UNSAVE', props);
			break;
		case 'Attachment':
			attachmentClick.call(this, props);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		default:
			break;
	}
}

//附件
function attachmentClick(props) {
	let checkedrows = props.table.getCheckedRows(tableId);
	// billId 是单据主键
	let billId = checkedrows[0].data.values['pk_apply'].value;
	// billId 是单据号
	let billNo = checkedrows[0].data.values['bill_code'].value;
	props.ncUploader.show('assign-uploader', {
		billId: 'pam/assign_apply/' + billId,
		billNo
	});
}

/**
 * 打印
 * @param {*} props 
 */
export function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint }); /* 国际化多语处理：选择需要打印的数据 */
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		printUrl, // 后台打印服务url
		printData
	);
}

/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput }); /* 国际化多语处理：选择需要输出的数据 */
		return;
	}
	output({
		url: printUrl,
		data: printData
	});
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let { printFuncode, printNodekey } = appConfig;
	let checkedRows = props.table.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length == 0) {
		return false;
	}
	let pks = [];
	checkedRows.map((item) => {
		pks.push(item.data.values['pk_apply'].value);
	});
	let printData = {
		filename: pagecode, // 文件名称
		nodekey: printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

//提交、收回
export function commit(OperatorType, props, content) {
	let data = props.table.getCheckedRows(tableId);
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values.pk_apply.value;
		let ts = v.data.values.ts.value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	let commitType = '';
	if (OperatorType === 'SAVE') {
		//提交传这个
		commitType = 'commit';
	}
	ajax({
		url: commitOrUn,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: OperatorType,
			pageid: cardPageCode,
			commitType: commitType, //提交
			content: content
		},
		success: (res) => {
			if (content) {
				this.setState({
					compositedisplay: false
				});
			}
			getScriptListReturn.call(
				this,
				params,
				res,
				props,
				'pk_apply',
				cardFormId,
				pageConfig.tableId,
				false,
				dataSource
			);
		}
	});
}
//列表复制
function copy(props) {
	let datas = props.table.getCheckedRows(tableId);
	let pk_apply;
	if (datas && datas.length > 0) {
		pk_apply = datas[0].data.values.pk_apply.value;
	}
	if (pk_apply) {
		linkToCard.call(this, props, { [pkField]: { value: pk_apply } }, UISTATE.copyAdd);
	} else {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseCopy }); /* 国际化多语处理：请选择需要复制的数据 */
	}
}
/**
 * 批量删除
 * @param {*} props 
 */
export function delConfirm(props) {
	showConfirm.call(this, props, {
		type: MsgConst.Type.DelSelect,
		beSureBtnClick: deleteAction
	});
}

function deleteAction(props) {
	let data = props.table.getCheckedRows(tableId);
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values.pk_apply.value;
		let ts = v.data.values.ts.value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	ajax({
		url: apply_delete,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: cardPageCode
		},
		success: (res) => {
			getScriptListReturn.call(this, params, res, props, 'pk_apply', cardFormId, tableId, true, dataSource);
		}
	});
}

const refreshAction = (props) => {
	batchRefresh.call(this, props, searchBtnClick);
};

/**
 * 跳转卡片
 * @param {*} props 
 * @param {*} record 
 */
export function linkToCard(props, record = {}, status = 'browse') {
	props.pushTo(cardRouter, {
		pagecode: cardPageCode,
		status,
		id: record[pkField] ? record[pkField].value : ''
	});
}
/**
 * 设置批量按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBatchBtnsEnable(props, moduleId) {
	let checkedRows = props.table.getCheckedRows(moduleId);
	props.button.setButtonDisabled(batchBtns, !(checkedRows && checkedRows.length > 0));
}
