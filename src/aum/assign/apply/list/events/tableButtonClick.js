import { ajax, toast } from 'nc-lightapp-front';
import { pageConfig, appConfig } from '../const';
import { linkToCard } from './buttonClick';
import ampub from 'ampub';

const { components, commonConst } = ampub;
const { UISTATE } = commonConst.StatusUtils;
const { getScriptListReturn } = components.ScriptReturnUtils;

let { cardPageCode, cardFormId } = pageConfig;
let { node_code, dataSource } = appConfig;
//列表行操作
export default function tableButtonClick(props, key, text, record, index) {
	switch (key) {
		case 'Edit':
			ajax({
				url: '/nccloud/aum/assign/apply_edit.do',
				data: {
					pk: record.pk_apply.value,
					resourceCode: node_code
				},
				success: (res) => {
					if (res.data) {
						toast({ color: 'warning', content: res.data });
					} else {
						linkToCard.call(this, props, record, UISTATE.edit);
					}
				}
			});

			break;
		case 'Delete':
			let paramInfoMap = {};
			paramInfoMap[record.pk_apply.value] = record.ts.value;
			let params = [ { id: record.pk_apply.value, index: index } ];
			ajax({
				url: '/nccloud/aum/assign/apply_delete.do',
				data: {
					paramInfoMap: paramInfoMap,
					dataType: 'listData',
					OperatorType: 'DELETE',
					pageid: cardPageCode
				},
				success: (res) => {
					getScriptListReturn(
						params,
						res,
						props,
						'pk_apply',
						cardFormId,
						pageConfig.tableId,
						true,
						dataSource
					);
				}
			});
			break;
		case 'Copy':
			copy.call(this, props, key, text, record, index);
			break;
		case 'Commit':
			commit.call(this, 'SAVE', props, key, text, record, index);
			break;
		case 'UnCommit':
			commit.call(this, 'UNSAVE', props, key, text, record, index);
			break;
		case 'QueryAboutBillFlow':
			queryAboutBillFlow.call(this, props, key, text, record, index);
			break;
		default:
			break;
	}
}
//审批详情
function queryAboutBillFlow(props, key, text, record, index) {
	//单据主键
	let pk_apply = record.pk_apply.value;
	let transtype = record.transi_type.value;
	this.setState({
		showApprove: true,
		pk_bill: pk_apply,
		transtype
	});
}
//列表行复制
function copy(props, key, text, record, index) {
	let pk_apply = record.pk_apply.value;
	if (pk_apply) {
		linkToCard.call(this, props, record, UISTATE.copyAdd);
	}
}
//提交、收回
function commit(OperatorType, props, key, text, record, index) {
	let paramInfoMap = {};
	paramInfoMap[record.pk_apply.value] = record.ts.value;
	let params = [ { id: record.pk_apply.value, index: index } ];
	let commitType = '';
	if (OperatorType === 'SAVE') {
		//提交传这个
		commitType = 'commit';
	}
	ajax({
		url: '/nccloud/aum/assign/commitOrUn.do',
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: OperatorType,
			pageid: cardPageCode,
			commitType: commitType //提交
		},
		success: (res) => {
			getScriptListReturn.call(
				this,
				params,
				res,
				props,
				'pk_apply',
				cardFormId,
				pageConfig.tableId,
				false,
				dataSource
			);
		}
	});
}
