import { base, ajax, cardCache } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import { appConfig } from '../../const';

import ampub from 'ampub';

const { components, commonConst, utils } = ampub;
const { defRefCondition } = components.refInit;
const { openEquipCardByPk } = components.queryAboutUtils;
const { loginContext, getContext, loginContextKeys } = components.LoginContext;
const { IBusiRoleConst } = commonConst.CommonKeys;
const { BILLSTATUS } = commonConst.StatusUtils;
const { getMultiLangByID } = utils.multiLangUtils;
const { afterModifyCardMeta } = utils.cardUtils;

let { updateCache } = cardCache;
let { billType, pkField, dataSource } = appConfig;

const { formId, pagecode, tableId } = pageConfig;
const { openUrl, closeUrl } = pageConfig.url;
export default function(props) {
	props.createUIDom(
		{
			pagecode: pagecode //页面id
		},
		(data) => {
			if (data) {
				if (data.context) {
					//初始化上下文变量
					loginContext.call(this, data.context);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.template) {
					let meta = data.template;
					modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta, () => {
						this.init();
					});
				}
			}
		}
	);
}

function tableButtonClick(props, key, text, record, index) {
	let status = props.getUrlParam('status');
	switch (key) {
		case 'OpenCard':
			let status = props.cardTable.getStatus(tableId);
			if (status == 'edit') {
				props.cardTable.openModel(tableId, 'edit', record, index);
			} else if (status == 'browse') {
				props.cardTable.toggleRowView(tableId, record);
			}
			break;
		case 'DelLine':
			props.cardTable.delRowsByIndex(tableId, index);
			let num = props.cardTable.getNumberOfRows(tableId);
			let checkedRows = [];
			if (num > 0) {
				checkedRows = props.cardTable.getCheckedRows(tableId);
			}
			props.button.setButtonDisabled([ 'DelLine' ], !(checkedRows && checkedRows.length > 0));
			break;
		case 'OpenLine':
			openLine.call(this, props, record, index, tableId);
			break;
		case 'CloseLine':
			closeLine.call(this, props, record, index, tableId);
			break;
		default:
			break;
	}
}
//表头表体区域参照条件过滤
function refcondition(meta, props) {
	const transi_type = getContext(loginContextKeys.transtype);
	const pk_group = getContext(loginContextKeys.groupId);
	const bill_type = billType;

	//表头区域参照过滤
	meta[formId].items.map((item) => {
		if (item.attrcode == 'pk_org_v') {
			item.queryCondition = () => {
				return { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgVidRefFilter' };
			};
		} else if (item.attrcode == 'pk_unit_used_v') {
			//使用权
			item.queryCondition = () => {
				let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
				return {
					pk_org_unitused: pk_org,
					TreeRefActionExt: 'nccloud.web.aum.assign.apply.refcondition.PK_UNIT_USED_VSqlBuilder'
				}; // 根据pk_org过滤
			};
		} else if (item.attrcode == 'pk_usedept_v') {
			//使用部门（借用部门）
			item.queryCondition = () => {
				let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
				return { pk_org: pk_org, busifuncode: IBusiRoleConst.ASSETORG }; // 根据pk_org过滤
			};
		} else if (item.attrcode == 'pk_user') {
			//使用人（借用人）
			item.queryCondition = () => {
				let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
				let pk_usedept = props.form.getFormItemsValue(formId, 'pk_usedept').value; // 使用部门
				return { pk_org: pk_org, pk_dept: pk_usedept, busifuncode: IBusiRoleConst.ASSETORG }; // 根据pk_org过滤
			};
		} else if (item.attrcode == 'pk_applier') {
			//申请人
			item.queryCondition = () => {
				let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
				return { pk_org: pk_org, busifuncode: IBusiRoleConst.ASSETORG, isTreelazyLoad: false }; // 根据pk_unit_used过滤
			};
		} else {
			defRefCondition.call(this, props, item, formId, pk_group);
		}
	});
	//表体区域参照过滤
	meta[tableId].items.map((item) => {
		if (item.attrcode == 'pk_equip') {
			//设备卡片
			item.isMultiSelectedEnabled = true; //设置可以多选
			//设置超链接
			if (item.attrcode == 'pk_equip') {
				//给资产编码列添加超链接
				item.renderStatus = 'browse';
				item.render = (text, record, index) => {
					return (
						<div class="card-table-browse">
							<span
								className="code-detail-link"
								onClick={() => {
									openEquipCardByPk(props, record.values.pk_equip.value);
								}}
							>
								{record.values.pk_equip && record.values.pk_equip.display}
							</span>
						</div>
					);
				};
			}

			//设置设备的查询条件
			item.queryCondition = () => {
				let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
				return {
					pk_org: pk_org,
					transi_type,
					bill_type,
					GridRefActionExt: 'nccloud.web.aum.assign.apply.refcondition.PK_EQUIPSqlBuilder'
				}; // 根据pk_org过滤
			};
		} else if (item.attrcode == 'pk_category') {
			item.isMultiSelectedEnabled = true;
			//类别
			item.queryCondition = () => {
				return { pk_group };
			};
		} else if (item.attrcode == 'pk_material_v') {
			//物料
			item.queryCondition = () => {
				let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
				return { pk_org };
			};
		} else if (item.attrcode == 'pk_jobmngfil') {
			//项目
			item.queryCondition = () => {
				let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
				return { pk_org, pk_group };
			};
		} else {
			defRefCondition.call(this, props, item, formId, pk_group);
		}
	});
	afterModifyCardMeta.call(this, meta, tableId);
}

//添加操作列、设置参照过滤条件
function modifierMeta(props, meta) {
	let label = getMultiLangByID('amcommon-000000'); /*操作*/
	let porCol = {
		attrcode: 'opr',
		label,
		visible: true,
		itemtype: 'customer',
		className: 'table-opr',
		width: 150,
		fixed: 'right',
		render(text, record, index) {
			let status = props.cardTable.getStatus(tableId);
			let closeFlag = record.values.close_flag;
			let close = false;
			if (closeFlag && closeFlag.value) {
				close = closeFlag.value;
			}
			let buttonAry = status === 'browse' ? [ 'OpenCard' ] : [ 'OpenCard', 'DelLine' ];
			if (status === 'browse' && !close) {
				let bill_status = props.form.getFormItemsValue(formId, 'bill_status').value;
				if (BILLSTATUS.check_pass == bill_status) {
					buttonAry = [ 'OpenCard', 'CloseLine' ];
				}
			} else if (status === 'browse' && close) {
				buttonAry = [ 'OpenCard', 'OpenLine' ];
			}
			return props.button.createOprationButton(buttonAry, {
				area: 'card_body_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => {
					tableButtonClick.call(this, props, key, text, record, index);
				}
			});
		}
	};
	meta[tableId].items.push(porCol);

	meta = refcondition.call(this, meta, props);
	return meta;
}
/**
 * 关闭
 * @param {*} props 
 * @param {*} record 
 * @param {*} index 
 */
function closeLine(props, record, index, tableId) {
	let pk_apply = props.form.getFormItemsValue(formId, pkField).value;
	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let rows = [ record.values.pk_apply_b.value ];
	CardData.rows = rows;
	CardData.pagecode = pagecode;
	ajax({
		url: closeUrl,
		data: CardData,
		success: (res) => {
			if (res.data && res.data.head) {
				props.form.setAllFormValue({ [formId]: res.data.head[formId] });
			}
			if (res.data && res.data.body) {
				props.cardTable.setTableData(tableId, res.data.body[tableId]);
			}
			let bill_status = props.form.getFormItemsValue(formId, 'bill_status');
			if (bill_status && bill_status.value == BILLSTATUS.close) {
				props.button.setButtonVisible([ 'UnCommit' ], false);
			}
			updateCache(pkField, pk_apply, res.data, formId, dataSource);
		}
	});
}

/**
 * 打开
 * @param {*} props 
 * @param {*} record 
 * @param {*} index 
 */
function openLine(props, record, index, tableId) {
	let CardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let rows = [ record.values.pk_apply_b.value ];
	let pk_apply = props.form.getFormItemsValue(formId, pkField).value;
	CardData.rows = rows;
	CardData.pagecode = pagecode;
	ajax({
		url: openUrl,
		data: CardData,
		success: (res) => {
			if (res.data && res.data.head) {
				props.form.setAllFormValue({ [formId]: res.data.head[formId] });
			}
			if (res.data && res.data.body) {
				props.cardTable.setTableData(tableId, res.data.body[tableId]);
			}
			let bill_status = props.form.getFormItemsValue(formId, 'bill_status');
			if (bill_status && bill_status.value == BILLSTATUS.check_pass) {
				props.button.setButtonVisible([ 'UnCommit' ], true);
			}
			updateCache(pkField, pk_apply, res.data, formId, dataSource);
		}
	});
}
