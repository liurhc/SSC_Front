import { ajax, cardCache } from 'nc-lightapp-front';
import { pageConfig, appConfig } from '../const';
import ampub from 'ampub';

const { utils } = ampub;
const { setHeadAreaData } = utils.cardUtils;

let { getCacheById, updateCache } = cardCache;
let { dataSource, pkField } = appConfig;
export default function(props, pk) {
	let cardData = getCacheById(pk, dataSource);
	if (cardData) {
		this.putValueToView.call(this, cardData);
	} else {
		let data = {
			pk: pk,
			pagecode: pageConfig.pagecode
		};
		ajax({
			url: '/nccloud/aum/assign/apply_querycard.do',
			data: data,
			success: (res) => {
				if (res.data) {
					this.putValueToView.call(this, res.data);
					let pk_apply = props.form.getFormItemsValue(pageConfig.formId, pkField).value;
					let billcode = this.props.form.getFormItemsValue(this.formId, 'bill_code');
					updateCache(pkField, pk_apply, res.data, pageConfig.formId, dataSource);
					let showdata = { status: this.nowUIStatus, bill_code: '' };
					if (billcode && billcode.value) {
						showdata.bill_code = billcode.value;
					}
					setHeadAreaData.call(this, props, showdata); //设置返回按钮、单据号、分页控件显示隐藏
				}
			}
		});
	}
}
