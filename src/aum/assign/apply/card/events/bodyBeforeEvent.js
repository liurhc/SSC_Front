import { pageConfig } from '../const';
const formId = pageConfig.formId;
const tableId = pageConfig.tableId;
const pageId = pageConfig.pagecode;

export default function bodyBeforeEvent(props, moduleId, key, value, index, record) {
	let editFlag = true;
	editFlag = isEdit.call(this, props, moduleId, key, value, index, record);
	return editFlag;
}
//设备有值，物料编码、设备名称、设备类别、规格、型号、数量不允许编辑
function isEdit(props, moduleId, key, value, index, record) {
	if (
		key == 'equip_name' ||
		key == 'pk_category' ||
		key == 'pk_material_v' ||
		key == 'model' ||
		key == 'spec' ||
		key == 'quantity'
	) {
		if (props.cardTable.getValByKeyAndIndex(moduleId, index, 'pk_equip')) {
			let pk_equip = props.cardTable.getValByKeyAndIndex(moduleId, index, 'pk_equip').value;
			if (pk_equip) {
				return false;
			}
		}
	}
	return true;
}
