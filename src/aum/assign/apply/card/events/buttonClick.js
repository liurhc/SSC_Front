import { ajax, toast, print, output, cardCache } from 'nc-lightapp-front';
import { headAfterEvent } from './afterEvent';
import { pageConfig } from '../const';
import { appConfig } from '../../const';
import ampub from 'ampub';

const { components, commonConst, utils } = ampub;
const { getScriptCardReturnData } = components.ScriptReturnUtils;
const { UISTATE } = commonConst.StatusUtils;
const { getMultiLangByID } = utils.multiLangUtils;
const { showConfirm, MsgConst, showMessage } = utils.msgUtils;

let { listPageCode, pagecode } = pageConfig;
let { printUrl, dataSource, node_code, pkField } = appConfig;
let { getCurrentLastId } = cardCache;
export default function(props, id) {
	let _this = this;
	switch (id) {
		case 'Add':
			let pk_apply = props.form.getFormItemsValue(pageConfig.formId, 'pk_apply').value;
			this.old_pk_apply = pk_apply;
			props.form.EmptyAllFormValue(this.formId);
			props.cardTable.setTableData(this.tableId, { rows: [] }, undefined, true);
			this.setDefaultValue();
			this.toggleShow(UISTATE.add);
			let pk_org_v_value = props.form.getFormItemsValue(this.formId, 'pk_org_v');
			let old_value = {
				display: '',
				scale: '-1',
				value: ''
			};
			headAfterEvent.call(this, props, this.formId, 'pk_org_v', pk_org_v_value, old_value, false);
			break;
		case 'Edit':
			edit.call(this, props);
			break;
		case 'Delete':
			deleteClick.call(this, props);
			break;
		case 'Copy':
			copycard.call(this, props);
			break;
		case 'Back':
			props.pushTo('/list', {
				pagecode: listPageCode
			});
			break;
		case 'Save':
			this.saveClick.call(this);
			break;
		case 'Cancel':
			cancelConfirm.call(this, props);
			break;
		case 'Commit':
			commitClick.call(this, props, 'SAVE', 'commit');
			break;
		case 'SaveCommit':
			commitClick.call(this, props, 'SAVE', 'saveCommit');
			break;
		case 'UnCommit':
			commitClick.call(this, props, 'UNSAVE', '');
			break;
		case 'QueryAboutBillFlow':
			let pk = props.form.getFormItemsValue(pageConfig.formId, 'pk_apply').value;
			let transtype = props.form.getFormItemsValue(pageConfig.formId, 'transi_type').value;
			this.setState({
				showApprove: true,
				pk_apply: pk,
				transtype
			});
			break;
		case 'QueryAboutBusiness':
			let pk1 = props.form.getFormItemsValue(pageConfig.formId, 'pk_apply').value;
			_this.setState({
				show: true,
				pk_apply: pk1
			});
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'AddLine':
			addLine.call(this, props, true);
			break;
		case 'DelLine':
			delLine.call(this, props);
			break;
		case 'Refresh':
			refreshCard.call(this, props);
			break;
		case 'Attachment':
			attachmentClick.call(this, props);
			break;
		case 'BatchAlter':
			props.cardTable.batchChangeTableData(pageConfig.tableId);
			break;
		default:
			break;
	}
}
//删除
function deleteClick(props) {
	showConfirm.call(this, props, {
		type: MsgConst.Type.Delete,
		beSureBtnClick: delConfirm
	});
}
//删除单据
function delConfirm(props) {
	let paramInfoMap = {};
	let pk_apply = props.form.getFormItemsValue(this.formId, pkField).value;
	paramInfoMap[pk_apply] = props.form.getFormItemsValue(this.formId, 'ts').value;
	ajax({
		url: '/nccloud/aum/assign/apply_delete.do',
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: this.pagecode
		},
		success: (res) => {
			if (res) {
				getScriptCardReturnData.call(
					this,
					res,
					this.props,
					this.formId,
					this.tableId,
					pkField,
					dataSource,
					null,
					true,
					this.getdata
				);
			}
		}
	});
}
//修改
function edit(props) {
	let pk = props.form.getFormItemsValue(pageConfig.formId, 'pk_apply').value;
	ajax({
		url: '/nccloud/aum/assign/apply_edit.do',
		data: {
			pk: pk,
			resourceCode: node_code
		},
		success: (res) => {
			if (res.data) {
				toast({ color: 'danger', content: res.data });
			} else {
				props.cardTable.closeExpandedRow(pageConfig.tableId);
				this.toggleShow(UISTATE.edit);
			}
		}
	});
}
/**
 * 增行
*/
export function addLine(props, isFocus = false) {
	props.cardTable.addRow(this.tableId, undefined, { close_flag: { value: false } }, isFocus);
}
/**
* 取消
* @param {*} props 
*/
export function cancelConfirm(props) {
	showConfirm.call(this, props, {
		type: MsgConst.Type.Cancel,
		beSureBtnClick: (props) => {
			cancelAction.call(this, props, UISTATE.browse);
		}
	});
}
/**
 * 界面状态由编辑态取消到对应status
*/
export function cancelAction(props, status) {
	let pk_apply = props.form.getFormItemsValue(pageConfig.formId, 'pk_apply').value;
	if (pk_apply) {
		props.form.cancel(this.formId);
		props.cardTable.resetTableData(this.tableId);
		this.nowUIStatus = UISTATE.browse;
		this.getdata(pk_apply);
	} else {
		//新增取消如果没有选择过组织需要调用此方法恢复字段的可编辑性
		props.resMetaAfterPkorgEdit();
		if (this.old_pk_apply) {
			this.nowUIStatus = UISTATE.browse;
			this.getdata(this.old_pk_apply);
		} else {
			this.nowUIStatus = UISTATE.browse;
			let pk = getCurrentLastId(dataSource);
			if (pk) {
				this.getdata(pk);
			} else {
				props.form.EmptyAllFormValue(this.formId);
				props.cardTable.setTableData(this.tableId, { rows: [] });
				this.toggleShow(UISTATE.blank);
			}
		}
	}
}
//附件
function attachmentClick(props) {
	let billId = props.form.getFormItemsValue(pageConfig.formId, 'pk_apply').value;
	props.ncUploader.show('assign-uploader', {
		billId: 'pam/assign_apply/' + billId
	});
}

/**
* 删除行
* @param {*} props 
*/
export function delLine(props) {
	let checkedRows = props.cardTable.getCheckedRows(pageConfig.tableId);
	let checkedIndex = [];
	checkedRows.map((item) => {
		checkedIndex.push(item.index);
	});
	props.cardTable.delRowsByIndex(pageConfig.tableId, checkedIndex);
	props.button.setButtonDisabled([ 'DelLine' ], true);
}
//卡片界面刷新
function refreshCard(props) {
	let pk_apply = props.form.getFormItemsValue(pageConfig.formId, 'pk_apply').value;
	if (pk_apply) {
		let data = { pk: pk_apply, pagecode: pageConfig.pagecode };
		ajax({
			url: '/nccloud/aum/assign/apply_querycard.do',
			data,
			success: (res) => {
				if (res.data) {
					this.putValueToView.call(this, res.data);
					this.toggleShow(UISTATE.browse);
					showMessage.call(this, props, { type: MsgConst.Type.RefreshSuccess }); /* 国际化多语处理：刷新成功 */
				} else {
					cardCache.deleteCacheById(pkField, pk_apply, dataSource);
					this.putValueToView();
					this.setButtonVisible(UISTATE.blank);
					showMessage.call(this, props, { type: MsgConst.Type.DataDeleted }); /* 国际化多语处理：数据已经被删除 */
				}
			}
		});
	}
}
//卡片界面复制
function copycard(props) {
	let pk_apply = props.form.getFormItemsValue(pageConfig.formId, 'pk_apply').value;
	this.old_pk_apply = pk_apply;
	let data = { pk: pk_apply, pagecode: pagecode };
	ajax({
		url: '/nccloud/aum/assign/apply_copydata.do',
		data,
		success: (res) => {
			if (res.data) {
				this.putValueToView.call(this, res.data);
				this.toggleShow(UISTATE.copyAdd);
			} else {
				toast({ color: 'warning', content: getMultiLangByID('452001004A-000005') /* 您复制的数据已被删除，无法复制*/ });
			}
		}
	});
}
//提交、保存提交、收回
export function commitClick(props, OperatorType, commitType, content) {
	if (commitType === 'saveCommit' || commitType == 'commit') {
		let isCommit = commitType == 'commit' ? true : false;
		//校验表头必输项
		let pass = this.validateBeforeSave(props, isCommit);
		if (!pass) {
			return;
		}
	}
	let pk = props.form.getFormItemsValue(pageConfig.formId, 'pk_apply').value;
	let CardData = props.createMasterChildDataSimple(pageConfig.pagecode, pageConfig.formId, pageConfig.tableId);
	let paramInfoMap = {};
	paramInfoMap[pk] = ''; //卡片页面ts为空也可以 CardData 里有ts
	let obj = {
		dataType: 'cardData',
		OperatorType: OperatorType,
		commitType: commitType,
		pageid: pageConfig.pagecode,
		paramInfoMap: paramInfoMap,
		content: content
	};
	CardData.userjson = JSON.stringify(obj);
	if (commitType === 'saveCommit' && !content) {
		props.validateToSave(CardData, () => {});
		ajax({
			url: '/nccloud/aum/assign/commitOrUn.do',
			data: CardData,
			success: (res) => {
				if (content) {
					this.setState({
						compositedisplay: false
					});
				}
				let callback = () => {
					this.toggleShow(UISTATE.browse);
				};
				getScriptCardReturnData.call(
					this,
					res,
					props,
					pageConfig.formId,
					pageConfig.tableId,
					'pk_apply',
					dataSource,
					null,
					false,
					callback,
					pageConfig.pagecode
				);
			}
		});
	} else {
		ajax({
			url: '/nccloud/aum/assign/commitOrUn.do',
			data: CardData,
			success: (res) => {
				if (content) {
					this.setState({
						compositedisplay: false
					});
				}
				let callback = () => {
					this.toggleShow(UISTATE.browse);
				};
				getScriptCardReturnData.call(
					this,
					res,
					props,
					pageConfig.formId,
					pageConfig.tableId,
					'pk_apply',
					dataSource,
					null,
					false,
					callback
				);
			}
		});
	}
}
/**
 * 打印
 * @param {*} props 
 */
function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint }); /* 国际化多语处理：请选择需要打印的数据 */
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		printUrl, // 后台打印服务url
		printData
	);
}

/**
 * 输出
 * @param {*} props 
 */
function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput }); /* 国际化多语处理：请选择需要输出的数据 */
		return;
	}
	output({
		url: printUrl,
		data: printData
	});
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let { printFuncode, printNodekey } = appConfig;
	let pk = props.form.getFormItemsValue(pageConfig.formId, 'pk_apply');
	if (!pk || !pk.value) {
		return false;
	}
	let pks = [ pk.value ];
	let printData = {
		filename: pageConfig.pagecode, // 文件名称
		nodekey: printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}
