import { addLine } from './buttonClick';
import { pageConfig } from '../const';
import ampub from 'ampub';

const { components, utils } = ampub;
const { orgChangeEvent } = components.OrgChangeEvent;
const { commonHeadAfterEvent, commonBodyAfterEvent } = utils.cardUtils;

let clearFields = [ 'pk_usedept', 'pk_usedept_v', 'pk_unit_used', 'pk_unit_used_v', 'pk_user', 'pk_applier' ];

export default function afterEvent(props, moduleId, key, value, changedrows, index, record, type, eventType) {
	if (moduleId == pageConfig.formId) {
		if (key === 'pk_org_v') {
			//组织切换确定后发送后台请求
			let callback = () => {
				headAfterEvent.call(this, props, moduleId, key, value, changedrows, index, record, type, eventType);
			};
			let editButtons = [ 'AddLine', 'BatchAlter' ];
			//组织切换
			orgChangeEvent.call(
				this,
				props,
				pageConfig.pagecode,
				pageConfig.formId,
				pageConfig.tableId,
				key,
				value,
				changedrows,
				clearFields,
				editButtons,
				callback
			);
		} else {
			headAfterEvent.call(this, props, moduleId, key, value, changedrows, index, record, type, eventType);
		}
	} else if (moduleId == pageConfig.tableId && !isEqual(changedrows)) {
		bodyAfterEvent.call(this, props, moduleId, key, value, changedrows, index, record, type, eventType);
	}
}

/**
 * 表头编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} oldValue 
 * @param {*} val 
 */
export function headAfterEvent(props, moduleId, key, value, oldValue, val) {
	if (key == 'pk_org_v' || key == 'pk_user') {
		if (value.value) {
			if (key == 'pk_org_v') {
				addLine.call(this, props);
			}
			let config = {
				afterEditUrl: '/nccloud/aum/assign/applyHeadAfterEdit.do',
				pagecode: pageConfig.pagecode,
				key,
				value,
				oldValue
			};
			commonHeadAfterEvent.call(this, props, config);
		}
	}
}
function isEqual(changedrows) {
	if (changedrows) {
		if (Object.prototype.toString.call(changedrows) === '[object Array]') {
			return false;
		}
		let newValue = changedrows.newvalue;
		let oldValue = changedrows.oldvalue;
		if (!oldValue) {
			oldValue = '';
		}
		if (!newValue) {
			newValue = '';
		}
		if (newValue == oldValue) {
			return true;
		}
		return false;
	}
}

/**
 * 表体编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedRows 
 * @param {*} index 
 * @param {*} record 
 * @param {*} type 
 * @param {*} eventType 
 */
function bodyAfterEvent(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	// 平台的自动增行不会附默认值，这里调用自己的增行,转单不自动增行
	if (this.srctype != 'transfer') {
		let num = props.cardTable.getNumberOfRows(pageConfig.tableId);
		if (num == index + 1) {
			addLine.call(this, props);
		}
	}
	let config = {
		afterEditUrl: '/nccloud/aum/assign/applyBodyAfterEdit.do',
		pagecode: pageConfig.pagecode,
		key,
		record,
		changedRows,
		index,
		keys: [ 'pk_equip', 'pk_category' ]
	};
	commonBodyAfterEvent.call(this, props, config);
}
