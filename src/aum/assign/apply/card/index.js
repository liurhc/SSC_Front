import React, { Component } from 'react';
import { createPage, ajax, base, toast, high, cardCache } from 'nc-lightapp-front';
import { buttonClick, initTemplate, afterEvent, pageInfoClick, bodyBeforeEvent, rowSelected } from './events';
import { pageConfig, appConfig } from './const';
import { commitClick } from '../card/events/buttonClick';
import { headAfterEvent } from '../card/events/afterEvent';

import ampub from 'ampub';

const { components, commonConst, utils } = ampub;
const { ApprovalTrans } = components;
const { beforeSaveValidator } = components.saveValidatorsUtil;
const { getContext, loginContextKeys } = components.LoginContext;
const { UISTATE, BILLSTATUS } = commonConst.StatusUtils;
const { getMultiLangByID } = utils.multiLangUtils;
const { showMessage, MsgConst } = utils.msgUtils;
const { closeBrowserUtils } = utils;
const { setCardValue, createCardTitleArea, createCardPaginationArea, setHeadAreaData } = utils.cardUtils;

const { NCAffix } = base;
const { BillTrack, ApproveDetail } = high;
const { dataSource, billType, title, pkField } = appConfig;
let { listPageCode, listRouter } = pageConfig;
let { updateCache, addCache } = cardCache;

class Card extends Component {
	constructor(props) {
		super(props);
		this.formId = pageConfig.formId;
		this.searchId = pageConfig.searchcode;
		this.tableId = pageConfig.tableId;
		this.pagecode = pageConfig.pagecode;
		this.state = {
			transtype: '4A37-01',
			pk_org: '',
			showApprove: false, //审批详情
			pk_apply: '',
			show: false, //单据追溯
			compositedisplay: false,
			compositedata: {}
		};
		this.nowUIStatus = this.props.getUrlParam('status');
		this.pk_apply = this.props.getUrlParam('id');
		this.old_pk_apply = ''; //复制、新增时记录旧的pk以便恢复旧值

		closeBrowserUtils.call(this, props, { from: [ this.formId ], cardTable: [ this.tableId ] });
		initTemplate.call(this, props);
	}
	componentDidMount() {}

	//模板加载后进行初始化设置
	init() {
		if (this.nowUIStatus == UISTATE.add) {
			this.setDefaultValue();
			this.toggleShow(this.nowUIStatus);
			let pk_org_v_value = this.props.form.getFormItemsValue(this.formId, 'pk_org_v');
			let old_value = {
				display: '',
				scale: '-1',
				value: ''
			};
			headAfterEvent.call(this, this.props, this.formId, 'pk_org_v', pk_org_v_value, old_value, false);
		} else if (this.nowUIStatus == UISTATE.copyAdd) {
			this.copydata.call(this, this.pk_apply);
		} else {
			if (this.pk_apply && this.pk_apply != 'undefined') {
				this.getdata(this.pk_apply);
			}
			this.toggleShow(this.nowUIStatus);
		}
	}
	//设置组织不可编辑
	setOrgDisable = (flag) => {
		this.props.form.setFormItemsDisabled(this.formId, { pk_org_v: flag, pk_org: flag });
	};
	setDefaultValue = () => {
		let freeStatus = getMultiLangByID('statusUtils-000000'); /*自由态*/
		this.props.form.setFormItemsValue(this.formId, { bill_status: { value: '0', display: freeStatus } });
		this.props.form.setFormItemsValue(this.formId, { bill_type: { value: billType, display: billType } });
		// 集团
		let groupId = getContext(loginContextKeys.groupId);
		let groupName = getContext(loginContextKeys.groupName);
		// 交易类型
		let transi_type = getContext(loginContextKeys.transtype);
		let pk_transitype = getContext(loginContextKeys.pk_transtype);
		// 默认主组织
		let pk_org = getContext(loginContextKeys.pk_org);
		let org_Name = getContext(loginContextKeys.org_Name);
		let pk_org_v = getContext(loginContextKeys.pk_org_v);
		let org_v_Name = getContext(loginContextKeys.org_v_Name);
		this.props.form.setFormItemsValue(this.formId, {
			pk_group: { value: groupId, display: groupName },
			pk_org: { value: pk_org, display: org_Name },
			pk_org_v: { value: pk_org_v, display: org_v_Name },
			bill_type: { value: billType },
			transi_type: { value: transi_type },
			pk_transitype: { value: pk_transitype }
		});
	};

	//切换页面状态
	toggleShow = (status) => {
		this.nowUIStatus = status;
		let billcode = this.props.form.getFormItemsValue(this.formId, 'bill_code');
		let flag = status === UISTATE.browse || status === UISTATE.blank ? 'browse' : 'edit';
		this.props.form.setFormStatus(this.formId, flag);
		this.props.cardTable.setStatus(this.tableId, flag);
		this.setButtonVisible(status);
		if (UISTATE.add == status) {
			this.setOrgDisable(false);
			// 设字段的可编辑性
			let pk_org_v = this.props.form.getFormItemsValue(this.formId, 'pk_org_v');
			if (!pk_org_v.value) {
				this.props.initMetaByPkorg('pk_org_v');
			}
			this.props.button.setButtonDisabled([ 'DelLine' ], true);
		} else if (UISTATE.edit == status || UISTATE.copyAdd == status) {
			//组织不为空时增行、批改可用 删行不可用
			this.props.button.setButtonDisabled([ 'AddLine', 'BatchAlter' ], false);
			this.props.button.setButtonDisabled([ 'DelLine' ], true);
			this.setOrgDisable(true);
		}
		let showdata = { status, bill_code: '' };
		if (billcode && billcode.value) {
			showdata.bill_code = billcode.value;
		}
		setHeadAreaData.call(this, this.props, showdata); //设置返回按钮、单据号、分页控件显示隐藏
	};
	//设置按钮的可见性
	setButtonVisible = (status) => {
		let pk_org_v = this.props.form.getFormItemsValue(pageConfig.formId, 'pk_org_v').value;

		let allButtons = [
			'SaveGroup',
			'Save',
			'SaveCommit',
			'Cancel',
			'EditGroup',
			'Add',
			'Edit',
			'Delete',
			'Copy',
			'Commit',
			'UnCommit',
			'QueryAboutBillFlow',
			'Attachment',
			'Print',
			'PrintGroup',
			'Output',
			'Refresh',
			'LineGroup',
			'AddLine',
			'DelLine',
			'OpenCard',
			'BatchAlter'
		];
		let blankButtons = [ 'EditGroup', 'Add' ];
		let editButtons = [
			'SaveGroup',
			'Save',
			'SaveCommit',
			'Cancel',
			'LineGroup',
			'AddLine',
			'DelLine',
			'OpenCard',
			'BatchAlter'
		];
		let borwseButtons_free = [
			'EditGroup',
			'Add',
			'Edit',
			'Copy',
			'Delete',
			'Commit',
			'Attachment',
			'QueryAboutBillFlow',
			'Print',
			'PrintGroup',
			'Output',
			'Refresh',
			'OpenCard'
		];
		//已提交
		let borwseButtons_uncheck = [
			'EditGroup',
			'Add',
			'Copy',
			'UnCommit',
			'Attachment',
			'QueryAboutBillFlow',
			'Print',
			'PrintGroup',
			'Output',
			'Refresh',
			'OpenCard'
		];
		//审批中
		let borwseButtons_checkgoing = [
			'EditGroup',
			'Add',
			'Copy',
			'Attachment',
			'QueryAboutBillFlow',
			'Print',
			'PrintGroup',
			'Output',
			'Refresh',
			'OpenCard'
		];
		//审核通过
		let borwseButtons_check = [
			'EditGroup',
			'Add',
			'Copy',
			'UnCommit',
			'Attachment',
			'QueryAboutBillFlow',
			'Print',
			'PrintGroup',
			'Output',
			'Refresh',
			'OpenCard'
		];
		//审批不通过
		let borwseButtons_nopass = [
			'EditGroup',
			'Add',
			'Edit',
			'Copy',
			'Delete',
			'Attachment',
			'QueryAboutBillFlow',
			'Print',
			'PrintGroup',
			'Output',
			'Refresh',
			'OpenCard'
		];

		//关闭
		let borwseButtons_close = [
			'EditGroup',
			'Add',
			'Copy',
			'Attachment',
			'QueryAboutBillFlow',
			'Print',
			'PrintGroup',
			'Output',
			'Refresh',
			'OpenCard'
		];

		let bill_status = this.props.form.getFormItemsValue(this.formId, 'bill_status');
		if (bill_status) {
			bill_status = bill_status.value;
		}

		this.props.button.setButtonVisible(allButtons, false);
		switch (status) {
			case UISTATE.add:
				this.props.button.setButtonVisible(editButtons, true);
				break;
			case UISTATE.edit:
				this.props.button.setButtonVisible(editButtons, true);
				break;
			case UISTATE.copyAdd:
				this.props.button.setButtonVisible(editButtons, true);
				break;
			case UISTATE.browse:
				if (bill_status == BILLSTATUS.free_check || bill_status == BILLSTATUS.check_nopass) {
					this.props.button.setButtonVisible(borwseButtons_free, true);
				} else if (bill_status == BILLSTATUS.un_check) {
					this.props.button.setButtonVisible(borwseButtons_uncheck, true);
				} else if (bill_status == BILLSTATUS.check_going) {
					this.props.button.setButtonVisible(borwseButtons_checkgoing, true);
				} else if (bill_status == BILLSTATUS.check_pass) {
					this.props.button.setButtonVisible(borwseButtons_check, true);
				} else if (bill_status == BILLSTATUS.close) {
					this.props.button.setButtonVisible(borwseButtons_close, true);
				} else if (BILLSTATUS.check_nopass == bill_status) {
					this.props.button.setButtonVisible(borwseButtons_nopass, true);
				} else {
					this.props.button.setButtonVisible(blankButtons, true);
				}
				break;
			case UISTATE.blank:
				this.props.button.setButtonVisible(blankButtons, true);
				break;
			default:
				break;
		}
		if (!pk_org_v) {
			//组织为空时增行、删行、批改不可用
			this.props.button.setButtonDisabled([ 'AddLine', 'DelLine', 'BatchAlter' ], true);
		} else {
			//组织为空时增行、删行、批改可用
			this.props.button.setButtonDisabled([ 'AddLine', 'BatchAlter' ], false);
		}
	};

	//通过单据id查询单据信息
	getdata = (pk) => {
		if (!pk) {
			//pk为空时直接设置界面数据为空
			this.putValueToView.call(this, null);
			this.toggleShow(this.nowUIStatus);
		} else {
			let cacheData = null; //cardCache.getCacheById(pk, dataSource); //平台缓存同步的方式
			if (cacheData) {
				this.putValueToView.call(this, cacheData);
				this.toggleShow(this.nowUIStatus);
			} else {
				let data = { pk, pagecode: this.pagecode };
				ajax({
					url: '/nccloud/aum/assign/apply_querycard.do',
					data,
					success: (res) => {
						if (res.data) {
							this.putValueToView.call(this, res.data);
							this.toggleShow(this.nowUIStatus);
							//更新或增加缓存
							updateCache(pkField, pk, res.data, pageConfig.formId, dataSource);
						} else {
							showMessage.call(this, this.props, {
								type: MsgConst.Type.DataDeleted
							}); /* 国际化多语处理：数据已经被删除 */
						}
					}
				});
			}
		}
	};
	//复制数据--列表界面点击复制
	copydata = (pk) => {
		let data = { pk, pagecode: this.pagecode };
		ajax({
			url: '/nccloud/aum/assign/apply_copydata.do',
			data,
			success: (res) => {
				if (res.data) {
					this.putValueToView.call(this, res.data);
					this.toggleShow(UISTATE.copyAdd);
				} else {
					showMessage.call(this, props, { type: MsgConst.Type.DataDeleted }); /* 国际化多语处理：数据已经被删除 */
					this.toggleShow(UISTATE.add);
					this.setDefaultValue();
				}
			}
		});
	};

	//设置数据到界面展示
	putValueToView = (data) => {
		setCardValue.call(this, this.props, data);
	};
	/**
* 保存前校验
* @param {*} props 
*/
	validateBeforeSave = (props, isCommit) => {
		//校验表头必输项
		let pass = beforeSaveValidator.call(this, this.props, this.formId, this.tableId, [
			'quantity',
			'close_flag',
			'assign_quantity'
		]);
		if (!pass) {
			return false;
		}
		// 表体必输校验
		let allRows = props.cardTable.getVisibleRows(this.tableId);
		if (!allRows || allRows.length == 0) {
			showMessage.call(this, props, { type: MsgConst.Type.BodyNotNull }); /* 国际化多语处理：表体行必须要一条数据 */
			return false;
		} else if (!isCommit) {
			//保存时校验
			let errs1 = []; //检验领用归还日期，必须大于领用日期
			let errs2 = []; //检验领用日期必须晚于当前日期
			allRows.map((row, index) => {
				let expect_start_date = row.values['expect_start_date'];
				let expect_end_date = row.values['expect_end_date'];
				if (expect_start_date && expect_start_date.value) {
					let start = new Date(expect_start_date.value).getTime();
					let curr = new Date(
						getContext(loginContextKeys.businessDate).substring(0, 10) + ' 00:00:00'
					).getTime();
					if (start < curr) {
						errs2.push(index + 1);
					}
					if (expect_end_date && expect_end_date.value) {
						let end = new Date(expect_end_date.value).getTime();
						if (start > end) {
							errs1.push(index + 1);
						}
					}
				}
			});
			let errMsg = '';
			if (errs1 && errs1.length > 0) {
				errMsg =
					'<div>' +
					getMultiLangByID('452001004A-000003', {
						index: [ ...errs1 ].join('、')
					}) +
					'</div>'; /*第{index}行【领用日期】不能晚于【预计归还日期】*/
			}
			if (errs2 && errs2.length > 0) {
				errMsg =
					errMsg +
					'<div>' +
					getMultiLangByID('452001004A-000004', {
						index: [ ...errs2 ].join('、')
					}) +
					'</div>'; /*第{index}行【领用日期】不能早于预【当前日期】*/
			}
			if (errMsg) {
				toast({ content: errMsg, color: 'danger', isNode: true });
				return false;
			}
		}
		return true;
	};

	//保存单据
	saveClick = () => {
		let pass = this.validateBeforeSave(this.props);
		if (!pass) {
			return;
		}
		let CardData = this.props.createMasterChildDataSimple(this.pagecode, this.formId, this.tableId);
		let url = '/nccloud/aum/assign/apply_save.do'; //新增保存
		if (this.nowUIStatus === UISTATE.edit) {
			url = '/nccloud/aum/assign/apply_update.do'; //修改保存
		}
		this.props.validateToSave(CardData, () => {
			ajax({
				url: url,
				data: CardData,
				success: (res) => {
					if (res.success) {
						if (res.data) {
							setCardValue.call(this, this.props, res.data);
						}
						let pk_apply = this.props.form.getFormItemsValue(pageConfig.formId, pkField).value;
						let cacheData = this.props.createMasterChildData(this.pagecode, this.formId, this.tableId);
						if (this.nowUIStatus === UISTATE.edit) {
							updateCache(pkField, pk_apply, cacheData, pageConfig.formId, dataSource);
						} else {
							addCache(pk_apply, cacheData, pageConfig.formId, dataSource);
						}
						showMessage.call(this, this.props, { type: MsgConst.Type.SaveSuccess }); /* 国际化多语处理：保存成功 */
						this.toggleShow(UISTATE.browse);
					}
				}
			});
		});
	};

	modelSave = (props) => {
		this.saveClick();
	};
	//返回列表界面
	backToList = () => {
		this.props.pushTo(listRouter, {
			pagecode: listPageCode
		});
	};
	//获取列表肩部信息
	getTableHeadLeft = () => {
		let status = this.nowUIStatus;
		if (status != 'browse') {
			return '';
		}
		let { cardTable } = this.props;
		let rowCount = 0;
		let applycount = 0;
		let allRows = cardTable.getVisibleRows(this.tableId);
		let numArray = cardTable.getColValue(this.tableId, 'quantity', false, false);
		if (numArray) {
			for (let num in numArray) {
				if (!isNaN(num)) {
					applycount = applycount + parseInt(numArray[num].value);
				}
			}
		}
		if (allRows) {
			rowCount = allRows.length;
		}
		let str = getMultiLangByID('cardUtils-000000', { num: rowCount }); /* 国际化处理： 总计 :{num}行*/
		let messages = [];
		if (str) {
			messages = str.split(rowCount);
		}
		return (
			<div className="shoulder-definition-area">
				<div className="definition-search">
					<div>
						<span className="definition-search-title">{messages[0]}</span>
						<span className="count">{rowCount}</span>
						<span>{messages[1]}</span>
						<span> {getMultiLangByID('452001004A-000001') /*申请数量:*/}</span>
						<span className="count">{applycount}</span>
						<span>{getMultiLangByID('452001004A-000002') /*个*/}</span>
					</div>
				</div>
			</div>
		);
	};
	//获取列表肩部信息
	getTableHead = () => {
		let { button } = this.props;
		let { createButtonApp } = button;
		return (
			<div className="shoulder-definition-area">
				<div className="definition-icons">
					{createButtonApp({
						area: 'card_body',
						buttonLimit: 3,
						onButtonClick: (props, id) => {
							buttonClick.call(this, props, id, this.tableId);
						},
						popContainer: document.querySelector('.header-button-area')
					})}
				</div>
			</div>
		);
	};
	//提交及指派 回调
	getAssginUsedr = (value) => {
		commitClick.call(this, this.props, 'SAVE', 'commit', value);
	};
	//取消 指派
	turnOff = () => {
		this.setState({
			compositedisplay: false
		});
	};

	render() {
		let { cardTable, form, button, cardPagination, ncUploader } = this.props;
		const { createCardPagination } = cardPagination;
		let buttons = this.props.button.getButtons();
		let { createForm } = form;
		let { createNCUploader } = ncUploader;
		let { createCardTable } = cardTable;
		let { createButton } = button;
		let status = this.nowUIStatus;
		let multiTitle = getMultiLangByID(title);
		return (
			<div className="nc-bill-card">
				<div className="nc-bill-top-area">
					<NCAffix>
						<div className="nc-bill-header-area">
							{/* 标题 title */}
							{createCardTitleArea.call(this, this.props, {
								title: multiTitle,
								formId: this.formId,
								backBtnClick: this.backToList
							})}
							<div className="header-button-area">
								{this.props.button.createButtonApp({
									area: 'card_head',
									buttonLimit: 3,
									onButtonClick: buttonClick.bind(this),
									popContainer: document.querySelector('.header-button-area')
								})}
							</div>
							{createCardPaginationArea.call(this, this.props, {
								formId: this.formId,
								dataSource,
								pageInfoClick
							})}
						</div>
					</NCAffix>
					<div className="nc-bill-form-area">
						{createForm(this.formId, {
							onAfterEvent: afterEvent.bind(this)
						})}
					</div>
				</div>
				<div className="nc-bill-bottom-area">
					<div className="nc-bill-table-area">
						{createCardTable(this.tableId, {
							tableHeadLeft: this.getTableHeadLeft.bind(this),
							tableHead: this.getTableHead.bind(this),
							modelSave: this.modelSave.bind(this),
							onAfterEvent: afterEvent.bind(this),
							onBeforeEvent: bodyBeforeEvent.bind(this),
							showCheck: true,
							showIndex: true,
							selectedChange: rowSelected.bind(this)
						})}
					</div>
				</div>
				{/* 确认取消框 */}
				{createNCUploader('assign-uploader', {})}
				{/* 审批详情 */}
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transtype}
					billid={this.state.pk_apply}
				/>
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002') /*指派*/}
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				) : (
					''
				)}
				<BillTrack
					show={this.state.show}
					close={() => {
						this.setState({ show: false });
					}}
					pk={this.state.pk_apply} //单据id
					type={billType} //单据类型
				/>
			</div>
		);
	}
}

Card = createPage({
	billinfo: {
		billtype: 'card',
		pagecode: pageConfig.pagecode,
		headcode: pageConfig.formId,
		bodycode: pageConfig.tableId
	}
})(Card);
export default Card;
