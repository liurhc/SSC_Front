export {appConfig} from '../const';

//卡片界面常量
export const pageConfig={
    pagecode:'452001004A_card',
    formId:'card_head',
    searchcode:'searchArea',
    tableId:'bodyvos',
    listRouter:'/list',
    listPageCode:'452001004A_list',// 列表界面页面编码

    url:{
        openUrl:'/nccloud/aum/assign/openLine.do',
        closeUrl:'/nccloud/aum/assign/closeLine.do'
    }
}