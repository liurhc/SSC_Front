import { pageConfig } from '../const';
import ampub from 'ampub';

const { components, commonConst } = ampub;
const { defRefCondition, commonRefCondition } = components.refInit;
const { getContext, loginContext, loginContextKeys } = components.LoginContext;
const { IBusiRoleConst } = commonConst.CommonKeys;
const { UISTATE } = commonConst.StatusUtils;
const { AssetOrgMultiRefFilter } = components.assetOrgMultiRefFilter;

let { pageCode, headCode, searchCode, sourcePageCode, dataSource, sourceInfoField, sourceAppCode } = pageConfig;
const { sourceUrl } = pageConfig.url;

export default function(props) {
	let sourceInfo = getContext(sourceInfoField, dataSource);
	sourceAppCode = sourceInfo.appcode;
	props.createUIDom(
		{
			pagecode: pageCode, //页面id
			appcode: sourceAppCode //上游应用编码（转单应用的页面注册在上游）
		},
		function(data) {
			if (data) {
				if (data.context) {
					loginContext.call(this, data.context);
				}
				if (data.button) {
					let button = data.button;
					button[0].isenable = false; //默认不可用
					props.button.setButtons(button);
				}
				if (data.template) {
					let meta = data.template;
					meta = addLink(props, meta);
					meta = addCondition(props, meta);
					props.meta.addMeta(meta);
				}
			}
		}
	);
}
//增加查询相关条件控制
function addCondition(props, meta) {
	let pk_group = getContext(loginContextKeys.groupId);
	//查询区参照过滤
	meta[searchCode].items.map((item) => {
		if (item.attrcode == 'pk_org') {
			item.queryCondition = () => {
				return { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter' };
			};
		}
		if (item.attrcode == 'pk_unit_used' || item.attrcode == 'bodyvos.pk_category') {
			//（设备类别）
			item.queryCondition = () => {
				return { pk_group }; // 根据pk_group过滤
			};
		} else if (item.attrcode == 'pk_material' || item.attrcode == 'pk_jobmngfil') {
			//组织多选参照过滤
			AssetOrgMultiRefFilter.call(this, props, searchCode, item);
			//（项目、物料）
			item.queryCondition = () => {
				let pk_org = getSearchValueOrg.call(this, props, 'pk_org'); // 主组织
				return { pk_org: pk_org }; // 根据主组织过滤
			};
		} else if (item.attrcode == 'pk_usedept' || item.attrcode == 'pk_user') {
			//组织多选参照过滤
			AssetOrgMultiRefFilter.call(this, props, searchCode, item);
			//使用部门 使用人(领用部门 领用人)
			item.queryCondition = () => {
				let pk_org = getSearchValueOrg.call(this, props, 'pk_org'); //主组织
				if (pk_org) {
					return { pk_org: pk_org, isShowUnit: false, busifuncode: IBusiRoleConst.ASSETORG }; // 有pk_org过滤
				} else {
					return { isShowUnit: true, busifuncode: IBusiRoleConst.ASSETORG }; //组织多选时，无pk_org过滤
				}
			};
		} else if (item.attrcode == 'pk_applier') {
			//组织多选参照过滤
			AssetOrgMultiRefFilter.call(this, props, searchCode, item);
			//申请人
			item.queryCondition = () => {
				let pk_org = getSearchValueOrg.call(this, props, 'pk_org'); //主组织
				if (pk_org) {
					return { pk_org: pk_org, isShowUnit: false, busifuncode: IBusiRoleConst.ASSETORG }; // 根据pk_org过滤
				} else {
					return { isShowUnit: true, busifuncode: IBusiRoleConst.ASSETORG }; // 根据pk_org过滤
				}
			};
		} else {
			let bodyDefPrefix = 'bodyvos.def';
			defRefCondition.call(this, props, item, searchCode, pk_group, true, 'pk_org', bodyDefPrefix);
			defRefCondition.call(this, props, item, searchCode, pk_group, true);
		}
		// 添加【执行时包含下级】默认勾选
		commonRefCondition.call(this, props, item);
	});
	return meta;
}
//获取查询条件的值
function getSearchValueOrg(props, field) {
	let data = props.search.getSearchValByField(searchCode, field);
	if (data && data.value && data.value.firstvalue && 20 == data.value.firstvalue.length) {
		return data.value.firstvalue;
	}
}
//添加超链接
function addLink(props, meta) {
	meta[headCode].items = meta[headCode].items.map((item, key) => {
		if (item.attrcode == 'bill_code') {
			item.render = (text, record, index) => {
				return (
					<span
						className="code-detail-link"
						onClick={() => {
							props.openTo(sourceUrl, {
								pagecode: sourcePageCode,
								status: UISTATE.browse,
								appcode: sourceAppCode,
								id: record.pk_apply.value
							});
						}}
					>
						{record.bill_code && record.bill_code.value}
					</span>
				);
			};
		}
		return item;
	});
	return meta;
}
