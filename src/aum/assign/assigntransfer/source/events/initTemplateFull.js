import { pageConfig } from '../const';
import ampub from 'ampub';

const { components, commonConst } = ampub;
const { getContext } = components.LoginContext;
const { UISTATE } = commonConst.StatusUtils;

let { mainCode, sourceAppCode, mainPageCode, sourcePageCode, dataSource, sourceInfoField } = pageConfig;
let { sourceUrl } = pageConfig.url;

//主子拉平模板
export default function initTemplateFull(props) {
	let sourceInfo = getContext(sourceInfoField, dataSource);
	sourceAppCode = sourceInfo.appcode;
	props.createUIDom(
		{
			pagecode: mainPageCode, //页面编码
			appcode: sourceAppCode //上游应用编码
		},
		function(data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = addLink(props, meta);
					props.meta.addMeta(meta, () => {});
				}
			}
		}
	);
}

//添加超链接
function addLink(props, meta) {
	meta[mainCode].items = meta[mainCode].items.map((item, key) => {
		if (item.attrcode == 'bill_code') {
			item.render = (text, record, index) => {
				return (
					<span
						className="code-detail-link"
						onClick={() => {
							props.openTo(sourceUrl, {
								pagecode: sourcePageCode,
								status: UISTATE.browse,
								appcode: sourceAppCode,
								id: record.pk_apply.value
							});
						}}
					>
						{record.bill_code && record.bill_code.value}
					</span>
				);
			};
		}
		return item;
	});
	return meta;
}
