import { ajax } from 'nc-lightapp-front';
import { pageConfig } from '../const';

let {
	headCode,
	bodyCode,
	pageCode,
	searchCode,
	head_pkfield,
	body_pkfield,
	sourceAppCode,
	sourceInfoField,
	dataSource
} = pageConfig;

import ampub from 'ampub';

const { components, utils } = ampub;
const { getContext } = components.LoginContext;
const { MsgConst, showMessage } = utils.msgUtils;

//点击查询，获取查询区数据
export default function clickSearchBtn(props, searchVal) {
	let sourceInfo = getContext(sourceInfoField, dataSource);
	let data = props.search.getQueryInfo(searchCode, true);
	let billtype = sourceInfo.billtype;
	let transtype = sourceInfo.transtype;
	if (data) {
		data.billtype = billtype;
		data.transtype = transtype;
		data.pagecode = pageCode;
		data.appcode = sourceAppCode;
		data.userdefObj = { busiTypes: sourceInfo.busitypes };
	}
	ajax({
		url: '/nccloud/aum/assign/srcapply_query.do',
		data,
		success: (res) => {
			if (res.data) {
				props.transferTable.setTransferTableValue(headCode, bodyCode, res.data, head_pkfield, body_pkfield);
				showMessage.call(this, props, { type: MsgConst.Type.QuerySuccess }); /*查询成功*/
			} else {
				props.transferTable.setTransferTableValue(headCode, bodyCode, [], head_pkfield, body_pkfield);
				showMessage.call(this, props, { type: MsgConst.Type.QueryNoData }); /*未查询出符合条件的数据*/
			}
		}
	});
}
