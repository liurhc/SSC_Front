import { pageConfig } from '../const';
import ampub from 'ampub';

const { components } = ampub;
const { isMultiCorpRefHandler } = components.assetOrgMultiRefFilter;

const { searchCode } = pageConfig;
export default function afterEvent(key, value) {
	if (key === 'pk_org') {
		isMultiCorpRefHandler.call(this, this.props, value, searchCode, [ 'pk_usedept', 'pk_user' ]);
	}
}
