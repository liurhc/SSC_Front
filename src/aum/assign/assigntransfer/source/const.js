export const pageConfig = {
	//页面编码
    pageCode: '452001004A_transferlist',
	//上游_领用申请小应用编码
    sourceAppCode: '452001004A',
    //上游_页面编码（超链接跳转使用）
    sourcePageCode:'452001004A_card',
    //下游_资产领用小应用编码
    nextAppCode: '452001012A',
    //下游_资产领用小应用转单页面编码
    nextPageCode: '452001012A_transfercard',
    //表头列表code
    headCode: 'head',
    //表体列表code
    bodyCode: 'bodyvos',
    //主子拉平展示页面编码
    mainPageCode:'452001004A_transfermain',
    // 主子拉平显示区域编码
    mainCode: 'transfermain',
    // 查询区域code
    searchCode: 'searchArea',
    // 主表主键字段
    head_pkfield: 'pk_apply',
    //子表主键字段
    body_pkfield: 'pk_apply_b',
    //缓存上游信息的字段
    sourceInfoField:'sourceInfo',
	//转单缓存标识
    dataSource: 'aum.assign.assigntransfer.apply',
    url :{
        sourceUrl:'/aum/assign/apply/main/#/card',
        nextUrl:'/card',
        initSourceUrl:'/nccloud/ampub/common/querySrcAppInfo.do'
    }
};
