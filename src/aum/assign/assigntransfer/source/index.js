import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast, createPageIcon } from 'nc-lightapp-front';
import { initTemplate, searchBtnClick, initTemplateFull, buttonClick, searchAfterEvent } from './events';
import { pageConfig } from './const';
import ampub from 'ampub';

const { components, commonConst, utils } = ampub;
const { setContext, getContext } = components.LoginContext;
const { TransferConst } = commonConst.CommonKeys;
const { UISTATE } = commonConst.StatusUtils;
const { getMultiLangByID } = utils.multiLangUtils;

const { NCToggleViewBtn, NCAffix } = base;
const {
	headCode,
	searchCode,
	bodyCode,
	mainCode,
	dataSource,
	nextAppCode,
	nextPageCode,
	sourceAppCode,
	sourceInfoField
} = pageConfig;
let { nextUrl, initSourceUrl } = pageConfig.url;

class AssignApplyTransfer extends Component {
	constructor(props) {
		super(props);
		this.state = {
			expand: true
		};
		let transferAppCode = props.getSearchParam('c');
		if (!transferAppCode) {
			transferAppCode = '452001008A';
		}
		let nextBillType = '4A14';
		let sourceBillType = '4A37';
		let defaultSourceTranstype = '4A37-01';
		let data = { transferAppCode, billtype: nextBillType };
		//根据转单应用编码和其注册的参数信息初始化上游信息
		ajax({
			url: initSourceUrl,
			data,
			success: (res) => {
				let sourceInfo = {
					appcode: sourceAppCode,
					billtype: sourceBillType,
					transtype: defaultSourceTranstype,
					nextAppCode: '',
					transferAppCode: transferAppCode,
					busitypes: new Array()
				};
				if (res.data) {
					let srcAppInfo = res.data;
					if (srcAppInfo.length > 0) {
						if (srcAppInfo.length == 1) {
							sourceInfo.billtype = sourceBillType;
							sourceInfo.transtype = srcAppInfo[0].src_transtype;
							sourceInfo.busitypes = srcAppInfo[0].busitypes;
							sourceInfo.nextAppCode = srcAppInfo[0].next_appcode;
						}
					} else {
						toast({
							color: 'warning',
							title: getMultiLangByID('452001008A-000004'),
							content: getMultiLangByID('452001008A-000003')
						});
					}
				} else {
					toast({
						color: 'warning',
						title: getMultiLangByID('452001008A-000004'),
						content: getMultiLangByID('452001008A-000003')
					});
				}
				setContext(sourceInfoField, sourceInfo, dataSource);
				initTemplate.call(this, props); //加载主子模板
				initTemplateFull.call(this, props); //加载主子拉平模板
			}
		});
	}

	render() {
		const { transferTable, form, button, search } = this.props;
		const { NCCreateSearch } = search;
		let { createButtonApp } = button;
		const { createTransferTable, createTransferList } = transferTable;
		let selectedShow = transferTable.getSelectedListDisplay(headCode);
		let btnCreatNextName = getMultiLangByID('452001008A-000000'); /* 生成领用单 */

		return (
			<div id="transferDemo">
				{!selectedShow ? (
					<div>
						<div className="nc-bill-list">
							<NCAffix>
								<div className="nc-bill-header-area">
									<div className="header-title-search-area">
										{createPageIcon()}
										<h2 className="title-search-detail">
											{getMultiLangByID('452001008A-000001') /* 选择领用申请单 */}
										</h2>
									</div>
									<div className="header-button-area">
										<NCToggleViewBtn
											expand={this.state.expand}
											onClick={() => {
												if (!this.props.meta.getMeta()[mainCode]) {
													initTemplateFull(this.props); //加载主子拉平模板
												}
												this.props.transferTable.changeViewType(headCode);
												let expandtemp = !this.state.expand;
												this.setState({ expand: expandtemp });
											}}
										/>
									</div>
								</div>
							</NCAffix>
							<div className="nc-bill-search-area">
								{NCCreateSearch(
									searchCode, //模块id
									{
										clickSearchBtn: searchBtnClick.bind(this),
										onAfterEvent: searchAfterEvent.bind(this)
									}
								)}
							</div>
						</div>
					</div>
				) : (
					''
				)}

				<div className="nc-bill-transferTable-area">
					{createTransferTable({
						searchAreaCode: searchCode,
						tableType: 'full', //选填 表格默认显示的类型 full:主子拉平 nest:主子表
						headTableId: headCode, //表格组件id
						bodyTableId: bodyCode, //子表模板id
						fullTableId: mainCode, //主子拉平模板id
						transferBtnText: btnCreatNextName, //转单按钮显示文字
						containerSelector: '#transferDemo', //容器的选择器 必须唯一,用于设置底部已选区域宽度
						dataSource: dataSource, //单页应用缓存key
						componentInitFinished: () => {
							//缓存数据赋值成功的钩子函数
							//若初始化数据后需要对数据做修改，可以在这里处理
						},

						selectArea: () => {
							//已选列表自定义区域
							return (
								<div className="header-button-area">
									{createButtonApp({
										area: 'bottom',
										buttonLimit: 3,
										onButtonClick: buttonClick.bind(this),
										popContainer: document.querySelector('.header-button-area')
									})}
								</div>
							);
						},
						onTransferBtnClick: (ids) => {
							//清空缓存
							// let { deleteCache } = this.props.transferTable;
							// deleteCache(dataSource);
							let sourceInfo = getContext(sourceInfoField, dataSource);
							if (sourceInfo) {
								//点击转单按钮钩子函数
								this.props.pushTo(nextUrl, {
									appcode: sourceInfo.nextAppCode,
									transferAppCode: sourceInfo.transferAppCode,
									pagecode: nextPageCode,
									status: UISTATE.transferAdd,
									type: TransferConst.type
								});
							} else {
								//点击转单按钮钩子函数
								this.props.pushTo(nextUrl, {
									appcode: nextAppCode,
									transferAppCode: '452001008A',
									pagecode: nextPageCode,
									status: UISTATE.transferAdd,
									type: TransferConst.type
								});
							}
						},
						onChangeViewClick: () => {
							//点击切换视图钩子函数
							if (!this.props.meta.getMeta()[mainCode]) {
								initTemplateFull(this.props); //加载主子拉平模板
							}
							this.props.transferTable.changeViewType(headCode);
						},
						onCheckedChange: (flag, record, index, bodys) => {
							//勾选的回调函数
							if (flag) {
								this.props.button.setButtonDisabled([ 'Close' ], false);
							} else {
								let selectData = this.props.transferTable.getTransferTableSelectedValue();
								if (selectData && selectData.head && selectData.head.length > 0) {
									this.props.button.setButtonDisabled([ 'Close' ], false);
								} else {
									this.props.button.setButtonDisabled([ 'Close' ], true);
								}
							}
						},
						onClearAll: () => {
							this.props.button.setButtonDisabled([ 'Close' ], true);
						}
					})}
				</div>
			</div>
		);
	}
}

AssignApplyTransfer = createPage({})(AssignApplyTransfer);

export default AssignApplyTransfer;
