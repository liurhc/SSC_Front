import { RenderRouter } from 'nc-lightapp-front';
import routes from './router';
import ampub from 'ampub';

const { utils } = ampub;
const { initMultiLangByModule } = utils.multiLangUtils;

(function main(routers, htmlTagid) {
	let moduleIds = { aum: [ '452001012A', '452001008A' ], ampub: [ 'common' ] };
	// 加载多语资源
	initMultiLangByModule(moduleIds, () => {
		// 路由渲染页面
		RenderRouter(routers, htmlTagid);
	});
})(routes, 'app');
