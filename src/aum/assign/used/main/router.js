import { asyncComponent } from 'nc-lightapp-front';

const List = asyncComponent(() =>
	import(/* webpackChunkName: "aum/assign/used/list/list" */ /* webpackMode: "eager" */ '../list')
);
const Card = asyncComponent(() =>
	import(/* webpackChunkName: "aum/assign/used/card/card" */ /* webpackMode: "eager" */ '../card')
);
const AssignApplyTransfer = asyncComponent(() =>
	import(/* webpackChunkName: "aum/assign/assigntransfer/source/source" */ /* webpackMode: "eager" */ '../../assigntransfer/source')
);
const routes = [
	{
		path: '/',
		component: List,
		exact: true
	},
	{
		path: '/list',
		component: List
	},
	{
		path: '/card',
		component: Card
	},
	{
		path: '/transfer',
		component: AssignApplyTransfer
	}
];

export default routes;
