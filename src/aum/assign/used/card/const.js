// 页面配置
export const pageConfig = {
	// 应用编码
	appcode: '452001012A',
	// 应用名称(国际化处理 领用单维护)
	title: '452001012A-000000',
	// 表单区域id
	formId: 'card_head',
	// 表格区域id
	tableId: 'bodyvos',
	// 页面编码
	pagecode: '452001012A_card',
	// 编辑态按钮
	editBtns: [
		'SaveGroup',
		'Cancel',
		'OpenCard',
		'AddLine',
		'DelLine',
		'BatchAlter',
		'QueryAboutEquipReport' //资产台账
	],
	// 浏览态按钮
	browseBtns: [
		'Add',
		'Edit',
		'Delete',
		'Commit',
		'UnCommit',
		'Attachment',
		'OpenCard',
		'QueryAboutBusiness', //单据追溯
		'QueryAboutEquipReport', //资产台账
		'QueryAboutBillFlow', //审批详情
		'Print',
		'Refresh'
	],
	// 主表主键字段
	pkField: 'pk_assign',
	// 子表主键字段
	pkFieldB: 'pk_assign_b',
	// 单据类型
	bill_type: '4A14',
	// 交易类型
	transi_type: '4A14-01',
	// 文件名称(国际化处理 领用单维护)
	printFilename: '452001012A-000000',
	// 请求链接
	url: {
		queryCardUrl: '/nccloud/aum/assign/used_querycard.do',
		insertUrl: '/nccloud/aum/assign/used_insert.do',
		updateUrl: '/nccloud/aum/assign/used_update.do',
		editUrl: '/nccloud/aum/assign/used_edit.do',
		printUrl: '/nccloud/aum/assign/used_printCard.do',
		commitUrl: '/nccloud/aum/assign/used_commit.do',
		unCommitUrl: '/nccloud/aum/assign/used_unCommit.do',
		headAfterEditUrl: '/nccloud/aum/assign/used_headAfterEdit.do',
		bodyAfterEditUrl: '/nccloud/aum/assign/used_bodyAfterEdit.do',
		batchAlterUrl: '/nccloud/aum/assign/used_batchAlter.do',
		getDataBySrcUrl: '/nccloud/aum/assign/used_changevo_srcids.do'
	},
	// 过滤空行相关无效字段
	notFilterFields: [
		'pk_unit_used_after',
		'pk_unit_used_after_v',
		'pk_usedept_after',
		'pk_usedept_after_v',
		'pk_user_after'
	],
	// 组织切换清空字段
	clearFields: [ 'pk_recorder', 'bill_status', 'pk_taker', 'pk_dept', 'pk_dept_v', 'memo', 'take_date' ],
	// 打印模板标识
	printNodekey: null,
	resourceCode: '4520016005',
	listRouter: '/list',
	// 缓存上游信息的字段
	sourceInfoField: 'sourceInfo',
	// 页面缓存标识
	dataSource: 'aum.assign.used.main',
	//转单缓存标识（需要与上游转单保持一致）
	transferDataSource: 'aum.assign.assigntransfer.apply'
};

//转单下游相关信息
export const TRANSFERINFO = {
	pagecode: '452001012A_transfercard',
	leftarea: 'leftarea',
	srctype: 'transfer',
	resourceCode: '4520016005',
	transferRouter: '/transfer'
};
