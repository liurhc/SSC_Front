import { ajax } from 'nc-lightapp-front';
import { setBodyBtnsEnable, addLine } from './buttonClick';
import { pageConfig, TRANSFERINFO } from '../const';
import ampub from 'ampub';

const { components, utils } = ampub;
const { orgChangeEvent } = components.OrgChangeEvent;
const { commonBodyAfterEvent, updateHeadData, updateBodyData } = utils.cardUtils;
const { formId, tableId, url, clearFields } = pageConfig;

/**
 * 表头编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} oldValue 
 * @param {*} val 
 */
export function headAfterEvent(props, moduleId, key, value, oldValue, val) {
	let pagecode = this.pagecode;
	if (key == 'pk_org_v') {
		// 组织切换确定后发送后台请求,做特殊处理
		let callback = () => {
			if (value.value) {
				// 若组织切换后有值则自动增行
				addLine.call(this, props);
				let data = props.createHeadAfterEventData(pagecode, formId, tableId, moduleId, key, value);
				headAfterEditAjax.call(this, props, data);
			}
			setBodyBtnsEnable.call(this, props, tableId);
		};
		//组织切换
		orgChangeEvent.call(this, props, pagecode, formId, tableId, key, value, oldValue, clearFields, false, callback);
	} else if ([ 'pk_taker', 'pk_dept_v' ].includes(key)) {
		// 需要传表体数据到后台，做特殊处理
		let data = props.createHeadAfterEventData(pagecode, formId, tableId, moduleId, key, value);
		headAfterEditAjax.call(this, props, data);
	}
}

/**
 * 表体编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedRows 
 * @param {*} index 
 * @param {*} record 
 * @param {*} type 
 * @param {*} eventType 
 */
export function bodyAfterEvent(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	let pagecode = this.pagecode;
	// 平台的自动增行不会附默认值，这里调用自己的增行,转单不自动增行
	if (this.srctype != TRANSFERINFO.srctype) {
		let num = props.cardTable.getNumberOfRows(tableId);
		if (num == index + 1) {
			addLine.call(this, props);
		}
	}
	let callback = () => {
		if (this.srctype == TRANSFERINFO.srctype) {
			this.synTransferData();
		}
	};
	let config = {
		afterEditUrl: url.bodyAfterEditUrl,
		pagecode,
		key,
		record,
		changedRows,
		index,
		keys: [ 'pk_equip', 'pk_unit_used_after_v', 'pk_user_after' ],
		callback
	};
	commonBodyAfterEvent.call(this, props, config);
}

/**
 * 调后台处理编辑后事件的数据
 * @param {*} props 
 * @param {*} data 
 */
function headAfterEditAjax(props, data) {
	ajax({
		url: url.headAfterEditUrl,
		data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				// 性能优化，关闭表单和表格渲染
				props.beforeUpdatePage();
				updateHeadData.call(this, props, { data });
				updateBodyData.call(this, props, { data });
				if (this.srctype == TRANSFERINFO.srctype) {
					this.synTransferData();
				}
				// 性能优化，表单和表格统一渲染
				props.updatePage(formId, tableId);
			}
		}
	});
}
