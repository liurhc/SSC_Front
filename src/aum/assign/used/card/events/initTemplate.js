import tableButtonClick from './tableButtonClick';
import { setStatus, getDataBySrcId, loadDataByPk, edit, add, setBtnsVisible } from './buttonClick';
import { pageConfig } from '../const';
import ampub from 'ampub';

const { components, commonConst, utils } = ampub;
const { defRefCondition } = components.refInit;
const { openEquipCardByPk } = components.queryAboutUtils;
const { getContext, loginContextKeys, loginContext } = components.LoginContext;
const { IBusiRoleConst } = commonConst.CommonKeys;
const { UISTATE } = commonConst.StatusUtils;
const { createOprationColumn, afterModifyCardMeta } = utils.cardUtils;

let { formId, tableId, bill_type } = pageConfig;

/**
 * 初始化模板
 * @param {*} props 
 */
export default function(props) {
	props.createUIDom(
		{
			pagecode: this.pagecode // 页面编码
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.template) {
					let meta = data.template;
					modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta, () => {
						afterInit.call(this, props);
					});
				}
			}
		}
	);
}

/**
 * 模板初始化以后
 * @param {*} props 
 */
function afterInit(props) {
	let status = props.getUrlParam('status') || UISTATE.browse;
	if (status == 'add') {
		add.call(this, props);
	} else if (status == UISTATE.transferAdd) {
		setStatus.call(this, props, status);
		setBtnsVisible.call(this, props);
		//转单
		getDataBySrcId.call(this, props);
	} else if (status == UISTATE.edit) {
		setStatus.call(this, props, UISTATE.browse);
		setBtnsVisible.call(this, props);
		props.button.setButtonVisible('Add', false);
		let pk = props.getUrlParam('id');
		if (pk && pk != 'undefined' && pk != 'null') {
			let callback = () => {
				edit.call(this, props, false);
			};
			loadDataByPk.call(this, props, pk, callback);
		}
	} else {
		setStatus.call(this, props, status);
		setBtnsVisible.call(this, props);
		let pk = props.getUrlParam('id');
		if (pk && pk != 'undefined' && pk != 'null') {
			let callback = () => {
				setBtnsVisible.call(this, props);
			};
			loadDataByPk.call(this, props, pk, callback);
		}
	}
}

/**
 * 修改模板
 * @param {*} props 
 * @param {*} meta 
 */
function modifierMeta(props, meta) {
	const transi_type = getContext(loginContextKeys.transtype);
	const pk_group = getContext(loginContextKeys.groupId);
	if (meta[formId] && meta[formId].items && meta[formId].items.length > 0) {
		//表头组织过滤
		meta[formId].items.map((item) => {
			// 过滤有权限的组织
			if (item.attrcode == 'pk_org_v') {
				item.queryCondition = () => {
					return { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgVidRefFilter' };
				};
			} else if (item.attrcode == 'pk_recorder' || item.attrcode == 'pk_dept_v' || item.attrcode == 'pk_taker') {
				//经办人和领用部门根据pk_org和设置了权限的业务单元过滤
				item.queryCondition = () => {
					let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
					return {
						pk_org,
						busifuncode: IBusiRoleConst.ASSETORG
					};
				};
			} else {
				//自定义项的过滤
				defRefCondition.call(this, props, item, formId, pk_group);
			}
		});
	}
	if (meta[tableId] && meta[tableId].items && meta[tableId].items.length > 0) {
		// 表体组织过滤
		meta[tableId].items.map((item) => {
			if (item.attrcode == 'pk_equip') {
				item.isMultiSelectedEnabled = true;
				//给资产编码列添加超链接
				item.renderStatus = 'browse';
				item.render = (text, record) => {
					return (
						<div class="card-table-browse">
							<span
								className="code-detail-link"
								onClick={() => {
									openEquipCardByPk.call(this, props, record.values[item.attrcode].value);
								}}
							>
								{record.values[item.attrcode] && record.values[item.attrcode].display}
							</span>
						</div>
					);
				};
				//设置设备的查询条件
				item.queryCondition = () => {
					let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value; // 调用相应组件的取值API
					return {
						pk_org,
						transi_type,
						bill_type,
						GridRefActionExt: 'nccloud.web.aum.assign.used.refcondition.PK_EQUIPSqlBuilder'
					};
				};
			} else if (item.attrcode == 'pk_unit_used_after_v') {
				//使用权
				item.queryCondition = () => {
					let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value; // 调用相应组件的取值API
					return {
						pk_org_unitused: pk_org,
						TreeRefActionExt: 'nccloud.web.aum.borrow.used.refcondition.PK_UNIT_USED_VSqlBuilder',
						DataPowerOperationCode: 'AMUsedunit'
					}; // 根据pk_org过滤
				};
			} else if (item.attrcode == 'pk_user_after' || item.attrcode == 'pk_usedept_after_v') {
				//使用人，使用部门参照当前使用权可见的人或者部门
				item.queryCondition = () => {
					// let rowData = props.cardTable.getClickRowIndex(tableId);
					let index = props.cardTable.getCurrentIndex(tableId);
					let rowData = props.cardTable.getDataByIndex(tableId, index);
					if (rowData && rowData.values) {
						let pk_unit_used_after = rowData.values['pk_unit_used_after'].value;
						return {
							pk_org: pk_unit_used_after,
							busifuncode: IBusiRoleConst.ASSETORG,
							isTreelazyLoad: false
						};
					}
				};
			} else if (item.attrcode == 'pk_location_after') {
				//使用位置参照当前资产组织
				item.queryCondition = () => {
					let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
					return {
						pk_org: pk_org
					};
				};
			} else {
				//自定义项的过滤
				defRefCondition.call(this, props, item, formId, pk_group);
			}
		});
		// 添加表格操作列
		let oprCol = createOprationColumn.call(this, props, { tableButtonClick });
		meta[tableId].items.push(oprCol);
	}
	// 卡片模板修改后公共处理
	meta = afterModifyCardMeta.call(this, props, meta);

	return meta;
}
