import React, { Component } from 'react';
import { createPage, high, base } from 'nc-lightapp-front';
import {
	buttonClick,
	initTemplate,
	headAfterEvent,
	bodyAfterEvent,
	pageInfoClick,
	save,
	transferBtnClick,
	transferSaveCommit,
	transferSave,
	backToList,
	rowSelected,
	commit,
	saveCommit,
	setBtnsVisible,
	setStatus,
	setValue
} from './events';
import { pageConfig, TRANSFERINFO } from './const';
import './index.less';
import ampub from 'ampub';

const { components, commonConst, utils } = ampub;
const { TransferConst } = commonConst.CommonKeys;
const { UISTATE } = commonConst.StatusUtils;
const { getMultiLangByID } = utils.multiLangUtils;
const { createCardTitleArea, createCardPaginationArea, getCommonTableHeadLeft } = utils.cardUtils;
const { closeBrowserUtils } = utils;
const { ApprovalTrans } = components;

const { BillTrack, ApproveDetail } = high;
const { NCAffix } = base;
const { title, formId, tableId, pagecode, bill_type, dataSource, transferDataSource, pkField, pkFieldB } = pageConfig;

/**
 * 主子表卡片页面入口文件
 */
class MasterChildCard extends Component {
	constructor(props) {
		super(props);
		this.state = {
			pk_bill: '',
			showApprove: false,
			compositedisplay: false,
			compositedata: {}
		};

		this.srctype = props.getUrlParam(TransferConst.srctype);
		this.transferAppCode = props.getUrlParam('transferAppCode');
		this.pagecode = pagecode;
		if (this.srctype == TRANSFERINFO.srctype) {
			this.pagecode = TRANSFERINFO.pagecode;
		}
		this.transferIndex = 0; //转单时左侧选择的数据序号
		this.transferEditId = new Array(); //转单后已处理单据又点修改的主键存储
		this.nowUIStatus = props.getUrlParam('status'); //存储界面当前状态

		closeBrowserUtils.call(this, props, { from: [ formId ], cardTable: [ tableId ] });
		initTemplate.call(this, props);
	}

	//设置转单右侧界面数据
	putTransferRightValue = (record, index, status, changeIndex = true) => {
		if (changeIndex) {
			this.transferIndex = index;
		}
		if (status) {
			let headValue = record.head[formId].rows[0];
			let pkFieldValue = headValue.values[pkField];
			if (this.transferEditId.indexOf(pkFieldValue.value) == -1) {
				// 设置状态与值
				setStatus.call(this, this.props, UISTATE.transferBorwse);
				setValue.call(this, this.props, record);
				setBtnsVisible.call(this, this.props);
				return;
			} else {
				setStatus.call(this, this.props, UISTATE.transferEdit);
			}
		} else {
			setStatus.call(this, this.props, UISTATE.transferAdd);
		}
		//点击转单缩略图的钩子函数 status:转单处理状态  先设置页面状态，再设置值
		setValue.call(this, this.props, record);
		setBtnsVisible.call(this, this.props);
	};

	//同步单据信息到转单控件
	synTransferData = () => {
		//重取界面现有数据赋值到转单中
		let cardData = this.props.createMasterChildData(this.pagecode, formId, tableId);
		this.props.transferTable.setTransferListValueByIndex(TRANSFERINFO.leftarea, cardData, this.transferIndex);
	};

	//获取列表合计行信息
	getTableHeadLeft = () => {
		return getCommonTableHeadLeft.call(this, this.props);
	};

	//获取列表肩部信息
	getTableHead = () => {
		let { button } = this.props;
		let { createButtonApp } = button;

		return (
			<div className="shoulder-definition-area">
				<div className="definition-icons">
					{createButtonApp({
						area: 'card_body',
						buttonLimit: 3,
						onButtonClick: (props, id) => {
							buttonClick.call(this, props, id, tableId);
						},
						popContainer: document.querySelector('.header-button-area')
					})}
				</div>
			</div>
		);
	};

	//提交及指派 回调，注意保存提交时候需要调用保存提交的方法
	getAssginUsedr = (content) => {
		let status = this.props.form.getFormStatus(formId) || UISTATE.browse;
		if (status == UISTATE.browse || status == UISTATE.transferBorwse) {
			commit.call(this, this.props, content);
		} else {
			if (this.srctype == 'transfer') {
				transferSaveCommit.call(this, this.props, content);
			} else {
				saveCommit.call(this, this.props, content);
			}
		} //原提交方法添加参数content
	};
	//取消 指派
	turnOff = () => {
		this.setState({
			compositedisplay: false
		});
	};

	render() {
		let { cardTable, form, button, ncUploader, transferTable } = this.props;
		let { createNCUploader } = ncUploader;
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createButtonApp } = button;
		let { leftarea } = TRANSFERINFO;

		let { showBillTrack, pk_bill, showApprove, transi_type, compositedata, compositedisplay } = this.state; // 需要输出的数据

		let srctype = this.srctype;
		const { createTransferList } = transferTable;
		if (srctype == TRANSFERINFO.srctype) {
			return (
				<div className="nc-bill-transferList AssignTransferCard">
					<NCAffix>
						<div className="nc-bill-header-area">
							{/* 标题 title */}
							{createCardTitleArea.call(this, this.props, {
								title: getMultiLangByID(title),
								formId,
								backBtnClick: backToList.bind(this)
							})}
							{/* 按钮区 btn-area */}
							<div className="header-button-area">
								{this.props.button.createButtonApp({
									area: 'card_head',
									buttonLimit: 3,
									onButtonClick: transferBtnClick.bind(this),
									popContainer: document.querySelector('.header-button-area')
								})}
							</div>
						</div>
					</NCAffix>
					<div className="nc-bill-transferList-content">
						{createTransferList({
							headcode: formId,
							transferListId: leftarea, //转单列表id
							dataSource: transferDataSource,
							pkname: pkFieldB,
							componentInitFinished: () => {
								//缓存数据赋值成功的钩子函数
								//若初始化数据后需要对数据做修改，可以在这里处理
							},
							onTransferItemSelected: (record, status, index) => {
								this.putTransferRightValue(record, index, status);
							},
							onTransferItemClick: (record, index, status) => {
								this.putTransferRightValue(record, index, status);
							}
						})}
						<div className="nc-bill-card transferList-content-right">
							<div className="nc-bill-form-area">
								{createForm(formId, {
									onAfterEvent: headAfterEvent.bind(this)
								})}
							</div>
							<div className="nc-bill-bottom-area">
								<div className="nc-bill-table-area">
									{createCardTable(tableId, {
										tableHeadLeft: this.getTableHeadLeft.bind(this),
										tableHead: this.getTableHead.bind(this),
										modelSave: transferSave.bind(this),
										onAfterEvent: bodyAfterEvent.bind(this),
										showCheck: true,
										showIndex: true,
										selectedChange: rowSelected.bind(this)
									})}
								</div>
							</div>
						</div>
						{/* 审批详情 */}
						{pk_bill && (
							<ApproveDetail
								show={showApprove}
								close={() => {
									this.setState({ showApprove: false });
								}}
								billtype={transi_type}
								billid={pk_bill}
							/>
						)}
						{/*单据追溯区*/}
						<BillTrack
							show={showBillTrack}
							close={() => {
								this.setState({
									showBillTrack: false
								});
							}}
							pk={pk_bill}
							type={bill_type}
						/>
						{/* 提交及指派 */}
						{this.state.compositedisplay ? (
							<ApprovalTrans
								title={getMultiLangByID('amcommon-000002')}
								data={compositedata}
								display={compositedisplay}
								getResult={this.getAssginUsedr}
								cancel={this.turnOff}
							/>
						) : (
							''
						)}
						{/* 附件管理框 */}
						{createNCUploader(`${this.pagecode}-uploader`, {})}
					</div>
				</div>
			);
		}

		return (
			<div className="nc-bill-card">
				<div className="nc-bill-top-area">
					<NCAffix>
						<div className="nc-bill-header-area">
							{/* 标题 title */}
							{createCardTitleArea.call(this, this.props, {
								title: getMultiLangByID(title),
								formId,
								backBtnClick: backToList.bind(this)
							})}
							{/* 按钮区 btn-area */}
							<div className="header-button-area">
								{createButtonApp({
									area: 'card_head',
									buttonLimit: 3,
									onButtonClick: buttonClick.bind(this),
									popContainer: document.querySelector('.header-button-area')
								})}
							</div>
							{/* 分页区 pagination-area */}
							{createCardPaginationArea.call(this, this.props, {
								formId,
								dataSource,
								pageInfoClick
							})}
						</div>
					</NCAffix>
					<div className="nc-bill-form-area">
						{/* 表单区 form-area */}
						{createForm(formId, {
							onAfterEvent: headAfterEvent.bind(this)
						})}
					</div>
				</div>
				<div className="nc-bill-bottom-area">
					<div className="nc-bill-table-area">
						{/* 表格区 table-area */}
						{createCardTable(tableId, {
							tableHeadLeft: this.getTableHeadLeft.bind(this),
							tableHead: this.getTableHead.bind(this),
							modelSave: save.bind(this),
							onAfterEvent: bodyAfterEvent.bind(this),
							showCheck: true,
							showIndex: true,
							selectedChange: rowSelected.bind(this)
						})}
					</div>
				</div>
				{/* 审批详情 */}
				<ApproveDetail
					show={showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={transi_type}
					billid={pk_bill}
				/>
				{/*单据追溯区*/}
				<BillTrack
					show={showBillTrack}
					close={() => {
						this.setState({
							showBillTrack: false
						});
					}}
					pk={pk_bill}
					type={bill_type}
				/>
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002')}
						data={compositedata}
						display={compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				) : (
					''
				)}
				{/* 附件管理框 */}
				{createNCUploader(`${this.pagecode}-uploader`, {})}
			</div>
		);
	}
}

const MasterChildCardBase = createPage({
	billinfo: {
		billtype: 'card',
		// 此处仅传领用单维护的pagecode，因为未设置领用申请处理转单下游的元数据编辑关联项，
		// 默认两者元数据编辑关联项相同，若后续存在不同，可传入pagecode为null，会自动获取
		pagecode: pagecode,
		headcode: formId,
		bodycode: tableId
	}
})(MasterChildCard);
// const MasterChildCardBase = createPage({})(MasterChildCard);

// 渲染页面
// ReactDOM.render(<MasterChildCardBase pageConfig={pageConfig} />, document.querySelector('#app'));

export default MasterChildCardBase;
