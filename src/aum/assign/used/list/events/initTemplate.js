import tableButtonClick from './tableButtonClick';
import { linkToCard, setBatchBtnsEnable } from './buttonClick';
import { pageConfig } from '../const';
import ampub from 'ampub';

const { components, utils } = ampub;
const { refCommonFilterForSearchArea, appPermissionOrgRefFilter, commonRefFilter } = components.RefFilterForSearchArea;
const { loginContext } = components.LoginContext;
const { getMultiLangByID } = utils.multiLangUtils;
const { createOprationColumn } = utils.listUtils;

const { searchAreaId, tableId, pagecode } = pageConfig;

/**
 * 模板初始化
 * @param {*} props 
 */
export default function(props) {
	props.createUIDom(
		{
			// 页面编码
			pagecode
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					// 行删除时悬浮框提示
					props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001')); /* 国际化处理：确定要删除吗？ SVG*/
				}
				if (data.template) {
					let meta = data.template;
					modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta, () => {
						afterInit.call(this, props);
					});
				}
			}
		}
	);
}

/**
 * 模板初始化以后
 * @param {*} props 
 */
function afterInit(props) {
	// let searchVal = cacheTools.get(`${pagecode}_searchParams`);
	// if (searchVal) {
	// 	props.search.setSearchValue(searchAreaId, searchVal.conditions);
	// 	searchBtnClick.call(this, props, searchVal);
	// }
	setBatchBtnsEnable.call(this, props, tableId);
}

/**
 * 修改模板
 * @param {*} props 
 * @param {*} meta 
 */
function modifierMeta(props, meta) {
	// 查询区字段参照过滤
	meta[searchAreaId].items.map((item) => {
		let filterByPkOrg = [
			'pk_dept',
			'pk_taker',
			'pk_recorder',
			'bodyvos.pk_jobmngfil',
			'bodyvos.pk_location_after'
		];
		let filterByboddUsedUnit = [ 'bodyvos.pk_usedept_after', 'bodyvos.pk_user_after' ];
		// 查询区公共过滤
		refCommonFilterForSearchArea.call(this, props, item, searchAreaId);
		// 组织权限过滤
		if (
			item.attrcode.endsWith('pk_org') ||
			item.attrcode.endsWith('pk_usedorg') ||
			item.attrcode.endsWith('pk_ownerorg')
		) {
			appPermissionOrgRefFilter.call(this, item);
		}
		if (filterByPkOrg.includes(item.attrcode)) {
			// 部门、人员、经办人、项目根据资产组织过滤
			commonRefFilter.call(this, props, item, searchAreaId, filterByPkOrg, 'pk_org');
		} else if (filterByboddUsedUnit.includes(item.attrcode)) {
			// 根据表体使用权过滤
			commonRefFilter.call(this, props, item, searchAreaId, filterByboddUsedUnit, 'bodyvos.pk_usedunit');
		} else if (item.attrcode == 'bodyvos.pk_unit_used_after') {
			// 表体使用权根据表体使用管理组织过滤
			commonRefFilter.call(
				this,
				props,
				item,
				searchAreaId,
				[ 'bodyvos.pk_unit_used_after' ],
				'bodyvos.pk_org',
				'pk_org_unitused',
				{
					'bodyvos.pk_unit_used_after': {
						TreeRefActionExt: 'nccloud.web.aum.borrow.used.refcondition.PK_USEDUNIT_IN_VSqlBuilder'
					}
				}
			);
		}
	});
	//修改列渲染样式
	meta[tableId].items.map((item, key) => {
		if (item.attrcode == 'bill_code') {
			item.render = (text, record, index) => {
				return (
					<div class="simple-table-td" field="bill_code" fieldname="assign_bill_code">
						<span
							className="code-detail-link"
							onClick={() => {
								linkToCard.call(this, props, record, 'browse');
							}}
						>
							{record && record.bill_code && record.bill_code.value}
						</span>
					</div>
				);
			};
		}
		return item;
	});

	// 添加表格操作列
	let oprCol = createOprationColumn.call(this, props, { tableButtonClick });
	meta[tableId].items.push(oprCol);
	return meta;
}
