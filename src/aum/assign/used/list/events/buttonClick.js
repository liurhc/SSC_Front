import { ajax, output, print } from 'nc-lightapp-front';
import searchBtnClick from './searchBtnClick';
import { pageConfig } from '../const';
import ampub from 'ampub';

const { components, utils } = ampub;
const { getMultiLangByID } = utils.multiLangUtils;
const { showConfirm, MsgConst, showMessage } = utils.msgUtils;
const { batchRefresh, setBillFlowBtnsEnable } = utils.listUtils;
const { getScriptListReturn } = components.ScriptReturnUtils;

const { tableId, pagecode, printFilename, pkField, url, printNodekey, cardRouter, dataSource } = pageConfig;

/**
 * 按钮点击
 * @param {*} props 
 * @param {*} id 
 */
export default function(props, id) {
	switch (id) {
		case 'Add':
			add.call(this, props);
			break;
		case 'Delete':
			delConfirm.call(this, props);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'Commit':
			commit.call(this, props);
			break;
		case 'UnCommit':
			unCommit.call(this, props);
			break;
		case 'Refresh':
			refresh.call(this, props);
			break;
		case 'QueryAboutBusiness':
			queryAboutBusiness.call(this, props);
			break;
		case 'Attachment':
			attachment.call(this, props);
			break;
		default:
			break;
	}
}

/**
 * 新增
 * @param {*} props 
 */
export function add(props) {
	linkToCard.call(this, props, undefined, 'add');
}

/**
 * 批量删除
 * @param {*} props 
 */
export function delConfirm(props) {
	let checkedRows = props.table.getCheckedRows(tableId);
	if (checkedRows.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseDelete });
		return;
	}
	showConfirm.call(this, props, { type: MsgConst.Type.DelSelect, beSureBtnClick: batchDel });
}

/**
 * 跳转卡片
 * @param {*} props 
 * @param {*} record 
 */
export function linkToCard(props, record = {}, status = 'browse') {
	props.pushTo(cardRouter, {
		pagecode: pagecode.replace('list', 'card'),
		status,
		id: record[pkField] ? record[pkField].value : ''
	});
}

/**
 * 设置批量按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBatchBtnsEnable(props, moduleId) {
	setBillFlowBtnsEnable.call(this, props, { tableId: moduleId });
}

/**
 * 打印
 * @param {*} props 
 */
export function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		url.printUrl, // 后台打印服务url
		printData
	);
}

/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });
		return;
	}
	output({
		url: url.printUrl,
		data: printData
	});
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let checkedRows = props.table.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length == 0) {
		return false;
	}
	let pks = [];
	checkedRows.map((item) => {
		pks.push(item.data.values[pkField].value);
	});
	let printData = {
		filename: getMultiLangByID(printFilename) /*国际化处理 借用单维护 svg */,
		nodekey: printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

/**
 * 批量删除
 * 
 * @param {*} props 
 */
function batchDel(props) {
	let data = props.table.getCheckedRows(tableId);
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values[pkField].value;
		let ts = v.data.values['ts'].value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	ajax({
		url: url.commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: pagecode.replace('list', 'card'),
			commitType: 'commit' //提交
		},
		success: (res) => {
			getScriptListReturn.call(
				this,
				params,
				res,
				props,
				pkField,
				tableId.replace('list', 'card'),
				tableId,
				true,
				dataSource
			);
		}
	});
}

/**
 * 提交
 * 
 * @param {*} props 
 */
export function commit(props, content) {
	runScript.call(this, props, 'SAVE', content);
}

/**
 * 收回
 * 
 * @param {*} props 
 */
function unCommit(props) {
	runScript.call(this, props, 'UNSAVE');
}

/**
 * 提交
 * 
 * @param {*} props 
 */
function runScript(props, actionType, content) {
	let data = props.table.getCheckedRows(tableId);
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values[pkField].value;
		let ts = v.data.values['ts'].value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	ajax({
		url: url.commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: actionType,
			pageid: pagecode.replace('list', 'card'),
			commitType: 'commit', //提交
			content: content
		},
		success: (res) => {
			if (content) {
				this.setState({
					compositedisplay: false
				});
			}
			getScriptListReturn.call(
				this,
				params,
				res,
				props,
				pkField,
				tableId.replace('list', 'card'),
				tableId,
				false,
				dataSource
			);
		}
	});
}

/**
* 刷新
* @param {*} props 
*/
export function refresh(props) {
	batchRefresh.call(this, props, searchBtnClick);
}

/**
 * 单据追溯
 * @param {*} props 
 */
function queryAboutBusiness(props) {
	let data = props.table.getCheckedRows(tableId);
	if (!data || data.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOne });
		return;
	}
	let pkVal = data[0].data.values[pkField];
	if (pkVal && pkVal.value) {
		this.setState({
			pk_bill: pkVal.value,
			showBillTrack: true
		});
	}
}

/**
 * 附件管理
 * @param {*} props 
 */
function attachment(props) {
	let checkedrows = props.table.getCheckedRows(tableId);
	if (!checkedrows || checkedrows.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOne });
		return;
	}
	// billNo 是单据编码
	let billNo = checkedrows[0].data.values['bill_code'].value;
	// billId 是单据主键
	let billId = checkedrows[0].data.values[pkField].value;
	props.ncUploader.show(`${pagecode}-uploader`, {
		billId: 'pam/assign/' + billId,
		billNo
	});
}
