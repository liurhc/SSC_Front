import { ajax } from 'nc-lightapp-front';
import { linkToCard } from './buttonClick';
import { pageConfig } from '../const';
import ampub from 'ampub';

const { components, commonConst, utils } = ampub;
const { openListApprove } = components.queryAboutUtils;
const { UISTATE } = commonConst.StatusUtils;
const { showMessage } = utils.msgUtils;
const { getScriptListReturn } = components.ScriptReturnUtils;

const { tableId, pagecode, pkField, url, resourceCode, dataSource } = pageConfig;

/**
 * 表格操作按钮
 * @param {*} props 
 * @param {*} key 
 * @param {*} text 
 * @param {*} record 
 * @param {*} index 
 */
export default function(props, key, text, record, index, tableId) {
	switch (key) {
		case 'Edit':
			edit.call(this, props, record, index);
			break;
		case 'Delete':
			singleDel.call(this, props, record, index);
			break;
		case 'Commit':
			commit.call(this, props, record, index);
			break;
		case 'UnCommit':
			unCommit.call(this, props, record, index);
			break;
		case 'QueryAboutBillFlow':
			openListApprove.call(this, this, record, pkField);
			break;
		default:
			break;
	}
}

/**
 * 表格行修改
 * @param {*} props 
 * @param {*} record 
 */
function edit(props, record) {
	let pk = record[pkField].value;
	let data = {
		pk,
		resourceCode
	};
	ajax({
		url: url.editUrl,
		data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					showMessage.call(this, props, { color: 'warning', content: data });
				} else {
					linkToCard.call(this, props, record, UISTATE.edit);
				}
			}
		}
	});
}

/**
 * 删除
 * 
 * @param {*} props 
 */
function singleDel(props, record, index) {
	let id = record[pkField].value;
	let ts = record['ts'].value;
	let params = [
		{
			id,
			index
		}
	];
	let paramInfoMap = {
		[id]: ts
	};

	ajax({
		url: url.commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: pagecode.replace('list', 'card'),
			commitType: 'commit' //提交
		},
		success: (res) => {
			getScriptListReturn.call(
				this,
				params,
				res,
				props,
				pkField,
				tableId.replace('list', 'card'),
				tableId,
				true,
				dataSource
			);
		}
	});
}

/**
 * 提交
 * 
 * @param {*} props 
 */
function commit(props, record, index) {
	runScript.call(this, props, 'SAVE', record, index);
}

/**
 * 提交
 * 
 * @param {*} props 
 */
function unCommit(props, record, index) {
	runScript.call(this, props, 'UNSAVE', record, index);
}

/**
 * 提交
 * 
 * @param {*} props 
 */
function runScript(props, actionType, record, index) {
	let id = record[pkField].value;
	let ts = record['ts'].value;
	let params = [
		{
			id,
			index
		}
	];
	let paramInfoMap = {
		[id]: ts
	};

	ajax({
		url: url.commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: actionType,
			pageid: pagecode.replace('list', 'card'),
			commitType: 'commit' //提交
		},
		success: (res) => {
			getScriptListReturn.call(
				this,
				params,
				res,
				props,
				pkField,
				tableId.replace('list', 'card'),
				tableId,
				false,
				dataSource
			);
		}
	});
}
