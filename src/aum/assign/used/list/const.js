// 页面配置
export const pageConfig = {
	// 应用主键
	appid: '0001Z910000000004NSJ',
	// 应用编码
	appcode: '452001012A',
	// 应用名称(国际化处理 领用单维护)
	title: '452001012A-000000',
	// 查询区域id
	searchAreaId: 'searchArea',
	// 表格区域id
	tableId: 'list_head',
	// 列表页面编码
	pagecode: '452001012A_list',
	// 卡片页面编码
	cardPageId: '452001012A_card',
	// 主键字段
	pkField: 'pk_assign',
	// 单据类型
	bill_type: '4A14',
	// 交易类型
	transi_type: '4A14-01',
	// 文件名称(国际化处理 领用单维护)
	printFilename: '452001012A-000000',
	// 请求链接
	url: {
		queryPageUrl: '/nccloud/aum/assign/used_querypagegridbypks.do',
		queryUrl: '/nccloud/aum/assign/used_query.do',
		printUrl: '/nccloud/aum/assign/used_printCard.do',
		commitUrl: '/nccloud/aum/assign/used_commit.do',
		unCommitUrl: '/nccloud/aum/assign/used_unCommit.do',
		editUrl: '/nccloud/aum/assign/used_edit.do'
	},
	// 打印模板所在NC节点及标识
	printFuncode: '4520032005',
	printNodekey: null,
	cardUrl: '/aum/assign/used/card/index.html',
	resourceCode: '4520016005',
	cardRouter: '/card',
	dataSource: 'aum.assign.used.main'
};
