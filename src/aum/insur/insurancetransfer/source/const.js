export const pageConfig = {
	// 主子结构页面编码
	pageCode: '452003504A_transferlist',
	pageCodeCard: '452003504A_card',
	//小应用主键
	appId: '0001Z910000000000LO6',
	appCode: '452003504A',
	//表头列表code
	headCode: 'head',
	//表体列表code
	bodyCode: 'bodyvos',
	//主子拉平页面编码
	mainPageCode: '452003504A_transfermain',
	//主子拉平显示面板code
	mainCode: 'transfermain',
	//查询区域code
	searchCode: 'search',
	//查询区域模板主键
	search_templetid: '1001Z910000000007Z1G',
	//主表主键字段
	headPkField: 'pk_insurance',
	//子表主键字段
	bodyPkField: 'pk_insurance_b',
	// 单据类型
	billType: '4A23',
	// 交易类型
	transType: '4A23-01',
	//缓存命名空间
	dataSourceTransfer: 'aum.insur.insurancetransfer.source',
	//下游卡片路由
	cardRouter: '/card',
	// 上游卡片页面路由，超链接用
	sourceUrl: '/aum/insur/insurance/main/#/card',
	// 上游卡片页面的pagecode
	sourcePageCode: '452003504A_card',
	srcinsurance_query: '/nccloud/aum/insurance/srcinsurance_query.do',
	compensationNm: '452003504A-000006' /* 国际化处理：生成资产索赔*/,
	payAbleNm: '452003504A-000007' /* 国际化处理：生成应付单*/
};
