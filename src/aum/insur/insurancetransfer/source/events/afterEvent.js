import { pageConfig } from '../const';
import ampub from 'ampub';
const { components } = ampub;
const { assetOrgMultiRefFilter } = components;
const { isMultiCorpRefHandler } = assetOrgMultiRefFilter;

const { searchCode } = pageConfig;

export default function afterEvent(key, value) {
	if (key === 'pk_org') {
		isMultiCorpRefHandler.call(this, this.props, value, searchCode, [ 'pk_recorder' ]);
	} else if (key === 'bodyvos.pk_equip.pk_usedunit') {
		isMultiCorpRefHandler.call(this, this.props, value, searchCode, [
			'bodyvos.pk_equip.pk_usedept',
			'bodyvos.pk_equip.pk_user'
		]);
	}
}
