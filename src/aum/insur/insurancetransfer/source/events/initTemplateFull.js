import { pageConfig } from '../const';
import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { transferUtils } = utils;
const { LoginContext } = components;
const { getContext } = LoginContext;
const { addLinkToTemplateFull } = transferUtils;
const { CommonKeys } = commonConst;
const { TransferConst } = CommonKeys;
const { mainPageCode, appCode, mainCode, sourceUrl, sourcePageCode, headPkField } = pageConfig;

//主子拉平模板
export default function initTemplateFull(props) {
	let query_appcode = appCode;
	let appcodeCache = getContext(TransferConst.query_appcode, TransferConst.dataSource);
	if (appcodeCache) {
		query_appcode = appcodeCache;
	}
	props.createUIDom(
		{
			pagecode: mainPageCode, //页面id
			appcode: query_appcode //注册按钮的id
		},
		(data) => {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta, query_appcode);
					props.meta.addMeta(meta);
				}
			}
		}
	);
}

function modifierMeta(props, meta, sourceAppCode) {
	meta = addLinkToTemplateFull(props, meta, mainCode, sourceUrl, sourcePageCode, sourceAppCode, headPkField);
	return meta;
}
