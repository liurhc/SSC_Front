export const pageConfig = {
	// 主子结构页面编码
	pageCode: '452003508A_transfer',
	pageCodeCard: '452003504A_card',
	appCode: '452003508A',
	//表头列表code
	headCode: 'head',
	//表体列表code
	bodyCode: 'bodyvos',
	//主子拉平页面编码
	mainPageCode: '452003508A_main',
	//主子拉平显示面板code
	mainCode: 'transfermain',
	//查询区域code
	searchCode: 'search',
	//主表主键字段
	headPkField: 'pk_compensation',
	//子表主键字段
	bodyPkField: 'pk_compensation_b',
	// 单据类型
	billType: '4A49',
	// 交易类型
	transType: '4A49-01',
	//缓存命名空间
	dataSourceTransfer: 'aum.insur.compensationtransfer.source',
	//下游卡片路由
	cardRouter: '/card',
	// 上游卡片页面路由，超链接用
	sourceUrl: '/aum/insur/compensation/main/#/card',
	// 上游卡片页面的pagecode
	sourcePageCode: '452003508A_card',
	srccompensation_query: '/nccloud/aum/compensation/srccompensation_query.do'
};
