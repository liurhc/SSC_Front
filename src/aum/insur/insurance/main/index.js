import { RenderRouter } from 'nc-lightapp-front';
import routes from './router';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { initMultiLangByModule } = multiLangUtils;
(function main(routers, htmlTagid) {
	initMultiLangByModule({ aum: [ '452003504A' ], ampub: [ 'common' ] }, () => {
		RenderRouter(routers, htmlTagid);
	});
})(routes, 'app');
