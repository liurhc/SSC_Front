//主子表列表
import React, { Component } from 'react';
import { createPage, high, base, createPageIcon } from 'nc-lightapp-front';
import {
	buttonClick,
	initTemplate,
	searchBtnClick,
	pageInfoClick,
	tableModelConfirm,
	doubleClick,
	rowSelected,
	afterEvent,
	commitAction,
	setBatchBtnsEnable
} from './events';
const { NCAffix } = base;
import { pageConfig } from './const';
const { bill_type, tableId, searchId, dataSource, title, pageId } = pageConfig;

import ampub from 'ampub';
const { utils, components } = ampub;
const { ApprovalTrans, queryVocherSrcUtils } = components;
const { multiLangUtils } = utils;
const { queryVoucherSrc } = queryVocherSrcUtils;
const { getMultiLangByID } = multiLangUtils;
class List extends Component {
	constructor(props) {
		super(props);
		this.searchId = searchId;
		this.tableId = tableId;
		this.transi_type = '';
		this.state = {
			bill_type: bill_type,
			show: false,
			showApprove: false,
			pk_insurance: '',
			transi_type: '',
			compositedisplay: false,
			compositedata: {}
		};
		initTemplate.call(this, props);
	}
	componentDidMount() {
		//凭证联查来源单据
		queryVoucherSrc(this.props, tableId, 'pk_insurance', pageId);
	}
	//提交及指派 回调
	getAssginUsedr = (value) => {
		commitAction.call(this, 'SAVE', this.props, value);
	};
	//取消 指派
	turnOff = () => {
		this.props.table.selectAllRows(this.tableId, false);
		setBatchBtnsEnable.call(this, this.props, this.tableId);
		this.setState({
			compositedisplay: false
		});
	};
	render() {
		let { table, button, search, ncmodal, ncUploader } = this.props;
		let { createNCUploader } = ncUploader;
		let { createModal } = ncmodal;
		let buttons = this.props.button.getButtons();
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { createButtonApp } = button;
		const { BillTrack, ApproveDetail, PrintOutput } = high;
		return (
			<div className="nc-bill-list">
				{createNCUploader('warcontract-uploader', {})}
				<NCAffix>
					<div className="nc-bill-header-area">
						<div className="header-title-search-area">
							{createPageIcon()}
							<h2 className="title-search-detail">{getMultiLangByID(title)}</h2>
						</div>
						<div className="header-button-area">
							{this.props.button.createButtonApp({
								area: 'list_head',
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
						</div>
					</div>
				</NCAffix>
				<div className="nc-bill-search-area">
					{NCCreateSearch(this.searchId, {
						clickSearchBtn: searchBtnClick.bind(this),
						onAfterEvent: afterEvent.bind(this),
						dataSource: dataSource,
						pkname: 'pk_insurance'
					})}
				</div>

				<div className="nc-bill-table-area">
					{createSimpleTable(this.tableId, {
						handlePageInfoChange: pageInfoClick,
						tableModelConfirm: tableModelConfirm,
						showIndex: true,
						showCheck: true,
						onRowDoubleClick: doubleClick.bind(this),
						onSelected: rowSelected.bind(this),
						onSelectedAll: rowSelected.bind(this),
						dataSource: dataSource,
						pkname: 'pk_insurance',
						componentInitFinished: () => {
							//缓存数据赋值成功的钩子函数
							//若初始化数据后需要对数据做修改，可以在这里处理
							setBatchBtnsEnable.call(this, this.props, tableId);
						}
					})}
				</div>

				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transi_type}
					billid={this.state.pk_insurance}
				/>
				<BillTrack
					show={this.state.show}
					close={() => {
						this.setState({ show: false });
					}}
					pk={this.state.pk_insurance} //单据id
					type={this.state.bill_type} //单据类型
				/>
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002')}
						/* 国际化处理： 指派*/
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				) : (
					''
				)}
				{createModal(`${tableId}-del-confirm`, { color: 'warning' })}
			</div>
		);
	}
}

List = createPage({})(List);

// ReactDOM.render(<List />, document.querySelector('#app'));
export default List;
