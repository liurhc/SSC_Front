import { pageConfig } from '../const';
const { cardRouter, card_pageId } = pageConfig;
export default function doubleClick(record, index, props) {
	props.pushTo(cardRouter, {
		pagecode: card_pageId,
		status: 'browse',
		id: record.pk_insurance.value
	});
}
