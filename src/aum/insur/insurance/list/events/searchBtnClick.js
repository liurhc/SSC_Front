import { ajax, toast } from 'nc-lightapp-front';

import { pageConfig } from '../const';
import ampub from 'ampub';
const { utils } = ampub;
const { listUtils } = utils;
const { setQueryInfoCache, setListValue, listConst } = listUtils;
const { bill_type, tableId, pageId, searchId, list_query } = pageConfig;
import { setBatchBtnsEnable } from './buttonClick';

//点击查询，获取查询区数据
export default function clickSearchBtn(props, searchVal, type, queryInfo, isRefresh) {
	if (!searchVal) {
		return;
	}
	let pageInfo = props.table.getTablePageInfo(tableId);
	if (!queryInfo) {
		queryInfo = props.search.getQueryInfo(searchId);
	}
	queryInfo.pagecode = pageId;
	queryInfo.pageInfo = pageInfo;
	queryInfo.billtype = bill_type;
	queryInfo.transtype = this.transi_type;
	// 缓存查询条件
	setQueryInfoCache.call(this, queryInfo, isRefresh, type);
	ajax({
		url: list_query,
		data: queryInfo,
		success: (res) => {
			setListValue.call(
				this,
				props,
				res,
				tableId,
				isRefresh ? listConst.SETVALUETYPE.refresh : listConst.SETVALUETYPE.query
			);
			setBatchBtnsEnable.call(this, props, tableId);
		}
	});
}
