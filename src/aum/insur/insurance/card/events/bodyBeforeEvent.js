import { toast } from 'nc-lightapp-front';
import { pageConfig } from '../const';
const { formId } = pageConfig;
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
export default function bodyBeforeEvent(props, moduleId, key, value, index, record) {
	let editFlag = true;
	refCondtion.call(this, props, moduleId, key, value, index, record);
	editFlag = isEdit.call(this, props, moduleId, key, value, index, record);
	return editFlag;
}
//设备有值，物料编码、设备名称、设备类别、规格、型号、数量不允许编辑
function isEdit(props, moduleId, key, value, index, record) {
	let pk_org = props.form.getFormItemsValue(formId, 'pk_org');
	if (!pk_org) {
		toast({ content: getMultiLangByID('452003504A-000002') /* 国际化处理： 请选择资产组织!*/, color: 'warning' });
		return false;
	}
	return true;
}
//表体字段参照处理
function refCondtion(props, moduleId, key, value, index, record) {
	let meta = props.meta.getMeta();
	props.meta.setMeta(meta);
}
