import { ajax, toast } from 'nc-lightapp-front';
import { pageConfig } from '../const';
const { bodyAfterEditUrl, form_tableId, card_pageId, formId, headAfterEditUrl } = pageConfig;
import { toggleShow } from './buttonClick';
import ampub from 'ampub';
const { utils, components } = ampub;
const { cardUtils, multiLangUtils } = utils;
const { OrgChangeEvent } = components;
const { commonHeadAfterEvent, commonBodyAfterEvent, updateHeadData, updateBodyData } = cardUtils;
const { getMultiLangByID } = multiLangUtils;
const { orgChangeEvent } = OrgChangeEvent;

export default function afterEvent(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	if (moduleId == formId) {
		headAfterEvent.call(this, props, moduleId, key, value, changedRows, index, record, type, eventType);
	} else if (moduleId == form_tableId) {
		bodyAfterEvent.call(this, props, moduleId, key, value, changedRows, index, record, type, eventType);
	}
}

/**
 * 表头编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} oldValue 
 * @param {*} val 
 */
export function headAfterEvent(props, moduleId, key, value, oldValue, val) {
	if (key === 'pk_org_v') {
		//清空表头和主组织相关字段-和原nc保持一致
		let fields = [ 'pk_recorder', 'pk_fiorg' ];
		let callback = () => {
			let callback1 = () => {
				if (value.value != undefined) {
					props.cardTable.addRow(form_tableId, undefined, undefined, false);
				}
				toggleShow.call(this);
			};
			let config = {
				afterEditUrl: headAfterEditUrl,
				pagecode: card_pageId,
				key,
				value,
				oldValue,
				callback: callback1
			};
			commonHeadAfterEvent.call(this, props, config);

			// let data = props.createHeadAfterEventData(card_pageId, formId, form_tableId, moduleId, key, value);
			// ajax({
			// 	url: headAfterEditUrl,
			// 	data: data,
			// 	success: (res) => {
			// 		if (res.success) {
			// 			if (res.data) {
			// 				if (res.data.head && res.data.head[formId]) {
			// 					this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
			// 				}
			// 				if (res.data.body && res.data.body[form_tableId]) {
			// 					this.props.cardTable.setTableData(form_tableId, res.data.body[form_tableId]);
			// 				}
			// 				if (value.value != undefined) {
			// 					props.cardTable.addRow(form_tableId, undefined, undefined, false);
			// 				}
			// 				toggleShow.call(this);
			// 			}
			// 		}
			// 	}
			// });
		};
		//组织切换
		orgChangeEvent.call(
			this,
			props,
			card_pageId,
			formId,
			form_tableId,
			key,
			value,
			oldValue,
			fields,
			[ 'AddLine', 'BatchAlter' ],
			callback
		);
	}

	//财务组织切换
	if (key === 'pk_fiorg_v') {
		if (value.value != undefined) {
			let config = {
				afterEditUrl: headAfterEditUrl,
				pagecode: card_pageId,
				key,
				value,
				oldValue,
				keys: [ 'pk_fiorg_v' ]
			};
			commonHeadAfterEvent.call(this, props, config);

			// let data = props.createHeadAfterEventData(card_pageId, formId, form_tableId, moduleId, key, value);
			// ajax({
			// 	url: headAfterEditUrl,
			// 	data: data,
			// 	success: (res) => {
			// 		if (res.success) {
			// 			if (res.data) {
			// 				if (res.data.head && res.data.head[formId]) {
			// 					this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
			// 				}
			// 				if (res.data.body && res.data.body[form_tableId]) {
			// 					this.props.cardTable.setTableData(form_tableId, res.data.body[form_tableId]);
			// 				}
			// 			}
			// 		}
			// 	}
			// });
		}
	}

	if (key === 'money_insurance_bill') {
		if (value) {
			let config = {
				afterEditUrl: headAfterEditUrl,
				pagecode: card_pageId,
				key,
				value,
				oldValue,
				keys: [ 'money_insurance_bill' ]
			};
			commonHeadAfterEvent.call(this, props, config);

			// let data = props.createHeadAfterEventData(card_pageId, formId, form_tableId, moduleId, key, value);
			// ajax({
			// 	url: headAfterEditUrl,
			// 	data: data,
			// 	success: (res) => {
			// 		if (res.success) {
			// 			if (res.data) {
			// 				if (res.data.head && res.data.head[formId]) {
			// 					this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
			// 				}
			// 				if (res.data.body && res.data.body[form_tableId]) {
			// 					this.props.cardTable.setTableData(form_tableId, res.data.body[form_tableId]);
			// 				}
			// 			}
			// 		}
			// 	}
			// });
		}
	}
}

/**
 * 表体编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedRows 
 * @param {*} index 
 * @param {*} record 
 * @param {*} type 
 * @param {*} eventType 
 */
export function bodyAfterEvent(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	let data = props.createBodyAfterEventData(card_pageId, formId, form_tableId, moduleId, key, changedRows, index);
	if (key === 'money_insurance_org') {
		if (value) {
			if (value < 0) {
				toast({
					content: getMultiLangByID('452003504A-000009') /* 国际化处理： 输入值不能小于0*/,
					color: 'warning'
				});
				props.cardTable.setValByKeyAndIndex(form_tableId, index, key, {
					value: ''
				});
				return;
			}
			// let config = {
			// 	afterEditUrl: bodyAfterEditUrl,
			// 	pagecode: form_tableId,
			// 	key,
			// 	record,
			// 	changedRows,
			// 	index,
			// 	keys: [ 'money_insurance_org' ]
			// };
			// commonBodyAfterEvent.call(this, props, config);

			ajax({
				url: bodyAfterEditUrl,
				data,
				async: false,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						// 性能优化，关闭表单和表格渲染
						props.beforeUpdatePage();
						updateHeadData.call(this, props, { data });
						updateBodyData.call(this, props, { data, index: 0 });
						// 性能优化，表单和表格统一渲染
						props.updatePage(this.formId, this.tableId);
					}
				}
			});
		}
	}

	//调试开始日期 和 结束时间编辑后校验
	if (key === 'begin_date' || key === 'end_date') {
		if (value) {
			let begin_date = props.cardTable.getValByKeyAndIndex(form_tableId, index, 'begin_date').value;
			let end_date = props.cardTable.getValByKeyAndIndex(form_tableId, index, 'end_date').value;
			if (begin_date && end_date) {
				if (CompareDate(begin_date.substring(0, 10), end_date.substring(0, 10))) {
					toast({
						content: getMultiLangByID('452003504A-000001') /* 国际化处理： 保险开始日期必须早于保险结束日期*/,
						color: 'warning'
					});
					props.cardTable.setValByKeyAndIndex(form_tableId, index, key, {
						value: ''
					});
				}
			}
		}
	}

	if (key === 'pk_equip') {
		let pk_equip = props.cardTable.getValByKeyAndIndex(form_tableId, index, key);
		if (pk_equip && pk_equip.value) {
			// 处理多选，带出联动项
			let config = {
				afterEditUrl: bodyAfterEditUrl,
				pagecode: form_tableId,
				key,
				record,
				changedRows,
				index,
				keys: [ 'pk_equip' ]
			};
			commonBodyAfterEvent.call(this, props, config);

			// ajax({
			// 	url: bodyAfterEditUrl,
			// 	data,
			// 	success: (res) => {
			// 		let { success, data } = res;
			// 		if (success) {
			// 			if (res.data) {
			// 				if (res.data.head && res.data.head[formId]) {
			// 					this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
			// 				}
			// 				if (res.data.body && res.data.body[form_tableId]) {
			// 					this.props.cardTable.setTableData(form_tableId, res.data.body[form_tableId]);
			// 				}
			// 			}
			// 		}
			// 	}
			// });
		} else {
			// 清空联动项
			let meta = props.meta.getMeta();
			meta[form_tableId].items.map((item) => {
				if (
					item.attrcode.startsWith(key) ||
					item.attrcode.endsWith('_before') ||
					item.attrcode.endsWith('_after')
				) {
					props.cardTable.setValByKeyAndIndex(form_tableId, index, item.attrcode, {
						value: null,
						display: null
					});
				}
			});
		}
	}
}

function CompareDate(d1, d2) {
	return new Date(d1) > new Date(d2);
}
