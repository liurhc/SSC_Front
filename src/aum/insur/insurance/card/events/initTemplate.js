import { pageConfig } from '../const';
const { formId, bill_type, form_tableId, card_pageId } = pageConfig;
import tableButtonClick from './tableButtonClick';
import { init } from './buttonClick';
import ampub from 'ampub';
const { utils, components, commonConst } = ampub;
const { multiLangUtils, cardUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { afterModifyCardMeta, createOprationColumn } = cardUtils;
const { CommonKeys } = commonConst;
const { IBusiRoleConst } = CommonKeys;
const { queryAboutUtils, LoginContext, refInit } = components;
const { openEquipCardByPk } = queryAboutUtils;
const { loginContext, getContext, loginContextKeys } = LoginContext;
const { defRefCondition } = refInit;
const tableId = form_tableId;
export default function(props) {
	let _this = this;
	props.createUIDom(
		{
			pagecode: card_pageId
		},
		function(data) {
			if (data) {
				let status = props.getUrlParam('status');
				let pk_org_v;
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
					_this.transi_type = getContext(loginContextKeys.transtype);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.template) {
					let meta = data.template;
					meta = filter.call(_this, props, meta);
					modifierMeta.call(_this, props, meta);
					props.meta.setMeta(meta, () => {
						if (status === 'add') {
							init.call(_this, props);
						}
					});
				}
			}
		}
	);
}

//参照过滤
function filter(props, meta) {
	const transi_type = this.transi_type;
	const pk_group = getContext(loginContextKeys.groupId);
	meta[formId].items.map((item) => {
		if (item['attrcode'] === 'pk_recorder' || item['attrcode'] === 'pk_raorg_v') {
			item.queryCondition = () => {
				// 添加过滤方法

				let data = props.form.getFormItemsValue(formId, [ 'pk_org' ])[0].value || ''; // 获取前面选中参照的值
				return { pk_org: data, busifuncode: IBusiRoleConst.ASSETORG, isShowUnit: true }; // 根据pk_org过滤
			};
		} else if (item.attrcode == 'pk_org_v') {
			item.queryCondition = () => {
				return { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgVidRefFilter' };
			};
		} else if (item.attrcode == 'pk_fiorg_v') {
			item.queryCondition = () => {
				return { TreeRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgVidRefFilter' };
			};
		} else {
			defRefCondition.call(this, props, item, formId, pk_group);
		}
	});

	meta[form_tableId].items.map((item) => {
		if (item['attrcode'] === 'pk_equip') {
			//1.使用管理组织或货主管理组织等于当前资产组织 2.非抵押设备 3.交易规则支持的设备状态
			item.isMultiSelectedEnabled = true;
			item.renderStatus = 'browse';
			item.render = (text, record, index) => {
				return (
					<div class="card-table-browse">
						<span
							className="code-detail-link"
							onClick={() => {
								openEquipCardByPk(props, record.values.pk_equip.value);
							}}
						>
							{record.values.pk_equip && record.values.pk_equip.display}
						</span>
					</div>
				);
			};
			item.queryCondition = () => {
				// 添加过滤方法
				let data = props.form.getFormItemsValue(formId, [ 'pk_org' ])[0].value || '';
				return {
					pk_org: data,
					transi_type: transi_type,
					bill_type: bill_type,
					GridRefActionExt: 'nccloud.web.aum.insur.insurance.refcondition.PK_EQUIPSqlBuilder'
				}; // 根据pk_org过滤
			};
		} else if (item['attrcode'] === 'pk_jobmngfil') {
			//项目参照:根据组织参照
			item.queryCondition = () => {
				// 添加过滤方法
				let data = props.form.getFormItemsValue(formId, [ 'pk_org' ])[0].value || '';
				return { pk_org: data, pk_group: pk_group }; // 根据pk_org过滤
			};
		} else {
			defRefCondition.call(this, props, item, formId, pk_group);
		}
	});
	// 卡片模板修改后公共处理
	meta = afterModifyCardMeta.call(this, props, meta);
	return meta;
}

function modifierMeta(props, meta) {
	let _this = this;
	let status = props.getUrlParam('status');
	meta[formId].status = status;
	meta[tableId].status = status;

	let porCol = {
		attrcode: 'opr',
		label: getMultiLangByID('amcommon-000000') /* 国际化处理： 操作*/,
		visible: true,
		className: 'table-opr',
		itemtype: 'customer',
		width: 130,
		fixed: 'right',
		render(text, record, index) {
			let status = props.cardTable.getStatus(tableId);
			var bill_status = props.form.getFormItemsValue(formId, 'bill_status').value;
			let isClose = record.values.close_flag.value;
			let buttonArys;
			if (bill_status === '3' || bill_status === '6') {
				//审批通过 或者 关闭状态
				if (isClose) {
					//关闭状态
					buttonArys = [ 'OpenCard', 'OpenLine' ];
				} else {
					buttonArys = [ 'OpenCard', 'CloseLine' ];
				}
			} else {
				buttonArys = [ 'OpenCard' ];
			}
			let buttonAry = status === 'browse' ? buttonArys : [ 'OpenCard', 'DelLine' ];
			return props.button.createOprationButton(buttonAry, {
				area: 'card_body_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) =>
					tableButtonClick.call(_this, tableId, props, key, text, record, index, status)
			});
		}
	};
	meta[tableId].items.push(porCol);

	return meta;
}
