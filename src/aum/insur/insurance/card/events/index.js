import buttonClick, { delConfirm, saveClick, setDefaultValue, copydata, getdata, commitClick } from './buttonClick';
import initTemplate from './initTemplate';
import afterEvent from './afterEvent';
import pageInfoClick from './pageInfoClick';
import rowSelected from './rowSelected';
import bodyBeforeEvent from './bodyBeforeEvent';
export {
	buttonClick,
	afterEvent,
	initTemplate,
	pageInfoClick,
	rowSelected,
	bodyBeforeEvent,
	delConfirm,
	saveClick,
	setDefaultValue,
	copydata,
	getdata,
	commitClick
};
