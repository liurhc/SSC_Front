import { ajax, toast, cardCache } from 'nc-lightapp-front';
import { pageConfig } from '../const';
const { formId, form_tableId, card_pageId, openBodyUrl, closeBodyUrl, dataSource } = pageConfig;
import { setBtnsEnable, toggleShow, setBatchBtnsEnable } from './buttonClick';
import { bodyAfterEvent } from './afterEvent';
import ampub from 'ampub';
const { utils } = ampub;

const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const tableId = form_tableId;
/**
 * 表格操作按钮
 * @param {*} props 
 * @param {*} key 
 * @param {*} text 
 * @param {*} record 
 * @param {*} index 
 */
export default function(tableId, props, key, text, record, index, status) {
	switch (key) {
		// 表格操作按钮
		case 'DelLine':
			delRow.call(this, props, tableId, index, record);
			break; // 表格操作按钮
		case 'OpenCard':
			OpenCard.call(this, props, tableId, status, record, index);
			break;
		case 'OpenLine':
			openLine.call(this, props, record);
			break;
		case 'CloseLine':
			closeLine.call(this, props, record);
			break;
		default:
			break;
	}
}

function openLine(props, record) {
	let CardData = props.createMasterChildData(card_pageId, formId, tableId);
	let rows = [ record.values.pk_insurance_b.value ];
	CardData.rows = rows;
	CardData.pagecode = card_pageId;
	let url = openBodyUrl;
	ajax({
		url: url,
		data: CardData,
		success: (res) => {
			if (res.success) {
				if (res.data) {
					if (res.data.head && res.data.head[formId]) {
						props.form.setAllFormValue({ [formId]: res.data.head[formId] });
					}
					if (res.data.body && res.data.body[tableId]) {
						props.cardTable.setTableData(tableId, res.data.body[tableId]);
					}
					let pk = props.form.getFormItemsValue(formId, 'pk_insurance').value;
					cardCache.updateCache('pk_insurance', pk, res.data, formId, dataSource);
				}
				toggleShow.call(this);
				toast({ content: getMultiLangByID('452003504A-000003') /* 国际化处理： 打开成功*/, color: 'success' });
			}
		},
		error: (res) => {
			if (res && res.message) {
				toast({ color: 'danger', content: res.message });
			}
		}
	});
}

function closeLine(props, record) {
	let CardData = props.createMasterChildData(card_pageId, formId, tableId);
	let rows = [ record.values.pk_insurance_b.value ];
	CardData.rows = rows;
	CardData.pagecode = card_pageId;
	let url = closeBodyUrl;
	ajax({
		url: url,
		data: CardData,
		success: (res) => {
			if (res.success) {
				if (res.data) {
					if (res.data.head && res.data.head[formId]) {
						props.form.setAllFormValue({ [formId]: res.data.head[formId] });
					}
					if (res.data.body && res.data.body[tableId]) {
						props.cardTable.setTableData(tableId, res.data.body[tableId]);
					}
					let pk = props.form.getFormItemsValue(formId, 'pk_insurance').value;
					cardCache.updateCache('pk_insurance', pk, res.data, formId, dataSource);
				}
				toggleShow.call(this);
				toast({ content: getMultiLangByID('452003504A-000004') /* 国际化处理： 关闭成功*/, color: 'success' });
			}
		},
		error: (res) => {
			if (res && res.message) {
				toast({ color: 'danger', content: res.message });
			}
		}
	});
}

/**
 * 删除数据行
 */
function delRow(props, tableId, index, record) {
	let money_insurance_org = props.cardTable.getValByKeyAndIndex(tableId, index, 'money_insurance_org').value;
	props.cardTable.delRowsByIndex(tableId, index);
	if (money_insurance_org && money_insurance_org != 0 && money_insurance_org != '0.00') {
		bodyAfterEvent.call(this, props, tableId, 'money_insurance_org', { value: '0.00' }, [], 1);
	}
	setBatchBtnsEnable.call(this, props, tableId);
	setBtnsEnable.call(this, props, tableId);
}
/**
 * 卡片态展开事件处理
 */
function OpenCard(props, tableId, status, record, index) {
	//浏览态展开卡片
	if (status === 'browse') {
		props.cardTable.toggleRowView(tableId, record);
	} else {
		//编辑态展开侧栏
		props.cardTable.openModel(tableId, 'edit', record, index);
		this.stopPropagation();
	}
}
