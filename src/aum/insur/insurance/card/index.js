//主子表卡片
import React, { Component } from 'react';
import { createPage, base, high } from 'nc-lightapp-front';
const { NCAffix } = base;
import {
	buttonClick,
	initTemplate,
	afterEvent,
	pageInfoClick,
	rowSelected,
	bodyBeforeEvent,
	saveClick,
	copydata,
	getdata,
	commitClick
} from './events';
import { pageConfig } from './const';
const { card_pageId, formId, bill_type, form_tableId, form_pageId, searchId, title, dataSource } = pageConfig;
import ampub from 'ampub';
const { utils, components } = ampub;
const { ApprovalTrans } = components;
const { multiLangUtils, cardUtils, closeBrowserUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { createCardTitleArea, createCardPaginationArea, getCommonTableHeadLeft } = cardUtils;
class Card extends Component {
	constructor(props) {
		super(props);
		this.formId = formId;
		this.searchId = searchId;
		this.tableId = form_tableId;
		this.transi_type = '';
		this.state = {
			bill_type: bill_type,
			pk_org: '',
			bill_code: '',
			show: false,
			showApprove: false,
			pk_insurance: props.getUrlParam('id'),
			transi_type: '',
			compositedisplay: false,
			compositedata: {}
		};
		this.nowUIStatus = this.props.getUrlParam('status');
		this.old_pk = ''; //记录旧的pk 方便取消时候恢复
		closeBrowserUtils.call(this, props, {
			form: [ this.formId ],
			cardTable: [ this.tableId ]
		});
		initTemplate.call(this, props);
	}
	componentDidMount() {
		let status = this.nowUIStatus;
		if (status != 'add') {
			if (status == 'copyAdd') {
				copydata.call(this, this.state.pk_insurance);
			} else {
				let pk = this.props.getUrlParam('id');
				if (pk && pk != 'undefined') {
					getdata.call(this, pk);
				}
			}
		} else {
			// setDefaultValue.call(this);
		}
	}

	//返回列表界面
	backToList = () => {
		this.props.pushTo('/list', {
			pagecode: form_pageId
		});
	};

	//提交及指派 回调
	getAssginUsedr = (value) => {
		commitClick.call(this, 'SAVE', 'commit', value);
	};
	//取消 指派
	turnOff = () => {
		this.setState({
			compositedisplay: false
		});
	};
	modelSave = (props) => {
		// props.cardTable.closeModel(this.tableId);
		saveClick.call(this);
	};

	//获取列表肩部信息
	getTableHeadLeft = () => {
		return getCommonTableHeadLeft.call(this, this.props);
	};

	//获取列表肩部信息
	getTableHead = () => {
		return (
			<div className="shoulder-definition-area">
				<div className="definition-icons">
					{this.props.button.createButtonApp({
						area: 'card_body',
						buttonLimit: 3,
						onButtonClick: buttonClick.bind(this),
						popContainer: document.querySelector('.header-button-area')
					})}
				</div>
			</div>
		);
	};
	render() {
		let { cardTable, form, ncmodal, cardPagination, ncUploader } = this.props;
		let { createNCUploader } = ncUploader;
		const { createCardPagination } = cardPagination;
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createModal } = ncmodal;
		let status = form.getFormStatus(formId) || 'browse';
		const { BillTrack, ApproveDetail, PrintOutput } = high;

		return (
			<div className="nc-bill-card">
				{createNCUploader('warcontract-uploader', {})}
				<div className="nc-bill-top-area">
					<NCAffix>
						<div className="nc-bill-header-area">
							{/* 标题 title */}
							{createCardTitleArea.call(this, this.props, {
								title: getMultiLangByID(title),
								formId,
								backBtnClick: this.backToList
							})}

							<div className="header-button-area">
								{this.props.button.createButtonApp({
									area: 'card_head',
									buttonLimit: 3,
									onButtonClick: buttonClick.bind(this),
									popContainer: document.querySelector('.header-button-area')
								})}
							</div>
							{createCardPaginationArea.call(this, this.props, {
								formId,
								dataSource,
								pageInfoClick
							})}
						</div>
					</NCAffix>
					<div className="nc-bill-form-area">
						{createForm(this.formId, {
							onAfterEvent: afterEvent.bind(this)
						})}
					</div>
				</div>

				<div className="nc-bill-bottom-area">
					<div className="nc-bill-table-area">
						{/* {this.getTableHead()} */}
						{createCardTable(this.tableId, {
							tableHeadLeft: this.getTableHeadLeft.bind(this),
							tableHead: this.getTableHead.bind(this),
							modelSave: this.modelSave.bind(this),
							showIndex: true,
							showCheck: true,
							isAddRow: true,
							onAfterEvent: afterEvent.bind(this),
							onBeforeEvent: bodyBeforeEvent.bind(this),
							onSelected: rowSelected.bind(this),
							onSelectedAll: rowSelected.bind(this)
						})}
					</div>
				</div>
				<BillTrack
					show={this.state.show}
					close={() => {
						this.setState({ show: false });
					}}
					pk={this.state.pk_insurance} //单据id
					type={this.state.bill_type} //单据类型
				/>
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transi_type}
					billid={this.state.pk_insurance}
				/>
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002')}
						/* 国际化处理： 指派*/
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				) : (
					''
				)}
				{createModal(`${card_pageId}-confirm`, { color: 'warning' })}
			</div>
		);
	}
}

Card = createPage({
	billinfo: {
		// 单据类型
		billtype: 'card',
		// 节点对应的模板编码
		pagecode: card_pageId,
		// 表头(表单)区域编码
		headcode: formId,
		// 表体(表格)区域编码
		bodycode: form_tableId
	}
})(Card);
// ReactDOM.render(<Card />, document.querySelector('#app'));
export default Card;
