// 页面配置
export const pageConfig = {
	/**
 * 常量
 */
	tableId: 'list_head',

	pageId: '452003504A_list',

	appid: '0001Z910000000000LO6', //小应用id
	appcode: '452003504A', //小应用code

	searchId: 'searchArea', //查询区

	oid: '1001Z910000000007Z1G', //查询模板id
	title: '452003504A-000000' /* 国际化处理： 资产投保*/,

	printFilename: '452003504A-000000' /* 国际化处理： 资产投保*/,
	bill_type: '4A23',
	transi_type: '4A23-01',

	// form节点编码
	form_tableId: 'bodyvos',

	form_pageId: '452003504A_list',

	card_pageId: '452003504A_card',

	formId: 'card_head',

	pk_group: '0001S210000000000AYW',

	//url
	list_url: '/aum/insur/insurance/list/index.html', //列表url
	card_url: '/aum/insur/insurance/card/index.html', //卡片url

	printListUrl: '/nccloud/aum/insurance/printCard.do', //列表打印url
	printCardUrl: '/nccloud/aum/insurance/printCard.do', //卡片打印url

	openBodyUrl: '/nccloud/aum/insurance/openLine.do', //打开
	closeBodyUrl: '/nccloud/aum/insurance/closeLine.do', //关闭

	copydata_url: '/nccloud/aum/insurance/copydata.do', //复制url
	bodyAfterEditUrl: '/nccloud/aum/insurance/bodyAfterEdit.do', //表体编辑后事件url
	headAfterEditUrl: '/nccloud/aum/insurance/headAfterEdit.do', //表头编辑后事件url

	EditOpr_url: '/nccloud/aum/insurance/insurance_edit.do',
	BatchAlter_url: '/nccloud/aum/insurance/batchAlter.do', //
	node_code: '4520030005',

	printFuncode: '452003504A',
	printNodekey: null,

	//操作 action url
	list_delete: '/nccloud/aum/insurance/delete.do',
	list_query: '/nccloud/aum/insurance/query.do',
	list_querybypks: '/nccloud/aum/insurance/querypagegridbypks.do',

	card_query: '/nccloud/aum/insurance/querycard.do',
	card_addsave: '/nccloud/aum/insurance/save.do',
	card_updatesave: '/nccloud/aum/insurance/update.do',

	card_commit: '/nccloud/aum/insurance/commit.do',

	dataSource: 'aum.insur.insurance.main',
	cardRouter: '/card',

	//   card_rollbackBillCode : '/nccloud/aum/common/rollbackBillCode.do',
	card_billCodeGenBack: '/nccloud/ampub/common/billCodeGenBack.do',

	//精度处理
	precision_url: '/nccloud/aum/insurance/precision.do',

	//资产组织切换事件处理
	orgchangeevent_url: '/nccloud/aum/insurance/orgchangeevent.do',

	//操作 action url

	// 编辑态按钮
	editBtns: [ 'save', 'cancel', 'add', 'delete' ],
	// 浏览态按钮
	browseBtns: [ 'add', 'edit', 'delete' ],
	// 所有按钮颜色
	btnColors: {
		add: 'main-button',
		edit: 'main-button',
		delete: 'secondary-button',
		save: 'main-button',
		cancel: 'secondary-button'
	}
};
