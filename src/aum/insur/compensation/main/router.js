import { asyncComponent } from 'nc-lightapp-front';

const List = asyncComponent(() =>
	import(/* webpackChunkName: "aum/insur/compensation/list/list" */ /* webpackMode: "eager" */ '../list')
);
const Card = asyncComponent(() =>
	import(/* webpackChunkName: "aum/insur/compensation/card/card" */ /* webpackMode: "eager" */ '../card')
);
const transfer = asyncComponent(() =>
	import(/* webpackChunkName: "aum/insur/insurancetransfer/source/source" */ /* webpackMode: "eager" */ '../../insurancetransfer/source')
);

const routes = [
	{
		path: '/',
		component: List,
		exact: true
	},
	{
		path: '/list',
		component: List
	},
	{
		path: '/card',
		component: Card
	},
	{
		path: '/transfer',
		component: transfer
	}
];

export default routes;
