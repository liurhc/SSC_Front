import { pageConfig } from '../const';
import ampub from 'ampub';
const { components } = ampub;
const { assetOrgMultiRefFilter } = components;
const { isMultiCorpRefHandler } = assetOrgMultiRefFilter;

const { searchId } = pageConfig;

export default function afterEvent(key, value) {
	if (key === 'pk_org') {
		isMultiCorpRefHandler.call(this, this.props, value, searchId, [ 'auditor', 'billmaker' ]);
	}
}
