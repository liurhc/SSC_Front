// 页面配置
export const pageConfig = {
	/**
 * 保险索赔常量
 */
	dataSource: 'aum.insur.compensation.main',
	cardRouter: '/card',
	sourceRouter: '/transfer',
	tableId: 'list_head',
	printFilename: '452003508A-000000' /* 国际化处理： 资产索赔*/,
	title: '452003508A-000000' /* 国际化处理： 资产索赔*/,
	pageId: '452003508A_list',

	appid: '0001Z910000000000LP9', //小应用id
	appcode: '452003508A', //小应用code

	searchId: 'searchArea', //查询区

	oid: '1001Z91000000000C3CL', //查询模板id

	bill_type: '4A49',

	transi_type: '4A49-01',

	// form节点编码
	form_tableId: 'bodyvos',

	form_pageId: '452003508A_list',

	card_pageId: '452003508A_card',

	formId: 'card_head',

	pk_group: '0001S210000000000AYW',

	//url
	list_url: '/aum/insur/compensation/list/index.html', //列表url
	card_url: '/aum/insur/compensation/card/index.html', //卡片url

	printListUrl: '/nccloud/aum/compensation/printCard.do', //列表打印url
	printCardUrl: '/nccloud/aum/compensation/printCard.do', //卡片打印url
	bodyAfterEditUrl: '/nccloud/aum/compensation/bodyAfterEdit.do', //表体编辑后事件url

	EditOpr_url: '/nccloud/aum/compensation/compensation_edit.do',
	node_code: '4520030010',

	printFuncode: '452003508A',
	printNodekey: null,

	//操作 action url
	list_delete: '/nccloud/aum/compensation/delete.do',
	list_query: '/nccloud/aum/compensation/query.do',
	list_querybypks: '/nccloud/aum/compensation/querypagegridbypks.do',

	card_query: '/nccloud/aum/compensation/querycard.do',
	card_addsave: '/nccloud/aum/compensation/save.do',
	card_updatesave: '/nccloud/aum/compensation/update.do',
	card_commit: '/nccloud/aum/compensation/commit.do',

	//   card_rollbackBillCode : '/nccloud/aum/common/rollbackBillCode.do',
	card_billCodeGenBack: '/nccloud/ampub/common/billCodeGenBack.do',

	//精度处理
	precision_url: '/nccloud/aum/compensation/precision.do',

	//资产组织切换事件处理
	orgchangeevent_url: '/nccloud/aum/compensation/orgchangeevent.do',
	// 转单上游缓存命名空间
	dataSourceTransfer: 'aum.insur.insurancetransfer.source',
	//转单下游相关信息
	TRANSFERINFO: {
		pagecode: '452003508A_transfercard',
		appid: '0001Z910000000000LP9',
		leftarea: 'leftarea',
		srctype: 'transfer'
	}
};
