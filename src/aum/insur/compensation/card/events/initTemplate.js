import { pageConfig } from '../const';
import { ajax } from 'nc-lightapp-front';
const { formId, bill_type, form_tableId, card_pageId, TRANSFERINFO } = pageConfig;
import tableButtonClick from './tableButtonClick';
import { init, getdata } from './buttonClick';
const tableId = form_tableId;
import ampub from 'ampub';
const { utils, components, commonConst } = ampub;

const { multiLangUtils, cardUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { afterModifyCardMeta } = cardUtils;

const { CommonKeys, StatusUtils } = commonConst;
const { IBusiRoleConst, ButtonConst, TransferConst, linkQueryConst } = CommonKeys;
const { UISTATE } = StatusUtils;
const { queryAboutUtils, LoginContext, refInit } = components;
const { openEquipCardByPk } = queryAboutUtils;
const { loginContext, getContext, loginContextKeys } = LoginContext;
const { defRefCondition } = refInit;
const { transferUtils } = utils;
const { conactAddButton } = transferUtils;

export default function(props) {
	let _this = this;
	let pageId = card_pageId;
	//来源是转单的加载不同模板
	if (TRANSFERINFO.srctype == props.getUrlParam(TransferConst.srctype)) {
		pageId = TRANSFERINFO.pagecode;
		//转单下游界面的卡片区域编码必须与原卡片界面一致
	}
	props.createUIDom(
		{
			pagecode: pageId
		},
		function(data) {
			if (data) {
				let status = props.getUrlParam('status');
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
					_this.transi_type = getContext(loginContextKeys.transtype);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button, () => {
						if (
							TRANSFERINFO.srctype != props.getUrlParam(TransferConst.srctype) &&
							props.getUrlParam(linkQueryConst.SCENE) != linkQueryConst.SCENETYPE.approvesce
						) {
							//不是转单下游时需要处理拉单按钮
							conactAddButton(props, bill_type, _this.transi_type, 'card_head');
							if (status == UISTATE.edit || status == UISTATE.add) {
								props.button.setButtonVisible(ButtonConst.AddTransferGroup, false);
							}
						}
					});
				}
				if (data.template) {
					let meta = data.template;
					meta = filter.call(_this, props, meta);
					modifierMeta.call(_this, props, meta);
					props.meta.setMeta(meta, () => {
						initAfter.call(_this, props);
					});
				}
			}
		}
	);
}
function initAfter(props) {
	let status = this.nowUIStatus;
	if (status == UISTATE.add) {
		init.call(this, props);
	} else if (status == UISTATE.browse || status == UISTATE.edit) {
		let pk = this.props.getUrlParam('id');
		if (pk) {
			getdata.call(this, pk);
		}
	} else if (status == UISTATE.transferAdd) {
		//转单
		this.getDataBySrcId.call(this, props);
	}
}

//参照过滤
function filter(props, meta) {
	const transi_type = this.transi_type;
	const pk_group = getContext(loginContextKeys.groupId);
	meta[formId].items.map((item) => {
		if (item['attrcode'] === 'pk_recorder' || item['attrcode'] === 'pk_raorg_v') {
			item.queryCondition = () => {
				// 添加过滤方法
				let data = props.form.getFormItemsValue(formId, [ 'pk_org' ])[0].value || ''; // 获取前面选中参照的值
				return { pk_org: data, busifuncode: IBusiRoleConst.ASSETORG, isShowUnit: true }; // 根据pk_org过滤
			};
		} else if (item.attrcode == 'pk_org_v') {
			item.queryCondition = () => {
				return { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgVidRefFilter' };
			};
		} else if (item.attrcode == 'pk_fiorg_v') {
			item.queryCondition = () => {
				return { TreeRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgVidRefFilter' };
			};
		} else if (item.attrcode == 'pk_insurance') {
			item.renderStatus = 'browse';
			item.render = () => {
				let record = props.form.getFormItemsValue(formId, [ 'pk_insurance' ])[0];
				return (
					<div class="card-form-browse">
						<span
							style={{ color: '#007ace', cursor: 'pointer' }}
							onClick={() => {
								let rowData = props.cardTable.getVisibleRows(tableId)[0];
								openInsuranceCardByPk.call(
									this,
									props,
									rowData.values.transi_type_src.value,
									record.value
								);
							}}
						>
							{record && record.display}
						</span>
					</div>
				);
			};
		} else {
			defRefCondition.call(this, props, item, formId, pk_group);
		}
	});

	meta[form_tableId].items.map((item) => {
		if (item['attrcode'] === 'pk_equip') {
			//设置设备参照 参照范围；使用管理组织等于当前资产组织，且符合条件的设备卡片；
			item.isMultiSelectedEnabled = true;
			item.renderStatus = 'browse';
			item.render = (text, record, index) => {
				return (
					<div class="card-table-browse">
						<span
							className="code-detail-link"
							onClick={() => {
								openEquipCardByPk(props, record.values.pk_equip.value);
							}}
						>
							{record.values.pk_equip && record.values.pk_equip.display}
						</span>
					</div>
				);
			};
			item.queryCondition = () => {
				// 添加过滤方法
				let data = props.form.getFormItemsValue(formId, [ 'pk_org' ])[0].value || '';
				return {
					pk_org: data,
					transi_type: transi_type,
					bill_type: bill_type,
					GridRefActionExt: 'nccloud.web.aum.insur.compensation.refcondition.PK_EQUIPSqlBuilder'
				}; // 根据pk_org过滤
			};
		} else if (item['attrcode'] === 'pk_jobmngfil') {
			//项目参照:根据组织参照
			item.queryCondition = () => {
				// 添加过滤方法
				let data = props.form.getFormItemsValue(formId, [ 'pk_org' ])[0].value || '';
				return { pk_org: data, pk_group: pk_group }; // 根据pk_org过滤
			};
		} else {
			defRefCondition.call(this, props, item, formId, pk_group);
		}
	});
	// 卡片模板修改后公共处理
	meta = afterModifyCardMeta.call(this, props, meta);
	return meta;
}

function modifierMeta(props, meta) {
	let status = props.getUrlParam('status');
	meta[formId].status = status;
	meta[tableId].status = status;

	let porCol = {
		attrcode: 'opr',
		label: getMultiLangByID('amcommon-000000') /* 国际化处理： 操作*/,
		visible: true,
		className: 'table-opr',
		itemtype: 'customer',
		width: 130,
		fixed: 'right',
		render(text, record, index) {
			let status = props.cardTable.getStatus(tableId);
			let buttonAry = status === 'browse' ? [ 'OpenCard' ] : [ 'OpenCard', 'DelLine' ];
			return props.button.createOprationButton(buttonAry, {
				area: 'card_body_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) =>
					tableButtonClick.call(this, tableId, props, key, text, record, index, status)
			});
		}
	};
	meta[tableId].items.push(porCol);

	return meta;
}

function openInsuranceCardByPk(props, insuranceTranstype, insurancePk) {
	ajax({
		url: '/nccloud/ampub/common/amLinkQuery.do',
		data: { [linkQueryConst.ID]: insurancePk, [linkQueryConst.TRANSTYPE]: insuranceTranstype },
		success: (res) => {
			if (res.data) {
				let linkData = res.data;
				linkData['status'] = 'browse';
				props.openTo(linkData[linkQueryConst.URL], linkData);
			} else {
				toast({ content: getMultiLangByID('452003508A-000002') /* 国际化处理： 无权限*/, color: 'warning' });
			}
		},
		error: (res) => {
			toast({ content: res.message, color: 'warning' });
		}
	});
}
