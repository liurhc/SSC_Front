import { ajax, toast, cardCache } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import { toggleShow, cancel, businessValidator } from './buttonClick';
import ampub from 'ampub';
const { utils, components, commonConst } = ampub;

const { msgUtils, cardUtils } = utils;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { setCardValue } = cardUtils;

const { CommonKeys, StatusUtils } = commonConst;
const { UISTATE } = StatusUtils;
const { approveConst, TransferConst } = CommonKeys;

const { saveValidatorsUtil, LoginContext } = components;
const { getContext } = LoginContext;
const { beforeSaveValidator } = saveValidatorsUtil;

const { transferUtils } = utils;
const { pushToTransferPage } = transferUtils;
const {
	bill_type,
	card_addsave,
	card_updatesave,
	TRANSFERINFO,
	dataSource,
	dataSourceTransfer,
	listRouter,
	form_pageId,
	sourceRouter,
	card_pageId,
	list_delete
} = pageConfig;
//转单按钮事件处理
export default function(props, id) {
	let headpk = 'pk_compensation';
	let bodypk = 'pk_compensation_b';
	let _this = this;
	//保存
	if (id === 'Save') {
		transferSave.call(this);
	} else if (id === 'Cancel') {
		let cancelClick = () => {
			//取消
			let headpkvalue = props.form.getFormItemsValue(_this.formId, headpk);
			let index = _this.transferEditId.indexOf(headpkvalue.value);
			if (headpkvalue.value && index != -1) {
				_this.transferEditId.splice(index, 1);
				// _this.transferEditId.remove(headpkvalue.value);
				//转单已处理后又点保存取消
				cancel.call(_this, UISTATE.transferBorwse);
				_this.synTransferData(); //同步取消后的数据到转单界面
			} else {
				let count = props.transferTable.getTransformFormAmount(TRANSFERINFO.leftarea); //转单数据的数量
				if (count == 1) {
					pushToTransfer.call(_this);
				} else {
					props.transferTable.setTransformFormStatus(TRANSFERINFO.leftarea, {
						status: false,
						childId: bodypk,
						onChange: (current, next) => {
							// toast({ color: 'success', content: '取消成功' });
						}
					});
				}
			}
		};
		showConfirm.call(this, props, {
			type: MsgConst.Type.Cancel,
			beSureBtnClick: cancelClick
		});
	} else if (id === 'Delete') {
		let delConfirm = () => {
			let pkField = 'pk_compensation';
			let pk = props.form.getFormItemsValue(_this.formId, pkField).value;
			let paramInfoMap = {};
			paramInfoMap[pk] = props.form.getFormItemsValue(_this.formId, 'ts').value;
			ajax({
				url: list_delete,
				data: {
					paramInfoMap: paramInfoMap,
					dataType: 'listData',
					OperatorType: 'DELETE',
					pageid: card_pageId
				},
				success: (res) => {
					let { success, data } = res;
					if (res) {
						if (res.data.success === approveConst.ALLSUCCESS) {
							//缓存处理
							cardCache.deleteCacheById(pkField, pk, dataSource);
							let count = props.transferTable.getTransformFormAmount(TRANSFERINFO.leftarea); //转单数据的数量
							if (count == 1) {
								pushToTransfer.call(_this);
							} else {
								props.transferTable.setTransformFormStatus(TRANSFERINFO.leftarea, {
									status: false,
									childId: bodypk,
									onChange: (current, next) => {
										showMessage.call(this, props, { type: MsgConst.Type.DeleteSuccess });
										/* 国际化处理： 删除成功*/
									}
								});
							}
						}
					}
				},
				error: (res) => {
					if (res && res.message) {
						toast({ content: res.message, color: 'danger' });
					}
				}
			});
		};
		showConfirm.call(this, props, { type: MsgConst.Type.Delete, beSureBtnClick: delConfirm });
	} else if (id === 'Quit') {
		//判断当前单据是否存在没有处理的
		let count = props.transferTable.getTransformFormAmount(TRANSFERINFO.leftarea); //转单数据的数量
		let status = true;
		for (let index = 0; index < count; index++) {
			status = props.transferTable.getTransformFormStatus(TRANSFERINFO.leftarea, index);
			if (!status) {
				break;
			}
		}
		if (!status) {
			let quit = () => {
				props.pushTo(listRouter, {
					pagecode: form_pageId
				});
			};
			showConfirm.call(this, props, { type: MsgConst.Type.Quit, beSureBtnClick: quit });
		} else {
			props.pushTo(listRouter, {
				pagecode: form_pageId
			});
		}
	} else if (id == 'Edit') {
		let headpkvalue = props.form.getFormItemsValue(_this.formId, headpk).value;
		if (_this.transferEditId.indexOf(headpkvalue) == -1) {
			_this.transferEditId.push(headpkvalue);
		}
		toggleShow.call(_this, UISTATE.transferEdit);
	} else {
		_this.selfButtonClick.call(_this, props, id);
	}
}
/**
 * 跳转到拉单界面
 */
export function pushToTransfer() {
	pushToTransferPage.call(
		this,
		sourceRouter,
		getContext(TransferConst.src_billtype_cache, TransferConst.dataSource),
		getContext(TransferConst.src_transtype_cache, TransferConst.dataSource),
		bill_type,
		this.transi_type,
		'452003508A_transfercard'
	);
}

export function transferSave() {
	let _this = this;
	let headpk = 'pk_compensation';
	let bodypk = 'pk_compensation_b';
	// 保存前校验
	let pass = beforeSaveValidator.call(this, this.props, this.formId, this.tableId, []);
	if (!pass) {
		return;
	}
	if (!businessValidator.call(this)) {
		return;
	}
	let url = card_addsave;
	let cardData = this.props.createMasterChildData(this.pageid, this.formId, this.tableId);
	cardData.bill_type = bill_type;
	if (this.nowUIStatus == UISTATE.transferEdit) {
		url = card_updatesave;
	}
	//调用Ajax保存数据
	ajax({
		url: url,
		data: cardData,
		success: (res) => {
			if (res.success) {
				if (res.data) {
					setCardValue.call(_this, this.props, res.data);
					toggleShow.call(_this, UISTATE.transferBorwse);
					_this.synTransferData();

					let pk = this.props.form.getFormItemsValue(_this.formId, headpk).value;
					this.setState({ pk_compensation: pk });
					let pkvalues = this.props.cardTable.getColValue(_this.tableId, 'pk_bill_b_src', false, false); //表体的pk、数组
					pkvalues = pkvalues.map((e) => {
						return e.value;
					});
					let index = _this.transferEditId.indexOf(pk);
					if (index != -1) {
						_this.transferEditId.splice(index, 1);
					}
					cardData = this.props.createMasterChildData(this.pageid, this.formId, this.tableId);
					if (_this.nowUIStatus == UISTATE.transferEdit) {
						//下游查询列表添加对应的pk
						cardCache.updateCache('pk_compensation', pk, cardData, _this.formId, dataSource);
					} else {
						//下游查询列表添加对应的pk
						cardCache.addCache(pk, cardData, _this.formId, dataSource);
						//转单上游缓存移除对应的pk
						// cardCache.deleteCacheById(headpk, pk, dataSourceTransfer);
						this.props.transferTable.setSavedTransferTableDataPk(pkvalues);
					}
					this.props.transferTable.setTransformFormStatus(TRANSFERINFO.leftarea, {
						status: true,
						childId: bodypk,
						onChange: (current, next, currentIndex) => {
							//props.transferTable.setTransferListValueByIndex(TRANSFERINFO.leftarea,next,currentIndex);
							_this.transferIndex = currentIndex + 1;
							// toggleShow.call(this, UISTATE.transferBorwse);
							//保存成功提示并设置页面跳转状态
							showMessage.call(this, this.props, { type: MsgConst.Type.SaveSuccess });
							/* 国际化处理： 保存成功*/
						}
					});
				}
			}
		},
		error: (res) => {
			toast({ color: 'danger', content: res.message });
		}
	});
}
