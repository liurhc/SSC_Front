import { ajax } from 'nc-lightapp-front';
import { pageConfig } from '../const';
const { form_tableId, card_pageId, formId, TRANSFERINFO, bodyAfterEditUrl, headAfterEditUrl } = pageConfig;
import { setDefaultValue, toggleShow } from './buttonClick';
import ampub from 'ampub';
const { utils } = ampub;
const { cardUtils } = utils;
const { commonHeadAfterEvent, commonBodyAfterEvent, updateHeadData, updateBodyData } = cardUtils;
export default function afterEvent(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	if (moduleId == formId) {
		headAfterEvent.call(this, props, moduleId, key, value, changedRows, index, record, type, eventType);
	} else if (moduleId == form_tableId) {
		bodyAfterEvent.call(this, props, moduleId, key, value, changedRows, index, record, type, eventType);
	}
	//来源是转单的操作需要同步右边数据到左边
	if (this.srctype == TRANSFERINFO.srctype) {
		this.synTransferData();
	}
}

/**
 * 表头编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} oldValue 
 * @param {*} val 
 */
export function headAfterEvent(props, moduleId, key, value, oldValue, val) {
	if (key === 'pk_org_v') {
		//清空表头和主组织相关字段-和原nc保持一致
		props.form.setFormItemsValue(formId, {
			pk_recorder: '',
			pk_fiorg: ''
		});
		if (value.value != undefined) {
			let callback1 = () => {
				toggleShow.call(this);
				props.button.setButtonDisabled([ 'AddLine' ], false);
				this.props.resMetaAfterPkorgEdit();
			};
			let config = {
				afterEditUrl: headAfterEditUrl,
				pagecode: card_pageId,
				key,
				value,
				oldValue,
				callback: callback1
			};
			commonHeadAfterEvent.call(this, props, config);
			// let data = props.createHeadAfterEventData(card_pageId, formId, form_tableId, moduleId, key, value);
			// ajax({
			// 	url: headAfterEditUrl,
			// 	data: data,
			// 	success: (res) => {
			// 		if (res.success) {
			// 			if (res.data) {
			// 				if (res.data.head && res.data.head[formId]) {
			// 					this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
			// 				}
			// 				if (res.data.body && res.data.body[form_tableId]) {
			// 					this.props.cardTable.setTableData(form_tableId, res.data.body[form_tableId]);
			// 				}
			// 				toggleShow.call(this);
			// 			}
			// 		}
			// 	}
			// });
		} else {
			setDefaultValue.call(this, true);
			this.props.cardTable.setTableData(form_tableId, { rows: [] });
			this.props.initMetaByPkorg('pk_org_v');
			this.props.button.setButtonDisabled([ 'AddLine' ], true);
		}
	}

	//财务组织切换
	if (key === 'pk_fiorg_v') {
		let callback1 = () => {
			toggleShow.call(this);
		};
		let config = {
			afterEditUrl: headAfterEditUrl,
			pagecode: card_pageId,
			key,
			value,
			oldValue,
			keys: [ 'pk_fiorg_v' ],
			callback: callback1
		};
		commonHeadAfterEvent.call(this, props, config);

		// let data = props.createHeadAfterEventData(card_pageId, formId, form_tableId, moduleId, key, value);
		// ajax({
		// 	url: headAfterEditUrl,
		// 	data: data,
		// 	success: (res) => {
		// 		if (res.success) {
		// 			if (res.data) {
		// 				if (res.data.head && res.data.head[formId]) {
		// 					this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
		// 				}
		// 				if (res.data.body && res.data.body[form_tableId]) {
		// 					this.props.cardTable.setTableData(form_tableId, res.data.body[form_tableId]);
		// 				}
		// 				toggleShow.call(this);
		// 			}
		// 		}
		// 	}
		// });
	}
}

/**
 * 表体编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedRows 
 * @param {*} index 
 * @param {*} record 
 * @param {*} type 
 * @param {*} eventType 
 */
function bodyAfterEvent(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	let data = props.createBodyAfterEventData(card_pageId, formId, form_tableId, moduleId, key, changedRows, index);
	if (key === 'pk_equip') {
		let pk_equip = props.cardTable.getValByKeyAndIndex(form_tableId, index, key);
		if (pk_equip && pk_equip.value) {
			// 处理多选，带出联动项
			let config = {
				afterEditUrl: bodyAfterEditUrl,
				pagecode: form_tableId,
				key,
				record,
				changedRows,
				index,
				keys: [ 'pk_equip' ]
			};
			commonBodyAfterEvent.call(this, props, config);
			// ajax({
			// 	url: bodyAfterEditUrl,
			// 	data,
			// 	success: (res) => {
			// 		let { success, data } = res;
			// 		if (success) {
			// 			if (res.data) {
			// 				if (res.data.head && res.data.head[formId]) {
			// 					this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
			// 				}
			// 				if (res.data.body && res.data.body[form_tableId]) {
			// 					this.props.cardTable.setTableData(form_tableId, res.data.body[form_tableId]);
			// 				}
			// 			}
			// 		}
			// 	}
			// });
		} else {
			// 清空联动项
			let meta = props.meta.getMeta();
			meta[form_tableId].items.map((item) => {
				if (
					item.attrcode.startsWith(key) ||
					item.attrcode.endsWith('_before') ||
					item.attrcode.endsWith('_after')
				) {
					props.cardTable.setValByKeyAndIndex(form_tableId, index, item.attrcode, {
						value: null,
						display: null
					});
				}
			});
		}
	}
	if (key === 'compensat_money_org') {
		if (value) {
			if (value < 0) {
				toast({
					content: getMultiLangByID('452003508A-000006') /* 国际化处理： 输入值不能小于0*/,
					color: 'warning'
				});
				props.cardTable.setValByKeyAndIndex(form_tableId, index, key, {
					value: ''
				});
				return;
			}
			let config = {
				afterEditUrl: bodyAfterEditUrl,
				pagecode: form_tableId,
				key,
				record,
				changedRows,
				index,
				keys: [ 'compensat_money_org' ]
			};
			commonBodyAfterEvent.call(this, props, config);
			// ajax({
			// 	url: bodyAfterEditUrl,
			// 	data,
			// 	async: false,
			// 	success: (res) => {
			// 		let { success, data } = res;
			// 		if (success) {
			// 			if (res.data) {
			// 				if (res.data.head && res.data.head[formId]) {
			// 					this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
			// 				}
			// 				if (res.data.body && res.data.body[form_tableId]) {
			// 					this.props.cardTable.setTableData(form_tableId, res.data.body[form_tableId]);
			// 				}
			// 			}
			// 		}
			// 	}
			// });
		}
	}
}
