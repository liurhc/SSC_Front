import { setBatchBtnsEnable } from './buttonClick';
import ampub from 'ampub';
const { commonConst } = ampub;
const { StatusUtils } = commonConst;
const { UISTATE } = StatusUtils;

/**
 * 行选中事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} record 
 * @param {*} index 
 * @param {*} status 
 */
export default function(props, moduleId, record, index, status) {
	if (this.nowUIStatus != UISTATE.transferAdd && this.nowUIStatus != UISTATE.transferEdit) {
		setBatchBtnsEnable.call(this, props, moduleId);
	}
}
