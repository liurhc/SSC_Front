/**
 * @author:zhangxxu
 * @description:Tab页中的统计图组件
 * 
 */
import React, { Component } from 'react';
import echarts from 'echarts';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
export default class EchartsTab extends Component {
	constructor(props) {
		super(props);
		this.state = {
			planData: {}
		};
		this.option = {};
		this.myChart = '';
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.state.planData && nextProps.state.planData.rows) {
			let echartsData = this.constructEChartsData(nextProps.state.planData.rows);
			this.setState(
				{
					planData: nextProps.state.planData
				},
				() => {
					this.option.xAxis.data = echartsData.orgs;
					this.option.series[0].data = echartsData.checked_num;
					this.option.series[1].data = echartsData.unchecked_num;
					this.myChart.setOption(this.option);
				}
			);
		} else {
			this.option.xAxis.data = null;
			this.option.series[0].data = null;
			this.option.series[1].data = null;
			this.myChart.setOption(this.option);
		}
	}

	componentDidMount() {
		this.option = {
			noDataLoadingOption: {
				text: getMultiLangByID('452015012A-000000') /* 国际化处理：无数据 */,
				effect: 'bubble',
				effectOption: { effect: { n: 0 } }
			},
			legend: {
				data: [
					getMultiLangByID('452015012A-000001') /* 国际化处理：已盘 */,
					getMultiLangByID('452015012A-000002') /* 国际化处理：未盘 */
				]
			},
			yAxis: {
				type: 'value'
			},
			xAxis: {
				type: 'category',
				//刻度
				axisTick: {
					show: false
				},
				//标签
				axisLabel: {
					rotate: -45,
					interval: 0
				},
				data: [] //公司名称
			},
			//工具箱 --保存到本地
			// toolbox: {
			// 	show: true,
			// 	feature: {
			// 		saveAsImage: {
			// 			title: getMultiLangByID('452015012A-000003') /* 国际化处理：保存 */
			// 		}
			// 	}
			// },
			//悬浮提示框
			tooltip: {
				trigger: 'axis',
				axisPointer: {
					type: 'shadow'
				},
				formatter: function(params) {
					let xAxis = params[0]['name'];
					let nameArr = params.map(({ seriesName }) => {
						return seriesName;
					});
					let dataArr = params.map(({ data }) => {
						return data;
					});
					let totalNum = 0;
					for (let i = 0; i < dataArr.length; i++) {
						totalNum += dataArr[i] * 1;
					}
					let str = '';

					for (let i = 0; i < nameArr.length; i++) {
						str +=
							'<p>' +
							nameArr[i] +
							'：<span>' +
							dataArr[i] +
							getMultiLangByID('452015012A-000017') /* 国际化处理: 个 */ +
							'</span></p>';
					}
					return `<div class="toolTip">
						<p>${xAxis} <span>${getMultiLangByID('452015012A-000015', {
						totalNum
					})}</span></p>${str}</div>`; /* 国际化处理：共${totalNum}个资产 */
				}
			},
			//数据区域放缩
			dataZoom: [
				{
					show: true,
					realtime: true,
					start: 0,
					end: 50
				}
			],
			series: [
				{
					name: getMultiLangByID('452015012A-000001') /* 国际化处理：已盘 */,
					type: 'bar',
					stack: getMultiLangByID('452015012A-000016') /* 国际化处理：总量 */,
					barWidth: 15,
					// data: [ 32, 30, 32, 33, 39, 33, 32 ] //已盘数据
					data: [] //已盘数据
				},
				{
					name: getMultiLangByID('452015012A-000002') /* 国际化处理：未盘 */,
					type: 'bar',
					stack: getMultiLangByID('452015012A-000016') /* 国际化处理：总量 */,
					barWidth: 15,
					data: [] //未盘数据
				}
			],
			color: [ '#FF9A48', '#20CBAF' ]
		};
		this.myChart = echarts.init(document.getElementById('echarts-tab'));
		this.myChart.setOption(this.option);
		//当浏览器窗口发生变化时候，echarts大小随着变化
		window.addEventListener('resize', () => {
			this.myChart.resize();
		});
	}

	constructEChartsData = (rows) => {
		let echartsData = {};
		echartsData.orgs = [];
		echartsData.checked_num = [];
		echartsData.unchecked_num = [];
		rows.map((item) => {
			echartsData.orgs.push(item.values.pk_org_v.display);
			echartsData.checked_num.push(item.values.checked_num.value);
			echartsData.unchecked_num.push(item.values.unchecked_num.value);
		});
		return echartsData;
	};
	render() {
		return <div id="echarts-tab" />;
	}
}
