import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, base, createPageIcon } from 'nc-lightapp-front';
import { buttonClick, initTemplate, searchBtnClick } from './events';
import { EchartsTab } from './component';
import { pageConfig } from './const';
import './index.less';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { initMultiLangByModule, getMultiLangByID } = multiLangUtils;

const { NCTabs } = base;
const NCTabPane = NCTabs.NCTabPane;
const nodecode = pageConfig.pagecode;
const searchId = pageConfig.searchId;

class SingleTable extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isshowseal: false,
			planData: null,
			sumData: {}
		};
		initTemplate.call(this, props);
	}

	componentDidMount() {}
	componentDidUpdate() {}

	getSumData = () => {
		let sumData = [];
		let total_num = 0;
		let checked_num = 0;
		let unchecked_num = 0;
		let match_num = 0;
		let unmatch_num = 0;
		let profit_num = 0;
		let loss_num = 0;
		this.state.planData.rows.map((item) => {
			total_num = item.values.total_num.value * 1 + total_num;
			checked_num = item.values.checked_num.value * 1 + checked_num;
			unchecked_num = item.values.unchecked_num.value * 1 + unchecked_num;
			match_num = item.values.match_num.value * 1 + match_num;
			unmatch_num = item.values.unmatch_num.value * 1 + unmatch_num;
			profit_num = item.values.profit_num.value * 1 + profit_num;
			loss_num = item.values.loss_num.value * 1 + loss_num;
		});
		sumData = { total_num, checked_num, unchecked_num, match_num, unmatch_num, profit_num, loss_num };
		this.setState({ sumData });
		return sumData;
	};

	render() {
		const { button, ncmodal, search, table } = this.props;
		const { createButtonApp } = button;
		const { createModal } = ncmodal;
		let { NCCreateSearch } = search;
		let tableId = pageConfig.tableId;
		let { createSimpleTable } = table;
		let sumData = {};

		this.state.planData &&
			this.state.planData.rows &&
			!this.state.sumData.total_num &&
			(sumData = this.getSumData());
		// this.state.sumData.total_num && (sumData = this.state.sumData);
		return (
			<div className="nc-bill-list" id="report-info">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						{createPageIcon()}
						<h2 className="title-search-detail">
							{getMultiLangByID('452015012A-000004') /* 国际化处理：盘点单报告 */}
						</h2>
					</div>
					<div className="header-button-area">
						{createButtonApp({
							//按钮区域（在数据库中注册的按钮区域）
							area: 'list_head',
							//按钮数量限制，超出指定数量的按钮将放入更多显示
							buttonLimit: 3,
							onButtonClick: buttonClick.bind(this),
							//指定下拉弹窗渲染的位置，不设置的话，下拉弹窗随滚动条滚动
							popContainer: document.querySelector('.header-button-area')
						})}
						{/* {createModal('delModal', { color: 'warning' })} */}
						{createModal(`${tableId}-del-confirm`, { color: 'warning' })}
					</div>
				</div>
				<div className="nc-bill-search-area">
					{NCCreateSearch(searchId, {
						clickSearchBtn: searchBtnClick.bind(this),
						showAdvBtn: false
					})}
				</div>
				<div className="nc-bill-table-area reportTab">
					<NCTabs navtype="turn" contenttype="moveleft" defaultActiveKey="echartsTab">
						<NCTabPane tab={getMultiLangByID('452015012A-000005') /* 国际化处理：统计图 */} key="echartsTab">
							{(this.state.planData == null || JSON.stringify(this.state.planData) == '{}') && (
								<div className="nodata-text">
									<span className="iconfont icon-jinggao" />
									<p>{getMultiLangByID('452015012A-000006') /* 国际化处理：暂无数据 */}</p>
								</div>
							)}
							<EchartsTab {...this.props} state={this.state} />
						</NCTabPane>
						<NCTabPane tab={getMultiLangByID('452015012A-000007') /* 国际化处理：明细 */} key="tableTab">
							{createSimpleTable(tableId, {})}
							{this.state.sumData &&
							this.state.sumData.total_num &&
							this.state.planData.rows && (
								<div className="detail-txt">
									<p>
										{getMultiLangByID('452015012A-000008') /* 国际化处理：总计： */}
										<span>{this.state.sumData.total_num}</span>
									</p>
									<p>
										{getMultiLangByID('452015012A-000009') /* 国际化处理：已盘点数： */}
										<span>{this.state.sumData.checked_num}</span>
									</p>
									<p>
										{getMultiLangByID('452015012A-000010') /* 国际化处理：未盘点数： */}
										<span>{this.state.sumData.unchecked_num}</span>
									</p>
									<p>
										{getMultiLangByID('452015012A-000011') /* 国际化处理：相符： */}
										<span>{this.state.sumData.match_num}</span>
									</p>
									<p>
										{getMultiLangByID('452015012A-000012') /* 国际化处理：不符： */}
										<span>{this.state.sumData.unmatch_num}</span>
									</p>
									<p>
										{getMultiLangByID('452015012A-000013') /* 国际化处理：盘盈： */}
										<span>{this.state.sumData.profit_num}</span>
									</p>
									<p>
										{getMultiLangByID('452015012A-000014') /* 国际化处理：盘亏： */}
										<span>{this.state.sumData.loss_num}</span>
									</p>
								</div>
							)}
						</NCTabPane>
					</NCTabs>
				</div>
			</div>
		);
	}
}

SingleTable = createPage({})(SingleTable);

initMultiLangByModule({ aum: [ '452015012A' ], ampub: [ 'common' ] }, () => {
	ReactDOM.render(<SingleTable />, document.querySelector('#app'));
});
