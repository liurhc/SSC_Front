import { toast, print, output } from 'nc-lightapp-front';
import { pageConfig } from './../const.js';
import ampub from 'ampub';
const { utils } = ampub;
const { msgUtils } = utils;
const { MsgConst, showMessage } = msgUtils;
export default function buttonClick(props, id) {
	switch (id) {
		case 'Print':
			printTemp.call(this);
			break;
		case 'Output':
			outputTemp.call(this);
			break;
		default:
			break;
	}
}

/**
 * 打印
 */
export function printTemp() {
	let printData = getPrintData.call(this, this.props, 'print');
	if (!printData) {
		showMessage.call(this, this.props, { type: MsgConst.Type.ChoosePrint });
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		'/nccloud/aum/report/print.do', // 后台打印服务url
		printData
	);
}

/**
 * 输出
 */
export function outputTemp() {
	let printData = getPrintData.call(this, this.props, 'output');
	if (!printData) {
		showMessage.call(this, this.props, { type: MsgConst.Type.ChooseOutput });
		return;
	}
	output({
		url: pageConfig.url.printCardUrl,
		data: printData
	});
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let pk = props.search.getAllSearchData('searchArea').conditions[0].value.firstvalue;
	if (!pk) {
		return;
	}
	let pks = [ pk ];
	let printData = {
		filename: pageConfig.pagecode, // 文件名称
		nodekey: pageConfig.printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}
