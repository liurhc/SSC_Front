import { pageConfig } from './../const.js';
import { searchBtnClick } from './index';
import ampub from 'ampub';
const { commonConst } = ampub;
const { StatusUtils } = commonConst;
const { UISTATE } = StatusUtils;

const tableId = pageConfig.tableId;

export default function initTemplate(props) {
	props.createUIDom(
		{
			//页面编码
			pagecode: pageConfig.pagecode
		},
		(data) => {
			if (data) {
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					props.button.setButtonsVisible(pageConfig.browseHeadBtns);
				}
				if (data.template) {
					let meta = data.template;
					modifierMeta(props, meta);
					props.meta.setMeta(meta, () => {
						afterInit.call(this);
					});
				}
			}
		}
	);
}

function afterInit() {
	let pk_plan = this.props.getUrlParam('id');
	let display = this.props.getUrlParam('display');
	if (pk_plan) {
		this.props.search.setSearchValue(pageConfig.searchId, [
			{
				field: 'pk_plan',
				oprtype: '=',
				value: { firstvalue: pk_plan },
				display: display
			}
		]);
		searchBtnClick.call(this, this.props, this.props.search.getAllSearchData(pageConfig.searchId));
	} else {
		this.props.button.setButtonDisabled(pageConfig.unableButton);
	}
}

const modifierMeta = (props, meta) => {
	meta[tableId].status = 'browse';

	meta[tableId].items.map((item) => {
		if (item.attrcode != 'pk_org_v.code' && item.attrcode != 'pk_org_v') {
			item.renderStatus = UISTATE.browse;
			item.render = (text, record, index) => {
				return (
					<div class="card-table-browse">
						<span
							className="code-detail-link"
							onClick={() => {
								props.openTo('/aum/inventory/inventory/main/index.html#/card', {
									appcode: pageConfig.inventory_app_code,
									pagecode: pageConfig.inventory_card_page_code,
									pk_plan: props.search.getSearchValByField(pageConfig.searchId, 'pk_plan').value
										.firstvalue,
									pk_org: record.pk_org_v.value,
									category: item.attrcode,
									status: 'detail'
								});
							}}
						>
							{record[item.attrcode] && record[item.attrcode].value}
						</span>
					</div>
				);
			};
		}
	});
	return meta;
};
