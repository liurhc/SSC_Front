import { ajax, cacheTools } from 'nc-lightapp-front';
import { pageConfig } from './../const.js';
import ampub from 'ampub';
const { utils } = ampub;
const { msgUtils } = utils;
const { MsgConst, showMessage } = msgUtils;
// 引入配置变量
const tableId = pageConfig.tableId;

//点击查询，获取查询区数据
export default function clickSearchBtn(props, searchVal) {
	if (!searchVal) {
		return;
	}
	// let pageInfo = props.table.getTablePageInfo(pageConfig.tableId);
	let data = props.search.getQueryInfo(pageConfig.searchId, true);
	if (data) {
		data.pagecode = pageConfig.pagecode;
		data.billtype = pageConfig.plan_bill_type;
	}

	ajax({
		url: '/nccloud/aum/report/query.do',
		data,
		success: (res) => {
			let searchVal = props.search.getAllSearchData('searchArea');
			cacheTools.set('searchParams', searchVal);
			if (res.data) {
				this.setState({ planData: res.data['list_head'], sumData: { total_num: null } });
				props.table.setAllTableData(tableId, res.data[tableId]);
				props.button.setButtonDisabled(pageConfig.enableButton);
			} else {
				this.setState({ planData: null, sumData: null });
				props.table.setAllTableData(tableId, { rows: [] });
				props.button.setButtonDisabled(pageConfig.unableButton);
				showMessage.call(this, props, { type: MsgConst.Type.QueryNoData });
			}
		}
	});
}
