import buttonClick from './buttonClicks';
import initTemplate from './initTemplate';
import searchBtnClick from './searchBtnClick';

export { buttonClick, initTemplate, searchBtnClick };
