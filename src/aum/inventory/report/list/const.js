// 页面配置
const pageConfig = {
	// 小应用主键
	appcode: '452015012A',
	// 表格区域id
	tableId: 'list_head',
	// 查询区
	searchId: 'searchArea',
	//列表区域类型
	areaType: 'table',
	// 表格节点编码
	pagecode: '452015012A_list',
	// 盘点计划单据类型
	plan_bill_type: '4A36',
	// 盘点单维护appcode
	inventory_app_code: '452005008A',
	// 盘点单维护卡片页面编码
	inventory_card_page_code: '452005008A_card',
	// 打印模板节点标识
	printNodekey: null,
	url: {
		printCardUrl: '/nccloud/aum/report/print.do'
	},
	// 浏览态按钮
	browseHeadBtns: {
		Print: true,
		Output: true
	},
	// 按钮可用
	enableButton: {
		Print: false,
		Output: false
	},
	// 按钮不可用
	unableButton: {
		Print: true,
		Output: true
	}
};

export { pageConfig };
