import { pageConfig } from '../const';

const card_head = pageConfig.areas.card.card_head;
const bodyvos = pageConfig.areas.card.bodyvos;

export default function afterEvent(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	if (moduleId == pageConfig.areas.card.baseArea) {
		baseAreaAfterEvent.call(this, props, key, value, changedRows, index, record, type, eventType);
	} else if (moduleId == pageConfig.areas.card.orgArea) {
		orgAreaAfterEvent.call(this, props, key, value, changedRows, index, record, type, eventType);
	} else if (moduleId == pageConfig.areas.card.dateArea) {
		dateAreaAfterEvent.call(this, props, key, value, changedRows, index, record, type, eventType);
	} else if (moduleId == pageConfig.areas.card.memoArea) {
		memoAreaAfterEvent.call(this, props, key, value, changedRows, index, record, type, eventType);
	} else if (typeof moduleId == 'object') {
		searchAreaAfterEvent.call(this, props, key, value, changedRows, index, record, type, eventType);
	}
}

function baseAreaAfterEvent(props, key, data, changedRows, index, record, type, eventType) {
	key == pageConfig.fieldName.bill_code &&
		this.props.form.setFormItemsValue(card_head, { bill_code: { value: data.value } });
	key == pageConfig.fieldName.bill_name &&
		this.props.form.setFormItemsValue(card_head, { bill_name: { value: data.value } });
}
function orgAreaAfterEvent(props, key, data, changedRows, index, record, type, eventType) {
	let indexs = [];
	this.props.cardTable.getVisibleRows(bodyvos).map((item, index) => {
		indexs.push(index);
	});

	this.props.cardTable.delRowsByIndex(bodyvos, indexs);

	let pk_orgs = data.value.split(',');
	if (pk_orgs[0]) {
		pk_orgs.map((item, index) => {
			this.props.cardTable.addRow(bodyvos, index, { pk_org_b: { value: item } }, false);
		});
	}
}
function searchAreaAfterEvent(props, key, data, changedRows, index, record, type, eventType) {}
function dateAreaAfterEvent(props, key, data, changedRows, index, record, type, eventType) {
	key == pageConfig.fieldName.start_date &&
		this.props.form.setFormItemsValue(card_head, { start_date: { value: data.value } });
	key == pageConfig.fieldName.end_date &&
		this.props.form.setFormItemsValue(card_head, { end_date: { value: data.value } });
}
function memoAreaAfterEvent(props, key, data, changedRows, index, record, type, eventType) {
	key == pageConfig.fieldName.memo && this.props.form.setFormItemsValue(card_head, { memo: { value: data.value } });
}
