import React, { Component } from 'react';
import { afterEvent } from './index';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
/**
 * @description:盘点计划查询区组件
 * 
 */
export default class CheckPlan extends Component {
	constructor(props) {
		super(props);
		this.childform0ID = 'searchArea';
		this.childform1ID = 'childform1';
		this.state = {
			// 新定义，全部按照此数据为准
			/***********/

			/***********/
			start_date: null, //盘点开始时间
			end_date: null, //盘点结束时间
			compareCheckedVal: {},
			pk_plan: null,
			alertTxt: '' //盘点范围提示框
		};
	}

	componentDidMount() {}

	render() {
		let { form, search, meta, cardTable } = this.props;
		let { createForm } = form;
		let { NCCreateSearch } = search;
		let { createCardTable } = cardTable;
		return (
			<div className="container-area">
				<div className="title-area">{getMultiLangByID('452005004A-000000') /* 国际化处理：编号与名称 */}</div>
				<div className="form-area1">
					{createForm('baseArea', {
						onAfterEvent: afterEvent.bind(this)
					})}
				</div>
				<div className="title-area">{getMultiLangByID('452005004A-000001') /* 国际化处理：参与资产组织 */}</div>
				<div className="form-area1">
					{createForm('orgArea', {
						onAfterEvent: afterEvent.bind(this)
					})}
				</div>
				<div className="title-area">
					{getMultiLangByID('452005004A-000002') /* 国际化处理：盘点范围 */}
					<span
						className="icon-alert iconfont icon-tixing"
						onMouseMove={() => {
							this.setState({ alertTxt: 'alertShow' });
						}}
						onMouseOut={() => {
							this.setState({ alertTxt: '' });
						}}
					/>
					{this.state.alertTxt === 'alertShow' && (
						<p className="alert-txt">
							{getMultiLangByID('452005004A-000003') /* 国际化处理：盘点范围为空，则默认是全部资产！ */}
						</p>
					)}
				</div>
				<div className="form-area2">
					{NCCreateSearch('searchArea', {
						onAfterEvent: afterEvent.bind(this),
						showAdvBtn: false,
						hideBtnArea: true
					})}
				</div>
				<div className="title-area">{getMultiLangByID('452005004A-000004') /* 国际化处理：盘点时间 */}</div>
				<div className="form-area1 form-date">
					{createForm('dateArea', { onAfterEvent: afterEvent.bind(this) })}
				</div>
				<div className="title-area">{getMultiLangByID('452005004A-000005') /* 国际化处理：盘点计划说明 */}</div>
				<div className="form-area1 form-textarea">
					{createForm('memoArea', { onAfterEvent: afterEvent.bind(this) })}
				</div>
				<div className="hide-area">{createForm('card_head', {})}</div>
				<div className="hide-area">{createCardTable('bodyvos', {})}</div>
			</div>
		);
	}
}
