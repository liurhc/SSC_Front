import React, { Component } from 'react';
import { createPage, base, high } from 'nc-lightapp-front';
import { CheckPlan } from './components';
import { buttonClick, initTemplate, pageInfoClick, commitAction } from './events';
import './index.less';
import { pageConfig } from './const';
import ampub from 'ampub';
const { components, commonConst, utils } = ampub;
const { StatusUtils } = commonConst;
const { UISTATE, BILLSTATUS } = StatusUtils;
const { ApprovalTrans, LoginContext } = components;
const { getContext, loginContextKeys } = LoginContext;
const { cardUtils, multiLangUtils, closeBrowserUtils } = utils;
const { createCardTitleArea } = cardUtils;
const { getMultiLangByID } = multiLangUtils;

const { NCAffix } = base;
/**
 * 盘点计划
 * @author : liuzy10
 */
class Maininfo extends Component {
	constructor(props) {
		super(props);
		this.state = {
			// 新定义，全部按照此数据为准
			/***********/
			showApprove: false,
			billCardData: {},
			ui_status: UISTATE.add,
			bill_status: BILLSTATUS.free_check,
			printData: {},
			bill_code: '',
			compositedisplay: false,
			compositedata: {},
			pk_plan: props.getUrlParam('id'),
			/***********/
			status,
			current: 0 //步骤条对应页面的参数
		};
		this.context = '';
		closeBrowserUtils(props, { form: [ 'baseArea', 'orgArea', 'dateArea', 'memoArea' ] });
		initTemplate.call(this, props);
		this.pk_org = 'pk_org';
		this.echartsData = {
			total_num: 0,
			start_time: 0,
			end_time: 0,
			pk_org: '',
			pk_type: '',
			linedata: {
				data: [],
				name: []
			}
		};
		this.childformData = {
			headRow: [],
			bodyRows: []
		};
		this.billCardData = {};
	}

	componentDidMount() {}

	backToList = () => {
		this.props.pushTo('/list', { pagecode: pageConfig.pagecode.list_pagecode });
	};

	//提交及指派 回调
	getAssginUsedr = (value) => {
		if (this.props.form.getFormStatus(pageConfig.areas.card.orgArea) == pageConfig.ui_status.edit) {
			commitAction.call(this, 'SAVE', 'saveCommit', value);
		} else {
			commitAction.call(this, 'SAVE', 'commit', value);
		}
	};
	//取消 指派
	turnOff = () => {
		this.setState({
			compositedisplay: false
		});
	};

	render() {
		let { data } = this.state;
		let { button, cardPagination, ncmodal } = this.props;
		let { createCardPagination } = cardPagination;
		let { createModal } = ncmodal;
		const { ApproveDetail } = high;
		const { createButtonApp } = button;
		let ui_status = this.state.ui_status;
		return (
			<div id="nc-bill-card">
				<div className="nc-bill-card" id="inventory-plan">
					{/* <div className="nc-bill-top-area"> */}
					<NCAffix>
						<div className="nc-bill-header-area">
							{createCardTitleArea.call(this, this.props, {
								title: getMultiLangByID(pageConfig.title),
								formId: pageConfig.areas.card.card_head,
								backBtnClick: this.backToList
							})}
							<div className="header-button-area">
								{createButtonApp({
									area: 'card_head',
									buttonLimit: 3,
									onButtonClick: buttonClick.bind(this),
									popContainer: document.querySelector('.header-button-area')
								})}
							</div>
							{ui_status == 'browse' ? (
								<div className="header-cardPagination-area">
									{createCardPagination({
										formId: pageConfig.areas.card.card_head,
										dataSource: pageConfig.dataSource,
										handlePageInfoChange: pageInfoClick.bind(this)
									})}
								</div>
							) : (
								''
							)}
						</div>
					</NCAffix>
					{/* </div> */}
					<div className="nc-bill-table-area">
						{
							<CheckPlan
								{...this.props}
								{...{ data }}
								childformData={this.childformData}
								// handleBillCardData={this.handleBillCardData}
								billCardData={this.billCardData}
							/>
						}
					</div>
				</div>
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002')} /* 国际化处理：指派 */
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				) : (
					''
				)}
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={getContext(loginContextKeys.transtype)}
					billid={this.state.pk_plan}
				/>

				{this.props.ncUploader.createNCUploader('warcontract-uploader', {})}
			</div>
		);
	}
}
const MasterChildCardBase = createPage({})(Maininfo);
export default MasterChildCardBase;
