import { appConfig } from './../const';

// 页面配置
const pageConfig = {
	...appConfig,
	// 节点名称
	title: '452005004A-000009' /* 国际化处理：盘点计划 */,
	// 查询类型
	querytype: 'tree',
	// 字段名
	fieldName: {
		bill_code: 'bill_code',
		bill_name: 'bill_name',
		start_date: 'start_date',
		end_date: 'end_date',
		memo: 'memo'
	},
	noCheckKeys: [ 'pk_group', 'dr', 'pk_plan', 'pk_org', 'pk_org_v', 'ts' ],
	// 卡片态按钮
	cardButtonVisible: {
		addHeadBtns: {
			Save: true,
			SaveCommit: true,
			Cancel: true,
			Add: false,
			Edit: false,
			Delete: false,
			Commit: false,
			UnCommit: false,
			File: false,
			Print: false,
			Output: false,
			Attachment: false,
			Refresh: false,
			QueryAboutBillFlow: false
		},
		editHeadBtns: {
			Save: true,
			SaveCommit: true,
			Cancel: true,
			Add: false,
			Edit: false,
			Delete: false,
			Commit: false,
			UnCommit: false,
			File: false,
			Print: false,
			Output: false,
			Attachment: false,
			Refresh: false,
			QueryAboutBillFlow: false
		},
		browseHeadBtns: {
			noData: {
				Save: false,
				SaveCommit: false,
				Cancel: false,
				Add: true,
				Edit: false,
				Delete: false,
				Commit: false,
				UnCommit: false,
				File: true,
				Print: false,
				Output: false,
				Attachment: false,
				Refresh: false,
				QueryAboutBillFlow: false
			},
			free_check: {
				Save: false,
				SaveCommit: false,
				Cancel: false,
				Add: true,
				Edit: true,
				Delete: true,
				Commit: true,
				UnCommit: false,
				File: true,
				Print: true,
				Output: true,
				Attachment: true,
				Refresh: true,
				QueryAboutBillFlow: true
			},
			un_check: {
				Save: false,
				SaveCommit: false,
				Cancel: false,
				Add: true,
				Edit: false,
				Delete: false,
				Commit: false,
				UnCommit: true,
				File: true,
				Print: true,
				Output: true,
				Attachment: true,
				Refresh: true,
				QueryAboutBillFlow: true
			},
			check_going: {
				Save: false,
				SaveCommit: false,
				Cancel: false,
				Add: true,
				Edit: false,
				Delete: false,
				Commit: false,
				UnCommit: false,
				File: true,
				Print: true,
				Output: true,
				Attachment: true,
				Refresh: true,
				QueryAboutBillFlow: true
			},
			check_pass: {
				Save: false,
				SaveCommit: false,
				Cancel: false,
				Add: true,
				Edit: false,
				Delete: false,
				Commit: false,
				UnCommit: true,
				File: true,
				Print: true,
				Output: true,
				Attachment: true,
				Refresh: true,
				QueryAboutBillFlow: true
			},
			check_nopass: {
				Save: false,
				SaveCommit: false,
				Cancel: false,
				Add: true,
				Edit: true,
				Delete: true,
				Commit: false,
				UnCommit: false,
				File: true,
				Print: true,
				Output: true,
				Attachment: true,
				Refresh: true,
				QueryAboutBillFlow: true
			}
		}
	}
};

export { pageConfig };
