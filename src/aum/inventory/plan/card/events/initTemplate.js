import { pageConfig } from '../const.js';
import { setStatus } from './buttonClick';
import { getDataByPk, addAction } from './index';
import ampub from 'ampub';
const { components } = ampub;
const { LoginContext, refInit } = components;
const { loginContext, getContext, loginContextKeys } = LoginContext;
const { commonRefCondition } = refInit;

export default function(props) {
	props.createUIDom(
		{
			pagecode: pageConfig.pagecode.card_pagecode //页面编码
		},
		(data) => {
			if (data.context) {
				// 初始化上下文变量
				loginContext(data.context);
			}
			if (data.button) {
				let button = data.button;
				props.button.setButtons(button, () => {});
			}
			if (data.template) {
				let meta = data.template;
				modifierMeta.call(this, props, meta);
				props.meta.setMeta(meta, () => {
					afterSetMeta.call(this);
				});
			}
		}
	);
}

/**
 * 模板设置完毕后操作
 */
function afterSetMeta() {
	let pk = this.props.getUrlParam('id');
	let ui_status = this.props.getUrlParam('status');
	if (ui_status == pageConfig.ui_status.add) {
		// 新增操作
		addAction.call(this, this.props);
	} else {
		// 修改操作
		excuteEdit.call(this, ui_status, pk);
	}
	this.setState({ ui_status });
}

/**
 * 执行修改操作
 * 
 * @param {*} status 
 * @param {*} pk 
 */
function excuteEdit(ui_status, pk) {
	if (pk && pk != 'undefined') {
		getDataByPk.call(this, pk);
		if (ui_status == pageConfig.ui_status.edit) {
			this.props.button.setButtonsVisible(pageConfig.cardButtonVisible.addHeadBtns);
		}
	} else {
		setStatus.call(this, this.props, ui_status);
	}
}

/**
 * 修改模板
 * 
 * @param {*} props 
 * @param {*} meta 
 */
function modifierMeta(props, meta) {
	let status = props.getUrlParam('status');
	meta[pageConfig.areas.card.baseArea].status = status;
	meta[pageConfig.areas.card.orgArea].status = status;
	meta[pageConfig.areas.card.dateArea].status = status;
	meta[pageConfig.areas.card.memoArea].status = status;
	// 盘点范围区域过滤
	meta[pageConfig.areas.card.searchArea].items.map((item) => {
		if (item.attrcode == 'pk_used_status') {
			item.queryCondition = () => {
				return {
					transi_type: getContext(loginContextKeys.transtype),
					bill_type: pageConfig.bill_type,
					GridRefActionExt: 'nccloud.web.aum.inventory.refcondition.EQUIP_STATUSSqlBuilder'
				};
			};
		}
		commonRefCondition.call(this, props, item);
	});
}
