import buttonClick, {
	hasNoData,
	setInterfaceValue,
	getDataByPk,
	editAction,
	setUIStatus,
	commitAction,
	addAction
} from './buttonClick';
import initTemplate from './initTemplate';
import pageInfoClick from './pageInfoClick';

export {
	buttonClick,
	initTemplate,
	pageInfoClick,
	hasNoData,
	setInterfaceValue,
	getDataByPk,
	editAction,
	setUIStatus,
	commitAction,
	addAction
};
