import { getDataByPk } from './index';

export default function(props, pk) {
	if (!pk || pk == 'null') {
		return;
	}
	// 更新参数
	props.setUrlParam({ id: pk });
	getDataByPk.call(this, pk);
}
