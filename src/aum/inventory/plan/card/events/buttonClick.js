import { ajax, toast, print, cardCache, output } from 'nc-lightapp-front';
import { pageConfig } from '../const.js';
const cardButtonVisible = pageConfig.cardButtonVisible;
const card_head = pageConfig.areas.card.card_head;
const bodyvos = pageConfig.areas.card.bodyvos;

import ampub from 'ampub';
const { components, commonConst, utils } = ampub;
const { StatusUtils, CommonKeys } = commonConst;
const { UISTATE, BILLSTATUS } = StatusUtils;
const { ScriptReturnUtils, LoginContext, queryAboutUtils } = components;
const { getScriptCardReturnData } = ScriptReturnUtils;
const { getContext, loginContextKeys } = LoginContext;
const { cardUtils, msgUtils } = utils;
const { setCardValue, setHeadAreaData } = cardUtils;
const { openApprove } = queryAboutUtils;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { approveConst } = CommonKeys;

export default function buttonClick(props, id) {
	switch (id) {
		case 'Save':
			// 保存
			saveAction.call(this, props);
			break;
		case 'Add':
			// 新增
			addAction.call(this, props);
			break;
		case 'Edit':
			// 修改
			editAction.call(this, props);
			break;
		case 'Cancel':
			// 取消
			cancelAction.call(this, props);
			break;
		case 'Delete':
			// 删除
			delConfirm.call(this, props);
			break;
		case 'Commit':
			// 提交
			commitAction.call(this, 'SAVE', 'commit');
			break;
		case 'SaveCommit':
			// 保存提交
			commitAction.call(this, 'SAVE', 'saveCommit');
			break;
		case 'UnCommit':
			// 收回
			commitAction.call(this, 'UNSAVE', '');
			break;
		case 'Print':
			// 打印
			printTemp.call(this, props);
			break;
		case 'Output':
			// 输出
			outputTemp.call(this, props);
			break;
		case 'Refresh':
			// 刷新
			refreshAction.call(this);
			break;
		case 'Attachment':
			//附件
			attachmentAction.call(this);
			break;
		case 'QueryAboutBillFlow':
			openApprove(this);
			break;
		default:
			break;
	}
}

function attachmentAction() {
	let billId = this.props.form.getFormItemsValue(card_head, pageConfig.vo_name.pk_plan).value;
	this.props.ncUploader.show('warcontract-uploader', {
		billId
	});
}

/**
 * 刷新按钮动作
 */
function refreshAction() {
	let pk = this.props.form.getFormItemsValue(pageConfig.areas.card.card_head, 'pk_plan').value;
	if (!pk) {
		return;
	}
	getDataByPk.call(this, pk, () => {
		showMessage.call(this, this.props, { type: MsgConst.Type.RefreshSuccess });
	});
}

function delConfirm(props) {
	let pk = this.props.form.getFormItemsValue(pageConfig.areas.card.card_head, 'pk_plan').value;
	if (!pk) {
		return;
	}
	showConfirm.call(this, props, { type: MsgConst.Type.Delete, beSureBtnClick: deleteAction });
}

/**
 * 打印
 * @param {*} props 
 */
export function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		showMessage.call(this, this.props, { type: MsgConst.Type.ChoosePrint });
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		pageConfig.url.printListUrl, // 后台打印服务url
		printData
	);
}

/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, this.props, { type: MsgConst.Type.ChooseOutput });
		return;
	}
	output({
		url: pageConfig.url.printCardUrl,
		data: printData
	});
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let pk = this.props.getUrlParam('id');
	if (!pk) {
		showMessage.call(this, this.props, { type: MsgConst.Type.ChoosePrint });
		return;
	}

	let pks = [ pk ];
	let printData = {
		filename: pageConfig.pagecode.list_pagecode, // 文件名称
		nodekey: '', // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

//提交
export function commitAction(OperatorType, commitType, content) {
	let commitData = null;
	let queryInfo = null;
	let pk = this.props.form.getFormItemsValue(card_head, 'pk_plan').value;
	let paramInfoMap = {};
	if (commitType === 'saveCommit') {
		// 保存前校验
		let pass = beforeSaveValidator.call(this, this.props, card_head, bodyvos, []);
		if (!pass) {
			return;
		}
		queryInfo = this.props.search.getQueryInfo(pageConfig.areas.card.searchArea, true);
		let CardData = this.props.createMasterChildData(pageConfig.pagecode.card_pagecode, card_head, bodyvos);
		CardData.head[pageConfig.areas.card.card_head].rows[0].values.pk_org.value = getContext(
			loginContextKeys.groupId
		);
		CardData.head[pageConfig.areas.card.card_head].rows[0].values.range_detail.value = JSON.stringify(
			queryInfo.querycondition
		);
		paramInfoMap[pk] = ''; //卡片页面ts为空也可以 CardData 里有ts
		let obj = {
			dataType: 'cardData',
			OperatorType: OperatorType,
			commitType: commitType,
			pageid: pageConfig.pagecode.card_pagecode,
			paramInfoMap: paramInfoMap,
			content: content,
			queryInfo
		};
		CardData.userjson = JSON.stringify(obj);
		commitData = CardData;
	} else {
		let ts = this.props.form.getFormItemsValue(card_head, 'ts').value;
		paramInfoMap[pk] = ts;
		commitData = {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: OperatorType,
			pageid: pageConfig.pagecode.card_pagecode,
			commitType: commitType, //提交
			content: content
		};
	}
	ajax({
		url: '/nccloud/aum/plan/commit.do',
		data: commitData,
		success: (res) => {
			if (res.data.success == approveConst.ALLSUCCESS) {
				if (content) {
					this.setState({
						compositedisplay: false
					});
				}
				res.data.cardVos[0].head[
					pageConfig.areas.card.card_head
				].rows[0].values.pk_org.display = this.props.form.getAllFormValue(
					pageConfig.areas.card.orgArea
				).rows[0].values.pk_org.display;
				res.data.cardVos[0].body = this.state.billCardData.body;
				let callBack = () => {
					this.props.form.setFormStatus(pageConfig.areas.card.card_head, pageConfig.ui_status.browse);
					this.setState({
						billCardData: res.data.cardVos[0],
						bill_status:
							res.data.cardVos[0].head[pageConfig.areas.card.card_head].rows[0].values.bill_status.value,
						bill_code:
							res.data.cardVos[0].head[pageConfig.areas.card.card_head].rows[0].values.bill_code.value,
						pk_plan: res.data.cardVos[0].head[pageConfig.areas.card.card_head].rows[0].values.pk_plan.value
					});
					this.props.setUrlParam({
						id: res.data.cardVos[0].head[pageConfig.areas.card.card_head].rows[0].values.pk_plan.value,
						status: UISTATE.browse
					});
					setStatus.call(this, this.props, UISTATE.browse, this.state.bill_status);
				};
				getScriptCardReturnData.call(
					this,
					res,
					this.props,
					pageConfig.areas.card.card_head,
					pageConfig.areas.card.bodyvos,
					pageConfig.vo_name.pk_plan,
					pageConfig.dataSource,
					setMasterAndChildData,
					false,
					callBack
				);
			} else if (res.data.success === approveConst.HASAPPROVEALTRAN) {
				//指派信息
				let workflowData = res.data.approvealTranData;
				if (
					workflowData.workflow &&
					(workflowData.workflow == approveConst.APPROVEFLOW ||
						workflowData.workflow == approveConst.WORKFLOW)
				) {
					this.setState({
						compositedata: workflowData,
						compositedisplay: true
					});
				}
			} else {
				toast({ content: res.data.errorMsg, color: 'danger' });
			}
		}
	});
}

/**
 * 保存
 * @param {*} props 
 */
function saveAction(props) {
	// 保存前校验
	let pass = beforeSaveValidator.call(this, this.props, card_head, bodyvos, pageConfig.noCheckKeys);
	if (!pass) {
		return;
	}
	let billCard = this.props.createMasterChildData(pageConfig.pagecode.card_pagecode, card_head, bodyvos);
	let queryInfo = props.search.getQueryInfo(pageConfig.areas.card.searchArea, true);
	billCard.head[pageConfig.areas.card.card_head].rows[0].values.range_detail.value = JSON.stringify(
		queryInfo.querycondition
	);
	let url = pageConfig.url.insertUrl; //新增保存
	if (this.state.ui_status === pageConfig.ui_status.edit) {
		url = pageConfig.url.updateUrl; //修改保存
	}
	let saveData = {
		billCard,
		queryInfo
	};
	ajax({
		url: url,
		data: saveData,
		success: (res) => {
			afterSave.call(this, res.data);
		}
	});
}

/**
 * 保存后操作
 * 
 * @param {*} data 
 */
function afterSave(data) {
	data.head[pageConfig.areas.card.card_head].rows[0].values.pk_org.display = this.props.form.getAllFormValue(
		pageConfig.areas.card.orgArea
	).rows[0].values.pk_org.display;
	showMessage.call(this, this.props, { type: MsgConst.Type.SaveSuccess });
	// 设置主子表数据(此区域是隐藏区域)
	setMasterAndChildData.call(this, data);
	// 保存成功后处理缓存
	if (this.state.ui_status == pageConfig.ui_status.add) {
		dealCache.call(this, pageConfig.operateType.add, data);
	} else {
		dealCache.call(this, pageConfig.operateType.update, data);
	}
	this.setState({
		ui_status: UISTATE.browse,
		billCardData: data,
		bill_status: data.head[pageConfig.areas.card.card_head].rows[0].values.bill_status.value,
		bill_code: data.head[pageConfig.areas.card.card_head].rows[0].values.bill_code.value,
		pk_plan: data.head[pageConfig.areas.card.card_head].rows[0].values.pk_plan.value
	});
	this.props.setUrlParam({
		id: data.head[pageConfig.areas.card.card_head].rows[0].values.pk_plan.value,
		status: UISTATE.browse
	});
	// 设置UI状态：按钮、表单
	setUIStatus.call(this, false, this.state.bill_status);
	this.props.cardPagination.setCardPaginationId({
		id: data.head[pageConfig.areas.card.card_head].rows[0].values.pk_plan.value,
		status: 1
	});
}

/**
 * 修改
 * @param {*} props 
 */
export function editAction(props) {
	this.setState({ ui_status: pageConfig.ui_status.edit });
	// setStatus.call(this, props, pageConfig.ui_status.edit);
	setUIStatus.call(
		this,
		false,
		this.props.form.getFormItemsValue(pageConfig.areas.card.card_head, 'bill_status').value,
		pageConfig.ui_status.edit
	);
}

/**
 * 新增
 * @param {*} props 
 */
export function addAction(props) {
	// 清空数据
	emptyAllValue.call(this);
	// 设置默认值
	setDefaultValue.call(this);
	// 设置界面状态和按钮显示
	setUIStatus.call(
		this,
		false,
		this.props.form.getFormItemsValue(card_head, 'bill_status').value,
		pageConfig.ui_status.add
	);
}

export function setDefaultValue() {
	let pk_group = getContext(loginContextKeys.groupId);
	// 因集团级节点，所以pk_org和pk_org_v设置为pk_group
	let pk_org = getContext(loginContextKeys.groupId);
	let pk_org_v = getContext(loginContextKeys.groupId);
	let bill_type = pageConfig.bill_type;
	let transi_type = getContext(loginContextKeys.transtype);
	let pk_transitype = getContext(loginContextKeys.pk_transtype);
	let start_date = getContext(loginContextKeys.businessDate);
	// 界面展示的默认值设置
	this.props.form.setFormItemsValue(pageConfig.areas.card.dateArea, { start_date: { value: start_date } });
	// 主子表结构默认值设置
	this.props.form.setFormItemsValue(card_head, {
		pk_group: { value: pk_group },
		pk_org: { value: pk_org },
		pk_org_v: { value: pk_org_v },
		bill_type: { value: bill_type },
		transi_type: { value: transi_type },
		pk_transitype: { value: pk_transitype },
		start_date: { value: start_date },
		bill_status: { value: '0' }
	});
}

/**
 * 取消
 * @param {*} props 
 */
function cancelAction(props) {
	showConfirm.call(this, props, { type: MsgConst.Type.Cancel, beSureBtnClick: doCancel });
}

function doCancel(props) {
	/* 
	列表界面新增——取消
	缓存无数据：卡片界面空白，有新增按钮（组）
	缓存有数据：加载当前页最后一条数据
	*/
	/* 
	卡片界面新增——取消
	加载当前列表最后一条数据
	*/
	if (this.state.ui_status == pageConfig.ui_status.edit) {
		let pk = this.props.getUrlParam('id');
		getDataByPk.call(this, pk, () => {
			setUIStatus.call(
				this,
				false,
				this.props.form.getFormItemsValue(pageConfig.areas.card.card_head, 'bill_status').value,
				pageConfig.ui_status.browse
			);
		});
	} else {
		let currentLastId = cardCache.getCurrentLastId(pageConfig.dataSource);
		// 平台bug，缓存暂时取不到
		currentLastId
			? getDataByPk.call(this, currentLastId, (data) => {
					setUIStatus.call(
						this,
						false,
						data.head[pageConfig.areas.card.card_head].rows[0].values.bill_status.value,
						pageConfig.ui_status.browse
					);
				})
			: hasNoData.call(this);
	}
}

/**
 * 根据pk查询盘点计划
 * @param {*} pk 
 */
export function getDataByPk(pk, callBack) {
	if (!pk) {
		return;
	}
	ajax({
		url: '/nccloud/aum/plan/queryCard.do',
		data: { pk, pagecode: pageConfig.pagecode.card_pagecode },
		success: (res) => {
			if (res.data) {
				let headData = res.data.head[pageConfig.areas.card.card_head].rows[0].values;
				// 设置state属性值
				this.setState({
					bill_status: headData.bill_status.value,
					ui_status: this.state.ui_status,
					billCardData: res.data,
					bill_code: headData.bill_code.value
				});
				// 设置主子表数据
				setMasterAndChildData.call(this, res.data);
				// 设置界面显示值
				setInterfaceValue.call(this, res.data);
				// 设置界面状态和按钮显示
				setUIStatus.call(this, false, headData.bill_status.value);
				callBack && typeof callBack == 'function' && callBack.call(this, res.data);
			} else {
				hasNoData.call(this);
			}
		}
	});
}

/**
 * 设置主子表数据
 * @param {*} data 
 */
export function setMasterAndChildData(data) {
	setCardValue.call(this, this.props, data, pageConfig.areas.card.card_head, pageConfig.areas.card.bodyvos);
}

/**
 * 删除
 * @param {*} props 
 */
function deleteAction(props) {
	let paramInfoMap = {};
	let pk = this.props.form.getFormItemsValue(pageConfig.areas.card.card_head, 'pk_plan').value;
	paramInfoMap[pk] = this.props.form.getFormItemsValue(pageConfig.areas.card.card_head, 'ts').value;
	ajax({
		url: '/nccloud/aum/plan/delete.do',
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: pageConfig.pagecode.card_pagecode
		},
		success: (res) => {
			let { success, data } = res;
			if (success) {
				let callback = (newpk) => {
					if (!newpk) {
						this.props.button.setButtonsVisible(pageConfig.cardButtonVisible.browseHeadBtns.noData);
						emptyAllValue.call(this);
						setHeadAreaData.call(this, props, { bill_code: '' });
					}
					// 更新参数
					this.props.setUrlParam({ id: newpk });
					// 加载下一条数据
					getDataByPk.call(this, newpk);
				};
				getScriptCardReturnData.call(
					this,
					res,
					this.props,
					pageConfig.areas.card.card_head,
					pageConfig.areas.card.bodyvos,
					pageConfig.vo_name.pk_plan,
					pageConfig.dataSource,
					undefined,
					true,
					callback
				);
			}
		}
	});
}

function loadDataByPk(props, newpk) {
	if (!newpk) {
		return;
	}
	ajax({
		url: '/nccloud/aum/plan/queryCard.do',
		data: { newpk, pagecode: pageConfig.pagecode.card_pagecode },
		success: (res) => {
			if (res.data) {
				this.state.bill_status =
					res.data.billCard.head[pageConfig.areas.card.card_head].rows[0].values.bill_status.value;
				this.state.ui_status = ui_status;
				this.state.billCardData = res.data.billCard;
				if (res.data.billCard.head && res.data.billCard.head[pageConfig.areas.card.card_head]) {
					props.form.setAllFormValue({
						[pageConfig.areas.card.card_head]: res.data.billCard.head[pageConfig.areas.card.card_head]
					});
				}
				if (res.data.billCard.body && res.data.billCard.body[pageConfig.areas.card.bodyvos]) {
					props.cardTable.setTableData(
						pageConfig.areas.card.bodyvos,
						res.data.billCard.body[pageConfig.areas.card.bodyvos]
					);
				}
				setInterfaceValue.call(this, res);
				setStatus.call(this, props, ui_status, this.state.bill_status);
			} else {
				this.props.button.setButtonsVisible(pageConfig.cardButtonVisible.browseHeadBtns.noData);
				emptyAllValue.call(this);
			}
		}
	});
}

/**
 * 设置界面状态
 * @param {*} props 
 * @param {*} status 
 */
export function setStatus(props, ui_status, bill_status) {
	this && this.setState({ ui_status: ui_status });
	let buttonsVisible = cardButtonVisible;
	props.form.setFormStatus(pageConfig.areas.card.baseArea, ui_status);
	props.form.setFormStatus(pageConfig.areas.card.orgArea, ui_status);
	props.form.setFormStatus(pageConfig.areas.card.dateArea, ui_status);
	props.form.setFormStatus(pageConfig.areas.card.memoArea, ui_status);
	setHeadAreaData.call(this, this.props, { status: ui_status });
	switch (ui_status) {
		// ui状态为 新增
		case UISTATE.add:
			props.button.setButtonsVisible(buttonsVisible.addHeadBtns);
			props.search.setDisabled(pageConfig.areas.card.searchArea, false);
			break;
		// ui状态为	修改
		case UISTATE.edit:
			props.button.setButtonsVisible(buttonsVisible.editHeadBtns);
			props.search.setDisabled(pageConfig.areas.card.searchArea, false);
			break;
		// ui状态为 浏览
		case UISTATE.browse:
			!bill_status && (bill_status = this.billCardData.bill_status);
			if (bill_status == BILLSTATUS.free_check) {
				// 单据状态	自由态
				props.button.setButtonsVisible(buttonsVisible.browseHeadBtns.free_check);
			} else if (bill_status == BILLSTATUS.un_check) {
				// 单据状态	 已提交，未审核
				props.button.setButtonsVisible(buttonsVisible.browseHeadBtns.un_check);
			} else if (bill_status == BILLSTATUS.check_going) {
				// 单据状态	 审核中
				props.button.setButtonsVisible(buttonsVisible.browseHeadBtns.check_going);
			} else if (bill_status == BILLSTATUS.check_pass) {
				// 单据状态	 审核通过
				props.button.setButtonsVisible(buttonsVisible.browseHeadBtns.check_pass);
			} else if (bill_status == BILLSTATUS.check_nopass) {
				// 单据状态	 审核不通过
				props.button.setButtonsVisible(buttonsVisible.browseHeadBtns.check_nopass);
			}
			props.search.setDisabled(pageConfig.areas.card.searchArea, true);
			break;
		default:
			break;
	}
}

export function getAllInterfaceData(props) {
	let InterfaceData = {};
	// 获取 计划编号 和 计划名称
	let bill_code = props.form.getAllFormValue(pageConfig.areas.card.baseArea).rows[0].values.bill_code;
	let bill_name = props.form.getAllFormValue(pageConfig.areas.card.baseArea).rows[0].values.bill_name;
	// 获取 开始结束日期
	let date = props.form.getAllFormValue(pageConfig.areas.card.dateArea).rows[0].values;
	let invStartDate = date.start_date;
	let invEndDate = date.end_date;
	// 获取 参与资产组织
	let orgsObj4web = props.form.getAllFormValue(pageConfig.areas.card.orgArea).rows[0].values.pk_org.value;
	// 获取 查询条件
	let queryInfo = props.search.getQueryInfo(pageConfig.areas.card.searchArea, true);
	// 获取 范围明细
	let rangeObj4web = null;
	queryInfo.querycondition.conditions.length != 0 && (rangeObj4web = queryInfo.querycondition.conditions);
	// 获取 备注
	let memo = props.form.getAllFormValue(pageConfig.areas.card.memoArea).rows[0].values.memo.value;
	InterfaceData.baseArea = {};
	InterfaceData.baseArea.bill_code = bill_code;
	InterfaceData.baseArea.bill_name = bill_name;
	InterfaceData.orgsObj4web = orgsObj4web;
	InterfaceData.queryInfo = queryInfo;
	InterfaceData.rangeObj4web = rangeObj4web;
	InterfaceData.dateArea = {};
	InterfaceData.dateArea.invStartDate = invStartDate;
	InterfaceData.dateArea.invEndDate = invEndDate;
	InterfaceData.memo = memo;
	return InterfaceData;
}

export function constructBillCard(props, InterfaceData, pk_plan, vo_status) {
	let BillCard = {};
	BillCard.pageid = pageConfig.pagecode.card_pagecode;
	BillCard.bill_type = pageConfig.bill_type;
	// 组装表体
	BillCard.body = {};
	BillCard.body[pageConfig.areas.card.bodyvos] = {};
	BillCard.body[pageConfig.areas.card.bodyvos].areaType = 'table';
	BillCard.body[pageConfig.areas.card.bodyvos].areacode = pageConfig.areas.card.bodyvos;
	BillCard.body[pageConfig.areas.card.bodyvos].rows = [];
	let orgArr = InterfaceData.orgsObj4web.split(',');
	orgArr.map((item) => {
		BillCard.body[pageConfig.areas.card.bodyvos].rows.push({
			status: vo_status,
			values: { pk_org_b: { display: null, scale: 0, value: item } }
		});
	});
	// 组装表头
	BillCard.head = {};
	BillCard.head.pageid = pageConfig.pagecode.card_pagecode;
	BillCard.head[pageConfig.areas.card.card_head] = {};
	BillCard.head[pageConfig.areas.card.card_head].areaType = 'form';
	BillCard.head[pageConfig.areas.card.card_head].areacode = pageConfig.areas.card.card_head;
	BillCard.head[pageConfig.areas.card.card_head].rows = [];
	BillCard.head[pageConfig.areas.card.card_head].rows.push({});
	BillCard.head[pageConfig.areas.card.card_head].rows[0].status = vo_status;
	BillCard.head[pageConfig.areas.card.card_head].rows[0].values = {};
	BillCard.head[pageConfig.areas.card.card_head].rows[0].values.bill_code = InterfaceData.baseArea.bill_code;
	BillCard.head[pageConfig.areas.card.card_head].rows[0].values.bill_name = InterfaceData.baseArea.bill_name;
	BillCard.head[pageConfig.areas.card.card_head].rows[0].values.start_date = InterfaceData.dateArea.invStartDate;
	BillCard.head[pageConfig.areas.card.card_head].rows[0].values.end_date = InterfaceData.dateArea.invEndDate;
	BillCard.head[pageConfig.areas.card.card_head].rows[0].values.check_range = {
		display: null,
		value: null
	};
	BillCard.head[pageConfig.areas.card.card_head].rows[0].values.range_detail = {
		display: null,
		value: null
	};
	BillCard.head[pageConfig.areas.card.card_head].rows[0].values.memo = {
		value: InterfaceData.memo
	};
	BillCard = setDefaultValues.call(this, BillCard, pk_plan);

	return BillCard;
}

function setDefaultValues(BillCard, pk_plan) {
	pk_plan &&
		(BillCard.head[pageConfig.areas.card.card_head].rows[0].values.pk_plan = {
			value: pk_plan
		});
	BillCard.head[pageConfig.areas.card.card_head].rows[0].values.transi_type = {
		value: getContext(loginContextKeys.transtype)
	};
	BillCard.head[pageConfig.areas.card.card_head].rows[0].values.pk_group = {
		value: getContext(loginContextKeys.groupId)
	};
	BillCard.head[pageConfig.areas.card.card_head].rows[0].values.pk_org = {
		value: getContext(loginContextKeys.groupId)
	};
	BillCard.head[pageConfig.areas.card.card_head].rows[0].values.pk_org_v = {
		value: getContext(loginContextKeys.groupId)
	};
	return BillCard;
}

/**
 * 界面无数据，卡片界面为空，显示新增按钮（组）
 * @param {*} ui_status 
 */
export function hasNoData() {
	emptyAllValue.call(this);
	setUIStatus.call(this, true, undefined, pageConfig.ui_status.browse);
}

/**
 * 设置界面状态
 * 		1.按钮可用状态
 * 		2.界面可编辑性
 * @param {*} specialStatus 特殊参数
 * 		true 有特殊性，需要设置界面为空状态，无数据
 * 		false 无特殊性，根据ui_status设置界面样式
 * @param {*} bill_status 单据状态
 * 		pageConfig.bill_status
 * @param {*} newUI_status UI状态
 * 		pageConfig.ui_status 
 */
export function setUIStatus(specialStatus, bill_status, newUI_status) {
	let ui_status = this.state.ui_status;
	if (newUI_status) {
		ui_status = newUI_status;
	}
	if (specialStatus) {
		this.props.form.setFormStatus(pageConfig.areas.card.card_head, pageConfig.ui_status.browse);
		// 按钮只显示新增
		this.props.button.setButtonsVisible(pageConfig.cardButtonVisible.browseHeadBtns.noData);
		// 界面变为不可用
		setInterfaceUIStatus.call(this, false);
		setHeadAreaData.call(this, this.props, { status: pageConfig.ui_status.browse });
	} else if (ui_status == pageConfig.ui_status.add || ui_status == pageConfig.ui_status.edit) {
		this.props.form.setFormStatus(pageConfig.areas.card.card_head, ui_status);
		// 界面可用
		setInterfaceUIStatus.call(this, true);
		// 按钮显示新增态按钮
		this.props.button.setButtonsVisible(pageConfig.cardButtonVisible.addHeadBtns);
		setHeadAreaData.call(this, this.props, { status: ui_status });
	} else if (ui_status == pageConfig.ui_status.browse) {
		if (bill_status == BILLSTATUS.free_check) {
			// 单据状态	自由态
			this.props.button.setButtonsVisible(pageConfig.cardButtonVisible.browseHeadBtns.free_check);
		} else if (bill_status == BILLSTATUS.un_check) {
			// 单据状态	 已提交，未审核
			this.props.button.setButtonsVisible(pageConfig.cardButtonVisible.browseHeadBtns.un_check);
		} else if (bill_status == BILLSTATUS.check_going) {
			// 单据状态	 审核中
			this.props.button.setButtonsVisible(pageConfig.cardButtonVisible.browseHeadBtns.check_going);
		} else if (bill_status == BILLSTATUS.check_pass) {
			// 单据状态	 审核通过
			this.props.button.setButtonsVisible(pageConfig.cardButtonVisible.browseHeadBtns.check_pass);
		} else if (bill_status == BILLSTATUS.check_nopass) {
			// 单据状态	 审核不通过
			this.props.button.setButtonsVisible(pageConfig.cardButtonVisible.browseHeadBtns.check_nopass);
		}
		this.props.form.setFormStatus(pageConfig.areas.card.card_head, ui_status);
		// 界面变为不可用
		setInterfaceUIStatus.call(this, false);
		setHeadAreaData.call(this, this.props, { status: ui_status });
	}
	this.setState({ ui_status });
}

/**
 * 设置界面UI状态：可用、不可用
 * @default boolean=false
 * @param {*} boolean 
 */
export function setInterfaceUIStatus(boolean = false) {
	if (boolean) {
		this.props.form.setFormStatus(pageConfig.areas.card.baseArea, pageConfig.ui_status.edit);
		this.props.form.setFormStatus(pageConfig.areas.card.orgArea, pageConfig.ui_status.edit);
		// 注意：暂时改为浏览态，平台bug，有值情况下，编辑态会报：e.toDate is not a function
		this.props.form.setFormStatus(pageConfig.areas.card.dateArea, pageConfig.ui_status.edit);
		this.props.form.setFormStatus(pageConfig.areas.card.memoArea, pageConfig.ui_status.edit);
	} else {
		this.props.form.setFormStatus(pageConfig.areas.card.baseArea, pageConfig.ui_status.browse);
		this.props.form.setFormStatus(pageConfig.areas.card.orgArea, pageConfig.ui_status.browse);
		this.props.form.setFormStatus(pageConfig.areas.card.dateArea, pageConfig.ui_status.browse);
		this.props.form.setFormStatus(pageConfig.areas.card.memoArea, pageConfig.ui_status.browse);
	}
	this.props.search.setDisabled(pageConfig.areas.card.searchArea, !boolean);
}

/**
 * 设置界面显示值
 * @param {*} data 
 */
export function setInterfaceValue(data) {
	if (!data) {
		return;
	}
	let headData = data.head[pageConfig.areas.card.card_head].rows[0].values;
	// 给盘点计划和名称赋值
	this.props.form.setFormItemsValue(pageConfig.areas.card.baseArea, {
		bill_code: { value: headData.bill_code.value },
		bill_name: { value: headData.bill_name.value }
	});
	// 给参与资产组织赋值orgAreaData
	let orgAreaData = {};
	data.body[pageConfig.areas.card.bodyvos].rows.map((item, index) => {
		if (item.values.pk_org_b) {
			if (index == 0) {
				orgAreaData.value = item.values.pk_org_b.value;
				orgAreaData.display = item.values.pk_org_b.display;
			} else {
				orgAreaData.value = orgAreaData.value + ',' + item.values.pk_org_b.value;
				orgAreaData.display = orgAreaData.display + ',' + item.values.pk_org_b.display;
			}
		}
	});
	this.props.form.setFormItemsValue(pageConfig.areas.card.orgArea, {
		pk_org: { value: orgAreaData.value, display: orgAreaData.display }
	});
	// 给盘点范围赋值
	let queryInfo = JSON.parse(headData.range_detail.value);
	if (queryInfo && queryInfo.conditions && queryInfo.conditions.length > 0) {
		this.props.search.setSearchValue(pageConfig.areas.card.searchArea, queryInfo);
	} else {
		this.props.search.clearSearchArea(pageConfig.areas.card.searchArea);
	}
	// 给盘点开始结束日期赋值
	this.props.form.setFormItemsValue(pageConfig.areas.card.dateArea, {
		end_date: { value: headData.end_date.value }
	});
	// 给盘点开始日期赋值
	this.props.form.setFormItemsValue(pageConfig.areas.card.dateArea, {
		start_date: { value: headData.start_date.value }
	});
	// 给盘点计划说明赋值
	this.props.form.setFormItemsValue(pageConfig.areas.card.memoArea, {
		memo: { value: headData.memo.value }
	});
}

/**
 * 处理缓存：新增、更新、删除
 * @param {*} operateType 
 */
export function dealCache(operateType = pageConfig.operateType.add, data) {
	let pk = this.props.form.getFormItemsValue(pageConfig.areas.card.card_head, 'pk_plan').value;
	if (operateType == pageConfig.operateType.add) {
		cardCache.addCache(pk, data, pageConfig.areas.card.card_head, pageConfig.dataSource);
	} else if (operateType == pageConfig.operateType.update) {
		cardCache.updateCache('pk_plan', pk, data, pageConfig.areas.card.card_head, pageConfig.dataSource);
	} else {
		// 清除缓存，加载下一条数据
		cardCache.deleteCacheById('pk_plan', pk, pageConfig.dataSource);
		// 加载下一条数据
		let newpk = cardCache.getNextId(pk, pageConfig.dataSource);
		loadDataByPk.call(this, this.props, newpk);
	}
}

/**
 * 清空所有显示数据
 */
export function emptyAllValue() {
	this.props.form.EmptyAllFormValue(pageConfig.areas.card.baseArea);
	this.props.form.EmptyAllFormValue(pageConfig.areas.card.orgArea);
	this.props.form.EmptyAllFormValue(pageConfig.areas.card.dateArea);
	this.props.form.EmptyAllFormValue(pageConfig.areas.card.memoArea);
	this.props.search.clearSearchArea(pageConfig.areas.card.searchArea);
	this.props.form.EmptyAllFormValue(card_head);
	this.props.cardTable.setTableData(bodyvos, { rows: [] });
	this.setState({ bill_code: '' });
}

function beforeSaveValidator(props, formId, moduleId, keys = []) {
	// 清除空表体白行
	props.cardTable.filterEmptyRows(moduleId, keys);
	//校验表头必输项
	if (
		!(
			props.form.isCheckNow(pageConfig.areas.card.baseArea) &&
			props.form.isCheckNow(pageConfig.areas.card.orgArea) &&
			props.form.isCheckNow(pageConfig.areas.card.dateArea)
		)
	) {
		return false;
	}
	return true;
}
