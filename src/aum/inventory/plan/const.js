import ampub from 'ampub';
const { commonConst } = ampub;
const { StatusUtils } = commonConst;
const { UISTATE, BILLSTATUS, VOSTATUS } = StatusUtils;

// 应用级配置
const appConfig = {
	appcode: '452005004A',
	bill_type: '4A36',
	transi_type: '4A36-01',
	dataSource: 'aum.inventory.plan.main',
	operateType: {
		add: 'add',
		delete: 'delete',
		update: 'update'
	},
	ui_status: {
		...UISTATE
	},
	bill_status: {
		...BILLSTATUS
	},
	vo_status: {
		...VOSTATUS
	},
	vo_name: {
		pk_plan: 'pk_plan',
		bill_code: 'bill_code'
	},
	// 页面编码
	pagecode: {
		list_pagecode: '452005004A_list',
		// 卡片页面编码
		card_pagecode: '452005004A_card'
	},
	url: {
		printListUrl: '/nccloud/aum/plan/printCard.do',
		printCardUrl: '/nccloud/aum/plan/printCard.do',
		insertUrl: '/nccloud/aum/plan/save.do',
		updateUrl: '/nccloud/aum/plan/update.do'
	},
	areas: {
		list: {
			// 表格区
			tableId: 'list_head',
			// 查询区域编码
			searchId: 'searchArea'
		},
		card: {
			// 表头信息
			card_head: 'card_head',
			// 详细信息
			bodyvos: 'bodyvos',
			// 参与资产组织
			baseArea: 'baseArea',
			// 参与资产组织
			orgArea: 'orgArea',
			// 盘点计划之范围查询
			searchArea: 'searchArea',
			// 详情
			memoArea: 'memoArea',
			// 开始结束日期
			dateArea: 'dateArea'
		}
	}
};

export { appConfig };
