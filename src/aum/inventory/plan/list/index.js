//主子表列表

import React, { Component } from 'react';
import { createPage, high, createPageIcon } from 'nc-lightapp-front';
import { buttonClick, initTemplate, searchBtnClick, pageInfoClick, doubleClick, rowSelected } from './events';
import { pageConfig } from './const.js';
import { setBatchBtnsEnable, commitAction } from './events';
import ampub from 'ampub';
const { components, utils } = ampub;
const { ApprovalTrans, LoginContext } = components;
const { getContext, loginContextKeys } = LoginContext;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
class List extends Component {
	constructor(props) {
		super(props);
		this.state = {
			printData: {},
			compositedisplay: false,
			compositedata: {},
			showApprove: false
		};
		initTemplate.call(this, props);
	}
	componentDidMount() {}

	//提交及指派 回调
	getAssginUsedr = (value) => {
		commitAction.call(this, 'SAVE', this.props, value);
	};
	//取消 指派
	turnOff = () => {
		this.props.table.selectAllRows(pageConfig.areas.list.tableId, false);
		setBatchBtnsEnable.call(this, this.props);
		this.setState({
			compositedisplay: false
		});
	};

	render() {
		let { table, button, ncmodal, search } = this.props;
		const { createButtonApp } = button;
		const { createModal } = ncmodal;

		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { ApproveDetail } = high;
		return (
			<div className="nc-bill-list">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						{createPageIcon()}
						<h2 className="title-search-detail">
							{getMultiLangByID('452005004A-000009') /* 国际化处理：盘点计划 */}
						</h2>
					</div>
					<div className="header-button-area">
						{createButtonApp({
							area: 'list_head',
							buttonLimit: 3,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
						{createModal(`${pageConfig.areas.list.tableId}-del-confirm`, { color: 'warning' })}
					</div>
				</div>
				<div className="nc-bill-search-area">
					{NCCreateSearch(pageConfig.areas.list.searchId, {
						clickSearchBtn: searchBtnClick.bind(this),
						dataSource: pageConfig.dataSource
					})}
				</div>
				<div className="nc-bill-table-area">
					{createSimpleTable(pageConfig.areas.list.tableId, {
						handlePageInfoChange: pageInfoClick,
						showIndex: true,
						showCheck: true,
						onRowDoubleClick: doubleClick.bind(this),
						onSelected: rowSelected.bind(this),
						onSelectedAll: rowSelected.bind(this),
						dataSource: pageConfig.dataSource,
						pkname: 'pk_plan'
					})}
				</div>
				{/* 审批详情 */}
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={getContext(loginContextKeys.transtype)}
					billid={this.state.pk_plan}
				/>
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002') /* 国际化处理：指派 */}
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				) : (
					''
				)}
				{this.props.ncUploader.createNCUploader('warcontract-uploader', {})}
			</div>
		);
	}
}

const MasterChildListBase = createPage({})(List);

export default MasterChildListBase;
