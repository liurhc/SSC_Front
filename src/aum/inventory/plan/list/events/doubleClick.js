import { pageConfig } from '../const';
import ampub from 'ampub';
const { commonConst } = ampub;
const { StatusUtils } = commonConst;
const { UISTATE } = StatusUtils;

export default function doubleClick(record, index, e) {
	this.props.pushTo('/card', {
		pagecode: pageConfig.pagecode.card_pagecode,
		status: UISTATE.browse,
		id: record.pk_plan.value
	});
}
