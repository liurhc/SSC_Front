import { pageConfig } from './../const.js';
import { ajax, toast } from 'nc-lightapp-front';
import ampub from 'ampub';
const { components, commonConst } = ampub;
const { queryAboutUtils, ScriptReturnUtils } = components;
const { StatusUtils } = commonConst;
const { UISTATE } = StatusUtils;
const { getScriptListReturn } = ScriptReturnUtils;
const { openListApprove } = queryAboutUtils;
const dataSource = pageConfig.dataSource;
/**
 * 表格操作按钮
 * @param {*} props 
 * @param {*} key 
 * @param {*} text 
 * @param {*} record 
 * @param {*} index 
 */
export default function(props, key, text, record, index) {
	switch (key) {
		case 'Delete':
			// 删除行
			deleteLineAction.call(this, props, index, text, record);
			break;
		case 'Edit':
			// 修改
			props.pushTo('/card', {
				pagecode: pageConfig.pagecode.card_pagecode,
				id: record.pk_plan.value,
				status: UISTATE.edit
			});
			break;
		case 'Commit':
			// 提交
			commitClick.call(this, 'SAVE', props, index, record);
			break;
		case 'UnCommit':
			// 收回
			commitClick.call(this, 'UNSAVE', props, index, record);
			break;
		case 'CheckResult':
			// 查看盘点结果 TODO：此处跳转需要修改
			props.openTo('/aum/inventory/report/list/index.html', {
				appcode: pageConfig.reportAppCode,
				pagecode: pageConfig.reportPageCode,
				id: record.pk_plan.value,
				display: record.bill_name.value
			});
			break;
		case 'QueryAboutBillFlow':
			openListApprove(this, record, pageConfig.vo_name.pk_plan);
			break;
		default:
			break;
	}
}

/**
 * 提交
 * @param {*} props 
 * @param {*} index 
 * @param {*} record 
 */
function commitClick(OperatorType, props, index, record) {
	let paramInfoMap = {};
	paramInfoMap[record.pk_plan.value] = record.ts.value;
	let params = [ { id: record.pk_plan.value, index: index } ];

	ajax({
		url: '/nccloud/aum/plan/commit.do',
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: OperatorType,
			pageid: pageConfig.pagecode.card_pagecode,
			commitType: 'commit'
		},
		success: (res) => {
			getScriptListReturn.call(
				this,
				params,
				res,
				props,
				'pk_plan',
				pageConfig.areas.card.card_head,
				pageConfig.areas.list.tableId,
				false,
				dataSource
			);
		}
	});
}

/**
 * 表格行删除
 * @param {*} props 
 * @param {*} index 
 * @param {*} text
 */
function deleteLineAction(props, index, text, record) {
	let paramInfoMap = {};
	paramInfoMap[record.pk_plan.value] = record.ts.value;
	let params = [ { id: record.pk_plan.value, index: index } ];
	ajax({
		url: '/nccloud/aum/plan/delete.do',
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: pageConfig.pagecode.card_pagecode
		},
		success: (res) => {
			getScriptListReturn(
				params,
				res,
				props,
				'pk_plan',
				pageConfig.areas.card.card_head,
				pageConfig.areas.list.tableId,
				true
			);
		}
	});
}
