import { ajax } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import ampub from 'ampub';
const { utils } = ampub;
const { listUtils } = utils;
const { setListValue } = listUtils;

export default function(props, config, pks) {
	let data = {
		allpks: pks,
		pagecode: pageConfig.pagecode.list_pagecode
	};
	ajax({
		url: '/nccloud/aum/plan/queryPage.do',
		data: data,
		success: function(res) {
			setListValue.call(this, props, res);
		}
	});
}
