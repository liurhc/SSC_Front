import { ajax, toast } from 'nc-lightapp-front';
import { pageConfig } from './../const.js';
import { setBatchBtnsEnable } from './index';
import ampub from 'ampub';
const { components, utils } = ampub;
const { LoginContext } = components;
const { loginContextKeys, getContext } = LoginContext;
const { listUtils } = utils;
const { setQueryInfoCache, setListValue, listConst } = listUtils;

//点击查询，获取查询区数据
export default function clickSearchBtn(props, searchVal, type, queryInfo, isRefresh) {
	if (!searchVal) {
		return;
	}
	if (!queryInfo) {
		queryInfo = props.search.getQueryInfo(pageConfig.areas.list.searchId);
	}
	let pageInfo = props.table.getTablePageInfo(pageConfig.areas.list.tableId);
	queryInfo.pagecode = pageConfig.pagecode.list_pagecode;
	queryInfo.pageInfo = pageInfo;
	queryInfo.billtype = pageConfig.bill_type;
	queryInfo.transtype = getContext(loginContextKeys.transtype);
	// 缓存查询条件
	setQueryInfoCache.call(this, queryInfo, isRefresh, type);
	ajax({
		url: '/nccloud/aum/plan/query.do',
		data: queryInfo,
		success: (res) => {
			setListValue.call(
				this,
				props,
				res,
				pageConfig.areas.list.tableId,
				isRefresh ? pageConfig.setValueType.refresh : pageConfig.setValueType.query
			);
			setBatchBtnsEnable.call(this, props, pageConfig.areas.list.tableId);
		}
	});
}
