import { ajax, toast, print, output } from 'nc-lightapp-front';
import { pageConfig } from './../const.js';
import { searchBtnClick } from './index';
import ampub from 'ampub';
const { components, commonConst, utils } = ampub;
const { ScriptReturnUtils } = components;
const { getScriptListReturn } = ScriptReturnUtils;
const { listUtils, msgUtils, multiLangUtils } = utils;
const { setBillFlowBtnsEnable, batchRefresh } = listUtils;
const { StatusUtils } = commonConst;
const { UISTATE } = StatusUtils;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { getMultiLangByID } = multiLangUtils;

const dataSource = pageConfig.dataSource;

export default function buttonClick(props, id) {
	switch (id) {
		case 'Add':
			props.pushTo('/card', {
				pagecode: '452005004A_card',
				status: UISTATE.add
			});
			break;
		case 'Delete':
			delConfirm.call(this, props);
			break;
		case 'Commit':
			commitAction.call(this, 'SAVE', props);
			break;
		case 'UnCommit':
			commitAction.call(this, 'UNSAVE', props);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'Refresh':
			refreshAction.call(this, props);
			break;
		case 'Attachment':
			attachmentAction.call(this);
			break;
		default:
			break;
	}
}

/**
 * 附件
 */
function attachmentAction() {
	let checkedrows = this.props.table.getCheckedRows(pageConfig.areas.list.tableId);
	let billNo = checkedrows[0].data.values['bill_code'].value;
	let billId = checkedrows[0].data.values['pk_plan'].value;
	this.props.ncUploader.show('warcontract-uploader', {
		billId,
		billNo
	});
}

function refreshAction(props) {
	batchRefresh.call(this, props, searchBtnClick);
}

/**
 * 打印
 * @param {*} props 
 */
export function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		pageConfig.url.printListUrl, // 后台打印服务url
		printData
	);
}

/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });
		return;
	}
	output({
		url: pageConfig.url.printCardUrl,
		data: printData
	});
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let checkedRows = props.table.getCheckedRows(pageConfig.areas.list.tableId);
	if (!checkedRows || checkedRows.length == 0) {
		return false;
	}
	let pks = [];
	checkedRows.map((item) => {
		pks.push(item.data.values['pk_plan'].value);
	});
	let printData = {
		filename: pageConfig.pagecode.list_pagecode, // 文件名称
		nodekey: '', // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

/**
 * 表头删除按钮
 * 
 * @param {*} props 
 */
function delConfirm(props) {
	let checkedRows = props.table.getCheckedRows(pageConfig.areas.list.tableId);
	if (checkedRows.length == 0) {
		showMessage.call(this, this.props, { type: MsgConst.Type.ChooseDelete });
		return;
	}
	showConfirm.call(this, props, {
		type: MsgConst.Type.DelSelect,
		beSureBtnClick: deleteAction
	});
}

/**
 * 表头提交按钮
 * @param {*} props 
 */
export function commitAction(OperatorType, props, content) {
	let data = props.table.getCheckedRows(pageConfig.areas.list.tableId);
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values.pk_plan.value;
		let ts = v.data.values.ts.value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	let commitType = '';
	if (OperatorType === 'SAVE') {
		//提交传这个
		commitType = 'commit';
	}
	ajax({
		url: '/nccloud/aum/plan/commit.do',
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: OperatorType,
			pageid: pageConfig.pagecode.card_pagecode,
			commitType: commitType, //提交
			content: content
		},
		success: (res) => {
			if (content) {
				this.setState({
					compositedisplay: false
				});
			}
			getScriptListReturn.call(
				this,
				params,
				res,
				props,
				'pk_plan',
				pageConfig.areas.card.card_head,
				pageConfig.areas.list.tableId,
				false,
				dataSource
			);
		}
	});
}

/**
 * 表头收回按钮
 * @param {*} props 
 */
const UnCommitAction = (props) => {
	let data = props.table.getCheckedRows(pageConfig.areas.list.tableId);
	let hasData = JSON.stringify(data) != '[]';
	if (hasData) {
		let paramInfoMap = {};
		data.map((v) => {
			let id = v.data.values.pk_plan.value;
			let ts = v.data.values.ts.value;
			paramInfoMap[id] = ts;
		});
		ajax({
			url: '/nccloud/aum/plan/commit.do',
			data: {
				paramInfoMap: paramInfoMap,
				dataType: 'listData',
				OperatorType: 'UNSAVE',
				pageid: pageConfig.pagecode,
				commitType: 'commit' //提交
			},
			success: (res) => {
				if (res.data.success === 'allsuccess') {
					toast({ content: res.data.successMsg, color: 'success' });
					props.table.updateDataByIndexs(pageConfig.areas.list.tableId, [
						{ index: 0, data: res.data.cardVo.head.rows[0] }
					]);
				} else {
					toast({ content: res.data.errorMsg, color: 'warning' });
				}
			}
		});
	} else {
		showMessage.call(this, props, {
			content: getMultiLangByID('452005004A-000010') /* 国际化处理：请选择要收回的数据： */,
			color: 'warning'
		});
	}
};

/**
 * 浏览态删除单个数据
 * @param {*} props 
 * @param {*} index 
 * @param {*} text
 */
export function deleteAction(props, data) {
	data = props.table.getCheckedRows(pageConfig.areas.list.tableId);
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values.pk_plan.value;
		let ts = v.data.values.ts.value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	ajax({
		url: '/nccloud/aum/plan/delete.do',
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: pageConfig.pagecode.card_pagecode
		},
		success: (res) => {
			getScriptListReturn(
				params,
				res,
				props,
				'pk_plan',
				pageConfig.areas.card.card_head,
				pageConfig.areas.list.tableId,
				true
			);
		}
	});
}

/**
 * 批量设置按钮是否可用
 * @param {*} props 
 */
export function setBatchBtnsEnable(props) {
	setBillFlowBtnsEnable.call(this, props);
}
