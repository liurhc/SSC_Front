import { pageConfig } from './../const.js';
import tableButtonClick from './tableButtonClick';
import { setBatchBtnsEnable } from './buttonClick.js';
import ampub from 'ampub';
const { components, commonConst, utils } = ampub;
const { StatusUtils } = commonConst;
const { UISTATE } = StatusUtils;
const { LoginContext } = components;
const { loginContext } = LoginContext;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

export default function(props) {
	props.createUIDom(
		{
			pagecode: pageConfig.pagecode.list_pagecode //页面id
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button, () => {
						// props.button.setButtonsVisible(pageConfig.btnConfig.btnVisible.head);
						setBatchBtnsEnable.call(this, props);
					});
					props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001') /* 国际化处理：确定要删除吗？ */);
				}
				if (data.template) {
					let meta = data.template;
					modifierMeta.call(this, props, meta, pageConfig.areas.list.tableId);
					props.meta.setMeta(meta, () => {});
				}
			}
		}
	);
}

function modifierMeta(props, meta) {
	const { button } = props;
	const { createOprationButton } = button;
	//分页控件显示标识
	meta[pageConfig.areas.list.tableId].pagination = true;
	//添加操作列
	meta[pageConfig.areas.list.tableId].items.push({
		attrcode: 'opr',
		label: getMultiLangByID('amcommon-000000') /* 国际化处理：操作 */,
		width: '230px',
		fixed: 'right',
		className: 'table-opr',
		itemtype: 'customer',
		visible: true,
		render: (text, record, index) => {
			let bill_status = record.bill_status.value;
			let buttonAry = [];
			bill_status == 0 && (buttonAry = pageConfig.btnConfig.btnVisible.list.free_check);
			bill_status == 1 && (buttonAry = pageConfig.btnConfig.btnVisible.list.un_check);
			bill_status == 2 && (buttonAry = pageConfig.btnConfig.btnVisible.list.check_going);
			bill_status == 3 && (buttonAry = pageConfig.btnConfig.btnVisible.list.check_pass);
			bill_status == 4 && (buttonAry = pageConfig.btnConfig.btnVisible.list.check_nopass);
			return createOprationButton(buttonAry, {
				area: 'list_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => tableButtonClick.call(this, props, key, text, record, index)
			});
		}
	});

	//修改列渲染样式
	meta[pageConfig.areas.list.tableId].items.map((item, key) => {
		if (item.attrcode == 'bill_code') {
			item.render = (text, record, index) => {
				return (
					<div
						class="simple-table-td"
						field="bill_code"
						fieldname={getMultiLangByID('452005004A-000011') /* 国际化处理：计划编号 */}
					>
						<span
							className="code-detail-link"
							onClick={() => {
								props.pushTo('/card', {
									pagecode: pageConfig.pagecode.card_pagecode,
									status: UISTATE.browse,
									id: record.pk_plan.value
								});
							}}
						>
							{record && record.bill_code && record.bill_code.value}
						</span>
					</div>
				);
			};
		}
		return item;
	});

	return meta;
}
