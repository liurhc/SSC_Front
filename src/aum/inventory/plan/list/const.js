import { appConfig } from './../const';

// 页面配置
const pageConfig = {
	...appConfig,
	// 节点名称
	title: '452005004A-000009' /* 国际化处理：盘点计划 */,
	// 浏览态查询区查询类型
	querytype: 'tree',
	setValueType: {
		refresh: 'refresh',
		query: 'query'
	},
	// 浏览态按钮
	browseHeadBtns: {
		Add: true,
		Commit: false
	},
	// 盘点报告应用编码
	reportAppCode: '452015012A',
	reportPageCode: '452015012A_list',
	// 单据 所有 行操作按钮
	// all: ['Edit', 'Delete', 'Commit', 'UnCommit', 'CheckResult'],
	// 单据 自由态 行操作按钮
	free: [ 'Edit', 'Delete', 'Commit', 'CheckResult' ],
	// 单据 已提交 行操作按钮
	submission: [ 'UnCommit', 'CheckResult' ],
	// 单据 已审核 行操作按钮
	audited: [ 'CheckResult' ],
	// 列表态按钮
	listButtonVisible: {
		headBtns: {
			Save: true,
			SaveCommit: true,
			Cancel: true,
			Add: false,
			Edit: false,
			Delete: false,
			Commit: false,
			UnCommit: false,
			File: false,
			Print: false,
			Preview: false,
			OutPut: false
		},
		innerBtns: {
			free: [ 'Commit', 'Edit', 'Delete' ],
			submission: [ 'UnCommit', 'CheckResult' ],
			audited: [ 'UnCommit', 'CheckResult' ]
		}
	},
	btnConfig: {
		btnVisible: {
			head: {
				Add: true,
				Delete: true,
				Commit: true,
				UnCommit: true,
				Attachment: true,
				Print: true,
				Output: true,
				Refresh: true
			},
			list: {
				/** 自由态 */ free_check: [ 'Commit', 'Edit', 'Delete' ],
				/** 未审核(已提交) */ un_check: [ 'UnCommit', 'QueryAboutBillFlow' ],
				/** 审核中 */ check_going: [ 'QueryAboutBillFlow' ],
				/** 审核通过 */ check_pass: [ 'UnCommit', 'QueryAboutBillFlow', 'CheckResult' ],
				/** 审核未通过 */ check_nopass: [ 'Edit', 'Delete', 'QueryAboutBillFlow' ]
			}
		},
		btnUsability: {
			// 无数据
			noData: {
				Add: false,
				Delete: true,
				Commit: true,
				UnCommit: true,
				Attachment: true,
				QueryAbout: true,
				QueryAboutBusiness: true,
				Print: true,
				Output: true
			},
			multi: {
				Add: false,
				Delete: false,
				Commit: false,
				UnCommit: false,
				Attachment: false,
				Print: false,
				Output: false,
				Refresh: false
			},
			un_checkAndCheck_pass: {
				Add: false,
				Delete: true,
				Commit: true,
				UnCommit: false,
				Attachment: false,
				Print: false,
				Output: false,
				Refresh: false
			},
			// 只有自由态
			onlyFree_check: {
				Add: false,
				Delete: false,
				Commit: false,
				UnCommit: true,
				Attachment: false,
				Print: false,
				Output: false,
				Refresh: false
			},
			// 只有未审核
			onlyUn_check: {
				Add: false,
				Delete: true,
				Commit: true,
				UnCommit: false,
				Attachment: false,
				Print: false,
				Output: false,
				Refresh: false
			},
			// 只有审核中
			onlyCheck_going: {
				Add: false,
				Delete: true,
				Commit: true,
				UnCommit: true,
				Attachment: false,
				Print: false,
				Output: false,
				Refresh: false
			},
			// 只有审核通过
			onlyCheck_pass: {
				Add: false,
				Delete: true,
				Commit: true,
				UnCommit: false,
				Attachment: false,
				Print: false,
				Output: false,
				Refresh: false
			}
		}
	}
};

export { pageConfig };
