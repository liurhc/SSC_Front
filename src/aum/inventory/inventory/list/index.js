//主子表列表

import React, { Component } from 'react';
import { createPage, high, createPageIcon } from 'nc-lightapp-front';
import { buttonClick, initTemplate, searchBtnClick, pageInfoClick, doubleClick, rowSelected } from './events';
import { pageConfig } from './const.js';
import { commitAction, setBatchBtnsEnable } from './events';
import ampub from 'ampub';
const { components, utils } = ampub;
const { ApprovalTrans } = components;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

class List extends Component {
	constructor(props) {
		super(props);
		this.state = {
			show: false /* 单据追溯显示标识 */,
			showApprove: false /* 审批详情显示标识 */,
			pk_inventory: '' /* 盘点单主键 */,
			compositedisplay: false /* 指派标识 */,
			compositedata: {} /* 指派信息 */
		};
		initTemplate.call(this, props);
	}

	/* 提交及指派 回调 */
	getAssginUsedr = (value) => {
		commitAction.call(this, 'SAVE', this.props, value);
	};

	/* 取消指派 */
	turnOff = () => {
		/* 指派前有可能选中所有的行，取消指派时需要取消选中的所有行 */
		this.props.table.selectAllRows(pageConfig.areas.list.tableId, false);
		setBatchBtnsEnable.call(this, this.props);
		this.setState({ compositedisplay: false });
	};

	render() {
		let { table, button, search } = this.props;
		let { createButtonApp } = button;
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { BillTrack, ApproveDetail } = high;
		return (
			<div className="nc-bill-list">
				{/* 头部区 */}
				<div className="nc-bill-header-area">
					{/* 标题区 */}
					<div className="header-title-search-area">
						{createPageIcon()}
						<h2 className="title-search-detail">{getMultiLangByID('452005008A-000033') /* 国际化处理：盘点单 */}</h2>
					</div>
					{/* 按钮区 */}
					<div className="header-button-area">
						{createButtonApp({
							area: 'list_head',
							buttonLimit: 3,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				{/* 查询区 */}
				<div className="nc-bill-search-area">
					{NCCreateSearch(pageConfig.areas.list.searchId, {
						clickSearchBtn: searchBtnClick.bind(this)
					})}
				</div>
				{/* 表格区 */}
				<div className="nc-bill-table-area">
					{createSimpleTable(pageConfig.areas.list.tableId, {
						handlePageInfoChange: pageInfoClick,
						showIndex: true,
						showCheck: true,
						onRowDoubleClick: doubleClick.bind(this),
						onSelected: rowSelected.bind(this),
						onSelectedAll: rowSelected.bind(this),
						dataSource: pageConfig.dataSource,
						pkname: pageConfig.vo_name.pk_inventory
					})}
				</div>
				{/* 审批详情 */}
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transi_type}
					billid={this.state.pk_inventory}
				/>
				{/* 单据追溯 */}
				<BillTrack
					show={this.state.show}
					close={() => {
						this.setState({ show: false });
					}}
					pk={this.state.pk_inventory} //单据id
					type={pageConfig.bill_type} //单据类型
				/>
				{/* 附件 */}
				{this.props.ncUploader.createNCUploader('warcontract-uploader', {})}
				{/* 提交及指派 */}
				{this.state.compositedisplay && (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002') /* 国际化处理：指派 */}
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				)}
			</div>
		);
	}
}

const MasterChildListBase = createPage({})(List);

export default MasterChildListBase;
