import { pageConfig } from './../const.js';
import { setBatchBtnsEnable } from './buttonClick';
import tableButtonClick from './tableButtonClick';
import ampub from 'ampub';
const { components, commonConst, utils } = ampub;
const { assetOrgMultiRefFilter, LoginContext, refInit } = components;
const { AssetOrgMultiRefFilter } = assetOrgMultiRefFilter;
const { CommonKeys } = commonConst;
const { IBusiRoleConst } = CommonKeys;
const { loginContext, getContext, loginContextKeys } = LoginContext;
const { defRefCondition, commonRefCondition } = refInit;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

const tableId = pageConfig.areas.list.tableId;
const searchId = pageConfig.areas.list.searchId;

export default function(props) {
	props.createUIDom(
		{
			pagecode: pageConfig.pagecode.list_pagecode //页面id
		},
		(data) => {
			if (data) {
				if (data.context) {
					//初始化上下文变量
					loginContext.call(this, data.context);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button, () => {
						setBatchBtnsEnable.call(this, props);
					});
					// 行删除时悬浮框提示
					props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001') /* 国际化处理：确定要删除吗？ */);
				}
				if (data.template) {
					let meta = data.template;
					modifierMeta.call(this, props, meta, tableId);
					props.meta.setMeta(meta, () => {});
				}
			}
		}
	);
}

function modifierMeta(props, meta) {
	//分页控件显示标识
	meta[tableId].pagination = true;
	// 参照过滤
	filterRefer.call(this, props, meta);
	// 增加操作列
	addOperateCol.call(this, props, meta);
	// 单据号添加超链接
	addLinkInBillCode.call(this, props, meta);
	return meta;
}

function addLinkInBillCode(props, meta) {
	meta[tableId].items = meta[tableId].items.map((item, key) => {
		//单据号添加下划线超链接
		if (item.attrcode == 'bill_code') {
			item.render = (text, record, index) => {
				return (
					<div
						class="simple-table-td"
						field="bill_code"
						fieldname={getMultiLangByID('452005008A-000044') /* 国际化处理：盘点单号 */}
					>
						<span
							className="code-detail-link"
							onClick={() => {
								props.pushTo('/card', {
									pagecode: pageConfig.pagecode.card_pagecode,
									status: pageConfig.ui_status.result,
									id: record.pk_inventory.value
								});
							}}
						>
							{record && record.bill_code && record.bill_code.value}
						</span>
					</div>
				);
			};
		}
		return item;
	});
}

function addOperateCol(props, meta) {
	//添加操作列
	meta[tableId].items.push({
		attrcode: 'opr',
		label: getMultiLangByID('amcommon-000000') /* 国际化处理：操作 */,
		width: '250px',
		fixed: 'right',
		className: 'table-opr',
		itemtype: 'customer',
		visible: true,
		render: (text, record, index) => {
			let bill_status = record.bill_status.value;
			let buttonAry = [];
			// 自由态
			bill_status == 0 && (buttonAry = pageConfig.listButtonVisible.innerBtns.freeAudit);
			// 已提交(未审核)
			bill_status == 1 && (buttonAry = pageConfig.listButtonVisible.innerBtns.unAudit);
			// 审核中
			bill_status == 2 && (buttonAry = pageConfig.listButtonVisible.innerBtns.auditing);
			// 审核通过
			bill_status == 3 && (buttonAry = pageConfig.listButtonVisible.innerBtns.auditPass);
			// 审核不通过
			bill_status == 4 && (buttonAry = pageConfig.listButtonVisible.innerBtns.auditNoPass);
			// 平台22号盘props不包括自定义属性，先用_props
			return props.button.createOprationButton(buttonAry, {
				area: 'list_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => tableButtonClick.call(this, props, key, text, record, index)
			});
		}
	});
}
function filterRefer(props, meta) {
	// 参照查询条件过滤
	meta[searchId].items.map((item, key) => {
		if (
			item.attrcode == 'pk_org' ||
			item.attrcode == 'bodyvos.pk_equip.pk_ownerorg' ||
			item.attrcode == 'bodyvos.pk_equip.pk_usedorg'
		) {
			item.queryCondition = () => {
				return { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter' };
			};
		} else if (
			item.attrcode == 'billmaker' ||
			item.attrcode == 'auditor' ||
			item.attrcode == 'bodyvos.pk_equip.pk_manager' ||
			item.attrcode == 'bodyvos.pk_equip.pk_user'
		) {
			// 组织多选参照过滤  注：此处参照的还是集团加组织下的所有人员，只是加了上面可以根据组织过滤的框，
			// 所以，编辑后事件不用加组织单选条件下不显示业务单元的控制
			AssetOrgMultiRefFilter.call(this, props, searchId, item, 'pk_org');
		} else if (item.attrcode == 'bodyvos.pk_usedept_before' || item.attrcode == 'bodyvos.pk_assetuser_before') {
			// 盘点前使用部门、责任人参照盘点前使用权
			if (item.attrcode == 'bodyvos.pk_usedept_before') {
				item.isRunWithChildren = true;
				item.defaultRunWithChildren = true;
			}
			AssetOrgMultiRefFilter.call(this, props, searchId, item, 'bodyvos.pk_usedunit_before');
			getQueryCondition.call(this, props, item, 'bodyvos.pk_usedunit_before');
		} else if (item.attrcode == 'bodyvos.pk_usedept_after' || item.attrcode == 'bodyvos.pk_assetuser_after') {
			// 盘点后使用部门、责任人参照盘点后使用权
			if (item.attrcode == 'bodyvos.pk_usedept_after') {
				item.isRunWithChildren = true;
				item.defaultRunWithChildren = true;
			}
			AssetOrgMultiRefFilter.call(this, props, searchId, item, 'bodyvos.pk_usedunit_after');
			getQueryCondition.call(this, props, item, 'bodyvos.pk_usedunit_after');
		} else if (item.attrcode == 'bodyvos.pk_equip.pk_mandept' || item.attrcode == 'bodyvos.pk_equip.pk_mandept_v') {
			item.isRunWithChildren = true;
			item.defaultRunWithChildren = true;
			AssetOrgMultiRefFilter.call(this, props, searchId, item, 'bodyvos.pk_equip.pk_ownerorg');
		} else if (item.attrcode == 'bodyvos.pk_equip.pk_usedept' || item.attrcode == 'bodyvos.pk_equip.pk_usedept_v') {
			item.isRunWithChildren = true;
			item.defaultRunWithChildren = true;
			AssetOrgMultiRefFilter.call(this, props, searchId, item, 'bodyvos.pk_equip.pk_usedorg');
		} else if (item.attrcode == 'bodyvos.pk_category') {
			// 给资产类别加上权限控制
			item.dataPowerOperationCode = 'default';
			item.isDataPowerEnable = true;
		} else {
			// 适配自定义项1
			let bodyDefPrefix = 'bodyvos.def';
			let groupId = getContext(loginContextKeys.groupId);
			defRefCondition.call(this, props, item, searchId, groupId, true, 'pk_org', bodyDefPrefix);
			defRefCondition.call(this, props, item, searchId, groupId, true);
		}
		commonRefCondition.call(this, props, item);
	});
}

/** 
 * *获取查询条件的值
 */
function getSearchValue(props, field) {
	let data = props.search.getSearchValByField(searchId, field);
	if (data && data.value && data.value.firstvalue) {
		let firstvalue = data.value.firstvalue;
		if (firstvalue.split(',').length == 1) {
			return firstvalue;
		}
	}
}

/**
 * 获取字段的过滤条件，针对是部门和人员等需要组织多选参照的字段
 */
function getQueryCondition(props, item, org) {
	item.queryCondition = () => {
		let pk_org = getSearchValue.call(this, props, org); //主组织
		let filter = { busifuncode: IBusiRoleConst.ASSETORG, isShowUnit: true };
		if (pk_org) {
			filter['pk_org'] = pk_org;
		}
		return filter; // 根据pk_org过滤
	};
}
