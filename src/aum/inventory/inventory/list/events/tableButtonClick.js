import { pageConfig } from './../const.js';
import { ajax, toast } from 'nc-lightapp-front';
import ampub from 'ampub';
const { components } = ampub;
const { ScriptReturnUtils, queryAboutUtils } = components;
const { getScriptListReturn } = ScriptReturnUtils;
const { openListApprove } = queryAboutUtils;

const dataSource = pageConfig.dataSource;

/**
 * 表格操作按钮
 * @param {*} props 
 * @param {*} key 
 * @param {*} text 
 * @param {*} record 
 * @param {*} index 
 */
export default function(props, key, text, record, index) {
	switch (key) {
		case 'Delete':
			// 删除行
			delLine.call(this, props, index, text, record);
			break;
		case 'ReAssign':
			// 重新分配盘点人
			editAction.call(this, props, text, record);
			break;
		case 'Commit':
			// 提交
			commitClick.call(this, 'SAVE', props, index, record);
			break;
		case 'UnCommit':
			// 收回
			commitClick.call(this, 'UNSAVE', props, index, record);
			break;
		case 'CheckResult':
			// 查看盘点结果
			props.pushTo('/card', {
				id: record.pk_inventory.value,
				status: pageConfig.ui_status.result,
				pagecode: pageConfig.pagecode.card_pagecode
			});
			break;
		case 'QueryAboutBillFlow':
			openListApprove.call(this, this, record, 'pk_inventory');
			break;
		default:
			break;
	}
}

function editAction(props, text, record) {
	ajax({
		url: '/nccloud/aum/inventory/inventory_card_edit.do',
		data: {
			pk: record.pk_inventory.value,
			resourceCode: pageConfig.node_code
		},
		success: (res) => {
			if (res.data) {
				toast({ color: 'warning', content: res.data });
			} else {
				props.pushTo('/card', {
					id: record.pk_inventory.value,
					status: pageConfig.ui_status.edit,
					appcode: pageConfig.appcode,
					pagecode: pageConfig.pagecode.card_pagecode
				});
			}
		}
	});
}

/**
 * 提交 收回 动作
 * @param {*} props 
 * @param {*} tableId 
 * @param {*} index 
 * @param {*} record 
 */
function commitClick(OperatorType, props, index, record) {
	let paramInfoMap = {};
	paramInfoMap[record.pk_inventory.value] = record.ts.value;
	let params = [ { id: record.pk_inventory.value, index: index } ];
	ajax({
		url: '/nccloud/aum/inventory/commit.do',
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: OperatorType,
			pageid: pageConfig.pagecode.card_pagecode,
			commitType: 'commit'
		},
		success: (res) => {
			getScriptListReturn.call(
				this,
				params,
				res,
				props,
				'pk_inventory',
				pageConfig.areas.card.card_head,
				pageConfig.areas.list.tableId,
				false,
				dataSource
			);
		}
	});
}

/**
 * 表格行删除
 * @param {*} props 
 * @param {*} index 
 */
function delLine(props, index, text, record) {
	let paramInfoMap = {};
	paramInfoMap[record.pk_inventory.value] = record.ts.value;
	let params = [ { id: record.pk_inventory.value, index: index } ];
	ajax({
		url: '/nccloud/aum/inventory/delete.do',
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: pageConfig.pagecode.card_pagecode
		},
		success: (res) => {
			getScriptListReturn(
				params,
				res,
				props,
				'pk_inventory',
				pageConfig.areas.card.card_head,
				pageConfig.areas.list.tableId,
				true
			);
		}
	});
}
