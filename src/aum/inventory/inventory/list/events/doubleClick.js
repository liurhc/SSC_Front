import { pageConfig } from '../const';

/**
 * 双击表体动作
 * @param {*} record 
 * @param {*} index 
 * @param {*} props 
 */
export default function doubleClick(record, index, props) {
	this.props.pushTo('/card', {
		pagecode: pageConfig.pagecode.card_pagecode,
		status: pageConfig.ui_status.result,
		id: record.pk_inventory.value
	});
}
