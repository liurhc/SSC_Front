import { ajax, toast, print, output } from 'nc-lightapp-front';
import { pageConfig } from './../const.js';
import { searchBtnClick } from './index';
import ampub from 'ampub';
const { components, utils } = ampub;
const { ScriptReturnUtils } = components;
const { getScriptListReturn } = ScriptReturnUtils;
const { listUtils, msgUtils } = utils;
const { setBillFlowBtnsEnable, batchRefresh } = listUtils;
const { showConfirm, MsgConst, showMessage } = msgUtils;

const card_pagecode = pageConfig.pagecode.card_pagecode;
const tableId = pageConfig.areas.list.tableId;
const vo_name = pageConfig.vo_name;
const url = pageConfig.url;
const dataSource = pageConfig.dataSource;
/**
 * 按钮点击事件
 * 
 * @param {*} props 
 * @param {*} id 
 */
export default function buttonClick(props, id) {
	switch (id) {
		case 'Add' /* 新增 */:
			props.pushTo('/card', { pagecode: card_pagecode, status: pageConfig.ui_status.add });
			break;
		case 'Delete' /* 删除 */:
			delConfirm.call(this, props);
			break;
		case 'Commit' /* 提交 */:
			commitAction.call(this, 'SAVE', props);
			break;
		case 'UnCommit' /* 收回 */:
			commitAction.call(this, 'UNSAVE', props);
			break;
		case 'Print' /* 打印 */:
			printTemp.call(this, props);
			break;
		case 'Output' /* 输出 */:
			outputTemp.call(this, props);
			break;
		case 'Refresh' /* 刷新 */:
			refreshAction.call(this, props);
			break;
		case 'Attachment' /* 附件 */:
			attachmentAction.call(this);
			break;
		default:
			break;
	}
}

/**
 * 附件
 */
function attachmentAction() {
	/* 附件选取的是选中行的第一条，所以在选取的时候取的是checkedrows[0] */
	let checkedrows = this.props.table.getCheckedRows(tableId);
	let billNo = checkedrows[0].data.values['bill_code'].value; // 单据编码
	let billId = checkedrows[0].data.values[vo_name.pk_inventory].value; // 单据主键
	this.props.ncUploader.show('warcontract-uploader', { billId: 'pam/inventory/' + billId, billNo });
}

/**
 * 打印
 * @param {*} props 
 */
export function printTemp(props) {
	let printData = getPrintData.call(this, props);
	if (!printData) {
		showMessage.call(this, this.props, { type: MsgConst.Type.ChoosePrint });
		return;
	}
	/* 支持两种打印方式：'html'为模板打印，'pdf'为pdf打印 */
	print('pdf', pageConfig.url.printUrl, printData);
}

/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, this.props, { type: MsgConst.Type.ChooseOutput });
		return;
	}
	output({ url: pageConfig.url.printUrl, data: printData });
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let checkedRows = props.table.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length == 0) {
		return false;
	}
	let pks = [];
	checkedRows.map((item) => {
		pks.push(item.data.values[vo_name.pk_inventory].value);
	});
	pks = JSON.stringify(pks);
	let category = null;
	let printData = {
		filename: pageConfig.pagecode.list_pagecode, // 文件名称
		nodekey: pageConfig.printNodekey, // 模板节点标识
		oids: [ pks, category ], // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

/**
 * 表头提交按钮
 * @param {*} props 
 */
export function commitAction(OperatorType, props, content) {
	let data = props.table.getCheckedRows(tableId);
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values[vo_name.pk_inventory].value;
		let ts = v.data.values.ts.value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	let commitType = '';
	if (OperatorType === 'SAVE') {
		//提交传这个
		commitType = 'commit';
	}
	ajax({
		url: url.commitUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: OperatorType,
			pageid: card_pagecode,
			commitType: commitType, //提交
			content: content
		},
		success: (res) => {
			if (content) {
				this.setState({
					compositedisplay: false
				});
			}
			getScriptListReturn.call(
				this,
				params,
				res,
				props,
				vo_name.pk_inventory,
				pageConfig.areas.card.card_head,
				tableId,
				false,
				dataSource
			);
		}
	});
}

/**
 * 删除确认
 * @param {*} props 
 */
export function delConfirm(props) {
	let checkedRows = props.table.getCheckedRows(tableId);
	if (checkedRows.length == 0) {
		showMessage.call(this, this.props, { type: MsgConst.Type.ChooseDelete });
		return;
	}
	showConfirm.call(this, props, {
		type: MsgConst.Type.DelSelect,
		beSureBtnClick: () => {
			deleteAction.call(this, props, checkedRows);
		}
	});
}

/**
 * 删除动作执行
 * @param {*} props 
 * @param {*} data 
 */
function deleteAction(props, data) {
	let paramInfoMap = {};
	let params = data.map((v) => {
		let id = v.data.values[vo_name.pk_inventory].value;
		let ts = v.data.values.ts.value;
		let index = v.index;
		paramInfoMap[id] = ts;
		return {
			id,
			index
		};
	});
	ajax({
		url: url.deleteUrl,
		data: {
			paramInfoMap: paramInfoMap,
			dataType: 'listData',
			OperatorType: 'DELETE',
			pageid: card_pagecode
		},
		success: (res) => {
			getScriptListReturn.call(
				this,
				params,
				res,
				this.props,
				vo_name.pk_inventory,
				pageConfig.areas.card.card_head,
				tableId,
				true
			);
		}
	});
}

/**
 * 刷新
 * @param {*} props 
 */
function refreshAction(props) {
	batchRefresh.call(this, props, searchBtnClick);
}

/**
 * 批量设置按钮是否可用
 * @param {*} props 
 */
export function setBatchBtnsEnable(props) {
	setBillFlowBtnsEnable.call(this, props);
}
