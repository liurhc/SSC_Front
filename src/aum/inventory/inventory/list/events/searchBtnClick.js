import { ajax } from 'nc-lightapp-front';
import { pageConfig } from './../const.js';
import { setBatchBtnsEnable } from './index';
import ampub from 'ampub';
const { components, utils } = ampub;
const { LoginContext } = components;
const { getContext, loginContextKeys } = LoginContext;
const { listUtils } = utils;
const { setQueryInfoCache, setListValue } = listUtils;

const tableId = pageConfig.areas.list.tableId;

/**
 * 查询
 * @param {*} props 
 * @param {*} querycondition 
 * @param {*} type 
 * @param {*} queryInfo 
 * @param {*} isRefresh 
 */
export default function clickSearchBtn(props, querycondition, type, queryInfo, isRefresh) {
	if (!querycondition) {
		return;
	}
	if (!queryInfo) {
		queryInfo = props.search.getQueryInfo(pageConfig.areas.list.searchId);
	}
	let pageInfo = props.table.getTablePageInfo(tableId);
	queryInfo.pagecode = pageConfig.pagecode.list_pagecode;
	queryInfo.pageInfo = pageInfo;
	queryInfo.billtype = pageConfig.bill_type;
	queryInfo.transtype = getContext(loginContextKeys.transtype);
	// 缓存查询条件
	setQueryInfoCache.call(this, queryInfo, isRefresh, type);
	/* 使用场景，默认Default，可以传Query和Refresh */
	let scene = isRefresh ? pageConfig.setValueType.refresh : pageConfig.setValueType.query;
	ajax({
		url: pageConfig.url.searchQueryUrl,
		data: queryInfo,
		success: (res) => {
			setListValue.call(this, props, res, tableId, scene);
			setBatchBtnsEnable.call(this, props, tableId);
		}
	});
}
