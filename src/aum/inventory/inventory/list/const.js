import { appConfig } from './../const';

// 页面配置
const pageConfig = {
	...appConfig,
	// 节点名称
	title: '452005008A-000045' /* 国际化处理：盘点计划编制 */,
	// 浏览态查询区查询类型
	querytype: 'tree',
	setValueType: {
		refresh: 'refresh',
		query: 'query'
	},
	// 浏览态按钮
	browseHeadBtns: {
		Add: true,
		Commit: false
	},
	// 单据 自由态 行操作按钮
	free: [ 'Edit', 'Delete', 'Commit', 'CheckResult' ],
	// 单据 已提交 行操作按钮
	submission: [ 'UnCommit', 'CheckResult' ],
	// 单据 已审核 行操作按钮
	audited: [ 'CheckResult' ],
	// 列表态按钮
	listButtonVisible: {
		headBtns: {
			Save: true,
			SaveCommit: true,
			Cancel: true,
			Add: true,
			Edit: true,
			Delete: true,
			Commit: true,
			UnCommit: true,
			File: true,
			Print: true,
			Preview: true,
			OutPut: true
		},
		innerBtns: {
			// 自由态_0
			freeAudit: [ 'Commit', 'Delete', 'ReAssign' ],
			// 已提交(未审核)_1
			unAudit: [ 'UnCommit', 'QueryAboutBillFlow' ],
			// 审核中_2
			auditing: [ 'QueryAboutBillFlow' ],
			// 审核通过_3
			auditPass: [ 'UnCommit', 'QueryAboutBillFlow' ],
			// 审核未通过_4
			auditNoPass: [ 'QueryAboutBillFlow', 'Delete' ],
			// 关闭_6
			close: [ '' ]
		}
	},
	btnConfig: {
		btnUsability: {
			noData: {
				Add: false,
				Delete: true,
				Commit: true,
				UnCommit: true,
				Attachment: true,
				QueryAbout: true,
				QueryAboutBusiness: true,
				Print: true,
				OutPut: true,
				Refresh: false
			},
			hasData: {
				Add: false,
				Delete: false,
				Commit: false,
				UnCommit: false,
				Attachment: false,
				QueryAbout: false,
				QueryAboutBusiness: false,
				Print: false,
				OutPut: false,
				Refresh: false
			},
			multi: {
				Add: false,
				Delete: true,
				Commit: true,
				UnCommit: true,
				Attachment: false,
				QueryAbout: false,
				QueryAboutBusiness: true,
				Print: false,
				OutPut: false,
				Refresh: false
			},
			// 只有自由态
			onlyFree_check: {
				Add: false,
				Delete: false,
				Commit: false,
				UnCommit: true,
				Attachment: false,
				QueryAbout: false,
				QueryAboutBusiness: true,
				Print: false,
				OutPut: false,
				Refresh: false
			},
			// 只有未审核
			onlyUn_check: {
				Add: false,
				Delete: true,
				Commit: true,
				UnCommit: false,
				Attachment: false,
				QueryAbout: false,
				QueryAboutBusiness: false,
				Print: false,
				OutPut: false,
				Refresh: false
			},
			// 只有审核中
			onlyCheck_going: {
				Add: false,
				Delete: true,
				Commit: true,
				UnCommit: true,
				Attachment: false,
				QueryAbout: false,
				QueryAboutBusiness: false,
				Print: false,
				OutPut: false,
				Refresh: false
			},
			// 只有审核通过
			onlyCheck_pass: {
				Add: false,
				Delete: true,
				Commit: true,
				UnCommit: false,
				Attachment: false,
				QueryAbout: false,
				QueryAboutBusiness: false,
				Print: false,
				OutPut: false,
				Refresh: false
			}
		}
	}
};

export { pageConfig };
