import React, { Component } from 'react';
import echarts from 'echarts';
import { pageConfig } from './../const';
import { sortFun } from './events';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

export default class SurePage extends Component {
	constructor(props) {
		super(props);
		this.onClickLine = props.onClickLine.bind(this);
	}
	componentDidMount() {
		let { state } = this.props;
		let barData = state.barData;
		let tempData = Object.assign({}, barData);
		let nameArr = [];
		let uncheckedArr = new Array();
		let checkedArr = new Array();
		nameArr = sortFun(tempData, 'null');
		for (let index in nameArr) {
			uncheckedArr.push(barData[nameArr[index]].uncheck || 0);
			checkedArr.push(barData[nameArr[index]].checked || 0);
		}
		nameArr.map((item, index) => {
			if (item == 'null') {
				nameArr[index] = getMultiLangByID('452005008A-000006') /* 国际化处理：未分配人员 */;
			}
		});
		let formatterTemp = getMultiLangByID('452005008A-000007', {
			equipsNum: this.props.state.equipsNum
		}); /* 国际化处理：总共{equipsNum}个资产 */
		let option = {
			title: {
				text: getMultiLangByID('452005008A-000015') /* 国际化处理：资产总数 */,
				x: 'left',
				align: 'right',
				textStyle: {
					fontSize: 16,
					fontWeight: 'normal'
				}
			},
			series: [
				{
					type: 'pie',
					radius: [ '55%', '65%' ],
					avoidLabelOverlap: false, //是否允许提示文字重叠，false 提示语显示在中心
					label: {
						normal: {
							show: true,
							position: 'center',
							formatter: formatterTemp,
							color: '#ccc',
							fontSize: 18
						}
					},
					data: [ { value: 335 } ]
				}
			],
			color: [ '#20CBAF' ]
		};
		let optionLine = {
			title: {
				text: getMultiLangByID('452005008A-000016') /* 国际化处理：如下人员去盘点 */,
				x: 'left',
				align: 'right',
				textStyle: {
					fontSize: 16,
					fontWeight: 'normal'
				}
			},
			//悬浮提示框
			tooltip: {
				trigger: 'axis',
				axisPointer: {
					type: 'shadow'
				},
				formatter: function(params) {
					let xAxis = params[0]['name'];
					let nameArr = params.map(({ seriesName }) => {
						return seriesName;
					});
					let dataArr = params.map(({ data }) => {
						return data;
					});
					let totalNum = 0;
					for (let i = 0; i < dataArr.length; i++) {
						totalNum += dataArr[i] * 1;
					}
					let str = '';
					for (let i = 0; i < nameArr.length; i++) {
						str +=
							'<p>' +
							nameArr[i] +
							'：<span>' +
							dataArr[i] +
							getMultiLangByID('452005008A-000046') /*国际化处理: 个 */ +
							'</span></p>';
					}
					return `<div class="toolTip">
						<p>${xAxis} <span>${getMultiLangByID('452005008A-000014', {
						totalNum
					})}</span></p>${str}</div>`; /* 国际化处理：共分配${totalNum}个资产 */
				}
			},
			legend: {
				data: [
					getMultiLangByID('452005008A-000008') /* 国际化处理：已盘 */,
					getMultiLangByID('452005008A-000009') /* 国际化处理：未盘 */
				]
			},
			//数据区域放缩
			dataZoom: [
				{
					show: true,
					realtime: true,
					start: 0,
					end: 50
				}
			],
			xAxis: [
				{
					type: 'category',
					data: nameArr,
					axisTick: {
						//是否显示坐标刻度线
						show: false
					},
					axisLine: {
						//坐标轴是否显示
						show: true
					},
					minInterval: 1
				}
			],
			yAxis: [
				{
					type: 'value',
					splitLine: {
						//是否显示网格线
						show: false
					},
					axisTick: {
						//是否显示坐标刻度线
						show: false
					},
					axisLine: {
						//坐标轴是否显示
						show: true
					}
				}
			],
			series: [
				{
					name: getMultiLangByID('452005008A-000008') /* 国际化处理：已盘 */,
					type: 'bar',
					stack: getMultiLangByID('452005008A-000041') /* 国际化处理：总量 */,
					barWidth: 15,
					data: checkedArr
				},
				{
					name: getMultiLangByID('452005008A-000009') /* 国际化处理：未盘 */,
					type: 'bar',
					stack: getMultiLangByID('452005008A-000041') /* 国际化处理：总量 */,
					barWidth: 15,
					data: uncheckedArr
				}
			],

			color: [ '#FF9A48', '#20CBAF' ]
		};
		const myChart = echarts.init(document.getElementById('echartPieBox'));
		const myChartLine = echarts.init(document.getElementById('echartsLineBox'));
		myChart.setOption(option);
		myChartLine.setOption(optionLine);
		//当浏览器窗口发生变化时候，echarts大小随着变化
		window.addEventListener('resize', () => {
			myChart.resize();
			myChartLine.resize();
		});
		myChartLine.on('click', (context) => {
			if (context.name == getMultiLangByID('452005008A-000006') /* 国际化处理：未分配人员 */) {
				this.onClickLine(context);
			}
		});
	}

	getRangeData = () => {};

	render() {
		let pk_org,
			pk_type,
			rangeData = null;
		this.props.state.ui_status == pageConfig.ui_status.add && (pk_org = this.props.echartsData.pk_org);
		this.props.state.ui_status == pageConfig.ui_status.add &&
			(pk_type = this.props.search.getSearchValByField(pageConfig.areas.card.rangeArea, 'pk_category'));
		let headInfo = this.props.state.billCard.head[pageConfig.areas.card.card_head].rows[0].values;
		this.props.state.ui_status == pageConfig.ui_status.add &&
			(rangeData = this.props.search.getAllSearchData(pageConfig.areas.card.rangeArea));
		return (
			<div className="echarts-box">
				<div id="echartPieBox" />
				<div class="assetInfo">
					{this.props.state.ui_status == pageConfig.ui_status.browse ||
						(this.props.state.ui_status == pageConfig.ui_status.edit && (
							<p>
								{getMultiLangByID('452005008A-000010') /* 国际化处理：单据号 */}：
								<span>
									{
										this.props.state.billCard.head[pageConfig.areas.card.card_head].rows[0].values
											.bill_code.value
									}
								</span>
							</p>
						))}
					<p>
						{getMultiLangByID('452005008A-000012') /* 国际化处理：盘点时间 */}：<span>{headInfo.start_date.value.slice(0, 10)}</span>---<span>{headInfo.end_date.value ? headInfo.end_date.value.slice(0, 10) : null}</span>
					</p>
					<p>{getMultiLangByID('452005008A-000013') /* 国际化处理：盘点范围 */}：</p>
					{this.props.state.ui_status == pageConfig.ui_status.add ? (
						<p className="scope-p">
							{rangeData.conditions.map((conditionItem) => {
								let rangeDetail = this.props.meta.getMeta().searchArea.items.map((nameItem) => {
									if (nameItem.attrcode == conditionItem.field) {
										return (
											<div>
												<span>{nameItem.label}</span>
												<span>{conditionItem.oprtype}</span>
												<span title={conditionItem.display}>{conditionItem.display}</span>
											</div>
										);
									}
								});
								return rangeDetail;
							})}
						</p>
					) : (
						<p className="scope-p">
							<div>
								<span title={headInfo.check_range.value}>{headInfo.check_range.value}</span>
							</div>
						</p>
					)}
					<p />
				</div>
				<div className="divisionLine" />
				<div id="echartsLineBox" />
			</div>
		);
	}
}
