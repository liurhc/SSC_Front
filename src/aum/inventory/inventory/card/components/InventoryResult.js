import React, { Component } from 'react';
import echarts from 'echarts';
import { pageConfig } from './../const';
import { sortFun } from './events';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

export default class InventoryResult extends Component {
	constructor(props) {
		super(props);
		this.state = {
			equipsNum: 0,
			barData: {},
			legendData: [],
			legendDetail: {},
			billCard: {}
		};
		this.onResultClickLine = props.onResultClickLine.bind(this);
		this.onMyChart = props.onMyChart.bind(this);
		this.onLegendClick = this.onLegendClick.bind(this);
		this.option = {};
		this.optionLine = {};
		this.myChart = '';
		this.myChartLine = '';
		this.flag = true;
	}
	componentWillReceiveProps(nextProps) {
		if (this.flag) {
			this.setState(
				{
					equipsNum: nextProps.state.equipsNum,
					legendData: nextProps.state.legendData,
					barData: nextProps.state.barData
				},
				() => {
					this.option.series[0].label.normal.formatter = getMultiLangByID('452005008A-000007', {
						equipsNum: this.state.equipsNum
					}) /* 国际化处理：总共{equipsNum}个资产 */;
					this.option.series[0].data = this.getLegendArr();
					this.optionLine.xAxis.data = this.getXAxisArr();
					this.optionLine.series[0].data = this.getCheckedArr();
					this.optionLine.series[1].data = this.getUnCheckedArr();
					this.optionLine.xAxis.data.map((item, index) => {
						if (item == 'noAssign') {
							this.optionLine.xAxis.data[index] = getMultiLangByID('452005008A-000006') /* 国际化处理：未分配人员 */;
						}
					});
					this.myChart.setOption(this.option);
					this.myChartLine.setOption(this.optionLine);
				}
			);
		}
	}
	componentDidUpdate() {}
	componentDidMount() {
		let formatterTemp = null;
		let legendInfo = [];
		let barXAxisData = [];
		let barCheckedData = [];
		let barUnCheckedData = [];
		this.option = {
			series: [
				{
					type: 'pie',
					// 控制圆环内部大小和向内向外拓展
					radius: [ '45%', '55%' ],
					avoidLabelOverlap: false,
					label: {
						normal: {
							show: true,
							formatter: formatterTemp,
							color: '#ccc',
							position: 'center',
							fontSize: 18
						}
					},
					data: legendInfo
				}
			],
			color: [ '#20CBAF', '#2ec25b', '#facc14', '#ef4864', '#3000ff' ]
		};

		this.optionLine = {
			//悬浮提示框
			tooltip: {
				trigger: 'axis',
				axisPointer: {
					// 坐标轴指示器，坐标轴触发有效
					type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
				},
				formatter: function(params) {
					let xAxis = params[0]['name'];
					let nameArr = params.map(({ seriesName }) => {
						return seriesName;
					});
					let dataArr = params.map(({ data }) => {
						return data;
					});
					let totalNum = 0;
					for (let i = 0; i < dataArr.length; i++) {
						totalNum += dataArr[i] * 1;
					}
					let str = '';
					for (let i = 0; i < nameArr.length; i++) {
						str +=
							'<p>' +
							nameArr[i] +
							'：<span>' +
							dataArr[i] +
							getMultiLangByID('452005008A-000046') /* 个 */ +
							'</span></p>';
					}
					return `<div class="toolTip">
					<p>${xAxis} <span>${getMultiLangByID('452005008A-000014', {
						totalNum
					})} </span></p>${str}</div>`; /* 国际化处理：共分配${totalNum}个资产 */
				}
			},
			//数据区域放缩
			dataZoom: [
				{
					show: true,
					realtime: true,
					start: 0,
					end: 50
				}
			],
			//图例
			legend: {
				data: [
					getMultiLangByID('452005008A-000008') /* 国际化处理：已盘 */,
					getMultiLangByID('452005008A-000009') /* 国际化处理：未盘 */
				]
			},
			xAxis: {
				type: 'category',
				data: barXAxisData,
				axisTick: {
					//是否显示坐标刻度线
					show: false
				},
				axisLine: {
					//坐标轴是否显示
					show: false
				}
			},
			yAxis: {
				type: 'value',
				minInterval: 1
			},
			series: [
				{
					name: getMultiLangByID('452005008A-000008') /* 国际化处理：已盘 */,
					type: 'bar',
					stack: getMultiLangByID('452005008A-000041') /* 国际化处理：总量 */,
					barWidth: 15,
					data: barCheckedData
				},
				{
					name: getMultiLangByID('452005008A-000009') /* 个国际化处理：未盘 */,
					type: 'bar',
					stack: getMultiLangByID('452005008A-000041') /* 国际化处理：总量 */,
					barWidth: 15,
					data: barUnCheckedData
				}
			],
			color: [ '#FF9A48', '#20CBAF' ]
		};
		this.myChart = echarts.init(document.getElementById('echartPieBox'));
		this.myChartLine = echarts.init(document.getElementById('echartsLineBox'));
		this.myChart.setOption(this.option);
		this.myChartLine.setOption(this.optionLine);
		this.myChart.on('legendselectchanged', this.onMyChart);
		//当浏览器窗口发生变化时候，echarts大小随着变化
		window.addEventListener('resize', () => {
			this.myChart.resize();
			this.myChartLine.resize();
		});
		// 点击事件 -- 点击柱状图的未分配人员
		this.myChartLine.on('click', (context) => {
			if (context.name == getMultiLangByID('452005008A-000006') /* 国际化处理：未分配人员 */) {
				this.onResultClickLine(context);
			}
		});
	}

	getXAxisArr = () => {
		let tempData = Object.assign({}, this.state.barData);
		let XAxisArr = sortFun(tempData, 'noAssign');
		return XAxisArr;
	};
	getCheckedArr = () => {
		let checkedArr = [];
		let xAxisArr = this.optionLine.xAxis.data;
		for (let index in xAxisArr) {
			checkedArr.push(this.state.barData[xAxisArr[index]]['true'] || 0);
		}
		return checkedArr;
	};
	getUnCheckedArr = () => {
		let unCheckedArr = [];
		let xAxisArr = this.optionLine.xAxis.data;
		for (let index in xAxisArr) {
			unCheckedArr.push(this.state.barData[xAxisArr[index]]['false'] || 0);
		}
		return unCheckedArr;
	};

	getLegendArr = () => {
		let legendNum = [ 0, 0, 0, 0, 0, 0, 0 ];
		let legendArr = [
			{ value: 0, name: 'unsettled' },
			{ value: 0, name: 'match' },
			{ value: 0, name: 'unmatch' },
			{ value: 0, name: 'profit' },
			{ value: 0, name: 'loss' }
		];
		Object.keys(this.state.legendData).map((item, index) => {
			if (item == 'unsettled') {
				legendArr[0].value = this.state.legendData[item];
				legendNum[0] = this.state.legendData[item];
				legendNum[5] = legendNum[5] * 1 + legendNum[0] * 1;
			} else if (item == 'match') {
				legendArr[1].value = this.state.legendData[item];
				legendNum[1] = this.state.legendData[item];
				legendNum[5] = legendNum[5] * 1 + legendNum[1] * 1;
			} else if (item == 'unmatch') {
				legendArr[2].value = this.state.legendData[item];
				legendNum[2] = this.state.legendData[item];
				legendNum[5] = legendNum[5] * 1 + legendNum[2] * 1;
				legendNum[6] = legendNum[6] * 1 + legendNum[2] * 1;
			} else if (item == 'profit') {
				legendArr[3].value = this.state.legendData[item];
				legendNum[3] = this.state.legendData[item];
				legendNum[5] = legendNum[5] * 1 + legendNum[3] * 1;
				legendNum[6] = legendNum[6] * 1 + legendNum[3] * 1;
			} else if (item == 'loss') {
				legendArr[4].value = this.state.legendData[item];
				legendNum[4] = this.state.legendData[item];
				legendNum[5] = legendNum[5] * 1 + legendNum[4] * 1;
				legendNum[6] = legendNum[6] * 1 + legendNum[4] * 1;
			}
		});
		let legendChange = false;
		legendArr.forEach((e) => {
			if (e.value != 0) {
				legendChange = true;
			}
		});
		if (legendChange) {
			// 当数据变化时再进行渲染，避免出现多次渲染结果
			this.setState({ legendDetail: { legendArr, legendNum } });
			return legendArr;
		}
		return [];
	};
	getThroughName = () => {
		return [
			getMultiLangByID('452005008A-000009') /* 国际化处理：未盘 */,
			getMultiLangByID('452005008A-000002') /* 国际化处理：相符 */,
			getMultiLangByID('452005008A-000003') /* 国际化处理：不符 */,
			getMultiLangByID('452005008A-000000') /* 国际化处理：盘盈 */,
			getMultiLangByID('452005008A-000001') /* 国际化处理：盘亏 */,
			getMultiLangByID('452005008A-000005') /* 国际化处理：全部 */,
			getMultiLangByID('452005008A-000003') +
				'+' +
				getMultiLangByID('452005008A-000000') +
				'+' +
				getMultiLangByID('452005008A-000001') /* 国际化处理：不符+盘盈+盘亏 */
		];
	};
	getThroughNum = () => {
		return this.state.legendDetail.legendNum;
	};

	onLegendClick = (index) => {
		return index;
	};

	getBillStatus = (statusNum) => {
		if (statusNum == 0) {
			return getMultiLangByID('statusUtils-000000') /* 国际化处理：自由态 */;
		} else if (statusNum == 1) {
			return getMultiLangByID('statusUtils-000001') /* 国际化处理：已提交*/;
		} else if (statusNum == 2) {
			return getMultiLangByID('statusUtils-000002') /* 国际化处理：审批中 */;
		} else if (statusNum == 3) {
			return getMultiLangByID('statusUtils-000003') /* 国际化处理：审批通过 */;
		} else if (statusNum == 4) {
			return getMultiLangByID('statusUtils-000004') /* 国际化处理：审批不通过 */;
		}
	};

	render() {
		let { state, onLegendClick } = this.props;
		let legendNum = [];
		let legendName = this.getThroughName();
		this.state.legendDetail.legendNum && (legendNum = this.state.legendDetail.legendNum);
		let bill_code = null;
		let bill_status = null;
		let start_date = null;
		let end_date = null;
		let check_range = null;
		if (state.extBillCard.head) {
			let data = state.extBillCard.head[pageConfig.areas.card.card_head].rows[0].values;
			bill_code = data.bill_code.value;
			bill_status = this.getBillStatus(data.bill_status.value);
			start_date = data.start_date.value.slice(0, 10);
			end_date = data.end_date.value;
			end_date ? (end_date = end_date.slice(0, 10)) : (end_date = '');
			check_range = data.check_range.value;
		}
		let str = getMultiLangByID('452005008A-000047', { begin_date: start_date, end_date });
		let fromStr = [];
		let toStr = [];
		str && (fromStr = str.split(start_date));
		fromStr.length > 0 && (toStr = fromStr[1].split(end_date));
		return (
			<div className="echarts-area  page-break">
				<div className="msg-area">
					<div>
						<p>
							{getMultiLangByID('452005008A-000010') /* 国际化处理：单据号 */}：<span>{bill_code}</span>
						</p>
						<p>
							{getMultiLangByID('452005008A-000011') /* 国际化处理：单据状态 */}：<span>{bill_status}</span>
						</p>
					</div>
				</div>
				<div className="pie-box">
					<div id="echartPieBox" />
					<div className="legend-area">
						{legendName.map((item, index) => {
							return (
								<p
									className={'legend-' + (index * 1 + 1)}
									onClick={() => this.props.onLegendClick(index)}
								>
									{item}
									<span>{legendNum[index]}</span>
								</p>
							);
						})}
					</div>
					<div class="assetInfo">
						<p>
							{fromStr[0]}
							<span>{start_date} </span>
							{toStr[0]}
							<span>{end_date}</span>
						</p>
						<p>{getMultiLangByID('452005008A-000013') /* 国际化处理：盘点范围 */}：</p>
						{
							<div>
								<span className="inventory-scope-text" title={check_range}>
									{check_range}
								</span>
							</div>
						}
						<p />
					</div>
				</div>
				<div id="echartsLineBox" />
			</div>
		);
	}
}
