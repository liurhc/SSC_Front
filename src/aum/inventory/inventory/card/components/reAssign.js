import React, { Component } from 'react';
import { pageConfig } from './../const.js';
export default class extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		let { editTable } = this.props;
		let { createEditTable } = editTable;
		return (
			// 未分配盘点人列表
			<div className="table-area table-info">
				{createEditTable(pageConfig.areas.card.reAssignArea, {
					showIndex: true
				})}
			</div>
		);
	}
}
