import React, { Component } from 'react';
import { pageConfig } from './../const.js';
import { throughAreaAfterEvent, throughAreaBeforeEvent } from './events';

export default class extends Component {
	constructor(props) {
		super(props);
	}

	componentDidMount() {}

	render() {
		let { cardTable } = this.props;
		let { createCardTable } = cardTable;
		return (
			// 盘点明细
			<div className="table-area">
				{createCardTable(pageConfig.areas.card.throughArea, {
					showIndex: true,
					// 只能设置鼠标离开最后一行不默认增行
					isAddRow: false,
					onAfterEvent: throughAreaAfterEvent.bind(this),
					onBeforeEvent: throughAreaBeforeEvent.bind(this),
					hideModelSave: true,
					hideAdd: true,
					hideDel: true
				})}
			</div>
		);
	}
}
