import React, { Component } from 'react';
import { pageConfig } from './../const';
import { rangeAreaAfterEvent } from './events';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

export default class StepOne extends Component {
	constructor(props) {
		super(props);
		this.childform1ID = 'childform1';
		this.childform2ID = 'childform2';
		this.state = {
			start_date: null, //盘点开始时间
			end_date: null, //盘点结束时间
			compareCheckedVal: {}
		};
	}
	componentDidMount() {
		let { form, formID1, formID2 } = this.props;
		form.setFormStatus(formID1, 'edit');
		form.setFormStatus(formID2, 'edit');
	}
	reSetRangeData = (rangeAreaData) => {
		if (rangeAreaData) {
			this.props.search.setSearchValue(pageConfig.areas.card.rangeArea, rangeAreaData.conditions);
		}
	};

	render() {
		let { form, search, rangeAreaData } = this.props;
		let { createForm } = form;
		let { NCCreateSearch } = search;
		return (
			<div className="container-area">
				<div>
					<p>{getMultiLangByID('452005008A-000013') /* 国际化处理：盘点范围 */}</p>
					<div>
						{NCCreateSearch(pageConfig.areas.card.rangeArea, {
							showSearchBtn: false,
							renderCompleteEvent: this.reSetRangeData.bind(this, rangeAreaData),
							onAfterEvent: rangeAreaAfterEvent.bind(this)
						})}
					</div>
				</div>
				<div>
					<p>{getMultiLangByID('452005008A-000012') /* 国际化处理：盘点时间 */}</p>
					<div>{createForm('dateArea', {})}</div>
				</div>
			</div>
		);
	}
}
