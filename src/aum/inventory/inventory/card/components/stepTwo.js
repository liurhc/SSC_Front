import React, { Component } from 'react';
import { pageConfig } from './../const.js';
import { assignAreaAfterEvent } from './events';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

export default class stepTwo extends Component {
	constructor(props) {
		super(props);
	}

	componentDidMount = () => {
		// 重新分配时将数据重新赋给组件
		if (this.props.state.assignData != null) {
			this.props.editTable.setTableData(pageConfig.areas.card.assignArea, this.props.state.assignData);
		}
	};

	addlineClick = (props, id) => {
		switch (id) {
			case 'AddAssLine':
				props.editTable.addRow(
					pageConfig.areas.card.assignArea,
					props.editTable.getNumberOfRows(pageConfig.areas.card.assignArea),
					true
				);
				break;
			case 'DelAssLine':
				let checkedRows = this.props.editTable.getCheckedRows(pageConfig.areas.card.assignArea);
				let deleteIndexs = [];
				checkedRows.map((item) => {
					deleteIndexs.push(item.index);
				});
				props.editTable.deleteTableRowsByIndex(pageConfig.areas.card.assignArea, deleteIndexs);
				break;
			default:
				break;
		}
	};

	rowSelected = (props, moduleId, record, index, status) => {
		let checkedRows = this.props.editTable.getCheckedRows(moduleId);
		checkedRows && checkedRows.length != 0
			? this.props.button.setButtonDisabled(pageConfig.buttonDisabled.enable)
			: this.props.button.setButtonDisabled(pageConfig.buttonDisabled.unable);
	};

	render() {
		let { handleChange } = this.props;
		let { checkedValue } = this.props;
		let { createEditTable } = this.props.editTable;
		let { button } = this.props;
		let { createButtonApp } = button;
		let { addlineClick } = this;
		const radioArr = [
			getMultiLangByID('452005008A-000017') /* 国际化处理：责任人盘点 */,
			getMultiLangByID('452005008A-000018') /* 国际化处理：管理人盘点 */,
			getMultiLangByID('452005008A-000019') /* 国际化处理：使用部门盘点 */,
			getMultiLangByID('452005008A-000020') /* 国际化处理：管理部门盘点 */,
			getMultiLangByID('452005008A-000021') /* 国际化处理：资产类别盘点 */,
			getMultiLangByID('452005008A-000022') /* 国际化处理：按类别 + 管理部门盘点 */,
			getMultiLangByID('452005008A-000023') /* 国际化处理：按类别 + 使用部门盘点 */
		];
		return (
			<div className="allocation-person">
				<div className="radioLists">
					<p>
						<span>
							{getMultiLangByID('452005008A-000024', {
								equipsNum: this.props.state.equipsNum
							}) /* 国际化处理：共有{equipsNum}个资产设备，请选择分配类型 */}
						</span>
					</p>
					{radioArr.map((item, index) => {
						return (
							<div
								className={checkedValue - 1 == index ? 'active' : ''}
								onClick={() => handleChange(index + 1)}
							>
								{item}
							</div>
						);
					})}
				</div>
				<div className="takeStockInfo">
					{checkedValue == '1' || checkedValue == '2' ? (
						<div className="alert-info">
							<div className="alert-img-area" />
							<p>{getMultiLangByID('452005008A-000025') /* 国际化处理：使用人、管理人盘点：每台设备的使用、管理人盘点自己的设备 */}</p>
						</div>
					) : (
						<div>
							<div className="editTable-btn-area ">
								{createButtonApp({
									//按钮区域（在数据库中注册的按钮区域）
									area: 'card_assign_table',
									//按钮数量限制，超出指定数量的按钮将放入更多显示
									onButtonClick: addlineClick,
									//指定下拉弹窗渲染的位置，不设置的话，下拉弹窗随滚动条滚动
									popContainer: document.querySelector('.header-button-area')
								})}
							</div>
							<div>
								{createEditTable(
									pageConfig.areas.card.assignArea, //表格id
									{
										showIndex: true,
										showCheck: true,
										isAddRow: false,
										onSelected: this.rowSelected.bind(this),
										onSelectedAll: this.rowSelected.bind(this),
										onAfterEvent: assignAreaAfterEvent.bind(this)
									}
								)}
							</div>
						</div>
					)}
				</div>
			</div>
		);
	}
}
