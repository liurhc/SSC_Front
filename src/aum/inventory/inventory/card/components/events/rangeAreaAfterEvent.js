import { pageConfig } from '../../const';

export default function(key, value) {
	if (key == pageConfig.vo_name.pk_org) {
		if (value.refpk) {
			this.props.search.setDisabled(pageConfig.areas.card.rangeArea, false);
		} else {
			this.props.search.setDisabled(pageConfig.areas.card.rangeArea, true);
			this.props.search.setDisabledByField(pageConfig.areas.card.rangeArea, pageConfig.vo_name.pk_org, false);
		}
	}
}
