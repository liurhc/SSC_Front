import { ajax, toast } from 'nc-lightapp-front';
import { pageConfig } from './../../const';

import ampub from 'ampub';
const { components, commonConst } = ampub;
const { LoginContext, refInit } = components;
const { getContext, loginContextKeys } = LoginContext;
const { defRefCondition } = refInit;
const { CommonKeys } = commonConst;
const { IBusiRoleConst } = CommonKeys;

export default function(props, moduleId, key, value, index, record) {
	let values = this.props.state.extBillCard.head[pageConfig.areas.card.card_head].rows[0].values;
	let pk_org = values.pk_org.value;
	let meta = props.meta.getMeta();
	meta[pageConfig.areas.card.throughArea].items.map((item) => {
		throughAreaAlter.call(this, item, index, record, key);
	});
	meta[pageConfig.areas.card.bodyvos_edit].items.map((item) => {
		throughAreaAlter.call(this, item, index, record, key);
	});
	props.meta.setMeta(meta);
	if (key == pageConfig.vo_name.equip_name) {
		let check_result = this.props.cardTable.getValByKeyAndIndex(moduleId, index, pageConfig.vo_name.check_result);
		if (check_result.value == pageConfig.check_result.PROFIT) {
			return true;
		} else {
			return false;
		}
	}
	return true;
}

/**
 * 过滤资产状态
 * 
 * @param {*} item 
 */
function filterUsedStatus(item) {
	item.queryCondition = () => {
		return {
			transi_type: getContext(loginContextKeys.transtype),
			bill_type: pageConfig.bill_type,
			GridRefActionExt: 'nccloud.web.aum.inventory.refcondition.EQUIP_STATUSSqlBuilder'
		};
	};
}

function throughAreaAlter(item, index, record, key) {
	let values = this.props.state.extBillCard.head[pageConfig.areas.card.card_head].rows[0].values;
	let pk_org = values.pk_org.value;
	let isPriority = record.values.check_result.value == '1' ? true : false;
	if (
		(item.attrcode == 'pk_mandept_after' ||
			item.attrcode == 'pk_mandept_after_v' ||
			item.attrcode == 'pk_manager_after') &&
		(key == 'pk_mandept_after' || key == 'pk_mandept_after_v' || key == 'pk_manager_after')
	) {
		if (isPriority) {
			item.queryCondition = () => {
				return { pk_org, busifuncode: IBusiRoleConst.ASSETORG };
			};
		} else {
			item.queryCondition = () => {
				//同步请求
				ajax({
					loading: true,
					url: '/nccloud/aum/inventory/queryORGAction.do',
					data: {
						key: 'pk_ownerorg',
						pk_equip: record.values.pk_equip.value
					},
					async: false,
					success: (res) => {
						let { data, success } = res;
						if (success && data) {
							pk_org = data;
						}
					}
				});
				return { pk_org, busifuncode: IBusiRoleConst.ASSETORG };
			};
		}
	} else if (
		(item.attrcode == 'pk_usedunit_after' || item.attrcode == 'pk_usedunit_after_v') &&
		(key == 'pk_usedunit_after' || key == 'pk_usedunit_after_v')
	) {
		if (isPriority) {
			item.queryCondition = () => {
				return {
					pk_org,
					busifuncode: IBusiRoleConst.ASSETORG,
					TreeRefActionExt: 'nccloud.web.aum.inventory.refcondition.PK_USEDUNITSqlBuilder'
				};
			};
		} else {
			item.queryCondition = () => {
				//同步请求
				ajax({
					loading: true,
					url: '/nccloud/aum/inventory/queryORGAction.do',
					data: {
						key: 'pk_usedorg',
						pk_equip: record.values.pk_equip.value
					},
					async: false,
					success: (res) => {
						let { data, success } = res;
						if (success && data) {
							pk_org = data;
						}
					}
				});
				return {
					pk_org,
					busifuncode: IBusiRoleConst.ASSETORG,
					TreeRefActionExt: 'nccloud.web.aum.inventory.refcondition.PK_USEDUNITSqlBuilder'
				};
			};
		}
	} else if (
		item.attrcode == 'pk_usedept_after' ||
		item.attrcode == 'pk_usedept_after_v' ||
		item.attrcode == 'pk_assetuser_after'
	) {
		if (isPriority) {
			item.queryCondition = () => {
				return { pk_org, busifuncode: IBusiRoleConst.ASSETORG };
			};
		} else {
			item.queryCondition = () => {
				pk_org = this.props.cardTable.getValByKeyAndIndex(
					pageConfig.areas.card.throughArea,
					index,
					'pk_usedunit_after'
				).value;
				return { pk_org, busifuncode: IBusiRoleConst.ASSETORG, isShowUnit: true };
			};
		}
	} else if (item.attrcode == 'pk_usingstatus_after') {
		filterUsedStatus.call(this, item);
	} else if (item.attrcode == 'planedcheckusername') {
		return { pk_org, busifuncode: IBusiRoleConst.ASSETORG, isShowUnit: true };
	} else if (item.attrcode == 'pk_category' && key == 'pk_category') {
		item.onlyLeafCanSelect = true;
	} else {
		defRefCondition.call(
			this,
			this.props,
			item,
			pageConfig.areas.card.throughArea,
			getContext(loginContextKeys.groupId)
		);
	}
}
