import { pageConfig } from './../../const';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

/**
 * 排序
 *  对对象进行排序
 *  对象结构：
 *      obj:{
 *          a:num1;
 *          b:num2;
 *      }
 *  主要场景：
 *      柱状图界面，对人员按照需要盘点总数进行排序
 * 
 * @param {*} obj 
 * @param {*} str 
 */
export function sortFun(obj = {}, str = getMultiLangByID('452005008A-000006') /* 国际化处理：未分配人员 */) {
	let newObj = []; //排好序的新的对象
	let keys = Object.keys(obj); //获取obj对象的属性集合，是个数组
	if (keys.join('').indexOf(str) > -1) {
		//排除掉‘未分配人员’
		newObj.push(str);
		delete obj[str];
	}
	let values = [];
	for (let key in obj) {
		let total = 0;
		for (let val in obj[key]) {
			total += obj[key][val] * 1;
		}
		obj[key]['total'] = total;
		values.push(total);
	}
	values.sort((a, b) => {
		return b - a;
	});
	for (let i = 0; i < values.length; i++) {
		//按照属性值对对象进行排序
		for (let key in obj) {
			if (values[i] == obj[key]['total']) {
				newObj.push(key);
				delete obj[key];
				continue;
			}
		}
	}
	return newObj;
}

/**
 * 根据单据状态获取页面状态
 * 
 * @param {*} bill_status 
 */
export function getPageStatusByBillStatus(bill_status = pageConfig.bill_status.free_check) {
	if (bill_status == pageConfig.bill_status.free_check) {
		return pageConfig.pageStatus.checkResult.free_check;
	} else if (bill_status == pageConfig.bill_status.un_check) {
		return pageConfig.pageStatus.checkResult.un_check;
	} else if (bill_status == pageConfig.bill_status.check_going) {
		return pageConfig.pageStatus.checkResult.check_going;
	} else if (bill_status == pageConfig.bill_status.check_pass) {
		return pageConfig.pageStatus.checkResult.check_pass;
	} else if (bill_status == pageConfig.bill_status.check_nopass) {
		return pageConfig.pageStatus.checkResult.free_check;
	} else {
		return pageConfig.pageStatus.checkResult.free_check;
	}
}

export function getPksByValues(values, key) {
	return values.map((item) => {
		return item.values[key].value;
	});
}
