import { pageConfig } from './../../const.js';

/**
 * 表格操作按钮
 * @param {*} props 
 * @param {*} key 
 * @param {*} text 
 * @param {*} record 
 * @param {*} index 
 */
export default function(props, key, text, record, index) {
	switch (key) {
		case 'DelAssLine':
			delAssLineAction.call(this, props, index);
		default:
			break;
	}
}

/**
 * 分配盘点人列表行操作：删行
 * @param {*} props 
 * @param {*} index 
 */
function delAssLineAction(props, index) {
	/* 
	删除分配盘点人列表行
	最后一个参数是真删除，默认是假删除
	*/
	props.editTable.deleteTableRowsByIndex(pageConfig.areas.card.assignArea, index, true);
}
