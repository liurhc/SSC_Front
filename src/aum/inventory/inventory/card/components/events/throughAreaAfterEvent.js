import { pageConfig, invItems } from './../../const.js';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

const throughArea = pageConfig.areas.card.throughArea;
const vo_name = pageConfig.vo_name;
export default function(props, id, key, value, changedrows, index, data, type, eventType) {
	// 如果编辑的是使用权，并且使用权字段为空，则清空使用人和使用部门————bug号：NCCLOUD-85410
	if ((key == 'pk_usedunit_after' || key == 'pk_usedunit_after_v') && (value.values == null || value.values == '')) {
		this.props.cardTable.setValByKeyAndIndex(throughArea, index, 'pk_assetuser_after', {
			value: null,
			display: null
		});
		this.props.cardTable.setValByKeyAndIndex(throughArea, index, 'pk_usedept_after', {
			value: null,
			display: null
		});
		this.props.cardTable.setValByKeyAndIndex(throughArea, index, 'pk_usedept_after_v', {
			value: null,
			display: null
		});
		this.props.cardTable.setValByKeyAndIndex(throughArea, index, 'pk_usedunit_after', {
			value: null,
			display: null
		});
		this.props.cardTable.setValByKeyAndIndex(throughArea, index, 'pk_usedunit_after_v', {
			value: null,
			display: null
		});
	}
	let issettled = this.props.cardTable.getValByKeyAndIndex(throughArea, index, vo_name.issettled).value;
	let equipcode = this.props.cardTable.getValByKeyAndIndex(throughArea, index, vo_name.equip_code).value;
	if (checkProfit.call(this, equipcode, index)) {
		// 如果编辑了条形码信息，需要校验重复性		但是nccloud条形码不可见，是否校验需要和需求确定
		if (key == vo_name.bar_code) {
			// 暂时按照我自己定义的后台返回的数据进行提示
			checkBarCodeNotRepeat.call(this);
		}
	} else {
		if (key != vo_name.ischecked && !value.values) {
			checkInvResult.call(this, id, key, value, changedrows, index, data, type, eventType);
			return;
		}
		// 当前编辑的字段不是issettled
		// 首先将处理状态设置为 已处理  （理由是其他任何字段编辑，都会被认为是状态已经处理，这个时候只需要去考虑盘点结果如何）
		this.props.cardTable.setValByKeyAndIndex(throughArea, index, vo_name.issettled, {
			value: true
		});
		if (key === vo_name.ischecked) {
			// TODO  核对盘点结果
			checkInvResult.call(this, id, key, value, changedrows, index, data, type, eventType);
		} else if (key.slice(-'_v'.length) == '_v') {
			this.props.cardTable.setValByKeyAndIndex(throughArea, index, vo_name.ischecked, {
				value: true
			});
			// 带_v的字段编辑后会走元数据编辑关联项，元数据编辑关联项事件是在编辑后事件之前走
			checkInvResult.call(this, id, key, value, changedrows, index, data, type, eventType);
		} else if (key == vo_name.planedcheckusername) {
			// 修改盘点人不做任何处理  不影响盘点结果
		} else if (
			key === vo_name.pk_usedunit_after ||
			key === vo_name.pk_usedept_after ||
			key === vo_name.pk_mandept_after ||
			key === vo_name.pk_assetuser_after ||
			key === vo_name.pk_usingstatus_after ||
			key === vo_name.pk_position_after
		) {
			this.props.cardTable.setValByKeyAndIndex(throughArea, index, vo_name.ischecked, {
				value: true
			});
			checkInvResult.call(this, id, key, value, changedrows, index, data, type, eventType);
		}
	}
}

// 检查条形码是否重复，如果重复，会有提示
function checkBarCodeNotRepeat() {
	let pk_inventory = this.props.getUrlParam('id');
	let bar_code = this.props.cardTable.getValByKeyAndIndex(throughArea, index, vo_name.bar_code).value;
	ajax({
		url: '/nccloud/aum/inventory/checkBarCode.do',
		data: {
			pk_inventory,
			bar_code
		},
		success: (res) => {
			toast({ content: res.data.msg, color: 'warning' });
		}
	});
}

/**
 * 判断是否为盘盈资产
 * 如果是盘盈资产，则编辑后  
 * 		是否已处理：是		
 * 		盘点结果：盘盈		
 * 		是否盘到：是
 * 
 * @param {*} equipcode 
 * @param {*} index 
 */
function checkProfit(equipcode, index) {
	if (equipcode == '' || equipcode == null) {
		//编码为空，则定义为盘盈资产
		this.props.cardTable.setValByKeyAndIndex(throughArea, index, vo_name.issettled, {
			value: true
		});
		this.props.cardTable.setValByKeyAndIndex(throughArea, index, vo_name.check_result, {
			value: '1',
			display: getMultiLangByID('452005008A-000000') /* 国际化处理：盘盈 */
		});
		this.props.cardTable.setValByKeyAndIndex(throughArea, index, vo_name.ischecked, {
			value: true
		});
		return true;
	}
	return false;
}

/**
 * 核对盘点结果
 * 
 * @param {*} id 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedrows 
 * @param {*} index 
 * @param {*} data 
 * @param {*} type 
 * @param {*} eventType 
 */
export function checkInvResult(id, key, value, changedrows, index, data, type, eventType) {
	// 首先需要获取  是否已处理（issettled）
	let issettled = this.props.cardTable.getValByKeyAndIndex(throughArea, index, vo_name.issettled).value;
	let ischecked = this.props.cardTable.getValByKeyAndIndex(throughArea, index, vo_name.ischecked).value;
	// 如果为空或者false  盘点结果设置为未处理
	if (issettled == false) {
		this.props.cardTable.setValByKeyAndIndex(throughArea, index, vo_name.check_result, {
			value: '4',
			display: getMultiLangByID('452005008A-000004') /* 国际化处理：未处理 */
		});
	} else if (!ischecked) {
		// 如果是未盘到， 盘点结果设置为 盘亏,清空盘点后字段值
		this.props.cardTable.setValByKeyAndIndex(throughArea, index, vo_name.check_result, {
			value: '2',
			display: getMultiLangByID('452005008A-000001') /* 国际化处理：盘亏 */
		});
		// 清空盘点后字段值
		let meta = this.props.meta.getMeta().throughArea;
		meta.items.map((item, item_index) => {
			if (RegExp(/after/).test(item.attrcode)) {
				this.props.cardTable.setValByKeyAndIndex(pageConfig.areas.card.throughArea, index, item.attrcode, {
					value: null,
					display: null
				});
			}
		});
	} else {
		/******************************************
		 * 根据盘点后相关字段对比设置盘点结果是否相符
		 * 首先要过滤出需要盘点的字段：
		 * 		用户模板中设置为显示的字段才需要进行对比
		 * 如果未显示的盘点后的字段没值时需要附上盘点前的值
		 *******************************************/
		let meta = this.props.meta.getMeta().throughArea;
		// 将未显示的盘点后已清空的字段的值附上盘点前的值
		let items_before_all = invItems.INVITEMS_BEFORE_ALL;
		let items_after_all = invItems.INVITEMS_AFTER_ALL;
		let indices = getInvisibleItem(meta);
		indices.forEach((i) => {
			let item_value_before = this.props.cardTable.getValByKeyAndIndex(throughArea, index, items_before_all[i]);
			this.props.cardTable.setValByKeyAndIndex(throughArea, index, items_after_all[i], {
				value: item_value_before.value,
				display: item_value_before.display
			});
		});
		let items_before = invItems.INVITEMS_BEFORE;
		let items_after = invItems.INVITEMS_AFTER;
		let isMatch = true;
		meta.items.map((meta_item) => {
			/* 盘点项目中去除模板为中隐藏的字段 */
			meta_item.visible == false &&
				items_before.map((item_before, item_index) => {
					if (meta_item.attrcode == item_before.value) {
						delete items_before[item_index];
						delete items_after[item_index];
					}
				});
		});
		items_before.map((item_before, item_index) => {
			let before_field = item_before.value;
			if (item_before.value.includes('_v')) {
				before_field = before_field.substring(0, before_field.length - 2);
			}
			let item_value_before = this.props.cardTable.getValByKeyAndIndex(throughArea, index, before_field).value;
			let item_value_after = this.props.cardTable.getValByKeyAndIndex(
				throughArea,
				index,
				items_after[item_index].value
			).value;
			if (item_value_before != item_value_after) {
				if (item_value_before == null && item_value_after == '') {
				} else {
					isMatch = false;
					this.props.cardTable.setValByKeyAndIndex(throughArea, index, vo_name.check_result, {
						value: '3',
						display: getMultiLangByID('452005008A-000003') /* 国际化处理：不符 */
					});
				}
			}
		});
		if (isMatch) {
			this.props.cardTable.setValByKeyAndIndex(throughArea, index, vo_name.check_result, {
				value: '0',
				display: getMultiLangByID('452005008A-000002') /* 国际化处理：相符 */
			});
		}
	}
}

/**
 * 获取模板里前后字段均未显示的index
 * @param {*} items 
 */
function getInvisibleItem(meta) {
	let items_before_all = invItems.INVITEMS_BEFORE_ALL;
	let items_after_all = invItems.INVITEMS_AFTER_ALL;
	let indices = [];
	for (let i = 0; i < items_before_all.length; i++) {
		let hidden_before = false;
		let hidden_after = false;
		meta.items.forEach((item) => {
			if (item.attrcode == items_before_all[i] && item.visible == false) {
				hidden_before = true;
			}
			if (item.attrcode == items_after_all[i] && item.visible == false) {
				hidden_after = true;
			}
		});

		if (hidden_before && hidden_after) {
			indices.push(i);
		}
	}

	return indices;
}
