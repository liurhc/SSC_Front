import { pageConfig, invItems } from './../../const.js';
import { checkInvResult } from './index';
const tableId = pageConfig.areas.card.throughArea;

/**
 * 表格操作按钮
 * @param {*} props 
 * @param {*} key 
 * @param {*} text 
 * @param {*} record 
 * @param {*} index 
 */
export default function(props, key, text, record, index) {
	switch (key) {
		// 表格操作按钮
		case 'Details':
			props.cardTable.toggleRowView(pageConfig.areas.card.throughArea, record);
			break;
		case 'OpenCard':
			props.cardTable.openModel(pageConfig.areas.card.throughArea, 'edit', record, index);
			break;
		case 'DeleteThrouthLine':
			// 盘盈  删除
			delLine.call(this, props, index, text);
			break;
		case 'Edit':
			// 修改： 相符 盘盈 不符 未处理
			editAction.call(props, e, tableId, status, record, index);
			break;
		case 'CancelLoss':
			cancelLossAction.call(this, index);
			break;
		default:
			break;
	}
}

/**
 * 取消盘亏
 * 		是否盘到->是
 * 		判断当前是相符or不符
 * 
 * @param {*} index 
 */
function cancelLossAction(index) {
	// 是否盘到->true
	this.props.cardTable.setValByKeyAndIndex(pageConfig.areas.card.throughArea, index, 'ischecked', {
		value: true
		// display: '相符'
	});
	checkInvResult.call(this, null, null, null, null, index);
}

/**
 * 修改事件处理，展开侧栏
 * 
 * @param {*} record 
 */
const editAction = (props, e, tableId, status, record, index) => {
	props.cardTable.openModel(tableId, 'edit', record, index);
	e.stopPropagation();
};

/**
 * 表格行删除
 * 
 * @param {*} props 
 * @param {*} index 
 */
function delLine(props, index, text) {
	props.cardTable.delRowsByIndex(pageConfig.areas.card.throughArea, index);
}
