import { pageConfig } from './../../const.js';
import { getPksByValues } from './index';

const vo_name = pageConfig.vo_name;

export default function(props, assignArea = { assignArea }, key, value, changedrows, index, record, type, eventType) {
	if (key == vo_name.dept_name) {
		if (value.length == 0) {
			this.props.editTable.setValByKeyAndIndex(assignArea, index, vo_name.deptincluedsub, { value: false });
			this.props.editTable.setValByKeyAndIndex(assignArea, index, vo_name.dept_pk, { value: null });
		} else {
			/***********************************
			 * 同一单元格，是否包含下级是相同的
			 * 如果不选中执行时包含下级，平台默
			 * 认不传runWithChildren参数
			 ***********************************/
			if (value[0].runWithChildren || true == value[0].runWithChildren) {
				this.props.editTable.setValByKeyAndIndex(assignArea, index, vo_name.deptincluedsub, {
					value: true
				});
			} else {
				this.props.editTable.setValByKeyAndIndex(assignArea, index, vo_name.deptincluedsub, {
					value: false
				});
			}
			/* 设置pk值 */
			this.props.editTable.setValByKeyAndIndex(assignArea, index, vo_name.dept_pk, {
				value: record.values.dept_name.value
			});
		}
	} else if (key == vo_name.category_name) {
		if (value.length == 0) {
			this.props.editTable.setValByKeyAndIndex(assignArea, index, vo_name.categincluedsub, { value: false });
			this.props.editTable.setValByKeyAndIndex(assignArea, index, vo_name.category_pk, { value: null });
		} else {
			/* 资产类别默认包含下级 */
			this.props.editTable.setValByKeyAndIndex(assignArea, index, vo_name.deptincluedsub, { value: true });
			/* 设置pk值 */
			this.props.editTable.setValByKeyAndIndex(assignArea, index, vo_name.category_pk, {
				value: record.values.category_name.value
			});
		}
	}
}
