import { pageConfig } from './../const.js';
import { ajax } from 'nc-lightapp-front';
import { setButtonsStatus } from './buttonClick';
import { assignAreaTableButtonClick, throughAreaTableButtonClick } from './../components/events';
import { getInventoryCardById, getPlanDataByPk_msg, getDetailByPlan, getDetail } from './index';
import ampub from 'ampub';
const { components, commonConst, utils } = ampub;
const { CommonKeys, StatusUtils } = commonConst;
const { IBusiRoleConst } = CommonKeys;
const { UISTATE } = StatusUtils;
const { queryAboutUtils, LoginContext, refInit } = components;
const { loginContext, getContext, loginContextKeys } = LoginContext;
const { openEquipCardByPk } = queryAboutUtils;
const { msgUtils, multiLangUtils } = utils;
const { showMessage } = msgUtils;
const { getMultiLangByID } = multiLangUtils;
const { defRefCondition, commonRefCondition } = refInit;

export default function(props) {
	props.createUIDom(
		{
			pagecode: pageConfig.pagecode.card_pagecode //页面编码
		},
		(data) => {
			if (data) {
				if (data.context) {
					//初始化上下文变量
					loginContext.call(this, data.context);
				}
				let ui_status = this.props.getUrlParam('status');
				if (data.button) {
					let button = data.button;
					modifierButtons.call(this, button);
					props.button.setButtons(button, () => {
						afterSetButtons.call(this, ui_status);
					});
				}
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this, this.props, meta);
					this.props.meta.setMeta(meta, () => {
						afterSetMeta.call(this, ui_status);
					});
				}
			}
		}
	);
}

/**
 * 调整按钮最初显隐性
 * 		设置为隐藏
 * 原因：为了避免刚刚刷新界面的时候所有按钮都显示造成的视觉上的冲击
 * @param {*} meta 
 */
function modifierButtons(buttons) {
	buttons.map((button) => {
		if (!RegExp(/inner/).test(button.area)) {
			if (button.children.length != 0) {
				button.children.map((itemButton) => {
					itemButton.visible = false;
				});
			} else {
				button.visible = false;
			}
		}
	});
}

/**
 * 修改模板
 * 
 * @param {*} props 
 * @param {*} meta 
 */
function modifierMeta(props, meta) {
	// let status = props.getUrlParam('status');
	meta[pageConfig.areas.card.dateArea].status = pageConfig.ui_status.edit;
	//参照过滤
	meta = filterRef.call(this, props, meta);
	/* 
	添加操作列，包括：
		1.分配盘点人界面操作列
		2.盘点结果穿透列表界面操作列
	*/
	addOperationColumn.call(this, props, meta);
	// 调整区域样式
	adjustAreasStyle.call(this, meta);
	return meta;
}

/**
 * 设置完按钮后操作
 * 		判断页面状态
 * @param {*} ui_status 
 */
function afterSetButtons(ui_status) {
	if (ui_status == UISTATE.add) {
		setButtonsStatus(this.props, pageConfig.pageStatus.stepOne.stepOne);
	} else if (ui_status == UISTATE.edit) {
		setButtonsStatus(this.props, pageConfig.pageStatus.stepTwo.beforeNext);
	}
}

/**
 * 设置完页面模板后操作
 * 		判断页面来源：
 * 			1.盘点报告详情界面穿透
 * 			2.待办中心点击消息生成当前组织下的盘点单
 * 			3.盘点单新增
 * 			4.盘点单修改
 * 			5.盘点单查看盘点结果
 * 			6.详情
 * 
 * @param {*} ui_status 
 */
function afterSetMeta(ui_status) {
	let pk_plan = this.props.getUrlParam('pk_plan');
	let pk_msg = this.props.getUrlParam('pk_msg');
	if (null != pk_plan) {
		executeReport.call(this, pk_plan);
	} else if (null != pk_msg) {
		ajax({
			url: '/nccloud/aum/inventory/queryMsg.do',
			data: {
				pkMsg: pk_msg
			},
			success: (res) => {
				if (res.data) {
					showMessage.call(this, this.props, {
						content: getMultiLangByID('452005008A-000032') /* 国际化处理：消息已被删除或已生成盘点单！ */,
						color: 'warning'
					});
					ui_status = pageConfig.ui_status.add;
					this.props.setUrlParam({ pk_msg: '' });
					setDefaultValue.call(this);
					// 消息被删除或者已生成盘点单时隐藏下一步按钮
					this.props.button.setButtonVisible(pageConfig.buttonName.NEXT, false);
				} else {
					executeMessage.call(this, pk_msg);
				}
			}
		});
	} else if (ui_status == pageConfig.ui_status.edit) {
		executeEdit.call(this);
	} else if (
		ui_status == pageConfig.ui_status.result ||
		ui_status == pageConfig.ui_status.browse ||
		this.props.getUrlParam('scene') == 'approve'
	) {
		executeResult.call(this);
	} else if (ui_status == pageConfig.ui_status.detail) {
		// TODO 单独抽取详情视图
		executeDetail.call(this);
	} else {
		setDefaultValue.call(this);
	}
	this.setState({ ui_status: ui_status });
}

function setDefaultValue() {
	// 默认主组织
	let pk_org = getContext(loginContextKeys.pk_org);
	let org_Name = getContext(loginContextKeys.org_Name);
	let bill_date = getContext(loginContextKeys.businessDate);
	this.props.search.setSearchValByField(pageConfig.areas.card.rangeArea, pageConfig.vo_name.pk_org, {
		value: pk_org,
		display: org_Name
	});
	this.props.form.setFormItemsValue(pageConfig.areas.card.dateArea, {
		start_date: { value: bill_date }
	});
	if (!pk_org) {
		this.props.search.setDisabled(pageConfig.areas.card.rangeArea, true);
		this.props.search.setDisabledByField(pageConfig.areas.card.rangeArea, pageConfig.vo_name.pk_org, false);
		// 当主组织没有时，清空其它条件
		this.props.search.clearSearchArea(pageConfig.areas.card.rangeArea);
	} else {
		// 有主组织时，先清空其它条件，再填上主组织
		this.props.search.clearSearchArea(pageConfig.areas.card.rangeArea);
		this.props.search.setSearchValByField(pageConfig.areas.card.rangeArea, pageConfig.vo_name.pk_org, {
			value: pk_org,
			display: org_Name
		});
	}
}

/**
 * 处理：盘点报告穿透
 */
function executeReport(pk_plan) {
	let pk_org_v = this.props.getUrlParam(pageConfig.vo_name.pk_org);
	let category = this.props.getUrlParam('category');
	getDetailByPlan.call(this, pk_plan, pk_org_v, category);
	this.props.button.setButtonsVisible(pageConfig.headBtnsVisible.checkDetail.browse);
	this.state.current = pageConfig.current.checkDetail;
}

/**
 * 处理：待办中心消息
 * 
 * @param {*} pk_msg 
 */
function executeMessage(pk_msg) {
	getPlanDataByPk_msg.call(this, pk_msg);
}

/**
 * 处理：盘点单修改
 */
export function executeEdit() {
	let pk = this.props.getUrlParam('id');
	if (pk && pk != 'undefined') {
		getInventoryCardById.call(this, pk, pageConfig.ui_status.edit);
	}
	this.props.editTable.setStatus(pageConfig.areas.card.assignArea, pageConfig.ui_status.edit);
	this.state.current = pageConfig.current.stepTwo.beforeNext;
}

/**
 * 处理：查看盘点结果
 */
export function executeResult() {
	let pk = this.props.getUrlParam('id');
	if (pk && pk != 'undefined') {
		getInventoryCardById.call(this, pk, pageConfig.ui_status.result);
	}
	this.props.button.setButtonsVisible(pageConfig.headBtnsVisible.checkResult);
	// 查看盘点结果
	this.setState({
		current: pageConfig.current.checkResult,
		ui_status: pageConfig.ui_status.result
	});
}

/**
 * 处理：详情
 */
function executeDetail() {
	getDetail.call(this);
}

/**
 * 调整区域样式
 * 
 * @param {*} meta 
 */
function adjustAreasStyle(meta) {
	meta[pageConfig.areas.card.assignArea].items = meta[pageConfig.areas.card.assignArea].items.map((item, key) => {
		item.width = 110;
		return item;
	});
	meta.searchArea.items.map((item) => {
		item.col = 6;
	});
	meta.dateArea.items.map((item) => {
		item.col = 4;
	});
}

/**
 * 添加操作列，包括
 * 		1.分配盘点人列表
 * 		2.盘点结果穿透区域
 * 
 * @param {*} props 
 * @param {*} meta 
 */
function addOperationColumn(props, meta) {
	//	分配盘点人列表添加操作列
	meta[pageConfig.areas.card.assignArea].items.push({
		attrcode: 'opr',
		label: getMultiLangByID('amcommon-000000') /* 国际化处理：操作 */,
		width: '200px',
		fixed: 'right',
		itemtype: 'customer',
		className: 'table-opr',
		visible: true,
		render: (text, record, index) => {
			let buttonAry = [ 'DelAssLine' ];
			let buttonarea = props.button.createOprationButton(buttonAry, {
				area: 'card_assign_table_inner',
				buttonLimit: 1,
				onButtonClick: (props, key) => assignAreaTableButtonClick.call(this, props, key, text, record, index)
			});
			return buttonarea;
		}
	});
	// 盘点结果穿透列表添加操作列
	meta[pageConfig.areas.card.throughArea].items.push({
		attrcode: 'opr',
		label: getMultiLangByID('amcommon-000000') /* 国际化处理：操作 */,
		width: '150px',
		fixed: 'right',
		itemtype: 'customer',
		className: 'table-opr',
		visible: true,
		render: (text, record, index) => {
			let buttonAry = [ 'Details' ];
			let cardTableStatus = props.cardTable.getStatus(pageConfig.areas.card.throughArea);
			let check_result;
			if (cardTableStatus == pageConfig.ui_status.edit) {
				check_result = record.values.check_result.value;
				// 相符
				check_result == 0 && (buttonAry = pageConfig.throughInnerBtnsVisible.editFreeCheck.match);
				// 盘盈
				check_result == 1 && (buttonAry = pageConfig.throughInnerBtnsVisible.editFreeCheck.profit);
				// 盘亏
				check_result == 2 && (buttonAry = pageConfig.throughInnerBtnsVisible.editFreeCheck.loss);
				// 不符
				check_result == 3 && (buttonAry = pageConfig.throughInnerBtnsVisible.editFreeCheck.unMatch);
				// 未处理
				check_result == 4 && (buttonAry = pageConfig.throughInnerBtnsVisible.editFreeCheck.unSettled);
			}
			let buttonarea = props.button.createOprationButton(buttonAry, {
				area: 'card_body_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => throughAreaTableButtonClick.call(this, props, key, text, record, index)
			});
			return buttonarea;
		}
	});
}

/**
 * 参照过滤
 * 
 * @param {*} props 
 * @param {*} meta 
 */
function filterRef(props, meta) {
	// 盘点范围区域过滤
	meta[pageConfig.areas.card.rangeArea].items.map((item) => {
		// 组织过滤  包括使用管理组织和货主管理组织
		if (item.attrcode == 'pk_org' || item.attrcode == 'pk_ownerorg' || item.attrcode == 'pk_usedorg') {
			item.queryCondition = () => {
				return { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter' };
			};
		} else if (item.attrcode == 'pk_used_status') {
			filterUsedStatus.call(this, item);
		} else if (item.attrcode == 'pk_location' || item.attrcode == 'pk_jobmngfil') {
			item.queryCondition = () => {
				let pk_org = this.props.search.getSearchValByField(pageConfig.areas.card.rangeArea, 'pk_org').value
					.firstvalue;
				return { pk_org };
			};
		} else if (item.attrcode == 'pk_mandept' || item.attrcode == 'pk_manager') {
			// 管理人、管理部门根据货主管理组织过滤
			item.queryCondition = () => {
				let pk_org = this.props.search.getSearchValByField(pageConfig.areas.card.rangeArea, 'pk_org').value
					.firstvalue;
				return { pk_org, busifuncode: IBusiRoleConst.ASSETORG };
			};
			if (item.attrcode == 'pk_mandept') {
				item.isRunWithChildren = true;
				item.defaultRunWithChildren = true;
			}
		} else if (item.attrcode == 'pk_usedept' || item.attrcode == 'pk_user') {
			// 使用人、使用部门根据使用权过滤
			item.queryCondition = () => {
				let pk_usedunit = this.props.search.getSearchValByField(pageConfig.areas.card.rangeArea, 'pk_usedunit')
					.value.firstvalue;
				return { pk_org: pk_usedunit, busifuncode: IBusiRoleConst.ASSETORG };
			};
			if (item.attrcode == 'pk_usedept') {
				item.isRunWithChildren = true;
				item.defaultRunWithChildren = true;
			}
		} else if (item.attrcode == 'pk_usedunit') {
			//使用权
		} else if (item.attrcode == 'pk_ownerunit') {
			//货主 根据货主委托关系进行参照
			item.queryCondition = () => {
				let pk_ownerorg = this.props.search.getSearchValByField(pageConfig.areas.card.rangeArea, 'pk_org')
					.value;
				return {
					pk_ownerorg,
					TreeRefActionExt: 'nccloud.web.aim.equip.equip.refCondition.PK_OWNERUNITSqlBuilder',
					DataPowerOperationCode: 'AMOwnerunit'
				};
			};
		} else if (item.attrcode == 'pk_used_status') {
			// 资产状态增加权限校验
			setDataPower(item);
		} else {
			// 适配自定义项1
			let bodyDefPrefix = 'bodyvos.def';
			defRefCondition.call(
				this,
				props,
				item,
				pageConfig.areas.card.rangeArea,
				getContext(loginContextKeys.groupId),
				true,
				'pk_org',
				bodyDefPrefix
			);
			defRefCondition.call(
				this,
				props,
				item,
				pageConfig.areas.card.rangeArea,
				getContext(loginContextKeys.groupId),
				true
			);
		}
		commonRefCondition.call(this, props, item);
	});

	// 分配盘点人区域过滤
	meta[pageConfig.areas.card.assignArea].items.map((item) => {
		// 部门 根据组织过滤
		if (item.attrcode == 'dept_name') {
			// 部门支持多选
			item.isMultiSelectedEnabled = true;
			item.isRunWithChildren = true;
			item.defaultRunWithChildren = true;
			item.queryCondition = () => {
				let pk_org = this.echartsData.pk_org;
				return { pk_org, busifuncode: IBusiRoleConst.ASSETORG, isShowUnit: true };
			};
		} else if (item.attrcode == 'category_name') {
			// 类别支持多选
			item.isMultiSelectedEnabled = true;
			item.isRunWithChildren = true;
			item.defaultRunWithChildren = true;
		} else if (item.attrcode == 'planedcheckusername') {
			// 计划盘点人  根据盘点范围汇总选择的资产组织过滤
			item.queryCondition = () => {
				let pk_org = this.echartsData.pk_org;
				return { pk_org, busifuncode: IBusiRoleConst.ASSETORG, isShowUnit: true };
			};
		}
	});

	// 重新分配盘点人区域过滤
	meta[pageConfig.areas.card.reAssignArea].items.map((item) => {
		// 计划盘点人  根据盘点范围中选择的资产组织过滤
		if (item.attrcode == 'planedcheckusername') {
			item.queryCondition = () => {
				let pk_org = this.echartsData.pk_org;
				return { pk_org, busifuncode: IBusiRoleConst.ASSETORG, isShowUnit: true };
			};
		} else {
			defRefCondition.call(
				this,
				props,
				item,
				pageConfig.areas.card.reAssignArea,
				getContext(loginContextKeys.groupId)
			);
		}
	});

	meta[pageConfig.areas.card.throughArea].items.map((item) => {
		if (item.attrcode == 'equip_code') {
			item.renderStatus = pageConfig.ui_status.browse;
			item.render = (text, record, index) => {
				if (record.values.check_result.value != '1') {
					return (
						<div class="card-table-browse">
							<span
								className="code-detail-link"
								onClick={() => {
									openEquipCardByPk.call(
										this,
										this.props,
										record.values[pageConfig.vo_name.pk_equip].value
									);
								}}
							>
								{record.values[item.attrcode] && record.values[item.attrcode].value}
							</span>
						</div>
					);
				}
				return (
					<div class="card-table-browse">
						<span>{record.values[item.attrcode] && record.values[item.attrcode].value}</span>
					</div>
				);
			};
		}
	});
	return meta;
}

/**
 * 过滤资产状态
 * 
 * @param {*} item 
 */
function filterUsedStatus(item) {
	item.queryCondition = () => {
		return {
			transi_type: getContext(loginContextKeys.transtype),
			bill_type: pageConfig.bill_type,
			GridRefActionExt: 'nccloud.web.aum.inventory.refcondition.EQUIP_STATUSSqlBuilder'
		};
	};
}

/**
 * 给基本档案设置数据权限
 * @param {*} item 
 */
function setDataPower(item) {
	item.dataPowerOperationCode = 'default';
	item.isDataPowerEnable = true;
}
