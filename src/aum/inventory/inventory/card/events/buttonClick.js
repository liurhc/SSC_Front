import { ajax, toast, print, cardCache, output } from 'nc-lightapp-front';
import { pageConfig, buttonName, invItems } from './../const.js';
import { getPageStatusByBillStatus } from './../components/events';
import { executeResult } from './index';
import ampub from 'ampub';
const { components, commonConst, utils } = ampub;
const { StatusUtils, CommonKeys } = commonConst;
const { UISTATE } = StatusUtils;
const { approveConst } = CommonKeys;
const { LoginContext } = components;
const { getContext, loginContextKeys } = LoginContext;
const { multiLangUtils, msgUtils } = utils;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { getMultiLangByID } = multiLangUtils;

const card_pagecode = pageConfig.pagecode.card_pagecode;

export default function buttonClick(props, id) {
	switch (id) {
		case buttonName.PREV:
			// 上一步
			prevAction.call(this, props);
			break;
		case buttonName.NEXT:
			// 下一步
			nextAction.call(this, props);
			break;
		case buttonName.SURE:
			// 确认
			stepDoneAction.call(this, props);
			break;
		case buttonName.DELETE:
			// 删除
			deleteAction.call(this, props);
			break;
		case buttonName.REASSIGN:
			// 重新分配
			reAssignAction.call(this, props);
			break;
		case buttonName.CANCEL:
			// 取消
			cancelAction.call(this, props);
			break;
		case buttonName.SAVE:
			// 保存
			if (this.state.ui_status == 'detail') {
				savePartAction.call(this, props);
			} else {
				saveSimpleAction.call(this, props);
			}
			break;
		case buttonName.EDIT:
			// 修改
			editAction.call(this);
			break;
		case buttonName.ADDLINE:
			// 增行
			addRowAction.call(this);
			break;
		case buttonName.COMMIT:
			// 提交
			commitAction.call(this, 'SAVE', 'commit');
			break;
		case buttonName.UNCOMMIT:
			// 收回
			commitAction.call(this, 'UNSAVE', 'commit');
			break;
		case buttonName.QUERYABOUTBILLFLOW:
			this.setState({
				showApprove: true,
				transi_type: this.state.extBillCard.head[pageConfig.areas.card.card_head].rows[0].values.transi_type
					.value
			});
			break;
		case buttonName.PRINT:
			// 打印
			printTemp.call(this, props);
			break;
		case buttonName.OUTPUT:
			// 输出
			outputTemp.call(this, props);
			break;
		case buttonName.BATCHALTER:
			// 批改
			batchAlterAction.call(this);
			break;
		case buttonName.REFRESH:
			//刷新
			refresh.call(this, props);
			break;
		case buttonName.ATTACHMENT:
			//附件
			attachmentAction.call(this);
			break;
		case buttonName.AllMatch:
			allMatchAction.call(this);
			break;
		default:
			break;
	}
}

function allMatchAction() {
	let visibleRows = this.props.cardTable.getVisibleRows(pageConfig.areas.card.throughArea);
	if (visibleRows == null) {
		return;
	}
	visibleRows.map((item, index) => {
		// 盘盈资产不处理
		if (item.values.check_result.value == '1') {
			return;
		}
		let meta = this.props.meta.getMeta().throughArea;
		let items_before = invItems.INVITEMS_BEFORE;
		let items_after = invItems.INVITEMS_AFTER;
		meta.items.map((meta_item) => {
			meta_item.visible == true &&
				items_before.map((item_before, item_index) => {
					if (meta_item.attrcode == item_before.value) {
						if (item_before.value.slice(-'_v'.length) == '_v') {
							let oid = item_before.value.substring(0, item_before.value.length - 2);
							let item_ovalue_before = this.props.cardTable.getValByKeyAndIndex(
								pageConfig.areas.card.throughArea,
								index,
								oid
							).value;
							let item_odisplay_before = this.props.cardTable.getValByKeyAndIndex(
								pageConfig.areas.card.throughArea,
								index,
								oid
							).display;
							this.props.cardTable.setValByKeyAndIndex(
								pageConfig.areas.card.throughArea,
								index,
								items_after[item_index].value,
								{
									value: item_ovalue_before,
									display: item_odisplay_before
								}
							);
							// 也给带v的after字段设值
							let item_value_before = this.props.cardTable.getValByKeyAndIndex(
								pageConfig.areas.card.throughArea,
								index,
								item_before.value
							).value;
							let item_display_before = this.props.cardTable.getValByKeyAndIndex(
								pageConfig.areas.card.throughArea,
								index,
								item_before.value
							).display;
							this.props.cardTable.setValByKeyAndIndex(
								pageConfig.areas.card.throughArea,
								index,
								items_after[item_index].value + '_v',
								{
									value: item_value_before,
									display: item_display_before
								}
							);
						} else {
							// 处理前后字段都不带v的情况
							let item_value_before = this.props.cardTable.getValByKeyAndIndex(
								pageConfig.areas.card.throughArea,
								index,
								item_before.value
							).value;
							let item_display_before = this.props.cardTable.getValByKeyAndIndex(
								pageConfig.areas.card.throughArea,
								index,
								item_before.value
							).display;
							this.props.cardTable.setValByKeyAndIndex(
								pageConfig.areas.card.throughArea,
								index,
								items_after[item_index].value,
								{
									value: item_value_before,
									display: item_display_before
								}
							);
						}
					}
				});
		});
		this.props.cardTable.setValByKeyAndIndex(
			pageConfig.areas.card.throughArea,
			index,
			pageConfig.vo_name.issettled,
			{
				value: true
			}
		);
		this.props.cardTable.setValByKeyAndIndex(
			pageConfig.areas.card.throughArea,
			index,
			pageConfig.vo_name.check_result,
			{
				value: '0',
				display: getMultiLangByID('452005008A-000002') /* 国际化处理：相符 */
			}
		);
		this.props.cardTable.setValByKeyAndIndex(
			pageConfig.areas.card.throughArea,
			index,
			pageConfig.vo_name.ischecked,
			{
				value: true
			}
		);
	});
}

/**
 * 附件
 */
function attachmentAction() {
	let billId = this.state.extBillCard.head[pageConfig.areas.card.card_head].rows[0].values.pk_inventory.value;
	this.props.ncUploader.show('warcontract-uploader', {
		billId: 'pam/inventory/' + billId
	});
}

/**
 * 穿透列表界面增行
 * 		1.增加一行
 * 		2.设置默认值（盘盈）
 * 			盘盈设备回写盘点人为当前操作用户
 * 		3.增行设置设备名称可编辑
 */
function addRowAction() {
	let numberOfRows = this.props.cardTable.getNumberOfRows(pageConfig.areas.card.throughArea, false);
	this.props.cardTable.addRow(pageConfig.areas.card.throughArea, numberOfRows, {
		check_result: { display: getMultiLangByID('452005008A-000000') /* 国际化处理：盘盈 */, value: '1' },
		ischecked: { value: true },
		issettled: { value: true },
		checkusername: {
			display: getContext(loginContextKeys.userName),
			value: getContext(loginContextKeys.userId)
		}
	});
	this.props.cardTable.setEditableByIndex(
		pageConfig.areas.card.throughArea,
		numberOfRows,
		[ 'equip_name', 'pk_category' ],
		true
	);
}

/**
* 刷新
*/
export function refresh() {
	let pk = this.props.getUrlParam('id');
	if (!pk) {
		return;
	}
	let ui_status = this.state.ui_status;
	if (ui_status == pageConfig.ui_status.result || ui_status == pageConfig.ui_status.browse) {
		// 查询到最新单据数据
		getInventoryCardById.call(this, pk, pageConfig.ui_status.result, () => {
			showMessage.call(this, this.props, { type: MsgConst.Type.RefreshSuccess });
		});
	} else {
		getDetail.call(this, () => {
			showMessage.call(this, this.props, { type: MsgConst.Type.RefreshSuccess });
		});
	}
}

/**
 * 批改
 * @param {*} props 
 */
export function batchAlterAction() {
	// 获取原始数据
	let changeData = this.props.editTable.getTableItemData(pageConfig.areas.card.reAssignArea);
	if (changeData.batchChangeKey == 'planedcheckusername') {
		// 如果需要批改的字段只是简单的赋值操作，则走前台的批改操作
		this.props.editTable.batchChangeTableData(pageConfig.areas.card.reAssignArea);
	}
}

/**
 * 打印
 * @param {*} props 
 */
export function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		return;
	}
	let category = props.getUrlParam('category');
	let status = props.getUrlParam('status');
	if (status == 'detail' && category != null) {
		printData.category = category;
		print(
			'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
			pageConfig.url.printUrl, // 后台打印服务url
			printData
		);
	} else {
		window.print();
	}
}

/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });
		return;
	}
	output({
		url: pageConfig.url.printUrl,
		data: printData
	});
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let pk = props.getUrlParam('id');
	if (!pk) {
		return;
	}
	let category = props.getUrlParam('category');
	if (!category && category != '0') {
		category = null;
	}
	let pks = JSON.stringify([ pk ]);
	let printData = {
		filename: card_pagecode, // 文件名称
		nodekey: pageConfig.printNodekey, // 模板节点标识
		oids: [ pks, category ], // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

/**
 * 穿透列表修改部分盘点单
 * @param {*} props 
 */
function savePartAction(props) {
	props.cardTable.filterEmptyRows(pageConfig.areas.card.throughArea, [ 'equip_name' ], 'include');
	// let changedRows = props.cardTable.getChangedRows(pageConfig.areas.card.throughArea);
	let extBillCard = this.state.extBillCard;
	delete extBillCard.bodys[pageConfig.areas.card.reAssignArea];
	extBillCard.bodys[pageConfig.areas.card.throughArea] = props.cardTable.getAllData(
		pageConfig.areas.card.throughArea
	);
	extBillCard.bodys[pageConfig.areas.card.throughArea].areacode = pageConfig.areas.card.throughArea;
	props.validateToSave(extBillCard, () => {
		ajax({
			url: '/nccloud/aum/inventory/update.do',
			data: { extBillCard },
			success: (res) => {
				this.state.extBillCard.head = res.data.head;
				this.state.extBillCard.bodys.throughArea = res.data.bodys.throughArea;
				props.cardTable.setTableData(
					pageConfig.areas.card.throughArea,
					res.data.bodys.throughArea,
					null,
					null,
					true
				);
				props.cardTable.setStatus(pageConfig.areas.card.throughArea, pageConfig.ui_status.browse);
				setButtonsStatus(props, pageConfig.pageStatus.checkDetail.borwseFreeCheck);
				// 保存成功后处理缓存
				cardCache.updateCache(
					'pk_inventory',
					res.data.head[pageConfig.areas.card.card_head].rows[0].values['pk_inventory'].value,
					res.data,
					pageConfig.areas.card.card_head,
					pageConfig.dataSource
				);
			}
		});
	});
}

//提交
export function commitAction(OperatorType, commitType, content) {
	let pk = this.props.getUrlParam('id');
	let paramInfoMap = {};
	paramInfoMap[pk] = this.state.extBillCard.head[pageConfig.areas.card.card_head].rows[0].values.ts.value;
	let obj = {
		dataType: 'listData',
		OperatorType: OperatorType,
		commitType: commitType,
		pageid: card_pagecode,
		paramInfoMap: paramInfoMap,
		content: content
	};
	let CardData = {};
	CardData.bill_type = pageConfig.bill_type;
	CardData.userjson = JSON.stringify(obj);
	ajax({
		url: '/nccloud/aum/inventory/commit.do',
		data: CardData,
		success: (res) => {
			if (content) {
				this.setState({
					compositedisplay: false
				});
			}
			getScriptCardReturn.call(this, res);
		}
	});
}

function getScriptCardReturn(res) {
	if (res.data.success === approveConst.HASAPPROVEALTRAN) {
		//指派信息
		let workflowData = res.data.approvealTranData;
		if (
			workflowData.workflow &&
			(workflowData.workflow == approveConst.APPROVEFLOW || workflowData.workflow == approveConst.WORKFLOW)
		) {
			this.setState({
				compositedata: workflowData,
				compositedisplay: true
			});
		}
	} else {
		if (res.data.success === approveConst.ALLSUCCESS) {
			toast({ content: res.data.successMsg, color: 'success' });
			this.state.extBillCard.head = res.data.cardVos[0].head;
			/* 为了让state刷新，界面重新渲染，手动setState,不想改变整个extBillCard的值 */
			this.setState({ current: pageConfig.current.checkResult });
			cardCache.updateCache(
				'pk_inventory',
				res.data.cardVos[0].head[pageConfig.areas.card.card_head].rows[0].values['pk_inventory'].value,
				res.data.cardVos[0],
				pageConfig.areas.card.card_head,
				pageConfig.dataSource
			);
			setButtonsStatus.call(
				this,
				this.props,
				getPageStatusByBillStatus.call(
					this,
					res.data.cardVos[0].head.card_head.rows[0].values.bill_status.value
				)
			);
		} else {
			toast({ content: res.data.errorMsg, color: 'warning' });
		}
	}
}

/**
 * 修改
 */
function editAction() {
	let pk_inventory = this.props.getUrlParam('id');
	ajax({
		url: '/nccloud/aum/inventory/inventory_edit.do',
		data: {
			pk: pk_inventory,
			resourceCode: pageConfig.node_code
		},
		success: (res) => {
			if (res.data) {
				toast({ color: 'warning', content: res.data });
			} else {
				setButtonsStatus(this.props, pageConfig.pageStatus.checkDetail.editFreeCheck);
				this.props.cardTable.setStatus(pageConfig.areas.card.throughArea, pageConfig.ui_status.edit);
			}
		}
	});
}

/**
 * 通过分配盘点人，保存或者修改保存整个盘点单
 * @param {*} props 
 */
function saveSimpleAction(props) {
	if (this.state.checkedValue != 1 && this.state.checkedValue != 1) {
		var assignData = props.editTable.getAllData(pageConfig.areas.card.assignArea);
	}
	let saveData = null;
	let status = props.getUrlParam('status');
	let saveUrl = null;
	// TODO 后面会将保存的billCard换为extCard，现在先从后台拼
	status == UISTATE.add &&
		(saveData = {
			pks: this.state.equipPks,
			schemeAssignInfo: {
				inventoryWay: this.state.checkedValue,
				rows: assignData ? assignData.rows : []
			},
			billCard: this.state.billCard,
			bill_type: pageConfig.bill_type,
			transi_type: getContext(loginContextKeys.transtype),
			pk_transi_type: getContext(loginContextKeys.pk_transtype),
			invOrgId: this.echartsData.pk_org
		});
	status == UISTATE.edit &&
		(saveData = {
			pk_inventory: props.getUrlParam('id'),
			schemeAssignInfo: {
				inventoryWay: this.state.checkedValue,
				rows: assignData ? assignData.rows : []
			},
			billCard: this.state.billCard,
			invOrgId: this.echartsData.pk_org
		});
	/*****************************注意*************************************
		 判断是否是从消息过来，如果是消息过来，将pk_transitype_src设置为消息主键，
		 用于在保存时判断是否执行设置消息已读状态后规则
		 后规则检测到此字段不为空需要修改此字段为来源单据主键
		*********************************************************************/
	let pk_msg = props.getUrlParam('pk_msg');
	pk_msg != null &&
		pk_msg != '' &&
		(saveData.billCard.head[pageConfig.areas.card.card_head].rows[0].values.pk_transitype_src.value = pk_msg);
	status == UISTATE.add && (saveUrl = '/nccloud/aum/inventory/save.do');
	status == UISTATE.edit && (saveUrl = '/nccloud/aum/inventory/update.do');
	ajax({
		url: saveUrl,
		data: saveData,
		success: (res) => {
			showMessage.call(this, props, { type: MsgConst.Type.SaveSuccess });
			props.setUrlParam({
				status: pageConfig.ui_status.result,
				id: res.data.head[pageConfig.areas.card.card_head].rows[0].values['pk_inventory'].value
			});
			// 保存成功后处理缓存
			if (status == UISTATE.add) {
				cardCache.addCache(
					res.data.head[pageConfig.areas.card.card_head].rows[0].values['pk_inventory'].value,
					res.data,
					pageConfig.areas.card.card_head,
					pageConfig.dataSource
				);
			} else {
				cardCache.updateCache(
					'pk_inventory',
					res.data.head[pageConfig.areas.card.card_head].rows[0].values['pk_inventory'].value,
					res.data,
					pageConfig.areas.card.card_head,
					pageConfig.dataSource
				);
			}
			executeResult.call(this);
		}
	});
}

/**
 * 取消
 * @param {*} props 
 */
function cancelAction(props) {
	if (this.state.current == pageConfig.current.checkDetail) {
		this.props.cardTable.filterEmptyRows(pageConfig.areas.card.throughArea, [ 'equip_name' ], 'include');
		this.props.cardTable.resetTableData(pageConfig.areas.card.throughArea);
		setButtonsStatus(this.props, pageConfig.pageStatus.checkDetail.borwseFreeCheck);
	} else {
		props.ncmodal.show(`${card_pagecode}-cancel-confirm`, {
			title: getMultiLangByID('msgUtils-000002') /* 国际化处理：取消 */,
			content: getMultiLangByID('452005008A-000026') /* 国际化处理：确定要取消当前操作信息，返回列表？ */,
			beSureBtnClick: () => {
				props.pushTo('/list', { pagecode: pageConfig.pagecode.list_pagecode });
			}
		});
	}
}

/**
 * 重新分配
 * @param {*} props 
 */
function reAssignAction(props) {
	let tableData = props.editTable.getAllData(pageConfig.areas.card.assignArea);
	this.setState({ current: pageConfig.current.stepTwo.beforeNext, assignData: tableData });
	setButtonsStatus.call(this, props, pageConfig.pageStatus.stepTwo.beforeNext);
}

/**
 * 删除
 * @param {*} props 
 */
function deleteAction(props) {
	showConfirm.call(this, props, {
		type: MsgConst.Type.Delete,
		beSureBtnClick: () => {
			let oldPk = this.childformData.headRow[0].values.pk_inventory.value;
			let paramInfoMap = {};
			paramInfoMap[oldPk] = this.childformData.headRow[0].values.ts.value;
			ajax({
				url: '/nccloud/aum/inventory/delete.do',
				data: {
					paramInfoMap: paramInfoMap,
					dataType: 'listData',
					OperatorType: 'DELETE',
					pageid: card_pagecode
				},
				success: (res) => {
					//消息处理
					//全部成功
					if (res.data.success === approveConst.ALLSUCCESS) {
						cardCache.deleteCacheById(pageConfig.vo_name.pk_inventory, oldPk, pageConfig.dataSource);
						toast({ content: res.data.successMsg, color: 'success' });
						//有错误信息
					} else {
						toast({ content: res.data.errorMsg, color: 'warning' });
					}
					props.pushTo('/list', { pagecode: pageConfig.pagecode.list_pagecode });
				}
			});
			let current = 0;
			this.setState({ current });
			setButtonsStatus(props, pageConfig.pageStatus.stepOne.stepOne);
		}
	});
}

/**
 * 确认
 */
function stepDoneAction() {
	/* 如果是盘点结果穿透进入，只修改计划盘点人，确认即为保存 */
	if (this.state.current == pageConfig.current.reAssign) {
		let extBillCard = this.state.extBillCard;
		delete extBillCard.bodys[pageConfig.areas.card.throughArea];
		extBillCard.bodys[pageConfig.areas.card.reAssignArea] = this.props.editTable.getAllData(
			pageConfig.areas.card.reAssignArea
		);
		extBillCard.bodys[pageConfig.areas.card.reAssignArea].areacode = pageConfig.areas.card.reAssignArea;
		this.props.validateToSave(extBillCard, () => {
			ajax({
				url: '/nccloud/aum/inventory/update.do',
				data: { extBillCard },
				success: (res) => {
					this.props.editTable.setTableData(pageConfig.areas.card.reAssignArea, res.data.bodys.reAssignArea);
					this.props.setUrlParam({ status: 'result' });
					executeResult.call(this);
				}
			});
		});
	} else {
		/* 根据步骤进行计划盘点人的修改 */
		let reAssignData = this.props.editTable.getAllData(pageConfig.areas.card.reAssignArea);
		/* 新增状态时：直接统计计划盘点人，执行翻译 */
		stepDoneCallBack.call(this, reAssignData);
	}
}

/**
 * 确认回调
 * 		1.新增直接调用
 * 		2.修改时作为修改查询分配的回调，
 * 		用于保证盘点结果穿透过来时，billCard
 * 		和barData取到最新值
 * 
 * @param {*} reAssignData 
 */
function stepDoneCallBack(reAssignData) {
	this.state.billCard.body.reAssignArea.areaType = 'table';
	this.state.billCard.body.reAssignArea.rows = reAssignData.rows;
	let linedataSource = reAssignData.rows;
	/*********************************************
	 * 处理barData，防止其传递引用改变this.echartsData.barData
	 * 使确认后穿透回列表仍然保持barData数据不变，确保统计数据准确
	 *********************************************/
	let barData = JSON.stringify(this.echartsData.barData);
	barData = JSON.parse(barData);
	/*****************************************
	 * 汇总统计表格计划盘点人统计数据，统一到后台执行翻译 
	 ****************************************/
	linedataSource.map((value) => {
		/* 此处处理bug：用户点击参照，但没有赋值，此时表格中计划盘点人value值变为''的情况 */
		if (!value.values.planedcheckusername.value) {
			value.values.planedcheckusername.value = null;
		}
		let nameValue = value.values.planedcheckusername.value;
		if (!barData[nameValue]) {
			barData[nameValue] = {};
			barData[nameValue].uncheck = 1;
		} else {
			barData[nameValue].uncheck = barData[nameValue].uncheck * 1 + 1;
		}
	});
	ajax({
		url: '/nccloud/aum/inventory/translatePsn.do',
		data: { barData },
		success: (res) => {
			this.setState({
				current: pageConfig.current.stepThree.boforeSure,
				barData: res.data.barData
			});
			setButtonsStatus(this.props, pageConfig.pageStatus.stepThree.boforeSure);
		}
	});
}
/**
 * 下一步
 * @param {*} props 
 */
function nextAction(props) {
	/* 根据当前的步骤判断是哪一个下一步 */
	switch (this.state.current) {
		/* 第一步 确定盘点范围 */
		case pageConfig.current.stepOne: {
			// 必输项校验
			if (!checkRequiredItem.call(this)) {
				return false;
			}
			this.rangeAreaData = this.props.search.getAllSearchData(pageConfig.areas.card.rangeArea);
			this.dateAreaData = this.props.form.getAllFormValue(pageConfig.areas.card.dateArea);
			// 准备echarts数据
			prepareEchartsData.call(this);
			// 获取设备数量和所有pks
			getEquipsNumsAndPks.call(this);
			break;
		}
		/* 第二步 分配盘点人 */
		case pageConfig.current.stepTwo.beforeNext: {
			if (this.state.checkedValue != 1 && this.state.checkedValue != 2) {
				this.props.editTable.filterEmptyRows(
					pageConfig.areas.card.assignArea,
					pageConfig.noCheckKeys.assignArea
				);
				var assignData = this.props.editTable.getAllRows(pageConfig.areas.card.assignArea, true);
				// let flag = this.props.editTable.checkRequired(pageConfig.areas.card.assignArea, assignData);
				// if (flag == false) {
				// 	toast({ content: '计划盘点人不能为空', color: 'warning' });
				// 	return;
				// }
			}
			/* 判断当前单据是新增的还是修改 */
			this.state.ui_status == pageConfig.ui_status.add
				? addQueryAssign.call(this, assignData)
				: editQueryAssign.call(this, assignData);
			break;
		}
		default:
			break;
	}
}

/**
 * 上一步
 * @param {*} props 
 */
function prevAction(props) {
	let { current } = this.state;
	if (current == pageConfig.current.stepTwo.beforeNext && this.state.ui_status === pageConfig.ui_status.edit) {
		showMessage.call(this, this.props, {
			content: getMultiLangByID('452005008A-000027') /* 可重新分配盘点人，不允许修改盘点范围！ */,
			color: 'warning'
		});
	} else {
		this.echartsData.linedata.name = [];
		this.echartsData.linedata.data = [];
		current = pageConfig.current.stepOne;
		this.setState({ current });
		setButtonsStatus(props, pageConfig.pageStatus.stepOne.stepOne);
		this.props.form.setAllFormValue({ dateArea: this.dateAreaData });
	}
}

/**
 * 根据不同页面和状态设置按钮显隐性
 * @param {*} props 
 */
export function setButtonsStatus(props, pageStatus) {
	// 根据不同的页面状态设置按钮的显隐性和主次关系
	if (pageStatus === pageConfig.pageStatus.stepOne.stepOne) {
		props.button.setButtonsVisible(pageConfig.headBtnsVisible.stepOne);
		props.button.setMainButton({
			Next: true,
			Prev: false,
			Sure: false,
			Delete: false,
			ReAssign: false
		});
	} else if (pageStatus === pageConfig.pageStatus.stepTwo.beforeNext) {
		props.button.setButtonsVisible(pageConfig.headBtnsVisible.stepTwo.beforeNext);
		/* 
		获取dom元素，判断表格是否存在
			不存在：说明是第一次进节点，按钮设置为不可用
			存在：判断分配盘点人表格是否选中了数据，根据是否选中设置删行按钮可用性
		*/
		if (document.getElementsByTagName('table').length == 0) {
			props.button.setButtonDisabled(pageConfig.buttonDisabled.unable);
		} else {
			let checkedRows = this.props.editTable.getCheckedRows('assignArea');
			checkedRows.length == 0 && this.props.button.setButtonDisabled(pageConfig.buttonDisabled.unable);
		}
		props.button.setMainButton({
			Next: true,
			Prev: false,
			Sure: false,
			Delete: false,
			ReAssign: false
		});
	} else if (pageStatus === pageConfig.pageStatus.stepTwo.reAssign) {
		props.button.setButtonsVisible(pageConfig.headBtnsVisible.stepTwo.reAssign);
		props.button.setMainButton({
			Next: false,
			Prev: false,
			Sure: true,
			Delete: false,
			ReAssign: false
		});
	} else if (pageStatus === pageConfig.pageStatus.stepThree.boforeSure) {
		props.button.setButtonsVisible(pageConfig.headBtnsVisible.stepThree.boforeSure);
		props.button.setMainButton({
			Next: false,
			Prev: false,
			Sure: true,
			Delete: false,
			ReAssign: false
		});
	} else if (pageStatus === pageConfig.pageStatus.stepThree.afterSure) {
		props.button.setButtonsVisible(pageConfig.headBtnsVisible.stepThree.afterSure);
		props.button.setMainButton({
			Next: false,
			Prev: false,
			Sure: false,
			Delete: false,
			ReAssign: false
		});
	} else if (pageStatus === pageConfig.pageStatus.checkResult.blank) {
		props.button.setButtonsVisible(pageConfig.headBtnsVisible.checkResult.blank);
	} else if (pageStatus === pageConfig.pageStatus.checkResult.free_check) {
		props.button.setButtonsVisible(pageConfig.headBtnsVisible.checkResult.free_check);
	} else if (pageStatus === pageConfig.pageStatus.checkResult.un_check) {
		props.button.setButtonsVisible(pageConfig.headBtnsVisible.checkResult.un_check);
	} else if (pageStatus === pageConfig.pageStatus.checkResult.check_going) {
		props.button.setButtonsVisible(pageConfig.headBtnsVisible.checkResult.check_going);
	} else if (pageStatus === pageConfig.pageStatus.checkResult.check_pass) {
		props.button.setButtonsVisible(pageConfig.headBtnsVisible.checkResult.check_pass);
	} else if (pageStatus === pageConfig.pageStatus.checkResult.check_nopass) {
		props.button.setButtonsVisible(pageConfig.headBtnsVisible.checkResult.check_nopass);
	} else if (pageStatus === pageConfig.pageStatus.checkDetail.borwseFreeCheck) {
		props.button.setButtonsVisible(pageConfig.headBtnsVisible.checkDetail.borwseFreeCheck);
	} else if (pageStatus === pageConfig.pageStatus.checkDetail.editFreeCheck) {
		props.button.setButtonsVisible(pageConfig.headBtnsVisible.checkDetail.editFreeCheck);
	} else if (pageStatus === pageConfig.pageStatus.checkDetail.browse) {
		props.button.setButtonsVisible(pageConfig.headBtnsVisible.checkDetail.browse);
	} else if (pageStatus === pageConfig.pageStatus.throughReAssign.throughReAssign) {
		props.button.setButtonsVisible(pageConfig.headBtnsVisible.reAssign);
	}
}

export function getInventoryCardById(pk, status, callBack) {
	ajax({
		url: '/nccloud/aum/inventory/queryCard.do',
		data: {
			pk,
			pagecode: card_pagecode
		},
		success: (res) => {
			editPrepareEchartsData.call(this, res);
			if (res.data) {
				let checkedValue =
					res.data.extBillCard.head[pageConfig.areas.card.card_head].rows[0].values.inventoryway.value;
				checkedValue = checkedValue ? checkedValue : pageConfig.inventoryWay.USERINV;
				this.setState({
					equipsNum: res.data.equipsNum,
					legendData: res.data.legendData,
					barData: res.data.barData,
					checkedValue: checkedValue, // 默认责任人盘点
					extBillCard: res.data.extBillCard,
					billCard: res.data,
					pk_inventory:
						res.data.extBillCard.head[pageConfig.areas.card.card_head].rows[0].values.pk_inventory.value
				});
				// 设置表格数据（分配盘点人区域）
				this.props.editTable.setTableData(
					pageConfig.areas.card.assignArea,
					res.data.extBillCard.bodys[pageConfig.areas.card.assignArea]
				);
				// 如果未分配盘点人有数据，则设置未分配盘点人列表结果
				if (res.data.extBillCard.bodys.reAssignArea) {
					this.props.editTable.setTableData(
						pageConfig.areas.card.reAssignArea,
						res.data.extBillCard.bodys[pageConfig.areas.card.reAssignArea]
					);
				}
				// 根据选中的盘点方式，设置右侧表格列显隐性
				changeSelectValue.call(this, checkedValue);
				if (status == 'result') {
					let bill_status =
						res.data.extBillCard.head[pageConfig.areas.card.card_head].rows[0].values.bill_status.value;
					let pageStatus = null;
					if (bill_status == 0) {
						pageStatus = pageConfig.pageStatus.checkResult.free_check;
					} else if (bill_status == 1) {
						pageStatus = pageConfig.pageStatus.checkResult.un_check;
					} else if (bill_status == 2) {
						pageStatus = pageConfig.pageStatus.checkResult.check_going;
					} else if (bill_status == 3) {
						pageStatus = pageConfig.pageStatus.checkResult.check_pass;
					} else if (bill_status == 4) {
						pageStatus = pageConfig.pageStatus.checkResult.check_nopass;
					}
					setButtonsStatus(this.props, pageStatus);
				}
			} else {
				let checkedValue = pageConfig.inventoryWay.USERINV;
				this.setState({
					equipsNum: 0,
					legendData: {},
					barData: {},
					checkedValue: checkedValue, // 默认责任人盘点
					extBillCard: {},
					billCard: {},
					pk_inventory: ''
				});
				// 设置表格数据（分配盘点人区域）
				this.props.editTable.setTableData(pageConfig.areas.card.assignArea, { rows: [] });
				// 如果未分配盘点人有数据，则设置未分配盘点人列表结果
				this.props.editTable.setTableData(pageConfig.areas.card.reAssignArea, { rows: [] });
				// 根据选中的盘点方式，设置右侧表格列显隐性
				changeSelectValue.call(this, checkedValue);

				setButtonsStatus(this.props, pageConfig.pageStatus.checkResult.blank);
			}
			typeof callBack == 'function' && callBack.call(this);
		}
	});
}

/**
 * 根据选中的盘点方式，设置右侧表格列的显隐性
 * 
 * @param {*} checkedValue 
 */
function changeSelectValue(checkedValue) {
	// 获取区域
	let meta = this.props.meta.getMeta().assignArea;
	//给盘点方式赋值
	this.state.checkedValue = checkedValue;
	if (checkedValue == '3' || checkedValue == '4') {
		meta.items.map((subItem) => {
			if (subItem.attrcode === 'category_name') {
				subItem.visible = false;
			}
			if (subItem.attrcode === 'dept_name') {
				subItem.visible = true;
			}
		});
		this.setState({ isHint: true });
	} else if (checkedValue == '5') {
		meta.items.map((subItem) => {
			if (subItem.attrcode === 'dept_name') {
				subItem.visible = false;
			}
			if (subItem.attrcode === 'category_name') {
				subItem.visible = true;
			}
		});
		this.setState({ isHint: true });
	} else if (checkedValue == '6' || checkedValue == '7') {
		meta.items.map((subItem) => {
			if (subItem.attrcode === 'dept_name') {
				subItem.visible = true;
			}
			if (subItem.attrcode === 'category_name') {
				subItem.visible = true;
			}
		});
		this.setState({ isHint: true });
	}
}
export function getPlanDataByPk_msg(pk_msg) {
	ajax({
		url: '/nccloud/aum/inventory/queryPlan.do',
		data: { id: pk_msg },
		success: (res) => {
			afterGetMessage.call(this, res.data);
		}
	});
}

function afterGetMessage(data) {
	// 给资产组织赋值并且设置此字段不可编辑
	if (data.pk_org) {
		this.props.search.setSearchValByField(pageConfig.areas.card.rangeArea, pageConfig.vo_name.pk_org, {
			value: data.pk_org,
			display: data.pk_org_display
		});
	}
	// 给盘点范围赋值
	if (data.range_detail) {
		this.props.search.setSearchValue(pageConfig.areas.card.rangeArea, JSON.parse(data.range_detail));
	}
	// 给盘点结束日期赋值
	if (data.invEndDate) {
		this.props.form.setFormItemsValue(pageConfig.areas.card.dateArea, {
			end_date: { value: data.invEndDate, display: null }
		});
	}
	// 给盘点开始日期赋值
	if (data.invStartDate) {
		this.props.form.setFormItemsValue(pageConfig.areas.card.dateArea, {
			start_date: { value: data.invStartDate, display: null }
		});
	}
	// 消息过来的盘点计划生成盘点单，资产组织不可编辑
	this.props.search.setDisabledByField(pageConfig.areas.card.rangeArea, pageConfig.vo_name.pk_org, true);
}

export function clearAssignAreaData() {
	this.props.editTable.setTableData(pageConfig.areas.card.assignArea, { rows: [] });
}

function checkRequiredItem() {
	if (this.props.search.getAllSearchData(pageConfig.areas.card.rangeArea) === false) {
		showMessage.call(this, this.props, {
			content: getMultiLangByID('452005008A-000028') /* 国际化处理：请选择资产组织！ */,
			color: 'warning'
		});
		return false;
	}
	if (this.props.form.getAllFormValue(pageConfig.areas.card.dateArea).rows[0].values.start_date.value === null) {
		showMessage.call(this, this.props, {
			content: getMultiLangByID('452005008A-000029') /* 国际化处理：请选择盘点开始日期！ */,
			color: 'warning'
		});
		return false;
	}
	if (this.props.form.getAllFormValue(pageConfig.areas.card.dateArea).rows[0].values.end_date.value) {
		if (
			this.props.form.getAllFormValue('dateArea').rows[0].values.end_date.value <
			this.props.form.getAllFormValue('dateArea').rows[0].values.start_date.value
		) {
			showMessage.call(this, this.props, {
				content: getMultiLangByID('452005008A-000030') /* 国际化处理：盘点结束日期不能早于盘点开始日期！*/,
				color: 'warning'
			});
			return false;
		}
	}
	return true;
}
/* 准备echarts数据（支持第一步到第二步跳转时操作） */
function prepareEchartsData() {
	let newPk_org = this.props.search.getSearchValByField(pageConfig.areas.card.rangeArea, 'pk_org').value.firstvalue;
	/* 
	判断资产组织是否切换
		是：清空第二步中分配盘点人信息
		否：不做任何操作，保留第二步信息
	*/
	this.echartsData.pk_org != null && this.echartsData.pk_org != newPk_org && clearAssignAreaData.call(this);
	// 设置 资产组织
	this.echartsData.pk_org = newPk_org;
	// 设置 资产类别
	this.props.search.getAllSearchData(pageConfig.areas.card.rangeArea) != null &&
		this.props.search.getAllSearchData(pageConfig.areas.card.rangeArea).conditions.map((item) => {
			if (item.field === 'pk_category') {
				this.echartsData.pk_type = item.display;
			}
		});
	this.echartsData.start_date = this.props.form.getAllFormValue(
		pageConfig.areas.card.dateArea
	).rows[0].values.start_date.value;
	this.echartsData.end_date = this.props.form.getAllFormValue(
		pageConfig.areas.card.dateArea
	).rows[0].values.end_date.value;
}

function getEquipsNumsAndPks() {
	// 获取资产组织
	let invOrgId = this.echartsData.pk_org;
	// 获取 盘点开始结束、开始日期
	let invStartDate = this.echartsData.start_date;
	let invEndDate = this.echartsData.end_date;
	let queryInfo = this.props.search.getQueryInfo(pageConfig.areas.card.rangeArea, true);
	let queryAssignData = {
		invOrgId,
		queryInfo,
		invStartDate,
		invEndDate
	};
	ajax({
		url: '/nccloud/aum/inventory/queryEquips.do',
		data: queryAssignData,
		success: (res) => {
			if (res.data.equipsNum > 0) {
				let current = this.state.current + 1;
				this.setState({ current, equipsNum: res.data.equipsNum, equipPks: res.data.pks });
				setButtonsStatus.call(this, this.props, pageConfig.pageStatus.stepTwo.beforeNext);
			} else {
				showMessage.call(this, this.props, {
					content: getMultiLangByID('452005008A-000031') /* 国际化处理：盘点范围内没有任何设备，请重新确定盘点范围！*/,
					color: 'warning'
				});
			}
		}
	});
}

function addQueryAssign(assignData, callBack) {
	let queryAssignData = getAddQueryAssignData.call(this, assignData);
	ajax({
		url: '/nccloud/aum/inventory/taskAssign.do',
		data: queryAssignData,
		success: (res) => {
			let { billCard, barData, current } = this.state;
			billCard = res.data.billCard;
			barData = res.data.barData;
			this.echartsData.barData = res.data.barData;
			let flag = res.data.billCard.body != null ? true : false;
			// 如果有未分配盘点人的设备
			if (flag == true) {
				current = pageConfig.current.stepTwo.reAssign;
			} else {
				// 如果全部分配
				current = current + 1;
			}
			this.setState({ billCard, barData, current }, () => {
				if (flag == true) {
					this.props.editTable.setTableData(
						pageConfig.areas.card.reAssignArea,
						res.data.billCard.body.reAssignArea
					);
					this.props.editTable.setStatus(pageConfig.areas.card.reAssignArea, 'edit');
					setButtonsStatus.call(this, this.props, pageConfig.pageStatus.stepTwo.reAssign);
				} else {
					setButtonsStatus.call(this, this.props, pageConfig.pageStatus.stepThree.boforeSure);
				}
				callBack && callBack();
			});
		}
	});
}

function getAddQueryAssignData(assignData) {
	// 获取资产组织
	let invOrgId = this.echartsData.pk_org;
	// 获取 盘点范围 高级查询区域参数
	// let querycondition = this.props.search.getAllSearchData(pageConfig.areas.card.rangeArea);
	let queryInfo = this.props.search.getQueryInfo(pageConfig.areas.card.rangeArea);
	queryInfo.pagecode = card_pagecode;
	// 获取 盘点开始结束、开始日期
	let invStartDate = this.echartsData.start_date;
	let invEndDate = this.echartsData.end_date;
	// 组装查询分配参数
	return {
		invOrgId,
		pks: this.state.equipPks,
		queryInfo,
		invStartDate,
		invEndDate,
		schemeAssignInfo: {
			inventoryWay: this.state.checkedValue,
			rows: assignData ? assignData : []
		},
		pagecode: card_pagecode
	};
}
/**
 * callBack可作为判断是否是盘点结果穿透
 * 
 * @param {*} assignData 
 * @param {*} callBack 
 */
function editQueryAssign(assignData, callBack) {
	let pk_inventory = this.props.getUrlParam('id');
	let taskAssignData = {
		schemeAssignInfo: {
			inventoryWay: this.state.checkedValue,
			rows: assignData ? assignData : []
		},
		pk_inventory,
		pagecode: card_pagecode
	};
	ajax({
		url: '/nccloud/aum/inventory/taskAssign.do',
		data: taskAssignData,
		success: (res) => {
			/* 不渲染界面，直接赋最新值 */
			this.state.billCard = res.data.billCard;
			this.state.barData = res.data.barData;
			// 更新统计数据
			this.echartsData.barData = res.data.barData;
			/******************************************
			 * flag: 判断跳转界面标识
			 * 		true:未分配盘点人界面
			 *  	false：第三步确认界面
			 *****************************************/
			let flag = res.data.billCard.body != null ? true : false;
			if (flag == true) {
				/* 盘点结果穿透不更新数据 */
				!callBack &&
					this.props.editTable.setTableData(
						pageConfig.areas.card.reAssignArea,
						res.data.billCard.body.reAssignArea
					);
				this.props.editTable.setStatus(pageConfig.areas.card.reAssignArea, pageConfig.ui_status.edit);
				let current = 5;
				this.setState({ current });
				setButtonsStatus(this.props, pageConfig.pageStatus.stepTwo.reAssign);
			} else {
				// 如果全部分配
				let current = this.state.current + 1;
				this.setState({ current });
				setButtonsStatus(this.props, pageConfig.pageStatus.stepThree.boforeSure);
			}
			/***************************************
			 * 穿透回调
			 * 应用范围：从盘点结果穿透执行的回调
			 ****************************************/
			callBack && callBack();
		}
	});
}

function editPrepareEchartsData(res) {
	if (res && res.data && res.data.extBillCard) {
		this.echartsData.pk_org =
			res.data.extBillCard.head[pageConfig.areas.card.card_head].rows[0].values.pk_org.value;
	} else {
		this.echartsData.pk_org = '';
	}
}

/**
 * 根据盘点计划查询盘点单明细
 * for：盘点报告穿透
 * 依据：
 * 		1.盘点计划主键
 * 		2.组织主键
 * 		3.类别
 * 
 * @param {*} pk_plan 
 * @param {*} pk_org_v 
 * @param {*} category 
 */
export function getDetailByPlan(pk_plan, pk_org_v, category) {
	category = dealWithCategory.call(this, category);
	ajax({
		url: '/nccloud/aum/inventory/queryDetailByPlan.do',
		data: {
			pk_plan,
			pk_org_v,
			category,
			pagecode: card_pagecode
		},
		success: (res) => {
			this.props.setUrlParam({ category, id: res.data.head.card_head.rows[0].values.pk_inventory.value });
			res.data.bodys != null &&
				this.props.cardTable.setTableData(
					pageConfig.areas.card.throughArea,
					res.data.bodys[pageConfig.areas.card.throughArea]
				);
		}
	});
}

/**
 * 根据类别查询盘点单明细
 * @param {*} category 
 */
export function getDetail(callBack) {
	let category = this.props.getUrlParam('category');
	let pk_inventory = this.props.getUrlParam('id');
	ajax({
		url: '/nccloud/aum/inventory/queryDetail.do',
		data: {
			pk_inventory,
			category,
			page: card_pagecode
		},
		success: (res) => {
			this.props.setUrlParam({ category });
			res.data.bodys != null &&
				this.props.cardTable.setTableData(
					pageConfig.areas.card.throughArea,
					res.data.bodys[pageConfig.areas.card.throughArea],
					null,
					false
				);
			this.setState({
				pk_inventory: res.data.head[pageConfig.areas.card.card_head].rows[0].values.pk_inventory.value,
				extBillCard: {
					bodys: res.data.bodys,
					head: this.state.extBillCard.head,
					pageid: this.state.extBillCard.pageid,
					templetid: this.state.extBillCard.templetid,
					userjson: this.state.extBillCard.userjson
				},
				current: pageConfig.current.checkDetail
			});
			let bill_status = res.data.head[pageConfig.areas.card.card_head].rows[0].values.bill_status.value;
			if (bill_status == 0) {
				setButtonsStatus(this.props, pageConfig.pageStatus.checkDetail.borwseFreeCheck);
			} else {
				setButtonsStatus(this.props, pageConfig.pageStatus.checkDetail.browse);
			}
			typeof callBack == 'function' && callBack.call(this);
		}
	});
}

/**
 * 根据category名称映射标识
 * 
 * @param {*} category 
 */
function dealWithCategory(category) {
	if (!category) {
		return;
	}
	if (category == 'unchecked_num') {
		return 0;
	} else if (category == 'match_num') {
		return 1;
	} else if (category == 'unmatch_num') {
		return 2;
	} else if (category == 'profit_num') {
		return 3;
	} else if (category == 'loss_num') {
		return 4;
	} else if (category == 'total_num') {
		return 5;
	} else if (category == 'checked_num') {
		return 7;
	}
}
