import { ajax } from 'nc-lightapp-front';
import { pageConfig } from '../const';
import { setButtonsStatus } from './buttonClick';

export default function(props, pk) {
	let data = {
		pk: pk,
		pageid: pageConfig.pagecode.card_pagecode
	};
	ajax({
		url: '/nccloud/aum/inventory/queryCard.do',
		data: data,
		success: (res) => {
			props.setUrlParam({ id: pk }); //动态修改地址栏中的id的值
			this.setState(
				{
					equipsNum: res.data.equipsNum,
					legendData: res.data.legendData,
					barData: res.data.barData,
					checkedValue:
						res.data.extBillCard.head[pageConfig.areas.card.card_head].rows[0].values.inventoryway.value,
					extBillCard: res.data.extBillCard,
					billCard: res.data
				},
				() => {
					this.props.editTable.setTableData(
						pageConfig.areas.card.assignArea,
						res.data.extBillCard.bodys[pageConfig.areas.card.assignArea]
					);
					if (this.state.ui_status == 'result') {
						let bill_status =
							res.data.extBillCard.head[pageConfig.areas.card.card_head].rows[0].values.bill_status.value;
						let pageStatus = null;
						if (bill_status == 0) {
							pageStatus = pageConfig.pageStatus.checkResult.free_check;
						} else if (bill_status == 1) {
							pageStatus = pageConfig.pageStatus.checkResult.un_check;
						} else if (bill_status == 2) {
							pageStatus = pageConfig.pageStatus.checkResult.check_going;
						} else if (bill_status == 3) {
							pageStatus = pageConfig.pageStatus.checkResult.check_pass;
						}
						setButtonsStatus(this.props, pageStatus);
					}
				}
			);
		}
	});
}
