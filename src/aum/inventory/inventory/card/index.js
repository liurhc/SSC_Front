import './index.less';
import React, { Component } from 'react';
import { createPage, ajax, base, high, toast } from 'nc-lightapp-front';
import { StepOne, StepTwo, StepThree, ReAssign, InventoryResult, CheckDetail } from './components';
import { pageConfig } from './const.js';
import {
	buttonClick,
	initTemplate,
	pageInfoClick,
	clearAssignAreaData,
	commitAction,
	executeResult,
	executeEdit
} from './events';
import { setButtonsStatus } from './events/buttonClick';
import ampub from 'ampub';
const { components, commonConst, utils } = ampub;
const { CommonKeys } = commonConst;
const { linkQueryConst } = CommonKeys;
const { ApprovalTrans } = components;
const { multiLangUtils, msgUtils, closeBrowserUtils } = utils;
const { showMessage } = msgUtils;
const { getMultiLangByID } = multiLangUtils;

const { NCStep, NCBackBtn, NCAffix } = base;
const { NCSteps } = NCStep;
const steps = [
	{ title: getMultiLangByID('452005008A-000034') /* 国际化处理：确定盘点范围 */ },
	{ title: getMultiLangByID('452005008A-000035') /* 国际化处理：分配盘点人 */ },
	{ title: getMultiLangByID('452005008A-000036') /* 国际化处理：确认 */ }
];

/**
 * 资产盘点
 * @author : liuzy10 
 */
class Maininfo extends Component {
	constructor(props) {
		super(props);
		this.state = {
			/*重构代码必要参数*/
			ui_status: pageConfig.ui_status.add,
			bill_status: pageConfig.bill_status.free_check,
			compositedisplay: false,
			compositedata: {},
			pk_inventory: '',
			checkedValue: 1, //步骤二中分配盘点方式的值，默认选中使用人盘点
			assignData: null, // 步骤二盘点方式table中的数据
			// 审批详情
			showApprove: false,

			status: '',
			current: pageConfig.current.stepOne, //步骤条对应页面的参数
			isHint: false,
			equipsNum: 0,
			equipPks: [],
			extBillCard: {},
			barData: {},
			legendData: {},
			billCard: {}
		};
		closeBrowserUtils(props, {
			editTable: [
				pageConfig.areas.card.assignArea,
				pageConfig.areas.card.reAssignArea,
				pageConfig.areas.card.throughArea
			]
		});
		this.rangeAreaData = null;
		this.dateAreaData = null;
		initTemplate.call(this, props);
		this.echartsData = {
			// 第一步获取 或者 修改态进入赋值
			pk_org: null,
			start_date: 0,
			end_date: 0,
			pk_type: '',
			check_range: '',
			// 第二步获取
			total_num: 0,
			linedata: {
				data: [],
				name: []
			},
			barData: null
		};
		this.childformData = {
			headRow: [],
			bodyRows: []
		};
		this.onClickLine = this.onClickLine.bind(this);
		this.onResultClickLine = this.onResultClickLine.bind(this);
	}

	componentDidMount() {}

	//提交及指派 回调
	getAssginUsedr = (value) => {
		commitAction.call(this, 'SAVE', 'commit', value);
	};
	//取消 指派
	turnOff = () => {
		this.setState({
			compositedisplay: false
		});
	};

	/* 根据当前选中的盘点方式进行切换 */
	changeSelectValue = (value, path) => {
		clearAssignAreaData.call(this);
		//给盘点方式赋值
		this.state.checkedValue = value;
		if (value == '3' || value == '4') {
			this.props.editTable.hideColByKey(pageConfig.areas.card.assignArea, [ 'category_name' ]);
			this.props.editTable.showColByKey(pageConfig.areas.card.assignArea, [ 'dept_name' ]);
		} else if (value == '5') {
			this.props.editTable.hideColByKey(pageConfig.areas.card.assignArea, [ 'dept_name' ]);
			this.props.editTable.showColByKey(pageConfig.areas.card.assignArea, [ 'category_name' ]);
		} else if (value == '6' || value == '7') {
			this.props.editTable.showColByKey(pageConfig.areas.card.assignArea, [ 'dept_name' ]);
			this.props.editTable.showColByKey(pageConfig.areas.card.assignArea, [ 'category_name' ]);
		}
		this.setState({ isHint: true, checkedValue: value }, () => {
			if (value != '1' && value != '2') {
				this.props.editTable.setTableData(pageConfig.areas.card.assignArea, { rows: [] });
				this.props.editTable.addRow(pageConfig.areas.card.assignArea, 0, false);
			}
		});
	};

	//分配盘点人中   分配盘点方式
	handleChange = (value) => {
		if (value == this.state.checkedValue) {
			return;
		}
		let path = this.props.meta.getMeta().assignArea;
		if (this.state.isHint) {
			this.props.editTable.filterEmptyRows(pageConfig.areas.card.assignArea, pageConfig.noCheckKeys.assignArea);
			let assignData = this.props.editTable.getAllData(pageConfig.areas.card.assignArea);
			if (assignData.rows.length != 0) {
				this.props.ncmodal.show(`${pageConfig.pagecode.card_pagecode}-del-confirm`, {
					content: getMultiLangByID('452005008A-000037') /* 国际化处理：切换分配类型，将清空未盘资产的计划盘点人，确认切换？ */,
					beSureBtnClick: () => this.changeSelectValue(value, path)
				});
			} else {
				this.changeSelectValue(value, path);
			}
		} else {
			this.changeSelectValue(value, path);
		}
	};

	onMyChart = (params) => {
		this.props.editTable.setTableData(pageConfig.areas.card.throughArea, this.childformData.bodyRows);
	};

	onLegendClick = (category) => {
		let pk_inventory = this.props.getUrlParam('id');
		ajax({
			url: '/nccloud/aum/inventory/queryDetail.do',
			data: {
				pk_inventory,
				category
			},
			success: (res) => {
				this.props.setUrlParam({ category });
				this.props.setUrlParam({ status: 'detail' });
				setTimeout(() => {
					res.data.bodys != null &&
						this.props.cardTable.setTableData(
							pageConfig.areas.card.throughArea,
							res.data.bodys[pageConfig.areas.card.throughArea],
							null,
							true
						);
				}, 1);
				this.setState({
					ui_status: pageConfig.ui_status.detail,
					extBillCard: {
						bodys: res.data.bodys,
						head: this.state.extBillCard.head,
						pageid: this.state.extBillCard.pageid,
						templetid: this.state.extBillCard.templetid,
						userjson: this.state.extBillCard.userjson
					},
					current: 11
				});
				let bill_status = this.state.extBillCard.head[pageConfig.areas.card.card_head].rows[0].values
					.bill_status.value;
				if (bill_status == 0 || bill_status == 4) {
					setButtonsStatus(this.props, pageConfig.pageStatus.checkDetail.borwseFreeCheck);
				} else {
					setButtonsStatus(this.props, pageConfig.pageStatus.checkDetail.browse);
				}
			},
			error: (res) => {
				if (res && res.message) {
					toast({ content: res.message, color: 'warning' });
				}
			}
		});
	};

	/**
	 * 返回
	 * 		1.盘点明细返回盘点结果
	 * 		2.其他界面返回列表
	 */
	goBack = (current) => {
		if (current == pageConfig.current.checkDetail || current == pageConfig.current.reAssign) {
			/* 将穿透列表界面改回浏览态 */
			this.props.cardTable.setStatus(pageConfig.areas.card.throughArea, pageConfig.ui_status.browse);
			/* 清空穿透列表界面数据 */
			this.props.cardTable.setTableData(pageConfig.areas.card.throughArea, { rows: [] }, null, false);
			this.props.setUrlParam({ status: 'result', category: null });
			executeResult.call(this);
		} else {
			this.props.pushTo('/list', { pagecode: pageConfig.pagecode.list_pagecode });
		}
	};
	//步骤条详细说明
	Ondescription = (current, index) => {
		if (index == current) {
			return getMultiLangByID('452005008A-000038') /* 国际化处理：进行中*/;
		} else if (index < current) {
			return getMultiLangByID('452005008A-000039') /* 国际化处理：已完成*/;
		} else {
			return;
		}
	};

	/**
	 * 第三步柱状图未分配盘点人点击事件
	 */
	onClickLine() {
		this.setState({ current: pageConfig.current.stepTwo.reAssign });
		setButtonsStatus(this.props, pageConfig.pageStatus.stepTwo.reAssign);
	}

	/**
	 * 盘点结果柱状图穿透
	 * 条件：自由态单据
	 */
	onResultClickLine() {
		/* 非自由态，不可穿透 */
		if (
			this.state.extBillCard.head[pageConfig.areas.card.card_head].rows[0].values.bill_status.value !=
			pageConfig.bill_status.free_check
		) {
			showMessage.call(this, this.props, {
				content: getMultiLangByID('452005008A-000040') /* 国际化处理：单据已提交并有后续操作，不允许重新分配*/,
				color: 'warning'
			});
			return;
		}
		this.props.setUrlParam({ status: pageConfig.ui_status.edit });
		executeEdit.call(this);
		/* 和修改不同的是，这里需要设置重新分配区域的值,设置重新分配区域的状态 */
		this.props.editTable.setStatus(pageConfig.areas.card.reAssignArea, pageConfig.ui_status.edit);
		this.setState({
			current: pageConfig.current.reAssign,
			ui_status: pageConfig.ui_status.edit
		});
		setButtonsStatus(this.props, pageConfig.pageStatus.throughReAssign.throughReAssign);
	}

	/**
	 * 处理步骤条current
	 * 当current值不在0——2范围内时，保证步骤条显示正确
	 */
	handleCurrentChange = (current) => {
		if (current == pageConfig.current.stepTwo.reAssign) {
			return pageConfig.current.stepTwo.beforeNext;
		} else {
			return current;
		}
	};

	/**
	 * 头部显示数据控制
	 * 		1.返回按钮
	 * 		2.标题
	 * 		3.盘点单号
	 */
	createCardTitleArea = (current) => {
		let pk_plan = this.props.getUrlParam('pk_plan');
		/**************************************** 
		 *flag:返回按钮是否显示标识
		 *条件：查看盘点结果 或 第三步保存后 或 盘点明细（盘点结果穿透列表）
		 *		并且 非审批小应用 并且 非盘点报告穿透
		 ****************************************/
		let flag =
			(current === pageConfig.current.checkResult /* 查看盘点结果 */ ||
			current === pageConfig.current.stepThree.afterSure /* 第三步保存后 */ ||
				current === pageConfig.current.checkDetail ||
				current == pageConfig.current.reAssign) /* 盘点明细（盘点结果穿透列表） */ &&
			this.props.getUrlParam('scene') != linkQueryConst.SCENETYPE.approvesce /* 非审批小应用 */ &&
			!pk_plan /* 非盘点报告穿透 */;

		/* 单据号 是否显示 */
		let bill_code = null;
		this.state.extBillCard.head &&
			(bill_code = this.state.extBillCard.head[pageConfig.areas.card.card_head].rows[0].values.bill_code.value);
		bill_code = bill_code
			? bill_code
			: this.state.billCard.head
				? this.state.billCard.head[pageConfig.areas.card.card_head].rows[0].values.bill_code.value
				: null;
		let title = getMultiLangByID('452005008A-000033') /* 国际化处理：盘点单 */;
		if (bill_code) {
			title = `${getMultiLangByID('452005008A-000033') /* 国际化处理：盘点单 */} :${bill_code}`;
		}
		return (
			<div className="header-title-search-area">
				{flag && (
					<NCBackBtn
						className="title-search-detail"
						onClick={() => {
							this.goBack.call(this, current);
						}}
					/>
				)}
				<h2 className="title-search-detail">{title}</h2>
			</div>
		);
	};

	render() {
		let { current, checkedValue, data } = this.state;
		let { handleChange } = this;
		let { button, ncmodal, cardPagination } = this.props;
		let { createCardPagination } = cardPagination;
		let { createButtonApp } = button;
		const { ApproveDetail } = high;
		const { createModal } = ncmodal;

		return (
			<div className="nc-bill-card" id="inventory-area">
				<NCAffix>
					<div className="nc-bill-header-area nonprint">
						{/* 标题 title */}
						{this.createCardTitleArea.call(this, current)}
						<div className="header-button-area">
							{createButtonApp({
								//按钮区域（在数据库中注册的按钮区域）
								area: 'card_head',
								//按钮数量限制，超出指定数量的按钮将放入更多显示
								buttonLimit: 3,
								onButtonClick: buttonClick.bind(this),
								//指定下拉弹窗渲染的位置，不设置的话，下拉弹窗随滚动条滚动
								popContainer: document.querySelector('.header-button-area')
							})}
						</div>
						{current === pageConfig.current.checkResult &&
						linkQueryConst.SCENETYPE.approvesce != this.props.getUrlParam('scene') ? (
							<div className="header-cardPagination-area">
								{createCardPagination({
									handlePageInfoChange: pageInfoClick.bind(this)
								})}
							</div>
						) : (
							''
						)}
					</div>
				</NCAffix>
				<div className="nc-bill-form-area">
					{current !== pageConfig.current.checkResult &&
					current !== pageConfig.current.checkDetail &&
					current !== pageConfig.current.reAssign && (
						<div className="step-area">
							<NCSteps current={this.handleCurrentChange(current)}>
								{steps.map((item, index) => (
									<NCStep
										key={item.title}
										title={item.title}
										description={this.Ondescription(this.handleCurrentChange(current), index)}
									/>
								))}
							</NCSteps>
						</div>
					)}
					{current === pageConfig.current.stepOne && (
						<StepOne
							{...this.props}
							{...{ data }}
							rangeAreaData={this.rangeAreaData}
							childformData={this.childformData}
						/>
					)}
					{current === pageConfig.current.stepTwo.beforeNext && (
						<StepTwo {...this.props} {...{ checkedValue }} handleChange={handleChange} state={this.state} />
					)}
					{(current === pageConfig.current.stepThree.boforeSure ||
						current === pageConfig.current.stepThree.afterSure) && (
						<StepThree
							{...this.props}
							echartsData={this.echartsData}
							onClickLine={this.onClickLine}
							state={this.state}
						/>
					)}
					{(current === pageConfig.current.stepTwo.reAssign || current === pageConfig.current.reAssign) && (
						<ReAssign {...this.props} />
					)}
					{current === pageConfig.current.checkResult && (
						<InventoryResult
							{...this.props}
							state={this.state}
							onResultClickLine={this.onResultClickLine}
							childformData={this.childformData}
							onMyChart={(params) => this.onMyChart(params)}
							onLegendClick={this.onLegendClick.bind(this)}
						/>
					)}
					{current === pageConfig.current.checkDetail && <CheckDetail {...this.props} state={this.state} />}
					{createModal(`${pageConfig.pagecode.card_pagecode}-del-confirm`, {
						color: 'warning'
					})}
					{createModal(`${pageConfig.pagecode.card_pagecode}-cancel-confirm`, {
						color: 'warning'
					})}
				</div>
				<ApproveDetail
					show={this.state.showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={this.state.transi_type}
					billid={this.state.pk_inventory}
				/>
				{/* 附件 */}
				{this.props.ncUploader.createNCUploader('warcontract-uploader', {})}
				{/* 提交及指派 */}
				{this.state.compositedisplay ? (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002') /* 国际化处理：指派 */}
						data={this.state.compositedata}
						display={this.state.compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				) : (
					''
				)}
			</div>
		);
	}
}

const MasterChildCardBase = createPage({
	billinfo: {
		// 单据类型
		billtype: 'grid',
		// 节点对应的模板编码
		pagecode: pageConfig.pagecode.card_pagecode,
		// 表体(表格)区域编码
		bodycode: pageConfig.areas.card.throughArea,
		// 表格类型
		tabletype: 'cardTable'
	}
})(Maininfo);

export default MasterChildCardBase;
