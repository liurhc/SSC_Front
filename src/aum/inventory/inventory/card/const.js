import { appConfig } from './../const';

// 页面配置
const pageConfig = {
	...appConfig,
	// 节点名称
	title: '452005008A-000033',
	// 翻译json编码
	translateCode: '452005008A_tanslate',
	// 查询区查询类型
	querytype: 'tree',
	// 单据类型
	billtype: 'extcard',
	noCheckKeys: {
		assignArea: [ 'deptincluedsub', 'categincluedsub' ]
	},
	// 盘点方式
	inventoryWay: {
		USERINV: 1,
		MANAGERINV: 2,
		USEDEPTINV: 3,
		MANDEPTINV: 4,
		EQUIPCATEGORYINV: 5,
		CATEGORY_MANDEPTINV: 6,
		CATEGORY_USEDEPTINV: 7
	},
	// 盘点结果
	check_result: {
		MATCH: 0,
		PROFIT: 1,
		LOSS: 2,
		UNMATCH: 3,
		UNSETTLED: 4
	},
	// 头部按钮显隐性设置
	headBtnsVisible: {
		// 第一步
		stepOne: {
			Prev: false,
			Next: true,
			Sure: false,
			BatchAlter: false,
			Save: false,
			Commit: false,
			UnCommit: false,
			ReAssign: false,
			Attachment: false,
			Delete: false,
			Cancel: true,
			QueryAboutBillFlow: false,
			Print: false,
			Output: false,
			Refresh: false,
			Edit: false,
			AddLine: false,
			AddAssLine: false,
			DelAssLine: false,
			AllMatch: false
		},
		// 第二步
		stepTwo: {
			// 下一步前
			beforeNext: {
				Prev: true,
				Next: true,
				Sure: false,
				BatchAlter: false,
				Save: false,
				Commit: false,
				UnCommit: false,
				ReAssign: false,
				Attachment: false,
				Delete: false,
				Cancel: true,
				QueryAboutBillFlow: false,
				Print: false,
				Output: false,
				Refresh: false,
				Edit: false,
				AddLine: false,
				AddAssLine: true,
				DelAssLine: true,
				AllMatch: false
			},
			// 未分配盘点人
			reAssign: {
				Prev: false,
				Next: false,
				Sure: true,
				BatchAlter: true,
				Save: false,
				Commit: false,
				UnCommit: false,
				ReAssign: true,
				Attachment: false,
				Delete: false,
				Cancel: true,
				QueryAboutBillFlow: false,
				Print: false,
				Output: false,
				Refresh: false,
				Edit: false,
				AddLine: false,
				AddAssLine: false,
				DelAssLine: false,
				AllMatch: false
			}
		},
		// 第三步
		stepThree: {
			// 确认前
			boforeSure: {
				Prev: false,
				Next: false,
				Sure: false,
				BatchAlter: false,
				Save: true,
				Commit: false,
				UnCommit: false,
				ReAssign: false,
				Attachment: false,
				Delete: false,
				Cancel: true,
				QueryAboutBillFlow: false,
				Print: false,
				Output: false,
				Refresh: false,
				Edit: false,
				AddLine: false,
				AddAssLine: false,
				DelAssLine: false,
				AllMatch: false
			},
			// 确认后
			afterSure: {
				Prev: false,
				Next: false,
				Sure: false,
				BatchAlter: false,
				Save: false,
				Commit: false,
				UnCommit: false,
				ReAssign: true,
				Attachment: true,
				Delete: true,
				Cancel: false,
				QueryAboutBillFlow: false,
				Print: false,
				Output: false,
				Refresh: false,
				Edit: false,
				AddLine: false,
				AddAssLine: false,
				DelAssLine: false,
				AllMatch: false
			}
		},
		// 盘点结果
		checkResult: {
			/** 空页面 */
			blank: {
				Prev: false,
				Next: false,
				Sure: false,
				BatchAlter: false,
				Save: false,
				Commit: false,
				UnCommit: false,
				ReAssign: false,
				Attachment: false,
				Delete: false,
				Cancel: false,
				QueryAboutBillFlow: false,
				Print: false,
				Output: false,
				Refresh: false,
				Edit: false,
				AddLine: false,
				AddAssLine: false,
				DelAssLine: false,
				AllMatch: false
			},
			/** 自由态_0 */
			free_check: {
				Prev: false,
				Next: false,
				Sure: false,
				BatchAlter: false,
				Save: false,
				Commit: true,
				UnCommit: false,
				ReAssign: false,
				Attachment: true,
				Delete: false,
				Cancel: false,
				QueryAboutBillFlow: true,
				Print: true,
				Output: true,
				Refresh: true,
				Edit: false,
				AddLine: false,
				AddAssLine: false,
				DelAssLine: false,
				AllMatch: false
			},
			/** 未审核_1 */
			un_check: {
				Prev: false,
				Next: false,
				Sure: false,
				BatchAlter: false,
				Save: false,
				Commit: false,
				UnCommit: true,
				ReAssign: false,
				Attachment: true,
				Delete: false,
				Cancel: false,
				QueryAboutBillFlow: true,
				Print: true,
				Output: true,
				Refresh: true,
				Edit: false,
				AddLine: false,
				AddAssLine: false,
				DelAssLine: false,
				AllMatch: false
			},
			/** 审核中_2 */
			check_going: {
				Prev: false,
				Next: false,
				Sure: false,
				BatchAlter: false,
				Save: false,
				Commit: false,
				UnCommit: false,
				ReAssign: false,
				Attachment: true,
				Delete: false,
				Cancel: false,
				QueryAboutBillFlow: true,
				Print: true,
				Output: true,
				Refresh: true,
				Edit: false,
				AddLine: false,
				AddAssLine: false,
				DelAssLine: false,
				AllMatch: false
			},
			/** 审核通过_3 */
			check_pass: {
				Prev: false,
				Next: false,
				Sure: false,
				BatchAlter: false,
				Save: false,
				Commit: false,
				UnCommit: true,
				ReAssign: false,
				Attachment: true,
				Delete: false,
				Cancel: false,
				QueryAboutBillFlow: true,
				Print: true,
				Output: true,
				Refresh: true,
				Edit: false,
				AddLine: false,
				AddAssLine: false,
				DelAssLine: false,
				AllMatch: false
			} /** 审核不通过_3 */,
			check_nopass: {
				Prev: false,
				Next: false,
				Sure: false,
				BatchAlter: false,
				Save: false,
				Commit: false,
				UnCommit: false,
				ReAssign: false,
				Attachment: true,
				Delete: true,
				Cancel: false,
				QueryAboutBillFlow: true,
				Print: true,
				Output: true,
				Refresh: true,
				Edit: false,
				AddLine: false,
				AddAssLine: false,
				DelAssLine: false,
				AllMatch: false
			}
		},
		// 穿透列表
		checkDetail: {
			borwseFreeCheck: {
				Prev: false,
				Next: false,
				Sure: false,
				BatchAlter: false,
				Save: false,
				Commit: false,
				UnCommit: false,
				ReAssign: false,
				Attachment: false,
				Delete: false,
				Cancel: false,
				QueryAboutBillFlow: false,
				Print: true,
				Output: true,
				Refresh: true,
				Edit: true,
				AddLine: false,
				AddAssLine: false,
				DelAssLine: false,
				AllMatch: false
			},
			editFreeCheck: {
				Prev: false,
				Next: false,
				Sure: false,
				BatchAlter: false,
				Save: true,
				Commit: false,
				UnCommit: false,
				ReAssign: false,
				Attachment: false,
				Delete: false,
				Cancel: true,
				QueryAboutBillFlow: false,
				Print: false,
				Output: false,
				Refresh: false,
				Edit: false,
				AddLine: true,
				AddAssLine: false,
				DelAssLine: false,
				AllMatch: true
			},
			browse: {
				Prev: false,
				Next: false,
				Sure: false,
				BatchAlter: false,
				Save: false,
				Commit: false,
				UnCommit: false,
				ReAssign: false,
				Attachment: false,
				Delete: false,
				Cancel: false,
				QueryAboutBillFlow: false,
				Print: true,
				Output: true,
				Refresh: false,
				Edit: false,
				AddLine: false,
				AddAssLine: false,
				DelAssLine: false,
				AllMatch: false
			}
		},
		reAssign: {
			Prev: false,
			Next: false,
			Sure: true,
			BatchAlter: true,
			Save: false,
			Commit: false,
			UnCommit: false,
			ReAssign: false,
			Attachment: false,
			Delete: false,
			Cancel: false,
			QueryAboutBillFlow: false,
			Print: false,
			Output: false,
			Refresh: false,
			Edit: false,
			AddLine: false,
			AddAssLine: false,
			DelAssLine: false,
			AllMatch: false
		}
	},
	// 头部按钮主次关系
	headBtnsMain: {
		// 第一步
		stepOne: {
			Next: true,
			Prev: false,
			Sure: false,
			Delete: false,
			ReAssign: false
		},
		// 第二步
		stepTwo: {
			// 下一步前
			beforeNext: {
				Next: true,
				Prev: false,
				Sure: false,
				Delete: false,
				ReAssign: false
			},
			// 未分配盘点人
			reAssign: {
				Next: false,
				Prev: false,
				Sure: true,
				Delete: false,
				ReAssign: false
			}
		},
		// 第三步
		stepThree: {
			// 确认前
			boforeSure: {
				Next: false,
				Prev: false,
				Sure: true,
				Delete: false,
				ReAssign: false
			},
			// 确认后
			afterSure: {
				Next: false,
				Prev: false,
				Sure: false,
				Delete: false,
				ReAssign: false
			}
		}
	},
	throughInnerBtnsVisible: {
		borwseFreeCheck: [ 'Details' ],
		editFreeCheck: {
			// 相符_0
			match: [ 'OpenCard' ],
			// 盘盈_1
			profit: [ 'DeleteThrouthLine', 'OpenCard' ],
			// 盘亏_2
			loss: [ 'CancelLoss', 'OpenCard' ],
			// 不符_3
			unMatch: [ 'OpenCard' ],
			// 未处理_4
			unSettled: [ 'OpenCard' ]
		},
		browse: [ 'Details' ]
	},
	// 编辑态表格中按钮显隐性
	innerBrowseBtnsVisible: {
		// 相符_0
		match: [ 'Edit' ],
		// 盘盈_1
		profit: [ 'Delete', 'Edit' ],
		// 盘亏_2
		loss: [ 'CancelLoss' ],
		// 不符_3
		unMatch: [ 'Edit' ],
		// 未处理_4
		unSettled: [ 'Edit' ]
	},
	// 浏览态表格中按钮显隐性
	innerBrowseBtnsVisible: {
		Detail: true,
		Edit: false,
		Delete: false,
		CancelLoss: false
	},
	// 页面状态标识
	pageStatus: {
		// 第一步
		stepOne: {
			stepOne: 'stepOne'
		},
		// 第二步
		stepTwo: {
			// 下一步前
			beforeNext: 'beforeNext',
			// 未分配盘点人
			reAssign: 'reAssign'
		},
		// 第三步
		stepThree: {
			// 确认前
			boforeSure: 'boforeSure',
			// 确认后
			afterSure: 'afterSure'
		},
		// 盘点结果
		checkResult: {
			/** 空页面 */
			blank: 'blank',
			/** 自由态_0 */
			free_check: 'free_check',
			/** 未审核_1 */
			un_check: 'un_check',
			/** 审核中_2 */
			check_going: 'check_going',
			/** 审核通过_3 */
			check_pass: 'check_pass',
			/** 审核不通过_3 */
			check_nopass: 'check_nopass'
		},
		// 穿透列表
		checkDetail: {
			// 单据自由态的浏览态
			borwseFreeCheck: 'borwseFreeCheck',
			// 单据自由态的修改态
			editFreeCheck: 'editFreeCheck',
			// 所有提交后单据的浏览态
			browse: 'browse'
		},
		throughReAssign: {
			throughReAssign: 'throughReAssign'
		}
	},
	current: {
		// 第一步
		stepOne: 0,
		// 第二步
		stepTwo: {
			// 下一步前
			beforeNext: 1,
			// 未分配盘点人
			reAssign: 5
		},
		// 第三步
		stepThree: {
			// 确认前
			boforeSure: 2,
			// 确认后
			afterSure: 3
		},
		// 盘点结果
		checkResult: 10,
		// 穿透列表
		checkDetail: 11,
		// 穿透重新分配列表
		reAssign: 12
	},
	// 按钮名称常量
	buttonName: {
		NEXT: 'Next',
		PREV: 'Prev',
		SURE: 'Sure',
		DELETE: 'Delete',
		REASSIGN: 'ReAssign',
		COMMIT: 'Commit',
		UNCOMMIT: 'UnCommit',
		QUERYABOUTBUSINESS: 'QueryAboutBusiness',
		CANCEL: 'Cancel',
		SAVE: 'Save',
		EDIT: 'Edit',
		ADDLINE: 'AddLine',
		PRINT: 'Print',
		OUTPUT: 'Output',
		BATCHALTER: 'BatchAlter',
		DELASSLINE: 'DelAssLine'
	},
	// 设置按钮可用性（第二步中删行）
	buttonDisabled: {
		enable: {
			DelAssLine: false
		},
		unable: {
			DelAssLine: true
		}
	}
};

// 按钮名称常量
const buttonName = {
	NEXT: 'Next',
	PREV: 'Prev',
	SURE: 'Sure',
	DELETE: 'Delete',
	REASSIGN: 'ReAssign',
	COMMIT: 'Commit',
	UNCOMMIT: 'UnCommit',
	CANCEL: 'Cancel',
	SAVE: 'Save',
	EDIT: 'Edit',
	ADDLINE: 'AddLine',
	PRINT: 'Print',
	OUTPUT: 'Output',
	BATCHALTER: 'BatchAlter',
	REFRESH: 'Refresh',
	QUERYABOUTBILLFLOW: 'QueryAboutBillFlow',
	ATTACHMENT: 'Attachment',
	AllMatch: 'AllMatch'
};

// 盘点字段
const invItems = {
	INVITEMS_BEFORE: [
		{
			value: 'pk_usedunit_before_v'
		},
		{
			value: 'pk_usedept_before_v'
		},
		{
			value: 'pk_mandept_before_v'
		},
		{
			value: 'pk_assetuser_before'
		},
		{
			value: 'pk_usingstatus_before'
		},
		{
			value: 'pk_position_before'
		}
	],
	INVITEMS_AFTER: [
		{
			index: 0,
			value: 'pk_usedunit_after'
		},
		{
			index: 0,
			value: 'pk_usedept_after'
		},
		{
			index: 0,
			value: 'pk_mandept_after'
		},
		{
			index: 0,
			value: 'pk_assetuser_after'
		},
		{
			index: 0,
			value: 'pk_usingstatus_after'
		},
		{
			index: 0,
			value: 'pk_position_after'
		}
	],
	// 不要破坏这些字段的前后一一对应关系
	INVITEMS_BEFORE_ALL: [
		'pk_usingstatus_before',
		'pk_mandept_before_v',
		'pk_mandept_before',
		'pk_assetuser_before',
		'pk_usedunit_before',
		'pk_usedunit_before_v',
		'pk_usedept_before',
		'pk_usedept_before_v',
		'pk_position_before',
		'pk_manager_before'
	],
	INVITEMS_AFTER_ALL: [
		'pk_usingstatus_after',
		'pk_mandept_after_v',
		'pk_mandept_after',
		'pk_assetuser_after',
		'pk_usedunit_after',
		'pk_usedunit_after_v',
		'pk_usedept_after',
		'pk_usedept_after_v',
		'pk_position_after',
		'pk_manager_after'
	]
};

export { pageConfig, buttonName, invItems };
