import ampub from 'ampub';
const { commonConst } = ampub;
const { StatusUtils } = commonConst;
const { UISTATE, BILLSTATUS } = StatusUtils;
// 应用级配置
const appConfig = {
	appcode: '452005008A',
	// 资产盘点单据类型
	bill_type: '4A35',
	// 交易类型
	transi_type: '4A35-01',
	// 原nc节点编码
	node_code: '4520036005',
	// 数据源
	dataSource: 'aum.inventory.inventory.main',
	// 页面编码
	pagecode: {
		// 列表页面编码
		list_pagecode: '452005008A_list',
		// 卡片页面编码
		card_pagecode: '452005008A_card'
	},
	// 打印模板节点标识
	printNodekey: null,
	// 页面状态常量
	ui_status: {
		...UISTATE,
		result: 'result',
		detail: 'detail'
	},
	bill_status: {
		...BILLSTATUS
	},
	// VO字段名称
	vo_name: {
		dept_name: 'dept_name',
		category_name: 'category_name',
		equip_name: 'equip_name',
		check_result: 'check_result',
		deptincluedsub: 'deptincluedsub',
		categincluedsub: 'categincluedsub',
		dept_pk: 'dept_pk',
		category_pk: 'category_pk',
		pk_dept: 'pk_dept',
		pk_category: 'pk_category',
		pk_org: 'pk_org',
		issettled: 'issettled',
		equip_code: 'equip_code',
		// after为盘点后字段
		pk_usingstatus_after: 'pk_usingstatus_after',
		pk_assetuser_after: 'pk_assetuser_after',
		pk_usedunit_after: 'pk_usedunit_after',
		pk_usedept_after: 'pk_usedept_after',
		pk_mandept_after: 'pk_mandept_after',
		pk_position_after: 'pk_position_after',
		planedcheckusername: 'planedcheckusername',
		bar_code: 'bar_code',
		pk_inventory: 'pk_inventory',
		ischecked: 'ischecked',
		pk_equip: 'pk_equip'
	},
	url: {
		printUrl: '/nccloud/aum/inventory/printCard.do',
		commitUrl: '/nccloud/aum/inventory/commit.do',
		deleteUrl: '/nccloud/aum/inventory/delete.do',
		queryPageUrl: '/nccloud/aum/inventory/queryPage.do',
		searchQueryUrl: '/nccloud/aum/inventory/query.do'
	},
	areas: {
		list: {
			searchId: 'searchArea',
			tableId: 'list_head'
		},
		card: {
			orgArea: 'orgArea',
			rangeArea: 'searchArea',
			dateArea: 'dateArea',
			assignArea: 'assignArea',
			reAssignArea: 'reAssignArea',
			throughArea: 'throughArea',
			childThroughArea: 'childThroughArea',
			card_head: 'card_head',
			bodyvos_edit: 'bodyvos_edit'
		}
	}
};

export { appConfig };
