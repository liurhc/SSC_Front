import { RenderRouter } from 'nc-lightapp-front';
import routes from './router';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { initMultiLangByModule } = multiLangUtils;

(function main(routers, htmlTagid) {
	let moduleIds = { aum: [ '452005008A' ], ampub: [ 'common' ] };
	initMultiLangByModule(moduleIds, () => {
		RenderRouter(routers, htmlTagid);
	});
})(routes, 'app');
