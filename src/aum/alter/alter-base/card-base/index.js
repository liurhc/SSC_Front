import React, { Component } from 'react';
import { base, high } from 'nc-lightapp-front';
import ampub from 'ampub';

const { components, utils, commonConst } = ampub;

const { ApprovalTrans } = components;

const { multiLangUtils, cardUtils, closeBrowserUtils } = utils;

const { getMultiLangByID } = multiLangUtils;
const { createCardTitleArea, createCardPaginationArea, getCommonTableHeadLeft } = cardUtils;

const { StatusUtils } = commonConst;

const { UISTATE } = StatusUtils;

import {
	buttonClick,
	initTemplate,
	headAfterEvent,
	bodyAfterEvent,
	pageInfoClick,
	save,
	backToList,
	rowSelected,
	commit,
	saveCommit
} from './events';
import { pageConfig } from './const';

const { NCAffix } = base;
const { BillTrack, ApproveDetail } = high;

/**
 * 主子表卡片页面入口文件
 */
class MasterChildCardBase extends Component {
	constructor(props) {
		super(props);
		this.state = {
			pk_bill: '',
			transi_type: '',
			showApprove: false,
			showBillTrack: false,
			compositedata: {},
			compositedisplay: false
		};
		// 变动项目字段是否显示
		this.isAlterInfoShow = false;
		this.alterItems = [];
		const { pageConfig = {} } = props;
		const { formId, tableId } = pageConfig;
		closeBrowserUtils.call(this, props, { form: [ formId ], cardTable: [ tableId ] });
		initTemplate.call(this, props);
	}

	// 提交及指派回调
	getAssginUsedr = (content) => {
		const { pageConfig = {} } = this.props;
		const { formId } = pageConfig;
		let status = this.props.form.getFormStatus(formId) || UISTATE.browse;
		if (status == UISTATE.browse) {
			commit.call(this, this.props, content);
		} else {
			saveCommit.call(this, this.props, content);
		}
	};

	// 取消指派
	turnOff = () => {
		this.setState({
			compositedisplay: false
		});
	};

	// 注册列表肩部合计行
	getTableHeadLeft = () => {
		return getCommonTableHeadLeft.call(this, this.props);
	};

	// 获取列表肩部信息
	getTableHead = () => {
		const { button, pageConfig = {} } = this.props;
		const { createButtonApp } = button;
		const { tableId } = pageConfig;
		return (
			<div className="shoulder-definition-area">
				<div className="definition-icons">
					{createButtonApp({
						area: 'card_body',
						buttonLimit: 3,
						onButtonClick: (props, id) => {
							buttonClick.call(this, props, id, tableId);
						},
						popContainer: document.querySelector('.header-button-area')
					})}
				</div>
			</div>
		);
	};

	render() {
		const { cardTable, form, button, ncUploader, pageConfig = {} } = this.props;
		const { createCardTable } = cardTable;
		const { createForm } = form;
		const { createButtonApp } = button;
		const { createNCUploader } = ncUploader;
		const { title, formId, tableId, pagecode, bill_type, dataSource } = pageConfig;

		let { pk_bill, transi_type, showApprove, showBillTrack, compositedata, compositedisplay } = this.state;

		return (
			<div className="nc-bill-card">
				<div className="nc-bill-top-area">
					<NCAffix>
						{/* 头部 header */}
						<div className="nc-bill-header-area">
							{/* 标题 title */}
							{createCardTitleArea.call(this, this.props, {
								title: getMultiLangByID(title) /* 国际化处理： 资产变动*/,
								formId,
								backBtnClick: backToList
							})}
							{/* 按钮区 btn-area */}
							<div className="header-button-area">
								{createButtonApp({
									area: 'card_head',
									buttonLimit: 3,
									onButtonClick: (props, id) => {
										buttonClick.call(this, props, id, formId);
									},
									popContainer: document.querySelector('.header-button-area')
								})}
							</div>
							{createCardPaginationArea.call(this, this.props, {
								formId,
								dataSource,
								pageInfoClick
							})}
						</div>
					</NCAffix>
					{/* 表单区 form-area */}
					<div className="nc-bill-form-area">
						{createForm(formId, {
							onAfterEvent: headAfterEvent.bind(this)
						})}
					</div>
				</div>
				<div className="nc-bill-bottom-area">
					{/* 列表区 table-area */}
					<div className="nc-bill-table-area">
						{createCardTable(tableId, {
							tableHeadLeft: this.getTableHeadLeft.bind(this),
							tableHead: this.getTableHead.bind(this),
							modelSave: save.bind(this),
							onAfterEvent: bodyAfterEvent.bind(this),
							showCheck: true,
							showIndex: true,
							selectedChange: rowSelected.bind(this)
						})}
					</div>
				</div>
				{/* 单据追溯框 */}
				<BillTrack
					show={showBillTrack}
					close={() => {
						this.setState({
							showBillTrack: false
						});
					}}
					pk={pk_bill}
					type={bill_type}
				/>
				{/* 审批详情 */}
				<ApproveDetail
					show={showApprove}
					close={() => {
						this.setState({ showApprove: false });
					}}
					billtype={transi_type}
					billid={pk_bill}
				/>
				{/* 提交及指派 */}
				{compositedisplay && (
					<ApprovalTrans
						title={getMultiLangByID('amcommon-000002') /* 国际化处理： 指派*/}
						data={compositedata}
						display={compositedisplay}
						getResult={this.getAssginUsedr}
						cancel={this.turnOff}
					/>
				)}
				{/* 附件管理框 */}
				{createNCUploader(`${pagecode}-uploader`, {})}
			</div>
		);
	}
}

export default MasterChildCardBase;

export const baseConfig = pageConfig;
