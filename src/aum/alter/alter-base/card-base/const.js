// 页面配置
export const pageConfig = {
	// 应用主键
	appid: '0001Z9100000000012LQ',
	// 应用编码
	appcode: '452002536A',
	// 应用名称
	title: '452002536A-000006' /* 国际化处理： 资产变动*/,
	// 表单区域id
	formId: 'card_head',
	// 表格区域id
	tableId: 'bodyvos',
	// 页面编码
	pagecode: '452002536A_card',
	// 编辑态按钮
	editBtns: [ 'Save', 'SaveCommit', 'Cancel', 'OpenCard', 'AddLine', 'DelLine', 'BatchAlter' ],
	// 浏览态按钮
	browseBtns: [
		'Add',
		'Edit',
		'Delete',
		'Commit',
		'UnCommit',
		'Attachment',
		'OpenCard',
		'QueryAboutBillFlow',
		'ReceiptShow',
		'ReceiptScan',
		'Print',
		'Output',
		'Refresh'
	],
	// 主键字段
	pkField: 'pk_alter',
	// 单据类型
	bill_type: '4A07',
	// 请求链接
	url: {
		transferEquipUrl: '/nccloud/aum/alter/transferEquip.do',
		queryCardUrl: '/nccloud/aum/alter/queryCard.do',
		insertUrl: '/nccloud/aum/alter/insert.do',
		updateUrl: '/nccloud/aum/alter/update.do',
		editUrl: '/nccloud/aum/alter/edit.do',
		printUrl: '/nccloud/aum/alter/printCard.do',
		commitUrl: '/nccloud/aum/alter/commit.do',
		deleteUrl: '/nccloud/aum/alter/commit.do',
		headAfterEditUrl: '/nccloud/aum/alter/headAfterEdit.do',
		bodyAfterEditUrl: '/nccloud/aum/alter/bodyAfterEdit.do',
		batchAlterUrl: '/nccloud/aum/alter/batchAlter.do'
	},
	// 输出文件名称
	printFilename: '452002536A-000006' /* 国际化处理： 资产变动*/,
	// 打印模板节点标识
	printNodekey: null,
	listRouter: '/list',
	// 权限资源编码
	resourceCode: '4520024005',
	dataSource: 'aum.alter.assetalter.main'
};
