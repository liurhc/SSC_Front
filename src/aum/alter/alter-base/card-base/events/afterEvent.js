import { ajax } from 'nc-lightapp-front';
import ampub from 'ampub';

const { components, utils } = ampub;

const { OrgChangeEvent } = components;

const { orgChangeEvent } = OrgChangeEvent;

const { cardUtils } = utils;

const { commonHeadAfterEvent, commonBodyAfterEvent, updateHeadData, updateBodyData } = cardUtils;

import { setBtnsEnable, addLine, setAlterItemVisiable } from './buttonClick';

/**
 * 表头编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} oldValue 
 * @param {*} val 
 */
export function headAfterEvent(props, moduleId, key, value, oldValue, val) {
	const { pageConfig = {} } = props;
	const { formId, tableId, pagecode, url } = pageConfig;
	if (key == 'pk_org_v') {
		// 组织切换确定后发送后台请求
		let callback = () => {
			if (value.value) {
				// 若组织切换后有值则自动增行
				addLine.call(this, props);
			}
			setBtnsEnable.call(this, props, tableId);
			if (value.value) {
				let config = {
					afterEditUrl: url.headAfterEditUrl,
					pagecode,
					key,
					value,
					oldValue
				};
				commonHeadAfterEvent.call(this, props, config);
			}
		};
		// 组织切换
		orgChangeEvent.call(
			this,
			props,
			pagecode,
			formId,
			tableId,
			key,
			value,
			oldValue,
			[ 'pk_recorder' ],
			false,
			callback
		);
	} else if (key == 'alterinfo') {
		let newAlterinfo = value || {};
		let flag = false;
		let newkeys = [];
		if (newAlterinfo && newAlterinfo.value) {
			newkeys = newAlterinfo.value.split(',');
			// 是否变动使用权，变动使用权需要同步变动使用部门和责任人
			if (newkeys.includes('pk_usedunit_v')) {
				if (!newkeys.includes('pk_usedept_v')) {
					newkeys.push('pk_usedept_v');
					flag = true;
				}
				if (!newkeys.includes('pk_user')) {
					newkeys.push('pk_user');
					flag = true;
				}
			}
			this.alterItems = newkeys;
		} else {
			this.alterItems = [];
		}
		if (flag) {
			newAlterinfo = { value: newkeys.join(',') };
			props.form.setFormItemsValue(formId, {
				alterinfo: newAlterinfo
			});
		}
		let oldAlterinfo = oldValue || {};

		// 变动项目显示并且前后值不一致时候设置表体变动前后项目显示与否
		if (this.isAlterInfoShow && oldAlterinfo.value != newAlterinfo.value) {
			setAlterItemVisiable.call(this, props);
		}
		// 重新获取单据数据
		let data = props.createHeadAfterEventData(pagecode, formId, tableId, moduleId, key, value);
		// 平台参照value前后一致不刷新display问题
		if (flag) {
			data.newvalue = newAlterinfo;
			props.form.setFormItemsValue(formId, {
				alterinfo: oldAlterinfo
			});
		}
		ajax({
			url: url.headAfterEditUrl,
			data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					// 性能优化，关闭表单和表格渲染
					props.beforeUpdatePage();
					updateHeadData.call(this, props, { data });
					updateBodyData.call(this, props, { data, index: 0 });
					// 性能优化，表单和表格统一渲染
					props.updatePage(formId, tableId);
				}
			}
		});
	} else {
		let config = {
			afterEditUrl: url.headAfterEditUrl,
			pagecode,
			key,
			value,
			oldValue,
			keys: [ 'pk_fiorg_v' ]
		};
		commonHeadAfterEvent.call(this, props, config);
	}
}

/**
 * 表体编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedRows 
 * @param {*} index 
 * @param {*} record 
 * @param {*} type 
 * @param {*} eventType 
 */
export function bodyAfterEvent(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	const { pageConfig = {} } = props;
	const { tableId, pagecode, url } = pageConfig;

	let srctype = props.getUrlParam('srctype');
	// 平台的自动增行不会附默认值，这里调用自己的增行,转单不自动增行
	if (srctype != 'transfer') {
		let num = props.cardTable.getNumberOfRows(tableId);
		if (num == index + 1) {
			addLine.call(this, props);
		}
	}

	let config = {
		afterEditUrl: url.bodyAfterEditUrl,
		pagecode,
		key,
		record,
		changedRows,
		index,
		keys: [ 'pk_equip', 'pk_usedunit_v_after' ]
	};
	commonBodyAfterEvent.call(this, props, config);
}
