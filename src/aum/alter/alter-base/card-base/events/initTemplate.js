import { ajax } from 'nc-lightapp-front';
import ampub from 'ampub';

const { components, utils, commonConst } = ampub;

const { queryAboutUtils, refInit, LoginContext } = components;

const { openEquipCardByPk } = queryAboutUtils;
const { defRefCondition } = refInit;
const { loginContext, getContext, loginContextKeys } = LoginContext;

const { cardUtils } = utils;

const { createOprationColumn, afterModifyCardMeta } = cardUtils;

const { StatusUtils, CommonKeys } = commonConst;

const { UISTATE } = StatusUtils;
const { IBusiRoleConst } = CommonKeys;

import tableButtonClick from './tableButtonClick';
import {
	add,
	edit,
	setStatus,
	setValue,
	loadDataByPk,
	setBtnsVisible,
	setBtnsEnable,
	setAlterItemVisiable
} from './buttonClick';

/**
 * 初始化模板
 * @param {*} props 
 */
export default function(props) {
	const { pageConfig = {} } = props;
	const { pagecode } = pageConfig;
	props.createUIDom(
		{
			// 页面编码
			pagecode
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta, () => {
						afterInit.call(this, props);
					});
				}
			}
		}
	);
}

/**
 * 模板初始化以后
 * @param {*} props 
 */
function afterInit(props) {
	const { pageConfig = {} } = props;
	const { pagecode, formId, tableId, url } = pageConfig;
	let status = props.getUrlParam('status') || UISTATE.browse;
	let srctype = props.getUrlParam('srctype');
	if (status == 'add') {
		if (srctype == 'transfer') {
			// 设置新增态
			setStatus.call(this, props, UISTATE.add);
			// 设置按钮显隐性
			setBtnsVisible.call(this, props);
			let pks = [];
			let pk = props.getUrlParam('equipid');
			let ts = props.getUrlParam('equipts');
			if (pk) {
				pks.push({ head: { pk, ts } });
			}
			if (pks && pks.length > 0) {
				const transi_type = getContext(loginContextKeys.transtype);
				let userdefObj = {
					alterinfo: this.alterItems.join(',')
				};
				ajax({
					url: url.transferEquipUrl,
					data: {
						pageid: pagecode,
						pks,
						nexttranstype: transi_type,
						userdefObj
					},
					success: (res) => {
						let { success, data } = res;
						if (success) {
							// 性能优化，关闭表单和表格渲染
							props.beforeUpdatePage();
							setValue.call(this, props, data);
							// 设置组织不可编辑
							props.form.setFormItemsDisabled(formId, {
								pk_org_v: true,
								pk_org: true
							});
							setBtnsEnable.call(this, props, tableId);
							// 性能优化，表单和表格统一渲染
							props.updatePage(formId, tableId);
						}
					}
				});
			}
		} else {
			add.call(this, props);
		}
	} else if (status == UISTATE.edit) {
		setStatus.call(this, props, UISTATE.browse);
		let pk = props.getUrlParam('id');
		if (pk && pk != 'undefined' && pk != 'null') {
			let oldAlterinfo = props.form.getFormItemsValue(formId, 'alterinfo') || {};
			props.button.setButtonVisible('Add', false);
			let callback = (data) => {
				setValue.call(this, props, data);

				let newAlterinfo = props.form.getFormItemsValue(formId, 'alterinfo') || {};
				// 变动项目显示并且前后值不一致时候设置表体变动前后项目显示与否
				if (this.isAlterInfoShow && oldAlterinfo.value != newAlterinfo.value) {
					setAlterItemVisiable.call(this, props);
				}
				edit.call(this, props, false);
			};
			loadDataByPk.call(this, props, pk, callback);
		} else {
			// 设置按钮显隐性
			setBtnsVisible.call(this, props);
		}
	} else {
		setStatus.call(this, props, status);
		let pk = props.getUrlParam('id');
		if (pk && pk != 'undefined' && pk != 'null') {
			let oldAlterinfo = props.form.getFormItemsValue(formId, 'alterinfo') || {};
			let callback = (data) => {
				setValue.call(this, props, data);

				let newAlterinfo = props.form.getFormItemsValue(formId, 'alterinfo') || {};
				// 变动项目显示并且前后值不一致时候设置表体变动前后项目显示与否
				if (this.isAlterInfoShow && oldAlterinfo.value != newAlterinfo.value) {
					setAlterItemVisiable.call(this, props);
				}

				// 设置按钮显隐性
				setBtnsVisible.call(this, props);
			};
			loadDataByPk.call(this, props, pk, callback);
		} else {
			// 设置按钮显隐性
			setBtnsVisible.call(this, props);
		}
	}
}

/**
 * 修改模板
 * @param {*} props 
 * @param {*} meta 
 */
function modifierMeta(props, meta) {
	const pk_group = getContext(loginContextKeys.groupId);
	const transi_type = getContext(loginContextKeys.transtype);
	const { pageConfig = {} } = props;
	const { formId, tableId, bill_type } = pageConfig;
	// 表头参照过滤
	if (meta[formId] && meta[formId].items && meta[formId].items.length > 0) {
		meta[formId].items.map((item) => {
			// 卡片界面组织字段默认不让编辑，新增才让编辑
			if (item.attrcode == 'pk_org_v') {
				item.queryCondition = () => {
					return { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgVidRefFilter' };
				};
			} else if (item.attrcode == 'pk_recorder') {
				// 经办人根据pk_org和设置了权限的业务单元过滤
				item.queryCondition = () => {
					let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
					return {
						pk_org,
						busifuncode: IBusiRoleConst.ASSETORG
					};
				};
			} else if (item.attrcode == 'alterinfo') {
				// 变动项目多选
				item.isMultiSelectedEnabled = true;
				if (item.visible) {
					this.isAlterInfoShow = true;
				}
			} else {
				// 自定义项的过滤
				defRefCondition.call(this, props, item, formId, pk_group);
			}
		});
	}
	if (this.isAlterInfoShow) {
		// 处理侧拉框
		const editId = tableId + '_edit';
		if (meta[editId] && meta[editId].items && meta[editId].items.length > 0) {
			meta[editId].items.map((item) => {
				if (item.attrcode.endsWith('_before') || item.attrcode.endsWith('_after')) {
					item.visible = false;
				}
			});
		}
		// 处理向下展开框
		const browseId = tableId + '_browse';
		if (meta[browseId] && meta[browseId].items && meta[browseId].items.length > 0) {
			meta[browseId].items.map((item) => {
				if (item.attrcode.endsWith('_before') || item.attrcode.endsWith('_after')) {
					item.visible = false;
				}
			});
		}
	}
	// 表体参照过滤
	if (meta[tableId] && meta[tableId].items && meta[tableId].items.length > 0) {
		this.alterItems = [];
		// 资产变动，初始需要把所有变动字段隐藏，用户手动选择
		if (this.isAlterInfoShow) {
			meta[tableId].items.map((item) => {
				if (item.attrcode.endsWith('_before') || item.attrcode.endsWith('_after')) {
					item.visible = false;
				}
			});
		} else {
			meta[tableId].items.map((item) => {
				if (item.visible && item.attrcode.endsWith('_after')) {
					this.alterItems.push(item.attrcode.substring(0, item.attrcode.length - 6));
				}
			});
		}
		meta[tableId].items.map((item) => {
			if (item.attrcode == 'pk_equip') {
				// 资产编码
				item.isMultiSelectedEnabled = true;
				//给资产编码列添加超链接
				item.renderStatus = UISTATE.browse;
				item.render = (text, record, index) => {
					return (
						<div class="card-table-browse">
							<span
								className="code-detail-link"
								onClick={() => {
									openEquipCardByPk.call(this, props, record.values[item.attrcode].value);
								}}
							>
								{record.values[item.attrcode] && record.values[item.attrcode].display}
							</span>
						</div>
					);
				};
				//设置设备的查询条件
				item.queryCondition = () => {
					let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
					let alterinfo = props.form.getFormItemsValue(formId, 'alterinfo').value;
					return {
						pk_org,
						transi_type,
						bill_type,
						alterinfo,
						GridRefActionExt: 'nccloud.web.aum.alter.assetalter.refcondition.PK_EQUIPSqlBuilder'
					};
				};
			} else if (item.attrcode == 'fk_parent_after') {
				// 变动后父资产根据当前资产卡片的使用管理组织进行过滤
				item.queryCondition = () => {
					// let rowData = props.cardTable.getClickRowIndex(tableId);
					let index = props.cardTable.getCurrentIndex(tableId);
					let rowData = props.cardTable.getDataByIndex(tableId, index);
					let pk_equip = rowData.values['pk_equip'].value;
					return {
						pk_equip,
						GridRefActionExt: 'nccloud.web.aum.alter.assetalter.refcondition.FK_PARENTSqlBuilder'
					};
				};
			} else if (item.attrcode == 'pk_usedunit_v_after') {
				// 变动后使用权
				item.queryCondition = () => {
					let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
					// let rowData = props.cardTable.getClickRowIndex(tableId);
					let index = props.cardTable.getCurrentIndex(tableId);
					let rowData = props.cardTable.getDataByIndex(tableId, index);
					let pk_equip = '';
					if (rowData && rowData.values) {
						pk_equip = rowData.values['pk_equip'].value;
					}

					return {
						pk_org,
						pk_equip,
						TreeRefActionExt: 'nccloud.web.aum.alter.assetalter.refcondition.PK_USEDUNIT_V_AFTERSqlBuilder'
					};
				};
			} else if (item.attrcode == 'pk_usedept_v_after') {
				// 变动后使用部门
				item.queryCondition = () => {
					let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
					// let rowData = props.cardTable.getClickRowIndex(tableId);
					let index = props.cardTable.getCurrentIndex(tableId);
					let rowData = props.cardTable.getDataByIndex(tableId, index);
					let pk_equip = '';
					let pk_usedunit_after = '';
					if (rowData && rowData.values) {
						pk_equip = rowData.values['pk_equip'].value;
						pk_usedunit_after = rowData.values['pk_usedunit_after'].value;
					}

					// 使用部门根据使用权过滤
					return {
						head_pk_org: pk_org,
						pk_org: pk_usedunit_after,
						busifuncode: IBusiRoleConst.ASSETORG,
						isTreelazyLoad: false,
						pk_usedunit_after,
						pk_equip,
						TreeRefActionExt: 'nccloud.web.aum.alter.assetalter.refcondition.PK_USEDEPT_V_AFTERSqlBuilder'
					};
				};
			} else if (item.attrcode == 'pk_user_after') {
				// 变动后使用人
				item.queryCondition = () => {
					let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
					// let rowData = props.cardTable.getClickRowIndex(tableId);
					let index = props.cardTable.getCurrentIndex(tableId);
					let rowData = props.cardTable.getDataByIndex(tableId, index);
					let pk_equip = '';
					let pk_usedunit_after = '';
					if (rowData && rowData.values) {
						pk_equip = rowData.values['pk_equip'].value;
						pk_usedunit_after = rowData.values['pk_usedunit_after'].value;
					}

					// 使用人根据使用权过滤
					return {
						head_pk_org: pk_org,
						pk_org: pk_usedunit_after,
						busifuncode: IBusiRoleConst.ASSETORG,
						isTreelazyLoad: false,
						pk_usedunit_after,
						pk_equip,
						TreeRefActionExt: 'nccloud.web.aum.alter.assetalter.refcondition.PK_USER_AFTERSqlBuilder'
					};
				};
			} else if (item.attrcode == 'pk_used_status_after') {
				// 变动后状态
				item.queryCondition = () => {
					let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
					return {
						pk_org,
						GridRefActionExt: 'nccloud.web.aum.alter.assetalter.refcondition.PK_USED_STATUS_AFTERSqlBuilder'
					};
				};
			} else if (item.attrcode == 'pk_jobmngfil') {
				// 项目
				item.queryCondition = () => {
					let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
					return { pk_org };
				};
			} else if (item.attrcode == 'pk_jobmngfil_after') {
				// 变动后项目
				item.queryCondition = () => {
					let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
					return { pk_org };
				};
			} else if (item.attrcode == 'pk_mandept_v_after') {
				// 管理部门参照当前资产组织可见的部门
				item.queryCondition = () => {
					let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
					// let rowData = props.cardTable.getClickRowIndex(tableId);
					let index = props.cardTable.getCurrentIndex(tableId);
					let rowData = props.cardTable.getDataByIndex(tableId, index);
					let pk_equip = '';
					if (rowData && rowData.values) {
						pk_equip = rowData.values['pk_equip'].value;
					}

					return {
						pk_org,
						busifuncode: IBusiRoleConst.ASSETORG,
						isTreelazyLoad: false,
						pk_equip,
						TreeRefActionExt: 'nccloud.web.aum.alter.assetalter.refcondition.PK_MANDEPT_V_AFTERSqlBuilder'
					};
				};
			} else if (item.attrcode == 'pk_manager_after') {
				// 管理人参照当前资产组织可见的人
				item.queryCondition = () => {
					let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
					// let rowData = props.cardTable.getClickRowIndex(tableId);
					let index = props.cardTable.getCurrentIndex(tableId);
					let rowData = props.cardTable.getDataByIndex(tableId, index);
					let pk_equip = '';
					if (rowData && rowData.values) {
						pk_equip = rowData.values['pk_equip'].value;
					}

					return {
						pk_org,
						busifuncode: IBusiRoleConst.ASSETORG,
						isTreelazyLoad: false,
						pk_equip,
						TreeRefActionExt: 'nccloud.web.aum.alter.assetalter.refcondition.PK_MANAGER_AFTERSqlBuilder'
					};
				};
			} else if (item.attrcode == 'pk_specialty_after') {
				// 专业
				item.queryCondition = () => {
					let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
					return { pk_org };
				};
			} else if (item.attrcode == 'pk_supplier_after') {
				// 变动后供应商
				item.queryCondition = () => {
					let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
					return { pk_org };
				};
			} else if (item.attrcode == 'pk_location_after') {
				// 变动后位置
				item.queryCondition = () => {
					let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
					// let rowData = props.cardTable.getClickRowIndex(tableId);
					let index = props.cardTable.getCurrentIndex(tableId);
					let rowData = props.cardTable.getDataByIndex(tableId, index);
					let pk_equip = '';
					if (rowData && rowData.values) {
						pk_equip = rowData.values['pk_equip'].value;
					}

					return {
						pk_org: pk_org,
						pk_equip,
						TreeRefActionExt: 'nccloud.web.aum.alter.assetalter.refcondition.PK_LOCATION_AFTERSqlBuilder'
					};
				};
			} else if (item.attrcode == 'pk_category_after') {
				// 变动后类别
				// 父类别不能被选中
				item.onlyLeafCanSelect = true;
			} else {
				// 自定义项的过滤
				defRefCondition.call(this, props, item, formId, pk_group);
			}
		});
		// 添加表格操作列
		let oprCol = createOprationColumn.call(this, props, { tableButtonClick });
		meta[tableId].items.push(oprCol);
	}

	// 卡片模板修改后公共处理
	meta = afterModifyCardMeta.call(this, props, meta);

	return meta;
}
