import { setValue, loadDataByPk, setBtnsVisible, setAlterItemVisiable } from './buttonClick';

/**
 * 分页查询
 * @param {*} props 
 * @param {*} pk 
 */
export default function(props, pk) {
	if (!pk || pk == 'undefined' || pk == 'null') {
		return;
	}
	const { pageConfig = {} } = props;
	const { formId } = pageConfig;
	let oldAlterinfo = props.form.getFormItemsValue(formId, 'alterinfo') || {};
	let callback = (data) => {
		setValue.call(this, props, data);

		let newAlterinfo = props.form.getFormItemsValue(formId, 'alterinfo') || {};
		// 变动项目显示并且前后值不一致时候设置表体变动前后项目显示与否
		if (this.isAlterInfoShow && oldAlterinfo.value != newAlterinfo.value) {
			setAlterItemVisiable.call(this, props);
		}

		// 设置按钮显隐性
		setBtnsVisible.call(this, props);
	};
	loadDataByPk.call(this, props, pk, callback);
}
