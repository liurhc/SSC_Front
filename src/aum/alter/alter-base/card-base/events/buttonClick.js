import { ajax, print, output, cardCache } from 'nc-lightapp-front';
import ampub from 'ampub';

const { components, utils, commonConst } = ampub;

const { ScriptReturnUtils, saveValidatorsUtil, LoginContext, imageHandle } = components;

const { getScriptCardReturnData } = ScriptReturnUtils;
const { beforeSaveValidator } = saveValidatorsUtil;
const { getContext, loginContextKeys } = LoginContext;
const { amImageScan, amImageView } = imageHandle;

const { cardUtils, msgUtils, multiLangUtils } = utils;

const { setBillFlowBtnsVisible, setCardValue, setHeadAreaData } = cardUtils;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { getMultiLangByID } = multiLangUtils;

const { StatusUtils } = commonConst;

const { UISTATE } = StatusUtils;

import { headAfterEvent } from './afterEvent';

/**
 * 按钮点击
 * @param {*} props 
 * @param {*} id 
 * @param {*} moduleId 
 */
export default function(props, id, moduleId) {
	switch (id) {
		case 'Add':
			add.call(this, props);
			break;
		case 'Edit':
			edit.call(this, props);
			break;
		case 'Delete':
			delConfirm.call(this, props);
			break;
		case 'Save':
			save.call(this, props);
			break;
		case 'Cancel':
			cancelConfirm.call(this, props);
			break;
		case 'Refresh':
			refresh.call(this, props);
			break;
		case 'AddLine':
			addLine.call(this, props, true);
			break;
		case 'DelLine':
			delLine.call(this, props);
			break;
		case 'BatchAlter':
			batchAlter.call(this, props);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'Commit':
			commit.call(this, props);
			break;
		case 'SaveCommit':
			saveCommit.call(this, props);
			break;
		case 'UnCommit':
			unCommit.call(this, props);
			break;
		case 'QueryAboutBusiness':
			queryAboutBusiness.call(this, props);
			break;
		case 'QueryAboutBillFlow':
			queryAboutBillFlow.call(this, props);
			break;
		case 'Attachment':
			attachment.call(this, props);
			break;
		case 'ReceiptScan':
			receiptScan.call(this, props);
			break;
		case 'ReceiptShow':
			receiptShow.call(this, props);
		default:
			break;
	}
}

/**
 * 新增
 * @param {*} props 
 */
export function add(props) {
	const { pageConfig = {} } = props;
	const { formId, tableId } = pageConfig;
	// 关闭所有展开行
	props.cardTable.closeExpandedRow(tableId);
	// 设置新增态
	setStatus.call(this, props, UISTATE.add);
	// 设置按钮显隐性
	setBtnsVisible.call(this, props);
	// 设置组织可编辑
	props.form.setFormItemsDisabled(formId, {
		pk_org_v: false,
		pk_org: false
	});
	let oldAlterinfo = props.form.getFormItemsValue(formId, 'alterinfo') || {};
	// 清空数据
	setValue.call(this, props, undefined);
	// 设置默认值
	setDefaultValue.call(this, props);
	let newAlterinfo = props.form.getFormItemsValue(formId, 'alterinfo') || {};
	// 变动项目显示并且前后值不一致时候设置表体变动前后项目显示与否
	if (this.isAlterInfoShow && oldAlterinfo.value != newAlterinfo.value) {
		setAlterItemVisiable.call(this, props);
	}
	// 设置字段的可编辑性
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	if (pk_org_v && pk_org_v.value) {
		headAfterEvent.call(this, props, formId, 'pk_org_v', pk_org_v, { value: '' }, pk_org_v.value);
	} else {
		// 设置其他字段不可编辑
		props.initMetaByPkorg('pk_org_v');
		// 设置按钮禁用状态
		setBtnsEnable.call(this, props, tableId);
	}
}

/**
 * 新增设置默认值
 * @param {*} props 
 */
function setDefaultValue(props) {
	const { pageConfig = {} } = props;
	const { formId, bill_type } = pageConfig;

	// 模板默认值处理

	// 公共默认值处理
	// 集团
	let groupId = getContext(loginContextKeys.groupId);
	let groupName = getContext(loginContextKeys.groupName);
	// 交易类型
	let transi_type = getContext(loginContextKeys.transtype);
	let pk_transitype = getContext(loginContextKeys.pk_transtype);
	// 默认主组织
	let pk_org = getContext(loginContextKeys.pk_org);
	let org_Name = getContext(loginContextKeys.org_Name);
	let pk_org_v = getContext(loginContextKeys.pk_org_v);
	let org_v_Name = getContext(loginContextKeys.org_v_Name);

	let headValue = {
		pk_group: { value: groupId, display: groupName },
		pk_org: { value: pk_org, display: org_Name },
		pk_org_v: { value: pk_org_v, display: org_v_Name },
		bill_type: { value: bill_type },
		transi_type: { value: transi_type },
		pk_transitype: { value: pk_transitype }
	};
	// 变动项目
	if (!this.isAlterInfoShow) {
		// 设置显示的字段为变动项目
		headValue.alterinfo = { value: this.alterItems.join(',') };
	}
	props.form.setFormItemsValue(formId, headValue);
}

/**
* 修改
* @param {*} props 
*/
export function edit(props, validatePermission = true) {
	const { pageConfig = {} } = props;
	const { formId, tableId, pkField, url, resourceCode } = pageConfig;
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	if (!pk || pk == 'undefined' || pk == 'null') {
		pk = props.getUrlParam('id');
	}
	if (!pk || pk == 'undefined' || pk == 'null') {
		return;
	}
	let editAction = () => {
		// 关闭所有展开行
		props.cardTable.closeExpandedRow(tableId);
		// 设置修改态
		setStatus.call(this, props, UISTATE.edit);
		// 设置按钮显隐性
		setBtnsVisible.call(this, props);
		// 设置组织字段不可编辑
		props.form.setFormItemsDisabled(formId, {
			pk_org_v: true,
			pk_org: true
		});
	};

	if (validatePermission) {
		let data = {
			pk,
			resourceCode
		};
		ajax({
			url: url.editUrl,
			data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if (data) {
						showMessage.call(this, props, { color: 'warning', content: data });
					} else {
						editAction();
					}
				}
			}
		});
	} else {
		editAction();
	}
}

/**
* 删除确认
* @param {*} props 
*/
export function delConfirm(props) {
	const { pageConfig = {} } = props;
	const { formId, pkField } = pageConfig;
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	if (!pk) {
		return;
	}
	showConfirm.call(this, props, { type: MsgConst.Type.Delete, beSureBtnClick: singleDel });
}

/**
 * 删除
 * @param {*} props 
 * @param {*} operatorType 
 * @param {*} commitType 
 */
function singleDel(props) {
	const { pageConfig = {} } = props;
	const { pagecode, formId, tableId, pkField, url, dataSource } = pageConfig;
	let oldAlterinfo = props.form.getFormItemsValue(formId, 'alterinfo') || {};
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let ts = props.form.getFormItemsValue(formId, 'ts').value;
	let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let paramInfoMap = {
		[pk]: ts
	};
	let obj = {
		dataType: 'cardData',
		OperatorType: 'DELETE',
		commitType: 'commit',
		pageid: pagecode,
		paramInfoMap: paramInfoMap
	};
	cardData.userjson = JSON.stringify(obj);
	ajax({
		url: url.deleteUrl,
		data: cardData,
		success: (res) => {
			let { success } = res;
			if (success) {
				// 性能优化，关闭表单和表格渲染
				props.beforeUpdatePage();
				let callback = (newpk) => {
					let loadcallback = (data) => {
						setValue.call(this, props, data);

						let newAlterinfo = props.form.getFormItemsValue(formId, 'alterinfo') || {};
						// 变动项目显示并且前后值不一致时候设置表体变动前后项目显示与否
						if (this.isAlterInfoShow && oldAlterinfo.value != newAlterinfo.value) {
							setAlterItemVisiable.call(this, props);
						}

						// 设置按钮显隐性
						setBtnsVisible.call(this, props);
					};
					// 加载下一条数据
					loadDataByPk.call(this, props, newpk, loadcallback);
				};
				getScriptCardReturnData.call(
					this,
					res,
					props,
					formId,
					tableId,
					pkField,
					dataSource,
					null,
					true,
					callback,
					pagecode
				);
				// 性能优化，表单和表格统一渲染
				props.updatePage(formId, tableId);
			}
		}
	});
}

/**
* 返回
* @param {*} props 
*/
export function backToList(props) {
	const { pageConfig = {} } = props;
	const { listRouter, pagecode } = pageConfig;
	props.pushTo(listRouter, { pagecode: pagecode.replace('card', 'list') });
}

/**
* 保存
* @param {*} props 
*/
export function save(props) {
	const { pageConfig = {} } = props;
	const { formId, tableId, pagecode, url, pkField, dataSource } = pageConfig;
	//必输项校验和过滤无效行
	let flag = beforeSaveValidator.call(this, props, formId, tableId);
	if (!flag) {
		return;
	}
	let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let reqUrl = url.insertUrl; //新增保存
	let oldstatus = props.form.getFormStatus(formId);
	if (oldstatus == UISTATE.edit) {
		reqUrl = url.updateUrl; //修改保存
	}
	// 保存前执行验证公式
	props.validateToSave(cardData, () => {
		// 调用Ajax保存数据
		ajax({
			url: reqUrl,
			data: cardData,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					// 性能优化，关闭表单和表格渲染
					props.beforeUpdatePage();
					// 设置浏览态
					setStatus.call(this, props, UISTATE.browse);
					// 设置值
					setValue.call(this, props, data);
					showMessage.call(this, props, { type: MsgConst.Type.SaveSuccess });
					let pk = props.form.getFormItemsValue(formId, pkField).value;
					let cardData = props.createMasterChildData(pagecode, formId, tableId);
					// 保存成功后处理缓存
					if (oldstatus == UISTATE.add) {
						cardCache.addCache(pk, cardData, formId, dataSource);
					} else {
						cardCache.updateCache(pkField, pk, cardData, formId, dataSource);
					}
					// 设置按钮显隐性
					setBtnsVisible.call(this, props);
					// 性能优化，表单和表格统一渲染
					props.updatePage(formId, tableId);
				}
			}
		});
	});
}

/**
* 取消确认
* @param {*} props 
*/
function cancelConfirm(props) {
	showConfirm.call(this, props, { type: MsgConst.Type.Cancel, beSureBtnClick: cancel });
}

/**
* 取消
* @param {*} props 
*/
function cancel(props) {
	const { pageConfig = {} } = props;
	const { formId, dataSource } = pageConfig;
	// 恢复字段的可编辑性
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	if (!pk_org_v || !pk_org_v.value) {
		props.resMetaAfterPkorgEdit();
	}
	setStatus.call(this, props, UISTATE.browse);
	let oldAlterinfo = props.form.getFormItemsValue(formId, 'alterinfo') || {};
	// 加载编辑前数据，如果编辑前没有数据，则加载当前列表最后一条数据，如果还没有，显示为空不处理
	let pk = cardCache.getCurrentId(dataSource);
	if (!pk) {
		pk = cardCache.getCurrentLastId(dataSource);
	}
	let callback = (data) => {
		setValue.call(this, props, data);

		let newAlterinfo = props.form.getFormItemsValue(formId, 'alterinfo') || {};
		// 变动项目显示并且前后值不一致时候设置表体变动前后项目显示与否
		if (this.isAlterInfoShow && oldAlterinfo.value != newAlterinfo.value) {
			setAlterItemVisiable.call(this, props);
		}

		// 设置按钮显隐性
		setBtnsVisible.call(this, props);
	};
	loadDataByPk.call(this, props, pk, callback);
}

/**
* 刷新
* @param {*} props 
*/
function refresh(props) {
	const { pageConfig = {} } = props;
	const { formId, pkField } = pageConfig;
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	if (!pk) {
		return;
	}
	let oldAlterinfo = props.form.getFormItemsValue(formId, 'alterinfo') || {};
	let callback = (data) => {
		setValue.call(this, props, data);

		let newAlterinfo = props.form.getFormItemsValue(formId, 'alterinfo') || {};
		// 变动项目显示并且前后值不一致时候设置表体变动前后项目显示与否
		if (this.isAlterInfoShow && oldAlterinfo.value != newAlterinfo.value) {
			setAlterItemVisiable.call(this, props);
		}

		// 设置按钮显隐性
		setBtnsVisible.call(this, props);
		if (data) {
			showMessage.call(this, props, { type: MsgConst.Type.RefreshSuccess });
		} else {
			showMessage.call(this, props, { type: MsgConst.Type.DataDeleted });
		}
	};
	// 查询到最新单据数据
	getDataByPk.call(this, props, pk, callback);
}

/**
* 新增行
* @param {*} props 
* @param {*} isFocus 是否聚焦
*/
export function addLine(props, isFocus = false) {
	const { pageConfig = {} } = props;
	const { tableId } = pageConfig;
	props.cardTable.addRow(tableId, undefined, undefined, isFocus);
}

/**
* 删除行
* @param {*} props 
*/
function delLine(props) {
	const { pageConfig = {} } = props;
	const { tableId } = pageConfig;
	let checkedRows = props.cardTable.getCheckedRows(tableId);
	let checkedIndex = [];
	checkedRows.map((item) => {
		checkedIndex.push(item.index);
	});
	props.cardTable.delRowsByIndex(tableId, checkedIndex);
	setBtnsEnable.call(this, props, tableId);
}

/**
 * 批改操作
 * @param {*} props 
 */
function batchAlter(props) {
	const { pageConfig = {} } = props;
	const { formId, tableId, pagecode, url } = pageConfig;
	let num = props.cardTable.getNumberOfRows(tableId);
	if (num <= 1) {
		return;
	}
	// 获取原始数据
	let changeData = props.cardTable.getTableItemData(tableId);
	// 如果需要批改的字段需要特殊的处理，则调用后端的批改操作
	let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	ajax({
		url: url.batchAlterUrl,
		data: {
			card: cardData,
			batchChangeIndex: changeData.batchChangeIndex, // 原始数据行号
			batchChangeKey: changeData.batchChangeKey, // 原始数据key
			batchChangeValue: changeData.batchChangeValue, // 原始数据value
			// 原始数据所在的vo，对应聚合VO中的tableCodes，一主一子可为空，一主多子要传对应的值
			tableCode: ''
		},
		success: (res) => {
			let { success, data } = res;
			if (success) {
				// 性能优化，关闭表单和表格渲染
				props.beforeUpdatePage();
				//把数据设置到界面上
				setValue.call(this, props, data, false);
				// 性能优化，表单和表格统一渲染
				props.updatePage(formId, tableId);
			}
		}
	});
}

/**
 * 设置界面状态
 * @param {*} props 
 * @param {*} status 
 */
export function setStatus(props, status = UISTATE.browse) {
	const { pageConfig = {} } = props;
	const { formId, tableId } = pageConfig;
	switch (status) {
		case UISTATE.add:
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, UISTATE.edit);
			setBtnsEnable.call(this, props, tableId);
			break;
		case UISTATE.edit:
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, status);
			setBtnsEnable.call(this, props, tableId);
			break;
		case UISTATE.browse:
			props.form.setFormStatus(formId, status);
			props.cardTable.setStatus(tableId, status);
			break;
		default:
			break;
	}
	setHeadAreaData.call(this, props, { status });
}

/**
 * 通过单据id获取缓存单据信息，没有缓存则重新查询
 * @param {*} props 
 * @param {*} pk 
 */
export function loadDataByPk(props, pk, callback) {
	if (!pk || pk == 'null') {
		typeof callback == 'function' && callback();
	} else {
		const { pageConfig = {} } = props;
		const { dataSource } = pageConfig;
		let cachData = cardCache.getCacheById(pk, dataSource);
		if (cachData) {
			typeof callback == 'function' && callback(cachData);
		} else {
			getDataByPk.call(this, props, pk, callback);
		}
	}
}

/**
 * 通过单据id查询单据信息
 * @param {*} props 
 * @param {*} pk 
 */
function getDataByPk(props, pk, callback) {
	const { pageConfig = {} } = props;
	const { formId, pagecode, pkField, url, dataSource } = pageConfig;
	ajax({
		url: url.queryCardUrl,
		data: {
			pagecode,
			pk
		},
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					cardCache.updateCache(pkField, pk, data, formId, dataSource);
				} else {
					let cachData = cardCache.getCacheById(pk, dataSource);
					if (cachData) {
						cardCache.deleteCacheById(pkField, pk, dataSource);
					}
					showMessage.call(this, props, { type: MsgConst.Type.DataDeleted });
				}
				typeof callback == 'function' && callback(data);
			}
		}
	});
}

/**
 * 设置界面值
 * @param {*} props 
 * @param {*} data 
 * @param {*} refreshBtns 
 */
export function setValue(props, data) {
	setCardValue.call(this, props, data);
}

/**
 * 设置按钮显示隐藏
 * @param {*} props 
 */
export function setBtnsVisible(props) {
	const { pageConfig = {} } = props;
	const { formId, pkField, editBtns, browseBtns } = pageConfig;
	let status = props.form.getFormStatus(formId) || UISTATE.browse;
	let btnObj = {};
	if (status != UISTATE.browse) {
		browseBtns.map((item) => {
			btnObj[item] = false;
		});
		editBtns.map((item) => {
			btnObj[item] = true;
		});
	} else {
		editBtns.map((item) => {
			btnObj[item] = false;
		});
		let pkVal = props.form.getFormItemsValue(formId, pkField);
		if (!pkVal || !pkVal.value) {
			browseBtns.map((item) => {
				btnObj[item] = false;
			});
			btnObj['Add'] = true;
		} else {
			browseBtns.map((item) => {
				btnObj[item] = true;
			});
		}
	}
	props.button.setButtonVisible(btnObj);
	setBillFlowBtnsVisible.call(this, props, formId, pkField);
}

/**
 * 设置表体按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBtnsEnable(props, moduleId) {
	const { pageConfig = {} } = props;
	const { formId } = pageConfig;
	let pk_org_v = props.form.getFormItemsValue(formId, 'pk_org_v');
	if (!pk_org_v || !pk_org_v.value) {
		props.button.setButtonDisabled([ 'AddLine', 'DelLine', 'BatchAlter' ], true);
	} else {
		props.button.setButtonDisabled([ 'AddLine', 'BatchAlter' ], false);
		let checkedRows = [];
		let num = props.cardTable.getNumberOfRows(moduleId, false);
		if (num > 0) {
			checkedRows = props.cardTable.getCheckedRows(moduleId);
		}
		props.button.setButtonDisabled([ 'DelLine' ], !(checkedRows && checkedRows.length > 0));
	}
}

/**
 * 根据表头alterinfo设置变动项目显隐性
 * @param {*} props 
 * @param {*} flag true以alterItems处理显示，false以alterinfo处理显示
 */
export function setAlterItemVisiable(props) {
	const { pageConfig = {} } = props;
	const { formId, tableId } = pageConfig;
	// 先隐藏所有项目，再显示需要的项目
	let meta = props.meta.getMeta();
	if (!meta || !meta[tableId] || !meta[tableId].items) {
		return;
	}
	let data = {};
	let olditems = [];
	meta[tableId].items.map((item) => {
		if (item.attrcode.endsWith('_before') || item.attrcode.endsWith('_after')) {
			olditems.push(item.attrcode);
		}
	});
	data.hideKeys = olditems;

	let alterinfo = props.form.getFormItemsValue(formId, 'alterinfo');
	let newitems = [];
	if (!this.isAlterInfoShow && this.alterItems && this.alterItems.length > 0) {
		this.alterItems.map((item) => {
			newitems.push(`${item}_before`);
			newitems.push(`${item}_after`);
		});
	} else if (this.isAlterInfoShow && alterinfo && alterinfo.value) {
		this.alterItems = alterinfo.value.split(',');
		this.alterItems.map((item) => {
			newitems.push(`${item}_before`);
			newitems.push(`${item}_after`);
		});
	}
	data.showKeys = newitems;

	// 一个方法同时设置侧拉和展开模板字段显隐性
	props.cardTable.setColVisibleByKey(tableId, data);
	// props.cardTable.setColVisibleByKey(tableId + '_edit', data);
	// props.cardTable.setColVisibleByKey(tableId + '_browse', data);
}

/**
 * 打印
 * @param {*} props 
 */
function printTemp(props) {
	const { pageConfig = {} } = props;
	const { url } = pageConfig;
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		url.printUrl, // 后台打印服务url
		printData
	);
}

/**
 * 输出
 * @param {*} props 
 */
function outputTemp(props) {
	const { pageConfig = {} } = props;
	const { url } = pageConfig;
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });
		return;
	}
	output({
		url: url.printUrl,
		data: printData
	});
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	const { pageConfig = {} } = props;
	const { formId, pkField, printFilename, printNodekey } = pageConfig;
	let pk = props.form.getFormItemsValue(formId, pkField);
	if (!pk || !pk.value) {
		return false;
	}
	let pks = [ pk.value ];
	let printData = {
		filename: getMultiLangByID(printFilename), // 文件名称
		nodekey: printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

/**
 * 保存提交
 * @param {*} props 
 * @param {*} operatorType 
 * @param {*} commitType 
 */
export function saveCommit(props, content) {
	const { pageConfig = {} } = props;
	const { pagecode, formId, tableId, pkField, url, dataSource } = pageConfig;
	//必输项校验和过滤无效行
	let flag = beforeSaveValidator.call(this, props, formId, tableId);
	if (!flag) {
		return;
	}

	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let ts = props.form.getFormItemsValue(formId, 'ts').value;
	let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let paramInfoMap = {
		[pk]: ts
	};
	let obj = {
		dataType: 'cardData',
		OperatorType: 'SAVE',
		commitType: 'saveCommit',
		pageid: pagecode,
		paramInfoMap: paramInfoMap,
		content
	};
	cardData.userjson = JSON.stringify(obj);
	let saveCommitAction = () => {
		// 调用Ajax保存提交数据
		ajax({
			url: url.commitUrl,
			data: cardData,
			success: (res) => {
				let { success } = res;
				if (success) {
					// 性能优化，关闭表单和表格渲染
					props.beforeUpdatePage();
					if (content) {
						this.setState({
							compositedisplay: false
						});
					}
					let setCommonValue = (data) => {
						// 设置浏览态
						setStatus.call(this, props, UISTATE.browse);
						// 设置值
						setValue.call(this, props, data);
					};
					let callback = () => {
						// 设置按钮显隐性
						setBtnsVisible.call(this, props);
					};
					getScriptCardReturnData.call(
						this,
						res,
						props,
						formId,
						tableId,
						pkField,
						dataSource,
						setCommonValue,
						false,
						callback,
						pagecode
					);
					// 性能优化，表单和表格统一渲染
					props.updatePage(formId, tableId);
				}
			}
		});
	};
	if (content) {
		saveCommitAction();
	} else {
		// 保存提交前执行验证公式
		props.validateToSave(cardData, saveCommitAction);
	}
}

/**
 * 提交
 * @param {*} props 
 * @param {*} operatorType 
 * @param {*} commitType 
 */
export function commit(props, content) {
	runScript.call(this, props, 'SAVE', 'commit', content);
}

/**
 * 收回
 * @param {*} props 
 * @param {*} operatorType 
 * @param {*} commitType 
 */
function unCommit(props) {
	runScript.call(this, props, 'UNSAVE', 'commit');
}

/**
 * 提交
 * @param {*} props 
 * @param {*} operatorType 
 * @param {*} commitType 
 */
function runScript(props, operatorType, commitType, content) {
	const { pageConfig = {} } = props;
	const { pagecode, formId, tableId, pkField, url, dataSource } = pageConfig;
	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let ts = props.form.getFormItemsValue(formId, 'ts').value;
	let cardData = props.createMasterChildDataSimple(pagecode, formId, tableId);
	let paramInfoMap = {
		[pk]: ts
	};
	let obj = {
		dataType: 'cardData',
		OperatorType: operatorType,
		commitType: commitType,
		pageid: pagecode,
		paramInfoMap: paramInfoMap,
		content
	};
	cardData.userjson = JSON.stringify(obj);
	ajax({
		url: url.commitUrl,
		data: cardData,
		success: (res) => {
			let { success } = res;
			if (success) {
				// 性能优化，关闭表单和表格渲染
				props.beforeUpdatePage();
				if (content) {
					this.setState({
						compositedisplay: false
					});
				}
				let setCommonValue = (data) => {
					// 设置值
					setValue.call(this, props, data);
					// 设置按钮显隐性
					setBtnsVisible.call(this, props);
				};
				getScriptCardReturnData.call(
					this,
					res,
					props,
					formId,
					tableId,
					pkField,
					dataSource,
					setCommonValue,
					false,
					null,
					pagecode
				);
				// 性能优化，表单和表格统一渲染
				props.updatePage(formId, tableId);
			}
		}
	});
}

/**
 * 单据追溯
 * @param {*} props 
 */
function queryAboutBusiness(props) {
	const { pageConfig = {} } = props;
	const { formId, pkField } = pageConfig;
	let pkVal = props.form.getFormItemsValue(formId, pkField);
	if (pkVal && pkVal.value) {
		this.setState({
			pk_bill: pkVal.value,
			showBillTrack: true
		});
	}
}

/**
 * 审批详情
 * 
 * @param {*} props 
 */
function queryAboutBillFlow(props) {
	const { pageConfig = {} } = props;
	const { formId, pkField } = pageConfig;
	let pkVal = props.form.getFormItemsValue(formId, pkField);
	let transi_type = props.form.getFormItemsValue(formId, 'transi_type');
	if (pkVal && pkVal.value) {
		this.setState({
			pk_bill: pkVal.value,
			transi_type: transi_type.value,
			showApprove: true
		});
	}
}

/**
 * 附件管理
 * @param {*} props 
 */
function attachment(props) {
	const { pageConfig = {} } = props;
	const { pagecode, formId, pkField } = pageConfig;
	// billId 是单据主键
	let billId = props.form.getFormItemsValue(formId, pkField).value;
	props.ncUploader.show(`${pagecode}-uploader`, {
		billId: 'pam/alter/' + billId
	});
}

/**
 * 影像扫描
 * @param {*} props 
 */
function receiptScan(props) {
	let { pageConfig } = props;
	let { pagecode, pkField, formId, tableId } = pageConfig;
	let imageData = {
		pagecode,
		formId,
		tableId,
		pkField
	};
	amImageScan(props, imageData);
}

/**
 * 影像查看
 * @param {*} props 
 */
function receiptShow(props) {
	let { pageConfig } = props;
	let { pagecode, pkField, formId, tableId } = pageConfig;
	let imageData = {
		pagecode,
		formId,
		tableId,
		pkField
	};
	amImageView(props, imageData);
}
