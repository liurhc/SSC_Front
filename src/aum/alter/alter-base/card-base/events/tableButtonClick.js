import { setBtnsEnable } from './buttonClick';

/**
 * 表格操作按钮
 * @param {*} props 
 * @param {*} key 
 * @param {*} text 
 * @param {*} record 
 * @param {*} index 
 * @param {*} moduleId 
 */
export default function(props, key, text, record, index, tableId) {
	switch (key) {
		// 表格操作按钮
		case 'OpenCard':
			openCard.call(this, props, record, index, tableId);
			break;
		case 'DelLine':
			delLine.call(this, props, index, tableId);
			break;
		default:
			break;
	}
}

/**
 * 表格行展开
 * @param {*} props 
 * @param {*} record 
 * @param {*} index 
 */
function openCard(props, record, index, tableId) {
	let status = props.cardTable.getStatus(tableId);
	if (status == 'edit') {
		props.cardTable.setClickRowIndex(tableId, { record, index });
		props.cardTable.openModel(tableId, status, record, index);
	} else if (status == 'browse') {
		props.cardTable.toggleRowView(tableId, record);
	}
}

/**
 * 表格行删除
 * @param {*} props 
 * @param {*} index 
 */
function delLine(props, index, tableId) {
	props.cardTable.delRowsByIndex(tableId, index);
	setBtnsEnable.call(this, props, tableId);
}
