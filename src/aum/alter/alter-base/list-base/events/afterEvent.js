import ampub from 'ampub';

const { components } = ampub;

const { assetOrgMultiRefFilter } = components;

const { isMultiCorpRefHandler } = assetOrgMultiRefFilter;

/**
 * 查询区编辑后事件
 * @param {*} key 
 * @param {*} value 
 */
export default function afterEvent(key, value) {
	let { pageConfig = {} } = this.props;
	let { searchAreaId } = pageConfig;
	if (key == 'pk_org') {
		isMultiCorpRefHandler.call(this, this.props, value, searchAreaId, [ 'pk_usedept', 'pk_manager', 'pk_user' ]);
	}
}
