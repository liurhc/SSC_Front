import ampub from 'ampub';

const { components, utils, commonConst } = ampub;

const { LoginContext, refInit, assetOrgMultiRefFilter } = components;

const { loginContext, getContext, loginContextKeys } = LoginContext;
const { defRefCondition, commonRefCondition } = refInit;
const { AssetOrgMultiRefFilter } = assetOrgMultiRefFilter;

const { listUtils, multiLangUtils } = utils;

const { createOprationColumn } = listUtils;
const { getMultiLangByID } = multiLangUtils;

const { CommonKeys } = commonConst;

const { IBusiRoleConst } = CommonKeys;

import tableButtonClick from './tableButtonClick';
import { linkToCard, setBatchBtnsEnable } from './buttonClick';

/**
 * 模板初始化
 * @param {*} props 
 */
export default function(props) {
	const { pageConfig = {} } = props;
	const { pagecode } = pageConfig;
	props.createUIDom(
		{
			// 页面编码
			pagecode
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
				if (data.button) {
					// 设置按钮
					let button = data.button;
					props.button.setButtons(button);
					// 行删除时悬浮框提示
					props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001') /* 国际化处理： 确定要删除吗？*/);
				}
				if (data.template) {
					// 修改模板
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta, () => {
						afterInit.call(this, props);
					});
				}
			}
		}
	);
}

/**
 * 模板初始化以后
 * @param {*} props 
 */
function afterInit(props) {
	const { pageConfig = {} } = props;
	const { tableId } = pageConfig;
	setBatchBtnsEnable.call(this, props, tableId);
}

/**
 * 修改模板
 * @param {*} props 
 * @param {*} meta 
 */
function modifierMeta(props, meta) {
	const { pageConfig = {} } = props;
	const { tableId, searchAreaId } = pageConfig;
	const pk_group = getContext(loginContextKeys.groupId);
	// 参照查询条件过滤
	if (meta[searchAreaId] && meta[searchAreaId].items && meta[searchAreaId].items.length > 0) {
		meta[searchAreaId].items.map((item, key) => {
			if (item.attrcode == 'pk_org') {
				item.queryCondition = () => {
					return { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter' };
				};
			} else if (item.attrcode == 'pk_recorder') {
				// 经办人根据组织过滤
				AssetOrgMultiRefFilter.call(this, props, searchAreaId, item, 'pk_org');
				getQueryCondition.call(this, props, item, 'pk_org');
			} else if (item.attrcode == 'bodyvos.pk_equip.pk_usedept' || item.attrcode == 'bodyvos.pk_equip.pk_user') {
				// 组织多选参照过滤
				AssetOrgMultiRefFilter.call(this, props, searchAreaId, item, 'bodyvos.pk_equip.pk_usedunit');
				getQueryCondition.call(this, props, item, 'bodyvos.pk_equip.pk_usedunit');
				if (item.attrcode == 'bodyvos.pk_equip.pk_usedept') {
					addRunWithChildren.call(this, item);
				}
			} else if (
				item.attrcode == 'bodyvos.pk_equip.pk_mandept' ||
				item.attrcode == 'bodyvos.pk_equip.pk_manager'
			) {
				// 组织多选参照过滤
				AssetOrgMultiRefFilter.call(this, props, searchAreaId, item, 'bodyvos.pk_equip.pk_ownerorg');
				getQueryCondition.call(this, props, item, 'bodyvos.pk_equip.pk_ownerorg');
				if (item.attrcode == 'bodyvos.pk_equip.pk_mandept') {
					addRunWithChildren.call(this, item);
				}
			} else {
				let bodyDefPrefix = 'bodyvos.def';
				defRefCondition.call(this, props, item, searchAreaId, pk_group, true, 'pk_org', bodyDefPrefix);
				defRefCondition.call(this, props, item, searchAreaId, pk_group, true);
			}
			// 添加【执行时包含下级】默认勾选
			commonRefCondition.call(this, props, item);
		});
	}
	if (meta[tableId] && meta[tableId].items && meta[tableId].items.length > 0) {
		// 修改列渲染样式
		meta[tableId].items.map((item, key) => {
			if (item.attrcode == 'bill_code') {
				item.render = (text, record, index) => {
					return (
						<div
							class="simple-table-td"
							field="bill_code"
							fieldname={getMultiLangByID('452002536A-000007') /* 国际化处理： 变动单号*/}
						>
							<span
								className="code-detail-link"
								onClick={() => {
									linkToCard.call(this, props, record);
								}}
							>
								{record && record.bill_code && record.bill_code.value}
							</span>
						</div>
					);
				};
			}
			return item;
		});
		// 添加表格操作列
		let oprCol = createOprationColumn.call(this, props, { tableButtonClick });
		meta[tableId].items.push(oprCol);
	}

	return meta;
}

/**
 *  添加【执行时包含下级】默认勾选
 * @param {*} item 
 */
function addRunWithChildren(item) {
	item.isRunWithChildren = true;
	item.defaultRunWithChildren = true;
}

/** 
 * *获取查询条件的值
 */
function getSearchValue(props, field) {
	const { pageConfig = {} } = props;
	const { searchAreaId } = pageConfig;
	let data = props.search.getSearchValByField(searchAreaId, field);
	if (data && data.value && data.value.firstvalue) {
		let firstvalue = data.value.firstvalue;
		if (firstvalue.split(',').length == 1) {
			return data.value.firstvalue;
		}
	}
}

/**
 * 获取字段的过滤条件，针对是部门和人员等需要组织多选参照的字段
 */
function getQueryCondition(props, item, org) {
	item.queryCondition = () => {
		let pk_org = getSearchValue.call(this, props, org); //主组织
		let filter = { busifuncode: IBusiRoleConst.ASSETORG, isShowUnit: true };
		if (pk_org) {
			filter['pk_org'] = pk_org;
		}
		return filter; // 根据pk_org过滤
	};
}
