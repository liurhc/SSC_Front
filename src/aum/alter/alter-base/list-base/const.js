// 页面配置
export const pageConfig = {
	// 应用主键
	appid: '0001Z9100000000012LQ',
	// 应用编码
	appcode: '452002536A',
	// 应用名称
	title: '452002536A-000006' /* 国际化处理： 资产变动*/,
	// 查询区域id
	searchAreaId: 'searchArea',
	// 表格区域id
	tableId: 'list_head',
	// 页面编码
	pagecode: '452002536A_list',
	// 主键字段
	pkField: 'pk_alter',
	// 单据类型
	bill_type: '4A07',
	// 请求链接
	url: {
		queryUrl: '/nccloud/aum/alter/query.do',
		queryPageUrl: '/nccloud/aum/alter/querypagegridbypks.do',
		editUrl: '/nccloud/aum/alter/edit.do',
		printUrl: '/nccloud/aum/alter/printCard.do',
		commitUrl: '/nccloud/aum/alter/commit.do',
		deleteUrl: '/nccloud/aum/alter/commit.do'
	},
	// 输出文件名称
	printFilename: '452002536A-000006' /* 国际化处理： 资产变动*/,
	// 打印模板节点标识
	printNodekey: null,
	cardRouter: '/card',
	// 权限资源编码
	resourceCode: '4520024005',
	dataSource: 'aum.alter.assetalter.main'
};
