import React, { Component } from 'react';
import { createPage } from 'nc-lightapp-front';
import MasterChildCardBase, { baseConfig } from '../../alter-base/card-base';

// 页面配置
const pageConfig = {
	...baseConfig,
	// 应用主键
	appid: '0001Z9100000000012LP',
	// 应用编码
	appcode: '452002532A',
	// 应用名称
	title: '452002536A-000005' /* 国际化处理： 资产项目变动*/,
	// 页面编码
	pagecode: '452002532A_card',
	// 输出文件名称
	printFilename: '452002536A-000005' /* 国际化处理： 资产项目变动*/,
	// 打印模板节点标识
	printNodekey: null,
	listRouter: '/list',
	// 权限资源编码
	resourceCode: '4520024040',
	dataSource: 'aum.alter.jobmngfilalter.main'
};

const MasterChildCard = createPage({
	billinfo: {
		billtype: 'card',
		pagecode: pageConfig.pagecode,
		headcode: pageConfig.formId,
		bodycode: pageConfig.tableId
	}
})(MasterChildCardBase);

export default class Card extends Component {
	render() {
		return <MasterChildCard pageConfig={pageConfig} />;
	}
}
