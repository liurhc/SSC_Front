import React, { Component } from 'react';
import { createPage } from 'nc-lightapp-front';
import MasterChildListBase, { baseConfig } from '../../alter-base/list-base';

// 页面配置
const pageConfig = {
	...baseConfig,
	// 应用主键
	appid: '0001Z9100000000012LP',
	// 应用编码
	appcode: '452002532A',
	// 应用名称
	title: '452002536A-000005' /* 国际化处理： 资产项目变动*/,
	// 页面编码
	pagecode: '452002532A_list',
	// 查询模板id
	quertTempletId: '1001Z91000000000KGEN',
	// 输出文件名称
	printFilename: '452002536A-000005' /* 国际化处理： 资产项目变动*/,
	// 打印模板节点标识
	printNodekey: null,
	cardRouter: '/card',
	// 权限资源编码
	resourceCode: '4520024040',
	dataSource: 'aum.alter.jobmngfilalter.main'
};

const MasterChildList = createPage({})(MasterChildListBase);

export default class List extends Component {
	render() {
		return <MasterChildList pageConfig={pageConfig} />;
	}
}
