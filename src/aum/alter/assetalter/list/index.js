import React, { Component } from 'react';
import { createPage } from 'nc-lightapp-front';
import MasterChildListBase, { baseConfig } from '../../alter-base/list-base';

// 页面配置
const pageConfig = {
	...baseConfig,
	// 应用主键
	appid: '0001Z9100000000012LQ',
	// 应用编码
	appcode: '452002536A',
	// 应用名称
	title: '452002536A-000006' /* 国际化处理： 资产变动*/,
	// 页面编码
	pagecode: '452002536A_list',
	// 查询模板id
	quertTempletId: '1001Z91000000000KGEN',
	// 输出文件名称
	printFilename: '452002536A-000006' /* 国际化处理： 资产变动*/,
	// 打印模板节点标识
	printNodekey: null,
	cardRouter: '/card',
	// 权限资源编码
	resourceCode: '452002536A',
	dataSource: 'aum.alter.assetalter.main'
};

const MasterChildList = createPage({})(MasterChildListBase);

export default class List extends Component {
	render() {
		return <MasterChildList pageConfig={pageConfig} />;
	}
}
