import React, { Component } from 'react';
import { createPage } from 'nc-lightapp-front';
import MasterChildCardBase, { baseConfig } from '../../alter-base/card-base';

// 页面配置
const pageConfig = {
	...baseConfig,
	// 应用主键
	appid: '0001Z9100000000012LQ',
	// 应用编码
	appcode: '452002536A',
	// 应用名称
	title: '452002536A-000006' /* 国际化处理： 资产变动*/,
	// 页面编码
	pagecode: '452002536A_card',
	// 输出文件名称
	printFilename: '452002536A-000006' /* 国际化处理： 资产变动*/,
	// 打印模板节点标识
	printNodekey: null,
	listRouter: '/list',
	// 权限资源编码
	resourceCode: '452002536A',
	dataSource: 'aum.alter.assetalter.main'
};

const MasterChildCard = createPage({
	billinfo: {
		billtype: 'card',
		pagecode: pageConfig.pagecode,
		headcode: pageConfig.formId,
		bodycode: pageConfig.tableId
	}
})(MasterChildCardBase);

export default class Card extends Component {
	render() {
		return <MasterChildCard pageConfig={pageConfig} />;
	}
}
