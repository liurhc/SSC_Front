import React, { Component } from 'react';
import { createPage } from 'nc-lightapp-front';
import MasterChildListBase, { baseConfig } from '../../alter-base/list-base';

// 页面配置
const pageConfig = {
	...baseConfig,
	// 应用主键
	appid: '0001Z9100000000012LL',
	// 应用编码
	appcode: '452002516A',
	// 应用名称
	title: '452002536A-000001' /* 国际化处理： 资产使用部门变动*/,
	// 页面编码
	pagecode: '452002516A_list',
	// 查询模板id
	quertTempletId: '1001Z91000000000KGEN',
	// 输出文件名称
	printFilename: '452002536A-000001' /* 国际化处理： 资产使用部门变动*/,
	// 打印模板节点标识
	printNodekey: null,
	cardRouter: '/card',
	// 权限资源编码
	resourceCode: '4520024010',
	dataSource: 'aum.alter.usedeptalter.main'
};

const MasterChildList = createPage({})(MasterChildListBase);

export default class List extends Component {
	render() {
		return <MasterChildList pageConfig={pageConfig} />;
	}
}
