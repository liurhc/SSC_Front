import { asyncComponent } from 'nc-lightapp-front';

const List = asyncComponent(() =>
	import(/* webpackChunkName: "aum/alter/usedeptalter/list/list" */ /* webpackMode: "eager" */ '../list')
);
const Card = asyncComponent(() =>
	import(/* webpackChunkName: "aum/alter/usedeptalter/card/card" */ /* webpackMode: "eager" */ '../card')
);

const routes = [
	{
		path: '/',
		component: List,
		exact: true
	},
	{
		path: '/list',
		component: List
	},
	{
		path: '/card',
		component: Card
	}
];

export default routes;
