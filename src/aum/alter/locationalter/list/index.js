import React, { Component } from 'react';
import { createPage } from 'nc-lightapp-front';
import MasterChildListBase, { baseConfig } from '../../alter-base/list-base';

// 页面配置
const pageConfig = {
	...baseConfig,
	// 应用主键
	appid: '0001Z9100000000012LN',
	// 应用编码
	appcode: '452002524A',
	// 应用名称
	title: '452002536A-000003' /* 国际化处理： 资产位置变动*/,
	// 页面编码
	pagecode: '452002524A_list',
	// 查询模板id
	quertTempletId: '1001Z91000000000KGEN',
	// 输出文件名称
	printFilename: '452002536A-000003' /* 国际化处理： 资产位置变动*/,
	// 打印模板节点标识
	printNodekey: null,
	cardRouter: '/card',
	// 权限资源编码
	resourceCode: '4520024020',
	dataSource: 'aum.alter.locationalter.main'
};

const MasterChildList = createPage({})(MasterChildListBase);

export default class List extends Component {
	render() {
		return <MasterChildList pageConfig={pageConfig} />;
	}
}
