import { ajax, toast,print,output } from 'nc-lightapp-front';
// let { Message } = base;
let tableid = 'ebank_palog';
let pageId = '36101ADLQ_L01';
export default function buttonClick(props, id) {
    //获取所有数据
    // let alldata = props.editTable.getAllRows(tableid,true);
    // let pks = [];
    // let checkedRows = props.editTable.getCheckedRows(tableid);
    console.log(id)
	switch (id) {	
        case 'output':
            outputData(props); 
            break;
        case 'Print':
            printData(props);
            break;
		default:
			break;
	}
}
/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */

function printData(props){
	let printData = props.table.getCheckedRows(tableid);
		let pks = [];
		printData.forEach((item) => {
			pks.push(item.data.values.pk_id.value);
		});  
		print(
			'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
			'/nccloud/obm/ebankautodownlog/print.do',
			{
				funcode:'36101ADLQ', //功能节点编码，即模板编码
				appcode:'36101ADLQ',
				oids: pks // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
			}
		);
}

function outputData(props){
	let printData = props.table.getCheckedRows(tableid);
		let pks = [];
		printData.forEach((item) => {
			pks.push(item.data.values.pk_id.value);
		});  
		output({
			url:'/nccloud/obm/ebankautodownlog/print.do',
			data: { 
			  	oids: pks,
			  	outputType: 'output'
			}
		});
}