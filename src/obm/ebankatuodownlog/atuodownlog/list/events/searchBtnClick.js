import { ajax, toast } from 'nc-lightapp-front';
let searchid = '36101Q';
let tableid = 'ebank_palog';
//点击查询，获取查询区数据
export default function searchBtnClick(props,value) {
	let pageInfo = this.props.table.getTablePageInfo(tableid);
	let queryInfo = props.search.getQueryInfo(searchid);
	queryInfo.pageInfo = pageInfo;
	let oid = queryInfo.oid;
	if(value){
		// let data = {
		// 	pageId: props.getSearchParam('p'),
		// 	queryInfo:queryInfo
		// };
		let data = {
			conditions:value.conditions || value,
			pagecode: "36101Q",
			pageInfo: pageInfo,
			queryAreaCode: "36101Q",
			oid: oid,
			queryType: "simple"
		};
		ajax({
			url: '/nccloud/obm/ebankautodownlog/queryschem.do',
			data: data,
			success: (res) => {
				let { success, data } = res;
				if(success){
					if(data && data[tableid]){
						props.table.setAllTableData(tableid, res.data[tableid]);
						toast({ content: [this.props.MutiInit.getIntl("36101ADLQ") && this.props.MutiInit.getIntl("36101ADLQ").get('36101ADLQ-000000')]+res.data[tableid].rows.length+[this.props.MutiInit.getIntl("36101ADLQ") && this.props.MutiInit.getIntl("36101ADLQ").get('36101ADLQ-000001')], color: "success" });/* 国际化处理： 查询成功,共,条*/
					}else{
						props.table.setAllTableData(tableid, {rows:[]});
						toast({ content: this.props.MutiInit.getIntl("36101ADLQ") && this.props.MutiInit.getIntl("36101ADLQ").get('36101ADLQ-000002'), color: "warning" });/* 国际化处理： 无数据*/
					}	
				}
			}
		});
	}else{
		
	}
	
}

