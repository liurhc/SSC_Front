import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, high,createPageIcon } from 'nc-lightapp-front';
const { PrintOutput } = high;
// let { Message } = base;
// const { NCBreadcrumb } = base;
// const NCBreadcrumbItem = NCBreadcrumb.NCBreadcrumbItem;
import { buttonClick, initTemplate, afterEvent, searchBtnClick, pageInfoClick, tableModelConfirm } from './events';
import './index.less';
let tableid = 'ebank_palog';
let searchid='36101Q';

class SingleTable extends Component {
	constructor(props) {
		super(props);
		this.props = props;
		this.state = {
			data: {},
			printData: {}
			
		}
	}

	getButtonNames = (codeId) => {
	
	};

	componentDidMount() {
		this.getData();
	}


	
  
  
	//请求列表数据
	getData = () => {
        
	};
	rowSelected = (props, moduleId, record, index, status) => {
		let checkedRows =this.props.table.getCheckedRows(moduleId);
		props.button.setButtonDisabled(['output', 'Print'], !(checkedRows && checkedRows.length > 0));
	};
	render() {
	
		let { table, button, search } = this.props;
		let { createButtonApp } = this.props.button;
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { createButton } = button;
		let { createSimpleSearch } = this.props.simpleSearch;
		let printUrl ='/nccloud/obm/ebankautodownlog/print.do'; // 后台输出服务url
		let { printData = {} } = this.state; // 需要输出的数据
		return (
			<div className="nc-bill-list">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						<h2 className='title-search-detail'>
						    {/*页面大图标*/}
							{createPageIcon()}
							{[this.props.MutiInit.getIntl("36101ADLQ") && this.props.MutiInit.getIntl("36101ADLQ").get('36101ADLQ-000004')]+"-"+[this.props.MutiInit.getIntl("36101ADLQ") && this.props.MutiInit.getIntl("36101ADLQ").get('36101ADLQ-000005')]}</h2>{/* 国际化处理： 自动下载日志,查询*/}
					</div>
					{/* 按钮区 btn-area */}
					<div className="header-button-area">
						{createButtonApp({
							area: 'list_head',
							buttonLimit: 3,
							onButtonClick: (props, id) => {
								buttonClick.call(this, props, id, tableid);
							},
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
                <div className="nc-bill-search-area">
					{NCCreateSearch(searchid, {//查询区
						clickSearchBtn: searchBtnClick.bind(this),
						oid: ""
					})}
				</div>
				<div className="table-area">
					{createSimpleTable(tableid, {//列表区
						onAfterEvent: afterEvent,
						handlePageInfoChange: pageInfoClick,
						tableModelConfirm: tableModelConfirm,
						onSelected: this.rowSelected.bind(this),
						onSelectedAll: this.rowSelected.bind(this),
						showCheck: true,
						showIndex: true
					})}
				</div>
				{/* 输出区 printOutput */}
				<PrintOutput ref="printOutput" url={printUrl} data={printData} />
			</div>
		);
	}
}

SingleTable = createPage({
	mutiLangCode: '36101ADLQ',
	initTemplate: initTemplate
})(SingleTable);

ReactDOM.render(<SingleTable />, document.querySelector('#app'));
