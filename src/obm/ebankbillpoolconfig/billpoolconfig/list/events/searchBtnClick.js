import {ajax,toast} from 'nc-lightapp-front';
let tableid = 'ebankbillpoolconfig';
//点击查询，获取查询区数据
export default function clickSearchBtn(props,searchVal) {
    console.log(searchVal);
    let pageInfo = props.table.getTablePageInfo(this.tableid);
    let metaData = props.meta.getMeta();
    let data={
        conditions:searchVal.conditions || searchVal,
        pageInfo:pageInfo,
        pagecode: "36101010_L01",
        queryAreaCode:"36101010",
        //oid:"1001Z61000000000IZN9",
        queryType:"simple"
     };
    console.log(data);
    ajax({
        url: '/nccloud/obm/ebankbillpoolconfig/query.do',//'/nccloud/reva/search/query.do',
        data: data,
        success:  (res)=> {
            let { success, data } = res;
            if(success){
                if(data && data[tableid]){
                    props.table.setAllTableData(tableid, res.data[tableid]);
                }else{
                    props.table.setAllTableData(tableid, {rows:[]});
                    toast({content:props.MutiInit.getIntl("36101010_List") && props.MutiInit.getIntl("36101010_List").get('36101010_List-000003'),color:"warning"});/* 国际化处理： 无数据*/
                }	
            }
        },
        error : (res)=>{
			console.log(res.message);
		}
    });

};
