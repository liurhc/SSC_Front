import { ajax } from 'nc-lightapp-front';
let tableid = 'pk_billpoolconfig_bbank';

export default function (props, config, pks) {
	let data = {
		allpks: pks,
		pageid: "36101010_L01"
	};
	//得到数据渲染到页面
	let that = this;
	ajax({
		url: '/nccloud/obm/ebankbillpoolconfig/querypage.do',
		data: data,
		success: function (res) {
			let { success, data } = res;
			if (success) {
				if (data) {
					props.table.setAllTableData(tableid, data[tableid]);
				} else {
					props.table.setAllTableData(tableid, { rows: [] });
				}
			}
		}
	});
}
