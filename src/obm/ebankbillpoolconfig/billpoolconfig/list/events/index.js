import buttonClick from './buttonClick';
import initTemplate from './initTemplate';
// import afterEvent  from './afterEvent';
import tableModelConfirm from './tableModelConfirm';
//import searchBtnClick from './searchBtnClick';
import doubleClick from './doubleClick';
import pageInfoClick from './pageInfoClick';
export { buttonClick, initTemplate, tableModelConfirm, doubleClick, pageInfoClick };
