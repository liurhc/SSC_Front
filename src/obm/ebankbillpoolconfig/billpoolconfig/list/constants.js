/**
 * 表体区域
 */
export const tableId = 'ebankbillpoolconfig'; 

/**
 * 查询区域编码
 */
// export const searchId = 'search';
 
/**
 * 页面编码
 */
export const pagecode = '36101010_L01';
export const appcode = '36101010';
export const appid = '1001Z61000000001L5VW';

/**
 * 编辑态按钮
 */
export const editButtons = ['Save', 'Cancel'];

/**
 * 浏览态按钮
 */
export const browseButtons = ['Add','Delete','ConnectTest','Refresh','Edit'];