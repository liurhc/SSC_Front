
import { base, ajax } from 'nc-lightapp-front';
// import intl from 'react-intl-universal';
import { tableId, formId,appid, pagecode,appcode,tabs } from "../constants";
import {tableButtonClick} from "./tableButtonClick";
import { initData } from "./cardEvent"; 

let { NCPopconfirm } = base;

export default function(props) {
	props.createUIDom(
		{
			pagecode: pagecode,//页面id
			appcode:appcode
			// appid: appid//注册按钮的id
		},
		(data)=> {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta)
					props.meta.setMeta(meta);

					props.meta.renderTabs(
                        meta,
                        tabs.tabOrder,
                        tabs.tabShow,
                        initData.bind(this, props)
					);
					this.refresh();
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
			}
			
		}
		
	)
}

function modifierMeta(props, meta) {
	let status = props.getUrlParam('status');
	meta[formId].status = status;
	meta[tableId].status = status; 
	let items = meta["head"].items;
	let options = [];
	options[0] = {
		"display": props.MutiInit.getIntl("36101010_Card") && props.MutiInit.getIntl("36101010_Card").get('36101010_Card-000020'),/* 国际化处理： 未启用*/
		"value": 0
	};
	options[1] = {
		"display": props.MutiInit.getIntl("36101010_Card") && props.MutiInit.getIntl("36101010_Card").get('36101010_Card-000021'),/* 国际化处理： 已启用*/
		"value": 1
	};
	items.find((item) => item.attrcode == 'usestate').options = options;
	
	return meta;
}
