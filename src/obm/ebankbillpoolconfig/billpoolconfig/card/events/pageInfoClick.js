import { ajax, cardCache } from 'nc-lightapp-front';
import { tableId,tableId2, formId, pagecode,appcode, editButtons, browseButtons,dataSource,pkname,tabs }  from '../constants';
export default function (props, pks) {
    let {getCacheById,updateCache,addCache,deleteCacheById}=cardCache; 
    props.setUrlParam({id:pks});
    if(pks==null||pks=="null"||pks==undefined){
        return;
    }
    // let cardData=getCacheById(pks,dataSource);
    /*
    * id：数据主键的值
    * dataSource: 缓存数据命名空间
    */

    if (pks) {
        let cardData = getCacheById(pks, dataSource); 
		let keys = tabs.tabShow;
		if(cardData){
			this.props.form.setAllFormValue({ [formId]: cardData.head[formId] });
			this.props.cardTable.setAllTabsData(
				cardData.bodys,
				tabs.tabOrder, 
				null,
				Object.keys(cardData.bodys) == keys
					? keys
					: keys.concat(Object.keys(cardData.bodys))
            );
            this.setState({
                srvid: cardData.head[formId].rows[0].values.srvid.value,
            });
            this.toggleShow();
		}  
        // if (cardData) {
        //     this.props.form.setAllFormValue({ [formId]: cardData.head[formId] });
        //     this.props.cardTable.setTableData(tableId, cardData.body[tableId]);
        //     this.setState({
        //         srvid: cardData.head[formId].rows[0].values.srvid.value,
        //     });
        //     this.toggleShow();
        // } 
        else {
            let dataArr = pks;
            //主键数组
            // dataArr.push(pks);
            let data = {
                pk: dataArr,
                pageCode: pagecode 
            }; 
            ajax({
                url: '/nccloud/obm/ebankbillpoolconfig/cardquery.do',
                data: data,
                success: (res) => { 
                    if (res.data) {  
                        if (res.data.head) {											
                            this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
                            let srvid =res.data.head[formId].rows[0].values.srvid.value;
                            this.setState({
                                srvid:srvid
                            });
                        }
                        if (res.data.bodys) {
                            this.props.cardTable.setTableData(tableId, res.data.bodys[tableId]);
                            this.props.cardTable.setTableData(tableId2, res.data.bodys[tableId2]); 
                        }
                        //添加	添加或更新缓存
                        //存入缓存
                        let pk_billpoolconfig = res.data.head[formId].rows[0].values.pk_billpoolconfig.value;
                        let cardData = getCacheById(pk_billpoolconfig, dataSource);
                        if(cardData){
                            //更新缓存
                            updateCache(pkname,pk_billpoolconfig,res.data,formId,dataSource);
                        }else{
                            //添加缓存
                            addCache(pk_billpoolconfig,res.data,formId,dataSource);
                        }
                        //更新Tab数据
                        this.updateTabData(null);
                        //页面切换
                        this.toggleShow();                   
                    } else {
			this.props.form.EmptyAllFormValue(formId);
                        this.props.cardTable.setTableData(tableId, { rows: [] });
                        this.props.cardTable.setTableData(tableId2, { rows: [] });
			this.toggleShow(); 
			}
                }
            });
        }
    }
}
