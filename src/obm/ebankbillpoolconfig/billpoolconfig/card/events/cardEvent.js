/**
 * 贷款管理卡片公共事件函数
 * @author：zhangyangz
 */
import { ajax,toast,cacheTools,cardCache,deepClone} from "nc-lightapp-front"; 
import { orgVersionView } from "../../../../../tmpub/pub/util/version/index";
import { tableId, formId, pagecode,appcode, editButtons, browseButtons,dataSource,pkname,tabs } from "../constants";

let {
    setDefData,
    getDefData,
    getCacheById,
    updateCache,
    addCache,
    getNextId,
    deleteCacheById
} = cardCache;
  
export function initData(props) { 
    props.cardTable.setAllTabsData(
        null,
        tabs.tabOrder,
        null,
        []
    ); 
    // if (id) { 
        // let cardData = getCacheById(id, cache);
        // if (cardData && cardData.head && cardData.head[formId]) {
        //     //有缓存且不是刷新按钮
        //     cardData.head &&
        //         props.form.setAllFormValue({
        //             [formId]: cardData.head[formId]
        //         });
        //     if (cardData.bodys && JSON.stringify(cardData.bodys) !== "{}") {
        //         let tabDefData = getDefData(tabCache, dataSource);
        //         let tabKeys = (tabDefData && tabDefData.get(id)) || [];
        //         let keys = tabKeys.length ? tabKeys : tabs.tabShow;
        //         props.cardTable.setAllTabsData(
        //             cardData.bodys,
        //             tabs.tabOrder,
        //             afterSetData.bind(
        //                 this,
        //                 props,
        //                 Object.keys(cardData.bodys) == keys
        //                     ? keys
        //                     : keys.concat(Object.keys(cardData.bodys))
        //             ),
        //             Object.keys(cardData.bodys) == keys
        //                 ? keys
        //                 : keys.concat(Object.keys(cardData.bodys))
        //         );
        //     } else {
        //         props.cardTable.setAllTabsData(
        //             null,
        //             tabs.tabOrder,
        //             null,
        //             []
        //         );
        //     }
        //     setState(
        //         {
        //             billNo:
        //                 cardData.head[formId].rows[0].values.vbillno.value
        //         },
        //         () => {
        //             buttonVisible(props);
        //         }
        //     );
        //     return;
        // }
    // }
} 

