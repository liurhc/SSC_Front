import { ajax, base, toast,cardCache,promptBox } from 'nc-lightapp-front';
import { appid, formId, tableId, pagecode,appcode, dataSource} from '../constants.js';
let { getCurrentLastId, updateCache, addCache, getNextId, deleteCacheById } = cardCache;

export default function(props, id) {
    let CardData = this.props.createMasterChildData(pagecode, formId, tableId);
  switch (id) {
    case 'Add':
        //清空卡片上的值 
        props.form.EmptyAllFormValue(formId);
        props.cardTable.setTableData(tableId, { rows: [] }); 
        //设置默认值
        let value={"display":"/ufbank/ufbank.asp?client=java","value":"/ufbank/ufbank.asp?client=java"};
		props.form.setFormItemsValue(formId,{servelt:value});
        //跳转到卡片
        props.pushTo("/card", {   
          status: 'add',
          id:props.getUrlParam('id'),
          type:'link',
          appcode:appcode,
          pagecode:'36101010_C01'
        })
        // this.LoadXiaLa();
        // this.loadNetStyle(CardData); 
        this.toggleShow();
        break;
    case 'Reback':
        props.pushTo("/list", {   
            type:'link',
            appcode:appcode,
            pagecode:'36101010_C01'
          });
        break;
    case 'Save':
        this.saveBill();
        this.toggleShow();
        break
    case 'AsslineDown':
        this.generalAssLineFile();
        this.toggleShow();
        break
    case 'Refresh':
        this.reGetdata();
        this.toggleShow();
        break
    case 'Edit':
        if(!props.getUrlParam('id')){
            toast({ color: 'info', content: props.MutiInit.getIntl("36101010_Card") && props.MutiInit.getIntl("36101010_Card").get('36101010_Card-000024') });/* 国际化处理： 没有数据可以修改！*/
		    return;
        }
        props.pushTo("/card", {   
            status: 'edit',
            id: props.getUrlParam('id'),
            type:'link',
            appcode:appcode,
            pagecode:'36101010_C01'
        })
        // this.LoadXiaLa();
        // this.loadNetStyle(CardData);
        this.toggleShow();
        break;
    case 'Delete':
        promptBox({
            color: "warning",
            title: props.MutiInit.getIntl("36101010_Card") && props.MutiInit.getIntl("36101010_Card").get('36101010_Card-000017'),/* 国际化处理： 删除*/
			content: props.MutiInit.getIntl("36101010_Card") && props.MutiInit.getIntl("36101010_Card").get('36101010_Card-000018'),/* 国际化处理： 确定要删除吗？*/
			beSureBtnClick: () => { 
                this.delConfirm();
            }
        });
        break;
    case 'Cancel': 
        promptBox({
            color: "warning",
            title: props.MutiInit.getIntl("36101010_Card") && props.MutiInit.getIntl("36101010_Card").get('36101010_Card-000000'),/* 国际化处理： 取消*/
            content: props.MutiInit.getIntl("36101010_Card") && props.MutiInit.getIntl("36101010_Card").get('36101010_Card-000001'),/* 国际化处理： 确定要取消吗？*/
            beSureBtnClick: cancelConfirm.bind(this, props)
        }) ;
        break;
    case 'ConnectTest':
        this.connectTest();
        this.toggleShow();
        break;
    case 'AddLine':
        props.cardTable.addRow(tableId) 
        break; 
    case 'DelLine':
        let delcurrRows = props.cardTable.getCheckedRows(tableId);
        let currSelect2 = [];
        if (delcurrRows && delcurrRows.length > 0) {
            for (let item of delcurrRows) {
              currSelect2.push(item.index);
            }
        }
        if (currSelect2.length == 0) {
            toast({ color: 'warning', content: props.MutiInit.getIntl("36101010_Card") && props.MutiInit.getIntl("36101010_Card").get('36101010_Card-000002') });/* 国际化处理： 未选择数据!*/
            return ;
        }
        props.cardTable.delRowsByIndex(tableId, currSelect2);
        break; 
    default:
      break
  }
}


// 编辑保存是取消按钮确认框
const cancelConfirm = function(props) {
    if ((props.getUrlParam('status') === 'edit') || (props.getUrlParam('status') === 'add')){
        let current_pk = props.getUrlParam('id');
        // 表单返回上一次的值
        props.form.cancel(this.formId);
        // 表格返回上一次的值
        if (!current_pk) {
            current_pk = getCurrentLastId(dataSource);
        }
        props.pushTo("/card", {   
            status: 'browse',
            type:'link',
            appcode:appcode,
            id: current_pk,
            pagecode:'36101010_C01'
        }); 
        this.reGetdata();
        this.toggleShow();
    } 
}