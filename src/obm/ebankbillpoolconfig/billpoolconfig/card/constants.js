/**
 * 表体区域
 */
export const tableId = 'pk_billpoolconfig_bbank';
export const tableId2 = 'pk_billpoolconfig_borg';
// 卡片页面tab区域相关编码
export const tabs = {
    tabCode: "pk_billpoolconfig_bbank", //tab区域code编码 
    bodyCode: "tabs_body", //tab区域表格区域按钮code
    tabOrder: [
        "pk_billpoolconfig_bbank",
        "pk_billpoolconfig_borg"
    ], //tab区域排序 
    tabShow: ["pk_billpoolconfig_bbank", "pk_billpoolconfig_borg"], //默认显示tab  
};
/**
 * 表头区域
 */
export const formId = 'head';



/**
 * 页面编码
 */
export const pagecode = '36101010_C01';
export const appcode = '36101010';
export const appid = '1001Z61000000001L5VW';

/**
 * 编辑态按钮
 */
export const editButtons = ['Save','Cancel','AddLine','DelLine'];

/**
 * 浏览态按钮
 * 
 */
export const browseButtons = ['Add','Delete','ConnectTest','Refresh','Edit','AsslineDown'];

export const dataSource = 'tm.obm.ebankbillpoolconfig.billpoolconfig';
export const pkname='pk_billpoolconfig';