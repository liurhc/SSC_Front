import { high } from 'nc-lightapp-front';

const { Refer } = high;

export default function (props = {}) {
	var conf = {
		multiLang: {
            domainName: "obm",
            currentLocales: "zh-CN",
            moduleId: "pubRefer"
        },
		refType: "gridTree", 
		refName : "refer-0007",/* 国际化处理： 账号参照*/
		placeholder: "refer-0007",/* 国际化处理： 账号参照*/
		rootNode: { refname: "refer-0008", refpk: "root" },
		refCode: "obm.refer.pub.ObmBankaccSubDefaultRef",
		queryTreeUrl: "/nccloud/uapbd/ref/BankaccSubUseTreeRef.do",
		queryGridUrl: "/nccloud/obm/refer/ObmBankaccSubDefaultRef.do",
		columnConfig: [
			{
				name:[ "refer-0009","refer-0010","refer-0011","refer-0012","refer-0013","refer-0008" ],
				code:[ "bd_bankaccsub.accnum","bd_bankaccsub.accname","bd_bankaccbas.code","bd_bankaccbas.name","bd_bankaccbas.mnecode","bd_banktype.name"]
			}
		],
		treeConfig:{name:["refer-0011", "refer-0012"],code: ["refcode", "refname"]},
		isMultiSelectedEnabled: false,
	};

	return <Refer {...Object.assign(conf, props)} />
}
