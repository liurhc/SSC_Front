import { high } from 'nc-lightapp-front';
const { Refer } = high;

export default function (props = {}) {
	var conf = {
		multiLang: {
            domainName: "obm",
            currentLocales: "zh-CN",
            moduleId: "pubRefer"
        },
		refType: "grid",
		refName: "refer-0003", 			//多语资源：票据池网银数据下载方案-集团,
		placeholder: "refer-0003", 		//多语资源：票据池网银数据下载方案-集团,
		refCode: "obm.refer.pub.BillpoolDataDownGroupRefModel",	
        queryGridUrl: "/nccloud/obm/refer/BillpoolDataDownGroupRefModel.do", 
        columnConfig: [{name: [ "refer-0011", "refer-0012" ],code: [ "facode", "facode" ]}],
		isMultiSelectedEnabled: true,
        isShowUnit:false
	};

	return <Refer {...Object.assign(conf, props)} />
}