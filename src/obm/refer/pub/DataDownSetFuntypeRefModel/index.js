import { high } from 'nc-lightapp-front';
const { Refer } = high;

export default function (props = {}) {
	var conf = {
		multiLang: {
            domainName: "obm",
            currentLocales: "zh-CN",
            moduleId: "pubRefer"
        },
		refType: 'grid',
		refName: "refer-0015",	//数据下载方案-组织
		placeholder: "refer-0015",	//数据下载方案-组织
		refCode: 'obm.refer.pub.DataDownSetFuntypeRefModel',	
        queryGridUrl: '/nccloud/obm/refer/DataDownSetFuntypeRefModel.do',
        // queryGridUrl:'/nccloud/uapbd/ref/PostDefaultGridRef.do',
        columnConfig: [{name: ["refer-0011", "refer-0012"  ],code: [ 'funcode', 'funtype' ]}],
		isMultiSelectedEnabled: true,
        isShowUnit:false
	};

	return <Refer {...Object.assign(conf, props)} />
}