import { high } from 'nc-lightapp-front';

const { Refer } = high;

export default function (props = {}) {
	var conf = {
		multiLang: {
            domainName: "obm",
            currentLocales: "zh-CN",
            moduleId: "pubRefer"
        },
		refType: "gridTree",
		refName: "refer-0002",
        placeholder: "refer-0002",
		refCode:"obm.refer.pub.ObmBankaccSubGridTreeRef",
		queryTreeUrl: "/nccloud/uapbd/ref/BankaccSubDefaultTreeRef.do",
		queryGridUrl: "/nccloud/obm/refer/ObmBankaccSubGridRef.do",
        columnConfig: [{
			name: [ "refer-0009","refer-0010","refer-0011","refer-0012","refer-0013","refer-0008"],
            code: [ "accnum","accname","refcode", "refname","mnecode","pk_banktype"],
            checked: {
                mnecode: true
            }
        }],
		treeConfig:{name:["refer-0011", "refer-0012"],code: ["refcode", "refname"]},
        rootNode: { refname: "refer-0008", refpk: "root" },
		isMultiSelectedEnabled: false
	};

	return <Refer {...Object.assign(conf, props)} />
}
