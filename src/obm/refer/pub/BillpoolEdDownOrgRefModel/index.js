import { high } from 'nc-lightapp-front';
const { Refer } = high;

export default function (props = {}) {
	var conf = {
		multiLang: {
            domainName: "obm",
            currentLocales: "zh-CN",
            moduleId: "pubRefer"
        },
		refType: 'grid',
		refName: "refer-0006",	//票据池网银额度下载方案-组织
		placeholder: "refer-0006",	//票据池网银额度下载方案-组织
		refCode: 'obm.refer.pub.BillpoolEdDownOrgRefModel',	
        queryGridUrl: '/nccloud/obm/refer/BillpoolEdDownOrgRefModel.do', 
        columnConfig: [{name: ["refer-0011", "refer-0012" ],code: [ 'facode', 'facode' ]}],
		isMultiSelectedEnabled: true,
        isShowUnit:false
	};

	return <Refer {...Object.assign(conf, props)} />
}