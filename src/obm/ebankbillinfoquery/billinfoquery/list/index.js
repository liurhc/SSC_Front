﻿import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, formDownload, toast,createPageIcon } from 'nc-lightapp-front';
import { afterEvent, buttonClick, initTemplate, pageInfoClick, searchBtnClick, tableModelConfirm, onAfterEvent } from './events';
import './index.less';
import { appid, pagecode, tableid, searchid, downloadid, eurinputid, appcode } from './constants.js'; 
// import { high } from 'nc-lightapp-front';
// const { NCUploader } = high;
// import { Upload } from 'tinper-bee';
const { NCModal, NCButton } = base;

class SingleTable extends Component {
	constructor(props) {
		super(props);
		this.props = props;
		this.moduleId = appcode;
		this.state = {
			data: {},
			showmodal: false 
		};
		 
	}  

	getButtonNames = (codeId) => {
		return 'main-button';
	};


	//请求列表数据
	getData = () => {

	}; 

	componentDidMount() {
		this.toggleShow();
	}
	//切换页面状态
	toggleShow = () => {
		this.props.button.setDisabled({
			download: false,
			query:false,
			refresh: false,
			synchrostate:true, 
			printgroupbtn: true,
			output: true 
		});
	}; 
  
	//点击打印按钮，打印单据
	onPrint = () => {
		let printData = this.props.table.getCheckedRows(tableId);
		let pks = [];
		printData.forEach((item) => {
			pks.push(item.data.values.pk_dfgz.value);
		});  
		print(
			'pdf',  //支持两类: 'html'为模板打印, 'pdf'为pdf打印
			'nccloud/print.do', //后台服务url
			{
				nodekey: 'NCCLOUD', //模板节点标识 
				oids: pks   // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
				//  outputType: 'output'   // 打印按钮不用传该参数,输出按钮(文件下载)需加参数outputType,值为output。
			},
            false
		)
	}

	//单击表格，编辑选中行数据
	onSelected = () => {
		let selectDatas = this.props.table.getCheckedRows(tableid);
		//判断是否有选中行
		if (selectDatas == undefined || selectDatas.length == 0) {
			this.props.button.setDisabled({ printgroupbtn: true });
			this.props.button.setDisabled({ output: true });
			this.props.button.setDisabled({ synchrostate: true });
			return;
		} else {
			this.props.button.setDisabled({ printgroupbtn: false });
			this.props.button.setDisabled({ output: false });
			this.props.button.setDisabled({ synchrostate: false });
		}
	}



	render() {
		let { table, button, form, search, modal } = this.props;
		let buttons = this.props.button.getButtons();
		let { createSimpleTable } = table;
		let { createButtonApp } = this.props.button;
		let { createButton, getButtons } = button;
		let { createForm } = form;
		let { NCCreateSearch } = search;
		let { createModal } = modal;
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		// let Uploader = this.props.Uploader;
		return (
			<div className="nc-bill-list">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						<h2 className='title-search-detail'>
						{/*页面大图标*/}
						{createPageIcon()}
						{multiLang && multiLang.get('361010CX-000011')/* 国际化处理： 电票信息查询*/}</h2>
					</div>
					<div className="header-button-area">
						{/* 小应用注册按钮 */}
						{this.props.button.createButtonApp({
							area: 'list_head',
							buttonLimit: 4,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				<div className="nc-bill-search-area">
					{NCCreateSearch(searchid, {
						showAdvBtn: false,
						clickSearchBtn: searchBtnClick.bind(this),
						defaultConditionsNum: 4, //默认显示几个查询条件						
						onAfterEvent: onAfterEvent.bind(this) //编辑后事件
					})}
				</div>
				{/* <div style={{height:"10px"}}></div> */}
				<div className="nc-bill-table-area" style={{ borderRadius: "3px 3px 0 0", overflow: "hidden" }}>
					{createSimpleTable(tableid, {//列表区
						onAfterEvent: afterEvent,
						handlePageInfoChange: pageInfoClick,
						tableModelConfirm: tableModelConfirm,
						onSelected: this.onSelected,
						onSelectedAll: this.onSelected,
						adaptionHeight: true,
						showCheck: true,
						showIndex: true
					})}
				</div>
   
				{createModal(downloadid, {
					title: multiLang && multiLang.get('361010CX-000009')/* 国际化处理： 在线下载*/,
					className: 'senior',
					hasCloseBtn: true,
					style: { width: "520px", height: "268px" },
					content:
						<div className="form-wrapper">
							{createForm(downloadid, {
								style: { width: "520px", height: "268px" },
								//编辑后事件
								onAfterEvent: afterEvent.bind(this)
							})}

						</div>
					,
					beSureBtnClick: () => {
						//数据校验
						const queryData = form.getAllFormValue(downloadid);

						if (form.isCheckNow(downloadid)) {
							let data = {
								model: {
									pageInfo: {
										pageIndex: 0,
										pageSize: 10,
										total: 0,
										totalPage: 0
									},
									...queryData
								},
							};
							ajax({
								url: '/nccloud/obm/ebankbillinfoquery/download.do',
								data,
								success: (res) => {
									let { success, data } = res;
									if (success) {
										if (data && data[tableid]) {
											this.props.table.setAllTableData(tableid, data[tableid]);
										} else {
											this.props.table.setAllTableData(tableid, { rows: [] });
										}
									}
								}
							});
						}
					}
				})} 

			</div>
		);
	}
}

SingleTable = createPage({
	mutiLangCode: appcode,
	initTemplate: initTemplate
})(SingleTable);

ReactDOM.render(<SingleTable />, document.querySelector('#app'));
