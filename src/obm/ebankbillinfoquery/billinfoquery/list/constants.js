/**
 * 页面ID
 */
export const appid = '1001Z61000000001L5VL';
export const appcode = '361010CX';
/**
 * 页面编码
 */
export const pagecode = '361010CX_L01';
export const funcode = '361010CX';
/**
 * 表体区域
 */
export const tableid = 'ebank_billinfoquery'; 
/**
 * 查询区域
 */
export const searchid = 'search'; 
export const searchCashCode = 'search_billinfoquery';
/**
 * 弹出对话框区域
 */
export const downloadid = 'download'; 

