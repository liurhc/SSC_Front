import {ajax} from 'nc-lightapp-front';
import {pagecode,tableid,searchid} from '../constants.js';

export default function (props, config, pks) {
    //let pageInfo = props.table.getTablePageInfo(tableid);
    //let searchVal = props.search.getAllSearchData(searchid);   
    // 后台还没更新，暂不可用
    let data = {
        "pks": pks,
        "pageid": pagecode
    };
    ajax({
        url: '/nccloud/obm/ebankbillinfoquery/pagequery.do',
        data: data,
        async: true,
        success: function (res) {
            props.table.setAllTableData(tableid, res.data[tableid])
        }
    });
}