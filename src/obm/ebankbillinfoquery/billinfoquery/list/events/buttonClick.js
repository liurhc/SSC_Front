﻿import { ajax,base,print,output,toast,cacheTools } from 'nc-lightapp-front';  
import {downloadid,inputid,tableid,funcode,appcode,searchCashCode} from '../constants.js';
import searchBtnClick from './searchBtnClick'; 

export default function buttonClick(props, id) { 
	switch (id) {		
		case 'download':				
			this.props.modal.show(downloadid);
			break;
		case 'query':
			this.props.modal.show('query');
			break; 
		case 'synchrostate':
			syncstatus(props);
			break;
		case 'printgroupbtn':
			printData(props);
			break;
		case 'output': 
			outputData(props); 
			break; 
		case 'refresh':  
			let searchData = cacheTools.get(searchCashCode); 
			searchBtnClick(props,searchData,true);
			break; 
	}
}

function printData(props){
	let printData = props.table.getCheckedRows(tableid);
		let pks = [];
		printData.forEach((item) => {
			pks.push(item.data.values.pk_ebankbillinfoquery.value);
		});  
		print(
			'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
			'/nccloud/obm/ebankbillinfoquery/print.do',
			{
				funcode:funcode, //功能节点编码，即模板编码
				//nodekey: 'web_print', //模板节点标识
				appcode:appcode,
				// printTemplateID: '1001Z610000000003SNR',
				oids: pks // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
				// outputType: 'output'
			}
		);
}

function outputData(props){
	let printData = props.table.getCheckedRows(tableid);
		let pks = [];
		printData.forEach((item) => {
			pks.push(item.data.values.pk_ebankbillinfoquery.value);
		});  
		output({
			url: '/nccloud/obm/ebankbillinfoquery/print.do',
			data: { 
			  	oids: pks,
			  	outputType: 'output'
			}
		});
}

function syncstatus(props){
	let printData = props.table.getCheckedRows(tableid);
		let pks = [];
		printData.forEach((item) => {
			pks.push(item.data.values.pk_ebankbillinfoquery.value);
		}); 
		ajax({
			url: '/nccloud/obm/ebankbillinfoquery/syncstatus.do',
			data:{
				pks:pks
			},
			success: (res) => {
				//同步成功
				toast({ color: 'success', content: multiLang && multiLang.get('361010CX-000007') }); /* 国际化处理： 同步成功*/
			}
		}); 
}