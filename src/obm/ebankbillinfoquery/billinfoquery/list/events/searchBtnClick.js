import {ajax,toast,cacheTools} from 'nc-lightapp-front';
import {appid,pagecode,tableid,searchid,searchCashCode,appcode} from '../constants.js';

//点击查询，获取查询区数据
export default function clickSearchBtn(props,searchVal,refreshflag) {
    if(searchVal){
        let pageInfo = props.table.getTablePageInfo(tableid);
        let data={
            conditions:searchVal.conditions || searchVal,
            pageInfo:pageInfo,
            pagecode: pagecode,
            queryAreaCode:searchid,  //查询区编码
            oid: appid,  //查询模板id，手工添加在界面模板json中，放在查询区，后期会修改
            queryType:'simple'
        };
        cacheTools.set(searchCashCode, searchVal);
        ajax({ 
            url: '/nccloud/obm/ebankbillinfoquery/query.do',
            data: data,
            success: (res) => {
                let { success, data } = res;
                if (success) {
                    if(data){
                        if(refreshflag){
                            toast({
                                duration: 6,
                                color: 'success',
                                content: props.MutiInit.getIntl(appcode) && props.MutiInit.getIntl(appcode).get('361010CX-000017')/* 国际化处理： 刷新成功*/
                            })
                        }else{
                            giveToast(props, data[tableid].allpks.length);
                        }                       
                        props.table.setAllTableData(tableid, data[tableid]);
                    }else{
                        props.table.setAllTableData(tableid, {rows:[]});
                         giveToast(props, ); 
                    }
                    
                }
            }
        });
    }
    
};

function giveToast(props, resLength) {
    let multiLang = props.MutiInit.getIntl(appcode);
    if (resLength && resLength > 0) {
        let contentHead = multiLang && multiLang.get('361010CX-000001');/* 国际化处理： 查询成功，共*/
        let contentEnd = multiLang && multiLang.get('361010CX-000002');/* 国际化处理： 条*/
        toast({
            duration: 6,
            color: 'success',
            content: contentHead + resLength + contentEnd
        })
    }
    else {
        toast({
            duration: 6,
            color: 'warning',
            content: multiLang && multiLang.get('361010CX-000003')/* 国际化处理： 未查询到符合条件的数据*/
        })
    }
}