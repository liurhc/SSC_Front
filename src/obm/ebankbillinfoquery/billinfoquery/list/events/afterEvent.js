import { ajax } from 'nc-lightapp-front';
import {inputid,downloadid,searchid} from '../constants.js';

export default function (props, moduleId , key, changerows, value, data, index) {
    const { form } = this.props;
    if(moduleId===downloadid){
        if (key === 'pk_banktype') {
            //银行类别切换 银行账号清空
            form.setFormItemsValue(moduleId, { 'curacc': { value: '', display: '' } }); 

        } else if (key === 'pk_org') {
            const pk_org = form.getFormItemsValue(moduleId, 'pk_org').value;
            if (pk_org) {

                //财务组织切换 银行类别 银行账号清空
                if (form.getFormItemsValue(moduleId, 'pk_banktype').value) {
                    form.setFormItemsValue(moduleId, { 'pk_banktype': { value: '', display: '' } });
                }

                if (form.getFormItemsValue(moduleId, 'curacc').value) {
                    form.setFormItemsValue(moduleId, { 'curacc': { value: '', display: '' } });
                }
            }
        } else if (key === 'curacc') {
            //选择银行账号 
            let pk_banktype = '';
            let banktypename = '';
            //TODO 此处有bug 选择多个银行账户 之后删除一个后 获取不到参照的其它信息
            if (data.values['bd_banktype.pk_banktype']) {
                pk_banktype = data.values['bd_banktype.pk_banktype'].value;
                banktypename = data.values['bd_banktype.name'].value;
                form.setFormItemsValue(moduleId, { 'pk_banktype': { value: pk_banktype, display: banktypename } });
            }else{
                pk_banktype = '';
                banktypename = '';
                form.setFormItemsValue(moduleId, { 'pk_banktype': { value: pk_banktype, display: banktypename } });
            } 
            
        }
    }
    
}