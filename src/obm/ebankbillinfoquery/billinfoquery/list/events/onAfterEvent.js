import {searchid} from '../constants.js';
export default function (field, val) {

    const { search } = this.props;

    if (field === 'pk_banktype') {
        //银行类别切换 银行账号清空
        search.setSearchValByField(searchid, 'curacc', {
            value: '',
            display: ''
        })

    } else if (field === 'pk_org') {

        if (val.refpk) {
            //财务组织切换 银行类别 银行账号清空
            if (search.getSearchValByField(searchid, 'pk_banktype')) {
                search.setSearchValByField(searchid, 'pk_banktype', {
                    value: '',
                    display: ''
                })
            }
            if (search.getSearchValByField(searchid, 'curacc')) {
                search.setSearchValByField(searchid, 'curacc', {
                    value: '',
                    display: ''
                })
            }
        }
    } else if (field === 'curacc') {
        //选择银行账号 
        let pk_banktype = '';
        let banktypename = '';
        if (val.length > 0 && val[0].values) {
            pk_banktype = val[0].values.bd_banktypeid.value;
            banktypename = val[0].values.pk_banktype.value;
            search.setSearchValByField(searchid, 'pk_banktype', {
                value: pk_banktype,
                display: banktypename
            }) 
        }  
    }
}
