﻿import { ajax, base } from 'nc-lightapp-front'; 
import {appid,appcode,pagecode,funcode,tableid,searchid,downloadid,inputid,eurinputid} from '../constants.js';

export default function(props) {
	//请求模板数据
	props.createUIDom(
		{
			pagecode: pagecode,//页面id
			appcode: appcode //注册按钮的id	
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template; 
					//TODO 新盘去掉 
					meta[downloadid].status = 'edit'; 
					meta = modifierMeta(props, meta);
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button); 
					props.button.setDisabled({printgroupbtn:true});
					props.button.setDisabled({output:true});
				} 
			}
		}
	)
} 

function modifierMeta(props, meta) {
	let multiLang = props.MutiInit.getIntl(appcode);
	const { search, form } = props;
	// //设置银行账号多选
	// meta[searchid].items.find((e) => e.attrcode === 'curacc').isMultiSelectedEnabled = true;
	// // meta[searchid].items.find((e) => e.attrcode === 'pk_org').isMultiSelectedEnabled = true;
	// meta[downloadid].items.find((e) => e.attrcode === 'bankacc').isMultiSelectedEnabled = true; 
	// //查询界面字段增加过滤条件
	// meta[searchid].items.map((item) => {
	// 	if (item.attrcode == 'pk_org') {
	// 		item.queryCondition = () => {
	// 			return {
	// 				funcode: funcode,
	// 				TreeRefActionExt: 'nccloud.web.tmpub.filter.FinanceOrgPermissionFilter' 
	// 			};
	// 		};
	// 	} else if (item.attrcode == 'curacc') {
	// 		item.queryCondition = () => {

	// 			let pk_org = null;
	// 			let pk_banktype = null;
	// 			if (search.getSearchValByField(searchid, 'pk_org')) {
	// 				pk_org = search.getSearchValByField(searchid, 'pk_org').value.firstvalue;
	// 			}

	// 			if (search.getSearchValByField(searchid, 'pk_banktype')) {
	// 				pk_banktype = search.getSearchValByField(searchid, 'pk_banktype').value.firstvalue;
	// 			}

	// 			return {
	// 				refnodename: multiLang && multiLang.get('361010CX-000000')/* 国际化处理： 核算归属权*/,
	// 				funcode: funcode,
	// 				pk_org: pk_org,
	// 				pk_banktype: pk_banktype,
	// 				GridRefActionExt: 'nccloud.web.obm.refbuilder.BankAccRefBuilder'
	// 			};
	// 		};

	// 	}
	// });
	// //下载界面字段增加过滤条件
	// meta[downloadid].items.map((item) => {
	// 	if (item.attrcode == 'pk_org') {
	// 		item.queryCondition = () => {
	// 			return {
	// 				funcode: funcode,
	// 				TreeRefActionExt: 'nccloud.web.tmpub.filter.FinanceOrgPermissionFilter' 
	// 			};
	// 		};
	// 	} else if (item.attrcode == 'bankacc') {
	// 		item.queryCondition = () => {

	// 			let pk_org2 = null;
	// 			let pk_banktype2 = null;
	// 			if (form.getFormItemsValue(downloadid, 'pk_org')) {
	// 				pk_org2 = form.getFormItemsValue(downloadid, 'pk_org').value;
	// 			}

	// 			if (form.getFormItemsValue(downloadid, 'banktype')) {
	// 				pk_banktype2 = form.getFormItemsValue(downloadid, 'banktype').value;
	// 			}

	// 			return {
	// 				refnodename: multiLang && multiLang.get('361010CX-000000')/* 国际化处理： 核算归属权*/,
	// 				funcode: funcode,
	// 				pk_org: pk_org2,
	// 				pk_banktype: pk_banktype2,
	// 				GridRefActionExt: 'nccloud.web.obm.refbuilder.BankAccRefBuilder'
	// 			};
	// 		};

	// 	}
	// }); 
	return meta;
}