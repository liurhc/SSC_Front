import { ajax, toast } from 'nc-lightapp-front';
import { save,setStatus } from './buttonClick';
let searchid = '36100501';
let tableid = 'MerchantNoVO';
//点击查询，获取查询区数据
export default function searchBtnClick(props,value) {
	//列表编辑态，切换组织或从新查询，应提示是否保存当前录入的信息
	let status = props.editTable.getStatus(tableid);
	if(status=='edit'){
		// props.ncmodal.show(`enableModal`, {
		// 	title: [props.MutiInit.getIntl("36100501") && props.MutiInit.getIntl("36100501").get('36100501-000021')],/* 国际化处理： 提示*/
		// 	content: [props.MutiInit.getIntl("36100501") && props.MutiInit.getIntl("36100501").get('36100501-000022')],/* 国际化处理： 是否保存当前录入的信息？*/
		// 	beSureBtnClick: () => {
		// 		save.call(this, props);
		// 	},
		// 	cancelBtnClick: () => {
		// 		setStatus.call(this, props, 'browse');
		// 		query.call(this, props,value);
		// 	}
		// });
	}else{
		query.call(this, props,value);
	}
}
export function query(props,value){
	if(value){
		let conditions = [];
		let searchVal = props.search.getAllSearchData(searchid);
		//获取查询模板信息
		let queryInfo = props.search.getQueryInfo(searchid);
		let oid = queryInfo.oid;
		let data = {
			conditions: searchVal == null ? null : searchVal.conditions,
			custconditions: conditions,
			pagecode: "36100501",
			queryAreaCode: searchid,  //查询区编码
			oid: oid,  //查询模板id，手工添加在界面模板json中，放在查询区，后期会修改
			queryType: 'simple'
		};

		ajax({
			url: '/nccloud/obm/ebankmerchantno/queryschem.do',
			data,
			success: (res) => {
				console.log(res);
				let { success, data } = res;
				if (success) {
					if (data && data[tableid]) {
						props.editTable.setTableData(tableid, res.data[tableid]);
						toast({ content: [this.props.MutiInit.getIntl("36100501") && this.props.MutiInit.getIntl("36100501").get('36100501-000023')]+res.data[tableid].rows.length+[this.props.MutiInit.getIntl("36100501") && this.props.MutiInit.getIntl("36100501").get('36100501-000024')], color: "success" });/* 国际化处理： 查询成功,共,条*/
					} else {
						props.editTable.setTableData(tableid, { rows: [] });
						toast({ content: this.props.MutiInit.getIntl("36100501") && this.props.MutiInit.getIntl("36100501").get('36100501-000025'), color: "warning" });/* 国际化处理： 无数据*/
					}
					this.setState(this.state);
				}
			},
			error: (res) => {
				console.log(res.message);
			}
		});
	}
}
