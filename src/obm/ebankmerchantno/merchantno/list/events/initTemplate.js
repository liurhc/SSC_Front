﻿import { ajax, excelImportconfig } from 'nc-lightapp-front';
import { setStatus } from './buttonClick';
// const { NCPopconfirm, NCIcon, NCMessage } = base;
// import Utils from '../../../../../uap/public/utils';
// const { excelImportconfig } = Utils;
let tableid = 'MerchantNoVO';
let pageId = '36100501_L01';
let searchid = '36100501';
export default function (props) {
	let excelimportconfig = excelImportconfig(props,"obm", "36100501", true, "", { "appcode": "36100501", "pagecode": "36100501_L01"});
	//请求模板数据
	props.createUIDom(
		{
			pagecode: pageId,//页面id
			appcode: '36100501' //注册按钮的code  id'0001Z61000000004M04R'
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta);
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					setStatus.call(this, props, 'browse');
					props.button.setButtonDisabled([ 'Edit','Delete', 'SetPwd', 'Enable', 'Disable'], true);
					props.button.setButtonDisabled(['Add', 'Input', 'output', 'Refresh'], false);
					// let excelimportconfig = Utils.excelImportconfig(props, "obm", "36100501", true, "", { "appcode": "36100501", "pagecode": "36100501_L01"});
					props.button.setUploadConfig("Input", excelimportconfig);
				}

			}
		}
	)
}
function modifierMeta(props, meta) {
	meta[searchid].items = meta[searchid].items.map((item, key) => {
		item.visible = true;
		return item;
	});
	meta[searchid].items.map((ele) => {
		ele.visible = true;
	});
	//财务组织用户过滤
	meta[searchid].items.map((item) => {
		if (item.attrcode == 'pk_org') {
			item.queryCondition = () => {
				return {
					funcode: '36100501',
					TreeRefActionExt: 'nccloud.web.tmpub.filter.FinanceOrgPermissionFilter'
				};
			};
		}
	});
	meta[tableid].items.map((item) => {
		if (item.attrcode == 'pk_org') {
			item.queryCondition = () => {
				return {
					funcode: '36100501',
					TreeRefActionExt: 'nccloud.web.tmpub.filter.FinanceOrgPermissionFilter'
				};
			};
		}
	});
	return meta;
}


//刷新页面数据方法
function refreshFun(props) {
	
}

