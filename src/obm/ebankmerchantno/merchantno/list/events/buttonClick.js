﻿import { ajax, toast } from 'nc-lightapp-front';
// import ExcelImport from 'nc-lightapp-front';
// const { Message } = base;
const tableid = 'MerchantNoVO';
let pageId = '36100501_L01';
let searchid = '36100501';
export default function buttonClick(props, id) {
	console.log(id)
	switch (id) {
		case 'Add':
			addRow.call(this, props);
			break;
		case 'Edit':
			edit.call(this, props);
			break;
		case 'Delete':
			batchDelRow.call(this, props);
			break;
		case 'Save':
			save.call(this, props);
			break;
		case 'Cancel':
			cancel.call(this, props);
			// this.loadData();
			break;
		case 'Refresh':
			// query.call(this, props);
			refreshAction.call(this, props);
			break;
		case 'Input':
			// this.InputClick();
			// setStatus.call(this, props, 'browse');
			break;
		// case 'InputGrp':
		// 	this.InputClick();
		// 	setStatus.call(this, props, 'browse');
		// 	break;
		case 'output':
			this.DownloadFileClick();
			// Output.call(this, props);
			break;
		case 'Enable':
			enableAction.call(this, props);
			setStatus.call(this, props, 'browse');
			break;
		case 'Disable':
			disableAction.call(this, props);
			setStatus.call(this, props, 'browse');
			break;
		case 'SetPwd':
			let checkedRows = props.editTable.getCheckedRows(tableid);
			if (checkedRows.length == 0) {
				toast({ content: props.MutiInit.getIntl("36100501") && props.MutiInit.getIntl("36100501").get('36100501-000000'), color: 'warning' });/* 国际化处理： 请先选中需要设置的数据*/
				return;
			}
			this.InputClick();
			break;

		default:
			break;
	}
}
/**
 * 下载联行号
 * @param {*} props 
 */
export function downloadlhhAction(props) {
	ajax({
		url: '/nccloud/obm/ebankmerchantno/downloadlhh.do',
		data: null,
		success: (res) => {
			let { success } = res;
			if (success) {
				toast({ color: 'success', content: res.data });
			}
		},
		error: (res) => {
			toast({ color: 'danger', content: res.message });
		}
	});
}
/**
 * 启用
 * @param {*} props 
 */
export function enableAction(props) {
	let checkedRows = props.editTable.getCheckedRows(tableid);
	if (checkedRows.length == 0) {
		toast({ content: props.MutiInit.getIntl("36100501") && props.MutiInit.getIntl("36100501").get('36100501-000001'), color: 'warning' });/* 国际化处理： 请先选中需要启用的数据*/
		return;
	}
	props.ncmodal.show(`enableModal`, {
		title: props.MutiInit.getIntl("36100501") && props.MutiInit.getIntl("36100501").get('36100501-000002'),/* 国际化处理： 启用*/
		content: props.MutiInit.getIntl("36100501") && props.MutiInit.getIntl("36100501").get('36100501-000003'),/* 国际化处理： 是否确认启用？*/
		beSureBtnClick: () => {
			this.enableAction();
		}
		
	});
}
/**
 * 停用
 * @param {*} props 
 */
export function disableAction(props) {
	let checkedRows = props.editTable.getCheckedRows(tableid);
	if (checkedRows.length == 0) {
		toast({ content: props.MutiInit.getIntl("36100501") && props.MutiInit.getIntl("36100501").get('36100501-000004'), color: 'warning' });/* 国际化处理： 请先选中需要停用的数据*/
		return;
	}
	props.ncmodal.show(`enableModal`, {
		title: props.MutiInit.getIntl("36100501") && props.MutiInit.getIntl("36100501").get('36100501-000005'),/* 国际化处理： 停用*/
		content: props.MutiInit.getIntl("36100501") && props.MutiInit.getIntl("36100501").get('36100501-000006'),/* 国际化处理： 是否确认停用？*/
		beSureBtnClick: () => {
			this.disableAction();
		}
		
	});
}
/**
 * 刷新
 * @param {*} props 
 */
export function refreshAction(props) {
	this.loadData();
}
/**
 * 新增
 * @param {*} props 
 */
export function addRow(props, autoFocus = true) {
	setStatus.call(this, props, 'edit');
	let allrows = props.editTable.getNumberOfRows(tableid);
	ajax({
		url: '/nccloud/obm/ebankmerchantno/add.do',
		data: {
			'pagecode': pageId,
			'areacode': tableid
		},
		success: (res) => {
			let { success, data } = res;
			if (success) {
				props.editTable.setStatus(tableid, 'edit');
				let defaultvalue = {
					pk_group: data[tableid]['rows'][0]['values']['pk_group'],
					enablestate: data[tableid]['rows'][0]['values']['enablestate']
				}
				props.editTable.addRow(tableid, allrows, true, defaultvalue);
				let meta = props.meta.getMeta();
				props.meta.setMeta(meta);
			}
		}
	});

}

/**
 * 修改
 * @param {*} props 
 */
export function edit(props) {
	let allRows = props.editTable.getAllRows(tableid, true);
	if (allRows.length == 0) {
		toast({ color: 'info', content: props.MutiInit.getIntl("36100501") && props.MutiInit.getIntl("36100501").get('36100501-000007') });/* 国际化处理： 没有数据可以修改！*/
		return;
	}
	setStatus.call(this, props, 'edit');
	}

/**
 * 批量删除
 * @param {*} props 
 */
export function batchDelRow(props) {
	let checkedRows = props.editTable.getCheckedRows(tableid);
	if (checkedRows.length == 0) {
		toast({ content: props.MutiInit.getIntl("36100501") && props.MutiInit.getIntl("36100501").get('36100501-000008'), color: 'warning' });/* 国际化处理： 请先选中需要删除的数据*/
		return;
	}
	let indexArr = [];
	checkedRows.map((row) => {
		indexArr.push(row.index);
		let recordVal = row.data.values;
	});
	let status = props.editTable.getStatus(tableid);
	if (status == undefined || status == 'browse') {
		if (checkedRows.length == 1) {
			props.ncmodal.show(`${tableid}-del-confirm`, {
				title: props.MutiInit.getIntl("36100501") && props.MutiInit.getIntl("36100501").get('36100501-000009'),/* 国际化处理： 删除*/
				content: props.MutiInit.getIntl("36100501") && props.MutiInit.getIntl("36100501").get('36100501-000010'),/* 国际化处理： 确定要删除吗？*/
				beSureBtnClick: () => {
					batchDel.call(this, props, indexArr);
				},
				// rightBtnName: props.MutiInit.getIntl("36100501") && props.MutiInit.getIntl("36100501").get('36100501-000011')/* 国际化处理： 取消*/
			});
		}else{
			props.ncmodal.show(`${tableid}-del-confirm`, {
				title: props.MutiInit.getIntl("36100501") && props.MutiInit.getIntl("36100501").get('36100501-000009'),/* 国际化处理： 删除*/
				content: props.MutiInit.getIntl("36100501") && props.MutiInit.getIntl("36100501").get('36100501-000012'),/* 国际化处理： 确定要删除所选数据吗？*/
				beSureBtnClick: () => {
					batchDel.call(this, props, indexArr);
				},
				// rightBtnName: props.MutiInit.getIntl("36100501") && props.MutiInit.getIntl("36100501").get('36100501-000011')/* 国际化处理： 取消*/
			});
		}
	} else {
		delRow.call(this, props, indexArr);
	}
}

/**
 * 前台删除
 * @param {*} props 
 * @param {*} index 
 */
export function delRow(props, index) {
	props.editTable.deleteTableRowsByIndex(tableid, index);
}

/**
 * 浏览态直接删除单个数据
 * @param {*} props 
 * @param {*} tableid 
 * @param {*} index 
 */
export function singleDel(props, index) {
	let allRows = props.editTable.getAllRows(tableid);
	allRows[index].status = '3';
	let changedRows = [allRows[index]];
	const data = {
		pageid: pageId,
		model: {
			areaType: 'table',
			areacode: tableid,
			rows: changedRows
		}
	};
	ajax({
		url: '/nccloud/obm/ebankmerchantno/save.do',
		data,
		success: (res) => {
			let { success } = res;
			if (success) {
				props.editTable.deleteTableRowsByIndex(tableid, index);
				toast({ color: 'success', content: props.MutiInit.getIntl("36100501") && props.MutiInit.getIntl("36100501").get('36100501-000013') });/* 国际化处理： 删除成功*/
			}
		},
		error: (res) => {
			toast({ color: 'danger', content: res.message });
		}
	});
}

/**
 * 后台删除
 * @param {*} props 
 */
export function batchDel(props) {
	let checkedRows = props.editTable.getCheckedRows(tableid);
	if (!checkedRows || checkedRows.length == 0) {
		return;
	}
	let changedIndexs = [];
	let delRows = [];
	// 设置删除状态
	checkedRows.map((item, index) => {
		// 0原始1修改2新增3删除
		item.data.status = '3';
		changedIndexs.push(item.index);
		delRows.push(item.data);
	});
	const data = {
		pageid: pageId,
		model: {
			areaType: 'table',
			areacode: tableid,
			rows: delRows
		}
	};
	ajax({
		url: '/nccloud/obm/ebankmerchantno/save.do',
		data,
		success: (res) => {
			let { success } = res;
			if (success) {
				props.editTable.deleteTableRowsByIndex(tableid, changedIndexs);
				toast({ color: 'success', content: props.MutiInit.getIntl("36100501") && props.MutiInit.getIntl("36100501").get('36100501-000013') });/* 国际化处理： 删除成功*/
			}
		},
		error: (res) => {
			toast({ color: 'danger', content: res.message });
			checkedRows.map((item, index) => {
				// 0原始1修改2新增3删除
				item.data.status = '0';
			});
		}
	});
}
/**
 * 表格数启用状态值转化
 * @param rows数组
 */
export function convertGridEnablestate(rows,props, enablestate = 'enablestate') {
	if (rows && rows.length > 0) {
		rows.map((ele, key) => {
			if (ele.values[enablestate].value === props.MutiInit.getIntl("36100501") && props.MutiInit.getIntl("36100501").get('36100501-000014')) {/* 国际化处理： 未启用*/
				ele.values[enablestate].value = '1';
			} else if (ele.values[enablestate].value === props.MutiInit.getIntl("36100501") && props.MutiInit.getIntl("36100501").get('36100501-000015')) {/* 国际化处理： 已启用*/
				ele.values[enablestate].value = '2';
			} else if (ele.values[enablestate].value === props.MutiInit.getIntl("36100501") && props.MutiInit.getIntl("36100501").get('36100501-000016')) {/* 国际化处理： 已停用*/
				ele.values[enablestate].value = '3';
			}
		});
	}
	return rows;
}
/**
 * 保存
 * @param {*} props 
 */
export function save(props) {
	let changedRows = props.editTable.getChangedRows(tableid);
	let allRequire=props.editTable.checkRequired(tableid,changedRows);
	if(!allRequire){
		return;
	}
	props.editTable.filterEmptyRows(tableid, ['pk_org', 'merchantno', 'def1']);
	
	if (!changedRows || changedRows.length == 0) {
		props.editTable.cancelEdit(tableid);
		props.editTable.setStatus(tableid, 'browse');
		setStatus.call(this, props, 'browse');
		// this.loadData();
		return;
	}
	changedRows = convertGridEnablestate.call(this, changedRows,props);
	const data = {
		pageid: pageId,
		model: {
			areaType: 'table',
			areacode: tableid,
			rows: changedRows
		}
	};
	ajax({
		url: '/nccloud/obm/ebankmerchantno/save.do',
		data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				let allRows = props.editTable.getAllRows(tableid);
				let retData = data[tableid];
				let newRows = [];
				let i = 0;
				allRows.map((item, index) => {
					// 0原始1修改2新增3删除
					if (item.status == '1' || item.status == '2') {
						if (retData && retData.rows && retData.rows.length > i) {
							newRows.push(retData.rows[i]);
						}
						i++;
					} else if (item.status == '3') {
						i++;
					} else {
						newRows.push(item);
					}
				});
				let allData = props.editTable.getAllData(tableid);
				allData.rows = newRows;
				props.editTable.setTableData(tableid, allData);
				setStatus.call(this, props, 'browse');
				toast({ color: 'success', content: props.MutiInit.getIntl("36100501") && props.MutiInit.getIntl("36100501").get('36100501-000017') });/* 国际化处理： 保存成功*/
			}
		},
		error: (res) => {
			toast({ color: 'danger', content: res.message });
		}
	});
}

/**
 * 取消
 * @param {*} props 
 */
export function cancel(props) {
	props.ncmodal.show(`${tableid}-del-confirm`, {
		title: props.MutiInit.getIntl("36100501") && props.MutiInit.getIntl("36100501").get('36100501-000011'),/* 国际化处理： 取消*/
		content: props.MutiInit.getIntl("36100501") && props.MutiInit.getIntl("36100501").get('36100501-000018'),/* 国际化处理： 确定要取消吗？*/
		beSureBtnClick: () => {
			props.editTable.cancelEdit(tableid);
			props.editTable.setStatus(tableid, 'browse');
			setStatus.call(this, props, 'browse');
		}
	});
}

/**
 * 导出
 */
export function Output(props) {
	let outputData = this.props.editTable.getCheckedRows(tableid);
	let pk_bills = [];
	outputData.forEach((val) => {
		pk_bills.push(val.data.values.pk_merchantno.value);
	});
	if (pk_bills.length == 0) {
		toast({ color: 'warning', content: props.MutiInit.getIntl("36100501") && props.MutiInit.getIntl("36100501").get('36100501-000019') });/* 国际化处理： 请选中一行数据!*/
		return;
	}
	console.log(pk_bills);
	this.setState({
		selectedPKS: pk_bills  //传递主键数组,之前nc需要导出的加主键
	}, () => {
		debugger;
		this.props.modal.show('exportFileModal');//不需要导出的只执行这行代码
	})
}

/**
 * 设置界面状态
 * @param {*} props 
 * @param {*} status 
 */
export function setStatus(props, status) {
	let browseBtns = ['Add', 'Edit', 'Delete', 'SetPwd', 'Enable', 'Disable', 'Input', 'output', 'Refresh'];
	let editBtns = ['Save', 'Cancel'];
	switch (status) {
		case 'edit':
			props.editTable.setStatus(tableid, 'edit');
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(browseBtns, false);
			props.button.setButtonVisible(editBtns, true);
			// 保存为主要按钮
			editBtns.map((item) => {
				if (item == 'Save') {
					props.button.setMainButton(item, true);
				} else {
					props.button.setMainButton(item, false);
				}
			});
			// 行删除时不需要悬浮框提示
			props.button.setPopContent('Delete', undefined);
			props.search.setDisabled(searchid,true);
			break;
		default:
			props.editTable.setStatus(tableid, 'browse');
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(editBtns, false);
			props.button.setButtonVisible(browseBtns, true);
			// 如果有新增按钮，改为主要按钮
			browseBtns.map((item) => {
				if (item == 'Add') {
					props.button.setMainButton(item, true);
				} else if (!(item == 'Enable' || item == 'Input')) {
					props.button.setMainButton(item, false);
				}
			});
			// 行删除时悬浮框提示
			props.button.setPopContent('Delete', props.MutiInit.getIntl("36100501") && props.MutiInit.getIntl("36100501").get('36100501-000020'));/* 国际化处理： 确认要删除吗？*/
			props.search.setDisabled(searchid,false);
			break;
	}
}
