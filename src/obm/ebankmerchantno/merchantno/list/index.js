﻿import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { high,createPage, ajax, base, cardCache, toast, formDownload,createPageIcon } from 'nc-lightapp-front';
const { NCModal, NCButton, NCUpload, NCCheckbox } = base;
const { setDefData, getDefData } = cardCache;
// import Transfer from '../../../../uap/public/excomponents/Transfer';
// let { NCFormControl } = base;
// const NCBreadcrumbItem = NCBreadcrumb.NCBreadcrumbItem;
// import { ExcelImport } from '../../../../uap/public/excelImport';
const {	ExcelImport} = high;
import { buttonClick, initTemplate, afterEvent, searchBtnClick, tableModelConfirm } from './events';
import './index.less';
import Utils from '../../../../uap/public/utils';
let tableid = 'MerchantNoVO';
let searchid = '36100501';
let inputid = 'input';

class SingleTable extends Component {
	constructor(props) {
		super(props);
		this.props = props;
		this.state = {
			data: {},
			showmodal: false,
			selectedPKS: [],//导出数据的主键pk
			// leftTreeData:{},
			// rightTreeData:{},
			// treeValue:{},
			moduleName:'obm',
			billType:'36100501',
			checked: false
		};
		// let res = {data:[
		//     {   
		//         "id": "0001Z61000000004UK0C",
		//         "type": "button_main",
		//         "key": "Output",
		//         "title": "导出",
		//         "area": "list_head",
		//         "children": []
		//     },
		// ]   
		// }
		// this.props.button.setButtons(res.data);
		// this.beforeUpload = this.beforeUpload.bind(this); 
		// this.upload_change = this.upload_change.bind(this);
		// this.importProps = {
		// 	name: 'file',
		// 	action: '/nccloud/obm/ebankmerchantno/fileupload.do',
		// 	// accept:'text/xml, application/xml, text/xls, application/xls',
		// 	data: {},
		// 	multiple: false,
		// 	// beforeUpload: this.beforeUpload,
		// 	headers: {
		// 		authorization: 'authorization-text',
		// 	},
		// 	onChange: this.upload_change,
		// };
	}
	// appSureBtnClick =()=>{
	// 	let _this =this;
	// 	let data = {
	// 				"rightdata" : this.state.rightTreeData , 
	// 				"moduleName":this.state.moduleName,
	// 				"billType":this.state.billType,
	// 				}    
	// 	// _this.props.downLoad({data:data,url:'/nccloud/uapdr/trade/excelexport.do'});
	// 	ajax({

	// 		url: '/nccloud/uapdr/trade/excelexport.do',
	// 		data :data ,
	// 		success: (res) => {
	// 			if(res.data){
	// 			//   this.setState({
	// 			//       leftTreeData: res.data.leftdata,
	// 			//       rightTreeData:res.data.rightdata,
	// 			//       treeValue: res.data
	// 			//   })
	// 			}    
	// 		},
	// 		error:(res) =>{
	// 			toast({ color: 'danger', content:'错误：'+res.message });
	// 		}
	// 	});
	//   }
	//文件上传状态改变处理
	// upload_change(info) {
	// 	if (info.file.status !== 'uploading') {
	// 		console.log(info.file, info.fileList);
	// 	}
	// 	if (info.file.status === 'done') {

	// 		if (info.file.response && info.file.response.success) {

	// 			let { data } = info.file.response;
	// 			if (data) {
	// 				let pathvalue = { value: data, display: data };
	// 				this.props.form.setFormItemsValue(inputid, { path: pathvalue });
	// 			}
	// 		} else {
	// 			toast({ content: info.file.response.error.message, color: "warning" });
	// 		}
	// 	} else if (info.file.status === 'error') {
	// 		info.fileList.splice(0, info.fileList.length);
	// 	}

	// }
	getButtonNames = (codeId) => {
		return 'main-button';
	};
	excelimportconfig =() => Utils.getImportConfig("OBM","36100501", (resultinfo) => {
		this.props.editTable.setTableData(tableid, resultinfo);
		this.props.modal.show('hintimportLog');
    });
	//点击按钮，打开对话框
	InputClick = () => {
		// this.props.modal.show(`input`);
		this.setState(
			{
				showmodal: true
			}
		)
	}
	//点击确定按钮
	InputOkClick = () => {
		//数据校验
		document.getElementById("psw-error-msg").innerText = "";
		const password1 = document.getElementById('password1').value;
		const password2 = document.getElementById('password2').value;
		if (!(password1 && password2)) {
			document.getElementById("psw-error-msg").innerText = this.props.MutiInit.getIntl("36100501") && this.props.MutiInit.getIntl("36100501").get('36100501-000026');/* 国际化处理： 密码或确认密码不能为空！*/
			return;
		}
		if (password1 != password2) {
			document.getElementById("psw-error-msg").innerText = this.props.MutiInit.getIntl("36100501") && this.props.MutiInit.getIntl("36100501").get('36100501-000027');/* 国际化处理： 两次输入密码不相同！*/
			return;
		}
		let checkedRows = this.props.editTable.getCheckedRows(tableid);
		let pks = [];
		checkedRows.map(row => {
			pks.push(row.data.values.pk_merchantno.value);
		})
		let data = {
			password1: password1,
			password2: password2,
			pks: pks
		}
		ajax({
			url: '/nccloud/obm/ebankmerchantno/setpwd.do',
			data: data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					// this.props.modal.close(`input`);
					this.setState(
						{
							showmodal: false
						}
					);
					// this.loadData();
					toast({ content: this.props.MutiInit.getIntl("36100501") && this.props.MutiInit.getIntl("36100501").get('36100501-000028'), color: "success" });/* 国际化处理： 密码设置成功*/
				}
			}
		});
	}

	//点击取消按钮，关闭页面
	InputCancelClick = () => {
		// this.props.modal.close(`input`);
		this.setState(
			{
				showmodal: false
			}
		)
	}
	componentDidMount() {
		this.getData();
	}

	componentWillMount() {
		window.onbeforeunload = () => {
			if ([ 'edit' ].includes(this.props.editTable.getStatus(tableid)) ) {
				return this.props.MutiInit.getIntl("36100501") && this.props.MutiInit.getIntl("36100501").get('36100501-000039'); /* 国际化处理： 当前单据未保存, 您确定离开此页面?*/
				//return "国际化处理： 当前单据未保存, 您确定离开此页面?";
			}
		};
	}

	//请求列表数据
	getData = () => {
	
	};
	//启用
	enableAction = () => {

		let checkedRows = this.props.editTable.getCheckedRows(tableid);

		let rows = [];

		let pks = [];
		let index=[];
		checkedRows.map(row => {

			if (row.data.values.enablestate.value == null || row.data.values.enablestate.value == '1' || row.data.values.enablestate.value == '3' || row.data.values.enablestate.value == [this.props.MutiInit.getIntl("36100501") && this.props.MutiInit.getIntl("36100501").get('36100501-000014')] || row.data.values.enablestate.value == [this.props.MutiInit.getIntl("36100501") && this.props.MutiInit.getIntl("36100501").get('36100501-000016')]) {/* 国际化处理： 未启用,已停用*/

				rows.push(row.data);
				pks.push(row.data.values.pk_merchantno.value);
				
			}
			index.push(row.index);
		})

		if (pks[0] && pks[0] != undefined) {
			let data = {
				model: { rows: rows }
			}
			ajax({
				url: '/nccloud/obm/ebankmerchantno/enable.do',
				data: data,
				success: (res) => {
					// checkedRows=convertGridEnablestate(checkedRows);
					toast({ color: "success", content: this.props.MutiInit.getIntl("36100501") && this.props.MutiInit.getIntl("36100501").get('36100501-000029') });/* 国际化处理： 启用成功*/
					// this.loadData();
					for (let item of rows) {
						this.props.editTable.setValByKeyAndRowId(tableid,item.rowid,'enablestate',{ value:"2", display:this.props.MutiInit.getIntl("36100501") && this.props.MutiInit.getIntl("36100501").get('36100501-000015'),scale:null });/* 国际化处理： 已启用*/
					}
					this.props.editTable.selectTableRows(tableid, index, false);
					// this.props.editTable.setTableData(tableid, res.data[tableid]);

				}
			})
		} else {
			toast({ color: "warning", content: this.props.MutiInit.getIntl("36100501") && this.props.MutiInit.getIntl("36100501").get('36100501-000030') });/* 国际化处理： 没有可启用数据*/
		}
		this.setState(this.state);
	}
	//停用
	disableAction = () => {
		let checkedRows = this.props.editTable.getCheckedRows(tableid);

		let rows = [];

		let pks = [];
		let index=[];
		checkedRows.map(row => {

			if (row.data.values.enablestate.value == '2' || row.data.values.enablestate.value == [this.props.MutiInit.getIntl("36100501") && this.props.MutiInit.getIntl("36100501").get('36100501-000015')]) {/* 国际化处理： 已启用*/

				rows.push(row.data);
				pks.push(row.data.values.pk_merchantno.value);
				
			}
			index.push(row.index);
		})

		if (pks[0] && pks[0] != undefined) {
			let data = {
				model: { rows: rows }
			}
			ajax({
				url: '/nccloud/obm/ebankmerchantno/disable.do',
				data: data,
				success: (res) => {
					toast({ color: "success", content: this.props.MutiInit.getIntl("36100501") && this.props.MutiInit.getIntl("36100501").get('36100501-000031') });/* 国际化处理： 停用成功*/
					// this.loadData();
					// this.props.editTable.setTableData(tableid, res.data[tableid]);
					for (let item of rows) {
						this.props.editTable.setValByKeyAndRowId(tableid,item.rowid,'enablestate',{ value:"3", display:this.props.MutiInit.getIntl("36100501") && this.props.MutiInit.getIntl("36100501").get('36100501-000016'),scale:null });/* 国际化处理： 已停用*/
					}
					this.props.editTable.selectTableRows(tableid, index, false);
				}
			})
		} else {
			toast({ color: "warning", content: this.props.MutiInit.getIntl("36100501") && this.props.MutiInit.getIntl("36100501").get('36100501-000032') });/* 国际化处理： 没有可停用数据*/
		}
		this.setState(this.state);
	}
	onCheckShowDisable() {

		// cacheTools.set('isChecked',!isChecked);
		this.setState(
			{ checked: !this.state.checked },
			() => {
				this.loadData()
			}
		)

	}
	loadData() {

		// let enablesate = {

		// 	field: 'enablestate',
		// 	oprtype: '\u003d',
		// 	value: {
		// 		firstvalue: '2',
		// 		secondvalue: null
		// 	}
		// }

		let conditions = [];
		// let pageInfo = this.props.table.getTablePageInfo(tableid);
		let searchVal = this.props.search.getAllSearchData(searchid,false);
		// let isChecked = this.state.checked;
		// if (isChecked == false) {
		//     conditions.push(enablesate);
		// }
		//获取查询模板信息
		// let queryInfo = this.props.search.getQueryInfo(searchid);
		// let OID = queryInfo.oid;
		let queryInfo = this.props.search.getQueryInfo(searchid);
		let oid = queryInfo.oid;
		let data = {
			conditions: searchVal == null ? null : searchVal.conditions,
			custconditions: conditions,
			pagecode: "36100501",
			// pageInfo:{
			// 	pageIndex : "0", 
			// 	pageSize : "10" 
			// },
			queryAreaCode: searchid,  //查询区编码
			oid: oid,  //查询模板id，手工添加在界面模板json中，放在查询区，后期会修改
			queryType: 'simple'
		};

		ajax({
			url: '/nccloud/obm/ebankmerchantno/queryschem.do',
			data,
			success: (res) => {
				console.log(res);
				let { success, data } = res;
				if (success) {
					if (data && data[tableid]) {
						this.props.editTable.setTableData(tableid, res.data[tableid]);
						
					} else {
						this.props.editTable.setTableData(tableid, { rows: [] });
						toast({ content: this.props.MutiInit.getIntl("36100501") && this.props.MutiInit.getIntl("36100501").get('36100501-000025'), color: "warning" });/* 国际化处理： 无数据*/
					}
					this.setState(this.state);
				}
			},
			error: (res) => {
				console.log(res.message);
			}
		});
	}
	//点击下载模板按钮，下载模板
	DownloadFileClick = () => {
		let data = 'xlsx';
		formDownload({
			params: { data },
			url: '/nccloud/obm/ebankmerchantno/filedownload.do',
			enctype: 1
		});
	}
	rowSelected = (props, moduleId, record, index, status) => {
		let checkedRows = props.editTable.getCheckedRows(moduleId);
		let allRows=props.editTable.getAllRows(tableid);
		if(checkedRows && checkedRows.length == 1){
			checkedRows.map(row => {
				if (row.data.values.enablestate.value == '2' || row.data.values.enablestate.value == [this.props.MutiInit.getIntl("36100501") && this.props.MutiInit.getIntl("36100501").get('36100501-000015')]) {/* 国际化处理： 已启用*/
					props.button.setButtonDisabled(['Edit','Delete', 'SetPwd','Add','Input', 'output', 'Refresh','Disable'],false);
					props.button.setButtonDisabled(['Enable'], true);
				}else if (row.data.values.enablestate.value == null || row.data.values.enablestate.value == '1' || row.data.values.enablestate.value == '3' || row.data.values.enablestate.value == [this.props.MutiInit.getIntl("36100501") && this.props.MutiInit.getIntl("36100501").get('36100501-000014')] || row.data.values.enablestate.value == [this.props.MutiInit.getIntl("36100501") && this.props.MutiInit.getIntl("36100501").get('36100501-000016')]) {/* 国际化处理： 未启用,已停用*/
					props.button.setButtonDisabled(['Edit', 'Delete', 'SetPwd','Add','Input', 'output', 'Refresh','Enable'],false);
					props.button.setButtonDisabled(['Disable'], true);
				}
			})
		}else{
			props.button.setButtonDisabled(['Delete', 'SetPwd', 'Enable', 'Disable'], !(checkedRows && checkedRows.length > 0));
			props.button.setButtonDisabled(['Add','Input', 'output', 'Refresh'], false);
			props.button.setButtonDisabled('Edit',(allRows.length == 0));
		}
	};
	render() {
		let { editTable, ncmodal, button, table, search, form } = this.props;
		let { NCCreateSearch } = search;
		// let { createSimpleTable } = table;
		// let { createButton } = button;
		const { createEditTable } = editTable;
		const { createModal } = ncmodal;
		let { createButtonApp } = this.props.button;
		// let { createForm } = form;
		// let buttons = this.props.button.getButtons();
		return (
			<div className="nc-bill-list">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						{/*页面大图标*/}
						{createPageIcon()}
						<h2 className='title-search-detail'>
						{this.props.MutiInit.getIntl("36100501") && this.props.MutiInit.getIntl("36100501").get('36100501-000036')}</h2>{/* 国际化处理： 商户号*/}
						{/* 显示停用  showOff*/}
						{/* <NCCheckbox onChange={this.onCheckShowDisable.bind(this)} checked={this.state.checked}>显示停用</NCCheckbox>	 */}
					</div>
					{/* 按钮区 btn-area */}
					<div className="header-button-area">
						{createButtonApp({
							area: 'list_head',
							buttonLimit: 8,
							onButtonClick: (props, id) => {
								buttonClick.call(this, props, id, tableid);
							},
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				<div className="nc-bill-search-area">
					{NCCreateSearch(searchid, {//查询区
						clickSearchBtn: searchBtnClick.bind(this),
						oid: ""
					})}
				</div>
				<div className="nc-bill-table-area" style={{ borderRadius: "3px 3px 0 0", overflow: "hidden" }}>
					{createEditTable(tableid, {//列表区
						onAfterEvent: afterEvent,
						// handlePageInfoChange: pageInfoClick,
						tableModelConfirm: tableModelConfirm,
						selectedChange: this.rowSelected.bind(this),
						onSelectedAll:this.rowSelected.bind(this),
						adaptionHeight: true,
						showCheck: true,
						showIndex: true
					})}
				</div>
				{/* 弹框 */}
				{createModal(`${tableid}-del-confirm`, { color: 'warning', hasCloseBtn: true })}
				{createModal('enableModal', { hasCloseBtn: true })}

			
				<NCModal
					show={this.state.showmodal}
					onHide={this.InputCancelClick.bind(this)}
					backdrop={true}
					className={inputid}
					style={{ width: '520px',height:'268px' }}
				>
					<NCModal.Header closeButton={true}>
					<NCModal.Title>
						{this.props.MutiInit.getIntl("36100501") && this.props.MutiInit.getIntl("36100501").get('36100501-000033')/* 国际化处理： 设置密码*/}
					</NCModal.Title>
					</NCModal.Header>

					<NCModal.Body>
						<div className="psw-p">
							<div className="psw-error-msg" id="psw-error-msg"></div>
							<div className="panel-label">
								<label id="pswlabel1">{this.props.MutiInit.getIntl("36100501") && this.props.MutiInit.getIntl("36100501").get('36100501-000034')}</label>{/* 国际化处理： 密码*/}
							</div>
							<div className="panel-input">
								<input field="password1" fieldname={this.props.MutiInit.getIntl("36100501") && this.props.MutiInit.getIntl("36100501").get('36100501-000034')} id="password1" className="u-form-control text" type="password" placeholder="" />{/* 国际化处理： 密码*/}
							</div>
							<div className="panel-div" ></div>
							<div className="panel-label">
								<label id="pswlabel2">{this.props.MutiInit.getIntl("36100501") && this.props.MutiInit.getIntl("36100501").get('36100501-000035')}</label>{/* 国际化处理： 确认密码*/}
							</div>
							<div className="panel-input">
								<input field="password2" fieldname={this.props.MutiInit.getIntl("36100501") && this.props.MutiInit.getIntl("36100501").get('36100501-000035')} id="password2" className="u-form-control text" type="password" placeholder="" />{/* 国际化处理： 确认密码*/}
							</div>
						</div>
					</NCModal.Body>

					<NCModal.Footer>
						<NCButton onClick={this.InputOkClick.bind(this)} colors="primary">
							{this.props.MutiInit.getIntl("36100501") && this.props.MutiInit.getIntl("36100501").get('36100501-000038')/* 国际化处理： 确定*/}
						</NCButton>
						<NCButton onClick={this.InputCancelClick.bind(this)} shape="border">
							{this.props.MutiInit.getIntl("36100501") && this.props.MutiInit.getIntl("36100501").get('36100501-000011')/* 国际化处理： 取消*/}
						</NCButton>
					</NCModal.Footer>
				</NCModal>
				{createModal('importModal', {
							noFooter: true,
							className: 'import-modal',
							hasBackDrop: false,
				})}
				<ExcelImport
					{...Object.assign(this.props)}
					moduleName='obm'//模块名
					billType='36100501'//单据类型
					appcode = '36100501'//appcode
                    pagecode = '36100501_L01'//pagecode
					selectedPKS={this.state.selectedPKS}
				/>
				

			</div>

		);
	}
}

SingleTable = createPage({
	mutiLangCode: '36100501',
	initTemplate: initTemplate
})(SingleTable);

ReactDOM.render(<SingleTable />, document.querySelector('#app'));
