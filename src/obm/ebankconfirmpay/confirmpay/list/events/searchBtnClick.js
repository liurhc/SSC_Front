import {ajax,toast,cacheTools,cardCache} from 'nc-lightapp-front';
let { setDefData, getDefData } = cardCache;
import {appcode,tableId, searchid, list_page_code,dataSource,cacheTabActiveKey, cachesearchKey,cacheTabKey} from '../../cons/constant.js';
import clickBtn from './setButtonUsability';
//点击查询，获取查询区数据
export default function  searchBtnClick(props,value) {
	//缓存查询条件	
	cacheTools.set(cachesearchKey, value);
	if(value){
		setDefData(cachesearchKey, dataSource, value);
        let cachestateTabActiveKey = getDefData(cacheTabActiveKey, dataSource);
        let billstate = '';
        if(cachestateTabActiveKey){
            if(cachestateTabActiveKey === '0'){
                billstate = '-1';
            }else if(cachestateTabActiveKey === '1'){
                billstate = '3';
            }else if(cachestateTabActiveKey === '2'){
                billstate = '2';
            }else if(cachestateTabActiveKey === '3'){
                billstate = '1';
            }else if(cachestateTabActiveKey === '4'){
                billstate = '0';
            }else if(cachestateTabActiveKey === '5'){
                billstate = null;
            }
        }else{
            billstate = '-1';
        }
		 
		let pageInfo = props.table.getTablePageInfo(tableId);
        let queryInfo = props.search.getQueryInfo(searchid, false);
        let oid = queryInfo.oid;
		let data = {
			querycondition: value,
			custcondition: {
                conditions: [
					{
						field: 'billstate',
						value: {
							firstvalue: billstate,
							secondvalue: null
						},
						oprtype: '=',
						datatype: 203,
					}
				],
                logic: "and",
            },
			pagecode: "36100CONFM_L01",
			pageInfo: pageInfo,
			queryAreaCode: searchid,
			oid: oid,//"0001Z61000000000LUJH",
			querytype: "tree"
		};
		console.log(data);
		ajax({
			url:  '/nccloud/obm/ebankconfirmpay/query.do',
			data: data,
        success:  (res)=> {
            let { success, data } = res;
            if(success){
				if(data){
					if(data.grid){
					    props.table.setAllTableData(tableId, data.grid[tableId]);
					}
					if (data.numvalues) {
						this.setState({ numvalues: data.numvalues});
						 setDefData(cacheTabKey, dataSource, this.state.numvalues);
						 setDefData(cacheTabActiveKey, dataSource, this.state.activeKey);
						 if(data.numvalues.ALL){
							toast({
								duration: 6,
								color: 'success',
								content:props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000031') /* 国际化处理： 查询成功*/
							})
						}else{
							/* 国际化处理：未查询出符合条件的数据! */
							toast({
								duration: 6,
								color: 'warning',
								content:props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000033') /* 国际化处理： 未查询出符合条件的数据*//* 国际化处理： 未查询出符合条件的数据*/
							})
								this.props.table.setAllTableData(this.tableId, { rows: [] });
						}
					}else{
						toast({
							duration: 6,
							color: 'warning',
							content:props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000033') /* 国际化处理： 未查询出符合条件的数据*//* 国际化处理： 未查询出符合条件的数据*/
						})
						this.setState({ numvalues: {} });
					}
					clickBtn.call(this,this.props);
					
				} else {
					toast({
						duration: 6,
						color: 'warning',
						content:props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000033') /* 国际化处理： 未查询出符合条件的数据*//* 国际化处理： 未查询出符合条件的数据*/
					})
				    props.table.setAllTableData(tableId, { rows: [] });
					this.setState({ numvalues: {} });
				}
               
            }
          }
		});
	}
};




function giveToast(props, resLength) {
    if (resLength && resLength > 0) {
        let contentHead = props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000031'); /* 国际化处理： 查询成功，共*//* 国际化处理： 查询成功，共 */
        let contentEnd = props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000032') /* 国际化处理： 条。*//* 国际化处理： 条。*/
        toast({
            duration: 6,
            color: 'success',
            content: contentHead + resLength + contentEnd
        })
    }
    else {
        toast({
            duration: 6,
            color: 'warning',
            content:props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000033') /* 国际化处理： 未查询出符合条件的数据*//* 国际化处理： 未查询出符合条件的数据*/
        })
    }
}
