import { createPage, ajax, base,cacheTools } from 'nc-lightapp-front';
import searchBtnClick from './searchBtnClick';
import tabButtonClick from './tabButtonClick';
let { NCPopconfirm, NCIcon, NCTooltip:Tooltip } = base;

import {appcode,tableId, searchid, list_page_code,dataSource, cachesearchKey,cacheTabKey} from '../../cons/constant.js';
export default function(props) {
	//请求模板数据
	props.createUIDom(
		{
			pagecode: list_page_code,//页面id
			appcode: appcode//注册按钮的code
		},
		 (data) =>{
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta)
					props.meta.setMeta(meta,()=>{

						//如果有缓存条件，查询
						let querycondition = cacheTools.get(cachesearchKey);
						console.log(props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000030'));/* 国际化处理： 缓存查询条件*/
						console.log(querycondition);
						if (querycondition) {
							props.search.setSearchValue(searchid, querycondition);
						//	searchBtnClick(props, querycondition);
						}
					});
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					props.button.setPopContent('delete_inner', props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000024'));/* 国际化处理： 确定要删除吗？*/
				}
			}
		}
	)
}

function modifierMeta(props, meta) {
	meta[searchid].items = meta[searchid].items.map((item, key) =>{
		item.visible=true;
		item.col = '3';
		return item;
	})
// 查询条件多选
	meta[searchid].items.find((e) => e.attrcode === 'pk_org').isMultiSelectedEnabled = true;
//财务组织用户过滤
	meta[searchid].items.map((item)=>{
		if (item.attrcode == 'pk_org' ) {
			       item.queryCondition = () => {
			       		return {
			            	funcode: appcode,
			            	TreeRefActionExt: 'nccloud.web.tmpub.filter.FinanceOrgPermissionFilter'
			        	};
			        };
			     }
	});

	meta[tableId].items = meta[tableId].items.map((item, key) => {
		item.width = 180;
		if (item.attrcode == 'vbillno') {
			item.render = (text, record, index) => {
				//let tip = (<div>{record.vbillno.value}</div>)
				return (
					//<Tooltip trigger="hover" placement="top" inverse={false} overlay={tip}>
					<a
						style={{ cursor: 'pointer' }}//textDecoration: 'underline', 
						onClick={() => {
							props.pushTo("/card",{
								status: 'browse',
								id: record.pk_confirmpay_h.value
							});
						}}
					>
						{record && record.vbillno && record.vbillno.value}
					</a>
					//</Tooltip>
				);
			};
		}
		else if (item.attrcode == 'dbilldate'){
			item.render = (text, record, index) => {
				return (
					<span>
						{record.dbilldate && seperateDate(record.dbilldate.value)}
					</span>
				);
			};
		}
		return item;
	});

	//添加操作列
	meta[tableId].items.push({
		attrcode: 'opr',
		label: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000012'),/* 国际化处理： 操作*/
		className: 'table-opr',
		width: 200,
		visible: true,
		itemtype: 'customer',
		fixed: 'right',
		render: (text, record, index) => {
			// 审批状态 0=审批未通过，1=审批通过，2=审批进行中，3=提交，-1=自由， 
			let billstate = record.billstate;
			let buttonAry = [];

		
			if(billstate && billstate.value){


				if(billstate.value == -1){//自由
						buttonAry.push('commit_inner');
						buttonAry.push('edit_inner');
						buttonAry.push('delete_inner');
				}
				if(billstate.value == 1){//审批通过
						buttonAry.push('reviewapprove_inner');
				}
				
				
				if(billstate.value == 2||billstate.value == 3){
					buttonAry.push('reviewapprove_inner');
					buttonAry.push('uncommit_inner');
				}
			}
			return props.button.createOprationButton(buttonAry, {
				area: "list_inner",
				buttonLimit: 3,
				onButtonClick: (props, key) => tabButtonClick.call(this, props, key, text, record, index)
				
			});
		}
	});

	return meta;
}

//刷新页面数据方法
function refreshFun(props) {
	let searchVal = props.search.getAllSearchData(searchid);
	let data = {
		conditions: searchVal,
		pageInfo: {
			currentPageIndex: 0,
			pageSize: 10,
			total: 0,
			pageCount: 0
		}
	};

	ajax({
		url: '/nccloud/obm/ebankconfirmpay/query.do',
		data: data,
		success: function(res) {
			props.table.setAllTableData(tableid, res.data['0001']);
		}
	});
}
