import {appcode,tableId, searchid, list_page_code,listbutton} from '../../cons/constant.js';

export default function clickBtn(props) {
	let selectdata = props.table.getCheckedRows(tableId);
	props.button.setMainButton(['file'], true)
	if (!selectdata || selectdata.length == 0) {
		//没有选中
		props.button.setButtonDisabled(listbutton.selectnulldisabled, true);
	} else if (selectdata.length == 1) {
		// 选择一条数据按钮根据状态可用
		props.button.setButtonDisabled(listbutton.selectnulldisabled, false);
		// 审批状态 0=审批未通过，1=审批通过，2=审批进行中，3=提交，-1=自由， 
		let billstate;
		//处理选择数据
		selectdata.forEach((val) => {
			billstate = val.data.values.billstate.value;
		});
		
		if(billstate == '0'){//审批未通过
			props.button.setButtonDisabled(listbutton.approvefaildisabled, true);
		}
		if(billstate == '1'){//审批通过
			props.button.setButtonDisabled(listbutton.approvesuccdisabled, true);
		}
		if(billstate == '2'){//审批进行中
			props.button.setButtonDisabled(listbutton.approveingdisabled, true);
		}
		if(billstate == '3'){//提交
			props.button.setButtonDisabled(listbutton.commitdisabled, true);
		}
		if(billstate == '-1'){//自由
			props.button.setButtonDisabled(listbutton.freedisabled, true);
		}
		
	} else {//选中多条
		props.button.setButtonDisabled(listbutton.selectnulldisabled, false);
	}
	
}
