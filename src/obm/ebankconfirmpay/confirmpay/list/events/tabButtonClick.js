import { createPage, ajax, base, toast, cardCache,promptBox} from 'nc-lightapp-front';
let { NCPopconfirm, NCIcon } = base;
import {appcode,tableId, searchid, list_page_code,dataSource, cachesearchKey,cacheTabKey} from '../../cons/constant.js';
let { getNextId, getCurrentLastId, deleteCacheById, deleteCacheId, getCacheById, updateCache, addCache, setDefData, getDefData } = cardCache;
export default function tabButtonClick(props, key, text, record, index) {
    let that = this;
    switch (key) {
        // 修改
        case 'edit_inner':
        this.props.pushTo("/card",{
            status: 'edit',
            id: record.pk_confirmpay_h.value
        });
            
            break;
        // 删除
        case 'delete_inner':

            ajax({
                url: '/nccloud/obm/ebankconfirmpay/delete.do',
                data: [
                    {
                        id: record.pk_confirmpay_h && record.pk_confirmpay_h.value,
                        ts: record.ts && record.ts.value
                    }
                ],
                success: (res) => {
                    if (res.success) {
                        if (res.data && res.data.errormsg) {
                            toast({ color: 'warning', content: res.data.errormsg });

                        } else {
                            toast({ color: 'success', content: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000018') });/* 国际化处理： 删除成功*//* 国际化处理： 删除成功*/
                            refresh(that, props);
                        }
                    }
                }
            });
            break;
        // 提交
        case 'commit_inner':
        promptBox({
            color: "warning",
            title: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000039'),/* 国际化处理： 注意*/
            content: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000040'),/* 国际化处理： 支付状态是否已和银行柜台确认？如果没有，可能会造成重复支付。\n为了保证信息准确，建议发送指令后，间隔15分钟以上再进行确认。*/
            beSureBtnClick: commit.bind(this,that,record, props)
          });

            break;
        // 收回
        case 'umcommit_inner':
            ajax({
                url: '/nccloud/obm/ebankconfirmpay/uncommit.do',
                data: {
                    pks: [record.pk_confirmpay_h.value]
                },
                success: function (res) {
                    if (res.success) {
                        if(res.data && res.data.errorMsg){
                          toast({ color: 'warning', content: res.data.errorMsg });
                        }else{
                          toast({ color: 'success', content: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000001') });/* 国际化处理： 收回成功*/
                        }
                        refresh(that, props);
                      }
                }
            });
            break;

        // 查看审批意见
        case 'reviewapprove_inner':
            if (record) {
                let pk_linktradetype = '36CC'; //单据pk

                this.setState(
                    {
                        billId: record.pk_confirmpay_h && record.pk_confirmpay_h.value, //单据pk
                        billtype: pk_linktradetype
                    },
                    () => {
                        this.setState({
                            showAppr: true
                        });
                    }
                );
            }
            break;

        default:
            break;
    }
}


/**
 * 提交
 */
function commit(that,record,props) {
    ajax({
        url: '/nccloud/obm/ebankconfirmpay/commit.do',
        data: {
            pks: [record.pk_confirmpay_h.value]
        },
        success: function (res) {
            if (res.success) {
                if(res.data && res.data.errorMsg){
                  toast({ color: 'warning', content: res.data.errorMsg });
                }else{
                  toast({ color: 'success', content: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000000') });/* 国际化处理： 提交成功*/
                }
                refresh(that, props);
              }
           
        }
    });
  }

//刷新列表信息
function refresh(that, props) {
    //分页
    let refreshpageInfo = props.table.getTablePageInfo(tableId);
    //查询condition
    let refreshsearchVal = props.search.getAllSearchData(searchid);
    let queryInfo = props.search.getQueryInfo(searchid, false);
    let oid= queryInfo.oid;
    if(refreshsearchVal && refreshsearchVal.conditions){
        setDefData(cachesearchKey, dataSource, refreshsearchVal);
        let data={
            querycondition: refreshsearchVal,
            custcondition: {
                logic: "and",   //逻辑操作符，and、or
                conditions: [
                ],
            },
            conditions: refreshsearchVal.conditions || refreshsearchVal,
            pageInfo: refreshpageInfo,
            pagecode: list_page_code,
            //查询区编码
            queryAreaCode: searchid,
            //查询模板id，手工添加在界面模板json中，放在查询区
            oid: oid,  
            querytype: 'tree'
        };
    
        if(refreshsearchVal && refreshsearchVal.conditions){
            ajax({
                url: '/nccloud/obm/ebankconfirmpay/query.do',
                data: data,
                success: (res) => {
                    let { success, data } = res;
                    if (success) {
                        if(data.grid){
                            props.table.setAllTableData(tableId, data.grid[tableId]);
                        }else{
                            props.table.setAllTableData(tableId, { rows: [] });
                        }
                        if (data.numvalues) {
                            that.setState({ numvalues: data.numvalues, activeKey: '0' });
                            setDefData(cacheTabKey, dataSource, that.state.numvalues);
                        }else{
                            that.setState({ numvalues: {} });
                        }
                    }
                }
            });
        }
    }
}
