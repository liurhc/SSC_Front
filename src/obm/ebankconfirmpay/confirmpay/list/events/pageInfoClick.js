import { ajax } from 'nc-lightapp-front';
import {appcode,tableId, searchid, list_page_code,dataSource, cachesearchKey,cacheTabKey} from '../../cons/constant.js';

export default function (props, config, pks) {
    //分页根据pks查询数据
    let data = {
        pks: pks,
        pageid: list_page_code
    };
    console.log(pks);
    ajax({
        url: '/nccloud/obm/ebankconfirmpay/querybypks.do',
        data: data,
        success: (res) => {
            let { success, data } = res;
            if (success) {
                if(data){
                    props.table.setAllTableData(tableId, data[tableId]);
                }else{
                    props.table.setAllTableData(tableId, {rows:[]});
                }
                
            }
        }
    });
  
}
