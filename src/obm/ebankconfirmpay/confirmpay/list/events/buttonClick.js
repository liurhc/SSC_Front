import { createPage, ajax, base, toast, cacheTools, print,cardCache ,promptBox} from 'nc-lightapp-front';
const { NCMessage } = base;
let { deleteCacheId, getCacheById, updateCache, setDefData } = cardCache;
import {appcode,tableId, searchid, list_page_code,dataSource, cachesearchKey,cacheTabKey} from '../../cons/constant.js';
export default function buttonClick(props, id) {
    let that = this;
    switch (id) {
        //删除，可以支持批量
        case 'deleteBtn':
            //deleteAction.call(this, props);
            //this.props.modal.show('delete');
            promptBox({
                title: this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000023'),/* 国际化处理： 删除*/
                color: "warning",
                content: this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000038'),/* 国际化处理： 确定要删除所选数据吗?*/
                beSureBtnClick: this.delConfirm.bind(this, props),
              });
            break;
        case 'editBtn':
            let editDatas = props.table.getCheckedRows(tableId);
            //判断是否有选中行
            if (editDatas == null || editDatas.length == 0) {
                toast({ color: 'warning', content: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000025') });/* 国际化处理： 未选中行！*/
                return;
            }
            else if (editDatas.length > 1) {
                toast({ color: 'warning', content: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000026') });/* 国际化处理： 一次只能修改一行！*/
                return;
            }

            let pk = editDatas[0].data.values.pk_confirmpay_h.value;
            props.pushTo("/card",{
                status: 'edit',
                id: pk
            });
            break;
        //刷新
        case 'refreshBtn':
              refreshHtml(that, props);
            break;
        //提交
        case 'commitBtn':
        promptBox({
            color: "warning",
            title: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000039'),/* 国际化处理： 注意*/
            content: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000040'),/* 国际化处理： 支付状态是否已和银行柜台确认？如果没有，可能会造成重复支付。\n为了保证信息准确，建议发送指令后，间隔15分钟以上再进行确认。*/
            beSureBtnClick: commit.bind(this,that, props)
          });

            break;



        //提交取消-收回
        case 'uncommitBtn':

            let uncommitData = props.table.getCheckedRows(tableId);
            //数据校验
            if (uncommitData.length == 0) {
                toast({ color: 'warning', content: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000027') });/* 国际化处理： 请选择数据*/
                return
            }
            let indexArr7 = [];
            let dataArr7 = [];
            //处理选择数据
            uncommitData.forEach((val) => {
                dataArr7.push(val.data.values.pk_confirmpay_h.value);//主键数组
                indexArr7.push(val.index);
            });
            //自定义请求数据
            let data7 = {
                'pks': dataArr7
            };

            ajax({
                url: '/nccloud/obm/ebankconfirmpay/uncommit.do',
                data: data7,
                success: function (res) {
                    if (res.success) {
                        if(res.data && res.data.errorMsg){
                          toast({ color: 'warning', content: res.data.errorMsg });
                        }else{
                          refreshHtml(that, props);
                          toast({  duration: 3,color: 'success', content: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000001') });/* 国际化处理： 收回成功*/
                          
                        }
                       
                      }
                   
                }
            });
            break;

        //审批
        case 'approveBtn':

            let approveData = props.table.getCheckedRows(tableId);
            //数据校验
            if (approveData.length == 0) {
                toast({ color: 'warning', content: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000027') });/* 国际化处理： 请选择数据*/
                return
            }
            let indexArrapprove = [];
            let dataArrapprove = [];
            //处理选择数据
            approveData.forEach((val) => {
                dataArrapprove.push(val.data.values.pk_confirmpay_h.value);//主键数组
                indexArrapprove.push(val.index);
            });
            //自定义请求数据
            let data2 = {
                'pks': dataArrapprove
            };

            ajax({
                url: '/nccloud/obm/ebankconfirmpay/approve.do',
                data: data2,
                success: function (res) {
                    let { success, data } = res;
                    if (success) {
                        toast({ color: 'success', content: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000002') });/* 国际化处理： 审批成功*/
                        refreshHtml(that, props);
                    }
                }
            });
            break;

        //审批取消-退审
        case 'unapproveBtn':

            let unapproveData = props.table.getCheckedRows(tableId);
            //数据校验
            if (unapproveData.length == 0) {
                toast({ color: 'warning', content: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000027') });/* 国际化处理： 请选择数据*/
                return
            }
            let indexArr3 = [];
            let dataArr3 = [];
            //处理选择数据
            unapproveData.forEach((val) => {

                dataArr3.push(val.data.values.pk_confirmpay_h.value);//主键数组
                indexArr3.push(val.index);
            });
            //自定义请求数据
            let data3 = {
                'pks': dataArr3
            };

            ajax({
                url: '/nccloud/obm/ebankconfirmpay/unapprove.do',
                data: data3,
                success: function (res) {
                    let { success, data } = res;
                    if (success) {
                        toast({ color: 'success', content: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000003') });/* 国际化处理： 审批取消成功*/
                        refreshHtml(that, props);
                    }
                }
            });
            break;
        case 'viewapproveInfo':
            let linkaprvData = props.table.getCheckedRows(tableId);
            let pk_confirmpay_h = ''; //单据pk
            let pk_linktradetype = ''; //单据pk
            if (linkaprvData.length != 1) {
                toast({
                    color: 'warning',
                    content: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000028')/* 国际化处理： 请选择单条数据，查看审批意见!*/
                });
                return;
            }
            //处理
            linkaprvData.forEach((val) => {
                if (val.data.values.pk_confirmpay_h && val.data.values.pk_confirmpay_h.value != null) {
                    pk_confirmpay_h = val.data.values.pk_confirmpay_h.value;
                }
                pk_linktradetype = '36CC';
            });

            this.setState(
                {
                    billId: pk_confirmpay_h, //单据pk
                    billtype: pk_linktradetype
                },
                () => {
                    this.setState({
                        showAppr: true
                    });
                }
            );
            break;
        //打印
        case 'printBtn':
            let printData = props.table.getCheckedRows(tableId);
            //数据校验
            if (printData.length == 0) {
                toast({ color: 'warning', content: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000027') });/* 国际化处理： 请选择数据*/
                return
            }
            let printpks = [];
            printData.forEach((item) => {
                printpks.push(item.data.values.pk_confirmpay_h.value);
            });
            print(
                //支持两类: 'html'为模板打印, 'pdf'为pdf打印
                'pdf',
                '/nccloud/obm/ebankconfirmpay/print.do',
                {
                    //模板节点标识
                    nodekey: 'NCCLOUD',
                    // 功能节点的数据主键 oids含有多个元素(['xxx','xxx'])时为批量打印,
                    oids: printpks,
                }
            );
            break;
        case 'preview':
            printData(props);
            break;
        case 'export':
            let outputData = props.table.getCheckedRows(tableId);
            //数据校验
            if (outputData.length == 0) {
                toast({ color: 'warning', content: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000027') });/* 国际化处理： 请选择数据*/
                return
            }
            let outputpks = [];
            outputData.forEach((item) => {
                outputpks.push(item.data.values.pk_confirmpay_h.value);
            });
            this.setState(
                {
                    outputData: {
                        // 模板节点标识
                        nodekey: 'NCCLOUD',
                        // oids含有多个元素(['xxx','xxx'])时为批量打印,   
                        oids: outputpks,
                        outputType: 'output'
                    }
                },
                () => {
                    this.refs.printOutput.open();
                }
            );
            break;
        //联查
        case 'billLinkQueryBtn':
            linkData(props);
            break;
        // 附件管理
        case 'file':
            let accessoryBtnData = props.table.getCheckedRows(tableId);
            //数据校验
            if (accessoryBtnData.length == 0) {
                toast({ color: 'warning', content: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000027') });/* 国际化处理： 请选择数据*/
                return
            }
            //单据pk
            let pk_confirmpay_hfile = '';
            //单据编号
            let bill_nofile = '';
            //选择一个或者不选择，多选默认显示空数据
            if (accessoryBtnData.length == 1) {
                accessoryBtnData.forEach((val) => {
                    if (val.data.values.pk_confirmpay_h && val.data.values.pk_confirmpay_h.value) {
                        pk_confirmpay_hfile = val.data.values.pk_confirmpay_h.value;
                    }
                    if (val.data.values.vbillno && val.data.values.vbillno.value) {
                        bill_nofile = val.data.values.vbillno.value;
                    }
                });
            } else {

            }
            // 
            this.setState({
                //单据pk
                billId: pk_confirmpay_hfile,
                //附件管理使用单据编号
                billno: bill_nofile,
                showUploader: !this.state.showUploader,
                target: null
            })
            break;
    }
}

/**
 * 提交
 */
function commit(that,props) {
    let commitData = props.table.getCheckedRows(tableId);
    //数据校验
    if (commitData.length == 0) {
        toast({ color: 'warning', content: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000027') });/* 国际化处理： 请选择数据*/
        return
    }
    let indexArr6 = [];
    let dataArr6 = [];
    //处理选择数据
    commitData.forEach((val) => {
        dataArr6.push(val.data.values.pk_confirmpay_h.value);//主键数组
        indexArr6.push(val.index);
    });
    //自定义请求数据
    let data6 = {
        'pks': dataArr6
    };

    ajax({
        url: '/nccloud/obm/ebankconfirmpay/commit.do',
        data: data6,
        success: function (res) {
            if (res.success) {
                if(res.data && res.data.errorMsg){
                  toast({  color: 'warning', content: res.data.errorMsg });
                }else{
                  refreshHtml(that, props);
                  toast({  duration: 3,color: 'success', content: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000000') });/* 国际化处理： 提交成功*/
                }
                
              }
        }
    });
  }


function printData(props) {
    let printData = props.table.getCheckedRows(tableId);
    //数据校验
    if (printData.length == 0) {
        toast({ color: 'warning', content: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000027') });/* 国际化处理： 请选择数据*/
        return
    }
    let pks = [];
    printData.forEach((item) => {
        pks.push(item.data.values.pk_confirmpay_h.value);
    });
    print(
        'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
        '/nccloud/obm/ebankconfirmpay/print.do',
        {
            funcode: appcode, //功能节点编码，即模板编码
            nodekey: 'NCCLOUD', //模板节点标识
            printTemplateID: '1001Z6100000000045U9',//列表打印模板pk 
            oids: pks // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
        }
    );
}


function linkData(props) {
    let linkData = props.table.getCheckedRows(tableId);
    //数据校验
    if (linkData.length != 1) {
        toast({ color: 'warning', content: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000029') });/* 国际化处理： 请选择一条数据*/
        return
    }
    let sourcebillpk = linkData[0].data.values.sourcebillpk.value;
    let sourcebilltype = linkData[0].data.values.sourcebilltype.value;
    if (sourcebilltype == '36C9') {//工资清单
        props.openTo('/obm/ebankdfgz/dfgz/main/index.html#/card', {
            // status: 'browse',
            // id: sourcebillpk,
            // copyFlag: false,
            // srcbilltype:'36CC',
            // type: 'link',
            // appcode: '3610PAYR',
            // pagecode: ''
            status: 'browse',
            id: sourcebillpk,
            srcbilltype:'36CC',
            appcode: '3610PAYR',
            pagecode: '3610PAYR_C01'
        });
    }

    if (sourcebilltype == '36K4') {//上收
        props.openTo('/sf/delivery/delivery/main/index.html#/listlinkq', {
            status: 'browse',
            id: sourcebillpk,
            srcbilltype:'36CC',
            appcode: '36320FDA',
            pagecode: '36320FDA_card'
        });
    }
    
    if (sourcebilltype == '36K2') {//下拨
        props.openTo('/sf/allocation/allocate/main/index.html#/linklist', {
            status: 'browse',
            srcbillid: sourcebillpk,
            linkquerytype:'3',
            srcbilltype:'36CC',
            appcode: '36320FA',
            pagecode: '36320FA_L01'
        });
    }

  
    if (sourcebilltype == '36J5') {//委托付款书
        props.openTo('/fts/commission/commissionpayment/main/index.html#/list', {
            status: 'browse',
            id: sourcebillpk,
            srcbilltype:'36CC',
            appcode: '36300TP',
            pagecode: '36300TP_L01'
        });
    }  
    if (sourcebilltype == '36K6') {//资金调拨单
     // props.openTo('/sf/allocation/allocate/main/index.html#/card', {
     //     status: 'browse',
      //    id: sourcebillpk,
      //    srcbilltype:'36CC',
      //    appcode: '36320FA',
      //    pagecode: '36320FA_PAY_C01'
      //});
    }  
    if(sourcebilltype=='D2' || sourcebilltype.search('F2')==0){
        props.openTo('/nccloud/resources/arap/gatheringbill/gatheringbill/main/index.html#/card', {				
            id: sourcebillpk ,
            status: 'browse',
            srcbilltype:'36CC',            
            appcode:'20060GBM',
            pagecode:'20060GBM_CARD_LINK'   
        });
    }
    if(sourcebilltype=='D3' || sourcebilltype.search('F3')==0){
        props.openTo('/nccloud/resources/arap/paybill/paybill/main/index.html#/card', {			
            id: sourcebillpk ,
            status: 'browse',				
            srcbilltype:'36CC',
            appcode:'20080EBM',
            pagecode:'20080EBM_CARD_LINK'   
        });
    }
    if(sourcebilltype=='D4' || sourcebilltype.search('F4')==0){
        props.openTo('/nccloud/resources/cmp/billmanagement/recbill/linkcard/index.html', {			
            status: 'browse',
            id: sourcebillpk ,
            srcbilltype:'36CC',			
            appcode:'36070RBM',
            pagecode:'36070RBMLINK_C01'   
        });
    }
    if (sourcebilltype == 'D5'|| sourcebilltype.search('F5')==0) {//付款结算单
        props.openTo('/cmp/billmanagement/paybill/linkcard/index.html', {
            status: 'browse',
            id: sourcebillpk,
            srcbilltype:'36CC',
            appcode: '36070PBR',
            pagecode: '36070PBR_C02'
        });      
    }  
    if(sourcebilltype=='36J1' || sourcebilltype=='36J5'){
        //委托付款单
        props.openTo('', {			
            status: 'browse',
            id: sourcebillpk ,
            srcbilltype:'36CC',
            appcode:'36300TP',
            pagecode:'36300TP_L01'   
        });
    }
    if(sourcebilltype=='36J2' || sourcebilltype=='36J7'){
        //委托收款单
        props.openTo('/nccloud/resources/fts/commission/commissiongathering/main/index.html#/linkcard', {			
            status: 'browse',
            id: sourcebillpk ,
            srcbilltype:'36CC',
            appcode:'36300TG',
            pagecode:'36300TG_C01_LINK'   
        });
    }

}


/**
 * 删除按钮
 * @param {*} props 
 */
const deleteAction = (props) => {
    let data = props.table.getCheckedRows(tableId);
    if (data.length == 0) {
        toast({ color: 'warning', content: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000027') });/* 国际化处理： 请选择数据*/
        return
    }
    let params = data.map((v) => {
        let id = v.data.values.pk_confirmpay_h.value;
        let ts = v.data.values.ts.value;
        return {
            id, ts
        }
    })
    ajax({
        url: '/nccloud/obm/ebankconfirmpay/delete.do',
        data: params,
        success: (res) => {
            toast({ color: "success", content: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000018') });/* 国际化处理： 删除成功*/
            refreshHtml(that, props);
        }
    });
}


//刷新列表信息
function refreshHtml(that, props) {
    //分页
    let refreshpageInfo = props.table.getTablePageInfo(tableId);
    //查询condition
    let refreshsearchVal = props.search.getAllSearchData(searchid);
    let queryInfo = props.search.getQueryInfo(searchid, false);
    let oid= queryInfo.oid;
    if(refreshsearchVal && refreshsearchVal.conditions){
        setDefData(cachesearchKey, dataSource, refreshsearchVal);
        let data={
            querycondition: refreshsearchVal,
            custcondition: {
                conditions: [
					{
						field: 'billstate',
						value: {
							firstvalue: '-1',
							secondvalue: null
						},
						oprtype: '=',
						datatype: 203,
					}
				],
                logic: "and",
            },
            conditions: refreshsearchVal.conditions || refreshsearchVal,
            pageInfo: refreshpageInfo,
            pagecode: list_page_code,
            //查询区编码
            queryAreaCode: searchid,
            //查询模板id，手工添加在界面模板json中，放在查询区
            oid: oid,  
            querytype: 'tree'
        };
    
        if(refreshsearchVal && refreshsearchVal.conditions){
            ajax({
                url: '/nccloud/obm/ebankconfirmpay/query.do',
                data: data,
                success: (res) => {
                    let { success, data } = res;
                    if (success) {
                        if(data.grid){ 
                            props.table.setAllTableData(tableId, data.grid[tableId]);
                        }else{
                            props.table.setAllTableData(tableId, { rows: [] });
                        }
                        if (data.numvalues) {
                            that.setState({ numvalues: data.numvalues, activeKey:'0'});
                            setDefData(cacheTabKey, dataSource, that.state.numvalues);
                        }else{
                            that.setState({ numvalues: {} });
                        }
                        toast({ 
                            color: 'success', 
                            content:props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000032') /* 国际化处理： 刷新成功*/
                        });
                    }
                }
            });
        }
    }
}
