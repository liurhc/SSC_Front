import searchBtnClick from './searchBtnClick';
import initTemplate from './initTemplate';
import pageInfoClick from './pageInfoClick';
import buttonClick from './buttonClick';
import doubleClick from './doubleClick';
import tabButtonClick from './tabButtonClick';
import setButtonUsability  from './setButtonUsability';
export { searchBtnClick, pageInfoClick,initTemplate,buttonClick,doubleClick,tabButtonClick,setButtonUsability };
