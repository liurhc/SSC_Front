
//主子表列表

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, toast,base, high ,cardCache, getMultiLang,createPageIcon } from 'nc-lightapp-front';
import NCTabs from "../../../../tmpub/pub/util/NCTabs/index";
const { NCTabPane } = NCTabs;
const { NCBreadcrumb, NCMessage } = base;
let { getNextId, getCurrentLastId, deleteCacheById, deleteCacheId, getCacheById, updateCache, addCache, setDefData, getDefData } = cardCache;
const NCBreadcrumbItem = NCBreadcrumb.NCBreadcrumbItem;
const { ApproveDetail, NCUploader, PrintOutput } = high;
let { NCTabsControl, NCFormControl } = base;
import axios from 'axios';
import { buttonClick, initTemplate, afterEvent, searchBtnClick, pageInfoClick, doubleClick, tabButtonClick, tableModelConfirm,setButtonUsability } from './events';
import './index.less';
import {tableId, searchid, appcode,list_page_code,dataSource, cachesearchKey,cacheTabKey,cacheTabActiveKey} from '../cons/constant.js';

class List extends Component {
	constructor(props) {
		super(props);
		this.state = {
			// 选中页签的信息
			numvalues: {},
			activeKey: '0',
			billtype: '',
			showAppr: false,
			// 附件相关 start
			//单据pk
			billId: '',
			//附件管理使用单据编号
			billno: '',
			//控制附件弹出框
			showUploader: false,
			//控制弹出位置
			target: null,
			// 附件相关 end
			//输出用   
			outputData: {
				funcode: '', //功能节点编码，即模板编码
				nodekey: '', //模板节点标识
				printTemplateID: '', //模板id
				oids: [],
				outputType: 'output'
			}
		};
		initTemplate.call(this, props);
	}

	componentDidMount() {
		let { currentLocale } = this.state;
		let type = this.props.getUrlParam("type");
		
		if (type && type == "link") {//联查
			this.getlinkdata();
		}else{
			this.restoreData();
		}
		
		setButtonUsability.call(this,this.props);
	}


	//通过单据id查询单据信息
	getlinkdata = () => {
		let yurrefs = this.props.getUrlParam("yurrefs");
		console.log(this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000034'),yurrefs);/* 国际化处理： 联查支付确认单*/
		let pageInfo = this.props.table.getTablePageInfo(tableId);
		let data = {
			yurrefs: yurrefs,
			pageid: list_page_code,
			pageInfo: pageInfo,
		};
		ajax({
			url: '/nccloud/obm/ebankconfirmpay/querylinklistdata.do',
			data: data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if (data) {
						let rowlenght = data.grid[tableId].rows;
						if (rowlenght.length == 1) {
							let record = rowlenght[0];
							//1条数据跳转到卡片页面
							this.props.pushTo("/card", {
								status: 'browse',
								id: record.values.pk_confirmpay_h && record.values.pk_confirmpay_h.value
							});
						} else {
							//多条数据跳转到列表页面
							this.props.table.setAllTableData(tableId, data.grid[tableId]);
						}
					} else {
						this.props.table.setAllTableData(tableId, { rows: [] });
						
					}
					setButtonUsability.call(this, this.props);
				}
			}

		});

	};


// 还原列表页数据
	restoreData = () =>{
		//获取页签数据
		let cachestate = getDefData(cacheTabKey, dataSource);
		let cachestateTabActiveKey = getDefData(cacheTabActiveKey, dataSource);
		if (cachestate) {
			let keys = Object.keys(cachestate);
			this.setState({ numvalues: cachestate });
			this.setState({ activeKey: cachestateTabActiveKey });
		}
	}
	
	//关闭联查审批意见
	closeApprove = () => {
		this.setState({
			showAppr: false
		});
	};
	


	// 附件的关闭点击
	onHideUploader = () => {
		this.setState({
			showUploader: false
		})
	}


	//删除单据
	delConfirm = () => {
		const selectedData = this.props.table.getCheckedRows(tableId);
		let dataArr = [];
		let tsArr = [];
		if (selectedData.length == 0) {
			NCMessage.create({ content: this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000027'), color: 'warning', position: 'top' });/* 国际化处理： 请选择数据*/
			return;
		}

		let params = selectedData.map((v) => {
			let id = v.data.values.pk_confirmpay_h.value;
			let ts = v.data.values.ts.value;
			return {
				id, ts
			}
		})

		ajax({
			url: '/nccloud/obm/ebankconfirmpay/delete.do',
			data: params,
			success: (res) => {
				if (res.success) {
					if (res.data && res.data.errormsg) {
						toast({ color: 'warning', content: res.data.errormsg });

					} else {
						toast({ color: 'success', content: this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000018') });/* 国际化处理： 删除成功*/
						if (res.data && res.data.successpks) {
							const selecteDelData = this.props.table.getCheckedRows(tableId);
							let successpks = res.data.successpks.split(',');
							for (let index = 0; index < selecteDelData.length; index++) {
								const pk_confirmpay_h = selecteDelData[index].data.values.pk_confirmpay_h.value
								if (successpks.indexOf(pk_confirmpay_h) >= 0) {
									deleteCacheId(tableId, pk_confirmpay_h);
									this.props.table.deleteTableRowsByIndex(tableId, selecteDelData[index].index);
								}
							}
						}
					}
				}
			}
		});
	};

	beforeUpload(billId, fullPath, file, fileList) {
		// 参数：单据id，当前选中分组path、当前上传文件对象，当前文件列表
		console.log(billId, fullPath, file, fileList);

		const isJPG = file.type === 'image/jpeg';
		if (!isJPG) {
			alert(this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000019'))/* 国际化处理： 只支持jpg格式图片*/
		}
		const isLt2M = file.size / 1024 / 1024 < 2;
		if (!isLt2M) {
			alert(this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000020'))/* 国际化处理： 上传大小小于2M*/
		}
		return isJPG && isLt2M;
		// 备注： return false 不执行上传  return true 执行上传
	}

	getData = (serval) => {
		let servalTemp = serval && serval[0] ? serval[0] : null;
		//分页
		let datapageInfo = this.props.table.getTablePageInfo(tableId);
		//查询condition
		let datasearchVal = this.props.search.getAllSearchData(searchid);
		let queryInfo = this.props.search.getQueryInfo(searchid, false);
		let oid = queryInfo.oid;
		if (datasearchVal && datasearchVal.conditions) {
			let data = {
				querycondition: datasearchVal,
				custcondition: {
					conditions: [servalTemp],
					logic: "and",
				},
				pagecode: list_page_code,
				pageInfo: datapageInfo,
				queryAreaCode: searchid,
				oid: oid,
				querytype: 'tree'
			};

			ajax({
				url: '/nccloud/obm/ebankconfirmpay/query.do',
				data: data,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						if (data) {
							if (data.grid) {
								this.props.table.setAllTableData(tableId, data.grid[tableId]);
							} else {
								this.props.table.setAllTableData(tableId, { rows: [] });
							}
							if (data.numvalues) {
								this.setState({ numvalues: data.numvalues });
							}
							// this.setState({ activeKey: 5 });
							setDefData(cacheTabKey, dataSource, this.state.numvalues);
		                    setDefData(cacheTabActiveKey, dataSource, this.state.activeKey);
						} else {
							this.props.table.setAllTableData(tableId, { rows: [] });
							this.setState({ numvalues: {} });
						}
					}
				}
			});
			setButtonUsability.call(this,this.props);
		}
	};

	//页签筛选
	navChangeFun = (status, className, e) => {
		let serval;
		switch (status) {

			// 自由
			case '0':
				serval = [
					{
						field: 'billstate',
						value: {
							firstvalue: '-1',
							secondvalue: null
						},
						oprtype: '=',
						datatype: 203,
					}
				];
				this.getData(serval);
				this.setState({ activeKey: '0' });
				break;
			// 提交
			case '1':
				serval = [
					{
						field: 'billstate',
						value: {
							firstvalue: '3',
							secondvalue: null
						},
						oprtype: '=',
						datatype: 203,
					}
				];
				this.getData(serval);
				this.setState({ activeKey: '1' });
				break;
			// 审批中
			case '2':
				serval = [
					{
						field: 'billstate',
						value: {
							firstvalue: '2',
							secondvalue: null
						},
						oprtype: '=',
						datatype: 203,
					}
				];
				this.getData(serval);
				this.setState({ activeKey: '2' });
				break;
			// 审批通过
			case '3':
				serval = [
					{
						field: 'billstate',
						value: {
							firstvalue: '1',
							secondvalue: null
						},
						oprtype: '=',
						datatype: 203,
					}
				];
				this.getData(serval);
				this.setState({ activeKey: '3' });
				break;
			// 审批未通过
			case '4':
				serval = [
					{
						field: 'billstate',
						value: {
							firstvalue: '0',
							secondvalue: null
						},
						oprtype: '=',
						datatype: 203,
					}
				];
				this.getData(serval);
				this.setState({ activeKey: '4' });
				break;
			// 全部
			case '5':
				serval = [
					{
						field: 'billstate',
						value: {
							firstvalue: null,
							secondvalue: null
						},
						oprtype: '=',
						datatype: 203,
					}
				];
				this.setState({ activeKey: '5' });
				this.getData(serval);
				break;
			default:
				break;
		}
	};



	render() {
		let numvalues = this.state.numvalues;
		let { table, button, search, modal,ncmodal } = this.props;
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { createButton } = button;
		let { createModal } = modal;
		let { createSimpleSearch } = this.props.simpleSearch;
		let createNCModal = ncmodal.createModal;
		// 附件相关内容变量
		let { showUploader, target, billno, billId } = this.state;
		return (
			<div className="nc-bill-list">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
					{/*页面大图标*/}
						{createPageIcon()}
						<h2 className='title-search-detail'>
						{this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000022')}</h2>{/* 国际化处理： 支付确认单*/}
					</div>
					<div className="header-button-area">
						{/* 小应用注册button */}
						{this.props.button.createButtonApp({
							area: 'list_head',
							buttonLimit: 7,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}

					</div>
				</div>
				<div className="nc-bill-search-area">
					{NCCreateSearch(searchid, {//查询区
						// 显示高级按钮
						showAdvBtn: true,
						// 添加高级查询区自定义页签 (fun), return Dom 
						addAdvTabs: this.addAdvTabs,
						clickSearchBtn: searchBtnClick.bind(this),
						defaultConditionsNum: 10 //默认显示几个查询条件
					})}
				</div>
				
				<div className="tab-definInfo-area">
					<NCTabs activeKey={this.state.activeKey} 
						onChange={(v) => {
							this.navChangeFun.call(this, v);
						}}>
						{/* 自由 */}
						<NCTabPane key={'0'} tab={this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000035')+ '(' + (numvalues && numvalues.DTJ || 0) + ')'}/>{/* 国际化处理： 待提交*/}
						{/* 审批进行中*/}
						<NCTabPane key={'2'} tab={this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000036')+ '(' + (numvalues && numvalues.SPZ || 0) + ')'}/>{/* 国际化处理： 审批中*/}
						{/* 国际化处理： 全部*/}
						<NCTabPane key={'5'} tab={this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000037')}/>{/* 国际化处理： 全部*/}
					</NCTabs>
					
				</div>
				
				<div className="nc-bill-table-area">
					{createSimpleTable(tableId, {//列表区
						handlePageInfoChange: pageInfoClick,
						tableModelConfirm: tableModelConfirm,
						onRowDoubleClick: doubleClick.bind(this),
						showCheck: true,
						showIndex: true,
						onSelected: setButtonUsability.bind(this, this.props),
						onSelectedAll: setButtonUsability.bind(this, this.props),
						dataSource: dataSource,
						//给表格加pkname: 表格数据的主键名字(key)
						pkname: 'pk_confirmpay_h',
						componentInitFinished:()=>{
							//缓存数据赋值成功的钩子函数
							//若初始化数据后需要对数据做修改，可以在这里处理
						}
					})}
				</div>
				<div>
					<ApproveDetail
						billtype={this.state.billtype}
						billid={this.state.billId}
						show={this.state.showAppr}
						close={this.closeApprove}
					/>
				</div>
				<div className="nc-faith-demo-div2">
					{/* 这里是附件上传组件的使用，需要传入三个参数 */}
					{showUploader &&
						<NCUploader
							billId={billId}
							target={target}
							placement={'bottom'}
							billNo={billno}
							onHide={this.onHideUploader}
							//beforeUpload={this.beforeUpload}
						/>
					}
				</div>
				{/* 输出 */}
				<div>
					<PrintOutput
						ref="printOutput"
						url="/nccloud/obm/ebankconfirmpay/print.do"
						data={this.state.outputData}
						callback={this.onSubmit}
					/>
				</div>
				
				{createModal('delete', {
					title: this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000023'),/* 国际化处理： 删除*/
					content: this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000038'),/* 国际化处理： 确定要删除所选数据吗?*/
					beSureBtnClick: this.delConfirm,
					//hasCloseBtn: false,
                    //className:'senior'
				})}
				
			</div>

		);
	}
}

List = createPage({
	//initTemplate: initTemplate
	//billinfo: {
	//	billtype: 'grid',
	//	pagecode: list_page_code,
	//},
	mutiLangCode: appcode
})(List);

//ReactDOM.render(<List />, document.querySelector('#app'));
export default List;
