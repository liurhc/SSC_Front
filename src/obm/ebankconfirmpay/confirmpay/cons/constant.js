/**
 * 表体区域
 */
export const tableId = 'cdtrdetail';
/**
 * 表头区域
 */
export const formId = 'head';
/**
 * 操作信息区域
 */
export const card_fromtail_id = 'head_tail';
/**
 * 页面编码
 */
export const card_page_code = '36100CONFM_C01';
export const list_page_code = '36100CONFM_L01';

export const appid = '0001Z61000000003AE2P';
export const appcode = '36100CONFM';
/**
 * 编辑态按钮
 */
export const editButtons = ['saveBtn', 'cancelBtn', 'billLinkQueryBtn'];
/**
 * 浏览态按钮
 */
export const browseButtons = ['editBtn', 'deleteBtn','reviewapprove', 'commitBtn', 'billLinkQueryBtn', 'printBtn', 'export', 'file', 'refreshBtn'];
/**
 * 提交按钮组
 */
export const submitButtons = ['uncommitBtn', 'reviewapprove','billLinkQueryBtn', 'printBtn', 'export', 'file', 'refreshBtn'];
/**
 * 审批按钮组
 */
export const allButtons = ['saveBtn', 'cancelBtn', 'editBtn','reviewapprove', 'deleteBtn', 'commitBtn', 'uncommitBtn', 'reviewapprove','billLinkQueryBtn', 'printBtn', 'export', 'file', 'refreshBtn'];

export const dataSource = 'tm.obm.confirmpay.confirmpaydataSource';

export const searchid = '36100CONFM_search';


export const cachesearchKey = '36100CONFM_L01_search';
export const cacheTabActiveKey = '36100CONFM_L01_tabActive';

export const cacheTabKey = '36100CONFM_L01_tab';


const listbtn = {
    editBtn: 'editBtn',
    deleteBtn: 'deleteBtn',
    commitBtn: 'commitBtn',
    uncommitBtn: 'uncommitBtn',
    billLinkQueryBtn: 'billLinkQueryBtn',
    printBtn: 'printBtn',
    export: 'export',
    file: 'file',
    refreshBtn: 'refreshBtn',

};

export const listbutton = {
    selectnulldisabled: [
        listbtn.editBtn,
        listbtn.deleteBtn,
        listbtn.commitBtn,
        listbtn.uncommitBtn,
        listbtn.billLinkQueryBtn,
        listbtn.printBtn,
        listbtn.export,
        listbtn.file,
        //listbtn.refreshBtn,
    ],
    approvefaildisabled: [
        listbtn.editBtn,
        listbtn.deleteBtn,
        listbtn.commitBtn,
        listbtn.uncommitBtn,
        //listbtn.billLinkQueryBtn,
        //listbtn.printBtn,
        //listbtn.export,
        //listbtn.file,
        //listbtn.refreshBtn,
    ],
    approvesuccdisabled: [
        listbtn.editBtn,
        listbtn.deleteBtn,
        listbtn.commitBtn,
        //listbtn.uncommitBtn,
        //listbtn.billLinkQueryBtn,
        //listbtn.printBtn,
        //listbtn.export,
        //listbtn.file,
        //listbtn.refreshBtn,
    ],
    approveingdisabled: [
        listbtn.editBtn,
        listbtn.deleteBtn,
        listbtn.commitBtn,
        //listbtn.uncommitBtn,
        //listbtn.billLinkQueryBtn,
        //listbtn.printBtn,
        //listbtn.export,
        //listbtn.file,
        //listbtn.refreshBtn,
    ],
    commitdisabled: [
        listbtn.editBtn,
        listbtn.deleteBtn,
        listbtn.commitBtn,
        //listbtn.uncommitBtn,
        //listbtn.billLinkQueryBtn,
        //listbtn.printBtn,
        //listbtn.export,
        //listbtn.file,
        //listbtn.refreshBtn,
    ],
    freedisabled: [
       // listbtn.editBtn,
       //listbtn.deleteBtn,
       // listbtn.commitBtn,
        listbtn.uncommitBtn,
       // listbtn.billLinkQueryBtn,
       // listbtn.printBtn,
       // listbtn.export,
       // listbtn.file,
       // listbtn.refreshBtn,
    ],
    

};
