//主子表卡片
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast, high,cardCache ,getMultiLang,createPageIcon} from 'nc-lightapp-front';

import { Radio } from 'tinper-bee';
import Sign from '../../../../tmpub/pub/util/ca';
//引入组织版本试图api
import { orgVersionView } from "../../../../tmpub/pub/util/version/index";
import axios from 'axios';
import './index.less';
import {appcode,tableId,formId, searchid, card_page_code,list_page_code,
	editButtons,browseButtons,submitButtons,allButtons,dataSource,card_fromtail_id,cachesearchKey,cacheTabKey} from  '../cons/constant.js';
import { buttonClick, initTemplate, afterEvent, pageInfoClick,tableButtonClick ,buttonVisible} from "./events";
let { NCFormControl, NCButtonGroup, NCButton,NCScrollElement,NCScrollLink ,NCAnchor} = base;

let { getNextId, getCurrentLastId, deleteCacheById, deleteCacheId, getCacheById, updateCache, addCache, setDefData, getDefData } = cardCache;
const { ApproveDetail,NCUploader,PrintOutput } = high;
//返回button
const { NCBackBtn } = base;
const { NCAffix } = base;
class Card extends Component {
	constructor(props) {
		super(props);
		this.isCa= {};
		this.state = {
			bill_type: "36CC",
			showAppr: false,
			totalcount: 0,
			applycount: 0,
			//返回箭头
			showNCbackBtn: false,
			
			// 单据编号
			vbillno: '',
			// 附件相关 start
			//单据pk
			billId:'',
			//附件管理使用单据编号
            billno:'',
			//控制附件弹出框
            showUploader: false,
			//控制弹出位置
            target: null,

			// 附件相关 end
			//输出用   
			outputData: {
				funcode: '', //功能节点编码，即模板编码
				nodekey: '', //模板节点标识
				printTemplateID: '', //模板id
				oids: [],
				outputType: 'output'
			}
		};
		initTemplate.call(this, props);
	}


	componentDidMount() {
		orgVersionView(this.props, formId);
		let type = this.props.getUrlParam("type");
		let status = this.props.getUrlParam("status");
		let pk = this.props.getUrlParam("id");
		if (status == "add") {
				this.addConfirm();
		}else if (status == "browse") {

			if (type && type == "link") {//联查
				this.getlinkdata(pk);
			}else if (pk && pk != "undefined") {//跳转卡片
				this.getdata(pk);
			}
		}else if (status == "edit") {
			this.getdata(pk);
		}
	}

	componentWillMount() {
		
		// 关闭浏览器
		window.onbeforeunload = () => {
			let status = this.props.form.getFormStatus(formId);
			if (status != 'browse') {
				/* 国际化处理： 当前单据未保存，您确认离开此页面？*/
				return this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000014');/* 国际化处理： 当前单据未保存，您确认离开此页面？*/
			}
		};
	}



	//生成支付确认单
	addConfirm = () => {
        let pk = this.props.getUrlParam("id");
		let pk_org = this.props.getUrlParam("pk_org");
		let pk_group = this.props.getUrlParam("pk_group");
		let srcbillcode = this.props.getUrlParam("srcbillcode");
		let srcbilltype = this.props.getUrlParam("srcbilltype");
		let srcnodecode = this.props.getUrlParam("srcnodecode");
		let srcpkid = this.props.getUrlParam("srcpkid");
		let srcsystem = this.props.getUrlParam("srcsystem");
		let yurref = this.props.getUrlParam("yurref");
		let url = "/nccloud/obm/ebankconfirmpay/linkadd.do"; 
		if (srcbilltype === "36C3") {
			url = "/nccloud/obm/ebankpaylog/ebankLogCreatPmtConfirm.do"; 
		}
		ajax({
			url: url,
			data: {
				pk: pk,
				pk_org: pk_org,
				pk_group: pk_group,
				srcbillcode: srcbillcode,
				srcbilltype: srcbilltype,
				srcnodecode: srcnodecode,
				srcpkid: srcpkid,
				srcsystem: srcsystem,
				yurref: yurref
			},
			success: res => {
				if (res.data && res.data.head) {
					this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
					//单据号赋值
					let vbillno = res.data.head[formId].rows[0].values.vbillno;
					this.setState({
						vbillno: vbillno && vbillno.value
					});
					//updateCache('pk_confirmpay_h', res.data.head[formId].rows[0].values.pk_confirmpay_h.value, res.data, formId, dataSource);
				}
				if (res.data && res.data.body) {
					this.props.cardTable.setTableData(tableId, res.data.body[tableId]);
					let totalcount = this.props.cardTable.getNumberOfRows(tableId);
					let arr = this.props.cardTable.getAllRows(tableId);
					let count = 0;
					this.setState({ count });
					this.setState({ totalcount });
				}
				this.toggleShow();
			},

		})

	};

  //切换页面状态
   toggleShow = () => {
	//按钮显隐性
	buttonVisible.call(this, this.props);
    };

	
	//关闭联查审批意见
	closeApprove = () => {
		this.setState({
			showAppr: false
		});
	};

	//通过单据id查询单据信息
	getdata = pk => {
		ajax({
			url: '/nccloud/obm/ebankconfirmpay/querycard.do',
			data: { pk },
			success: res => {
				if (res.data && res.data.head) {
					this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
					//单据号赋值
					let vbillno = res.data.head[formId].rows[0].values.vbillno;
					let sourcebilltype = res.data.head[formId].rows[0].values.sourcebilltype;
					let sourcebilltypename=this.transtoname(sourcebilltype);
					this.props.form.setFormItemsValue(formId, { sourcebilltype: { display: sourcebilltypename, value: sourcebilltype.value } });

					this.setState({
						vbillno: vbillno && vbillno.value
					});
					updateCache('pk_confirmpay_h', res.data.head[formId].rows[0].values.pk_confirmpay_h.value, res.data, formId, dataSource);
				}
				if (res.data && res.data.body) {
					this.props.cardTable.setTableData(tableId, res.data.body[tableId]);
					let totalcount = this.props.cardTable.getNumberOfRows(tableId);
					let arr = this.props.cardTable.getAllRows(tableId);
					let count = 0;
					this.setState({ count });
					this.setState({ totalcount });
				}
				this.toggleShow();
			}
		});
	};


	// 来源单据类型编码转为名称
	transtoname = (sourcebilltype) => {

		if (sourcebilltype.value === "D5") {
			return this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000006');/* 国际化处理： 付款结算单*/
		}
		if (sourcebilltype.value === "36K2") {
			return this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000007');/* 国际化处理： 下拨单*/
		}
		if (sourcebilltype.value === "36K6") {
			return this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000008');/* 国际化处理： 资金调拨单*/
		}
		if (sourcebilltype.value === "36J5") {
			return this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000009');/* 国际化处理： 委托付款书*/
		}
		if (sourcebilltype.value === "36K4") {
			return this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000010');/* 国际化处理： 上收单*/
		}
		if (sourcebilltype.value === "36C9") {
			return this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000011');/* 国际化处理： 工资清单*/
		}
		
		return sourcebilltype.value;
	}
//通过单据id查询单据信息
getlinkdata = pk => {
	ajax({
		url: '/nccloud/obm/ebankconfirmpay/querylinkdata.do',
		data: { pk },
		success: res => {
			if (res.data && res.data.head) {
				this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
				//单据号赋值
				let vbillno = res.data.head[formId].rows[0].values.vbillno;
				this.setState({
					vbillno: vbillno && vbillno.value
				});
				//updateCache('pk_confirmpay_h', res.data.head[formId].rows[0].values.pk_confirmpay_h.value, res.data, formId, dataSource);
				addCache(res.data.head[formId].rows[0].values.pk_confirmpay_h.value, res.data, formId, dataSource);
			}
			if (res.data && res.data.body) {
				this.props.cardTable.setTableData(tableId, res.data.body[tableId]);
				let totalcount = this.props.cardTable.getNumberOfRows(tableId);
				let arr = this.props.cardTable.getAllRows(tableId);
				let count = 0;
				this.setState({ count });
				this.setState({ totalcount });
			}
			this.toggleShow();
		}
	});
};



//判断用户是否为ca用户
getIsca = () => {
	return new Promise(resolve => ajax({
		type: 'post',
		url: '/nccloud/tmpub/pub/iscauser.do', 
		loading: false,
		async: false,
		success: res => {
			if (res.success) {
				this.isCa= {
					signal: true,
					isCaUser: res.data
				};
				resolve(res.data);
			}
		},
		error: res => {
			this.isCa= {
				signal: true,
				isCaUser: false
			};
			resolve(false);
			toast({ color: 'warning', content: this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000015') });/* 国际化处理： 获取CA用户出错*/
		},
	}));
}

	//保存单据
	saveClick = async() => {

		let { signal, isCaUser }= this.isCa;
		if (!signal) {
			let isContinue= await this.getIsca();
			//第一次点保存的时候会走这
			if (!isContinue) {
				toast({ color: 'warning', content: this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000016') });/* 国际化处理： 此次进行的操作需要CA签名，请切换CA用户*/
				return;
			}
		} else if (signal && !isCaUser) {//再次点保存的时候会走这，优化性能
			toast({ color: 'warning', content: this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000016') });/* 国际化处理： 此次进行的操作需要CA签名，请切换CA用户*/
			return;
		}

		this.props.cardTable.filterEmptyRows(tableId, ['numberindex'], 'except');
		let CardData = this.props.createMasterChildData(card_page_code, formId, tableId);
		let saveBeforePk = this.props.form.getFormItemsValue(formId, 'pk_confirmpay_h');
		//let flag =this.props.form.isCheckNow(formId);
		//if(!flag)
		//    return;
		let tablecheck= this.props.cardTable.checkTableRequired(tableId);
		if(!tablecheck) {
		  return;
		}

         console.log(CardData,"sign before CardData");
		 let result = await Sign({
		 	data: CardData,
		 	isSign: true, 
		 	isKey:true,
		 	encryptVOClassName:"nccloud.dto.obm.ebankconfirmpay.confirmpay.vo.ConfirmPayBillEncryptVO4NCC"			
		 });
		 if (result.isStop) {
		 	return;
		 } 
		 CardData= result.data;
		 console.log(CardData,"sign after CardData");


		let url = "/nccloud/obm/ebankconfirmpay/save.do"; //新增保存
		let status = this.props.getUrlParam("status");
		if (status === "edit") {
			url = "/nccloud/obm/ebankconfirmpay/update.do"; //修改保存
		}
		ajax({
			url: url,
			data: CardData,
			success: res => {
				if (res.success) {
					if (res.data) {
						if (res.data.head && res.data.head[formId]) {
							this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
							//单据号赋值
				            let vbillno = res.data.head[formId].rows[0].values.vbillno;
				            this.setState({
					               vbillno: vbillno && vbillno.value
							});
							
							
							if(saveBeforePk && saveBeforePk.value){
								updateCache('pk_confirmpay_h', res.data.head[formId].rows[0].values.pk_confirmpay_h.value, res.data, formId, dataSource);
							}else{
								addCache(res.data.head[formId].rows[0].values.pk_confirmpay_h.value, res.data, formId, dataSource);
							}
							let sourcebilltype = res.data.head[formId].rows[0].values.sourcebilltype;
				        	let sourcebilltypename=this.transtoname(sourcebilltype);
				        	this.props.form.setFormItemsValue(formId, { sourcebilltype: { display: sourcebilltypename, value: sourcebilltype.value } });
						}
						if (res.data.body && res.data.body[tableId]) {
							this.props.cardTable.setTableData(tableId, res.data.body[tableId]);
						}
				
					}
					
					
					let pk_confirmpay_h = res.data.head[formId].rows[0].values.pk_confirmpay_h;
					this.props.pushTo("/card",{
						id: pk_confirmpay_h,
						status: 'browse',
					  });
					  this.toggleShow();
				}
				toast({ content: this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000017'), color: "success" });/* 国际化处理： 保存成功*/
			}
		});
	};


	//删除单据
	delConfirm = () => {
		ajax({
			url: "/nccloud/obm/ebankconfirmpay/delete.do",
			data: [
				{
					id: this.props.form.getFormItemsValue(formId, "pk_confirmpay_h").value,
					ts: this.props.form.getFormItemsValue(formId, "ts").value
				}
			],
			success: res => {
				if (res.success) {
					toast({ color: 'success', content: this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000018') });/* 国际化处理： 删除成功*//* 国际化处理： 删除成功*/
					// props.pushTo("/list");
					let deleteid = this.props.getUrlParam("id");
					//根据当前id,获取下个id
					/*
					* id：数据主键的值
					* dataSource: 缓存数据命名空间
					*/
					let deletenextId = getNextId(deleteid, dataSource);
					//调用删除缓存数据方法
					/*
					* idname: 数据主键的命名
					* id：数据主键的值
					* dataSource: 缓存数据命名空间
					*/
					deleteCacheById('pk_confirmpay_h', deleteid, dataSource);
					//根据nextId查询下条数据
					//注意：查询下条数据时，也同样需要先判断有没有缓存数据，没有缓存数据时，再发查询请求。
					this.props.setUrlParam({
						status: 'browse',
						id: deletenextId ? deletenextId:'',
					});
					this.setState({
						vbillno: null
				    });
					this.refresh();
					//if (res.data && res.data.errormsg) {
					//	toast({ color: 'warning', content: res.data.errormsg });

					//} else {
					//this.props.linkTo('/obm/ebankconfirmpay/confirmpay/list/index.html', {
					//	status: 'browse',
					//	type: 'link',
					//	appcode: appcode,
					//	pagecode: card_page_code
					 // })
				//}
			  }
			}
		});
	};


//加载数据刷新数据
refresh = () => {
	let status = this.props.getUrlParam('status');
	let pk = this.props.getUrlParam('id');
	//查询单据详情
	if (status === 'browse') {
		let refreshcardData = getCacheById(pk, dataSource);
		if(pk){
			if(refreshcardData){
				this.props.form.setAllFormValue({[formId]: refreshcardData.head[formId]});
				this.props.cardTable.setTableData(tableId, refreshcardData.body[tableId]);
				this.setState({
					vbillno: refreshcardData.head[formId].rows[0].values.vbillno.value,
				});
				this.props.pushTo("/card",{
					id: pk,
					status: 'browse',
				});
				this.toggleShow();
			}else{
				ajax({
					url: '/nccloud/obm/ebankconfirmpay/querycard.do',
					data: {pk},
					success: (res) => {
						if (res.data) {
							if (res.data.head) {
								this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
								//页签赋值
								let vbillno = res.data.head[formId].rows[0].values.vbillno;
								this.setState({
									vbillno: vbillno && vbillno.value
								});
								updateCache('pk_confirmpay_h', res.data.head[formId].rows[0].values.pk_confirmpay_h.value, res.data, formId, dataSource);
							}
							if (res.data.body) {
								this.props.cardTable.setTableData(tableId, res.data.body[tableId]);
							}
							
							this.props.pushTo("/card",{
								id: pk,
								status: 'browse',
							});
							this.toggleShow();
						} 
						this.setState({showNCbackBtn: true});
					}
				});
			}
		}else{
			//清空数据
			this.props.form.EmptyAllFormValue(formId);
			this.props.cardTable.setTableData(tableId, { rows: [] });
			this.props.pushTo("/card",{
				id: '',
				status: 'browse',
			});
			this.toggleShow();
		}
	}
	else if (status === 'edit') {
		let that = this;
		ajax({
			url: '/nccloud/obm/ebankconfirmpay/querycard.do',
			data: { pk },
			success: (res) => {
				if (res.data) {
					if (res.data.head) {
						this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
						//页签赋值
						let vbillno = res.data.head[formId].rows[0].values.vbillno;
						this.setState({
							vbillno: vbillno.value
						});
						updateCache('pk_confirmpay_h', res.data.head[formId].rows[0].values.pk_confirmpay_h.value, res.data, formId, dataSource);
					}
					if (res.data.body) {
						this.props.cardTable.setTableData(tableId, res.data.body[tableId]);
					}
					this.toggleShow();
				} 
			}
		});
	}

}


// 附件的关闭点击
    onHideUploader = () => {
        this.setState({
            showUploader: false
        })
    }

    beforeUpload(billId, fullPath, file, fileList) {  
        // 参数：单据id，当前选中分组path、当前上传文件对象，当前文件列表
        console.log(billId, fullPath, file, fileList);

        const isJPG = file.type === 'image/jpeg';
        if (!isJPG) {
            alert(this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000019'))/* 国际化处理： 只支持jpg格式图片*/
        }
        const isLt2M = file.size / 1024 / 1024 < 2;
        if (!isLt2M) {
            alert(this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000020'))/* 国际化处理： 上传大小小于2M*/
        }
        return isJPG && isLt2M;
        // 备注： return false 不执行上传  return true 执行上传
	}


	// 返回箭头按钮
	ncBackBtnClick = () => {
		//先跳转列表
		this.props.pushTo("/list");
		
	};

	render() {
		let { cardTable, form, modal,ncmodal, button,cardPagination } = this.props;
		let buttons = this.props.button.getButtons();
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createCardPagination } = cardPagination;
		let { createModal } = modal;
		let createNCModal = ncmodal.createModal;
		let { createButtonApp } = this.props.button;
		let { createButton, getButtons } = button;
		const { createBillHeadInfo } = this.props.BillHeadInfo;
		let { showUploader, target, billno, billId, showNCbackBtn,vbillno } = this.state;
		return (
			<div className="nc-bill-card">
				<div className="nc-bill-top-area">
			
				<NCAffix>
					<div className="nc-bill-header-area">
						<div className="header-title-search-area">
						{
								createBillHeadInfo(
									{
										title: this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000022'),  //标题
										billCode: this.state.vbillno,     //单据号
										backBtnClick: () => {           //返回按钮的点击事件
											this.ncBackBtnClick();
										}
									}
								)}
						</div>
						<div className="header-button-area">
							{createButtonApp({
								area: "card_head",
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector(".header-button-area")
							})}
						</div>
						<div className='header-cardPagination-area' style={{ float: 'right' }}>
								{createCardPagination({
									handlePageInfoChange: pageInfoClick.bind(this),
									//dataSource: dataSource
								})}
							</div>
					</div>
				</NCAffix>
				<NCScrollElement name='forminfo'>
				<div className="nc-bill-form-area">
					{createForm(formId, {
						onAfterEvent: afterEvent.bind(this)
					})}
				</div>
				</NCScrollElement>
				</div>
					<NCScrollElement name='businfo'>
						<div className="nc-bill-table-area">
							{createCardTable(tableId, {
								showIndex: true,
								showCheck: true,
								onAfterEvent: afterEvent.bind(this)
							})}
						</div>
					</NCScrollElement>

				{createModal("delete", {
					title: this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000023'),/* 国际化处理： 删除*/
					content: this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000024'),/* 国际化处理： 确定要删除吗？*/
					beSureBtnClick: this.delConfirm,
					//  模态框大小 sm/lg/xlg,
					size:'sm', 
					className:'junior'
				})}
				<div>
					<ApproveDetail
						billtype={this.state.billtype}
						billid={this.state.billId}
						show={this.state.showAppr}
						close={this.closeApprove}
					/>
				</div>
				 {/* 这里是附件上传组件的使用，需要传入三个参数 */}{/* 国际化处理： 这里是附件上传组件的使用,需要传入三个参数*/}
				<div className="nc-faith-demo-div2">
                    {showUploader &&
                        <NCUploader
                            billId={billId}
                            billNo={billno}
							onHide={this.onHideUploader}
							//beforeUpload={this.beforeUpload}
                        />
                    }
                </div>
                {/* 输出 */}
				<div>
					<PrintOutput
						ref="printOutput"
						url="/nccloud/obm/ebankconfirmpay/print.do"
						data={this.state.outputData}
						callback={this.onSubmit}
					/>
				</div>
			</div>
		);
	}
}

Card = createPage({
	billinfo:{
        billtype: 'card', 
        pagecode: card_page_code, 
        headcode: formId,
        bodycode: tableId
    },
	//initTemplate: initTemplate
	mutiLangCode: appcode
})(Card);

//ReactDOM.render(<Card />, document.querySelector('#app'));
export default Card;
