import { ajax, cardCache } from 'nc-lightapp-front';
import { tableId, searchid, card_page_code, formId, dataSource } from '../../cons/constant.js';
export default function (props, pks) {
    let { getCacheById, updateCache } = cardCache;
    props.setUrlParam(pks);
    /*
    * id：数据主键的值
    * dataSource: 缓存数据命名空间
    */

    if (pks) {
        let pk =pks;
        let cardData = getCacheById(pk, dataSource);
        if (cardData) {
            this.props.form.setAllFormValue({ [formId]: cardData.head[formId] });
            this.props.cardTable.setTableData(tableId, cardData.body[tableId]);
            this.setState({
                vbillno: cardData.head[formId].rows[0].values.vbillno.value,
            });
            this.toggleShow();
        } else {
           
            ajax({
                url: '/nccloud/obm/ebankconfirmpay/querycard.do',
                data: {pk},
                success: (res) => {
                    if (res.data) {
                        let vbillno = '';
                        if (res.data.head) {
                            this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
                            /*
                            * idname: 数据主键的命名
                            * id：数据主键的值
                            * headAreacode: 卡片表头的区域编码
                            * dataSource: 缓存数据命名空间
                            */
                            updateCache('pk_confirmpay_h', pk, res.data, formId, dataSource);
                            vbillno = res.data.head[formId].rows[0].values.vbillno.value;
                            this.setState({
                                vbillno: vbillno
                            });
                        }
                        if (res.data.bodys) {
                            this.props.cardTable.setTableData(tableId, res.data.bodys[tableId]);
                        }
                        props.pushTo("/card", {
                            status: 'browse',
                            id: pk,
                        });
                        this.toggleShow();
                    } else {
                        this.props.form.setAllFormValue({ [formId]: { rows: [] } });
                        this.props.cardTable.setTableData(tableId, { rows: [] });
                    }
                }
            });
        }
    }
}
