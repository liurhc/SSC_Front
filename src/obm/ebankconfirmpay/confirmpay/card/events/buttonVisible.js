/**
 * 处理按钮的可见性
 * @param {*} props 界面内置对象
 */
import {
    card_page_code, formId, tableId, card_fromtail_id, allButtons, editButtons, browseButtons
} from '../../cons/constant.js';

export default function buttonVisible(props) {
    let status = props.getUrlParam('status');
    let id = props.getUrlParam('id');
    // 审批状态 0=审批未通过，1=审批通过，2=审批进行中，3=提交，-1=自由，
    let billstate = props.form.getFormItemsValue(formId, 'billstate');
    props.button.setButtonVisible(allButtons, false);

    //设置按钮的可用状态
    if (status == "browse") {
        if (id) {//卡片删除数据后，有数据时

            this.setState({ showNCbackBtn: true });
            props.button.setButtonVisible(browseButtons, true);
            props.form.setFormStatus(formId, "browse");
            props.cardTable.setStatus(tableId, "browse");
            props.cardPagination.setCardPaginationVisible('cardPaginationBtn', true)
            if (billstate && billstate.value && billstate.value == 0) {
                //审批未通过
                props.button.setButtonVisible(['editBtn', 'deleteBtn', 'commitBtn'], false);
                props.button.setMainButton(['commitBtn'], false)
                props.button.setMainButton(['uncommitBtn'], false)
               // props.button.setMainButton(['file'], true)
            }
            if (billstate && billstate.value && billstate.value == 1) {
                //审批通过
                props.button.setButtonVisible(['editBtn', 'deleteBtn', 'commitBtn'], false);
                props.button.setButtonVisible(['uncommitBtn'], true);
                props.button.setMainButton(['commitBtn'], false)
                props.button.setMainButton(['uncommitBtn'], false)
                //props.button.setMainButton(['file'], true)

            }
            if (billstate && billstate.value && billstate.value == 2) {
                //审批进行中
                props.button.setButtonVisible(['editBtn', 'deleteBtn', 'commitBtn'], false);
                props.button.setButtonVisible(['uncommitBtn'], true);
                props.button.setMainButton(['commitBtn'], false)
                props.button.setMainButton(['uncommitBtn'], false)
                //props.button.setMainButton(['file'], true)

            }
            if (billstate && billstate.value && billstate.value == 3) {
                //提交
                props.button.setButtonVisible(['editBtn', 'deleteBtn', 'commitBtn'], false);
                props.button.setButtonVisible(['uncommitBtn'], true);
                props.button.setMainButton(['commitBtn'], false)
                props.button.setMainButton(['uncommitBtn'], false)
                //props.button.setMainButton(['file'], true)
            }

            if (billstate && billstate.value && billstate.value == -1) {
                //自由
                props.button.setMainButton(['commitBtn'], true)
                props.button.setMainButton(['uncommitBtn'], false)
               // props.button.setMainButton(['file'], false)
            }
        } else {
            this.setState({ showNCbackBtn: false });
            props.button.setButtonVisible(allButtons, false);
            props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false)
        }



    } else if (status == "edit") {//编辑
        this.setState({ showNCbackBtn: false });
        props.button.setButtonVisible(editButtons, true);
        props.cardTable.setStatus(tableId, "edit");
        props.form.setFormStatus(formId, "edit");
        props.button.setMainButton(['saveBtn'], true)
        props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false)

    } else if (status == "add") {//新增
        this.setState({ showNCbackBtn: false });
        props.button.setButtonVisible(editButtons, true);
        props.cardTable.setStatus(tableId, "edit");
        props.form.setFormStatus(formId, "edit");
        props.button.setMainButton(['saveBtn'], true)
        props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false)

    } else if (status == "cancel") {//取消
        this.setState({ showNCbackBtn: false });
        props.button.setButtonVisible(allButtons, false);
        props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false)
        props.form.setFormItemsDisabled(formId, { 'pk_org': true });
        props.form.setFormItemsDisabled(formId, { 'confirmstate': true });
        props.form.setFormItemsDisabled(formId, { 'confirmtime': true });
        props.form.setFormItemsDisabled(formId, { 'note': true });
        props.form.setFormItemsDisabled(formId, { 'creator': true });
        props.form.setFormItemsDisabled(formId, { 'modifier': true });
        
    }

}
