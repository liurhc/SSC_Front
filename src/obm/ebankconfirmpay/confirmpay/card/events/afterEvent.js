import { ajax } from 'nc-lightapp-front';
import { dateformat } from '../utils';
import { tableId, searchid, card_page_code,formId } from '../../cons/constant.js';
export default function afterEvent(props, moduleId, key, value, changedrows, i, s, g) {
	console.log(props, moduleId, key, value, changedrows, i, s, g)
	let now = new Date();
	let nowtime = dateformat(now, 'yyyy-MM-dd hh:mm:ss');
	let today = dateformat(now, 'yyyy-MM-dd');
	if (moduleId === formId){
		if (key === 'confirmstate') {
			let eventdata = props.createHeadAfterEventData(
				card_page_code,
				formId,
				tableId,
				moduleId,
				key,
				value
			);
			let newvalue = eventdata.newvalue.value;
			let newdisplay = eventdata.newvalue.display;
			let oldvalue = eventdata.oldvalue.value;
			if (oldvalue != newvalue && newvalue != null) {
				props.cardTable.setColsValue(tableId, [{ key:'confirmstate', data: {  display: newdisplay,value: newvalue }} ]);
				
				//确认时间
				let confirmtimevalue = props.form.getFormItemsValue(formId, 'confirmtime').value
				if(!confirmtimevalue){//确认时间为空时，默认当前日期
					props.form.setFormItemsValue(formId, { confirmtime: { display: today, value: nowtime } });
					props.cardTable.setColsValue(tableId, [{ key:'confirmtime', data: { display: today,value: nowtime }} ]);
				}
				//let rowCount = props.cardTable.getNumberOfRows(tableId);
				
			}
		}
	
		if (key === 'confirmtime') {
			let confirmtimedisplay = value.value.split(" ")[0];
			if (value != null) {
				   props.cardTable.setColsValue(tableId, [{ key:'confirmtime', data: { display: confirmtimedisplay,value: value.value }} ]);
				
			}
		}
	
	}
	
	//table中编辑后事件操作表格该行i
	if (moduleId === tableId) {
		//表体table行数
		let rowCount = props.cardTable.getNumberOfRows(tableId);
		if (rowCount == 1) {//单条表体
			//确认状态
			if (key === 'confirmstate') {
				//获取编辑的值
				
				 props.form.setFormItemsValue(formId, { confirmstate: { display: transName(props,value), value: value } });
				 let confirmtimevalue = props.cardTable.getValByKeyAndIndex(tableId, i, 'confirmtime').value
				if (!confirmtimevalue) {
					props.form.setFormItemsValue(formId, { confirmtime: { display: today, value: nowtime } });
				    props.cardTable.setValByKeyAndIndex(tableId, i, 'confirmtime', { display: today,value: nowtime});
				}
			}
			// 确认时间
			if (key === 'confirmtime') {
				if(value!=null){
					let confirmtimedisplay = value.split(" ")[0];
					props.form.setFormItemsValue(formId, { confirmtime: { display: confirmtimedisplay, value: value } });
				}else{
					props.form.setFormItemsValue(formId, { confirmtime: { display: null, value: null } });
				}
				
				 
			}
		}

		if (rowCount > 1) {//多条表体
			let diffconfirmstate=false;//表体行存在不同确认状态
			let diffconfirmtime=false;//表体行存在不同确认时间
			//确认状态
			if (key === 'confirmstate') {
					for(var j = 0; j < rowCount; j++){
						let confirmstatevalue = props.cardTable.getValByKeyAndIndex(tableId, j, 'confirmstate').value
						if(confirmstatevalue!=value&&confirmstatevalue != null){
							diffconfirmstate=true;
						}
					}
					if(diffconfirmstate){
						props.form.setFormItemsValue(formId, { confirmstate: { display: null, value: null } });
					}else{
						props.form.setFormItemsValue(formId, { confirmstate: { display: transName(props,value), value: value } });
					}
				 
			    
				let confirmtimevalue = props.cardTable.getValByKeyAndIndex(tableId, i, 'confirmtime').value
				if (!confirmtimevalue) {
					props.cardTable.setValByKeyAndIndex(tableId, i, 'confirmtime', { display: today,value: nowtime});
					for(var k = 0; k < rowCount; k++){
						let rowconfirmtimevalue = props.cardTable.getValByKeyAndIndex(tableId, k, 'confirmtime').value
						if(rowconfirmtimevalue.split(" ")[0]!=today.split(" ")[0]&&rowconfirmtimevalue != null){
							diffconfirmtime=true;
						}
					}
					if(diffconfirmtime){
						props.form.setFormItemsValue(formId, { confirmtime: { display: null, value: null } });
					}else{
						props.form.setFormItemsValue(formId, { confirmtime:{ display: today,value: nowtime} });
					}
				}
			}
			// 确认时间
			if (key === 'confirmtime') {
				 if (value!=null) {
					let confirmtime_arr = props.cardTable.getColValue(tableId, 'confirmtime');
					for (let index = 0; index < confirmtime_arr.length; index++) {
						//let confirmtimevalue = confirmtime_arr[index].value;
						if(confirmtime_arr[index].value.split(" ")[0]!=value.split(" ")[0]&&confirmtime_arr[index].value != null){
									diffconfirmtime=true;
								}
					}
					//for(var j = 0; j < rowCount; j++){
					//	let confirmtimevalue = props.cardTable.getValByKeyAndIndex(tableId, j, 'confirmtime').value
					//	if(confirmtimevalue.split(" ")[0]!=value.split(" ")[0]&&confirmtimevalue != null){
					//		diffconfirmtime=true;
					//	}
					//}
					if(diffconfirmtime){
						props.form.setFormItemsValue(formId, { confirmtime: { display: null, value: null } });
					}else{
						props.form.setFormItemsValue(formId, { confirmtime: { display: value.split(" ")[0], value: value } });
					}
				  }
			}
		}
	}
}

/**
 * 装换状态名称
 * value
 */
export const transName = function (props,value) {
	if(value==1)
	   return props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000042');/* 国际化处理： 失败*/
	if(value==0)
	   return props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000041');/* 国际化处理： 成功*/
}