import { base, ajax } from 'nc-lightapp-front';

let { NCPopconfirm } = base;
import tableButtonClick from './tableButtonClick';
import buttonVisible from './buttonVisible';
import { tableId, searchid, appcode ,card_page_code, formId, dataSource } from '../../cons/constant.js';

export default function(props) {
	let that = this;
	props.createUIDom(
		{
			pagecode: card_page_code,//页面id
			appcode: appcode//注册按钮的code
		},
		function (data) {
		// let data = cardTemplate.data;
			if (data) {
				if (data.template) {
					let meta = data.template;
					modifierMeta(that, props, meta)
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					// 按钮编辑性
					buttonVisible.call(that, props);
				}
				
			}
		}
	)
}

function modifierMeta(that,props, meta) {
    let items = meta[formId].items;
	let confirmstates = [];
	confirmstates[0] = {
		"display": props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000041'),/* 国际化处理： 成功*/
		"value": 0
	};
	confirmstates[1] = {
		"display": props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000042'),/* 国际化处理： 失败*/
		"value": 1
	};
	items.find((item) => item.attrcode == 'confirmstate').options = confirmstates;
	//billstate   自由=-1, 审批未通过=0, 审批通过=1, 审批进行中=2, 提交=3
	let billstates = [];
	billstates[0] = {
		"display": props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000043'),/* 国际化处理： 自由*/
		"value": -1
	};
	billstates[1] = {
		"display": props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000044'),/* 国际化处理： 审批未通过*/
		"value": 0
	};
	billstates[2] = {
		"display": props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000045'),/* 国际化处理： 审批通过*/
		"value": 1
	};
	billstates[3] = {
		"display": props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000046'),/* 国际化处理： 审批进行中*/
		"value": 2
	};
	billstates[4] = {
		"display": props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000047'),/* 国际化处理： 提交*/
		"value": 3
	};
	items.find((item) => item.attrcode == 'billstate').options = billstates;
	//sourcesys  结算系统=FTS,网银系统=OBM,调度系统=SF,现金管理=CMP,应收系统=AR,应付系统=AP,费用管理=ERM
	let sourcesyses = [];
	sourcesyses[0] = {
		"display": props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000048'),/* 国际化处理： 结算系统*/
		"value": 'FTS'
	};
	sourcesyses[1] = {
		"display": props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000049'),/* 国际化处理： 网银系统*/
		"value": 'OBM'
	};
	sourcesyses[2] = {
		"display": props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000050'),/* 国际化处理： 调度系统*/
		"value": 'SF'
	};
	sourcesyses[3] = {
		"display": props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000051'),/* 国际化处理： 现金管理*/
		"value": 'CMP'
	};
	sourcesyses[4] = {
		"display": props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000052'),/* 国际化处理： 应收系统*/
		"value": 'AR'
	};
	sourcesyses[5] = {
		"display": props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000053'),/* 国际化处理： 应付系统*/
		"value": 'AP'
	};
	sourcesyses[6] = {
		"display": props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000054'),/* 国际化处理： 费用管理*/
		"value": 'ERM'
	};
	items.find((item) => item.attrcode == 'sourcesys').options = sourcesyses;	

	meta[formId].items.map((item) => {
		if (item.attrcode == 'pk_org_v') {
			item.queryCondition = () => {
				let data = props.form.getFormItemsValue(formId, 'vbillcode').value;
				return { vbillcode: data };
			};
		}
	});

	let porCol = {
		attrcode: 'opr',
		label: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000012'),/* 国际化处理： 操作*/
		fixed: 'right',
		itemtype: 'customer',
		visible: true,
		width: '200px',
		render(text, record, index) {
			let status = props.getUrlParam("status");
			let buttonAry =['open_inner'];
			
			return props.button.createOprationButton(buttonAry, {
				area: 'card_body_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => tableButtonClick.call(that, props, key, text, record, index)
			});
		}
	};
	meta[tableId].items.push(porCol);

	return meta;
}
