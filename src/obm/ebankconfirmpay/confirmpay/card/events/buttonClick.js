import { ajax, base, toast, print,cardCache, promptBox} from 'nc-lightapp-front';
import { tableId, searchid, card_page_code, formId,appcode,dataSource } from '../../cons/constant.js';
let { getNextId, getCurrentLastId, deleteCacheById, deleteCacheId, getCacheById, updateCache, addCache, setDefData, getDefData } = cardCache;
export default function (props, id) {
  let that = this;
  let dataArr = [];
  dataArr.push(props.form.getFormItemsValue(formId, 'pk_confirmpay_h').value);//主键数组
  //自定义请求数据
  let data = {
    'pks': dataArr
  };
  console.log(data);
  switch (id) {
    //保存
    case 'saveBtn':
      this.saveClick();
      break;
    //修改
    case 'editBtn':
      edit.call(this, props);
      break;
    //刷新按钮
    case 'refreshBtn':
      refreshCard.call(this,that,props);
      break;
    //删除按钮
    case 'deleteBtn':
      //this.props.modal.show('delete');
      promptBox({
        /* 国际化处理：删除*/
        title: this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000023'),
        color: "warning",
        content: this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000024'),/* 国际化处理： 你确定要删除吗?*/
        beSureBtnClick: this.delConfirm.bind(this, props),
      });
      break;
    //返回按钮
    case 'backBtn':
      props.linkTo('/obm/ebankconfirmpay/confirmpay/list/index.html', {
        status: 'browse',
        type: 'link',
        appcode: appcode,
        pagecode: card_page_code
      })
      break;
    // 提交
    case 'commitBtn':
      promptBox({
                color: "warning",
                title: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000039'),/* 国际化处理： 注意*/
                content: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000040'),/* 国际化处理： 支付状态是否已和银行柜台确认？如果没有，可能会造成重复支付。\n为了保证信息准确，建议发送指令后，间隔15分钟以上再进行确认。*/
                beSureBtnClick: commit.bind(this,that,data, props)
              });
      break;
    // 提交退回
    case 'uncommitBtn':
      ajax({
        url: '/nccloud/obm/ebankconfirmpay/uncommit.do',
        data: data,
        success: function (res) {
          if (res.success) {
            if(res.data && res.data.errorMsg){
              toast({ color: 'warning', content: res.data.errorMsg });
            }else{
              refreshCard.call(this,that,props);
              toast({ duration: 3,color: 'success', content: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000001') });/* 国际化处理： 收回成功*/
              
            }
            
          }
          
        }
      });
      break;
    // 审批
    case 'approveBtn':
      ajax({
        url: '/nccloud/obm/ebankconfirmpay/approve.do',
        data: data,
        success: function (res) {
          let { success, data } = res;
          if (success) {
            //refreshCard(props);
            refreshCard.call(this,that,props);
            toast({ color: 'success', content: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000002') });/* 国际化处理： 审批成功*/

          }
        }
      });
      break;
    // 取消审批
    case 'unapproveBtn':
      ajax({
        url: '/nccloud/obm/ebankconfirmpay/unapprove.do',
        data: data,
        success: function (res) {
          let { success, data } = res;
          if (success) {
            //refreshCard(props);
            refreshCard.call(this,that,props);
            toast({ color: 'success', content: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000003') });/* 国际化处理： 审批取消成功*/

          }
        }
      });

      break;
    case 'viewapproveInfo':
      let linkaprvData = props.table.getCheckedRows(tableId);
      let pk_confirmpay_h = dataArr[0]; //单据pk
      let pk_linktradetype = '36CC'; //单据pk

      this.setState(
        {
          billid: pk_confirmpay_h, //单据pk
          billtype: pk_linktradetype
        },
        () => {
          this.setState({
            showAppr: true
          });
        }
      );
      break;
    case 'cancelBtn':
    promptBox({
      color: "warning",
      title: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000004'),/* 国际化处理： 取消*/
      content: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000005'),/* 国际化处理： 确认要取消吗？*/
      beSureBtnClick: cancelConfirm.bind(this, props)
    })
         
      break;
    case 'printBtn':
      print(
        'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
        '/nccloud/obm/ebankconfirmpay/print.do',
        {
          nodekey: 'NCCLOUD', //模板节点标识
          oids: dataArr // 功能节点的数据主键
        }
      );
      break;
    case 'preview':
      print(
        'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
        '/nccloud/obm/ebankconfirmpay/print.do',
        {
          funcode: appcode, //功能节点编码，即模板编码
          nodekey: 'NCCLOUD', //模板节点标识
          printTemplateID: '1001Z610000000003Y4Q',//卡片打印模板pk 
          oids: dataArr // 功能节点的数据主键
        }
      );
      break;
    case 'export':
     
      this.setState(
        {
            outputData: {
                // 模板节点标识
                nodekey: 'NCCLOUD',
                // oids含有多个元素(['xxx','xxx'])时为批量打印,   
                oids: dataArr,
                outputType: 'output'
            }
        },
        () => {
            this.refs.printOutput.open();
        }
    );
      break;
     // 查看审批意见
    case 'reviewapprove':

        this.setState(
            {
                billId: props.form.getFormItemsValue(formId, 'pk_confirmpay_h').value, //单据pk
                billtype: '36CC'
            },
            () => {
                this.setState({
                     showAppr: true
                });
            }
         );
     
      break;  
    case 'billLinkQueryBtn':
    
      let sourcebillpk = this.props.form.getFormItemsValue(formId, 'sourcebillpk').value;
      let sourcebilltype = this.props.form.getFormItemsValue(formId, 'sourcebilltype');
      if (sourcebilltype && sourcebilltype.value=== '36C9') {//工资清单
        props.openTo('/obm/ebankdfgz/dfgz/main/index.html#/card', {
            status: 'browse',
            id: sourcebillpk,
            srcbilltype:'36CC',
            copyFlag: false,
            type: 'link',
            appcode: '3610PAYR',
            pagecode: ''
        });
      }

      if (sourcebilltype && sourcebilltype.value === '36K4') {//上收
        props.openTo('/sf/delivery/delivery/main/index.html#/listlinkq', {
            status: 'browse',
            id: sourcebillpk,
            srcbilltype:'36CC',
            appcode: '36320FDA',
            pagecode: '36320FDA_list'
        });
      }
      if (sourcebilltype && sourcebilltype.value === '36K2') {//下拨
        props.openTo('/sf/allocation/allocate/main/index.html#/linklist', {
            status: 'browse',
            srcbillid: sourcebillpk,
            linkquerytype:'3',
            srcbilltype:'36CC',
            appcode: '36320FA',
            pagecode: '36320FA_L01'
        });
      }  
      if (sourcebilltype && sourcebilltype.value === '36J5') {//委托付款书
        props.openTo('/fts/commission/commissionpayment/main/index.html#/list', {
            status: 'browse',
            id: sourcebillpk,
            srcbilltype:'36CC',
            appcode: '36300TP',
            pagecode: '36300TP_L01'
        });
      }  
      if (sourcebilltype && sourcebilltype.value === '36K6') {//资金调拨单
         // props.openTo('/sf/allocation/allocate/main/index.html#/card', {
         //     status: 'browse',
          //    id: sourcebillpk,
          //    srcbilltype:'36CC',
          //    appcode: '36320FA',
          //    pagecode: '36320FA_PAY_C01'
          //});
        }  
        if (sourcebilltype && sourcebilltype.value === 'D5') {//付款结算单
          props.openTo('/cmp/billmanagement/paybill/linkcard/index.html', {
              status: 'browse',
              id: sourcebillpk,
              srcbilltype:'36CC',
              appcode: '36070PBR',
              pagecode: '36070PBR_C02'
          });      
        }  
        

    
      break;
    // 附件管理
    case 'file':
      //单据pk
      let filepk_confirmpay_h = this.props.form.getFormItemsValue(formId, 'pk_confirmpay_h');
      //单据编号
      let filevbillno = this.props.form.getFormItemsValue(formId, 'vbillno');
      
      this.setState({
        //单据pk
        billId: filepk_confirmpay_h && filepk_confirmpay_h.value,
        //附件管理使用单据编号
        billno: filevbillno && filevbillno.value,
        showUploader: !this.state.showUploader,
      })
      break;  
    default:
      break;
  }
}

// 编辑保存是取消按钮确认框
const cancelConfirm = function(props) {
  cancel.call(this, props);
}




/**
 * 修改
 */
function refreshCard(that,props) {
  let pk = props.form.getFormItemsValue(formId, 'pk_confirmpay_h').value;
  ajax({
    url: '/nccloud/obm/ebankconfirmpay/querycard.do',
    data: { pk },
    success: res => {
      if (res.data && res.data.head) {

        props.form.setAllFormValue({ [formId]: res.data.head[formId] });

        let sourcebilltype = res.data.head[formId].rows[0].values.sourcebilltype;
        let sourcebilltypename=transtoname(that,sourcebilltype);
        props.form.setFormItemsValue(formId, { sourcebilltype: { display: sourcebilltypename, value: sourcebilltype.value } });

        
        updateCache('pk_confirmpay_h', res.data.head[formId].rows[0].values.pk_confirmpay_h.value, res.data, formId, dataSource);
      }
      if (res.data && res.data.body) {
        props.cardTable.setTableData(tableId, res.data.body[tableId]);
      }
      that.props.pushTo("/card",{
        id: pk,
        status: 'browse',
      });
      that.setState({showNCbackBtn: true});
      that.toggleShow();
    }

  });

  
}


// 来源单据类型编码转为名称
function transtoname(that,sourcebilltype) {

  if (sourcebilltype.value === "D5") {
    return that.props.MutiInit.getIntl("36100CONFM") && that.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000006');/* 国际化处理： 付款结算单*/
  }
  if (sourcebilltype.value === "36K2") {
    return that.props.MutiInit.getIntl("36100CONFM") && that.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000007');/* 国际化处理： 下拨单*/
  }
  if (sourcebilltype.value === "36K6") {
    return that.props.MutiInit.getIntl("36100CONFM") && that.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000008');/* 国际化处理： 资金调拨单*/
  }
  if (sourcebilltype.value === "36J5") {
    return that.props.MutiInit.getIntl("36100CONFM") && that.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000009');/* 国际化处理： 委托付款书*/
  }
  if (sourcebilltype.value === "36K4") {
    return that.props.MutiInit.getIntl("36100CONFM") && that.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000010');/* 国际化处理： 上收单*/
  }
  if (sourcebilltype.value === "36C9") {
    return that.props.MutiInit.getIntl("36100CONFM") && that.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000011');/* 国际化处理： 工资清单*/
  }
  
  return sourcebilltype.value;
}

/**
 * 修改
 */
function edit(props) {
  props.cardTable.closeExpandedRow(tableId);
    this.props.pushTo("/card",{
      id: props.form.getFormItemsValue(formId, 'pk_confirmpay_h').value,
      status: 'edit',
    });
  this.toggleShow();
}

/**
 * 提交
 */
function commit(that,data,props) {
  ajax({
    url: '/nccloud/obm/ebankconfirmpay/commit.do',
    data: data,
    success: function (res) {

      if (res.success) {
        if(res.data && res.data.errorMsg){
          toast({ color: 'warning', content: res.data.errorMsg });
        }else{
          refreshCard.call(this,that,props);
          toast({  duration: 3,color: 'success', content: props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000000') });/* 国际化处理： 提交成功*/
          
        }
        
      }
    }
  });
}

/**
 * 取消
 */
function cancel(props) {
  //let status = props.form.getFormStatus(formId);
  let status = props.getUrlParam("status");
  if (status === 'edit') {
    props.form.cancel(formId);
    props.cardTable.resetTableData(tableId);
    this.props.pushTo("/card",{
      id: props.form.getFormItemsValue(formId, 'pk_confirmpay_h').value,
      status: 'browse',
    });
    this.setState({showNCbackBtn: true});
    this.toggleShow();
  } else if(status === 'add'){
    props.form.EmptyAllFormValue(formId);
    props.cardTable.setTableData(tableId, { rows: [] });
    this.props.pushTo("/card",{
      id: props.form.getFormItemsValue(formId, 'pk_confirmpay_h').value,
      status: 'cancel',
    });
    this.toggleShow();
  }
 
  props.resMetaAfterPkorgEdit();
}
