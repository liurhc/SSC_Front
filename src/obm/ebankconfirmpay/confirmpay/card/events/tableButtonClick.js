import { createPage, ajax, base, toast } from 'nc-lightapp-front';
import { tableId, searchid, card_page_code, oid, formId } from '../../cons/constant.js';



export default function tableButtonClick(props, key, text, record, index) {
    // console.log(key);
    let pk_org = props.form.getFormItemsValue(formId, 'pk_org');
    let org_val,org_display;
    if(pk_org){
      org_val = pk_org.value;
      org_display = pk_org.display;
    }
    switch (key) {
        //展开
        case 'open_inner':
            if (org_val && org_display) {
                //props.cardTable.openModel(tableId, 'edit', record, index);
                props.cardTable.toggleRowView(tableId, record);
            } else {
                toast({
                    'color': 'warning',
                    'content': props.MutiInit.getIntl("36100CONFM") && props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000013')/* 国际化处理： 请先填写财务组织*/
                });
                return;
            }
            break;
        
        default:
            break;
    }
}
