/**
 * 表体区域
 */
export const tableId = 'body';

/**
 * 表头区域
 */
export const formId = 'head';



/**
 * 页面编码
 */
export const pagecode = '36101022_C01';
export const appcode = '36101022';
export const appid = '1001AA1000000002I5DG';

/**
 * 编辑态按钮
 */
export const editButtons = ['Save','Cancel','AddLine','DelLine','copyRow'];

/**
 * 浏览态按钮
 * 
 */
export const browseButtons = ['Add','Delete','Refresh','Edit'];

export const dataSource = 'tm.obm.ebankdatadownloadgroup.datadownload';
export const pkname='pk_ebankdatadownload';