import { createPage, ajax, base, toast } from 'nc-lightapp-front';
import { appid, formId, tableId, pagecode } from '../constants.js';

export default function tableButtonClick(props, key, text, record, index) {
    console.log(key);

    switch (key) {
        //展开
        case 'Openedit':
            props.cardTable.toggleRowView(tableId, record)
            break;   
        case 'Addline':
              props.cardTable.pasteRow(tableId, index);
            break;
        case 'Delline':
            props.cardTable.delRowsByIndex(tableId, index);
            break;
        //编辑展开
        case 'editmoreBtn':
            props.cardTable.openModel(tableId, 'edit', record, index);
            break;
        //行 粘贴至末行
		case 'copyLastRow':
        index = props.cardTable.getNumberOfRows(this.tableId);
        copyResolve.call(this, props, this.tableId, selectArr, index);
        break;
        //行 复制
		case 'copyRow':
        copyRow.call(this, props);
        break;

    }

    
/**
 * 复制
 * @param {*} props  页面内置对象
 */
function copyRow(props) {
	this.setState({ isPaste: true }, () => {
		buttonVisible.call(this, props);
	});
}

    /**
 * 对复制行的数据进行粘贴处理
 * @param {*} props          页面内置对象
 * @param {*} tableId        当前选中table的code
 * @param {*} selectArr      选中的数据
 * @param {*} index          行下标  
 */
function copyResolve(props, tableId, selectArr, index) {
	props.cardTable.insertRowsAfterIndex(tableId, selectArr, index);
	props.cardTable.setValByKeyAndIndex(tableId, index, this.tablePrimaryId, { value: null });
	this.setState({ isPaste: false }, () => {
		buttonVisible.call(this, props);
		props.cardTable.setStatus(tableId, 'edit');
	});
}
}