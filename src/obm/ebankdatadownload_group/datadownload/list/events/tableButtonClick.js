﻿import { ajax, base, print,toast } from 'nc-lightapp-front';
const tableid = 'list_head';
const sreachId = 'search';

export default function tableButtonClick(props, key, text, record, index) {
	// console.log(id)
	switch (key) {
		//状态下载
		case 'EbankLogDownloadStateAction':
			let pks = [record.pk_ebank_paylog_h.value];
		
			ajax({
				url: '/nccloud/obm/ebankpaylog/ebankLogDownload.do',
				data: { pks },
				success: (res) => {
					if (res.data && res.data.msg) {
						toast({ content: res.data.msg, color: 'success' });
					}
				}
			});
			break;
		//状态同步
		case 'EbankLogFireEventSendStateAction':
		pks = [record.pk_ebank_paylog_h.value];
			
		ajax({
		  url: '/nccloud/obm/ebankpaylog/ebankLogFireEventSendState.do',
		  data: {pks},
		  success: (res) => {
			if (res.data && res.data.msg) {
			  toast({ content: res.data.msg , color: 'success' });
			}
		  }
		});
		  break;
		default:
			break;
	}
}


