import { ajax, toast ,cacheTools} from 'nc-lightapp-front';
import { appid, formId, tableId, pagecode,appcode} from '../constants.js';

export default function doubleClick(record, index, e) {
    let searchVal = this.props.search.getAllSearchData('search');  
    // cacheTools.set('list_head', searchVal); 
    cacheTools.set('search', searchVal); 

    this.props.pushTo('/card', {
        status: 'browse',
        appcode:'36101022',
        pagecode:'36101022_C01',
        id: record.pk_ebankdatadownload.value
    });
           // type:'link',/obm/ebankdatadownload_group/datadownload/card/index.html
}
