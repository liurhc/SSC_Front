const tableid = 'list_head';

export default function clickBtn(props) {
	let selectdata = props.table.getCheckedRows(tableid); 

	if (!selectdata || selectdata.length == 0) {
		//没有选中
		props.button.setDisabled({
			Edit:true,
			Delete:true,
		});
	}else if(selectdata.length >1){
		props.button.setDisabled({
		Edit:true,
		Delete:false,});
	} else {//选中
		props.button.setDisabled({
			Edit:false,
			Delete:false,
		});
	}
}
