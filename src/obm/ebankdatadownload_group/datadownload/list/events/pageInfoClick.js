import { ajax } from 'nc-lightapp-front';
const tableid = 'list_head';

export default function (props, config, pks) {
	let pageInfo = props.table.getTablePageInfo('list_head');
    let searchVal = props.search.getAllSearchData('search');  
	let data = {
		pks: pks,
		pageid: "36101022_L01"
	};
	//得到数据渲染到页面
	let that = this;
	ajax({
		url: '/nccloud/obm/ebankdatadownload_group/pagequery.do',
		data: data,
		async: true,
        success: function (res) {
            props.table.setAllTableData('list_head', res.data['list_head'])
        }
		// success: function (res) {
		// 	let { success, data } = res;
		// 	if (success) {
		// 		if (data) {
		// 			props.table.setAllTableData(tableid, data[tableid]);
		// 		} else {
		// 			props.table.setAllTableData(tableid, { rows: [] });
		// 		}
		// 	}
		// }
	});
}
