import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, toast, base,createPageIcon,cardCache } from 'nc-lightapp-front';
// const { NCBreadcrumb } = base;
// const NCBreadcrumbItem = NCBreadcrumb.NCBreadcrumbItem;
import { buttonClick, initTemplate, afterEvent, searchBtnClick, pageInfoClick, doubleClick,tableModelConfirm,setButtonUsability } from './events';
import './index.less';
import { cacheTools } from 'nc-lightapp-front';
import {dataSource} from "./constants";
// import BankaccDefaultGridTreeRef from '../../../uapbd/refer/bankacc/BankaccDefaultGridTreeRef/index.js';
// import BankaccDefaultGridTreeRef from '../../../../uapbd/refer/bankacc/BankaccDefaultGridTreeRef/index';
let tableid = 'list_head';
let sreachId = 'search';
let querycondition ="";
let {deleteCacheById }=cardCache;

class List extends Component {
	constructor(props) {
		super(props);
		this.props = props;
		this.state = {
			data: {}
			
		}
	}

	componentDidMount() {
		this.toggleShow();
	}

	componentWillMount() {
		let s = this.props.getUrlParam('status');
		let callback = (json, status, inlt) => {
			// json 多语json格式参数； status： 是否请求到json多语，可用来判断多语资源是否请求到并进行一些别的操作； inlt： 可用来进行占位符的一些操作
			if (status =='Add'  || status == 'Edit') {
				initTemplate.call(this, this.props, json, inlt); // 如模板内也有多语处理，平台推荐模板加载操作放于此处， 在取到json多语资源后用传参的方式传入intemplate模板函数中
				this.setState({ json, inlt }); // 保存json和inlt到页面state中并刷新页面
				this.afterGetLang(json);
			} else {
				console.log( this.props.MutiInit.getIntl("36101022") && this.props.MutiInit.getIntl("36101022").get('36101022-000029'),/* 国际化处理： 未加载到多语资源*/ ); // 未请求到多语资源的后续操作
			}
		};
		this.props.MultiInit.getMultiLang({ moduleId: '36101022', domainName: 'obm', callback });
	}

	//切换页面状态
	toggleShow = () => {
		this.props.button.setDisabled({
			Edit:true,
			Delete:true,
		});
		// let { hasCacheData } = this.props.table;
		// if(!hasCacheData(dataSource)){
		// 	this.props.table.setAllTableData('list_head', {rows:[]});
		// }

	}

	deleteAction= () =>{
		 querycondition = cacheTools.get('36101022_L01_search');

		let idstr="";
		let index = 0;
		let pk = null;
		let tss = null;
		let lts = "";
		let selectDatas = null;
		selectDatas =this.props.table.getCheckedRows('list_head');
		//数据校验
		if (selectDatas ==null || selectDatas.length == 0) {
			toast({ color: 'warning', content: this.props.MutiInit.getIntl("36101022") && this.props.MutiInit.getIntl("36101022").get('36101022-000010') });
			return
		} 
		idstr="";
		index = 0;
        lts = "";
		pk = null;
		while (index < selectDatas.length) {
			//获取行主键值
			pk = selectDatas[index].data.values.pk_ebankdatadownload.value;
			tss =  selectDatas[index].data.values.ts.value;
			//判空
			idstr = idstr + pk;
			lts = lts + tss;
            
			if (index<selectDatas.length-1){
				idstr = idstr + ",";
			}
			if (index<selectDatas.length-1){
				lts = lts + ",";
			}
			index++;
		} 
		ajax({
			url: '/nccloud/obm/ebankdatadownload_group/listdelete.do',
			data: { 
				id:idstr,
				ts:lts
			},
			success: (res) => {
				if (res) {
					deleteCacheById('list_head',idstr);
					this.refreshAction()
				}
			}
			});	
	}

	/**
 * 刷新
 * @param {*} props 
 */
refreshAction = () => {
	let searchVal = this.props.search.getAllSearchData(sreachId);
	let pageInfo = this.props.table.getTablePageInfo(tableid);
	let metaData = this.props.meta.getMeta();
	// let querycondition = cacheTools.get('36101022_L01_search');	
	console.log(searchVal);
	let data = {
		conditions: searchVal.conditions,
		// condition:querycondition,
		pageInfo: pageInfo,
		pagecode: "36101022_L01",
		queryAreaCode: "36101022",
		oid: '1001Z61000000002I5IL',
		queryType: "simple"
	};
	console.log(data);
	ajax({
		url: '/nccloud/obm/ebankdatadownload_group/query.do',
		data: data,
		success:  (res)=> {
            let { success, data } = res;
            if(success){
                if(data && data[tableid]){
                    this.props.table.setAllTableData(tableid, res.data[tableid]);
                }else{
					this.props.table.setAllTableData(tableid, {rows:[]});
                    toast({content:this.props.MutiInit.getIntl("36101022") && this.props.MutiInit.getIntl("36101022").get('36101022-000011'),color:"warning"});/* 国际化处理： 无数据*/
				}
				// setDefData(tableid,dataSource,data);	
            }
        },
		error: (res) => {
			console.log(res.message);
		}
	});
}

	render() {
		let { modal } = this.props;
		let { createModal } = modal;
		let buttons = this.props.button.getButtons();
		let { table, button, search } = this.props;
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { createButtonApp } = this.props.button;
		// let { createButton, getButtons} = button;
		
		return (
			<div className="nc-bill-list">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
					{/*页面大图标*/}
							{createPageIcon()}
						<h2 className='title-search-detail'>{this.props.MutiInit.getIntl("36101022") && this.props.MutiInit.getIntl("36101022").get('36101022-000008')/* 国际化处理： 电票数据下载设置-集团*/ }</h2></div>
					<div className="header-button-area">
						{
							this.props.button.createButtonApp({
								area:'list_head',
								buttonLimit:4,
								onButtonClick: buttonClick.bind(this),
								popContainer:document.querySelector('.header-button-area')
							})
						}

					</div>
				</div>
				<div className="nc-bill-search-area">
					{NCCreateSearch(sreachId, {
						showAdvBtn: true,
						clickSearchBtn: searchBtnClick.bind(this),
						defaultConditionsNum: 10
					})}
				</div>
				<div className="nc-bill-table-area" style={{ borderRadius: "3px 3px 0 0", overflow: "hidden" }}>
					{createSimpleTable("list_head", {//列表区
						dataSource: dataSource,
						handlePageInfoChange: pageInfoClick,
						adaptionHeight: true,
						showCheck: true,//复选框
						showIndex: true,//显示序号
						onSelected: setButtonUsability.bind(this, this.props),
						onSelectedAll: setButtonUsability.bind(this, this.props),

						onRowDoubleClick: doubleClick.bind(this)
					})}
				</div> 
			</div>
		);
	}
}


List = createPage({
	mutiLangCode: '36101022',
	initTemplate: initTemplate
})(List);

// export default List;

// ReactDOM.render(<List />, document.querySelector('#app'));
export default List;
