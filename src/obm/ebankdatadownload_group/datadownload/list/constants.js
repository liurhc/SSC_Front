/**
 * 表体区域
 */
export const tableId = 'list_head'; 

/**
 * 表头区域
 */
export const formId = 'head';

/**
 * 查询区域编码
 */
// export const searchId = 'search';
 
/**
 * 页面编码
 */
export const pagecode = '36101022_L01';
export const appcode = '36101022';
// export const appid = '0001Z61000000003AEAC';这个貌似错了，修改一下试试
export const appid = '1001AA1000000002I5DG';

export const dataSource = 'tm.obm.ebankdatadownloadgroup.datadownload';
/**
 * 编辑态按钮
 */
export const editButtons = ['Save', 'Cancel'];

/**
 * 浏览态按钮
 */
export const browseButtons = ['Add','Delete','Refresh','Edit'];