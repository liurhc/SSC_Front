/**
 * 表体区域
 */
export const tableId = 'body';

/**
 * 表头区域
 */
export const formId = 'head';



/**
 * 页面编码
 */
export const pagecode = '36101021_C01';
export const appcode = '36101021';
// export const appid = '0001Z61000000003AEAC';
export const appid = '1001Z61000000001KIQ8';

/**
 * 编辑态按钮
 */
export const editButtons = ['Save','Cancel','AddLine','DelLine','copyRow','copyLastRow'];

/**
 * 浏览态按钮
 * 
 */
export const browseButtons = ['Add','Delete','Refresh','Edit'];

export const dataSource = 'tm.obm.ebankdatadownload.datadownload';
export const pkname='pk_ebankdatadownload';