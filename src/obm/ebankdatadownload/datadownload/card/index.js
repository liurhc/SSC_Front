//主子表卡片
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast,high,cardCache,formDownload,cacheTools,createPageIcon } from 'nc-lightapp-front';
let { NCFormControl, NCPopconfirm,NCAnchor, NCScrollLink, NCScrollElement, NCAffix,NCBackBtn } = base;
import { tableId, formId, pagecode,appcode, editButtons, browseButtons,dataSource,pkname } from "./constants";
import { buttonClick, initTemplate, afterEvent,pageInfoClick } from './events';
import './index.less';
import { Record } from 'immutable';
let {getCacheById,updateCache,addCache, getNextId, deleteCacheById }=cardCache;
const{NCUploader}=high;
class Card extends Component {
	constructor(props) {
		super(props);  
		this.state ={
			billno:'',//单据编号,
			//返回箭头
			showBackBtn: true,
			data: []
		};
	}
	componentDidMount() {
		this.refresh();
	}

	componentWillMount() {
		// 关闭浏览器
		window.onbeforeunload = () => {
			let status = this.props.form.getFormStatus(formId);
			if (status != 'browse') {
				return this.props.MutiInit.getIntl("36101021") && this.props.MutiInit.getIntl("36101021").get('36101021-000030');/* 国际化处理： 当前单据未保存，您确认离开此页面？*/
				/* 国际化处理： 当前单据未保存，您确认离开此页面？*/
				// return this.props.MutiInit.getIntl("36100CONFM") && this.props.MutiInit.getIntl("36100CONFM").get('36100CONFM-000014');/* 国际化处理： 当前单据未保存，您确认离开此页面？*/
			}
		};
	}

	loadNetStyle=(key,CardData)=> { //加载对应类型的银行类别到表体下拉2018-8-21
		if (key === 'netbankinftpstyle') {	
			let that = this;
			ajax({
					data: CardData,
					url: '/nccloud/obm/serverconfig/selectbanktype.do',
					success: (res) => {
						if (res.data && res.data.alltypecode) {
							let that = this;
							let meta = that.props.meta.getMeta();
							let items = meta["table_srvconf_01"].items;
							let options = [];
							let index = res.data.alltypecode.indexOf('*');
							let srcinfo = res.data.alltypecode;
							if (index > 0) {
								let srcinfos = srcinfo.split('*');
								for (let i = 0; i < srcinfos.length; i++) {
									options[i] = {
										"display": srcinfos[i].split('+')[0],
										"value": srcinfos[i].split('+')[1]
									};
								}
							} else {
								let srcinfos = new Array(2);
								srcinfos = srcinfo.split('+');
								options[0] = {
									"display": srcinfos[0],
									"value": srcinfos[1]
								};
							}
							items.find((item) => item.attrcode == 'netbankinftpcode').options = options;
							that.props.meta.setMeta(meta);
						}
						
						// if(res.data && res.data.retcard){
						// 	if (res.data.retcard.head) {
						// 		that.props.form.setAllFormValue({ [formId]: res.data.retcard.head[formId] });
						// 		let billno =res.data.retcard.head[formId].rows[0].values.billno.value;
						// 		that.setState({billno:billno});
						// 		that.props.cardTable.setTableData(tableId, { rows: [] });
						// 	}
						// }
					}
			});
			
		}
		
	}
	

	// assLineDown=()=>{
	// 	let data='zip';
	// 	formDownload({
	// 		params:{data},
	// 		url:'/nccloud/obm/serverconfig/asslinenumwebdownload.do',
	// 		enctype:1
	// 	})
	// }

	 //通过单据id刷新单据信息
	 reGetdata = () => {
		// let netbankinftpcode;	
		let CardData = this.props.createMasterChildData(pagecode, formId, tableId);
		let id=CardData.head.head.rows[0].values.pk_ebankdatadownload.value;
		let data; 
		data = { pk: id }; 
		let that = this;
		ajax({
			url: '/nccloud/obm/ebankdatadownload/cardquery.do',
			data: data,
			success: (res) => {
				if (res.data) {
					let pk_ebankdatadownload = null;
					if (res.data.head) {											
						this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
						pk_ebankdatadownload = res.data.head[formId].rows[0].values.pk_ebankdatadownload.value;
						let billno = res.data.head[formId].rows[0].values.facode.value;
						this.setState({billno:billno});
					}
					if (res.data.body) {
						this.props.cardTable.setTableData(tableId, res.data.body[tableId]);
					}
					updateCache('pk_ebankdatadownload',pk_ebankdatadownload,res.data,formId,dataSource);
					this.toggleShow();
				} 
			}
		});
	};

	//加载数据
	refresh = () => {
		let netbankinftpcode;	
		let browseflag =false;
		let addflag = false;
		let editflag = false;
		if(this.props.getUrlParam('status') =='browse'){
				browseflag=true;		
				this.setState({showBackBtn: true});	
		}else if(this.props.getUrlParam('status') == 'edit'){
				editflag=true;
				this.setState({showBackBtn: false});	
		}else if(this.props.getUrlParam('status') == 'add'){
				addflag=true;
				this.setState({showBackBtn: false});	
		}

		if (browseflag || editflag || addflag) {			
				let data; 

				data = { pk: this.props.getUrlParam('id'), pageCode: pagecode }; 
				let that = this;
				ajax({
						url: '/nccloud/obm/ebankdatadownload/cardquery.do',
						data: data,
						success: (res) => {
							if (res.data) {
								//存入缓存
								let pk_ebankdatadownload = res.data.head[formId].rows[0].values.pk_ebankdatadownload.value;
								let cardData = getCacheById(pk_ebankdatadownload, dataSource);
								// if(cardData){
								// 	//更新缓存
								// 	updateCache(pkname,pk_ebankdatadownload,res.data,formId,dataSource);
								// }else{
								// 	//添加缓存
								// 	addCache(pk_ebankdatadownload,res.data,formId,dataSource);
								// }

								if (res.data.head) {											
									this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
								
								}
								if (res.data.body) {
									this.props.cardTable.setTableData(tableId, res.data.body[tableId]);
								
								}
								if(addflag){	
									this.setState({
										billno:''
									});	 
									this.props.form.setFormItemsValue(formId, { billno: null });  	
								}
								this.toggleShow();
							} else {
								this.props.form.EmptyAllFormValue(formId);
								this.props.cardTable.setTableData(tableId, { rows: [] });
							}
						}
			});
		
		}
		
	} 

	//切换页面状态
	toggleShow = ()  => {
		
		let	status = this.props.getUrlParam("status");
		// //添加	添加或更新缓存
		// let CardData = this.props.createMasterChildData(pagecode, formId, tableId);
		// let pk_ebankdatadownload=CardData.head.head.rows[0].values.pk_ebankdatadownload.value;
		// //存入缓存
		// // let pk_ebankdatadownload = this.props.data.head[formId].rows[0].values.pk_ebankdatadownload.value;
		// let cardData = getCacheById(pk_ebankdatadownload, dataSource);
		// if(cardData){
		// //更新缓存
		// updateCache(pkname,pk_ebankdatadownload,this.props.data,formId,dataSource);
		// }else{
		// //添加缓存
		// addCache(pk_ebankdatadownload,this.props.data,formId,dataSource);
		// }
		//按钮的显示状态
		if (status == "edit") {
			this.setState({showBackBtn: false});
			this.props.button.setButtonVisible(browseButtons, false);
			this.props.button.setButtonVisible(editButtons, true);
			this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false);

		} else if (status == "add") {
			this.setState({showBackBtn: false});
			this.props.button.setButtonVisible(browseButtons, false);
			this.props.button.setButtonVisible(editButtons, true);
			this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false);
			//卡片新增赋值
			const globalInfo = window.parent.GETBUSINESSINFO && window.parent.GETBUSINESSINFO();
			this.props.form.setFormItemsValue('head', {
				pk_group: { display: globalInfo.groupName, value: globalInfo.groupId },
			});
		} else if (status == "browse") {
			this.setState({showBackBtn: true});
			this.props.button.setButtonVisible(editButtons, false);
			this.props.button.setButtonVisible(browseButtons, true);
			this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', true);
		} else {
			this.setState({showBackBtn: false});
			this.props.button.setButtonVisible(editButtons, false);
			this.props.button.setButtonVisible(browseButtons, true);
			this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false);
		}
		this.props.form.setFormStatus(formId, status);
		if (status == "edit" || status == "add"){
			this.props.cardTable.setStatus(tableId, "edit");
		}else{
			this.props.cardTable.setStatus(tableId, status);
		}
		
	}; 
	
	link2ListPage(){
		// this.props.pushTo('/list',{appcode:appcode})
		let querycondition = cacheTools.get('36101021_L01_search');
		// let pageInfo = cacheTools.getTablePageInfo('search');
		this.props.pushTo('/list',{
			status: 'browse',
			appcode:appcode,
			pagecode:'36101021_L01',
			id:''
		  });
	}


		//删除单据
		delConfirm = () => {
			let CardData = this.props.createMasterChildData(pagecode, formId, tableId);
			let url = '/nccloud/obm/ebankdatadownload/delete.do'; 
			ajax({
				url: url,
				data: CardData,
				success: (res) => {
					if (res) {
						this.props.pushTo("/list", {     
							status: 'browse',
							appcode:appcode,
							pagecode:'36101021_L01'
						  });
					}
				}
			});
		};

	//删除单据
	// delConfirm = () => {
	// 	let CardData = this.props.createMasterChildData(pagecode, formId, tableId);
	// 	let url = '/nccloud/obm/ebankbillpooldownload/delete.do'; 
	// 	ajax({
	// 		url: url,
	// 		data: CardData,
	// 		success: (res) => {
	// 			if (res) {
	// 				this.props.linkTo('/obm/ebankbillpooldownload/download/list/index.html',{   
	// 					type:'link',
	// 					appcode:appcode,
	// 					pagecode:'36101021_C01'
	// 				  });
	// 			}
	// 		}
	// 	});
	// };

	btnOperation = (path, content) => {
		let pkMapTs = new Map();
		let CardData = this.props.createMasterChildData(pagecode, formId, tableId);
		let pk = this.props.form.getFormItemsValue('head', 'pk_ebankdatadownload').value;
		let ts = this.props.form.getFormItemsValue('head', 'ts').value;
		//主键与tsMap
		if (pk && ts) {
			pkMapTs.set(pk, ts);
		}
		ajax({
			url :'/nccloud/obm/ebankdatadownload/delete.do',
			data:CardData,
			success: (res) => {
				if (res.success) {
					toast({ color: 'success', content });
					if (path === 'delete') {
						// 获取下一条数据的id
						let nextId = getNextId(pk, dataSource);
						if (nextId) {
							this.props.pushTo("/list", {     
								status: 'browse',
								appcode:appcode,
								pagecode:'36101021_L01'
							  });
							//删除缓存
							deleteCacheById('pk_ebankdatadownload', pk, dataSource);
							this.props.setUrlParam({ id: nextId });
							// getCardData.call(this, nextId);
							updateCache('pk_ebankdatadownload', id, data, 'head', dataSource);
						} else {
							this.props.pushTo("/list", {     
								status: 'browse',
								appcode:appcode,
								pagecode:'36101021_L01'
							  });
							//删除缓存
							deleteCacheById('pk_ebankdatadownload', pk, dataSource);
							// 删除的是最后一个的操作
							this.props.setUrlParam({ id: '' });
							
						}
					} else if (res.data) {
						updateCache('pk_ebankdatadownload', pk, res.data, 'head',dataSource);
						// buttonVisible.call(this, this.props);
					}
				}
			}
		});
	};
   
	
	
	//保存单据 billno ip port servelt overtime usestate weight netbankinftpstyle
	saveBill = () => {
		let CardData = this.props.createMasterChildData(pagecode, formId, tableId);
	
		let pk_org=CardData.head.head.rows[0].values.pk_org.value
		let facode=CardData.head.head.rows[0].values.facode.value
		if(pk_org==null||pk_org==''){
			toast({ color: 'warning', content: this.props.MutiInit.getIntl("36101021") && this.props.MutiInit.getIntl("36101021").get('36101021-000031')/* 国际化处理： 组织不能为空，无法保存！*/ });
			return ;	
		}
		if(facode==null||facode==''){
			toast({ color: 'warning', content: this.props.MutiInit.getIntl("36101021") && this.props.MutiInit.getIntl("36101021").get('36101021-000032')/*国际化处理： 方案不能为空，无法保存！*/ });
			return ;	
		}
	
		let bodyDatas = this.props.cardTable.getAllRows(tableId);
		// if(bodyDatas.length>0){
		// 	for (var i=0;i < bodyDatas.length;i++) {
		// 		let pk_banktype=bodyDatas[i].values.pk_banktype.display;
		// 		if(pk_banktype==null){
		// 			let rownum=i+1;
		// 			toast({ color: 'warning', content: '表体数据第'+rownum+'行银行类型为空!' });
		// 			return ;
		// 		} ;
		// 	}
		// }
		//保存校验
		if (!this.props.editTable.checkRequired(tableId, bodyDatas)) {
			return;
		}
		if (bodyDatas === undefined) {
			return
		}
		else if (bodyDatas && bodyDatas.length === 0) {
			toast({ content:  this.props.MutiInit.getIntl("36101021") && this.props.MutiInit.getIntl("36101021").get('36101021-000033')/*国际化处理： 表体数据不能为空*/, color: 'info' })
			this.props.editTable.cancelEdit(tableId)
			return
		}

		let url = '/nccloud/obm/ebankdatadownload/insert.do'; //新增保存
		if (this.props.getUrlParam('status') === 'edit') {
			url = '/nccloud/obm/ebankdatadownload/update.do'; //修改保存
		}
		ajax({
			url: url,
			data: CardData,
			success: (res) => {
				let pk_ebankdatadownload = null;
				if (res.success) {
					if (res.data) {
						if (res.data.head && res.data.head[formId]) {
							this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
							pk_ebankdatadownload = res.data.head[formId].rows[0].values.pk_ebankdatadownload.value;
						}
						if (res.data.body && res.data.body[tableId]) {
							this.props.cardTable.setTableData(tableId, res.data.body[tableId]);
						}
					}
				}
				if(url == '/nccloud/obm/ebankdatadownload/insert.do'){
					//addCache(pk_srvconf,res.data,formId,dataSource);
					addCache(pk_ebankdatadownload,res.data,formId,dataSource);
				}
				// if(url == '/nccloud/obm/ebankdatadownload/update.do'){
				// 	// updateCache(pkname,pk_srvconf,res.data,formId,dataSource);
				// 	updateCache(pk_ebankdatadownload,res.data,formId,dataSource);
				// }
				this.props.pushTo('/card', {
					status: 'browse',
					type:'link',
					appcode:appcode,
					pagecode:'36101021_C01'
				});
				this.reGetdata();
				this.toggleShow();
				toast({ color: 'success', content: this.props.MutiInit.getIntl("36101021") && this.props.MutiInit.getIntl("36101021").get('36101021-000034')/*国际化处理： 保存成功!*/ });
			},
			error: (res) => {
				toast({ color: 'danger', content: res.message });
			}
		});
	};

	getButtonNames = (codeId) => {
	};
	// renderCardChange=()=>{
	// 	let{createCardPagination}=this.props.cardPagination;
	// 	let status = this.props.getUrlParam('status');
	// 	if(status=='browse'){
	// 		return(createCardPagination({
	// 			dataSource:dataSource,
	// 			handlePageInfoChange:pageInfoClick.bind(this),
	// 		}))
	// 	}
	// }

	//获取列表肩部信息
	getTableHead = (buttons, tableId) => {
		let { createButton } = this.props.button;
		return (
			<div className="shoulder-definition-area">
				 <div className="definition-icons">
					{this.props.cardTable.createBrowseIcons(tableId, {
						iconArr: ['close', 'open', 'max'],
						maxDestAreaId: 'finance-fts-commissionpayment-card'
					})}
					{/* 应用注册按钮 */}
					  {this.props.button.createButtonApp({
                        area: 'card_body',
                        buttonLimit: 3, 
                        onButtonClick: buttonClick.bind(this), 
                        popContainer: document.querySelector('.header-button-area')
                    })}
				</div>
			</div>
		);
	}; 

	
	render() {
		let { cardTable, form, button,modal,cardPagination,ncmodal ,BillHeadInfo} = this.props;
		let buttons = this.props.button.getButtons();
		let multiLang = this.props.MutiInit.getIntl('2052');
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createButtonApp } = this.props.button;
		let { createButton, getButtons} = button;
		let { createCardPagination } = cardPagination;
		let { createModal } = ncmodal; 
		let { showBackBtn} = this.state;
		let status = this.props.getUrlParam('status') === 'browse';
		let scene = this.props.getUrlParam('scene') === 'linksce';
		let showPagintion = status && !scene;
		const that=this;
		let { createBillHeadInfo } = BillHeadInfo;
		const langData = this.props.MultiInit.getLangData('36101021');
		return ( 
			<div className="nc-bill-extCard fmc-demo">
				<div className="nc-bill-top-area">
					<NCAffix>
						<div className="nc-bill-header-area">
							{/* <div className="header-title-search-area">
								</div>
							<div >
							{
								showBackBtn?
								<NCBackBtn onClick={this.link2ListPage}></NCBackBtn>
								:''
							}
						    </div> */}
								
							<div className="header-title-search-area">
							        <span>
									{createBillHeadInfo({
										title: this.props.MutiInit.getIntl("36101021") && this.props.MutiInit.getIntl("36101021").get('36101021-000008'),/* 国际化处理： 电票数据下载设置-组织*/
										initShowBackBtn:showBackBtn,
										backBtnClick: () => {
											//返回按钮的点击事件
											this.link2ListPage()
										},
									})}	
									</span>
							</div>


							<div className="header-button-area">
							{this.props.button.createButtonApp({
								area: 'card_head',
								buttonLimit: 4,
								onButtonClick: buttonClick.bind(this),
								popContainer: document.querySelector('.header-button-area')
							})}
						  </div>
						  <div className="header-cardPagination-area" style={{ float: 'right' }}>
							{createCardPagination({
								dataSource:dataSource,
								handlePageInfoChange: pageInfoClick.bind(this)
							})}
						</div>	
						</div>
					</NCAffix>
					<NCScrollElement name="forminfo">
					<div className="nc-bill-form-area">
						{createForm(formId, {
							onAfterEvent: afterEvent.bind(this)
						})}
					</div>
				</NCScrollElement>

				</div>

            <NCScrollElement name="businfo">
					<div className="nc-bill-table-area" style={{ height: '4px' }}>
						{/* {this.getTableHead(buttons, tableId)} */}
						{createCardTable(tableId, {
							onButtonClick: buttonClick.bind(this),
							tableHead: this.getTableHead.bind(this, buttons, tableId),
							modelSave: this.saveBill,
							adaptionHeight: true,
							showCheck: true,
							// showIndex: true,
						})}
					</div>
				</NCScrollElement> 

				{createModal('delete', {
					title: this.props.MutiInit.getIntl("36101021") && this.props.MutiInit.getIntl("36101021").get('36101021-000025'),/* 国际化处理： 删除*/
					content: this.props.MutiInit.getIntl("36101021") && this.props.MutiInit.getIntl("36101021").get('36101021-000028'),/* 国际化处理： 确认要删除吗?*/
					beSureBtnClick: this.delConfirm.bind(this)
				})}
				{createModal(`confirm`, { color: 'warning', hasCloseBtn: true })}
			</div>

		);
	}
}

Card = createPage({
	mutiLangCode: '36101021',
	initTemplate: initTemplate
})(Card);

// ReactDOM.render(<Card />, document.querySelector('#app'));
export default Card;