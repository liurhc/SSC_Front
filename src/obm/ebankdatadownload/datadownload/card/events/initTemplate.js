
// let formId = 'head';
// let tableId = 'table_srvconf_01';
// let pageId = '36100SC_C01';

import { base, ajax } from 'nc-lightapp-front';
// import intl from 'react-intl-universal';
import { tableId, formId,appid, pagecode,appcode } from "../constants";
import {tableButtonClick} from "./tableButtonClick";

let { NCPopconfirm } = base;

export default function(props) {
	props.createUIDom(
		{
			pagecode: pagecode,//页面id
			appcode:appcode,
			// appid: appid//注册按钮的id
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta)
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
			}
		}
		
	)
}

function modifierMeta(props, meta) {
	let status = props.getUrlParam('status');
	meta[formId].status = status;
	meta[tableId].status = status; 

	//添加操作列 

	return meta;
}
