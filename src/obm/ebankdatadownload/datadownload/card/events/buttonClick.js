import { ajax, base, toast, promptBox,cardCache } from 'nc-lightapp-front';
import { appid, formId, tableId, pagecode,appcode,dataSource,pkname } from '../constants.js';
let {getCurrentLastId,getCacheById,updateCache } = cardCache;
export default function(props, id) {
  switch (id) {
    case 'Add':
        props.form.EmptyAllFormValue(formId)
        props.cardTable.setTableData(tableId)
        props.pushTo('/card', {
          status: 'add',
          id:props.getUrlParam('id'),
          type:'link',
          appcode:appcode,
          pagecode:'36101021_C01'
        })
        this.toggleShow();
        break;
    case 'Reback':
    props.pushTo('/list', {
        status: 'browse',
        appcode: appcode,
        pagecode: '36101021_L01',
      })
        break;
    case 'Save':
        this.saveBill();
        //this.toggleShow();
        break;
    case 'Refresh':
        // this.generalAssLineFile();
        // this.toggleShow();
        // break
        this.reGetdata();
        this.toggleShow();
        break
    case 'Edit':
        let pk = this.props.form.getFormItemsValue('head', 'pk_ebankdatadownload').value;
         //数据校验
        if (pk ==null || pk.length == 0) {
            toast({ color: 'warning', content: props.MutiInit.getIntl("36101021") && props.MutiInit.getIntl("36101021").get('36101021-000035')/* 国际化处理： 没有可修改数据! */ });
            return
        }
        props.pushTo('/card', {
            status: 'edit',
            id: props.getUrlParam('id'),
            type:'link',
            appcode:appcode,
            pagecode:'36101021_C01'
        })
        this.toggleShow();
        break;
    // case 'Delete':
    //     props.ncmodal.show('delete');
        // this.delConfirm();
        // this.refresh();
        // this.toggleShow();
        // break
        //头部 删除
        case 'Delete':
        let selectpk = this.props.form.getFormItemsValue('head', 'pk_ebankdatadownload').value;
        //数据校验
        if (selectpk ==null || selectpk.length == 0) {
            toast({ color: 'warning', content: props.MutiInit.getIntl("36101021") && props.MutiInit.getIntl("36101021").get('36101021-000036')/* 国际化处理： 没有可删除数据! */ });
            return
        }
        delBill.call(this,props);
        break;
    case 'Cancel':
        props.ncmodal.show('confirm', {
            title: props.MutiInit.getIntl("36101021") && props.MutiInit.getIntl("36101021").get('36101021-000037'),/* 国际化处理： 取消*/
            content: props.MutiInit.getIntl("36101021") && props.MutiInit.getIntl("36101021").get('36101021-000038'),/* 国际化处理： 确认要取消吗?*/
            beSureBtnClick: () => {
                if ((props.getUrlParam('status') === 'edit')){
                    // 表单返回上一次的值
                    props.form.cancel(this.formId)
                    // 表格返回上一次的值
                    props.pushTo('/card', {
                        status: 'browse',
                        appcode:appcode,
                        id: props.getUrlParam('id'),
                        pagecode:'36101021_C01'
                    }); 
                    this.reGetdata();
                    this.toggleShow();
                }else if(props.getUrlParam('status') === 'add'){
                    // 表单返回上一次的值
                    props.form.cancel(this.formId);
                    let pk = props.getUrlParam('id');
                    if(!pk){
                        pk=getCurrentLastId(dataSource);
                    } 
                    //表格返回上一次的值
                    props.pushTo('/card', {
                        status: 'browse',
                        appcode:appcode,
                        id: pk,
                        pagecode:'36101021_C01'
                    }); 
                   
                    let cardData = getCacheById(pk, dataSource);
                    if (cardData) {
                        this.props.form.setAllFormValue({ [formId]: cardData.head[formId] });
                        this.props.cardTable.setTableData(tableId, cardData.body[tableId]);
                        this.setState({
                            billno: cardData.head[formId].rows[0].values.billno.value,
                        });
                        this.toggleShow();
                    } else {
                        let dataArr = pk;
                        let data = {
                            pk: dataArr,
                            pageCode: '36101021_C01' 
                        }; 
                        ajax({
                            url: '/nccloud/obm/ebankdatadownload/cardquery.do',
                            data: data,
                            success: (res) => { 
                                if (res.data) {
                                    if (res.data.head) {											
                                        this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
                                        let billno =res.data.head[formId].rows[0].values.billno.value;
                                        this.setState({
                                            billno:billno
                                        });
                                    }
                                    if (res.data.body) {
                                        this.props.cardTable.setTableData(tableId, res.data.body[tableId]);
                                    }						
                                    this.toggleShow();   
                                    updateCache(pkname,pk,res.data,formId,dataSource);                     
                                } else {
                                    this.props.form.EmptyAllFormValue(formId);
                                    this.props.cardTable.setTableData(tableId, { rows: [] });
                                    this.toggleShow(); 
                                }
                            }
                        });
                    }

                }
               
            }
        });
        // this.reGetdata();
        // this.toggleShow();
        // cancelBill.call(this, props);
    break
    case 'AddLine':
        props.cardTable.addRow(tableId) 
        break;
    case 'DelLine':
        let delcurrRows = props.cardTable.getCheckedRows(tableId);
        let currSelect2 = [];
        if (delcurrRows && delcurrRows.length > 0) {
            for (let item of delcurrRows) {
              currSelect2.push(item.index);
            }
        }
        if (currSelect2.length == 0) {
            toast({ color: 'warning', content:  props.MutiInit.getIntl("36101021") && props.MutiInit.getIntl("36101021").get('36101021-000009')/* 国际化处理： 请选择数据! */});
            return ;
        }

        // promptBox({
        //     color: 'warning',
        //     title: '删除',
        //     content: '确定要删除所选数据吗？',
        //     beSureBtnClick: () => {
                props.cardTable.delRowsByIndex(tableId, currSelect2);
        //     }
        // });
      
        // props.cardTable.delRowsByIndex(tableId, currSelect2);
        break; 
        //行 复制
    case 'copyRow':
        let selectRows = props.cardTable.getCheckedRows(tableId);
        if (selectRows == null || selectRows.length == 0) {
           toast({
	        'color': 'warning',
	       'content': props.MutiInit.getIntl("36101021") && props.MutiInit.getIntl("36101021").get('36101021-000039')/* 国际化处理： 未选中要复制的行。*/
         });
        return false;
        }
        // 粘贴末行位置index
	     let copyindex = props.cardTable.getNumberOfRows(tableId, false);
	     let selectCopyData = [];
	     let selectRowCopy = JSON.parse(JSON.stringify(selectRows));
	     for (let item of selectRowCopy) {
	    item.data.selected = false;
	    item.data.values.pk_ebankdatadownload_b = {
	         value: null,
	         display: null
	    };
	    selectCopyData.push(item.data);
        }
         props.cardTable.insertRowsAfterIndex(tableId, selectCopyData, copyindex);
 
      break;
    default:
      break
  }

  /**
 * 取消
 * @param {*} props  页面内置对象
 */
function cancelBill(props) {
	promptBox({
        color: 'warning',
        title: props.MutiInit.getIntl("36101021") && props.MutiInit.getIntl("36101021").get('36101021-000037'),/* 国际化处理： 取消*/
		content: props.MutiInit.getIntl("36101021") && props.MutiInit.getIntl("36101021").get('36101021-000038'),/* 国际化处理： 确认要取消吗?*/
		beSureBtnClick: () => {
			handelCancel.call(this, props);
		}
	});
}

/**
 * 取消回调
 * @param {*} props  页面内置对象
 */
function handelCancel(props) {
	let id = props.getUrlParam('id');
	props.setUrlParam({ status: 'browse' });
	if (id) {
		props.form.cancel(this.formId);
		props.cardTable.resetTableData(this.tabCode);
		setEditStatus.call(this, 'browse');
	} else {
		setEditStatus.call(this, 'browse');
		clearAll.call(this, this.props);
	}
}

function clearAll(props) {
	let data = {
		rows: []
	};
	props.form.EmptyAllFormValue('head');
    props.cardTable.setAllTabsData(data,'body');
    setEditStatus.call(this, 'browse');
}

/**
 * 设置编辑性
 * @param {*} status  编辑状态，传入browse或者edit
 */
 function setEditStatus(status) {
	if (status === 'edit') {
		Promise.resolve(setItemDisabled(this.props, status)); //异步执行设置编辑性，目的是等待卡片切换成编辑态之后再设置编辑性，也可以使用setTimeout
	}
}


  /**
 * 删除
 */
function delBill(props) {
	promptBox({
		color: 'warning',
		title: props.MutiInit.getIntl("36101021") && props.MutiInit.getIntl("36101021").get('36101021-000025'),/* 国际化处理： 删除*/
		content: props.MutiInit.getIntl("36101021") && props.MutiInit.getIntl("36101021").get('36101021-000028'),/* 国际化处理： 确认要删除吗?*/
		beSureBtnClick: () => {
            this.btnOperation('delete', props.MutiInit.getIntl("36101021") && props.MutiInit.getIntl("36101021").get('36101021-000040')); /* 国际化处理： 删除成功!*/
            // this.delConfirm();
		}
	});
}
}
