/**
 * 表体区域
 */
export const tableId = 'list_head'; 

/**
 * 表头区域
 */
export const formId = 'head';

/**
 * 查询区域编码
 */
// export const searchId = 'search';
 
/**
 * 页面编码
 */
export const pagecode = '36101021_L01';
export const appcode = '36101021';
// export const appid = '0001Z61000000003AEAC';
export const appid = '1001Z61000000001KIQ8';
export const dataSource = 'tm.obm.ebankdatadownload.datadownload';
/**
 * 编辑态按钮
 */
export const editButtons = ['Save', 'Cancel'];

/**
 * 浏览态按钮
 */
export const browseButtons = ['Add','Delete','ConnectTest','Refresh','Edit'];