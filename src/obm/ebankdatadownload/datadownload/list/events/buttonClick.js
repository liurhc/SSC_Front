import { ajax, base, print,toast,output,cacheTools,promptBox  } from 'nc-lightapp-front';
import searchBtnClick from './searchBtnClick';
const tableid = 'list_head';
const sreachId = 'search';

export default function buttonClick(props, id) {
	console.log(id)
	switch (id) {
		case 'Add':
		// props.form.EmptyAllFormValue('head', { rows: [] })
		// props.cardTable.setTableData('body', { rows: [] })
        props.form.EmptyAllFormValue('head')
        props.cardTable.setTableData('body')
        props.pushTo('/card', {
          status: 'add',
          appcode:'36101021',
		  pagecode:'36101021_C01',
        })
        this.toggleShow();
		break;
		case 'Delete':
		// selectDatas = props.table.getCheckedRows('list_head');
		// if(selectDatas.length==1){
		// 	props.ncmodal.show('delete');
		// }else{
		// 	props.ncmodal.show('batchDel');
		// }
			promptBox({
				color: "warning",
				title: props.MutiInit.getIntl("36101021") && props.MutiInit.getIntl("36101021").get('36101021-000025'),/* 国际化处理： 删除*/
				content: props.MutiInit.getIntl("36101021") && props.MutiInit.getIntl("36101021").get('36101021-000028'),/* 国际化处理： 确认要删除吗?*/
				beSureBtnClick: () => { this.deleteAction()}
			});	  
			break; 
		case 'Refresh':
			refreshAction(props);
			this.toggleShow();
			break;
		
		
		case 'output':
			printData = props.table.getCheckedRows(tableid);
			//数据校验
			if (printData.length == 0) {
				toast({ color: 'warning', content: props.MutiInit.getIntl("36101021") && props.MutiInit.getIntl("36101021").get('36101021-000009')});/* 国际化处理： 请选择数据*/
				return
			}
			pks = [];
			printData.forEach((item) => {
				pks.push(item.data.values.pk_ebank_paylog_h.value);
			});
			output({
				url: '/nccloud/obm/ebankpaylog/print.do',
				data: { 
					 oids: pks,
					 outputType: 'output'
				}
			});
			break;
		case 'Print':
			printData = props.table.getCheckedRows(tableid);
			//数据校验
			if (printData.length == 0) {
				toast({ color: 'warning', content: props.MutiInit.getIntl("36101021") && props.MutiInit.getIntl("36101021").get('36101021-000009') });/* 国际化处理：  请选择数据*/
				return
			}
			pks = [];
			printData.forEach((item) => {
				pks.push(item.data.values.pk_ebank_paylog_h.value);
			});
			print(
				'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
				'/nccloud/obm/ebankpaylog/print.do',
				{
					funcode: '36101021', //功能节点编码，即模板编码
					appcode: '36101021', //模板节点标识
					oids: pks // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
				}
			);
			break;
		case 'Preview':
			printData(props);
			break;
			case 'Edit':
			let selectDatas = null;
			selectDatas =this.props.table.getCheckedRows('list_head');
			//数据校验
			if (selectDatas ==null || selectDatas.length == 0) {
			 toast({ color: 'warning', content: props.MutiInit.getIntl("36101021") && props.MutiInit.getIntl("36101021").get('36101021-000009') });/* 国际化处理：  请选择数据*/
			 return
		  }
		  props.pushTo('/card',{ 
		  status:'edit',
		  appcode:'36101021',
		  id:selectDatas[0].data.values.pk_ebankdatadownload.value,
		  pagecode:'36101021_C01'
	  })
	  break;

		default:
			break
	}
}

/**
 * 打印、预览
 * @param {*} props 
 */
function printData(props) {
	let printData = props.table.getCheckedRows(tableid);
	//数据校验
	if (printData.length == 0) {
		toast({ color: 'warning', content: props.MutiInit.getIntl("36101021") && props.MutiInit.getIntl("36101021").get('36101021-000009') });/* 国际化处理：  请选择数据*/
		return
	}
	let pks = [];
	printData.forEach((item) => {
		pks.push(item.data.values.pk_ebank_paylog_h.value);
	});
	print(
		'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
		'/nccloud/obm/ebankpaylog/print.do',
		{
			funcode: '36101021', //功能节点编码，即模板编码
			appcode: '36101021', //模板节点标识
			// printTemplateID: '1001Z61000000000LPN4',//列表打印模板pk 
			oids: pks // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
		}
	);
}


/**
 * 刷新
 * @param {*} props 
 */
const refreshAction = (props) => {
	// let searchVal = props.search.getAllSearchData(sreachId,false);
	let searchVal = props.search.getAllSearchData(sreachId);
	let pageInfo = props.table.getTablePageInfo(tableid);
	let metaData = props.meta.getMeta();
	//如果有缓存条件，查询
	let querycondition = cacheTools.get('36101021_L01_search');
	console.log(querycondition);
	if (!querycondition) {
		return;
	}
		cacheTools.set('36101021_C01_back', '1');
		// props.search.setSearchValue('36101021', querycondition);
		// let searchVal = props.search.getAllSearchData(sreachId);
		// let queryInfo = props.search.getQueryInfo('36101021', true);
        // let oid = queryInfo.oid;					
	let data = {
		conditions: searchVal.conditions || searchVal,
		pageInfo: pageInfo,
		pagecode: "36101021_L01",
		queryAreaCode: "36101021",
		oid: '1001Z61000000001KIQK',
		queryType: "simple"
	};
	console.log(data);
	ajax({
		url: '/nccloud/obm/ebankdatadownload/query.do',//'/nccloud/reva/search/query.do',
		data: data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data && data[tableid]) {
					props.table.setAllTableData(tableid, res.data[tableid]);
					toast({ color: 'success', content: props.MutiInit.getIntl("36101021") && props.MutiInit.getIntl("36101021").get('36101021-000014') });/* 国际化处理： 刷新成功*/
				} else {
					props.table.setAllTableData(tableid, { rows: [] });
					toast({ color: 'warning', content: props.MutiInit.getIntl("36101021") && props.MutiInit.getIntl("36101021").get('36101021-000011') });/* 国际化处理： 未查询出符合条件的数据！*/
				}
			}
		},
		error: (res) => {
			console.log(res.message);
		}
	});
}
