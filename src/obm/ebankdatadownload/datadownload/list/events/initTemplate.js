import { ajax, base, cacheTools } from 'nc-lightapp-front';
let { NCPopconfirm, NCIcon, NCTooltip:Tooltip } = base;
import searchBtnClick from './searchBtnClick';
import tableButtonClick from './tableButtonClick';

let tableid = 'list_head';
let pageId = '36101021_L01';
let searchId = 'search';
export default function (props) {
	//请求模板数据
	props.createUIDom(
		{
			pagecode: pageId,//页面id
			appcode: '36101021'//注册按钮的id
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta)
					props.meta.setMeta(meta, () => {

						//如果有缓存条件，查询
						let querycondition = cacheTools.get('36101021_L01_search');
						console.log(querycondition);
						if (querycondition) {
							cacheTools.set('36101021_C01_back', '1');
							props.search.setSearchValue('search', querycondition);
							searchBtnClick(props, querycondition);
						}
					});
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
			}
		}
	)
}

function modifierMeta(props, meta) {
	meta[searchId].items = meta[searchId].items.map((item, key) => {
		// item.visible = true;
		item.col = '2';
		return item;
	})

	// 查询条件多选
	meta[searchId].items.find((e) => e.attrcode === 'pk_org').isMultiSelectedEnabled = true;
	//财务组织用户过滤
	meta[searchId].items.map((item)=>{
		if (item.attrcode == 'pk_org' ) {
			       item.queryCondition = () => {
			       		return {
			            	funcode: '36101021',
			            	TreeRefActionExt: 'nccloud.web.tmpub.filter.FinanceOrgPermissionFilter'
			        	};
			        };
			     }
	});


	meta[tableid].items = meta[tableid].items.map((item, key) => {
		// item.width = 180;
		if (item.attrcode == 'srcbillcode') {
			item.render = (text, record, index) => {
				let tip = (<div>{record.srcbillcode.value}</div>)
				return (
					<a
						style={{ textDecoration: '', cursor: 'pointer' }}
						onClick={() => {
							props.pushTo('/card', {
								status: 'browse',
								pagecode: '36101021_C01',
								appcode:'36101021',
								id: record.pk_ebankdatadownload.value
							});
						}}
					>
						{record && record.srcbillcode && record.srcbillcode.value}
					</a>
				);
			};
		}
		return item;
	});


	return meta;
}
