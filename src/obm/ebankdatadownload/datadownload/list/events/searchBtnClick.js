import {ajax,toast,cacheTools,cardCache} from 'nc-lightapp-front';
import {dataSource} from "../constants";
let{setDefData}=cardCache;
let tableid = 'list_head';
//点击查询，获取查询区数据
export default function clickSearchBtn(props,searchVal) {
    let backmark = cacheTools.get('36101021_C01_back');
    // setDefData('search', dataSource, searchVal);
    cacheTools.set('search', searchVal); 
    console.log(searchVal);
    //缓存查询条件
    cacheTools.set('36101021_C01_back', '0');
	cacheTools.set('36101021_L01_search', searchVal);
    let pageInfo = props.table.getTablePageInfo(tableid);
    let metaData = props.meta.getMeta();
    let queryInfo = props.search.getQueryInfo('search', false);
    //let oid = queryInfo.oid;2019年6月26日
    let oid = "1001Z61000000001KIQK";//查询区域主键
    let data={
        conditions:searchVal.conditions || searchVal,
        pageInfo:pageInfo,
        pagecode: "36101021_L01",
        queryAreaCode:"36101021",
        oid:oid,
        queryType:"simple"
     };

    console.log(data);
    ajax({
        url: '/nccloud/obm/ebankdatadownload/query.do',
        data: data,
        success:  (res)=> {
            let { success, data } = res;
            if(success){
                // this.setState({ numvalues: data.numvalues});
                // setDefData('36101021_list_tab', dataSource, this.state.numvalues);
                // setDefData('36101021_list_tabActive', dataSource, this.state.activeKey);
                props.button.setDisabled({
                    EbankLogDownloadStateAction: true,
                    EbankLogFireEventSendStateAction:true,
                    Print:true,
                    output:true,
                    Refresh:false
                });
                if(data && data[tableid]){
                    props.table.setAllTableData(tableid, res.data[tableid]);
                    if(!backmark || backmark==="0"){
                       let begString =props.MutiInit.getIntl("36101021") && props.MutiInit.getIntl("36101021").get('36101021-000012');
                        let endString = props.MutiInit.getIntl("36101021") && props.MutiInit.getIntl("36101021").get('36101021-000013');
                        let length = res.data[tableid].allpks.length;
                        toast({content: begString + length + endString,color:"success"});/* 国际化处理： 查询成功，共,条*/
                    }
                    setDefData(tableid,dataSource,data);
                }else{
                    props.table.setAllTableData(tableid, {rows:[]});
                    if(!backmark || backmark==="0"){
                        toast({content:props.MutiInit.getIntl("36101021") && props.MutiInit.getIntl("36101021").get('36101021-000011'),color:"warning"});/* 国际化处理： 无数据*/
                    }
                }	
            }
        },
        error : (res)=>{
			console.log(res.message);
		}
    });

};
