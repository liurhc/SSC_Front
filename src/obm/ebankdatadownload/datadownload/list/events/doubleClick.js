import { ajax, toast ,cacheTools} from 'nc-lightapp-front';
import { appid, formId, tableId, pagecode,appcode} from '../constants.js';

export default function doubleClick(record, index, e) {
    let searchVal = this.props.search.getAllSearchData('search');  
    // cacheTools.set('list_head', searchVal); 
    cacheTools.set('search', searchVal); 
    this.props.pushTo('/card', {
        status: 'browse',
        appcode:'36101021',
        pagecode:'36101021_C01',
        id: record.pk_ebankdatadownload.value
    });
    // this.props.linkTo('/obm/ebankdatadownload/datadownload/card/index.html', {
    //     status: 'browse',
    //     type:'link',
	// 	appcode:'36101021',
    //     id: record.pk_ebankdatadownload.value,
    //     pagecode:'36101021_L01'
    // });
}
