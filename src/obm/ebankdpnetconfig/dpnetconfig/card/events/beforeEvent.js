import { ajax } from 'nc-lightapp-front';
import { tableId, formId,appid, pagecode } from "../constants";

export default function beforeEvent(props, moduleId, key, value) {
    if (key === 'netbankinftpcode') {
        let CardData = this.props.createMasterChildData(pagecode, formId, tableId);
        this.loadNetStyle(CardData);
    }
    return true;
}