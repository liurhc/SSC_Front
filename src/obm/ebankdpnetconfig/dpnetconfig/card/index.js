//主子表卡片
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast, high, cardCache, formDownload, createPageIcon } from 'nc-lightapp-front';
let { NCFormControl, NCPopconfirm, NCAnchor, NCScrollLink, NCScrollElement, NCAffix, NCBackBtn } = base;
import { tableId, tableId2, formId, pagecode, appcode, editButtons, browseButtons, dataSource, pkname, tabs } from "./constants";
import { buttonClick, initTemplate, beforeEvent, afterEvent, pageInfoClick } from './events';
import './index.less';
let { getCacheById, updateCache, addCache, deleteCacheById } = cardCache;

class Card extends Component {
	constructor(props) {
		super(props);
		this.state = {
			srvid: '',//单据编号,
			//返回箭头
			showNCbackBtn: false,
			data: []
			// copyflag: false,
			// switchflag1:false,//控制展开 收起按钮显隐性
			// index:null//控制当前行展开收起
		};
		initTemplate.bind(this);
		initTemplate.call(this, props);
	}
	componentDidMount() {
		// this.refresh();
	}

	componentWillMount() {
		window.onbeforeunload = () => {
			if (!['browse'].includes(this.props.getUrlParam('status'))) {
				return this.props.MutiInit.getIntl("361010SC_Card") && this.props.MutiInit.getIntl("361010SC_Card").get('361010SC_Card-000025'); /* 国际化处理： 当前单据未保存, 您确定离开此页面?*/
			}
		};
	}

	loadNetStyle = (CardData) => { //加载对应类型的银行类别到表体下拉2018-8-21
		let that = this;
		ajax({
			data: CardData,
			url: '/nccloud/obm/ebankdpnetconfig/selectbanktype.do',
			async: false,
			success: (res) => {
				if (res.data && res.data.alltypecode) {
					let that = this;
					let meta = that.props.meta.getMeta();
					let items = meta[tableId].items;
					let options = [];
					let index = res.data.alltypecode.indexOf('*');
					let srcinfo = res.data.alltypecode;
					if (index > 0) {
						let srcinfos = srcinfo.split('*');
						for (let i = 0; i < srcinfos.length; i++) {
							options[i] = {
								"display": srcinfos[i].split('+')[0],
								"value": srcinfos[i].split('+')[1]
							};
						}
					} else {
						let srcinfos = new Array(2);
						srcinfos = srcinfo.split('+');
						options[0] = {
							"display": srcinfos[0],
							"value": srcinfos[1]
						};
					}
					items.find((item) => item.attrcode == 'netbankinftpcode').options = options;
					that.props.meta.setMeta(meta);
				}
			}
		});
	}

	//转换banktype.xml增加的新银行
	//新增加的银行查询到界面上显示时{value:'',display:''} value和display相同,需要转换
	converNetbankinftpcode = (bodys) => {

		let bankData = bodys[tableId];
		if (bankData && bankData.rows) {
			let meta = this.props.meta.getMeta();
			let items = meta[tableId].items;
			let options = [];
			options = items.find((item) => item.attrcode == 'netbankinftpcode').options;

			let netBankInftpCodeMap = new Map();
			options.map((option) => {
				netBankInftpCodeMap.set(option.value, option.display)
			})

			bankData.rows.forEach((item) => {
				let netbankinftpcode = item.values.netbankinftpcode.value;
				let display = netBankInftpCodeMap.get(netbankinftpcode);
				item.values.netbankinftpcode.display = display;
			})
		}
	}

	LoadXiaLa = () => {//加载枚举下拉数据
		let meta = this.props.meta.getMeta();
		let items = meta["head"].items;
		let options = [];
		options[0] = {
			"display": this.props.MutiInit.getIntl("361010SC_Card") && this.props.MutiInit.getIntl("361010SC_Card").get('361010SC_Card-000020'),/* 国际化处理： 未启用*/
			"value": 0
		};
		options[1] = {
			"display": this.props.MutiInit.getIntl("361010SC_Card") && this.props.MutiInit.getIntl("361010SC_Card").get('361010SC_Card-000021'),/* 国际化处理： 已启用*/
			"value": 1
		};
		items.find((item) => item.attrcode == 'usestate').options = options;

		this.props.meta.setMeta(meta);

	}

	//通过单据id刷新单据信息
	reGetdata = () => {
		this.refresh();

	};

	//加载数据
	refresh = () => {
		let netbankinftpcode;
		let browseflag = false;
		let addflag = false;
		let editflag = false;
		if (this.props.getUrlParam('status') == 'browse') {
			browseflag = true;
			this.setState({ showNCbackBtn: true });
		} else if (this.props.getUrlParam('status') == 'edit') {
			editflag = true;
			this.setState({ showNCbackBtn: false });
		} else if (this.props.getUrlParam('status') == 'add') {
			addflag = true;
			this.setState({ showNCbackBtn: false });
		}

		if (browseflag || editflag) {
			let data;
			data = { pk: this.props.getUrlParam('id'), pageCode: pagecode };
			// let that = this;
			ajax({
				url: '/nccloud/obm/ebankdpnetconfig/cardquery.do',
				data: data,
				success: (res) => {
					if (res.data) {
						//add by ydxiongbo 加载banktype.xml
						this.loadNetStyle(res.data);
						//end
						if (res.data.head) {
							this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
							let srvid = res.data.head[formId].rows[0].values.srvid.value;
							this.setState({
								srvid: srvid
							});
						}
						if (res.data.bodys) {
							this.converNetbankinftpcode(res.data.bodys);
							this.props.cardTable.setTableData(tableId, res.data.bodys[tableId]);
							this.props.cardTable.setTableData(tableId2, res.data.bodys[tableId2]);
						}
						//add by 
						// if (editflag) {
						// 	this.loadNetStyle(res.data);
						// }
						//添加	添加或更新缓存
						//存入缓存
						// let pk_billpoolconfig = res.data.head[formId].rows[0].values.pk_billpoolconfig.value;
						// let cardData = getCacheById(pk_billpoolconfig, dataSource);
						// if(cardData){
						// 	//更新缓存
						// 	updateCache(pkname,pk_billpoolconfig,res.data,formId,dataSource);
						// }else{
						// 	//添加缓存
						// 	addCache(pk_billpoolconfig,res.data,formId,dataSource);
						// }
						//更新Tab数据 
						this.updateTabData(null, res.data);
						//页面切换
						this.toggleShow();
					} else {
						this.props.form.EmptyAllFormValue(formId);
						this.props.cardTable.setTableData(tableId, { rows: [] });
					}
				}
			});

		}

		if (addflag) {
			this.setState({
				srvid: ''
			});
			this.props.form.setFormItemsValue(formId, { srvid: null });
			let value = { "display": "/ufbank/ufbank.asp?client=java", "value": "/ufbank/ufbank.asp?client=java" };
			this.props.form.setFormItemsValue(formId, { servelt: value });
			//页面切换
			this.toggleShow();
		}

	}

	//切换页面状态
	toggleShow = () => {
		// let copyflag = this.state.copyflag || false;
		let status = this.props.getUrlParam("status");
		let flag = status === 'browse' ? false : true;

		//操作列的按钮显示状态
		// props.button.setButtonVisible(['openline','copyline'], flag && !copyflag);
		// props.button.setButtonVisible(['copyatline','cancellines'], flag && copyflag);	 

		//按钮的显示状态
		this.setState({ showNCbackBtn: !flag });
		this.props.button.setButtonVisible(browseButtons, !flag);
		this.props.button.setButtonVisible(editButtons, flag);
		this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', !flag)
		this.props.form.setFormStatus(formId, status);
		if (status == "edit") {
			// this.props.form.setFormItemsDisabled(formId,{servelt: true});
			this.props.cardTable.setStatus(tableId, "edit");
		} else if (status == "add") {
			this.props.cardTable.setStatus(tableId, "edit");
		} else {
			this.props.cardTable.setStatus(tableId, status);
		}

	};

	link2ListPage = () => {

		this.props.pushTo("/list", {
			type: 'link',
			appcode: appcode,
			pagecode: '361010SC_C01'
		});
	}


	//删除单据
	delConfirm = () => {
		let CardData = this.props.createMasterChildData(pagecode, formId, tableId);
		let CardData2 = this.props.createTabsCardData(
			pagecode,
			formId,
			tabs.tabOrder
		);
		let id = this.props.getUrlParam("id");
		let url = '/nccloud/obm/ebankdpnetconfig/delete.do'; //新增保存
		ajax({
			url: url,
			data: CardData2,
			success: (res) => {
				//调用删除缓存数据方法
				/*
				* idname: 数据主键的命名
				* id：数据主键的值
				* dataSource: 缓存数据命名空间
				*/
				deleteCacheById(pkname, id, dataSource);
				this.updateTabData(null);
				//跳转到列表页
				if (res) {
					this.props.pushTo("/list", {
						type: 'link',
						appcode: appcode,
						pagecode: '361010SC_C01'
					});
				}
			}
		});
	};

	//连接测试
	connectTest = () => {
		let CardData = this.props.createMasterChildData(pagecode, formId, tableId);
		let url = '/nccloud/obm/ebankdpnetconfig/connect.do';
		ajax({
			url: url,
			data: CardData,
			success: (res) => {
				let pk_billpoolconfig = null;
				if (res.success) {
					if (res.data) {
						if (res.data.head && res.data.head[formId]) {
							this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
							pk_billpoolconfig = res.data.head[formId].rows[0].values.pk_billpoolconfig.value;
						}
						if (res.data.bodys && res.data.bodys[tableId]) {
							this.props.cardTable.setTableData(tableId, res.data.bodys[tableId]);
						}
					}
				}
				this.toggleShow();
			}
		});
		// }
	};

	//保存单据 srvid ip port servelt overtime usestate weight 
	saveBill = () => {
		let CardData = this.props.createMasterChildData(pagecode, formId, tableId);
		let CardData2 = this.props.createTabsCardData(
			pagecode,
			formId,
			tabs.tabOrder
		);
		let srvid = CardData.head.head.rows[0].values.srvid.value;
		let ip = CardData.head.head.rows[0].values.ip.value;
		let port = CardData.head.head.rows[0].values.port.value;
		let servelt = CardData.head.head.rows[0].values.servelt.value;
		let overtime = CardData.head.head.rows[0].values.overtime.value;
		let usestate = CardData.head.head.rows[0].values.usestate.value;
		let weight = CardData.head.head.rows[0].values.weight.value;
		if (srvid == null || srvid == '') {
			toast({ color: 'warning', content: this.props.MutiInit.getIntl("361010SC_Card") && this.props.MutiInit.getIntl("361010SC_Card").get('361010SC_Card-000006') });/* 国际化处理： 服务器编码不能为空，无法保存！*/
			return;
		}
		if (ip == null || ip == '') {
			toast({ color: 'warning', content: this.props.MutiInit.getIntl("361010SC_Card") && this.props.MutiInit.getIntl("361010SC_Card").get('361010SC_Card-000007') });/* 国际化处理： ip地址不能为空，无法保存！*/
			return;
		}
		if (port == null || port == '') {
			toast({ color: 'warning', content: this.props.MutiInit.getIntl("361010SC_Card") && this.props.MutiInit.getIntl("361010SC_Card").get('361010SC_Card-000008') });/* 国际化处理： 端口不能为空，无法保存！*/
			return;
		}
		if (servelt == null || servelt == '') {
			toast({ color: 'warning', content: this.props.MutiInit.getIntl("361010SC_Card") && this.props.MutiInit.getIntl("361010SC_Card").get('361010SC_Card-000009') });/* 国际化处理： servelt服务不能为空，无法保存！*/
			return;
		}
		if (overtime == null || overtime == '') {
			toast({ color: 'warning', content: this.props.MutiInit.getIntl("361010SC_Card") && this.props.MutiInit.getIntl("361010SC_Card").get('361010SC_Card-000010') });/* 国际化处理： 超时（秒）不能为空，无法保存！*/
			return;
		}
		if (usestate == null || usestate == '') {
			toast({ color: 'warning', content: this.props.MutiInit.getIntl("361010SC_Card") && this.props.MutiInit.getIntl("361010SC_Card").get('361010SC_Card-000011') });/* 国际化处理： 启用状态不能为空，无法保存！*/
			return;
		}
		if (weight == null || weight == '') {
			toast({ color: 'warning', content: this.props.MutiInit.getIntl("361010SC_Card") && this.props.MutiInit.getIntl("361010SC_Card").get('361010SC_Card-000012') });/* 国际化处理： 权重不能为空，无法保存！*/
			return;
		}
		let bodyDatas = CardData2.bodys[tableId].rows;
		if (bodyDatas.length > 0) {
			for (var i = 0; i < bodyDatas.length; i++) {
				if (bodyDatas[i].values.netbankinftpcode) {
					let selectvalue = bodyDatas[i].values.netbankinftpcode.display;
					if (selectvalue == null) {
						let rownum = i + 1;
						toast({ color: 'warning', content: this.props.MutiInit.getIntl("361010SC_Card") && this.props.MutiInit.getIntl("361010SC_Card").get('361010SC_Card-000014') + rownum + this.props.MutiInit.getIntl("361010SC_Card") && this.props.MutiInit.getIntl("361010SC_Card").get('361010SC_Card-000015') });/* 国际化处理： 表体数据第,行银行接口类型为空!*/
						return;
					};
				}
			}
		}

		let url = '/nccloud/obm/ebankdpnetconfig/insert.do'; //新增保存
		if (this.props.getUrlParam('status') === 'edit') {
			url = '/nccloud/obm/ebankdpnetconfig/update.do'; //修改保存
		}
		ajax({
			url: url,
			data: CardData2,
			success: (res) => {
				let pk_billpoolconfig = null;
				if (res.success) {
					if (res.data) {
						if (res.data.head && res.data.head[formId]) {
							this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
							pk_billpoolconfig = res.data.head[formId].rows[0].values.pk_billpoolconfig.value;
						}
						if (res.data.bodys && res.data.bodys[tableId]) {
							this.converNetbankinftpcode(res.data.bodys);
							this.props.cardTable.setTableData(tableId, res.data.bodys[tableId]);
							this.props.cardTable.setTableData(tableId2, res.data.bodys[tableId2]);
						}
					}
				}
				if (url == '/nccloud/obm/ebankdpnetconfig/insert.do') {
					addCache(pk_billpoolconfig, res.data, formId, dataSource);
				}
				if (url == '/nccloud/obm/ebankdpnetconfig/update.do') {
					updateCache(pkname, pk_billpoolconfig, res.data, formId, dataSource);
				}
				this.updateTabData(pk_billpoolconfig);
				//切换为浏览态
				this.props.pushTo("/card", {
					status: 'browse',
					type: 'link',
					appcode: appcode,
					id: pk_billpoolconfig,
					pagecode: '361010SC_C01'
				});
				this.toggleShow();
				toast({ color: 'success', content: this.props.MutiInit.getIntl("361010SC_Card") && this.props.MutiInit.getIntl("361010SC_Card").get('361010SC_Card-000016') });/* 国际化处理： 保存成功!*/
			},
			error: (res) => {
				toast({ color: 'danger', content: res.message });
			}
		});
	};

	getButtonNames = (codeId) => {
	};


	//获取列表肩部信息
	getTableHead = (buttons, tableId) => {
		let { createButton } = this.props.button;
		return (
			<div className="shoulder-definition-area">
				<div className="definition-icons">
					{this.props.cardTable.createBrowseIcons(tableId, {
						iconArr: ['close', 'open', 'max'],
						maxDestAreaId: 'finance-fts-commissionpayment-card'
					})}
					{/* 应用注册按钮 */}
					{this.props.button.createButtonApp({
						area: 'card_body',
						buttonLimit: 3,
						onButtonClick: buttonClick.bind(this),
						popContainer: document.querySelector('.header-button-area')
					})}
				</div>
			</div>
		);
	};

	/**
     * tabs-table切换页签回调
     * @param {*} key    当前选中页签的code编码，其值等于this.props.cardTable.getCurTabKey()
     */
	tabsChange = key => {
		//页面切换  
	};

	updateTabData = (pk_billpoolconfig, cardData) => {
		if (!cardData) {
			if (!pk_billpoolconfig || pk_billpoolconfig == "") {
				pk_billpoolconfig = this.props.getUrlParam("id");
			}
			cardData = getCacheById(pk_billpoolconfig, dataSource);
		}
		let keys = tabs.tabShow;
		if (cardData) {
			if (cardData.bodys && JSON.stringify(cardData.bodys) !== "{}") {
				this.props.cardTable.setAllTabsData(
					cardData.bodys,
					tabs.tabOrder,
					null,
					Object.keys(cardData.bodys) == keys ? keys : keys.concat(Object.keys(cardData.bodys))
				);
			} else {
				this.props.cardTable.setAllTabsData(
					null,
					tabs.tabOrder,
					null,
					[]
				);
			}
		}

	}

	render() {
		let { cardTable, form, button, modal, cardPagination, ncmodal } = this.props;
		let buttons = this.props.button.getButtons();
		let { createForm } = form;
		let { createTabsTable, createCardTable } = cardTable;
		let { createButtonApp } = this.props.button;
		let { createButton, getButtons } = button;
		let { createCardPagination } = cardPagination;
		let { createModal } = ncmodal;
		const { createBillHeadInfo } = this.props.BillHeadInfo;
		let { showNCbackBtn } = this.state;
		const that = this;
		return (
			<div className="nc-bill-card">
				<div className="nc-bill-top-area">
					<NCAffix>
						<div className="nc-bill-header-area">
							<div className="header-title-search-area">
								{
									createBillHeadInfo(
										{
											title: this.props.MutiInit.getIntl("361010SC_Card") && this.props.MutiInit.getIntl("361010SC_Card").get('361010SC_Card-000019'),  //标题
											billCode: this.state.srvid,     //单据号
											initShowBackBtn: showNCbackBtn,
											backBtnClick: () => {           //返回按钮的点击事件
												this.link2ListPage();
											}
										}
									)}
							</div>
							<div className="header-button-area">
								{this.props.button.createButtonApp({
									area: 'card_head',
									buttonLimit: 4,
									onButtonClick: buttonClick.bind(this),
									popContainer: document.querySelector('.header-button-area')
								})}
							</div>
							<div className="header-cardPagination-area" style={{ float: 'right' }}>
								{createCardPagination({
									handlePageInfoChange: pageInfoClick.bind(this)
								})}
							</div>
						</div>
					</NCAffix>
					<NCScrollElement name="forminfo">
						<div className="nc-bill-form-area">
							{createForm(formId, {
								onAfterEvent: afterEvent.bind(this)
							})}
						</div>
					</NCScrollElement>
				</div>
				<div className="nc-bill-bottom-area">
					<NCScrollElement name="businfo">
						<div className="nc-bill-table-area">
							{createTabsTable(tableId, {
								tableHead: this.getTableHead.bind(this, buttons, tableId),
								modelSave: this.saveBill,
								onBeforeEvent: beforeEvent.bind(this),
								adaptionHeight: true,
								showCheck: true,
								// showIndex: true,
								onTabChange: this.tabsChange.bind(this)
							})}
						</div>
					</NCScrollElement>
				</div>

				{createModal(`delete`, { color: 'warning', hasCloseBtn: true })}
				{createModal(`confirm`, { color: 'warning', hasCloseBtn: true })}
			</div>
		);
	}
}

Card = createPage({
	mutiLangCode: '361010SC_Card',
	// initTemplate: initTemplate      
})(Card);

// ReactDOM.render(<Card />, document.querySelector('#app'));
export default Card;