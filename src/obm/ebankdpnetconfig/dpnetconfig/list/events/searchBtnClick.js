import {ajax,toast} from 'nc-lightapp-front';
let tableid = 'ebank_dpnetconfig';
//点击查询，获取查询区数据
export default function clickSearchBtn(props,searchVal) {
    console.log(searchVal);
    let pageInfo = props.table.getTablePageInfo(this.tableid);
    let metaData = props.meta.getMeta();
    let data={
        conditions:searchVal.conditions || searchVal,
        pageInfo:pageInfo,
        pagecode: "361010SC_L01",
        queryAreaCode:"361010SC",
        // oid:"1001Z61000000000IZN9",
        queryType:"simple"
     };
    console.log(data);
    ajax({
        url: '/nccloud/obm/ebankdpnetconfig/query.do',//'/nccloud/reva/search/query.do',
        data: data,
        success:  (res)=> {
            let { success, data } = res;
            if(success){
                if(data && data[tableid]){
                    props.table.setAllTableData(tableid, res.data[tableid]);
                }else{
                    props.table.setAllTableData(tableid, {rows:[]});
                    toast({content:props.MutiInit.getIntl("361010SC_List") && props.MutiInit.getIntl("361010SC_List").get('361010SC_List-000003'),color:"warning"});/* 国际化处理： 无数据*/
                }	
            }
        },
        error : (res)=>{
			console.log(res.message);
		}
    });

};
