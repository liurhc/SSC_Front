﻿import { ajax, base } from 'nc-lightapp-front';
import buttonClick from './buttonClick';
// let { NCTooltip:Tooltip} = base;
import { tableId, pagecode,appid,appcode} from "../constants"; 
import tableButtonClick from "./tableButtonClick"; 

export default function (props) {
	let that =this;
	//请求模板数据
	props.createUIDom(
		{
			pagecode: pagecode,//页面id
			appcode:appcode
			// appid: appid//注册按钮的id
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta,that)
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					props.button.setButtonDisabled(['Delete', 'ConnectTest','Edit'], true);
				}
			}
		}
	)
}



//刷新页面数据方法
function refreshFun(props) {

}

function seperateDate(date){
	if (typeof date !=='string') return ;
	let result = date.split(' ') && date.split(' ')[0];
	return result;
}

function modifierMeta(props, meta,that) {

	meta[tableId].items = meta[tableId].items.map((item, key) => {
		item.width = 150;
		if (item.attrcode == 'srvid') {
			item.render = (text, record, index) => {
				// let tip = (<div>{record.srvid.value}</div>)
				return (
					// <Tooltip trigger="hover" placement="top" inverse={false} overlay={tip}>
					<a
						style={{ cursor: 'pointer' }}
						onClick={() => {
							props.pushTo("/card", { 
								status:'browse',
								type:'link',
								appcode:appcode,
								id:record.pk_billpoolconfig.value,
								pagecode:'361010SC_C01'
							})
						}}
					>
						{
							record.srvid ? record.srvid.value : ''
						}
					</a>
					// </Tooltip>
				);
			};
		}
		return item;
	});
	//添加操作列
	meta[tableId].items.push({
		attrcode: 'opr',
		label: props.MutiInit.getIntl("361010SC_List") && props.MutiInit.getIntl("361010SC_List").get('361010SC_List-000002'),//操作/* 国际化处理： 操作*/
		width: 200,
		fixed: 'right',
		className: "table-opr",
		itemtype:"customer",
		visible: true,
		render: (text, record, index) => {
			let buttonArr =  ["Editin"];
			return props.button.createOprationButton(buttonArr, {
				area: "list_inner",
				buttonLimit: 1,
				
				onButtonClick: (props, key) => tableButtonClick.call(this,props, key, text, record, index)
			});
		}
	});
	return meta;
}
