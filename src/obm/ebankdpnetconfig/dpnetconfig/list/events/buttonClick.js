﻿import { ajax,base,toast,promptBox } from 'nc-lightapp-front';
import { tableId, pagecode,appcode  } from "../constants";
// let tableid = 'ebank_srvconf_b';

export default function buttonClick(props, id) {
	console.log(id)
	let selectDatas = null; 			
	let pksel = null;
	switch (id) {	
		case 'Add':
			props.pushTo("/card", {
				status: 'add',
				type:'link',
				appcode:appcode,
				from: 'list',
				pagecode:'361010SC_L01'
			})
			break;
		case 'Edit':
			let selectDatas = props.table.getCheckedRows(tableId);
			let pk_billpoolconfig = selectDatas[0].data.values.pk_billpoolconfig.value;
			props.pushTo("/card", { 
				status:'edit',
				type:'link',
				appcode:appcode,
				id:pk_billpoolconfig,
				// id:pksel,
				pagecode:'361010SC_C01'
			})
			break;
		case 'Delete':
			// selectDatas = props.table.getCheckedRows(tableId);
			// if(selectDatas.length==1){
			// 	props.ncmodal.show(`delete`, {
			// 		title: props.MutiInit.getIntl("361010SC_List") && props.MutiInit.getIntl("361010SC_List").get('361010SC_List-000004'),/* 国际化处理： 删除*/
			// 		content:props.MutiInit.getIntl("361010SC_List") && props.MutiInit.getIntl("361010SC_List").get('361010SC_List-000005'),/* 国际化处理： 确定要删除吗？*/
			// 		beSureBtnClick: () => { 
			// 			this.deleteAction();
			// 		}
			// 	});
			// }else{
			// 	props.ncmodal.show(`batchDel`, {
			// 		title: props.MutiInit.getIntl("361010SC_List") && props.MutiInit.getIntl("361010SC_List").get('361010SC_List-000004'),/* 国际化处理： 删除*/
			// 		content:props.MutiInit.getIntl("361010SC_List") && props.MutiInit.getIntl("361010SC_List").get('361010SC_List-000006'),/* 国际化处理： 确定要删除所选数据吗？*/
			// 		beSureBtnClick: () => { 
			// 			this.deleteAction();
			// 		}
			// 	});
			// }
			promptBox({
				color: "warning",
				title: props.MutiInit.getIntl("361010SC_List") && props.MutiInit.getIntl("361010SC_List").get('361010SC_List-000004'),/* 国际化处理： 删除*/
				content: props.MutiInit.getIntl("361010SC_List") && props.MutiInit.getIntl("361010SC_List").get('361010SC_List-000006'),/* 国际化处理： 确定要删除所选数据吗？*/
				beSureBtnClick: () => { this.deleteAction()}
			});	  
			
			break; 
			
		case "ConnectTest"://测试连接
			selectDatas = props.table.getCheckedRows(tableId);
			//判断是否有选中行
			if (selectDatas == null || selectDatas.length == 0) {
				toast({ color: 'warning', content: props.MutiInit.getIntl("361010SC_List") && props.MutiInit.getIntl("361010SC_List").get('361010SC_List-000000') });/* 国际化处理： 未选中行！*/
				return;
			}
			else if (selectDatas.length > 1) {
				toast({ color: 'warning', content: props.MutiInit.getIntl("361010SC_List") && props.MutiInit.getIntl("361010SC_List").get('361010SC_List-000001') });/* 国际化处理： 一次只能测试一条！*/
				return;
			}
			pksel = selectDatas[0] && selectDatas[0].data && selectDatas[0].data.values && selectDatas[0].data.values.pk_billpoolconfig && selectDatas[0].data.values.pk_billpoolconfig.value;
			this.ConnectTestAction(props,pksel);
			break;
		case "Refresh"://刷新
			this.refreshAction(props);
			break;
		default:
			break
	}
}
