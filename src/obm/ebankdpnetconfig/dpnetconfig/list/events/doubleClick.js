import { ajax, toast ,cacheTools} from 'nc-lightapp-front';
import { tableId, pagecode ,appcode } from "../constants";

export default function doubleClick(record, index, e) {
    this.props.pushTo("/card", {
        status: 'browse',
        type:'link',
		appcode:appcode,
        id: record.pk_billpoolconfig.value,
        pagecode:'361010SC_L01'
    });
}
