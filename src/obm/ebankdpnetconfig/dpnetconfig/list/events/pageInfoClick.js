import { ajax } from 'nc-lightapp-front';
let tableid = 'pk_billpoolconfig_bbank';

export default function (props, config, pks) {
	let data = {
		allpks: pks,
		pageid: "361010SC_L01"
	};
	//得到数据渲染到页面
	let that = this;
	ajax({
		url: '/nccloud/obm/ebankdpnetconfig/querypage.do',
		data: data,
		success: function (res) {
			let { success, data } = res;
			if (success) {
				if (data) {
					props.table.setAllTableData(tableid, data[tableid]);
				} else {
					props.table.setAllTableData(tableid, { rows: [] });
				}
			}
		}
	});
}
