/**
 * 表体区域
 */
export const tableId = 'ebank_dpnetconfig';  

/**
 * 查询区域编码
 */
// export const searchId = 'search';
 
/**
 * 页面编码
 */
export const pagecode = '361010SC_L01';
export const appcode = '361010SC';
export const appid = '1001Z61000000001PA57';

/**
 * 编辑态按钮
 */
export const editButtons = ['Save', 'Cancel'];

/**
 * 浏览态按钮
 */
export const browseButtons = ['Add','Delete','ConnectTest','Refresh','Edit'];