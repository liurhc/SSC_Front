
import { app_code } from '../../cons/constant.js';

export default function (props, moduleId, key, changerows, value, data, index) {
    // if (key === 'pk_org'&&props.table.getTableValueBykey(moduleId, 'pk_org')) {
    //     props.table.setTableValueBykey(moduleId, 'nsinglemny', '222');
    // }

    let multiLang = props.MutiInit.getIntl(app_code);


    const { form } = this.props;

    if (moduleId === 'ebank_onlinequery') {
        if (key === 'banktype') {
            //银行类别切换 银行账号清空
            form.setFormItemsValue(moduleId, { 'bankacc': { value: '', display: '' } });


        } else if (key === 'bankaccorg') {
            const bankaccorg = form.getFormItemsValue(moduleId, 'bankaccorg').value;
            if (bankaccorg) {

                //财务组织切换 银行类别 银行账号清空
                if (form.getFormItemsValue(moduleId, 'banktype').value) {
                    form.setFormItemsValue(moduleId, { 'banktype': { value: '', display: '' } });
                }

                if (form.getFormItemsValue(moduleId, 'bankacc').value) {
                    form.setFormItemsValue(moduleId, { 'bankacc': { value: '', display: '' } });
                }
            }
        } else if (key === 'bankacc') {
            //选择银行账号 
            let pk_banktype = '';
            let banktypename = '';
            //TODO 此处有bug 选择多个银行账户 之后删除一个后 获取不到参照的其它信息
            if (data.length != 0) {
                pk_banktype = data[0].values['bd_banktype.pk_banktype'].value;
                banktypename = data[0].values['bd_banktype.name'].value;
            }

            if (data.length == 0) {
                pk_banktype = '';
                banktypename = '';
            }
            form.setFormItemsValue(moduleId, { 'banktype': { value: pk_banktype, display: banktypename } });
        }
    }else if(moduleId === 'ebank_correctbalance'){
        //TODO
    }else if(moduleId === 'ebank_fileinput'){
    
        if (key === 'banktype') {

            if((data.key)&&(data.key === '0')){
                form.setFormItemsValue(moduleId, { 'msg': { value: multiLang && multiLang.get('36100BQ-000000'), display: multiLang && multiLang.get('36100BQ-000000') } });/* 国际化处理： 请选择由NC余额导入Excel模板: ..\resources\ebank\system\NC_Ye_Templet.xls生成的xml数据文件,请选择由NC余额导入Excel模板: ..\resources\ebank\system\NC_Ye_Templet.xls生成的xml数据文件*/
            }

            if((data.key)&&(data.key !== '0')){
                form.setFormItemsValue(moduleId, { 'msg': { value: `${multiLang && multiLang.get('36100BQ-000001')}${data.label}${multiLang && multiLang.get('36100BQ-000002')}.`, display: `${multiLang && multiLang.get('36100BQ-000001')}${data.label}${multiLang && multiLang.get('36100BQ-000002')}.` } });/* 国际化处理： 请选择由,xxx提供的格式文件*/
            }
           
        }
    }


}
