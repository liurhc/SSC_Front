﻿import { ajax, base,getBusinessInfo,cacheTools } from 'nc-lightapp-front';
//引入常量定义
import { app_code, base_url,list_page_id,list_search_id,list_table_id,cache_key,fun_code } from '../../cons/constant.js';

let { NCPopconfirm, NCIcon, NCMessage } = base;
let searchId = list_search_id;
let tableId = list_table_id;
let pageId = list_page_id;
let appcode = app_code;

export default function (props) {
	//请求模板数据
	props.createUIDom(
		{
			pagecode: pageId,//页面id
			appcode: appcode//注册按钮编码
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					//TODO 新盘去掉
					//meta.formrelation={'ebank_onlinequery':[]};
					meta['ebank_onlinequery'].status = 'edit';
					meta['ebank_fileinput'].status = 'edit';
					meta['ebank_correctbalance'].status = 'edit';
					meta = modifierMeta(props, meta);
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					let queryData = cacheTools.get(cache_key);
					if(queryData){
						props.button.setButtonDisabled({
							refresh: false
						});
					}else{
						props.button.setButtonDisabled({
							refresh: true
						});
					}
					
				}
			}
		}
	)
}

function modifierMeta(props, meta) {

	const { search, form } = props;
	let multiLang = props.MutiInit.getIntl(app_code);
	let businessInfo = getBusinessInfo();
	let { groupId } = businessInfo
	
	//设置银行账号多选
	meta['search'].items.find((e) => e.attrcode === 'bankacc').isMultiSelectedEnabled = true;
	meta['ebank_onlinequery'].items.find((e) => e.attrcode === 'bankacc').isMultiSelectedEnabled = true;

	// meta[searchId].items.map((item) => {
	// 		if (item.attrcode == 'pk_acc_sub') {
	// 		item.queryCondition = () => {
	// 			let pk_org = props.search.getSearchValByField('search', 'pk_org').value.firstvalue;
	// 			return {
	// 				pk_org: pk_org,
	// 				ExtRefSqlBuilder: 'nccloud.web.cmp.ref.CMPBankaccSubDefaultBuilder'
	// 			};
	// 		};
	// 	}
	// });

	//查询界面字段增加过滤条件
	meta["search"].items.map((item) => {
		if (item.attrcode == 'bankaccorg') {
			item.queryCondition = () => {
				return {
					funcode: fun_code,
					// TreeRefActionExt: 'nccloud.web.obm.refbuilder.FinanceOrgRefBuilder'
					TreeRefActionExt:'nccloud.web.tmpub.filter.FinanceOrgPermissionFilter',

				};
			};
		} else if (item.attrcode == 'bankacc') {
			item.queryCondition = () => {

				let pk_org = null;
				let pk_banktype = null;
				if (search.getSearchValByField('search', 'bankaccorg')) {
					pk_org = search.getSearchValByField('search', 'bankaccorg').value.firstvalue;
				}

				if (search.getSearchValByField('search', 'banktype')) {
					pk_banktype = search.getSearchValByField('search', 'banktype').value.firstvalue;
				}

				return {
					refnodename: multiLang && multiLang.get('36100BQ-000004'),/* 国际化处理： 核算归属权*/
					funcode: fun_code,
					pk_org: pk_org,
					pk_banktype: pk_banktype,
					permissonOrgs: true,
					GridRefActionExt: 'nccloud.web.obm.refbuilder.BankAccRefBuilder'
				};
			};

		}
	});


	//在线下载界面 字段增加过滤条件
	//TODO 使用权参照存在bug 财务组织不选时 查询不到数据
	meta["ebank_onlinequery"].items.map((item) => {
		if (item.attrcode == 'bankaccorg') {
			item.queryCondition = () => {
				return {
					funcode: fun_code,
					// TreeRefActionExt: 'nccloud.web.obm.refbuilder.FinanceOrgRefBuilder'
					TreeRefActionExt:'nccloud.web.tmpub.filter.FinanceOrgPermissionFilter'
				};
			};
		} else if (item.attrcode == 'bankacc') {
			item.queryCondition = () => {

				let pk_org = form.getFormItemsValue('ebank_onlinequery', 'bankaccorg').value;
				let pk_banktype = form.getFormItemsValue('ebank_onlinequery', 'banktype').value;

				if(!pk_org){
					pk_org = groupId;
				}

				if(!pk_banktype){
					pk_banktype = '';
				}

				return {
					refnodename: multiLang && multiLang.get('36100BQ-000005'),/* 国际化处理： 使用权参照*/ 
					funcode: fun_code,
					pk_org: pk_org,
					pk_banktype: pk_banktype,
					disable:2,
					GridRefActionExt: 'nccloud.web.obm.refbuilder.BankAccRefBuilder'
				};
			};

		}
	});

	//调整余额
	meta["ebank_correctbalance"].items.map((item) => {
		if (item.attrcode == 'bankaccorg') {
			item.queryCondition = () => {
				return {
					funcode: fun_code,
					TreeRefActionExt:'nccloud.web.tmpub.filter.FinanceOrgPermissionFilter'
				};
			};
		} else if (item.attrcode == 'bankacc') {
			item.queryCondition = () => {

				let pk_org = form.getFormItemsValue('ebank_correctbalance', 'bankaccorg').value;			
				return {
					refnodename: multiLang && multiLang.get('36100BQ-000004'),/* 国际化处理： 核算归属权*/
					funcode: fun_code,
					pk_org: pk_org,
					permissonOrgs: false,
					GridRefActionExt: 'nccloud.web.obm.refbuilder.BankAccRefBuilder'
				};
			};


		}
	});

	//赋值下拉框option
	meta["ebank_fileinput"].items.map((item) => {
		if(item.attrcode == 'banktype'){
			let options = [];
			options[0] = {
				"display": multiLang && multiLang.get('36100BQ-000019'),/* 国际化处理： NC格式*/
				"value": "0"
			};
			options[1] = {
				"display": multiLang && multiLang.get('36100BQ-000020'),/* 国际化处理： 日本瑞穗*/
				"value": "rbrs"
			};
			options[2] = {
				"display": multiLang && multiLang.get('36100BQ-000021'),/* 国际化处理： 日本三菱银行*/
				"value": "btmu"
			};
			options[3] = {
				"display": multiLang && multiLang.get('36100BQ-000022'),/* 国际化处理： 花旗银行*/
				"value": "ctd"
			};
			item.options = options;
		}
	});
	return meta;
}


//刷新页面数据方法
function refreshFun(props) {
	// let searchVal = props.search.getAllSearchData(searchid);
	// let data = {
	// 	conditions: searchVal,
	// 	pageInfo: {
	// 		currentPageIndex: 0,
	// 		pageSize: 10,
	// 		total: 0,
	// 		pageCount: 0
	// 	}
	// };

	// ajax({
	// 	url: '/nccloud/obm/ebankdzd/refresh.do',
	// 	data: data,
	// 	success: function(res) {
	// 		props.table.setAllTableData(tableid, res.data['0001']);
	// 	}
	// });
}
