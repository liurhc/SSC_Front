import { ajax, toast, cacheTools } from 'nc-lightapp-front';
//引入常量定义
import { list_page_id, list_search_id, oid, cache_key,app_code } from '../../cons/constant.js';
//点击查询，获取查询区数据
export default function clickSearchBtn(props, searchVal) {
    let multiLang = props.MutiInit.getIntl(app_code);
    let that = this;
    if (searchVal) {
        let pageInfo = props.table.getTablePageInfo(this.tableId);
        let data = {
            conditions: searchVal.conditions || searchVal,
            pageInfo: pageInfo,
            pagecode: list_page_id,
            queryAreaCode: list_search_id,  //查询区编码
            oid: oid,  //查询模板id，手工添加在界面模板json中，放在查询区，后期会修改
            queryType: 'simple'
        };

        //暂存查询条件
        cacheTools.set(cache_key, data);
        if (data) {
            props.button.setButtonDisabled({
                refresh: false
            });
        } else {
            props.button.setButtonDisabled({
                refresh: true
            });
        }

        ajax({
            url: '/nccloud/obm/ebankbalanceqry/query.do',
            data: data,
            success: (res) => {
                let { success, data } = res;
                if (success) {
                    if (data) {
                        giveToast(props, data[that.tableId].allpks.length)
                        props.table.setAllTableData(that.tableId, data[that.tableId]);
                    } else {
                        props.table.setAllTableData(that.tableId, { rows: [] });
                        giveToast(props, );
                    }

                }
            }
        });
    }

};

function giveToast(props, resLength) {
    let multiLang = props.MutiInit.getIntl(app_code);
    
    if (resLength && resLength > 0) {
        let contentHead = multiLang && multiLang.get('36100BQ-000017'); /* 国际化处理： 查询成功，共 */
        let contentEnd = multiLang && multiLang.get('36100BQ-000018'); /* 国际化处理： 条。*/
        toast({
            duration: 6,
            color: 'success',
            content: contentHead + resLength + contentEnd
        })
    }
    else {
        toast({
            duration: 6,
            color: 'warning',
            content: multiLang && multiLang.get('36100BQ-000006') /* 国际化处理： 没有符合条件的记录。*/
        })
    }
}