export default function (field, val) {

    const { search } = this.props;

    if (field === 'banktype') {
        //银行类别切换 银行账号清空
        search.setSearchValByField('search', 'bankacc', {
            value: '',
            display: ''
        })

    } else if (field === 'bankaccorg') {

        if (val.refpk) {
            //财务组织切换 银行类别 银行账号清空
            if (search.getSearchValByField('search', 'banktype')) {
                search.setSearchValByField('search', 'banktype', {
                    value: '',
                    display: ''
                })
            }
            if (search.getSearchValByField('search', 'bankacc')) {
                search.setSearchValByField('search', 'bankacc', {
                    value: '',
                    display: ''
                })
            }
        }
    } else if (field === 'bankacc') {
        //选择银行账号 
        let pk_banktype = '';
        let banktypename = '';
        if (val.length > 0) {
            pk_banktype = val[0].values.bd_banktypeid.value;
            banktypename = val[0].values.pk_banktype.value;
        }

        //TODO 此处有bug 设置银行类别会报错
        search.setSearchValByField('search', 'banktype', {
            value: pk_banktype,
            display: banktypename
        })

    }
}
