import {ajax,toast} from 'nc-lightapp-front';
import { app_code ,list_page_id} from '../../cons/constant.js';
export default function (props, config, pks) {
    //let pageInfo = props.table.getTablePageInfo(this.tableId);
    //let searchVal = props.search.getAllSearchData('search');
    let data = {
        "pks": pks,
        "pageid": list_page_id
    };
    ajax({
        url: '/nccloud/obm/ebankbalanceqry/pagequery.do',
        data: data,
        async: true,
        success: (res) => {
            let { success, data } = res;

            let multiLang = props.MutiInit.getIntl(app_code);
            
            if (success) {
                if (data) {
                    this.props.table.setAllTableData(this.tableId, data[this.tableId]);
                } else {
                    this.props.table.setAllTableData(this.tableId, { rows: [] });
                    //toast({ content: multiLang && multiLang.get('36100BQ-000006'), color: "warning" });/* 国际化处理： 没有符合条件的记录。*/
                }
            }
        }
    });
}