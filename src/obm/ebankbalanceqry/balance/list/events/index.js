import searchBtnClick from './searchBtnClick';
import pageInfoClick from './pageInfoClick';
import buttonClick from './buttonClick';
import initTemplate from './initTemplate';
import afterEvent from './afterEvent';
import tableModelConfirm from './tableModelConfirm';
import onAfterEvent from './onAfterEvent';
export { searchBtnClick,pageInfoClick,buttonClick, initTemplate, afterEvent, onAfterEvent,tableModelConfirm };
