﻿import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast, getMultiLang,createPageIcon } from 'nc-lightapp-front';
import { searchBtnClick, pageInfoClick, buttonClick, initTemplate, afterEvent, onAfterEvent, tableModelConfirm } from './events';
import './index.less';
import moment from 'moment';
//引入常量定义
import { base_url, module_id, app_code, list_table_id, onlinequery_form_id, fileinput_form_id, correctbalance_form_id, button_limit, list_search_id, oid } from '../cons/constant.js';

const { NCUpload, NCModal, NCForm, NCFormControl, NCButton } = base;
const NCFormItem = NCForm.NCFormItem;
const formId = onlinequery_form_id
const onlineQueryFormId = onlinequery_form_id;
const fileInputId = fileinput_form_id;
const correctBalanceFormId = correctbalance_form_id;

class SingleTable extends Component {
	constructor(props) {
		super(props);
		this.moduleId = app_code;
		this.searchId = list_search_id;
		this.tableId = list_table_id;
		this.open_upload = this.open_upload.bind(this);
		this.cancel_upload = this.cancel_upload.bind(this);
		this.beforeUpload = this.beforeUpload.bind(this);
		this.afterUpload = this.afterUpload.bind(this);
		this.upload_change = this.upload_change.bind(this);
		this.isCheckPass = false;
		this.state = {
			showModal_upload: false,
			showModal_onlinequery: false,
			showModal_correctbalance: false,
			json: {},
			importProps: {
				name: 'file',
				action: '/nccloud/obm/ebankbalanceqry/fileinput.do',
				accept: 'text/xml, application/xml',
				data: {},
				multiple: false,
				// beforeUpload: this.beforeUpload,
				headers: {
					authorization: 'authorization-text',
				},
				onChange: this.upload_change,
			}
		};

		// this.importProps = {
		// 	name: 'file',
		// 	action: '/nccloud/obm/ebankbalanceqry/fileinput.do',
		// 	accept: 'text/xml, application/xml',
		// 	data: {},
		// 	multiple: false,
		// 	beforeUpload: this.beforeUpload,
		// 	headers: {
		// 		authorization: 'authorization-text',
		// 	},
		// 	onChange: this.upload_change,
		// };
	}

	//文件上传状态改变处理
	upload_change(info) {

		if (info.file.status !== 'uploading') {
			console.log(info.file, info.fileList);
		}
		if (info.file.status === 'done') {

			if (info.file.response && info.file.response.success) {

				// let { data } = info.file.response;
				// if (data) {
				// 	this.props.table.setAllTableData(this.tableId, data[this.tableId]);
				// } else {
				// 	this.props.table.setAllTableData(this.tableId, { rows: [] });
				// 	toast({ content: "没有导入的余额信息。", color: "warning" });
				// }
				let { data } = info.file.response;
				let filename = { value: data, display: data };
				this.props.form.setFormItemsValue(fileInputId, { 'filename': filename });

			} else {
				toast({ content: info.file.response.error.message, color: "warning" });
			}
			info.fileList.splice(0, info.fileList.length);
			// if (this.isCheckPass) {
			// 	this.cancel_upload();
			// }
		} else if (info.file.status === 'error') {
			info.fileList.splice(0, info.fileList.length);
		}

	}

	getButtonNames = (codeId) => {
		return 'main-button';
	};

	componentWillMount() {
		// let callback = (json) => {
		// 	this.setState({ json });
		// 	initTemplate.call(this,this.props);
		// };
		// getMultiLang({ moduleId: app_code, domainName: 'obm', callback });
	}



	componentDidMount() {
		this.setDefaultValue();
	}

	componentDidUpdate() {
		this.setDefaultValue();
	}

	setDefaultValue = () => {
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		//设置开始日期与结束日期
		const begindate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
		const enddate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
		this.props.form.setFormItemsValue(onlineQueryFormId, { 'begindate': { value: begindate, display: begindate }, 'enddate': { value: enddate, display: enddate } });
		this.props.form.setFormItemsValue(fileInputId, { 'msg': { value: multiLang && multiLang.get('36100BQ-000000'), display: multiLang && multiLang.get('36100BQ-000000') }, 'banktype': { value: '0', display: multiLang && multiLang.get('36100BQ-000007') } });/* 国际化处理： 请选择由NC余额导入Excel模板: ..\resources\ebank\system\NC_Ye_Templet.xls生成的xml数据文件,请选择由NC余额导入Excel模板: ..\\resources\\ebank\\system\\NC_Ye_Templet.xls生成的xml数据文件,NC格式*/
	}

	//点击关闭按钮，调用父页面的回调函数，关闭页面
	cancel_upload = () => {
		this.setState({ showModal_upload: false });
	}

	//点击上传
	upload = () => {
		//this.setState({ showModal_upload: false });
	}



	//上传这后关闭弹出框
	afterUpload = () => {
		// if (this.isCheckPass) {
		// 	this.cancel_upload();
		// }	
	}

	//文件上传前操作
	beforeUpload = () => {
		let { form } = this.props;
		let banktype = form.getFormItemsValue(fileInputId, 'banktype').value;
		this.state.importProps.data = { banktype: banktype };
		this.setState(this.state);
		//数据校验
		// if (form.isCheckNow(fileInputId)) {
		// 	this.importProps.data = {
		// 		"banktype": form.getFormItemsValue(fileInputId, 'banktype').value
		// 	}
		// 	this.isCheckPass = true;
		// } else {
		// 	this.isCheckPass = false;
		// 	return false;
		// }
	}

	//打开上传框
	open_upload() {
		this.setState({ showModal_upload: true });
	}

	//点击确认按钮导入文件
	fileImport = () => {
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		let { form } = this.props;
		let banktype = form.getFormItemsValue(fileInputId, 'banktype').value;
		let filename = form.getFormItemsValue(fileInputId, 'filename').value;

		if (!filename) {
			toast({ content: multiLang && multiLang.get('36100BQ-000008'), color: "warning" });/* 国际化处理： 请选择文件。*/
			return;
		}

		let data = {
			banktype: banktype,
			filename: filename
		};
		ajax({
			url: '/nccloud/obm/ebankbalanceqry/confirm.do',
			data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if (data) {
						toast({ content: multiLang && multiLang.get('36100BQ-000016'), color: "success" });/* 国际化处理： 导入成功。*/

						this.props.table.setAllTableData(this.tableId, data[this.tableId]);
					} else {
						this.props.table.setAllTableData(this.tableId, { rows: [] });
					}

					this.setState({ showModal_upload: false });
				}
			}
		});
	}


	render() {
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		let { table, button, modal, search, form } = this.props;
		let { createSimpleTable } = table;
		let { createButton } = button;
		let buttons = this.props.button.getButtons();
		let { NCCreateSearch } = search;
		let { createModal } = modal;
		let { createButtonApp } = button;
		let { createForm } = this.props.form;
		let Uploader = this.props.Uploader;

		return (
			<div className="nc-bill-list">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						{/*页面大图标*/}
						{createPageIcon()}
						<h2 className="title-search-detail">
							{multiLang && multiLang.get('36100BQ-000011')}</h2>{/* 国际化处理： 账户余额在线查询*/}
					</div>
					<div className="header-button-area">
						{this.props.button.createButtonApp({
							area: 'page_header',
							buttonLimit: 4,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				<div className="nc-bill-search-area">
					{NCCreateSearch(this.searchId, {
						oid: oid,
						showAdvBtn: false,
						clickSearchBtn: searchBtnClick.bind(this),
						onAfterEvent: onAfterEvent.bind(this),
						defaultConditionsNum: 4 //默认显示几个查询条件
					})}
				</div>
				{/* <div style={{ height: '10px' }}></div> */}
				<div className="nc-bill-table-area" style={{ borderRadius: "3px 3px 0 0", overflow: "hidden" }}>
					{createSimpleTable(this.tableId, {//列表区
						onAfterEvent: afterEvent,
						handlePageInfoChange: pageInfoClick.bind(this),
						tableModelConfirm: tableModelConfirm,
						showCheck: true,
						showIndex: true
					})}
				</div>
				{createModal(onlineQueryFormId, {//在线下载区
					title: multiLang && multiLang.get('36100BQ-000009'),/* 国际化处理： 在线下载*/
					className: 'senior',
					content:
						// <OnlineQueryForm />
						<div className="form-wrapper">
							{createForm(onlineQueryFormId, {
								//编辑后事件
								onAfterEvent: afterEvent.bind(this)
							})}
						</div>
					,
					beSureBtnClick: () => {
						//数据校验
						const queryData = form.getAllFormValue(onlineQueryFormId);

						if (form.isCheckNow(onlineQueryFormId)) {

							let data = {
								model: {
									pageInfo: {
										pageIndex: 0,
										pageSize: 10,
										total: 0,
										totalPage: 0
									},
									...queryData
								},
							};

							ajax({
								url: '/nccloud/obm/ebankbalanceqry/onlinequery.do',
								data: data,
								success: (res) => {
									let { success, data } = res;
									if (success) {
										if (data) {
											table.setAllTableData(this.tableId, data[this.tableId]);
										} else {
											table.setAllTableData(this.tableId, { rows: [] });
											toast({ content: multiLang && multiLang.get('36100BQ-000006'), color: "warning" });/* 国际化处理： 没有符合条件的记录。*/
										}
									}
								}
							});
						} else {
							// modal.show('onlinequery');
							//form.setAllFormValue(queryData);
						}
					}
					,
					cancelBtnClick: () => {
						//props.form.setFormItemsValue(this.formId,{enablestate:true});
						//return;
					}
				})}

				{createModal(correctBalanceFormId, {//调整余额区
					title: multiLang && multiLang.get('36100BQ-000010'),/* 国际化处理： 调整余额*/
					className: 'senior',
					content:
						<div className="form-wrapper">
							{createForm(correctBalanceFormId, {
								//编辑后事件
								onAfterEvent: afterEvent.bind(this)
							})}
						</div>
					,
					beSureBtnClick: () => {
						//数据校验
						const queryData = form.getAllFormValue(correctBalanceFormId);

						if (form.isCheckNow(correctBalanceFormId)) {

							let data = {
								model: {
									pageInfo: {
										pageIndex: 0,
										pageSize: 10,
										total: 0,
										totalPage: 0
									},
									...queryData
								},
							};

							ajax({
								url: '/nccloud/obm/ebankbalanceqry/correctbalance.do',
								data: data,
								success: (res) => {
									let { success, data } = res;
									if (success) {
										if (data) {
											toast({ content: data, color: 'success' });
										}
										// if (data) {
										// 	table.setAllTableData(this.tableId, data[this.tableId]);
										// } else {
										// 	table.setAllTableData(this.tableId, { rows: [] });
										// 	toast({ content: "没有符合条件的记录。", color: "warning" });
										// }
									}
								}
								// error: (res) => {
								// 	let { data } = res;
								// 	toast({ content: res.data.error.message, color: "warning" });
								// }
							});
						} else {
							// modal.show('onlinequery');
							//form.setAllFormValue(queryData);
						}
					}
					,
					cancelBtnClick: () => {
						//props.form.setFormItemsValue(this.formId,{enablestate:true});
						//return;
					}
				})}


				<div className="form-wrapper">

					<NCModal show={this.state.showModal_upload} className="senior" onHide={this.cancel_upload.bind(this)}
					>
						<NCModal.Header closeButton={true}>
							<NCModal.Title>{multiLang && multiLang.get('36100BQ-000012')}</NCModal.Title>{/* 国际化处理： 导入文件*/}
						</NCModal.Header>

						<NCModal.Body>
							{createForm(fileInputId, {
								//编辑后事件
								onAfterEvent: afterEvent.bind(this)
							})}
						</NCModal.Body>

						<NCModal.Footer>
						    <NCButton className="button-primary" style={{ float: 'right' }} onClick={this.fileImport.bind(this)}>{multiLang && multiLang.get('36100BQ-000013')}</NCButton>{/* 国际化处理： 确定*/}
							<NCButton className="button-undefined u-button-border" style={{ float: 'right' }} onClick={this.cancel_upload.bind(this)}>{multiLang && multiLang.get('36100BQ-000014')}</NCButton>{/* 国际化处理： 取消*/}
							<NCUpload {...this.state.importProps}>
								<NCButton className="button-undefined u-button-border">{multiLang && multiLang.get('36100BQ-000015')}</NCButton>{/* 国际化处理： 文件*/}
							</NCUpload>
						</NCModal.Footer>

					</NCModal>

				</div>

			</div>
		);
	}
}

SingleTable = createPage({
	mutiLangCode: app_code,
	initTemplate: initTemplate
})(SingleTable);

ReactDOM.render(<SingleTable />, document.querySelector('#app'));
