/**
 * 公共配置
 */
//应用id
export const app_id = '0001Z6100000000324K1';
//模块编码
export const module_id = '3610';
//应用编码
export const app_code = '36100BQ';
//功能节点编码
export const fun_code = '36100BQ';
//实体id
export const oid = '1001Z6OBM00000000NYK';
//请求后端基础地址
export const base_url = '/nccloud/obm/ebankbalanceqry/';
//按钮平铺显示数量
export const button_limit = 2;
/**
 * 列表
 */
//页面编码
export const list_page_id = '36100BQ_L01';
//查询区域编码
export const list_search_id = 'search';
//表格区域编码
export const list_table_id = 'ebank_balance';

/**
 * 卡片
 */
//在线查询页面编码
export const onlinequery_form_id = 'ebank_onlinequery';
//文件导入页面编码
export const fileinput_form_id = 'ebank_fileinput';
//调整余额页面编码
export const correctbalance_form_id = 'ebank_correctbalance';

//缓存
export const cache_key = '36100BQ_L01_search';





