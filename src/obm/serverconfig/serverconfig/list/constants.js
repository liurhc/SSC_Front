/**
 * 表体区域
 */
export const tableId = 'ebank_srvconf_b'; 

/**
 * 查询区域编码
 */
// export const searchId = 'search';
 
/**
 * 页面编码
 */
export const pagecode = '36100SC_L01';
export const appcode = '36100SC';
export const appid = '0001Z61000000004PCHT';

/**
 * 编辑态按钮
 */
export const editButtons = ['Save', 'Cancel'];

/**
 * 浏览态按钮
 */
export const browseButtons = ['Add','Delete','ConnectTest','Refresh','Edit'];