﻿import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base,toast,cacheTools,cardCache,createPageIcon } from 'nc-lightapp-front';
let { Message, NCDatePicker } = base;
const { NCBreadcrumb } = base;
const NCBreadcrumbItem = NCBreadcrumb.NCBreadcrumbItem;
import { buttonClick, initTemplate, afterEvent,pageInfoClick, doubleClick,tableModelConfirm } from './events';
import './index.less';
import { tableId, formId,pagecode,  editButtons, browseButtons } from "./constants";
import { dataSource } from '../card/constants';
let{setDefData,getDefDate}=cardCache;
class List extends Component {
	constructor(props) {
		super(props);
		this.props = props;
		this.state = {
			data: {}
			
		}
	}

	componentDidMount () {  
		// getData();
        let pageInfo = this.props.table.getTablePageInfo(tableId);
        ajax({
			url: '/nccloud/obm/serverconfig/query.do',
			data: {
				conditions:null,
				pageInfo:pageInfo,
				pagecode: "36100SC_L01",
				queryAreaCode:"36100SC",
				oid:"",
				queryType:"simple"
			},
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if (data && data[tableId]) {
						this.props.table.setAllTableData(tableId, data[tableId]);
					} else {
						this.props.table.setAllTableData(tableId, { rows: [] });
					}
					setDefData(tableId,dataSource,data);
				}
			}
		});
	}
	
	/**
 	* 刷新
 	*/
	refreshAction= (props) =>{
		ajax({
			url: '/nccloud/obm/serverconfig/query.do',
			data: {
				pageid: pagecode
			},
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if (data && data[tableId]) {
						this.props.table.setAllTableData(tableId, data[tableId]);
					} else {
						this.props.table.setAllTableData(tableId, { rows: [] });
					}
				}
			}
		});
	}
	/**
 	* 测试连接
 	*/
	ConnectTestAction= (props,pksel) =>{
		ajax({
			url: '/nccloud/obm/serverconfig/listconnecttest.do',
			data: {
				pk: pksel
			},
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if (data && data[tableId]) {
						this.props.table.setAllTableData(tableId, data[tableId]);
					} else {
						this.props.table.setAllTableData(tableId, { rows: [] });
					}
				}
			}
		});
	}

	deleteAction= () =>{
		let idstr="";
		let index = 0;
		let pk = null;
		let selectDatas = null;
		selectDatas =this.props.table.getCheckedRows(tableId);
		//数据校验
		if (selectDatas ==null || selectDatas.length == 0) {
			toast({ color: 'warning', content: this.props.MutiInit.getIntl("36100SC_List") && this.props.MutiInit.getIntl("36100SC_List").get('36100SC_List-000000') });/* 国际化处理： 未选中行！*/
			return
		} 
		idstr="";
		index = 0;
		pk = null;
		while (index < selectDatas.length) {
			//获取行主键值
			pk = selectDatas[index] && selectDatas[index].data && selectDatas[index].data.values && selectDatas[index].data.values.pk_srvconf && selectDatas[index].data.values.pk_srvconf.value;
			//判空
			idstr = idstr + pk;
			if (index<selectDatas.length-1){
				idstr = idstr + ",";
			}
			index++;
		} 
		ajax({
			url: '/nccloud/obm/serverconfig/listdelete.do',
			data: { 
				id:idstr,
			},
			success: (res) => {
				let { success, data } = res;
				if (success) {
					// toast({ color: 'success', content: '删除成功' });
					if (data && data[tableId]) {
						this.props.table.setAllTableData(tableId, data[tableId]);
					} else {
						this.props.table.setAllTableData(tableId, { rows: [] });
					}
				}
			}
			});	
	}
	rowSelected = (props, moduleId, record, index, status) => {
		let checkedRows =this.props.table.getCheckedRows(moduleId);
		props.button.setButtonDisabled(['Delete'], !(checkedRows && checkedRows.length > 0));
		props.button.setButtonDisabled(['ConnectTest'], !(checkedRows && checkedRows.length ==1));
	};

	//请求列表数据
	// getData = () => {
	// };
	render() {
		let { table, button, form,modal,ncmodal } = this.props;
		let { createModal } = ncmodal;
		let buttons = this.props.button.getButtons();
		let { createSimpleTable } = table;
		let { createButtonApp } = this.props.button;
		let { createButton, getButtons } = button;
		let { createForm } = form;
		return (
			<div className="nc-bill-list">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
					{/*页面大图标*/}
						{createPageIcon()}
						<h2 className='title-search-detail'>
						{this.props.MutiInit.getIntl("36100SC_List") && this.props.MutiInit.getIntl("36100SC_List").get('36100SC_List-000007')}</h2>{/* 国际化处理： 支付通道设置*/}
					</div>
					<div className="header-button-area">
						{
							this.props.button.createButtonApp({
								area:'list_head',
								buttonLimit:4,
								onButtonClick: buttonClick.bind(this),
								popContainer:document.querySelector('.header-button-area')
							})
						}
					</div>
				</div>
				<div className="nc-bill-table-area" style={{ borderRadius: "3px 3px 0 0", overflow: "hidden" }}>
					{createSimpleTable(tableId, {//列表区
						handlePageInfoChange: pageInfoClick,
						tableModelConfirm: tableModelConfirm,
						onRowDoubleClick: doubleClick.bind(this),
						onSelected: this.rowSelected.bind(this),
						onSelectedAll: this.rowSelected.bind(this),
						adaptionHeight: true,
						showCheck: true,
						showIndex: true
					})}
				</div>
				{createModal(`delete`, { color: 'warning', hasCloseBtn: true })}
				{createModal(`batchDel`, { color: 'warning', hasCloseBtn: true })}
				
			</div>
		);
	}
}

List = createPage({
	mutiLangCode: '36100SC_List',
	initTemplate: initTemplate
})(List);

// ReactDOM.render(<List />, document.querySelector('#app'));
export default List;