/**
 * 表体区域
 */
export const tableId = 'table_srvconf_01';

/**
 * 表头区域
 */
export const formId = 'head';



/**
 * 页面编码
 */
export const pagecode = '36100SC_C01';
export const appcode = '36100SC';
export const appid = '0001Z61000000004PCHT';

/**
 * 编辑态按钮
 */
export const editButtons = ['Save','Cancel','AddLine','DelLine'];

/**
 * 浏览态按钮
 * 
 */
export const browseButtons = ['Add','Delete','ConnectTest','Refresh','Edit','AsslineDown'];

export const dataSource = 'tm.obm.serverconfig.serverconfig';
export const pkname='pk_srvconf';