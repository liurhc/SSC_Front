import { ajax, cardCache } from 'nc-lightapp-front';
import { tableId, pagecode,formId, dataSource,pkname } from '../constants';
export default function (props, pks) {
    let { getCacheById, updateCache } = cardCache;
    props.setUrlParam({id:pks});
    if(pks==null||pks=="null"||pks==undefined){
        return;
    }
    // let cardData=getCacheById(pks,dataSource);
    /*
    * id：数据主键的值
    * dataSource: 缓存数据命名空间
    */

    if (pks) {
        let cardData = getCacheById(pks, dataSource);
        if (cardData) {
            this.props.form.setAllFormValue({ [formId]: cardData.head[formId] });
            this.props.cardTable.setTableData(tableId, cardData.body[tableId]);
            this.setState({
                srvid: cardData.head[formId].rows[0].values.srvid.value,
            });
            this.toggleShow();
        } else {
            let dataArr = pks;
            //主键数组
            // dataArr.push(pks);
            let data = {
                pk: dataArr,
                pageCode: pagecode 
            }; 
            ajax({
                url: '/nccloud/obm/serverconfig/cardquery.do',
                data: data,
                success: (res) => { 
                    if (res.data) {
                        if (res.data.head) {											
							this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
							let srvid =res.data.head[formId].rows[0].values.srvid.value;
							this.setState({
								srvid:srvid
							});
						}
						if (res.data.body) {
							this.props.cardTable.setTableData(tableId, res.data.body[tableId]);
                        }
                        this.reGetdata();						
                        this.toggleShow();   
                        updateCache(pkname,pks,res.data,formId,dataSource);                     
                    } else {
						this.props.form.EmptyAllFormValue(formId);
						this.props.cardTable.setTableData(tableId, { rows: [] });
						this.toggleShow(); 
					}
                }
            });
        }
    }
}
