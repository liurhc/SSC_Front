import buttonClick from './buttonClick';
import initTemplate from './initTemplate';
import afterEvent from './afterEvent';
import beforeEvent from './beforeEvent';
import pageInfoClick from './pageInfoClick';
export { buttonClick, afterEvent,initTemplate,pageInfoClick,beforeEvent};
