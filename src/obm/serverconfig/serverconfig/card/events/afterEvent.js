import { ajax } from 'nc-lightapp-front';
import { tableId, formId,appid, pagecode } from "../constants";

export default function afterEvent(props, moduleId, key,value, changedrows, i) {
	if (key === 'netbankinftpstyle') {
	let CardData = this.props.createMasterChildData(pagecode, formId, tableId);
	let changevalue=changedrows.value;
	let id=CardData.head.head.rows[0].values.pk_srvconf.value;
	let netbankinftpstyle=CardData.head.head.rows[0].values.netbankinftpstyle.value;
	this.loadNetStyle(CardData);
	if(netbankinftpstyle==1){
		let value={"display":"/ufbank/ufbank.asp?client=java","value":"/ufbank/ufbank.asp?client=java"};
		props.form.setFormItemsValue(formId,{servelt:value});
	}else{
		let value={"display":"/access.jdo","value":"/access.jdo"};	
		props.form.setFormItemsValue(formId,{servelt:value});
	}
	if(changevalue!==value){
		let count = props.cardTable.getNumberOfRows(tableId);
        let arr = [];
        for (let j = 0; j < count; j++) {j
			arr[j] =j;
		}
		props.cardTable.delRowsByIndex(tableId, arr);
	}
}
}
