
// let formId = 'head';
// let tableId = 'table_srvconf_01';
// let pageId = '36100SC_C01';

import { base, ajax } from 'nc-lightapp-front';
// import intl from 'react-intl-universal';
import { tableId, formId,appid, pagecode,appcode } from "../constants";
import {tableButtonClick} from "./tableButtonClick";

let { NCPopconfirm } = base;

export default function(props) {
	let that = this;
	props.createUIDom(
		{
			pagecode: pagecode,//页面id
			appcode:appcode,
			// appid: appid//注册按钮的id
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta)
					props.meta.setMeta(meta);

					that.refresh();
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
			}
			
		}
		
	)
}

function modifierMeta(props, meta) {
	let status = props.getUrlParam('status');
	meta[formId].status = status;
	meta[tableId].status = status; 
	let items = meta["head"].items;
			let options = [];
			options[0] = {
				"display": props.MutiInit.getIntl("36100SC_Card") && props.MutiInit.getIntl("36100SC_Card").get('36100SC_Card-000020'),/* 国际化处理： 未启用*/
				"value": 0
			};
			options[1] = {
				"display": props.MutiInit.getIntl("36100SC_Card") && props.MutiInit.getIntl("36100SC_Card").get('36100SC_Card-000021'),/* 国际化处理： 已启用*/
				"value": 1
			};
			items.find((item) => item.attrcode == 'usestate').options = options;

			let xinoptions = [];
			xinoptions[0] = {
				"display": props.MutiInit.getIntl("36100SC_Card") && props.MutiInit.getIntl("36100SC_Card").get('36100SC_Card-000022'),/* 国际化处理： 网银适配器*/
				"value": 1
			};
			xinoptions[1] = {
				"display": props.MutiInit.getIntl("36100SC_Card") && props.MutiInit.getIntl("36100SC_Card").get('36100SC_Card-000023'),/* 国际化处理：银企联云*/
				"value": 2
			};
			items.find((item) => item.attrcode == 'netbankinftpstyle').options = xinoptions;
	return meta;
}
