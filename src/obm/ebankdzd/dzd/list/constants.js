/**
 * 页面ID
 */
export const appid = '0001Z6100000000346MU';
export const appcode = '36100DM';
/**
 * 页面编码
 */
export const pagecode = '36100DM_L01';
export const funcode = '36100DM';
/**
 * 表体区域
 */
export const tableid = 'ebank_dzd'; 
/**
 * 查询区域
 */
export const searchid = 'search';
export const oid ='1001Z6OBM00000000NYK';
export const searchCashCode = 'search_dzd';
/**
 * 弹出对话框区域
 */
export const downloadid = 'download';
export const inputid = 'input';
export const eurinputid = 'eurinput';

