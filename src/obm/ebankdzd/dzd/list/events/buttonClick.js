﻿import { ajax,base,print,output,cacheTools } from 'nc-lightapp-front';  
import {downloadid,inputid,tableid,funcode,appcode,searchCashCode} from '../constants.js';
import searchBtnClick from './searchBtnClick'; 

export default function buttonClick(props, id) { 
	switch (id) {		
		case 'download':				
				this.props.modal.show(downloadid);
				 break;
		case 'query':
				 this.props.modal.show('query');
				 break;
		case 'input': 
			this.InputClick(); 
			// this.open_bulu();
			break;
		// case 'eurinput': 
		// 	this.EurInputClick();
		// 	break;
		case 'printgroupbtn':
			printData(props);
			break;
		case 'output': 
			outputData(props); 
			break; 
		case 'refresh':  
			let searchData = cacheTools.get(searchCashCode); 
			searchBtnClick(props,searchData,true);
			break; 
	}
}

function printData(props){
	let printData = props.table.getCheckedRows(tableid);
		let pks = [];
		printData.forEach((item) => {
			pks.push(item.data.values.pk_ebank_dzd.value);
		});  
		print(
			'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
			'/nccloud/obm/ebankdzd/print.do',
			{
				funcode:funcode, //功能节点编码，即模板编码
				//nodekey: 'web_print', //模板节点标识
				appcode:appcode,
				// printTemplateID: '1001Z610000000003SNR',
				oids: pks // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
				// outputType: 'output'
			}
		);
}

function outputData(props){
	let printData = props.table.getCheckedRows(tableid);
		let pks = [];
		printData.forEach((item) => {
			pks.push(item.data.values.pk_ebank_dzd.value);
		});  
		output({
			url: '/nccloud/obm/ebankdzd/print.do',
			data: { 
			  	oids: pks,
			  	outputType: 'output'
			}
		});
}