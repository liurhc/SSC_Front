import { ajax } from 'nc-lightapp-front';
import {inputid,downloadid,searchid} from '../constants.js';

export default function (props, moduleId , key, changerows, value, data, index) {
    const { form } = this.props;
    if(moduleId===inputid){
        if (key==='banktype'){
            if (data.key==='cme'){
                form.setFormItemsDisabled(inputid, { ['bankaccount']: false });
            }else{
                form.setFormItemsDisabled(inputid, { ['bankaccount']: true });
                form.setFormItemsValue(inputid, { 'bankaccount': { value: null, display: null } });
            }
        }
        
    }else if(moduleId===downloadid){
        if (key === 'banktype') {
            //银行类别切换 银行账号清空
            form.setFormItemsValue(moduleId, { 'bankacc': { value: '', display: '' } }); 

        } else if (key === 'bankaccorg') {
            const bankaccorg = form.getFormItemsValue(moduleId, 'bankaccorg').value;
            if (bankaccorg) {

                //财务组织切换 银行类别 银行账号清空
                if (form.getFormItemsValue(moduleId, 'banktype').value) {
                    form.setFormItemsValue(moduleId, { 'banktype': { value: '', display: '' } });
                }

                if (form.getFormItemsValue(moduleId, 'bankacc').value) {
                    form.setFormItemsValue(moduleId, { 'bankacc': { value: '', display: '' } });
                }
            }
        } else if (key === 'bankacc') {
            //选择银行账号 
            let pk_banktype = '';
            let banktypename = '';
            //TODO 此处有bug 选择多个银行账户 之后删除一个后 获取不到参照的其它信息
            if (data.length != 0 && data[0].values['bd_banktypeid']) {
                pk_banktype = data[0].values['bd_banktypeid'].value;
                banktypename = data[0].values['pk_banktype'].value;
                form.setFormItemsValue(moduleId, { 'banktype': { value: pk_banktype, display: banktypename } });
            }

            if (data.length == 0) {
                pk_banktype = '';
                banktypename = '';
                form.setFormItemsValue(moduleId, { 'banktype': { value: pk_banktype, display: banktypename } });
            }
            
        }
    }
    
}