﻿import { ajax, base } from 'nc-lightapp-front'; 
import {appid,appcode,pagecode,funcode,tableid,searchid,downloadid,inputid,eurinputid} from '../constants.js';

export default function(props) {
	//请求模板数据
	props.createUIDom(
		{
			pagecode: pagecode,//页面id
			appcode: appcode //注册按钮的id	
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template; 
					//TODO 新盘去掉 
					meta[downloadid].status = 'edit';
					meta[inputid].status = 'edit';
					meta[eurinputid].status = 'edit';
					meta = modifierMeta(props, meta);
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button); 
					props.button.setDisabled({printgroupbtn:true});
					props.button.setDisabled({output:true});
				} 
			}
		}
	)
} 

function modifierMeta(props, meta) {
	let multiLang = props.MutiInit.getIntl(appcode);
	const { search, form } = props;
	//设置银行账号多选
	meta[searchid].items.find((e) => e.attrcode === 'curacc').isMultiSelectedEnabled = true;
	// meta[searchid].items.find((e) => e.attrcode === 'bankaccorg').isMultiSelectedEnabled = true;
	meta[downloadid].items.find((e) => e.attrcode === 'bankacc').isMultiSelectedEnabled = true; 
	//查询界面字段增加过滤条件
	meta[searchid].items.map((item) => {
		if (item.attrcode == 'bankaccorg') {
			item.queryCondition = () => {
				return {
					funcode: funcode,
					TreeRefActionExt: 'nccloud.web.tmpub.filter.FinanceOrgPermissionFilter' 
				};
			};
		} else if (item.attrcode == 'curacc') {
			item.queryCondition = () => {

				let pk_org = null;
				let pk_banktype = null;
				if (search.getSearchValByField(searchid, 'bankaccorg')) {
					pk_org = search.getSearchValByField(searchid, 'bankaccorg').value.firstvalue;
				}

				if (search.getSearchValByField(searchid, 'banktype')) {
					pk_banktype = search.getSearchValByField(searchid, 'banktype').value.firstvalue;
				}

				return {
					refnodename: multiLang && multiLang.get('36100DM-000000')/* 国际化处理： 核算归属权*/,
					funcode: funcode,
					pk_org: pk_org,
					pk_banktype: pk_banktype,
					GridRefActionExt: 'nccloud.web.obm.refbuilder.BankAccRefBuilder'
				};
			};

		}
	});
	//下载界面字段增加过滤条件
	meta[downloadid].items.map((item) => {
		if (item.attrcode == 'bankaccorg') {
			item.queryCondition = () => {
				return {
					funcode: funcode,
					TreeRefActionExt: 'nccloud.web.tmpub.filter.FinanceOrgPermissionFilter' 
				};
			};
		} else if (item.attrcode == 'bankacc') {
			item.queryCondition = () => {

				let pk_org2 = null;
				let pk_banktype2 = null;
				if (form.getFormItemsValue(downloadid, 'bankaccorg')) {
					pk_org2 = form.getFormItemsValue(downloadid, 'bankaccorg').value;
				}

				if (form.getFormItemsValue(downloadid, 'banktype')) {
					pk_banktype2 = form.getFormItemsValue(downloadid, 'banktype').value;
				}

				return {
					refnodename: multiLang && multiLang.get('36100DM-000000')/* 国际化处理： 核算归属权*/,
					funcode: funcode,
					pk_org: pk_org2,
					pk_banktype: pk_banktype2,
					GridRefActionExt: 'nccloud.web.obm.refbuilder.BankAccRefBuilder'
				};
			};

		}
	});
	meta[inputid].items.map((item) => {
		if(item.attrcode == 'type'){
			let options = [];
			options[0] = {
				"display": multiLang && multiLang.get('36100DM-000018'),/* 国际化处理： 按日期覆盖*/
				"value": 0
			};
			item.options = options;
		}
		if(item.attrcode == 'banktype'){
			let options = [];
			options[0] = {
				"display": multiLang && multiLang.get('36100DM-000019'),/* 国际化处理： NC对账单excel标准格式*/
				"value": "1"
			};
			options[1] = {
				"display": multiLang && multiLang.get('36100DM-000020'),/* 国际化处理： NC对账单xml标准格式*/
				"value": "0"
			};
			options[2] = {
				"display": multiLang && multiLang.get('36100DM-000021'),/* 国际化处理： 工商银行*/
				"value": "icbc"
			};
			options[3] = {
				"display": multiLang && multiLang.get('36100DM-000022'),/* 国际化处理： 建设银行*/
				"value": "ccb"
			};
			options[4] = {
				"display": multiLang && multiLang.get('36100DM-000023'),/* 国际化处理： 日本全银*/
				"value": "jba"
			};
			options[5] = {
				"display": multiLang && multiLang.get('36100DM-000024'),/* 国际化处理： 日本瑞穗-日本*/
				"value": "rbrs_rb"
			};
			options[6] = {
				"display": multiLang && multiLang.get('36100DM-000025'),/* 国际化处理： 日本瑞穗-大连*/
				"value": "rbrs_dl"
			};
			options[7] = {
				"display": multiLang && multiLang.get('36100DM-000026'),/* 国际化处理： 日本瑞穗-上海*/
				"value": "rbrs_sh"
			};
			options[8] = {
				"display": multiLang && multiLang.get('36100DM-000027'),/* 国际化处理： 日本三菱银行*/
				"value": "btmu"
			};
			options[9] = {
				"display": multiLang && multiLang.get('36100DM-000028'),/* 国际化处理： 花旗银行*/
				"value": "ctd"
			};
			options[10] = {
				"display": multiLang && multiLang.get('36100DM-000029'),/* 国际化处理： 中国农行*/
				"value": "cme"
			};
			options[11] = {
				"display": multiLang && multiLang.get('36100DM-000030'),/* 国际化处理： 中国银行*/
				"value": "cb"
			};
			item.options = options;
		}
	});
	return meta;
}