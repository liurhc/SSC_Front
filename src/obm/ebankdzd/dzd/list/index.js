﻿import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, formDownload, toast, createPageIcon } from 'nc-lightapp-front';
import { afterEvent, buttonClick, initTemplate, pageInfoClick, searchBtnClick, tableModelConfirm, onAfterEvent } from './events';
import './index.less';
import { appid, pagecode, tableid, searchid, downloadid, inputid, eurinputid, appcode } from './constants.js';
import PayBuluForm from '../../../ebankbulu/bulu/form/index';
// import { high } from 'nc-lightapp-front';
// const { NCUploader } = high;
// import { Upload } from 'tinper-bee';
const { NCModal, NCUpload, NCButton } = base;

class SingleTable extends Component {
	constructor(props) {
		super(props);
		this.props = props;
		this.moduleId = appcode;
		this.state = {
			data: {},
			showmodal: false,
			showmodaleur: false,
			showModal_bulu: false
		};
		this.eurfile = false;
		// this.beforeUpload = this.beforeUpload.bind(this); 
		this.upload_change = this.upload_change.bind(this);
		this.importProps = {
			name: 'file',
			action: '/nccloud/obm/ebankdzd/fileupload.do',
			// accept:'text/xml, application/xml, text/xls, application/xls',
			data: {},
			multiple: false,
			// beforeUpload: this.beforeUpload,
			headers: {
				authorization: 'authorization-text',
			},
			onChange: this.upload_change,
		};
		this.close_bulu = this.close_bulu.bind(this);
		this.open_bulu = this.open_bulu.bind(this);
	}

	close_bulu() {
		this.setState({ showModal_bulu: false });
	}

	open_bulu = () => {
		//弹出对话 框
		this.setState({ showModal_bulu: true });
	}

	//文件上传状态改变处理
	upload_change(info) {
		if (info.file.status !== 'uploading') {
			console.log(info.file, info.fileList);
		}
		if (info.file.status === 'done') {

			if (info.file.response && info.file.response.success) {

				let { data } = info.file.response;
				if (data) {
					let pathvalue = { value: data, display: data };
					if (this.eurfile) {
						this.props.form.setFormItemsValue(eurinputid, { path: pathvalue });
					} else {
						this.props.form.setFormItemsValue(inputid, { path: pathvalue });
					}
				}
			} else {
				toast({ content: info.file.response.error.message, color: "warning" });
			}
		} else if (info.file.status === 'error') {
			info.fileList.splice(0, info.fileList.length);
		}

	}


	getButtonNames = (codeId) => {
		return 'main-button';
	};


	//请求列表数据
	getData = () => {

	};

	//点击导入按钮，打开导入对话框
	InputClick = () => {

		this.eurfile = false;
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		let defaultmsg = multiLang && multiLang.get('36100DM-000004');/* 国际化处理： 对账单导入Excel模板: ..\resources\ebank\NC_Detail_Templet.xls*/
		let msgvalue = { value: defaultmsg, display: defaultmsg };
		this.props.form.setFormItemsValue(inputid, { msg: msgvalue });
		// this.props.form.setFormItemsValue(inputid,{'path':''});
		this.setState(
			{
				showmodal: true
			}
		)
	}

	//点击下载模板按钮，下载模板
	DownloadFileClick = () => {
		let data = 'xls';
		formDownload({
			params: { data: data },
			url: '/nccloud/obm/ebankdzd/filedownload.do',
			enctype: 1
		});
	}

	//点击下载模板按钮，下载模板
	DownloadFileClick2 = () => {
		let data = 'xml';
		formDownload({
			params: { data },
			url: '/nccloud/obm/ebankdzd/filedownloadxml.do',
			enctype: 1
		});
	}

	//点击预览按钮，预览对账单
	InputPreviewClick = () => {
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		let path = this.props.form.getFormItemsValue(inputid, 'path').value;
		if (!path) {
			toast({ content: multiLang && multiLang.get('36100DM-000006'), color: "warning" });/* 国际化处理： 请选择文件*/
			return;
		}
		let data = {
			inputtype: 0,
			bankaccount: this.props.form.getFormItemsValue(inputid, 'bankaccount').value,
			path: this.props.form.getFormItemsValue(inputid, 'path').value,
			banktype: this.props.form.getFormItemsValue(inputid, 'banktype').value
		};
		ajax({
			url: '/nccloud/obm/ebankdzd/fileinput.do',
			data,
			success: (res) => {
				let { success, data } = res;
				let multiLang = this.props.MutiInit.getIntl(this.moduleId);
				if (success) {
					if (data && data[tableid]) {
						this.props.table.setAllTableData(tableid, data[tableid]);
					} else {
						this.props.table.setAllTableData(tableid, { rows: [] });
					};
					this.setState(
						{
							showmodal: false
						}
					);
					toast({ color: 'success', content: multiLang && multiLang.get('36100DM-000005') }); /* 国际化处理： 预览成功*/
				};

			}
		});
	}

	//点击确定按钮，导入对账单
	InputOkClick = () => {
		if (this.props.form.isCheckNow(inputid)) {
			let multiLang = this.props.MutiInit.getIntl(this.moduleId);
			let path = this.props.form.getFormItemsValue(inputid, 'path').value;
			if (!path) {
				toast({ content: multiLang && multiLang.get('36100DM-000006'), color: "warning" });/* 国际化处理： 请选择文件*/
				return;
			}
			let data = {
				inputtype: 1,
				bankaccount: this.props.form.getFormItemsValue(inputid, 'bankaccount').value,
				path: this.props.form.getFormItemsValue(inputid, 'path').value,
				banktype: this.props.form.getFormItemsValue(inputid, 'banktype').value
			};
			ajax({
				url: '/nccloud/obm/ebankdzd/fileinput.do',
				data,
				success: (res) => {
					let { success, data } = res;
					let multiLang = this.props.MutiInit.getIntl(this.moduleId);
					if (success) {
						if (data && data[tableid]) {
							this.props.table.setAllTableData(tableid, data[tableid]);
						} else {
							this.props.table.setAllTableData(tableid, { rows: [] });
						};
						this.setState(
							{
								showmodal: false
							}
						);
						toast({ color: 'success', content: multiLang && multiLang.get('36100DM-000007') }); /* 国际化处理： 导入成功*/
					}
				}
			});
		}
	}

	//点击取消按钮，关闭页面
	InputCancelClick = () => {
		this.setState(
			{
				showmodal: false
			}
		)
	}

	//点击欧元导入按钮，打开欧元导入对话框
	EurInputClick = () => {
		this.eurfile = true;
		this.setState(
			{
				showmodaleur: true
			}
		)
	}

	//点击取消按钮，关闭欧元导入页面
	EurInputCancelClick = () => {
		this.setState(
			{
				showmodaleur: false
			}
		)
	}

	//点击确定按钮，导入欧元对账单
	EurInputOkClick = () => {
		let data = {
			inputtype: 1,
			path: this.props.form.getFormItemsValue(inputid, 'path').value
		};
		ajax({
			url: '/nccloud/obm/ebankdzd/europinput.do',
			data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if (data && data[tableid]) {
						this.props.table.setAllTableData(tableid, data[tableid]);
					} else {
						this.props.table.setAllTableData(tableid, { rows: [] });
					};
					this.setState(
						{
							showmodaleur: false
						}
					);
				}
			}
		});
	}

	//点击打印按钮，打印单据
	onPrint = () => {
		print(
			'html',  //支持两类: 'html'为模板打印, 'pdf'为pdf打印
			'nccloud/print.do', //后台服务url
			{
				billtype: '2502-Cxx-01',  //单据类型
				funcode: '20521030',      //功能节点编码，即模板编码
				nodekey: 'web_print',     //模板节点标识
				oids: ['1001A41000000000A9LR']    // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
				//  outputType: 'output'   // 打印按钮不用传该参数,输出按钮(文件下载)需加参数outputType,值为output。
			}
		)
	}

	//单击表格，编辑选中行数据
	onSelected = () => {
		let selectDatas = this.props.table.getCheckedRows(tableid);
		//判断是否有选中行
		if (selectDatas == undefined || selectDatas.length == 0) {
			this.props.button.setDisabled({ printgroupbtn: true });
			this.props.button.setDisabled({ output: true });
			return;
		} else {
			this.props.button.setDisabled({ printgroupbtn: false });
			this.props.button.setDisabled({ output: false });
		}
	}


	render() {
		let { table, button, form, search, modal } = this.props;
		let buttons = this.props.button.getButtons();
		let { createSimpleTable } = table;
		let { createButtonApp } = this.props.button;
		let { createButton, getButtons } = button;
		let { createForm } = form;
		let { NCCreateSearch } = search;
		let { createModal } = modal;
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		// let Uploader = this.props.Uploader;
		return (
			<div className="nc-bill-list">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						{/*页面大图标*/}
						{createPageIcon()}
						<h2 className='title-search-detail'>
							{multiLang && multiLang.get('36100DM-000011')/* 国际化处理： 银行对账单下载*/}</h2>
					</div>
					<div className="header-button-area">
						{/* 小应用注册按钮 */}
						{this.props.button.createButtonApp({
							area: 'list_head',
							buttonLimit: 4,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				<div className="nc-bill-search-area">
					{NCCreateSearch(searchid, {
						showAdvBtn: false,
						clickSearchBtn: searchBtnClick.bind(this),
						defaultConditionsNum: 4, //默认显示几个查询条件						
						onAfterEvent: onAfterEvent.bind(this) //编辑后事件
					})}
				</div>
				{/* <div style={{height:"10px"}}></div> */}
				<div className="nc-bill-table-area" style={{ borderRadius: "3px 3px 0 0", overflow: "hidden" }}>
					{createSimpleTable(tableid, {//列表区
						onAfterEvent: afterEvent,
						handlePageInfoChange: pageInfoClick,
						tableModelConfirm: tableModelConfirm,
						onSelected: this.onSelected,
						onSelectedAll: this.onSelected,
						adaptionHeight: true,
						showCheck: true,
						showIndex: true
					})}
				</div>

				{/* {createModal('query', {
					title: '查询',
					content: <OfflineQueryForm{...this.props} />,
					beSureBtnClick: this.QueryButtonYes
					// cancelBtnClick: this.cancelBtnClick, //取消按钮事件回调
					// closeModalEve: this.closeModalEve, //关闭按钮事件回调
					// userControl:true,  // 点确定按钮后，是否自动关闭弹出框.true:手动关。false:自动关
					// size:'lg', //  模态框大小 sm/lg/xlg
					// noFooter : true, //是否需要底部按钮,默认true
					// userControl: true, // 点确定按钮后，不自动关闭模态框
					// rightBtnName : '关闭', //右侧按钮名称,默认关闭
					// leftBtnName : '确认'  //左侧按钮名称， 默认确认 
				})} */}

				{createModal(downloadid, {
					title: multiLang && multiLang.get('36100DM-000009')/* 国际化处理： 在线下载*/,
					className: 'senior',
					hasCloseBtn: true,
					style: { width: "520px", height: "268px" },
					content:
						<div className="form-wrapper">
							{createForm(downloadid, {
								style: { width: "520px", height: "268px" },
								//编辑后事件
								onAfterEvent: afterEvent.bind(this)
							})}

						</div>
					,
					beSureBtnClick: () => {
						//数据校验
						const queryData = form.getAllFormValue(downloadid);

						if (form.isCheckNow(downloadid)) {
							let data = {
								model: {
									pageInfo: {
										pageIndex: 0,
										pageSize: 10,
										total: 0,
										totalPage: 0
									},
									...queryData
								},
							};
							ajax({
								url: '/nccloud/obm/ebankdzd/download.do',
								data,
								success: (res) => {
									let { success, data } = res;
									if (success) {
										if (data && data[tableid]) {
											this.props.table.setAllTableData(tableid, data[tableid]);
										} else {
											this.props.table.setAllTableData(tableid, { rows: [] });
										}
									}
								}
							});
						}
					}
				})}

				<NCModal show={this.state.showmodal} onHide={this.InputCancelClick.bind(this)} style={{ width: '520' }} className={'senior'}>
					<NCModal.Header closeButton={true}>
						<NCModal.Title>{multiLang && multiLang.get('36100DM-000012')/* 国际化处理： 导入文件*/}</NCModal.Title>
					</NCModal.Header>

					<NCModal.Body >
						<div >
							{
								createForm(inputid,
									{
										//编辑后事件
										onAfterEvent: afterEvent.bind(this)
									}
								)
							}
							{/* <span>文件: </span><input id='path'></input> */}

						</div>

					</NCModal.Body>

					<NCModal.Footer>
						<NCButton style={{ float: 'right' }} onClick={this.InputCancelClick.bind(this)}>{multiLang && multiLang.get('36100DM-000013')/* 国际化处理： 取消*/}</NCButton>
						<NCButton style={{ float: 'right' }} className="button-primary" onClick={this.InputOkClick.bind(this)}>{multiLang && multiLang.get('36100DM-000014')/* 国际化处理： 确定*/}</NCButton>
						<NCButton style={{ float: 'right' }} onClick={this.InputPreviewClick.bind(this)}>{multiLang && multiLang.get('36100DM-000015')/* 国际化处理： 预览*/}</NCButton>
						<NCButton style={{ float: 'right' }} onClick={this.DownloadFileClick.bind(this)}>{multiLang && multiLang.get('36100DM-000016')/* 国际化处理： 模板*/}XLS</NCButton>
						<NCButton style={{ float: 'right' }} onClick={this.DownloadFileClick2.bind(this)}>{multiLang && multiLang.get('36100DM-000016')/* 国际化处理： 模板*/}XML</NCButton>
						<NCUpload {...this.importProps}>
							<NCButton >{multiLang && multiLang.get('36100DM-000010')/* 国际化处理： 文件*/}</NCButton>
							{/* {createButton('upload', { name: '文件', buttonColor: 'main-button', style: { height: '32px', 'line-height': '33px', width: '68px', 'font-size': '13px', 'font-family': 'PingFangHk' } })} */}
						</NCUpload>
					</NCModal.Footer>

				</NCModal>

				<NCModal show={this.state.showmodaleur} style={{ width: '600px' }}>
					<NCModal.Header closeButton={true}>
						<NCModal.Title>{multiLang && multiLang.get('36100DM-000017')/* 国际化处理： 导入欧元*/}</NCModal.Title>
					</NCModal.Header>

					<NCModal.Body>
						<div >
							{
								createForm(eurinputid,
									{
										//编辑后事件
										onAfterEvent: afterEvent.bind(this)
									}
								)
							}
							{/* <span>文件: </span><input id='eurpath'></input> */}
							<NCUpload {...this.importProps}>
								{createButton('upload', { name: multiLang && multiLang.get('36100DM-000010'), buttonColor: 'main-button', style: { height: '32px', 'line-height': '33px', width: '68px', 'font-size': '13px', 'font-family': 'PingFangHk' } })/* 国际化处理： 文件*/}
							</NCUpload>
						</div>
					</NCModal.Body>

					<NCModal.Footer>
						<NCButton className="button-primary" onClick={this.EurInputOkClick.bind(this)}>{multiLang && multiLang.get('36100DM-000014')/* 国际化处理： 确定*/}</NCButton>
						<NCButton onClick={this.EurInputCancelClick.bind(this)}>{multiLang && multiLang.get('36100DM-000013')/* 国际化处理： 取消*/}</NCButton>
					</NCModal.Footer>

				</NCModal>

				<PayBuluForm showmodal={this.state.showModal_bulu} modal={modal}
					//输入参数： 
					onLineData={this.state.onLineData}
					modelType={this.state.modelType}
					moduleType={this.state.moduleType}
					//点击确定按钮的回调函数
					onSureClick={(retPayMsg) => {
						//处理补录信息(输出参数：PaymentRetMsg[])

						//关闭对话框
						this.setState({
							showModal_bulu: false
						})
					}}
					//点击关闭按钮的回调函数
					onCloseClick={() => {
						//关闭对话框
						this.setState({
							showModal_bulu: false
						})
					}}>
				</PayBuluForm>

			</div>
		);
	}
}

SingleTable = createPage({
	mutiLangCode: appcode,
	initTemplate: initTemplate
})(SingleTable);

ReactDOM.render(<SingleTable />, document.querySelector('#app'));
