import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, base, ajax, toast, promptBox,createPageIcon } from 'nc-lightapp-front';
import Utils from '../../../../uapbd/public/utils'
import './index.less';
//引入常量定义
import { list_table_id, app_id, app_code, list_page_id } from '../cons/constant.js';
import ObmBankaccSubDefaultRef from '../../../../obm/refer/pub/ObmBankaccSubDefaultRef';
import FinanceOrgTreeRef from '../../../../uapbd/refer/org/FinanceOrgTreeRef';
import { initTemplate } from './events';

const tableid = list_table_id;
const appid = app_id;
const pagecode = list_page_id;
const primaryKey = 'pk_id';
const appcode = app_code;
const urls = {
	add: '/nccloud/obm/ebankquotaconfig/add.do',
	save: '/nccloud/obm/ebankquotaconfig/save.do',
	query: '/nccloud/obm/ebankquotaconfig/query.do',
	bankAccChg: '/nccloud/obm/ebankquotaconfig/bankaccchg.do',
	queryTemplet: '/nccloud/platform/templet/querypage.do',
	queryContext: '/nccloud/platform/appregister/queryappcontext.do'
};
let { NCPopconfirm, NCModal, NCMenu: Menu } = base;
let allTableData = {};

class QuotaConfig extends Component {
	constructor(props) {
		super(props);
		this.props = props;
		this.props.button.setButtonsVisible({
			add: false,
			edit: false,
			save: false,
			cancel: false,
			delete: false,
			refresh: false
		});
		this.state = {
			curOrg: '',						//当前组织
			curBankAcc: '',                  //当前银行账户
			pks: null,
			disabledOrg: false,
			disabledBankAcc: true,
			context: {}                       //上下文
		}

		this.getData = this.getData.bind(this);
		initTemplate.call(this, props);
	}
	componentDidMount() {
		//this.getContext();
		//this.getData(false);
		this.updateButtonStatus();
	} 

	componentWillMount() {
		window.onbeforeunload = () => {
			// if (![ 'browse' ].includes(this.props.getUrlParam('status'))) {
			// 	return this.props.MutiInit.getIntl("36101010_Card") && this.props.MutiInit.getIntl("36101010_Card").get('36101010_Card-000025'); /* 国际化处理： 当前单据未保存, 您确定离开此页面?*/
			// 
			// }			
			// let tableStatus = this.props.cardTable.getStatus(tableid);
			let tableStatus = this.props.editTable.getStatus(tableid);
			if (tableStatus == 'edit') {
				return this.props.MutiInit.getIntl("36100PLC") && this.props.MutiInit.getIntl("36100PLC").get('36100PLC-000017'); /* 国际化处理： 当前单据未保存, 您确定离开此页面?*/
			}
		};
	}

	/**
     * 获取当前登录用户组织
     */
	getContext() {
		var me = this;
		let requestparam = {
			appcode: appcode
		}
		ajax({
			async: false,
			url: urls['queryContext'],
			data: requestparam,
			success: function (res) {
				if (res.success) {
					me.state.context = res.data;
					me.state.curOrg = {
						refpk: res.data.pk_org,
						//refcode: 'zm2016041501',
						refname: res.data.org_Name
					};
					if (res.data.pk_org) {
						me.state.disabledBankAcc = false;
					} else {
						me.state.disabledBankAcc = true;
					}
					me.setState(me.state);
				}
			}
		});
	}

	//请求列表数据
	getData = (isRefresh) => {
		ajax({
			url: urls['query'],
			data: {
				pk_org: this.state.curOrg.refpk,
				pk_bankacc: this.state.curBankAcc.refpk,
				appcode: appcode,
				permissonOrgs: true
			},
			success: (res) => {
				let { success } = res;
				if (success) {
					let data = res.data['grid'];
					Utils.handleTableReData({
						data: data,
						tableid: tableid,
						props: this.props,
						empty: (data) => {	//数据为空时执行回调方法
							this.props.editTable.setTableData(tableid, { rows: [] });
						},
						notEmpty: (data) => {//数据不为空时执行回调方法
							this.props.editTable.setTableData(tableid, data[tableid]);
						},
						after: (data) => {	//数据处理完成后执行回调方法
							allTableData = data[tableid];
							this.userJson = JSON.parse(res.data['userjson']);
							this.permissonOrgs = this.userJson.permissionOrgs;
						}
					});
					if(isRefresh && isRefresh==true){
						let multiLang = this.props.MutiInit.getIntl(appcode);
						toast({ content: multiLang && multiLang.get('36100PLC-000018'), color: 'success' })/* 国际化处理： 刷新成功*/
					}
				}
			}
		});
	};

	//编辑前事件：不能编辑非当前组织（若为集团，则非当前集团）的数据
	onBeforeEvent(props, moduleId, item, index, value, record) {
		let dataOrg = record.values.pk_org.value;
		let dataGroup = record.values.pk_group.value;
		if (!this.canOperate(dataOrg, dataGroup))
			return false
		return true
	}

	canOperate(pk_org, pk_group) {
		//只能编辑组织的数据并且只能够编辑有权限的组织的数据
		return this.permissonOrgs.indexOf(pk_org) > -1 && pk_org != pk_group ? true : false
	}

	//表格编辑后事件
	onAfterEvent(props, moduleId, key, value, changedrows, index, data) {
		//props, moduleId(区域id), key(操作的键), value（当前值）, changedrows（新旧值集合）, record（行数据）, index（当前index）
		if (key === 'func') {
			//清空账号值.
			props.editTable.setValByKeyAndIndex(tableid, index, 'pk_bankacc', { value: null, display: null, scale: null });
			props.editTable.setValByKeyAndIndex(tableid, index, 'pk_currtype', { value: null, display: null, scale: null });
			props.editTable.setValByKeyAndIndex(tableid, index, 'pk_banktype', { value: null, display: null, scale: null });
			props.editTable.setValByKeyAndIndex(tableid, index, 'bank', { value: null, display: null, scale: null });
			props.editTable.setValByKeyAndIndex(tableid, index, 'accountcode', { value: null, display: null, scale: null });
			props.editTable.setValByKeyAndIndex(tableid, index, 'accountname', { value: null, display: null, scale: null });
		}
		//银行账号
		if (key === 'pk_bankacc') {
			let pk_bankacc = props.editTable.getValByKeyAndIndex(tableid, index, 'pk_bankacc');
			if (!pk_bankacc.value) {
				return;
			}
			let tableData = props.editTable.getChangedRows(tableid);
			let data = {
				pageid: pagecode,
				model: {
					areaType: "table",
					pageinfo: null,
					rows: []
				}
			};
			data.model.rows = tableData;
			ajax({
				url: urls['bankAccChg'],
				data,
				success: function (res) {     //此处使用箭头函数，如果不使用箭头函数，一定要bind(this)
					let { success, data } = res
					if (success) {
						if (data) {
							let trsamt = data[tableid].rows[0].values.trsamt;
							props.editTable.setValByKeyAndIndex(tableid, index, 'trsamt', trsamt);
						}
					}
				}.bind(this)
			});

		}
	}

	//更新按钮状态
	updateButtonStatus() {
		//此处控制按钮的隐藏显示及启用状态
		let tableData = this.props.editTable.getCheckedRows(tableid);
		let length = tableData.length;//获取列表页选择数据的行数
		let num = this.props.editTable.getNumberOfRows(tableid); //获取列表总行数
		if (num <= 0) {//未选择数据
			this.props.button.setButtonDisabled({
				edit: true
			});
		}
		else {//选择数据
			this.props.button.setButtonDisabled({
				edit: false
			});
		}

		if (length <= 0) {//未选择数据
			this.props.button.setButtonDisabled({
				delete: true
			});
		}
		else {//选择数据
			this.props.button.setButtonDisabled({
				delete: false
			});
		}

		if (this.props.editTable.getStatus(tableid) === 'edit') {//编辑状态
			this.props.button.setButtonsVisible({
				add: false,
				edit: false,
				save: true,
				cancel: true,
				delete: false,
				refresh: false
			});
			this.setState({
				disabledOrg: true,
				disabledBankAcc: true
			});

			//应UE要求：编辑态新增变为次要按钮
			this.props.button.setMainButton(['add'], false);
			this.props.button.setPopContent('delline', undefined);
		} else {//浏览态
			this.props.button.setButtonsVisible({
				add: true,
				edit: true,
				delete: true,
				save: false,
				cancel: false,
				refresh: true
			});

			let disabledBankAcc = true;
			if (this.state.curOrg.refpk) {
				disabledBankAcc = false;
				this.props.button.setButtonDisabled({
					add: false 
				});
			}else{
				disabledBankAcc = true;
				this.props.button.setButtonDisabled({
					add: true
				});
			}

			this.setState({
				disabledOrg: false,
				disabledBankAcc: disabledBankAcc
			});

			//应UE要求：编辑态新增变为次要按钮
			this.props.button.setMainButton(['add'], true)

			this.props.button.setPopContent('delline', '确认要删除吗？');
		}
	}

	addRow(isFocus = true) {
		let multiLang = this.props.MutiInit.getIntl(appcode);
		if (this.state.curOrg.refpk == null) {
			toast({ content: multiLang && multiLang.get('36100PLC-000005'), color: 'warning' })/* 国际化处理： 请先选择财务组织！*/
			return
		}
		let num = this.props.editTable.getNumberOfRows(tableid); //获取列表总行数
		ajax({
			url: urls['add'],
			data: {
				pk_org: this.state.curOrg.refpk
			},
			success: (res) => {
				if (res) {
					let { success, data } = res;
					if (success) {
						if (data) {
							let record = [{ index: num, data: data[tableid].rows[0] }];
							//this.props.editTable.addRow(tableid, num, isFocus,record);
							this.props.editTable.insertDataByIndexs(tableid, record);
							this.props.editTable.setStatus(tableid, 'edit');
						}
					}
				}
			}
		});
	}

	judgeOperation(selectedData) {
		let multiLang = this.props.MutiInit.getIntl(appcode);
		//只能维护当前组织的数据
		let curOrg = this.state.curOrg.refpk
		let canOperate = true
		selectedData.forEach(item => {
			let dataOrg = item.data ? item.data.values.pk_org.value : item.values.pk_org.value;
			let dataGroup = item.data ? item.data.values.pk_group.value : item.values.pk_group.value;
			if (!this.canOperate(dataOrg, dataGroup))
				canOperate = false
		})
		if (!canOperate) {
			toast({ content: multiLang && multiLang.get('36100PLC-000006'), color: 'warning' })/* 国际化处理： 组织节点只能维护当前节点有权限组织的数据！*/
		}
		return true
	}

	//唯一校验
	//同一组织内一个账号一种支付类型只能设置一条支付限额 
	isUnique() {
		//表体所有的数据
		let isResult = true;
		let allRow = this.props.editTable.getAllRows(tableid);

		allRow = allRow.filter(item => item.status != '3');

		if (allRow && allRow.length > 0) {
			let checkHM = new Map();
			let item = null;
			for (let i = 0; i < allRow.length; i++) {
				item = allRow[i];
				let banktypecode = item.data ? item.data.values.pk_banktype.value : item.values.pk_banktype.value;
				let func = item.data ? item.data.values.func.value : item.values.func.value;
				let pk_account = item.data ? item.data.values.pk_bankacc.value : item.values.pk_bankacc.value;
				let pk_org = item.data ? item.data.values.pk_org.value : item.values.pk_org.value;
				let key = banktypecode + "|" + func + "|" + pk_org + "|" + pk_account;

				if (checkHM.has(key)) {
					isResult = false;
					break;
				} else {
					checkHM.set(key, key);
				}
			}
		}
		return isResult;
	}

	//按钮点击事件
	onButtonClick(props, id) {

		let multiLang = this.props.MutiInit.getIntl(appcode);

		switch (id) {
			case 'add':
				this.addRow()
				break;
			case 'edit':
				//UE规范：点击修改时，如果没有选择财务组织，不可进入修改态
				// if (this.state.curOrg.refpk == null) {
				// 	toast({ content: '请先选择财务组织！', color: 'warning' })
				// 	return
				// }
				this.props.editTable.setStatus(tableid, 'edit');
				// this.props.button.setPopContent('delline', undefined);
				break;
			case 'cancel':
				// this.props.modal.show('cancel');
				promptBox({
					color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
					title: multiLang && multiLang.get('36100PLC-000013'),/* 国际化处理： 取消*/
					content: multiLang && multiLang.get('36100PLC-000014'),/* 国际化处理： 确定要取消吗？*/
					// noFooter: false,                // 是否显示底部按钮(确定、取消),默认显示(false),非必输
					// noCancelBtn: false,             // 是否显示取消按钮,，默认显示(false),非必输
					// beSureBtnName: "确定",          // 确定按钮名称, 默认为"确定",非必输
					// cancelBtnName: "取消",         // 取消按钮名称, 默认为"取消",非必输
					// hasCloseBtn:false,             //显示“X”按钮，默认不显示，不显示是false，显示是true
					beSureBtnClick: this.onCancelSureEvent.bind(this),   // 确定按钮点击调用函数,非必输
					// cancelBtnClick: functionCancel,  // 取消按钮点击调用函数,非必输
					// closeBtnClick:functionClose, //关闭按钮点击调用函数，非必输
					// closeByClickBackDrop:false,//点击遮罩关闭提示框，默认是false点击不关闭,点击关闭是true
				})
				//this.props.editTable.showColByKey(tableid,'opr');//显示操作列
				break;
			case 'save':
				// if (this.state.curOrg.refpk == null) {
				// 	toast({ content: multiLang && multiLang.get('36100PLC-000005'), color: 'warning' })/* 国际化处理： 请先选择财务组织！*/
				// 	return
				// }
				//this.props.editTable.filterEmptyRows(tableid, keys);
				let tableData = this.props.editTable.getChangedRows(tableid);   //保存时，只获取改变的数据行而不是table的所有数据行，减少数据传输

				//保存校验
				if (!this.props.editTable.checkRequired(tableid, tableData)) {
					return;
				}
				if (tableData === undefined) {
					return
				}
				else if (tableData && tableData.length === 0) {
					toast({ content: multiLang && multiLang.get('36100PLC-000007'), color: 'info' })/* 国际化处理： 没有要保存的数据*/
					this.props.editTable.cancelEdit(tableid)
					return
				}

				//做下校验：现在通过行操作列删除的时候，组织节点可以删除集团的数据，但是我们没有办法做校验不让删（获取不到该组件选择的组织信息）
				//所以通过保存的时候做下校验
				if (!this.judgeOperation(tableData))
					return

				//数据唯一校验
				if (!this.isUnique()) {
					toast({ content: multiLang && multiLang.get('36100PLC-000008'), color: 'danger' })/* 国际化处理： 同一组织内一个账号一种支付类型只能设置一条支付限额*/
					return;
				}
				let data = {
					pageid: pagecode,
					model: {
						areaType: "table",
						pageinfo: null,
						rows: []
					}
				};
				data.model.rows = tableData;
				// Utils.convertGridEnablestate(data.model.rows)
				ajax({
					url: urls['save'],
					data,
					success: function (res) {     //此处使用箭头函数，如果不使用箭头函数，一定要bind(this)
						let { success, data } = res
						if (success) {
							this.props.editTable.setStatus(tableid, 'browse');//设置表格状态为浏览态
							if (data) {
								toast({ content: multiLang && multiLang.get('36100PLC-000016'), color: 'success' });/* 国际化处理： 保存成功*/
								// Utils.convertGridEnablestate(data[tableid].rows)
								let allD = this.props.editTable.getAllData(tableid);
								//Utils.filterDelRows(allD.rows);//过滤清除删除状态的行
								allD.rows = allD.rows.filter(item => item.status != '3');
								Utils.filterResult(allD, data[tableid].rows);
								this.props.editTable.setTableData(tableid, allD);
							}
							//this.props.editTable.showColByKey(tableid,'opr');//显示操作列
						}
					}.bind(this)
				});
				break;
			case 'delete':
				let selectedData = this.props.editTable.getCheckedRows(tableid);
				if (selectedData.length == 0) {
					toast({ content: multiLang && multiLang.get('36100PLC-000009'), color: 'warning' });/* 国际化处理： 请选择要删除的数据*/
					return
				}

				//编辑态才做如此校验，如果是浏览状态先弹出删除对话框，然后在提示信息
				if (this.props.editTable.getStatus(tableid) === 'edit') {
					let canOperate = this.judgeOperation(selectedData)
					if (!canOperate) {
						return
					}
				}

				if (this.props.editTable.getStatus(tableid) === 'edit') {//编辑状态
					let indexArr = [];
					selectedData.forEach((val) => {
						indexArr.push(val.index);
					});
					this.props.editTable.deleteTableRowsByIndex(tableid, indexArr);
				} else {
					// this.props.modal.show('modal');
					promptBox({
						color: "warning",
						title: multiLang && multiLang.get('36100PLC-000010'),//标题/* 国际化处理： 删除*/
						content: multiLang && multiLang.get('36100PLC-000012'),	//内容/* 国际化处理： 确定删除所选数据？*/
						beSureBtnClick: this.onDelForBrowse.bind(this),		//确定按钮事件回调
					})
				}
				break;
			case 'refresh':
				if (this.state.curOrg.refpk == null) {
					toast({ content: multiLang && multiLang.get('36100PLC-000005'), color: 'warning' })/* 国际化处理： 请先选择财务组织！*/
					break;
				}
				this.getData(true);
				break;
		}

	}

	onRowClick(props, moduleId, record, index) {
		if (this.props.editTable.getStatus(tableid) === 'edit') {
			let pk_org = record.values.pk_org.value;
			let pk_group = record.values.pk_group.value;
			if (!this.canOperate(pk_org, pk_group)) {
				props.editTable.setEditableByKey(tableid, record.rowid, item, false)
			}
		}
	}

	//浏览态确认删除事件
	onDelForBrowse() {
		let multiLang = this.props.MutiInit.getIntl(appcode);
		let selectedData = this.props.editTable.getCheckedRows(tableid);
		let canOpe = this.judgeOperation(selectedData)
		if (!canOpe)
			return
		let indexArr = [];
		let dataArr = [];
		selectedData.forEach((val) => {
			let delObj = {
				status: '3',
				values: {
					ts: {
						display: multiLang && multiLang.get('36100PLC-000002'),/* 国际化处理： 时间戳*/
					}
					// pk_defdoclist: {
					// 	display: '主键',
					// }
				}
			};
			delObj.values[primaryKey] = {
				display: multiLang && multiLang.get('36100PLC-000003')/* 国际化处理： 主键*/
			}
			delObj.rowId = val.data.rowId;
			delObj.values.ts.value = val.data.values.ts.value;
			delObj.values[primaryKey].value = val.data.values[primaryKey].value;
			dataArr.push(delObj);
			indexArr.push(val.index);
		});
		let data = {
			pageid: pagecode,
			model: {
				areaType: 'table',
				pageinfo: null,
				rows: dataArr
			}
		};
		ajax({
			url: urls['save'],
			data,
			success: (res) => {     //此处使用箭头函数，如果不使用箭头函数，一定要bind(this)
				let { success, data } = res;
				if (success) {
					this.props.editTable.deleteTableRowsByIndex(tableid, indexArr);
					let allD = this.props.editTable.getAllData(tableid);
					//Utils.filterDelRows(allD.rows);//过滤清除删除状态的行
					allD.rows = allD.rows.filter(item => item.status != '3')
					this.props.editTable.setTableData(tableid, allD);
					toast({ content: multiLang && multiLang.get('36100PLC-000004'), color: 'success' });/* 国际化处理： 删除成功*/
				}
			}
		});
	}

	onCancelSureEvent() {
		this.props.editTable.cancelEdit(tableid);
		this.updateButtonStatus();
	}

	onOrgChange(val) {
		let selectedOrg = null
		let disabledBankAcc = true;

		//平台似乎变过一次，val变成了数组了，后来又有人说变成了单个对象了，此处校验一下，反正各个版本挺乱的~~(-_-)
		if (Array.isArray(val)) {
			selectedOrg = val[0]
		}
		else {
			selectedOrg = val
		}

		if (selectedOrg.refpk) {
			disabledBankAcc = false;
			this.props.button.setButtonDisabled({
				add: false
			});
		}else{
			this.props.button.setButtonDisabled({
				add: true
			});
		}

		if (selectedOrg.refpk) {
			this.setState({
				curOrg: selectedOrg,
				disabledBankAcc: disabledBankAcc
			}, () => {
				this.getData();
			})
		} else {
			this.setState({
				curOrg: selectedOrg,
				disabledBankAcc: disabledBankAcc
			})
		}


	}

	onBankAccChange(val) {
		let selectedCurBankAcc = null;

		//平台似乎变过一次，val变成了数组了，后来又有人说变成了单个对象了，此处校验一下，反正各个版本挺乱的~~(-_-)
		if (Array.isArray(val)) {
			selectedCurBankAcc = val[0];
		}
		else {
			selectedCurBankAcc = val;
		}

		this.setState({
			curBankAcc: selectedCurBankAcc
		}, () => {
			this.getData();
		})
	}

	render() {

		let multiLang = this.props.MutiInit.getIntl(appcode);
		let { table, button, search, editTable, modal, ncmodal } = this.props;
		let { createEditTable } = editTable;
		let { NCCreateSearch } = search;
		let { createButtonApp } = button;
		let { NCFormControl, NCCheckbox } = base;
		let { createModal } = modal;
		let createNCModal = ncmodal.createModal;
		const { Item } = Menu;

		let createOrgRender = () => {
			return (
				<div className="search-box" style={{ width: '200px', marginLeft: '20px' }}>
					<span style={{ position: "absolute", left: "2%", top: "35%", "z-index": "10", color: "red", font: "inherit" }}>*</span>
					{FinanceOrgTreeRef({
						onChange: this.onOrgChange.bind(this),
						value: this.state.curOrg,
						disabled: this.state.disabledOrg,
						queryCondition: () => {
							return {
								// TreeRefActionExt: "nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder"
								funcode: appcode,
								TreeRefActionExt: 'nccloud.web.tmpub.filter.FinanceOrgPermissionFilter'
							}
						}
					})}
				</div>
			)
		};


		let createBankaccRender = () => {

			return (
				<div className="search-box" style={{ width: '200px' }}>
					{ObmBankaccSubDefaultRef({
						onChange: this.onBankAccChange.bind(this),
						value: this.state.curBankAcc,
						disabled: this.state.disabledBankAcc,
						fieldDisplayed: 'refcode',
						queryCondition: () => {
							return {
								pk_org: this.state.curOrg.refpk,
								acctypeflag: 0
							}
						}
					})}
				</div>
			)
		};

		return (
			<div className="nc-bill-list">
				<div className="nc-bill-header-area" style={{ borderRadius: "3px 3px 0 0", overflow: "hidden" }}>
					{/* 标题 title */}
					<div className="header-title-search-area">
						{/*页面大图标*/}
						{createPageIcon()}
						<h2 className="title-search-detail">		
							{multiLang && multiLang.get('36100PLC-000015')}</h2>{/* 国际化处理： 账户支付限额设置*/}
						<div style={{ marginRight: "10px" }}>
							{createOrgRender()}
						</div>

						<div style={{ marginRight: "20px" }}>
							{createBankaccRender()}
						</div>
					</div>
					{/* 按钮区  btn-group */}
					<div className="header-button-area">
						{createButtonApp({
							area: 'list_head',
							buttonLimit: 3,
							onButtonClick: this.onButtonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')

						})}
					</div>
				</div>

				{/* 列表区 */}
				<div className="nc-singleTable-table-area">
					{createEditTable(tableid, {//列表区
						//onCloseModel: onCloseModelFn,                    // 弹窗控件点击关闭事件 
						onAfterEvent: this.onAfterEvent.bind(this),                      // 控件的编辑后事件  
						//onBeforeEvent: this.onBeforeEvent.bind(this),
						useFixedHeader: true,
						//onSelected: onSelectedFn,                        // 左侧选择列单个选择框回调
						//onSelectedAll: onSelectedAllFn,                  // 左侧选择列全选回调
						selectedChange: this.updateButtonStatus.bind(this),                // 选择框有变动的钩子函数
						statusChange: this.updateButtonStatus.bind(this),				//表格状态监听
						adaptionHeight: true,
						showIndex: true,				//显示序号
						showCheck: true,			//显示复选框
						onRowClick: this.onRowClick.bind(this)
						//params: 'test',                                  // 自定义传参

					})}
				</div>
				{/* 删除前确认模态框 */}
				{createNCModal('modal', {
					title: multiLang && multiLang.get('36100PLC-000010'),//标题/* 国际化处理： 删除*/
					content: multiLang && multiLang.get('36100PLC-000012'),	//内容/* 国际化处理： 确定删除所选数据？*/
					beSureBtnClick: this.onDelForBrowse.bind(this),		//确定按钮事件回调
				})}
				{createNCModal(('cancel'), {
					title: multiLang && multiLang.get('36100PLC-000013'),/* 国际化处理： 取消*/
					content: multiLang && multiLang.get('36100PLC-000014'),/* 国际化处理： 确定要取消吗？*/
					beSureBtnClick: this.onCancelSureEvent.bind(this)
				})}
			</div>
		);
	}
}




QuotaConfig = createPage({
	mutiLangCode: appcode,
	billinfo: {
		billtype: 'grid',
		pagecode: pagecode,
		bodycode: tableid
	},
	// initTemplate: initTemplate
})(QuotaConfig);

ReactDOM.render(<QuotaConfig />, document.querySelector('#app'));
