import { base, ajax, getBusinessInfo, cacheTools, toast } from 'nc-lightapp-front';
//引入常量定义
import { app_id, list_page_id, list_table_id, app_code } from '../../cons/constant.js';
//账户参照
import ObmBankaccSubDefaultRef from '../../../../refer/pub/ObmBankaccSubDefaultRef/index.js';
import { tableButtonClick } from '../events';

let { NCPopconfirm } = base;
const pagecode = list_page_id;
const tableid = list_table_id;
const primaryKey = 'pk_id';
const appcode = app_code;
const urls = {
	add: '/nccloud/obm/ebankquotaconfig/add.do',
	save: '/nccloud/obm/ebankquotaconfig/save.do',
	query: '/nccloud/obm/ebankquotaconfig/query.do',
	queryTemplet: '/nccloud/platform/templet/querypage.do',
	queryContext: '/nccloud/platform/appregister/queryappcontext.do'
};

export default function (props) {
	//请求模板数据
	props.createUIDom(
		{
			pagecode: pagecode,//页面id
			appid: app_id,//注册按钮的id
			appcode: appcode
		},
		(data) => {
			if (data) {

				let multiLang = props.MutiInit.getIntl(appcode);

				let meta = data.template;
				meta = modifierMeta(props, meta)
				//设置上下文
				setContext.call(this,data.context);
				props.meta.setMeta(meta);
				data.button && props.button.setButtons(data.button);
				props.button.setPopContent('delline', multiLang && multiLang.get('36100PLC-000000')); /* 国际化处理： 确定要删除吗？*//* 设置操作列上删除按钮的弹窗提示 */
			}
		}
	)

}


//对表格模板进行加工操作
function modifierMeta(props, meta) {
	let multiLang = props.MutiInit.getIntl(appcode);
	//控制比录字段的必输属性
	let requriedAttr = ['code', 'name']
	meta[tableid].items.map((obj) => {
		if (requriedAttr.indexOf(obj['attrcode']) >= 0) {
			obj['required'] = false;
		}
	});

	//表体字段增加过滤条件
	meta[tableid].items.map((item) => {
		if (item.attrcode == 'pk_bankacc') {
			item.editAfterFlag = true;
			item.render = function (text, record, index) {
				return (
					ObmBankaccSubDefaultRef({
						fieldDisplayed:'refcode',
						queryCondition: () => {
							let pk_org = record.values.pk_org.value;
							let acctypeflag = '0';
							if (record.values['func'].value) {

								if ("dfdk" === record.values['func'].value) {
									acctypeflag = "2";
								} else {
									acctypeflag = "1";
								}
							}
							return {
								pk_org: pk_org,
								acctypeflag: acctypeflag
							};
						}
					})
				);
			}
		}
	});

	//添加表格操作列
	let event = {
		label: multiLang && multiLang.get('36100PLC-000001'),/* 国际化处理： 操作*/
		attrcode: 'opr',
		key: 'opr',
		fixed: 'right',
		itemtype: 'customer',
		visible: true,
		render(text, record, index) {
			let buttonAry =['delline'];
			return props.button.createOprationButton(buttonAry, {
				area: "list_inner",
				buttonLimit: 3,
				onButtonClick: (props, key) => tableButtonClick.call(this,props, key, text, record, index)
			});

		}
	};

	meta[tableid].items.push(event);

	return meta;
}

/*
*
* 设置上下文
*/
function setContext(context) {

	let disabledBankAcc = true;
	if (context.pk_org) {
		disabledBankAcc = false;
	} else {
		disabledBankAcc = true;
	}
	let curOrg = {
		refpk: context.pk_org,
		refname: context.org_Name
	}
	this.setState({
		disabledBankAcc: disabledBankAcc,
		context: context,
		curOrg: curOrg
	});

	this.getData();
}
