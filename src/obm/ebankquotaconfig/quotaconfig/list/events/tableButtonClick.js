import { createPage, ajax, base, toast } from 'nc-lightapp-front';

import { list_table_id, list_page_id} from '../../cons/constant.js';

const tableid = list_table_id;
const pagecode = list_page_id;
const primaryKey = 'pk_id';

export default function tableButtonClick(props, key, text, record, index) {
	switch (key) {
		case 'delline':
		
			if(props.editTable.getStatus(tableid) === 'edit'){//编辑状态
				props.editTable.deleteTableRowsByIndex(tableid, index);
			}else{//浏览态
				let delObj = {
					rowid: index,
					status: '3',
					values: {
						ts: {
							display: props.MutiInit.getIntl("36100PLC") && props.MutiInit.getIntl("36100PLC").get('36100PLC-000002'),/* 国际化处理： 时间戳*/
							value: record.values.ts.value
						}
					}
				};
				delObj.values[primaryKey] = {
					display: props.MutiInit.getIntl("36100PLC") && props.MutiInit.getIntl("36100PLC").get('36100PLC-000003'),/* 国际化处理： 主键*/
					value: record.values[primaryKey].value
				}
				let indexArr=[];
				indexArr.push(index);
				let data = {
					pageid: pagecode,
					model: {
						areaType: 'table',
						pageinfo: null,
						rows: [ delObj ]
					}
				};
				ajax({
					url: '/nccloud/obm/ebankquotaconfig/save.do',
					data,
					success: function(res) {
						let { success, data } = res;
						if (success) {
							props.editTable.deleteTableRowsByIndex(tableid, indexArr);
							let allD = props.editTable.getAllData(tableid);
							allD.rows = allD.rows.filter(item => item.status != '3')
							props.editTable.setTableData(tableid,allD);
							toast({content:props.MutiInit.getIntl("36100PLC") && props.MutiInit.getIntl("36100PLC").get('36100PLC-000004'),color:'success'});/* 国际化处理： 删除成功*/
						}
					}.bind(this)
				});
			}
			break;
		default:
			console.log(key, index);
			break;
	}
}
