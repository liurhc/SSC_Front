/**
 * 表体区域
 */
export const tableId = 'pk_ebankpooldownload';

/**
 * 表头区域
 */
// export const formId = 'search';

/**
 * 查询区域编码
 */
export const searchId = 'search'; 
export const searchparam = '36101030_L01_search';
export const searchback = '36101030_C01_back';
export const searchoid = '1001Z61000000001ZL6M';
/**
 * 页面编码
 */
export const pagecode = '36101030_L01';
export const appcode = '36101030';
export const appid = '0001Z61000000001ZZTL';
export const pagecode_card = '36101030_C01';
/**
 * 编辑态按钮
 */
export const editButtons = ['Save', 'Cancel'];

/**
 * 浏览态按钮
 */
export const browseButtons = ['Add','Delete','ConnectTest','Refresh','Edit'];