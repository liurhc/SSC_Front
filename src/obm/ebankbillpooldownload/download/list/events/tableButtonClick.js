﻿import { ajax, base, print,toast,promptBox  } from 'nc-lightapp-front';
import {tableId,searchId,pagecode,appcode,pagecode_card} from '../constants'; 

export default function tableButtonClick(props, key, text, record, index) {
	let that = this;
  let pksel = record.pk_ebankpooldownload.value;
  let ts = record.ts.value;
	switch (key) {
		//修改	
		case 'EditLine' :
			gotoCard(props,pksel,'edit',false);
			break;
		//删除
		case 'DeleteLine':
			deleteData(props,pksel,ts,that);
			break;
		//状态下载
		case 'EbankLogDownloadStateAction':
			let pks = [record.pk_ebank_paylog_h.value];
		
			ajax({
				url: '/nccloud/obm/ebankpaylog/ebankLogDownload.do',
				data: { pks },
				success: (res) => {
					if (res.data && res.data.msg) {
						toast({ content: res.data.msg, color: 'success' });
					}
				}
			});
			break;
		//状态同步
		case 'EbankLogFireEventSendStateAction':
		pks = [record.pk_ebank_paylog_h.value];
			
		ajax({
		  url: '/nccloud/obm/ebankpaylog/ebankLogFireEventSendState.do',
		  data: {pks},
		  success: (res) => {
			if (res.data && res.data.msg) {
			  toast({ content: res.data.msg , color: 'success' });
			}
		  }
		});
		  break;
		default:
			break;
	}
}


function gotoCard(props,pk,status,copyFlag){  
	props.pushTo('/card',{ 
		status:status,
		type:'link',
		appcode:appcode,
		id:pk,
		pagecode:pagecode_card
	}); 
}

function deleteData(props,pk,ts,that){
	promptBox({
		color: "warning",
		title: props.MutiInit.getIntl("36101030") && props.MutiInit.getIntl("36101030").get('36101030-000025'),/* 国际化处理： 删除*/
		content: props.MutiInit.getIntl("36101030") && props.MutiInit.getIntl("36101030").get('36101030-000028'),/* 国际化处理： 确认要删除吗?*/
		beSureBtnClick: () => { 
			ajax({
				url: '/nccloud/obm/ebankbillpooldownload/listdelete.do',
				data: { 
					id:pk,
				},
				success: (res) => {
					if (res) {
						that.refreshAction()
					}
				} 
			});	
		}
	});	 	  
}