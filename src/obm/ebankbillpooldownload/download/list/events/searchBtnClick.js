import {ajax,toast,cacheTools,cardCache} from 'nc-lightapp-front';
import { dataSource } from '../../card/constants';
let{setDefData,getDefDate,getDefData,deleteCacheById}=cardCache;
import {tableId,searchId,pagecode,searchoid,searchparam,searchback} from '../constants'; 
//点击查询，获取查询区数据
export default function clickSearchBtn(props,searchVal) {
    let backmark = cacheTools.get(searchback);
    console.log(searchVal);
    //缓存查询条件
    cacheTools.set(searchback, '0');
	cacheTools.set(searchparam, searchVal);
    let pageInfo = props.table.getTablePageInfo(tableId);
    let metaData = props.meta.getMeta();
    let queryInfo = props.search.getQueryInfo(searchId, false);
    //let oid = queryInfo.oid;2019年6月26日
    let oid = searchoid;//查询区域主键
    let conditions = null;
    if(searchVal){
        conditions = searchVal.conditions;
    }
    let data={
        conditions:conditions ,
        pageInfo:pageInfo,
        pagecode: pagecode,
        queryAreaCode:searchId,
        oid:oid,
        queryType:"simple"
     };

    console.log(data);
    ajax({
        url: '/nccloud/obm/ebankbillpooldownload/query.do',//'/nccloud/reva/search/query.do',
        data: data,
        success:  (res)=> {
            let { success, data } = res;
            if(success){
                props.button.setDisabled({
                    Add: false,
                    Edit:true,
                    Delete:true,
                    Refresh:false
                });
                if(data && data[tableId]){
                    props.table.setAllTableData(tableId, res.data[tableId]);
                    setDefData(tableId,dataSource,data);
                    if(!backmark || backmark==="0"){
                        let size =  data[tableId].rows.length;
                        let begString =props.MutiInit.getIntl("36101030") && props.MutiInit.getIntl("36101030").get('36101030-000012');
                        let endString = props.MutiInit.getIntl("36101030") && props.MutiInit.getIntl("36101030").get('36101030-000013');
                        let length = res.data[tableId].allpks.length;
                        toast({content: begString + size + endString,color:"success"});/* 国际化处理： 查询成功，共,条*/
                    } 
                }else{
                    props.table.setAllTableData(tableId, {rows:[]});
                    if(!backmark || backmark==="0"){
                        toast({content: props.MutiInit.getIntl("36101030") && props.MutiInit.getIntl("36101030").get('36101030-000011'),color:"warning"});
                    }
                }	
            }
        },
        error : (res)=>{
			console.log(res.message);
		}
    });

};
