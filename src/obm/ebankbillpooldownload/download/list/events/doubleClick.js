import { ajax, toast ,cacheTools} from 'nc-lightapp-front';
import { appid, formId, tableId, pagecode,appcode,searchparam, searchId} from '../constants.js';

export default function doubleClick(record, index, e) {
    let searchVal = this.props.search.getAllSearchData(searchId);
    cacheTools.set(searchparam, searchVal);
    this.props.pushTo('/card', {
        status: 'browse',
        type:'link',
		appcode:appcode,
        id: record.pk_ebankpooldownload.value,
        pagecode:pagecode
    });
}
