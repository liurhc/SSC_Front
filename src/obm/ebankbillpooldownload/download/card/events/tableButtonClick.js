import { createPage, ajax, base, toast } from 'nc-lightapp-front';
import { appid, formId, tableId, pagecode } from '../constants.js';

export default function tableButtonClick(props, key, text, record, index) {
    console.log(key);

    switch (key) {
        //展开
        case 'Openedit':
            props.cardTable.toggleRowView(tableId, record)
            break;   
        case 'Addline':
              props.cardTable.pasteRow(tableId, index);
            break;
        case 'Delline':
            props.cardTable.delRowsByIndex(tableId, index);
            break;
        case 'Copyline':
            props.cardTable.pasteRow(tableId, index);
          break;
        //编辑展开
        case 'editmoreBtn':
            props.cardTable.openModel(tableId, 'edit', record, index);
            break;

    }
}