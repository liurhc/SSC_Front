/**
 * 表体区域
 */
export const tableId = 'body';

/**
 * 表头区域
 */
export const formId = 'head';



/**
 * 页面编码
 */
export const pagecode = '36101029_C01';
export const appcode = '36101029';
export const appid = '0001Z61000000001ZZTL';

/**
 * 编辑态按钮
 */
export const editButtons = ['Save','Cancel','AddLine','DelLine','CopyLine'];

/**
 * 浏览态按钮
 * 
 */
export const browseButtons = ['Add','Delete','Refresh','Edit'];

export const dataSource = 'tm.obm.ebankbillpooldownload.downloadgroup';
export const pkname='pk_ebankpooldownload';