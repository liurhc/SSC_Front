import { base, ajax } from 'nc-lightapp-front';
import { tableId, formId,appid, pagecode,appcode } from "../constants";
import {tableButtonClick} from "./tableButtonClick";

let { NCPopconfirm } = base;

export default function(props) {
	props.createUIDom(
		{
			pagecode: pagecode,//页面id
			appcode:appcode,
			// appid: appid//注册按钮的id
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta)
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
			}
			
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					let status = props.getUrlParam('status');
					//按钮的显示状态
					if (status == 'edit' || status == 'add') {
						props.button.setButtonVisible(['Edit', 'Add', 'Delete'], false);
						props.button.setButtonVisible(['Save', 'Cancel', 'AddLine', 'DelLine','CopyLine'], true);
					} else {
						props.button.setButtonVisible(['Save', 'Cancel', 'AddLine', 'DelLine','CopyLine'], false);
						props.button.setButtonVisible(['Edit', 'Add', 'Delete'], true);
					}
				}
			// }
		}
		
	)
}

function modifierMeta(props, meta) {
	let status = props.getUrlParam('status');
	meta[formId].status = status;
	meta[tableId].status = status; 

	//添加操作列 

	return meta;
}
