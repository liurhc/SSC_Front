import { ajax, base, toast,promptBox } from 'nc-lightapp-front';
import { appid, formId, tableId, pagecode,appcode} from '../constants.js';
export default function(props, id) {
  switch (id) {
    case 'Add':
        props.form.EmptyAllFormValue(formId)
        props.cardTable.setTableData(tableId, { rows: [] })
        props.pushTo('/card', {
          status: 'add',
          id:props.getUrlParam('id'),
          type:'link',
          appcode:appcode,
          pagecode:pagecode
        })
        this.toggleShow();
        break;
    case 'Reback': 
        props.pushTo('/list',{   
            type:'link',
            appcode:appcode,
            pagecode:pagecode
          });
        break;
    case 'Save':
        //保存数据
        this.saveBill();
        break;
    case 'Refresh':
        this.reGetdata();
        this.toggleShow();
        break
    case 'Edit':
        props.pushTo('/card', {
            status: 'edit',
            id: props.getUrlParam('id'),
            type:'link',
            appcode:appcode,
            pagecode:pagecode
        })
        this.toggleShow();
        break;
    case 'Delete':
        promptBox({
            color: "warning",
            title: props.MutiInit.getIntl("36101029") && props.MutiInit.getIntl("36101029").get('36101029-000025'),/* 国际化处理： 删除*/
            content: props.MutiInit.getIntl("36101029") && props.MutiInit.getIntl("36101029").get('36101029-000028'),/* 国际化处理： 确认要删除吗?*/
            beSureBtnClick: () => { 
                this.delConfirm();
            }
        });	 	 
        break
    case 'Cancel':
        promptBox({
            color: "warning",
            title: props.MutiInit.getIntl("36101029") && props.MutiInit.getIntl("36101029").get('36101029-000000'),/* 国际化处理： 取消*/
            content: props.MutiInit.getIntl("36101029") && props.MutiInit.getIntl("36101029").get('36101029-000001'),/* 国际化处理： 确定要取消吗？*/
            beSureBtnClick: () => {
                if ((props.getUrlParam('status') === 'edit')||(props.getUrlParam('status') === 'add')){
                    // 表单返回上一次的值
                    // props.form.cancel(this.formId)
                    // 表格返回上一次的值
                    props.pushTo('/card', {
                        status: 'browse',
                        type:'link',
                        appcode:appcode,
                        id: props.getUrlParam('id'),
                        pagecode:pagecode
                    });  
                }  
                this.reGetdata(); 
                this.toggleShow();
            }
        });
        break
   
    case 'AddLine':
        props.cardTable.addRow(tableId) 
        break;
    case 'DelLine':
        let delcurrRows = props.cardTable.getCheckedRows(tableId);
        let currSelect2 = [];
        if (delcurrRows && delcurrRows.length > 0) {
            for (let item of delcurrRows) {
              currSelect2.push(item.index);
            }
        }
        if (currSelect2.length == 0) {
            toast({ color: 'warning', content: props.MutiInit.getIntl("36101029") && props.MutiInit.getIntl("36101029").get('36101029-000010')/* 国际化处理： 未选中行，请选中数据！*/ });
            return ;
        }
        props.cardTable.delRowsByIndex(tableId, currSelect2);
        break; 
    case 'CopyLine':
        let selectRows = props.cardTable.getCheckedRows(tableId);
        if (selectRows == null || selectRows.length == 0) {
            toast({
              color: 'warning',
              content: props.MutiInit.getIntl("36101029") && props.MutiInit.getIntl("36101029").get('36101029-000010') /* 国际化处理： 未选中行，请选中数据！*/ 
            });
            return false;
        }
        // 粘贴末行位置index
        let copyindex = props.cardTable.getNumberOfRows(tableId, false);
        let selectCopyData = [];
        let selectRowCopy = JSON.parse(JSON.stringify(selectRows));
        for (let item of selectRowCopy) {
            item.data.selected = false;
            item.data.values.pk_ebankpooldownload_b = {
                value: null,
                display: null
            };
            selectCopyData.push(item.data);
        }
        props.cardTable.insertRowsAfterIndex(tableId, selectCopyData, copyindex);         
        break;
        
    default:
      break
  }
}

function refreshCard(that,props) {
    let pk = props.form.getFormItemsValue(formId, 'pk_ebankbillpooldownload').value;
    ajax({
      url: '/nccloud/obm/ebankbillpooldownload/querycard.do',
      data: { pk },
      success: res => {
        if (res.data && res.data.head) {
          props.form.setAllFormValue({ [formId]: res.data.head[formId] });
          updateCache('pk_ebankbillpooldownload', res.data.head[formId].rows[0].values.pk_ebankbillpooldownload.value, res.data, formId, dataSource);
        }
        if (res.data && res.data.body) {
          props.cardTable.setTableData(tableId, res.data.body[tableId]);
        }
        that.props.pushTo("/card",{
          id: pk,
          status: 'browse',
        });
        that.setState({showNCbackBtn: true});
        that.toggleShow();
      }
  
    });

  }


    
