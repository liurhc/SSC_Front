//主子表卡片
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast,high,cardCache,formDownload } from 'nc-lightapp-front';
let { NCFormControl, NCPopconfirm,NCAnchor, NCScrollLink, NCScrollElement, NCAffix,NCBackBtn } = base;
import { tableId, formId, pagecode,appcode, editButtons, browseButtons,dataSource,pkname } from "./constants";
import { buttonClick, initTemplate, afterEvent,pageInfoClick } from './events';
import './index.less';
let {getCacheById,updateCache,addCache}=cardCache;
const{NCUploader}=high;

class Card extends Component {
	constructor(props) {
		super(props);  
		this.state ={
			facode:'',//单据编号,
			//返回箭头
			// showNCbackBtn: false,
			data: []
		};
	}
	componentDidMount() {
		this.refresh();

		if (![ 'browse' ].includes(this.props.getUrlParam('status'))) {
			this.props.button.setButtonVisible(['Refresh'], false);
		}
		
	
	}

	componentWillMount() {
		window.onbeforeunload = () => {
			if (![ 'browse' ].includes(this.props.getUrlParam('status'))) {
				return this.props.MutiInit.getIntl("36101029") && this.props.MutiInit.getIntl("36101029").get('36101029-000002'); /* 国际化处理： 当前单据未保存, 您确定离开此页面吗?*/
			}
		};
	} 

	//通过单据id刷新单据信息
	reGetdata = () => {
		//let netbankinftpcode;	
		let CardData = this.props.createMasterChildData(pagecode, formId, tableId);
		let id=this.props.getUrlParam('id');
		if(id==null){
			return;
		}
		let data; 
		data = { pk: id }; 
		let that = this;
		ajax({
			url: '/nccloud/obm/ebankbillpooldownload/cardquery.do',
			data: data,
			success: (res) => {
				if (res.data) {
					let pk_ebankpooldownload = null;
					if (res.data.head) {											
						this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
						pk_ebankpooldownload = res.data.head[formId].rows[0].values.pk_ebankpooldownload.value;
						let facode = res.data.head[formId].rows[0].values.facode.value;
						this.setState({facode:facode});
					}
					if (res.data.body) {
						this.props.cardTable.setTableData(tableId, res.data.body[tableId]);
					}
					updateCache('pk_ebankpooldownload',pk_ebankpooldownload,res.data,formId,dataSource);
					this.toggleShow();
					} 
			}
		});
	};

	//加载数据
	refresh = () => {
		let netbankinftpcode;	
		let browseflag =false;
		let addflag = false;
		let editflag = false;
		if(this.props.getUrlParam('status') =='browse'){
				browseflag=true;		
				// this.setState({showNCbackBtn: true});	
		}else if(this.props.getUrlParam('status') == 'edit'){
				editflag=true;
		}else if(this.props.getUrlParam('status') == 'add'){
				addflag=true;
		}

		if (browseflag || editflag) {			
				let data; 
				data = { pk: this.props.getUrlParam('id'), pageCode: pagecode }; 
				let that = this;
				ajax({
						url: '/nccloud/obm/ebankbillpooldownload/cardquery.do',
						data: data,
						success: (res) => {
							if (res.data) {
								if (res.data.head) {											
									this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
								
								}
								if (res.data.body) {
									this.props.cardTable.setTableData(tableId, res.data.body[tableId]);
							
								} 
								// //添加	添加或更新缓存
								// //存入缓存
								// let pk_ebankpooldownload = res.data.head[formId].rows[0].values.pk_ebankpooldownload.value;
								// let cardData = getCacheById(pk_ebankpooldownload, dataSource);
								// if(cardData){
								// 	//更新缓存
								// 	updateCache('pk_ebankpooldownload',pk_ebankpooldownload,res.data,formId,dataSource);
								// }else{
								// 	//添加缓存
								// 	addCache(pk_ebankpooldownload,res.data,formId,dataSource);
								// }
								this.toggleShow();
							} else {
								this.props.form.EmptyAllFormValue(formId);
								this.props.cardTable.setTableData(tableId, { rows: [] });
							}
							
						}
			});
		
		}

		if(addflag){	
			this.setState({
				facode:''
			});	 
			this.props.form.setFormItemsValue(formId, { facode: null });  	
			this.toggleShow();
		}
		
		
	} 

	//切换页面状态
	toggleShow = ()  => {
		
		let	status = this.props.getUrlParam("status");
		
		//按钮的显示状态
		if (status == "edit" || status == "add") {
			// this.setState({showNCbackBtn: false});
			this.props.button.setButtonVisible(browseButtons, false);
			this.props.button.setButtonVisible(editButtons, true);
			this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false)
		} else if (status == "browse") {
			// this.setState({showNCbackBtn: true});
			this.props.button.setButtonVisible(editButtons, false);
			this.props.button.setButtonVisible(browseButtons, true);
			this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', true)
		} else {
			// this.setState({showNCbackBtn: false});
			this.props.button.setButtonVisible(editButtons, false);
			this.props.button.setButtonVisible(browseButtons, true);
			this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false)
		}
		this.props.form.setFormStatus(formId, status);
		if (status == "edit" || status == "add"){
			this.props.cardTable.setStatus(tableId, "edit");
		}else{
			this.props.cardTable.setStatus(tableId, status);
		}

		if (status === 'browse') {
			this.props.BillHeadInfo.setBillHeadInfoVisible({
				showBackBtn: true, //控制显示返回按钮: true为显示,false为隐藏 ---非必传
				showBillCode: true //控制显示单据号：true为显示,false为隐藏 ---非必传 
			});
		} else if (status === 'edit') {
			this.props.BillHeadInfo.setBillHeadInfoVisible({
				showBackBtn: false, //控制显示返回按钮: true为显示,false为隐藏 ---非必传
				showBillCode: true //控制显示单据号：true为显示,false为隐藏 ---非必传 
			});
		} else if (status === 'add' || status == "copy") {
			this.props.BillHeadInfo.setBillHeadInfoVisible({
				showBackBtn: false, //控制显示返回按钮: true为显示,false为隐藏 ---非必传
				showBillCode: false //控制显示单据号：true为显示,false为隐藏 ---非必传 
			});
		}
		
	}; 
	
	link2ListPage=()=>{ 
		this.props.pushTo('/list',{   
			type:'link',
			appcode:appcode,
			pagecode:pagecode
		  });
	}

	//删除单据
	delConfirm = () => {
		let CardData = this.props.createMasterChildData(pagecode, formId, tableId);
		let url = '/nccloud/obm/ebankbillpooldownload/delete.do'; 
		ajax({
			url: url,
			data: CardData,
			success: (res) => {
				if (res) {
					this.props.pushTo('/list',{   
						type:'link',
						appcode:appcode,
						pagecode:pagecode
					  });
				}
			}
		});
	};
   
	
	//连接测试
	connectTest= () => {
		let CardData = this.props.createMasterChildData(pagecode, formId, tableId);
		let url = '/nccloud/obm/ebankbillpooldownload/connecttest.do'; //新增保存
		ajax({
			url: url,
			data: CardData,
			success: (res) => {
				let pk_srvconf = null;
				if (res.success) {
					if (res.data) { 
						if (res.data.head && res.data.head[formId]) {
							this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
							pk_srvconf = res.data.head[formId].rows[0].values.pk_srvconf.value;
						}
						if (res.data.body && res.data.body[tableId]) {
							this.props.cardTable.setTableData(tableId, res.data.body[tableId]);
						}
					}
				}				
				this.toggleShow();
			}
		});
		// }
	};
	
	//保存单据 facode ip port servelt overtime usestate weight netbankinftpstyle
	saveBill = () => {
		let CardData = this.props.createMasterChildData(pagecode, formId, tableId);
		let pk_group=CardData.head.head.rows[0].values.pk_group.value
		let facode=CardData.head.head.rows[0].values.facode.value
		if(facode==null||facode==''){
			toast({ 
				color: 'warning', 
				content: this.props.MutiInit.getIntl("36101029") && this.props.MutiInit.getIntl("36101029").get('36101029-000030') /* 国际化处理： 方案不能为空*/ 
			});
			return ;	
		}
	
		let bodyDatas = this.props.cardTable.getAllRows(tableId);
		if(bodyDatas.length>0){
			for (var i=0;i < bodyDatas.length;i++) {
				let pk_banktype=bodyDatas[i].values.pk_banktype.display;
				if(pk_banktype==null || pk_banktype==""){
					let rownum=i+1;
					toast({ 
						color: 'warning', 
						content: this.props.MutiInit.getIntl("36101029") && this.props.MutiInit.getIntl("36101029").get('36101029-000031') +rownum+ this.props.MutiInit.getIntl("36101029") && this.props.MutiInit.getIntl("36101029").get('36101029-000032') /* 国际化处理： 表体第XX行银行类型为空!*/ 
					});
					return ;
				} ;
				let pk_currtype=bodyDatas[i].values.pk_currtype.display;
				if(pk_currtype==null || pk_currtype==""){
					let rownum=i+1;
					toast({ 
						color: 'warning', 
						content: this.props.MutiInit.getIntl("36101029") && this.props.MutiInit.getIntl("36101029").get('36101029-000031')+rownum+this.props.MutiInit.getIntl("36101029") && this.props.MutiInit.getIntl("36101029").get('36101029-000033')  /* 国际化处理： 表体第XX行币种为空!*/ 
					});
					return ;
				} ;
			}
		}else{
			toast({ 
				color: 'warning', 
				content: this.props.MutiInit.getIntl("36101029") && this.props.MutiInit.getIntl("36101029").get('36101029-000034') /* 国际化处理： 表体为空，无法保存！*/ 
			});
			return ;	
		}

		let url = '/nccloud/obm/ebankbillpooldownload/insert.do'; //新增保存
		if (this.props.getUrlParam('status') === 'edit') {
			url = '/nccloud/obm/ebankbillpooldownload/update.do'; //修改保存
		}
		ajax({
			url: url,
			data: CardData,
			success: (res) => {
				let pk_ebankpooldownload = null;
				if (res.success) {
					if (res.data) {
						if (res.data.head && res.data.head[formId]) {
							this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
							pk_ebankpooldownload = res.data.head[formId].rows[0].values.pk_ebankpooldownload.value;
						}
						if (res.data.body && res.data.body[tableId]) {
							this.props.cardTable.setTableData(tableId, res.data.body[tableId]);
						}
					}
				}
				if(url == '/nccloud/obm/ebankbillpooldownload/insert.do'){ 
					addCache(pk_ebankpooldownload,res.data,formId,dataSource);
				}
				// if(url == '/nccloud/obm/ebankbillpooldownload/update.do'){ 
				// 	updateCache('pk_ebankpooldownload',pk_ebankpooldownload,res.data,formId,dataSource);
				// }

				this.props.pushTo('/card', {
					status:'browse',
					type:'link',
					appcode:appcode,
					id: pk_ebankpooldownload,
					pagecode:pagecode
				});
				this.reGetdata();
				this.toggleShow();
				toast({ 
					color: 'success', 
					content: this.props.MutiInit.getIntl("36101029") && this.props.MutiInit.getIntl("36101029").get('36101029-000014') /* 国际化处理： 保存成功*/ 
				});
			},
			error: (res) => {
				toast({ color: 'danger', content: res.message });
			}
		});
	};

	getButtonNames = (codeId) => {
	}; 

	//获取列表肩部信息
	getTableHead = (buttons, tableId) => {
		let { createButton } = this.props.button;
		return (
			<div className="shoulder-definition-area">
				 <div className="definition-icons">
					{this.props.cardTable.createBrowseIcons(tableId, {
						iconArr: ['close', 'open', 'max'],
						maxDestAreaId: 'finance-fts-commissionpayment-card'
					})}
					{/* 应用注册按钮 */}
					  {this.props.button.createButtonApp({
                        area: 'card_body',
                        buttonLimit: 3, 
                        onButtonClick: buttonClick.bind(this), 
                        popContainer: document.querySelector('.header-button-area')
                    })}
				</div>
			</div>
		);
	}; 

	
	render() {
		let { cardTable, form, button,modal,cardPagination,ncmodal } = this.props;
		let buttons = this.props.button.getButtons();
		let multiLang = this.props.MutiInit.getIntl('2052');
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createButtonApp } = this.props.button;
		let { createButton, getButtons} = button;
		let { createCardPagination } = cardPagination;
		let { createModal } = ncmodal; 
		// let { showNCbackBtn} = this.state;
		const { createBillHeadInfo } = this.props.BillHeadInfo;
		const that=this;
		return ( 
			<div className="nc-bill-card">
				<div className="nc-bill-top-area">
					<NCAffix>
						<div className="nc-bill-header-area">
							<div className="header-title-search-area">
								{
									createBillHeadInfo(
										{ 
											title: this.props.MutiInit.getIntl("36101029") && this.props.MutiInit.getIntl("36101029").get('36101029-000008'), /* 国际化处理： 票据池数据下载设置-集团*/
											billCode: this.state.facode,     //单据号
											backBtnClick: () => {           //返回按钮的点击事件
												this.link2ListPage();
											}
										}
									)
								}
							</div>

							<div className="header-button-area">
								{this.props.button.createButtonApp({
									area: 'card_head',
									buttonLimit: 4,
									onButtonClick: buttonClick.bind(this),
									popContainer: document.querySelector('.header-button-area')
								})}
							</div> 
							<div className='header-cardPagination-area' style={{ float: 'right' }}>
								{createCardPagination({ 
									dataSource:dataSource,
									handlePageInfoChange: pageInfoClick.bind(this) 
								})}
							</div>
						</div>
					</NCAffix>
					<NCScrollElement name="forminfo">
						<div className="nc-bill-form-area">
							{createForm(formId, {
								onAfterEvent: afterEvent.bind(this)
							})}
						</div>
					</NCScrollElement>
				</div>
				<div className="nc-bill-bottom-area">
					<NCScrollElement name="businfo">
						<div className="nc-bill-table-area">
							{/* {this.getTableHead(buttons, tableId)} */}
							{createCardTable(tableId, {
								tableHead: this.getTableHead.bind(this, buttons, tableId),
								modelSave: this.saveBill,
								adaptionHeight: true,
								showCheck: true
								// showIndex: true
							})}
						</div>
					</NCScrollElement> 
				</div> 
			</div>
		);
	}
}

Card = createPage({
	mutiLangCode: '36101029',
	initTemplate: initTemplate
})(Card);

// ReactDOM.render(<Card />, document.querySelector('#app'));
export default Card;
