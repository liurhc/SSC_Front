import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base,createPageIcon } from 'nc-lightapp-front';
// const { NCBreadcrumb } = base;
// const NCBreadcrumbItem = NCBreadcrumb.NCBreadcrumbItem;
import { buttonClick, initTemplate, afterEvent, searchBtnClick, pageInfoClick, doubleClick,tableModelConfirm,setButtonUsability } from './events';
import './index.less';
import { dataSource } from '../card/constants'; 
import {tableId,searchId,searchoid,pagecode} from './constants';   

class List extends Component {
	constructor(props) {
		super(props);
		this.props = props;
		this.state = {
			data: {} 
		};
		initTemplate.call(this, props);
	}

	componentDidMount() {
		this.toggleShow();

		//设置初始化的按钮
		let selectDatas =this.props.table.getCheckedRows(tableId);
		if (selectDatas ==null || selectDatas.length == 0) {			
            this.props.button.setDisabled({
				Delete:true,
				Edit:true,				
			});
		} 
	}

	//切换页面状态
	toggleShow = () => {
		let	status = this.props.getUrlParam("status");
		this.props.button.setDisabled({
			Cancel:true,
			Save:true,
			Edit:true,
			Refresh:false
		});

		if (status == "edit" || status == "add") {
			this.setState({showNCbackBtn: false});
			this.props.button.setButtonVisible(browseButtons, false);
			this.props.button.setButtonVisible(editButtons, true);
			this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false)
		}
	}

	//切换页面状态
	toggleShow = (flag) => {
		let	status =flag;
		if (status == "edit" || status == "add") {
			this.setState({showNCbackBtn: false});
			this.props.button.setButtonVisible(browseButtons, false);
			this.props.button.setButtonVisible(editButtons, true);
			this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false)
		}
	}

	deleteAction= () =>{
		let idstr="";
		let index = 0;
		let pk = null;
		let selectDatas = null;
		selectDatas =this.props.table.getCheckedRows(tableId);
		//数据校验
		if (selectDatas ==null || selectDatas.length == 0) {
			toast({ color: 'warning', content: this.props.MutiInit.getIntl("36101030") && this.props.MutiInit.getIntl("36101030").get('36101030-000010')  });
			return ;
		} 
		idstr="";
		index = 0;

		pk = null;
		while (index < selectDatas.length) {
			//获取行主键值
			pk = selectDatas[index].data.values.pk_ebankpooldownload.value;
			//判空
			idstr = idstr + pk;
			if (index<selectDatas.length-1){
				idstr = idstr + ",";
			}
			index++;
		} 
		ajax({
			url: '/nccloud/obm/ebankbillpooldownload/listdelete.do',
			data: { 
				id:idstr,
			},
			success: (res) => {
				if (res) {
					this.refreshAction()
				}
			} 
		});	
	}

/**
 * 刷新
 * @param {*} props 
 */
refreshAction = (props) => {
	let searchVal = props.search.getAllSearchData(searchId);
	let pageInfo = props.table.getTablePageInfo(tableId);
	let metaData = props.meta.getMeta();
	console.log(searchVal);
	let data = {
		conditions: searchVal.conditions ,
		pageInfo: pageInfo,
		pagecode: pagecode,
		queryAreaCode: searchId,
		oid: searchoid,
		queryType: "simple"
	};
	console.log(data);
	ajax({
		url: '/nccloud/obm/ebankbillpooldownload/groupquery.do',//'/nccloud/reva/search/query.do',
		data: data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data && data[tableId]) {
					props.table.setAllTableData(tableId, res.data[tableId]);
					toast({ color: 'success', content: props.MutiInit.getIntl("36101030") && props.MutiInit.getIntl("36101030").get('36101030-000007') /* 国际化处理： 刷新成功*/ });
				} else {
					props.table.setAllTableData(tableId, { rows: [] });					
					toast({ color: 'warning', content: props.MutiInit.getIntl("36101030") && props.MutiInit.getIntl("36101030").get('36101030-000011') /* 国际化处理： 未查询到符合条件的数据*/});
				}
				setDefData(tableId,dataSource,data);
			}
		},
		error: (res) => {
			console.log(res.message);
		}
	});
}

/**
 * 刷新
 */
refreshAction = () => {
	let searchVal = this.props.search.getAllSearchData('search');
	let pageInfo = this.props.table.getTablePageInfo(tableId);
	let metaData = this.props.meta.getMeta();
	console.log(searchVal);
	let data = {
		conditions: searchVal.conditions || searchVal,
		pageInfo: pageInfo,
		pagecode: pagecode,
		queryAreaCode: searchId,
		oid: searchoid,
		queryType: "simple"
	};
	console.log(data);
	ajax({
		url: '/nccloud/obm/ebankbillpooldownload/groupquery.do',//'/nccloud/reva/search/query.do',
		data: data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data && data[tableId]) {
					this.props.table.setAllTableData(tableId, res.data[tableId]);
					toast({ color: 'success', content: props.MutiInit.getIntl("36101029") && props.MutiInit.getIntl("36101029").get('36101029-000007') /* 国际化处理： 刷新成功*/ });
				} else {
					this.props.table.setAllTableData(tableId, { rows: [] });
					toast({ color: 'warning', content: props.MutiInit.getIntl("36101029") && props.MutiInit.getIntl("36101029").get('36101029-000011') /* 国际化处理： 未查询到符合条件的数据*/ });
				}
				setDefData(tableId,dataSource,data);
			}
		},
		error: (res) => {
			console.log(res.message);
		}
	});
}

	render() {
		let { modal } = this.props;
		let { createModal } = modal;
		let buttons = this.props.button.getButtons();
		let { table, button, search } = this.props;
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { createButtonApp } = this.props.button;
		// let { createButton, getButtons} = button;
		
		return (
			<div className="nc-bill-list">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						{/*页面大图标*/}
						{createPageIcon()}
						<h2 className='title-search-detail'>
							{this.props.MutiInit.getIntl("36101029") && this.props.MutiInit.getIntl("36101029").get('36101029-000008') /* 国际化处理： 票据池数据下载设置-集团*/}
						</h2>
					</div>
					<div className="header-button-area">
						{
							this.props.button.createButtonApp({
								area:'list_head',
								buttonLimit:4,
								onButtonClick: buttonClick.bind(this),
								popContainer:document.querySelector('.header-button-area')
							})
						}
					</div>
				</div>
				<div className="nc-bill-search-area">
					{NCCreateSearch(searchId, {
						showAdvBtn: true,
						clickSearchBtn: searchBtnClick.bind(this),
						defaultConditionsNum: 10
					})}
				</div>
				<div className="nc-bill-table-area" style={{ borderRadius: "3px 3px 0 0", overflow: "hidden" }}>
					{createSimpleTable(tableId, {//列表区
						dataSource:dataSource,
						pkname:'pk_ebankpooldownload',
						handlePageInfoChange: pageInfoClick,
						adaptionHeight: true,
						showCheck: true,//复选框
						showIndex: true,//显示序号
						onSelected: setButtonUsability.bind(this, this.props),
						onSelectedAll: setButtonUsability.bind(this, this.props),
						onRowDoubleClick: doubleClick.bind(this)
					})}
				</div>				
			</div>
		);
	}
}

List = createPage({
	mutiLangCode: '36101029'
	// initTemplate: initTemplate
})(List);

// ReactDOM.render(<List />, document.querySelector('#app'));
export default List;
