import { ajax, base, cacheTools } from 'nc-lightapp-front';
let { NCPopconfirm, NCIcon, NCTooltip:Tooltip } = base;
import searchBtnClick from './searchBtnClick';
import tableButtonClick from './tableButtonClick';
import {tableId,searchId,searchparam,searchback,pagecode,appcode,pagecode_card} from '../constants';   

export default function (props) {
	//请求模板数据
	props.createUIDom(
		{
			pagecode: pagecode,//页面id
			appcode: appcode//注册按钮的id
		},
		(data) => {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this,props, meta);
					props.meta.setMeta(meta, () => {

						//如果有缓存条件，查询
						let querycondition = cacheTools.get(searchparam);
						console.log(querycondition);
						if (querycondition) {
							cacheTools.set(searchback, '1');
							props.search.setSearchValue(searchId, querycondition);
							searchBtnClick(props, querycondition);
						}
					});
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
			}
		}
	)
}

function modifierMeta(props, meta) {
	meta[searchId].items = meta[searchId].items.map((item, key) => {
		// item.visible = true;
		item.col = '2';
		return item;
	})

	// 查询条件多选
	meta[searchId].items.find((e) => e.attrcode === 'pk_org').isMultiSelectedEnabled = true;
	//财务组织用户过滤
	meta[searchId].items.map((item)=>{
		if (item.attrcode == 'pk_org' ) {
			       item.queryCondition = () => {
			       		return {
			            	funcode: appcode,
			            	TreeRefActionExt: 'nccloud.web.tmpub.filter.FinanceOrgPermissionFilter'
			        	};
			        };
			     }
	});


	meta[tableId].items = meta[tableId].items.map((item, key) => {
		// item.width = 180;
		if (item.attrcode == 'srcbillcode') {
			item.render = (text, record, index) => {
				let tip = (<div>{record.srcbillcode.value}</div>)
				return (
					<a
						style={{ textDecoration: '', cursor: 'pointer' }}
						onClick={() => {
							props.pushTo('/card', {
								status: 'browse',
								pagecode: pagecode_card,
								appcode:appcode,
								id: record.pk_ebankpooldownload.value
							});
						}}
					>
						{record && record.srcbillcode && record.srcbillcode.value}
					</a>
				);
			};
		}
		return item;
	});

	//添加操作列
	meta[tableId].items.push({
		attrcode: 'opr',
		label: props.MutiInit.getIntl("36101029") && props.MutiInit.getIntl("36101029").get('36101029-000003'),/* 国际化处理： 操作*/
		fixed: 'right',
		width: '200px',
		itemtype: 'customer',
		visible: true,
		render: (text, record, index) => {

			let buttonAry =["EditLine","DeleteLine"];
 
			return props.button.createOprationButton(buttonAry, {
				area: "list_inner",
				buttonLimit: 2,
				onButtonClick: (props, key) => tableButtonClick.call(this,props, key, text, record, index)
			});


		}
	});

	return meta;
}
