import {tableId,searchId,pagecode} from '../constants'; 

export default function clickBtn(props) {
	let selectdata = props.table.getCheckedRows(tableId);
	props.button.setMainButton(['Add','Edit'], false);
	if (!selectdata || selectdata.length == 0) {
		//没有选中
		props.button.setDisabled({
			Delete: true,
			Edit:true,
		    Add:false,
			Refresh:false
		});
		props.button.setMainButton(['Add'], true);
	} else {//选中
		props.button.setDisabled({
			Add:true,
			Delete: false,
			Edit:false,
			Refresh:false
		});
		props.button.setMainButton(['Edit'], true);
	}
}
