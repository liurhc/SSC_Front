import { ajax, base, print,toast,output,cacheTools,promptBox } from 'nc-lightapp-front';
import searchBtnClick from './searchBtnClick';
import { dataSource } from '../../card/constants'; 
import {tableId,searchId,searchoid,appcode,pagecode,pagecode_card} from '../constants';  
export default function buttonClick(props, id) {
	console.log(id)
	let selectDatas = null; 
	switch (id) {
		case 'Add':
		
        		props.pushTo('/card', {
		          status: 'add',
		          type:'link',
		          appcode:appcode,
		          pagecode:pagecode_card
		        }) 
        break;
		case 'Refresh':
			this.refreshAction(props)
			this.toggleShow();
			break;
		case 'Delete':
		    // selectDatas = props.table.getCheckedRows(tableId);
			// if(selectDatas.length==1){
			// 	props.ncmodal.show('delete');
			// }else{
			// 	props.ncmodal.show('batchDel');
			// }
			promptBox({
				color: "warning",
				title: props.MutiInit.getIntl("36101029") && props.MutiInit.getIntl("36101029").get('36101029-000025'),/* 国际化处理： 删除*/
				content: props.MutiInit.getIntl("36101029") && props.MutiInit.getIntl("36101029").get('36101029-000028'),/* 国际化处理： 确认要删除吗?*/
				beSureBtnClick: () => { this.deleteAction()}
			});	 		
			break; 
		case 'output':
			printData = props.table.getCheckedRows(tableId);
			//数据校验
			if (printData.length == 0) {
				toast({ color: 'warning', content: props.MutiInit.getIntl("36101029") && props.MutiInit.getIntl("36101029").get('36101029-000009')});/* 国际化处理： 请选择数据*/
				return
			}
			pks = [];
			printData.forEach((item) => {
				pks.push(item.data.values.pk_ebank_paylog_h.value);
			});
			output({
				url: '/nccloud/obm/ebankpaylog/print.do',
				data: { 
					 oids: pks,
					 outputType: 'output'
				}
			});
			break;
		case 'Print':
			printData = props.table.getCheckedRows(tableId);
			//数据校验
			if (printData.length == 0) {
				toast({ color: 'warning', content: props.MutiInit.getIntl("36101029") && props.MutiInit.getIntl("36101029").get('36101029-000009') });/* 国际化处理：  请选择数据*/
				return
			}
			pks = [];
			printData.forEach((item) => {
				pks.push(item.data.values.pk_ebank_paylog_h.value);
			});
			print(
				'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
				'/nccloud/obm/ebankpaylog/print.do',
				{
					funcode: appcode, //功能节点编码，即模板编码
					appcode: appcode, //模板节点标识
					oids: pks // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
				}
			);
			break;
		case 'Edit':
		    let selectDatas = null;
		    selectDatas =this.props.table.getCheckedRows(tableId);
		    //数据校验
		    if (selectDatas ==null || selectDatas.length == 0) {
				toast({ color: 'warning', content: props.MutiInit.getIntl("36101029") && props.MutiInit.getIntl("36101029").get('36101029-000009') });/* 国际化处理：  请选择数据*/
				return
		    }
		    props.pushTo('/card',{ 
			status:'edit',
			type:'link',
			appcode:appcode,
			id:selectDatas[0].data.values.pk_ebankpooldownload.value,
			pagecode:pagecode_card
		})
		break;
		default:
			break
	}
}

/**
 * 打印、预览
 * @param {*} props 
 */
function printData(props) {
	let printData = props.table.getCheckedRows(tableId);
	//数据校验
	if (printData.length == 0) {
		toast({ color: 'warning', content: props.MutiInit.getIntl("36101029") && props.MutiInit.getIntl("36101029").get('36101029-000009') });/* 国际化处理：  请选择数据*/
		return
	}
	let pks = [];
	printData.forEach((item) => {
		pks.push(item.data.values.pk_ebank_paylog_h.value);
	});
	print(
		'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
		'/nccloud/obm/ebankpaylog/print.do',
		{
			funcode: appcode, //功能节点编码，即模板编码
			appcode: appcode, //模板节点标识
			// printTemplateID: '1001Z61000000000LPN4',//列表打印模板pk 
			oids: pks // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
		}
	);
}


/**
 * 刷新
 * @param {*} props 
 */
const refreshAction = (props) => {
	let searchVal = props.search.getAllSearchData(sreachId);
	let pageInfo = props.table.getTablePageInfo(tableId);
	let metaData = props.meta.getMeta();
	console.log(searchVal);
	let data = {
		conditions: searchVal.conditions || searchVal,
		pageInfo: pageInfo,
		pagecode: pagecode,
		queryAreaCode: "search",
		oid: searchoid,
		queryType: "simple"
	};
	console.log(data);
	ajax({
		url: '/nccloud/obm/ebankbillpooldownload/groupquery.do',//'/nccloud/reva/search/query.do',
		data: data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data && data[tableId]) {
					props.table.setAllTableData(tableId, res.data[tableId]); 
					toast({ color: 'success', content: props.MutiInit.getIntl("36101029") && props.MutiInit.getIntl("36101029").get('36101029-000007') });/* 国际化处理：  刷新成功*/
				} else {
					props.table.setAllTableData(tableId, { rows: [] });
					toast({ color: 'warning', content: props.MutiInit.getIntl("36101029") && props.MutiInit.getIntl("36101029").get('36101029-000011') });/* 国际化处理：  未查询到符合条件的数据*/
				}
				setDefData(tableId,dataSource,data);
			}
		},
		error: (res) => {
			console.log(res.message);
		}
	});
}
