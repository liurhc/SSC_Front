//主子表卡片
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast,print,output,cardCache,high,formDownload } from 'nc-lightapp-front';
// let { NCFormControl } = base;
let { NCFormControl, NCAnchor, NCScrollLink, NCScrollElement, NCAffix,NCBackBtn } = base;
import { tableId, formId,searchId,inputId, appcode,pagecode, editButtons, browseButtons,card_page_url,list_page_url, browseButtons1, browseButtons2,browseButtons3,browseButtons4,browseButtons5,browseButtons6,browseButtons7,browseButtons8,dataSource } from "./constants";
import { buttonClick, initTemplate, afterEvent,pageInfoClick } from './events';
let { getNextId, getCurrentLastId, deleteCacheById, deleteCacheId, getCacheById, updateCache, addCache, setDefData, getDefData } = cardCache;

//import './index.less';  
// import Sign from '../../../../tmpub/pub/util/ca'; 
const { NCModal,NCButton,NCUpload } = base;
const {NCUploader} = high;
const { ApproveDetail} = high;

class Card extends Component {
	constructor(props) {
		super(props);  
		this.state ={
			vbillno:'',//单据编号,
			//返回箭头
			showNCbackBtn: true,
			data: [],
			showmodal : false,
			currdigist:0,
			// 附件相关 start
			//单据pk
			billId:'',
			//附件管理使用单据编号
            billno:'',
			//控制附件弹出框
            showUploader: false,
			//控制弹出位置
            target: null,
			// 附件相关 end
			billid: '',
			billtype: '',
			showAppr: false,
			//精度
			digistnum:'8'
		};
		this.upload_change = this.upload_change.bind(this);
		this.importProps = {
			name: 'file',
			action: '/nccloud/obm/ebankdfgz/upload.do',
			// accept:'text/xml, application/xml, text/xls, application/xls',
			data: {},
			multiple: false,
			// beforeUpload: this.beforeUpload,
			headers: {
				authorization: 'authorization-text',
			},
			onChange: this.upload_change,
		};
		initTemplate.call(this, props);
	}

	//文件上传状态改变处理
	upload_change(info) {
		if (info.file.status !== 'uploading') {
			console.log(info.file, info.fileList);
		}
		if (info.file.status === 'done') {

			if (info.file.response && info.file.response.success) {

				let { data } = info.file.response;
				if (data) { 
					let pathvalue = {value:data,display:data};  
					this.props.form.setFormItemsValue(inputId, { path: pathvalue }); 
				} 
			} else {
				toast({ content: info.file.response.error.message, color: "warning" });
			} 
		} else if (info.file.status === 'error') {
			info.fileList.splice(0, info.fileList.length);
		}
	}

	componentWillMount() {
		
		// 关闭浏览器
		window.onbeforeunload = () => {
			let status = this.props.getUrlParam("status");
			if (status != 'browse') {
				/* 国际化处理： 当前单据未保存，您确认离开此页面？*/
				return this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000009');/* 国际化处理： 当前单据未保存，您确认离开此页面？*/
			}
		};
	}


	componentDidMount() {		
		let status = this.props.getUrlParam('status');
		let pk = this.props.getUrlParam('id');
		
			if(status == 'add' ){
				this.toggleShow();
			}
			if (status == 'edit') {
				this.getdata();	
			}
			if (status == 'copy') {
					this.getdata();	
			}
			if (status == 'browse') {
				if(pk){
					this.getdata();	
				}else{
			//清空数据
			this.props.form.EmptyAllFormValue(formId);
			this.props.cardTable.setTableData(tableId, { rows: [] });
			this.props.pushTo("/card",{
				id: '',
				status: 'browse',
			});
			this.setState({ showNCbackBtn: false });
			this.props.button.setButtonVisible(browseButtons, false);	
			//this.props.button.setButtonVisible(browseButtons6, true);
			//this.props.button.setMainButton(['add'], true);
			this.props.button.setButtonVisible(['addLine', 'delLine', 'copyLine'], false);
            this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false)
		}
				
			}  
		
		
	}

	
	//通过单据id查询单据信息
	getdata = () => {
		let data = { pk: this.props.getUrlParam('id'), pageCode: pagecode }; 
			
		ajax({
			url: '/nccloud/obm/ebankdfgz/querycard.do',
			data: data,
			success: (res) => {
				if (res.data) {
					if (res.data.head) {											
						this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
						let vbillno =res.data.head[formId].rows[0].values.vbillno.value;
						this.setState({
							vbillno:vbillno
						});
					}
					if (res.data.body) {
						this.props.cardTable.setTableData(tableId, res.data.body[tableId]);
					}						
					
				} else {
					this.props.form.EmptyAllFormValue(formId);
					this.props.cardTable.setTableData(tableId, { rows: [] });
					
				}
				this.toggleShow();
			}
		});
	}
	//切换页面状态
	toggleShow = () => {
			
		let status = this.props.getUrlParam("status");
		//新增时，清除数据
		if(status == "add"){ 
				// 清空表单form所有数据
				this.props.form.EmptyAllFormValue(formId);
				this.setState({vbillno:''});	
				this.props.cardTable.setTableData(tableId, { rows: [] });
				//新增时除了主组织可编辑 其他字段都不能编辑
				this.props.initMetaByPkorg();
				this.props.button.setButtonVisible(['addLine', 'delLine', 'copyLine'], true);
				this.props.button.setButtonDisabled(['addLine', 'delLine', 'copyLine'], true);
				this.props.button.setButtonVisible(browseButtons, false);
			    this.props.button.setButtonVisible(editButtons, true);
			    this.props.button.setDisabled({addLine:false,insLine:false,delLine:false,copyLine:false});
			     //设置卡片翻页的显隐性
			    this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false);
			    

			    this.props.cardTable.setStatus(tableId, "edit");
			    this.props.form.setFormStatus(formId, "edit");
			    //this.props.resMetaAfterPkorgEdit();
		}

			
		if (status == "edit") {
			this.props.button.setButtonVisible(browseButtons, false);
			this.props.button.setButtonVisible(editButtons, true);
			this.props.button.setDisabled({addLine:false,insLine:false,delLine:false,copyLine:false});
			//设置卡片翻页的显隐性
			this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false);
			this.props.button.setButtonVisible(['addLine', 'delLine', 'copyLine'], true);
			this.props.cardTable.setStatus(tableId, "edit");
			this.props.form.setFormStatus(formId, "edit");
			//this.props.resMetaAfterPkorgEdit();
			}
			//设置按钮的可用状态
		if (status == "browse"){
			this.props.button.setButtonVisible(editButtons, false);
			this.props.button.setButtonVisible(browseButtons, false);	
			this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', true);	
			this.props.button.setButtonVisible(['addLine', 'delLine', 'copyLine'], false);
			this.props.cardTable.setStatus(tableId, "browse");
			this.props.form.setFormStatus(formId, "browse");
			this.props.button.setMainButton(['add'], false);
			this.props.button.setMainButton(['commit','unCommit'], false);
			this.props.button.setMainButton(['download'], false);
			if (this.props.form.getFormItemsValue(formId, 'vbillstatus')) {
				let vbillstatus = this.props.form.getFormItemsValue(formId, 'vbillstatus').value;
				if (vbillstatus === '0') {
					//审批未通过
					this.props.button.setButtonVisible(browseButtons2, true);
					this.props.button.setMainButton(['unCommit'], true);  
				} else if (vbillstatus === '1') {
					//审批通过 
					if (this.props.form.getFormItemsValue(formId, 'billpaystate')) {
						let billpaystate = this.props.form.getFormItemsValue(formId, 'billpaystate').value;
						if (billpaystate === '2') {
							//支付不明
							this.props.button.setButtonVisible(browseButtons5, true);
							this.props.button.setMainButton(['download'], true); 
						} else if (billpaystate === '0') {
							//支付成功
							this.props.button.setButtonVisible(browseButtons8, true);
							this.props.button.setMainButton(['add'], true);
						} else if (billpaystate === '1') {
							//支付失败
							this.props.button.setButtonVisible(browseButtons7, true);
							this.props.button.setMainButton(['add'], true);
						}
						else {
							//尚未支付
							this.props.button.setButtonVisible(browseButtons4, true);
							this.props.button.setMainButton(['unCommit'], true);
						}
					} else {
						this.props.button.setButtonVisible(browseButtons4, true);
						this.props.button.setMainButton(['unCommit'], true);
					}

				} else if (vbillstatus === '2') {
					//审批进行中
					this.props.button.setButtonVisible(browseButtons3, true);
					this.props.button.setMainButton(['add'], true); 
				} else if (vbillstatus === '-1') {
					//自由态
					this.props.button.setButtonVisible(browseButtons1, true);
					this.props.button.setMainButton(['commit'], true); 
				}else if (vbillstatus === '3') {
					//提交
					this.props.button.setButtonVisible(browseButtons3, true);
					this.props.button.setMainButton(['unCommit'], true); 
				} else {
					//空白单据
					this.props.button.setButtonVisible(browseButtons6, true);
					this.props.button.setMainButton(['add'], true);
				}	
			}else{
				this.props.button.setButtonVisible(browseButtons6, true);	
				this.props.button.setMainButton(['add'],true);
			}						
		}

		if(status == "copy"){	
				this.props.button.setButtonVisible(browseButtons, false);
			    this.props.button.setButtonVisible(editButtons, true);
			    this.props.button.setDisabled({addLine:false,insLine:false,delLine:false,copyLine:false});
			    //设置卡片翻页的显隐性
			    this.props.cardPagination.setCardPaginationVisible('cardPaginationBtn', false);
				this.props.button.setButtonVisible(['addLine', 'delLine', 'copyLine'], true);
                this.props.cardTable.setStatus(tableId, "edit");
			    this.props.form.setFormStatus(formId, "edit");
			    this.props.resMetaAfterPkorgEdit();
				this.props.form.setFormItemsDisabled(formId, { 'pk_org': true });
				this.props.form.setFormItemsDisabled(formId, { 'pk_banktype': false });
				this.props.form.setFormItemsDisabled(formId, { 'pk_currtype': false });
				this.props.form.setFormItemsDisabled(formId, { 'salaryperiod': false });
				this.props.form.setFormItemsDisabled(formId, { 'busnar': false });
				this.props.form.setFormItemsDisabled(formId, { 'nusage': false });
				this.props.form.setFormItemsDisabled(formId, { 'crdttyp': false });
				this.setState({vbillno:''});	 
				let tempstatus = {"display":this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000010'),"value":"-1"};/* 国际化处理： 自由*/
				this.props.form.setFormItemsValue(formId, { vbillno: null }); 
				this.props.form.setFormItemsValue(formId, { pk_dfgz: null });
				this.props.form.setFormItemsValue(formId, { vbillstatus: tempstatus });
				this.props.form.setFormItemsValue(formId, { modifier: null });
				this.props.form.setFormItemsValue(formId, { modifiedtime: null });
				this.props.form.setFormItemsValue(formId, { submitdate: null });
				this.props.form.setFormItemsValue(formId, { submiter: null });
				this.props.form.setFormItemsValue(formId, { submittime: null });
				this.props.form.setFormItemsValue(formId, { approvedate: null });
				this.props.form.setFormItemsValue(formId, { approvenote: null });
				this.props.form.setFormItemsValue(formId, { approver: null });
				this.props.form.setFormItemsValue(formId, { approvetime: null });
				this.props.form.setFormItemsValue(formId, { gensrc: null });
				this.props.form.setFormItemsValue(formId, { ts: null });
				this.props.form.setFormItemsValue(formId, { paypackageid: null }); 
				this.props.form.setFormItemsValue(formId, { billpaystate: null }); 
				this.props.form.setFormItemsValue(formId, { paymsg: null }); 
				this.props.form.setFormItemsValue(formId, { paydate: null }); 
				this.props.form.setFormItemsValue(formId, { factpaytotal: null }); 
				this.props.form.setFormItemsValue(formId, { successnum: null }); 
				this.props.form.setFormItemsValue(formId, { failednum: null }); 
				this.props.form.setFormItemsValue(formId, { dbtacc: null }); 
				this.props.form.setFormItemsValue(formId, { bbknbr: null }); 
				this.props.form.setFormItemsValue(formId, { dealstate: null }); 
				this.props.form.setFormItemsValue(formId, { gennewbillno: null }); 
				this.props.cardTable.setColValue(tableId,'paystate',{'value':null,'display':null});
				this.props.cardTable.setColValue(tableId,'errmsg',{'value':null,'display':null});
				this.props.cardTable.setColValue(tableId,'pk_dfgz',{'value':null,'display':null});
				this.props.cardTable.setColValue(tableId,'pk_dfgz_b',{'value':null,'display':null});
				this.props.cardTable.setColValue(tableId,'childpackageid',{'value':null,'display':null});
				this.props.cardTable.setColValue(tableId,'paypackageid',{'value':null,'display':null});
				this.props.cardTable.setColValue(tableId,'headpackageid',{'value':null,'display':null});
				this.props.cardTable.setColValue(tableId,'confirmflag',{'value':null,'display':null});
				this.props.cardTable.setColValue(tableId,'confirminfo',{'value':null,'display':null});
				this.props.cardTable.setColValue(tableId,'confirmuser',{'value':null,'display':null});
				this.props.cardTable.setColValue(tableId,'confirmtime',{'value':null,'display':null});
				this.props.cardTable.setColValue(tableId,'ts',{'value':null,'display':null});
				this.props.cardTable.setColValue(tableId,'pk_org',{'value':null,'display':null});
				this.props.cardTable.setColValue(tableId,'pk_group',{'value':null,'display':null});
			
		}
 
		if (status === 'browse') {
			this.props.BillHeadInfo.setBillHeadInfoVisible({
				showBackBtn: true, //控制显示返回按钮: true为显示,false为隐藏 ---非必传
				showBillCode: true //控制显示单据号：true为显示,false为隐藏 ---非必传 
			});
		} else if (status === 'edit') {
			this.props.BillHeadInfo.setBillHeadInfoVisible({
				showBackBtn: false, //控制显示返回按钮: true为显示,false为隐藏 ---非必传
				showBillCode: true //控制显示单据号：true为显示,false为隐藏 ---非必传 
			});
		} else if (status === 'add' || status == "copy") {
			this.props.BillHeadInfo.setBillHeadInfoVisible({
				showBackBtn: false, //控制显示返回按钮: true为显示,false为隐藏 ---非必传
				showBillCode: false //控制显示单据号：true为显示,false为隐藏 ---非必传 
			});
		}
			
	}; 

	//删除单据
	delDfgz = () => {
		ajax({
			url: '/nccloud/obm/ebankdfgz/delete.do',
			data: {
				id: this.props.getUrlParam('id'),
				ts: this.props.form.getFormItemsValue(formId, 'ts').value
			},
			success: (res) => {
				if (res) {
					//this.props.pushTo("/list",{
					//	type:'link',
					//	appcode: appcode,
					//	pagecode:pagecode}
					//);
					toast({ color: 'success', content: this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000036') }); /* 国际化处理： 删除成功*/
					let deleteid = this.props.getUrlParam("id");
					//根据当前id,获取下个id
					/*
					* id：数据主键的值
					* dataSource: 缓存数据命名空间
					*/
					let deletenextId = getNextId(deleteid, dataSource);
					//调用删除缓存数据方法
					/*
					* idname: 数据主键的命名
					* id：数据主键的值
					* dataSource: 缓存数据命名空间
					*/
					deleteCacheById('pk_dfgz', deleteid, dataSource);
					//根据nextId查询下条数据
					//注意：查询下条数据时，也同样需要先判断有没有缓存数据，没有缓存数据时，再发查询请求。
					this.props.setUrlParam({
						status: 'browse',
						id: deletenextId ? deletenextId:'',
					});
					this.setState({
						vbillno: null
				    });
					this.refreshCard(this.props.getUrlParam("id"));
				}
			}
		});
	};

// 保存前校验
saveBillBeforeCheck = () => {
	
	let rowCount = this.props.cardTable.getNumberOfRows(tableId);
	if(rowCount <= 0){
		toast({ color: 'warning', content: this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000011') });/* 国际化处理： 请录入表体数据!*//* 国际化处理： 请录入表体数据!*/
		return false;
	}
	
	// form必输项校验
	let flag = this.props.form.isCheckNow(formId);
	//table必输项校验
	let tableflag = this.props.cardTable.checkTableRequired(tableId);
	return flag && tableflag;
};

	//保存单据
	saveBill = () => {
		if (this.props.getUrlParam('status') == 'copy') {
			this.props.form.setFormItemsValue(formId, { pk_dfgz: null });
			this.props.form.setFormItemsValue(formId, { ts: null });
		} 
		let saveflag = this.saveBillBeforeCheck();
		if(!saveflag)
		    return;
		let CardData = this.props.createMasterChildData(pagecode, formId, tableId); 
		let saveBeforePk = this.props.form.getFormItemsValue(formId, 'pk_dfgz');
		let url = '/nccloud/obm/ebankdfgz/insert.do'; //新增保存
		if (this.props.getUrlParam('status') == 'edit') {
			url = '/nccloud/obm/ebankdfgz/update.do'; //修改保存
		}
		ajax({
			url: url,
			data: CardData,
			success: (res) => {
				if (res.success) {
					if (res.data) {
						toast({ color: 'success', content: this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000012') });/* 国际化处理： 保存成功*/
						/*
						* idname: 数据主键的命名
						* id：数据主键的值
						* headAreacode: 卡片表头的区域编码
						* dataSource: 缓存数据命名空间
						*/
						
						if(saveBeforePk && saveBeforePk.value){
							updateCache('pk_dfgz', res.data.head[formId].rows[0].values.pk_dfgz.value, res.data, formId, dataSource);
						}else{
							addCache(res.data.head[formId].rows[0].values.pk_dfgz.value, res.data, formId, dataSource);
						}
						
						this.props.pushTo("/card", {
							status: 'browse',
							id: res.data.head[formId].rows[0].values.pk_dfgz.value,
							type:'link',
							appcode: appcode,
							pagecode:pagecode
						});
						this.componentDidMount();
					}
				}
				toast({ content: this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000012'), color: "success" });/* 国际化处理： 保存成功*/
			}
		});
		// }
	};

	//保存新增单据
	saveAddBill = () => {
		if (this.props.getUrlParam('status') == 'copy') {
			this.props.form.setFormItemsValue(formId, { pk_dfgz: null });
			this.props.form.setFormItemsValue(formId, { ts: null });
		}
		let flag =this.props.form.isCheckNow(formId);
		if(!flag)
		    return;
		//过滤表格空行 
		let CardData = this.props.createMasterChildData(pagecode, formId, tableId);
		let saveBeforePk = this.props.form.getFormItemsValue(formId, 'pk_dfgz');
		let url = '/nccloud/obm/ebankdfgz/insert.do'; //新增保存
		if (this.props.getUrlParam('status') == 'edit') {
			url = '/nccloud/obm/ebankdfgz/update.do'; //修改保存
		}
		ajax({
			url: url,
			data: CardData,
			success: (res) => {
				let pk_dfgz = null;
				if (res.success) {
					if (res.data) {
						if(saveBeforePk && saveBeforePk.value){
							updateCache('pk_dfgz', res.data.head[formId].rows[0].values.pk_dfgz.value, res.data, formId, dataSource);
						}else{
							addCache(res.data.head[formId].rows[0].values.pk_dfgz.value, res.data, formId, dataSource);
						}
						toast({ color: 'success', content: this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000012') }); /* 国际化处理： 保存成功*/
						this.props.pushTo("/card", {
							status: 'add' ,
							type:'link',
							appcode: appcode,
							pagecode:pagecode
						});
						this.componentDidMount();
					}
				}
				toast({ content: this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000012'), color: "success" });/* 国际化处理： 保存成功*/
			}
		});
		// }
	};

	//保存提交单据
	saveCommitBill = (id,msg) => {
		if (this.props.getUrlParam('status') == 'copy') {
			this.props.form.setFormItemsValue(formId, { pk_dfgz: null });
			this.props.form.setFormItemsValue(formId, { ts: null });
		} 
		let flag =this.props.form.isCheckNow(formId);
		if(!flag)
		    return;
		let CardData = this.props.createMasterChildData(pagecode, formId, tableId); 
		let saveBeforePk = this.props.form.getFormItemsValue(formId, 'pk_dfgz');
		let url = '/nccloud/obm/ebankdfgz/insert.do'; //新增保存
		if (this.props.getUrlParam('status') == 'edit') {
			url = '/nccloud/obm/ebankdfgz/update.do'; //修改保存
		}
		ajax({
			url: url,
			data: CardData,
			success: (res) => {


				let pk_dfgz = null;
				if (res.success) {
					if (res.data) { 
						let tsstr="";
						pk_dfgz = res.data.head[formId].rows[0].values.pk_dfgz.value;
						tsstr = res.data.head[formId].rows[0].values.ts.value; 
						//刷新页面
						this.refreshCard(pk_dfgz);
						if (pk_dfgz != null) {		
							ajax({
								url: '/nccloud/obm/ebankdfgz/commit.do',
								data: { 
									id:pk_dfgz,
									ts:tsstr
								},
								success: (res) => {
									let { success, data } = res;
									if (success) {
										if(saveBeforePk && saveBeforePk.value){
											updateCache('pk_dfgz', pk_dfgz, res.data, formId, dataSource);
										}else{
											addCache(pk_dfgz, res.data, formId, dataSource);
										}
										//刷新页面
										this.refreshCard(pk_dfgz);
										toast({ color: 'success', content: this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000013') }); /* 国际化处理： 保存提交成功*/
									}
									toast({ content: this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000012'), color: "success" });/* 国际化处理： 保存成功*/
								}
							});
						} 
					}
				}
				
			}
		});
		// }
	};

	//点击导入按钮，打开导入对话框
    InputClick = () => {
		let defaultmsg = "{NC_HOME}/resources/ebank/NC_Dfgz_Templet.xls";
		let msgvalue = {value:defaultmsg,display:defaultmsg};		
		this.props.form.setFormItemsValue(inputId,{templet:msgvalue});
		let filetype = "EXCEL";
		let filetypevalue = {value:filetype,display:filetype};
		this.props.form.setFormItemsValue(inputId,{filetype:filetypevalue});
		// this.props.form.setFormItemsValue();
		this.setState(
			{
				showmodal: true
			}
		)
	}

	//点击取消按钮，关闭导入页面
    InputCancelClick = () => {
        this.setState(
			{
				showmodal: false
			}
		)
	}  

	//点击确定按钮，开启导入页面
    InputOkClick = () => {

		let path = this.props.form.getFormItemsValue(inputId, 'path').value
		if(!path){
			toast({ color: 'warning', content: this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000014') });/* 国际化处理： 请选择导入文件*/
			return;
		}

		let data = {
			cardflag:'0', 
			path: path 
		};
		ajax({
			url: '/nccloud/obm/ebankdfgz/input.do',
			data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					//if (data && data[tableId]) {
					//	this.props.table.setAllTableData(tableId, data[tableId]);
					//} else {
					//	this.props.table.setAllTableData(tableId, { rows: [] });
					//};
					if (res.data.head) {											
						this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
						let vbillno =res.data.head[formId].rows[0].values.vbillno.value;
						this.setState({
							vbillno:vbillno
						});
					}
					if (res.data.body) {
						this.props.cardTable.setTableData(tableId, res.data.body[tableId]);
					}						
                    //刷新页面
                    //this.refreshCard(res.data.head[formId].rows[0].values.pk_dfgz.value);
					
					this.setState(
						{
							showmodal: false
						}
					);
				}
			}
		}); 
	}

	//点击下载模板按钮，下载模板
	DownloadFileClick= () => {
		let data = 'xls';		
		formDownload({
			params: {data:data},
			url: '/nccloud/obm/ebankdfgz/filedownload.do',
			enctype:1
		});
	} 

	dealWithData  = (id,msg) => {
		let pk_dfgz="";
		let tsstr="";
		pk_dfgz = this.props.form.getFormItemsValue(formId, 'pk_dfgz').value;
		tsstr = this.props.form.getFormItemsValue(formId, 'ts').value; 
		if (pk_dfgz != null) {		
			ajax({
				url: '/nccloud/obm/ebankdfgz/' + id + '.do',
				data: { 
					id:pk_dfgz,
					ts:tsstr
				},
				success: (res) => {
					let { success, data } = res;
					if (res.success) {
						if(res.data && res.data.errorMsg){
						  toast({ color: 'warning', content: res.data.errorMsg });
						}else{
						  toast({ color: 'success', content: this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000015') });/* 国际化处理： 已成功*/
						  this.refreshCard(pk_dfgz);
						}
						
					  }
				}
			});
		}
	}
	
	//刷新卡片
	refreshCard = (pk_dfgz)  => {		
		this.props.pushTo("/card", {
			status: 'browse',
			id: pk_dfgz,
			type:'link',
            appcode: appcode,
            pagecode:pagecode
		});
		this.componentDidMount();
	}

	//打印
	printData = () =>{
		let pk = this.props.form.getFormItemsValue(formId, 'pk_dfgz').value;
		let pks = [];
		pks.push(pk); 
		print(
			'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
			'/nccloud/obm/ebankdfgz/print.do',
			{
				nodekey: 'NCCLOUD', //模板节点标识
                oids: pks // 功能节点的数据主键
				//billtype: '36C9', //单据类型
				//funcode: '3610PAYR', //功能节点编码，即模板编码   
				//oids: pks // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
			},
			false
		);
	}

	//输出
	ouputData = () =>{
		let pk = this.props.form.getFormItemsValue(formId, 'pk_dfgz').value;
		let pks = [];
		pks.push(pk); 
		output({
			url: '/nccloud/obm/ebankdfgz/print.do',
			data: { 
			 	oids: pks,
			  	outputType: 'output'
			}
		});
	}

	getButtonNames = (codeId) => {
		// if (codeId === 'edit' || codeId === 'add') {
		// 	return 'main-button';
		// } else {
		// 	return 'secondary-button';
		// }
	}; 

	//获取列表肩部信息
	getTableHead = (buttons, tableId) => {
		let { createButtonApp } = this.props.button;
		return (
			<div className="shoulder-definition-area">
				 <div className="definition-icons">
					{this.props.cardTable.createBrowseIcons(tableId, {
						iconArr: ['close', 'open', 'max'],
						maxDestAreaId: 'finance-fts-commissionpayment-card'
					})}
					{/* 应用注册按钮 */}
					  {this.props.button.createButtonApp({
                        area: 'card_body',
                        buttonLimit: 3, 
                        onButtonClick: buttonClick.bind(this), 
                        popContainer: document.querySelector('.header-button-area')
                    })}
				</div>
			</div>
		);
	}; 

	// 附件的关闭点击
    onHideUploader = () => {
        this.setState({
            showUploader: false
        })
	}

	ApproveInfo = (pk) => {		
		let pk_linkDfgz = pk; //单据pk
		let pk_linktradetype = '36C9'; //单据类型 
		this.setState(
			{
				billid: pk_linkDfgz, //单据pk
				billtype: pk_linktradetype
			},
			() => {
				this.setState({
					showAppr: true
				});
			}
		);
	}
	
	//关闭联查审批意见
	closeApprove = () => {
		this.setState({
			showAppr: false
		});
	};

    // beforeUpload(billId, fullPath, file, fileList) {  
    //     // 参数：单据id，当前选中分组path、当前上传文件对象，当前文件列表
    //     console.log(billId, fullPath, file, fileList);

    //     const isJPG = file.type === 'image/jpeg';
    //     if (!isJPG) {
    //         alert('只支持jpg格式图片')
    //     }
    //     const isLt2M = file.size / 1024 / 1024 < 2;
    //     if (!isLt2M) {
    //         alert('上传大小小于2M')
    //     }
    //     return isJPG && isLt2M;
    //     // 备注： return false 不执行上传  return true 执行上传
	// }

	link2ListPage = () => {
		
		this.props.pushTo("/list",{
			status: 'browse'
		}
	);
	}

	render() {
		// let { form } = this.props;s
		let { cardTable, form, button,modal,ncmodal,cardPagination } = this.props;
		let buttons = this.props.button.getButtons();
		let multiLang = this.props.MutiInit.getIntl('2052');
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createButtonApp } = this.props.button;
		let { getButtons} = button;
		let { createCardPagination } = cardPagination;
		let { createModal } = modal; 
		let createNCModal = ncmodal.createModal;
		let { showUploader, target, billno, billId,showNCbackBtn } = this.state;
		const { createBillHeadInfo } = this.props.BillHeadInfo;
		let status = this.props.getUrlParam('status');
		return ( 

			<div className="nc-bill-card">
				<div className="nc-bill-top-area">
					<NCAffix>
						<div className="nc-bill-header-area">
							<div className="header-title-search-area">
							{
									createBillHeadInfo(
										{
											title: this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000016'),  //标题
											billCode: this.state.bill_code,     //单据号
											backBtnClick: () => {           //返回按钮的点击事件
												this.link2ListPage();
											}
										}
									)}
							</div> 
							<div className="header-button-area">
								{this.props.button.createButtonApp({
									area: 'card_head', 
									onButtonClick: buttonClick.bind(this),
									popContainer: document.querySelector('.header-button-area')
								})}
							</div>
							<div className='header-cardPagination-area' style={{ float: 'right' }}>
								{createCardPagination({
									handlePageInfoChange: pageInfoClick.bind(this),
									// dataSource: dataSource
								})}
							</div>
						</div>
					</NCAffix>  				
					<NCScrollElement name="forminfo">
						<div className="nc-bill-form-area">
							{createForm(formId, {
								onAfterEvent: afterEvent.bind(this)
							})}
						</div>
					</NCScrollElement>
				</div>
				<div className="nc-bill-bottom-area">
					<NCScrollElement name="businfo">
						<div className="nc-bill-table-area">
							{/* {this.getTableHead(buttons, tableId)} */}
							{createCardTable(tableId, {
								tableHead: this.getTableHead.bind(this, buttons, tableId),
								modelSave: this.saveBill,
								onAfterEvent: afterEvent.bind(this),
								adaptionHeight: true,
								showCheck: true,
								showIndex: true
							})}
						</div>
					</NCScrollElement> 
				</div>
				{createModal('delete', {
					title: this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000018'),/* 国际化处理： 删除*/
					content: this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000019'),/* 国际化处理： 确定要删除吗？*/
					beSureBtnClick: this.delDfgz.bind(this),
					//  模态框大小 sm/lg/xlg,
					size:'sm', 
					className:'junior'
				})}
				
				<NCModal show={this.state.showmodal} onHide={this.InputCancelClick.bind(this)} style={{width: '520' }} className={'senior'}>
					<NCModal.Header closeButton={true}>
						<NCModal.Title>{this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000020')}</NCModal.Title>{/* 国际化处理： 导入*/}
					</NCModal.Header>

					<NCModal.Body>
						<div >
                            {
                                createForm(inputId, 
                                    {
                                    //编辑后事件
                                    onAfterEvent: afterEvent.bind(this)
                                    }
                                )
							}     
						</div> 						
					</NCModal.Body>

					<NCModal.Footer>						 
						<NCButton style={{float:'right'}} onClick={this.InputCancelClick.bind(this)}>{this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000004')}</NCButton>						{/* 国际化处理： 取消*/}
						<NCButton style={{float:'right'}} className="button-primary" onClick={this.InputOkClick.bind(this)}>{this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000021')}</NCButton>		{ /* 国际化处理： 确定*/}
						<NCButton style={{float:'right'}} onClick={this.DownloadFileClick.bind(this)}>{this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000022')}</NCButton>{/* 国际化处理： 模板*/}
						<NCUpload  {...this.importProps}>
							<NCButton>{this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000023')}</NCButton>{/* 国际化处理： 文件*/}
						</NCUpload>					 
					</NCModal.Footer>

				</NCModal>

				{/* 这里是附件上传组件的使用，需要传入三个参数 */}
				<div className="nc-faith-demo-div2">
                    {showUploader &&
                        <NCUploader
                            billId={billId}
                            billNo={billno}
							onHide={this.onHideUploader}
							// beforeUpload={this.beforeUpload}
                        />
                    }
                </div>

				<div>
					<ApproveDetail
						billtype={this.state.billtype}
						billid={this.state.billid}
						show={this.state.showAppr}
						close={this.closeApprove}
					/>
				</div>   
			</div>
		);
	}
}

Card = createPage({
	//initTemplate: initTemplate
	mutiLangCode: appcode
})(Card);

//ReactDOM.render(<Card />, document.querySelector('#app'));
export default Card;
