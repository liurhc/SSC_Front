/**
 * 表体区域
 */
export const tableId = 'pk_dfgz_b';

/**
 * 表头区域
 */
export const formId = 'head';

/**
 * 查询区域编码
 */
export const searchCashCode = 'search_dfgz';
export const searchId = 'search';

/**
 * 导入编码
 */
export const inputId = 'input';

/**
 * 页面编码
 */
export const pagecode = '3610PAYR_C01';
export const appcode = '3610PAYR';
export const appid = '0001Z61000000003AE2F';

// //list页跳转路径
export const list_page_url= '/obm/ebankdfgz/dfgz/list/index.html';
// //cardt页跳转路径
export const card_page_url= '/obm/ebankdfgz/dfgz/card/index.html';
//list页跳转路径
// export const list_page_url= '../list/index.html'; 
//cardt页跳转路径
// export const card_page_url= '../card/index.html'; 
/**
 * 编辑态按钮
 */
export const editButtons = ['save', 'saveCommit', 'saveAdd', 'cancel'];

/**
 * 浏览态按钮
 */
export const browseButtons =  ['add', 'edit' ,'gennew','delete', 'copy','commit','unCommit','import','filemanage', 'approveinfo','qlpaybill','download','confirm','gennew','print','output','refresh','back']; //浏览
export const browseButtons1 = ['add', 'edit' ,'gennew','delete', 'copy','commit','import','filemanage', 'print','printgroup','output','qlpaybill','refresh']; //自由
export const browseButtons2 = ['add', 'copy','gennew','unCommit','import','filemanage','approveinfo','print','printgroup','output','qlpaybill','refresh']; //提交
export const browseButtons3 = ['add', 'copy','gennew','import','unCommit','filemanage','approveinfo','print','printgroup','output','qlpaybill','refresh']; //审批中
export const browseButtons4 = ['add', 'copy' ,'gennew','import','unCommit','filemanage','approveinfo','print','printgroup','qlpaybill','output','refresh']; //审批通过未支付
export const browseButtons5 = ['add', 'copy' ,'gennew','download','confirm','import','filemanage','approveinfo','qlpaybill','print','printgroup','output','refresh']; //审批通过支付不明
export const browseButtons7 = ['add', 'copy' ,'gennew','import','filemanage','approveinfo','qlpaybill','print','printgroup','output','refresh']; //审批通过支付失败
export const browseButtons8 = ['add', 'copy' ,'gennew','import','filemanage','approveinfo','qlpaybill','print','printgroup','output','refresh']; //审批通过支付成功

export const browseButtons6 = ['add']; //空白单据
export const dataSource = 'tm.obm.ebankdfgz.dfgz';
