import { ajax } from 'nc-lightapp-front';
import { appid,appcode, formId, tableId, pagecode,list_page_url,card_page_url  } from '../constants.js'; 

export default function afterEvent(props, moduleId, key,value, changedrows, i, s, g, isAutoSet) {
	console.log(props, moduleId, key,value, changedrows, i, s, g)
	if (key === 'pk_org') { 
		//主组织处理
		//props.resMetaAfterPkorgEdit();
		//获取编辑后表单的值
		let eventdata = props.createHeadAfterEventData(
			pagecode,
			formId,
			tableId,
			moduleId,
			key,
			value
		);
		let newvalue = eventdata.newvalue.value;
		let oldvalue = eventdata.oldvalue.value;
		let oldorgDis = eventdata.oldvalue.display;
		
		if (newvalue) {
			if (newvalue != oldvalue) {
				//props.cardTable.addRow(tableId);
			ajax({
				url: '/nccloud/obm/ebankdfgz/orgchange.do',
				data: eventdata,
				success: (res) => {
					if (res.success) {
						if (res.data) {
							if (res.data.head) {
								props.form.setAllFormValue({ [formId]: res.data.head[formId] });
							}
							//选择主组织以后，恢复其他字段的编辑性
							props.resMetaAfterPkorgEdit();
							props.cardTable.setTableData(tableId, { rows: [] });
							//if (res.data.body) {
							//	props.cardTable.setTableData(tableId, res.data.body[tableId]);
							//}
							//根据币种，设置表体金额列的精度
							if(res.data.userjson){
								
								let digist = res.data.userjson; 
								this.setState({digistnum: digist});
								//props.cardTable.setColScale([{areacode: tableId, filedcode: 'amount', scale: digist}]) ;
							}
							if(!isAutoSet){
								this.props.cardTable.addRow(tableId);
							}
						} else {
							props.form.setAllFormValue({ [formId]: { rows: [] } });
							props.cardTable.setTableData(tableId, { rows: [] });
						}
			            props.button.setButtonDisabled([   
						// 新增行   删除行 复制行
						'addLine', 'delLine', 'copyLine',
					], false);
						
					}
				} 
			}); 
			
		}else{
			props.button.setButtonDisabled([   
						// 新增行   删除行 复制行
						'addLine', 'delLine', 'copyLine',
					], true);
		}
		
	}
	}
	
	if (key == 'pk_currtype') { 
		//获取编辑后表单的值
		let eventdata = props.createHeadAfterEventData(
			pagecode,
			formId,
			tableId,
			moduleId,
			key,
			value
		);
		let newvalue = eventdata.newvalue.value;
		let oldvalue = eventdata.oldvalue.value;
		let oldorgDis = eventdata.oldvalue.display;
		if (newvalue != null) {
			ajax({
				url: '/nccloud/obm/ebankdfgz/orgchange.do',
				data: eventdata,
				success: (res) => {
					if (res.success) {
						if (res.data) {
							if (res.data.head) {
								props.form.setAllFormValue({ [formId]: res.data.head[formId] });
							}
							if (res.data.body) {
								props.cardTable.setTableData(tableId, res.data.body[tableId]);
							}
							//根据币种，设置表体金额列的精度
							if(res.data.userjson){
								let digist = res.data.userjson; 
								this.setState({digistnum: digist});
								//props.cardTable.setColScale([{areacode: tableId, filedcode: 'amount', scale: digist}]) ;
							}
						} else {
							props.form.setAllFormValue({ [formId]: { rows: [] } });
							props.cardTable.setTableData(tableId, { rows: [] });
						}
						
					}
				} 
			}); 
		}

	}

	if (key === 'amount') {
		if(value){
			let data = props.createBodyAfterEventData(pagecode, formId, moduleId, moduleId, key, changedrows, i);
			if(data){
				let newvalue = data.changedrows[0].newvalue && data.changedrows[0].newvalue.value;
				let oldvalue = data.changedrows[0].oldvalue && data.changedrows[0].oldvalue.value;
				if (newvalue != oldvalue) {
					ajax({
						url: '/nccloud/obm/ebankdfgz/dfgzbodyafterevent.do',
						data: data,
						success: (res) => {
							if (res.success) {
								if (res.data) {
									
									if (res.data.head) {
										this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
									}
									if (res.data.body) {
										this.props.cardTable.updateDataByRowId(tableId, res.data.body[tableId]);
									}
								}
							}
						}
					});
				}
			}
		}
	}
	
}
