import { base, ajax } from 'nc-lightapp-front'; 
import { tableId, formId,appcode, pagecode } from "../constants";
import tableButtonClick from "./tableButtonClick"; 

let { NCPopconfirm } = base;
// import cardTemplate from './cardTemplate';

export default function(props) {
	let that = this;
	props.createUIDom(
		{
			pagecode: pagecode,//页面id
			appcode: appcode//注册按钮的id
		},
		function (data) {
		// let data = cardTemplate.data;
			if (data) {
				let status = props.getUrlParam('status');
				
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(that,props, meta)
					props.meta.setMeta(meta);
					//props.initMetaByPkorg(); 
					if (status === 'add'){
						props.initMetaByPkorg()
						props.button.setButtonDisabled(['addLine', 'delLine', 'copyLine',], true);
					}
					     
				//}
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
			}
		}
	)
}

function modifierMeta(that,props, meta) {
	let status = props.getUrlParam('status');
	meta[formId].status = status;
	let multiLang = props.MutiInit.getIntl("3610PAYR");
	//meta[tableId].status = status; 	
	//财务组织字段增加过滤条件
	meta[formId].items.map((item) => {
		if (item.attrcode == 'pk_org') {
			item.queryCondition = () => {
				return {
					funcode: appcode,
					TreeRefActionExt: 'nccloud.web.tmpub.filter.FinanceOrgPermissionFilter' 
				};
			};
		}
		
		if(item.attrcode == 'crdttyp'){
			let options = [];
			options[0] = {
				"display": multiLang && multiLang.get('3610PAYR-000042'),/* 国际化处理： 借记卡或存折*/
				"value": "1"
			};
			options[1] = {
				"display": multiLang && multiLang.get('3610PAYR-000043'),/* 国际化处理： 信用卡*/
				"value": "4"
			};
			options[2] = {
				"display": multiLang && multiLang.get('3610PAYR-000044'),/* 国际化处理： 公务卡*/
				"value": "5"
			};
			options[3] = {
				"display": multiLang && multiLang.get('3610PAYR-000045'),/* 国际化处理： 他行卡*/
				"value": "6"
			};
			item.options = options;
		}		
		
	}); 
	
	
	

	let porCol = {
		attrcode: 'opr',
		label: props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000008'),/* 国际化处理： 操作*/
		fixed: 'right',
		itemtype: 'customer',
		visible: true,
		width: '200px',
		render(text, record, index) {
			let status = props.getUrlParam("status");
			let buttonAry =
			status === "browse"
			? ['openedit']:['editmoreBtn', 'copybody', 'insertline', 'deleteline' ];
			
			return props.button.createOprationButton(buttonAry, {
				area: 'card_body_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => tableButtonClick.call(that, props, key, text, record, index)
			});
		}
	};
	meta[tableId].items.push(porCol);

	return meta;
}
