import { createPage, ajax, base, toast } from 'nc-lightapp-front';
import { appid, formId, tableId, pagecode } from '../constants.js';

export default function tableButtonClick(props, key, text, record, index) {
    console.log(key);

    switch (key) {
        //展开
        case 'openedit':
            props.cardTable.toggleRowView(tableId, record)
            break;
        //编辑展开
        case 'editmoreBtn':
            props.cardTable.openModel(tableId, 'edit', record, index);
            break; 
        case 'copybody':
			//props.cardTable.pasteRow(tableId, index);
            //props.cardTable.setValByKeyAndIndex(tableId, index+1, 'pk_dfgz_b', { value: null});
            let selectRows = props.cardTable.getRowsByIndexs(tableId,index);
            let copyindex = props.cardTable.getNumberOfRows(tableId, false);
            let selectRowCopy = JSON.parse(JSON.stringify(selectRows));
            let copyrow = selectRowCopy[0];
            copyrow.values.pk_dfgz_b= {value:null, display: null}
            props.cardTable.insertRowsAfterIndex(tableId, copyrow, copyindex);
			break;    
        case 'insertline':
            props.cardTable.addRow(tableId, index);
          break; 
        case 'deleteline':
			props.cardTable.delRowsByIndex(tableId, index);
			break;    
    }
}
