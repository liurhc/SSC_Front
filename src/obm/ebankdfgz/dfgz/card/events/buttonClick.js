import { ajax, base, toast,promptBox,output} from 'nc-lightapp-front';
import { appid,appcode, formId, tableId, pagecode,list_page_url,card_page_url  } from '../constants.js'; 

export default function(props, id) { 
  switch (id) {
    case 'add':
    props.pushTo("/card", {
            status: 'add' ,
            id:props.getUrlParam('id'),
            type:'link',
            appcode: appcode,
            pagecode:pagecode            
        })
        this.toggleShow();
        break;
    case 'save':
        this.saveBill();
        break;
    case 'saveAdd':
        this.saveAddBill();
        break;
    case 'saveCommit':
        this.saveCommitBill(); 
        // this.dealWithData('commit','提交'); 
        break;
    case 'edit':
    props.pushTo("/card", {
            status: 'edit',
            id: props.getUrlParam('id'),
            type:'link',
            appcode: appcode,
            pagecode:pagecode
        })
        this.toggleShow()
        break;
    case 'copy':
    props.pushTo("/card", {
            status: 'copy',
            id: props.getUrlParam('id'),
            copyFlag: true,
            type:'link',
            appcode: appcode,
            pagecode:pagecode
        })
        this.toggleShow()
        break 
    case 'delete':
        // this.dealWithData(id,'删除'); 
        //props.modal.show('delete');
        promptBox({
            title: this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000018'),/* 国际化处理： 删除*/
            color: "warning",
            content: this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000019'),/* 国际化处理： 确定要删除吗？*/
            beSureBtnClick:  this.delDfgz.bind(this, props),
          });
        break
    case 'refresh':
         props.pushTo("/card", {
            status: 'browse',
            id: props.form.getFormItemsValue(formId, 'pk_dfgz').value
        });
        this.getdata();
        break
    case 'commit':  
        this.dealWithData(id,props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000000')); /* 国际化处理： 提交*/
        break;
    case 'unCommit':  
        this.dealWithData(id,props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000001')); /* 国际化处理： 收回*/
        break;
    // case 'approvegroup':  
    //     break;
    // case 'approve':  
    //     this.dealWithData(id,'审批'); 
    //     break;
    // case 'unApprove':  
    //     this.dealWithData(id,'弃审'); 
    //     break;
    case 'back':
       props.pushTo("/list",{
            pagecode:pagecode
        });
        break ;
    case 'filemanage':
        //附件管理
        //单据pk
        let pk_dfgz = this.props.form.getFormItemsValue(formId, 'pk_dfgz');
        //单据编号
        let filevbillno = this.props.form.getFormItemsValue(formId, 'vbillno');
        
        this.setState({
            //单据pk
            billId: pk_dfgz && pk_dfgz.value,
            //附件管理使用单据编号
            billno: filevbillno && filevbillno.value,
            showUploader: !this.state.showUploader,
        })
        break;
    case 'download':  
        this.dealWithData(id,props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000002')); /* 国际化处理： 下载支付状态*/
        break; 
    case 'confirm':  
        LogStateConfirm(props);
        break;
    case 'import': 
        this.InputClick();
        break;
    case 'gennew': 
        this.dealWithData(id,props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000003')); /* 国际化处理： 重新生单*/
        break; 
    case 'link':  
        let pk = props.form.getFormItemsValue(formId, 'pk_dfgz').value; 
        this.ApproveInfo(pk);
        break;
    case 'approveinfo':  
        pk = props.form.getFormItemsValue(formId, 'pk_dfgz').value; 
        this.ApproveInfo(pk);
        break;
    case 'qlpaybill':  
        LinkPayBill(props);
        break;
    case 'print':
        this.printData();
        break; 
    case 'output':
    output({
        url:  '/nccloud/obm/ebankdfgz/print.do',
        data: {
            //功能节点编码，即模板编码
            appcode: appcode,
            // 模板节点标识
            nodekey: 'NCCLOUD',
            // oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,   
            oids: [props.form.getFormItemsValue(formId, 'pk_dfgz').value],
            outputType: 'output'
        }
      });
        break;
    case 'cancel':
    promptBox({
        color: "warning",
        title: props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000004'),/* 国际化处理： 取消*/
        content: props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000005'),/* 国际化处理： 确认要取消吗？*/
        beSureBtnClick: cancelConfirm.bind(this, props)
      })

        
        break ;

    case 'addLine':
        props.cardTable.addRow(tableId) 
        break;
    case 'insLine':
        let inscurrRows = props.cardTable.getCheckedRows(tableId);
        let currSelect1 = [];
        if (inscurrRows && inscurrRows.length > 0) {
            for (let item of inscurrRows) {
              currSelect1.push(item.index);
            }
        }
        if (currSelect1.length == 0) {
            toast({ color: 'warning', content: props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000006') });/* 国际化处理： 未选择数据!*/
            return ;
        }
        props.cardTable.addRow(tableId, currSelect1);
        break;
    case 'delLine':
        let delcurrRows = props.cardTable.getCheckedRows(tableId);
        let currSelect2 = [];
        if (delcurrRows && delcurrRows.length > 0) {
            for (let item of delcurrRows) {
              currSelect2.push(item.index);
            }
        }
        if (currSelect2.length == 0) {
            toast({ color: 'warning', content: props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000006') });/* 国际化处理： 未选择数据!*/
            return ;
        }
        props.cardTable.delRowsByIndex(tableId, currSelect2);
        break; 
      case 'copyLine':
      let selectRows = props.cardTable.getCheckedRows(tableId);
      if (selectRows == null || selectRows.length == 0) {
          toast({
              'color': 'warning',
              'content': props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000007')/* 国际化处理： 未选中要复制的行。*/
          });
          return false;
      }
      // 粘贴末行位置index
      let copyindex = props.cardTable.getNumberOfRows(tableId, false);
      let selectCopyData = [];
      let selectRowCopy = JSON.parse(JSON.stringify(selectRows));
      for (let item of selectRowCopy) {
          item.data.selected = false;
          item.data.values.pk_dfgz_b = {
              value: null,
              display: null
          };
          selectCopyData.push(item.data);
      }
      props.cardTable.insertRowsAfterIndex(tableId, selectCopyData, copyindex);
         
          break;
      default:
          break
  }
}


// 编辑保存是取消按钮确认框
const cancelConfirm = function(props) {
    let pk = props.form.getFormItemsValue(formId, 'pk_dfgz').value
    let id = props.getUrlParam('id')
    if(pk){
        props.pushTo("/card", {
            status: 'browse',
            id: pk
        });
        this.setState({showNCbackBtn: true});
        this.getdata();
    }else if(id){

        props.pushTo("/card", {
            status: 'browse',
            id: id
        });
        this.setState({showNCbackBtn: true});
        this.getdata();
        
    }else{
        props.form.EmptyAllFormValue(formId);
        props.cardTable.resetTableData(tableId);
        props.pushTo("/card", {
            status: 'browse',
            id: ''
        });
        this.setState({showNCbackBtn: true});
        this.toggleShow()
    }
    
      
    
       
    
}


function LogStateConfirm(props){    
	let pk = props.form.getFormItemsValue(formId, 'pk_dfgz').value;
	let pk_group = props.form.getFormItemsValue(formId, 'pk_group').value;
	let pk_org = props.form.getFormItemsValue(formId, 'pk_org').value;
	let vbillno = props.form.getFormItemsValue(formId, 'vbillno').value;
	let yurref = props.form.getFormItemsValue(formId, 'yurref').value;
	props.openTo('/obm/ebankconfirmpay/confirmpay/main/index.html#/card', {			
        // appcode: '36100CONFM',
        // pagecode:'36100CONFM_C01',
        // yurref: yurref,
        // id: yurref,         
        // type:'link', 
        status: 'add',
		pk: '',
		pk_org: pk_org,
		pk_group: pk_group,
		srcsystem: 'OBM',
		srcbillcode: vbillno,
		srcbilltype: '36C9',
		srcnodecode: '3610PAYR',
		srcpkid: pk,			
        yurref: yurref,
        type:'link',
		// appcode: appcode,
        // pagecode:'DS'
        appcode: '36100CONFM',
		pagecode:'36100CONFM_C01',
	});

} 

function LinkPayBill(props){    
    let vbillstatus = props.form.getFormItemsValue(formId, 'vbillstatus').value;
	let srcpkid = props.form.getFormItemsValue(formId, 'srcpaypkid').value;
	let srcfuncode = props.form.getFormItemsValue(formId, 'srcpayfuncode').value;
    let yurref = props.form.getFormItemsValue(formId, 'yurref').value;
    if(vbillstatus!=1){
		toast({ color: 'warning', content: props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000041')});/* 国际化处理： 还未与付款单建立关联，不能联查！*/
		return;
	}
	if (srcfuncode == null || srcfuncode==='36071505'){
		srcfuncode="36070PBR";
	}
	if(srcpkid== null){
		let data = {
			"yurref": yurref 
		};
		ajax({
			url: '/nccloud/obm/ebankdfgz/qlpaybill.do',
			data: data,
			async: true,
			success: function (res) {
				srcpkid = res.data;
				LinkPayBill2(props,srcfuncode,srcpkid);
			}
		});
	}else{
		LinkPayBill2(props,srcfuncode,srcpkid);
	} 

} 

function LinkPayBill2(props,srcfuncode,srcpkid){  
	if (srcfuncode==="36070PBR"){
		props.openTo('/cmp/billmanagement/paybill/linkcard/index.html', {			
			src:'dfgz',
			status: 'browse',
            id: srcpkid ,
            type:'link',
		    appcode: '36070PBR',
            pagecode:'36070PBR_C02'
		});
	}
} 

