/**
 * 表体区域
 */
export const tableId = 'ebank_dfgz'; 

/**
 * 查询区域编码
 */
export const searchCashCode = 'search_dfgz';
export const searchId = 'search';
export const inputId = 'input';
export const cacheTabKey = '3610PAYR_list_tab';
export const cacheTabActiveKey = '3610PAYR_list_tabActive';
export const cachesearchKey = '3610PAYR_L01_search';
/**
 * 页面编码
 */
export const pagecode = '3610PAYR_L01'; 
export const appcode = '3610PAYR';
export const appid = '0001Z61000000003AE2F';
export const cardcode = '3610PAYR_C01';

//list页跳转路径
// export const list_page_url= '../list/index.html';
export const list_page_url= '/obm/ebankdfgz/dfgz/list/index.html';
//cardt页跳转路径
// export const card_page_url= '../card/index.html';
export const card_page_url= '/obm/ebankdfgz/dfgz/card/index.html';
/**
 * 编辑态按钮
 */
export const editButtons = ['save', 'saveCommit', 'saveAdd', 'cancel'];

/**
 * 浏览态按钮
 */ 
export const browseButtons =  ['add', 'delete', 'copy','commit','unCommit','import','filemanage', 'link','qlpaybill','download','confirm','gennew','print','output','refresh']; //浏览
export const browseButtons1 = ['add', 'delete', 'copy','commit','import','filemanage', 'print','output','refresh']; //自由
export const browseButtons2 = ['add', 'copy','unCommit','import','filemanage','print','output','refresh']; //提交
export const browseButtons3 = ['add', 'copy','unCommit','import','filemanage','print','output','refresh']; //审批中
export const browseButtons4 = ['add', 'copy' ,'unCommit','gennew','import','filemanage','print','output','refresh']; //审批通过未支付
export const browseButtons5 = ['add', 'copy' ,'unCommit','download','confirm','import','filemanage','link','qlpaybill','print','output','refresh']; //审批通过支付不明
export const browseButtons7 = ['add', 'copy' ,'unCommit','gennew','import','filemanage','link','qlpaybill','print','output','refresh']; //审批通过支付失败
export const browseButtons8 = ['add', 'copy' ,'unCommit','import','filemanage','link','qlpaybill','print','output','refresh']; //审批通过支付成功

export const dataSource = 'tm.obm.ebankdfgz.dfgz';
