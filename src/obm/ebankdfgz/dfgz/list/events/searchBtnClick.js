import {ajax,toast,cacheTools,cardCache} from 'nc-lightapp-front';
let { setDefData, getDefData } = cardCache;
import { tableId,searchId, formId, pagecode ,searchCashCode ,cacheTabKey,cacheTabActiveKey,dataSource} from "../constants";

//点击查询，获取查询区数据
export default function clickSearchBtn(props,searchVal) {
    //缓存查询条件	
	cacheTools.set(searchCashCode, searchVal);
    if(searchVal){
        setDefData(searchCashCode, dataSource, searchVal);
        let cachestateTabActiveKey = getDefData(cacheTabActiveKey, dataSource);
        let vbillstatus = '';
        if(cachestateTabActiveKey){
            if(cachestateTabActiveKey === '1'){
                vbillstatus = '-1';
            }else if(cachestateTabActiveKey === '2'){
                vbillstatus = '2';
            }else if(cachestateTabActiveKey === '3'){
                vbillstatus = null;
            }
        }else{
            vbillstatus = '-1';
        }
        let pageInfo = props.table.getTablePageInfo(tableId);
        let queryInfo = props.search.getQueryInfo(searchId, false);
        let oid = queryInfo.oid;
        let data = {
            querycondition: searchVal,
            custcondition: {
                conditions: [
					{
						field: 'vbillstatus',
						value: {
							firstvalue: vbillstatus,
							secondvalue: null
						},
						oprtype: '=',
						datatype: 203,
					}
				],
                logic: "and",
            },
			pagecode: pagecode,
			pageInfo: pageInfo,
			queryAreaCode: searchId,
			oid: oid,//"0001Z61000000000LUJH",
			querytype: "tree"
		};
        ajax({
            url: '/nccloud/obm/ebankdfgz/query.do',
            data: data,
            success: (res) => {
                let { success, data } = res;
                if (success) {
                    if(data){
                        if(data.grid){
                            this.props.table.setAllTableData(tableId, data.grid[tableId]);
                        }
                        if (data.numvalues) {
                            this.setState({ numvalues: data.numvalues});
		                    setDefData(cacheTabKey, dataSource, this.state.numvalues);
                            setDefData(cacheTabActiveKey, dataSource, this.state.activeKey);
                            if(data.numvalues.ALL){  
                                toast({
                                    duration: 6,
                                    color: 'success',
                                    content: props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000032') /* 国际化处理： 查询成功 */ 
                                })
                            }else{
                                /* 国际化处理：未查询出符合条件的数据! */
                                toast({
                                    duration: 6,
                                    color: 'warning',
                                    content:props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000034') /* 国际化处理： 未查询出符合条件的数据*//* 国际化处理： 未查询出符合条件的数据*/
                                })
                                this.props.table.setAllTableData(tableId, { rows: [] });
                                this.setState({ numvalues: {}});
                            }
                        }else{
                            toast({
                                duration: 6,
                                color: 'warning',
                                content:props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000034') /* 国际化处理： 未查询出符合条件的数据*//* 国际化处理： 未查询出符合条件的数据*/
                            })
                            this.props.table.setAllTableData(tableId, { rows: [] });
                            this.setState({ numvalues: {}});
                        }
                       
                    } else {
                        toast({
                            duration: 6,
                            color: 'warning',
                            content:props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000034') /* 国际化处理： 未查询出符合条件的数据*//* 国际化处理： 未查询出符合条件的数据*/
                        })
                        this.props.table.setAllTableData(tableId, { rows: [] });
                        this.setState({ numvalues: {}});
                    }
                }
            }
        });
    }
};


function giveToast(props, resLength) {
    if (resLength && resLength > 0) {
        let contentHead = props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000032'); /* 国际化处理： 查询成功 */
        toast({
            duration: 6,
            color: 'success',
            content: contentHead 
        })
    }
    else {
        toast({
            duration: 6,
            color: 'warning',
            content:props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000034') /* 国际化处理： 未查询出数据*//* 国际化处理： 未查询出数据*/
        })
    }
}
