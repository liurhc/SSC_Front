﻿import { ajax, base,cacheTools } from 'nc-lightapp-front'; 
import { tableId,searchId,searchCashCode, formId, pagecode,appcode,card_page_url  } from "../constants"; 
let { NCPopconfirm, NCIcon, NCTooltip:Tooltip } = base;
import searchBtnClick from './searchBtnClick'; 
import tableButtonClick from "./tableButtonClick";

export default function(props) {
	//请求模板数据
	props.createUIDom(
		{
			pagecode: pagecode,//页面id
			appcode: appcode//注册按钮的code
		},
		(data) => {
			if (data) {
				if (data.template) {
					let meta = data.template; 
					// //TODO 新盘去掉 
					// meta['download'].status = 'edit';
					meta['input'].status = 'edit';
					meta = modifierMeta.call(this,props, meta);
					props.meta.setMeta(meta);  
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					props.button.setPopContent('deletein', props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000019')); /* 国际化处理： 确定要删除吗？*/
					props.button.setDisabled({copy:true,commit:true,unCommit:true,delete:true,download:true,confirm:true,gennew:true,filemanage:true,qlpaybill:true,print:true,output:true}); 
				} 
				//更新查询条件
				let searchData = cacheTools.get(searchCashCode); 
				if (searchData && searchData.conditions){   
					props.search.setSearchValue(searchId,searchData.conditions); 
					//searchBtnClick(props,searchData);
				}
			}
		}
	)
} 

function modifierMeta(props, meta) {

	// //设置银行账号多选
	meta[searchId].items.find((e) => e.attrcode === 'pk_org').isMultiSelectedEnabled = true;
	meta[searchId].items.find((e) => e.attrcode === 'pk_banktype').isMultiSelectedEnabled = true;
	// meta['download'].items.find((e) => e.attrcode === 'bankacc').isMultiSelectedEnabled = true; 
	const { search, form } = props;
	let multiLang = props.MutiInit.getIntl("3610PAYR");
	//查询界面字段增加过滤条件
	meta[searchId].items.map((item) => {
		if (item.attrcode == 'pk_org') {
			item.queryCondition = () => {
				return {
					funcode: appcode,
					TreeRefActionExt: 'nccloud.web.tmpub.filter.FinanceOrgPermissionFilter' 
				};
			};
		} 
		// else if (item.attrcode == 'dbtacc') {
		// 	item.queryCondition = () => {

		// 		let pk_org = null;
		// 		let pk_banktype = null;
		// 		if (search.getSearchValByField(searchId, 'pk_org')) {
		// 			pk_org = search.getSearchValByField(searchId, 'pk_org').value.firstvalue;
		// 		}

		// 		if (search.getSearchValByField(searchId, 'pk_banktype')) {
		// 			pk_banktype = search.getSearchValByField(searchId, 'pk_banktype').value.firstvalue;
		// 		}

		// 		return {
		// 			refnodename: '核算归属权',
		// 			funcode: funcode,
		// 			pk_org: pk_org,
		// 			pk_banktype: pk_banktype,
		// 			GridRefActionExt: 'nccloud.web.obm.refbuilder.BankAccRefBuilder'
		// 		};
		// 	};

		// } 
	}); 

	//单据编号，变为超链接，点击调到卡片界面
	meta[tableId].items = meta[tableId].items.map((item, key) => {
		item.width = 150;
		if (item.attrcode == 'vbillno') {
			item.render = (text, record, index) => {
			  //let tip = (<div>{record.vbillno.value}</div>)
			
				return (
					//<Tooltip trigger="hover" placement="top" inverse={false} overlay={tip}>
					<a
						style={{cursor: 'pointer' }}
						onClick={() => {
							// this.setStateCache();
							// props.pushTo("/card",{status:"browse",id:record.pk_paybill.value,
							// pagecode: record.trade_type.value,bill_status:record.bill_status.value}); 

							//let searchVal = props.search.getAllSearchData(searchId);  
    						//cacheTools.set(searchCashCode, searchVal); 
							props.pushTo("/card", {
								status: 'browse',
								id: record.pk_dfgz.value,
								type:'link',
								appcode: appcode,
								pagecode:'DS'
							});
						}}
					>
						{record.vbillno ? record.vbillno.value : ''}
					</a>
					//</Tooltip>
				);
			};
		} 
		return item;
	}); 

	//添加操作列
	meta[tableId].items.push({
		attrcode: 'opr',
		label: props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000008'),/* 国际化处理： 操作*/
		fixed: 'right',
		width: '200px',
		itemtype: 'customer',
		visible: true,
		render: (text, record, index) => {

			let buttonAry =[];

			if(record && record.vbillstatus && record.vbillstatus.value == '-1' ){
				/* 单据状态：自由  */
				buttonAry=["commitin","editin","deletein"];
			}else if(record && record.vbillstatus && record.vbillstatus.value == '3'){
				/* 单据状态：提交 */
				buttonAry=["approveinfoin","uncommitin"];
			}else{
				buttonAry =["approveinfoin"];
			}

			return props.button.createOprationButton(buttonAry, {
				area: "list_inner",
				buttonLimit: 3,
				onButtonClick: (props, key) => tableButtonClick.call(this,props, key, text, record, index)
			});


		}
	});



	meta[tableId].items.map((item) => {
		if(item.attrcode == 'crdttyp'){
			let options = [];
			options[0] = {
				"display": multiLang && multiLang.get('3610PAYR-000042'),/* 国际化处理： 借记卡或存折*/
				"value": "1"
			};
			options[1] = {
				"display": multiLang && multiLang.get('3610PAYR-000043'),/* 国际化处理： 信用卡*/
				"value": "4"
			};
			options[2] = {
				"display": multiLang && multiLang.get('3610PAYR-000044'),/* 国际化处理： 公务卡*/
				"value": "5"
			};
			options[3] = {
				"display": multiLang && multiLang.get('3610PAYR-000045'),/* 国际化处理： 他行卡*/
				"value": "6"
			};
			item.options = options;
		}		
		
	}); 
	
	return meta;
}
