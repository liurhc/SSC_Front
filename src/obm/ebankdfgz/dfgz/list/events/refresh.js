import { createPage, ajax, cardCache,base, toast,cacheTools } from 'nc-lightapp-front';
let { setDefData, getDefData } = cardCache;
import { tableId, formId,searchId,inputId, cacheTabKey,cacheTabActiveKey,pagecode, dataSource, editButtons, browseButtons,searchCashCode } from "../constants";

export default function refresh(props) {	 
    let that = this;
	let searchVal =  props.search.getAllSearchData(searchId);; 
	if(searchVal){
        setDefData(searchCashCode, dataSource, searchVal);
        let pageInfo = props.table.getTablePageInfo(tableId);
        let queryInfo = props.search.getQueryInfo(searchId, false);
        let oid = queryInfo.oid;
        let data = {
            querycondition: searchVal,
            custcondition: {
                conditions: [
					{
						field: 'vbillstatus',
						value: {
							firstvalue: '-1',
							secondvalue: null
						},
						oprtype: '=',
						datatype: 203,
					}
				],
                logic: "and",
            },
			pagecode: pagecode,
			pageInfo: pageInfo,
			queryAreaCode: searchId,
			oid: oid,//"0001Z61000000000LUJH",
			querytype: "tree"
		};
        ajax({
            url: '/nccloud/obm/ebankdfgz/query.do',
            data: data,
            success: (res) => {
                let { success, data } = res;
                if (success) {
                    if(data){
                        if(data.grid){
                            //toast({ color: 'success' });
                            props.table.setAllTableData(tableId, data.grid[tableId]);
                        }else{
                            /* 国际化处理：未查询出符合条件的数据! */
                            //toast({ color: 'warning', content:"未查询出符合条件的数据!" });
                            props.table.setAllTableData(tableId, { rows: [] });
                        }
                        if (data.numvalues) {
                            that.setState({ numvalues: data.numvalues, activeKey: '1' });
		                    setDefData(cacheTabKey, dataSource, that.state.numvalues);
		                    setDefData(cacheTabActiveKey, dataSource, that.state.activeKey);
                        }else{
                            that.setState({ numvalues: {} });
                        }
                    } else {
                        /* 国际化处理：未查询出符合条件的数据! */
                        //toast({ color: 'warning', content: '未查询出符合条件的数据!'});
                        props.table.setAllTableData(tableId, { rows: [] });
                        that.setState({ numvalues: {} });
                    }
                }
            }
        });
    }
}
