import afterEvent  from './afterEvent';
import buttonClick from './buttonClick';
import initTemplate from './initTemplate';
import pageInfoClick from './pageInfoClick';
import searchBtnClick from './searchBtnClick'; 
import tableModelConfirm from './tableModelConfirm';
import doubleClick from './doubleClick'; 
import refresh from './refresh';
export { afterEvent,buttonClick,initTemplate,pageInfoClick,searchBtnClick,tableModelConfirm, doubleClick,refresh};
 
