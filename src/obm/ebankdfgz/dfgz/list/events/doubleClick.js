import { ajax, toast ,cacheTools} from 'nc-lightapp-front';
import { tableId,searchId, appcode,pagecode,searchCashCode,cardcode,list_page_url,card_page_url  } from "../constants";

export default function doubleClick(record, index, e) {
    let searchVal = this.props.search.getAllSearchData(searchId);  
    cacheTools.set(searchCashCode, searchVal); 
    this.props.pushTo("/card", {
        status: 'browse',
        id: record.pk_dfgz.value,
        type:'link',
		appcode: appcode,
        pagecode:'DS'
    });
}
