import {ajax} from 'nc-lightapp-front';
import { tableId,searchId, formId, pagecode  } from "../constants";

export default function (props, config, pks) {
    let pageInfo = props.table.getTablePageInfo(tableId);
    let searchVal = props.search.getAllSearchData(searchId);  
    // 后台还没更新，暂不可用
    let data = {
        "pks": pks,
        "pageid": pagecode
    };
    ajax({
        url: '/nccloud/obm/ebankdfgz/pagequery.do',
        data: data,
        async: true,
        success: function (res) {
            props.table.setAllTableData(tableId, res.data[tableId])
        }
    });
}
