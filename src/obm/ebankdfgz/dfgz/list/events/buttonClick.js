﻿import { ajax,base,toast,print,output,cardCache,promptBox,formDownload} from 'nc-lightapp-front';  

import { tableId, formId,searchId,searchCashCode,inputId, dataSource,appcode,pagecode,cacheTabKey,cacheTabActiveKey , editButtons, browseButtons } from "../constants";
let { getNextId, getCurrentLastId, deleteCacheById, deleteCacheId, getCacheById, updateCache, addCache, setDefData, getDefData } = cardCache;
export default function buttonClick(props, id) {  
	let selectDatas = null; 
	let pksel = null;
	let that = this;
	switch (id) {	
		case 'add': 
			gotoCard(props,pksel,'add',false);					
			break;
		case 'edit':
			selectDatas = props.table.getCheckedRows(tableId);
			//判断是否有选中行
			if (selectDatas == null || selectDatas.length == 0) {
				toast({ color: 'warning', content: props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000024') });/* 国际化处理： 未选中行！*/
				return;
			}
			else if (selectDatas.length > 1) {
				toast({ color: 'warning', content: props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000025') });/* 国际化处理： 一次只能修改一行！*/
				return;
			}
			pksel = selectDatas[0] && selectDatas[0].data && selectDatas[0].data.values && selectDatas[0].data.values.pk_dfgz && selectDatas[0].data.values.pk_dfgz.value;
			gotoCard(props,pksel,'edit',false);
			break;
		case 'delete':
			// dealWithData(props,id,'删除'); 
			//props.modal.show('delete');
			promptBox({
				title: props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000018'),/* 国际化处理： 删除*/
				color: "warning",
				content: props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000040'),/* 国际化处理： 确定要删除吗？*/
				beSureBtnClick:  this.delDfgz.bind(this, props),
			  });
			break; 
		case 'copy':
			selectDatas = props.table.getCheckedRows(tableId);
			//判断是否有选中行
			if (selectDatas == null || selectDatas.length == 0) {
				toast({ color: 'warning', content: props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000024') });/* 国际化处理： 未选中行！*/
				return;
			}
			if (selectDatas.length > 1) {
				toast({ color: 'warning', content: props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000026') });/* 国际化处理： 只支持单笔数据复制！*/
				return;
			}
			pksel = selectDatas[0] && selectDatas[0].data && selectDatas[0].data.values && selectDatas[0].data.values.pk_dfgz && selectDatas[0].data.values.pk_dfgz.value;
			gotoCard(props,pksel,'copy',true);
			break
		case 'commit':  
			dealWithData(props,id,props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000000'),that); /* 国际化处理： 提交*/
			break;
		case 'commitgroup':
			break;
		case 'unCommit':  
			dealWithData(props,id,props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000001'),that); /* 国际化处理： 收回*/
			break;
		case 'approvegroup':  
			break;
		case 'approve':  
			dealWithData(props,id,props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000027'),that); /* 国际化处理： 审批*/
			break;
		case 'unApprove':  
			dealWithData(props,id,props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000028'),that); /* 国际化处理： 弃审*/
			break;
		case 'approveInfo':  
			let linkaprvData = props.table.getCheckedRows(tableId);
			//let pk_billtype='36S2';
			let pk_linkDfgz = ''; //单据pk
			let pk_linktradetype = ''; //单据pk
			if (linkaprvData.length != 1) {
				toast({
					color: 'warning',
					content: props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000029')/* 国际化处理： 请选择单条数据，查看审批意见!*/
				});
				return;
			}
			//处理
			linkaprvData.forEach((val) => {
				if (val.data.values.pk_dfgz && val.data.values.pk_dfgz.value != null) {
					pk_linkDfgz = val.data.values.pk_dfgz.value;
				}
				// if (val.data.values.trade_type && val.data.values.trade_type.value != null) {
				// 	pk_linktradetype = val.data.values.trade_type.value;
				// }
				pk_linktradetype = '36C9';
			});

			this.setState(
				{
					billid: pk_linkDfgz, //单据pk
					billtype: pk_linktradetype
				},
				() => {
					this.setState({
						showAppr: true
					});
				}
			);
			break;
		case 'filemanage':
			//附件管理 
			let pk_dfgz = null;
			let filevbillno =null;
			let selectDatas = null;
			selectDatas = props.table.getCheckedRows(tableId);
			//数据校验
			if (selectDatas ==null || selectDatas.length == 0) {
				toast({ color: 'warning', content: props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000024') });/* 国际化处理： 未选中行！*/
				return
			} 
			if (selectDatas.length > 1) {
				toast({ color: 'warning', content: props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000030') + msg + '！' });/* 国际化处理： 只支持单笔数据*/
				return;
			}  
			let index = 0; 
			while (index < selectDatas.length) {
				//获取行单据pk
				pk_dfgz = selectDatas[index] && selectDatas[index].data && selectDatas[index].data.values && selectDatas[index].data.values.pk_dfgz && selectDatas[index].data.values.pk_dfgz.value;
				//获取行单据编号
				filevbillno = selectDatas[index] && selectDatas[index].data && selectDatas[index].data.values && selectDatas[index].data.values.vbillno && selectDatas[index].data.values.vbillno.value;				
				index++;
			} 
			if (selectDatas.length > 0) {		 
				this.setState({
					//单据pk
					billId: pk_dfgz,
					//附件管理使用单据编号
					billno: filevbillno ,
					showUploader: !this.state.showUploader,
				}) 
			}
			break;
		case 'download':  
			dealWithData(props,id,props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000002'),that); /* 国际化处理： 下载支付状态*/
			break;
		case 'confirm':  
			LogStateConfirm(props);
			break;
		case 'import': 
			this.InputClick();
			break;
		case 'exportfile':
			let data = 'xls';
			formDownload({
				params: { data: data },
				url: '/nccloud/obm/ebankdfgz/filedownload.do',
				enctype: 1
			});
			break;
		case 'gennew': 
			dealWithData(props,id,props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000003'),that); /* 国际化处理： 重新生单*/
			break; 
		case 'qlpaybill':  
			LinkPayBill(props);
			break;
		case 'print':
			printData(props);
			break; 
		case 'output': 
			outputData(props); 
			break; 
		case 'refresh':
		    refreshHtml(that, props);
			break;
	}
}

function gotoCard(props,pk_dfgz,status,copyFlag){
    props.pushTo("/card", {
        status: status,
		id: pk_dfgz,
		//copyFlag:copyFlag,
		//type:'link',
		//appcode: appcode,
		//pagecode: pagecode 
		// appcode: appcode,
		// billtype:'36C9'
    });
}

function dealWithData(props,id,msg,that){
	let idstr="";
	let tsstr="";
	let index = 0;
	let pk = null;
	let ts =null;
	let selectDatas = null;
	selectDatas = props.table.getCheckedRows(tableId);
	//数据校验
	if (selectDatas ==null || selectDatas.length == 0) {
		toast({ color: 'warning', content: props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000024') });/* 国际化处理： 未选中行！*/
		return
	} 
	if (selectDatas.length > 1) {
		toast({ color: 'warning', content: props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000030') + msg + '！' });/* 国际化处理： 只支持单笔数据*/
		return;
	} 
	idstr="";
	tsstr="";
	index = 0;
	pk = null;
	ts =null;
	while (index < selectDatas.length) {
		//获取行主键值
		pk = selectDatas[index] && selectDatas[index].data && selectDatas[index].data.values && selectDatas[index].data.values.pk_dfgz && selectDatas[index].data.values.pk_dfgz.value;
		//获取行ts时间戳
		ts = selectDatas[index] && selectDatas[index].data && selectDatas[index].data.values && selectDatas[index].data.values.ts && selectDatas[index].data.values.ts.value;				
		//判空
		idstr = idstr + pk;
		tsstr = tsstr + ts;
		if (index<selectDatas.length-1){
			idstr = idstr + ",";
			tsstr = tsstr + ",";
		}
		index++;
	} 
	if (selectDatas.length > 0) {		
		ajax({
			url: '/nccloud/obm/ebankdfgz/' + id + '.do',
			data: { 
				id:idstr,
				ts:tsstr
			},
			success: (res) => {
				let { success, data } = res;
				if (success) {
					let { success, data } = res;
					if (res.success) {
						if(res.data && res.data.errorMsg){
						  toast({ color: 'warning', content: res.data.errorMsg });
						}else{
						  refreshHtml(that, props);	
						  toast({ color: 'success', content: props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000015') });/* 国际化处理： 已成功*/
						}
						
					  }					
				}
			}
		});
	}
} 


function refreshHtml(that, props){
	let searchVal = props.search.getAllSearchData(searchId);
	if(searchVal){
        setDefData(searchCashCode, dataSource, searchVal);
        let pageInfo = props.table.getTablePageInfo(tableId);
        let queryInfo = props.search.getQueryInfo(searchId, false);
        let oid = queryInfo.oid;
        let data = {
            querycondition: searchVal,
            custcondition: {
                conditions: [
					{
						field: 'vbillstatus',
						value: {
							firstvalue: '-1',
							secondvalue: null
						},
						oprtype: '=',
						datatype: 203,
					}
				],
                logic: "and",
            },
			pagecode: pagecode,
			pageInfo: pageInfo,
			queryAreaCode: searchId,
			oid: oid,//"0001Z61000000000LUJH",
			querytype: "tree"
		};
        ajax({
            url: '/nccloud/obm/ebankdfgz/query.do',
            data: data,
            success: (res) => {
                let { success, data } = res;
                if (success) {
                    if(data){
                        if(data.grid){
                            toast({ color: 'success', content:props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000033') });/* 国际化处理： 刷新成功*/
                            props.table.setAllTableData(tableId, data.grid[tableId]);
                        }else{
                            /* 国际化处理：未查询出数据 */
                            toast({ color: 'warning', content:props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000031') });/* 国际化处理： 未查询出数据*/
                            props.table.setAllTableData(tableId, { rows: [] });
                        }
                        if (data.numvalues) {
                            that.setState({ numvalues: data.numvalues, activeKey: '1' });
		                    setDefData(cacheTabKey, dataSource, that.state.numvalues);
		                    setDefData(cacheTabActiveKey, dataSource, that.state.activeKey);
                        }else{
                            that.setState({ numvalues: {} });
                        }
                    } else {
                        /* 国际化处理：未查询出数据 */
                        toast({ color: 'warning', content: props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000031')});/* 国际化处理： 未查询出数据*/
                        props.table.setAllTableData(tableId, { rows: [] });
                        that.setState({ numvalues: {} });
                    }
                }
            }
        });
    }
}

function LogStateConfirm(props){   
	let selectDatas = null;
	selectDatas = props.table.getCheckedRows(tableId);
	//数据校验
	if (selectDatas ==null || selectDatas.length == 0) {
		toast({ color: 'warning', content: props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000024') });/* 国际化处理： 未选中行！*/
		return
	} 
	if (selectDatas.length > 1) {
		toast({ color: 'warning', content: props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000030') + msg + '！' });/* 国际化处理： 只支持单笔数据*/
		return;
	} 
	let values = selectDatas[0].data.values;
	let pk = values.pk_dfgz.value;
	let pk_group = values.pk_group.value;
	let pk_org = values.pk_org.value;
	let vbillno = values.vbillno.value;
	let yurref = values.yurref.value;
	props.openTo('/obm/ebankconfirmpay/confirmpay/main/index.html#/card', {			
		status: 'add',
		pk: '',
		pk_org: pk_org,
		pk_group: pk_group,
		srcsystem: 'OBM',
		srcbillcode: vbillno,
		srcbilltype: '36C9',
		srcnodecode: '3610PAYR',
		srcpkid: pk,			
		yurref: yurref,
		type:'link',
		appcode: '36100CONFM',
		pagecode:'36100CONFM_C01',
	});

} 

function LinkPayBill(props){   
	let selectDatas = null;
	selectDatas = props.table.getCheckedRows(tableId);
	//数据校验
	if (selectDatas ==null || selectDatas.length == 0) {
		toast({ color: 'warning', content: props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000024') });/* 国际化处理： 未选中行！*/
		return
	} 
	if (selectDatas.length > 1) {
		toast({ color: 'warning', content: props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000030') + msg + '！' });/* 国际化处理： 只支持单笔数据*/
		return;
	} 
	let values = selectDatas[0].data.values;
	let srcpkid = values.srcpaypkid.value;
	let vbillstatus = values.vbillstatus.value;
	let srcfuncode = values.srcpayfuncode.value;	 
	let yurref = values.yurref.value;
	if(vbillstatus!=1){
		toast({ color: 'warning', content: props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000041')});/* 国际化处理： 还未与付款单建立关联，不能联查！*/
		return;
	}
	if (srcfuncode == null || srcfuncode==='36071505'){
		srcfuncode="36070PBR";
	}
	if(srcpkid== null){
		let data = {
			"yurref": yurref 
		};
		ajax({
			url: '/nccloud/obm/ebankdfgz/qlpaybill.do',
			data: data,
			async: true,
			success: function (res) {
				srcpkid = res.data;
				LinkPayBill2(props,srcfuncode,srcpkid);
			}
		});
	}else{
		LinkPayBill2(props,srcfuncode,srcpkid);
	}
	

} 

function LinkPayBill2(props,srcfuncode,srcpkid){  
	if (srcfuncode==="36070PBR"){
		props.openTo('/cmp/billmanagement/paybill/linkcard/index.html', {			
			src:'dfgz',
			status: 'browse',
			id: srcpkid ,
			type:'link',
			appcode: appcode,
			pagecode:'DS'
		});
	}
}

function printData(props){
	let printData = props.table.getCheckedRows(tableId);
		let pks = [];
		printData.forEach((item) => {
			pks.push(item.data.values.pk_dfgz.value);
		});  
		print(
			'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
			'/nccloud/obm/ebankdfgz/print.do',
			{
				nodekey: 'NCCLOUD', //模板节点标识
                oids: pks // 功能节点的数据主键
				//billtype: '36C9', //单据类型
				//funcode: '3610PAYR', //功能节点编码，即模板编码  
				//oids: pks // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
			},
            false
		);
}

function outputData(props){
	let printData = props.table.getCheckedRows(tableId);
	//数据校验
	if (printData ==null || printData.length == 0) {
		toast({ color: 'warning', content: props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000024') });/* 国际化处理： 未选中行！*/
		return
	} 
	if (printData.length > 1) {
		toast({ color: 'warning', content: props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000030') + msg + '！' });/* 国际化处理： 只支持单笔数据*/
		return;
	} 
		let pks = [];
		printData.forEach((item) => {
			pks.push(item.data.values.pk_dfgz.value);
		});   
		output({
			url: '/nccloud/obm/ebankdfgz/print.do',
			data: { 
				//功能节点编码，即模板编码
				appcode: appcode,
				// 模板节点标识
				nodekey: 'NCCLOUD',
			  	oids: pks,
			  	outputType: 'output'
			}
		});
}
