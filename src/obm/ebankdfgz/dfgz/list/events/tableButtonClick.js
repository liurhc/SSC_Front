import { ajax,base,toast,cacheTools,print,output} from 'nc-lightapp-front';  
import { tableId, formId, pagecode,appcode,searchId,searchCashCode,cardcode,list_page_url,card_page_url   } from "../constants";
//import refresh from './refresh.js';

export default function tableButtonClick(props, key, text, record, index) {
    console.log(key);
    let that = this;
    let pksel = record.pk_dfgz.value;
    let ts = record.ts.value;
    switch (key) { 
        case 'editin':
            gotoCard(props,pksel,'edit',false);
            break;
        case 'deletein':
            deleteData(props,pksel,ts,that);
            break;
        case 'commitin':
            dealWithData(props,'commit',props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000000'),pksel,ts,that); /* 国际化处理： 提交*/
            break;
        case 'uncommitin':
            dealWithData(props,'unCommit',props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000001'),pksel,ts,that); /* 国际化处理： 收回*/
            break;  
        case "approveinfoin":
            let pk_linktradetype = '36C9';
            this.setState(
				{
					billid: pksel, //单据pk
                    billtype: pk_linktradetype
                    // showAppr: true
				},
				() => {
					this.setState({
						showAppr: true
					});
				}
			);
			break; 
    }
}

function gotoCard(props,pk_dfgz,status,copyFlag){
	let searchVal = props.search.getAllSearchData(searchId); 
    cacheTools.set(searchCashCode, searchVal);
    props.pushTo("/card", {//.linkTo(card_page_url,
        status: status,
		id: pk_dfgz,
		copyFlag:copyFlag,
		type:'link',
		appcode: appcode,
		pagecode: pagecode  
    });
}

function dealWithData(props,id,msg,pk,ts,that){  
	  
    ajax({
        url: '/nccloud/obm/ebankdfgz/' + id + '.do',
        data: { 
            id:pk,
            ts:ts
        },
        success: (res) => {
            let { success, data } = res;
            if (success) {
                //刷新页面
                // refresh(props);
                that.refresh();
                toast({ color: 'success', content: msg + props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000035') });/* 国际化处理： 成功*/
                                    
            }
        }
    }); 
}

function deleteData(props,pk,ts,that){
    ajax({
        url: '/nccloud/obm/ebankdfgz/delete.do',
        data: { 
            id:pk,
            ts:ts
        },
        success: (res) => {
            let { success, data } = res;
            if (success) {
                //刷新页面
                //refresh(props);
                that.refresh();
                toast({ color: 'success', content: props.MutiInit.getIntl("3610PAYR") && props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000036') }); /* 国际化处理： 删除成功*/
            }
        }
    });
}
