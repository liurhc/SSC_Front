﻿import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast,cardCache ,cacheTools,high,formDownload,createPageIcon} from 'nc-lightapp-front';   
import { afterEvent,buttonClick,initTemplate,pageInfoClick,searchBtnClick,tableModelConfirm, doubleClick,refresh } from './events';
import { tableId, appcode,formId,searchId,searchCashCode,inputId, dataSource,pagecode,cacheTabKey,cacheTabActiveKey , editButtons, browseButtons } from "./constants";
let { getNextId, getCurrentLastId, deleteCacheById, deleteCacheId, getCacheById, updateCache, addCache, setDefData, getDefData } = cardCache;
// import './index.less';   
import NCTabs from "../../../../tmpub/pub/util/NCTabs/index"; 
const { NCTabPane } = NCTabs;
const { NCModal,NCButton,NCUpload } = base;
const onlineQueryFormId = 'download';   
const { NCUploader,ApproveDetail} = high;

class List extends Component {
	constructor(props) {
		super(props);
		this.props = props;    
		this.state = {
			// 选中页签的信息
			numvalues: {},
			activeKey: '1',
			data: {} ,
			showmodal : false,
			billid: '',
			billtype: '',
			showAppr: false,
			// 附件相关 start
			//单据pk
			billId:'',
			//附件管理使用单据编号
            billno:'',
			//控制附件弹出框
            showUploader: false,
			//控制弹出位置
            target: null,
			// 附件相关 end
		}; 
		initTemplate.call(this, props);
		this.upload_change = this.upload_change.bind(this);
		this.importProps = {
			name: 'file',
			action: '/nccloud/obm/ebankdfgz/upload.do',
			// accept:'text/xml, application/xml, text/xls, application/xls',
			data: {},
			multiple: false,
			// beforeUpload: this.beforeUpload,
			headers: {
				authorization: 'authorization-text',
			},
			onChange: this.upload_change,
		};

	}

	componentDidMount() {		
		this.restoreData();
	}

	// 还原列表页数据
	restoreData = () =>{
		//获取页签数据
		let cachestate = getDefData(cacheTabKey, dataSource);
		let cachestateTabActiveKey = getDefData(cacheTabActiveKey, dataSource);
		if (cachestate) {
			let keys = Object.keys(cachestate);
			this.setState({ numvalues: cachestate });
			this.setState({ activeKey: cachestateTabActiveKey });
		}
		
	}
	getButtonNames = (codeId) => {
		return 'main-button';
	}
	
	//文件上传状态改变处理
	upload_change(info) {
		if (info.file.status !== 'uploading') {
			console.log(info.file, info.fileList);
		}
		if (info.file.status === 'done') {

			if (info.file.response && info.file.response.success) {

				let { data } = info.file.response;
				if (data) { 
					let pathvalue = {value:data,display:data};  
					this.props.form.setFormItemsValue(inputId, { path: pathvalue }); 
				} 
			} else {
				toast({ content: info.file.response.error.message, color: "warning" });
			} 
		} else if (info.file.status === 'error') {
			info.fileList.splice(0, info.fileList.length);
		}
	}

	//点击导入按钮，打开导入对话框
    InputClick = () => {
		let defaultmsg = "{NC_HOME}/resources/ebank/NC_Dfgz_Templet.xls";
		let msgvalue = {value:defaultmsg,display:defaultmsg};		
		this.props.form.setFormItemsValue(inputId,{templet:msgvalue});
		let filetype = "EXCEL";
		let filetypevalue = {value:filetype,display:filetype};
		this.props.form.setFormItemsValue(inputId,{filetype:filetypevalue});
		// this.props.form.setFormItemsValue();
		this.setState(
			{
				showmodal: true
			}
		)
	}

	//点击取消按钮，关闭导入页面
    InputCancelClick = () => {
        this.setState(
			{
				showmodal: false
			}
		)
	}  

	//点击确定按钮，开启导入页面
    InputOkClick = () => {
		let path = this.props.form.getFormItemsValue(inputId, 'path').value
		if(!path){
			toast({ color: 'warning', content: this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000014') });/* 国际化处理： 请选择导入文件*/
			return;
		}
		let data = {
			cardflag:'0', 
			path: path
		};
		ajax({
			url: '/nccloud/obm/ebankdfgz/input.do',
			data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if (data && data[tableId]) {
						this.props.table.setAllTableData(tableId, data[tableId]);
					} else {
						this.props.table.setAllTableData(tableId, { rows: [] });
					};
					this.setState(
						{
							showmodal: false
						}
					);
				}
			}
		}); 
	}

	//点击下载模板按钮，下载模板
	DownloadFileClick= () => {
		let data = 'xls';		
		formDownload({
			params: {data:data},
			url: '/nccloud/obm/ebankdfgz/filedownload.do',
			enctype:1
		});
	} 

	//关闭联查审批意见
	closeApprove = () => {
		this.setState({
			showAppr: false
		});
	};

	fileManage(props){
		//附件管理 
		let pk_dfgz = null;
		let filevbillno =null;
		let selectDatas = null;
		selectDatas = props.table.getCheckedRows(tableId);
		//数据校验
		if (selectDatas ==null || selectDatas.length == 0) {
			toast({ color: 'warning', content: this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000024') });/* 国际化处理： 未选中行！*/
			return
		} 
		if (selectDatas.length > 1) {
			toast({ color: 'warning', content: this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000030') + msg + '！' });/* 国际化处理： 只支持单笔数据*/
			return;
		}  
		let index = 0; 
		while (index < selectDatas.length) {
			//获取行单据pk
			pk_dfgz = selectDatas[index] && selectDatas[index].data && selectDatas[index].data.values && selectDatas[index].data.values.pk_dfgz && selectDatas[index].data.values.pk_dfgz.value;
			//获取行单据编号
			filevbillno = selectDatas[index] && selectDatas[index].data && selectDatas[index].data.values && selectDatas[index].data.values.vbillno && selectDatas[index].data.values.vbillno.value;				
			index++;
		} 
		if (selectDatas.length > 0) {		 
			this.setState({
				//单据pk
				billId: pk_dfgz && pk_dfgz.value,
				//附件管理使用单据编号
				billno: filevbillno && filevbillno.value,
				showUploader: !this.state.showUploader,
			}) 
		}
	}

	//单击表格，编辑选中行数据
    onSelected = (props,moduleId, record,index,status) => {
		let selectDatas = props.table.getCheckedRows(tableId);
		let index2 = null;
		//先禁用掉按钮
		props.button.setDisabled({commit:true,unCommit:true,download:true,delete:true,confirm:true,gennew:true,filemanage:true,qlpaybill:true,print:true,output:true});
		//判断是否有选中行
		if (selectDatas == null || selectDatas.length == 0) {  
			//未选中行
			props.button.setDisabled({copy:true});
			return;
		}else if (selectDatas.length > 1){
			//选中多行
			
			props.button.setDisabled({copy:true});
			props.button.setDisabled({delete:false,print:false,output:false});
			return;
		}else{ 
			//选中一行，更加单据状态和支付状态，设置按钮是否可用
			index2 = selectDatas[0].index;
			props.button.setDisabled({copy:false});
		}
		//更加当前选中行，判断是否启用按钮
		let values = props.table.getAllTableData(tableId).rows[index2].values; 
		let vbillstatus = values.vbillstatus.value;
		if(vbillstatus==='0'){
			//审批未通过
			props.button.setDisabled({edit:true});
			props.button.setDisabled({delete:true});
			props.button.setDisabled({gennew:false,unCommit:false,import:false,filemanage:false,print:false,output:false});			 
		}else if(vbillstatus==='1'){
			//审批通过			
			props.button.setDisabled({edit:true});
			props.button.setDisabled({delete:true});
			let billpaystate = values.billpaystate.value;
			if (billpaystate==='2'){
				//支付不明
				props.button.setDisabled({gennew:false,download:false,confirm:false,import:false,filemanage:false,qlpaybill:false,print:false,output:false});		   
			}else if (billpaystate==='0'){
				//支付成功
				props.button.setDisabled({gennew:false,import:false,filemanage:false,qlpaybill:false,print:false,output:false});  
			}else if (billpaystate==='1'){
				//支付失败
				props.button.setDisabled({gennew:false,import:false,filemanage:false,qlpaybill:false,print:false,output:false});		   	
			}else{
				//尚未支付
				props.button.setDisabled({gennew:false,unCommit:false,import:false,filemanage:false,print:false,output:false});		   	
			} 
			
		}else if(vbillstatus==='2'){
			//审批进行中
			props.button.setDisabled({edit:true});
			props.button.setDisabled({delete:true});
			props.button.setDisabled({gennew:false,import:false,filemanage:false,print:false,output:false});   
		}else if(vbillstatus==='-1'){
			//自由态
			props.button.setDisabled({edit:false});
			props.button.setDisabled({delete:false});
			props.button.setDisabled({gennew:false,commit:false,import:false,filemanage:false,print:false,output:false}); 
		}else if (vbillstatus==='3'){
			//提交
			props.button.setDisabled({edit:true});
			props.button.setDisabled({delete:true}); 
			props.button.setDisabled({gennew:false,unCommit:false,import:false,filemanage:false,print:false,output:false});
		}				
    }  
	
	//删除单据
	delDfgz = () => {
		let selectDatas = null;
		selectDatas = this.props.table.getCheckedRows(tableId);
		//数据校验
		if (selectDatas ==null || selectDatas.length == 0) {
			toast({ color: 'warning', content: this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000024') });/* 国际化处理： 未选中行！*/
			return
		} 
		let idstr="";
		let tsstr="";
		let index = 0;
		let pk = null;
		let ts =null; 
		idstr="";
		tsstr="";
		index = 0;
		pk = null;
		ts =null;
		while (index < selectDatas.length) {
			//获取行主键值
			pk = selectDatas[index] && selectDatas[index].data && selectDatas[index].data.values && selectDatas[index].data.values.pk_dfgz && selectDatas[index].data.values.pk_dfgz.value;
			//获取行ts时间戳
			ts = selectDatas[index] && selectDatas[index].data && selectDatas[index].data.values && selectDatas[index].data.values.ts && selectDatas[index].data.values.ts.value;				
			//判空
			idstr = idstr + pk;
			tsstr = tsstr + ts;
			if (index<selectDatas.length-1){
				idstr = idstr + ",";
				tsstr = tsstr + ",";
			}
			index++;
		} 
		ajax({
			url: '/nccloud/obm/ebankdfgz/delete.do',
			data: { 
				id:idstr,
				ts:tsstr
			},
			success: (res) => {
				let { success, data } = res;
				if (success) {
					//刷新页面
					//this.refresh();
					const selecteDelData = this.props.table.getCheckedRows(tableId);
					let successpks = idstr.split(',');
					for (let index = 0; index < selecteDelData.length; index++) {
						const pk_dfgz = selecteDelData[index].data.values.pk_dfgz.value
						if (successpks.indexOf(pk_dfgz) >= 0) {
							deleteCacheById('pk_dfgz', pk_dfgz, dataSource);
							this.props.table.deleteTableRowsByIndex(tableId, selecteDelData[index].index);
						}
					}
					toast({ color: 'success', content: this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000036') }); /* 国际化处理： 删除成功*/
				}
			}
		});
	};

	// 附件的关闭点击
    onHideUploader = () => {
        this.setState({
            showUploader: false
        })
	}

getData = (serval) => {
		let servalTemp = serval && serval[0] ? serval[0] : null;
		//分页
		let datapageInfo = this.props.table.getTablePageInfo(tableId);
		//查询condition
		let datasearchVal = this.props.search.getAllSearchData(searchId);
		let queryInfo = this.props.search.getQueryInfo(searchId, false);
		let oid = queryInfo.oid;
		if(datasearchVal && datasearchVal.conditions){
			let data = {
				querycondition: datasearchVal,
				custcondition: {
					conditions: [servalTemp],
					logic: "and",
				},
				pagecode: pagecode,
				pageInfo: datapageInfo,
				queryAreaCode: searchId,
				oid: oid,
				querytype: 'tree'
			};
			ajax({
				url: '/nccloud/obm/ebankdfgz/query.do',
				data: data,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						if (data) {
							if (data.grid) {
								this.props.table.setAllTableData(tableId, data.grid[tableId]);
							} else {
								this.props.table.setAllTableData(tableId, { rows: [] });
							}
							if (data.numvalues) {
								this.setState({ numvalues: data.numvalues });
							}
							// this.setState({ activeKey: 5 });
							setDefData(cacheTabKey, dataSource, this.state.numvalues);
		                    setDefData(cacheTabActiveKey, dataSource, this.state.activeKey);
						} else {
							this.props.table.setAllTableData(tableId, { rows: [] });
							this.setState({ numvalues: {} });
						}
					}
				}
			});
		}
	};

//加载数据刷新数据
refresh = () => {
	let searchVal = this.props.search.getAllSearchData(searchId);
	if(searchVal){
        setDefData(searchCashCode, dataSource, searchVal);
        let pageInfo = this.props.table.getTablePageInfo(tableId);
        let queryInfo = this.props.search.getQueryInfo(searchId, false);
        let oid = queryInfo.oid;
        let data = {
            querycondition: searchVal,
            custcondition: {
                conditions: [
					{
						field: 'vbillstatus',
						value: {
							firstvalue: '-1',
							secondvalue: null
						},
						oprtype: '=',
						datatype: 203,
					}
				],
                logic: "and",
            },
			pagecode: pagecode,
			pageInfo: pageInfo,
			queryAreaCode: searchId,
			oid: oid,//"0001Z61000000000LUJH",
			querytype: "tree"
		};
        ajax({
            url: '/nccloud/obm/ebankdfgz/query.do',
            data: data,
            success: (res) => {
                let { success, data } = res;
                if (success) {
                    if(data){
                        if(data.grid){
                            //toast({ color: 'success' });
                            this.props.table.setAllTableData(tableId, data.grid[tableId]);
                        }else{
                            /* 国际化处理：未查询出符合条件的数据! */
                            //toast({ color: 'warning', content:"未查询出符合条件的数据!" });
                            this.props.table.setAllTableData(tableId, { rows: [] });
                        }
                        if (data.numvalues) {
                            this.setState({ numvalues: data.numvalues, activeKey: '1' });
		                    setDefData(cacheTabKey, dataSource, this.state.numvalues);
		                    setDefData(cacheTabActiveKey, dataSource, this.state.activeKey);
                        }else{
                            this.setState({ numvalues: {} });
                        }
                    } else {
                        /* 国际化处理：未查询出符合条件的数据! */
                        //toast({ color: 'warning', content: '未查询出符合条件的数据!'});
                        this.props.table.setAllTableData(tableId, { rows: [] });
                        this.setState({ numvalues: {} });
                    }
                }
            }
        });
    }
}

	//页签筛选
	navChangeFun = (status, className, e) => {
		let serval;
		switch (status) {
			case '1':
				serval = [
					{
						field: 'vbillstatus',
						value: {
							firstvalue: '-1',
							secondvalue: null
						},
						oprtype: '=',
						datatype: 203,
					}
				];
				
				this.getData(serval);
				this.setState({ activeKey: '1' });
				break;
			// 审批中
			case '2':
				serval = [
					{
						field: 'vbillstatus',
						value: {
							firstvalue: '2',
							secondvalue: null
						},
						oprtype: '=',
						datatype: 203,
					}
				];
				this.setState({ activeKey: '2' });
				this.getData(serval);
				break;
			// 全部
			case '3':
				serval = [
					{
						field: 'vbillstatus',
						value: {
							firstvalue: null,
							secondvalue: null
						},
						oprtype: '=', 
						datatype: 203, 
					}
				];
				this.setState({ activeKey: '3' });
				this.getData(serval);
				break;
			default:
				break;
		}
	};
	
	render() {  
		let numvalues = this.state.numvalues;
		let { table, button, form, search,modal ,ncmodal} = this.props;
		let buttons = this.props.button.getButtons();
		let { createSimpleTable } = table;
		let { createButtonApp } = this.props.button;
		let {  getButtons } = button;
		let { createForm } = form;
		let { NCCreateSearch } = search;
		let { createModal } = modal;
		let createNCModal = ncmodal.createModal;
		let { showUploader, target, billno, billId, showAppr  } = this.state; //附件相关内容变量
		// let Uploader = this.props.Uploader; 
		return (
			<div className="nc-bill-list">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						{/*页面大图标*/}
						{createPageIcon()}
						<h2 className='title-search-detail'>						
						{this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000017')}</h2>{/* 国际化处理： 工资清单*/}
					</div> 
					<div className="header-button-area">
						{/* 小应用注册按钮 */}
						{this.props.button.createButtonApp({
							area: 'list_head',
							buttonLimit: 4,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				<div className="nc-bill-search-area">
					{NCCreateSearch(searchId, { 
						// 显示高级按钮
						showAdvBtn: true,
						// 添加高级查询区自定义页签 (fun), return Dom 
						addAdvTabs: this.addAdvTabs,
						clickSearchBtn: searchBtnClick.bind(this),
						defaultConditionsNum: 4 //默认显示几个查询条件
					})}
				</div>
				<div className="tab-definInfo-area">
					<NCTabs activeKey={this.state.activeKey} 
						onChange={(v) => {
							this.navChangeFun.call(this, v);
						}}>
						{/* 国际化处理： 待提交*/}
						<NCTabPane key={'1'} tab={this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000037')+ '(' + (numvalues && numvalues.DTJ || 0) + ')'}/* 国际化处理： 待提交*/
						/>
						{/* 国际化处理： 待审批*/}
						<NCTabPane key={'2'} tab={this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000038')+ '(' + (numvalues && numvalues.SPZ || 0) + ')'}/* 国际化处理： 审批中*/
						/>
						{/* 国际化处理： 全部*/}
						<NCTabPane key={'3'} tab={this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000039')}/* 国际化处理： 全部*/
						/>
					</NCTabs>
				</div>
				<div className="nc-bill-table-area">
					{createSimpleTable(tableId, {//列表区
						onAfterEvent: afterEvent,
						handlePageInfoChange: pageInfoClick,
						tableModelConfirm: tableModelConfirm,
						// onRowClick: this.rowClick,
						dataSource: dataSource,
						pkname: 'pk_dfgz',
						componentInitFinished:()=>{
							//缓存数据赋值成功的钩子函数
							//若初始化数据后需要对数据做修改，可以在这里处理
						},
						onRowDoubleClick: doubleClick.bind(this),
						onSelected: this.onSelected,
						adaptionHeight: true,
						showCheck: true,
						showIndex: true
					})}
				</div>

				{createModal('delete', {
					title: this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000018'),/* 国际化处理： 删除*/
					content: this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000040'),/* 国际化处理： 确定要删除所选数据吗？*/
					beSureBtnClick: this.delDfgz.bind(this),
					//  模态框大小 sm/lg/xlg,
					size:'sm', 
					className:'junior'
				})}
				
				<NCModal show={this.state.showmodal} onHide={this.InputCancelClick.bind(this)} style={{width: '520' }} className={'senior'}>
					<NCModal.Header closeButton={true}>
						<NCModal.Title>{this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000020')}</NCModal.Title>{/* 国际化处理： 导入*/}
					</NCModal.Header>

					<NCModal.Body>
						<div >
                            {
                                createForm(inputId, 
                                    {
                                    //编辑后事件
                                    onAfterEvent: afterEvent.bind(this)
                                    }
                                )
							}    
							{/* <span>文件: </span><input id='eurpath'></input> */}
							
						</div> 						
					</NCModal.Body>

					<NCModal.Footer>	
						<NCButton style={{float:'right'}} onClick={this.InputCancelClick.bind(this)}>{this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000004')}</NCButton>						{/* 国际化处理： 取消*/}
						<NCButton style={{float:'right'}} className="button-primary" onClick={this.InputOkClick.bind(this)}>{this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000021')}</NCButton>		{/* 国际化处理： 确定*/}
						<NCButton style={{float:'right'}} onClick={this.DownloadFileClick.bind(this)}>{this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000022')}</NCButton>{/* 国际化处理： 模板*/}
						<NCUpload {...this.importProps}>
							<NCButton>{this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000023')}</NCButton>{/* 国际化处理： 文件*/}
						</NCUpload>					  
					</NCModal.Footer>
				</NCModal>

				{/* 这里是附件上传组件的使用，需要传入三个参数 */}
				<div className="nc-faith-demo-div2">
                    {showUploader &&
                        <NCUploader
                            billId={billId}
                            billNo={billno}
							onHide={this.onHideUploader}
							// beforeUpload={this.beforeUpload}
                        />
                    }
                </div>

				<div>
					<ApproveDetail
						billtype={this.state.billtype}
						billid={this.state.billid}
						show={this.state.showAppr}
						close={this.closeApprove}
					/>
				</div>   

			</div>
		);
	}
}

List = createPage({
	// initTemplate: initTemplate
	mutiLangCode: appcode
})(List);

//ReactDOM.render(<List />, document.querySelector('#app'));
export default List;
