import { ajax } from 'nc-lightapp-front';
// let { NCPopconfirm, NCIcon, NCMessage } = base;

let tableid = 'ebank_dppalog';
let pageId = '361010ADLQ_L01';
let searchid = '361010ADLQ';
export default function (props) {
	//请求模板数据
	props.createUIDom(
		{
			pagecode: pageId,//页面id
			appcode: '361010ADLQ' //注册按钮的id 36101ADLQ
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta)
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					props.button.setButtonDisabled(['output', 'Print'], true);
				}
			}
		}
	)
}

function modifierMeta(props, meta) {
	meta[searchid].items = meta[searchid].items.map((item, key) => {
		item.visible = true;
		return item;
	})
	// meta[tableid].items.push(event);
	meta[searchid].items.map((ele) => {
		ele.visible = true;
	});
	//财务组织用户过滤
	meta[searchid].items.map((item) => {
		if (item.attrcode == 'pk_org') {
			item.queryCondition = () => {
				return {
					funcode: '361010ADLQ',
					TreeRefActionExt: 'nccloud.web.tmpub.filter.FinanceOrgPermissionFilter'
				};
			};
		}
	});
	return meta;
}

//刷新页面数据方法
function refreshFun(props) {
	let searchVal = props.search.getAllSearchData(searchid);
	let data = {
		conditions: searchVal,
		pageInfo: {
			currentPageIndex: 0,
			pageSize: 10,
			total: 0,
			pageCount: 0
		}
	};

	ajax({
		url: '/nccloud/obm/ebankautodownlogquery/query.do',
		data: data,
		success: function (res) {
			props.table.setAllTableData(tableid, res.data['0001']);
		}
	});
}
