import { ajax, toast,print,output } from 'nc-lightapp-front';
// let { Message } = base;
let tableid = 'ebank_dppalog';
let pageId = '361010ADLQ_L01';
let searchid = '361010ADLQ';
export default function buttonClick(props, id) {
    //获取所有数据
    // let alldata = props.editTable.getAllRows(tableid,true);
    // let pks = [];
    // let checkedRows = props.editTable.getCheckedRows(tableid);
    console.log(id)
	switch (id) {	
		case 'Refresh':
			loaddata(props);
			break;
        case 'output':
            outputData(props); 
            break;
        case 'Print':
            printData(props);
            break;
		default:
			break;
	}
}
function loaddata(props){
	let pageInfo = props.table.getTablePageInfo(tableid);
	let queryInfo = props.search.getQueryInfo(searchid,false);
	queryInfo.pageInfo = pageInfo;
	let oid = queryInfo.oid;
	let searchVal = props.search.getAllSearchData(searchid,false);
	let data = {
		conditions: searchVal.conditions || searchVal,
		pagecode: "361010ADLQ",
		pageInfo: pageInfo,
		queryAreaCode: "361010ADLQ",
		oid: oid,
		queryType: "simple"
	};
	ajax({
		url: '/nccloud/obm/ebankautodownlogquery/queryschem.do',
		data: data,
		success: (res) => {
			let { success, data } = res;
			if(success){
				if(data && data[tableid]){
					props.table.setAllTableData(tableid, res.data[tableid]);
					toast({ content: [props.MutiInit.getIntl("361010ADLQ") && props.MutiInit.getIntl("361010ADLQ").get('361010ADLQ-000000')]+res.data[tableid].rows.length+[props.MutiInit.getIntl("361010ADLQ") && props.MutiInit.getIntl("361010ADLQ").get('361010ADLQ-000001')], color: "success" });/* 国际化处理： 查询成功,共,条*/
				}else{
					props.table.setAllTableData(tableid, {rows:[]});
					toast({ content: props.MutiInit.getIntl("361010ADLQ") && props.MutiInit.getIntl("361010ADLQ").get('361010ADLQ-000002'), color: "warning" });/* 国际化处理： 无数据*/
				}	
			}
		}
	});
	
}
/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */

function printData(props){
	let printData = props.table.getCheckedRows(tableid);
		let pks = [];
		printData.forEach((item) => {
			pks.push(item.data.values.pk_dppalog.value);
		});  
		print(
			'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
			'/nccloud/obm/ebankautodownlogquery/print.do',
			{
				funcode:'361010ADLQ', //功能节点编码，即模板编码
				appcode:'361010ADLQ',
				oids: pks // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
			}
		);
}

function outputData(props){
	let printData = props.table.getCheckedRows(tableid);
		let pks = [];
		printData.forEach((item) => {
			pks.push(item.data.values.pk_dppalog.value);
		});  
		output({
			url:'/nccloud/obm/ebankautodownlogquery/print.do',
			data: { 
			  	oids: pks,
			  	outputType: 'output'
			}
		});
}