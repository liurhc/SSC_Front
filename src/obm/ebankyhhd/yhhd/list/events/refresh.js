import { createPage, ajax, base, toast,cacheTools } from 'nc-lightapp-front';
 
export default function refresh(props) {	 
    let tableid = 'YhhdVO';
	let searchVal = props.search.getAllSearchData('search', false);
	let pageInfo = props.table.getTablePageInfo(tableid);
	let data = {
		conditions: searchVal.conditions || searchVal,
		pageInfo: pageInfo,
		pagecode: '3610HD_L01',
		queryAreaCode: 'search',  //查询区编码
		oid: '0001ZZ100000000066A3',  //查询模板id，手工添加在界面模板json中，放在查询区，后期会修改
		queryType: 'simple'
	};
	console.log(data);
	ajax({
		url: '/nccloud/obm/ebankyhhd/query.do',
		data: data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					props.table.setAllTableData(tableid, res.data[tableid]);
				} else {
					props.table.setAllTableData(tableid, { rows: [] });
					//  toast({content:this.props.MutiInit.getIntl("3610HD") && this.props.MutiInit.getIntl("3610HD").get('3610HD-000011'),color:"warning"});/* 国际化处理： 没有符合条件的记录。*/
					// toast({content:"没有符合条件的记录。",color:"warning"});
				}

			}
		},
		error: (res) => {
			console.log(res.message);
		}
	});
}
