﻿import { ajax, base, print,output, toast, cacheTools } from 'nc-lightapp-front';
import {printOnClient,printPreview} from "nc-lightapp-front";
// import { queryBills } from './queryBills';
import searchBtnClick from './searchBtnClick';
let tableid = 'YhhdVO';
let linkQueryBillId = 'linkquerybill';

export default function buttonClick(props, id) {
	console.log(id)
	switch (id) {
		case 'download':
			this.props.modal.show("download");
			break;
		case 'refresh':
			refreshAction(props);
			break;
		case 'downloadfile':
			//  this.DownloadFile();
			let selectDatas = props.table.getCheckedRows(tableid);
			//判断是否有选中行
			if (selectDatas == null || selectDatas.length == 0) {
				toast({ color: 'warning', content: this.props.MutiInit.getIntl("3610HD") && this.props.MutiInit.getIntl("3610HD").get('3610HD-000001') });/* 国际化处理： 未选中行！*/
				return;
			}
			// let index = 0;
			// let pk = selectDatas[index].data.values.pk_yhhd.value;
			let ids = [];
			for (let index = 0; index < selectDatas.length; index++) {
				ids[index] = selectDatas[index].data.values.pk_yhhd.value;
			}
			ajax({
				url: '/nccloud/obm/ebankyhhd/downloadfile.do',

				data: { pk: ids },
				success: (res) => {
					let { success, data } = res;
					if (success) {
						//如果有缓存条件，查询
						// let querycondition = cacheTools.get('3610HD_L01_search');
						// console.log(querycondition);
						// if (querycondition) {
						// 	cacheTools.set('3610HD_L01_back', '1');
						// 	props.search.setSearchValue('search', querycondition);
						// 	searchBtnClick(props, querycondition);
						// }
						let cachedata = [];
						cachedata = cacheTools.get('3610HD_L01_data');
						for (let index = 0; index < cachedata.rows.length; index++) {
							let pk_yhhd = cachedata.rows[index].values.pk_yhhd.value;
							if(ids.indexOf(pk_yhhd)>-1){
								cachedata.rows[index].values.downfileflag.value = true;
							}
						}
						var pageIndex =new Number(cachedata.pageInfo.pageIndex)-1;
						cachedata.pageInfo.pageIndex=pageIndex+"";
						props.table.setAllTableData(tableid, cachedata);
						cacheTools.set('3610HD_L01_data',cachedata);
						toast({ duration: 3, color: 'success', title: this.props.MutiInit.getIntl("3610HD") && this.props.MutiInit.getIntl("3610HD").get('3610HD-000002'), content: this.props.MutiInit.getIntl("3610HD") && this.props.MutiInit.getIntl("3610HD").get('3610HD-000003') })/* 国际化处理： 已成功,银行回单附件下载成功！*/
						return;
					}
				}
			});
			break;
		//打印
		case 'printBtn': 
			let printData = props.table.getCheckedRows(this.tableId);

			if (printData.length == 0) {
				toast({
					duration: 3,          // 消失时间，默认是3秒; 值为 infinity 时不消失,非必输
					color: 'warning',     // 提示类别，默认是 "success",非必输
					title: this.props.MutiInit.getIntl("3610HD") && this.props.MutiInit.getIntl("3610HD").get('3610HD-000004'),      // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输/* 国际化处理： 请注意*/
					content: this.props.MutiInit.getIntl("3610HD") && this.props.MutiInit.getIntl("3610HD").get('3610HD-000005')   // 提示内容,非必输/* 国际化处理： 请选择数据，进行打印!*/
				})
				return;
			}
			let pks = [];
            printData.forEach((item) => {
                pks.push(item.data.values.pk_yhhd.value);
			});
			//let filePks = [];
			let suffxData = [];
			let pkDocs = [];
			let suffx = null;
			let index = 0;
			let tempid=''; 
			let tempindex = 0;
			if (pks!=null){ 
				pks.forEach((it) =>{
					if (tempindex>0){
						tempid +=  ',';
					}
					tempindex = tempindex + 1;
					tempid = tempid + it; 
				});
				ajax({
					url:'/nccloud/obm/ebankyhhd/attachment.do',
					data:{
						billId: tempid, 
						fullPath: tempid
					},
					success: function(res) {
						if (res.data && res.date != []) {
							res.data.forEach((item) =>{
								console.log(item);
								if(item.pk!=null && item.pk_doc!=null &&  item.name!=null  ) {
									//filePks.push(item.pk);//获取文件pk值
									suffx = item.name.split('.')[1]; //获取后缀名
									if(suffx != null && suffx != "") {
										index  = index + 1;
										pkDocs.push(item.pk_doc+'.' + suffx);//获取pk_doc+后缀 作为pk
										//pkDocs.push(item.fullPath);
										suffxData.push(suffx);
									}
								} 
							});
							//打印回单
							printOnClient(
								props,  
								'/nccloud/obm/ebankyhhd/print.do',
								{
									oids:  pkDocs,    // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印
									suffx: suffxData, 
									funcode: '10220SLOG',
									 appcode: '10220SLOG',      //小应用编码
									 nodekey: null,     //模板节点标识
									 download: "fileStream",
									 type:  "3"//1打印2预览3文件打印(需要调用者传过来)
									 //ip:'10.16.1.147',
									 //port:'3006'		 
								},
								false
							);

						}
					}
				}); 
			} 
            
			break;
		//预览
		case 'previewBtn':

			let previewData = props.table.getCheckedRows(this.tableId);

			if (previewData.length == 0) {
				toast({
					duration: 3,          // 消失时间，默认是3秒; 值为 infinity 时不消失,非必输
					color: 'warning',     // 提示类别，默认是 "success",非必输
					title: this.props.MutiInit.getIntl("3610HD") && this.props.MutiInit.getIntl("3610HD").get('3610HD-000004'),      // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输/* 国际化处理： 请注意*/
					content: this.props.MutiInit.getIntl("3610HD") && this.props.MutiInit.getIntl("3610HD").get('3610HD-000005')   // 提示内容,非必输/* 国际化处理： 请选择数据，进行打印!*/
				})
				return;
			}
			let pkspreview = [];
			previewData.forEach((item) => {
				pkspreview.push(item.data.values.pk_yhhd.value);
			});
			print(
				'pdf',  //支持两类: 'html'为模板打印, 'pdf'为pdf打印
				'/nccloud/obm/ebankyhhd/print.do',
				{
					billtype: 'YHHD',  //单据类型
					funcode: '3610HD', //功能节点编码，即模板编码
					// nodekey: 'ot',     //模板节点标识
					// printTemplateID:'0001Z6100000000066BY', //输出打印模板id
					oids: pkspreview   // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,

				}
			);
			break
		//输出
		case 'outputBtn':
			outputData(props); 
			break
		//附件
		case 'file':

			let accessoryBtnData = props.table.getCheckedRows(this.tableId);

			let pk_rec = '';//单据pk
			let bill_no = '';//单据编号
			//选择一个或者不选择，多选默认显示空数据
			if (accessoryBtnData.length == 1) {
				accessoryBtnData.forEach((val) => {

					if (val.data.values.pk_yhhd && val.data.values.pk_yhhd.value != null) {
						pk_rec = val.data.values.pk_yhhd.value;
					}
					if (val.data.values.bill_no && val.data.values.bill_no.value != null) {
						bill_no = val.data.values.bill_no.value;
					}
				});
			} else {
				toast({
					duration: 3,          // 消失时间，默认是3秒; 值为 infinity 时不消失,非必输
					color: 'warning',     // 提示类别，默认是 "success",非必输
					title: this.props.MutiInit.getIntl("3610HD") && this.props.MutiInit.getIntl("3610HD").get('3610HD-000004'),      // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输/* 国际化处理： 请注意*/
					content: this.props.MutiInit.getIntl("3610HD") && this.props.MutiInit.getIntl("3610HD").get('3610HD-000006')   // 提示内容,非必输/* 国际化处理： 附件支持单条操作!*/
				})
				return;
			}
			console.log(bill_no, this.props.MutiInit.getIntl("3610HD") && this.props.MutiInit.getIntl("3610HD").get('3610HD-000007'));/* 国际化处理： 附件*/
			console.log(pk_rec, 'pk_rec');
			console.log(bill_no, 'bill_no');
			if (!pk_rec) {
				toast({
					duration: 3,          // 消失时间，默认是3秒; 值为 infinity 时不消失,非必输
					color: 'warning',     // 提示类别，默认是 "success",非必输
					title: this.props.MutiInit.getIntl("3610HD") && this.props.MutiInit.getIntl("3610HD").get('3610HD-000004'),      // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输/* 国际化处理： 请注意*/
					content: this.props.MutiInit.getIntl("3610HD") && this.props.MutiInit.getIntl("3610HD").get('3610HD-000008')   // 提示内容,非必输/* 国际化处理： 操作失败，无数据!*/
				})
				return;
			}

			this.setState({
				billId: pk_rec,//单据pk
				billno: bill_no,//附件管理使用单据编号
				showUploader: !this.state.showUploader,
				target: null
			})
			break
		//联查单据
		case 'linkquery': 
			let linkquerybillData = props.table.getCheckedRows(this.tableId);
			//数据校验
			if (linkquerybillData.length != 1) {
				toast({
					color: 'warning',
					content: this.props.MutiInit.getIntl("3610HD") && this.props.MutiInit.getIntl("3610HD").get('3610HD-000009')/* 国际化处理： 请选择单条数据，联查单据!*/
				});
				return;
			}
			//处理选择数据
			let showbilltrackpk;
			linkquerybillData.forEach((val) => {

				if (val.data.values.pk_yhhd && val.data.values.pk_yhhd.value) {
					showbilltrackpk = val.data.values.pk_yhhd.value;
				}

			});
			console.log(showbilltrackpk, 'billtrack_showbilltrackpk');

			// if (showbilltrackpk) {
			// 	this.setState({
			// 		showbilltrack: true,//显示联查单据
			// 		showbilltracktype: 'YHHD',//单据类型
			// 		showbilltrackpk: showbilltrackpk//单据pk
			// 	});
			// }
			let data = { 
				pk_yhhd:showbilltrackpk 
			};
			ajax({
				url: '/nccloud/obm/ebankyhhd/linkquerybill.do',
				data,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						if (data && data[linkQueryBillId]) {
							props.table.setAllTableData(linkQueryBillId, data[linkQueryBillId]);
						} else {
							props.table.setAllTableData(linkQueryBillId, { rows: [] });
						}
						this.setState(
							{
								showlinkquerybill: true
							}
						)
					}else{
						//手动关联失败
						toast({ color: 'warning', content: this.props.MutiInit.getIntl("3610HD") && this.props.MutiInit.getIntl("3610HD").get('3610HD-000027') }); /* 国际化处理： 未找到关联单据!*/
					} 
				}
			}); 
			break;
		case 'linkBtn': 
			let selDatas = props.table.getCheckedRows(tableid);
			let index2 = selDatas[0].index;
			let selvalues = props.table.getAllTableData(tableid).rows[index2].values;
			let dc_flag = selvalues.dc_flag.value;
			let meta = props.meta.getMeta();
			let items = meta['linkquery'].items;
			let itemCount = items == null ? 0 : items.length;
			let item = null;
			for (let j = 0; j < itemCount; j++) {
				if(items[j].attrcode=='billtype'){
					item = items[j];
					break;
				}
			} 
			let options = item.options;
			let tempoptions = null;
			if(dc_flag=='c' || dc_flag=='C'){
				//收款
				tempoptions = [{display:this.props.MutiInit.getIntl("3610HD").get('3610HD-000029'),value:1},{display:this.props.MutiInit.getIntl("3610HD").get('3610HD-000031'),value:3},{display:this.props.MutiInit.getIntl("3610HD").get('3610HD-000033'),value:5}];
			}else{
				//付款
				tempoptions = [{display:this.props.MutiInit.getIntl("3610HD").get('3610HD-000030'),value:2},{display:this.props.MutiInit.getIntl("3610HD").get('3610HD-000032'),value:4},{display:this.props.MutiInit.getIntl("3610HD").get('3610HD-000034'),value:6},{display:this.props.MutiInit.getIntl("3610HD").get('3610HD-000035'),value:7}];
			}
			item.options = tempoptions;
			props.meta.setMeta(meta);
			this.props.modal.show('linkquery');
			break;
		case 'linkCancelBtn':
			this.props.modal.show('linkcancel');
	}
}
/**
 * 刷新
 * @param {*} props 
 */
const refreshAction = (props) => {
	let searchVal = props.search.getAllSearchData('search', false);
	let pageInfo = props.table.getTablePageInfo(tableid);
	let data = {
		conditions: searchVal.conditions || searchVal,
		pageInfo: pageInfo,
		pagecode: '3610HD_L01',
		queryAreaCode: 'search',  //查询区编码
		oid: '0001ZZ100000000066A3',  //查询模板id，手工添加在界面模板json中，放在查询区，后期会修改
		queryType: 'simple'
	};
	console.log(data);
	ajax({
		url: '/nccloud/obm/ebankyhhd/query.do',
		data: data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					props.table.setAllTableData(tableid, res.data[tableid]);
					toast({color: 'success', content: props.MutiInit.getIntl("3610HD") && props.MutiInit.getIntl("3610HD").get('3610HD-000036')});/* 国际化处理： 刷新成功。*/
				} else {
					props.table.setAllTableData(tableid, { rows: [] });
					 toast({color: 'warning', content: props.MutiInit.getIntl("3610HD") && props.MutiInit.getIntl("3610HD").get('3610HD-000013')});/* 国际化处理： 没有符合条件的记录。*/
					// toast({content:"没有符合条件的记录。",color:"warning"});
				}

			}
		},
		error: (res) => {
			console.log(res.message);
		}
	});
}
function outputData(props){
	let printData = props.table.getCheckedRows(tableid);
		let pks = [];
		printData.forEach((item) => {
			pks.push(item.data.values.pk_yhhd.value);
		});  
		output({
			url: '/nccloud/obm/ebankyhhd/output.do',
			data: { 
			  	oids: pks,
			  	outputType: 'output'
			}
		});
}