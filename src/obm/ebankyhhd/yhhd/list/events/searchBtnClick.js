import { ajax, toast, cacheTools } from 'nc-lightapp-front';
let tableid = 'YhhdVO';
//点击查询，获取查询区数据
export default function clickSearchBtn(props, searchVal) {
    if (searchVal) {
        let backmark = cacheTools.get('3610HD_L01_back');
        console.log(searchVal);
        //缓存查询条件
        cacheTools.set('3610HD_L01_back', '0');
        cacheTools.set('3610HD_L01_search', searchVal);
        let pageInfo = props.table.getTablePageInfo(tableid);
        let data = {
            conditions: searchVal.conditions || searchVal,
            pageInfo: pageInfo,
            pagecode: '3610HD_L01',
            queryAreaCode: 'search',  //查询区编码
            oid: '0001ZZ100000000066A3',  //查询模板id，手工添加在界面模板json中，放在查询区，后期会修改
            queryType: 'simple'
        };
        ajax({
            url: '/nccloud/obm/ebankyhhd/query.do',
            data: data,
            success: (res) => {
                let { success, data } = res;
                if (success) {
                    if (data) {
                        props.table.setAllTableData(tableid, res.data[tableid]);
                        cacheTools.set('3610HD_L01_data', data[tableid]);
                        // let cachedata = [];
                        // cachedata = cacheTools.get('3610HD_L01_data');
                        // var pageIndex =new Number(cachedata.pageInfo.pageIndex)-1;
						// cachedata.pageInfo.pageIndex=pageIndex+"";
                        if (!backmark || backmark === "0") {
                            giveToast(props, data[tableid].allpks.length);
                        }
                    } else {
                        props.table.setAllTableData(tableid, { rows: [] });
                        if (!backmark || backmark === "0") {
                            giveToast(props, );
                        }
                    }

                }
            }
        });
    }

};
function giveToast(props, resLength) {

    if (resLength && resLength > 0) {
        let contentHead = props.MutiInit.getIntl("3610HD") && props.MutiInit.getIntl("3610HD").get('3610HD-000011');/* 国际化处理： 查询成功，共*/
        let contentEnd = props.MutiInit.getIntl("3610HD") && props.MutiInit.getIntl("3610HD").get('3610HD-000012');/* 国际化处理： 条。*/
        toast({
            duration: 6,
            color: 'success',
            content: contentHead + resLength + contentEnd
        })
    }
    else {
        toast({
            duration: 6,
            color: 'warning',
            content: props.MutiInit.getIntl("3610HD") && props.MutiInit.getIntl("3610HD").get('3610HD-000013')/* 国际化处理： 未查询到符合条件的记录*/
        })
    }
}
