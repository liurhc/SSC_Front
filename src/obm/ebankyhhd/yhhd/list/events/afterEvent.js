import { ajax, toast } from 'nc-lightapp-front';

let props2 = null;
const formId = "download";

export default function (props, moduleId, key, changerows, value, data, index) {
    if (moduleId === formId) {
        if (key === 'begindate' || key === 'enddate') {
            //开始时间//结束时间
            let begindate = props.form.getFormItemsValue(formId, 'begindate').value;
            let enddate = props.form.getFormItemsValue(formId, 'enddate').value;
            //开始日期与结束时间不能大于一个月
            if (begindate && enddate) {
                var date1 = new Date(begindate);  //开始时间
                var date2 = new Date(enddate);    //结束时间
                var date3 = date2.getTime() - date1.getTime()  //时间差的毫秒数
                //计算出相差天数
                var days = Math.floor(date3 / (24 * 3600 * 1000))
                if (days > 31) {
                    props.form.setFormItemsValue(moduleId, { 'enddate': { value: '', display: '' } });
                    toast({ content: this.props.MutiInit.getIntl("3610HD") && this.props.MutiInit.getIntl("3610HD").get('3610HD-000000'), color: "warning" });/* 国际化处理： 查询时间端不能超过一个月*/
                }
            }
        }
    }
}
