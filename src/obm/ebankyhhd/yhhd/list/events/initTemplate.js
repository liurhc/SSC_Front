﻿import { ajax, base, cacheTools } from 'nc-lightapp-front';
import searchBtnClick from './searchBtnClick';
let { NCPopconfirm, NCIcon, NCTooltip: Tooltip } = base;
let tableid = 'YhhdVO';
let pageId = '3610HD_L01';
let searchid = 'search';
export default function (props) {
	//请求模板数据
	props.createUIDom(
		{
			pagecode: pageId,//页面id
			appcode: '3610HD'
			// appid: '0001Z61000000003AEAU'//注册按钮的id
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta['download'].status = 'edit';
					meta['linkquery'].status = 'edit';
					meta = modifierMeta(props, meta)
					props.meta.setMeta(meta, () => {

						//如果有缓存条件，查询
						let querycondition = cacheTools.get('3610HD_L01_search');
						// console.log(props.MutiInit.getIntl("36100PZCQ") && props.MutiInit.getIntl("36100PZCQ").get('36100PZCQ-000007'));/* 国际化处理： 缓存查询条件*/
						console.log(querycondition);
						if (querycondition) {
							cacheTools.set('3610HD_L01_back', '1');
							props.search.setSearchValue('search', querycondition);
							searchBtnClick(props, querycondition);
						}
					});
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
			}
		}
	)
}

function modifierMeta(props, meta) {

	//设置银行账号多选
	meta['search'].items.find((e) => e.attrcode === 'acct_no').isMultiSelectedEnabled = true;
	meta['search'].items.find((e) => e.attrcode === 'pk_accorg').isMultiSelectedEnabled = true;
	meta['download'].items.find((e) => e.attrcode === 'acct_no').isMultiSelectedEnabled = true; 
	meta['download'].items.find((e) => e.attrcode === 'pk_accorg').isMultiSelectedEnabled = true;
	// meta[searchid].items.map((ele)=>{
	// 	ele.visible=true;
	// });
	//财务组织用户过滤
	meta['search'].items.map((item) => {
		if (item.attrcode == 'pk_accorg') {
			item.queryCondition = () => {
				return {
					funcode: '3610HD',
					TreeRefActionExt: 'nccloud.web.tmpub.filter.FinanceOrgPermissionFilter'
				};
			};
		}
		//银行账户子户过滤，根据组织过滤
		else if (item.attrcode == 'acct_no') {
			item.queryCondition = () => {

				let pk_org = null;
				let pk_banktype = null;
				if (props.search.getSearchValByField('search', 'acct_no')) {
					pk_org = props.search.getSearchValByField('search', 'pk_accorg').value.firstvalue;
				}
				if (props.search.getSearchValByField('search', 'banktype')) {
					pk_banktype = props.search.getSearchValByField('search', 'banktype').value.firstvalue;
				}
				return {
					// refnodename: props.MutiInit.getIntl("3610HD") && props.MutiInit.getIntl("3610HD").get('3610HD-000010'),/* 国际化处理： 核算归属权*/
					refnodename:'核算归属权',
					funcode: '3610HD',
					pk_org: pk_org,
					permissonOrgs: true,
					pk_banktype: pk_banktype,
					// GridRefActionExt: 'nccloud.web.obm.refbuilder.YhhdBankAccRefBuilder'
					GridRefActionExt: 'nccloud.web.obm.refbuilder.BankAccRefBuilder'
				};
			};

		}
	});
	meta['download'].items.map((item) => {
		if (item.attrcode == 'pk_accorg') {
			item.queryCondition = () => {
				return {
					funcode: '3610HD',
					TreeRefActionExt: 'nccloud.web.tmpub.filter.FinanceOrgPermissionFilter'
				};
			};
		}
		//银行账户子户过滤，根据组织过滤
		else if (item.attrcode == 'acct_no') {
			item.queryCondition = () => {

				let pk_org = null;
				let pk_banktype = null;
				if (props.form.getFormItemsValue('download', 'acct_no')) {
					pk_org = props.form.getFormItemsValue('download', 'pk_accorg').value;
				}

				if (props.form.getFormItemsValue('download', 'banktype')) {
					pk_banktype = props.form.getFormItemsValue('download', 'banktype').value;
				} 

				return {
					// refnodename: props.MutiInit.getIntl("3610HD") && props.MutiInit.getIntl("3610HD").get('3610HD-000010'),/* 国际化处理： 核算归属权*/
					refnodename:'核算归属权',
					funcode: '3610HD',
					pk_org: pk_org,
					permissonOrgs: true,
					pk_banktype: pk_banktype,
					GridRefActionExt: 'nccloud.web.obm.refbuilder.BankAccRefBuilder'
					// GridRefActionExt: 'nccloud.web.obm.refbuilder.YhhdBankAccRefBuilder'
				};
			};

		}
	});

	meta[tableid].items = meta[tableid].items.map((item, key) => {
		// item.width = 180;
		if (item.attrcode == 'bill_no') {
			item.render = (text, record, index) => {
				let tip = (<div>{record.bill_no.value}</div>)
				return (
					<Tooltip trigger="hover" placement="top" inverse={false} overlay={tip}>
						<a
							style={{ textDecoration: '', cursor: 'pointer' }}
							onClick={() => {
							}}
						>
							{record && record.bill_no && record.bill_no.value}
						</a>
					</Tooltip>
				);
			};
		}
		return item;
	});
	return meta;
}


