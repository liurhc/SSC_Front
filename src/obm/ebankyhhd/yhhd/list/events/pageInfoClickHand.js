import { ajax ,cacheTools} from 'nc-lightapp-front';
const tableid = 'linkquerytable';

export default function (props, config, pks) {
	let data = {
		allpks: pks,
		pageid: "3610HD_L01"
	};
	//得到数据渲染到页面
	let that = this;
	ajax({
		url: '/nccloud/obm/ebankyhhd/querypagehand.do',
		data: data,
		success: function (res) {
			let { success, data } = res;
			if (success) {
				if (data) {
					props.table.setAllTableData(tableid, data[tableid]);
					// cacheTools.set('3610HD_L01_data', data[tableid]);
				} else {
					props.table.setAllTableData(tableid, { rows: [] });
				}
			}
		}
	});
}
