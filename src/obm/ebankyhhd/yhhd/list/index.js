﻿import React, { Component } from 'react';
import ReactDOM from 'react-dom'; 
import { createPage, toast, ajax, base, high, print, cacheTools,createPageIcon} from 'nc-lightapp-front';
const { NCUploader, ApproveDetail, BillTrack } = high;
import { buttonClick, initTemplate, afterEvent, searchBtnClick, pageInfoClick,pageInfoClickHand,pageInfoClickBill,tableModelConfirm,onAfterEvent,refresh } from './events';
// import './index.less';  
const { NCModal, NCButton, NCUpload, NCCheckbox } = base; 
const onlineQueryFormId = 'download';
const linkQueryId = "linkquery";
const linkQueryTableId = "linkquerytable";
const linkCancelId = "linkcancel";
const linkQueryBillId = 'linkquerybill';
let tableid = 'YhhdVO';
let inputid = 'input';

class SingleTable extends Component {
	constructor(props) {
		super(props);
		this.props = props;
		this.searchId = 'search';
		this.tableId = tableid;
		this.inputId = "input";
		this.filepath = '';
		this.state = {
			data: {},
			billId: '',//单据pk
			billno: '',//附件管理使用单据编号
			showmodal: false,
			showbilltrack: false,//联查单据
			showbilltrackpk: '',//联查单据pk
			showbilltracktype: '',//联查单据类型
			showlinkquerybill:false,	//联查业务单据
			selvalue:null,		//选中的数据
			selpks:null,		//选中的数据（用于取消时）
			billtype:null,		//单据类型
			selhandlinkmap:new Map() 	//手动关联对话框中，被选中的数据
		}

	}

	getButtonNames = (codeId) => {
		return 'main-button';
	};

	//请求列表数据
	getData = () => {

	};
	componentDidMount() {
		this.toggleShow();
	}
	//切换页面状态
	toggleShow = () => {
		this.props.button.setDisabled({
			download: false,
			refresh: false,
			downloadfile: true,
			file: true,
			linkquery: true,
			printBtn: true,
			outputBtn: true,
			linkBtn:true,
			linkCancelBtn:true
		});
	};
	//手动关联选中或取消选中数据
	updateSelhandlink= (props,moduleid,record,index,status) => {
		let pk_detail = record.pk_detail.value;
		let tempmap = this.state.selhandlinkmap;
		if(status==true){
			if(tempmap.has(pk_detail)){ 
			}else{
				tempmap.set(pk_detail,record);
			}
		}else{
			if(tempmap.has(pk_detail)){
				tempmap.delete(pk_detail);
			}else{
				
			}
		}
		this.setState(
			{
				selhandlinkmap: tempmap
			}
		);
	}
	//手动关联选中或取消选中数据——全选，全消
	updateSelhandlinkAll= (props,moduleid,status,length) => {
		let index = 0;
		let tempmap = this.state.selhandlinkmap;
		while(index<length){
			let record = props.table.getAllTableData(linkQueryTableId).rows[index].values;
			let pk_detail = record.pk_detail.value; 
			if(status==true){
				if(tempmap.has(pk_detail)){ 
				}else{
					tempmap.set(pk_detail,record);
				}
			}else{
				if(tempmap.has(pk_detail)){
					tempmap.delete(pk_detail);
				}else{
					
				}
			} 
			index = index + 1;
		}
		this.setState(
			{
				selhandlinkmap: tempmap
			}
		);
	}
	//更新按钮状态
	updateButtonStatus = () => {
		var selDatas = this.props.table.getCheckedRows(tableid); 
		this.props.button.setDisabled({
			// addButton: false,
			//选择数据大于等于1时启用按钮
			// downloadfile:true
			downloadfile: selDatas && selDatas.length < 1,
			file: selDatas && selDatas.length < 1,
			printBtn: selDatas && selDatas.length < 1,
			outputBtn: selDatas && selDatas.length < 1 
		});

		let ishandlink = false;
		let cancellink = false;
		let index = 0; 
		let index2 = null;
		let values = null;
		let autolink = false;
		let handlink = false;
		let isquerylink = false;
		//判断是否有选中行
		if (selDatas == null || selDatas.length == 0 || selDatas.length > 1) { 
			ishandlink =false; 
			isquerylink = false;
			this.setState(
				{
					selvalue: null
				}
			);
		}else{
			//是否能够手动关联或联查单据
			index2 = selDatas[0].index;
			values = this.props.table.getAllTableData(tableid).rows[index2].values;
			this.setState(
				{
					selvalue: values
				}
			);
			autolink = values.autolink.value;
			if(autolink==true){
				ishandlink =false; 
				isquerylink = true;
			}else{
				handlink = values.handlink.value;
				if(handlink==null || handlink==false){
					ishandlink = true; 
					isquerylink = false;
				}else{
					ishandlink = false; 
					isquerylink = true;
				}
			}
		} 
		let dishandlink = !ishandlink;
		let disquerylink = !isquerylink;
		//是否可以取消关联 
		let pks = [];
		selDatas.forEach((item) => {
			pks.push(item.data.values.pk_yhhd.value);
		});
		this.setState(
			{
				selpks: pks
			}
		);
		if (selDatas == null || selDatas.length == 0 ) { 
			cancellink = false; 
		}else{
			//是否所有的数据都是已手动关联
			index = 0; 
			cancellink = true; 
			while (index < selDatas.length) {
				values = selDatas[index].data.values;
				handlink = values.handlink.value;
				if(handlink==null || handlink ==false){
					cancellink = false;
					break;
				} 
				index = index + 1;
			} 
		}
		let dishandlinkcancel = !cancellink; 
		this.props.button.setDisabled({
			linkBtn: dishandlink,
			linkCancelBtn:dishandlinkcancel,
			linkquery:disquerylink
		}); 
	}
	// 附件的关闭点击
	onHideUploader = () => {
		this.setState({
			showUploader: false
		})
	}
	//点击取消按钮，关闭页面
	InputCancelClick = () => {
		// this.props.modal.close(`input`);
		this.setState(
			{
				showmodal: false
			}
		)
	}
	//点击按钮，打开对话框
	InputClick = () => {
		// this.props.modal.show(`input`);
		this.setState(
			{
				showmodal: true
			}
		)
	}
	//点击确定按钮，导入
	InputOkClick = () => {
		//数据校验
		const queryData = form.getAllFormValue(onlineQueryFormId);

		if (form.isCheckNow(onlineQueryFormId)) {
			let data = {
				model: {
					pageInfo: {
						pageIndex: 0,
						pageSize: 10,
						total: 0,
						totalPage: 0
					},
					...queryData
				},
			};
			ajax({
				url: '/nccloud/obm/ebankyhhd/download.do',
				data,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						if (data && data[this.tableId]) {
							this.props.table.setAllTableData(this.tableId, data[this.tableId]);
							cacheTools.set('3610HD_L01_data', data[this.tableId]);
						} else {
							this.props.table.setAllTableData(this.tableId, { rows: [] });
						}
					}
					this.setState(
						{
							showmodal: false
						}
					);
				}
			});
		}

	}
	
	buttonYes = () => {
		let data = {
			begdate: this.state.querycond.begdate,
			enddate: this.state.querycond.enddate,
			org: this.state.querycond.org,
			curacc: this.state.querycond.curacc
		}
		ajax({
			url: '/nccloud/obm/ebankyhhd/query.do',
			data,
			success: (res) => {
				console.log("123");
				let { success, data } = res;
				if (success) {
					if (data && data[tableid]) {
						this.props.table.setAllTableData(tableid, data[tableid]);
					} else {
						this.props.table.setAllTableData(tableid, { rows: [] });
					}
				}
			}
		});
	}

	//点击取消按钮，关闭导入页面
    LinkCloseClick = () => {
        this.setState(
			{
				showlinkquerybill: false
			}
		)
	}  

	//点击查看按钮，打开业务单据页面
    LinkViewClick = () => {
		let selectDatas = null;
		selectDatas = this.props.table.getCheckedRows(linkQueryBillId);
		//数据校验
		if (selectDatas ==null || selectDatas.length == 0) {
			toast({ color: 'warning', content: this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000024') });/* 国际化处理： 未选中行！*/
			return
		} 
		if (selectDatas.length > 1) {
			toast({ color: 'warning', content: this.props.MutiInit.getIntl("3610PAYR") && this.props.MutiInit.getIntl("3610PAYR").get('3610PAYR-000030') + '！' });/* 国际化处理： 只支持单笔数据*/
			return;
		} 
		let values = selectDatas[0].data.values;
		let pk = values.pk_bill.value;
		let pk_billtype = values.pk_billtype.value; 
		let billcode = values.billcode.value; 
		if(pk_billtype=='D2' || pk_billtype.search('F2')==0){
			this.props.openTo('/nccloud/resources/arap/gatheringbill/gatheringbill/main/index.html#/card', {				
				id: pk ,
				status: 'browse',
				scene: 'linksce',
				appcode:'20060GBM',
        		pagecode:'20060GBM_CARD_LINK'   
			});
		}else if(pk_billtype=='D3' || pk_billtype.search('F3')==0){
			this.props.openTo('/nccloud/resources/arap/paybill/paybill/main/index.html#/card', {			
				id: pk ,
				status: 'browse',				
				scene: 'linksce',
				appcode:'20080EBM',
        		pagecode:'20080EBM_CARD_LINK'   
			});
		}else if(pk_billtype=='D4' || pk_billtype.search('F4')==0){
			this.props.openTo('/nccloud/resources/cmp/billmanagement/recbill/linkcard/index.html', {			
				status: 'browse',
				id: pk ,
				scene: 'linksce',				
				appcode:'36070RBM',
        		pagecode:'36070RBMLINK_C01'   
			});
		}else if(pk_billtype=='D5' || pk_billtype.search('F5')==0){
			this.props.openTo('/nccloud/resources/cmp/billmanagement/paybill/linkcard/index.html', {			
				status: 'browse',
				id: pk ,
				scene: 'linksce',
				appcode:'36070PBR',
        		pagecode:'36070PBR_C02'   
			});
		}else if(pk_billtype.search('263')==0){
			// if(pk_billtype=='2631'){
			// 	//差旅费借款单
			// 	this.props.openTo('/nccloud/resources/erm/expenseaccount/loanmanage/card/index.html#status=add', {			
			// 		status: 'browse',
			// 		id: pk ,
			// 		appcode:'201102JCLF',
			// 		pagecode:'201102JCLF_C'   
			// 	});
			// }else if(pk_billtype=='263X-Cxx-TYJKD'){
				//通用借款单 
				// this.props.openOut('/nccloud/resources/erm/expenseaccount/loanmanage/card/index.html#status=add', {	
				this.props.openTo('/nccloud/resources/erm/ybz/ybz/viewbill/index.html?tradetype=263X-Cxx-TYJKD', {		
					status: 'browse',
					id: pk ,
					scene: 'linksce',
					appcode:'201102JTYF',
					pagecode:'201102JTYF_C'   
				});
			// } 
		}else if(pk_billtype.search('264')==0){
			if(pk_billtype=='2647'){
				//还款单
				// this.props.openOut('/nccloud/resources/erm/expenseaccount/repaymentForPull/config/index.html#/pull?status=add', {			
				this.props.openTo('/nccloud/resources/erm/ybz/ybz/viewbill/index.html?tradetype=2647', {
					status: 'browse',
					id: pk ,
					scene: 'linksce',
					appcode:'201102HKD',
					pagecode:'201102HKD_card_001'   
				});
			}
			// else if(pk_billtype=='2641'){
			// 	//差旅费报销单
			// 	this.props.openTo('/nccloud/resources/erm/expenseaccount/expenseaccount/card/index.html#status=add', {			
			// 		status: 'browse',
			// 		id: pk ,
			// 		appcode:'201102BCLF',
			// 		pagecode:'201102BCLF_C'   
			// 	});
			// }
			// else if(pk_billtype=='264X-Cxx-TYBXD'){
			// 	//通用报销单
			// 	this.props.openTo('/nccloud/resources/erm/expenseaccount/expenseaccount/card/index.html#status=add&tradetype=264X-Cxx-TYBXD', {			
			// 		status: 'browse',
			// 		id: pk ,
			// 		appcode:'201102BTYF',
			// 		pagecode:'201102BTYF_C'   
			// 	});
			// }
			else{
				//报销单
				// this.props.openTo('/nccloud/resources/erm/expenseaccount/expenseaccount/card/index.html#status=add&tradetype=264X-Cxx-TYBXD', { 
				this.props.openTo('/nccloud/resources/erm/ybz/ybz/viewbill/index.html?tradetype=264X-Cxx-TYBXD', {			
					status: 'browse',
					id: pk ,
					scene: 'linksce',
					appcode:'201102BTYF',
					pagecode:'201102BTYF_C'   
				});
			}
			
		}else if(pk_billtype=='36J1' || pk_billtype=='36J5'){
			//委托付款单
			this.props.openTo('', {			
				status: 'browse',
				id: pk ,
				scene: 'linksce',
				appcode:'36300TP',
				pagecode:'36300TP_L01'   
			});
		}else if(pk_billtype=='36J2' || pk_billtype=='36J7'){
			//委托收款单
			this.props.openTo('/nccloud/resources/fts/commission/commissiongathering/main/index.html#/linkcard', {			
				status: 'browse',
				id: pk ,
				scene: 'linksce',
				appcode:'36300TG',
				pagecode:'36300TG_C01_LINK'   
			});
		}else if(pk_billtype=='36K4'){
			//上收单
			this.props.openTo('/nccloud/resources/sf/delivery/delivery/main/index.html#/cardlinkq', {			
				status: 'browse',
				id: pk ,
				scene: 'linksce',
				appcode:'36320FDA',
				pagecode:'36320FDA_Q01'   
			});
		}else if(pk_billtype=='36K2'){
			//下拨单
			this.props.openTo('/nccloud/resources/sf/allocation/allocate/main/index.html#/linkcard', {			
				status: 'browse',
				id: pk ,
				scene: 'linksce',
				appcode:'36320FA',
				pagecode:'36320FA_LINKC01'   
			});
		}
		
	}

	render() {
		let { table, button, form, search, modal } = this.props;
		let buttons = this.props.button.getButtons();
		let { createSimpleTable } = table;
		// let { createEditTable } = editTable;
		let { createButtonApp } = this.props.button;
		let { createButton, getButtons } = button;
		let { createForm } = form;
		let { NCCreateSearch } = search;
		let { createModal } = modal;
		let { showUploader, target, billno, billId } = this.state;//附件相关内容变量
		{createModal('printService', {
			className: 'print-service'
		  })}
		  <iframe id="printServiceframe" name="printServiceframe" style={{display: 'none'}}></iframe>
		return (
			<div className="nc-bill-list">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						{/*页面大图标*/}
						{createPageIcon()}
						<h2 className='title-search-detail'>							
							{this.props.MutiInit.getIntl("3610HD") && this.props.MutiInit.getIntl("3610HD").get('3610HD-000016')}</h2>{/* 国际化处理： 银行电子回单管理*/}
					</div>
					<div className="header-button-area">
						{/* 小应用注册按钮 */}
						{this.props.button.createButtonApp({
							area: 'list_head',
							buttonLimit: 4,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				<div className="nc-bill-search-area">
					{NCCreateSearch('search', {
						onAfterEvent: onAfterEvent.bind(this),
						showAdvBtn: false,
						clickSearchBtn: searchBtnClick.bind(this),
						oid: "0001ZZ100000000066A3",
						defaultConditionsNum: 4 //默认显示几个查询条件
					})}
				</div>
				<div className="nc-bill-table-area" style={{ borderRadius: "3px 3px 0 0", overflow: "hidden" }}>
					{createSimpleTable(this.tableId, {//列表区
						// onAfterEvent: afterEvent,
						adaptionHeight: true,
						handlePageInfoChange: pageInfoClick,
						tableModelConfirm: tableModelConfirm,
						onSelected: this.updateButtonStatus.bind(this),
						onSelectedAll: this.updateButtonStatus.bind(this),
						showCheck: true,
						showIndex: true
					})}
				</div>
				{/* 附件上传 */}
				<div className="nc-faith-demo-div2">
					{/* 这里是附件上传组件的使用，需要传入三个参数 */}
					{showUploader &&
						<NCUploader
							billId={billId}
							target={target}
							placement={'bottom'}
							billNo={billno}
							onHide={this.onHideUploader}
						/>
					}
				</div>
				{/** 联查单据 **/}
				<div>
					<BillTrack
						show={this.state.showbilltrack}
						close={() => {
							this.setState({ showbilltrack: false })
						}}
						pk={this.state.showbilltrackpk}  //单据id
						type={this.state.showbilltracktype}  //单据类型
					/>
				</div>
				{/* 在线下载——回单查询 */}
				{createModal(onlineQueryFormId, {
					title: this.props.MutiInit.getIntl("3610HD") && this.props.MutiInit.getIntl("3610HD").get('3610HD-000015'),/* 国际化处理： 在线明细查询*/
					className: 'senior',
					hasCloseBtn: true,
					style: { width: "520px", height: "268px" },
					content:
						<div className="form-wrapper">
							{createForm(onlineQueryFormId, {
								style: { width: "520px", height: "268px" },
								//编辑后事件
								onAfterEvent: afterEvent.bind(this)
							})}

						</div>
					,
					beSureBtnClick: () => {
						//数据校验
						const queryData = form.getAllFormValue(onlineQueryFormId);

						if (form.isCheckNow(onlineQueryFormId)) {
							let data = {
								model: {
									pageInfo: {
										pageIndex: 0,
										pageSize: 10,
										total: 0,
										totalPage: 0
									},
									...queryData
								},
							};
							ajax({
								url: '/nccloud/obm/ebankyhhd/download.do',
								data,
								success: (res) => {
									let { success, data } = res;
									if (success) {
										if (data && data[this.tableId]) {
											this.props.table.setAllTableData(this.tableId, data[this.tableId]);
											cacheTools.set('3610HD_L01_data', data[this.tableId]);
										} else {
											this.props.table.setAllTableData(this.tableId, { rows: [] });
										}
									}
								}
							});
						}
					}
				})}
				{/* 手动关联查询 */}
				{createModal(linkQueryId, {
					title: this.props.MutiInit.getIntl("3610HD") && this.props.MutiInit.getIntl("3610HD").get('3610HD-000017'),/* 国际化处理： 银行电子回单关联业务单据*/
					className: 'senior',
					hasCloseBtn: true,
					style: { width: "520px", height: "268px" },
					content:
						<div className="form-wrapper">
							{createForm(linkQueryId, {
								style: { width: "520px", height: "268px" },
								//编辑后事件
								onAfterEvent: afterEvent.bind(this)
							})}

						</div>
					,
					beSureBtnClick: () => {
						//数据校验
						const queryData = form.getAllFormValue(linkQueryId); 
						if (form.isCheckNow(linkQueryId)) {
							// var selDatas = this.props.table.getCheckedRows(tableid);
							// index2 = selDatas[0].index;
							// let values = this.props.table.getAllTableData(tableId).rows[index2].values;
							let values = this.state.selvalue;
							let acct_no = values.acct_no.value;
							let dc_flag = values.dc_flag.value;
							let trsamt = values.trsamt.value;
							this.setState(
								{
									billtype:queryData.rows[0].values['billtype'].value
								}
							);
							queryData.rows[0].values['account'].value = acct_no;
							queryData.rows[0].values['direction'].value = dc_flag;
							queryData.rows[0].values['money'].value = trsamt;
							let data = {
								model: {
									pageInfo: {
										pageIndex: 0,
										pageSize: 10,
										total: 0,
										totalPage: 0 
									},
									// account:acct_no,
									// direction:dc_flag,	
									// money:trsamt,						
									...queryData
								},
							};
							ajax({
								url: '/nccloud/obm/ebankyhhd/handlinkquery.do',
								data,
								success: (res) => {
									let { success, data } = res;
									if (success) {
										if (data && data[linkQueryTableId]) {
											this.props.table.setAllTableData(linkQueryTableId, data[linkQueryTableId]);
										} else {
											this.props.table.setAllTableData(linkQueryTableId, { rows: [] });
										} 
									}else{
										this.props.table.setAllTableData(linkQueryTableId, { rows: [] });
									}
									this.props.modal.show(linkQueryTableId);
									this.state.selhandlinkmap.clear(); 
								}
							}); 
							
						}
					}
				})}
				{/* 手动关联 */}
				{createModal(linkQueryTableId, {
					title: this.props.MutiInit.getIntl("3610HD") && this.props.MutiInit.getIntl("3610HD").get('3610HD-000018'),/* 国际化处理： 关联业务单据选择*/
					className: 'nc-bill-table-area',
					hasCloseBtn: true,
					// style: { width: "820px", height: "468px" },
					content:
						<div className="form-wrapper">
							{createSimpleTable(linkQueryTableId, {//列表区
								// onAfterEvent: afterEvent,
								handlePageInfoChange: pageInfoClickHand,
								tableModelConfirm: tableModelConfirm,
								onSelected: this.updateSelhandlink.bind(this),
								onSelectedAll: this.updateSelhandlinkAll.bind(this),
								showCheck: true,
								showIndex: true,
								height:'400px'
							})}

						</div>
					,
					beSureBtnClick: () => { 
						let index = 0;
						let pk = '';
						let billno = '';
						let idstr = '';
						let billnostr = '';
						let summoney = 0.0;
						let tempmoney = 0.0;
						let values = this.state.selvalue; 
						let dc_flag = values.dc_flag.value;
						let trsamt = parseFloat(values.trsamt.value);
						let transerial = values.bank_seq_no.value;
						let pk_yhhd = values.pk_yhhd.value;
						
						// var selectDatas = this.props.table.getCheckedRows(linkQueryTableId);						
						// while (index < selectDatas.length) {
						// 	//获取行主键值
						// 	pk = selectDatas[index].data.values.pk_detail.value;
						// 	billno = selectDatas[index].data.values.billcode.value;
						// 	idstr = idstr + pk; 
						// 	billnostr = billnostr + billno;
						// 	if (index<selectDatas.length-1){
						// 		idstr = idstr + ','; 
						// 		billnostr = billnostr + ',';
						// 	}
						// 	//判断金额
						// 	if(dc_flag=='d' || dc_flag=='D'){
						// 		tempmoney = parseFloat(selectDatas[index].data.values.paylocal.value);
						// 	}else{
						// 		tempmoney = parseFloat(selectDatas[index].data.values.receivelocal.value);
						// 	} 
						// 	summoney = summoney + tempmoney;
						// 	index++;
						// }  

						let tempmap = this.state.selhandlinkmap;
						let mapsize = tempmap.size;
						if(mapsize==0){
							toast({ color: 'warning', content: this.props.MutiInit.getIntl("3610HD") && this.props.MutiInit.getIntl("3610HD").get('3610HD-000019')  }); /* 国际化处理： 所选金额与回单金额不一致，无法手动关联！*/
							return {};
						}
						for (let [key, record] of tempmap) {
							pk = key;
							billno = record.billcode.value;
							idstr = idstr + pk; 
							billnostr = billnostr + billno;
							if (index<mapsize-1){
								idstr = idstr + ','; 
								billnostr = billnostr + ',';
							}
							//判断金额
							if(dc_flag=='d' || dc_flag=='D'){
								tempmoney = parseFloat(record.paylocal.value);
							}else{
								tempmoney = parseFloat(record.receivelocal.value);
							} 
							summoney = summoney + tempmoney;
							index++;
						}

						if(summoney!=trsamt){
							toast({ color: 'warning', content: this.props.MutiInit.getIntl("3610HD") && this.props.MutiInit.getIntl("3610HD").get('3610HD-000019')  }); /* 国际化处理： 所选金额与回单金额不一致，无法手动关联！*/
						}else{
							let pks = this.props.table.getPks(tableid);
							let data = { 
								pk_yhhd:pk_yhhd,
								billtype:this.state.billtype,
								transerial:transerial,	
								selpks:idstr,
								money:trsamt,
								selbillnos:billnostr
							};
							ajax({
								url: '/nccloud/obm/ebankyhhd/handlink.do',
								data,
								success: (res) => {
									let { success, data } = res;
									if (success) {
										//手动关联成功，刷新页面 
										pageInfoClick(this.props,null,pks);
										toast({ color: 'success', content: this.props.MutiInit.getIntl("3610HD").get('3610HD-000028') });
									}else{
										//手动关联失败
										toast({ color: 'warning', content: this.props.MutiInit.getIntl("3610HD") && this.props.MutiInit.getIntl("3610HD").get('3610HD-000020') }); /* 国际化处理： 手动关联失败！*/
									}
								}
							});
						} 
					}
				})}
				{/* 手动关联取消 */}
				{createModal(linkCancelId, {					
					title: this.props.MutiInit.getIntl("3610HD") && this.props.MutiInit.getIntl("3610HD").get('3610HD-000021') , /* 国际化处理： 银行电子回单取消关联业务单据*/
					className: 'senior',
					hasCloseBtn: true,
					style: { width: "520px", height: "268px" },
					content: 
						<div className="form-wrapper">
							{this.props.MutiInit.getIntl("3610HD") && this.props.MutiInit.getIntl("3610HD").get('3610HD-000022')} 
						</div>
					,
					beSureBtnClick: () => {  
						let selpks = this.state.selpks;
						let pks = this.props.table.getPks(tableid);
						let data = {
							selpks:selpks 
						};
						ajax({
							url: '/nccloud/obm/ebankyhhd/handlinkcancel.do',
							data,
							success: (res) => {
								let { success, data } = res;
								if (success) {
									//取消关联成功，刷新页面
									pageInfoClick(this.props,null,pks);
								}else{
									//取消关联失败
									toast({ color: 'warning', content: this.props.MutiInit.getIntl("3610HD") && this.props.MutiInit.getIntl("3610HD").get('3610HD-000023') }); /* 国际化处理： 取消关联失败*/
								}
							}
						});
					}
				})}
				{/** 联查单据 **/}
				{/* {createModal(linkQueryBillId, {
					title: '联查业务单据',
					className: 'nc-bill-table-area',
					hasCloseBtn: true, 
					content:
						<div className="form-wrapper">
							{createSimpleTable(linkQueryBillId, {//列表区
								// onAfterEvent: afterEvent,
								handlePageInfoChange: pageInfoClick,
								tableModelConfirm: tableModelConfirm,
								onSelected: this.updateButtonStatus.bind(this),
								showCheck: true,
								showIndex: true
							})}
						</div>
					,
					beSureBtnClick: () => {
						var selectDatas = this.props.table.getCheckedRows(linkQueryBillId); 
					}
				})} */}

				<NCModal show={this.state.showlinkquerybill} onHide={this.LinkCloseClick} style={{width: '800px' }}>
					<NCModal.Header>
						<NCModal.Title>{this.props.MutiInit.getIntl("3610HD") && this.props.MutiInit.getIntl("3610HD").get('3610HD-000024')}</NCModal.Title> {/* 国际化处理： 联查业务单据*/}
					</NCModal.Header>

					<NCModal.Body>
						<div >
                            {
								createSimpleTable(linkQueryBillId, {//列表区
									// onAfterEvent: afterEvent,
									handlePageInfoChange: pageInfoClickBill,
									tableModelConfirm: tableModelConfirm,
									// onSelected: this.updateButtonStatus.bind(this),
									showCheck: true,
									showIndex: true,
									height:'400px'
								}) 
							}      
						</div> 						
					</NCModal.Body>

					<NCModal.Footer>						 
						<NCButton onClick={this.LinkViewClick}>{this.props.MutiInit.getIntl("3610HD") && this.props.MutiInit.getIntl("3610HD").get('3610HD-000025')}</NCButton>  {/* 国际化处理： 联查*/}
						<NCButton onClick={this.LinkCloseClick}>{this.props.MutiInit.getIntl("3610HD") && this.props.MutiInit.getIntl("3610HD").get('3610HD-000026')}</NCButton>  {/* 国际化处理： 关闭*/}
					</NCModal.Footer>

				</NCModal>
			</div>
		);
	}
}

SingleTable = createPage({
	mutiLangCode: '3610HD',
	initTemplate: initTemplate
})(SingleTable);

ReactDOM.render(<SingleTable />, document.querySelector('#app'));
