import { ajax, toast } from 'nc-lightapp-front';
let searchid = '36101031_L01_search';
let tableid = 'table_atuodownlog_L01';
//点击查询，获取查询区数据
export default function searchBtnClick(props,value) {
	let pageInfo = this.props.table.getTablePageInfo(tableid);
	let queryInfo = props.search.getQueryInfo(searchid);
	queryInfo.pageInfo = pageInfo;
	let oid = queryInfo.oid;
	if(value){
		let data = {
			conditions:value.conditions || value,
			pagecode: "36101031_L01",
			pageInfo: pageInfo,
			queryAreaCode: "36101031_L01_search",
			oid: oid,
			queryType: "simple"
		};
		ajax({
			url: '/nccloud/obm/ebankpoolautodownlogqry/queryschem.do',  
			data: data,
			success: (res) => {
				let { success, data } = res;
				if(success){
					if(data && data[tableid]){
						props.table.setAllTableData(tableid, res.data[tableid]);
						toast({ content: [this.props.MutiInit.getIntl("36101031") && this.props.MutiInit.getIntl("36101031").get('36101031-000000')]+res.data[tableid].rows.length+[this.props.MutiInit.getIntl("36101031") && this.props.MutiInit.getIntl("36101031").get('36101031-000001')], color: "success" });/* 国际化处理： 查询成功,共,条*/
					}else{
						props.table.setAllTableData(tableid, {rows:[]});
						toast({ content: this.props.MutiInit.getIntl("36101031") && this.props.MutiInit.getIntl("36101031").get('36101031-000002'), color: "warning" });/* 国际化处理： 未查询到符合条件的数据*/
					}	
				}
			}
		});
	}else{
		
	}
	
}

