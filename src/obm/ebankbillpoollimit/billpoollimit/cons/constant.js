/**
 * 公共配置
 */
//应用id
export const app_id = '1001Z61000000001OVD6';
//模块编码
export const module_id = '3610';
//应用编码
export const app_code = '36101012';
//功能节点编码
export const fun_code = '36101012';
//实体id
export const oid = '0001Z61000000000P2TY';
//请求后端基础地址
export const base_url = '/nccloud/obm/ebankbillpoollimit/';
//按钮平铺显示数量
export const button_limit = 2;
/**
 * 列表
 */
//页面编码
export const list_page_id = '36101012_L01';
//查询区域编码
export const list_search_id = 'search';
//表格区域编码
export const list_table_id = 'ebank_billpoollimit';

/**
 * 卡片
 */
//在线查询页面编码
export const onlinequery_form_id = 'ebank_onlinequery';

//缓存
export const cache_key = '36101012_L01_search';





