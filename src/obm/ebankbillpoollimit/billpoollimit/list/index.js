﻿import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast, getMultiLang,createPageIcon } from 'nc-lightapp-front';
import { searchBtnClick, pageInfoClick, buttonClick, initTemplate, afterEvent, onAfterEvent, tableModelConfirm } from './events';
import './index.less';
import moment from 'moment';
//引入常量定义
import { base_url, module_id, app_code, list_table_id, onlinequery_form_id, button_limit, list_search_id, oid } from '../cons/constant.js';

const { NCUpload, NCModal, NCForm, NCFormControl, NCButton } = base;
const NCFormItem = NCForm.NCFormItem;
const formId = onlinequery_form_id
const onlineQueryFormId = onlinequery_form_id;

class SingleTable extends Component {
	constructor(props) {
		super(props);
		this.moduleId = app_code;
		this.searchId = list_search_id;
		this.tableId = list_table_id;
		this.isCheckPass = false;
		this.state = {
			showModal_onlinequery: false,
			json: {}
		};
	}

	getButtonNames = (codeId) => {
		return 'main-button';
	};

	componentWillMount() {
		// let callback = (json) => {
		// 	this.setState({ json });
		// 	initTemplate.call(this,this.props);
		// };
		// getMultiLang({ moduleId: app_code, domainName: 'obm', callback });
		// let tableStatus = this.editTable.getStatus(this.tableId)
		// if (tableStatus == 'edit') {
		// 	return this.props.MutiInit.getIntl("36101012") && this.props.MutiInit.getIntl("36101012").get('36101012-000023'); /* 国际化处理： 当前单据未保存, 您确定离开此页面?*/
		// }
	}



	componentDidMount() {
		this.setDefaultValue();
	}

	componentDidUpdate() {
		this.setDefaultValue();
	}

	setDefaultValue = () => {
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		//设置开始日期与结束日期
		const begindate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
		const enddate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
		this.props.form.setFormItemsValue(onlineQueryFormId, { 'begindate': { value: begindate, display: begindate }, 'enddate': { value: enddate, display: enddate } });
	}

	rowSelected = (props, moduleId, record, index, status) => {
		let checkedRows =this.props.table.getCheckedRows(moduleId);
		props.button.setButtonDisabled(['output', 'printgroupbtn'], !(checkedRows && checkedRows.length > 0));
	};


	render() {
		let multiLang = this.props.MutiInit.getIntl(this.moduleId);
		let { table, button, modal, search, form } = this.props;
		let { createSimpleTable } = table;
		let { createButton } = button;
		let buttons = this.props.button.getButtons();
		let { NCCreateSearch } = search;
		let { createModal } = modal;
		let { createButtonApp } = button;
		let { createForm } = this.props.form;

		return (
			<div className="nc-bill-list">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
						{/*页面大图标*/}
						{createPageIcon()}
						<h2 className="title-search-detail">
							{multiLang && multiLang.get('36101012-000011')}</h2>{/* 国际化处理： 银行票据池额度查询*/}
					</div>
					<div className="header-button-area">
						{this.props.button.createButtonApp({
							area: 'page_header',
							buttonLimit: 4,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				<div className="nc-bill-search-area">
					{NCCreateSearch(this.searchId, {
						oid: oid,
						showAdvBtn: false,
						clickSearchBtn: searchBtnClick.bind(this),
						onAfterEvent: onAfterEvent.bind(this),
						defaultConditionsNum: 4 //默认显示几个查询条件
					})}
				</div>
				{/* <div style={{ height: '10px' }}></div> */}
				<div className="nc-bill-table-area" style={{ borderRadius: "3px 3px 0 0", overflow: "hidden" }}>
					{createSimpleTable(this.tableId, {//列表区
						onAfterEvent: afterEvent,
						handlePageInfoChange: pageInfoClick.bind(this),
						tableModelConfirm: tableModelConfirm,
						onSelected: this.rowSelected.bind(this),
						onSelectedAll: this.rowSelected.bind(this),
						adaptionHeight: true,
						showCheck: true,
						showIndex: true
					})}
				</div>
				{createModal(onlineQueryFormId, {//在线下载区
					title: multiLang && multiLang.get('36101012-000009'),/* 国际化处理： 在线下载*/
					className: 'senior',
					content:
						// <OnlineQueryForm />
						<div className="form-wrapper">
							{createForm(onlineQueryFormId, {
								//编辑后事件
								onAfterEvent: afterEvent.bind(this)
							})}
						</div>
					,
					beSureBtnClick: () => {
						//数据校验
						const queryData = form.getAllFormValue(onlineQueryFormId);

						if (form.isCheckNow(onlineQueryFormId)) {

							let data = {
								model: {
									pageInfo: {
										pageIndex: 0,
										pageSize: 10,
										total: 0,
										totalPage: 0
									},
									...queryData
								},
							};

							ajax({
								url: '/nccloud/obm/ebankbillpoollimit/onlinequery.do',
								data: data,
								success: (res) => {
									let { success, data } = res;
									if (success) {
										if (data) {
											table.setAllTableData(this.tableId, data[this.tableId]);
										} else {
											table.setAllTableData(this.tableId, { rows: [] });
											toast({ content: multiLang && multiLang.get('36101012-000006'), color: "warning" });/* 国际化处理： 没有符合条件的记录。*/
										}
									}
								}
							});
						} else {
							// modal.show('onlinequery');
							//form.setAllFormValue(queryData);
						}
					}
					,
					cancelBtnClick: () => {
						//props.form.setFormItemsValue(this.formId,{enablestate:true});
						//return;
					}
				})}
			</div>
		);
	}
}

SingleTable = createPage({
	mutiLangCode: app_code,
	initTemplate: initTemplate
})(SingleTable);

ReactDOM.render(<SingleTable />, document.querySelector('#app'));
