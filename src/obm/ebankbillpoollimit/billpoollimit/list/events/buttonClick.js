﻿import { ajax, base, toast, print, output, cacheTools,formDownload } from 'nc-lightapp-front';
let { Message } = base;
//引入常量定义
import { base_url, fun_code, list_table_id, app_code, cache_key } from '../../cons/constant.js';

let tableid = list_table_id;
let funcode = fun_code;
let appcode = app_code;

export default function buttonClick(props, id) {
	switch (id) {
		case 'onlinequery':
			this.props.modal.show('ebank_onlinequery');
			break;
		case 'printgroupbtn':
			printData(props);
			break;
		case 'output':
			outputData(props);
		case 'refresh':
			refresh(props);
			break;
	}
}


function printData(props) {

	let multiLang = props.MutiInit.getIntl(app_code);

	let printData = props.table.getCheckedRows(tableid);
	let pks = [];
	printData.forEach((item) => {
		pks.push(item.data.values.pk_billpoollimit.value);
	});

	if (pks == null || pks.length == 0) {
		toast({ color: 'warning', content: multiLang && multiLang.get('36101012-000003') });/* 国际化处理： 未选中行！*/
		return;
	}

	print(
		'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
		base_url + 'print.do',
		{
			funcode: funcode, //功能节点编码，即模板编码
			//nodekey: 'web_print', //模板节点标识
			appcode: appcode,
			// printTemplateID: '1001Z610000000003XZT',
			oids: pks // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
			// outputType: 'output'
		}
	);
}


function outputData(props) {

	let multiLang = props.MutiInit.getIntl(app_code);
	let printData = props.table.getCheckedRows(tableid);
	let pks = [];
	printData.forEach((item) => {
		pks.push(item.data.values.pk_billpoollimit.value);
	});

	if (pks == null || pks.length == 0) {
		toast({ color: 'warning', content: multiLang && multiLang.get('36101012-000003') });/* 国际化处理： 未选中行！*/
		return;
	}
	output({
		url: base_url + 'print.do',
		data: {
			oids: pks,
			outputType: 'output'
		}
	});
}

function refresh(props) {
	//暂存查询条件
	let queryData = cacheTools.get(cache_key);

	if (queryData) {
		ajax({
			url: '/nccloud/obm/ebankbillpoollimit/query.do',
			data: queryData,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					if (data) {
						props.table.setAllTableData(tableid, data[tableid]);
						toast({ color: "success" , content: props.MutiInit.getIntl(app_code) && props.MutiInit.getIntl(app_code).get('36101012-000024') });
					}
					else {
						props.table.setAllTableData(tableid, { rows: [] });
						toast({ color: "warning" , content: props.MutiInit.getIntl(app_code) && props.MutiInit.getIntl(app_code).get('36101012-000006') });
					}	
				}
			}
		});
	}

}
