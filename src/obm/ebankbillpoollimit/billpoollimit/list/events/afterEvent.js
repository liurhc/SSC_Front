
import { app_code } from '../../cons/constant.js';

export default function (props, moduleId, key, changerows, value, data, index) {
    // if (key === 'pk_org'&&props.table.getTableValueBykey(moduleId, 'pk_org')) {
    //     props.table.setTableValueBykey(moduleId, 'nsinglemny', '222');
    // }

    let multiLang = props.MutiInit.getIntl(app_code);


    const { form } = this.props;

    if (moduleId === 'ebank_onlinequery') {
       if (key === 'pk_zjorg') {
            const pk_zjorg = form.getFormItemsValue(moduleId, 'pk_zjorg').value;
            if (pk_zjorg) {

                //资金组织切换 银行类别 银行账号清空
                if (form.getFormItemsValue(moduleId, 'banktype').value) {
                    form.setFormItemsValue(moduleId, { 'banktype': { value: '', display: '' } });
                }
            }
        } 
    }
}
