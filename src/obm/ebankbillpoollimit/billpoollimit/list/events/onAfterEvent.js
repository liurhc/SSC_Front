export default function (field, val) {

    const { search } = this.props;

    if (field === 'pk_zjorg') {

        if (val.refpk) {
            //财务组织切换 银行类别 银行账号清空
            if (search.getSearchValByField('search', 'banktype')) {
                search.setSearchValByField('search', 'banktype', {
                    value: '',
                    display: ''
                })
            }
        }
    } 
}
