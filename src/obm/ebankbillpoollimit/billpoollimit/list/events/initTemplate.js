﻿import { ajax, base,getBusinessInfo,cacheTools } from 'nc-lightapp-front';
//引入常量定义
import { app_code, base_url,list_page_id,list_search_id,list_table_id,cache_key,fun_code } from '../../cons/constant.js';

let { NCPopconfirm, NCIcon, NCMessage } = base;
let searchId = list_search_id;
let tableId = list_table_id;
let pageId = list_page_id;
let appcode = app_code;

export default function (props) {
	//请求模板数据
	props.createUIDom(
		{
			pagecode: pageId,//页面id
			appcode: appcode//注册按钮编码
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					//TODO 新盘去掉
					//meta.formrelation={'ebank_onlinequery':[]};
					meta['ebank_onlinequery'].status = 'edit';
					meta = modifierMeta(props, meta);
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					let queryData = cacheTools.get(cache_key);
					// if(queryData){
					 	props.button.setButtonDisabled({
							refresh: false
					 	});
					//  }else{
					// 	props.button.setButtonDisabled({
					// 		refresh: true
					// 	});
					// }

					props.button.setButtonDisabled(['output', 'printgroupbtn'], true);
					
				}
			}
		}
	)
}

function modifierMeta(props, meta) {

	const { search, form } = props;
	let multiLang = props.MutiInit.getIntl(app_code);
	let businessInfo = getBusinessInfo();
	let { groupId } = businessInfo

	//查询界面字段增加过滤条件
	meta["search"].items.map((item) => {
		if (item.attrcode == 'pk_zjorg') {
			item.queryCondition = () => {
				return {
					funcode: fun_code,
					// TreeRefActionExt: 'nccloud.web.obm.refbuilder.FinanceOrgRefBuilder'
					TreeRefActionExt:'nccloud.web.tmpub.filter.FinanceOrgPermissionFilter',

				};
			};
		} 
	});


	//在线下载界面 字段增加过滤条件
	//TODO 使用权参照存在bug 财务组织不选时 查询不到数据
	meta["ebank_onlinequery"].items.map((item) => {
		if (item.attrcode == 'pk_zjorg') {
			item.queryCondition = () => {
				return {
					funcode: fun_code,
					// TreeRefActionExt: 'nccloud.web.obm.refbuilder.FinanceOrgRefBuilder'
					TreeRefActionExt:'nccloud.web.tmpub.filter.FinanceOrgPermissionFilter'
				};
			};
		} 
	});
	return meta;
}


//刷新页面数据方法
function refreshFun(props) {
	// let searchVal = props.search.getAllSearchData(searchid);
	// let data = {
	// 	conditions: searchVal,
	// 	pageInfo: {
	// 		currentPageIndex: 0,
	// 		pageSize: 10,
	// 		total: 0,
	// 		pageCount: 0
	// 	}
	// };

	// ajax({
	// 	url: '/nccloud/obm/ebankdzd/refresh.do',
	// 	data: data,
	// 	success: function(res) {
	// 		props.table.setAllTableData(tableid, res.data['0001']);
	// 	}
	// });
}
