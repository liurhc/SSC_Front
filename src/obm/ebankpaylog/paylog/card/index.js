//主子表卡片
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast,createPageIcon } from 'nc-lightapp-front';
let { NCFormControl, NCAnchor, NCScrollLink, NCScrollElement, NCAffix,NCBackBtn } = base;
import { buttonClick, initTemplate, afterEvent,pageInfoClick,tableButtonClick } from './events';
import './index.less';
class Card extends Component {
	constructor(props) {
		super(props);
		this.state = {
			bill_code: "",
			openflag: 'true'	// 展开
		}
		this.formId = 'head';
		this.tableId = 'glgx';

		initTemplate.call(this, props);

	}
	componentDidMount() {
		// this.toggleShow();
		let status = this.props.getUrlParam("status");
		if (status != "add") {
			let pk = this.props.getUrlParam("id");
			if (pk && pk != "undefined") {
				this.getdata(pk, 0);
			}
		} else {
			this.setDefaultValue();
		}
	}

	setDefaultValue = () => {
		this.props.form.setFormItemsValue(formId, {
			bill_status: { value: "0", display: this.props.MutiInit.getIntl("36100PZCQ") && this.props.MutiInit.getIntl("36100PZCQ").get('36100PZCQ-000005') }/* 国际化处理： 自由态*/
		});
	};

	//通过单据id查询单据信息
	getdata = (pk, mark) => {
		ajax({
			url: '/nccloud/obm/ebankpaylog/querycard.do',
			data: { pk },
			success: res => {
				if (res.data && res.data.body) {
					this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
				}else{
					this.props.cardTable.setTableData(this.tableId, { rows: [] });
					this.props.form.EmptyAllFormValue(this.formId);
					this.setState({ bill_code:"" });
					toast({ content: this.props.MutiInit.getIntl("36100PZCQ") && this.props.MutiInit.getIntl("36100PZCQ").get('36100PZCQ-000006'), color: 'danger' });/* 国际化处理： 数据已被删除*/
					return;
				}
				if (res.data && res.data.head) {
					this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
					let bill_code = res.data.head[this.formId].rows[0].values.srcbillcode.value;
					this.setState({ bill_code });
				}
				if (mark == 1) {
					toast({ content: this.props.MutiInit.getIntl("36100PZCQ") && this.props.MutiInit.getIntl("36100PZCQ").get('36100PZCQ-000007'), color: 'success' });/* 国际化处理： 刷新成功*/
				}
			}
		});
	};

	// 返回箭头按钮
	ncBackBtnClick = () => { 
		this.props.pushTo('/list',{   
			type:'link',  
			appcode: '36100PZCQ',
			pagecode: '36100PZCQ_L01',
        });
	};


	render() {
		// let { form } = this.props;s
		let { cardTable, form, button,cardPagination } = this.props;
		let buttons = this.props.button.getButtons();
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createCardPagination } = cardPagination;
		let { createButtonApp } = this.props.button;
		let { createButton, getButtons } = button;
		const { createBillHeadInfo } = this.props.BillHeadInfo;
		// let { createModal } = modal;
		return (
			<div className="nc-bill-card">
				<div className="nc-bill-top-area">
				<NCAffix>
					<div className="nc-bill-header-area">
						<div className="header-title-search-area">
							{
								createBillHeadInfo(
									{
										title: this.props.MutiInit.getIntl("36100PZCQ") && this.props.MutiInit.getIntl("36100PZCQ").get('36100PZCQ-000008'),  //标题
										billCode: this.state.bill_code,     //单据号
										backBtnClick: () => {           //返回按钮的点击事件
											this.ncBackBtnClick();
										}
									}
								)}
						</div>
						<div className="header-button-area">
							{
								this.props.button.createButtonApp({
									area: 'card_head',
									buttonLimit: 4,
									onButtonClick: buttonClick.bind(this),
									popContainer: document.querySelector('.header-button-area')
								})
							}

						</div>
						<div className="header-cardPagination-area" style={{ float: 'right' }}>
							{createCardPagination({
								handlePageInfoChange: pageInfoClick.bind(this)
							})}
						</div>
					</div>
				</NCAffix>
				<NCScrollElement name='forminfo'>
					<div className="nc-bill-form-area">
						{createForm(this.formId, {
							onAfterEvent: afterEvent.bind(this)
						})}
					</div>
				</NCScrollElement>
				</div>
				<NCScrollElement name='businfo'>
					<div className="nc-bill-table-area">
						{createCardTable(this.tableId, {
							adaptionHeight: true,
							showCheck: true,
							showIndex: true,
							onAfterEvent: afterEvent.bind(this)
						})}
					</div>
				</NCScrollElement>
			</div>
		);
	}
}

Card = createPage({
	mutiLangCode: '36100PZCQ'
	// initTemplate: initTemplate
})(Card);

// ReactDOM.render(<Card />, document.querySelector('#app'));
export default Card;
