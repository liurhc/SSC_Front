import { ajax, cardCache } from 'nc-lightapp-front';
import { tableId, pageId, formId, dataSource } from '../constants';
export default function (props, pks) {
    let { getCacheById, updateCache } = cardCache;
    props.setUrlParam(pks);
    /*
    * id：数据主键的值
    * dataSource: 缓存数据命名空间
    */

    if (pks) {
        let cardData = getCacheById(pks, dataSource);
        if (cardData) {
            this.props.form.setAllFormValue({ [formId]: cardData.head[formId] });
            this.props.cardTable.setTableData(tableId, cardData.body[tableId]);
            this.setState({
                bill_code: cardData.head[formId].rows[0].values.srcbillcode.value,
            });
            // this.toggleShow();
        } else {
            let dataArr = pks;
            //主键数组
            // dataArr.push(pks);
            let data = {
                pk: dataArr,
                pageId: pageId 
            }; 
            ajax({
                url: '/nccloud/obm/ebankpaylog/querycard.do',
                data: data,
                success: (res) => {
                    if (res.data) {
                        let bill_code = '';
                        if (res.data.head) {
                            this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
                            /*
                            * idname: 数据主键的命名
                            * id：数据主键的值
                            * headAreacode: 卡片表头的区域编码
                            * dataSource: 缓存数据命名空间
                            */
                            updateCache('pk_paylog_h', pks, res.data, formId, dataSource);
                            bill_code = res.data.head[formId].rows[0].values.srcbillcode.value;
                            this.setState({
                                bill_code
                            });
                        }
                        if (res.data.body) {
                            this.props.cardTable.setTableData(tableId, res.data.body[tableId]);
                        }
                        props.pushTo("/card", {
                            status: 'browse',
                            id: pks,
                        });
                        // this.toggleShow();
                    } else {
                        this.props.form.setAllFormValue({ [formId]: { rows: [] } });
                        this.props.cardTable.setTableData(tableId, { rows: [] });
                    }
                }
            });
        }
    }
}
