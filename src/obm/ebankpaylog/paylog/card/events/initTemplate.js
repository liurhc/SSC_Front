import { base, ajax } from 'nc-lightapp-front';
import tableButtonClick from './tableButtonClick';

let { NCPopconfirm } = base;

let formId = 'head';
let tableId = 'glgx';
let pageId = '36100PZCQ_C01';


export default function(props) {

	let that = this;

	props.createUIDom(
		{
			pagecode: pageId,//页面id
			appcode: '36100PZCQ'//注册按钮的id
		},
		function (data) {
		// let data = cardTemplate.data;
			if (data) {
				if (data.template) {
					let meta = data.template;
					modifierMeta(that,props, meta)
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
			}
		}
	)
}


function modifierMeta(that,props,meta) {

	let porCol = {
		attrcode: 'opr',
		label: props.MutiInit.getIntl("36100PZCQ") && props.MutiInit.getIntl("36100PZCQ").get('36100PZCQ-000003'),/* 国际化处理： 操作*/
		fixed: 'right',
		itemtype: 'customer',
		visible: true,
		width: '200px',
		render(text, record, index) {
			let status = props.getUrlParam("status");
			let buttonAry = ['EbankLogCreatPmtConfirmAction', that.state.openflag&&'open_inner',!that.state.openflag&&'close_inner'];
			
			return props.button.createOprationButton(buttonAry, {
				area: 'card_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => tableButtonClick.call(that, props, key, text, record, index)
			});
		}
	};

	meta[tableId].items.push(porCol);
	
	return meta;
}
