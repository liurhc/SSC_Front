import { ajax, base, toast, print,output } from 'nc-lightapp-front';

export default function(props, id) {
  switch (id) {
    //刷新按钮
    case 'Refresh':
      this.getdata(props.getUrlParam('id'),1);
      break;
    //状态下载
    case 'EbankLogDownloadStateAction':
    let pk = props.getUrlParam('id');
    let pks = [props.getUrlParam('id')];
    ajax({
      url: '/nccloud/obm/ebankpaylog/ebankLogDownload.do',
      data: {pks},
      success: (res) => {
        this.getdata(pk,1);
        if (res.data && res.data.msg) {
          toast({ content: res.data.msg , color: 'success' });
        }
      }
    });
      break;
    //状态确认
    case 'EbankLogCreatPmtConfirmAction':
    
      let pk1 = props.getUrlParam('id');
      pk1 = pk1 + ',';

      let data = props.cardTable.getCheckedRows("glgx");
      let markxz = '0';//有选择行
      if(data && data.length >1 && data[0].data.values.func.value!='dfdk'){
        toast({ color: 'warning', content: props.MutiInit.getIntl("36100PZCQ") && props.MutiInit.getIntl("36100PZCQ").get('36100PZCQ-000001')});/* 国际化处理： 对公支付一次只能确认一条,请选择一条表体数据进行确认.*/
				return
      }
      if(data && data.length >0){
        let bid = data[0].data.values.pk_ebank_paylog.value;//选中的表体pk
        pk1 = pk1+bid;
      }else{
        markxz = '1';//没有选择行
        data = props.cardTable.getAllRows("glgx");
        if(data && data.length >1 && data[0].values.func.value!='dfdk'){
          toast({ color: 'warning', content: props.MutiInit.getIntl("36100PZCQ") && props.MutiInit.getIntl("36100PZCQ").get('36100PZCQ-000001')});/* 国际化处理： 对公支付一次只能确认一条,请选择一条表体数据进行确认.*/
          return
        }
      }
      let paystate = '0';
      data.map((v) => {
        // let bid = v.values.pk_ebank_paylog.value;//选中的表体pk
        let state;
        if(markxz=='0'){
          state = v.data.values.paystate.value
        }else{
          state = v.values.paystate.value
        }
        
       
        if(state && state ==="2"){//只要有一张失败的单据就可以传到后台
          paystate = '1';
        }
        // if(bid && bid != "undefined"){
        //   pk1 = pk1+bid;
        // }
      });
      
      if(paystate != "1"){
        toast({ color: 'warning', content: props.MutiInit.getIntl("36100PZCQ") && props.MutiInit.getIntl("36100PZCQ").get('36100PZCQ-000002')});/* 国际化处理： 没有需要确认的指令！*/
				return
      }
      props.openTo('/obm/ebankconfirmpay/confirmpay/main/index.html#/card', {
         status: 'add',
         srcbilltype:'36C3',//支付指令状态
         appcode:'36100CONFM',
         pagecode:'36100CONFM_C01',
         id: pk1
      });
      break;
    //状态同步
    case 'EbankLogFireEventSendStateAction':
    let pk2 = props.getUrlParam('id');
    pks = [props.getUrlParam('id')];
    ajax({
      url: '/nccloud/obm/ebankpaylog/ebankLogFireEventSendState.do',
      data: {pks},
      success: (res) => {
        this.getdata(pk2,1);
        if (res.data && res.data.msg) {
          toast({ content: res.data.msg , color: 'success' });
        }
      }
    });
      break;
    case 'Print':
      printData(props.getUrlParam('id'));
      break;
    case 'output':
      outputData(props.getUrlParam('id'));
      break;
    case 'Preview':
      printData(props.getUrlParam('id'));
      break;
    case 'backBtn': 
      props.pushTo('/list',{   
        type:'link',  
        appcode: '36100PZCQ',
        pagecode: '36100PZCQ_L01',
      });
      break;
    default:
      break
  }
}


/**
 * 打印、预览
 * @param {*} pk 
 */
function printData(pk) {
  let pks = [pk];
  
  print(
      'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
      '/nccloud/obm/ebankpaylog/print.do',
      {
          funcode: '36100PZCQ', //功能节点编码，即模板编码
          appcode: '36100PZCQ', //模板节点标识
          // printTemplateID: '1001Z61000000000LPN4',//列表打印模板pk 
          oids: pks // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
      }
  );
}


function outputData(pk) {
  let pks = [pk];
  
  output({
    url: '/nccloud/obm/ebankpaylog/print.do',
    data: { 
        oids: pks,
        outputType: 'output'
    }
  });
}
