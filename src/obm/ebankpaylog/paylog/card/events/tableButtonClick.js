import { createPage, ajax, base, toast } from 'nc-lightapp-front';
const tableid = 'glgx';

// export default function tableButtonClick(props, key, text, record, index) {
	export default function (props, key, text, record, index){
	switch (key) {
		//状态确认
		case 'EbankLogCreatPmtConfirmAction':
			let bid = record.values.pk_ebank_paylog.value;
			let state = record.values.paystate.value;//选中的表体状态
			let pk1 = props.getUrlParam('id');
			pk1 = pk1 + ','+bid;
			if(state != "2"){
        	toast({ color: 'warning', content: props.MutiInit.getIntl("36100PZCQ") && props.MutiInit.getIntl("36100PZCQ").get('36100PZCQ-000004')});/* 国际化处理： 该指令不需要进行确认！*/
				return
      		}
			props.openTo('/obm/ebankconfirmpay/confirmpay/main/index.html#/card', {
				status: 'add',
				srcbilltype:'36C3',//支付指令状态
				appcode:'36100CONFM',
				pagecode:'36100CONFM_C01',
				id: pk1
			}); 
			break;
		//展开
		case 'open_inner':
			props.cardTable.toggleRowView(tableid, record);
			this.setState({ openflag: false });
			break;
		//收起
		case 'close_inner':
			props.cardTable.toggleRowView(tableid, record);
			this.setState({ openflag: true });
			break;
		default:
			break;
	}
}


