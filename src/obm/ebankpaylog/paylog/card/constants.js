/**
 * 表体区域
 */
export const tableId = 'glgx';

/**
 * 表头区域
 */
export const formId = 'head';

/**
 * 页面编码
 */
export const pageId = '36100PZCQ_C01';

export const appcode = '36100PZCQ';

export const dataSource = 'tm.obm.paylog.paylogdataSource';
