const tableid = 'ebank_paylog_h';

export default function clickBtn(props) {
	let selectdata = props.table.getCheckedRows(tableid);

	if (!selectdata || selectdata.length == 0) {
		//没有选中
		props.button.setDisabled({
			EbankLogDownloadStateAction: true,
			EbankLogFireEventSendStateAction:true,
			Print:true,
			output:true,
			Refresh:false
		});
	} else {//选中
		props.button.setDisabled({
			EbankLogDownloadStateAction: false,
			EbankLogFireEventSendStateAction:false,
			Print:false,
			output:false,
			Refresh:false
		});

	}
}
