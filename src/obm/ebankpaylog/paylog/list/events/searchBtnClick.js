import {ajax,toast,cacheTools,cardCache} from 'nc-lightapp-front';
import { dataSource } from '../../card/constants';
let{setDefData,getDefDate,getDefData,deleteCacheById}=cardCache;
let tableid = 'ebank_paylog_h';
//点击查询，获取查询区数据
export default function clickSearchBtn(props,searchVal) {
    let backmark = cacheTools.get('36100PZCQ_C01_back');
    console.log(searchVal);
    //缓存查询条件
    cacheTools.set('36100PZCQ_C01_back', '0');
	cacheTools.set('36100PZCQ_L01_search', searchVal);
    let pageInfo = props.table.getTablePageInfo(tableid);
    let metaData = props.meta.getMeta();
    let queryInfo = props.search.getQueryInfo('36100PZCQ', false);
    let oid = queryInfo.oid;
    let data={
        conditions:searchVal.conditions || searchVal,
        pageInfo:pageInfo,
        pagecode: "36100PZCQ_L01",
        queryAreaCode:"36100PZCQ",
        oid:oid,
        queryType:"simple"
     };

    console.log(data);
    ajax({
        url: '/nccloud/obm/ebankpaylog/query.do',//'/nccloud/reva/search/query.do',
        data: data,
        success:  (res)=> {
            let { success, data } = res;
            if(success){
                props.button.setDisabled({
                    EbankLogDownloadStateAction: true,
                    EbankLogFireEventSendStateAction:true,
                    Print:true,
                    output:true,
                    Refresh:false
                });
                if(data && data[tableid]){
                    props.table.setAllTableData(tableid, res.data[tableid]);
                    setDefData(tableid,dataSource,data);
                    if(!backmark || backmark==="0"){
                       let begString =props.MutiInit.getIntl("36100PZCQ") && props.MutiInit.getIntl("36100PZCQ").get('36100PZCQ-000012');
                        let endString = props.MutiInit.getIntl("36100PZCQ") && props.MutiInit.getIntl("36100PZCQ").get('36100PZCQ-000013');
                        let length = res.data[tableid].allpks.length;
                        toast({content: begString + length + endString,color:"success"});/* 国际化处理： 查询成功，共,条*/
                    }
                }else{
                    props.table.setAllTableData(tableid, {rows:[]});
                    if(!backmark || backmark==="0"){
                        toast({content:props.MutiInit.getIntl("36100PZCQ") && props.MutiInit.getIntl("36100PZCQ").get('36100PZCQ-000011'),color:"warning"});/* 国际化处理： 无数据*/
                    }
                }	
            }
        },
        error : (res)=>{
			console.log(res.message);
		}
    });

};
