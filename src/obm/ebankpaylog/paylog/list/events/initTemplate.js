import { ajax, base, cacheTools } from 'nc-lightapp-front';
let { NCPopconfirm, NCIcon, NCTooltip:Tooltip } = base;
import searchBtnClick from './searchBtnClick';
import tableButtonClick from './tableButtonClick';

let tableid = 'ebank_paylog_h';
let pageId = '36100PZCQ_L01';
let searchId = '36100PZCQ';
export default function (props) {
	//请求模板数据
	props.createUIDom(
		{
			pagecode: pageId,//页面id
			appcode: '36100PZCQ'//注册按钮的id
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta)
					props.meta.setMeta(meta, () => {

						//如果有缓存条件，查询
						let querycondition = cacheTools.get('36100PZCQ_L01_search');
						console.log(querycondition);
						if (querycondition) {
							cacheTools.set('36100PZCQ_C01_back', '1');
							props.search.setSearchValue('36100PZCQ', querycondition);
							searchBtnClick(props, querycondition);
						}
					});
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
			}
		}
	)
}

function modifierMeta(props, meta) {
	meta[searchId].items = meta[searchId].items.map((item, key) => {
		// item.visible = true;
		item.col = '2';
		return item;
	})

	// 查询条件多选
	meta[searchId].items.find((e) => e.attrcode === 'pk_org').isMultiSelectedEnabled = true;
	//财务组织用户过滤
	meta[searchId].items.map((item)=>{
		if (item.attrcode == 'pk_org' ) {
			       item.queryCondition = () => {
			       		return {
			            	funcode: '36100PZCQ',
			            	TreeRefActionExt: 'nccloud.web.tmpub.filter.FinanceOrgPermissionFilter'
			        	};
			        };
			     }
	});


	meta[tableid].items = meta[tableid].items.map((item, key) => {
		// item.width = 180;
		if (item.attrcode == 'srcbillcode') {
			item.render = (text, record, index) => {
				let tip = (<div>{record.srcbillcode.value}</div>)
				return (
					<a
						style={{ textDecoration: '', cursor: 'pointer' }}
						onClick={() => {
							let searchVal = props.search.getAllSearchData("36100PZCQ");
    						cacheTools.set("36100PZCQ_L01_search", searchVal); 
							props.pushTo('/card', {
								status: 'browse',
								type:'link', 
								id: record.pk_ebank_paylog_h.value,
								pagecode: '36100PZCQ_C01',
								appcode:'36100PZCQ'
							});
						}}
					>
						{record && record.srcbillcode && record.srcbillcode.value}
					</a>
				);
			};
		}
		return item;
	});
	//添加操作列
	meta[tableid].items.push({
		attrcode: 'opr',
		label: props.MutiInit.getIntl("36100PZCQ") && props.MutiInit.getIntl("36100PZCQ").get('36100PZCQ-000003'),/* 国际化处理： 操作*/
		width: 200,
		className: 'table-opr',
		itemtype: 'customer',
		// 锁定
		fixed:'right',
		visible: true,
		render: (text, record, index) => {
			let buttonAry = ['EbankLogFireEventSendStateAction', 'EbankLogDownloadStateAction'];
			return props.button.createOprationButton(buttonAry, {
				area: 'list_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => {
					tableButtonClick.call(this, props, key, text, record, index);
				}
			});
		}
	});

	return meta;
}
