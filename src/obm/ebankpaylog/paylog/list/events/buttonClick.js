import { ajax, base, print,toast,output,cacheTools } from 'nc-lightapp-front';
import searchBtnClick from './searchBtnClick';
const tableid = 'ebank_paylog_h';
const sreachId = '36100PZCQ';

export default function buttonClick(props, id) {
	console.log(id)
	switch (id) {
		case 'Refresh':
			refreshAction(props);
			break;
		//状态下载
		case 'EbankLogDownloadStateAction':
			let printData = props.table.getCheckedRows(tableid);
			//数据校验
			if (printData.length == 0) {
				toast({ color: 'warning', content:props.MutiInit.getIntl("36100PZCQ") && props.MutiInit.getIntl("36100PZCQ").get('36100PZCQ-000009')});/* 国际化处理： 请选择数据*/
				return
			}
			let pks = [];
			printData.forEach((item) => {
				pks.push(item.data.values.pk_ebank_paylog_h.value);
			});
			ajax({
				url: '/nccloud/obm/ebankpaylog/ebankLogDownload.do',
				data: { pks },
				success: (res) => {
					if (res.data && res.data.msg) {
						toast({ content: res.data.msg, color: 'success' });
					}
				}
			});
			break;
		//状态同步
		case 'EbankLogFireEventSendStateAction':
		   printData = props.table.getCheckedRows(tableid);
			//数据校验
			if (printData.length == 0) {
				toast({ color: 'warning', content: props.MutiInit.getIntl("36100PZCQ") && props.MutiInit.getIntl("36100PZCQ").get('36100PZCQ-000009')});/* 国际化处理： 请选择数据*/
				return
			}
			pks = [];
			printData.forEach((item) => {
				pks.push(item.data.values.pk_ebank_paylog_h.value);
			});
		ajax({
		  url: '/nccloud/obm/ebankpaylog/ebankLogFireEventSendState.do',
		  data: {pks},
		  success: (res) => {
			if (res.data && res.data.msg) {
			  toast({ color: 'success', content: res.data.msg});
			}
		  }
		});
		  break;
		case 'output':
			printData = props.table.getCheckedRows(tableid);
			//数据校验
			if (printData.length == 0) {
				toast({ color: 'warning', content: props.MutiInit.getIntl("36100PZCQ") && props.MutiInit.getIntl("36100PZCQ").get('36100PZCQ-000009')});/* 国际化处理： 请选择数据*/
				return
			}
			pks = [];
			printData.forEach((item) => {
				pks.push(item.data.values.pk_ebank_paylog_h.value);
			});
			output({
				url: '/nccloud/obm/ebankpaylog/print.do',
				data: { 
					 oids: pks,
					 outputType: 'output'
				}
			});
			break;
		case 'Print':
			printData = props.table.getCheckedRows(tableid);
			//数据校验
			if (printData.length == 0) {
				toast({ color: 'warning', content: props.MutiInit.getIntl("36100PZCQ") && props.MutiInit.getIntl("36100PZCQ").get('36100PZCQ-000009') });/* 国际化处理：  请选择数据*/
				return
			}
			pks = [];
			printData.forEach((item) => {
				pks.push(item.data.values.pk_ebank_paylog_h.value);
			});
			print(
				'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
				'/nccloud/obm/ebankpaylog/print.do',
				{
					funcode: '36100PZCQ', //功能节点编码，即模板编码
					appcode: '36100PZCQ', //模板节点标识
					oids: pks // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
				}
			);
			break;
		case 'Preview':
			printData(props);
			break;
		default:
			break
	}
}

/**
 * 打印、预览
 * @param {*} props 
 */
function printData(props) {
	let printData = props.table.getCheckedRows(tableid);
	//数据校验
	if (printData.length == 0) {
		toast({ color: 'warning', content: props.MutiInit.getIntl("36100PZCQ") && props.MutiInit.getIntl("36100PZCQ").get('36100PZCQ-000009') });/* 国际化处理：  请选择数据*/
		return
	}
	let pks = [];
	printData.forEach((item) => {
		pks.push(item.data.values.pk_ebank_paylog_h.value);
	});
	print(
		'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
		'/nccloud/obm/ebankpaylog/print.do',
		{
			funcode: '36100PZCQ', //功能节点编码，即模板编码
			appcode: '36100PZCQ', //模板节点标识
			// printTemplateID: '1001Z61000000000LPN4',//列表打印模板pk 
			oids: pks // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
		}
	);
}


/**
 * 刷新
 * @param {*} props 
 */
const refreshAction = (props) => {
	// let searchVal = props.search.getAllSearchData(sreachId,false);
	let pageInfo = props.table.getTablePageInfo(tableid);
	let metaData = props.meta.getMeta();
	//如果有缓存条件，查询
	let querycondition = cacheTools.get('36100PZCQ_L01_search');
	console.log(querycondition);
	if (!querycondition) {
		return;
	}
		cacheTools.set('36100PZCQ_C01_back', '1');
		props.search.setSearchValue('36100PZCQ', querycondition);
		let searchVal = props.search.getAllSearchData(sreachId);
		let queryInfo = props.search.getQueryInfo('36100PZCQ', false);
        let oid = queryInfo.oid;					
	let data = {
		conditions: searchVal.conditions || searchVal,
		pageInfo: pageInfo,
		pagecode: "36100PZCQ_L01",
		queryAreaCode: "36100PZCQ",
		oid: oid,
		queryType: "simple"
	};
	console.log(data);
	ajax({
		url: '/nccloud/obm/ebankpaylog/query.do',//'/nccloud/reva/search/query.do',
		data: data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data && data[tableid]) {
					props.table.setAllTableData(tableid, res.data[tableid]);
					toast({ color: 'success', content: props.MutiInit.getIntl("36100PZCQ") && props.MutiInit.getIntl("36100PZCQ").get('36100PZCQ-000014') });/* 国际化处理： 刷新成功*/
				} else {
					props.table.setAllTableData(tableid, { rows: [] });
					toast({ color: 'warning', content: props.MutiInit.getIntl("36100PZCQ") && props.MutiInit.getIntl("36100PZCQ").get('36100PZCQ-000011') });/* 国际化处理： 未找到符合条件的记录*/
				}
			}
		},
		error: (res) => {
			console.log(res.message);
		}
	});
}
