import { ajax, toast ,cacheTools} from 'nc-lightapp-front';

export default function doubleClick(record, index, e) {
    let searchVal = this.props.search.getAllSearchData("36100PZCQ");
    cacheTools.set("36100PZCQ_L01_search", searchVal);
    this.props.pushTo('/card', {
        status: 'browse',
        type:'link', 
        id: record.pk_ebank_paylog_h.value,
        pagecode: '36100PZCQ_C01',
        appcode:'36100PZCQ'
    });
}
