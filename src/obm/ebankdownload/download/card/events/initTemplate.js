import { base, ajax,cacheTools } from 'nc-lightapp-front';

import { tableId, formId,appid, pagecode,funtypeForm } from "../constants";


export default function(props) {
	props.createUIDom(
		{
			pagecode: pagecode,//页面id
			appcode: '36100DC'//注册按钮的id
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta)
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				//设置上下文
				// setContext.call(this,data.context);
				//数据加载
				let pk_org = data.context.pk_org;
				console.log("data.context.pk_org");
				console.log(pk_org);
				//缓存
				cacheTools.set('36100DC_C01_pkorg', pk_org);
				// funtypeBypkOrg(props);
				if (pk_org) {
					console.log("cacheTools.36100DC_C01_pkorg");
				    console.log(cacheTools.get('36100DC_C01_pkorg'));
					funtypeBypkOrg(props)
				} 
			}
		}
	)
}

function modifierMeta(props, meta) {
	let status = props.getUrlParam('status');
	// meta[formId].status = 'edit';
	meta[tableId].status = 'browse'; 

	meta[formId].items.map((item)=>{
		if (item.attrcode == 'pk_org' ) {
			       item.queryCondition = () => {
			       		return {
			            	funcode: '36100DC',
			            	TreeRefActionExt: 'nccloud.web.tmpub.filter.FinanceOrgPermissionFilter'
			        	};
			        };
			     }
	});
	return meta;
}

/*
*
* 设置上下文
*/
// function setContext(context) {
// 	this.setState({
// 		context_org: context.pk_org
// 	});
// }

function funtypeBypkOrg(props) { //加载银行账户下载设置的数据

	let	param = {
			// pkorg: me.state.context_org,
			pkorg: cacheTools.get('36100DC_C01_pkorg'),
			funtype: '',
			banktype: '',
			markGroup: '0'
		};
	ajax({
		data: param,
		url: '/nccloud/obm/ebankdownload/EbankdownloadFuntypeAction.do',
		success: function (res) {
			if (res.data && res.data.QueryDownloadFunType) {
				let meta = props.meta.getMeta();
				let items = meta[formId].items;
				let options = [];
				let index = res.data.QueryDownloadFunType.indexOf('*');
				let srcinfo = res.data.QueryDownloadFunType;
				if (index > 0) {
					let srcinfos = srcinfo.split('*');
					for (let i = 0; i < srcinfos.length; i++) {
						options[i] = {
							"display": srcinfos[i].split('+')[1],
							"value": srcinfos[i].split('+')[0]
						};
					}
				} else {
					let srcinfos = new Array(2);
					srcinfos = srcinfo.split('+');
					options[0] = {
						"display": srcinfos[1],
						"value": srcinfos[0]
					};
				}
				items.find((item) => item.attrcode == 'funtype').options = options;
				props.meta.setMeta(meta);
			}
			if (res.data && res.data.retcard && res.data.retcard.head) {
				// me.props.form.setAllFormValue({ ['head']: res.data.retcard.head['head'] });
				props.search.setSearchValByField(formId, 'deffuntype', {
					value: res.data.retcard.head.head.rows[0].values.deffuntype.value,
					display: res.data.retcard.head.head.rows[0].values.deffuntype.display
				});
				props.search.setSearchValByField(formId, 'pk_org', {
					value: res.data.retcard.head.head.rows[0].values.pk_org.value,
					display: res.data.retcard.head.head.rows[0].values.pk_org.display
				});
				// this.setState({
				// 	context_org: res.data.retcard.head.head.rows[0].values.pk_org.value
				// });
				// this.pkorg = res.data.retcard.head.head.rows[0].values.pk_org.value;
				props.button.setDisabled({
					DownloadFAEdit: false,
					Delete: false,
					plangroup: false,
					plan: false,
					Edit: false,
					Add: false,
					Refresh: false
				});
			}
			if (res.data && res.data.retcard && res.data.retcard.body) {
			    props.editTable.setTableData(tableId, res.data.retcard.body[tableId]);
			} else {
				props.editTable.setTableData(tableId, { rows: [] });
			}
			if (res.data && res.data.QueryDownloadSelectFunType) {
				let selectFunTypes = res.data.QueryDownloadSelectFunType.split('+');
				// this.funtype = selectFunTypes[0];
				// this.setState({
				// 	context_funtype: selectFunTypes[0]
				// });
				props.search.setSearchValByField(formId, 'funtype', {
					value: selectFunTypes[0],
					display: selectFunTypes[1]
				});
			}
		},
		error: (res) => {
			console.log(res.message);
		}
	});
}

// function funtypeBypkOrg(props,pk_org) {
// 	let	param = {
// 			pkorg: pk_org,
// 			funtype: '',
// 			banktype: '',
// 			markGroup:'0'
// 		};
// 	ajax({
// 		data: param,
// 		url: '/nccloud/obm/ebankdownload/EbankdownloadFuntypeAction.do',
// 		success: function (res) {
// 			if (res.data && res.data.QueryDownloadFunType) {
// 				let meta = props.meta.getMeta();
// 				let items = meta[formId].items;
// 				let options = [];
// 				let index = res.data.QueryDownloadFunType.indexOf('*');
// 				let srcinfo = res.data.QueryDownloadFunType;
// 				if (index > 0) {
// 					let srcinfos = srcinfo.split('*');
// 					for (let i = 0; i < srcinfos.length; i++) {
// 						options[i] = {
// 							"display": srcinfos[i].split('+')[1],
// 							"value": srcinfos[i].split('+')[0]
// 						};
// 					}
// 				} else {
// 					let srcinfos = new Array(2);
// 					srcinfos = srcinfo.split('+');
// 					options[0] = {
// 						"display": srcinfos[1],
// 						"value": srcinfos[0]
// 					};
// 				}
// 				items.find((item) => item.attrcode == 'funtype').options = options;
// 				props.meta.setMeta(meta);
// 			}
// 			if (res.data && res.data.retcard && res.data.retcard.head) {
// 				props.form.setAllFormValue({ [formId]: res.data.retcard.head[formId] });
// 				props.button.setDisabled({
// 					DownloadFAEdit: false,
// 					Delete:false,
// 					plangroup:false,
// 					plan:false,
// 					Edit:false,
// 					Add:false,
// 					Refresh:false
// 				});
// 			}
// 			if (res.data && res.data.retcard && res.data.retcard.body) {
// 				props.cardTable.setTableData(tableId, res.data.retcard.body[tableId]);
// 			}else{
// 				props.cardTable.setTableData(tableId, { rows: [] });
// 			}
// 			if (res.data && res.data.QueryDownloadSelectFunType) {
// 				let selectFunTypes = res.data.QueryDownloadSelectFunType.split('+');
// 				// funtype = selectFunTypes[0];
// 				props.form.setFormItemsValue(formId, { funtype: { value: selectFunTypes[0], display: selectFunTypes[1] } });
// 			}
// 		},
// 		error: (res) => {
// 			console.log(res.message);
// 		}
// 	});

// }
