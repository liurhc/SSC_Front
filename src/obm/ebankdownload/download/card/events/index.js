import buttonClick from './buttonClick';
import initTemplate from './initTemplate';
import afterEvent from './afterEvent';
import tableAfterEvent from './tableAfterEvent';
export { buttonClick, afterEvent,initTemplate,tableAfterEvent};
