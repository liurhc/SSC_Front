import { ajax, base, toast,promptBox } from 'nc-lightapp-front';
import { tableId, formId, appid, pagecode, funtypeForm } from "../constants";

export default function (props, id) {
  switch (id) {
    //刷新按钮
    case 'Refresh':
      this.loadEbanktype(true);
      break;
    //修改按钮
    case 'Edit':
      props.button.setButtonVisible(['Save', 'Cancel'], true);
      props.button.setButtonVisible(['plangroup', 'DownloadFAEdit', 'Delete', 'plan', 'Edit', 'Add', 'Refresh','accessoryBtn'], false);
      props.search.setDisabledByField(formId, 'pk_org', true);
      props.search.setDisabledByField(formId, 'funtype', true);
      props.syncTree.setNodeDisable('tree', true);
      props.editTable.setStatus(tableId, 'edit');
      break;
    //保存按钮
    case 'Save':
      let changedRows = props.editTable.getChangedRows(tableId);
      if (changedRows && changedRows.length != 0) {
        let data = props.createMasterChildData(pagecode, 'head', tableId, 'editTable');
      data.body.ebank_download.rows = changedRows;
      // data.head.head.rows[0].values.funtype = this.funtype;
      ajax({
        url: '/nccloud/obm/ebankdownload/EbankdownloadSaveAction.do',
        data: data,
        success: (res) => {
          this.loadEbanktype();
          toast({ color: 'success', content:this.props.MutiInit.getIntl("36100DC") && this.props.MutiInit.getIntl("36100DC").get('36100DC-000000')});/* 国际化处理： 保存成功*/
        },
        error: (res) => {
          toast({ color: 'danger', content: res.message });
        }
      });
      }
      props.button.setButtonVisible(['Save', 'Cancel'], false);
      props.button.setButtonVisible(['plangroup', 'DownloadFAEdit', 'Delete', 'plan', 'Edit', 'Add', 'Refresh','accessoryBtn'], true);
      props.search.setDisabledByField(formId, 'pk_org', false);
      props.search.setDisabledByField(formId, 'funtype', false);
      props.syncTree.setNodeDisable('tree', false);
      props.editTable.setStatus(tableId, 'browse');
      break;
    //取消按钮
    case 'Cancel':
    promptBox({
        color: 'warning',
        title: props.MutiInit.getIntl("36100DC") && props.MutiInit.getIntl("36100DC").get('36100DC-000014'),/* 国际化处理： 取消*/
        content: props.MutiInit.getIntl("36100DC") && props.MutiInit.getIntl("36100DC").get('36100DC-000015'),/* 国际化处理：  确定要取消吗？*/
				beSureBtnClick: this.cancelConfirm
      });
      break;
    case 'Add':
      this.pkorg = props.search.getSearchValByField(formId, 'pk_org').value.firstvalue;
      this.setState({
        showModal_publish: true,
        billId: '',//单据pk
        showModal_title:this.props.MutiInit.getIntl("36100DC") && this.props.MutiInit.getIntl("36100DC").get('36100DC-000001')/* 国际化处理： 新增方案*/
      });
      props.form.EmptyAllFormValue(funtypeForm);
      props.form.setFormStatus(funtypeForm, 'edit');
      break;
    case 'DownloadFAEdit':
      let allData = this.props.editTable.getAllData(tableId);
      this.setState({
        showModal_publish: true,
        billId: allData.rows[0].values.pk_ebank_download_h.value,//单据pk
        showModal_title:this.props.MutiInit.getIntl("36100DC") && this.props.MutiInit.getIntl("36100DC").get('36100DC-000002')/* 国际化处理： 修改方案*/
      });
      props.form.EmptyAllFormValue(funtypeForm); 
      let funtypeValue = props.search.getSearchValByField(formId, 'funtype');
      let deffuntypeValue = props.search.getSearchValByField(formId, 'deffuntype');
      let funname = funtypeValue.display.split('-');
      props.form.setFormItemsValue(funtypeForm, { funtype: { value: funname[1].trim(), display: funname[1].trim() } })
      props.form.setFormItemsValue(funtypeForm, { funcode: { value: funname[0].trim(), display: funname[0].trim() } })
      props.form.setFormItemsValue(funtypeForm, { deffuntype: { value: deffuntypeValue.value.firstvalue, display: deffuntypeValue.value.firstvalue } })
      props.form.setFormStatus(funtypeForm, 'edit');
      break;
    case 'Delete':
    promptBox({
      color: 'warning',
      title: props.MutiInit.getIntl("36100DC") && props.MutiInit.getIntl("36100DC").get('36100DC-000012'),/* 国际化处理： 删除*/
					content: props.MutiInit.getIntl("36100DC") && props.MutiInit.getIntl("36100DC").get('36100DC-000013'),/* 国际化处理： 确定要删除所有数据吗？*/
					beSureBtnClick: this.delConfirm
});
      break;
    case 'accessoryBtn':
      let accessoryBtnData = props.search.getSearchValByField(formId, 'funtype').value.firstvalue;
      let pk_rec =undefined;
      //选择一个或者不选择，多选默认显示空数据
      if (accessoryBtnData) {
        
            pk_rec = accessoryBtnData;
         
      } else {
        toast({
          duration: 3,          // 消失时间，默认是3秒; 值为 infinity 时不消失,非必输
          color: 'warning',     // 提示类别，默认是 "success",非必输
          title: this.props.MutiInit.getIntl("36100DC") && this.props.MutiInit.getIntl("36100DC").get('36100DC-000003'),      // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输/* 国际化处理： 请注意*/
          content: this.props.MutiInit.getIntl("36100DC") && this.props.MutiInit.getIntl("36100DC").get('36100DC-000004') /* 国际化处理： 附件支持单条操作!*/
        })
        return;
      }
      
      console.log(pk_rec, this.props.MutiInit.getIntl("36100DC") && this.props.MutiInit.getIntl("36100DC").get('36100DC-000005'));/* 国际化处理： 附件*/
      console.log(pk_rec, 'pk_rec');
    
      if (!pk_rec) {
        toast({
            duration: 3,          // 消失时间，默认是3秒; 值为 infinity 时不消失,非必输
            color: 'warning',     // 提示类别，默认是 "success",非必输
            title: this.props.MutiInit.getIntl("36100DC") && this.props.MutiInit.getIntl("36100DC").get('36100DC-000003'),      // 提示标题, 默认不同类别下分别为："已成功"/"帮助信息"/"请注意"/"出错啦",非必输/* 国际化处理： 请注意*/
            content: this.props.MutiInit.getIntl("36100DC") && this.props.MutiInit.getIntl("36100DC").get('36100DC-000006')   // 提示内容,非必输/* 国际化处理： 操作失败，无数据!*/
        
        })
        return;
    }
    
    this.setState({
        billId: pk_rec,//单据pk
        billno: pk_rec,//附件管理使用单据编号
        showUploader: !this.state.showUploader,
        target: null
    })
      break;
    default:
      break;
  }
}
