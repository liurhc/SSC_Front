import { ajax } from 'nc-lightapp-front';
import { dateformat } from '../utils';
import { tableId, formId,appid, pagecode,funtypeForm } from "../constants";

export default function afterEvent(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	if (key === 'isdetaildownload' ) {
		if(value){
			let now = new Date();
			let nowtime = dateformat(now, 'yyyy-MM-dd hh:mm:ss');
			let today = dateformat(now, 'yyyy-MM-dd');
			//日期
			props.editTable.setValByKeyAndIndex(tableId, index, 'firstday', { display: today, value: nowtime });
		}else{
			props.editTable.setValByKeyAndIndex(tableId, index, 'firstday', { display: '', value: '' });
		}
	}
}
