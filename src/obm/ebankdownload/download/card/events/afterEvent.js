import { ajax } from 'nc-lightapp-front';
import { tableId, formId,appid, pagecode,funtypeForm } from "../constants";

export default function afterEvent(key,value) {
	this.pkorg = this.props.search.getSearchValByField(formId, 'pk_org').value.firstvalue;
	if (key === 'pk_org' ) {
		console.log("pk_org1");
		if(value.refpk && value.refpk != "undefined"){
			this.pkorg = value.refpk;
		}else{
			this.props.search.clearSearchArea(formId);
			this.props.editTable.setTableData(tableId, { rows: [] });
			this.props.button.setDisabled({
				DownloadFAEdit: true,
				Delete: true,
				plangroup: false,
				plan: false,
				Edit: true,
				Add: true,
				accessoryBtn: true,
				Refresh: false
			});
			return
		}
		this.funtype = undefined;
		this.props.button.setDisabled({
		    DownloadFAEdit: false,
		    Delete:false,
			plangroup:false,
			plan:false,
			Edit:false,
			Add:false,
			accessoryBtn: false,
			Refresh:false
		});
	}
	if(key === 'funtype'){
		this.funtype = value;
	}
	this.loadEbanktype();
}
