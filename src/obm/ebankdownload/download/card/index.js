//主子表卡片
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast, high, cacheTools,createPageIcon } from 'nc-lightapp-front';
const { NCUploader } = high
const { NCAffix,NCPopconfirm,NCFormControl,NCAnchor,NCScrollElement,NCScrollLink,NCModal,NCButton } = base;
import { tableId, formId, pagecode, funtypeForm } from "./constants";
import { buttonClick, initTemplate, afterEvent,tableAfterEvent } from './events';
import './index.less';
var urls = {
    queryContext: '/nccloud/platform/appregister/queryappcontext.do'
};

class Card extends Component {
	constructor(props) {
		super(props);
		this.state = {
			billId: '',//单据pk
			billno: '',//附件管理使用单据编号
			showUploader: false,//控制附件弹出框
			showModal_publish: false,
			showModal_title: '',
			context_org: '',
			context_funtype:'',
			target: null//控制弹出位置
		}
	}

	/**
     * 获取当前登录用户组织
     */
    getCurrOrg(){
        var me = this;
        let requestparam = {
            appcode: '36100DC'
        }
        ajax({
            url: urls['queryCurrOrg'],
            //async:false,
            data:requestparam,
            success: function (res) {
                if(res.success){
                    me.state.context_org = res.data.pk_org;
                    me.setState(me.state);
                }
            }
        })
        console.log(this.state);
    }

	componentDidMount() {
		// this.bankTypeRoot = {//为树创建一个根节点
		// 	isleaf: false,
		// 	pid: "",
		// 	// refname: this.props.MutiInit.getIntl("36100DC") && this.props.MutiInit.getIntl("36100DC").get('36100DC-000008'),/* 国际化处理： 全部账号*/
		// 	refname:'全部账号',
		// 	refpk: "0"
		// };
		this.curEbanktype = undefined;  //定义选中银行类别
		this.pkorg = undefined;
		this.funtype = undefined;
		this.toggleShow();
		this.loadBankType();
	 }

	componentWillMount(){
		//关闭浏览器
		 window.onbeforeunload = () => {
			 let status = this.props.editTable.getStatus(tableId)
			 if (status != 'browse') {
				 return this.props.MutiInit.getIntl("36100DC") && this.props.MutiInit.getIntl("36100DC").get('36100DC-000007');/* 国际化处理： 当前单据未保存，您确认离开此页面？*/
			 }
		 }
	 }

	close = () => {
		this.setState({ showModal_publish: false });
	}

	//删除
	delConfirm = () => {
		var me = this,
			param = {
				pk: me.funtype ? me.funtype.value ? me.funtype.value : me.funtype : '',
				pkorg: me.props.search.getSearchValByField(formId, 'pk_org').value.firstvalue
			};
		ajax({
			url: '/nccloud/obm/ebankdownload/EbankdownloadDelFuntypeAction.do',
			data: param,
			success: res => {
				this.funtype = undefined;
				this.props.search.setSearchValByField(formId, 'funtype', {
					value: '',
					display: ''
				});
				setTimeout(() => this.loadEbanktype(), 10);
				 toast({ color: 'success', content: this.props.MutiInit.getIntl("36100DC") && this.props.MutiInit.getIntl("36100DC").get('36100DC-000009') });/* 国际化处理：  删除成功*/
			}
		});
	};

	//取消
    cancelConfirm = () => {
		this.loadEbanktype();
	    this.props.button.setButtonVisible([ 'Save','Cancel' ], false);
	    this.props.button.setButtonVisible([ 'plangroup','DownloadFAEdit','Delete','plan','Edit','Add','Refresh','accessoryBtn' ], true);
		this.props.search.setDisabledByField(formId, 'pk_org', false);
		this.props.search.setDisabledByField(formId, 'funtype', false);
		this.props.syncTree.setNodeDisable('tree', false);
		this.props.editTable.setStatus(tableId, 'browse');
    };

	//确定方案
	sureConfirm = () => {
		let cardData = this.props.createMasterChildData(pagecode, funtypeForm, tableId,'editTable');
		let allData = this.props.editTable.getAllData(tableId);
		cardData.head.funtypeForm.rows[0].values.pk_org.value = this.pkorg;
		cardData.body.ebank_download.rows = allData.rows;
		cardData.head.funtypeForm.rows[0].values.obmdef5.value = this.state.billId;
		ajax({
			url: '/nccloud/obm/ebankdownload/EbankdownloadSaveFuntypeAction.do',
			data: cardData,
			success: res => {
				if (res.data) {
					this.setState({ showModal_publish: false });
					this.funtype = res.data.head.head.rows[0].values.pk_ebank_download_h.value;
					this.props.search.setSearchValByField(formId, 'funtype', {
						value: res.data.head.head.rows[0].values.pk_ebank_download_h.value,
						display: res.data.head.head.rows[0].values.pk_ebank_download_h.value
					});
					
				}
				this.loadEbanktype();
				toast({ color: 'success', content: this.props.MutiInit.getIntl("36100DC") && this.props.MutiInit.getIntl("36100DC").get('36100DC-000010') });/* 国际化处理： 操作成功*/
			}
		});
	};


	loadBankType() {//加载树数据
		// ajax({
		// 	url: '/nccloud/obm/ebankdownload/ebankdownloadTreeBanktypeAction.do',
		// 	// data: {pkorg:this.state.curOrg.refpk}, //参数带上选中的行政组织
		// 	success: (res) => {
		// 		this.props.syncTree.setSyncTreeData('tree', [Object.assign({ ...this.bankTypeRoot }, { children: res.data })]); //返回值合并根节点后设置树数据
		// 		this.props.syncTree.setNodeSelected('tree', this.bankTypeRoot.refpk);
		// 	}
		// });
		ajax({
			url: '/nccloud/obm/ebankdownload/ebankdownloadTreeBanktypeAction.do',
			// data: {pkorg:this.state.curOrg.refpk}, //参数带上选中的行政组织
			success: (res) => {
				let data = [Object.assign( {
					"isleaf": false,
					"key":"~",
					"id":"~",
					"innercode":"~",
					"pid": "",
					"refname": this.props.MutiInit.getIntl("36100DC") && this.props.MutiInit.getIntl("36100DC").get('36100DC-000008'),/* 国际化处理： 全部账号*/
					"refpk": "~"
				}, { children: res.data } )];
				this.props.syncTree.setSyncTreeData('tree', data); //返回值合并根节点后设置树数据
				this.props.syncTree.setNodeSelected('tree', "~");
			}
		});
	}

	//切换页面状态
	toggleShow = () => {
		this.props.button.setButtonVisible(['Save', 'Cancel'], false);
		this.props.button.setButtonVisible(['plangroup', 'DownloadFAEdit', 'Delete', 'plan', 'Edit', 'Add', 'Refresh'], true);
		this.props.button.setDisabled({
			DownloadFAEdit: true,
			Delete: true,
			plangroup: false,
			plan: false,
			Edit: true,
			Add: true,
			accessoryBtn: true,
			Refresh: true
		});


		this.props.cardTable.setStatus(tableId, 'browse');
	};
	onEbanktypeSelect(node, item, ischange) {   //选定银行类型
		this.curEbanktype = node;        //设置当前选中银行类型, 加载银行账户下载设置
		if (this.curEbanktype && this.curEbanktype != "undefined") {
			setTimeout(() => this.loadEbanktype(), 10);
		}
	}

	funtypeBypkOrg() { //加载银行账户下载设置的数据
		var me = this,
			param = {
				pkorg: cacheTools.get('36100DC_C01_pkorg'),
				funtype: '',
				banktype: '',
				markGroup: '0'
			};
		ajax({
			data: param,
			url: '/nccloud/obm/ebankdownload/EbankdownloadFuntypeAction.do',
			success: function (res) {
				if (res.data && res.data.QueryDownloadFunType) {
					let meta = me.props.meta.getMeta();
					let items = meta[formId].items;
					let options = [];
					let index = res.data.QueryDownloadFunType.indexOf('*');
					let srcinfo = res.data.QueryDownloadFunType;
					if (index > 0) {
						let srcinfos = srcinfo.split('*');
						for (let i = 0; i < srcinfos.length; i++) {
							options[i] = {
								"display": srcinfos[i].split('+')[1],
								"value": srcinfos[i].split('+')[0]
							};
						}
					} else {
						let srcinfos = new Array(2);
						srcinfos = srcinfo.split('+');
						options[0] = {
							"display": srcinfos[1],
							"value": srcinfos[0]
						};
					}
					items.find((item) => item.attrcode == 'funtype').options = options;
					me.props.meta.setMeta(meta);
				}
				if (res.data && res.data.retcard && res.data.retcard.head) {
					// me.props.form.setAllFormValue({ ['head']: res.data.retcard.head['head'] });
					me.props.search.setSearchValByField(formId, 'deffuntype', {
						value: res.data.retcard.head.head.rows[0].values.deffuntype.value,
						display: res.data.retcard.head.head.rows[0].values.deffuntype.display
					});
					me.props.search.setSearchValByField(formId, 'pk_org', {
						value: res.data.retcard.head.head.rows[0].values.pk_org.value,
						display: res.data.retcard.head.head.rows[0].values.pk_org.display
					});
					me.pkorg = res.data.retcard.head.head.rows[0].values.pk_org.value;
					me.props.button.setDisabled({
						DownloadFAEdit: false,
						Delete: false,
						plangroup: false,
						plan: false,
						Edit: false,
						Add: false,
						accessoryBtn: false,
						Refresh: false
					});
				}
				if (res.data && res.data.retcard && res.data.retcard.body) {
					me.props.editTable.setTableData(tableId, res.data.retcard.body[tableId]);
				} else {
					me.props.editTable.setTableData(tableId, { rows: [] });
				}
				if (res.data && res.data.QueryDownloadSelectFunType) {
					let selectFunTypes = res.data.QueryDownloadSelectFunType.split('+');
					me.funtype = selectFunTypes[0];
					me.props.search.setSearchValByField(formId, 'funtype', {
						value: selectFunTypes[0],
						display: selectFunTypes[1]
					});
				}
			},
			error: (res) => {
				console.log(res.message);
			}
		});
	}

	loadEbanktype(isRefresh) { //加载银行账户下载设置的数据
		var me = this,
			param = {
				pkorg: me.props.search.getSearchValByField(formId, 'pk_org').value.firstvalue,
				funtype: me.funtype,
				banktype: me.curEbanktype ? me.curEbanktype.id : '',
				markGroup: '0'
			};
		ajax({
			data: param,
			url: '/nccloud/obm/ebankdownload/EbankdownloadFuntypeAction.do',
			success: function (res) {
				if (res.data && res.data.QueryDownloadFunType) {
					let meta = me.props.meta.getMeta();
					let items = meta[formId].items;
					let options = [];
					let index = res.data.QueryDownloadFunType.indexOf('*');
					let srcinfo = res.data.QueryDownloadFunType;
					if (index > 0) {
						let srcinfos = srcinfo.split('*');
						for (let i = 0; i < srcinfos.length; i++) {
							options[i] = {
								"display": srcinfos[i].split('+')[1],
								"value": srcinfos[i].split('+')[0]
							};
						}
					} else {
						let srcinfos = new Array(2);
						srcinfos = srcinfo.split('+');
						options[0] = {
							"display": srcinfos[1],
							"value": srcinfos[0]
						};
					}
					items.find((item) => item.attrcode == 'funtype').options = options;
					me.props.meta.setMeta(meta);
				}
				if (res.data && res.data.retcard && res.data.retcard.head) {
					// me.props.form.setAllFormValue({ ['head']: res.data.retcard.head['head'] });
					me.props.search.setSearchValByField(formId, 'deffuntype', {
						value: res.data.retcard.head.head.rows[0].values.deffuntype.value,
						display: res.data.retcard.head.head.rows[0].values.deffuntype.display
					});
					//me.props.search.setSearchValByField(formId, 'pk_org', {
					//	value: res.data.retcard.head.head.rows[0].values.pk_org.value,
				//		display: res.data.retcard.head.head.rows[0].values.pk_org.display
				//	});
					me.pkorg = me.props.search.getSearchValByField(formId, 'pk_org').value.firstvalue;
					// me.props.search.setSearchValue(formId,res.data.retcard.head);
				}
				if (res.data && res.data.retcard && res.data.retcard.body) {
					me.props.editTable.setTableData(tableId, res.data.retcard.body[tableId]);
				} else {
					me.props.editTable.setTableData(tableId, { rows: [] });
				}
				if (res.data && res.data.QueryDownloadSelectFunType) {
					let selectFunTypes = res.data.QueryDownloadSelectFunType.split('+');
					me.funtype = selectFunTypes[0];
					me.props.search.setSearchValByField(formId, 'funtype', {
						value: selectFunTypes[0],
						display: selectFunTypes[1]
					});
					// me.props.search.setFormItemsValue(formId, { funtype: { value: selectFunTypes[0], display: selectFunTypes[1] } });
				}
				if(isRefresh && isRefresh==true){
					toast({ color: 'success', content: me.props.MutiInit.getIntl("36100DC") && me.props.MutiInit.getIntl("36100DC").get('36100DC-000021') });/* 国际化处理： 刷新成功*/
				}
			},
			error: (res) => {
				console.log(res.message);
			}
		});
	}


	render() {
		// let { form } = this.props;s
		let { syncTree, editTable, form,search, DragWidthCom, button, modal,ncmodal } = this.props;
		let { createEditTable } = editTable;
		let { createSyncTree } = syncTree;
		let buttons = this.props.button.getButtons();
		let { createForm } = form;
		let { NCCreateSearch } = search;
		let { createButtonApp } = this.props.button;
		let { createButton, getButtons } = button;
		let  createNCModal  = ncmodal.createModal;
		let { createModal } = modal;
		let { showUploader, target, billno, billId } = this.state;//附件相关内容变量
		
		return (
			<div className="nc-bill-list bankaccountbook-list">
				<div className="nc-bill-header-area">
					{/* 标题 title */}
					<div className="header-title-search-area">
						{/*页面大图标*/}
						{createPageIcon()}
						<h2 className='title-search-detail'>
						{this.props.MutiInit.getIntl("36100DC") && this.props.MutiInit.getIntl("36100DC").get('36100DC-000018')}-{this.props.MutiInit.getIntl("36100DC") && this.props.MutiInit.getIntl("36100DC").get('36100DC-000019')}</h2>{/* 国际化处理： 数据下载设置,组织*/}
						<div className='title-search-detail'>
						{NCCreateSearch(formId, {
							showAdvBtn: false,
							showSearchBtn:false,
							showClearBtn:false,
							defaultConditionsNum: 4,
							oid:"",
							onAfterEvent: afterEvent.bind(this)
						})}
				</div>
				</div>
						<div className="header-button-area">
							{
								this.props.button.createButtonApp({
									area: 'card_head',
									buttonLimit: 4,
									onButtonClick: buttonClick.bind(this),
									popContainer: document.querySelector('.header-button-area')
								})
							}

						</div>
				</div>
				
				{/*树表区域 tree-table*/}
				<div className="tree-table">
					<DragWidthCom
						// 左树区域
						leftDom={
							<div className="tree-area">
								{createSyncTree({
									searchType: 'filtration', // 搜索框查询方式，定位/过滤  location/filtration，默认定位
									treeId: 'tree',
									needEdit: false, //不启用编辑
									showLine: true, //显示连线
									needSearch: true, //是否需要查询框，默认为true,显示。false: 不显示
									defaultExpandAll: true, //默认展开所有节点
									onSelectedChange: this.onEbanktypeSelect.bind(this)
									// onSelectEve: this.onDeptSelect.bind(this)
								})}
							</div>}     //左侧区域dom
						// 右卡片区域
						rightDom={
							<div className="nc-singleTable-table-area">
								{createEditTable(tableId, {//列表区
									//onCloseModel: onCloseModelFn,                    // 弹窗控件点击关闭事件 
									onAfterEvent: tableAfterEvent.bind(this),                     // 控件的编辑后事件  
									useFixedHeader: true,
									adaptionHeight: true,
									// selectedChange: this.updateButtonStatus.bind(this),                // 选择框有变动的钩子函数
									// statusChange: this.updateButtonStatus.bind(this),				//表格状态监听
									showCheck:true,			//显示复选框
									showIndex: true,
									//params: 'test',                                  // 自定义传参
									tableModelConfirm: function () { alert(this.props.MutiInit.getIntl("36100DC") && this.props.MutiInit.getIntl("36100DC").get('36100DC-000011')); }/* 国际化处理： 提交保存数据*/

								})}
							</div>}     //右侧区域dom 
						defLeftWid='270px'      // 默认左侧区域宽度，px/百分百 
					/>
				</div>
				{createNCModal("delete", {
					title: this.props.MutiInit.getIntl("36100DC") && this.props.MutiInit.getIntl("36100DC").get('36100DC-000012'),/* 国际化处理： 删除*/
					content: this.props.MutiInit.getIntl("36100DC") && this.props.MutiInit.getIntl("36100DC").get('36100DC-000013'),/* 国际化处理： 确定要删除所有数据吗？*/
					// hasCloseBtn: true,
					// className: 'junior',
					beSureBtnClick: this.delConfirm
				})}
				{createNCModal("cancel", {
					title: this.props.MutiInit.getIntl("36100DC") && this.props.MutiInit.getIntl("36100DC").get('36100DC-000014'),/* 国际化处理： 取消*/
					content: this.props.MutiInit.getIntl("36100DC") && this.props.MutiInit.getIntl("36100DC").get('36100DC-000015'),/* 国际化处理：  确定要取消吗？*/
					beSureBtnClick: this.cancelConfirm,
					// size: 'sm',
					// className: 'junior'
				})}
				{/* 增加方案框 */}
				<NCModal show={this.state.showModal_publish} onHide={this.close} className={'senior'}>
					<NCModal.Header closeButton={true}>
						<NCModal.Title>{this.state.showModal_title}</NCModal.Title>
					</NCModal.Header>

					<NCModal.Body>
						<NCScrollElement name='forminfo'>
							<div className="nc-bill-form-area">
								{createForm(funtypeForm)}
							</div>
						</NCScrollElement>
					</NCModal.Body>

					<NCModal.Footer>
						<NCButton className="button-primary" onClick={this.sureConfirm}>
							{this.props.MutiInit.getIntl("36100DC") && this.props.MutiInit.getIntl("36100DC").get('36100DC-000020')} {/* 国际化处理： 确定*/}
                            			</NCButton>
						<NCButton onClick={this.close}>{this.props.MutiInit.getIntl("36100DC") && this.props.MutiInit.getIntl("36100DC").get('36100DC-000014')}</NCButton> {/* 国际化处理： 取消*/}
					</NCModal.Footer>

				</NCModal>

				<div className="nc-faith-demo-div2">
					{/* 这里是附件上传组件的使用，需要传入三个参数 */}
					{showUploader &&
						<NCUploader
							billId={billId}
							target={target}
							placement={'bottom'}
							billNo={billno}
							onHide={()=>{
								this.setState({
									showUploader: false
								})
							}}
						/>
					}
				</div>

			</div>
		);
	}
}

Card = createPage({
	mutiLangCode: '36100DC',
	initTemplate: initTemplate
})(Card);

ReactDOM.render(<Card />, document.querySelector('#app'));
