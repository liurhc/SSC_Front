//主子表卡片
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast,high,createPageIcon } from 'nc-lightapp-front';
const { NCUploader } = high
const { NCAffix,NCPopconfirm,NCFormControl,NCAnchor,NCScrollElement,NCScrollLink,NCModal,NCButton } = base;
import { tableId, formId, pagecode, funtypeForm } from "./constants";
import { buttonClick, initTemplate, afterEvent,tableAfterEvent } from './events';
import './index.less';
const moduleId= '36100DD';

class Card extends Component {
	constructor(props) {
		super(props);
		this.state = {
			billId: '',//单据pk
			billno: '',//附件管理使用单据编号
			showUploader: false,//控制附件弹出框
			showModal_publish: false,
			showModal_title: '',
			target: null//控制弹出位置
		}
	}
	componentDidMount() {
		// this.bankTypeRoot = {//为树创建一个根节点
		// 	isleaf: false,
		// 	pid: "",
		// 	// refname: this.props.MutiInit.getIntl("36100DD") && this.props.MutiInit.getIntl("36100DD").get('36100DD-000007'),/* 国际化处理： 全部账号*/
		// 	refname:'全部账号',
		// 	refpk: "0"
		// };
		this.curEbanktype = undefined;  //定义选中银行类别
		this.pkorg = undefined;
		this.funtype = undefined;
		this.toggleShow();
		this.loadBankType();
		this.loadEbanktypeByGroup();
	}
	
	componentWillMount(){
		//关闭浏览器
		 window.onbeforeunload = () => {
			 let status = this.props.editTable.getStatus(tableId)
			 if (status != 'browse') {
				 return this.props.MutiInit.getIntl("36100DD") && this.props.MutiInit.getIntl("36100DD").get('36100DD-000008');/* 国际化处理： 当前单据未保存，您确认离开此页面？*/
			 }
		 }
	 }

	close = () => {
		this.setState({ showModal_publish: false });
	}

	//删除
	delConfirm = () => {
		var me = this,
			param = {
				pk: me.funtype ? me.funtype.value?me.funtype.value:me.funtype:'',
				pkorg: me.pkorg ? me.pkorg : ''
			};
		ajax({
			url: '/nccloud/obm/ebankdownload/EbankdownloadDelFuntypeAction.do',
			data: param,
			success: res => {
				this.funtype = undefined;
				setTimeout(() => this.loadEbanktype(), 10);
				toast({ color: 'success', content:this.props.MutiInit.getIntl("36100DD") && this.props.MutiInit.getIntl("36100DD").get('36100DD-000009')});/* 国际化处理：  删除成功*/
			}
		});
	};
    //取消
    cancelConfirm = () => {
		this.loadEbanktype();
	    this.props.button.setButtonVisible([ 'Save','Cancel' ], false);
	    this.props.button.setButtonVisible([ 'plangroup','DownloadFAEdit','Delete','plan','Edit','Add','Refresh','accessoryBtn' ], true);
		this.props.search.setDisabledByField(formId, 'pk_org', false);
		this.props.search.setDisabledByField(formId, 'funtype', false);
		this.props.syncTree.setNodeDisable('tree', false);
		this.props.editTable.setStatus(tableId, 'browse');
    };

	//确定方案
	sureConfirm = () => {
		let cardData = this.props.createMasterChildData(pagecode, funtypeForm, tableId,'editTable');
		let allData = this.props.editTable.getAllData(tableId);
		cardData.head.funtypeForm.rows[0].values.pk_org.value = this.pkorg;
		cardData.body.ebank_download.rows = allData.rows;
		cardData.head.funtypeForm.rows[0].values.obmdef5.value = this.state.billId;
		ajax({
			url: '/nccloud/obm/ebankdownload/EbankdownloadSaveFuntypeAction.do',
			data: cardData,
			success: res => {
				if (res.data){
					this.setState({showModal_publish: false});
					this.funtype=res.data.head.head.rows[0].values.pk_ebank_download_h.value;
					// this.funtype.value=res.data.head.head.rows[0].values.pk_ebank_download_h.value;
				}
				this.loadEbanktype();
				toast({ color: 'success', content:this.props.MutiInit.getIntl("36100DD") && this.props.MutiInit.getIntl("36100DD").get('36100DD-000010')});/* 国际化处理： 操作成功*//* 国际化处理： 保存成功*/
			}
		});
	};


	loadBankType() {//加载树数据
		// ajax({
		// 	url: '/nccloud/obm/ebankdownload/ebankdownloadTreeBanktypeAction.do',
		// 	// data: {pkorg:this.state.curOrg.refpk}, //参数带上选中的行政组织
		// 	success: (res) => {
		// 		this.props.syncTree.setSyncTreeData('tree', [Object.assign({ ...this.bankTypeRoot }, { children: res.data })]); //返回值合并根节点后设置树数据
		// 		this.props.syncTree.setNodeSelected('tree', this.bankTypeRoot.refpk);
		// 	}
		// });
		ajax({
			url: '/nccloud/obm/ebankdownload/ebankdownloadTreeBanktypeAction.do',
			// data: {pkorg:this.state.curOrg.refpk}, //参数带上选中的行政组织
			success: (res) => {
				let data = [Object.assign( {
					"isleaf": false,
					"key":"~",
					"id":"~",
					"innercode":"~",
					"pid": "",
					"refname": this.props.MutiInit.getIntl("36100DD") && this.props.MutiInit.getIntl("36100DD").get('36100DD-000007'),/* 国际化处理： 全部账号*/
					"refpk": "~"
				}, { children: res.data } )];
				this.props.syncTree.setSyncTreeData('tree', data); //返回值合并根节点后设置树数据
				this.props.syncTree.setNodeSelected('tree', "~");
			}
		});
	}

	//切换页面状态
	toggleShow = () => {
		this.props.button.setButtonVisible(['Save', 'Cancel'], false);
		this.props.button.setButtonVisible(['plangroup', 'DownloadFAEdit', 'Delete', 'plan', 'Edit', 'Add', 'Refresh'], true);
		this.props.button.setDisabled({
			DownloadFAEdit: true,
			Delete: true,
			plangroup: false,
			plan: false,
			Edit: true,
			Add: true,
			accessoryBtn: true,
			Refresh:false
		});


		this.props.cardTable.setStatus(tableId, 'browse');
	};
	onEbanktypeSelect(node, item, ischange) {   //选定银行类型
		this.curEbanktype = node;        //设置当前选中银行类型, 加载银行账户下载设置
		if (this.curEbanktype && this.curEbanktype != "undefined") {
			setTimeout(() => this.loadEbanktype(), 10);
		}
	}

	loadEbanktypeByGroup() { //自动加载银行账户下载设置的数据
		var me = this,
			param = {
				pkorg: 'Group',
				funtype: '',
				banktype: '',
				markGroup:'1'
			};
		ajax({
			data: param,
			url: '/nccloud/obm/ebankdownload/EbankdownloadFuntypeAction.do',
			success: function (res) {
				
				if (res.data && res.data.QueryDownloadFunType) {
					console.log(res.data);
					let meta = me.props.meta.getMeta();
					let items = meta[formId].items;
					let options = [];
					let index = res.data.QueryDownloadFunType.indexOf('*');
					let srcinfo = res.data.QueryDownloadFunType;
					if (index > 0) {
						let srcinfos = srcinfo.split('*');
						for (let i = 0; i < srcinfos.length; i++) {
							options[i] = {
								"display": srcinfos[i].split('+')[1],
								"value": srcinfos[i].split('+')[0]
							};
						}
					} else {
						let srcinfos = new Array(2);
						srcinfos = srcinfo.split('+');
						options[0] = {
							"display": srcinfos[1],
							"value": srcinfos[0]
						};
					}
					items.find((item) => item.attrcode == 'funtype').options = options;
					me.props.meta.setMeta(meta);
				}
				if (res.data && res.data.QueryDownloadSelectFunType) {
					let selectFunTypes = res.data.QueryDownloadSelectFunType.split('+');
					me.funtype = selectFunTypes[0];
					me.props.search.setSearchValByField(formId, 'funtype', {
						value: selectFunTypes[0],
						display: selectFunTypes[1]
					});
				}
				if (res.data && res.data.retcard && res.data.retcard.head) {
					// me.props.form.setAllFormValue({ ['head']: res.data.retcard.head['head'] });
					me.props.search.setSearchValByField(formId, 'deffuntype', {
						value: res.data.retcard.head.head.rows[0].values.deffuntype.value,
						display: res.data.retcard.head.head.rows[0].values.deffuntype.display
					});
					me.props.search.setSearchValByField(formId, 'pk_group', {
						value: res.data.retcard.head.head.rows[0].values.pk_group.value,
						display: res.data.retcard.head.head.rows[0].values.pk_group.display
					});
					me.pkorg = res.data.retcard.head.head.rows[0].values.pk_group.value;
					me.props.button.setDisabled({
						DownloadFAEdit: false,
						Delete: false,
						plangroup: false,
						plan: false,
						Edit: false,
						Add: false,
						accessoryBtn: false,
						Refresh: false
					});
				}
				if (res.data && res.data.retcard && res.data.retcard.body) {
					me.props.editTable.setTableData(tableId, res.data.retcard.body[tableId]);
				}else{
					me.props.editTable.setTableData(tableId, { rows: [] });
				}
				
			},
			error: (res) => {
				console.log(res.message);
			}
		});
	}


	loadEbanktype(isRefresh) { //加载银行账户下载设置的数据
		var me = this,
			param = {
				pkorg: me.pkorg ? me.pkorg : '',
				funtype: me.funtype ? me.funtype.value?me.funtype.value:me.funtype:'',
				banktype: me.curEbanktype ? me.curEbanktype.id : '',
				markGroup:'1'
			};
		ajax({
			data: param,
			url: '/nccloud/obm/ebankdownload/EbankdownloadFuntypeAction.do',
			success: function (res) {
				if (res.data && res.data.QueryDownloadFunType) {
					let meta = me.props.meta.getMeta();
					let items = meta[formId].items;
					let options = [];
					let index = res.data.QueryDownloadFunType.indexOf('*');
					let srcinfo = res.data.QueryDownloadFunType;
					if (index > 0) {
						let srcinfos = srcinfo.split('*');
						for (let i = 0; i < srcinfos.length; i++) {
							options[i] = {
								"display": srcinfos[i].split('+')[1],
								"value": srcinfos[i].split('+')[0]
							};
						}
					} else {
						let srcinfos = new Array(2);
						srcinfos = srcinfo.split('+');
						options[0] = {
							"display": srcinfos[1],
							"value": srcinfos[0]
						};
					}
					items.find((item) => item.attrcode == 'funtype').options = options;
					me.props.meta.setMeta(meta);
				}
				if (res.data && res.data.QueryDownloadSelectFunType) {
					let selectFunTypes = res.data.QueryDownloadSelectFunType.split('+');
					me.funtype = selectFunTypes[0];
					me.props.search.setSearchValByField(formId, 'funtype', {
						value: selectFunTypes[0],
						display: selectFunTypes[1]
					});
					// me.props.search.setFormItemsValue(formId, { funtype: { value: selectFunTypes[0], display: selectFunTypes[1] } });
				}
				if (res.data && res.data.retcard && res.data.retcard.head) {
					// me.props.form.setAllFormValue({ ['head']: res.data.retcard.head['head'] });
					me.props.search.setSearchValByField(formId, 'deffuntype', {
						value: res.data.retcard.head.head.rows[0].values.deffuntype.value,
						display: res.data.retcard.head.head.rows[0].values.deffuntype.display
					});
					me.props.search.setSearchValByField(formId, 'pk_group', {
						value: res.data.retcard.head.head.rows[0].values.pk_group.value,
						display: res.data.retcard.head.head.rows[0].values.pk_group.display
					});
					me.pkorg = res.data.retcard.head.head.rows[0].values.pk_group.value;
					// me.props.search.setSearchValue(formId,res.data.retcard.head);
				}
				if (res.data && res.data.retcard && res.data.retcard.body) {
					me.props.editTable.setTableData(tableId, res.data.retcard.body[tableId]);
				}else{
					me.props.editTable.setTableData(tableId, { rows: [] });
				}
				toast({ color: 'success', content:me.props.MutiInit.getIntl("36100DD") && me.props.MutiInit.getIntl("36100DD").get('36100DD-000021')}); /* 国际化处理： 刷新成功*/
			},
			error: (res) => {
				console.log(res.message);
			}
		});
	}


	render() {
		// let { form } = this.props;s
		let { syncTree, editTable, form,search, DragWidthCom, button, modal,ncmodal } = this.props;
		let { createEditTable } = editTable;
		let { createSyncTree } = syncTree;
		let buttons = this.props.button.getButtons();
		let { createForm } = form;
		let { NCCreateSearch } = search;
		let { createButtonApp } = this.props.button;
		let { createButton, getButtons } = button;
		let  createNCModal  = ncmodal.createModal;
		let { createModal } = modal;
		let { showUploader, target, billno, billId } = this.state;//附件相关内容变量
		
		return (
			<div className="nc-bill-list bankaccountbook-list">
				<div className="nc-bill-header-area">
					{/* 标题 title */}
					<div className="header-title-search-area">
						{/*页面大图标*/}
						{createPageIcon()}
						<h2 className='title-search-detail'>
						{this.props.MutiInit.getIntl("36100DD") && this.props.MutiInit.getIntl("36100DD").get('36100DD-000019')}-{this.props.MutiInit.getIntl("36100DD") && this.props.MutiInit.getIntl("36100DD").get('36100DD-000020')}</h2>{/* 国际化处理： 数据下载设置,集团*/}
						<div className='title-search-detail'>
						{NCCreateSearch(formId, {
							showAdvBtn: false,
							showSearchBtn:false,
							showClearBtn:false,
							defaultConditionsNum: 4,
							oid:"",
							onAfterEvent: afterEvent.bind(this)
						})}
				</div>
				</div>
						<div className="header-button-area">
							{
								this.props.button.createButtonApp({
									area: 'card_head',
									buttonLimit: 4,
									onButtonClick: buttonClick.bind(this),
									popContainer: document.querySelector('.header-button-area')
								})
							}

						</div>
				</div>
				
				{/*树表区域 tree-table*/}
				<div className="tree-table">
					<DragWidthCom
						// 左树区域
						leftDom={
							<div className="tree-area">
								{createSyncTree({
									searchType: 'filtration', // 搜索框查询方式，定位/过滤  location/filtration，默认定位
									treeId: 'tree',
									needEdit: false, //不启用编辑
									showLine: true, //显示连线
									needSearch: true, //是否需要查询框，默认为true,显示。false: 不显示
									defaultExpandAll: true, //默认展开所有节点
									onSelectedChange: this.onEbanktypeSelect.bind(this)
									// onSelectEve: this.onDeptSelect.bind(this)
								})}
							</div>}     //左侧区域dom
						// 右卡片区域
						rightDom={
							<div className="nc-singleTable-table-area">
								{createEditTable(tableId, {//列表区
									//onCloseModel: onCloseModelFn,                    // 弹窗控件点击关闭事件 
									onAfterEvent: tableAfterEvent.bind(this),                     // 控件的编辑后事件  
									useFixedHeader: true,
									adaptionHeight: true,
									// selectedChange: this.updateButtonStatus.bind(this),                // 选择框有变动的钩子函数
									// statusChange: this.updateButtonStatus.bind(this),				//表格状态监听
									showCheck:true,			//显示复选框
									showIndex: true,
									//params: 'test',                                  // 自定义传参
									tableModelConfirm: function () { alert(this.props.MutiInit.getIntl("36100DD") && this.props.MutiInit.getIntl("36100DD").get('36100DD-000012'))}/* 国际化处理： 提交保存数据*/

								})}
							</div>}     //右侧区域dom
						defLeftWid = '270px'      // 默认左侧区域宽度，px/百分百
					/>
				</div>
				{createNCModal("delete", {
					title: this.props.MutiInit.getIntl("36100DD") && this.props.MutiInit.getIntl("36100DD").get('36100DD-000013'),/* 国际化处理： 删除*/
					content: this.props.MutiInit.getIntl("36100DD") && this.props.MutiInit.getIntl("36100DD").get('36100DD-000014'),/* 国际化处理： 确定要删除所有数据吗？*/
					// hasCloseBtn: true,
					// className: 'junior',
					beSureBtnClick: this.delConfirm
				})}
				{createNCModal("cancel", {
					title: this.props.MutiInit.getIntl("36100DD") && this.props.MutiInit.getIntl("36100DD").get('36100DD-000015'),/* 国际化处理： 取消*/
					content: this.props.MutiInit.getIntl("36100DD") && this.props.MutiInit.getIntl("36100DD").get('36100DD-000016'),/* 国际化处理：  确定要取消吗？*/
					beSureBtnClick: this.cancelConfirm,
					// size: 'sm',
					// className: 'junior'
				})}
				{/* 增加方案框 */}
				{createModal('modal', {
					title: this.props.MutiInit.getIntl("36100DD") && this.props.MutiInit.getIntl("36100DD").get('36100DD-000017'),		//标题/* 国际化处理： 增加方案,标题*//* 国际化处理： 增加方案*/
					content: <NCScrollElement name='forminfo'>
						<div className="nc-bill-form-area">
							{createForm(funtypeForm)}
						</div>
					</NCScrollElement>,						//内容
					// size:'lg',
					// style:{width:"1000px",height:"500px"},
					beSureBtnClick: this.sureConfirm,		//确定按钮事件回调
					//cancelBtnClick : this.closeDelModal.bind(this),			//取消按钮事件回调
					leftBtnName: this.props.MutiInit.getIntl("36100DD") && this.props.MutiInit.getIntl("36100DD").get('36100DD-000018'), /* 国际化处理： 确定*/
					rightBtnName: this.props.MutiInit.getIntl("36100DD") && this.props.MutiInit.getIntl("36100DD").get('36100DD-000015')/* 国际化处理： 取消*/
				})}

				<NCModal show={this.state.showModal_publish} onHide={this.close} className={'senior'}>
					<NCModal.Header closeButton={true}>
						<NCModal.Title>{this.state.showModal_title}</NCModal.Title>
					</NCModal.Header>

					<NCModal.Body>
					<NCScrollElement name='forminfo'>
						<div className="nc-bill-form-area">
							{createForm(funtypeForm)}
						</div>
					</NCScrollElement>
					</NCModal.Body>

					<NCModal.Footer>
						<NCButton className="button-primary" onClick={this.sureConfirm}>
                                			{this.props.MutiInit.getIntl("36100DD") && this.props.MutiInit.getIntl("36100DD").get('36100DD-000018')}{/* 国际化处理： 确定*/}
                            			</NCButton>
                            			<NCButton onClick={this.close}>{this.props.MutiInit.getIntl("36100DD") && this.props.MutiInit.getIntl("36100DD").get('36100DD-000015')}</NCButton>{/* 国际化处理： 取消*/}
					</NCModal.Footer>

				</NCModal>

				<div className="nc-faith-demo-div2">
					{/* 这里是附件上传组件的使用，需要传入三个参数 */}
					{showUploader &&
						<NCUploader
							billId={billId}
							target={target}
							placement={'bottom'}
							billNo={billno}
							onHide={()=>{
								this.setState({
									showUploader: false
								})
							}}
						/>
					}
				</div>

			</div>
		);
	}
}

Card = createPage({
	mutiLangCode: '36100DD',
	initTemplate: initTemplate
})(Card);

ReactDOM.render(<Card />, document.querySelector('#app'));
