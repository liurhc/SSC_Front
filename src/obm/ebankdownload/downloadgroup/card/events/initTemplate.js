import { base, ajax } from 'nc-lightapp-front';
// import intl from 'react-intl-universal';
import { tableId, formId,appid, pagecode,funtypeForm } from "../constants";


export default function(props) {
	props.createUIDom(
		{
			pagecode: pagecode,//页面id
			appcode: '36100DD'//注册按钮的id
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta)
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
			}
		}
	)
}

function modifierMeta(props, meta) {
	let status = props.getUrlParam('status');
	// meta[formId].status = 'edit';
	meta[tableId].status = 'browse'; 


	return meta;
}

function FuntypeByGroup(props,meta) {
		let	param = {
				pkorg: 'Group',
				funtype: '',
				banktype: '',
				markGroup:'1'
			};
		ajax({
			data: param,
			url: '/nccloud/obm/ebankdownload/EbankdownloadFuntypeAction.do',
			success: function (res) {
				if (res.data && res.data.QueryDownloadFunType) {
					// let meta = props.meta.getMeta();
					let items = meta[formId].items;
					let options = [];
					let index = res.data.QueryDownloadFunType.indexOf('*');
					let srcinfo = res.data.QueryDownloadFunType;
					if (index > 0) {
						let srcinfos = srcinfo.split('*');
						for (let i = 0; i < srcinfos.length; i++) {
							options[i] = {
								"display": srcinfos[i].split('+')[1],
								"value": srcinfos[i].split('+')[0]
							};
						}
					} else {
						let srcinfos = new Array(2);
						srcinfos = srcinfo.split('+');
						options[0] = {
							"display": srcinfos[1],
							"value": srcinfos[0]
						};
					}
					items.find((item) => item.attrcode == 'funtype').options = options;
					props.meta.setMeta(meta);
				}
				if (res.data && res.data.retcard && res.data.retcard.head) {
					props.form.setAllFormValue({ [formId]: res.data.retcard.head[formId] });
					pkorg = res.data.retcard.head.head.rows[0].values.pk_org;
				}
				if (res.data && res.data.retcard && res.data.retcard.body) {
					props.editTable.setTableData(tableId, res.data.retcard.body[tableId]);
				}else{
					props.editTable.setTableData(tableId, { rows: [] });
				}
				if (res.data && res.data.QueryDownloadSelectFunType) {
					let selectFunTypes = res.data.QueryDownloadSelectFunType.split('+');
					funtype = selectFunTypes[0];
					props.form.setFormItemsValue(formId, { funtype: { value: selectFunTypes[0], display: selectFunTypes[1] } });
				}
			},
			error: (res) => {
				console.log(res.message);
			}
		});

}
