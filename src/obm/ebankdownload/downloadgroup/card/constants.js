/**
 * 表体区域
 */
export const tableId = 'ebank_download';

/**
 * 表头区域
 */
// export const formId = 'head';
export const formId = 'search';

/**
 * 页面编码
 */
export const pagecode = '36100DD_C01';

export const appid = '0001Z61000000003AEAC';

export const funtypeForm = 'funtypeForm';
