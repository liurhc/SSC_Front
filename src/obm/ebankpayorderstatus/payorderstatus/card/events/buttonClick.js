import { ajax, base, toast, print, output, promptBox } from 'nc-lightapp-front';
import { tableId, appcode,pagecode,funcode,pagecode_list, formId, dataSource, funtypeForm } from '../constants';

export default function (props, id) {
  switch (id) {
    //刷新按钮
    case 'Refresh':
      this.getdata(props.getUrlParam('id'), 1);
      break;
    //状态下载
    case 'DownStatusAction':
      let pk = props.getUrlParam('id');
      let pks = [props.getUrlParam('id')];
      ajax({
        url: '/nccloud/obm/ebankpayorderstatus/ebankpayorderstatusdownload.do',
        data: { pks },
        success: (res) => {
          this.getdata(pk, 1);
          if (res.data && res.data.msg) {
            toast({ content: res.data.msg, color: 'success' });
          }
        }
      });
      break;
    //状态确认
    case 'ConfirmStatusAction':
      let selectData = props.cardTable.getCheckedRows(tableId);
      let rows = props.cardTable.getCheckedRows(tableId);
      if (rows.length == 0) {
        toast({ color: 'warning', content: this.props.MutiInit.getIntl("36101011") && this.props.MutiInit.getIntl("36101011").get('36101011-000012') });/* 国际化处理： 请选择需要确认的表体*/
        return;
      }
      let selectIndex = rows[0].index;
      promptBox({
        color: "warning",
        title: props.MutiInit.getIntl("36101011") && props.MutiInit.getIntl("36101011").get('36101011-000013'),//标题/* 国际化处理： 确认*/
        content: props.MutiInit.getIntl("36101011") && props.MutiInit.getIntl("36101011").get('36101011-000018'),	//内容/* 国际化处理： 状态确认前，请先与银行进一步确认具体状态！*/
        beSureBtnClick: this.onConfirm.bind(this,selectIndex),		//确定按钮事件回调
      })
      break;
    //状态同步
    case 'SyncStatusAction':
      let pk2 = props.getUrlParam('id');
      pks = [props.getUrlParam('id')];
      ajax({
        url: '/nccloud/obm/ebankpayorderstatus/ebankpayorderstatussynchrostatus.do',
        data: { pks },
        success: (res) => {
          this.getdata(pk2, 1);
          if (res.data && res.data.msg) {
            toast({ content: res.data.msg, color: 'success' });
          }
        }
      });
      break;
    case 'Print':
      printData(props.getUrlParam('id'));
      break;
    case 'output':
      outputData(props.getUrlParam('id'));
      break;
    case 'Preview':
      printData(props.getUrlParam('id'));
      break;
    case 'backBtn':
      props.pushTo('/list', {
        status: 'browse',
        appcode: appcode,
        pagecode: pagecode_list,
        id: ''
      })
      break;
    default:
      break
  }
}


/**
 * 打印、预览
 * @param {*} pk 
 */
function printData(pk) {
  let pks = [pk];

  print(
    'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
    '/nccloud/obm/ebankpayorderstatus/print.do',
    {
      funcode: funcode, //功能节点编码，即模板编码
      appcode: appcode, //模板节点标识
      // printTemplateID: '1001Z61000000000LPN4',//列表打印模板pk 
      oids: pks // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
    }
  );
}


function outputData(pk) {
  let pks = [pk];

  output({
    url: '/nccloud/obm/ebankpayorderstatus/print.do',
    data: {
      oids: pks,
      outputType: 'output'
    }
  });
}
