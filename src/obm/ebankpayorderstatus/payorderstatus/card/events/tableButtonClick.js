import { createPage, ajax, base, toast, promptBox } from 'nc-lightapp-front';
import { tableId } from '../constants'; 

// export default function tableButtonClick(props, key, text, record, index) {
export default function (props, key, text, record, index) {
	switch (key) {
		//状态确认成功
		case 'ConfirmStatusAction':
			promptBox({
				color: "warning",
				title: props.MutiInit.getIntl("36101011") && props.MutiInit.getIntl("36101011").get('36101011-000013'),//标题/* 国际化处理： 确认*/
				content: props.MutiInit.getIntl("36101011") && props.MutiInit.getIntl("36101011").get('36101011-000018'),	//内容/* 国际化处理： 状态确认前，请先与银行进一步确认具体状态！*/
				beSureBtnClick: this.onConfirm.bind(this,index),		//确定按钮事件回调
			})
			break;
		//展开
		case 'open_inner':
			props.cardTable.toggleRowView(tableId, record);
			this.setState({ openflag: false });
			break;
		//收起
		case 'close_inner':
			props.cardTable.toggleRowView(tableId, record);
			this.setState({ openflag: true });
			break;
		default:
			break;
	}
}


