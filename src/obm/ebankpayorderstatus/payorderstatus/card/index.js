//主子表卡片
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast, createPageIcon, cardCache } from 'nc-lightapp-front';
let { NCFormControl, NCAnchor, NCScrollLink, NCScrollElement, NCModal, NCAffix, NCBackBtn, NCButton } = base;
let { updateCache } = cardCache;
import { buttonClick, initTemplate, afterEvent, pageInfoClick, tableButtonClick } from './events';
import { dataSource, appcode,pagecode_list,tableId,formId,funtypeForm } from './constants';
import moment from 'moment';

import './index.less';
class Card extends Component {
	constructor(props) {
		super(props);
		this.state = {
			bill_code: "",
			showModal_publish: false,
			openflag: 'true'	// 展开
		}  
		initTemplate.call(this, props);
	}
	componentDidMount() {
		// this.toggleShow();
		let status = this.props.getUrlParam("status");
		if (status != "add") {
			let pk = this.props.getUrlParam("id");
			if (pk && pk != "undefined") {
				this.getdata(pk, 0);
			}
		} else {
			this.setDefaultValue();
		}
	}

	setDefaultValue = () => {
		this.props.form.setFormItemsValue(formId, {
			bill_status: { value: "0", display: this.props.MutiInit.getIntl("36101011") && this.props.MutiInit.getIntl("36101011").get('36101011-000007') }/* 国际化处理： 自由态*/
		});
	};

	close = () => {
		this.setState({ showModal_publish: false });
	};

	//确定
	sureConfirm = () => {
		let { form } = this.props;

		if (form.isCheckNow(funtypeForm)) {
			const selectIndex = form.getFormItemsValue(funtypeForm, 'selectindex').value;
			let confirmData = this.props.cardTable.getDataByIndex(tableId, selectIndex);
			let banktime = form.getFormItemsValue(funtypeForm, 'banktime').value;
			let bankDate = moment(banktime);
			let bankorderstatus = form.getFormItemsValue(funtypeForm, 'bankorderstatus').value;

			//组装待确认的数据
			let cardData = this.props.createMasterChildData('36101011_C01', funtypeForm, tableId, 'editTable');
			//取选中的表体数据
			//指令状态
			let orderstatus = confirmData.values.orderstatus.value;
			let ordersenddatetime = this.props.form.getFormItemsValue(formId, 'ordersenddate');
			let ordersenddate = moment(ordersenddatetime);

			//校验
			if (orderstatus !== "3") {
				toast({ color: 'warning', content: this.props.MutiInit.getIntl("36101011") && this.props.MutiInit.getIntl("36101011").get('36101011-000019') });/* 国际化处理： 状态为不明时才需要确认*/
				return;
			}
			if (ordersenddate.isAfter(bankDate)) {
				toast({ color: 'warning', content: this.props.MutiInit.getIntl("36101011") && this.props.MutiInit.getIntl("36101011").get('36101011-000020') });/* 国际化处理： 确认日期不能早于指令发送日期*/
				return;
			}
			let bodyPks = [];
			let headPk = confirmData.values.pk_ebankpaystatus_h.value;
			bodyPks[0] = confirmData.values.pk_ebankpaystatus_b.value;
			// selectData.forEach((item) => {
			// 	bodyPks.push(item.data.values.pk_ebankpaystatus_b.value);
			// });
			let submitData = {
				billId: headPk,
				bodyPks: bodyPks,
				banktime: banktime,
				bankorderstatus: bankorderstatus
			}
			let pks = [];
			pks[0] = headPk;
			ajax({
				url: '/nccloud/obm/ebankpayorderstatus/ebankpaystatusconfirm.do',
				data: submitData,
				success: res => {
					if (res.data) {
						// if (res.data.head) {
						// 	this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
						// 	/*
						// 	* idname: 数据主键的命名
						// 	* id：数据主键的值
						// 	* headAreacode: 卡片表头的区域编码
						// 	* dataSource: 缓存数据命名空间
						// 	*/
						// 	updateCache('pk_ebankpaystatus_h', pks, res.data, formId, dataSource);
						// 	// bill_code = res.data.head[formId].rows[0].values.srcbillno.value;
						// 	// this.setState({
						// 	// 	bill_code
						// 	// });
						// }
						// if (res.data.body) {
						// 	this.props.cardTable.setTableData(tableId, res.data.body[tableId]);
						// }
						this.setState({ showModal_publish: false });
					}
					this.getdata(submitData.billId);
					toast({ color: 'success', content: this.props.MutiInit.getIntl("36101011") && this.props.MutiInit.getIntl("36101011").get('36101011-000017') });/* 国际化处理： 操作成功*/
				}
			});
		}
	};

	//通过单据id查询单据信息
	getdata = (pk, mark) => {
		ajax({
			url: '/nccloud/obm/ebankpayorderstatus/querycard.do',
			data: { pk },
			success: res => {
				if (res.data && res.data.body) {
					this.props.cardTable.setTableData(tableId, res.data.body[tableId]);
				} else {
					this.props.cardTable.setTableData(tableId, { rows: [] });
					this.props.form.EmptyAllFormValue(formId);
					this.setState({ bill_code: "" });
					toast({ content: this.props.MutiInit.getIntl("36101011") && this.props.MutiInit.getIntl("36101011").get('36101011-000008'), color: 'danger' });/* 国际化处理： 数据已被删除*/
					return;
				}
				if (res.data && res.data.head) {
					this.props.form.setAllFormValue({ [formId]: res.data.head[formId] });
					let bill_code = res.data.head[formId].rows[0].values.srcbillno.value;
					this.setState({ bill_code });
				}
				this.props.button.setButtonDisabled(['CardGroup','ConfirmStatusAction'], true);
				if (mark == 1) {
					toast({ content: this.props.MutiInit.getIntl("36101011") && this.props.MutiInit.getIntl("36101011").get('36101011-000009'), color: 'success' });/* 国际化处理： 刷新成功*/
				}
			}
		});
	};

	// 返回箭头按钮
	ncBackBtnClick = () => {
		this.props.pushTo('/list', {
			status: 'browse',
			appcode: appcode,
			pagecode: pagecode_list,
			id: ''
		})
	};


	onEbanktypeSelect = (props) => {
		let selectDatas = props.cardTable.getCheckedRows(tableId);
		if(selectDatas.length > 0 && selectDatas[0].data.values.orderstatus.value=='3'){//交易不明是3  && selectDatas
			this.props.button.setButtonDisabled(['CardGroup','ConfirmStatusAction'], false);
		}else{
			this.props.button.setButtonDisabled(['CardGroup','ConfirmStatusAction'], true);
		}
	}


	//提示框确认
	onConfirm = (selectIndex) => {
		let selectData = this.props.cardTable.getDataByIndex(tableId, selectIndex);
		this.setState({
			showModal_publish: true,
			billId: selectData.values.pk_ebankpaystatus_h.value,//单据主表主键
			showModal_title: this.props.MutiInit.getIntl("36101011") && this.props.MutiInit.getIntl("36101011").get('36101011-000011')/* 国际化处理： 票据池指令状态确认*/
		});
		this.props.form.EmptyAllFormValue(funtypeForm);
		this.props.form.setFormItemsValue(funtypeForm, { 'bankorderstatus': { value: 1, display: this.props.MutiInit.getIntl("36101011") && this.props.MutiInit.getIntl("36101011").get('36101011-000015') } });/* 国际化处理： 成功*/
		const banktime = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
		this.props.form.setFormItemsValue(funtypeForm, { 'banktime': { value: banktime, display: banktime } });
		this.props.form.setFormItemsValue(funtypeForm, { 'selectindex': { value: selectIndex, display: selectIndex } });
		this.props.form.setFormStatus(funtypeForm, 'edit');
	}

	render() {
		// let { form } = this.props;s
		let { cardTable, form, button, cardPagination } = this.props;
		let buttons = this.props.button.getButtons();
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createCardPagination } = cardPagination;
		let { createButtonApp } = this.props.button;
		let { createButton, getButtons } = button;
		const { createBillHeadInfo } = this.props.BillHeadInfo;
		// let { createModal } = modal;
		return (
			<div className="nc-bill-card">
				<div className="nc-bill-top-area">
					<NCAffix>
						<div className="nc-bill-header-area">
							<div className="header-title-search-area">
								{
									createBillHeadInfo(
										{
											title: this.props.MutiInit.getIntl("36101011") && this.props.MutiInit.getIntl("36101011").get('36101011-000010'),  //标题
											billCode: this.state.bill_code,     //单据号
											backBtnClick: () => {           //返回按钮的点击事件
												this.ncBackBtnClick();
											}
										}
									)}
							</div>
							<div className="header-button-area">
								{
									this.props.button.createButtonApp({
										area: 'card_head',
										buttonLimit: 4,
										onButtonClick: buttonClick.bind(this),
										popContainer: document.querySelector('.header-button-area')
									})
								}

							</div>
							<div className="header-cardPagination-area" style={{ float: 'right' }}>
								{createCardPagination({
									handlePageInfoChange: pageInfoClick.bind(this)
								})}
							</div>
						</div>
					</NCAffix>
					<NCScrollElement name='forminfo'>
						<div className="nc-bill-form-area">
							{createForm(formId, {
								onAfterEvent: afterEvent.bind(this)
							})}
						</div>
					</NCScrollElement>
				</div>
				<NCScrollElement name='businfo'>
					<div className="nc-bill-table-area">
						{createCardTable(tableId, {
							adaptionHeight: true,
							showCheck: true,
							showIndex: true,
							onAfterEvent: afterEvent.bind(this),
							onSelectedChange: this.onEbanktypeSelect.bind(this),
							selectedChange: this.onEbanktypeSelect.bind(this),
						})}
					</div>
				</NCScrollElement>

				{/* 确认框 */}
				<NCModal show={this.state.showModal_publish} onHide={this.close} className={'senior'}>
					<NCModal.Header closeButton={true}>
						<NCModal.Title>{this.state.showModal_title}</NCModal.Title>
					</NCModal.Header>

					<NCModal.Body>
						<NCScrollElement name='forminfo'>
							<div className="nc-bill-form-area">
								{createForm(funtypeForm)}
							</div>
						</NCScrollElement>
					</NCModal.Body>

					<NCModal.Footer>
						<NCButton className="button-primary" onClick={this.sureConfirm}>
							{this.props.MutiInit.getIntl("36101011") && this.props.MutiInit.getIntl("36101011").get('36101011-000013')}
							{/* 国际化处理： 确定*/}
						</NCButton>
						<NCButton onClick={this.close}>{this.props.MutiInit.getIntl("36101011") && this.props.MutiInit.getIntl("36101011").get('36101011-000014')}</NCButton>
						{/* 国际化处理： 取消*/}
					</NCModal.Footer>

				</NCModal>
			</div>
		);
	}
}

Card = createPage({
	mutiLangCode: '36101011'
	// initTemplate: initTemplate
})(Card);

// ReactDOM.render(<Card />, document.querySelector('#app'));
export default Card;
