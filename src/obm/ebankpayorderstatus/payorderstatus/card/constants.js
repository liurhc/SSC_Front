/**
 * 表体区域
 */
export const tableId = 'table_body_C01';

/**
 * 表头区域
 */
export const formId = 'form_head_C01'; 
/**
 * 页面编码
 */
export const pagecode = '36101011_C01';
export const pagecode_list = '36101011_L01';
export const funcode = '36101011';
export const appcode = '36101011';
export const funtypeForm = 'form_confirm_C01';

/**
 * 缓存数据源
 */
export const dataSource = 'tm.obm.ebankpayorderstatus.payorderstatus';
