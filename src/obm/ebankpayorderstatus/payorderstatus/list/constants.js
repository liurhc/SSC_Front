/**
 * 表体区域
 */
export const tableId = 'table_payorderstatus_L01';
/**
 * 查询区域编码
 */
export const searchId = 'search_payorderstatus_L01'; 
export const searchparam = '36101011_L01_search';
export const searchback = '36101011_C01_back';
export const searchoid = '1001Z61000000001M98Z';
/**
 * 页面编码
 */
export const funcode = '36101011';
export const pagecode = '36101011_L01'; 
export const appcode = '36101011';
export const appid = '1001Z61000000001M989'; 
export const pagecode_card = '36101011_C01'; 
/**
 * 缓存数据源
 */
export const dataSource = 'tm.obm.ebankpayorderstatus.payorderstatus';
