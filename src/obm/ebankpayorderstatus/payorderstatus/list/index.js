import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base,createPageIcon } from 'nc-lightapp-front';
import { buttonClick, initTemplate, afterEvent, searchBtnClick, pageInfoClick, doubleClick,tableModelConfirm,setButtonUsability } from './events';
import {tableId,searchId} from './constants';  

class List extends Component {
	constructor(props) {
		super(props);
		this.props = props;
		this.state = {
			data: {}
			
		}
	}

	componentDidMount() {
		this.toggleShow();
	}

	//切换页面状态
	toggleShow = () => {
		this.props.button.setDisabled({
			DownStatusAction: true,
			ConfirmStatusAction:true,
			SyncStatusAction:true,
			Print:true,
			output:true,
			Refresh:false
		});
	}

	render() {
		let { modal } = this.props;
		let { createModal } = modal;
		let buttons = this.props.button.getButtons();
		let { table, button, search } = this.props;
		let { createSimpleTable } = table;
		let { NCCreateSearch } = search;
		let { createButtonApp } = this.props.button;
		// let { createButton, getButtons} = button;
		
		return (
			<div className="nc-bill-list">
				<div className="nc-bill-header-area">
					<div className="header-title-search-area">
					{/*页面大图标*/}
							{createPageIcon()}
						<h2 className='title-search-detail'>
							{this.props.MutiInit.getIntl("36101011") && this.props.MutiInit.getIntl("36101011").get('36101011-000001')}</h2></div>{/* 国际化处理： 支付指令状态*/}
					<div className="header-button-area">
						{
							this.props.button.createButtonApp({
								area:'list_head',
								buttonLimit:4,
								onButtonClick: buttonClick.bind(this),
								popContainer:document.querySelector('.header-button-area')
							})
						}

					</div>
				</div>
				<div className="nc-bill-search-area">
					{NCCreateSearch(searchId, {
						showAdvBtn: true,
						clickSearchBtn: searchBtnClick.bind(this),
						defaultConditionsNum: 10
					})}
				</div>
				<div className="nc-bill-table-area" style={{ borderRadius: "3px 3px 0 0", overflow: "hidden" }}>
					{createSimpleTable(tableId, {//列表区
						handlePageInfoChange: pageInfoClick,
						adaptionHeight: true,
						showCheck: true,//复选框
						showIndex: true,//显示序号
						onSelected: setButtonUsability.bind(this, this.props),
						onSelectedAll: setButtonUsability.bind(this, this.props),
						onRowDoubleClick: doubleClick.bind(this)
					})}
				</div>
			</div>
		);
	}
}

List = createPage({
	mutiLangCode: '36101011',
	initTemplate: initTemplate
})(List);

// ReactDOM.render(<List />, document.querySelector('#app')); 
export default List;