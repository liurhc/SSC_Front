import { ajax } from 'nc-lightapp-front';
import {tableId,searchId,searchparam,searchback,pagecode,pagecode_card,appcode,funcode} from '../constants';  

export default function (props, config, pks) {
	let data = {
		allpks: pks,
		pageid: pagecode
	};
	//得到数据渲染到页面
	let that = this;
	ajax({
		url: '/nccloud/obm/ebankpayorderstatus/querypage.do',
		data: data,
		success: function (res) {
			let { success, data } = res;
			if (success) {
				if (data) {
					props.table.setAllTableData(tableId, data[tableId]);
				} else {
					props.table.setAllTableData(tableId, { rows: [] });
				}
			}
		}
	});
}
