import { ajax, base, cacheTools } from 'nc-lightapp-front';
let { NCPopconfirm, NCIcon, NCTooltip:Tooltip } = base;
import searchBtnClick from './searchBtnClick';
import tableButtonClick from './tableButtonClick';
import {tableId,searchId,searchparam,searchback,pagecode,pagecode_card,appcode,funcode} from '../constants'; 
export default function (props) {
	//请求模板数据
	props.createUIDom(
		{
			pagecode: pagecode,//页面id
			appcode: appcode//注册按钮的id
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta)
					props.meta.setMeta(meta, () => {

						//如果有缓存条件，查询
						let querycondition = cacheTools.get(searchparam);
						console.log(querycondition);
						if (querycondition) {
							cacheTools.set(searchback, '1');
							props.search.setSearchValue(searchId, querycondition);
							searchBtnClick(props, querycondition);
						}
					});
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
			}
		}
	)
}

function modifierMeta(props, meta) {

	meta[tableId].pagination = true;
	meta[searchId].items = meta[searchId].items.map((item, key) => {
		// item.visible = true;
		item.col = '2';
		return item;
	})

	// 查询条件多选
	meta[searchId].items.find((e) => e.attrcode === 'pk_org').isMultiSelectedEnabled = true;
	//财务组织用户过滤
	meta[searchId].items.map((item)=>{
		if (item.attrcode == 'pk_org' ) {
			       item.queryCondition = () => {
			       		return {
			            	funcode: funcode,
			            	TreeRefActionExt: 'nccloud.web.tmpub.filter.FinanceOrgPermissionFilter'
			        	};
			        };
			     }
	});


	meta[tableId].items = meta[tableId].items.map((item, key) => {
		// item.width = 180;
		if (item.attrcode == 'srcbillno') {
			item.render = (text, record, index) => {
				let tip = (<div>{record.srcbillno.value}</div>)
				return (
					<a
						style={{ textDecoration: '', cursor: 'pointer' }}
						onClick={() => {
							props.pushTo('/card', {
								status: 'browse',
								pagecode: pagecode_card,
								appcode:appcode,
								id: record.pk_ebankpaystatus_h.value
							});
						}}
					>
						{record && record.srcbillno && record.srcbillno.value}
					</a>
				);
			};
		}
		return item;
	});
	//添加操作列
	meta[tableId].items.push({
		attrcode: 'opr',
		label: props.MutiInit.getIntl("36101011") && props.MutiInit.getIntl("36101011").get('36101011-000002'),/* 国际化处理： 操作*/
		width: 200,
		className: 'table-opr',
		itemtype: 'customer',
		// 锁定
		fixed:'right',
		visible: true,
		render: (text, record, index) => {
			let buttonAry = ['SyncStatusAction', 'DownStatusAction'];
			return props.button.createOprationButton(buttonAry, {
				area: 'list_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => {
					tableButtonClick.call(this, props, key, text, record, index);
				}
			});
		}
	});

	return meta;
}
