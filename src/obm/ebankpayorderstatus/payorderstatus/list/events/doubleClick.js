import { ajax, toast ,cacheTools} from 'nc-lightapp-front';
import {searchId,searchparam,pagecode_card,appcode} from '../constants';

export default function doubleClick(record, index, e) {
    let searchVal = this.props.search.getAllSearchData(searchId);
    cacheTools.set(searchparam, searchVal);
    this.props.pushTo('/card', {
        status: 'browse',
        pagecode: pagecode_card,
        appcode:  appcode,
        id: record.pk_ebankpaystatus_h.value
    });
}
