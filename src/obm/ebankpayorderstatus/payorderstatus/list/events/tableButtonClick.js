﻿import { ajax, base, print,toast } from 'nc-lightapp-front'; 

export default function tableButtonClick(props, key, text, record, index) { 
	switch (key) {
		//状态下载
		case 'DownStatusAction':
			let pks = [record.pk_ebankpaystatus_h.value];
		
			ajax({
				url: '/nccloud/obm/ebankpayorderstatus/ebankpayorderstatusdownload.do',
				data: { pks },
				success: (res) => {
					if (res.data && res.data.msg) {
						toast({ content: res.data.msg, color: 'success' });
					}
				}
			});
			break;
		//状态同步
		case 'SyncStatusAction':
		pks = [record.pk_ebankpaystatus_h.value];
			
		ajax({
		  url: '/nccloud/obm/ebankpayorderstatus/ebankpayorderstatussynchrostatus.do',
		  data: {pks},
		  success: (res) => {
			if (res.data && res.data.msg) {
			  toast({ content: res.data.msg , color: 'success' });
			}
		  }
		});
		  break;
		default:
			break;
	}
}


