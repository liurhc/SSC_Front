import { ajax, base, print,toast,output,cacheTools } from 'nc-lightapp-front';
import searchBtnClick from './searchBtnClick';
import {tableId,searchId,searchparam,searchback,appcode,funcode,pagecode} from '../constants';   

export default function buttonClick(props, id) {
	console.log(id)
	switch (id) {
		case 'Refresh':
			refreshAction(props);
			break;
		//状态下载
		case 'DownStatusAction':
			let printData = props.table.getCheckedRows(tableId);
			//数据校验
			if (printData.length == 0) {
				toast({ color: 'warning', content:props.MutiInit.getIntl("36101011") && props.MutiInit.getIntl("36101011").get('36101011-000003')});/* 国际化处理： 请选择数据*/
				return
			}
			let pks = [];
			printData.forEach((item) => {
				pks.push(item.data.values.pk_ebankpaystatus_h.value);
			});
			ajax({
				url: '/nccloud/obm/ebankpayorderstatus/ebankpayorderstatusdownload.do',
				data: { pks },
				success: (res) => {
					if (res.data && res.data.msg) {
						toast({ content: res.data.msg, color: 'success' });
					}
				}
			});
			break;
		//状态同步
		case 'SyncStatusAction':
		   printData = props.table.getCheckedRows(tableId);
			//数据校验
			if (printData.length == 0) {
				toast({ color: 'warning', content: props.MutiInit.getIntl("36101011") && props.MutiInit.getIntl("36101011").get('36101011-000003')});/* 国际化处理： 请选择数据*/
				return
			}
			pks = [];
			printData.forEach((item) => {
				pks.push(item.data.values.pk_ebankpaystatus_h.value);
			});
		ajax({
		  url: '/nccloud/obm/ebankpayorderstatus/ebankpayorderstatussynchrostatus.do',
		  data: {pks},
		  success: (res) => {
			if (res.data && res.data.msg) {
			  toast({ color: 'success', content: res.data.msg});
			}
		  }
		});
		  break;
		case 'output':
			printData = props.table.getCheckedRows(tableId);
			//数据校验
			if (printData.length == 0) {
				toast({ color: 'warning', content: props.MutiInit.getIntl("36101011") && props.MutiInit.getIntl("36101011").get('36101011-000003')});/* 国际化处理： 请选择数据*/
				return
			}
			pks = [];
			printData.forEach((item) => {
				pks.push(item.data.values.pk_ebankpaystatus_h.value);
			});
			output({
				url: '/nccloud/obm/ebankpayorderstatus/print.do',
				data: { 
					 oids: pks,
					 outputType: 'output'
				}
			});
			break;
		case 'Print':
			printData = props.table.getCheckedRows(tableId);
			//数据校验
			if (printData.length == 0) {
				toast({ color: 'warning', content: props.MutiInit.getIntl("36101011") && props.MutiInit.getIntl("36101011").get('36101011-000003') });/* 国际化处理：  请选择数据*/
				return
			}
			pks = [];
			printData.forEach((item) => {
				pks.push(item.data.values.pk_ebankpaystatus_h.value);
			});
			// print(
			// 	'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
			// 	'/nccloud/obm/ebankpayorderstatus/print.do',
			// 	{
			// 		funcode: funcode, //功能节点编码，即模板编码
			// 		appcode: appcode, //模板节点标识
			// 		oids: pks // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
			// 	}
			// );
			print(
				'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
				'/nccloud/obm/ebankpayorderstatus/print.do',
				{
					nodekey: 'NCCLOUD', //模板节点标识
                	oids: pks // 功能节点的数据主键
				},
				false
			);
			break;
		case 'Preview':
			printData(props);
			break;
		default:
			break
	}
}

/**
 * 打印、预览
 * @param {*} props 
 */
function printData(props) {
	let printData = props.table.getCheckedRows(tableId);
	//数据校验
	if (printData.length == 0) {
		toast({ color: 'warning', content: props.MutiInit.getIntl("36101011") && props.MutiInit.getIntl("36101011").get('36101011-000003') });/* 国际化处理：  请选择数据*/
		return
	}
	let pks = [];
	printData.forEach((item) => {
		pks.push(item.data.values.pk_ebankpaystatus_h.value);
	});
	print(
		'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
		'/nccloud/obm/ebankpayorderstatus/print.do',
		{
			funcode: funcode, //功能节点编码，即模板编码
			appcode: appcode, //模板节点标识
			// printTemplateID: '1001Z61000000000LPN4',//列表打印模板pk 
			oids: pks // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
		}
	);
}


/**
 * 刷新
 * @param {*} props 
 */
const refreshAction = (props) => { 
	let pageInfo = props.table.getTablePageInfo(tableId);
	let metaData = props.meta.getMeta();
	//如果有缓存条件，查询
	let querycondition = cacheTools.get(searchparam);
	console.log(querycondition);
	if (!querycondition) {
		return;
	}
	cacheTools.set(searchback, '1');
	props.search.setSearchValue(searchId, querycondition);
	let searchVal = props.search.getAllSearchData(searchId);
	let queryInfo = props.search.getQueryInfo(searchId, false);
	let oid = queryInfo.oid;					
	let data = {
		conditions: searchVal.conditions || searchVal,
		pageInfo: pageInfo,
		pagecode: pagecode,
		queryAreaCode: searchId,
		oid: oid,
		queryType: "simple"
	};
	console.log(data);
	ajax({
		url: '/nccloud/obm/ebankpayorderstatus/query.do',//'/nccloud/reva/search/query.do',
		data: data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data && data[tableId]) {
					props.table.setAllTableData(tableId, res.data[tableId]);
					toast({ color: 'success', content: props.MutiInit.getIntl("36101011") && props.MutiInit.getIntl("36101011").get('36101011-000009') });/* 国际化处理： 刷新成功*/
				} else {
					props.table.setAllTableData(tableId, { rows: [] });
					toast({ color: 'warning', content: props.MutiInit.getIntl("36101011") && props.MutiInit.getIntl("36101011").get('36101011-000004') });/* 国际化处理： 未查询到符合条件的数据*/
				}
			}
		},
		error: (res) => {
			console.log(res.message);
		}
	});
}
