import {tableId,searchId,searchparam,searchback,pagecode,pagecode_card,appcode,funcode} from '../constants';   
export default function clickBtn(props) {
	let selectdata = props.table.getCheckedRows(tableId);

	if (!selectdata || selectdata.length == 0) {
		//没有选中
		props.button.setDisabled({
			DownStatusAction: true,
			SyncStatusAction:true,
			Print:true,
			output:true,
			Refresh:false
		});
	} else {//选中
		props.button.setDisabled({
			DownStatusAction: false,
			SyncStatusAction:false,
			Print:false,
			output:false,
			Refresh:false
		});

	}
}
