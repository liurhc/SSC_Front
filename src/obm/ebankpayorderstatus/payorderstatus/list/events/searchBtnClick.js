import {ajax,toast,cacheTools} from 'nc-lightapp-front';
import {tableId,searchId,searchparam,searchback,pagecode,pagecode_card,appcode,funcode} from '../constants';   
//点击查询，获取查询区数据
export default function clickSearchBtn(props,searchVal) {
    let backmark = cacheTools.get(searchback);
    console.log(searchVal);
    //缓存查询条件
    cacheTools.set(searchback, '0');
	cacheTools.set(searchparam, searchVal);
    let pageInfo = props.table.getTablePageInfo(tableId);
    let metaData = props.meta.getMeta();
    let queryInfo = props.search.getQueryInfo(searchId, false);
    let oid = queryInfo.oid;
    let data={
        conditions:searchVal.conditions || searchVal,
        pageInfo:pageInfo,
        pagecode: pagecode,
        queryAreaCode:searchId,
        oid:oid,
        queryType:"simple"
     };

    console.log(data);
    ajax({
        url: '/nccloud/obm/ebankpayorderstatus/query.do',
        data: data,
        success:  (res)=> {
            let { success, data } = res;
            if(success){
                props.button.setDisabled({
                    DownStatusAction: true,
                    SyncStatusAction:true,
                    Print:true,
                    output:true,
                    Refresh:false
                });
                if(data && data[tableId]){
                    props.table.setAllTableData(tableId, res.data[tableId]);
                    if(!backmark || backmark==="0"){
                       let begString =props.MutiInit.getIntl("36101011") && props.MutiInit.getIntl("36101011").get('36101011-000005');
                        let endString = props.MutiInit.getIntl("36101011") && props.MutiInit.getIntl("36101011").get('36101011-000006');
                        let length = res.data[tableId].allpks.length;
                        toast({content: begString + length + endString,color:"success"});/* 国际化处理： 查询成功，共,条*/
                    }
                }else{
                    props.table.setAllTableData(tableId, {rows:[]});
                    if(!backmark || backmark==="0"){
                        toast({content:props.MutiInit.getIntl("36101011") && props.MutiInit.getIntl("36101011").get('36101011-000004'),color:"warning"});/* 国际化处理： 无数据*/
                    }
                }	
            }
        },
        error : (res)=>{
			console.log(res.message);
		}
    });

};
