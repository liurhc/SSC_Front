const tableid = 'ebank_paymentstatus_h';

export default function clickBtn(props) {
	let selectdata = props.table.getCheckedRows(tableid);

	if (!selectdata || selectdata.length == 0) {
		//没有选中
		props.button.setDisabled({
			DownStatusAction: true,
			SyncStatusAction:true,
			Print:true,
			output:true,
			Refresh:false
		});
	} else {//选中
		props.button.setDisabled({
			DownStatusAction: false,
			SyncStatusAction:false,
			Print:false,
			output:false,
			Refresh:false
		});

	}
}
