import { ajax, toast ,cacheTools} from 'nc-lightapp-front';

export default function doubleClick(record, index, e) {
    let searchVal = this.props.search.getAllSearchData("361010ZT");
    cacheTools.set("searchParams", searchVal);
    this.props.pushTo('/card', {
        status: 'browse',
        pagecode: '361010ZT_C01',
        appcode:'361010ZT',
        id: record.pk_ebankpaymentstatus_h.value
    });
}
