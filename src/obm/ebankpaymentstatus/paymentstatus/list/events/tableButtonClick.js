﻿import { ajax, base, print,toast } from 'nc-lightapp-front';
const tableid = 'ebank_paymentstatus_h';
const sreachId = '361010ZT';

export default function tableButtonClick(props, key, text, record, index) {
	// console.log(id)
	switch (key) {
		//状态下载
		case 'DownStatusAction':
			let pks = [record.pk_ebankpaymentstatus_h.value];
		
			ajax({
				url: '/nccloud/obm/ebankpaymentstatus/ebankpaymentstatusdownload.do',
				data: { pks },
				success: (res) => {
					if (res.data && res.data.msg) {
						toast({ content: res.data.msg, color: 'success' });
					}
				}
			});
			break;
		//状态同步
		case 'SyncStatusAction':
		pks = [record.pk_ebankpaymentstatus_h.value];

			
		ajax({
		  url: '/nccloud/obm/ebankpaymentstatus/ebankpaystatusfireeventsendState.do',
		  data: {pks},
		  success: (res) => {
			if (res.data && res.data.msg) {
			  toast({ content: res.data.msg , color: 'success' });
			}
		  }
		});
		  break;
		default:
			break;
	}
}


