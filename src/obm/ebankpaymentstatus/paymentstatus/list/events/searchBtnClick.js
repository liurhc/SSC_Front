import {ajax,toast,cacheTools,cardCache} from 'nc-lightapp-front';
import {dataSource} from '../../card/constants';
let{setDefData,getDefDate,getDefData,deleteCacheById}=cardCache;
let tableid = 'ebank_paymentstatus_h';
//点击查询，获取查询区数据
export default function clickSearchBtn(props,searchVal) {
    let backmark = cacheTools.get('361010ZT_C01_back');
    console.log(searchVal);
    //缓存查询条件
    cacheTools.set('361010ZT_C01_back', '0');
    cacheTools.set('361010ZT_L01_search', searchVal);
    let pageInfo = props.table.getTablePageInfo(tableid);
    let metaData = props.meta.getMeta();
    let queryInfo = props.search.getQueryInfo('361010ZT', false);
    let oid = queryInfo.oid;
    let data={
        conditions:searchVal.conditions || searchVal,
        pageInfo:pageInfo,
        pagecode: "361010ZT_L01",
        queryAreaCode:"361010ZT",
        oid:oid,
        queryType:"simple"
     };

    console.log(data);
    ajax({
        url: '/nccloud/obm/ebankpaymentstatus/query.do',
        data: data,
        success:  (res)=> {
            let { success, data } = res;
            if(success){
                props.button.setDisabled({
                    DownStatusAction: true,
                    SyncStatusAction:true,
                    Print:true,
                    output:true,
                    Refresh:false
                });
                if(data && data[tableid]){
                    props.table.setAllTableData(tableid, res.data[tableid]);
                    if(!backmark || backmark==="0"){
                       let begString =props.MutiInit.getIntl("361010ZT") && props.MutiInit.getIntl("361010ZT").get('361010ZT-000005');
                        let endString = props.MutiInit.getIntl("361010ZT") && props.MutiInit.getIntl("361010ZT").get('361010ZT-000006');
                        let length = res.data[tableid].allpks.length;
                        toast({content: begString + length + endString,color:"success"});/* 国际化处理： 查询成功，共,条*/
                    }
                    setDefData(tableid,dataSource,data);
                }else{
                    props.table.setAllTableData(tableid, {rows:[]});
                    if(!backmark || backmark==="0"){
                        toast({content:props.MutiInit.getIntl("361010ZT") && props.MutiInit.getIntl("361010ZT").get('361010ZT-000004'),color:"warning"});/* 国际化处理： 无数据*/
                    }
                }	
            }
        },
        error : (res)=>{
			console.log(res.message);
		}
    });

};
