import { ajax, base, cacheTools } from 'nc-lightapp-front';
let { NCPopconfirm, NCIcon, NCTooltip:Tooltip } = base;
import searchBtnClick from './searchBtnClick';
import tableButtonClick from './tableButtonClick';

let tableid = 'ebank_paymentstatus_h';
let pageId = '361010ZT_L01';
let searchId = '361010ZT';
export default function (props) {
	//请求模板数据
	props.createUIDom(
		{
			pagecode: pageId,//页面id
			appcode: '361010ZT'//注册按钮的id
		},
		function (data) {
			if (data) {
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta(props, meta)
					props.meta.setMeta(meta, () => {

						//如果有缓存条件，查询
						let querycondition = cacheTools.get('361010ZT_L01_search');
						console.log(querycondition);
						if (querycondition) {
							cacheTools.set('361010ZT_C01_back', '1');
							props.search.setSearchValue('361010ZT', querycondition);
							searchBtnClick(props, querycondition);
						}
					});
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
			}
		}
	)
}

function modifierMeta(props, meta) {
	meta[searchId].items = meta[searchId].items.map((item, key) => {
		// item.visible = true;
		item.col = '2';
		return item;
	})

	// 查询条件多选
	meta[searchId].items.find((e) => e.attrcode === 'pk_org').isMultiSelectedEnabled = true;
	//财务组织用户过滤
	meta[searchId].items.map((item)=>{
		if (item.attrcode == 'pk_org' ) {
			       item.queryCondition = () => {
			       		return {
			            	funcode: '361010ZT',
			            	TreeRefActionExt: 'nccloud.web.tmpub.filter.FinanceOrgPermissionFilter'
			        	};
			        };
			     }
	});


	meta[tableid].items = meta[tableid].items.map((item, key) => {
		// item.width = 180;
		if (item.attrcode == 'srcbillno') {
			item.render = (text, record, index) => {
				let tip = (<div>{record.srcbillno.value}</div>)
				return (
					<a
						style={{ textDecoration: '', cursor: 'pointer' }}
						onClick={() => {
							props.pushTo('/card', {
								status: 'browse',
								pagecode: '361010ZT_C01',
								appcode:'361010ZT',
								id: record.pk_ebankpaymentstatus_h.value
							});
						}}
					>
						{record && record.srcbillno && record.srcbillno.value}
					</a>
				);
			};
		}
		return item;
	});
	//添加操作列
	meta[tableid].items.push({
		attrcode: 'opr',
		label: props.MutiInit.getIntl("361010ZT") && props.MutiInit.getIntl("361010ZT").get('361010ZT-000002'),/* 国际化处理： 操作*/
		width: 200,
		className: 'table-opr',
		itemtype: 'customer',
		// 锁定
		fixed:'right',
		visible: true,
		render: (text, record, index) => {
			let buttonAry = ['SyncStatusAction', 'DownStatusAction'];
			return props.button.createOprationButton(buttonAry, {
				area: 'list_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => {
					tableButtonClick.call(this, props, key, text, record, index);
				}
			});
		}
	});

	return meta;
}
