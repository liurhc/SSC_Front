/**
 * 表体区域
 */
export const tableId = 'body';

/**
 * 表头区域
 */
export const formId = 'head';

/**
 * 页面编码
 */
export const pageId = '361010ZT_C01';

export const appcode = '361010ZT';
export const funtypeForm = 'Confirmform';

export const dataSource = 'tm.obm.ebankpaymentstatus.ebankpaymentstatusdataSource'; 
