//主子表卡片
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast,createPageIcon,cardCache } from 'nc-lightapp-front';
let { NCFormControl, NCAnchor, NCScrollLink, NCScrollElement, NCModal,NCAffix,NCBackBtn,NCButton } = base;
import { buttonClick, initTemplate, afterEvent,pageInfoClick,tableButtonClick} from './events';
import { dataSource } from './constants';
import './index.less';
let {getCacheById,updateCache,addCache,deleteCacheById}=cardCache; 
class Card extends Component {
	constructor(props) {
		super(props);
		this.state = {
			bill_code: "",
			showModal_publish: false,
			openflag: 'true'	// 展开
		}
		this.formId = 'head';
		this.tableId = 'body';
		this.funtypeForm = 'Confirmform';

		initTemplate.call(this, props);

	}
	componentDidMount() {
		let status = this.props.getUrlParam("status");
		if (status != "add") {
			let pk = this.props.getUrlParam("id");
			if (pk && pk != "undefined") {
				this.getdata(pk, 0);
			}
		} else {
			this.setDefaultValue();
		}
	}

	setDefaultValue = () => {
		this.props.form.setFormItemsValue(formId, {
			bill_status: { value: "0", display: this.props.MutiInit.getIntl("361010ZT") && this.props.MutiInit.getIntl("361010ZT").get('361010ZT-000007') }/* 国际化处理： 自由态*/
		});
	};

	close = () => {
		this.setState({ showModal_publish: false });
	};

	//确定
	sureConfirm = () => {
		let cardData = this.props.createMasterChildData('361010ZT_C01', this.funtypeForm, this.tableId,'editTable');
		let allData = this.props.editTable.getAllData(this.tableId);
		cardData.body.rows = allData.rows;
		let  confirmstatus = cardData.head.Confirmform.rows[0].values.confirmstatus.value;
		let  confirmreason = cardData.head.Confirmform.rows[0].values.confirmreason.value;
		if(confirmreason==null){
			toast({ color: 'warning', content: this.props.MutiInit.getIntl("361010ZT") && this.props.MutiInit.getIntl("361010ZT").get('361010ZT-000020')});/* 国际化处理： 确认原因，不能不能为空！*/
			return
		}
		if(confirmreason.length>50){
			toast({ color: 'warning', content: this.props.MutiInit.getIntl("361010ZT") && this.props.MutiInit.getIntl("361010ZT").get('361010ZT-000018')});/* 国际化处理： 确认原因，不能超过50个字符！*/
			return
		}
		let pk= this.state.billId+'&'+confirmstatus+'&'+confirmreason;
		ajax({
			url: '/nccloud/obm/ebankpaymentstatus/ebankpaystatusconfirm.do',
			data: { pk },
			success: res => {
				if (res.data){
					this.setState({showModal_publish: false});
				}
				this.getdata(this.props.getUrlParam("id"), 0);
				toast({ color: 'success', content:this.props.MutiInit.getIntl("361010ZT") && this.props.MutiInit.getIntl("361010ZT").get('361010ZT-000017')});/* 国际化处理： 操作成功*/
			}
		});		
	};

	//通过单据id查询单据信息
	getdata = (pk, mark) => {
		ajax({
			url: '/nccloud/obm/ebankpaymentstatus/querycard.do',
			data: { pk },
			success: res => {
				if (res.data && res.data.body) {
					this.props.cardTable.setTableData(this.tableId, res.data.body[this.tableId]);
				}else{
					this.props.cardTable.setTableData(this.tableId, { rows: [] });
					this.props.form.EmptyAllFormValue(this.formId);
					this.setState({ bill_code:"" });
					toast({ content: this.props.MutiInit.getIntl("361010ZT") && this.props.MutiInit.getIntl("361010ZT").get('361010ZT-000008'), color: 'danger' });/* 国际化处理： 数据已被删除*/
					return;
				}
				if (res.data && res.data.head) {
					this.props.form.setAllFormValue({ [this.formId]: res.data.head[this.formId] });
					let bill_code = res.data.head[this.formId].rows[0].values.srcbillno.value;
					this.setState({ bill_code });
				}
											
				this.props.button.setButtonDisabled(['CardGroup','ConfirmYesAction','ConfirmNoAction'], true);
				if (mark == 1) {
					toast({ content: this.props.MutiInit.getIntl("361010ZT") && this.props.MutiInit.getIntl("361010ZT").get('361010ZT-000009'), color: 'success' });/* 国际化处理： 刷新成功*/
				}
			}
		});
	};

	onEbanktypeSelect = (props) => {
		let selectDatas = props.cardTable.getCheckedRows(this.tableId);
		if(selectDatas.length > 0 && selectDatas[0].data.values.orderstatus.value=='3'){//交易不明是3  && selectDatas
			this.props.button.setButtonDisabled(['CardGroup','ConfirmYesAction','ConfirmNoAction'], false);
		}else{
			this.props.button.setButtonDisabled(['CardGroup','ConfirmYesAction','ConfirmNoAction'], true);
		}
	}

	updateTabData=(pk_ebankpaymentstatus_h)=>{
		//页面切换 
		if(!pk_ebankpaymentstatus_h || pk_ebankpaymentstatus_h==""){
			let id = this.props.getUrlParam("id");
			pk_ebankpaymentstatus_h = id;
		} 
		let cardData = getCacheById(pk_ebankpaymentstatus_h, dataSource); 
		if(cardData){
			this.props.cardTable.setTableData('body', cardData.body['body']);
		}  
	}

	// 返回箭头按钮
	ncBackBtnClick = () => {
		this.props.pushTo('/list', {
      status: 'browse',
      appcode: '361010ZT',
      pagecode: '361010ZT_L01',
      id: ''
    })
	};


	render() {
		// let { form } = this.props;s
		let { cardTable, form, button,cardPagination } = this.props;
		let buttons = this.props.button.getButtons();
		let { createForm } = form;
		let { createCardTable } = cardTable;
		let { createCardPagination } = cardPagination;
		let { createButtonApp } = this.props.button;
		let { createButton, getButtons } = button;
		const { createBillHeadInfo } = this.props.BillHeadInfo;
		// let { createModal } = modal;
		return (
			<div className="nc-bill-card">
				<div className="nc-bill-top-area">
				<NCAffix>
					<div className="nc-bill-header-area">
						<div className="header-title-search-area">
							{
								createBillHeadInfo(
									{
										title: this.props.MutiInit.getIntl("361010ZT") && this.props.MutiInit.getIntl("361010ZT").get('361010ZT-000010'),  //标题
										billCode: this.state.bill_code,     //单据号
										backBtnClick: () => {           //返回按钮的点击事件
											this.ncBackBtnClick();
										}
									}
								)}
						</div>
						<div className="header-button-area">
							{
								this.props.button.createButtonApp({
									area: 'card_head',
									buttonLimit: 4,
									onButtonClick: buttonClick.bind(this),
									popContainer: document.querySelector('.header-button-area')
								})
							}

						</div>
						<div className="header-cardPagination-area" style={{ float: 'right' }}>
							{createCardPagination({
								handlePageInfoChange: pageInfoClick.bind(this),
								dataSource: dataSource
							})}
						</div>
					</div>
				</NCAffix>
				<NCScrollElement name='forminfo'>
					<div className="nc-bill-form-area">
						{createForm(this.formId, {
							onAfterEvent: afterEvent.bind(this)
						})}
					</div>
				</NCScrollElement>
				</div>
				<NCScrollElement name='businfo'>
					<div className="nc-bill-table-area">
						{createCardTable(this.tableId, {
							showCheck: true,
							showIndex: true,
							selectedChange: this.onEbanktypeSelect.bind(this),
							onAfterEvent: afterEvent.bind(this)
						})}
					</div>
				</NCScrollElement>

				{/* 确认框 */}
				<NCModal show={this.state.showModal_publish} onHide={this.close} className={'senior'}>
					<NCModal.Header closeButton={true}>
						<NCModal.Title>{this.state.showModal_title}</NCModal.Title>
					</NCModal.Header>

					<NCModal.Body>
						<NCScrollElement name='forminfo'>
							<div className="nc-bill-form-area">
								{createForm(this.funtypeForm)}
							</div>
						</NCScrollElement>
					</NCModal.Body>

					<NCModal.Footer>
						<NCButton className="button-primary" onClick={this.sureConfirm}>
							{this.props.MutiInit.getIntl("361010ZT") && this.props.MutiInit.getIntl("361010ZT").get('361010ZT-000013')}
							{/* 国际化处理： 确定*/}
                            </NCButton>
						<NCButton onClick={this.close}>{this.props.MutiInit.getIntl("361010ZT") && this.props.MutiInit.getIntl("361010ZT").get('361010ZT-000014')}</NCButton>
						{/* 国际化处理： 取消*/}
					</NCModal.Footer>

				</NCModal>
			</div>
		);
	}
}

Card = createPage({
	mutiLangCode: '361010ZT'
	// initTemplate: initTemplate
})(Card);

// ReactDOM.render(<Card />, document.querySelector('#app'));
export default Card;
