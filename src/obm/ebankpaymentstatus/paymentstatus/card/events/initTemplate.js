import { base, ajax } from 'nc-lightapp-front';
import tableButtonClick from './tableButtonClick';

let { NCPopconfirm } = base;

let formId = 'head';
let tableId = 'body';
let pageId = '361010ZT_C01';


export default function(props) {

	let that = this;

	props.createUIDom(
		{
			pagecode: pageId,//页面id
			appcode: '361010ZT'//注册按钮的id
		},
		function (data) {
		// let data = cardTemplate.data;
			if (data) {
				if (data.template) {
					let meta = data.template;
					modifierMeta(that,props, meta)
					props.meta.setMeta(meta);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
			}
		}
	)
}


function modifierMeta(that,props,meta) {

	let porCol = {
		attrcode: 'opr',
		label: props.MutiInit.getIntl("361010ZT") && props.MutiInit.getIntl("361010ZT").get('361010ZT-000002'),/* 国际化处理： 操作*/
		fixed: 'right',
		itemtype: 'customer',
		visible: true,
		width: '250px',
		render(text, record, index) {
			let status = props.getUrlParam("status");
			let billstatus = record && record.values && record.values.orderstatus.value;
			let buttonAry = [that.state.openflag&&'open_inner',!that.state.openflag&&'close_inner'];
			if(billstatus=='3'){//交易不明是3 
				buttonAry = ['ConfirmYesAction','ConfirmNoAction', that.state.openflag&&'open_inner',!that.state.openflag&&'close_inner'];
			}
			return props.button.createOprationButton(buttonAry, {
				area: 'card_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => tableButtonClick.call(that, props, key, text, record, index)
			});
		}
	};

	meta[tableId].items.push(porCol);
	
	return meta;
}