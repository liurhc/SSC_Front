import { createPage, ajax, base, toast, promptBox } from 'nc-lightapp-front';
import { tableId, pageId, formId, dataSource,funtypeForm } from '../constants';

// export default function tableButtonClick(props, key, text, record, index) {
	export default function (props, key, text, record, index){
	switch (key) {
		//状态确认成功
		case 'ConfirmYesAction':
		let bid = record.values.pk_ebankpaymentstatus_b.value;
		promptBox({
			title: this.props.MutiInit.getIntl("361010ZT") && this.props.MutiInit.getIntl("361010ZT").get('361010ZT-000018')/* 国际化处理： 确认提醒*/,
			content: this.props.MutiInit.getIntl("361010ZT") && this.props.MutiInit.getIntl("361010ZT").get('361010ZT-000019')/* 国际化处理： 此操作不可逆，请务必与银行进行状态确认后操作!*/,
			beSureBtnClick: () => {
				this.setState({
					showModal_publish: true,
					billId: bid,//单据pk
					showModal_title:this.props.MutiInit.getIntl("361010ZT") && this.props.MutiInit.getIntl("361010ZT").get('361010ZT-000011')/* 国际化处理： 电票指令状态确认*/
				  });
				  props.form.EmptyAllFormValue(funtypeForm);
				  props.form.setFormItemsValue(funtypeForm, { 'confirmstatus':{ value: this.props.MutiInit.getIntl("361010ZT") && this.props.MutiInit.getIntl("361010ZT").get('361010ZT-000015'), display: this.props.MutiInit.getIntl("361010ZT") && this.props.MutiInit.getIntl("361010ZT").get('361010ZT-000015') }});/* 国际化处理： 确认成功*/
				  props.form.setFormStatus(funtypeForm, 'edit');
			}
		  });
		break;
		//状态确认失败
		case 'ConfirmNoAction':
		bid = record.values.pk_ebankpaymentstatus_b.value;
		promptBox({
			title: this.props.MutiInit.getIntl("361010ZT") && this.props.MutiInit.getIntl("361010ZT").get('361010ZT-000018')/* 国际化处理： 确认提醒*/,
			content: this.props.MutiInit.getIntl("361010ZT") && this.props.MutiInit.getIntl("361010ZT").get('361010ZT-000019')/* 国际化处理： 此操作不可逆，请务必与银行进行状态确认后操作!*/,
			beSureBtnClick: () => {
				this.setState({
					showModal_publish: true,
					billId: bid,//单据pk
					showModal_title:this.props.MutiInit.getIntl("361010ZT") && this.props.MutiInit.getIntl("361010ZT").get('361010ZT-000011')/* 国际化处理： 电票指令状态确认*/
				  });
				  props.form.EmptyAllFormValue(funtypeForm);
				  props.form.setFormItemsValue(funtypeForm, { 'confirmstatus':{ value: this.props.MutiInit.getIntl("361010ZT") && this.props.MutiInit.getIntl("361010ZT").get('361010ZT-000016'), display: this.props.MutiInit.getIntl("361010ZT") && this.props.MutiInit.getIntl("361010ZT").get('361010ZT-000016') }});/* 国际化处理： 确认失败*/
				  props.form.setFormStatus(funtypeForm, 'edit');
			}
		  });
		break;
		//展开
		case 'open_inner':
			props.cardTable.toggleRowView(tableId, record);
			this.setState({ openflag: false });
			break;
		//收起
		case 'close_inner':
		props.cardTable.toggleRowView(tableId, record);
		this.setState({ openflag: true });
		break;
		default:
			break;
	}
}


