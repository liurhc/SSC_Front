import buttonClick from './buttonClick';
import initTemplate from './initTemplate';
import afterEvent from './afterEvent';
import pageInfoClick from './pageInfoClick';
import tableButtonClick from './tableButtonClick';
export { buttonClick, afterEvent,initTemplate,pageInfoClick,tableButtonClick};
