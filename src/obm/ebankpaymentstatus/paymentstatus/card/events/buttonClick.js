import { ajax, base, toast, print,output,promptBox } from 'nc-lightapp-front';
import { tableId, pageId, formId, dataSource,funtypeForm } from '../constants';

export default function(props, id) {
  let record;
  switch (id) {
    //刷新按钮
    case 'Refresh':
      this.getdata(props.getUrlParam('id'),1);
      break;
    //状态下载
    case 'DownStatusAction':
    let pk = props.getUrlParam('id');
    let pks = [props.getUrlParam('id')];
    ajax({
      url: '/nccloud/obm/ebankpaymentstatus/ebankpaymentstatusdownload.do',
      data: {pks},
      success: (res) => {
        this.getdata(pk,1);
        if (res.data && res.data.msg) {
          toast({ content: res.data.msg , color: 'success' });
        }
      }
    });
      break;
    //状态确认成功
    case 'ConfirmYesAction':
    let data = props.cardTable.getCheckedRows("body");
    if(data.length==0 || data.length>1){
        toast({ color: 'warning', content: props.MutiInit.getIntl("361010ZT") && props.MutiInit.getIntl("361010ZT").get('361010ZT-000012')});/* 国际化处理： 请选择需要确认的一条表体*/
				return
    }
    promptBox({
      title: this.props.MutiInit.getIntl("361010ZT") && this.props.MutiInit.getIntl("361010ZT").get('361010ZT-000018')/* 国际化处理： 确认提醒*/,
      content: this.props.MutiInit.getIntl("361010ZT") && this.props.MutiInit.getIntl("361010ZT").get('361010ZT-000019')/* 国际化处理： 此操作不可逆，请务必与银行进行状态确认后操作!*/,
      beSureBtnClick: () => {
        this.setState({
          showModal_publish: true,
          billId: data[0].data.values.pk_ebankpaymentstatus_b.value,//单据pk
          showModal_title:this.props.MutiInit.getIntl("361010ZT") && this.props.MutiInit.getIntl("361010ZT").get('361010ZT-000011')/* 国际化处理： 电票指令状态确认*/
        });
        props.form.EmptyAllFormValue(funtypeForm);
        props.form.setFormItemsValue(funtypeForm, { 'confirmstatus':{ value: this.props.MutiInit.getIntl("361010ZT") && this.props.MutiInit.getIntl("361010ZT").get('361010ZT-000015'), display: this.props.MutiInit.getIntl("361010ZT") && this.props.MutiInit.getIntl("361010ZT").get('361010ZT-000015') }});/* 国际化处理： 确认成功*/
        props.form.setFormStatus(funtypeForm, 'edit');
      }
    });
    break;
    //状态确认失败
    case 'ConfirmNoAction':
    data = props.cardTable.getCheckedRows("body");
    if(data.length==0 || data.length>1){
        toast({ color: 'warning', content: props.MutiInit.getIntl("361010ZT") && props.MutiInit.getIntl("361010ZT").get('361010ZT-000012')});/* 国际化处理： 请选择需要确认的一条表体*/
				return
    }
    promptBox({
      title: this.props.MutiInit.getIntl("361010ZT") && this.props.MutiInit.getIntl("361010ZT").get('361010ZT-000018')/* 国际化处理： 确认提醒*/,
      content: this.props.MutiInit.getIntl("361010ZT") && this.props.MutiInit.getIntl("361010ZT").get('361010ZT-000019')/* 国际化处理： 此操作不可逆，请务必与银行进行状态确认后操作!*/,
      beSureBtnClick: () => {
        this.setState({
          showModal_publish: true,
          billId: data[0].data.values.pk_ebankpaymentstatus_b.value,//单据pk
          showModal_title:this.props.MutiInit.getIntl("361010ZT") && this.props.MutiInit.getIntl("361010ZT").get('361010ZT-000011')/* 国际化处理： 电票指令状态确认*/
        });
        props.form.EmptyAllFormValue(funtypeForm);
        props.form.setFormItemsValue(funtypeForm, { 'confirmstatus':{ value: this.props.MutiInit.getIntl("361010ZT") && this.props.MutiInit.getIntl("361010ZT").get('361010ZT-000016'), display: this.props.MutiInit.getIntl("361010ZT") && this.props.MutiInit.getIntl("361010ZT").get('361010ZT-000016') }});/* 国际化处理： 确认失败*/
        props.form.setFormStatus(funtypeForm, 'edit');
      }
    });
      break;
    //状态同步
    case 'SyncStatusAction':
    let pk2 = props.getUrlParam('id');
    pks = [props.getUrlParam('id')];
    ajax({
      url: '/nccloud/obm/ebankpaymentstatus/ebankpaystatusfireeventsendState.do',
      data: {pks},
      success: (res) => {
        this.getdata(pk2,1);
        if (res.data && res.data.msg) {
          toast({ content: res.data.msg , color: 'success' });
        }
      }
    });
      break;
    case 'Print':
      printData(props.getUrlParam('id'));
      break;
    case 'output':
      outputData(props.getUrlParam('id'));
      break;
    case 'Preview':
      printData(props.getUrlParam('id'));
      break;
    case 'backBtn':
    props.pushTo('/list', {
      status: 'browse',
      appcode: '361010ZT',
      pagecode: '361010ZT_L01',
      id: ''
    })
      break;
    default:
      break
  }
}


/**
 * 打印、预览
 * @param {*} pk 
 */
function printData(pk) {
  let pks = [pk];
  
  print(
      'pdf', //支持两类: 'html'为模板打印, 'pdf'为pdf打印
      '/nccloud/obm/ebankpaymentstatus/print.do',
      {
          funcode: '361010ZT', //功能节点编码，即模板编码
          appcode: '361010ZT', //模板节点标识
          // printTemplateID: '1001Z61000000000LPN4',//列表打印模板pk 
          oids: pks // 功能节点的数据主键   oids含有多个元素(['1001A41000000000A9LR','1001A410000000009JDD'])时为批量打印,
      }
  );
}


function outputData(pk) {
  let pks = [pk];
  
  output({
    url: '/nccloud/obm/ebankpaymentstatus/print.do',
    data: { 
        oids: pks,
        outputType: 'output'
    }
  });
}
