!function(e, t) {
    "object" == typeof exports && "object" == typeof module ? module.exports = t(require("nc-lightapp-front")) : "function" == typeof define && define.amd ? define(["nc-lightapp-front"], t) : "object" == typeof exports ? exports["ssctp/refer/sscbd/SSCWorkGroupGridRef/index"] = t(require("nc-lightapp-front")) : e["ssctp/refer/sscbd/SSCWorkGroupGridRef/index"] = t(e["nc-lightapp-front"])
} (window,
function(e) {
    return function(e) {
        var t = {};
        function r(n) {
            if (t[n]) return t[n].exports;
            var o = t[n] = {
                i: n,
                l: !1,
                exports: {}
            };
            return e[n].call(o.exports, o, o.exports, r),
            o.l = !0,
            o.exports
        }
        return r.m = e,
        r.c = t,
        r.d = function(e, t, n) {
            r.o(e, t) || Object.defineProperty(e, t, {
                configurable: !1,
                enumerable: !0,
                get: n
            })
        },
        r.r = function(e) {
            Object.defineProperty(e, "__esModule", {
                value: !0
            })
        },
        r.n = function(e) {
            var t = e && e.__esModule ?
            function() {
                return e.
            default
            }:
            function() {
                return e
            };
            return r.d(t, "a", t),
            t
        },
        r.o = function(e, t) {
            return Object.prototype.hasOwnProperty.call(e, t)
        },
        r.p = "../../../../",
        r(r.s = 592)
    } ({
        0 : function(t, r) {
            t.exports = e
        },
        592 : function(e, t, r) {
            e.exports = r(78)
        },
        78 : function(e, t, r) {
            "use strict";
            Object.defineProperty(t, "__esModule", {
                value: !0
            }),
            t.
        default = function() {
                var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                return React.createElement(n, Object.assign({
                    multiLang: {
                        moduleId: '7010',
                        domainName: 'ssctp',
                        currentLocale: 'simpchn'
                    },
                    refType: "tree",
                    refName: "refer-sscworkgroup-0001",
                    refCode: "ssccloud.refer.sscbd.SSCWorkGroupGridRef",
                    placeholder: "refer-sscworkgroup-0001",
                    queryTreeUrl: "/nccloud/ssccloud/sscbd/SSCWorkGroupGridRef.do",
                    isMultiSelectedEnabled: !1
                },
                e))
            };
            var n = r(0).high.Refer
        }
    })
});