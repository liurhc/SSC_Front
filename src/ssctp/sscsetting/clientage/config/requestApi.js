import { ajax } from 'nc-lightapp-front';

let requestApiOverwrite = {
    // 表模板初始化查询
    tpl: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/ClientageTempletAction.do',
            data: opt.data,
            success: opt.success
        });
    },
    // 树节点对应业务节点查询
    queryTableData: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/ClientageQueryRelationAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    // 新增
    add: (opt) => {
        opt.data = {
            head: opt.data
        }
        ajax({
            url: '/nccloud/ssctp/sscbd/ClientageAddRelationAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    // 保存
    save: (opt) => {
        opt.data = {
            head: opt.data
        }
        ajax({
            url: '/nccloud/ssctp/sscbd/ClientageSaveRelationAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    // 保存
    saveForm: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/ClientageSaveRelationAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    // 删除
    del: (opt) => {
        opt.data = {
            head: opt.data
        };
        ajax({
            url: '/nccloud/ssctp/sscbd/ClientageDelRelationAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        });
    },
    // 启用
    enable: (opt) => {
        // opt.data = {
        //     head: opt.data
        // }
        ajax({
            url: '/nccloud/ssctp/sscbd/ClientageEnableAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            },
        });
    },
    // 停用
    disabled: (opt) => {
        // opt.data = {
        //     head: opt.data
        // }
        ajax({
            url: '/nccloud/ssctp/sscbd/ClientageDisableAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        });
    },
    // 重置任务
    resetTask: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCTaskResetNoApproveTaskAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    }
}

export default requestApiOverwrite;