import React, { Component } from 'react';
import { createPage, base } from 'nc-lightapp-front';
import { buttonClick, initTemplate, afterEvent,selectedChangeEvent, query } from './events';
import dataModelChange from './events/dataModelChange';
import { Nav } from 'ssccommon/components/global-nav'
import { ProfileStyle, ProfileHead, ProfileBody, BodyRight, HeadCenterCustom, ButtonGroup } from 'ssccommon/components/profile';
import { EditTable } from 'ssccommon/components/table';
import SSCCenterRef from '../../../refer/sscbd/SSCUnitGridRef'
import './index.less';

const { NCCheckbox, NCModal, NCButton, NCFormControl } = base;

class ClientageGrid extends Component {
    constructor(props) {
        super(props);
        this.initTableData = [];     // 表格数据,用于查询使用
        this.state = {
            showModal:false,          // 表单Model显示/隐藏
            searchValue:'',           // 业务单元查询
            showDisabled: false,      // 显示停用
            isEdit: false,            // 编辑态/浏览态
            SSCCenter:{},             // 共享中心item
        }
        initTemplate.call(this, props);
    }

    componentWillMount() {
        window.onbeforeunload = () => {
            let status = this.props.editTable.getStatus(window.presetVar.listArea);
            if (status === 'edit') {
                return '';
            }
        }
    }    

    componentDidMount() {}

    render() {
        let { form } = this.props;
        let { createForm } = form;
        let multiLang = this.props.MutiInit.getIntl(7010); 
        return (
            <div id='clientage'>
                <Nav ref='nav' defaultActIdx={'1'} saveBeforeLink={this.state.isEdit} handleTruncation={buttonClick.handleTruncation.bind(this)} {...this.props}/>

                <ProfileStyle layout="singleTable" {...this.props}>
                    {/*页面头*/}
                    {/*"701001RWCL-0068": "设置委托关系"*/}
                    <ProfileHead title={multiLang && multiLang.get('701001RWCL-0068')}>
						
                        <HeadCenterCustom>
                        
                            <span style={{width:'200px'}}>
                                <span class="ssc-must">*</span>
                                {SSCCenterRef({
                                        onChange: query.onSSCCenterChange.bind(this),
                                        value:this.state.SSCCenter,
                                        disabled:this.state.isEdit,
                                    })}
                            </span>
                            <NCFormControl
                                // className="definition-search-handel"
                                value={this.state.searchValue}
                                type={'search'}
                                placeholder={multiLang && multiLang.get('701001RWCL-0069')}//请搜索业务单元
                                onChange={ query.searchAreaChange.bind(this)} 
                                disabled={this.state.isEdit} // 编辑态禁用/浏览态可用
                                //onSearch={query.searchAreaQuery.bind(this)} // 查询调用
                            />
                            <span className='showOff'>
                                {/*"701001RWCL-0070": "显示停用"*/}
                                <NCCheckbox  onChange={query.showDisabledChange.bind(this)}
                                            checked={this.state.showDisabled} disabled={this.state.isEdit}>{multiLang && multiLang.get('701001RWCL-0070')}</NCCheckbox>
                            </span>
                        </HeadCenterCustom>
                        <ButtonGroup
                            area={window.presetVar.btnArea}
                            ctrlEditTableArea={[window.presetVar.listArea]}
                            buttonEvent={
                                {
                                    Add: {click:buttonClick.addClick.bind(this)},
                                    Edit:{afterClick:buttonClick.afterEdit.bind(this)},
                                    Reset:{click:buttonClick.resetTask.bind(this)},
                                    Save:{
                                        url:'/nccloud/ssctp/sscbd/ClientageSaveRelationAction.do',
                                        getSendParam: dataModelChange.jsonToVo,
                                        afterClick: (ncprops, data) =>{buttonClick.afterSave(this, data)}
                                    },
                                    Cancel:{afterClick:buttonClick.afterCancel.bind(this)},
                                    BatchDelete:{click:buttonClick.batchDelete.bind(this)}
                                }
                            }
                        />
                    </ProfileHead>
                    {/*页面体*/}
                    <ProfileBody>
                        <BodyRight> 
                            <EditTable
                                showCheck={this.state.isEdit}
                                areaId={window.presetVar.listArea}
                                onAfterEvent={afterEvent.bind(this)}
                                isAddRow={false}
                                selectedChange={selectedChangeEvent.bind(this)}
                            />
                        </BodyRight>
                    </ProfileBody>
                </ProfileStyle>

                {/* 新增弹出框 */}
                <NCModal show = {this.state.showModal} onHide={()=>{this.setState({showModal:false})}} className='senior'>
                    <NCModal.Header closeButton={true}>
                        {/*"701001RWCL-0071": "新增共享委托关系"*/}
                        <NCModal.Title>{multiLang && multiLang.get('701001RWCL-0071')}</NCModal.Title>
                    </NCModal.Header>
                    <NCModal.Body>
                        <div className="ssc_edit_modal_form">
                        {createForm(window.presetVar.sscwtgx_form ,{
                            onAfterEvent: afterEvent.bind(this),
                        })}
                        </div>
                    </NCModal.Body>
                    <NCModal.Footer>
                        {/*"7010-0003": "保存","7010-0004": "取消"*/}
                        <NCButton colors="primary" onClick={buttonClick.addFormSave.bind(this)}>{multiLang && multiLang.get('7010-0003')}</NCButton>
                        <NCButton onClick={buttonClick.addFormCancel.bind(this)}>{multiLang && multiLang.get('7010-0004')}</NCButton>
                    </NCModal.Footer>
                </NCModal>
            </div>
        )
    }
}

let ClientageDom = createPage({
    mutiLangCode: '7010'
})(ClientageGrid);
export default ClientageDom;