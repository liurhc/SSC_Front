import 'ssccommon/components/globalPresetVar';
import fixedTemplet from './fixedTemplet';
import {getMultiLang} from 'nc-lightapp-front'

export default function(props) {
    //变量设置规范
    window.presetVar = {
        ...window.presetVar,
        pageId: '700102WTGX_L',
        sscwtgx_form:'sscwtgx_form',
        listArea: 'sscwtgx',
        btnArea: 'btnArea',
        listBtnArea: 'listbtnarea'
    };
    props.createUIDom({
            // pagecode: this.state.showModal == true ? window.presetVar.wtgx_form:window.presetVar.pageId, //页面id
            // appid: ' ' //注册按钮的id
    },
    (data)=>{
        getMultiLang({moduleId: 7010, domainName: 'ssctp',currentLocale: 'zh-CN', callback: (json) => {
            //createUIDom第一次加载是异步加载,以后F5刷新会走缓存
            // window.setTimeout(() => {
            let meta = data.template;
            //设置新增form编辑态
            meta[window.presetVar.sscwtgx_form].status = 'edit';
            //设置区域模板
            props.meta.setMeta(meta);
            //设置按钮模板
            props.button.setButtons(data.button);
            meta.sscwtgx_form.items.find((item)=>{
                if(item.attrcode === "pk_org"){
                    item.isShowUnit = true;
                    item.isMultiSelectedEnabled = true;
                    item.unitProps = {
                        placeholder: json['701001RWCL-0078'],//701001RWCL-0078：集团(所有)
                        refName: json['701001RWCL-0078'],
                        refType: "tree",
                        refCode: "uapbd.org.GroupDefaultTreeRef",
                        queryTreeUrl: "/nccloud/uapbd/ref/GroupDefaultTreeRef.do",
                        rootNode: { refname: json['701001RWCL-0078'], refpk: "root" },
                    }
                    // item.defaultUnitValue={ refname: '123',refpk: '345'}
                }
            })
            //设置按钮行为为弹窗
            // props.button.setPopContent('Delete','确认要删除该信息吗？') /* 设置操作列上删除按钮的弹窗提示 */
            //设置按钮的初始可见性
            props.button.setButtonVisible(['Add', 'Edit', 'Reset'], true);
            // props.button.setButtonVisible(['head_group', 'Reset'], true);
            props.button.setButtonVisible(['Save', 'Cancel', 'BatchDelete'], false);
            //设置按钮的禁用
            // props.button.setButtonDisabled(['Add', 'Edit', 'Reset'], true);
            props.button.setButtonDisabled(['Add', 'Edit', 'Reset'], true);
        }})
        }
    )
}