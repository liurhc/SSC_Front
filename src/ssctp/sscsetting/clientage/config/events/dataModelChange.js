let jsonToVo = (data) =>{
    data[window.presetVar.listArea].rows.map((one)=>{
        if(one.values.enablestate.value == true){
            one.values.enablestate.value=2;
        }else{
            one.values.enablestate.value=3;
        }
    })
    return data;
}

let voToJson = (data) =>{
    data[window.presetVar.listArea].rows.map((one)=>{
        if(one.values.enablestate.value == 2){
            one.values.enablestate={value:true};
        }else{
            one.values.enablestate={value:false};
        }
    })
    return data;
}

export default {jsonToVo, voToJson}