import { base, toast } from 'nc-lightapp-front';
import requestApi from '../requestApi'
import { setTableExtendCol } from 'ssccommon/components/profile'
import dataModelChange from './dataModelChange'
import tableExtendButtonClick from './tableExtendButtonClick';
import { promptBox } from "nc-lightapp-front"

const { NCMessage } = base;

/**
 * @Author:gaoymf
 * @Description:编辑按钮处理后事件
 * @Date: 16:12 2018/5/11
 * @param:
 */
let afterEdit = function() {
    this.setState({ isEdit: true });
    this.props.button.setButtonVisible(['Reset', 'Add','Edit'], false);
    this.props.button.setButtonVisible(['BatchDelete'], true);
    //设置表格的扩展按钮列
    setTableExtendCol(this.props, this.props.meta.getMeta(), [{
        areaId: window.presetVar.listArea,
        btnAreaId: window.presetVar.listBtnArea,
        buttonVisible: (record, index) => {
            return ['Delete']
        },
        onButtonClick: tableExtendButtonClick.bind(this)
    }]);
}


/**
 * @Author:gaoymf
 * @Description:保存按钮处理后事件
 * @Date: 16:12 2018/5/11
 * @param:
 */
let afterSave = (that, data) => {
    that.setState({ isEdit: false });
    that.props.button.setButtonVisible(['Reset', 'Add','Edit'], true);
    that.props.button.setButtonVisible(['BatchDelete'], false);
    that.props.meta.getMeta()[window.presetVar.listArea].items.pop();
    // 后台数据为空,返回值没有data.data
    if (data.data == undefined) {
        that.props.editTable.setTableData(window.presetVar.listArea, { rows: [] });
    } else {
        let newData = dataModelChange.voToJson(data.data);
        that.props.editTable.setTableData(window.presetVar.listArea, newData[window.presetVar.listArea]);
    }

}
/**
 * 取消按钮处理后事件
 * @param {*} that 
 */
let afterCancel = function() {
    this.setState({ isEdit: false });
    this.props.button.setButtonVisible(['Reset', 'Add','Edit'], true);
    this.props.button.setButtonVisible(['BatchDelete'], false);
    this.props.meta.getMeta()[window.presetVar.listArea].items.pop();
}
/**
 * 批量删除按钮处理后事件
 * @param {*} that 
 */
let batchDelete = function() {
    let data = this.props.editTable.getCheckedRows(window.presetVar.listArea);
    let arr = data.map(item => item.index);
    this.props.editTable.deleteTableRowsByIndex(window.presetVar.listArea, arr);
}


/**
 * @Author:gaoymf
 * @Description:新增按钮点击
 * @Date: 16:12 2018/5/11
 * @param:
 */
let addClick = function() {
    this.setState({ showModal: true });
    // 清空表单内容
    this.props.form.EmptyAllFormValue(window.presetVar.sscwtgx_form);
    // 新增表单共享中心pk赋值
    this.props.form.setFormItemsValue(window.presetVar.sscwtgx_form, { 'pk_sscunit': { value: this.state.SSCCenter.refpk, display: null } });
}
/**
 * @Author:gaoymf
 * @Description:表单保存
 * @Date: 16:12 2018/5/13
 * @param:
 */
let addFormSave = function() {
    let flag = true;
    // 关闭modal
    flag = this.props.form.isCheckNow(window.presetVar.sscwtgx_form);
    let multiLang = this.props.MutiInit.getIntl(7010); 
    if (flag) {
        // 获得form数据
        let data = this.props.form.getAllFormValue(window.presetVar.sscwtgx_form);
        // 校验同一共享服务中心不能委托相同的业务单元
        let tableRows = this.props.editTable.getAllRows(window.presetVar.listArea);
        let isExistOrg = [];
        tableRows.map((row) => {
            data.rows[0].values.pk_org.value.indexOf(row.values.pk_org.value) > -1 && isExistOrg.push(row.values.pk_org.display);
        })
        if (isExistOrg.length > 0) {
            toast({ content: multiLang && multiLang.get('701001RWCL-0072') + isExistOrg + ']', color: 'danger' })//同一共享服务中心下不能委托相同的业务单元[
        } else {
            // 设置新增状态
            data.rows[0].values.enablestate.value = 2;
            requestApi.add({
                data: data,
                success: (res) => {
                    let newres = dataModelChange.voToJson(res);

                    let nowTableData = this.props.editTable.getAllData(window.presetVar.listArea);
                    let nowRows = nowTableData.rows.concat(newres.sscwtgx.rows);
                    nowTableData.rows = nowRows;

                    this.props.editTable.setTableData(window.presetVar.listArea, nowTableData);
                    this.setState({ showModal: false });
                    this.props.form.EmptyAllFormValue([window.presetVar.sscwtgx_form]);
                }
            })
        }
    }
}

/**
 * @Author:gaoymf
 * @Description:表单取消
 * @Date: 16:12 2018/5/13
 * @param:
 */
let addFormCancel = function() {
    this.setState({ showModal: false });
    this.props.form.EmptyAllFormValue([window.presetVar.sscwtgx_form]);
}


/**
 * @Author:gaoymf
 * @Description:重置任务
 * @Date: 16:12 2018/5/14
 * @param:
 */
let resetTask = function() {
    let multiLang = this.props.MutiInit.getIntl(7010); 
    promptBox({
        color: 'warning',       // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
        title: multiLang && multiLang.get('701001RWCL-0077'),//"重置任务"
        content:  multiLang && multiLang.get('701001RWCL-0073'),//该操作会重置任务,请确定?
        beSureBtnClick: ()=>{
            requestApi.resetTask({
                data: {
                    pk_sscunit : this.state.SSCCenter.refpk
                },
                success: (res) => {
                    //701001RWCL-0075:重置完成，已重置,701001RWCL-0076:条任务
                    //NCMessage.create({ content: (multiLang && multiLang.get('701001RWCL-0075'))+res.resetnum+(multiLang && multiLang.get('701001RWCL-0076')), color: 'success', position: 'bottomRight' });
                    toast({ content: (multiLang && multiLang.get('701001RWCL-0075'))+res.resetnum+(multiLang && multiLang.get('701001RWCL-0076')), color:"success"});
                },
                error: (res) => {
                    console.log(res.message);
                }
            })
        }
    })
}

/**
 * @Author:gaoymf
 * @Description:导航条点击
 * @Date: 16:12 2018/9/15
 * @param:
 */

let handleTruncation = function(){
    let multiLang = this.props.MutiInit.getIntl(7010); 
    promptBox({
        color: 'warning',       // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
        title: multiLang && multiLang.get('701001RWCL-0047'),      //"提示信息"
        content:  multiLang && multiLang.get('701001RWCL-0074'),//此操作会丢失本次修改,请确定?
        beSureBtnClick: ()=>{
                this.refs.nav.linkByClickedIdx();
        }
    })
}


export default { afterEdit, afterSave, afterCancel, batchDelete, addClick, addFormSave, addFormCancel, resetTask,handleTruncation }