/**
 * 表格扩展按钮click事件
 */
function tableExtendButtonClick() {
    return {
        ['Delete']: (record, index) => {
            this.props.editTable.delRow(window.presetVar.listArea, index);
        }
    }
}

export default tableExtendButtonClick;