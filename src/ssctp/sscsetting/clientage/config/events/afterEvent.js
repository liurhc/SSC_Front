import { base } from "nc-lightapp-front";
import requestApi from "../requestApi";
let { NCMessage } = base;
export default function afterEvent(props, moduleId, key, changedrows, value, datas, data) {
    let multiLang = props.MutiInit.getIntl(7010); 
    switch (moduleId) {
        case window.presetVar.listArea:
            switch (key) {
                case "enablestate":
                    let tableData = props.editTable.getAllData(moduleId);
                    // 此处转换启用前端switch(true,false)和后端枚举不一致,暂时这么写
                    tableData.rows = tableData.rows.filter(item => {
                        if (item.rowid === data.rowid) {
                            if (value) {
                                item.values.enablestate.value = 1;
                            } else {
                                item.values.enablestate.value = 2;
                            }
                            return [item];
                        }
                    });
                    // 此处转换启用前端switch(true,false)和后端枚举不一致,暂时这么写
                    if (changedrows) {
                        // 启用
                        requestApi.enable({
                            data: {
                                head: tableData
                            },
                            success: res => {
                                NCMessage.create({ content: multiLang && multiLang.get('701001RWCL-0043'), color: "success", position: "topRight" });//启用成功
                                props.editTable.setValByKeyAndRowId(moduleId, data.rowid, "ts", {
                                    value: res.sscwtgx.rows[0].values.ts.value,
                                });
                                props.editTable.setValByKeyAndRowId(moduleId, data.rowid, "enablestate", { value: true });
                                let tableData = this.props.editTable.getAllData(window.presetVar.listArea);
                                props.editTable.setTableData(window.presetVar.listArea, tableData);
                                this.setState({ tableData: tableData });
                            }
                        });
                    } else {
                        // 停用
                        requestApi.disabled({
                            data: {
                                head: tableData
                            },
                            success: res => {
                                NCMessage.create({ content: multiLang && multiLang.get('701001RWCL-0044'), color: "success", position: "topRight" });//停用成功
                                props.editTable.setValByKeyAndRowId(moduleId, data.rowid, "ts", {
                                    value: res.sscwtgx.rows[0].values.ts.value,
                                });
                                props.editTable.setValByKeyAndRowId(moduleId, data.rowid, "enablestate", { value: false });
                                let nowData = this.props.editTable.getAllData(window.presetVar.listArea);
                                props.editTable.setTableData(window.presetVar.listArea, nowData);
                                this.setState({ tableData: tableData });
                            }
                        });
                    }
                    break;
                case "pk_org":
                    let rowid = data.rowid;
                    // 联动业务单元编码
                    props.editTable.setValByKeyAndRowId(moduleId, rowid, "pk_org.code", {
                        value: changedrows.hasOwnProperty('refcode') ?changedrows.refcode:'',
                        display: changedrows.hasOwnProperty('refcode') ? changedrows.refcode :''
                    });
                    // 联动业务单元所属集团
                    props.editTable.setValByKeyAndRowId(moduleId, rowid, "pk_org.pk_group.name", {
                        value: changedrows.hasOwnProperty('values') ? changedrows.values.gname.value :'',
                        display:changedrows.hasOwnProperty('values') ? changedrows.values.gname.value :''
                    });
                    break;
                default:
                    break;
            }
            break;
        case window.presetVar.sscwtgx_form:
            switch (key) {
                case "pk_org":
                    let refcodeArr = [];
                    let gnameArr = [];
                    if (datas.length > 0) {
                        datas.map(item => {
                            refcodeArr.push(item.refcode);
                            if (item.values != undefined) {
                                if (item.values.gname != undefined) {
                                    gnameArr.push(item.values.gname.value);
                                }
                            }
                        });
                        // 业务单元联动业务单元编码
                        props.form.setFormItemsValue(window.presetVar.sscwtgx_form, {
                            "pk_org.code": { value: "", display: refcodeArr.join(",") }
                        });
                        // 业务单元联动业务单元所属集团
                        props.form.setFormItemsValue(window.presetVar.sscwtgx_form, {
                            "pk_org.pk_group.name": { value: "", display: gnameArr.join(",") }
                        });
                    }else {
                        // 业务单元联动业务单元编码
                        props.form.setFormItemsValue(window.presetVar.sscwtgx_form, {
                            "pk_org.code": { value: "", display: "" }
                        });
                        // 业务单元联动业务单元所属集团
                        props.form.setFormItemsValue(window.presetVar.sscwtgx_form, {
                            "pk_org.pk_group.name": { value: "", display: "" }
                        });
                    }
                    break;
                default:
                    break;
            }
        default:
            break;
    }
}