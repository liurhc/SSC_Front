import requestApi from "../requestApi";
import dataModelChange from "./dataModelChange";
// 查询项对应的pk
function getSearchPkClientage(searchValue){
    let pk_ssclientage = '';
    if(searchValue!==''){
        let pkArr = [];
        let tableRows = this.initTableData.rows;
        // let tableRows = this.props.editTable.getAllRows(window.presetVar.listArea);
        tableRows.filter((row) => {
            if (row.values.pk_org.display.indexOf(searchValue) > -1) {
                pkArr.push(row.values.pk_ssclientage.value);
            }
        })
        if(pkArr.length>0){
            pk_ssclientage = pkArr.join(",");
        }else {
            return '00000000';
        }
    }
    return pk_ssclientage;
}


//共享中心改变事件
let onSSCCenterChange = function(e) {
    this.setState({ SSCCenter: e,searchValue: '',showDisabled:false });
    if (e.hasOwnProperty('refpk')) {
        let req = { pk: e.refpk, showDisabled: this.state.showDisabled,searchCondition :"" };
        queryTableData.call(this, req);
    } else {
        this.props.editTable.setTableData(window.presetVar.listArea, { rows: [] });
        this.props.button.setButtonDisabled(['Add', 'Edit', 'Reset'], true);
    }
};
/**
 * 显示停用复选框勾选事件
 * @param {*} e
 */
let showDisabledChange = function(e) {
    this.setState({ showDisabled: e });
    // 更改状态
    if(this.state.SSCCenter.hasOwnProperty('refpk')){
        //选中共享中心 执行查询
        let pk_ssclientages = getSearchPkClientage.call(this,this.state.searchValue);
        let req = { pk: this.state.SSCCenter.refpk, showDisabled: e,searchCondition: pk_ssclientages};
        queryTableData.call(this, req);
    }
};

/**
 * 查询业务单元内容改变事件
 * @param {*} searchValue
 */
let searchTime = null;
let searchAreaChange = function(searchValue) {
    this.setState({ searchValue: searchValue });

    clearTimeout(searchTime);
    searchTime = setTimeout(()=>{
        this.props.editTable.setFiltrateTableData(window.presetVar.listArea, ['pk_org'], searchValue, false)
    }, 500);
};

// 查询表数据 req:请求参数 
let queryTableData = function(req) {
    requestApi.queryTableData({
        data: req,
        success: res => {
            if (res) {
                let resNew = dataModelChange.voToJson(res);
                this.props.editTable.setTableData(window.presetVar.listArea, resNew[window.presetVar.listArea]);
                this.initTableData = resNew[window.presetVar.listArea];
            } else {
                this.props.editTable.setTableData(window.presetVar.listArea, { rows: [] });
            }
            this.props.button.setButtonDisabled(['Add', 'Edit', 'Reset'], false);
        }
    });
};
export default { onSSCCenterChange, showDisabledChange, searchAreaChange, queryTableData };