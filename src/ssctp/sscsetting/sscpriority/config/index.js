import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import TreeTable from './TreeTable';

ReactDOM.render(<TreeTable/>
        , document.querySelector('#app'));