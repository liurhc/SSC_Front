import requestApi from "ssccommon/components/requestApi";
import { ajax } from 'nc-lightapp-front';



let requestDomain = '';
let requestApiOverwrite = {
    ...requestApi,

    query: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/erm/erbudgetmg/FysqViewBillAction.do`,
            data: opt.data,
            success: (data) => {
                opt.success(data);
            }
        })
    },
    // 查询树节点
    queryTreeData: (opt) => {
        ajax({
            url: '/nccloud/sscbd/workinggroup/QueryAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    getTableData: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCPriorityQryAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    saveTableData: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCPrioritySaveAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    saveFormData: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCPrioritySaveAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    queryPublicFields: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/QueryPublicFieldAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    }

}

export default requestApiOverwrite;