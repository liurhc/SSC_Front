import React, { Component } from "react";
import { createPage, base, high } from "nc-lightapp-front";
import requestApi from "./requestApi";
import { buttonClick, initTemplate, afterEvent } from "./events";
import { ProfileStyle, ProfileHead, ProfileBody, BodyLeft, BodyRight, HeadCenterCustom, ButtonGroup } from "ssccommon/components/profile";
import { SyncTree } from "ssccommon/components/tree";
import { EditTable } from "ssccommon/components/table";
import { Nav } from "ssccommon/components/global-nav";
import SSCCenter from "../../../refer/sscbd/SSCUnitGridRef";
import "./index.less";

const { NCModal,NCButton } = base;
const { FormulaEditor } = high;

function PublicField({ setName, setExplain, name }) {
  let rowList = [];
  name.map(item => {
    rowList.push(
      <li
        onClick={() => {
          setExplain("");
        }}
        onDoubleClick={() => {
          setName("" + item.code + "");
        }}
      >
        {item.code}&nbsp;&nbsp;{item.name}
      </li>
    );
  });
  return <ul className='ssctp-formula-publicfiled-content'>{rowList}</ul>;
}
class TreeTable extends Component {
  constructor(props) {
    super();
    this.formulaInputValue = ""; // 公式内容
    this.state = {
      isHeadEdit: false,
      showAddModal: false,      // 新增modal
      isShowSearch: true,       // 控制树查询区显示
      isDisableSearch: false,   // 控制树查询区禁用/可用
      SSCCenter: {},            // 共享服务中心
      isAdjustOrder:false,      // 判断是否是调序状态,控制操作列显示内容
      showFormula:false,        // 判断是否显示公式编辑器
      publicFieldData: "",      // 公共字段
      showConfirmModal:false,   // 跳转确认框
      isEdit:false               // 是否是编辑态
    };
    initTemplate.call(this, props);
  }

  componentDidMount() {}

  //共享中心改变事件
  onSSCCenterChange = e => {
      this.setState({ SSCCenter: e });
      let { syncTree } = this.props;
      let { setSyncTreeData } = syncTree;
      if (e.hasOwnProperty('refpk')) {
          // 选中共享中心
          this.setState({isShowSearch:true}); // 显示树型的查询区
          let req = { pk_sscunit: e.refpk,enablestate:'2' }; // 2:查询启用的作业组  3:查询停用  不传Or传'':查询所有
          this.queryTreeData(req); // 查询左侧树
          this.props.editTable.setTableData(window.presetVar.head.tableId, { rows: [] });
      } else {
          // 清空共享中心
          this.setState({isShowSearch:false}); // 隐藏树型的查询区
          setSyncTreeData(window.presetVar.head.treeId, []); // 树数据清空
          this.props.button.setButtonDisabled(['Add', 'Edit', 'Delete'], true); // 按钮禁用
          this.props.editTable.setTableData(window.presetVar.head.tableId, { rows: [] }); // 表格内数据清空
      }
  };

  // 查询树节点数据
  queryTreeData(req) {
    let { syncTree } = this.props;
    let multiLang = this.props.MutiInit.getIntl(7010); 
    let { setSyncTreeData } = syncTree;
    this.setSyncTreeData = setSyncTreeData; //设置树根节点数据方法
    requestApi.queryTreeData({
      data: req,
      success: res => {
        res.nodes.push({
          refpk: "0001ZG100000SSCGROUP",
          refcode: "ROOT",
          refname: multiLang && multiLang.get('701001RWCL-0051'),//"作业组"
        });
        let newTree = syncTree.createTreeData(res.nodes); //创建树 组件需要的数据结构
        this.setSyncTreeData(window.presetVar.head.treeId, newTree);
      }
    });
  }

  openFormula() {
    let multiLang = this.props.MutiInit.getIntl(7010); 
    requestApi.queryPublicFields({
      data: "",
      success: res => {
        console.log(multiLang && multiLang.get('701001RWCL-0166')/*"公共字段数据"*/, res);
        this.setState({ publicFieldData: res, showFormula: true });
      }
    });
  }

  handleTruncation = ()=>{
    this.setState({showConfirmModal:true});
 }

  render() {
    let { form, modal } = this.props;
    let { createForm } = form;
    let { createModal } = modal;
    let multiLang = this.props.MutiInit.getIntl(7010); 
    buttonClick.addBtnEventConfig.click = buttonClick.addBtnEventConfig.click.bind(this);
    buttonClick.editBtnEventConfig.click = buttonClick.editBtnEventConfig.click.bind(this);
    buttonClick.saveBtnEventConfig.click = buttonClick.saveBtnEventConfig.click.bind(this);
    buttonClick.cancelBtnEventConfig.click = buttonClick.cancelBtnEventConfig.click.bind(this);
    buttonClick.deleteBtnEventConfig.click = buttonClick.deleteBtnEventConfig.click.bind(this);
    buttonClick.saveModalBtnEventConfig.click = buttonClick.saveModalBtnEventConfig.click.bind(this);
    buttonClick.cancelModalBtnEventConfig.click = buttonClick.cancelModalBtnEventConfig.click.bind(this);
    return (
        <div>
          <Nav defaultActIdx={"5"} ref='nav' saveBeforeLink={this.state.isAdjustOrder} handleTruncation={buttonClick.handleTruncation.bind(this)} {...this.props} />
          <ProfileStyle id='sscpriority' layout="treeTable" {...this.props}>
            {/*页面头*/}
            {/*"701001RWCL-0063": "优先级设置*/}
            <ProfileHead title={multiLang && multiLang.get('701001RWCL-0063')}>
              <HeadCenterCustom>
				        
                <div class="ssccenter">
                  <span class="ssc-must">*</span>
                  <SSCCenter
                    value={this.state.SSCCenter}
                    onChange={e => {
                      this.onSSCCenterChange(e);
                    }}
                    queryCondition={() => {
                      return { test: "" };
                    }}
                    disabled={this.state.isEdit}
                  />
                </div>
              </HeadCenterCustom>
              <ButtonGroup
                area="list_head"
                ctrlEditTableArea={[window.presetVar.head.tableId]}
                buttonEvent={{
                  Add: buttonClick.addBtnEventConfig,
                  Edit: buttonClick.editBtnEventConfig,
                  Save: buttonClick.saveBtnEventConfig,
                  Cancel: buttonClick.cancelBtnEventConfig,
                  Delete: buttonClick.deleteBtnEventConfig
                }}
              />
            </ProfileHead>
            {/*页面体*/}
            <ProfileBody>
              <BodyLeft>
                <SyncTree
                  areaId="WorkGroupTree"
                  onSelectEve={buttonClick.treeSelectEvent.bind(this)}
                  needSearch={this.state.isShowSearch}
                  defaultExpandAll="true"
                  disabledSearch={this.state.isDisableSearch}
                />
              </BodyLeft>
              <BodyRight>
                <EditTable
                  showCheck={!this.state.isAdjustOrder}
                  areaId={window.presetVar.head.tableId}
                  onAfterEvent={afterEvent.bind(this)}
                  selectedChange={buttonClick.selectedChange}
                />
              </BodyRight>
            </ProfileBody>
          </ProfileStyle>
          <div>
              <NCModal show={this.state.showAddModal} 
                       onHide={()=>{this.setState({showAddModal:false})}}
                       onEntered={() =>
                        (this.formulaInputId.value= this.formulaInputValue)}
                          backdrop='false'>
                  <NCModal.Header closeButton={true}>
                    <NCModal.Title>{multiLang && multiLang.get('701001RWCL-0063')}</NCModal.Title>
                  </NCModal.Header>
                  <NCModal.Body>
                  <div className="ssc_edit_modal_form">
                      {createForm(window.presetVar.head.modalId, {
                        //编辑后事件
                        onAfterEvent: afterEvent.bind(this)
                      })}
                      <div>
                        <FormulaEditor
                          ref="formulaEditor"
                          value={
                            this.formulaInputId == undefined
                              ? ""
                              : this.formulaInputId.value
                          }
                          show={this.state.showFormula}
                          attrConfig={[
                            {
                              tab: multiLang && multiLang.get('701001RWCL-0096'),//701001RWCL-0096：公共字段
                              TabPaneContent: PublicField,
                              params: { name: this.state.publicFieldData }
                            }
                          ]}
                          //701001RWCL-0097：表和字段，701001RWCL-0098：元数据属性
                          noShowAttr = {[multiLang && multiLang.get('701001RWCL-0097'),multiLang && multiLang.get('701001RWCL-0098')]}
                          // formulaConfig={[{tab:'常用', TabPaneContent: Example, params: {}}]}
                          onOk={value => {
                            this.setState({ showFormula: false });
                            this.formulaInputId.value = value;
                            form.setFormItemsValue(window.presetVar.head.modalId,{'formula':{'value':value,'display':value}});
                          }} //点击确定回调
                          onCancel={a => {
                            this.setState({ showFormula: false });
                          }} //点击确定回调
                          metaParam={{
                            pk_billtype: "",
                            bizmodelStyle: "fip",
                            classid: "7bd63d9b-f394-4a7d-a52c-72f8274d471f"
                          }}
                          onHide={() => {
                            this.setState({ showFormula: false });
                          }}
                        />
                      </div>
                      </div>
                  </NCModal.Body>
                  <NCModal.Footer>
                    {/*"7010-0003": "保存","7010-0004": "取消"*/}
                    <NCButton colors="primary" onClick={buttonClick.saveModalBtnEventConfig.click}>
                      {multiLang && multiLang.get('7010-0003')}
                    </NCButton>
                    <NCButton
                      onClick={buttonClick.cancelModalBtnEventConfig.click}
                    >
                      {multiLang && multiLang.get('7010-0004')}
                    </NCButton>
                  </NCModal.Footer>
                </NCModal>
          </div>
        </div>
    );
  }
}

TreeTable = createPage({
  mutiLangCode: '7010'
})(TreeTable);
export default TreeTable;
