export default function afterEvent(props, moduleId, key, value, changedrows) {
  console.log("afterEvent", props, moduleId, key, value, 22, changedrows);
  switch (moduleId) {
    case window.presetVar.head.modalId:
      switch (key) {
        case "upgraderule":
          if (value.value == "none") {
            // 晋级模式:无
            props.form.setFormItemsDisabled(moduleId, { 'upgraderulenum': true });  // 禁用晋级时间字段
            props.form.setFormItemsValue(moduleId,{'upgraderulenum':{value:'',display:''}}) // 初始化晋级时间字段
            props.form.setFormItemsRequired(moduleId,{'upgraderulenum':false});     // 必输晋级时间字段
          } else {
            // 晋级模式:有
            props.form.setFormItemsDisabled(moduleId, { 'upgraderulenum': false }); // 可用晋级时间字段
            props.form.setFormItemsRequired(moduleId,{'upgraderulenum':true});     // 必输晋级时间字段
          }
          break;
        default:
          break;
      }
      break;
    default:
      break;
  }
}
