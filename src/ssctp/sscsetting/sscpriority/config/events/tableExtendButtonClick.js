import requestApi from "../requestApi";
/**
 * 表格扩展按钮click事件
 */
function tableExtendButtonClick() {
    return {
        ['Delete_list'] : (record, index) => {
            let headData = this.props.editTable.getAllRowsRemoveKeys(window.presetVar.head.tableId,["opr"]);
            let pk_priority = headData[index].values.pk_priority.value;
            if (pk_priority != "") {
                let reqData = {};
                reqData.head = {
                    areaType: "form",
                    areacode: "head",
                    rows: [headData[index]]
                };
                reqData.head.rows[0].status = "3";
                reqData.userjson = headData[index].values.pk_workinggroup.value;
                requestApi.saveFormData({
                    data: reqData,
                    success: data => {
                        if (data) {
                            this.props.editTable.setTableData(window.presetVar.head.tableId,data[window.presetVar.head.tableId]);
                        } else {
                            this.props.editTable.setTableData(window.presetVar.head.tableId,{ rows: [] });
                        }
                    }
                });
            }
        },
        ['Edit_list'] : (record, index) => {
            this.setState({ showAddModal: true, isHeadEdit: true });
            this.props.form.setFormStatus(window.presetVar.head.modalId, "edit");
            let headValues = this.props.editTable.getAllRowsRemoveKeys(window.presetVar.head.tableId,["opr", "numberindex"])[index].values;
            this.formulaInputValue = headValues.formula.value !=undefined ?headValues.formula.value :'';
            let formValues  = {
                rows:[{values:headValues}]
            }
            this.props.form.setAllFormValue({ [window.presetVar.head.modalId]: formValues });
            if(headValues.upgraderule.value=='none'){
                this.props.form.setFormItemsDisabled(window.presetVar.head.modalId, { 'upgraderulenum': true });
                this.props.form.setFormItemsRequired(window.presetVar.head.modalId,{'upgraderulenum':false});     // 必输晋级时间字段
            }else {
                this.props.form.setFormItemsDisabled(window.presetVar.head.modalId, { 'upgraderulenum': false });
                this.props.form.setFormItemsRequired(window.presetVar.head.modalId,{'upgraderulenum':true});     // 必输晋级时间字段
            }
        },
        ['Up'] : (record, index) => {
            if(index!=0){
                this.props.editTable.setValByKeyAndIndex(window.presetVar.head.tableId, index-1, 'ordernum', {value: record.values.ordernum.value, display:record.values.ordernum.value});
                this.props.editTable.setValByKeyAndIndex(window.presetVar.head.tableId, index, 'ordernum', {value: parseInt(record.values.ordernum.value)-1, display:parseInt(record.values.ordernum.value)-1});
                this.props.editTable.moveRow(window.presetVar.head.tableId,index, index-1);
                this.props.editTable.setRowStatus(window.presetVar.head.tableId, [index,index-1], '1');
            }
        },
        ['Down'] : (record, index) => {
            let totalIndex = this.props.editTable.getNumberOfRows(window.presetVar.head.tableId);
            if(index!=totalIndex-1){
                this.props.editTable.setValByKeyAndIndex(window.presetVar.head.tableId, index+1, 'ordernum', {value: record.values.ordernum.value, display:record.values.ordernum.value});
                this.props.editTable.setValByKeyAndIndex(window.presetVar.head.tableId, index, 'ordernum', {value: parseInt(record.values.ordernum.value)+1, display:parseInt(record.values.ordernum.value)+1});
                this.props.editTable.moveRow(window.presetVar.head.tableId,index, index+1);
                this.props.editTable.setRowStatus(window.presetVar.head.tableId, [index,index+1], '1');
            }
        },
        ['SettingTop'] : (record, index) => {
            this.props.editTable.setRowPosition(window.presetVar.head.tableId,index,'up');
            let tableData = this.props.editTable.getAllData(window.presetVar.head.tableId);
            let changeIndex = []
            tableData.rows.map((row,idx)=>{
                this.props.editTable.setValByKeyAndIndex(window.presetVar.head.tableId, idx, 'ordernum', {value:idx+1, display:idx+1});
                changeIndex.push(idx);
            })
            this.props.editTable.setRowStatus(window.presetVar.head.tableId, changeIndex, '1');
        },
        ['SettingBottom'] : (record, index) => {
            this.props.editTable.setRowPosition(window.presetVar.head.tableId,index,'down');
            let tableData = this.props.editTable.getAllData(window.presetVar.head.tableId);
            let changeIndex = []
            tableData.rows.map((row,idx)=>{
                this.props.editTable.setValByKeyAndIndex(window.presetVar.head.tableId, idx, 'ordernum', {value:idx+1, display:idx+1});
                changeIndex.push(idx);
            })
            this.props.editTable.setRowStatus(window.presetVar.head.tableId, changeIndex, '1');
        },
    }
}


export default tableExtendButtonClick;