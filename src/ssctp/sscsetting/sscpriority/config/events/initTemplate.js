import "ssccommon/components/globalPresetVar";
import fixTemplate from "./fixedTemplet"
import { setTableExtendCol } from "ssccommon/components/profile";
import tableExtendButtonClick from './tableExtendButtonClick';
import {getMultiLang} from 'nc-lightapp-front';

//变量设置规范，如需要
window.presetVar = {
  ...window.presetVar,
  head: {
    treeId: "WorkGroupTree",
    tableId: "SSCPriority",
    tableExtendBtnArea:"listbtnarea",
    modalId: "mainModal"
  }
};

export default function(props) {
    let _this = this;
    
    let createUIDomPromise = new Promise((resolve, reject) => {
        props.createUIDom(
            {
                // pagecode: "700106SYXJ_list_001", //页面id
                // appid: "0001ZG10000000000TBY", //注册按钮的id
                // appcode: "700106SYXJ"
            },
            (data) => {
                resolve(data);
            }
        )
    })

    let getMultiLangPromise = new Promise((resolve, reject) => {
        getMultiLang({
            moduleId: 7010, domainName: 'ssctp', currentLocale: 'zh-CN', callback: (json) => {
                resolve(json);
            }
        })
    })

    Promise.all([createUIDomPromise, getMultiLangPromise]).then((resultList) => {
        let data = resultList[0];
        let json = resultList[1];
        let meta = data.template;
        let buttons = data.button;
        // 静态模板?
        meta[window.presetVar.head.modalId] = data.template['SSCPriority_form'];
        // 设置form编辑态
        meta[window.presetVar.head.modalId].status = 'edit';
        //let buttons = button.concat(fixTemplate.extendButtons);
        props.meta.setMeta(meta);
        props.button.setButtons(buttons);
        //设置按钮行为为弹窗
        props.button.setPopContent('Delete_list', json['701001RWCL-0039']) /* 确定要删除吗？ */
        // 设置默认显示的按钮
        props.button.setButtonsVisible({ Add: true, Edit: true, Delete: true, Save: false, Cancel: false });
        // 默认禁用
        props.button.setButtonDisabled(['Add', 'Edit', 'Delete'], true);
        // 设置操作按钮的事件
        setTableExtendCol(props, meta, [{
            width: '180px',
            areaId: window.presetVar.head.tableId,
            btnAreaId: window.presetVar.head.tableExtendBtnArea,
            buttonVisible: (record, index) => {
                if (_this.state.isAdjustOrder) {
                    return ['SettingTop', 'Up', 'Down', 'SettingBottom']
                } else {
                    return ['Edit_list', 'Delete_list']
                }

            },
            buttonLimit: 4,
            onButtonClick: tableExtendButtonClick.bind(_this)
        }]);
    })

  props.createUIDom(
    {
      // pagecode: "700106SYXJ_list_001", //页面id
      // appid: "0001ZG10000000000TBY", //注册按钮的id
      // appcode: "700106SYXJ"
    },
    function(data) {
      
    }
  );
  props.renderItem(
    "form",
    window.presetVar.head.modalId,
    "formula",
    <div className="formula-div">
      <input
        type="text"
        ref={ref => {
          this.formulaInputId = ref;
        }}
        className="formula-input"
      />
      <span
        className="icon-refer"
        onClick={() => {
          this.openFormula();
        }}
        style={{ cursor: "pointer" }}
      />
    </div>
  );
}

