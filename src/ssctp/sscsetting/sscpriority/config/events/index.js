
import initTemplate from './initTemplate';
import buttonClick  from './buttonClick';
import afterEvent from './afterEvent';

export { buttonClick,initTemplate,afterEvent};
