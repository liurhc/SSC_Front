import { toast } from "nc-lightapp-front";
import { promptBox } from "nc-lightapp-front"
import requestApi from "../requestApi";

//编辑
let editBtnEventConfig = {
  click(props){
    props.button.setButtonsVisible({Add: false,Edit: false,Delete: false,Save: true,Cancel: true});
    props.button.setButtonDisabled(['Save'], false);
    this.setState({isAdjustOrder:true,isDisableSearch:true,isEdit:true});
    props.syncTree.setNodeDisable('WorkGroupTree', true);
  }
};

//保存
let saveBtnEventConfig = {
  click(props){
    // props.button.setButtonsVisible({Add: true,Edit: true,Delete: true,Save: false,Cancel: false});
    let data = props.editTable.getAllData(window.presetVar.head.tableId);
    let req = {
      'SSCPriority':data,
      'userjson' : this.state.workGroupPk
     }
    let that = this;
    requestApi.saveTableData({
        data : req,
        success: (res) => {
          if(res==null){

          }else {
            that.props.editTable.setTableData(
              window.presetVar.head.tableId,
              res[window.presetVar.head.tableId]
            );
          }
          props.button.setButtonsVisible({Add: true,Edit: true,Delete: true,Save: false,Cancel: false});
          that.setState({isAdjustOrder:false,isDisableSearch:false,isEdit:false});
          props.syncTree.setNodeDisable('WorkGroupTree', false);
        }
    })
  }
};

//取消
let cancelBtnEventConfig = {
  click(props){
    props.button.setButtonsVisible({Add: true,Edit: true,Delete: true,Save: false,Cancel: false});
    props.editTable.cancelEdit(window.presetVar.head.tableId);
    this.setState({isAdjustOrder:false,isDisableSearch:false,isEdit:false});
    props.syncTree.setNodeDisable('WorkGroupTree', false);
  }
};


//增加
let addBtnEventConfig = {
  click(props) {
    this.setState({ showAddModal: true, isHeadEdit: false });
    // 新增表单赋默认值
    props.form.setFormItemsValue(window.presetVar.head.modalId, {
      pk_sscunit: { value: this.state.SSCCenter.refpk, display: null }
    });
    props.form.setFormItemsValue(window.presetVar.head.modalId, {
      pk_workinggroup: { value: this.state.workGroupPk, display: null }
    });
    // 公式编辑器默认值
    this.formulaInputValue = '';
  }
};

//删除
let deleteBtnEventConfig = {
  click(props) {
    let checkedData = props.editTable.getCheckedRows(
      window.presetVar.head.tableId
    );
    let delRows = [];
    let reqData = {};
    let multiLang = this.props.MutiInit.getIntl(7010); 
    if (checkedData.length == 0) {
      toast({ content: multiLang && multiLang.get('701001RWCL-0089'), color: "warning" });//701001RWCL-0089:请至少选择一行数据
      return;
    }

    promptBox({
      color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
      title: multiLang && multiLang.get('7010-0002'), //删除
      content: multiLang && multiLang.get('701001RWCL-0168'), //"701001RWCL-0168": "确定要删除所选数据吗？"
      beSureBtnClick: () => {
        for (let i = 0; i < checkedData.length; i++) {
          delRows.push(checkedData[i].data);
          delRows[i].status = "3";
        }
        reqData = {
          head: {
            areaType: "table",
            areacode: "head",
            rows: delRows
          }
        };
        reqData.userjson = this.state.workGroupPk;
        requestApi.saveFormData({
          data: reqData,
          success: data => {
            if (data) {
              props.editTable.setTableData(
                window.presetVar.head.tableId,
                data[window.presetVar.head.tableId]
              );
            } else {
              props.editTable.setTableData(window.presetVar.head.tableId, {
                rows: []
              });
            }
            toast({ color: "success", content: multiLang && multiLang.get('701001RWCL-0042') });//701001RWCL-0042:删除成功
          }
        });
      },   // 确定按钮点击调用函数,非必输
  })
  }
};

let saveModalBtnEventConfig = {
  click(props) {
    if(!this.props.form.isCheckNow(window.presetVar.head.modalId)){
      return;
    }
    let data = this.props.form.getAllFormValue(window.presetVar.head.modalId);
    data.rows[0].values.formula.value = (this.formulaInputId || {}).value || data.rows[0].values.formula.value;
    this.props.form.setFormItemsValue(window.presetVar.head.modalId,{'formula':{value:(this.formulaInputId || {}).value || data.rows[0].values.formula.value,display:null}});
    let reqData = {};
    if(!this.props.form.isCheckNow(window.presetVar.head.modalId)){
      return; 
    }
    // let needChecks = ['code','name','upgraderule','upgraderulenum'];
    // let name4NeedChecks = ['编码','名称','晋级模式','晋级时间'];
    // let ee = this.props.form.isCheckNow(window.presetVar.head.modalId);
    // if(this.formulaInputId.value==""||this.formulaInputId.value.trim()==""){
    //   toast({ content: "优先级条件不能为空", color: "danger" });
    //   return;
    // }
    // for(let i=0;i<needChecks.length;i++){
    //   if(needChecks[i]=='upgraderulenum'){
    //     if(data.rows[0].values['upgraderule'].value!='none'&&(data.rows[0].values['upgraderulenum'].value==null||data.rows[0].values['upgraderulenum'].value.trim()=="")){
    //       toast({ content: name4NeedChecks[i]+"不能为空", color: "danger" });
    //       return;
    //     }
    //     continue;
    //   }
    //   if(data.rows[0].values[needChecks[i]].value==null||data.rows[0].values[needChecks[i]].value.trim()==""){
    //     toast({ content: name4NeedChecks[i]+"不能为空", color: "danger" });
    //     return;
    //   }
    // }
    if (!this.state.isHeadEdit) {
      data.rows[0].status = "2";
    } else {
      data.rows[0].status = "1";
    }
    

    reqData.head = {
      areaType: "form",
      areacode: "head",
      rows: data.rows
    };
    reqData.userjson = this.state.workGroupPk;
    requestApi.saveFormData({
      data: reqData,
      success: data => {
        this.props.form.EmptyAllFormValue(window.presetVar.head.modalId);
        if (data) {
          this.setState({ showAddModal: false });
          this.props.editTable.setTableData(
            window.presetVar.head.tableId,
            data[window.presetVar.head.tableId]
          );
        } else {
          this.props.editTable.setTableData(window.presetVar.head.tableId, {
            rows: []
          });
        }
      }
    });
  }
};

let cancelModalBtnEventConfig = {
  click(props) {
    this.props.form.EmptyAllFormValue(window.presetVar.head.modalId);
    this.setState({ showAddModal: false });
  }
};

/**
 * 树节点选择事件
 */
function treeSelectEvent(key, item) {
  //设置表格数据
  let req = { pk_workinggroup: item.refpk};
  if(item.refpk=='0001ZG100000SSCGROUP'){
    // 按钮禁用
    this.props.button.setButtonDisabled(['Add', 'Edit', 'Delete'], true);
    // 表格清空
    this.props.editTable.setTableData(window.presetVar.head.tableId, {rows: []});
    return;
  }
  this.setState({ workGroupPk: item.refpk });
  requestApi.getTableData({
    data: req,
    success: data => {
      // 树节点点击后按钮可用
      this.props.button.setButtonDisabled(['Add', 'Edit'], false);
      if (data) {
        this.props.editTable.setTableData(window.presetVar.head.tableId,data[window.presetVar.head.tableId]);
      } else {
        this.props.editTable.setTableData(window.presetVar.head.tableId, {rows: []});
      }
    }
  });
}

function selectedChange(props, moduleId, newVal, oldVal) {
  console.log('selectedChangeEvent',props,moduleId,newVal,oldVal);
  if(newVal>0){
      props.button.setButtonDisabled(['Delete'], false);
  }else {
      props.button.setButtonDisabled(['Delete'], true);
  }
}


/**
 * @Author:gaoymf
 * @Description:导航条点击
 * @Date: 16:12 2018/9/15
 * @param:
 */
let handleTruncation = function(){
  let multiLang = this.props.MutiInit.getIntl(7010);
  promptBox({
      color: 'warning',       // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
      title: multiLang && multiLang.get('701001RWCL-0047'),      //"提示信息"
      content:  multiLang && multiLang.get('701001RWCL-0074'),//此操作会丢失本次修改,请确定?
      beSureBtnClick: ()=>{
              this.refs.nav.linkByClickedIdx();
      }
  })
}



export default { cancelBtnEventConfig, editBtnEventConfig, saveBtnEventConfig, deleteBtnEventConfig, addBtnEventConfig, treeSelectEvent, saveModalBtnEventConfig, cancelModalBtnEventConfig,selectedChange,handleTruncation};
