export default {
 /* addModal: {
    code: "SSCPriority",
    moduletype: "form",
    items: [
      {
        itemtype: "input",
        label: "主键",
        attrcode: "pk_priority",
        maxlength: "20"
      },
      {
        itemtype: "refer",
        label: "集团",
        attrcode: "pk_group",
        maxlength: "20"
      },
      {
        itemtype: "refer",
        label: "所属组织",
        attrcode: "pk_org",
        maxlength: "20"
      },
      {
        itemtype: "input",
        visible: true,
        label: "编码",
        attrcode: "code",
        maxlength: "100",
        required: true
      },
      {
        itemtype: "input",
        visible: true,
        label: "名称",
        attrcode: "name",
        maxlength: "200",
        required: true
      },
      {
        itemtype: "input",
        visible: true,
        label: "优先级条件",
        attrcode: "formula",
        required: true
      },
      {
        itemtype: "input",
        visible: false,
        label: "优先级条件显示名称",
        attrcode: "formulashow",
        required: true
      },
      {
        itemtype: "select",
        visible: true,
        label: "晋级模式",
        attrcode: "upgraderule",
        maxlength: "50",
        required: true,
        options: [
          {
            display: "无",
            value: "none"
          },
          {
            display: "分钟",
            value: "minute"
          },
          {
            display: "小时",
            value: "hour"
          },
          {
            display: "天",
            value: "day"
          },
          {
            display: "周",
            value: "week"
          }
        ]
      },
      {
        itemtype: "number",
        visible: true,
        label: "晋级时间",
        attrcode: "upgraderulenum"
      },
      {
        itemtype: "number",
        visible: false,
        label: "优先级顺序",
        attrcode: "ordernum",
        "scale":"0"
      },
      {
        attrcode: "creator",
        maxlength: "20",
        label: "创建人",
        itemtype: "refer"
      },
      {
        attrcode: "creationtime",
        maxlength: "19",
        label: "创建时间",
        itemtype: "datepicker"
      },
      {
        attrcode: "modifier",
        maxlength: "20",
        label: "最后修改人",
        itemtype: "refer"
      },
      {
        attrcode: "modifiedtime",
        maxlength: "19",
        label: "最后修改时间",
        itemtype: "datepicker"
      },
      {
        itemtype: "datepicker",
        label: "时间戳",
        attrcode: "ts",
        maxlength: "19"
      }
    ],
    name: "优先级设置",
    pagination: false
  },*/
  extendButtons:[
    {
        "id": "0001ZG10000000000X7E",
        "type": "button_main",
        "key": "Edit_list",
        "title": "修改",
        "area": 'listbtnarea',
        "children": []
    },
    {
        "id": "0001ZG10000000000X7K",
        "type": "button_main",
        "key": "Delete_list",
        "title": "删除",
        "area": 'listbtnarea',
        "children": []
    },
    {
        "id": "0001ZG10000000000X7U",
        "type": "button_main",
        "key": "Up",
        "title": "上移",
        "area": 'listbtnarea',
        "children": []
    },
    {
        "id": "0001ZG10000000000X7D",
        "type": "button_main",
        "key": "Down",
        "title": "下移",
        "area": 'listbtnarea',
        "children": []
    },
    {
        "id": "0001ZG10000000000X7T",
        "type": "button_main",
        "key": "SettingTop",
        "title": "置顶",
        "area": 'listbtnarea',
        "children": []
    },
    {
      "id": "0001ZG10000000000X7G",
      "type": "button_main",
      "key": "SettingBottom",
      "title": "置底",
      "area": 'listbtnarea',
      "children": []
    }
]
};
