import requestApi from '../requestApi'
let pageProps = null;
function pageBtnsCtrl(active) {
    switch (active) {
        case 'save' :
        case 'cancel' :
            pageProps.button.setButtonsVisible({
                saveButton: false,
                cancelButton: false,
                editButton: true,
                addButton: true,
                delButton: true
            });
            break;
        default:
            pageProps.button.setButtonsVisible({
                saveButton: true,
                cancelButton: true,
                editButton: false,
                addButton: false,
                delButton: false
            });
            break;

    }
}

export default function buttonClick(props, id) {
    pageProps = props;
    switch (id) {
        case 'editButton':
            props.editTable.setStatus(window.bodyCode, 'edit');
            pageBtnsCtrl(null);
            break;
        case 'addButton':
            pageBtnsCtrl(null);
            props.editTable.addRow(window.bodyCode);
            break;
        case 'delButton':
            let rows = props.table.getCheckedRows(window.bodyCode);
            if (rows.length) {
                props.editTable.delRow(window.bodyCode, rows[0].index);
                let reqData = props.editTable.getAllData(window.bodyCode);
                requestApi.save({
                    data: reqData,
                    success: (data) => {
                        data && this.props.editTable.setTableData(window.bodyCode, data[window.bodyCode]);
                    }
                });
            }
            break;
        case 'saveButton':
            let reqData = props.editTable.getAllData(window.bodyCode);
            requestApi.save({
                data: reqData,
                success: (data) => {
                    pageBtnsCtrl('save');
                    props.editTable.setStatus(window.bodyCode, 'browse');
                    data && this.props.editTable.setTableData(window.bodyCode, data[window.bodyCode]);
                }
            });
            break;
        case 'cancelButton':
            props.editTable.setStatus(window.bodyCode, 'browse');
            pageBtnsCtrl('cancel');
            break;
        default: 
            break;
    }
}

