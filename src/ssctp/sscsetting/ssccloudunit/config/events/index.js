import buttonClick from './buttonClick';
import initTemplate from './initTemplate';
import afterEvent  from './afterEvent';
import tableModelConfirm from './tableModelConfirm';
export { buttonClick, initTemplate, afterEvent, tableModelConfirm};
