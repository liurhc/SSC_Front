import {ajax, base } from 'nc-lightapp-front';
import requestApi from '../requestApi'
let { NCPopconfirm} = base;

export default function (props) {

    let multiLang = props.MutiInit.getIntl(7010); 
    requestApi.tpl({
        data: {
            "pagecode": "ssccloudunit"
        },
        success: (data) => {
            let meta = data.data;
            console.log(1, meta);
            debugger
            meta.ssccloudunit.items.find((item) => item.attrcode === "pk_org").queryCondition = function () {
                    
                return {
                    "pk_group": "0001Z31000000000112T"
                }
            };

            // 添加表格操作列
            let event = {
                label: multiLang && multiLang.get('701001RWCL-0060'),//操作
                itemtype: "customer",
                attrcode: 'opr',
                disabled: true,
                visible: true,
                render(text, record, index) {
                    return  (
                        //701001RWCL-0039：确认删除?
                        <NCPopconfirm  placement="top" content={multiLang && multiLang.get('701001RWCL-0039')} id="delConfirmId" onClose={() => {
                            debugger;
                            props.editTable.delRow(window.bodyCode, index);
                             requestApi.del({
                                 data: record,
                                 success: (res) => {
                                 }
                             })
                        }}>
                        {/*"7010-0002": "删除"*/}
                        <font className="body-btn">{multiLang && multiLang.get('7010-0002')}</font>
                        </NCPopconfirm>
                    )
                }
            };
            meta[window.bodyCode].items.push(event);

            props.meta.setMeta(meta);
        }
    });
}
