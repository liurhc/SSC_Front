
import {ajax } from 'nc-lightapp-front';

let requestDomain =  '';

let requestApiOverwrite = {
    //单据模板接口
    tpl: (opt) => {
        ajax({
            url: opt.url || `${requestDomain}/nccloud/ssctp/sscbd/SSCUnitTempletAction.do`,
            data: opt.data,
            success: opt.success
        });
    },
    query:(opt) => {
        ajax({
            url: `${requestDomain}/nccloud/ssctp/sscbd/SSCUnitQueryAction.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    save: (opt) => {
        opt.data.rows.forEach((row) => {
            delete row.values.ts;
        });
        opt.data = {
            head: opt.data
        }
        ajax({
            url: `${requestDomain}/nccloud/ssccloud/sscbd/SSCUnitAddAction.do`,
            data: opt.data,
            success: (data) => {
                if (data.success) {
                    data = data.data;
                    opt.success(data);
                }
            }
        })
    }    
}
export default  requestApiOverwrite;