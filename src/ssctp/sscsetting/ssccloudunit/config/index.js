import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {createPage, ajax} from 'nc-lightapp-front';
import {buttonClick, initTemplate, afterEvent, tableModelConfirm} from './events';

import requestApi from './requestApi'
import './index.less';

window.bodyCode = 'ssccloudunit';

class SSCCloudUnit extends Component {

    componentDidMount() {

        requestApi.query({
            data: {
                "orgflag":"org",
                "pk_org":"0001Z310000000001145"
            },
            success: (data) => {
                //数据加载后处理，要把启用状态适配成开关状态
                data[window.bodyCode].rows.forEach((row) => {
                    if(row.values.enablestate.value === 3){
                        row.values.enablestate = {value:false};
                    }else{
                        row.values.enablestate = {value:true};
                    }
                });
                this.props.editTable.setTableData(window.bodyCode, data[window.bodyCode]);
            }
        });
        this.props.button.setButtonsVisible({
            saveButton: false,
            cancelButton: false
        })
    }


    render() {
        const {editTable, table, button} = this.props;
        const {createEditTable} = editTable;
        const {createButton} = button;
        let multiLang = this.props.MutiInit.getIntl(7010);
        return (
            <div className="container">
                <div className="title"> {multiLang && multiLang.get('701001RWCL-0045')} </div>
                <div className="header">
                    {createButton('addButton', {
                        name: multiLang && multiLang.get('701001RWCL-0046'),//新增
                        onButtonClick: buttonClick.bind(this),
                        buttonColor: 'main-button'
                    })}
                    {createButton('editButton', {
                        name: multiLang && multiLang.get('701001RWCL-0038'),//修改
                        onButtonClick: buttonClick.bind(this),
                        buttonColor: 'main-button'
                    })}
{/*                     {createButton('delButton', {
                        name: '删除',
                        onButtonClick: buttonClick.bind(this),
                        buttonColor: 'main-button'
                    })} */}
                    {createButton('saveButton', {
                        name: multiLang && multiLang.get('7010-0003'),//保存
                        onButtonClick: buttonClick.bind(this),
                        buttonColor: 'main-button'
                    })}
                    {createButton('cancelButton', {
                        name: multiLang && multiLang.get('7010-0004'),//取消
                        onButtonClick: buttonClick.bind(this)
                    })}
                </div>
                <div className="content">
                    {createEditTable(window.bodyCode, {
                        onAfterEvent: afterEvent
                    })}
                </div>
            </div>
        )
    }
}

SSCCloudUnit = createPage({
    initTemplate: initTemplate,
    mutiLangCode: '7010'
})(SSCCloudUnit);

ReactDOM.render(<SSCCloudUnit/>, document.querySelector('#app'));