import { ajax } from 'nc-lightapp-front';

let requestApi = {
    // 新增树节点
    addTreeData: (opt) => {
        ajax({
            url: '/nccloud/sscbd/workinggroup/InsertAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    // 删除树节点
    delTreeData: (opt) => {
        ajax({
            url: '/nccloud/sscbd/workinggroup/DeleteAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    // 修改树节点
    updateTreeData: (opt) => {
        ajax({
            url: '/nccloud/sscbd/workinggroup/UpdateAction.do',
            data: opt.data,
            success: (data) => {
                console.log(data);
                data = data.data;
                opt.success(data);
            }
        })
    },
    // 启用树节点
    enableTreeData: (opt) => {
        ajax({
            url: '/nccloud/sscbd/workinggroup/EnableAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
     // 停用树节点
     disableTreeData: (opt) => {
        ajax({
            url: '/nccloud/sscbd/workinggroup/DisableAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    // 查询树节点
    queryTreeData: (opt) => {
        ajax({
            url: '/nccloud/sscbd/workinggroup/QueryAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    // 重置任务
    resetTask: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCTaskResetNotakeTaskAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    // 保存规则
    saveFormData: (opt) => {
        ajax({
            url: '/nccloud/sscbd/allotrule/InsertAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    // 删除规则
    delFormData: (opt) => {
        ajax({
            url: '/nccloud/sscbd/allotrule/DeleteAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    // 修改规则
    updateFormData: (opt) => {
        ajax({
            url: '/nccloud/sscbd/allotrule/UpdateAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    // 查询规则
    queryFormListData: (opt) => {
        ajax({
            url: '/nccloud/sscbd/allotrule/QueryAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    // 查询父级作业组规则
    queryParentFormListData: (opt) => {
        ajax({
            url: '/nccloud/sscbd/allotrule/QueryAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    // 保存上级作业组规则
    copyParentRule: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/CopyParentRulesAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    // 启用/停用规则
    changeEnableState: (opt) => {
        ajax({
            url: '/nccloud/sscbd/allotrule/changeEnableStateAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            },
        });
    },
    // 查询交易类型/单据类型详细
    queryDetailTableData: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/BillTypeQueryAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            },
        });
    },
    // 查询单位范围详细
    queryGroupDetailTableData: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/OrgsQueryAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            },
        });
    },
    queryPublicFields: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/QueryPublicFieldAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    // 校验父级作业组规则是否能CRUD
    checkParentHandleEnable: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/CheckEnableAddRuleAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    }
}
export default requestApi;