import React, { Component } from "react";
import { createPage, base, high, createPageIcon } from "nc-lightapp-front";
import {
  initTemplate,
  formEveManager,
  treeEveManager,
  query,
  afterEvent
} from "./events";
import requestApi from "./requestApi";
import SSCCenter from "../../../refer/sscbd/SSCUnitGridRef";
import { Nav } from "ssccommon/components/global-nav";
import gxicon1 from "../../../public/image/gxicon1.png";
import gxicon2 from "../../../public/image/gxicon2.png";
import gxicon3 from "../../../public/image/gxicon3.png";
import gxicon4 from "../../../public/image/gxicon4.png";
import gxicon5 from "../../../public/image/gxicon5.png";
import "./index.less";
const {
  NCModal,
  NCButton,
  NCSwitch,
  NCFormControl,
  NCCheckbox,
  NCPopconfirm,
  NCDiv: Div,
  NCTableSwitch
} = base;

const { NCTooltip } = base;

const { FormulaEditor } = high;

function PublicField({ setName, setExplain, name }) {
  let rowList = [];
  name.map(item => {
    rowList.push(
      <li
        onClick={() => {
          setExplain("");
        }}
        onDoubleClick={() => {
          setName("" + item.code + "");
        }}
      >
        {item.code}
        &nbsp;&nbsp;
        {item.name}
      </li>
    );
  });
  return <ul className="formula-publicfiled-content">{rowList}</ul>;
}
class WorkGroup extends Component {
  constructor(props) {
    super(props);
    this.treeId = "tree"; // 树组件模板
    this.treeFormId = "treeForm"; // 树节点新增/修改模板
    this.formId = "listForm"; // 表单模板
    this.groupTableId = "orglist"; // 单位范围表格模板
    this.tranctionTableId = "tranctionlist"; // 交易类型表格模板
    this.billtypeTableId = "billtypelist"; // 单据类型表格模板
    this.formModalId = "formModal"; // 表单modal
    this.treeModalId = "treeModal"; // 树表单modal
    this.detailModalId = "detailModal"; // 详情modal
    this.isRoot = false; // 判断虚拟工作组使用
    this.formulaInputValue = ""; // 公式内容
    // this.isShowPop = false;             // 控制拉取上级规则确认
    this.state = {
      SSCCenter: {}, // 共享中心
      disabledBtn: true, // 控制按钮是否禁用 true 禁用,false 启用
      showFormModal: false, // 新增/编辑表单模态显示
      showFormula: false, // 公式编辑器显示/隐藏
      publicFieldData: "", // 公式编辑器-公共字段内容
      showTreeModal: false, // 新增/编辑树模态显示
      showTreeSearch: true, // 控制树查询区显示
      showDetailModal: false, // 详细表格显示 [单位范围,交易类型,单据类型]
      showDisabled: false, // 显示停用
      isShowPop: false, // 控制拉取上级规则确认
      modalType: {}, // 控制显示全部模态内容显示  控制显示 [单位范围,交易类型,单据类型]
      detailTableData: [], // 详细信息的数据
      formListData: [] // 表单集合数据
    };
    initTemplate.call(this, props);
  }

  // 渲染表单集合
  drawFormList = () => {
    let multiLang = this.props.MutiInit.getIntl(7010); 
    let formList = [];
    let imgList = [gxicon1, gxicon2, gxicon3, gxicon4, gxicon5];
    this.state.formListData.map((item, index) => {
      let billTypeArr = item.values.billtype.display.split(",");
      let displayArr = billTypeArr.slice(0, 3); // 单据类型默认显示3个
      let billTypeStr = displayArr.join("  ;  ");
      let tranStr = "";
      if (item.values.tranction.display == "") {
        tranStr = "";
      } else {
        let tranctionArr = item.values.tranction.display.split(",");
        let displayTranArr = tranctionArr.slice(0, 8); // 交易类型默认显示8个
        tranStr = displayTranArr.join("  ;  ");
      }
      let groupArr = item.values.group.display.split(",");
      let displayGroupArr = groupArr.slice(0, 8); // 单位范围默认显示8个
      let groupStr = displayGroupArr.join("  ;  ");
      formList.push(
        <div className="ssctp-workinggroup-form">
          <div className="head-grow">
            <img className="img" src={imgList[index % 5]} />
            {/*"701001RWCL-0106": "规则名称"*/}
            <span className="name-area">
              {index + 1}
              、{multiLang && multiLang.get('701001RWCL-0110')}:&nbsp;
              {item.values.name.value}
            </span>
            <span className="operate-area">
              <span className="btn">
                {/*"701001RWCL-0105": "确定停用作业组规则?","701001RWCL-0106": "确定启用作业组规则?"*/}
                <NCPopconfirm
                  trigger="click"
                  placement="bottom"
                  content={
                    item.values.enablestate.value
                      ? multiLang && multiLang.get('701001RWCL-0105')
                      : multiLang && multiLang.get('701001RWCL-0106')
                  }
                  onClose={formEveManager.tableSwitchChange.bind(
                    this,
                    item.values.pk_allotrule.value,
                    !item.values.enablestate.value
                  )}
                >
                  <span>
                    <NCTableSwitch
                      {...this.props}
                      checked={item.values.enablestate.value}
                      /* {...{
                        onChange: formEveManager.tableSwitchChange.bind(
                          this,
                          item.values.pk_allotrule.value
                        )
                      }} */
                    />
                  </span>
                </NCPopconfirm>
                
              </span>
              <sapn
                className="btn"
                onClick={formEveManager.editFormButtonClick.bind(
                  this,
                  item.values.pk_allotrule.value
                )}
              >
                {multiLang && multiLang.get('701001RWCL-0038')}{/*"701001RWCL-0038": "修改""*/}
              </sapn>
              <sapn>
                <NCPopconfirm
                  placement="bottom"
                  content={multiLang && multiLang.get('701001RWCL-0039')}//701001RWCL-0039：确认删除?
                  id="delConfirmId"
                  onClose={formEveManager.delFormButtonClick.bind(
                    this,
                    item.values.pk_allotrule.value
                  )}
                >
                  <span className="btn">
                    {multiLang && multiLang.get('7010-0002')}{/*"7010-0002": "删除""*/}
                  </span>
                </NCPopconfirm>
              </sapn>
            </span>
          </div>
          <div className="grow">
            <span className="column-2-1">
              <span className="label-name">{multiLang && multiLang.get('701001RWCL-0050')}</span>
              <span className="label-single-value">
                {item.values.pk_sscnode.display}
              </span>
            </span>
            {/*"701001RWCL-0104": "单据类型","701001RWCL-0111":"【查看全部】"*/}
            <span className="column-2-2">
              <span className="label-name">{multiLang && multiLang.get('701001RWCL-0104')}:</span>
              <span className="label-value">{billTypeStr}</span>
              <span
                className="label-value-all"
                onClick={query.queryBillTypeDetails.bind(
                  this,
                  item,
                  this.billtypeTableId
                )}
              >
                {multiLang && multiLang.get('701001RWCL-0111')}
              </span>
            </span>
          </div>
          {/*"701001RWCL-0103": "交易类型"*/}
          <div className="grow">
            <sapn className="column-1-1">
              <span className="label-name">{multiLang && multiLang.get('701001RWCL-0103')}:</span>
              <span
                className={tranStr == "" ? "label-single-value" : "label-value"}
              >
                {tranStr}
              </span>
              {tranStr == "" ? (
                ""
              ) : (
                <span
                  className="label-value-all"
                  onClick={query.queryBillTypeDetails.bind(
                    this,
                    item,
                    this.tranctionTableId
                  )}
                >
                  {multiLang && multiLang.get('701001RWCL-0111')}
                </span>
              )}
            </sapn>
          </div>
          {/*"701001RWCL-0102": "单位范围"*/}
          <div className="grow">
            <sapn className="column-1-1">
              <span className="label-name">{multiLang && multiLang.get('701001RWCL-0102')}:</span>
              <span
                className={
                  groupStr == "" ? "label-single-value" : "label-value"
                }
              >
                {groupStr}
              </span>
              {groupStr == "" ? (
                ""
              ) : (
                <span
                  className="label-value-all"
                  onClick={query.queryGroupDetails.bind(this, item)}
                >
                  {multiLang && multiLang.get('701001RWCL-0111')}
                </span>
              )}
            </sapn>
          </div>
          <div className="grow">
            <sapn className="column-2-1">
              {/*"701001RWCL-0112": "更多范围"*/}
              <span className="label-name">{multiLang && multiLang.get('701001RWCL-0112')}:</span>
                {/* {item.values.formula.value} */}
                <NCTooltip inverse="true"  placement="top" overlay={item.values.formula.value}>
                  <span className="label-single-value">
                    {item.values.formula.value}
                  </span>
                </NCTooltip>
            </sapn>
            <span className="column-2-2">
              {/*"701001RWCL-0113": "包含下级单位"*/}
              <span className="label-name">{multiLang && multiLang.get('701001RWCL-0113')}:</span>
              <span className="label-single-value">
                {item.values.iscontainslower.display}
              </span>
            </span>
          </div>
        </div>
      );
    });
    return formList;
  };
  // 渲染模态详情
  drawDetailModelContent = () => {
    let { createEditTable } = this.props.editTable;
    switch (this.state.modalType.value) {
      case this.tranctionTableId:
        return createEditTable(this.tranctionTableId, {
          showIndex: true
        });
      case this.groupTableId:
        return createEditTable(this.groupTableId, {
          showIndex: true
        });
      case this.billtypeTableId:
        return createEditTable(this.billtypeTableId, {
          showIndex: true
        });
    }
  };
  // 关闭指定modal
  closeModal(key) {
    switch (key) {
      case this.treeModalId:
        this.setState({ showTreeModal: false });
        break;
      case this.detailModalId:
        this.setState({ showDetailModal: false });
        break;
      case this.formModalId:
        this.setState({ showFormModal: false });
        break;
      default:
        break;
    }
  }

  openFormula() {
    // 查询
    requestApi.queryPublicFields({
      data: "",
      success: res => {
        console.log("公共字段数据", res);
        this.setState({ publicFieldData: res, showFormula: true });
      }
    });
  }
  render() {
    let { syncTree, form, DragWidthCom } = this.props;
    let { createSyncTree } = syncTree;
    let { createForm } = form;
    let formLists = this.drawFormList();
    let detailModalContent = this.drawDetailModelContent();
    let multiLang = this.props.MutiInit.getIntl(7010);
    return (
      <div>
        {/* 导航条*/}
        <Nav defaultActIdx={"2"} {...this.props} />
        <Div areaCode={Div.config.HEADER}>
          {/* 头部 header*/}
          <div className="header">
            {/* 标题 title*/}
            {/* '701001RWCL-0114':'配置作业组工作'*/}
            {createPageIcon()}
            <div className="title">{multiLang && multiLang.get('701001RWCL-0114')}</div>
            <div className="search-box">
              <span class="ssc-must">*</span>
              {SSCCenter({
                onChange: query.onSSCCenterChange.bind(this),
                value: this.state.SSCCenter
              })}
            </div>
            <div className="showOff">
              <NCCheckbox
                onChange={query.showDisabledChange.bind(this)}
                checked={this.state.showDisabled}
                // disabled = { this.state.disabledBtn }
              >
              {/*"701001RWCL-0070": "显示停用"*/}
                {multiLang && multiLang.get('701001RWCL-0070')}
              </NCCheckbox>
            </div>
            {/* 按钮组 btn-group*/}
            <div className="btn-group">
              {this.state.isShowPop && this.state.formListData.length == 0 ? (
                <NCPopconfirm
                  placement="bottom"
                  content={multiLang && multiLang.get('701001RWCL-0107')}
                  id="addConfirmId"
                  onClose={formEveManager.saveParentRulesClick.bind(this)}
                  onCancel={formEveManager.addFormButtonClick.bind(this)}
                >
                {/*"701001RWCL-0046": "新增"*/}
                  <NCButton colors="primary" disabled={this.state.disabledBtn}>
                  {multiLang && multiLang.get('701001RWCL-0046')}
                  </NCButton>
                </NCPopconfirm>
              ) : (
                <NCButton
                  colors="primary"
                  onClick={formEveManager.addFormButtonClick.bind(this)}
                  disabled={this.state.disabledBtn}
                >
                  {multiLang && multiLang.get('701001RWCL-0046')}
                </NCButton>
              )}
              {/*"701001RWCL-0077": "重置任务"*/}
              <NCButton
                onClick={formEveManager.resetButtonClick.bind(this)}
                disabled={this.state.disabledBtn}
              >
                {multiLang && multiLang.get('701001RWCL-0077')}
              </NCButton>
            </div>
          </div>
        </Div>
        {/* 树卡区域 */}
        <div className="tree-card">
          <DragWidthCom
            // 左树区域
            leftDom={
              <div className="tree-area">
                {createSyncTree({
                  treeId: this.treeId,
                  selectedForInit: true,
                  needSearch: this.state.showTreeSearch,
                  showLine: true,
                  needEdit: true, //是否需要编辑节点功能，默认为true,可编辑；false：不可编辑
                  defaultExpandAll: true, //初始化展开所有节点,默认参数为false,不展开
                  showModal: false, //是否使用弹出式编辑
                  onSelectEve: treeEveManager.onSelectEve.bind(this), //选择节点回调方法
                  clickAddIconEve: treeEveManager.clickAddIconEve.bind(this),
                  clickEditIconEve: treeEveManager.clickEditIconEve.bind(this),
                  clickDelIconEve: treeEveManager.clickDelIconEve.bind(this),
                  clickStopIconEve: treeEveManager.clickStopIconEve.bind(this),//点击停用按钮
                  clickUpIconEve: treeEveManager.clickUpIconEve.bind(this)//点击启用按钮
                })}
              </div>
            } //左侧区域dom
            // 右卡片区域
            rightDom={<div className="card-area">{formLists}</div>} //右侧区域dom
            defLeftWid="20%" // 默认左侧区域宽度，px/百分百
          />
        </div>
        <div>
          {/*新增/修改树节点modal*/}
          <NCModal show={this.state.showTreeModal} onHide={()=>{this.setState({showTreeModal:false})}} className="junior">
            <NCModal.Header closeButton={true}>
              <NCModal.Title>{multiLang && multiLang.get('701001RWCL-0051')}</NCModal.Title>
            </NCModal.Header>
            <NCModal.Body><div className="ssc_edit_modal_form">{createForm(this.treeFormId)}</div></NCModal.Body>
            <NCModal.Footer>
              <div class="buttonGroup-component">
                <div class="u-button-group">
                  <NCButton
                    colors="primary"
                    onClick={treeEveManager.saveOrUpdateTreeButton.bind(this)}
                  >
                    {/*"7010-0003": "保存"*/}
                    {multiLang && multiLang.get('7010-0003')}
                  </NCButton>
                  {/*"701001RWCL-0115": "保存新增"*/}
                  <NCButton
                    onClick={treeEveManager.saveAndAddTreeButton.bind(this)}
                  >
                    {multiLang && multiLang.get('701001RWCL-0115')}
                  </NCButton>
                </div>
              </div>
              <NCButton onClick={treeEveManager.cancelTreeButton.bind(this)}>
              {multiLang && multiLang.get('7010-0004')}
              </NCButton>
            </NCModal.Footer>
          </NCModal>
          {/*新增/修改表单modal*/}
          <NCModal
            show={this.state.showFormModal}
            className="senior"
            onEntered={() =>
              (this.formulaInputId == undefined
                ? ""
                : this.formulaInputId.value)
            }
            onHide={()=>{this.setState({showFormModal:false})}}
            //backdrop={false}
            backdrop='false'
          >
            {/*'701001RWCL-0116'：'新增作业组规则'*/}
            <NCModal.Header closeButton={true}>
              <NCModal.Title>{multiLang && multiLang.get('701001RWCL-0116')}</NCModal.Title>
            </NCModal.Header>
            <NCModal.Body>
            <div className="ssc_edit_modal_form">
              {createForm(this.formId, {
                //编辑后事件
                onAfterEvent: afterEvent.bind(this)
              })}
              <div>
                <FormulaEditor
                  ref="formulaEditor"
                  value={
                    this.formulaInputId == undefined
                      ? ""
                      : this.formulaInputId.value
                  }
                  show={this.state.showFormula}
                  attrConfig={[
                    {
                      tab: multiLang && multiLang.get('701001RWCL-0096'),//701001RWCL-0096：公共字段
                      TabPaneContent: PublicField,
                      params: { name: this.state.publicFieldData }
                    }
                  ]}
                  //701001RWCL-0097：表和字段，701001RWCL-0098：元数据属性
                  noShowAttr={[multiLang && multiLang.get('701001RWCL-0097'),multiLang && multiLang.get('701001RWCL-0098')]}
                  // formulaConfig={[{tab:'常用', TabPaneContent: Example, params: {}}]}
                  onOk={value => {
                    this.setState({ showFormula: false });
                    this.formulaInputId.value = value;
                  }} //点击确定回调
                  onCancel={a => {
                    this.setState({ showFormula: false });
                  }} //点击确定回调
                  metaParam={{
                    pk_billtype: "",
                    bizmodelStyle: "fip",
                    classid: "7bd63d9b-f394-4a7d-a52c-72f8274d471f"
                  }}
                  onHide={() => {
                    this.setState({ showFormula: false });
                  }}
                />
              </div>
              </div>
            </NCModal.Body>
            <NCModal.Footer>
              <NCButton
                colors="primary"
                onClick={formEveManager.saveFormButtonClick.bind(this)}
              >
                {/*"7010-0003": "保存"*/}
                {multiLang && multiLang.get('7010-0003')}
              </NCButton>
              <NCButton
                onClick={formEveManager.cancelFormButtonClick.bind(this)}
              >
                {multiLang && multiLang.get('7010-0004')}
              </NCButton>
            </NCModal.Footer>
          </NCModal>
          {/*显示全部modal*/}
          <NCModal
            className="senior"
            id='workgroup-detail-modal'
            show={this.state.showDetailModal}
            onHide={e => this.closeModal(this.detailModalId)}
          >
            <NCModal.Header closeButton={true}>
              <NCModal.Title>
                {this.state.modalType.display}
                <span style={{ display: "inline-block", marginLeft: "8px" }}>
                  {/*701001RWCL-0109：搜索名称*/}
                  <NCFormControl
                    placeholder={multiLang && multiLang.get('701001RWCL-0109')}
                    value={this.state.searchValue}
                    onChange={searchValue => {
                      this.setState({ searchValue: searchValue });
                    }}
                    onSearch={query.queryDetailTable.bind(this)}
                    type="search"
                  />
                </span>
              </NCModal.Title>
            </NCModal.Header>
            <NCModal.Body>
              {detailModalContent}
            </NCModal.Body>
          </NCModal>
        </div>
      </div>
    );
  }
}

let WorkGroupDom = createPage({
  mutiLangCode: '7010'
})(WorkGroup);
export default WorkGroupDom;
