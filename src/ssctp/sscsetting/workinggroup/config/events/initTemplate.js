import { getMultiLang } from 'nc-lightapp-front';

export default function (props) {

    const _this = this
    props.createUIDom({},
        (data) => {
            getMultiLang({
                moduleId: 7010, domainName: 'ssctp', currentLocale: 'zh-CN', callback: (json) => {
                    console.log(data.template);
                    data.template.tranctionlist.items.map((item) => {
                        if (item.attrcode == 'pk_billtypecode') {
                            item.itemtype = 'label'
                        }
                    })
                    data.template.listForm.items.map((item) => {
                        if (item.attrcode == 'pk_sscnode') {
                            //   item.refcode = '/ssctp/refer/sscbd/SSCNodeGridRef/index.js'
                        } else if (item.attrcode == 'billtype') {
                            item.refcode = '/uap/refer/riart/billtype/index.js'
                        } else if (item.attrcode == 'tranction') {
                            //   item.refcode = '/uap/refer/riart/transtype/index.js'
                        } else if (item.attrcode == 'group') {
                            item.refcode = 'uapbd/refer/org/BusinessUnitTreeRef/index.js'
                            item.isShowUnit = true;
                            item.isRunWithChildren = false;
                            item.unitProps = {
                                placeholder: json['701001RWCL-0078'],//701001RWCL-0078：
                                refName: json['701001RWCL-0078'],
                                refType: "tree",
                                refCode: "uapbd.org.GroupDefaultTreeRef",
                                queryTreeUrl: "/nccloud/uapbd/ref/GroupDefaultTreeRef.do",
                                rootNode: { refname: json['701001RWCL-0078'], refpk: "root" },
                            }
                        }
                    })

                    let BilltypeRefSqlBuilder = 'nccloud.web.ssctp.sscbd.ref.sqlbuilder.BilltypeRefSqlBuilder';
                    let TranstypeRefSqlBuilder = 'nccloud.web.ssctp.sscbd.ref.sqlbuilder.TranstypeRefSqlBuilder';
                    let BusinessUnitRefSqlBuilder = 'nccloud.web.ssctp.sscbd.ref.sqlbuilder.BusinessUnitRefSqlBuilder'

                    data.template.listForm.items.find((item) => {

                        if (item.attrcode === "billtype") {
                            item.queryCondition = function () {
                                return { GridRefActionExt: BilltypeRefSqlBuilder };
                            }
                        } else if (item.attrcode === "tranction") {
                            item.queryCondition = function () {
                                let billtype = props.form.getFormItemsValue('listForm', 'billtype');
                                let data = billtype == null ? "" : billtype.value;
                                return { billtype: data, GridRefActionExt: TranstypeRefSqlBuilder };
                            }
                        } else if (item.attrcode === "group") {
                            item.queryCondition = function () {
                                let sscunit = _this.state.SSCCenter;
                                let data = sscunit.refpk == null ? "" : sscunit.refpk;
                                return { sscunit: data, TreeRefActionExt: BusinessUnitRefSqlBuilder };
                            }
                        }

                    });
                    data.template['listForm'].status = 'edit';
                    data.template['treeForm'].status = 'edit';
                    props.meta.setMeta(data.template);
                    // props.meta.setMeta(meta);

                    let FormulaComponent = (props) => {
                        return (
                            <div className="formula-div">
                                <input
                                    type="text"
                                    value={props.title}
                                    ref={ref => {
                                        this.formulaInputId = ref;
                                    }}
                                    className="formula-input"
                                />
                                <span
                                    className="icon-refer"
                                    onClick={() => {
                                        this.openFormula();
                                    }}
                                    style={{ cursor: "pointer" }}
                                />
                            </div>
                        )
                    }

                    props.renderItem(
                        "form",
                        "listForm",
                        "formula",
                        <FormulaComponent />
                    )

                }
            })
        })
}
