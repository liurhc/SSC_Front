import { base, promptBox, toast } from "nc-lightapp-front";
import requestApi from "./../requestApi";

const { NCMessage } = base;

/**
 * @Author:gaoymf
 * @Description:保存/更新按钮点击
 * @Date: 16:13 2018/6/11
 * @param:
 */
let saveFormButtonClick = function() {
  let formListData = this.props.form.getAllFormValue(this.formId);
  let multiLang = this.props.MutiInit.getIntl(7010); 
  formListData.rows[0].values.formula.value = this.formulaInputId == undefined? "": this.formulaInputId.value;
  delete formListData.rows[0].status;
  let editData = this.state.formListData.filter(formData => {
    return (
      formData.values.pk_allotrule.value ===
      formListData.rows[0].values.pk_allotrule.value
    );
  });
  if (this.props.form.isCheckNow(this.formId)) {
    // 发送请求保存
    if (
      formListData.rows[0].values.pk_allotrule.value !== undefined &&
      formListData.rows[0].values.pk_allotrule.value !== null
    ) {
      //console.log("修改规则-保存", formListData.rows[0]);
      // 模板中没有childrenVO 单独处理childrenVO,childrenVO做为后端ts校验用
      formListData.rows[0].values.childrenVO = editData[0].values.childrenVO;
      formListData.rows[0].values.parentVO = editData[0].values.parentVO;
      requestApi.updateFormData({
        data: formListData.rows[0],
        success: res => {
          toast({
            color: "success",
            content: multiLang && multiLang.get('701001RWCL-0118')
          });
          let selectNode = this.props.syncTree.getSelectNode(this.treeId);
          let req = {
            pk_workinggroup: selectNode.refpk,
            enablestate: this.state.showDisabled
          };
          queryFormListData.call(this, req);
          this.closeModal(this.formModalId);
        }
      });
    } else {
      // 特殊处理 ts数据
      delete formListData.rows[0].values.childrenVO;
      delete formListData.rows[0].values.parentVO,
       // console.log("新增规则-保存", formListData.rows[0]);
      requestApi.saveFormData({
        data: formListData.rows[0],
        success: res => {
          toast({
            color: "success",
            content: multiLang && multiLang.get('701001RWCL-0118')
          });
          let selectNode = this.props.syncTree.getSelectNode(this.treeId);
          let req = {
            pk_workinggroup: selectNode.refpk,
            enablestate: this.state.showDisabled
          };
          queryFormListData.call(this, req);
          this.closeModal(this.formModalId);
        }
      });
    }
  }
};

/**
 * @Author:gaoymf
 * @Description:取消按钮点击
 * @Date: 16:15 2018/6/11
 * @param:
 */
let cancelFormButtonClick = function() {
  this.props.form.EmptyAllFormValue(this.formId);
  this.formulaInputValue = "";
  this.closeModal(this.formModalId);
};

/**
 * @Author:gaoymf
 * @Description:任务重置
 * @Date: 15:59 2018/6/12
 * @param:
 */
let resetButtonClick = function() {
  let multiLang = this.props.MutiInit.getIntl(7010);
  promptBox({
    color: "warning", // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
    title: multiLang && multiLang.get('701001RWCL-0077'), //"重置任务"
    content: multiLang && multiLang.get('701001RWCL-0073'),//该操作会重置任务,请确定?
    beSureBtnClick: () => {
      requestApi.resetTask({
        data: {
          pk_sscunit: this.state.SSCCenter.refpk
        },
        success: res => {
			toast({title: multiLang && multiLang.get('701001RWCL-0162'), content: (multiLang && multiLang.get('701001RWCL-0163'))+res.resetnum+(multiLang && multiLang.get('701001RWCL-0076'))});//701001RWCL-0162:重置完成，701001RWCL-0163：已重置,701001RWCL-0076:条任务
        }
      });
    }
  });
};

/**
 * @Author:gaoymf
 * @Description:新增
 * @Date: 14:59 2018/6/12
 * @param:
 */
let addFormButtonClick = function() {
  let selectNode = this.props.syncTree.getSelectNode(this.treeId);
  let multiLang = this.props.MutiInit.getIntl(7010); 
  if (selectNode.isleaf) {
    // 末级节点可以直接修改
    // 清空表单
    this.props.form.EmptyAllFormValue(this.formId);
    // 初始化共享中心pk
    this.props.form.setFormItemsValue(this.formId, {
      pk_sscunit: { value: this.state.SSCCenter.refpk, display: "" }
    });
    // 初始化工作组的pk
    this.props.form.setFormItemsValue(this.formId, {
      pk_workinggroup: { value: selectNode.refpk, display: "" }
    });
    // 判断选中节点是父节点还是子节点  父节点共享环节非必填  子节点共享环节必填
    if (selectNode.isleaf) {
      // 末级作业组的作业组规则共享环节是必填
      this.props.form.setFormItemsRequired(this.formId, { pk_sscnode: true });
    } else {
      // 非末级作业组的作业组规则共享环节非必填
      this.props.form.setFormItemsRequired(this.formId, { pk_sscnode: false });
    }
    // 初始化公式字段
    this.formulaInputValue = "";
    this.setState({ showFormModal: true });
  } else {
    // 父级节点校验
    requestApi.checkParentHandleEnable({
      data: { pk_workinggroup: selectNode.refpk },
      success: res => {
        if (!res) {
          // 如果下级作业组有规则就不在允许修改规则
          //701001RWCL-0101：已存在下级作业组规则,不允许修改规则
          toast({
            color: "danger",
            content: multiLang && multiLang.get('701001RWCL-0101')
          });
        } else {
          // 清空表单
          this.props.form.EmptyAllFormValue(this.formId);
          // 初始化共享中心pk
          this.props.form.setFormItemsValue(this.formId, {
            pk_sscunit: { value: this.state.SSCCenter.refpk, display: "" }
          });
          // 初始化工作组的pk
          this.props.form.setFormItemsValue(this.formId, {
            pk_workinggroup: { value: selectNode.refpk, display: "" }
          });
          // 判断选中节点是父节点还是子节点  父节点共享环节非必填  子节点共享环节必填
          if (selectNode.isleaf) {
            // 末级作业组的作业组规则共享环节是必填
            this.props.form.setFormItemsRequired(this.formId, {
              pk_sscnode: true
            });
          } else {
            // 非末级作业组的作业组规则共享环节非必填
            this.props.form.setFormItemsRequired(this.formId, {
              pk_sscnode: false
            });
          }
          // 初始化公式字段
          this.formulaInputValue = "";
          this.setState({ showFormModal: true });
        }
      }
    });
  }
};
/**
 * @Author:gaoymf
 * @Description:保存上级
 * @Date: 18:58 2018/6/12
 * @param:
 */
let saveParentRulesClick = function() {
  let selectNode = this.props.syncTree.getSelectNode(this.treeId);
  requestApi.copyParentRule({
    data: selectNode,
    success: res => {
      //console.log("同步上级作业组规则返回", res);
      this.setState({ formListData: res });
    }
  });
};

/**
 * @Author:gaoymf
 * @Description:删除点击
 * @Date: 18:58 2018/6/12
 * @param:
 */
let delFormButtonClick = function(pk) {
  let selectNode = this.props.syncTree.getSelectNode(this.treeId);
  let multiLang = this.props.MutiInit.getIntl(7010); 
  if (selectNode.isleaf) {
    var newData = this.state.formListData.filter(item => {
      return pk == item.values.pk_allotrule.value;
    });
    requestApi.delFormData({
      data: newData[0],
      success: res => {
        let selectNode = this.props.syncTree.getSelectNode(this.treeId);
        let req = {
          pk_workinggroup: selectNode.refpk,
          enablestate: this.state.showDisabled
        };
        queryFormListData.call(this, req);
        NCMessage.create({
          content: multiLang && multiLang.get('701001RWCL-0042'),//删除成功
          color: "success",
          position: "bottomRight"
        });
      }
    });
  } else {
    // 父级节点校验
    requestApi.checkParentHandleEnable({
      data: { pk_workinggroup: selectNode.refpk },
      success: res => {
        if (!res) {
          // 如果下级作业组有规则就不在允许修改规则
          //701001RWCL-0101：已存在下级作业组规则,不允许修改规则
          toast({
            color: "danger",
            content: multiLang && multiLang.get('701001RWCL-0101')
          });
        } else {
          var newData = this.state.formListData.filter(item => {
            return pk == item.values.pk_allotrule.value;
          });
          requestApi.delFormData({
            data: newData[0],
            success: res => {
              let selectNode = this.props.syncTree.getSelectNode(this.treeId);
              let req = {
                pk_workinggroup: selectNode.refpk,
                enablestate: this.state.showDisabled
              };
              queryFormListData.call(this, req);
              NCMessage.create({
                content: multiLang && multiLang.get('701001RWCL-0042'),//删除成功
                color: "success",
                position: "bottomRight"
              });
            }
          });
        }
      }
    });
  }
};

/**
 * @Author:gaoymf
 * @Description:修改点击
 * @Date: 19:00 2018/6/12
 * @param:
 */
let editFormButtonClick = function(pk) {
  let selectNode = this.props.syncTree.getSelectNode(this.treeId);
  if (selectNode.isleaf) {
    this.setState({ showFormModal: true });
    let editFormItem = this.state.formListData.filter(item => {
      return pk == item.values.pk_allotrule.value;
    });
    // 公式编辑器单独复制
    this.formulaInputValue = editFormItem[0].values.formula.value;
    this.props.form.setAllFormValue({
      [this.formId]: { rows: JSON.parse(JSON.stringify(editFormItem)) }
    });
    this.props.form.setFormItemsValue(this.formId,{'formula':{'value':this.formulaInputValue,'display':this.formulaInputValue}});
  } else {
    // 父级节点校验
    requestApi.checkParentHandleEnable({
      data: { pk_workinggroup: selectNode.refpk },
      success: res => {
        if (!res) {
          // 如果下级作业组有规则就不在允许修改规则
          //701001RWCL-0101：已存在下级作业组规则,不允许修改规则
          toast({
            color: "danger",
            content: multiLang && multiLang.get('701001RWCL-0101')
          });
        } else {
          this.setState({ showFormModal: true });
          let editFormItem = this.state.formListData.filter(item => {
            return pk == item.values.pk_allotrule.value;
          });
          // 公式编辑器单独复制
          this.formulaInputValue = editFormItem[0].values.formula.value;
          this.props.form.setAllFormValue({
            [this.formId]: { rows: JSON.parse(JSON.stringify(editFormItem)) }
          });
          this.props.form.setFormItemsValue(this.formId,{'formula':{'value':this.formulaInputValue,'display':this.formulaInputValue}});
        }
      }
    });
  }
};

// 查询表单列表
function queryFormListData(req) {
  requestApi.queryFormListData({
    data: req,
    success: res => {
     // console.log("规则查询返回", res);
      if (res.length == 0) {
        let selectNode = this.props.syncTree.getSelectNode(this.treeId);
        let request = { pk_workinggroup: selectNode.pid, enablestate: false };
        requestApi.queryParentFormListData({
          data: request,
          success: res => {
            if (res.length > 0) {
              // 当前节点规则为空&&上级工作组有规则出现Pop提示拉取上级规则
              this.setState({ isShowPop: true });
            }
          }
        });
      }
      this.setState({
        formListData: res,
        isShowPop: false,
        disabledBtn: false
      });
    }
  });
}

/**
 * @Author:yushuaif
 * @Description:NCTableSwitch Change
 * @Date: 15:00 2018/6/14
 * @param {pk} String
 * @param {e} Boolean 开关新状态
 * @return {} Undefined
 */
let tableSwitchChange = function(pk, e) {
  // 请求启用/停止
  let data = this.state.formListData.filter(item => {
    return item.values.pk_allotrule.value == pk;
  });
  data[0].values.enablestate.value = e;
  let req = data[0];
  let multiLang = this.props.MutiInit.getIntl(7010); 
  // 启用/停用
  requestApi.changeEnableState({
    data: req,
    success: res => {
      let qReq = {
        pk_workinggroup: this.props.syncTree.getSelectNode(this.treeId).refpk,
        enablestate: this.state.showDisabled
      };
      queryFormListData.call(this, qReq);
      if (e) {
        NCMessage.create({
          content: multiLang && multiLang.get('701001RWCL-0043'),//启用成功
          color: "success",
          position: "topRight"
        });
      } else {
        NCMessage.create({
          content: multiLang && multiLang.get('701001RWCL-0044'),//停用成功
          color: "success",
          position: "topRight"
        });
      }
    }
  });
};

export default {
  addFormButtonClick,
  saveFormButtonClick,
  cancelFormButtonClick,
  saveParentRulesClick,
  delFormButtonClick,
  editFormButtonClick,
  queryFormListData,
  resetButtonClick,
  tableSwitchChange
};
