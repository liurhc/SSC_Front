import requestApi from '../requestApi'
import formEveManager from './formEveManager'
import treeEveManager from './treeEveManager'

/**
 * @Author:gaoymf
 * @Description:共享服务中心改变
 * @Date: 19:12 2018/6/12
 * @param:
 */
let onSSCCenterChange = function(ssc) {
    if (ssc.hasOwnProperty('refpk')) {
        // 选中操作
        this.setState({ SSCCenter: ssc, showTreeSearch: true, disabledBtn: true ,formListData: []});
        let req = { "pk_sscunit": ssc.refpk };
        treeEveManager.queryTreeData.call(this, req); // 查询左侧树
        this.props.syncTree.cancelSelectedNode(this.treeId);
    } else {
        // 清空共享中心操作
        treeEveManager.queryTreeData.call(this, ""); // 查询左侧树
        this.setState({ SSCCenter: {}, showTreeSearch: false, disabledBtn: true, formListData: [] });
        this.props.syncTree.cancelSelectedNode(this.treeId);
    }
}

/**
 * @Author:gaoymf
 * @Description:显示停用勾选
 * @Date: 19:17 2018/6/12
 * @param:
 */
let showDisabledChange = function(e) {
    this.setState({ showDisabled: e });
    let selectNode = this.props.syncTree.getSelectNode(this.treeId);
    // 没有选中树节点不执行查询
    if(selectNode!=null){
        let req = { pk_workinggroup: selectNode.refpk, enablestate: e };
        formEveManager.queryFormListData.call(this,req);
    }
}

/**
 * @Author:gaoymf
 * @Description:查询单位明细范围
 * @Date: 19:44 2018/6/12
 * @param:
 */
let queryGroupDetails = function(item) {
    let multiLang = this.props.MutiInit.getIntl(7010);
    requestApi.queryGroupDetailTableData({
        data: { group: item.values.group.value },
        success: (res) => {
            //701001RWCL-0102：单位范围
            this.setState({ showDetailModal: true, searchValue: '', modalType: { display: multiLang && multiLang.get('701001RWCL-0102'), value: this.groupTableId } });
            this.props.editTable.setTableData(this.groupTableId, res[this.groupTableId]);
        }
    })
}
/**
 * @Author:gaoymf
 * @Description:查询交易类型/单据类型明细
 * @Date: 19:46 2018/6/12
 * @param:
 */
let queryBillTypeDetails = function(item, key) {
    let multiLang = this.props.MutiInit.getIntl(7010);
    switch (key) {
        case this.tranctionTableId:
            requestApi.queryDetailTableData({
                data: { billtype: item.values.tranction.value, type: this.tranctionTableId },
                success: (res) => {
                    //multiLang && multiLang.get('701001RWCL-0103')：交易类型
                    this.setState({ showDetailModal: true, searchValue: '', modalType: { display: multiLang && multiLang.get('701001RWCL-0103'), value: this.tranctionTableId } });
                    this.props.editTable.setTableData(key, res[this.tranctionTableId]);
                }
            })
            break;
        case this.billtypeTableId:
            requestApi.queryDetailTableData({
                data: { billtype: item.values.billtype.value, type: this.billtypeTableId },
                success: (res) => {
                    //multiLang && multiLang.get('701001RWCL-0104')：单据类型
                    this.setState({ showDetailModal: true, searchValue: '', modalType: { display: multiLang && multiLang.get('701001RWCL-0104'), value: this.billtypeTableId } });
                    this.props.editTable.setTableData(key, res[this.billtypeTableId]);
                }
            })
            break;
    }
}
/**
 * @Author:gaoymf
 * @Description:查询详细信息
 * @Date: 19:44 2018/6/12
 * @param:
 */
let queryDetailTable = function(searchValue) {
    let filterKey = [];
    // 单据类型/交易类型查询 billtypename  单位范围查询name
    this.state.modalType.value == this.groupTableId ? filterKey.push('name') :filterKey.push('billtypename')
    this.props.editTable.setFiltrateTableData(this.state.modalType.value, filterKey, searchValue, false)
}
export default { onSSCCenterChange, showDisabledChange, queryGroupDetails, queryBillTypeDetails, queryDetailTable }