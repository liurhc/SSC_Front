import { toast,promptBox } from "nc-lightapp-front";
import requestApi from "./../requestApi";
import query from "./query";
import formEveManager from "./formEveManager"

/**
 * @Author:gaoymf
 * @Description:新增icon点击
 * @Date: 18:18 2018/6/12
 * @param:node 当前点击的节点
 */
let clickAddIconEve = function(node) {
    this.setState({ showTreeModal: true });
    this.isRoot = node.refpk == "0001ZG100000SSCGROUP";
    // 可编辑
    this.props.form.setFormStatus(this.treeFormId, 'edit');
    // 清空
    this.props.form.EmptyAllFormValue(this.treeFormId);
    this.props.form.setFormItemsValue(this.treeFormId, { pid: { value: node.refpk } });
};

/**
 * @Author:gaoymf
 * @Description:删除icon点击
 * @Date: 18:22 2018/6/12
 * @param:当前点击的节点
 */
let clickDelIconEve = function(node) {
    let multiLang = this.props.MutiInit.getIntl(7010); 
    promptBox({
        color: 'warning',               // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
        title: multiLang && multiLang.get('7010-0002'), //删除
        content: multiLang && multiLang.get('701001RWCL-0168'), //"701001RWCL-0168": "确定要删除所选数据吗？"
        beSureBtnClick: () => {
            let req = this.props.syncTree.getSyncTreeValue(this.treeId, node.refpk);
            requestApi.delTreeData({
                data: req,
                success: res => {
                    let req = { pk_sscunit: this.state.SSCCenter.refpk };
                    if (res.result == false) {
                        toast({ color: "danger", content: res.msg });
                    } else {
                        queryTreeData.call(this, req);
                    }
                }
            });
        },   // 确定按钮点击调用函数,非必输
    })
};

/**
 * @Author:gaoymf
 * @Description:树型编辑icon点击
 * @Date: 18:04 2018/6/12
 * @param:node 当前编辑的节点
 */
let clickEditIconEve = function(node) {
    this.setState({ showTreeModal: true });
    this.isRoot = true;
    this.props.form.EmptyAllFormValue(this.treeFormId);
    this.props.form.setFormItemsValue(this.treeFormId, {
        refname: { value: node.refname1?node.refname1:node.refname, display: node.refname1?node.refname1:node.refname },
        refname2: { value: node.refname2, display: node.refname2 },
        refname3: { value: node.refname3, display: node.refname3 },
        refcode: { display: node.refcode, value: node.refcode },
        pid: { display: node.pid, value: node.pid },
        refpk: { display: node.refpk, value: node.refpk }
    });
};
/**
 * @Author:gaoymf
 * @Description:树型停用icon点击
 * @Date: 18:04 2018/6/12
 * @param:node 当前编辑的节点
 */
let clickStopIconEve = function(node) {
    let treeNode = this.props.syncTree.getSyncTreeValue(this.treeId, node.refpk);  
    let arr = [];
    arr.push(treeNode.refpk);
    if( treeNode.refValue.enablestate=='3'){
        // 启用 :更新目标节点和上级节点
        // 查询所有上级节点并更新父节点启用状态
        findP.call(this,treeNode,arr);
        console.log('启用的树pk',arr);
        requestApi.enableTreeData({
            data: arr,
            success: res => {
                let req = { pk_sscunit: this.state.SSCCenter.refpk };
                queryTreeData.call(this, req);
                this.closeModal(this.treeModalId);
            }
        });
    }else {
        if(treeNode.children!=null){
            // 查询所有下级节点
            queryTreeList(treeNode.children,arr);
        }
        console.log('停用的树pk',arr);
        requestApi.disableTreeData({
            data: arr,
            success: res => {
                let req = { pk_sscunit: this.state.SSCCenter.refpk };
                queryTreeData.call(this, req);
                this.closeModal(this.treeModalId);
            }
        });
    }
};


/**
 * @Author:gaoymf
 * @Description:树型启用icon点击
 * @Date: 18:04 2018/6/12
 * @param:node 当前编辑的节点
 */
let clickUpIconEve = function(node) {
    let treeNode = this.props.syncTree.getSyncTreeValue(this.treeId, node.refpk);  
    if( treeNode.refValue.enablestate=='3'){
        // 启用 :更新目标节点和上级节点
        let ans = [];
          // 查询所有上级节点并更新父节点启用状态
        ans.push(treeNode.refpk);
        findP.call(this,treeNode,ans);
        requestApi.enableTreeData({
            data: ans,
            success: res => {
                let req = { pk_sscunit: this.state.SSCCenter.refpk };
                queryTreeData.call(this, req);
                this.closeModal(this.treeModalId);
            }
        });
    }else {
        let arr = [];
        arr.push(treeNode.refpk);
        if(treeNode.children!=null){
            // 查询所有下级节点
            queryTreeList(treeNode.children,arr);
        }
        requestApi.disableTreeData({
            data: arr,
            success: res => {
                let req = { pk_sscunit: this.state.SSCCenter.refpk };
                queryTreeData.call(this, req);
                this.closeModal(this.treeModalId);
            }
        });
    }
};
/**
 * @Author:gaoymf
 * @Description:递归树结构子集节点,更新作业组启用/停用状态
 * @Date: 16:13 2019/1/17
 * @param:
 */
let queryTreeList = function(treeNode,arr){
    for (var i = 0; i < treeNode.length; i++) {
        arr.push(treeNode[i].refpk);
        var children = treeNode[i].children;
        if (children != null) {
            queryTreeList(children, arr);
        } 
    }
    return arr;
};

/**
 * @Author:gaoymf
 * @Description:递归查询树结构父集节点
 * @Date: 16:13 2019/1/17
 * @param:
 */
function findP(treeNode,ans) {
    let pNode = this.props.syncTree.getSyncTreeValue(this.treeId, treeNode.pid); 
    if(pNode!=null){
        ans.push(pNode.refpk);
        findP.call(this,pNode,ans);
    }
}


/**
 * @Author:gaoymf
 * @Description:查询对应规则
 * @Date: 16:13 2018/6/11
 * @param:
 */
let onSelectEve = function(node) {
    // 虚拟节点点击不触发查询
    if (node === "0001ZG100000SSCGROUP") {
        this.setState({ disabledBtn: true, formListData: [] });
        return;
    } else {
        let req = { pk_workinggroup: node, enablestate: this.state.showDisabled };
        formEveManager.queryFormListData.call(this, req);
    }
};


/**
 * @Author:gaoymf
 * @Description:保存节点
 * @Date: 16:45 2018/7/18
 * @param:
 */
let saveNode = function(treeFormData, flag) {
    let req = {
        refpk: treeFormData.rows[0].values.refpk.value,
        refcode: treeFormData.rows[0].values.refcode.value,
        refname: treeFormData.rows[0].values.refname.value,
        refname1: treeFormData.rows[0].values.refname.value,
        refname2: treeFormData.rows[0].values.refname2?treeFormData.rows[0].values.refname2.value:'',
        refname3: treeFormData.rows[0].values.refname3?treeFormData.rows[0].values.refname3.value:'',
        pk_sscunit: this.state.SSCCenter.refpk
    };
    if (treeFormData.rows[0].values.pid.value !== "0001ZG100000SSCGROUP") {
        req.pid = treeFormData.rows[0].values.pid.value;
    }
    requestApi.addTreeData({
        data: req,
        success: res => {
            // this.props.syncTree.addNodeSuccess(this.treeId, res);
            // 清空
            this.props.form.EmptyAllFormValue(this.treeFormId);
            // pid初始化
            this.props.form.setFormItemsValue(this.treeFormId, { pid: { value: treeFormData.rows[0].values.pid.value } });
            let req = { pk_sscunit: this.state.SSCCenter.refpk };
            queryTreeData.call(this, req);
            if (flag) {
                this.closeModal(this.treeModalId);
            }

        }
    });
};

/**
 * @Author:gaoymf
 * @Description:保存同级节点
 * @Date: 16:13 2018/7/18
 * @param:
 */
let saveAndAddTreeButton = function() {
    if (this.props.form.isCheckNow(this.treeFormId)) {
        let treeFormData = this.props.form.getAllFormValue(this.treeFormId);
        // false控制点击后弹窗不消失
        saveNode.call(this, treeFormData, false);
    }
};

/**
 * @Author:gaoymf
 * @Description:保存按钮点击
 * @Date: 16:13 2018/7/18
 * @param:
 */
let saveOrUpdateTreeButton = function() {
    if (this.props.form.isCheckNow(this.treeFormId)) {
        let treeFormData = this.props.form.getAllFormValue(this.treeFormId);
        let refpk = treeFormData.rows[0].values.refpk.value;
        if (refpk == undefined) {
            // 新增节点
            saveNode.call(this, treeFormData, true);
        } else {
            // 更新节点
            let updateReq = this.props.syncTree.getSyncTreeValue(this.treeId, refpk);
            updateReq.refcode = treeFormData.rows[0].values.refcode.value;
            updateReq.refname = treeFormData.rows[0].values.refname.value;
            updateReq.refname1 = treeFormData.rows[0].values.refname.value;
            updateReq.refname2 = treeFormData.rows[0].values.refname2?treeFormData.rows[0].values.refname2.value:'';
            updateReq.refname3 = treeFormData.rows[0].values.refname3?treeFormData.rows[0].values.refname3.value:'';
            requestApi.updateTreeData({
                data: updateReq,
                success: res => {
                    // this.props.syncTree.editNodeSuccess(this.treeId,res);
                    let req = { pk_sscunit: this.state.SSCCenter.refpk };
                    queryTreeData.call(this, req);
                    this.closeModal(this.treeModalId);
                }
            });
        }
    }
};
/**
 * @Author:gaoymf
 * @Description:取消
 * @Date: 16:15 2018/6/11
 * @param:
 */
let cancelTreeButton = function() {
    this.closeModal(this.treeModalId);
};

// 查询树节点数据
function queryTreeData(req) {
    let { syncTree } = this.props;
    let multiLang = this.props.MutiInit.getIntl(7010); 
    let { setSyncTreeData } = syncTree;
    if (req == "") {
        setSyncTreeData(this.treeId, []);
    } else {
        requestApi.queryTreeData({
            data: req,
            success: res => {
                // 生成启用/停用数据结构
                let iconData = getUpAndStopIconData(res.nodes);
                // 设置虚拟根节点
                res.nodes.push({
                    refpk: "0001ZG100000SSCGROUP",
                    refcode: "ROOT",
                    refname: multiLang && multiLang.get('701001RWCL-0051')//作业组
                });
                let newTree = syncTree.createTreeData(res.nodes); //创建树 组件需要的数据结构
                setSyncTreeData(this.treeId, newTree);
                this.props.syncTree.setIconVisible('tree',iconData);// 设置启用/停用icon
                this.props.syncTree.hideIcon(this.treeId, "0001ZG100000SSCGROUP", { delIcon: false, editIcon: false });
            }
        });
    }
}
// 构建树组件启用/停用数据结构
let getUpAndStopIconData = function(datas){
     let iconDatas = datas.map((data) => {
        if(data.refValue.enablestate=='2'){
            return {
                key:data.refpk,     
                value:{                        
                    stopUpIon: 'up' // 启用
                }
            }
        }else {
            return {
                key:data.refpk,     
                value:{                        
                    stopUpIon: 'stop' // 停用
                }
            }
        }
    })
    return iconDatas;
}

export default { clickAddIconEve, clickDelIconEve, clickEditIconEve,clickStopIconEve,clickUpIconEve, queryTreeData, onSelectEve, saveAndAddTreeButton, saveOrUpdateTreeButton, cancelTreeButton };