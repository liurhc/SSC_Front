import React, {Component} from 'react'
import {createPage, base, toast} from 'nc-lightapp-front'
const {NCModal,NCButton} = base
import {buttonClick, initTemplate, afterEvent, query} from './events'
import {Nav} from 'ssccommon/components/global-nav'
import {EditTable} from 'ssccommon/components/table'
import SSCCenterRef from '../../../refer/sscbd/SSCUnitGridRef'
import './index.less'
import {
    ProfileStyle,
    ProfileHead,
    ProfileBody,
    HeadCenterCustom,
    ButtonGroup
} from 'ssccommon/components/profile'

let checkedColumns = []
 
class SSCLadeRule extends Component {
    constructor(props) {
        super(props);
        this.state = {
            SSCCenter: '',
            sscCenterPk: '',
            showModal: false,
            currentRow: null,
            currentLeftRowIdx: -1,
            // specBtn: null
            sscCenterDisabled: false,
        }
        this.leftTableFocusRowIndex = null; //左侧表格焦点行索引
        initTemplate.call(this, props)
    }

    componentDidMount() {}

    // 左侧列表行点击
    onLeftRowClick = (opt, areaId, column, index) => {
        let multiLang = this.props.MutiInit.getIntl(7010); 
        if(this.props.editTable.getStatus(window.presetVar.ladeRuleBodyTable)=="edit"){
            toast({content : multiLang && multiLang.get('701001RWCL-0083'),color : 'warning'});//701001RWCL-0083:您有未保存的数据，请保存后重试
            this.props.editTable.focusRowByIndex(window.presetVar.l_list, this.leftTableFocusRowIndex);
            return;
        }
        this.leftTableFocusRowIndex  = index;
        let {values: {
            pk_laderule: {value},
            pk_managelevel
        }} = column
        this.setState({currentRow: column, currentLeftRowIdx: index})
        if (pk_managelevel.value === '10') {
            query.queryDetails(this, {pk_laderule: value})
        } else {
            this.props.button.setButtonsVisible({'Add2': false})
            this.props.editTable.setTableData('ladeRuleBodyTable', { rows: [] })
        }
    }

    // 批量删除
    batchDelete = () => {
        let multiLang = this.props.MutiInit.getIntl(7010);
        if (checkedColumns.length > 0) {
            buttonClick.batchDelete(this, checkedColumns.map(each => each.data))
            return
        }
        toast({content : multiLang && multiLang.get('701001RWCL-0084'),color : 'warning'})//701001RWCL-0084:请至少选择一条数据
    }

    //左侧选择列单个选择框回调 暂存选中数据
    onSelected = (opt, areaId, column, index, onoff) => {
       if (onoff) {
            checkedColumns.push({
                index,
                data: column
            })
        } else {
            let idx = -1
            while (++idx < checkedColumns.length) {
                if (checkedColumns[idx].index == index) {
                    checkedColumns.splice(idx, 1)
                    return
                }
            }
        }
        
        
    }

    // 全选cb
    onSelectedAll = (props, areaId, onoff, idx) => {
        checkedColumns = []
        if (onoff) {
            let data = this.props.editTable.getAllRows(window.presetVar.l_list,true)
            let idx = -1
            while (++idx < data.length) {
                checkedColumns.push({
                    idx,
                    data: data[idx]
                })
            }
        }
    }

    openModal = () => {
        window.leftModalStatus = 'add'
        this.setState({showModal:true})
        this.initAddFromData()
    }

    initAddFromData = ()=>{
        this.props.form.setFormItemsDisabled(
            window.presetVar.addFormCode,
            {'thresholdvalue': true, 'details': true}
        )
        this.props.form.setFormItemsValue(
            window.presetVar.addFormCode,
            {'ladetype':{value:'0'},'pk_managelevel':{value:'0'}}
        )
    }

    // showSpecBtn(nextProps, nextState) {
    //     setTimeout(() => {
    //         const headData = this.props.editTable.getAllData(window.presetVar.l_list)
    //         if (this.state.sscCenterPk==null||this.state.sscCenterPk==''||headData.rows && headData.rows.length) {
    //             // this.setState({
    //             //     specBtn: null
    //             // })
    //         } else {
    //             // this.setState({
    //             //     specBtn: (
    //             //         <NCButton
    //             //             className='spec-btn'
    //             //             onClick={ this.openModal }
    //             //         >+添加</NCButton>
    //             //     )
    //             // })
    //         }
    //     }, 500)
    //     return true
    // }

    render() {

        let {form: {createForm}} = this.props
        let multiLang = this.props.MutiInit.getIntl(7010); 
        const btnModalConfig = {
            ['DelSpec'] : {
                title: multiLang && multiLang.get('7010-0002'),//删除
                content: multiLang && multiLang.get('701001RWCL-0168'),//确定要删除所选数据吗？
            }
        }

        return (
            <div className="container" id='sscladerule'>
                <Nav defaultActIdx={'4'} {...this.props}/>
                <div className="inner-container clearfix">
                    {/* 任务提取规则 */}
                    <div className="left-container">

                        {/* { this.state.specBtn } */}

                        <ProfileStyle layout="singleTable" {...this.props}>
                            {/*页面头*/}
                            <ProfileHead title={multiLang && multiLang.get('701001RWCL-0165')}/*"提取规则"*/>
                                <HeadCenterCustom>
									
                                    <div className="header-title-search-area ssccenter">
                                        <span class="ssc-must">*</span>
                                        {SSCCenterRef({
                                            onChange: (e)=>{
                                                query.onSSCCenterChange(this, e);
                                                // this.showSpecBtn(this.props,this.state);
                                            },
                                            disabled: this.state.sscCenterDisabled,
                                            value: this.state.SSCCenter,
                                        })}
                                    </div>
                                </HeadCenterCustom>
                                <ButtonGroup
                                    area={'l_top_btns'}
                                    buttonEvent={
                                        {
                                            Add: {click:()=>{this.openModal()}},
                                            DelSpec: {click:()=>{this.batchDelete()}}
                                        }
                                    }
                                    modalConfig={btnModalConfig}
                                />
                            </ProfileHead>
                            {/*页面体*/}
                            <ProfileBody>
                                <EditTable
                                    showCheck={true}
                                    areaId={window.presetVar.l_list}
                                    onRowClick={this.onLeftRowClick}
                                    onSelected={this.onSelected}
                                    onSelectedAll={this.onSelectedAll}
                                    selectedChange= {buttonClick.selectedChange}
                                />
                            </ProfileBody>
                        </ProfileStyle>
                    </div>
                    {/* 任务提取规则明细 */}
                    <div className="right-container">
                        <ProfileStyle layout="singleTable" {...this.props}>
                            {/*页面头*/}
                            {/*701001RWCL-0081：规则适用作业组*/}
                            <ProfileHead title={multiLang && multiLang.get('701001RWCL-0081')}>
                                <ButtonGroup
                                    area={'r_top_btns'}
                                    buttonEvent={{
                                        Add2: {click:()=>{buttonClick.addRuleBody(this)}},
                                        SaveDetails: {click:()=>{buttonClick.saveDetails(this)}},
                                        CancelDetails: {click:()=>{buttonClick.cancelDetails(this)}}
                                    }}
                                />
                            </ProfileHead>
                            {/*页面体*/}
                            <ProfileBody>
                                <EditTable
                                    showCheck={false}
                                    areaId={window.presetVar.ladeRuleBodyTable}
                                    onAfterEvent={afterEvent.bind(this)}
                                />
                            </ProfileBody>
                        </ProfileStyle>
                    </div>
                </div>
                <div>
                {/* 新增提取规则弹框 */}
                <NCModal
                    show = {this.state.showModal}
                    className='senior'
                    onHide={()=>{this.setState({showModal:false})}}
                    //backdrop={false}
                    backdrop='false'
                >
                    <NCModal.Header closeButton={true}>
                        {/* 701001RWCL-0087：新增提取规则；701001RWCL-0088：修改提取规则 */}
                        <NCModal.Title>
                            {
                                window.leftModalStatus === 'add'
                                ?
                                multiLang && multiLang.get('701001RWCL-0087')
                                :
                                multiLang && multiLang.get('701001RWCL-0088')
                            }
                        </NCModal.Title>
                    </NCModal.Header>
                    <NCModal.Body>
                        <div className="ssc_edit_modal_form">
                        {createForm(window.presetVar.addFormCode ,{
                            onAfterEvent: afterEvent.bind(this),
                        })}
                        </div>
                    </NCModal.Body>
                    <NCModal.Footer>
                        <NCButton colors="primary"
                            onClick={(e)=>{buttonClick.addFormSave(this)}} >{multiLang && multiLang.get('7010-0003')/*保存*/}</NCButton>
                        <NCButton 
                            onClick={(e)=>{buttonClick.addFormCancel(this)}}>{multiLang && multiLang.get('7010-0004')}</NCButton>
                    </NCModal.Footer>
                </NCModal>
                </div>
            </div>
        )
    }
}
// 初始化节点
const SSCLaderUle = createPage({
    mutiLangCode: '7010'
})(SSCLadeRule)
// 注册节点
ReactDOM.render(<SSCLaderUle />, document.querySelector('#app'))