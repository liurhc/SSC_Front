export default {
    btns: [
        /*{
            "id": "0001A41000000006J5B1",
            "type": "button_main",
            "key": "EditBtn",
            "title": "修改",
            "area": "ladeRuleHeadTable",
            "children": []
        },
        {
            "id": "0001A41000000006J5B1",
            "type": "button_main",
            "key": "DeleteBtn",
            "title": "删除",
            "area": "ladeRuleHeadTable",
            "children": []
        },
        {
            "id": "0001A41000000006J5B1",
            "type": "button_main",
            "key": "DeleteBtn2",
            "title": "删除",
            "area": "ladeRuleBodyTable",
            "children": []
        },
        // {
        //     "id": "0001A41000000006J5B1",
        //     "type": "button_main",
        //     "key": "Add",
        //     "title": "新增",
        //     "area": "l_top_btns",
        //     "parentCode":"head_group",
        //     "children": []
        // },
        // {
        //     "id": "0001A41000000006J5B1",
        //     "type": "button_secondary",
        //     "key": "DelSpec",
        //     "title": "删除",
        //     "area": "l_top_btns",
        //     "parentCode":"head_group",
        //     "children": []
        // },
        {
            "type": "buttongroup",
            "key": "head_group",
            "area": "l_top_btns",
            "isenable": true,
            "order": "7",
            "parentCode": null,
            "children": [{
                "id": "0001A41000000006J5B1",
                "type": "button_main",
                "key": "Add",
                "title": "新增",
                "area": "l_top_btns",
                "parentCode":"head_group",
                "children": []
            },
            {
                "id": "0001A41000000006J5B1",
                "type": "button_secondary",
                "key": "DelSpec",
                "title": "删除",
                "area": "l_top_btns",
                "parentCode":"head_group",
                "children": []
            }]
        },
        {
            "id": "0001A41000000006J5B1",
            "type": "button_main",
            "key": "Add2",
            "title": "新增",
            "area": "r_top_btns",
            "children": []
        },
        {
            "id": "0001A41000000006J5B1",
            "type": "button_main",
            "key": "SaveDetails",
            "title": "保存",
            "area": "r_top_btns",
            "children": []
        },
        {
            "id": "0001A41000000006J5B1",
            "type": "button_secondary",
            "key": "CancelDetails",
            "title": "取消",
            "area": "r_top_btns",
            "children": []
        }*/
    ]
    /*
    headAddForm: {
        pagination: false,
        code: "headAddForm",
        moduletype: "form",
        name: "提取规则新增",
        status: 'edit',
        items: [
            {
                "itemtype": "input",
                "visible": true,
                "label": "编码",
                "attrcode": "code",
                "maxlength": "50",
                "required": true,
                "disabled": false
            },
            {
                "itemtype": "input",
                "visible": true,
                "label": "名称",
                "attrcode": "name",
                "maxlength": "50",
                "required": true,
                "disabled": false,
                "col": "6"

            },
            {
                "itemtype": "radio",
                "visible": true,
                "label": "提取方式",
                "attrcode": "ladetype",
                "maxlength": "50",
                "required": true,
                "disabled": false,
                "options": [{
                        "display": "不限制提取",
                        "value": "0"
                    },
                    {
                        "display": "阀值提取",
                        "value": "10"
                    },
                    {
                        "display": "处理完毕后提取",
                        "value": "20"
                    }
                ]
            },
            {
                "itemtype": "number",
                "visible": true,
                "label": "每次提取数量",
                "attrcode": "eachextracttasks",
                "required": true,
                "disabled": false,
                "scale":"0"
            },
            {
                "itemtype": "number",
                "visible": true,
                "label": "在手任务阀值",
                "attrcode": "thresholdvalue",
                "disabled": true,
                "scale":"0"
            },
            {
                "itemtype": "radio",
                "visible": true,
                "label": "管理层级",
                "attrcode": "pk_managelevel",
                "maxlength": "20",
                "required": true,
                "disabled": false,
                "options": [{
                        "display": "共享中心",
                        "value": "0"
                    },
                    {
                        "display": "作业组",
                        "value": "10"
                    }
                ]
            },
            {
                "itemtype": "refer",
                "visible": true,
                "label": "规则适用作业组",
                "disabled": true,
                "attrcode": "details",
                "isMultiSelectedEnabled": true,
                "refcode": "/ssctp/refer/sscbd/SSCWorkGroupGridRef/index.js"
            },
            {
                "itemtype": "refer",
                "label": "组织",
                "attrcode": "pk_sscunit",
                "visible": false
            },
            {
                "itemtype": "number",
                "label": "序号",
                "attrcode": "seqno",
                "visible": false
            },
            {
                "itemtype": "datepicker",
                "label": "时间戳",
                "attrcode": "ts",
                "maxlength": "19"
            },
            {
                "itemtype": "input",
                "label": "主键",
                "attrcode": "pk_laderule",
                "maxlength": "20"
            },
            {
                "itemtype": "refer",
                "label": "集团",
                "attrcode": "pk_group",
                "maxlength": "20"
            }
        ]
    } */
}