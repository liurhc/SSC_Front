// import { ajax } from 'nc-lightapp-front';
import {getMultiLang} from 'nc-lightapp-front';
// import requestApi from '../requestApi'
// import fixedTpl from './fixedTpl'
import buttonClick from './buttonClick'

export default function(props) {
    window.presetVar = {
        ...window.presetVar,
        pageId: '700101_laderule',
        l_list: 'ladeRuleHeadTable',
        ladeRuleBodyTable: 'ladeRuleBodyTable',
        addFormCode: 'headAddForm'
    }
    const _this = this;
    let createUIDomPromise = new Promise((resolve, reject) => {
        props.createUIDom({}, (data) => {
            resolve(data);
        })
    });

    let getMultiLangPromise = new Promise((resolve, reject) => {
        getMultiLang({
            moduleId: 7010, domainName: 'ssctp',currentLocale: 'zh-CN',
            callback: (json) => {
                resolve(json);
            }
        })
    });
    Promise.all([createUIDomPromise, getMultiLangPromise]).then((resultList) => {
        let data = resultList[0];
        let json = resultList[1];
		
        if (data) {
            let {
                ladeRuleHeadTable, // 任务提取规则
                ladeRuleBodyTable // 任务提取规则明细
            } = data.template
            //设置表格的扩展按钮列
            let l_event = {
                label: json['701001RWCL-0060'],//操作
                attrcode: 'opr',
                itemtype: 'customer',
                visible: true,
                render: (text, record, index) => {
                    return props.button.createOprationButton(["EditBtn", "DeleteBtn"], {
                        area: window.presetVar.l_list,
                        onButtonClick: (props, btnKey, e) => {
                            buttonClick.rowBtnClick(props, btnKey, text, record, index, _this);
                        }
                    });
                }
            }
            let r_event = {
                label: json['701001RWCL-0060'],//操作
                attrcode: 'opr',
                itemtype: 'customer',
                visible: true,
                render: (text, record, index) => {
                    return props.button.createOprationButton(["DeleteBtn2"], {
                        area: window.presetVar.ladeRuleBodyTable,
                        onButtonClick: (props, btnKey) => {
                            buttonClick.rowBtnClick(props, btnKey, text, record, index, _this)
                        }
                    });
                }
            }

            props.button.setButtons(data.button)
            props.button.setPopContent('DeleteBtn', json['701001RWCL-0039'])
            props.button.setPopContent('DeleteBtn2', json['701001RWCL-0039'])

            props.button.setButtonsVisible({
                'Add': true,
                'DelSpec': true,
                'Add2': false,
                'SaveDetails': false,
                'CancelDetails': false
            })
            props.button.setButtonDisabled(['Add', 'DelSpec'], true);

            ladeRuleHeadTable.items.push(l_event)
            ladeRuleBodyTable.items.push(r_event)

            ladeRuleHeadTable.items.forEach(ele => {
                ele.width = '120px'
            })
            ladeRuleBodyTable.items.forEach(ele => {
                ele.width = '100px'
            })
            ladeRuleBodyTable.items[ladeRuleBodyTable.items.length - 1].width = '50px';

            data.template[window.presetVar.ladeRuleBodyTable].items.find((item) => item.attrcode === "refpk").queryCondition = function() {
                let pk_sscunit = _this.state.sscCenterPk
                return {
                    "pk_sscunit": pk_sscunit
                }
            };
            data.template[window.presetVar.addFormCode].items.find((item) => item.attrcode === "details").queryCondition = function() {
                let pk_sscunit = _this.state.sscCenterPk
                return {
                    "pk_sscunit": pk_sscunit
                }
            };

            data.template[window.presetVar.addFormCode].items.map((item) => {
                if (item.attrcode === 'details') {
                item.isRunWithChildren =false;
                }
                });

            data.template[window.presetVar.addFormCode].status = 'edit';
            props.meta.setMeta(data.template);
            
        }
		})
}