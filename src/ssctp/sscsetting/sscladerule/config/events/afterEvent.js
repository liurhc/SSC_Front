export default function (props, moduleId, key, value, data, index) {
    switch(key){
        case 'ladetype':
            props.form.setFormItemsDisabled(
                window.presetVar.addFormCode,
                {
                    thresholdvalue: value.value === '10' ? false : true
                }
            )
            props.form.setFormItemsRequired( window.presetVar.addFormCode,{'thresholdvalue':  value.value === '10' ? true : false});  
            if (value.value !== '10') {
                props.form.setFormItemsValue(
                    window.presetVar.addFormCode,
                    {'thresholdvalue':{value:null,display:null}}
                )
            }
            break;
        case 'pk_managelevel':
            props.form.setFormItemsDisabled(
                window.presetVar.addFormCode,
                {
                    details: value.value === '10' ? false : true
                }
            )
            props.form.setFormItemsRequired( window.presetVar.addFormCode,{'details':  value.value === '10' ? true : false});  
            if (value.value !== '10') {
                props.form.setFormItemsValue(
                    window.presetVar.addFormCode,
                    {'details':{value:null,display:null}}
                )
            }
            break;
        case 'refpk':
            props.editTable.setValByKeyAndRowId(moduleId,data[0].rowid,'refpk.code',{value:value.refcode,display:value.refcode});
            if(moduleId==window.presetVar.ladeRuleBodyTable&&value.length>0){
                let newIndex = index;
                for(let i=0;i<value.length;i++){
                    let rowData = { 'refpk':{'value': value[i].refpk, 'display': value[i].refname, disabled: "on"},
                            'refpk.code':{'value': value[i].refpk, 'display': value[i].refcode}};
                    if(i==0){
                        props.editTable.setValByKeyAndIndex(moduleId,newIndex,'refpk',rowData['refpk']);
                        props.editTable.setValByKeyAndIndex(moduleId,newIndex++,'refpk.code',rowData['refpk.code']);
                    }else{
                        props.editTable.addRow(moduleId, newIndex++, false, rowData);
                    }
                }
            }
            break;
        default:
            break;
    }
}