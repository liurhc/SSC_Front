import requestApi from '../requestApi'
import query from './query'
import {toast,base} from 'nc-lightapp-front'

// 删除
const batchDelete = (that, rows) => {
    let index = -1
    while (++index < rows.length) delete rows[index].values.details
    let param = {
        head: {
            areaType: 'table',
            areaCode: 'head',
            rows
        }
    }
    requestApi.batchDeleteRule({
        param,
        success: (res) => {
            query.queryTableData(that, {'pk_sscunit': that.state.sscCenterPk})
        }
    })
}

// 表格操作
const rowBtnClick = (props, btnKey, text, record, index, _this) => {
    switch (btnKey) {
        case 'DeleteBtn':
            //delete text.values.details
            let param = {
                head: {
                    areaType: 'table',
                    areaCode: 'head',
                    rows: [
                        record
                    ]
                }
            }
            requestApi.batchDeleteRule({
                param,
                success: (res) => {
                    props.editTable.setTableData(window.presetVar.ladeRuleBodyTable, { rows: [] })
                    props.button.setButtonsVisible({'Add2': false})
                    requestApi.queryLadeRule({
                        data: {'pk_sscunit': record.values.pk_sscunit.value},
                        success: (res) => {
                            if(res){
                                props.editTable.setTableData(
                                    window.presetVar.l_list,
                                    res[window.presetVar.l_list]
                                )
                                // _this.setState({
                                //     specBtn: null
                                // })
                            }else{
                                props.editTable.setTableData(
                                    window.presetVar.l_list,
                                    { rows: [] }
                                )
                                // _this.setState({
                                //     specBtn: (
                                //         <NCButton
                                //             className='spec-btn'
                                //             onClick={ 
                                //                 () => {
                                //                     window.leftModalStatus = 'add'
                                //                     _this.setState({showModal:true})
                                //                     _this.props.form.setFormItemsDisabled(
                                //                         window.presetVar.addFormCode,
                                //                         {'thresholdvalue': true, 'details': true}
                                //                     )
                                //                 }
                                //              }
                                //         >+添加</NCButton>
                                //     )
                                // })
                            }
                        }
                    })
                }
            })
            break;
        case 'DeleteBtn2':
            if(props.editTable.getStatus(window.presetVar.ladeRuleBodyTable)=="edit"){
                props.editTable.deleteTableRowsByIndex(window.presetVar.ladeRuleBodyTable, index);
                return;
            }
            const data = _this.state.currentRow.values
            delete data.details
            const paramData = {
                head: {head: {
                    areaType: 'form',
                    areacode: 'head',
                    rows: [{
                        status: '1',
                        values: data
                    }]
                }},
                body: {body: {
                    areaType: 'table',
                    areacode: 'body',
                    rows: []
                }}
            }
            let details = props.editTable.getAllRows(window.presetVar.ladeRuleBodyTable,true)
            details.splice(index, 1)
            let idx = -1
            while(++idx < details.length) {
                paramData.body.body.rows.push({
                    status: '2',
                    rowId: idx,
                    values: {refpk: {
                        value: details[idx].values.refpk.value
                    }}
                })
            }
            requestApi.saveLadeRule({
                data: paramData,
                success: (res) => {
                    _this.setState({
                        currentRow: res.head.ladeRuleHeadTable.rows[0]
                    })
                    let headData = props.editTable.getAllRows(window.presetVar.l_list,true)
                    headData[_this.state.currentLeftRowIdx] = res.head.ladeRuleHeadTable.rows[0]
                    props.editTable.setTableData(
                        window.presetVar.ladeRuleBodyTable,
                        res.body?res.body.ladeRuleBodyTable:{rows: []}
                    )
                    const setData = res.head.ladeRuleHeadTable
                    setData.rows = headData
                    props.editTable.setTableData(
                        window.presetVar.l_list,
                        setData
                    )
                }
            })
            
            break;
        case 'EditBtn':
            window.leftModalStatus = 'edit'
            _this.setState({showModal: true})
            const {values} = props.editTable.getAllRows(window.presetVar.l_list,true)[index]
            _this.setState({currentRow: props.editTable.getAllRows(window.presetVar.l_list,true)[index]})
            for (let key in values) {
                if (key === 'ladetype') {
                    props.form.setFormItemsValue(
                        window.presetVar.addFormCode,
                        {[key]:{
                            value:values[key].value || '',
                            display:values[key].display || ''
                        }}
                    )
                    if (values[key].value === '10') {
                        props.form.setFormItemsRequired( window.presetVar.addFormCode,{'thresholdvalue': true});  
                        props.form.setFormItemsDisabled(
                            window.presetVar.addFormCode,
                            {'thresholdvalue': false}
                        )
                        props.form.setFormItemsValue(
                            window.presetVar.addFormCode,
                            {'thresholdvalue':{
                                value: values.thresholdvalue.value,
                                display: null
                            }}
                        )
                    } else {
                        props.form.setFormItemsRequired( window.presetVar.addFormCode,{'thresholdvalue': false});  
                        setTimeout(() => {
                            props.form.setFormItemsDisabled(
                                window.presetVar.addFormCode,
                                {'thresholdvalue': true}
                            )
                            props.form.setFormItemsValue(
                                window.presetVar.addFormCode,
                                {'thresholdvalue':{value:null,display:null}}
                            )
                        },0)
                    }
                } else if (key === 'pk_managelevel') {
                    props.form.setFormItemsValue(
                        window.presetVar.addFormCode,
                        {[key]:{
                            value:values[key].value || '',
                            display:values[key].display || ''
                        }}
                    )
                    if (values[key].value === '10') {
                        props.form.setFormItemsRequired(window.presetVar.addFormCode,{'details': true});  
                        props.form.setFormItemsDisabled(
                            window.presetVar.addFormCode,
                            {'details': false}
                        )
                        let details = '', display = ''
                        requestApi.queryLadeRuleBody({
                            data: {pk_laderule: values.pk_laderule.value},
                            success: (res) => {
                                let ladeRuleBodyTable = null;
                                let {
                                    body
                                } = res
                                if(body)ladeRuleBodyTable = body.ladeRuleBodyTable;
                                if(ladeRuleBodyTable && ladeRuleBodyTable.rows){
                                    let idx = -1
                                    while (++idx < ladeRuleBodyTable.rows.length) {
                                        if(idx != 0 && details != ''){
                                            details += ","
                                            display += ","
                                        }
                                        if(ladeRuleBodyTable.rows[idx].values.refpk){
                                            details += ladeRuleBodyTable.rows[idx].values.refpk.value
                                            display += ladeRuleBodyTable.rows[idx].values.refpk.display
                                        }
                                    }
                                }
                                props.form.setFormItemsValue(
                                    window.presetVar.addFormCode,
                                    {'details':{
                                        value: details,
                                        display
                                    }}
                                )
                            }
                        })
                    } else {
                        props.form.setFormItemsRequired(window.presetVar.addFormCode,{'details': false});  
                        props.form.setFormItemsDisabled(
                            window.presetVar.addFormCode,
                            {'details': true}
                        )
                        props.form.setFormItemsValue(
                            window.presetVar.addFormCode,
                            {'details':{value:null,display:null}}
                        )
                    }
                } else if (key !== 'details') {
                    props.form.setFormItemsValue(
                        window.presetVar.addFormCode,
                        {[key]:{
                            value:values[key].value || '',
                            display:values[key].display || ''
                        }}
                    )
                }
            }
            break;
        default:
            break;
    }
}

// 新增表单保存事件
const addFormSave = (that) =>{
    // 获得form数据
    let data = that.props.form.getAllFormValue(window.presetVar.addFormCode)
    // let needCheck = ['code','name','ladetype','eachextracttasks','pk_managelevel'];
    // let name4NeedCheck = ['编码','名称','提取规则','每次提取数量','管理层级'];
    const {rows} = data
    if(!that.props.form.isCheckNow(window.presetVar.addFormCode)){
        return; 
    }
    // 表单验证
    // for(let i=0;i<needCheck.length;i++){
    //     if(!rows[0].values[needCheck[i]].value||rows[0].values[needCheck[i]].value.trim()==""){
    //         toast({content : name4NeedCheck[i]+"不能为空",color : 'danger'})
    //         return
    //     }
    // }
    // if(rows[0].values.ladetype.value !=undefined && rows[0].values.ladetype.value == 10 &&rows[0].values.thresholdvalue!=undefined && rows[0].values.thresholdvalue.value <= 0){
    //     toast({content : "在手任务阀值应该为大于0的整数",color : 'danger'})
    //     return
    // }
    // 报文
    data.areaType = 'form'
    data.areacode = 'head'
    data.rows[0].status = window.leftModalStatus === 'add' ? '2' : '1'
    data.rows[0].values.pk_sscunit.value = that.state.sscCenterPk
    const param = {head: {head: data}}
    // 规则适用作业组特殊处理
    if (data.rows[0].values.details.value) {
        let {value} = data.rows[0].values.details
        delete data.rows[0].values.details
        value = value.split(',')
        param.body = {
            body: {
                areaType: 'table',
                areacode: 'body',
                rows: []
            }
        }
        let idx = -1
        while (++idx < value.length) {
            param.body.body.rows.push({
                rowId: idx,
                status: '2',
                values: {refpk: {value: value[idx]}}
            })
        }
    }
    requestApi.saveLadeRule({
        data: param,
        success: (res) => {
            query.queryTableData(
                that,
                {'pk_sscunit': that.state.sscCenterPk}
            )
            that.props.form.EmptyAllFormValue([window.presetVar.addFormCode])
            that.setState({showModal:false})
        }
    })
}

// 新增表单取消事件
const addFormCancel = (that) => {
    that.setState({showModal:false})
    that.props.form.EmptyAllFormValue([window.presetVar.addFormCode])
}

// 新增作业组
const addRuleBody = (that) => {
    that.props.editTable.setStatus(window.presetVar.ladeRuleBodyTable, 'edit');
    that.props.editTable.addRow(window.presetVar.ladeRuleBodyTable) // 添加一行
    //that.props.editTable.hideColByKey(window.presetVar.ladeRuleBodyTable,"opr") // 隐藏操作列
    let bodyAllRows = that.props.editTable.getAllRows(window.presetVar.ladeRuleBodyTable,true)
    // if(bodyAllRows.length>0){
    //     for(let i = 0; i<bodyAllRows.length; i++){
    //         // that.props.editTable.setEditableRowByRowId(window.presetVar.ladeRuleBodyTable, bodyAllRows[i].rowid, false);
    //         if(i !== bodyAllRows.length-1){ // 非最后一项添加项
    //             that.props.editTable.setEditableRowByRowId(window.presetVar.ladeRuleBodyTable, bodyAllRows[i].rowid, false);
    //             that.props.editTable.setEditableByKey(window.presetVar.ladeRuleBodyTable, bodyAllRows[i].rowid, 'refcode', false);
    //             that.props.editTable.setEditableByKey(window.presetVar.ladeRuleBodyTable, bodyAllRows[i].rowid, 'refpk', false);
    //         }else{ // 最后一项
    //             that.props.editTable.setEditableByKey(window.presetVar.ladeRuleBodyTable, bodyAllRows[i].rowid, 'refcode', false);
    //         }
    //     }
    // }

    that.props.button.setButtonsVisible({
        'Add2': false,
        'SaveDetails': true,
        'CancelDetails': true,
        'Add': false,
        'DelSpec': false
    })

    //that.props.button.setButtonDisabled(['Add', 'DelSpec'], true);
    that.props.editTable.hideColByKey(window.presetVar.l_list, 'opr');
    that.setState({
        sscCenterDisabled: true
    })
	that.props.button.setPopContent('DeleteBtn2')

}

// 新增作业组保存
const saveDetails = (that) => {
    let headData = that.state.currentRow;
    let multiLang = that.props.MutiInit.getIntl(7010); 
    that.props.editTable.filterEmptyRows(window.presetVar.ladeRuleBodyTable, []);
    let bodyData = that.props.editTable.getAllData(window.presetVar.ladeRuleBodyTable);
    const reqData = {}
    headData.status = "1"
    delete headData.values.details
    let bodyRows = []
    for(let i = 0; i < bodyData.rows.length; i++){
        if(bodyData.rows[i].status=="3")continue;
        if(bodyData.rows[i].values.refpk.value==null||bodyData.rows[i].values.refpk.value==""){
            //701001RWCL-0079:第 ； 701001RWCL-0080：行作业组不能为空
            toast({content : multiLang && multiLang.get('701001RWCL-0079')+(i+1)+multiLang && multiLang.get('701001RWCL-0080'),color : 'danger'})
            return
        }
        bodyRows.push({
            rowId: i,
            status: '2',
            values: {
                refpk: {
                    value: bodyData.rows[i].values.refpk.value
                }
            }
        })
    }
    reqData.head = {
        head : {
            "areaType": "form",
            "areacode": "head",
            "rows": [headData]
        }
        
    }
    reqData.body = {
        body : {
            "areaType": "table",
            "areacode": "body",
            "rows": bodyRows
        } 
    }
    requestApi.saveLadeRule({
        data: reqData,
        success: (data) => {
            that.props.button.setButtonsVisible({
                'Add2': true,
                'SaveDetails': false,
                'CancelDetails': false,
                'Add': true,
                'DelSpec': true
            })
            that.props.editTable.setStatus(window.presetVar.ladeRuleBodyTable, 'browse')
            that.props.editTable.showColByKey(window.presetVar.ladeRuleBodyTable,"opr")
            if(data.body){
                that.props.editTable.setTableData(
                    window.presetVar.ladeRuleBodyTable,
                    data.body.ladeRuleBodyTable
                )
            }else{
                that.props.editTable.setTableData(
                    window.presetVar.ladeRuleBodyTable,
                    { rows: [] }
                )
            }
            //
            that.setState({
                currentRow: data.head.ladeRuleHeadTable.rows[0],
                sscCenterDisabled: false
            })
            let headData = that.props.editTable.getAllRows(window.presetVar.l_list,true)
            headData[that.state.currentLeftRowIdx] = data.head.ladeRuleHeadTable.rows[0]
            const setData = data.head.ladeRuleHeadTable
            setData.rows = headData
            that.props.editTable.setTableData(
                window.presetVar.l_list,
                setData
            )

            that.props.editTable.showColByKey(window.presetVar.l_list, 'opr');
			that.props.button.setPopContent('DeleteBtn2', multiLang && multiLang.get('701001RWCL-0168'))//确定要删除所选数据吗？
        }
    })
}

// 取消新增作业组
const cancelDetails = (that) => {
    that.props.button.setButtonsVisible({
        'Add2': true,
        'SaveDetails': false,
        'CancelDetails': false,
        'Add': true,
        'DelSpec': true
    })
    that.props.editTable.cancelEdit(window.presetVar.ladeRuleBodyTable)
    that.props.editTable.setStatus(window.presetVar.ladeRuleBodyTable, 'browse')
    that.props.editTable.showColByKey(window.presetVar.ladeRuleBodyTable,"opr")

    that.props.editTable.showColByKey(window.presetVar.l_list, 'opr');
    that.setState({
        sscCenterDisabled: false
    })
	that.props.button.setPopContent('DeleteBtn2', multiLang && multiLang.get('701001RWCL-0039'))
}
function selectedChange(props, moduleId, newVal, oldVal) {
    console.log('selectedChangeEvent',props,moduleId,newVal,oldVal);
    if(newVal>0){
        props.button.setButtonDisabled(['DelSpec'], false);
    }else {
        props.button.setButtonDisabled(['DelSpec'], true);
    }
  }

export default {
    batchDelete,
    rowBtnClick,
    addFormSave,
    addFormCancel,
    addRuleBody,
    saveDetails,
    cancelDetails,
    selectedChange
}