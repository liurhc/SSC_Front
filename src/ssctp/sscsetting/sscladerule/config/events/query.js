import requestApi from '../requestApi'
import {base} from 'nc-lightapp-front'
const {NCButton} = base

//共享中心改变事件
let onSSCCenterChange =(that, e)=>{
    that.setState({sscCenterPk:e.refpk, SSCCenter: e})
    if (e.refpk){
        // that.props.button.setButtonVisible(["Add","DelSpec"], true);
        that.props.button.setButtonDisabled(['Add'], false);
        let req = {'pk_sscunit':e.refpk}
        queryTableData(that, req)
    }else{
        // that.props.button.setButtonVisible(["Add","DelSpec"], false)
        that.props.button.setButtonDisabled(['Add', 'DelSpec'], true);
        that.props.editTable.setTableData(window.presetVar.l_list, { rows: [] })
    }
    that.props.editTable.setTableData(
        window.presetVar.ladeRuleBodyTable,
        { rows: [] }
    )
}

// 查询提取规则
let queryTableData = (that, req) =>{
    // 重置规则适用作业组
    that.props.editTable.setTableData(window.presetVar.ladeRuleBodyTable, { rows: [] })
    that.props.button.setButtonsVisible({'Add2': false})
    requestApi.queryLadeRule({
        data: req,
        success: (res) => {
            if(res){
                that.props.editTable.setTableData(
                    window.presetVar.l_list,
                    res[window.presetVar.l_list]
                )
                // that.setState({
                //     specBtn: null
                // })
            }else{
                that.props.editTable.setTableData(
                    window.presetVar.l_list,
                    { rows: [] }
                )
                // that.setState({
                //     specBtn: (
                //         <NCButton
                //             className='spec-btn'
                //             onClick={ 
                //                 () => {
                //                     window.leftModalStatus = 'add'
                //                     that.setState({showModal:true})
                //                     that.props.form.setFormItemsDisabled(
                //                         window.presetVar.addFormCode,
                //                         {'thresholdvalue': true, 'details': true}
                //                     )
                //                 }
                //              }
                //         >+添加</NCButton>
                //     )
                // })
            }
        }
    })
}

let queryDetails = (that, req) =>{
    requestApi.queryLadeRuleBody({
        data: req,
        success: (res) => {
            let body = res==null?null:res.body;
            that.props.button.setButtonsVisible({'Add2': true})
            if (body) {
                if(body.ladeRuleBodyTable){
                    that.props.editTable.setTableData(
                        window.presetVar.ladeRuleBodyTable,
                        body.ladeRuleBodyTable
                    )
                }else{
                    that.props.editTable.setTableData(
                        window.presetVar.l_list,
                        { rows: [] }
                    )
                }
            }else{
                that.props.editTable.setTableData('ladeRuleBodyTable', { rows: [] })
            }
        }
    })
}

export default {onSSCCenterChange, queryTableData, queryDetails}