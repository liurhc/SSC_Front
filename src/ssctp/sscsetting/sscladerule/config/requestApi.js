import {ajax } from 'nc-lightapp-front';

let requestDomain =  '';

let requestApiOverwrite = { 
    queryLadeRule:(opt) => {
        ajax({
            url: `${requestDomain}/nccloud/ssccloud/sscbd/LadeRuleQryAction.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    queryLadeRuleBody:(opt) => {
        ajax({
            url: `${requestDomain}/nccloud/ssccloud/sscbd/LadeRuleBodyQryAction.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    batchDeleteRule:(opt) => {
        ajax({
            url: `${requestDomain}/nccloud/ssccloud/sscbd/LadeRuleBatchDeleteAction.do`,
            data: opt.param,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },
    saveLadeRule:(opt) => {
        ajax({
            url: `${requestDomain}/nccloud/ssccloud/sscbd/LadeRuleSaveAction.do`,
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    }
}

export default  requestApiOverwrite;