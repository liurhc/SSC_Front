import React, { Component } from "react";
import ReactDOM from "react-dom";
import { createPage, base, createPageIcon } from "nc-lightapp-front";
import { initTemplate, buttonClick } from "./events";
import requestApi from "./requestApi";
import { Nav } from "ssccommon/components/global-nav";
import "./index.less";
import gxicon1 from '../../../public/image/gxicon1.png';
import gxicon2 from '../../../public/image/gxicon2.png';
import gxicon3 from '../../../public/image/gxicon3.png';
import gxicon4 from '../../../public/image/gxicon4.png';
import gxicon5 from '../../../public/image/gxicon5.png';
import { ProfileStyle, ProfileHead, ProfileBody, BodyRight, HeadCenterCustom, ButtonGroup } from 'ssccommon/components/profile';


const { NCModal, NCButton, NCSwitch, NCPopconfirm, NCMessage, NCTableSwitch } = base;


class SSCUnit extends Component {
  constructor(props) {
    super(props);
    this.props = props;
    this.formModalId = "formModal"; // 新增/编辑modal关闭
    this.formId = "sscunit_card"; // 新增表单组件模板id
    this.state = {
      showFormModal: false, // 新增/编辑表单模态显示
      editFormPk: "", // 当前编辑的form的pk
      data: []
    };
    initTemplate.call(this, props);
  }

  componentDidMount() {
    requestApi.query({
      data: {},
      success: data => {
        this.setState({ data: data ? data[this.formId].rows : [],multiLangName: data ? JSON.parse(decodeURIComponent(data.userjson)) :[]});
      }
    });
  }


  drawFormList = () => {
    let formList = [];
    let imgList = [gxicon1, gxicon2, gxicon3, gxicon4, gxicon5];
    let multiLang = this.props.MutiInit.getIntl(7010); //this.moduleId
    this.state.data.map((item, index) => {
      formList.push(
        <div className="ssc-form">
          <div className="head-grow">
            <span className="head-label">
                <img className="img" src={imgList[index%5]}/>
              {/*&nbsp;&nbsp;*/}
              {/*<span >sad</span><span>dsadasdasdasdasdasdasdasdasd</span>*/}
              <span className='itemText'>{item.values.code.value+"："}</span>
              <span>{this.state.multiLangName[index]}</span>
            </span>
            <span className="head-btn">
              <span className="btn">
              {/*   <NCSwitch
                  checked={item.values.enablestate.value === "2"}
                  onChange={e => {
                    this.onChange(e, item.values.pk_unit.value);
                  }}
                /> */}
                {/*701001RWCL-0099：确定停用共享中心?，701001RWCL-00100：确定启用共享中心?*/}
                <NCPopconfirm
                  trigger="click"
                  placement="bottom"
                  content={
                    item.values.enablestate.value === '2'
                      ? multiLang && multiLang.get('701001RWCL-0099')
                      : multiLang && multiLang.get('701001RWCL-0100')
                  }
                  onClose={() => {
                    this.onChange(item.values.pk_unit.value);
                  }}
                >
                  <span>
                    <NCTableSwitch
                      checked={item.values.enablestate.value == '2'}
                      /* {...{
                        onChange: formEveManager.tableSwitchChange.bind(
                          this,
                          item.values.pk_allotrule.value
                        )
                      }} */
                    />
                  </span>
                </NCPopconfirm>
              </span>
              <sapn
                className="btn"
                onClick={e => {
                  this.editForm(item.values.pk_unit.value);
                }}
              >{/*"701001RWCL-0038": "修改"*/}
                {multiLang && multiLang.get('701001RWCL-0038')}
              </sapn>
              <sapn className="btn">
                {/*"701001RWCL-0039": "确认删除?"*/}
                <NCPopconfirm
                  placement="bottom"
                  content={multiLang && multiLang.get('701001RWCL-0039')}
                  id="delConfirmId"
                  onClose={() => {
                    this.delForm(item.values.pk_unit.value);
                  }}
                >
                  {/*"7010-0002": "删除",*/}
                  <span className="customer-label">{multiLang && multiLang.get('7010-0002')}</span>
                </NCPopconfirm>
              </sapn>
            </span>
          </div>
          <div className="grow">
            <div className="column-1-1">
                {/*"701001RWCL-0040": "对应组织"*/}
              <span className="grow-label-name">{multiLang && multiLang.get('701001RWCL-0040')}</span>
              <span className="grow-label-value">
                {item.values.pk_org.display}
              </span>
            </div>
          </div>
          <div className="grow">
            <div className="column-1-1">
                {/*"701001RWCL-0041": "备注"*/}
              <span className="grow-label-name">{multiLang && multiLang.get('701001RWCL-0041')}</span>
              <span className="grow-label-value">
                {item.values.remark ? item.values.remark.value : ""}
              </span>
            </div>
          </div>
        </div>
      );
    });
    return formList;
  };

  // 删除表单
  delForm = pk => {
    let newData = this.state.data.filter(item => {
      return pk === item.values.pk_unit.value;
    });
    let sscunitForm = { form: { areaType: "form", rows: newData } };
    let multiLang = this.props.MutiInit.getIntl(7010); //this.moduleId
    // 请求删除
    requestApi.delFormData({
      data: sscunitForm,
      success: res => {
        this.setState({ data: res ? res[this.formId].rows : [] });
          // "701001RWCL-0042": "删除成功"
        NCMessage.create({
            content: multiLang && multiLang.get('701001RWCL-0042'),
          color: "success",
          position: "bottomRight"
        });
      }
    });
  };
  // 修改表单
  editForm = pk => {
    this.setState({ showFormModal: true, editFormPk: pk });
    let editFormItem = this.state.data.filter(item => {
      return pk == item.values.pk_unit.value;
    });
    editFormItem = JSON.parse(JSON.stringify(editFormItem));
    this.props.form.setAllFormValue({ [this.formId]: { rows: editFormItem } });
  };

  onChange = (pk) => {
    // 请求启用/停止
    let newData = this.state.data.filter(item => {
      return pk === item.values.pk_unit.value;
    });
    if (newData[0].values.enablestate.value != "2") {
      let sscunitForm = { form: { areaType: "form", rows: newData } };
      let multiLang = this.props.MutiInit.getIntl(7010); //this.moduleId
      requestApi.enableSSCUnit({
        data: sscunitForm,
        success: res => {
          this.setState({ data: res ? res[this.formId].rows : [] });
            // "701001RWCL-0043": "启用成功"
          NCMessage.create({
            content: multiLang && multiLang.get('701001RWCL-0043'),
            color: "success",
            position: "topRight"
          });
        }
      });
    } else {
      newData[0].values.enablestate.value = "2";
      let sscunitForm = { form: { areaType: "form", rows: newData } };
      let multiLang = this.props.MutiInit.getIntl(7010); //this.moduleId
      requestApi.disableSSCUnit({
        data: sscunitForm,
        success: res => {
          this.setState({ data: res ? res[this.formId].rows : [] });
            // "701001RWCL-0044": "停用成功"
          NCMessage.create({
            content: multiLang && multiLang.get('701001RWCL-0044'),
            color: "success",
            position: "topRight"
          });
        }
      });
    }
  };

  render() {
    let {
      form: { createForm }
    } = this.props;
    let formLists = this.drawFormList();
    let multiLang = this.props.MutiInit.getIntl(7010); //this.moduleId
    return (
      <div className="container">
        <Nav defaultActIdx={"0"} {...this.props} />

        <div className="content-wrap">
          <div className="bar">
              {/*"701001RWCL-0045": "共享中心管理"*/}
              {createPageIcon()}
            <h2 className="title-search-detail">{multiLang && multiLang.get('701001RWCL-0045')}</h2>
            <div className="header-button-area">
              <NCButton
                colors="primary"
                onClick={buttonClick.addButtonClick.bind(this)}
              >
                  {/*"701001RWCL-0046": "新增"*/}
                  {multiLang && multiLang.get('701001RWCL-0046')}
              </NCButton>
            </div>
          </div>
          <div className="form-content">
            {formLists}
          </div>
          {/*新增/修改表单modal*/}
              <NCModal show={this.state.showFormModal} onHide={()=>{this.setState({showFormModal:false})}}  className='junior' backdrop='false'>
              <NCModal.Header closeButton={true} style={{height: '40px', background: '#F3F3F3', lineHeight: '40px'}}>
                  {/*"701001RWCL-0045": "共享中心管理",*/}
                  <NCModal.Title style={{fontSize: '13px', color: '#111111'}}>{multiLang && multiLang.get('701001RWCL-0045')}</NCModal.Title>
              </NCModal.Header>
              <NCModal.Body><div className="ssc_edit_modal_form">{createForm(this.formId)}</div></NCModal.Body>
              <NCModal.Footer>
                  {/*"7010-0003": "保存",*/}
                  {/*"7010-0004": "取消",*/}
                  <NCButton
                      colors="primary"
                      onClick={buttonClick.saveFormButtonClick.bind(this)}
                  >
                      {multiLang && multiLang.get('7010-0003')}
                  </NCButton>
                  <NCButton onClick={buttonClick.cancelFormButtonClick.bind(this)}>
                      {multiLang && multiLang.get('7010-0004')}
                  </NCButton>
              </NCModal.Footer>
          </NCModal>
        </div>
      </div>
    );
  }
}

let SSCUnitDom = createPage({
    mutiLangCode: '7010'
})(SSCUnit);
export default SSCUnitDom;
