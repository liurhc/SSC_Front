import requestApi from "../requestApi";
import { toast } from "nc-lightapp-front";

/**
 * 新增按钮点击
 */
let addButtonClick = function() {
  this.props.form.EmptyAllFormValue([this.formId]);
  this.setState({ showFormModal: true, editFormPk: "" });
};
/**
 * 保存按钮点击
 */
let saveFormButtonClick = function() {
  let formData = this.props.form.getAllFormValue(this.formId);
  let multiLang = this.props.MutiInit.getIntl(7010);
  // if(formData && formData.rows[0].values.code.value && formData.rows[0].values.code.value.length>10){
  //   toast({ content: multiLang && multiLang.get('701001RWCL-0167'),color: 'danger' })
  //   return;
  // }
    if(!this.props.form.isCheckNow(this.formId)){
        return;
    }
  if (formData.rows[0].values.enablestate.value === true || formData.rows[0].values.enablestate.value === "2") {
    formData.rows[0].values.enablestate.value = "2";
  } else {
    formData.rows[0].values.enablestate.value = "3";
  }
  let sscunitForm = { form: formData };
  requestApi.saveFormData({
    data: sscunitForm,
    success: res => {
      this.setState({
        data: res ? res[this.formId].rows : [],
        showFormModal: false
      });
    }
  });
};
/**
 * 取消按钮点击
 */
let cancelFormButtonClick = function() {
  this.setState({ showFormModal: false });
  this.props.form.EmptyAllFormValue([this.formId]);
};

export default { addButtonClick, saveFormButtonClick, cancelFormButtonClick };
