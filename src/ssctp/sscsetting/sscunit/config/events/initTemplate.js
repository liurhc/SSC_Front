export default function (props) {

    props.createUIDom(
        {
            pagecode: '700101GXZX_C',//页面编码
            appcode: '700101GXZX'//小应用编码
        },
        function (data) {
            let meta = data.template;
            // 设置表格编辑态
            meta["sscunit_card"].status = 'edit';
            props.meta.setMeta(meta);
        });
}
