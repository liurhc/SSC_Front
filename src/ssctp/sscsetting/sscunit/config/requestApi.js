import {ajax} from 'nc-lightapp-front';

let requestApi = {

    //单据模板接口
    tpl: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCUnitTempletAction.do',
            data: opt.data,
            success: opt.success
        });
    },

    //查询共享中心
    query:(opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCUnitQueryAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },

    // 保存表单
    saveFormData:(opt) =>{
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCUnitAddAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },

    // 删除表单
    delFormData:(opt)=>{
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCUnitDeleteAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            }
        })
    },

    //停用
    disableSSCUnit:(opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCUnitStopAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            },
        });
    },

    //启用
    enableSSCUnit:(opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCUnitStartAction.do',
            data: opt.data,
            success: (data) => {
                data = data.data;
                opt.success(data);
            },
        });
    }
    
}
export default  requestApi;