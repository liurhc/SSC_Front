import buttonClick from './buttonClick';
import initTemplate from './initTemplate';
import manager from './managerWorkUser';
import affixButtonClick from './affixButtonClick';
export { initTemplate, buttonClick, affixButtonClick, manager };