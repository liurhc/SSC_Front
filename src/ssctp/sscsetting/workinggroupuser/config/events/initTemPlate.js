import manager from "./managerWorkUser";
import User from "../../../../refer/sscbd/SSCSMUserGridRef/index.js";
import { base,getMultiLang } from "nc-lightapp-front";

const { NCTooltip } = base;
export default function(props) {
  getMultiLang({moduleId: 7010, domainName: 'ssctp',currentLocale: 'zh-CN', callback: (json) => {
    let _this = this;
    let meta = {
      treeTableCol: {
        moduletype: "table",
        pagination: false,
        items: [
          {
            label: json['701001RWCL-0051'],//作业组
            col: 12,
            width: "174px",
            attrcode: "pk_workinggroup",
            itemtype: "label",
            visible: true
          },
          {
            label: json['701001RWCL-0119'],//组长
            col: 12,
            width: "174px",
            attrcode: "chargeman",
            itemtype: "label",
            visible: true,
            render(text, record, index) {
              let { checkInfoArr, isEdit, addInfoArr, delInfoArr } = _this.state;
              let itemList = [];// 结果集
              let checkArr = [];// 选中人员pkArr
              let pk_workinggroup = record.values.pk_workinggroup.value;
              let searchItem =  "<span class='nc-ssctp-orgin-span-search'>" +_this.state.searchValue +"</span>";
              if (isEdit) {
                checkInfoArr.map(obj => {
                    if ("chargeman" == obj.usertype &&pk_workinggroup == obj.pk_workinggroup) {
                        checkArr.push(obj.pk_user);
                    }
                });
                let initInfoArr = manager.getWorkUserArrByGroupPk.call(_this,[pk_workinggroup],"chargeman");        
                let renderInfoArr = manager.getRenderWorkGroupInfoArr(initInfoArr,addInfoArr,delInfoArr,"chargeman");
                renderInfoArr[0].pk_userArr.map((pk_user, idx) => {
                  let deptName = renderInfoArr[0].deptNames[idx];  // 部门名称
                  let orgName =  renderInfoArr[0].orgNames[idx];   // 组织名称
                  let username = renderInfoArr[0].display[idx];    // 人员名称
                  let search = username.replace(_this.state.searchValue,searchItem);
                  if (_this.state.searchValue !== "" &&username.indexOf(_this.state.searchValue) > -1) {
                    // 查询
                    if (checkArr.indexOf(pk_user) > -1) {
                      // 选中
                      if (renderInfoArr[0].status[idx] == 0) {
                        // 已查询 已选中 原始
                        //701001RWCL-0092:无
                        itemList.push(
                          <NCTooltip inverse="true"  placement="top"overlay={<div>{orgName=="" ? json['701001RWCL-0092']:orgName}&nbsp;&nbsp;{deptName=="" ? json['701001RWCL-0092']:deptName}</div>}>
                            <span
                              dangerouslySetInnerHTML={{ __html: search }}
                              draggable="true"
                              onDragStart={manager.drag.bind(_this,deptName,orgName,pk_workinggroup,pk_user,username,"chargeman",false)}
                              className="nc-ssctp-orgin-span-check"
                            />
                          </NCTooltip>
                        );
                      } else if (renderInfoArr[0].status[idx] == 1) {
                        let tempSearchItem = "<span class='nc-ssctp-temp-span-search'>" +_this.state.searchValue +"</span>";
                        let tempSearch = username.replace( _this.state.searchValue,tempSearchItem);
                        // 已查询 已选中 临时
                        itemList.push(
                          <NCTooltip inverse="true"  placement="top"overlay={<div>{orgName=="" ? json['701001RWCL-0092']:orgName}&nbsp;&nbsp;{deptName=="" ? json['701001RWCL-0092']:deptName}</div>}>
                            <span
                              dangerouslySetInnerHTML={{ __html: tempSearch }}
                              draggable="true"
                              onDragStart={manager.drag.bind(_this,deptName,orgName,pk_workinggroup,pk_user,username,"chargeman",true)}
                              className="nc-ssctp-add-span-check"
                            />
                          </NCTooltip>
                        );
                      }
                    } else {
                      // 未选中
                      if (renderInfoArr[0].status[idx] == 0) {
                        // 已查询 未选中 原始
                        itemList.push(
                          <NCTooltip inverse="true"  placement="top"overlay={<div>{orgName=="" ? json['701001RWCL-0092']:orgName}&nbsp;&nbsp;{deptName=="" ? json['701001RWCL-0092']:deptName}</div>}>
                            <span
                              dangerouslySetInnerHTML={{ __html: search }}
                              draggable="true"
                              onDragStart={manager.drag.bind(_this,deptName,orgName,pk_workinggroup,pk_user,username,"chargeman", false)}
                              className="nc-ssctp-orgin-span-edit"
                              onClick={manager.checkWorkUser.bind(_this,deptName,orgName,pk_workinggroup,pk_user,username,"chargeman",false)}
                            />
                          </NCTooltip>
                        );
                      } else {
                        let tempSearchItem = "<span class='nc-ssctp-temp-span-search'>" +_this.state.searchValue +"</span>";
                        let tempSearch = username.replace( _this.state.searchValue,tempSearchItem);
                        // 已查询 未选中 临时
                        itemList.push(
                          <NCTooltip inverse="true"  placement="top"overlay={<div>{orgName=="" ? json['701001RWCL-0092']:orgName}&nbsp;&nbsp;{deptName=="" ? json['701001RWCL-0092']:deptName}</div>}>
                            <span
                              dangerouslySetInnerHTML={{ __html: tempSearch }}
                              draggable="true"
                              onDragStart={manager.drag.bind(_this,deptName,orgName,pk_workinggroup, pk_user,username,"chargeman",true)}
                              className="nc-ssctp-add-span-uncheck"
                              onClick={manager.checkWorkUser.bind(_this,deptName,orgName,pk_workinggroup,pk_user, username,"chargeman",true)}
                            />
                          </NCTooltip>
                        );
                      }
                    }
                  } else {
                    // 未查询
                    if (checkArr.indexOf(pk_user) > -1) {
                      // 选中
                      if (renderInfoArr[0].status[idx] == 0) {
                        // 未查询 已选中 原始
                        itemList.push(
                          <NCTooltip inverse="true"  placement="top"overlay={<div>{orgName=="" ? json['701001RWCL-0092']:orgName}&nbsp;&nbsp;{deptName=="" ? json['701001RWCL-0092']:deptName}</div>}>
                            <span
                              draggable="true"
                              onDragStart={manager.drag.bind(_this,deptName,orgName,pk_workinggroup,pk_user,username,"chargeman", false)}
                              className="nc-ssctp-orgin-span-check"
                            >
                              {username}
                            </span>
                          </NCTooltip>
                        );
                      } else if (renderInfoArr[0].status[idx] == 1) {
                        // 未查询 已选中 临时
                        itemList.push(
                          <NCTooltip inverse="true"  placement="top"overlay={<div>{orgName=="" ? json['701001RWCL-0092']:orgName}&nbsp;&nbsp;{deptName=="" ? json['701001RWCL-0092']:deptName}</div>}>
                            <span
                              draggable="true"
                              onDragStart={manager.drag.bind(_this,deptName,orgName,pk_workinggroup,pk_user,username,"chargeman",true)}
                              className="nc-ssctp-add-span-check"
                            >
                              {username}
                            </span>
                          </NCTooltip>
                        );
                      }
                    } else {
                      // 未选中
                      if (renderInfoArr[0].status[idx] == 0) {
                        // 未查询 未选中 原始
                        itemList.push(
                          <NCTooltip inverse="true"  placement="top"overlay={<div>{orgName=="" ? json['701001RWCL-0092']:orgName}&nbsp;&nbsp;{deptName=="" ? json['701001RWCL-0092']:deptName}</div>}>
                              <span
                                draggable="true"
                                onDragStart={manager.drag.bind(_this,deptName,orgName,pk_workinggroup,pk_user,username,"chargeman",false)}
                                className="nc-ssctp-orgin-span-edit"
                                onClick={manager.checkWorkUser.bind(_this,deptName,orgName,pk_workinggroup,pk_user,username,"chargeman",false)}
                              >
                                {username}
                              </span>
                          </NCTooltip>
                        );
                      } else {
                        // 未查询 未选中 临时
                        itemList.push(
                          <NCTooltip inverse="true"  placement="top"overlay={<div>{orgName=="" ? json['701001RWCL-0092']:orgName}&nbsp;&nbsp;{deptName=="" ? json['701001RWCL-0092']:deptName}</div>}>
                            <span
                              draggable="true"
                              onDragStart={manager.drag.bind(_this,deptName,orgName,pk_workinggroup,pk_user,username,"chargeman",true)}
                              className="nc-ssctp-add-span-uncheck"
                              onClick={manager.checkWorkUser.bind( _this,deptName,orgName,pk_workinggroup,pk_user,username,"chargeman",true)}
                            >
                              {username}
                            </span>
                          </NCTooltip>
                        );
                      }
                    }
                  }
                });
                return (
                  <div
                    className="nc-ssctp-orgin-div"
                    onDrop={manager.drop.bind(_this, pk_workinggroup)}
                    onDragOver={manager.allowDrop.bind(_this)}
                  >
                    {itemList}
                  </div>
                );
              } else {
                // 浏览态
                record.values.chargeman.display.map((item, idx) => {
                  let deptName =  record.values.chargeman.deptNames[idx];   // 部门名称
                  let orgName = record.values.chargeman.orgNames[idx];      // 组织名称
                  if (_this.state.searchValue !== "" &&item.indexOf(_this.state.searchValue) > -1) {
                    let search = item.replace(_this.state.searchValue,searchItem);
                    itemList.push(
                      <NCTooltip inverse="true"  placement="top" overlay={<div>{orgName=="" ? json['701001RWCL-0092']:orgName}&nbsp;&nbsp;{deptName=="" ? json['701001RWCL-0092']:deptName}</div>}>
                        <span
                          dangerouslySetInnerHTML={{ __html: search }}
                          className="nc-ssctp-orgin-span"
                        />
                      </NCTooltip>
                    );
                  } else {
                    itemList.push(
                      <NCTooltip inverse="true"  placement="top" overlay={<div>{orgName=="" ? json['701001RWCL-0092']:orgName}&nbsp;&nbsp;{deptName=="" ? json['701001RWCL-0092']:deptName}</div>}>
                        <span className="nc-ssctp-orgin-span">{item}</span>
                      </NCTooltip>
                    );
                  }
                });
                return <div className="nc-ssctp-orgin-div">{itemList}</div>;
              }
            }
          },
          {
            label: json['701001RWCL-0120'],//组员
            col: 12,
            width: "544px",
            attrcode: "member",
            itemtype: "label",
            visible: true,
            render(text, record, index) {
              let { checkInfoArr, isEdit, addInfoArr, delInfoArr } = _this.state;
              let itemList = [];// 结果集
              let checkArr = [];// 选中的人员pk
              let searchItem = "<span class='nc-ssctp-orgin-span-search'>" +_this.state.searchValue +"</span>";
              let pk_workinggroup = record.values.pk_workinggroup.value;
              /*末级节点*/
              if (record.isleaf) {
                /*编辑态*/
                if (isEdit) {
                  // 暂存选中的人员
                  checkInfoArr.map(obj => {
                    if ("member" == obj.usertype &&pk_workinggroup == obj.pk_workinggroup) {
                      checkArr.push(obj.pk_user);
                    }
                  });
                  // 返回作业组原始的人员
                  let initInfoArr = manager.getWorkUserArrByGroupPk.call(_this,[pk_workinggroup],"member");
                  // 返回作业组显示的人员  显示人员 = 原始人员 + 新增人员 - 删除人员
                  let renderInfoArr = manager.getRenderWorkGroupInfoArr(initInfoArr,addInfoArr,delInfoArr,"member");
                  renderInfoArr[0].pk_userArr.map((pk_user, idx) => {
                    let orgName =  renderInfoArr[0].orgNames[idx];     // 组织名称
                    let deptName = renderInfoArr[0].deptNames[idx];    // 部门名称
                    let username = renderInfoArr[0].display[idx];      // 人员名称
                    let search = username.replace( _this.state.searchValue,searchItem);
                    if (_this.state.searchValue !== "" &&username.indexOf(_this.state.searchValue) > -1) {
                      // 已查询
                      if (checkArr.indexOf(pk_user) > -1) {
                        // 已选中
                        if (renderInfoArr[0].status[idx] == 0) {
                          //已查询 已选中 原始
                          itemList.push(
                            <NCTooltip  inverse="true"  placement="top" overlay={<div>{orgName=="" ? json['701001RWCL-0092']:orgName}&nbsp;&nbsp;{deptName=="" ? json['701001RWCL-0092']:deptName}</div>}>
                              <span
                                dangerouslySetInnerHTML={{ __html: search }}
                                draggable="true"
                                onDragStart={manager.drag.bind(_this,deptName,orgName,pk_workinggroup,pk_user,username,"member",false)}
                                className="nc-ssctp-orgin-span-check"
                              />
                            </NCTooltip>
                          );
                        } else if (renderInfoArr[0].status[idx] == 1) {
                          let tempSearchItem = "<span class='nc-ssctp-temp-span-search'>" +_this.state.searchValue +"</span>";
                          let tempSearch = username.replace( _this.state.searchValue,tempSearchItem);
                          //已查询 已选中 临时
                          itemList.push(
                            <NCTooltip  inverse="true"  placement="top" overlay={<div>{orgName=="" ? json['701001RWCL-0092']:orgName}&nbsp;&nbsp;{deptName=="" ? json['701001RWCL-0092']:deptName}</div>}>
                              <span
                                dangerouslySetInnerHTML={{ __html: tempSearch }}
                                draggable="true"
                                onDragStart={manager.drag.bind(_this,deptName,orgName,pk_workinggroup,pk_user,username,"member",true)}
                                className="nc-ssctp-add-span-check"
                              />
                            </NCTooltip>
                          );
                        }
                      } else {
                        // 未选中
                        if (renderInfoArr[0].status[idx] == 0) {
                          // 已查询 未选中 原始
                          itemList.push(
                            <NCTooltip  inverse="true"  placement="top" overlay={<div>{orgName=="" ? json['701001RWCL-0092']:orgName}&nbsp;&nbsp;{deptName=="" ? json['701001RWCL-0092']:deptName}</div>}>
                              <span
                                dangerouslySetInnerHTML={{ __html: search }}
                                draggable="true"
                                onDragStart={manager.drag.bind(_this,deptName,orgName,pk_workinggroup, pk_user,username,"member",false)}
                                className="nc-ssctp-orgin-span-edit"
                                onClick={manager.checkWorkUser.bind(_this,deptName,orgName,pk_workinggroup, pk_user,username,"member",false)}
                              />
                            </NCTooltip>
                          );
                        } else {
                          let tempSearchItem = "<span class='nc-ssctp-temp-span-search'>" +_this.state.searchValue +"</span>";
                          let tempSearch = username.replace( _this.state.searchValue,tempSearchItem);
                          // 已查询 未选中 临时数据
                          itemList.push(
                            <NCTooltip  inverse="true"  placement="top" overlay={<div>{orgName=="" ? json['701001RWCL-0092']:orgName}&nbsp;&nbsp;{deptName=="" ? json['701001RWCL-0092']:deptName}</div>}>
                                <span
                                  dangerouslySetInnerHTML={{ __html: tempSearch }}
                                  draggable="true"
                                  onDragStart={manager.drag.bind(_this,deptName,orgName,pk_workinggroup,pk_user,username,"member",true)}
                                  className="nc-ssctp-add-span-uncheck"
                                  onClick={manager.checkWorkUser.bind(_this,deptName,orgName,pk_workinggroup,pk_user,username,"member",true)}
                                />
                            </NCTooltip>
                          );
                        }
                      }
                    } else {
                      // 未查询
                      if (checkArr.indexOf(pk_user) > -1) {
                        // 选中
                        if (renderInfoArr[0].status[idx] == 0) {
                          // 未查询 已选中 原始
                          itemList.push(
                            <NCTooltip  inverse="true"  placement="top" overlay={<div>{orgName=="" ? json['701001RWCL-0092']:orgName}&nbsp;&nbsp;{deptName=="" ? json['701001RWCL-0092']:deptName}</div>}>
                              <span
                                draggable="true"
                                onDragStart={manager.drag.bind(_this,deptName,orgName,pk_workinggroup,pk_user,username,"member",false)}
                                className="nc-ssctp-orgin-span-check"
                              >
                                {username}
                              </span>
                            </NCTooltip>
                          );
                        } else if (renderInfoArr[0].status[idx] == 1) {
                          // 未查询 已选中 临时数据
                          itemList.push(
                            <NCTooltip  inverse="true"  placement="top" overlay={<div>{orgName=="" ? json['701001RWCL-0092']:orgName}&nbsp;&nbsp;{deptName=="" ? json['701001RWCL-0092']:deptName}</div>}>
                              <span
                                draggable="true"
                                onDragStart={manager.drag.bind(_this,deptName,orgName,pk_workinggroup,pk_user,username,"member",true)}
                                className="nc-ssctp-add-span-check"
                              >
                                {username}
                              </span>
                            </NCTooltip>
                          );
                        }
                      } else {
                        // 未选中
                        if (renderInfoArr[0].status[idx] == 0) {
                          // 未查询 未选中 原始
                          itemList.push(
                            <NCTooltip  inverse="true"  placement="top" overlay={<div>{orgName=="" ? json['701001RWCL-0092']:orgName}&nbsp;&nbsp;{deptName=="" ? json['701001RWCL-0092']:deptName}</div>}>
                              <span
                                draggable="true"
                                onDragStart={manager.drag.bind(_this,deptName,orgName,pk_workinggroup,pk_user,username,"member", false)}
                                className="nc-ssctp-orgin-span-edit"
                                onClick={manager.checkWorkUser.bind(_this,deptName,orgName,pk_workinggroup,pk_user,username,"member",false)}
                              >
                                {username}
                              </span>
                            </NCTooltip>
                          );
                        } else {
                          // 未查询 未选中 临时
                          itemList.push(
                            <NCTooltip  inverse="true"  placement="top" overlay={<div>{orgName=="" ? json['701001RWCL-0092']:orgName}&nbsp;&nbsp;{deptName=="" ? json['701001RWCL-0092']:deptName}</div>}>
                              <span
                                draggable="true"
                                onDragStart={manager.drag.bind(_this,deptName,orgName,pk_workinggroup,pk_user,username,"member",true)}
                                className="nc-ssctp-add-span-uncheck"
                                onClick={manager.checkWorkUser.bind(_this,deptName,orgName,pk_workinggroup,pk_user,username,"member",true)}
                              >
                                {username}
                              </span>
                            </NCTooltip>
                          );
                        }
                      }
                    }
                  });
                  return (
                    <div
                      className="nc-ssctp-orgin-div"
                      onDrop={manager.drop.bind(_this, pk_workinggroup)}
                      onDragOver={manager.allowDrop.bind(_this)}
                    >
                      {itemList}
                    </div>
                  );
                } else {
                  /*浏览态*/
                  record.values.member.display.map((item, idx) => {
                    let orgName = record.values.member.orgNames[idx];
                    let deptName = record.values.member.deptNames[idx];
                    if (_this.state.searchValue !== "" &&item.indexOf(_this.state.searchValue) > -1) {
                      let search = item.replace(_this.state.searchValue,searchItem);
                      itemList.push(
                        <NCTooltip inverse="true"  placement="top" overlay={<div>{orgName=="" ? json['701001RWCL-0092']:orgName}&nbsp;&nbsp;{deptName=="" ? json['701001RWCL-0092']:deptName}</div>} >
                          <span
                            dangerouslySetInnerHTML={{ __html: search }}
                            className="nc-ssctp-orgin-span"
                          />
                        </NCTooltip>
                      );
                    } else {
                      itemList.push(
                        <NCTooltip inverse="true"  placement="top" overlay={<div>{orgName=="" ? json['701001RWCL-0092']:orgName}&nbsp;&nbsp;{deptName=="" ? json['701001RWCL-0092']:deptName}</div>} >
                          <span className="nc-ssctp-orgin-span">{item}</span>
                        </NCTooltip>
                      );
                    }
                  });
                  return <div className="nc-ssctp-orgin-div">{itemList}</div>;
                }
              } else {
                if (isEdit) {
                  // 编辑态父节点不显示人数
                } else {
                  /* 浏览态父节点显示人数 */
                  let count = 0;
                  let children = [];
                  manager.getLeafChildrenById.call(_this,pk_workinggroup,children);
                  children.map(child => {
                    count += child.member.length;
                  });
                  return (
                    //701001RWCL-0121：人
                    <div className="nc-ssctp-orgin-div">
                      <span className="nc-ssctp-orgin-span">{count + json['701001RWCL-0121']}</span>
                    </div>
                  );
                }
              }
            }
          },
          {
            label: json['701001RWCL-0060'],//操作
            col: 12,
            width: "106px",
            itemtype: "customer",
            attrcode: "opr",
            visible: true,
            render(text, record, index) {
              let pk_workinggroup = record.values.pk_workinggroup.value;
              /*编辑态*/
              if (_this.state.isEdit) {
                return (
                  <div className="currency-opr-col">
                    {record.isleaf ? (
                      <div
                        onClick={manager.checkAllWorkUser.bind(
                          _this,
                          record.values
                        )}
                      >
                        {json['701001RWCL-0122']}
                      </div>
                    ) : (
                      ""
                    )}
                  </div>
                );
              } else {
                /*浏览态*/
                return (
                  <div className="currency-opr-col">
                    {User({
                      onChange: manager.addWorkGroupUser.bind(_this,pk_workinggroup,"chargeman"),
                      clickContainer: <span>+{json['701001RWCL-0119']}</span>,//组长
                      unitProps: {
                        placeholder: json['701001RWCL-0059'],//业务单元
                        refName: json['701001RWCL-0059'],
                        refType: "tree",
                        refCode: "uapbd.refer.org.BusinessUnitTreeRef",
                        queryTreeUrl: "/nccloud/uapbd/org/BusinessUnitTreeRef.do",
                        isMultiSelectedEnabled: true
                      },
                      queryCondition:{pk_workinggroup:pk_workinggroup,usertype:'chargeman'},
                      isCacheable:false,
                      isShowUnit: true
                    })}
                    {record.isleaf
                      ? User({
                          onChange: manager.addWorkGroupUser.bind( _this,pk_workinggroup,"member"),
                          clickContainer: (<span>+{json['701001RWCL-0120']}</span>),//组员
                          unitProps: {
                            placeholder: json['701001RWCL-0059'],//业务单元
                            refName: json['701001RWCL-0059'],//业务单元
                            refType: "tree",
                            refCode: "uapbd.refer.org.BusinessUnitTreeRef",
                            queryTreeUrl: "/nccloud/uapbd/org/BusinessUnitTreeRef.do",
                            isMultiSelectedEnabled: true
                          },
                          queryCondition:{pk_workinggroup:pk_workinggroup,usertype:'member'},
                          isCacheable:false,
                          isShowUnit: true
                        })
                      : ""}
                  </div>
                );
              }
            }
          }
        ]
      }
    };
    props.meta.setMeta(meta);
  }})
}
