import requestApi from "../requestApi";
import { base,toast,promptBox } from "nc-lightapp-front";
const { NCMessage } = base;
/**
 * 管理
 */
let managerButtonClick = function() {
    this.setState({ isEdit: true });
};
/**
 * 保存
 */
let saveButtonClick = function() {
    let { addInfoArr, delInfoArr, initInfoArr } = this.state;
    let treeTableData = this.props.treeTableManyCol.getAllValue("treeTableCol");
    let multiLang = this.props.MutiInit.getIntl(7010); 
    console.log(treeTableData.length);
    //701001RWCL-0117：表格无可操作数据,不允许保存
    if(treeTableData.length==0){
        toast({ content: multiLang && multiLang.get('701001RWCL-0117'),color: 'danger' })
    }else {
        let data = {
            deletes: [],
            inserts: addInfoArr
        };
        initInfoArr.map(initInfo => {
            delInfoArr.map(delInfo => {
                if (delInfo.pk_workinggroup == initInfo.pk_workinggroup) {
                    if (delInfo.usertype == "chargeman") {
                        initInfo.chargeman.map(charge => {
                            if (delInfo.pk_user == charge.value) {
                                data.deletes.push(charge.vos);
                            }
                        });
                    } else {
                        initInfo.member.map(mem => {
                            if (delInfo.pk_user == mem.value) {
                                data.deletes.push(mem.vos);
                            }
                        });
                    }
                }
            });
        });
        requestApi.saveWorkGroupUser({
            data: data,
            success: res => {
                this.queryData(this.state.SSCCenter.refpk);
                //701001RWCL-0118：保存成功
                //NCMessage.create({ content: multiLang && multiLang.get('701001RWCL-0118'), color: "success", position: "bottomRight" });
                toast({color:"success"})
            }
        });
    }
    
};
/**
 * 取消
 */
let cancelButtonClick = function() {
    let multiLang = this.props.MutiInit.getIntl(7010);
    promptBox({
        color: 'warning',       // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
        title: multiLang && multiLang.get('7010-0004'),      //"提示信息"
        content:  multiLang && multiLang.get('701001RWCL-0170'),//确定要取消吗?
        beSureBtnClick: ()=>{
            this.setState({
                isEdit: false,
                checkInfoArr: [],
                addInfoArr: [],
                delInfoArr: []
            });
        }
    })
    
};

/**
 * @Author:gaoymf
 * @Description:导航条点击
 * @Date: 16:12 2018/9/15
 * @param:
 */

let handleTruncation = function(){
    let multiLang = this.props.MutiInit.getIntl(7010);
    promptBox({
        color: 'warning',       // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
        title: multiLang && multiLang.get('701001RWCL-0047'),      //"提示信息"
        content:  multiLang && multiLang.get('701001RWCL-0074'),//此操作会丢失本次修改,请确定?
        beSureBtnClick: ()=>{
                this.refs.nav.linkByClickedIdx();
        }
    })
}

export default { managerButtonClick, saveButtonClick, cancelButtonClick,handleTruncation };