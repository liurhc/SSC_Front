import { base,toast } from 'nc-lightapp-front';
import requestApi from '../requestApi'
const { NCMessage } = base;
/**
 * @Author:gaoymf
 * @Description:根据id查找末级节点,递归计算作业组人员
 * @Date: 14:00 2018/6/11
 * @param:
 */
let getLeafChildrenById = function(id, children) {
    let { initInfoArr } = this.state;
    initInfoArr.map((initInfo) => {
        if (initInfo.pid == id) {
            if (initInfo.isleaf) {
                children.push(initInfo);
            } else {
                getLeafChildrenById.call(this, initInfo.pk_workinggroup, children);
            }
        }
    })
};

/**
 * @Author:gaoymf
 * @Description:获得当前组下的人员 = 原始数据 + 新增 - 删除
 * @Date: 16:40 2018/6/7
 * @param:
 */
let getRenderWorkGroupInfoArr = function(initInfoArr, addInfoArr, delInfoArr, usertype) {
    // 原始的数据
    initInfoArr.map((workGroupInfo) => {
        // 新增的
        addInfoArr.map((addInfo) => {
            if (addInfo.usertype == usertype) {
                if (workGroupInfo.pk_workinggroup == addInfo.pk_workinggroup) {
                    workGroupInfo.orgNames.push(addInfo.orgName);
                    workGroupInfo.deptNames.push(addInfo.deptName);
                    workGroupInfo.pk_userArr.push(addInfo.pk_user);
                    workGroupInfo.status.push(1);
                    workGroupInfo.display.push(addInfo.display);
                }
            }
        })
        // 删除的
        delInfoArr.map((delInfo) => {
            if (delInfo.usertype == usertype) {
                if (workGroupInfo.pk_workinggroup == delInfo.pk_workinggroup) {
                    let index = workGroupInfo.pk_userArr.indexOf(delInfo.pk_user);
                    workGroupInfo.deptNames.splice(index, 1);
                    workGroupInfo.orgNames.splice(index, 1);
                    workGroupInfo.pk_userArr.splice(index, 1);
                    workGroupInfo.status.splice(index, 1);
                    workGroupInfo.display.splice(index, 1);
                }
            }
        })
    })
    return initInfoArr;
}

/**
 * @Author:gaoymf
 * @Description:根据组的pk获得末级对应的组长/组员用户
 * @Date: 9:38 2018/6/7
 * @param: groupArr 选中组的pk  usertype 选中的类型
 */
let getWorkUserArrByGroupPk = function(groupArr, usertype) {
    let { initInfoArr } = this.state;
    let workGroupInfoArr = [];
    groupArr.map((group) => {
        initInfoArr.map((initInfo) => {
            let workGroupInfo = {
                pid: initInfo.pid,     // 作业组pid
                orgNames: [],          // 组织名称
                deptNames: [],         // 部门名称
                pk_workinggroup: group,// 作业组pk
                pk_userArr: [],        // 人员pks
                display: [],           // 人员名称
                status: [],            // 人员状态 0是原始  1 是新增
                isleaf: initInfo.isleaf// 是否是末级作业组
            };
            if (initInfo.isleaf) {
                // 末级组返回组员和组长信息
                if (initInfo.pk_workinggroup == group) {
                    if ('member' === usertype) {  
                        // 组员 member
                        initInfo.member.map((obj) => {
                            workGroupInfo.display.push(obj.name);
                            workGroupInfo.pk_userArr.push(obj.value);
                            workGroupInfo.deptNames.push(obj.deptNames);
                            workGroupInfo.orgNames.push(obj.orgNames);
                            workGroupInfo.status.push(0);
                        })
                        workGroupInfoArr.push(workGroupInfo);
                    } else {
                         // 组长 chargeman
                        initInfo.chargeman.map((obj) => {
                            workGroupInfo.display.push(obj.name);
                            workGroupInfo.pk_userArr.push(obj.value);
                            workGroupInfo.deptNames.push(obj.deptNames);
                            workGroupInfo.orgNames.push(obj.orgNames);
                            workGroupInfo.status.push(0);
                        })
                        workGroupInfoArr.push(workGroupInfo);
                    }
                }
            } else {
                // 父级作业组只返回组长信息
                if (initInfo.pk_workinggroup == group) {
                    initInfo.chargeman.map((obj) => {
                        workGroupInfo.display.push(obj.name);
                        workGroupInfo.pk_userArr.push(obj.value);
                        workGroupInfo.deptNames.push(obj.deptNames);
                        workGroupInfo.orgNames.push(obj.orgNames);
                        workGroupInfo.status.push(0);
                    })
                    workGroupInfoArr.push(workGroupInfo);
                }
            }
        })
    })
    return workGroupInfoArr;
}
/**
 * @Author:gaoymf
 * @Description: 新增作业组用户
 * @Date: 10:30 2018/6/6
 * @param:groupArr:选中的组pk checkInfoArr:选中的人员 isDrag:标识是拖拽的还是选中引起的新增
 */
let addWorkUser = function(groupArr, checkInfoArr, isDrag) {
    let { delInfoArr, addInfoArr } = this.state;
    let multiLang = this.props.MutiInit.getIntl(7010); 
    // 目标组的信息
    let workGroupInfoArr = getWorkUserArrByGroupPk.call(this, groupArr, checkInfoArr[0].usertype);
    // 现在的数据=原始数据+新增的数据+删除的数据
    let nowWorkUserInfoArr = getRenderWorkGroupInfoArr(workGroupInfoArr, addInfoArr, delInfoArr, checkInfoArr[0].usertype);
    // 1.判断重复数据
    let repeatArr = [];
    nowWorkUserInfoArr.map((workUserInfo) => {
        checkInfoArr.map((checkInfo)=>{
            if(workUserInfo.pk_userArr.indexOf(checkInfo.pk_user)>-1){
                repeatArr.push(checkInfo.display);
            }
        })
    })
    // 存在重复的数据
    if(repeatArr.length>0){
        //701001RWCL-0123:'目标作业组下[' , 701001RWCL-0124:']人员已存在'
        toast({ content: multiLang && multiLang.get('701001RWCL-0123') + repeatArr + (multiLang && multiLang.get('701001RWCL-0124')), color: 'danger' })
        return false;
    }else {
        nowWorkUserInfoArr.map((workUserInfo) => {
            if (workUserInfo.isleaf) {
                checkInfoArr.map((checkInfo) => {
                    addInfoArr.push({
                        orgName:checkInfo.orgName,       // 组织名称
                        deptName:checkInfo.deptName,     // 部门名称
                        pk_workinggroup: workUserInfo.pk_workinggroup, // 作业组pk
                        usertype: checkInfo.usertype,                  // 人员类型
                        display: checkInfo.display,                    // 人员名称
                        pk_user: checkInfo.pk_user,                    // 人员pk
                    })
                })
            }
        })
        if (isDrag) {
            this.setState({ addInfoArr: addInfoArr });
        } else {
            this.setState({ addInfoArr: addInfoArr, checkInfoArr: [] });
        }
        return true;
    }
}
/**
 * @Author:gaoymf
 * @Description:删除作业组用户
 * @Date: 10:31 2018/6/6
 * @param: checkInfoArr:选中的人员 isDrag:标识是拖拽的还是选中的,此处用来控制选中是否清空
 */
let delWorkUser = function(checkInfoArr, isDrag) {
    let { delInfoArr, addInfoArr } = this.state;
    checkInfoArr.map((checkInfo) => {
        if (checkInfo.isTemp) {
            // 临时数据删除
            addInfoArr.map((addInfo, idx) => {
                if (addInfo.pk_workinggroup == checkInfo.pk_workinggroup) {
                    if (addInfo.pk_user == checkInfo.pk_user) {
                        addInfoArr.splice(idx, 1);
                    }
                }
            })
        } else {
            delInfoArr.push({
                pk_workinggroup: checkInfo.pk_workinggroup,
                pk_user: checkInfo.pk_user,
                usertype: checkInfo.usertype,
                display: checkInfo.display
            })
        }
    })
    if (isDrag) {
        this.setState({ delInfoArr: delInfoArr, addInfoArr: addInfoArr });
    } else {
        this.setState({ delInfoArr: delInfoArr, addInfoArr: addInfoArr, checkInfoArr: [] });
    }
}

/**
 * @Author:gaoymf
 * @Description:选中作业组用户
 * @Date: 10:32 2018/6/6
 * @param:
 */
let checkWorkUser = function(deptName,orgName,pk_workinggroup, pk_user, display, usertype, isTemp) {
    let { checkInfoArr } = this.state;
    let flag = true;
    let content;
    let multiLang = this.props.MutiInit.getIntl(7010); 
    // 组长/组员分开操作
    if (checkInfoArr.length > 0) {
        if(checkInfoArr[0].usertype!==usertype){
            flag = false;
            if(checkInfoArr[0].usertype=='chargeman'){
                content = multiLang && multiLang.get('701001RWCL-0126');//请继续选择组员
            }else {
                content = multiLang && multiLang.get('701001RWCL-0125');//请继续选择组长
            }
        }
        if(flag){
            checkInfoArr.map((checkInfo)=>{
                if(checkInfo.pk_user==pk_user){
                     flag = false;
                     content = display+'已选中,请勿重复选择';
                }
             })
        }
    }
    if (flag) {
        // 临时数据点击
        checkInfoArr.push({
            deptName:deptName,                // 部门名称
            orgName:orgName,                  // 组织名称
            pk_workinggroup: pk_workinggroup, // 组的pk
            usertype: usertype,               // 人员类型 member 组员 chargman  组长
            pk_user: pk_user, // 用户pk
            display: display, // 显示值
            isTemp: isTemp    // 原始数据:false   临时数据:true
        })
        this.setState({ checkInfoArr: checkInfoArr })
    } else {
        toast({ content: content, color: 'danger' })
    }

}
/**
 * @Author:gaoymf
 * @Description:全选
 * @Date: 16:28 2018/6/6
 * @param:
 */
let checkAllWorkUser = function(record) {
    let { checkInfoArr, addInfoArr, delInfoArr } = this.state;
    let multiLang = this.props.MutiInit.getIntl(7010); 
    // 获得原始数据
    let workGroupInfoArr = getWorkUserArrByGroupPk.call(this, [record.pk_workinggroup.value], 'member');
    // 获得当前数据
    let renderWorkUserInfoArr = getRenderWorkGroupInfoArr(workGroupInfoArr, addInfoArr, delInfoArr, 'member');
    // 维护已经选中的人员
    let hasCheckPKUserArr = []
    // 判断选中区是否有相同的人员
    let flag = true
    // 提示内容
    let content = [];
    renderWorkUserInfoArr[0].pk_userArr.map((pk_user, idx) => {
        if (checkInfoArr.length > 0) {
            // 校验相同的人员是否已经被选中了
            checkInfoArr.map((checkInfo) => {
                // 获得当前作业组已经选中的人员 hasCheckPKUserArr
                if (checkInfo.pk_workinggroup == renderWorkUserInfoArr[0].pk_workinggroup) {
                    if (checkInfo.pk_user == pk_user) {
                        hasCheckPKUserArr.push(pk_user);
                    }
                }else {
                    // 已选中中有不同作业组的相同人员
                    if (checkInfo.pk_user == pk_user) {
                        flag = false;
                        content.push(checkInfo.display);
                    }
                }
            })
        }
    })
    if(flag){
        renderWorkUserInfoArr[0].pk_userArr.map((pk_user, idx) => {
            // 已选中中没有的人员新增到选中中
            if (hasCheckPKUserArr.indexOf(pk_user) == -1) {
                checkInfoArr.push({
                    deptName:renderWorkUserInfoArr[0].deptName,                // 部门名称    
                    orgName:renderWorkUserInfoArr[0].orgName,                  // 组织名称
                    pk_workinggroup: renderWorkUserInfoArr[0].pk_workinggroup, // 组的pk
                    usertype: 'member', // 人员类型 member 组员 chargman  组长
                    pk_user: pk_user,   // 用户pk
                    display: renderWorkUserInfoArr[0].display[idx], // 显示值
                    isTemp: renderWorkUserInfoArr[0].status[idx] == 1 ? true : false // 1 表示新增 0 是原始
                })
            }
        })
    }else {
        //701001RWCL-0127：']已选中,请勿重复选择'
        toast({ content: "["+content+ multiLang && multiLang.get('701001RWCL-0127'), color: 'danger' })
    }
    
    this.setState({ checkInfoArr: checkInfoArr, addInfoArr: addInfoArr });
}
/**
 * @Author:gaoymf
 * @Description:删除选中区的人员
 * @Date: 14:22 2018/6/6
 * @param:pk_workinggroup 组pk  pk_user:人员pk
 */
let delCheckWorkUser = function(pk_workinggroup, pk_user) {
    let newCheckInfoArr = this.state.checkInfoArr.filter((item) => {
        if (item.pk_workinggroup == pk_workinggroup) {
            if (item.pk_user !== pk_user) {
                return true
            } else {
                return false;
            }
        } else {
            return true;
        }
    })
    this.setState({ checkInfoArr: newCheckInfoArr })
}
/**
 * @Author:gaoymf
 * @Description:新增人员
 * @Date: 14:22 2018/6/6
 * @param:pk_workinggroup 组pk  usertype:人员类型 infoArr:选中的数据
 */
let addWorkGroupUser = function(pk_workinggroup, usertype, infoArr) {
    let data = []
    if (infoArr.length > 0) {
        infoArr.map((info) => {
            data.push({
                pk_workinggroup: pk_workinggroup,
                usertype: usertype,
                pk_user: info.refpk
            })
        })
        requestApi.addWorkGroupUser({
            data: data,
            success: (res) => {
                this.queryData(this.state.SSCCenter.refpk);
            }
        })
    }
}

/**
 * @Author:gaoymf
 * @Description:拖拽人员放下时触发
 * @Date: 14:22 2018/8/8
 * @param:pk_workinggroup 组pk  
 */
let drop = function(pk_workinggroup, e) {
    e.preventDefault();
    // 目标组新增
    let flag = addWorkUser.call(this, [pk_workinggroup], this.dragInfoArr, true);
    // 源组删除
    if(flag){
        delWorkUser.call(this, this.dragInfoArr, true);
        // 清除选中区中的人员
        delCheckWorkUser.call(this, this.dragInfoArr[0].pk_workinggroup, this.dragInfoArr[0].pk_user);
        this.dragInfoArr = [];
    }
}
/**
 * @Author:gaoymf
 * @Description:目标区域阻止浏览器事件
 * @Date: 14:22 2018/8/8
 * @param:
 */
let allowDrop = function(e) {
    e.preventDefault();
}
/**
 * @Author:gaoymf
 * @Description:拖动时触发
 * @Date: 14:22 2018/8/8
 * @param:
 */
let drag = function(deptName,orgName,pk_workinggroup,pk_user,pk_username,usertype,isTemp,e) {
    let dragInfoArr = [];
    dragInfoArr.push({
        orgName:orgName,                    // 组织名称
        deptName:deptName,                  // 部门名称
        pk_workinggroup:pk_workinggroup,    // 组的pk
        usertype:usertype,                  // 人员类型 member 组员 chargman  组长
        pk_user:pk_user,                    // 用户pk
        display:pk_username,                // 显示值
        isTemp:isTemp                       // 原始数据 false  临时数据 true
    })
    this.dragInfoArr = dragInfoArr;
}
export default { addWorkUser, delWorkUser, checkWorkUser, checkAllWorkUser, delCheckWorkUser, getRenderWorkGroupInfoArr, getWorkUserArrByGroupPk, getLeafChildrenById, addWorkGroupUser,drag,drop,allowDrop }