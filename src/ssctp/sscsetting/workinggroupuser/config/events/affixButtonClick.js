import manager from "./managerWorkUser";

/**
 * @Author:gaoymf
 * @Description:清空
 * @Date: 14:00 2018/6/20
 */
let emptyButtonClick = function() {
    this.setState({ checkInfoArr: [] });
};

/**
 * @Author:gaoymf
 * @Description:移除
 * @Date: 14:00 2018/6/20
 */
let delButtonClick = function() {
    let { checkInfoArr } = this.state;
    manager.delWorkUser.call(this, checkInfoArr);
};

/**
 * @Author:gaoymf
 * @Description:确定
 * @Date: 14:43 2018/6/15
 * @param:
 */
let modalSaveButton = function(type, groupInfoArr) {
    let { checkInfoArr } = this.state;
    let groupArr = [];
    groupInfoArr.map((groupInfo)=>{
        groupArr.push(groupInfo.refpk);
    })
    // 已选用户且已选目标作业组
    if (checkInfoArr.length > 0 && groupArr.length > 0) {
        switch (type) {
            case "copy":
                manager.addWorkUser.call(this, groupArr, checkInfoArr);
                break;
            case "move":
                let flag = manager.addWorkUser.call(this, groupArr, checkInfoArr);
                if(flag){
                    manager.delWorkUser.call(this, checkInfoArr);
                }
                break;
            default:
                break;
        }
    }
};

export default { emptyButtonClick, delButtonClick, modalSaveButton };