import React, { Component } from 'react';
import { createPage, base, createPageIcon} from 'nc-lightapp-front';
import { buttonClick, initTemplate, manager, affixButtonClick } from './events';
import requestApi from './requestApi'
import SSCCenter from '../../../refer/sscbd/SSCUnitGridRef'
import SSCWorkGroup from '../../../refer/sscbd/SSCWorkGroupGridRef'
import { Nav } from 'ssccommon/components/global-nav'
import './index.less';
const { NCFormControl, NCAffix, NCButton,NCDiv: Div } = base;

class WorkingGroupUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isEdit:false,          // 编辑态/浏览态
            SSCCenter:{},          // 共享中心
            searchValue:'',        // 查询项
            initInfoArr:[],        // 原始的人员
            checkInfoArr:[],       // 选中的人员
            addInfoArr:[],         // 新增的人员
            delInfoArr:[],         // 删除的人员
        };
        this.dragInfoArr = [];    // 拖拽的信息
        initTemplate.call(this, props);
    }
    componentDidMount() {
        // 初始化表格
        this.props.treeTableManyCol.initTreeTableData('treeTableCol', [], '');
    }
    //共享中心改变事件
    onSSCCenterChange = (ssc) => {
        if (ssc.refpk) {
            this.setState({ SSCCenter: ssc });
            this.queryData(ssc.refpk);
        } else {
            this.setState({ SSCCenter: {} });
            this.props.treeTableManyCol.initTreeTableData('treeTableCol', [], '');
        }
    }
    // 查询datas
    queryData(pk_sscunit) {
        requestApi.queryWorkGroupUser({
            data:{pk_sscunit:pk_sscunit},
            success:(res)=>{
                //console.log('作业组用户查询',res);
                // 以下代码维护一个初始化数据
                let initInfoArr = []
                if(res.data.rows){
                    res.data.rows.map((node)=>{
                       // pid为null的节点为顶级节点
                       node.values.pid==null&&delete node.values.pid;
                    })
                    res.data.rows.map((item)=>{
                        let initInfo ={
                            pid:item.values.pid!==undefined&&item.values.pid.value,  // 父节点pk
                            pk_workinggroup:item.values.pk_workinggroup.value,       // 工作组pk
                            pk_workinggroup_name:item.values.pk_workinggroup.display,// 工作组名称
                            isleaf:item.values.isleaf.value,                         // 是否末级节点
                            chargeman:[],                                            // 组长数组
                            member:[],                                               // 组员数组
                        }
                        item.values.chargeman.display.map((name,idx)=>{
                            initInfo.chargeman.push({
                                    orgNames:item.values.chargeman.orgNames[idx],   // 组织名称
                                    deptNames:item.values.chargeman.deptNames[idx], // 部门名称
                                    name:name,                                      // 组长名称
                                    value:item.values.chargeman.value[idx],         // 组长pk
                                    vos:item.values.chargeman.vos[idx]              // 接口返回VO
                                });
                        })
                        item.values.member.display.map((name,idx)=>{
                            initInfo.member.push({
                                orgNames:item.values.member.orgNames[idx],          // 组织名称
                                deptNames:item.values.member.deptNames[idx],        // 部门名称
                                name:name,                                          // 组员名称
                                value:item.values.member.value[idx],                // 组员pk
                                vos:item.values.member.vos[idx]                     // 接口返回VO
                            });
                        })
                        initInfoArr.push(initInfo);
                    })
                    //console.log('作业组用户初始化数据',initInfoArr)
                    //同步时，将数据转换成组件需要的数据结构。需要先添加refpk(值为自身主键的值)、pid(值为父元素主键的值)两个字段。
                    let data = this.props.treeTableManyCol.createNewData(res.data.rows);
                    this.props.treeTableManyCol.initTreeTableData('treeTableCol', data, 'pk_workinggroup',true);
                    this.setState({ initInfoArr: initInfoArr, addInfoArr: [], delInfoArr: [] ,isEdit: false });
                }else {
                    this.props.treeTableManyCol.initTreeTableData('treeTableCol', [], '');
                }
                // 以上代码维护一个初始化数据
            }
        })
    }
    // 渲染锚点操作行
    drawCtrContent = () => {
        let itemList = [];
        this.state.checkInfoArr.map((item)=>{
            itemList.push(
                <span className="nc-ssctp-bottom-item">
                    {item.display}
                    <span   className="nc-ssctp-bottom-item-del-icon" 
                            onClick={manager.delCheckWorkUser.bind(this,item.pk_workinggroup,item.pk_user)}>x</span>
                </span>
            )
        })
        return itemList;
    }

    render() {
        const { treeTableManyCol } = this.props;
        let { treeTableCol } = treeTableManyCol;
        let itemContent = this.drawCtrContent();
        let multiLang = this.props.MutiInit.getIntl(7010);
        return (
            <div className="container">
                {/* 导航条*/}
                <Nav defaultActIdx={'3'} ref='nav' saveBeforeLink={this.state.isEdit} handleTruncation={buttonClick.handleTruncation.bind(this)}  {...this.props} />
                <div className="nc-single-table">
                <Div areaCode={Div.config.HEADER} style={{background: "#fff"}}>
                    {/* 头部 header */}
                    <div className="nc-singleTable-header-area">
                        <div className="header-title-search-area">
                            {/* 标题 title */}
                            {/* 701001RWCL-0129:作业组用户配置 */}
                            {createPageIcon()}
                            <h2  className="title-search-detail">{multiLang && multiLang.get('701001RWCL-0129')}</h2>
                            <div className="title-search-detail" style={{width:'200px'}}>
                                <span class="ssc-must">*</span>
                                {/*共享中心参照*/}
                                {SSCCenter({
                                    onChange:this.onSSCCenterChange.bind(this),
                                    value:this.state.SSCCenter,
                                    disabled:this.state.isEdit
                                })}
                            </div>
                            <div className="title-search-detail">
                                <NCFormControl
                                    placeholder={multiLang && multiLang.get('701001RWCL-0128')}//701001RWCL-0128:请搜索用户名称
                                    value={this.state.searchValue}
                                    onChange={(searchValue) => {
                                        this.setState({ searchValue:searchValue });
                                    }}
                                    type="search"
                                />
                            </div>
                        </div>
                        {/* 按钮区  btn-group */}
                        <div className="header-button-area">
                            {
                                this.state.isEdit ?
                                <span>
                                    <NCButton colors="primary" onClick={buttonClick.saveButtonClick.bind(this)}>{multiLang && multiLang.get('7010-0003')/*保存*/}</NCButton>
                                    <NCButton onClick={buttonClick.cancelButtonClick.bind(this)}>{multiLang && multiLang.get('7010-0004')}</NCButton>
                                </span>
                                :
                                <NCButton colors="primary" disabled={this.state.SSCCenter.refpk==undefined ?true:false} onClick={buttonClick.managerButtonClick.bind(this)}>{multiLang && multiLang.get('701001RWCL-0038')}</NCButton>//修改
                            }
                        </div>
                    </div>
                </Div>
                
                {/* 列表区 */}
                <div className="nc-singleTable-table-area">
                    {treeTableCol('treeTableCol', {
                        async: false,                           //数据同步加载为false,异步加载为true
                    })}
                    </div>  
                {
                    this.state.isEdit ?
                        <NCAffix> 
                            <div className="nc-ssctp-bottom-area">
                                <span className="nc-ssctp-bottom-count-area">
                                    {/* 701001RWCL-0130:已选 */}
                                    {multiLang && multiLang.get('701001RWCL-0130')}({this.state.checkInfoArr.length})
                                </span>
                                <span className="nc-ssctp-bottom-items-area">
                                    {itemContent}
                                </span>
                                <span className="nc-ssctp-bottom-button-area">
                                    {/* 701001RWCL-0131:清空 */}
                                    <NCButton onClick={affixButtonClick.emptyButtonClick.bind(this)}>{multiLang && multiLang.get('701001RWCL-0131')}</NCButton>
                                    {/* 复制到功能和作业组人员唯一有冲突 */}
                                    {/* 701001RWCL-0132:复制到 */}
                                    {SSCWorkGroup({
                                            onChange:affixButtonClick.modalSaveButton.bind(this,'copy'),
                                            isMultiSelectedEnabled:true,
                                            queryCondition:{pk_sscunit:this.state.SSCCenter.refpk},
                                            onlyLeafCanSelect:true,
                                            clickContainer:<NCButton>{multiLang && multiLang.get('701001RWCL-0132')}</NCButton>
                                    })}
                                    {/* 701001RWCL-0131:移动到 */}
                                    {SSCWorkGroup({
                                            onChange:affixButtonClick.modalSaveButton.bind(this,'move'),
                                            isMultiSelectedEnabled:true,
                                            queryCondition:{pk_sscunit:this.state.SSCCenter.refpk},
                                            onlyLeafCanSelect:true,
                                            clickContainer:<NCButton>{multiLang && multiLang.get('701001RWCL-0133')}</NCButton>
                                    })}
                                    <NCButton onClick={affixButtonClick.delButtonClick.bind(this)}>{multiLang && multiLang.get('7010-0002')/*删除*/}</NCButton>
                                </span>
                            </div>
                        </NCAffix> :''
                }
            </div>
            </div>
        )
    }
}
WorkingGroupUser = createPage({
    mutiLangCode: '7010'
})(WorkingGroupUser);
export default WorkingGroupUser;