import { ajax } from 'nc-lightapp-front';

let requestDomain = '';

let requestApiOverwrite = {
    // 新增组长/组员接口
    addWorkGroupUser: (opt) => {
        ajax({
            url: '/nccloud/sscbd/workinggroupuser/InsertAction.do',
            data: opt.data,
            success: opt.success
        });
    },
    // 查询作业组人员接口
    queryWorkGroupUser: (opt) => {
        ajax({
            url: '/nccloud/sscbd/workinggroupuser/QueryAction.do',
            data: opt.data,
            success: opt.success
        });
    },
    // 保存接口
    saveWorkGroupUser: (opt) => {
        ajax({
            url: '/nccloud/sscbd/workinggroupuser/ConvertUserAction.do',
            data: opt.data,
            success: opt.success
        });
    }
}

export default requestApiOverwrite;