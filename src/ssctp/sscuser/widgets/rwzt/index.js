import React, { Component } from 'react';
// import './index.less';
import {ajax ,createPage} from 'nc-lightapp-front'

class Test1 extends Component {
	constructor(props) {
		super(props);
		this.state = {
			res: {}
		}
	}
	componentDidMount() {
		ajax({
		  url:  '/nccloud/ssctp/sscbd/SSCTaskHandleRwztWidgetAction.do',
		  data: {},
		  loading: false,
		  success: (res) => {
		  	this.setState({res: res.data});
		  }
	  });
	}
	open = (event, para) => {
        let multiLang = this.props.MutiInit.getIntl(2052); //this.moduleId
		event.stopPropagation()
		if(para){
			// window.parent.openNew({code:'701001RWCL',name:'我的作业'}, null, {tab: para});
			this.props.openTo(
				'/ssctp/sscuser/sscuser/list/index.html',
				{
					pagecode: '701001RWCL_list',
					appcode: '701001RWCL',
                    // "701001RWCL-0010": "我的作业"
					name: multiLang && multiLang.get('701001RWCL-0010'),
					process: para
				}
			)
		}else{
			// window.parent.openNew({code:'701001RWCL',name:'我的作业'}, null);
			this.props.openTo(
				'/ssctp/sscuser/sscuser/list/index.html',
				{
					pagecode: '701001RWCL_list',
					appcode: '701001RWCL',
                    // "701001RWCL-0010": "我的作业"
					name: multiLang && multiLang.get('701001RWCL-0010')
				}
			)
		}
	}
	render() {
        let multiLang = this.props.MutiInit.getIntl(7010); //this.moduleId
		return (
			<div id="test" className="app1X2 platform-app number" onClick={(e) => {this.open(e, null)}}>
                {/*"701001RWCL-0025": "我的作业"*/}
				<div className="title">{multiLang && multiLang.get('701001RWCL-0025')}</div>
				<img className="img" src="/nccloud/resources/ssctp/public/image/ywicon2.png"/>
				<div className="content">
					<table>
						<tbody>
							<tr className="one" onClick={(e) => {this.open(e, 'ADJUST')}}>
                                {/*"701001RWCL-0014": "待调整"*/}
								<td className="title-one">{multiLang && multiLang.get('701001RWCL-0014')}</td>
								<td className="content-one" id = 'adjust'>{this.state.res.adjust}</td>
							</tr>
							<tr className="one" onClick={(e) => {this.open(e, 'SSCREJECT')}}>
                                {/*"701001RWCL-0012": "已驳回"*/}
								<td className="title-one">{multiLang && multiLang.get('701001RWCL-0012')}</td>
								<td className="content-one" id = 'sscreject'>{this.state.res.sscreject}</td>
							</tr>
							<tr className="one" onClick={(e) => {this.open(e, 'HANDON')}}>
                                {/*"701001RWCL-0013": "已挂起"*/}
								<td className="title-one">{multiLang && multiLang.get('701001RWCL-0013')}</td>
								<td className="content-one" id = 'handon'>{this.state.res.handon}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		);
	}
}
Test1 = createPage({
    mutiLangCode: '7010'
})(Test1)
export default Test1
// ReactDOM.render(<Test1 />, document.querySelector('#app'));
