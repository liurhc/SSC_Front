import React, { Component } from 'react';
import './index.less';
import echarts from 'echarts';
import {ajax,base,createPage,getMultiLang } from 'nc-lightapp-front'
import randomId from "../../../../sscrp/public/common/utils/GenNonDuplicateID";

const { NCSelect } = base;
const NCOption = NCSelect.NCOption;

let option = {};
let myChart;
function initoption (that) {
    //let multiLang = props.MutiInit.getIntl(7010); 
    option={
        color: ['#F08080','#50D166'],
        tooltip: {
            trigger: 'axis'
        },
        grid: {
            left: '15px',
            right: '15px',
            top:'25px',
            bottom:'25px',
            containLabel: true
        },
        xAxis: {
            type: 'category',
            boundaryGap: true,
            data: [],
            axisLine: {
                lineStyle: {
                    color: '#888888'
                }
            }
        },
        yAxis: {
            type: 'value',
            minInterval : 1,
            boundaryGap: [0, 0.01],
            axisLine: {
                lineStyle: {
                    color: '#888888'
                }
            }
        },
        series: [
            {
                name:that.state.json['701001RWCL-0005'],//701001RWCL-0005:驳回
                type:'line',
                data:[],
                smooth: true
            },
            {
                name:that.state.json['701001RWCL-0164'],//701001RWCL-0151:处理
                type:'line',
                data:[],
                areaStyle: {},
                smooth: true
            }
        ]
    }
};


// function deal(flag,groupid) {
//     let multiLang = this.props.MutiInit.getIntl(7010);
//     ajax({
//         url:  '/nccloud/ssctp/sscbd/SSCTaskHandleLineChartAction.do',
//         data: {"flag":flag,"groupid":groupid},
//         loading: false,
//         success: (res) => {
//             option.xAxis.data =res.data.x;
//             option.series[0].data = res.data.yreject;
//             option.series[1].data = res.data.yapproved;
//
//             let myChart = echarts.init(document.getElementById('test1'));
//             myChart.clear();
//             myChart.setOption(option,true);
//             let title_rank;
//             let title_handleCount;
//             let title_rejectCount;
//             if("week" == flag){
//                 title_rank = this.state.json['701001RWCL-0151'];//本周排名
//                 title_handleCount = this.state.json['701001RWCL-0152'];//本周处理
//                 title_rejectCount = this.state.json['701001RWCL-0153'];//本周驳回
//             }else if("month" == flag){
//                 title_rank = this.state.json['701001RWCL-0154'];//本月排名
//                 title_handleCount = this.state.json['701001RWCL-0155'];//本月处理
//                 title_rejectCount = this.state.json['701001RWCL-0156'];//本月驳回
//             }else if("year" == flag){
//                 title_rank = this.state.json['701001RWCL-0157'];//本年排名
//                 title_handleCount = this.state.json['701001RWCL-0158'];//本年处理
//                 title_rejectCount = this.state.json['701001RWCL-0159'];//本年驳回
//             }
//             document.getElementById('title_rank').innerHTML=(title_rank);
//             document.getElementById('title_handleCount').innerHTML=(title_handleCount);
//             document.getElementById('title_rejectCount').innerHTML=(title_rejectCount);
//             // document.getElementById('rank').innerHTML=(res.data.rank);
//             // document.getElementById('handleCount').innerHTML=(res.data.handleCount);
//             // document.getElementById('rejectCount').innerHTML=(res.data.rejectCount);
//             // document.getElementById('avgHandleMinutes').innerHTML=(res.data.avgHandleMinutes);
//             // document.getElementById('allHandleCount').innerHTML=(res.data.allHandleCount);
//             this.setState({
//
//             })
//         }
//     });
// }

 class Test1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectTab: 'week',
            activeKey:'',
            groups: [],
            json: {},
            res: {},
            title_rank: '',
            title_handleCount: '',
            title_rejectCount: ''
        }
        this.echartsId = randomId(36)
    }
    deal(flag,groupid) {
        let multiLang = this.props.MutiInit.getIntl(7010);
        ajax({
            url:  '/nccloud/ssctp/sscbd/SSCTaskHandleLineChartAction.do',
            data: {"flag":flag,"groupid":groupid},
            loading: false,
            success: (res) => {
                option.xAxis.data =res.data.x;
                option.series[0].data = res.data.yreject;
                option.series[1].data = res.data.yapproved;

                myChart.clear();
                myChart.setOption(option,true);
                if("week" == flag){
                    this.setState({
                        title_rank : this.state.json['701001RWCL-0151'],//本周排名
                        title_handleCount : this.state.json['701001RWCL-0152'],//本周处理
                        title_rejectCount : this.state.json['701001RWCL-0153']//本周驳回
                    })
                }else if("month" == flag){
                    this.setState({
                        title_rank : this.state.json['701001RWCL-0154'],//本月排名
                        title_handleCount : this.state.json['701001RWCL-0155'],//本月处理
                        title_rejectCount : this.state.json['701001RWCL-0156']//本月驳回
                    })
                }else if("year" == flag){
                    this.setState({
                        title_rank : this.state.json['701001RWCL-0157'],//本年排名
                        title_handleCount : this.state.json['701001RWCL-0158'],//本年处理
                        title_rejectCount : this.state.json['701001RWCL-0159'],//本年驳回
                    })
                }
                this.setState({
                    res: res.data
                })
            }
        });
    }

    componentWillMount() {
		let callback = (json) => {
			this.setState({json})
		}
		getMultiLang({moduleId: 7010, currentLocale: 'zh-CN',domainName: 'ssctp',callback})
	}    

    componentDidMount() {
        initoption(this)
        myChart = echarts.init(document.getElementById(this.echartsId));
        console.log('myChart' + myChart)
        console.log(this.echartsId)
        ajax({
            url:  '/nccloud/ssctp/sscbd/SSCTaskHandleLineChartAction.do',
            data: {},
            loading: false,
            success: (res) => {
                if(res.data && res.data.groups && res.data.groups[0]){
                    this.setState({groups: res.data.groups, activeKey: res.data.groups[0].groupid});
                }
                this.deal.call(this, 'week',this.state.activeKey);
            }
        });
    }

    handleClick(flag, event) {
        this.setState({selectTab:flag});
        this.deal.call(this, flag,this.state.activeKey);
    }
    selectTabChange(selectKey){
        this.setState({activeKey: selectKey});
        this.deal.call(this, this.state.selectTab,selectKey);
    }

    render() {
        let multiLang = this.props.MutiInit.getIntl(7010);
        let getSelectedtab = (type) => {
            if(type == this.state.selectTab){
                return 'selected';
            }
            return null;
        }
        return (
            <div id='test' className="app2X3 platform-app number">
                <div>
                     <div className="title">
                             {/*701001RWCL-0160：作业处理量*/}
                             {this.state.json['701001RWCL-0160']}
                         </div>
                     <div id='rwcll-select'>
                                {
                                    this.state.activeKey
                                        ?
                                        (
                                            <NCSelect
                                                defaultValue={this.state.activeKey}
                                                onChange={this.selectTabChange.bind(this)}
                                                style={{ width: 100, marginRight: 6 }}
                                                getPopupContainer={() => document.body}
                                            >
                                                {
                                                    this.state.groups.map(one => <NCOption value={one.groupid}>{one.groupname}</NCOption>)
                                                }

                                            </NCSelect>
                                        )
                                        :
                                        null
                                }
                            </div>
                </div>
               <div className="chart">
                   <div id={this.echartsId} className='processing'/>
                   <div id='test2'>
                       <div className="select-tab">
                           {/*"701001RWCL-0093": "周","701001RWCL-0094": "月","701001RWCL-0095": "年"*/}
                           <div className={getSelectedtab('week')} onClick={this.handleClick.bind(this, 'week')}>{this.state.json['701001RWCL-0093']}</div>
                           <div className={getSelectedtab('month')} onClick={this.handleClick.bind(this, 'month')}>{this.state.json['701001RWCL-0094']}</div>
                           <div className={getSelectedtab('year')} onClick={this.handleClick.bind(this, 'year')}>{this.state.json['701001RWCL-0095']}</div>
                       </div>
                       <div className="ranking">
                           <div className="one">
                               {/*"701001RWCL-0151": "本周排名","701001RWCL-0152": "本周处理","701001RWCL-0153":"本周驳回","701001RWCL-0030":"平均处理时间","701001RWCL-0161":"总处理量"*/}
                               <div className="title-one" id = 'title_rank'>{this.state.title_rank}</div>
                               <br/>
                               <div className="content-one" id = 'rank'>{this.state.res.rank}</div>
                           </div>
                           <div className="one">
                               <div className="title-one" id = 'title_handleCount'>{this.state.title_handleCount}</div>
                               <br/>
                               <div className="content-one" id = 'handleCount'>{this.state.res.handleCount}</div>
                           </div>
                           <div className="one">
                               <div className="title-one" id = 'title_rejectCount'>{this.state.title_rejectCount}</div>
                               <br/>
                               <div className="content-one" id = 'rejectCount'>{this.state.res.rejectCount}</div>
                           </div>
                           <div className="one">
                               <div className="title-one">{this.state.json['701001RWCL-0030']}</div>
                               <br/>
                               <div className="content-one" id = 'avgHandleMinutes'>{this.state.res.avgHandleMinutes}</div>
                           </div>
                           <div className="one">
                               <div className="title-one">{this.state.json['701001RWCL-0161']}</div>
                               <br/>
                               <div className="content-one" id = 'allHandleCount'>{this.state.res.allHandleCount}</div>
                           </div>
                       </div>
                   </div>
               </div>
            </div>
        );
    }
}
Test1 = createPage({
    mutiLangCode: "7010"
})(Test1);
export default Test1
// ReactDOM.render(<Test1 />, document.querySelector('#app'));
