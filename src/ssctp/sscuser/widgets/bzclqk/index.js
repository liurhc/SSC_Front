import React, { Component } from 'react';
import './index.less';
import {ajax,base,createPage, getMultiLang} from 'nc-lightapp-front'

const { NCSelect } = base;
const NCOption = NCSelect.NCOption;


 class Test1 extends Component {
	constructor(props) {
		super(props);
		this.state={
			activeKey:'',
			groups: [],
			json: {},
			res: {}
		}
	}

	componentWillMount() {

	}
	deal(groupid) {
        if(!groupid || '' == groupid){
            return;
        }
        ajax({
            url:  '/nccloud/ssctp/sscbd/SSCTaskHandleBzclqkWidgetAction.do',
            data: {"groupid":groupid},
            loading: false,
            success: (res) => {
				this.setState({res: res.data});
            }
        });
	}

	componentDidMount() {
		let callback = (json) => {
			this.setState({json});

			ajax({
				url:  '/nccloud/ssctp/sscbd/SSCTaskHandleBzclqkWidgetAction.do',
				data: {},
				async: false,
				loading: false,
				success: (res) => {
				  if(res.data && res.data.groups && res.data.groups[0]){
					  this.setState({groups: res.data.groups, activeKey: res.data.groups[0].groupid});
					  this.deal.call(this, res.data.groups[0].groupid);
				  }
				}
			});

		}
		getMultiLang({moduleId: 7010, currentLocale: 'zh-CN',domainName: 'ssctp',callback});
	}

	selectTabChange(selectKey){
		this.setState({activeKey: selectKey});
		this.deal.call(this, selectKey);
	}
	render() {
		return (
			<div id='test' className="app1X2 platform-app number">
				<table>
					<tbody>
						<tr>
							<td colSpan="5" className="title">
								{/*"701001RWCL-0029": "本周作业情况"*/}
								{this.state.json['701001RWCL-0029']}
							</td>
							<td id='bzclqk-select'>
								{
									this.state.activeKey
									?
									(
										<NCSelect
											defaultValue={this.state.activeKey}
											onChange={this.selectTabChange.bind(this)}
											style={{ width: 100, marginRight: 6 }}
											getPopupContainer={() => document.body}
										>
											{
												this.state.groups.map(one => <NCOption value={one.groupid}>{one.groupname}</NCOption>)
											}
											
										</NCSelect>
									)
									:
									null
								}
							</td>
						</tr>
						<tr id="bzclqk-content">
							<td className="one">
                                {/*701001RWCL-0150:单*/}
               			    	{/*<div id = 'handleCount' className="one-content">{(this.state.res.handleCount || '') + (this.state.json['701001RWCL-0150'] || '')}</div>*/}
               			    	<div id = 'handleCount' className="one-content">{this.state.res.handleCount ? (this.state.res.handleCount + this.state.json['701001RWCL-0150']) : '--'}</div>
                                {/*"701001RWCL-0015": "已处理"*/}
                  				<div className="one-title">{this.state.json['701001RWCL-0015']}</div>
							</td>
							<td className="biankuang"></td>
							<td className="avg-minutes">
         					    <div id = 'avgHandleMinutes' className="one-content">{this.state.res.avgHandleMinutes ? this.state.res.avgHandleMinutes : '--'}</div>
                                {/*"701001RWCL-0030": "平均处理时间"*/}
          					    <div className="one-title">{this.state.json['701001RWCL-0030']}</div>
							</td>
							<td className="biankuang"></td>
							<td className="one">
            				    <div id = 'rank' className="one-content">{this.state.res.rank ? this.state.res.rank : '--'}</div>
                                {/*"701001RWCL-0031": "组内排名"*/}
            				    <div className="one-title">{this.state.json['701001RWCL-0031']}</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		);
	}
}
Test1 = createPage({
    // initTemplate: initTemplate,
    mutiLangCode: '7010'
})(Test1)
export default Test1
// ReactDOM.render(<Test1 />, document.querySelector('#app'));
