import React, { Component } from 'react';
import './index.less';
import {ajax, base,createPage, getMultiLang,toast } from 'nc-lightapp-front'
const {NCMessage} = base;


class Test1 extends Component {
	constructor(props) {
		super(props);
		this.state = {
			json: {},
            res: {}
		}
	}
	componentWillMount() {
		let callback = (json) => {
			this.setState({json})
		}
		getMultiLang({moduleId: 7010, currentLocale: 'zh-CN',domainName: 'ssctp',callback})
	}	
	componentDidMount() {
		ajax({
			url:  '/nccloud/ssctp/sscbd/SSCTaskHandleRwclWidgetAction.do',
			  data: {},
			  loading: false,
			success: (res) => {
				this.setState({res: res.data});
		  	}
	  	});
	}
	take(event){
		event.stopPropagation()
		ajax({
			url:  '/nccloud/ssctp/sscbd/SSCTaskHandleRwclWidgetAction.do',
			data: {},
			success: (res) => {
				if("0" == res.data.notake){
					// "701001RWCL-0024": "暂无可提取任务"
					toast({duration:2,color:"warning",content:this.state.json['701001RWCL-0024']});
					//NCMessage.create({content: this.state.json['701001RWCL-0024'], color: 'warning', position: 'topRight'});
				}else{
					ajax({
						url: '/nccloud/ssctp/sscbd/SSCTaskHandleTakeAction.do',
						data: {},
						success: (data) => {
							// "701001RWCL-0023": "提取成功"
							toast({duration:2,color:"success",content:this.state.json['701001RWCL-0023']});
							//NCMessage.create({content: this.state.json['701001RWCL-0023'], color: 'success', position: 'bottomRight'});
							ajax({
								url:  '/nccloud/ssctp/sscbd/SSCTaskHandleRwclWidgetAction.do',
									data: {},
									success: (res) => {

									}
							});
						}
					})
				}
			}
		});
	}
	open = (event, para) => {
		event.stopPropagation()
		if(para){
			// window.parent.openNew({code:'701001RWCL',name:'我的作业'}, null, {tab: para});
			this.props.openTo(
				'/ssctp/sscuser/sscuser/list/index.html',
				{
					pagecode: '701001RWCL_list',
					appcode: '701001RWCL',
                    // "701001RWCL-0010": "我的作业"
					name: this.state.json['701001RWCL-0010'],
					process: para
				}
			)
		}else{
			// window.parent.openNew({code:'701001RWCL',name:'我的作业'}, null);
			this.props.openTo(
				'/ssctp/sscuser/sscuser/list/index.html',
				{
					pagecode: '701001RWCL_list',
					appcode: '701001RWCL',
                    // "701001RWCL-0010": "我的作业"
					name: this.state.json['701001RWCL-0010']
				}
			)
		}
	}
	render() {
		return (
			<div id="test" className="app1X2 platform-app number" onClick={(e) => {this.open(e, null)}}>
                {/*"701001RWCL-0010": "我的作业"*/}
				<div className="title">{this.state.json['701001RWCL-0010']}</div>
				<img id="homework" className="img" src="/nccloud/resources/ssctp/public/image/ywicon2.png"/>
				<div className="content">
					<table>
						<tbody>
							<tr className="">
                                {/*"701001RWCL-0014": "待调整"*/}
								<td className="title-one character" onClick={(e) => {this.open(e, 'ADJUST')}}>{this.state.json['701001RWCL-0014']}</td>
								<td className="content-one number" id = 'rwcladjust' onClick={(e) => {this.open(e, 'ADJUST')}}>{this.state.res.adjust}</td>
								{/*"701001RWCL-0012": "已驳回"*/}
								<td className="title-one character" onClick={(e) => {this.open(e, 'SSCREJECT')}}>{this.state.json['701001RWCL-0012']}</td>
								<td className="content-one number" id = 'sscreject' onClick={(e) => {this.open(e, 'SSCREJECT')}}>{this.state.res.sscreject}</td>
							</tr>
							<tr className="">
                                {/*"701001RWCL-0011": "待处理"*/}
								<td className="title-one character" onClick={(e) => {this.open(e, 'PENDING')}}>{this.state.json['701001RWCL-0011']}</td>
								<td className="content-one number" id = 'pending' onClick={(e) => {this.open(e, 'PENDING')}}>{this.state.res.pending}</td>
								{/*"701001RWCL-0013": "已挂起"*/}
								<td className="title-one character" onClick={(e) => {this.open(e, 'HANDON')}}>{this.state.json['701001RWCL-0013']}</td>
								<td className="content-one number" id = 'handon' onClick={(e) => {this.open(e, 'HANDON')}}>{this.state.res.handon}</td>
							</tr>
							<tr className="">
                                {/*"701001RWCL-0021": "待提取"*/}
								<td className="title-one character" onClick={(e) => {this.open(e)}}>{this.state.json['701001RWCL-0021']}</td>
								<td className="content-one number" id = 'rwclnotake' onClick={(e) => {this.open(e)}}>{this.state.res.notake}</td>
								<td colspan='2' className="take-btn" onClick={(e) => {this.take(e)}}>
                                    {/*"701001RWCL-0022": "提取任务"*/}
									{/*<i className="icon-xia iconfont icon-daitiqu"></i>*/}
                                    {this.state.json['701001RWCL-0022']}
								</td>
							</tr>
						</tbody>
					</table>
                    <img  src="/nccloud/resources/ssctp/public/image/tpbg.png"/>
				</div>
			</div>
		);
	}
}
Test1 = createPage({
    // initTemplate: initTemplate,
    mutiLangCode: '7010'
})(Test1)
export default Test1
// ReactDOM.render(<Test1 />, document.querySelector('#app'));
//