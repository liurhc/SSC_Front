import {listButtonClick} from './index'

export default function getListButtonRender(props){
    const that = this
    let r_render
    let multiLang = props.MutiInit.getIntl(7010); 
    switch(this.queryKey.taskstatus){
        case 'pending':
            r_render = (text, record, index) =>{
                return props.button.createOprationButton(
                    ['AdjustApply', 'HangUp', 'QueryOperation'], 
                    {
                        area: "sscuserlist",
                        onButtonClick: (props, btnKey) => {
                            listButtonClick(props, btnKey, text, record, index, that)
                        }
                    }
                )
            }
            break
        case 'handon':
            r_render = (text, record, index) =>{
                return props.button.createOprationButton(
                    ['CancelHangUp', 'QueryOperation'], 
                    {
                        area: "sscuserlist",
                        onButtonClick: (props, btnKey) => {
                            listButtonClick(props, btnKey, text, record, index, that)
                        }
                    }
                )
            }
            break
        case 'adjust':
            r_render = (text, record, index) =>{
                return props.button.createOprationButton(
                    ['CancelAdjust', 'QueryOperation'], 
                    {
                        area: "sscuserlist",
                        onButtonClick: (props, btnKey) => {
                            listButtonClick(props, btnKey, text, record, index, that)
                        }
                    }
                )
            }
            break
        case 'handled':
            r_render = (text, record, index) =>{
                return props.button.createOprationButton(
                    ['CancelHandled', 'QueryOperation'], 
                    {
                        area: "sscuserlist",
                        onButtonClick: (props, btnKey) => {
                            listButtonClick(props, btnKey, text, record, index, that)
                        }
                    }
                )
            }
            break
        case 'sscreject':
            r_render = (text, record, index) =>{
                return props.button.createOprationButton(
                    ['QueryOperation'], 
                    {
                        area: "sscuserlist",
                        onButtonClick: (props, btnKey) => {
                            listButtonClick(props, btnKey, text, record, index, that)
                        }
                    }
                )
            }
            break
        default:
            r_render = (text, record, index) =>{
                return props.button.createOprationButton(
                    ['QueryOperation'],
                    {
                        area: "sscuserlist",
                        onButtonClick: (props, btnKey) => {
                            listButtonClick(props, btnKey, text, record, index, that)
                        }
                    }
                )
            }
            break
    }
    return {
        label: multiLang && multiLang.get('701001RWCL-0060'),//操作
        itemtype: 'customer',
        attrcode: 'opr',
        visible: true,
        width: '205px',
        fixed: 'right',
        render: r_render
    }
}