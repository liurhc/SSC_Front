import requestApi from '../requestApi'
import {initTemplate} from './index'
import { pageTo } from 'nc-lightapp-front'

export default function buttonClick(key, index) {
    switch (key) {
        case 'TaskExtract': // 任务提取
            requestApi.taskTakeAction({
                success: (data) => {
                    if(data.success) {
                        initTemplate.call(this, this.props)
                    }
                }
            })
            break
        case 'approve':
            this.setState({
                //showModal: true, 
                didAction: false, 
                didApprove: this.queryKey.taskstatus === 'handled' ? true : false,
                alarm:false,
                hitMessage:''
            }, () => {
                // 信用
                let timer = null
                const wrap = document.getElementsByClassName('uf-jewel')[0]
                const float = document.getElementsByClassName('uf-jewel-float')[0]
                wrap.onmouseenter = () => {
                    if (timer) clearTimeout(timer)
                    float.style.display = 'block'
                }
                wrap.onmouseleave = () => {
                    if (timer) clearTimeout(timer)
                    timer = setTimeout(() => {
                        float.style.display = 'none'
                    }, 800)
                }
                float.onmouseenter = () => {
                    if (timer) clearTimeout(timer)
                    float.style.display = 'block'
                }
                float.onmouseleave = () => {
                    if (timer) clearTimeout(timer)
                    timer = setTimeout(() => {
                        float.style.display = 'none'
                    }, 800)
                }
            })
            this.state.currentRowData = this.data.listRows[index].values
            this.setState({activeIndex: index + 1})
            const _this = this
            requestApi.openBill({
                data: {
                    billtypeCode: _this.state.currentRowData.billtypecode.value, 
                    transtypeCode: _this.state.currentRowData.transtypecode.value,
                    billid: _this.state.currentRowData.busiid.value,
                    pk_group: _this.state.currentRowData.pk_group.value
                },
                success: (data) => {
                    // debugger
					this.setState({showModal: true})
                    let url = data.data.url
                    let params = data.data.data
                    
                    if (url.indexOf('#') > -1) {
                        if (url.indexOf('#/') > -1) {
                            if(url.indexOf('?') < 0) {
                                url += '?'
                            }
                        }
                    } else {
                        url += '#/?'
                    }
                    let start = url.indexOf('/nccloud/resources') > -1 ? '' : '/nccloud/resources'
                    let approveUrl = start + url + 'status=' + params.status + '&scene=zycl'
                    // if( this.queryKey.taskstatus === 'handled'){
                    //     approveUrl+='&deal=done';
                    // }
                    approveUrl += `&deal=${this.queryKey.taskstatus}`
                    for (let attr in params) {
                        if (attr != 'status') {
                            approveUrl += `&${attr}=${params[attr]}`
                        }
                    }
                    
                    _this.setState({approveUrl}, () => {
                        pageTo.addUrlParam({
                            c: params.appcode,
                            p: params.pagecode
                        })
                    })
                }
            })
            // 信用是否设置
            let isCreditInstall
            requestApi.queryCredits({
                queryMethod: 'isCreditInstall',
                success: (data) => {
                    isCreditInstall = data.data.isCreditInstall
                    this.setState({isCreditInstall})
                    if (isCreditInstall) {
                        // 信用等级
                        requestApi.queryCredits({
                            queryMethod: 'queryCreditLevel',
                            success: (data) => {
                                let levels = data.data
                                if (levels.length > 5) levels.length = 5
                                _this.setState({levels}, () => {
                                    // 查询信用信息详情
                                    requestApi.queryCredits({
                                        queryMethod: 'queryCreditInfo',
                                        billMakerId: _this.state.currentRowData.billmaker.value,
                                        success: (data) => {
                                            _this.setState({creditLevelInfo: data.data})
                                        }
                                    })
                                })
                            }
                        })
                    }
                }
            })

            //是否有预警消息
            requestApi.queryBudgetMessage({
                data:{
                    busiid:_this.state.currentRowData.busiid.value,
                    billtype:_this.state.currentRowData.transtypecode.value,
                },
                success:(data) => {
                    if(data.data&&data.data['alarm']){
                        this.setState({
                            alarm:data.data['alarm'],
                            hitMessage:data.data['message']
                        }) 
                    } 
                }
            })
            break
        default: 
            break
    }
}