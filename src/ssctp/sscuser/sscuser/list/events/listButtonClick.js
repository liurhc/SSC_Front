import requestApi from '../requestApi'
import {initTemplate} from './index'

export default function listButtonClick(props, btnKey, text, record, index, that) {
    switch(btnKey){
        case "AdjustApply": // 申请调整
            that.setState({showadjustmodel: true, adjustmodeldata: record})
            break
        case "HangUp": // 挂起
            that.setState({showhandonmodel: true, handonmodeldata: record})
            break
        case "CancelHangUp": // 取消挂起
            that.setState({
                showUnHandonModal: true,
                unHandonModalData: record
            })
            break
        case "CancelAdjust": // 取消调整
            that.setState({
                showUnAdjustModal: true,
                unAdjustModalData: record
            })
            break
        case "CancelHandled": // 取消审核
            requestApi.taskUnApproveAction({
                data: {
                    pk_task: record.pk_task.value,
                    actiontype: "un-sscapprove",
                    ts: record.ts.value
                },
                success: (data) => {
                    if (data.success) {
                        initTemplate.call(that, props)
                    }
                }
            })
            break
        case 'QueryOperation':
            that.props.table.setAllTableData('ssctaskhistory', { rows: [] })
            that.setState({showOperateNote: true})
            requestApi.taskQueryOperationAction({
                data: {
                    busiid: record.busiid.value
                },
                success: (data) => {
                    if(data.success && data.data.ssctaskhistory) {
                        that.props.table.setAllTableData('ssctaskhistory', data.data.ssctaskhistory)
                    }
                }
            })
            break
        default:
            break
    }
}


