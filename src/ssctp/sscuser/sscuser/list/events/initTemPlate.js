import requestApi from '../requestApi'
import getListButtonRender from './getListBtn'
import fixedTemplate from './fixedTemplate'
import {buttonClick} from './index'
import {getMultiLang} from 'nc-lightapp-front'
import Showtipss from './Showtipss'

export default function (props) {

    const _this = this
    
    props.createUIDom(
        {
        },
        function (data) {
            getMultiLang({moduleId: 7010, domainName: 'ssctp',currentLocale: 'zh-CN', callback: (json) => {
                _this.searchAreaConfig = {
                    searchArea_pending: data.template.searchArea_pending.items,
                    searchArea_adjust: data.template.searchArea_adjust.items,
                    searchArea_handled: data.template.searchArea_handled.items,
                    searchArea_handon: data.template.searchArea_handon.items,
                    searchArea_sscreject: data.template.searchArea_reject.items
                }
                let buttonTpl = {}
                buttonTpl = {
                    button: (data.button == null || data.button == undefined)?[]:data.button
                }
                //if (buttonTpl.button && buttonTpl.button.length > 0) {
                 props.button.setButtons(buttonTpl.button)
                //} else {
                //    props.button.setButtons(fixedTemplate.buttons)
                //}
            
                data.template[window.presetVar.listCode].pagination = false
                data.template[window.presetVar.list] = data.template[window.presetVar.listCode]
               
                data.template[window.presetVar.list].items.forEach(ele => {
                    ele.width = '200px'
                })

                // 列表操作列
              
                const listButtonRender = getListButtonRender.call(_this, props)
                
                const oprBtnsRender = listButtonRender.render
                data.template[window.presetVar.list].items.push(
                    listButtonRender
                )
               

                // 定向提取
                if (!_this.state.oid) {
                    _this.setState({
                        oid: data.template.search_Area.oid,
                        queryAreaCode: data.template.search_Area.code
                    })
                }
                let searchAreaItems = data.template.search_Area.items
                let idx = -1
                while (++idx < searchAreaItems.length) {
                    if (!searchAreaItems[idx].required) searchAreaItems[idx].visible = false
                }
                data.template['search_Area'] = {
                    moduletype: 'search',
                    items: searchAreaItems
                }

                data.template[window.presetVar.list].items.map((item)=>{
                    if(item.attrcode==='billno' 
                        || item.attrcode==='pk_tradetype'
                        || item.attrcode==='pk_currtype'
                        || item.attrcode==='remark'
                        || item.attrcode==='amount'
                        || item.attrcode==='billdate'
                        || item.attrcode==='billmaker'){

                        item.isSort = true;
                    }else{
                     
                        item.isSort = false;
                    }
                })
                    
                setTimeout(() => {
                    let event33 = {
                        label: '',
                        itemtype: 'customer',
                        attrcode: 'opr1',
                        visible: true,
                        width: '70px',
                        isSort:false,
                       
                        // render: (text, record, index) =>{
                        //     return 
                        // } 
                    };
                    data.template[window.presetVar.list].items.unshift(event33);
                    const prevMeta = props.meta.getMeta()
                    if (!prevMeta.code) {
                        props.meta.setMeta(data.template)
                    }
                    // 操作列自定义
                    props.table.setTableRender(window.presetVar.list, "opr", oprBtnsRender)
                    // 单据编码列自定义
                    props.table.setTableRender(window.presetVar.list, "billno", (text, record, index)=>{
                        let ret = [
                            (
                                <a className="billnoa" onClick={buttonClick.bind(_this, 'approve', index)}>
                                    {record.billno.value}</a>
                            )
                        ]
                       
                        return ret
                    })
                    //单据状态列
                    props.table.setTableRender(window.presetVar.list, "opr1", (text, record, index)=>{
                        
                        let showtips = [
                            
                        ]
                        let ret = [
                           
                        ]
                        if (_this.queryKey.taskstatus === 'handled') return ret
                        
                        if(record.urgent && record.urgent.value == 'Y'){
                            ret.push( <img src="/nccloud/resources/ssctp/public/image/urgent1.png"/>)//紧急
                            showtips.push( <div><img src="/nccloud/resources/ssctp/public/image/urgent1.png"/><span className="showtipss">{json['701001RWCL-0056']}</span></div>)//紧急                        
                        }
                        if(record.ismereject && record.ismereject.value == 'Y'){
                            ret.push( <img src="/nccloud/resources/ssctp/public/image/ismereject1.png"/>)//本人驳回
                            showtips.push(<div><img src="/nccloud/resources/ssctp/public/image/ismereject1.png"/><span className="showtipss">{json['701001RWCL-0057']}</span></div>)//本人驳回
                           
                        }
                        if(record.isleaderreject && record.isleaderreject.value == 'Y'){
                            ret.push( <img src="/nccloud/resources/ssctp/public/image/isleaderreject1.png"/>)//上级驳回
                            showtips.push(<div><img src="/nccloud/resources/ssctp/public/image/isleaderreject1.png"/><span className="showtipss">{json['701001RWCL-0058']}</span></div>)//上级驳回
                           
                        }
                        if(record.isappointed && record.isappointed.value == 'Y'){
                            ret.push( <img src="/nccloud/resources/ssctp/public/image/isappointed1.png"/>)//强制分配
                            showtips.push(<div><img src="/nccloud/resources/ssctp/public/image/isappointed1.png"/><span className="showtipss">{json['701001RWCL-0055']}</span></div>)//强制分配
                          
                        }
                        if(record.isunapprove && record.isunapprove.value == 'Y'){
                            ret.push( <img src="/nccloud/resources/ssctp/public/image/isunapprove1.png"/>)//取消审批
                            showtips.push( <div><img src="/nccloud/resources/ssctp/public/image/isunapprove1.png"/><span className="showtipss">{json['701001RWCL-0169']}</span></div>)//取消审批

                        }
                        let sign='sign'+index;
                        
                        return(
                            <Showtipss showtips={showtips} ret={ret}>
                            </Showtipss>
                                  
                        )
                        

                    })
                    // 查询列表数据
                    _this.queryData()
                }, 200)
            }})
        }
    )
}
