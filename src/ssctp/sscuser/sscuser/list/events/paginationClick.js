import requestApi from '../requestApi'
import { pageTo } from 'nc-lightapp-front'

export default function paginationClick(position, that) {
    let srcurl = that.state.approveUrl

    that.setState({activeIndex: position})
    that.state.currentRowData = that.data.listRows[position - 1].values
    requestApi.openBill({
        data: {
            billtypeCode: that.state.currentRowData.billtypecode.value, 
            transtypeCode: that.state.currentRowData.transtypecode.value,
            billid: that.state.currentRowData.busiid.value
        },
        success: (data) => {
            let url = data.data.url
            let params = data.data.data
            if (url.indexOf('#') > -1) {
                if (url.indexOf('#/') > -1) {
                    if(url.indexOf('?') < 0) {
                        url += '?'
                    }
                }
            } else {
                url += '#/?'
            }
            let start = url.indexOf('/nccloud/resources') > -1 ? '' : '/nccloud/resources'
            let approveUrl = start + url + 'status=' + params.status + '&scene=zycl'
            approveUrl += `&deal=${that.queryKey.taskstatus}`
            for (let attr in params) {
                if (attr != 'status') {
                    approveUrl += `&${attr}=${params[attr]}`
                }
            }
            that.setState({
                approveUrl, 
                reason: '', 
                didApprove: that.state.currentRowData.isApproved,
                didAction: false,
                alarm:false,
                hitMessage:''
            }, () => {
                pageTo.addUrlParam({
                    c: params.appcode,
                    p: params.pagecode
                })
                let src = srcurl.substr(srcurl.indexOf("/nccloud"));
                src = src.substr(0,src.indexOf("#"));
                if(src == approveUrl.substr(0,approveUrl.indexOf("#"))){
                    document.getElementById("approve-detail").contentWindow.location.reload(true);
                }
            })
            

            // 信用是否设置
            let isCreditInstall
            requestApi.queryCredits({
                queryMethod: 'isCreditInstall',
                success: (data) => {
                    isCreditInstall = data.data.isCreditInstall
                    that.setState({isCreditInstall})
                    if (isCreditInstall) {
                        // 信用等级
                        requestApi.queryCredits({
                            queryMethod: 'queryCreditLevel',
                            success: (data) => {
                                let levels = data.data
                                if (levels.length > 5) levels.length = 5
                                that.setState({levels}, () => {
                                    // 查询信用信息详情
                                    requestApi.queryCredits({
                                        queryMethod: 'queryCreditInfo',
                                        billMakerId: that.state.currentRowData.billmaker.value,
                                        success: (data) => {
                                            that.setState({creditLevelInfo: data.data})
                                        }
                                    })
                                })
                            }
                        })
                    }
                }
            })

            //是否有预警消息
            requestApi.queryBudgetMessage({
                data:{
                    busiid:that.state.currentRowData.busiid.value,
                    billtype:that.state.currentRowData.transtypecode.value
                },
                success:(data) => {
                    if(data.data&&data.data['alarm']){
                        that.setState({
                            alarm:data.data['alarm'],
                            hitMessage:data.data['message']
                        }) 
                    } 
                }
            })
        }
    })
    
}