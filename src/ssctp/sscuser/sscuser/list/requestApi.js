import {ajax } from 'nc-lightapp-front'

let requestApiOverwrite = {
    // 查询条件 模板查询
    querySSCTemplateAction: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCTaskTempletAction.do',
            data: opt.data,
            success: (data) => {
                opt.success(data.data.searchArea)
            }
        })
    },
    // 作业任务查询
    query: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCTaskHandleQueryAction.do',
            data: opt.data,
            success: (data) => {
                opt.success(data.data)
            }
        })
    },
    // 作业任务申请调整 作业任务挂起
    taskAdjustAction: (opt) => {   
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCTaskHandleAdjustAction.do',
            data: opt.data,
            success: (data) => {
                opt.success(data)
            }
        })
    },
    // 作业任务挂起
    taskHandOnAction: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCTaskHandleHandonAction.do',
            data: opt.data,
            success: (data) => {
                opt.success(data)
            }
        })
    },
    // 作业任取消调整
    taskUnAdjustAction: (opt) => {   
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCTaskHandleUnAdjustAction.do',
            data: opt.data,
            success: (data) => {
                opt.success(data)
            }
        })
    },
    // 作业任务取消挂起
    taskUnHandOnAction: (opt) => {      
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCTaskHandleUnHandonAction.do',
            data: opt.data,
            success: (data) => {
                opt.success(data)
            }
        })
    },
    // 作业任务提取
    taskTakeAction: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCTaskHandleTakeAction.do',
            data: opt.data || {},
            success: (data) => {
                opt.success(data)
            }
        })
    },
    // 作业任务驳回
    taskRejectAction: (opt) => {       
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCTaskHandleRejectAction.do',
            data: opt.data,
            success: (data) => {
                opt.success(data)
            }
        })
    },
    // 驳回活动
    getRejectActivityAction: (opt) => {       
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCTaskGetRejectActivityAction.do',
            data: opt.data,
            success: (data) => {
                opt.success(data)
            }
        })
    },
    // 作业任务审核
    taskApproveAction: (opt) => {  
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCTaskHandleApproveAction.do',
            data: opt.data,
            success: (data) => {
                opt.success(data)
            }
        })
    },
    // 作业任务联查操作记录
    taskQueryOperationAction: (opt) => { 
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCTaskHandleQueryOperationAction.do',
            data: opt.data,
            success: (data) => {
                opt.success(data)
            }
        })
    },
    // 获取iframe url
    openBill: (opt) => {
        ajax({
            url: '/nccloud/sscrp/rpbill/BrowseBillAction.do',
            data: opt.data,
            success: (data) => {
                opt.success(data)
            }
        });
    },
    // 作业任务取消审核
    taskUnApproveAction: (opt) => {        
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCTaskHandleUnApproveAction.do',
            data: opt.data,
            success: (data) => {
                opt.success(data)
            }
        })
    },
    // 模糊查询
    queryFuzzyKey: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCTaskBlurQueryAction.do',
            data: opt.data,
            success: data => {
                opt.success(data)
            }
        })
    },
    // 查询云端信用指标
    queryCredits: (opt) => {
        /***
         * @param queryMethod {String} 查询方法
         * 1、queryStandard（查询启用的信用标准）
         * 2、queryCreditRuleClass（查询信用指标分类）
         * 3、queryCreditRule（查询信用指标）
         * 4、queryCreditInfo（查询信用值信息）
         * 5、queryCreditLevel(查询信用等级)
         * 6、isCreditInstall（是否设置信用）
         * **/
        ajax({
            url: '/nccloud/ssctp/credit/creditAction.do',
            data: {method: opt.queryMethod, billMakerId: opt.billMakerId || ''},
            success: data => {
                opt.success(data)
            }
        })
    },
    //
    queryCreditRule: (opt) => {
        ajax({
            url: '/nccloud/ssctp/credit/creditRuleAction.do',
            data: {
                "pid": "",
                "keyword": "",
                "queryCondition": {
                    "isShowUnit": false
                },
                "pageInfo": {
                    "pageSize": 50,
                    "pageIndex": -1
                }
            },
            success: data => {
                opt.success(data)
            }
        })
    },
    
    //查询预算预警信息
    queryBudgetMessage:(opt) => {
        ajax({
            url:'/nccloud/ssctp/sscbd/SSCTaskBudgetMessagAction.do',
            data:opt.data,
            success: data => {
                opt.success(data)
            }
        })
    }
}

export default requestApiOverwrite