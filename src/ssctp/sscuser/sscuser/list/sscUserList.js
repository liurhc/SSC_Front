import React, {Component} from 'react'
import {createPage,promptBox, base, pageTo} from 'nc-lightapp-front'
const {NCModal, NCButton, NCFormControl, NCPagination, NCIcon, NCBackBtn, NCAffix,NCTooltip} = base
import {buttonClick, initTemplate, cardButtonClick, paginationClick, onRowDoubleClick} from './events'
import requestApi from './requestApi'
import './index.less'

import WorkbenchTab from 'ssccommon/components/workbench-tab'
import WorkbenchSearcharea from 'ssccommon/components/workbench-searcharea'
import WorkbenchDetail from 'ssccommon/components/workbench-detail'

import RejectDialog from './components/rejectDialog'
import HandonDialog from './components/handonDialog'
import AdjustDialog from './components/AdjustDialog'
import CreditApprovalDialog from './components/creditApproval'
import UnHandonDialog from './components/unHandonDialog'
import UnAdjustDialog from './components/unAdjustDialog'

import GetTask from './components/getTask'

import level1_1 from '../../../public/image/level1_1.png'
import level1_2 from '../../../public/image/level1_2.png'
import level2_1 from '../../../public/image/level2_1.png'
import level2_2 from '../../../public/image/level2_2.png'
import level3_1 from '../../../public/image/level3_1.png'
import level3_2 from '../../../public/image/level3_2.png'
import level4_1 from '../../../public/image/level4_1.png'
import level4_2 from '../../../public/image/level4_2.png'
import level5_1 from '../../../public/image/level5_1.png'
import level5_2 from '../../../public/image/level5_2.png'

const levelImgs = [
    {img1: level1_1, img2: level1_2},
    {img1: level2_1, img2: level2_2},
    {img1: level3_1, img2: level3_2},
    {img1: level4_1, img2: level4_2},
    {img1: level5_1, img2: level5_2}
]

window.presetVar = {
    ...window.presetVar,
    list: 'sscuserlist',
    card: 'sscusercard',
    listCode: 'ssctaskhandle'
}

class SscUserList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            showType: 'list',
            showNumbers:{},
            searcharea:{
                conditions:[]
            },
            pageControll: 'none',
            fuzzyquerychild: [],
            showModal: false,
            showdialogmodel:false,
            showradio:false,
            ncoptions:'',
            defaultOption:'',
            showOperateNote: false,
            currentRowData: null,
            approveUrl: '',
            reason: '',
            activeIndex: 1,
            didApprove: false,
            defaultKey:'pending',
            currentKey: 'pending',
            showhandonmodel: false,
            handonmodeldata: null,
            showadjustmodel: false,
            adjustmodeldata: null,
            showcreditmodel: false,
            creditmodeldata: null,
            creditsArray: [],
            levels: [],
            creditLevelInfo: {},
            isCreditInstall: true,
            showUnHandonModal: false,
            unHandonModalData: null,
            showUnAdjustModal: false,
            unAdjustModalData: null,
            creditView: false,
            didAction: false,
            oid: '',
            queryAreaCode: '',
            jkCheck:false,
            ntbCheck:false,
            skipcodes:'',
            alarm:false,
            hitMessage:'',
            creditRuleItems: {}
        }
        // 查询条件
        this.queryKey = {
            taskstatus: 'pending',
            dateRange:"", 
            fuzzyQueryKey: [],
            pageinfo:{
                number: 1,
                size: 20,
                totalElements: 0,
                totalPages: 0
            }
        }
        this.searchAreaConfig = {}
        this.data={
            listRows:[],
            oid: '',
            queryAreaCode: ''
        }
        this.canChangPageFlag = true
        this.fuzzyKey = ""
        // initTemplate.call(this, props)
    }
    componentWillMount() {
        // debugger
        const key = this.props.getUrlParam("process") ? this.props.getUrlParam("process").toLowerCase() : 'pending'
        const that = this
        this.setState({defaultKey: key, currentKey: key},() => {
            this.queryKey.taskstatus = key
            initTemplate.call(that, that.props)
        })
    }
    componentDidMount() {
        let iframeObj = document.getElementById('approve-detail');
        iframeObj.onload = () => {
            if (iframeObj.contentWindow) {
                iframeObj.contentWindow.document.body.style.padding = 0;
            }
        }
        // Firefox
        window.addEventListener('DOMMouseScroll', (ev) => {
            if (!this.state.showModal) {
                ev = ev || window.event
                let onOff = null
                if(ev.detail < 0){
                    onOff = true
                }else{
                    onOff = false
                }
                let bh = document.documentElement.clientHeight || document.body.clientHeight
                let bottom = document.getElementById('sscuser-simple-table').getElementsByTagName('table')[1].getBoundingClientRect().bottom
                if(
                    !onOff 
                    && bottom - bh < 45 
                    && this.queryKey.pageinfo.totalPages > this.queryKey.pageinfo.number 
                    && this.canChangPageFlag
                ) {
                    this.canChangPageFlag = false
                    setTimeout(() => { this.queryData(true) }, 300)
                }
            }
        })
        // 其他浏览器
        window.onmousewheel = (ev) => {
            if (!this.state.showModal) {
                ev = ev || window.event
                let onOff = null
                if(ev.wheelDelta > 0){
                    onOff = true
                }else{
                    onOff = false
                }
                let bh = document.documentElement.clientHeight || document.body.clientHeight
                let bottom = document.getElementById('sscuser-simple-table').getElementsByTagName('table')[1].getBoundingClientRect().bottom
                if(
                    !onOff 
                    && bottom - bh < 45 
                    && this.queryKey.pageinfo.totalPages > this.queryKey.pageinfo.number 
                    && this.canChangPageFlag
                ) {
                    this.canChangPageFlag = false
                    setTimeout(() => { this.queryData(true) }, 300)
                }
            }
        }
        window.onresize = () =>{
            if (!this.state.showModal) return true
            const billFrame = document.getElementById('approve-detail')
            if (billFrame) {
              let clientHeight = document.documentElement.clientHeight || document.body.clientHeight
              clientHeight -= 78
              billFrame.style.minHeight = `${clientHeight}px`
            }
            return true
        }
    }
    componentDidUpdate() {
        // iframe高度适配
        if (!this.state.showModal) return true
        const billFrame = document.getElementById('approve-detail')
        if (billFrame) {
          let clientHeight = document.documentElement.clientHeight || document.body.clientHeight
          clientHeight -= 78
          billFrame.style.minHeight = `${clientHeight}px`
        }
        return true
      }
    // 查询数据（参数：是否是翻页动作）
    queryData(isAddPage){
        if(isAddPage){
            this.queryKey.pageinfo.number++
        }else{
            this.queryKey.pageinfo.number = 1
        }
        requestApi.query({
            data: this.queryKey,
            success: (data) => {
                this.queryDataCallback(data, isAddPage)
            }
        })
    }
    queryDataCallback = (data, isAddPage = false) => {
        let listRows = []
        if(isAddPage){
            this.data.listRows.map((one)=>{
                listRows.push(this.copyData(one))
            })
        }else{
            this.refs.detail.clearAllDetailData()
        }
        data.tasklist.rows.map((one)=>{
            listRows.push(this.copyData(one))
        })
        this.data.listRows = listRows
        // 设置列表数据
        this.props.table.setAllTableData(window.presetVar.list, {
            areacode: window.presetVar.list,
            rows: listRows
        },false)
        // 设置缩略数据
        this.refs.detail.addDetailData(data.taskcard)
        let newState = {}
        newState = this.state
        // 更新未完成数量
        if(data.pending){
            newState.showNumbers.pending = data.pending
        }
        if(data.handled){
            newState.showNumbers.handled = data.handled
        }
        if(data.handon){
            newState.showNumbers.handon = data.handon
        }
        if(data.sscreject){
            newState.showNumbers.sscreject = data.sscreject
        }
        if(data.adjust){
            newState.showNumbers.adjust = data.adjust
        }
        // 更新查询条件区域
        let conditions = null
        switch(this.queryKey.taskstatus) {
            case 'pending':
                conditions = this.searchAreaConfig.searchArea_pending
            break
            case 'handled':
                conditions = this.searchAreaConfig.searchArea_handled
            break
            case 'handon':
                conditions = this.searchAreaConfig.searchArea_handon
            break
            case 'sscreject':
                conditions = this.searchAreaConfig.searchArea_sscreject
            break
            case 'adjust':
                conditions = this.searchAreaConfig.searchArea_adjust
            break
            default:
                conditions = this.searchAreaConfig.searchArea_pending
            break
        }
        let idx = -1
        conditions.forEach((ele, index) => {
            if (ele.attrcode === 'pk_tradetype') {
                idx = index
            }
        })
        if (idx != -1) {
            conditions[idx] = data.searchArea.items[0]
        }
        newState.searcharea.conditions = conditions
        // 更新页信息
        this.queryKey.pageinfo = data.tasklist.pageinfo
        if(this.queryKey.pageinfo.totalPages > this.queryKey.pageinfo.number){
            newState.pageControll = 'notend'
        }else if(this.queryKey.pageinfo.totalPages == this.queryKey.pageinfo.number){
            newState.pageControll = 'end'
        }else{
            newState.pageControll = 'none'
        }
        newState.creditView = data.isCreditInstall || false
        if (newState.creditView) {
            //查询规则
            this.queryCreditRuleItems();
        }
        this.setState(newState)
        setTimeout(()=>{this.canChangPageFlag=true;},100)
    }
    // 复制列表数据
    copyData(data){
        let newData = {}
        newData.values = {}
        for(let child in data.values){
            newData.values[child] = {
                display: data.values[child].display,
                value: data.values[child].value,
                scale: data.values[child].scale 
            }
        }
        return newData
    }
    // 已完成/未完成 页签切换
    selectTabChange(selectKey){
        this.queryKey.taskstatus = selectKey
        this.setState({currentKey: selectKey}, () => {
            initTemplate.call(this, this.props)
        })
    }
    // 查询条件选择变更
    handleConditionChange(attrcode, value){
        this.queryKey[attrcode]=value
        this.queryData()
    }
    // 列表/缩略切换
    showTypeClick(type){
        this.setState({showType: type})
    }
    // 查询操作记录
    queryOperNote(){
        this.props.table.setAllTableData('ssctaskhistory', { rows: [] })
        this.setState({showOperateNote: true})
        requestApi.taskQueryOperationAction({
            data: {
                busiid: this.state.currentRowData.busiid.value
            },
            success: (data) => {
                if(data.success && data.data.ssctaskhistory) {
                    this.props.table.setAllTableData('ssctaskhistory', data.data.ssctaskhistory)
                }
            }
        })
    }
    // 申请调整
    adjustApply() {
        this.setState({showadjustmodel: true, adjustmodeldata: this.state.currentRowData})
    }
    successAdjust = (data) => {
        if (data.success) {
            initTemplate.call(this, this.props)
            this.setState({showadjustmodel: false,showModal: false, reason: '', activeIndex: 1, currentKey: this.queryKey.taskstatus, didAction: true})
        }
    }
    successUnAdjust = (data) => {
        if (data.success) {
            initTemplate.call(this, this.props)
            this.setState({showUnAdjustModal: false,showModal: false, reason: '', activeIndex: 1, currentKey: this.queryKey.taskstatus})
        }
    }
    // 取消调整
    adjustUnApply() {
        this.setState({
            showUnAdjustModal: true,
            unAdjustModalData: this.state.currentRowData
        })
    }
    // 按钮 挂起
    hangUp() {
        this.setState({showhandonmodel: true, handonmodeldata: this.state.currentRowData})
    }
    successHangUp = (data) => {
        if (data.success) {
            initTemplate.call(this, this.props)
            this.setState({showhandonmodel: false, showModal: false, reason: '', activeIndex: 1, currentKey: this.queryKey.taskstatus, didAction: true})
            top.location.reload(); 
        }
    }
    successUnHangUp = (data) => {
        if (data.success) {
            initTemplate.call(this, this.props)
            this.setState({showUnHandonModal: false, showModal: false, reason: '', activeIndex: 1, currentKey: this.queryKey.taskstatus})
        }
    }
    // 取消挂起
    cancelHangUp() {
        this.setState({
            showUnHandonModal: true,
            unHandonModalData: this.state.currentRowData
        })
    }
    reasonOnChange(val) {
        if (val.length > 200) return
        this.setState({reason: val})
    }
     // 批准
     taskApprove() {
        let multiLang = this.props.MutiInit.getIntl(7010); 
        requestApi.taskApproveAction({
            data: {
                pk_task: this.state.currentRowData.pk_task.value,
                reason: this.state.reason,
                billtype:this.state.currentRowData.billtypecode.value,
                billno:this.state.currentRowData.billno.value,
                busiid:this.state.currentRowData.busiid.value,
                skipcodes:this.state.skipcodes,
                actiontype: "sscapprove",
                ntbCheck:this.state.ntbCheck,
                jkCheck:this.state.jkCheck,
                ts: this.state.currentRowData.ts.value
            },
            success: (data) => {
                if (data.success) {
                    if(data.data&&data.data['jkAlarm']){
                        promptBox({
                            color: 'warning',       // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                            title: multiLang && multiLang.get('701001RWCL-0047'),//"提示信息"
                            content:  (multiLang && multiLang.get('701001RWCL-0048'))+ data.data['jkAlarm'] + (multiLang && multiLang.get('701001RWCL-0049')),
                            noFooter: false,  // 是否显示底部按钮(确定、取消),默认显示(false),非必输
                            noCancelBtn: false,    // 是否显示取消按钮,，默认显示(false),非必输
                            beSureBtnName:  multiLang && multiLang.get('7010-0001'),
                            cancelBtnName: multiLang && multiLang.get('7010-0004'),
                            beSureBtnClick: ()=>{
                                this.state.jkCheck=true;
                                this.taskApprove();
                            }
                            //cancelBtnClick: functionCancel  // 取消按钮点击调用函数,非必输
                        })
                    }else if(data.data&&data.data['bugetAlarm']){ 
                        promptBox({
                            color: 'warning',       // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                            title: multiLang && multiLang.get('701001RWCL-0047'),//"提示信息"
                            content:  (multiLang && multiLang.get('701001RWCL-0048'))+ data.data['bugetAlarm'] + (multiLang && multiLang.get('701001RWCL-0049')),
                            noFooter: false,  // 是否显示底部按钮(确定、取消),默认显示(false),非必输
                            noCancelBtn: false,    // 是否显示取消按钮,，默认显示(false),非必输
                            beSureBtnName:  multiLang && multiLang.get('7010-0001'),
                            cancelBtnName: multiLang && multiLang.get('7010-0004') ,
                            beSureBtnClick: ()=>{
                                this.state.ntbCheck = true;
                                this.state.skipcodes = data.data['skipcodes']
                                this.taskApprove();
                            }
                            //cancelBtnClick: functionCancel  // 取消按钮点击调用函数,非必输
                        })
                    } else {
                        this.setState({didApprove: true, approveUrl: ''}, () => {
                        requestApi.openBill({
                            data: {
                                billtypeCode: this.state.currentRowData.billtypecode.value, 
                                transtypeCode: this.state.currentRowData.transtypecode.value,
                                billid: this.state.currentRowData.busiid.value
                            },
                            success: (data) => {
                                let url = data.data.url
                                let params = data.data.data
                                if (url.indexOf('#') > -1) {
                                    if (url.indexOf('#/') > -1) {
                                        if(url.indexOf('?') < 0) {
                                            url += '?'
                                        }
                                    }
                                } else {
                                    url += '#/?'
                                }

                                this.state.currentRowData.isApproved = true;

                                let start = url.indexOf('/nccloud/resources') > -1 ? '' : '/nccloud/resources'
                                let approveUrl = start + url + 'status=' + params.status + '&scene=zycl&deal=handled'
                                for (let attr in params) {
                                    if (attr != 'status') {
                                        approveUrl += `&${attr}=${params[attr]}`
                                    }
                                }
                                this.setState({approveUrl}, () => {
                                    pageTo.addUrlParam({
                                        c: params.appcode,
                                        p: params.pagecode
                                    })
                                })
                            }
                        })
                    })
                 }
                    // document.getElementById('approve-detail').contentWindow.location.reload(true)
                }
            }
        })
        
    }
    // 取消批准
    taskUnApprove() {
        requestApi.taskUnApproveAction({
            data: {
                pk_task: this.state.currentRowData.pk_task.value,
                actiontype: "un-sscapprove",
                ts: this.state.currentRowData.ts.value
            },
            success: (data) => {
                if (data.success) {
                    this.state.ntbCheck = false;
                    this.state.jkCheck  = false;
                    this.setState({didApprove: false, approveUrl: ''}, () => {
                        requestApi.openBill({
                            data: {
                                billtypeCode: this.state.currentRowData.billtypecode.value, 
                                transtypeCode: this.state.currentRowData.transtypecode.value,
                                billid: this.state.currentRowData.busiid.value
                            },
                            success: (data) => {
                                let url = data.data.url
                                let params = data.data.data
                                if (url.indexOf('#') > -1) {
                                    if (url.indexOf('#/') > -1) {
                                        if(url.indexOf('?') < 0) {
                                            url += '?'
                                        }
                                    }
                                } else {
                                    url += '#/?'
                                }

                                this.state.currentRowData.isApproved = false;

                                let start = url.indexOf('/nccloud/resources') > -1 ? '' : '/nccloud/resources'
                                let approveUrl = start + url + 'status=' + params.status + '&scene=zycl&deal=pending'
                                for (let attr in params) {
                                    if (attr != 'status') {
                                        approveUrl += `&${attr}=${params[attr]}`
                                    }
                                }
                                this.setState({approveUrl}, () => {
                                    pageTo.addUrlParam({
                                        c: params.appcode,
                                        p: params.pagecode
                                    })
                                })
                            }
                        })
                    })
                    // document.getElementById('approve-detail').contentWindow.location.reload(true)
                }
            }
        })
    }
    // 按钮 驳回 
    taskReject() {
        requestApi.getRejectActivityAction({
            data:{
                billid:this.state.currentRowData.busiid.value,
                billtype:this.state.currentRowData.transtypecode.value
            },
            success:(data)=>{
                if(data.success){
                    this.setState({
                        showdialogmodel: true, 
                        operationData:this.state.currentRowData,
                        showradio:data.data.showradio,
                        ncoptions:data.data.NCOptions,
                        defaultOption:data.data.default
                    })
                    // this.refs.rejectDialog.setDefaultDefOption(data.data.default || '')
                }
            }
        })
    }
    // 分页点击
    paginationClick(position) {
        paginationClick(position, this)
    }
    // 模糊查询输入内容变化
    onFuzzyChange(data){
        this.fuzzyKey = data
        let newQueryKey = {}
        let multiLang = this.props.MutiInit.getIntl(7010)
        for(let child in this.queryKey){
            newQueryKey[child] = this.queryKey[child]
        }
        newQueryKey.fuzzyQueryKey = data
        setTimeout((that, data)=>{
            if(data == that.fuzzyKey){
                requestApi.queryFuzzyKey({
                    data: newQueryKey,
                    success: (data) => {
                        for(let childName in data.data){
                            data.data[childName].value = data.data[childName].value+"="+that.fuzzyKey;
                            data.data[childName].key = data.data[childName].key+(multiLang && multiLang.get('701001RWCL-0061'))+that.fuzzyKey;//包含
                        }
                        that.setState({fuzzyquerychild:data.data})
                    }
                })
            }
        },500, this, data)
    }
    // 模糊查询选择
    onFuzzySelected(data){
        this.setState({fuzzyquerychild:[]})
        this.queryKey.fuzzyQueryKey = data
        this.queryData()
    }
    // 日期范围查询
    onPickDateChange(date){
        this.setState({pickDate:date});
        this.queryKey.dateRange=date;
        this.queryData();
    }

    backToUserList(multiLang) {
        let $frame = document.querySelector("#approve-detail");
        let frameWin = $frame.contentWindow;
        let targetDom = frameWin.document.querySelector(".nc-bill-form-area .form-component-item-wrapper");
        if(targetDom && targetDom.className.includes('edit')) {
            alert(multiLang && multiLang.get('701004RWCL-0173'));
            return;
        }
  
        this.setState({
            showModal: false,
            reason: '',
            activeIndex: -1,
            approveUrl: '',
            currentKey: this.queryKey.taskstatus
        })
        // this.queryKey = {//作业处理卡片返回列表保留查询条件
        //     taskstatus: this.queryKey.taskstatus,
        //     fuzzyQueryKey: [],
        //     pageinfo:{
        //         number: 1,
        //         size: 20,
        //         totalElements: 0,
        //         totalPages: 0
        //     }
        // }
        initTemplate.call(this, this.props)
        // 重置查询区焦点
        // this.refs.searchArea.resetState()//作业处理卡片返回列表保留查询条件
    }
    taskTakeAction = (params) => {
        const {
            queryAreaCode,
            oid
        } = this.data
        
        requestApi.taskTakeAction({
            data: {
                querycondition: params,
                oid,
                queryAreaCode,
                querytype: 'tree',
                pagecode: window.presetVar.listCode
            },
            success: (data) => {
                if(data.success) {
                    initTemplate.call(this, this.props)
                }
            }
        })
    }
    // 删除信用指标
    deleteCreditByIdx = (idx) => {
        let newarr = this.state.creditsArray.filter((each, index) => index != idx)
        this.setState({creditsArray: newarr})
    }
    setCheckedCredits = (arr, mixin) => {
        this.setState({
            creditsArray: arr
        })
    }

    setCreditRuleItems = (data) => {
        this.setState({
            creditRuleItems: data
        })
    }
    queryCreditRuleItems = () => {
        requestApi.queryCreditRule({
            success: (data) => {
                let creditRuleItems = {};
                let rules = data.data.rows
                rules.forEach((rule) => {
                    if (!rule.pid) {
                        creditRuleItems[rule.refpk] || (creditRuleItems[rule.refpk] = {item: null, children: []})
                        creditRuleItems[rule.refpk].item = rule
                    } else {
                        creditRuleItems[rule.pid] || (creditRuleItems[rule.pid] = {item: null, children: []})
                        creditRuleItems[rule.pid].children.push(rule)
                    }
                })
                this.setState({
                    creditRuleItems
                })
            }
        });
    }

// 处理排序后的模板变化
handleTempletAfterSort (sortParam){
    if(sortParam[0].order=='flatscend'){
        this.queryKey.orderByInfo = [];
    }else {
        this.queryKey.orderByInfo = sortParam;
    }
    let sortObj = {};
    sortParam.forEach(item => {
         sortObj[item.field] = item;
    });
    let meta = this.props.meta.getMeta()
    meta[window.presetVar.list].items.forEach(item => {
        //保存返回的column状态，没有则终止order状态
        if(sortObj[item.attrcode]){
            item.order = sortObj[item.attrcode].order;
            item.orderNum = sortObj[item.attrcode].orderNum;
        }else {
            item.order = "flatscend";
            item.orderNum = "";
        }
    })
    this.props.meta.setMeta(meta);
}

    render() {
        const {button, table} = this.props
        const {createSimpleTable} = table
        const {queryAreaCode, oid, didApprove, defaultKey, currentKey, levels, creditLevelInfo, isCreditInstall} = this.state
        let multiLang = this.props.MutiInit.getIntl(7010); //this.moduleId
        let isShow = (type) =>{
            if(this.state.showType == "list" && type == "list"){
                return "show";
            }else if(this.state.showType == "card" && type == "card"){
                return "show";
            }else{
                return "hide";
            }
        }
        // 信用评级
        let img1 = ''
        let img2 = ''
        if (isCreditInstall && creditLevelInfo.creditscore) {
            const {creditscore} = creditLevelInfo
            let imgIdx = -1
            levels.forEach((ele, idx) => {
                if (creditscore == ele.limitscore) imgIdx = idx
            })
            if (imgIdx == -1) imgIdx = levels.length - 1
            if (imgIdx != -1) {
                img1 = levelImgs[imgIdx].img1
                img2 = levelImgs[imgIdx].img2
            }
        }

        let addMoreOnOff = this.data.listRows.length > 0 && this.queryKey.pageinfo.totalPages > this.queryKey.pageinfo.number

        let getDetailBtns = (data, index) =>{
            switch(this.queryKey.taskstatus){
                case 'pending':
                    return ['AdjustApply', 'HangUp', 'QueryOperation']
                case 'handon':
                    return ['CancelHangUp', 'QueryOperation']
                case 'adjust':
                    return ['CancelAdjust', 'QueryOperation']
                case 'handled':
                case 'sscreject':
                    return ['QueryOperation']
                default:
                    return ['AdjustApply', 'HangUp', 'QueryOperation']
            }
        }

        let hintContent = <div style={{maxWidth: '380px'}}>{this.state.hitMessage}</div>

        const pending = (
            <div className="approve-controls">
                {/* reason */}
                {
                    !this.state.didAction
                    &&
                    (
                        <NCFormControl
                            value={this.state.reason}
                            onChange={this.reasonOnChange.bind(this)} 
                            className={didApprove ? 'hide' : 'reason-control'}                   
                        />
                    )
                }
                {
                    !this.state.didAction
                    &&
                    <span className={didApprove ? 'hide reason-amount' : 'show reason-amount'}>
                    {this.state.reason.length}/200</span>
                }
                {
                   this.state.alarm?<NCTooltip defaultOverlayShown inverse placement="bottom" overlay={hintContent}>
                    <span className="iconfont icon-yujing"></span>
                </NCTooltip> : ""
                }
                {/* 批准 */}
                {
                    !this.state.didAction
                    &&
                    <NCButton
                    className={didApprove ? 'hide' : 'show'}
                    colors="primary"
                    // "701001RWCL-0003": "批准"
                    onClick={this.taskApprove.bind(this)} >{multiLang && multiLang.get('701001RWCL-0003')}</NCButton>
                }
                {
                    !this.state.didAction
                    &&
                    <NCButton
                        className={didApprove ? 'show' : 'hide'}
                        onClick={this.taskUnApprove.bind(this)}
                        // "701001RWCL-0004": "取消批准"
                    >{multiLang && multiLang.get('701001RWCL-0004')}</NCButton>
                }
                {/*"701001RWCL-0037": "信用批准"*/}
                {/* {
                    !this.state.didAction
                    &&
                    <NCButton
                        className={didApprove || !isCreditInstall ? 'hide' : 'show'}
                        onClick={() => {
                            this.setState({showcreditmodel: true, creditmodeldata: this.state.currentRowData})
                        }}
                    >
                        {multiLang && multiLang.get('701001RWCL-0037')}
                    </NCButton>
                } */}
                {
                    !this.state.didAction
                    &&
                    <NCButton
                    className={didApprove ? 'hide' : 'show'}
                    // "701001RWCL-0005": "驳回"
                    onClick={this.taskReject.bind(this)}>{multiLang && multiLang.get('701001RWCL-0005')}</NCButton>
                }
                {
                    !this.state.didAction
                    &&
                    <NCButton
                    className={didApprove ? 'hide' : 'show'}
                    // "701001RWCL-0006": "挂起"
                    onClick={this.hangUp.bind(this)}>{multiLang && multiLang.get('701001RWCL-0006')}</NCButton>
                }
                {
                    !this.state.didAction
                    &&
                    <NCButton
                    className={didApprove ? 'hide' : 'show'}
                    // "701001RWCL-0007": "申请调整"
                    onClick={this.adjustApply.bind(this)}>{multiLang && multiLang.get('701001RWCL-0007')}</NCButton>
                }
                {/* 操作记录 页签 */}
                <NCButton 
                    onClick={() => {
                        this.queryOperNote.call(this)
                        // "701001RWCL-0008": "操作记录"
                    }}
                >{multiLang && multiLang.get('701001RWCL-0008')}</NCButton>
                <NCPagination
                    className="approve-pagination"
                    first
                    last
                    prev
                    next
                    boundaryLinks
                    items={this.data.listRows.length}
                    activePage={this.state.activeIndex}
                    onSelect={this.paginationClick.bind(this)}
                />
            </div>
        )

        const sscreject = (
            <div className="approve-controls">
                <NCButton 
                    onClick={() => {
                        this.queryOperNote.call(this)
                        // "701001RWCL-0008": "操作记录"
                    }}>{multiLang && multiLang.get('701001RWCL-0008')}</NCButton>
                <NCPagination
                    className="approve-pagination"
                    first
                    last
                    prev
                    next
                    boundaryLinks
                    items={this.data.listRows.length}
                    activePage={this.state.activeIndex}
                    onSelect={this.paginationClick.bind(this)}
                />
            </div>
        )

        const handon = (
            <div className="approve-controls">
                <NCButton
                    // "701001RWCL-0018": "取消挂起"
                    onClick={this.cancelHangUp.bind(this)}>{multiLang && multiLang.get('701001RWCL-0018')}</NCButton>
                <NCButton 
                    onClick={() => {
                        this.queryOperNote.call(this)
                        // "701001RWCL-0008": "操作记录"
                    }}>{multiLang && multiLang.get('701001RWCL-0008')}</NCButton>
                <NCPagination
                    className="approve-pagination"
                    first
                    last
                    prev
                    next
                    boundaryLinks
                    items={this.data.listRows.length}
                    activePage={this.state.activeIndex}
                    onSelect={this.paginationClick.bind(this)}
                />
            </div>
        )

        const adjust = (
            <div className="approve-controls">
                <NCButton
                    // "701001RWCL-0017": "取消调整"
                    onClick={this.adjustUnApply.bind(this)}>{multiLang && multiLang.get('701001RWCL-0017')}</NCButton>
                <NCButton 
                    onClick={() => {
                        this.queryOperNote.call(this)
                        // "701001RWCL-0008": "操作记录"
                    }}>{multiLang && multiLang.get('701001RWCL-0008')}</NCButton>
                <NCPagination
                    className="approve-pagination"
                    first
                    last
                    prev
                    next
                    boundaryLinks
                    items={this.data.listRows.length}
                    activePage={this.state.activeIndex}
                    onSelect={this.paginationClick.bind(this)}
                />
            </div>
        )

        return (
            <div id="sscUserList" className={addMoreOnOff ? 'add-more-wrap' : ''}>
            
                <div className="approvel-details" style={{display: this.state.showModal ? 'block' : 'none'}}>
                    <div className="approve-header clearfix">
                        <div className="back">
                            <NCBackBtn onClick={ this.backToUserList.bind(this, multiLang) }></NCBackBtn>
                        </div>

                        {/*"701001RWCL-0002": "审批情况"*/}
                        <div className="approve-title">
                            {multiLang && multiLang.get('701001RWCL-0002')}
                        </div>
                        <div className='uf-jewel-wrap'
                            style={{display: this.state.isCreditInstall ? 'inline-block' : 'none'}}
                        >
                            <img
                                src={img1}
                                className='uf-jewel'
                            />
                            <div className='uf-jewel-float'>
                                <i className='uf-t'></i>
                                <div className='content'>
                                    <div className='content-left'>
                                        <img src={img2}/>
                                    </div>
                                    <div className='content-right'>
                                        <div className='user-name'>
                                            {this.state.creditLevelInfo.name}
                                        </div>
                                        <div className='credit-level'>
                                            {/*"701001RWCL-0036": "信用："*/}
                                            {multiLang && multiLang.get('701001RWCL-0036')}
                                            {this.state.creditLevelInfo.creditlevel}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {
                            (() => {
                                switch(this.queryKey.taskstatus) {
                                    case 'pending':
                                        return pending
                                    case 'sscreject':
                                        return sscreject
                                    case 'handon':
                                        return handon
                                    case 'adjust':
                                        return adjust
                                    case 'handled':
                                        return pending
                                    default:
                                        return pending
                                }
                            })()
                        }
                    </div>
                    <div className='approve-body'>
                        <iframe
                            frameborder='0'
                            id="approve-detail"
                            src={this.state.approveUrl}
                        >
                            {/*"701001RWCL-0019": "浏览器不支持iframe标签"*/}
                            {multiLang && multiLang.get('701001RWCL-0019')}
                        </iframe>
                    </div>
                </div>

                <div className="container" style={{display: this.state.showModal ? 'none' : 'block'}}>
                    <NCAffix offsetTop={0}>
                        <div className="workbench-tab">
                            {/* 作业任务状态 */}
                            <WorkbenchTab
                                defaultActiveKey={defaultKey}
                                tabs={[
                                    // "701001RWCL-0011": "待处理",
                                    // "701001RWCL-0012": "已驳回",
                                    // "701001RWCL-0013": "已挂起",
                                    // "701001RWCL-0014": "待调整",
                                    // "701001RWCL-0015": "已处理",
                                    {attrcode:"pending", label:multiLang && multiLang.get('701001RWCL-0011')},
                                    {attrcode:"sscreject", label:multiLang && multiLang.get('701001RWCL-0012')},
                                    {attrcode:"handon", label:multiLang && multiLang.get('701001RWCL-0013')},
                                    {attrcode:"adjust", label:multiLang && multiLang.get('701001RWCL-0014')},
                                    {attrcode:"handled", label:multiLang && multiLang.get('701001RWCL-0015')}
                                ]}
                                showNumbers={this.state.showNumbers}
                                selectTabChange={this.selectTabChange.bind(this)}
                                showBtnsRight={true}
                                btnsRight={[
                                    {
                                        // "701001RWCL-0016": "任务提取"
                                        name: multiLang && multiLang.get('701001RWCL-0016'),
                                        color: 'primary',
                                        key: 'TaskExtract'
                                    }
                                ]}
                                btnsClick={buttonClick.bind(this)}
                                currentKey={currentKey}
                                superBtn={(
                                    <GetTask
                                        {...this.props}
                                        queryAreaCode={queryAreaCode}
                                        oid={oid}
                                        taskTakeAction={this.taskTakeAction}
                                    />
                                )}
                            />
                        </div>
                    </NCAffix>
                    <div className='work-bench-search-area'>
                        {/* 查询条件 */}
                        <WorkbenchSearcharea
                            ref='searchArea'
                            {...this.props}
                            twoColNums="4"
                            cusType="tp_zycl"
                            conditions={this.state.searcharea.conditions}
                            handleConditionChange={this.handleConditionChange.bind(this)}
                            fuzzyquerychild = {this.state.fuzzyquerychild}
                            onFuzzyChange={(data) => {
                                this.onFuzzyChange.call(this, data)
                            }}
                            onFuzzySelected={this.onFuzzySelected.bind(this)}
                            showTypeClick={this.showTypeClick.bind(this)}
                            barcodeApi={requestApi.query}
                            queryKey={this.queryKey}
                            queryDataCallback={this.queryDataCallback}
                            onPickDateChange={this.onPickDateChange.bind(this)}
                        />
                    </div>
                    <div className={isShow("list")} id='sscuser-simple-table'>
                        {/* 列表模式 */}
                        {createSimpleTable(window.presetVar.list, {                            
                            showIndex:true,
                            adaptionHeight:true,
                            onRowDoubleClick: (...params)=>{onRowDoubleClick.call(this, ...params)},
                            sort:{
								mode:'single',
                                backSource:true,
                                sortFun:(a)=> {
                                    this.handleTempletAfterSort(a);
                                    this.queryData();
                                }
                            }
                        })}
                        <div
                            className='scroll-add-more'
                            style={{display: addMoreOnOff ? 'block' : 'none'}}
                        >{multiLang && multiLang.get('701001RWCL-0062')}</div>{/*"701001RWCL-0062": "滑动加载更多"*/}
                    </div>
                    <div className={isShow("card")}>
                        {/* 卡片模式 */}
                        <WorkbenchDetail  
                            ref="detail"
                            props={this.props}
                            doAction={cardButtonClick.bind(this)}
                            buttonArea={'sscuserlist'}
                            getBtns={(data, index)=>getDetailBtns(data, index)}
                        />
                        <div
                            className='scroll-add-more'
                            style={{display: addMoreOnOff ? 'block' : 'none'}}
                        >{multiLang && multiLang.get('701001RWCL-0062')}</div>{/*"701001RWCL-0062": "滑动加载更多"*/}
                    </div>
                </div>
                
                {/* 操作记录 */}
                <NCModal className="operate-note" show={this.state.showOperateNote}>
                    <NCModal.Header>
                        {/*"701001RWCL-0008": "操作记录"*/}
                        <NCModal.Title>{multiLang && multiLang.get('701001RWCL-0008')}</NCModal.Title>
                        <span
                            className="viewer-close"
                            onClick={(eve) => {
                                this.setState({showOperateNote: false})
                            }}
                        >X</span>
                    </NCModal.Header>
                    <NCModal.Body>
                        <div className="operate-note-body-div">
                            {createSimpleTable('ssctaskhistory', {
                                showIndex:true
                            })}
                        </div>
                    </NCModal.Body>
                </NCModal>
                
                {/* 挂起 */}
                <HandonDialog
                    {...this.props}
                    showModal={this.state.showhandonmodel}
                    handleCloseModal={() => {this.setState({showhandonmodel: false})}}
                    successHangUp={this.successHangUp}
                    data={this.state.handonmodeldata}
                />
                {/* 取消挂起 */}
                <UnHandonDialog
                    {...this.props}
                    showModal={this.state.showUnHandonModal}
                    handleCloseModal={() => {this.setState({showUnHandonModal: false})}}
                    successUnHangUp={this.successUnHangUp}
                    data={this.state.unHandonModalData}
                />

                {/* 申请调整 */}
                <AdjustDialog
                    {...this.props}
                    showModal={this.state.showadjustmodel}
                    handleCloseModal={() => {this.setState({showadjustmodel: false})}}
                    successAdjust={this.successAdjust}
                    data={this.state.adjustmodeldata}
                />
                {/* 取消调整 */}
                <UnAdjustDialog
                    {...this.props}
                    showModal={this.state.showUnAdjustModal}
                    handleCloseModal={() => {this.setState({showUnAdjustModal: false})}}
                    successUnAdjust={this.successUnAdjust}
                    data={this.state.unAdjustModalData}
                />

                {/* 驳回 */}
                <RejectDialog
                    {...this.props}
                    // ref='rejectDialog'
                   showModal={this.state.showdialogmodel}
                   data={this.state.operationData}
                   showRadio={this.state.showradio}
                   ncoptions={this.state.ncoptions}
                   defOption={this.state.defaultOption}
                   that={this}
                   creditsArray={this.state.creditsArray}
                   deleteCreditByIdx={this.deleteCreditByIdx}
                   setCheckedCredits={this.setCheckedCredits}
                   creditView={this.state.creditView}
                   creditRuleItems={this.state.creditRuleItems}
                    setCreditRuleItems={this.setCreditRuleItems}
                />
                {/* 信用审批 */}
                <CreditApprovalDialog
                    {...this.props}
                    showModal={this.state.showcreditmodel}
                    handleCloseModal={() => {
                        this.setState({showcreditmodel: false, creditsArray: []})
                    }}
                    successCreditApproval={() => {
                        this.setState({showcreditmodel: false, didApprove: true, approveUrl: ''}, () => {
                            requestApi.openBill({
                                data: {
                                    billtypeCode: this.state.currentRowData.billtypecode.value, 
                                    transtypeCode: this.state.currentRowData.transtypecode.value,
                                    billid: this.state.currentRowData.busiid.value
                                },
                                success: (data) => {
                                    let url = data.data.url
                                    let params = data.data.data
                                    if (url.indexOf('#') > -1) {
                                        if (url.indexOf('#/') > -1) {
                                            if(url.indexOf('?') < 0) {
                                                url += '?'
                                            }
                                        }
                                    } else {
                                        url += '#/?'
                                    }
                                    let approveUrl = '/nccloud/resources' + url + 'status=' + params.status + '&scene=zycl&deal=done'
                                    for (let attr in params) {
                                        if (attr != 'status') {
                                            approveUrl += `&${attr}=${params[attr]}`
                                        }
                                    }
                                    this.setState({approveUrl}, () => {
                                        pageTo.addUrlParam({
                                            c: params.appcode,
                                            p: params.pagecode
                                        })
                                    })
                                }
                            })
                        })
                        // document.getElementById('approve-detail').contentWindow.location.reload(true)
                    }}
                    data={this.state.creditmodeldata}
                    creditsArray={this.state.creditsArray}
                    deleteCreditByIdx={this.deleteCreditByIdx}
                    setCheckedCredits={this.setCheckedCredits}
                    currentRowData={this.state.currentRowData}
                    reason={this.state.reason}
                    creditView={this.state.creditView}
                    creditRuleItems={this.state.creditRuleItems}
                    setCreditRuleItems={this.setCreditRuleItems}
                />
            </div> 
        )
    }
}
SscUserList = createPage({
    mutiLangCode: '7010',
    appAutoFocus: true
})(SscUserList)
export default SscUserList