import React, {Component} from 'react';
import {connect} from 'react-redux';
import {base, ajax, toast} from 'nc-lightapp-front';
const {NCModal,NCButton, NCRadio,NCTextArea,NCSelect} = base;
const NCOption = NCSelect.NCOption;
import requestApi from '../requestApi'
import classnames from 'classnames';
import CreditRuleTreeRef from '../../../../refer/sscbd/CreditRuleTreeRef'

class RejectDialog extends Component {
    constructor(props) {
        super();
        this.state={
            actid:'',
            memo:'',
            repeat:'1',
            ruleSearchValue: ''
        }
        this.ruleSearchThresholdTime = null;
    }

    save = () =>{
        this.props.setUrlParam({deal:'sscreject'}) ;
        let multiLang = this.props.MutiInit.getIntl(7010);
        if(!this.state.actid && !this.props.defOption){
            toast({
                color: 'warning',
                title: multiLang && multiLang.get('701001RWCL-0135'),//701001RWCL-0135：请注意
                content: multiLang && multiLang.get('701001RWCL-0142')//701001RWCL-0142：请选择驳回流程
            })  
            return
        }
        // if(!this.state.memo){
        //     toast({
        //         color: 'warning',
        //         title: multiLang && multiLang.get('701001RWCL-0135'),//701001RWCL-0135：请注意
        //         content: multiLang && multiLang.get('701001RWCL-0143')//701001RWCL-0143:请输入驳回原因
        //     })
        //     return
        // }
        let credits = [],creditName=[];
        this.props.creditsArray.forEach(each => {
            credits.push(each.refpk)
            creditName.push(each.refname);
        })
        if(this.state.memo){
            this.state.memo=this.state.memo+';'+creditName.join(';');
        }else{
            this.state.memo=creditName.join(';');
        }
        if (this.props.creditView && credits.length == 0) {
            toast({
                color: 'warning',
                title: multiLang && multiLang.get('701001RWCL-0135'),//701001RWCL-0135：请注意
                content: multiLang && multiLang.get('701001RWCL-0136')//701001RWCL-0136：请选择至少一个信用指标
            })
            return
        }
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCTaskHandleRejectAction.do',
            data: {
                pk_task: this.props.data.pk_task.value, 
                actiontype: 'sscreject', 
                actid: this.state.actid || this.props.defOption,
                memo: this.state.memo,
                repeat: this.state.repeat,
                credits,
                ts: this.props.data.ts.value
            },
            success: ()=>{
                this.props.that.setState({showdialogmodel: false, creditsArray: [], didAction: true});
                this.setState({
                    actid:''
                });
                toast({
                    color: 'success',
                    title: multiLang && multiLang.get('701001RWCL-0144'),//701001RWCL-0144：驳回成功
                    content: multiLang && multiLang.get('701001RWCL-0144')
                })
                document.getElementById("approve-detail").contentWindow.postMessage(JSON.stringify({deal:'sscreject'}),`${window.location.origin}/arap/gatheringbill/gatheringbill/main/index.html#/card`)             
                return;
            }
        });
    }

    cancel = () => {
        this.setState({
            actid:'',
            memo:''
        });
        this.props.that.setState({showdialogmodel: false, creditsArray: []})
    }

    handleChange=(value)=>{
        this.setState({
            actid:value
        })   
    }

    handleRadio=(value)=>{
        this.setState({
            repeat:value
        })
    }

    ruleItemSelect = (item, selectedRuleItems) => {
        if (selectedRuleItems[item.refpk]) {
            delete selectedRuleItems[item.refpk]
        } else {
            selectedRuleItems[item.refpk] = item;
        }
        
        this.props.setCheckedCredits(Object.values(selectedRuleItems));
    }

    ruleSearchChange(e) {
        let value =  e.target.value;
        let trimValue = value.trim();
        this.setState({
            ruleSearchValue: value
        });
        clearTimeout(this.ruleSearchThresholdTime);
        this.ruleSearchThresholdTime = setTimeout(() => {
            
            let creditRuleItems = this.props.creditRuleItems;
            Object.values(creditRuleItems).forEach((rootItem) => {
                rootItem.children.forEach((item) => {
                    if (!trimValue || item.refname.indexOf(trimValue) != -1) {
                        item.notSearch = false;
                    } else {
                        item.notSearch = true;
                    }
                })
            })
    
            this.props.setCreditRuleItems(creditRuleItems);
        }, 200) 
        
        
    }

    render() {
        let {
            ncoptions,
            creditsArray,
            setCheckedCredits,
            creditView
        } = this.props
        let getNCOptions=()=>{
            let refHtml = [];
            if(ncoptions!=null&&ncoptions!=""&&ncoptions!=''){
               ncoptions.map((one)=>{
                refHtml.push(<NCOption value={one.value}>{one.name}</NCOption>)
                })
            }
            return refHtml
        }

        let selectedRuleItems = {}
        creditsArray && creditsArray.forEach((item) => {
            selectedRuleItems[item.refpk] = item;
        })
        let multiLang = this.props.MutiInit.getIntl(7010);
        
        return (
            <NCModal show = {this.props.showModal}  id="rejectdialog" onHide={()=>{this.cancel()}}>
                <NCModal.Header closeButton={true}>
                    {/*701001RWCL-0005:驳回*/}
                    <NCModal.Title>{multiLang && multiLang.get('701001RWCL-0005')}</NCModal.Title>
                </NCModal.Header>
                <NCModal.Body>
                    <div className='reject-content'>
                        <div className='reject-title--content'
                           >
                           {/*701001RWCL-0145:驳回至*/}
                            <span>{multiLang && multiLang.get('701001RWCL-0145')}</span>

                            <NCSelect 
                                defaultValue={this.props.defOption} 
                                onChange={this.handleChange}
                            > 
                                {getNCOptions()}
                            </NCSelect>
                            
                            {/* <div className={creditView?'contentdiv-show':'contentdiv-hide'}>
                            {CreditRuleTreeRef({
                                onChange: setCheckedCredits,
                                clickContainer: <span>信用评价</span>,
                                isMultiSelectedEnabled: true
                            })}
                            </div> */}
                        </div>
                        <div className='credits-box'>
                            <div className='credits-show clearfix'>
                                {
                                    creditsArray.map((each, idx) => (
                                        <span className='item-credit-view'>
                                            {each.refname}
                                            <i
                                                onClick={() => {
                                                    this.props.deleteCreditByIdx(idx)
                                                }}
                                            > - </i>
                                        </span>
                                    ))
                                } 
                            </div>
                            {/*701001RWCL-0143：请输入驳回原因*/}
                            <NCTextArea
                                placeholder={'请输入或选择驳回原因'}
                                className=""
                                showMax={true}
                                max={200}
                                onChange={(val)=>{this.state.memo=val}}
                            />
                        </div>
                    </div>
                    <div className="" style={{display: creditView ? 'block' : 'none'}}>
                        <div className="rule-title">
                         <i class="iconfont icon-sousuo-c">
                            {/*701001RWCL-0140:信用评价*/}
                            {'驳回原因'}
                            <input 
                                placeholder={multiLang && multiLang.get('701001RWCL-0138')}
                                onChange={this.ruleSearchChange.bind(this)} 
                                value={this.state.ruleSearchValue} 
                                type="text" 
                                className='rule-search-input'/>
                            </i>
                        </div>
                        <div className="rule-content-wrap">
                            {Object.values(this.props.creditRuleItems).map((rootItem) => {
                                return [
                                    <div className="first-level"><i className="icon iconfont icon-hangcaozuoxiala1"></i>{rootItem.item.refname}</div>,
                                    <ul>
                                        {rootItem.children.map((item) => {
                                            return (
                                                <li 
                                                    style={{display: !item.notSearch ? 'block' :  'none'}}
                                                    className={classnames('second-level', {'rule-item-selected' : selectedRuleItems[item.refpk]})} 
                                                    onClick={this.ruleItemSelect.bind(this, item, selectedRuleItems)}
                                                >
                                                    {item.refname}
                                                </li>
                                            )
                                        })}
                                    </ul>
                                ]
                            })}
                            
                        </div>
                        
                    </div>
                </NCModal.Body>
                <NCModal.Footer>
                    {this.props.showRadio?(
                    <NCRadio.NCRadioGroup
                        selectedValue={this.state.repeat}
                        onChange={this.handleRadio}
                        className='footer-radio-group'
                    >
                        {/*701001RWCL-0146：重走流程，701001RWCL-0147：不重走流程*/}
                        <NCRadio value="1" >{multiLang && multiLang.get('701001RWCL-0146')}</NCRadio>
                        <NCRadio value="2" >{multiLang && multiLang.get('701001RWCL-0147')}</NCRadio>
                    </NCRadio.NCRadioGroup>
                    ):("")}
                   

                    <NCButton colors="primary" onClick={this.save} >{multiLang && multiLang.get('701001RWCL-0005')}</NCButton>
                    <NCButton onClick={this.cancel}>{multiLang && multiLang.get('7010-0004')}</NCButton>
                </NCModal.Footer>
            </NCModal>
        )
    }
}

export default RejectDialog;