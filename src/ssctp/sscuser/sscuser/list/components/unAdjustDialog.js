import React, {Component} from 'react'
import {base} from 'nc-lightapp-front'
const {NCModal,NCButton,NCTextArea} = base
import requestApi from '../requestApi'

class UnAdjustDialog extends Component {
    constructor(props) {
        super(props)
        this.state={
            memo:''
        }
    }
    // 保存并关闭
    save = () => {
        requestApi.taskUnAdjustAction({
            data: {
                pk_task: this.props.data.pk_task.value,
                actiontype: "un-adjust",
                memo:this.state.memo,
                ts: this.props.data.ts.value
            },
            success: (data) => {
                this.setState({memo: ''})
                this.props.successUnAdjust(data)
            }
        })
    }
    
    cancel = () => {
        this.props.handleCloseModal()
        this.setState({memo:''})
    }

    render() {
        let multiLang = this.props.MutiInit.getIntl(7010);
        return (
            <NCModal show = {this.props.showModal}  id="handon-dialog" onHide={()=>{this.cancel()}}>
                <NCModal.Header closeButton={true}>
                    {/*701001RWCL-0017：取消调整*/}
                    <NCModal.Title>{multiLang && multiLang.get('701001RWCL-0017')}</NCModal.Title>
                </NCModal.Header>
                <NCModal.Body>
                    <div>
                        <div>
                            {/*701001RWCL-0148：请输入取消调整原因*/}
                            <NCTextArea
                                placeholder={multiLang && multiLang.get('701001RWCL-0148')}
                                showMax={true}
                                max={200}
                                onChange={(val)=>{this.state.memo=val}}
                            />
                        </div> 
                    </div>
                </NCModal.Body>
                <NCModal.Footer>
                    <NCButton colors="primary" onClick={this.save} >{multiLang && multiLang.get('701001RWCL-0017')}</NCButton>
                    <NCButton onClick={this.cancel} >{multiLang && multiLang.get('7010-0004')}</NCButton>
                </NCModal.Footer>
            </NCModal>
        )
    }
}

export default UnAdjustDialog