import React, {Component} from 'react'
import {base, toast,promptBox} from 'nc-lightapp-front'
const {NCModal,NCButton,NCTextArea} = base
import requestApi from '../requestApi'
import classnames from 'classnames';
import CreditRuleTreeRef from '../../../../refer/sscbd/CreditRuleTreeRef';


class CreditApprovalDialog extends Component {
    constructor(props) {
        super(props)
        this.state={
            memo:'',
            jkCheck:false,
			ntbCheck:false,
            memo:'',
            ruleSearchValue: ''
        }
        this.ruleSearchThresholdTime = null;
        

    }
    
    save = () => {
        let credits = []
        let multiLang = this.props.MutiInit.getIntl(7010); 
        this.props.creditsArray.forEach(each => {
            credits.push(each.refpk)
        })
        if (this.props.creditView && credits.length == 0) {
            toast({
                color: 'warning',
                title: multiLang && multiLang.get('701001RWCL-0135'),//701001RWCL-0135：请注意
                content: multiLang && multiLang.get('701001RWCL-0136')//701001RWCL-0136：请选择至少一个信用指标
            })            
            return
        }

        requestApi.taskApproveAction({                                                
            data: {
                pk_task: this.props.currentRowData.pk_task.value,
                info: this.state.memo,
                billtype:this.props.currentRowData.billtypecode.value,
                billno:this.props.currentRowData.billno.value,
                busiid:this.props.currentRowData.busiid.value,
                skipcodes:this.state.skipcodes,
                ntbCheck:this.state.ntbCheck,
                actiontype: "sscapprove",
                credits,
                jkCheck:this.state.jkCheck,
                ts: this.props.currentRowData.ts.value
            },
            success: (data) => {
                if (data.success) {
                    if(data.data&&data.data['jkAlarm']){
                        promptBox({
                            color: 'warning',       // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                            title: multiLang && multiLang.get('701001RWCL-0047'),//"提示信息"
                            content:  (multiLang && multiLang.get('701001RWCL-0048'))+ data.data['jkAlarm'] + (multiLang && multiLang.get('701001RWCL-0049')),
                            noFooter: false,  // 是否显示底部按钮(确定、取消),默认显示(false),非必输
                            noCancelBtn: false,    // 是否显示取消按钮,，默认显示(false),非必输
                            beSureBtnName:  multiLang && multiLang.get('7010-0001'),
                            cancelBtnName: multiLang && multiLang.get('7010-0004'),
                            beSureBtnClick: ()=>{
                                this.state.jkCheck=true;
                                this.save();
                            }
                            //cancelBtnClick: functionCancel  // 取消按钮点击调用函数,非必输
                        })
                    }else if(data.data&&data.data['bugetAlarm']){ 
                        promptBox({
                            color: 'warning',       // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                            title: multiLang && multiLang.get('701001RWCL-0047'),//"提示信息"
                            content:  (multiLang && multiLang.get('701001RWCL-0048'))+ data.data['bugetAlarm'] + (multiLang && multiLang.get('701001RWCL-0049')),
                            noFooter: false,  // 是否显示底部按钮(确定、取消),默认显示(false),非必输
                            noCancelBtn: false,    // 是否显示取消按钮,，默认显示(false),非必输
                            beSureBtnName:  multiLang && multiLang.get('7010-0001'),
                            cancelBtnName: multiLang && multiLang.get('7010-0004') ,
                            beSureBtnClick: ()=>{
                                this.state.ntbCheck = true;
                                this.state.skipcodes = data.data['skipcodes']
                                this.save();
                            }
                            //cancelBtnClick: functionCancel  // 取消按钮点击调用函数,非必输
                        })
                    } else {
                        this.props.successCreditApproval()
                    }
                }
            }
        })
    }
    
    cancel = () => {
        this.props.handleCloseModal()
        this.setState({memo:''})
    }

    ruleItemSelect = (item, selectedRuleItems) => {
        if (selectedRuleItems[item.refpk]) {
            delete selectedRuleItems[item.refpk]
        } else {
            selectedRuleItems[item.refpk] = item;
        }
        
        this.props.setCheckedCredits(Object.values(selectedRuleItems));
    }

    ruleSearchChange(e) {
        let value =  e.target.value;
        let trimValue = value.trim();
        this.setState({
            ruleSearchValue: value
        });
        clearTimeout(this.ruleSearchThresholdTime);
        this.ruleSearchThresholdTime = setTimeout(() => {
            
            let creditRuleItems = this.props.creditRuleItems;
            Object.values(creditRuleItems).forEach((rootItem) => {
                rootItem.children.forEach((item) => {
                    if (!trimValue || item.refname.indexOf(trimValue) != -1) {
                        item.notSearch = false;
                    } else {
                        item.notSearch = true;
                    }
                })
            })
            this.props.setCreditRuleItems(creditRuleItems);
        }, 200) 
        
        
    }
    
    render() {
        const {
            creditsArray,
            setCheckedCredits,
            creditView
        } = this.props
        
        let selectedRuleItems = {}
        let multiLang = this.props.MutiInit.getIntl(7010);
        creditsArray && creditsArray.forEach((item) => {
            selectedRuleItems[item.refpk] = item;
        })

        return (
            <NCModal show = {this.props.showModal}  id="credit-dialog" onHide={()=>{this.cancel()}}>
                <NCModal.Header closeButton={true}>
                    {/*701001RWCL-0139:信用审批*/}
                    <NCModal.Title>{multiLang && multiLang.get('701001RWCL-0139')}</NCModal.Title>
                </NCModal.Header>
                <NCModal.Body>
                    <div className='credits-box'>
                        <div className='credits-show clearfix'>
                            {
                                creditsArray.map((each, idx) => (
                                    <span className='item-credit-view'>
                                        {each.refname}
                                        <i
                                            onClick={() => {
                                                this.props.deleteCreditByIdx(idx)
                                            }}
                                        > - </i>
                                    </span>
                                ))
                            }
                        </div>
                        {/*"701001RWCL-0137": "请输入信用审批建议"*/}
                        <NCTextArea
                            placeholder={multiLang && multiLang.get('701001RWCL-0137')}
                            showMax={true}
                            max={200}
                            onChange={(val)=>{this.state.memo=val}}
                        />
                    </div>
                    <div className="" style={{display: creditView ? 'block' : 'none'}}>
                        <div className="rule-title">
                            {/*"701001RWCL-0140": "信用评价"*/}
                            {multiLang && multiLang.get('701001RWCL-0140')}
                            <i class="iconfont icon-sousuo-c">
                            {/*"701001RWCL-0138": "请查询"*/}
                            <input 
                                placeholder={multiLang && multiLang.get('701001RWCL-0138')}
                                onChange={this.ruleSearchChange.bind(this)} 
                                value={this.state.ruleSearchValue} 
                                type="text" 
                                className='rule-search-input'/>
                            </i>
                        </div>
                        <div className="rule-content-wrap">
                            {Object.values(this.props.creditRuleItems).map((rootItem) => {
                                return [
                                    <div className="first-level"><i className="icon iconfont icon-hangcaozuoxiala1"></i>{rootItem.item.refname}</div>,
                                    <ul>
                                        {rootItem.children.map((item) => {
                                            return (
                                                <li 
                                                    style={{display: !item.notSearch ? 'block' :  'none'}}
                                                    className={classnames('second-level', {'rule-item-selected' : selectedRuleItems[item.refpk]})} 
                                                    onClick={this.ruleItemSelect.bind(this, item, selectedRuleItems)}
                                                >
                                                    {item.refname}
                                                </li>
                                            )
                                        })}
                                    </ul>
                                ]
                            })}
                            
                        </div>
                        
                    </div>
                </NCModal.Body>
                <NCModal.Footer>
                    {/*701001RWCL-0037:信用批准，7010-0004：取消*/}
                    <NCButton colors="primary" onClick={this.save} >{multiLang && multiLang.get('701001RWCL-0037')}</NCButton>
                    <NCButton onClick={this.cancel}>{multiLang && multiLang.get('7010-0004')}</NCButton>
                </NCModal.Footer>
            </NCModal>
        )
    }
}

export default CreditApprovalDialog