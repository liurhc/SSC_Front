import React, {Component} from 'react'
import {base} from 'nc-lightapp-front'
const {NCModal,NCButton,NCTextArea} = base
import requestApi from '../requestApi'

class HandonDialog extends Component {
    constructor(props) {
        super(props)
        this.state={
            memo:''
        }
    }
    // 保存并关闭
    save = () => {
        requestApi.taskHandOnAction({
            data: {
                pk_task: this.props.data.pk_task.value,
                actiontype: "handon",
                memo:this.state.memo,
                ts: this.props.data.ts.value
            },
            success: (data) => {
                this.setState({memo: ''})
                this.props.successHangUp(data)
            }
        })
    }
    // 取消并关闭
    cancel = () => {
        this.props.handleCloseModal()
        this.setState({memo:''})
    }
    
    render() {
        let multiLang = this.props.MutiInit.getIntl(7010);
        return (
            <NCModal show = {this.props.showModal}  id="handon-dialog" onHide={()=>{this.cancel()}}>
                <NCModal.Header closeButton={true}>
                    <NCModal.Title>{multiLang && multiLang.get('701001RWCL-0006')}</NCModal.Title>
                </NCModal.Header>
                <NCModal.Body>
                    <div>
                        <div>
                            {/*701001RWCL-0141:请输入挂起原因*/}
                            <NCTextArea
                                placeholder={multiLang && multiLang.get('701001RWCL-0141')}
                                showMax={true}
                                max={200}
                                onChange={(val)=>{this.state.memo=val}}
                            />
                        </div> 
                    </div>
                </NCModal.Body>
                <NCModal.Footer>
                    {/*7010-0004:取消,701001RWCL-0006:挂起*/}
                    <NCButton colors="primary" onClick={this.save} >{multiLang && multiLang.get('701001RWCL-0006')}</NCButton>
                    <NCButton onClick={this.cancel}>{multiLang && multiLang.get('7010-0004')}</NCButton>
                </NCModal.Footer>
            </NCModal>
        )
    }
}

export default HandonDialog