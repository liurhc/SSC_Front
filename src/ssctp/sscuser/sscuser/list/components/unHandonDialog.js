import React, {Component} from 'react'
import {base} from 'nc-lightapp-front'
const {NCModal,NCButton,NCTextArea} = base
import requestApi from '../requestApi'

class UnHandonDialog extends Component {
    constructor(props) {
        super(props)
        this.state={
            memo:''
        }
    }
    // 保存并关闭
    save = () => {
        requestApi.taskUnHandOnAction({
            data: {
                pk_task: this.props.data.pk_task.value,
                actiontype: "un-handon",
                memo:this.state.memo,
                ts: this.props.data.ts.value
            },
            success: (data) => {
                this.setState({memo: ''})
                this.props.successUnHangUp(data)
            }
        })
    }
    
    cancel = () => {
        this.props.handleCloseModal()
        this.setState({memo:''})
    }
    
    render() {
        let multiLang = this.props.MutiInit.getIntl(7010);
        return (
            <NCModal show = {this.props.showModal}  id="handon-dialog" onHide={()=>{this.cancel()}}>
                <NCModal.Header closeButton={true}>
                    {/*701001RWCL-0018：取消挂起*/}
                    <NCModal.Title>{multiLang && multiLang.get('701001RWCL-0018')}</NCModal.Title>
                </NCModal.Header>
                <NCModal.Body>
                    <div>
                        <div>
                            {/*701001RWCL-0149：请输入取消挂起原因*/}
                            <NCTextArea
                                placeholder={multiLang && multiLang.get('701001RWCL-0149')}
                                showMax={true}
                                max={200}
                                onChange={(val)=>{this.state.memo=val}}
                            />
                        </div> 
                    </div>
                </NCModal.Body>
                <NCModal.Footer>
                    <NCButton colors="primary" onClick={this.save} >{multiLang && multiLang.get('701001RWCL-0018')}</NCButton>
                    <NCButton onClick={this.cancel} >{multiLang && multiLang.get('7010-0004')}</NCButton>
                </NCModal.Footer>
            </NCModal>
        )
    }
}

export default UnHandonDialog