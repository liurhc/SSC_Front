import React, {Component} from 'react'

export default class GetTask extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }
    
    render() {
        let multiLang = this.props.MutiInit.getIntl(7010);
        const {
            search: {NCCreateSearch},
            queryAreaCode,
            taskTakeAction,
            oid
        } = this.props

        return (
            NCCreateSearch(
                queryAreaCode,
                {
                    clickSearchBtn: (props, data, key) => {
                        debugger
                        taskTakeAction(data)
                    },
                    oid: oid,
                    onlyShowSuperBtn: true,
                    replaceSuperBtn: multiLang && multiLang.get('701001RWCL-0001')//701001RWCL-0001：定向提取
                }
            )
        )
    }
}