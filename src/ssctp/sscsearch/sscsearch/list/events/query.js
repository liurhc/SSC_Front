import requestApi from "../requestApi";
import presetVar from "../presetVar";

let queryData = function(isAddPage) {
  if (isAddPage) {
    // 更新查询条件
    this.queryKey.pageinfo.number++;
  } else {
    this.queryKey.pageinfo.number = 1;
  }
  this.queryKey.pageStatus = this.state.pageStatus;
  if (this.queryKey.queryKey == false) {
    return;
  }
  requestApi.query({
    data: this.queryKey,
    success: data => {
      let listRows = [];
      if (isAddPage) {
        this.data.listRows.map(one => {
          listRows.push(this.copyData(one));
        });
      } else {
        this.refs.detail.clearAllDetailData();
        this.pubMessage.querySuccess((((data.data || {})[presetVar.list] || {}).pageinfo || {}).totalElements);
      }
      data.data[presetVar.list].rows.map(one => {
        listRows.push(this.copyData(one));
      });
      this.data.listRows = listRows;
      this.props.table.setAllTableData(presetVar.list, {
        areacode: presetVar.list,
        rows: listRows
      });
      // 设置缩略数据
      this.refs.detail.addDetailData(data.data[presetVar.card]);
      let newState = {};
      newState = this.state;
      // 更新页信息
      this.queryKey.pageinfo = data.data[presetVar.list].pageinfo;
      if (this.queryKey.pageinfo.totalPages > this.queryKey.pageinfo.number) {
        newState.pageControll = "notend";
      } else if (
        this.queryKey.pageinfo.totalPages == this.queryKey.pageinfo.number
      ) {
        newState.pageControll = "end";
      } else {
        newState.pageControll = "none";
      }
      this.setState(newState);
      setTimeout(() => {
        this.canChangPageFlag = true;
      }, 100);
    }
  });
};
export default { queryData };
