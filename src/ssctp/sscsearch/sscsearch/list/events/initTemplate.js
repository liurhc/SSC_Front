import presetVar from "../presetVar";
import { listButtonClick } from "./index";

export default function(props) {
  const that = this;
  props.createUIDom({}, function(data) {
    let meta = data.template;
    meta[presetVar.searchArea].items.map(one => {
      if(one.attrcode == "pk_tradetype"){
          one.fieldValued = 'refcode';
      }
      if(one.attrcode == "pk_org"){
        one.isRunWithChildren =false;
        one.isShowDisabledData =true;
    }
      dealRefShow(one);
    });
    props.meta.setMeta(meta);

    props.table.setTableRender(
      presetVar.list,
      "billno",
      (text, record, index) => {
        return (
          <a
            className="billnoa"
            onClick={() => {
              listButtonClick.call(that, "OpenBill", record, index);
            }}
          >
            {record.billno.value}
          </a>
        );
      }
    );
    // 自定义状态列
    /*props.table.setTableRender(
      presetVar.list,
      "billstatus",
      (text, record, index) => {
        return (
          <div className="bill-status-wrap">
            <span
              className={
                record.billstatus.value == "0"
                  ? "not-complete-status"
                  : "complete-status"
              }
            />
            <span>{record.billstatus.display}</span>
          </div>
        );
      }
    );*/
  });
  const dealRefShow = function(data) {
    if (data.itemtype == "refer") {
      if (data.fieldDisplayed == "code") {
        data.fieldDisplayed = "refcode";
      } else if (data.fieldDisplayed == "name") {
        data.fieldDisplayed = "refname";
      }
    }
  };
}
