import { ajax } from "nc-lightapp-front";

let requestDomain = "";

let requestApiOverwrite = {
  // 查询接口
  query: opt => {
    ajax({
      url: "/nccloud/ssctp/sscbd/SSCTaskSearchAction.do",
      data: opt.data,
      success: opt.success
    });
  },
  // 查询接口
  queryFuzzyKey: opt => {
    ajax({
      url: "/nccloud/ssctp/sscbd/SSCTaskBlurQueryAction.do",
      data: opt.data,
      success: opt.success
    });
  },
  // 查看单据
  openBill: opt => {
    ajax({
      url: "/nccloud/sscrp/rpbill/BrowseBillAction.do",
      data: opt.data,
      success: data => {
        opt.success(data);
      }
    });
  }
};

export default requestApiOverwrite;
