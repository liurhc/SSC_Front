import { Component } from "react";
import { createPage, base, createPageIcon } from "nc-lightapp-front";
const { NCSelect, NCAffix } = base;
import WorkbenchDetail from "ssccommon/components/workbench-detail";
import pubMessage from 'ssccommon/utils/pubMessage';
import { initTemplate, listButtonClick, query } from "./events/index";
import presetVar from "./presetVar";
import requestApi from "./requestApi";
import "./index.less";
import {
    ProfileStyle,
    ProfileHead,
    ProfileBody,
    ButtonGroup,
    HeadCenterCustom,
    EmptyArea
} from 'ssccommon/components/profile';

class SscSearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showType: "list",
            fuzzyquerychild: [],
            menuShowType: {
                type: "icon-Group",
                title: "缩略显示"
                // "105602BZCX-0008":"缩略显示"
                //title:multiLang && multiLang.get('105602BZCX-0008')
            }
        };
        this.pubMessage = new pubMessage(props);
        // 查询条件
        this.queryKey = {
            fuzzyQueryKey: [],
            // 页信息
            pageinfo: {
                number: 1,
                size: 20,
                totalElements: 0,
                totalPages: 0
            },
            queryKey: {}
        };
        // 缓存查询结果数据
        this.data = {
            listRows: [],
            cardRows: []
        };
        // 延时控制标记
        // 翻页
        this.canChangPageFlag = true;
        // 模糊查询
        this.fuzzyKey = "";
        // 页面初始化
        initTemplate.call(this, props);
    }
    componentDidMount() {
        // Firefox
        window.addEventListener("DOMMouseScroll", ev => {
            ev = ev || window.event;
            let onOff = null;
            if (ev.detail < 0) {
                onOff = true;
            } else {
                onOff = false;
            }
            let t = document.documentElement.scrollTop || document.body.scrollTop;
            let h =
                document.documentElement.scrollHeight || document.body.scrollHeight;
            let bh =
                document.documentElement.clientHeight || document.body.clientHeight;
            if (
                !onOff &&
                h - bh - t < 30 &&
                this.queryKey.pageinfo.totalPages > this.queryKey.pageinfo.number &&
                this.canChangPageFlag
            ) {
                this.canChangPageFlag = false;
                setTimeout(() => {
                    query.queryData.call(this, true);
                }, 300);
            }
        });
        // 其他浏览器
        window.onmousewheel = ev => {
            ev = ev || window.event;
            let onOff = null;
            if (ev.wheelDelta > 0) {
                onOff = true;
            } else {
                onOff = false;
            }
            let t = document.documentElement.scrollTop || document.body.scrollTop;
            let h =
                document.documentElement.scrollHeight || document.body.scrollHeight;
            let bh =
                document.documentElement.clientHeight || document.body.clientHeight;
            if (
                !onOff &&
                h - bh - t < 30 &&
                this.queryKey.pageinfo.totalPages > this.queryKey.pageinfo.number &&
                this.canChangPageFlag
            ) {
                this.canChangPageFlag = false;
                setTimeout(() => {
                    query.queryData.call(this, true);
                }, 300);
            }
        };
    }
    menuClick = () => {
        // "105602BZCX-0008":"缩略显示",
        // "105602BZCX-0009":"列表显示"
        let multiLang = this.props.MutiInit.getIntl(7010);
        if (this.state.menuShowType.type == "icon-xingzhuang") {
            this.setState({
                menuShowType: {
                    type: "icon-Group",
                    //title: multiLang && multiLang.get("105602BZCX-0008")
                    title: "缩略显示"
                },
                showType: "list"
            });
        } else {
            this.setState({
                menuShowType: {
                    type: "icon-xingzhuang",
                    //title: multiLang && multiLang.get("105602BZCX-0009")
                    title: "列表显示"
                },
                showType: "card"
            });
        }
    };
    // 模糊查询输入内容变化
    onFuzzyChange(data) {
        this.fuzzyKey = data;
        let newQueryKey = {};
        let multiLang = this.props.MutiInit.getIntl(7010);
        for (let child in this.queryKey) {
            newQueryKey[child] = this.queryKey[child];
        }
        newQueryKey.fuzzyQueryKey = data;
        setTimeout(
            (that, data) => {
                if (data == that.fuzzyKey) {
                    requestApi.queryFuzzyKey({
                        data: newQueryKey,
                        success: data => {
                            for (let childName in data.data) {
                                data.data[childName].value =
                                    data.data[childName].value + "=" + that.fuzzyKey;
                                //701001RWCL-0061:包含
                                data.data[childName].key =
                                    data.data[childName].key + (multiLang && multiLang.get("701001RWCL-0061")) + that.fuzzyKey;
                            }
                            that.setState({ fuzzyquerychild: data.data });
                        }
                    });
                }
            },
            500,
            this,
            data
        );
    }
    // 模糊查询选择
    onFuzzySelected(data) {
        this.setState({ fuzzyquerychild: [] });
        // 更新查询条件
        this.queryKey.fuzzyQueryKey = data;
        // 查询数据
        query.queryData.call(this);
    }
    searchClick(props, data, type) {
        this.queryKey.queryKey = data;

        query.queryData.call(this);
    }
    // 复制列表数据
    copyData(data) {
        let newData = {};
        newData.values = {};
        for (let child in data.values) {
            newData.values[child] = {
                display: data.values[child].display,
                value: data.values[child].value,
                scale: data.values[child].scale
            };
        }
        return newData;
    }
    // 双击列表行切换到卡片态浏览
    onRowDoubleClick = (values, idx, props, events) => {
        requestApi.openBill({
            data: {
                billtypeCode: values.billtypecode.value,
                transtypeCode: values.transtypecode.value,
                billid: values.busiid.value
            },
            success: data => {
                this.props.specialOpenTo(data.data.url, {
                    ...data.data.data,
                    scene: "bzcx"
                });
            }
        });
    };

    render() {
        const { table, search, button } = this.props;
        const { createSimpleTable } = table;
        let { NCCreateSearch } = search;
        let multiLang = this.props.MutiInit.getIntl(7010);
        const { onRowDoubleClick } = this

        let isShow = type => {
            if (this.state.showType == "list" && type == "list") {
                return "show";
            } else if (this.state.showType == "card" && type == "card") {
                return "show";
            } else {
                return "hide";
            }
        };

        let addMoreOnOff = this.data.listRows.length > 0 && this.queryKey.pageinfo.totalPages > this.queryKey.pageinfo.number

        return (
            <ProfileStyle
                layout="singleTable"
                {...this.props}
                id="ssctasklist"
            >
                <ProfileHead
                    title={multiLang && multiLang.get("701003RWCX-0172")}
                >
                    <HeadCenterCustom>
                        <NCSelect
                            className="fuzzyquery"
                            height="24px"
                            tags
                            searchPlaceholder={
                                multiLang && multiLang.get("701003RWCX-0169")
                            }
                            placeholder={multiLang && multiLang.get("701003RWCX-0170")}
                            data={this.state.fuzzyquerychild}
                            onSearch={this.onFuzzyChange.bind(this)}
                            onChange={this.onFuzzySelected.bind(this)}
                        />
                        <i
                            className={`menu-type-icon iconfont ${
                                this.state.menuShowType.type
                                }`}
                            title={this.state.menuShowType.title}
                            onClick={this.menuClick}
                        />

                    </HeadCenterCustom>
                </ProfileHead>
                <ProfileBody>
                    <EmptyArea className='ssc-profile-search-area'>

                        <div className="create-search">
                            {NCCreateSearch(presetVar.searchArea, {
                                clickSearchBtn: this.searchClick.bind(this),
                                defaultConditionsNum: 1,
                                showAdvBtn: false
                                // searchBtnName: multiLang && multiLang.get('105602BZCX-0024')
                            })}
                        </div>

                    </EmptyArea>
                    <div className={isShow("list")}>
                        {createSimpleTable(presetVar.list, {
                            showIndex: true,
                            lazyload: false,
                            onRowDoubleClick
                        })}
                        {/* "701003RWCX-0171":"滑动加载更多" */}
                        <div
                            className='scroll-add-more'
                            style={{ display: addMoreOnOff ? 'block' : 'none' }}
                        >{multiLang && multiLang.get('701003RWCX-0171')}</div>
                    </div>
                    <div className={isShow("card")}>
                        <WorkbenchDetail
                            ref="detail"
                            btns={this.state.btns}
                            props={this.props}
                            doAction={listButtonClick.bind(this)}
                            getBtns={() => {
                                if (this.queryKey.isComplate == "true") {
                                    return ["Print", "Copy"];
                                } else {
                                    return ["Print", "Copy", "RemindingApproval"];
                                }
                            }}
                        />
                        <div
                            className='scroll-add-more'
                            style={{ display: addMoreOnOff ? 'block' : 'none' }}
                        >{multiLang && multiLang.get('701003RWCX-0171')}</div>
                    </div>

                </ProfileBody>
            </ProfileStyle>
        );
    }
}
SscSearch = createPage({
    mutiLangCode: "7010"
})(SscSearch);
export default SscSearch;
