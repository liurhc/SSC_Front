import React, { Component } from "react";
import "./index.less";
import echarts from "echarts";
import { ajax, base, createPage, getMultiLang } from "nc-lightapp-front";
import randomId from "../../../../sscrp/public/common/utils/GenNonDuplicateID";

const { NCSelect } = base;
const NCOption = NCSelect.NCOption;
let option = {}
let myChart;
function initoption (that) {
  option = {
    color: ["#5CA2FF"],
    tooltip: {
      trigger: "axis"
    },
    grid: {
      left: "15px",
      right: "15px",
      top: "25px",
      bottom: "15px",
      containLabel: true
    },
    dataZoom: {
      start: 0,
      end: 100,
      bottom: "12",
      height: "12",
      showDataShadow: false,
      show: false
    },
    xAxis: {
      type: "category",
      axisLabel: {
        interval: 0
      },
      data: [],
      axisLine: {
        lineStyle: {
            color: '#888888'
        }
      }
    },
    yAxis: {
      type: "value",
      minInterval : 1,
      boundaryGap: [0, 0.01],
      axisLine: {
        lineStyle: {
            color: '#888888'
        }
      }
    },
    series: [
      {
        data: [],
        name: that.state.json["701001RWCL-0033"],//在手作业量
      type: "bar",
      barMaxWidth: '20'
      }
    ]
  }
};

function deal(groupid) {
  // let myChart = echarts.init(document.getElementById("test1"));
    myChart.clear()

  if (!groupid || "" == groupid) {
       myChart.setOption(option, true)
    return;
  }
  ajax({
    url: "/nccloud/ssctp/sscbd/SSCTaskManageBarChartAction.do",
    data: { flag: "onhand", groupid: groupid },
    loading: false,
    success: res => {
      if (res.data) {
        if (res.data.x) {
          option.xAxis.data = res.data.x;
        } else {
          option.xAxis.data = [];
        }
        if (res.data.y) {
          option.series[0].data = res.data.y;
        } else {
          option.series[0].data = [];
        }
        let allsize = res.data.allsize;
        let showsize = 5;
        if (allsize > showsize) {
          option.grid.bottom = "27px";
          option.dataZoom.show = true;
          option.dataZoom.textStyle = false;
          option.dataZoom.start = 0;
          option.dataZoom.end = (showsize * 100) / allsize;
        } else {
          option.grid.bottom = "15px";
          option.dataZoom.show = false;
          option.dataZoom.start = 0;
          option.dataZoom.end = 100;
        }
      }
     myChart.setOption(option, true)
    }
  });
}

 class Test3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeKey: "",
      groups: [],
      json: {}
    };
    this.echartsId = randomId(36)
  }
  componentWillMount() {
		let callback = (json) => {
			this.setState({json})
		}
		getMultiLang({moduleId: 7010, currentLocale: 'zh-CN',domainName: 'ssctp',callback})
	}

  componentDidMount() {
    initoption(this)
    myChart = echarts.init(document.getElementById(this.echartsId));
    ajax({
      url: "/nccloud/ssctp/sscbd/SSCTaskManageBarChartAction.do",
      data: {},
      loading: false,
      success: res => {
        if (res.data && res.data.groups && res.data.groups[0]) {
          this.setState({
            groups: res.data.groups,
            activeKey: res.data.groups[0].groupid
          },()=>{
            deal(res.data.groups[0].groupid);
          });
        }else{
          deal("");
        }
      }
    });
  }
  selectTabChange(selectKey) {
    this.setState({ activeKey: selectKey });
    deal(selectKey);
  }
  render() {
    return (
      <div id="test_zsrwl" class="app2X3 platform-app">
        <div className="title">
          {/*"701001RWCL-0033": "在手作业量"*/}
          {this.state.json["701001RWCL-0033"]}
        </div>
        <div id="zsrwl-select">
          {this.state.activeKey ? (
            <NCSelect
              defaultValue={this.state.activeKey}
              onChange={this.selectTabChange.bind(this)}
              style={{ width: 100, marginRight: 6 }}
              getPopupContainer={() => document.body}
            >
              {this.state.groups.map(one => (
                <NCOption value={one.groupid}>{one.groupname}</NCOption>
              ))}
            </NCSelect>
          ) : null}
        </div>
        <div id={this.echartsId} className="canvas"/>
      </div>
    );
  }
}

Test3 = createPage({
  mutiLangCode: "7010"
})(Test3);
export default Test3
// ReactDOM.render(<Test1 />, document.querySelector("#app"));
