import React, { Component } from 'react'
import './index.less'
import {ajax,base ,createPage, getMultiLang} from 'nc-lightapp-front'
const { NCSelect } = base
const NCOption = NCSelect.NCOption

function deal(groupid) {
	if(!groupid || '' == groupid){
		document.getElementById('adjust').innerHTML=("--");
		document.getElementById('notake').innerHTML=("--");
		return;
	}
	ajax({
	  url:  '/nccloud/ssctp/sscbd/SSCTaskManageYwglWidgetAction.do',
	  data: {"groupid":groupid},
	  loading: false,
	  success: (res) => {
		  document.getElementById('adjust').innerHTML=(res.data.adjust);
		  document.getElementById('notake').innerHTML=(res.data.notake);
		}
	});
}

 class Test1 extends Component {
	constructor(props) {
		super(props);
		this.state={
			activeKey:'',
			groups: [],
			json: {}
		}
	}
	componentWillMount() {
		let callback = (json) => {
			this.setState({json})
		}
		getMultiLang({moduleId: 7010, currentLocale: 'zh-CN',domainName: 'ssctp',callback})
	}
	componentDidMount() {
		ajax({
			url:  '/nccloud/ssctp/sscbd/SSCTaskManageYwglWidgetAction.do',
			data: {},
			loading: false,
			async: false,
			success: (res) => {
				if(res.data && res.data.groups && res.data.groups[0]){
					this.setState({groups: res.data.groups, activeKey: res.data.groups[0].groupid});
					deal(res.data.groups[0].groupid);
				}
			}
		});
	}
	selectTabChange(selectKey){
		this.setState({activeKey: selectKey});
		deal(selectKey);
	}
	open = (event, para) => {
		event.stopPropagation()
		if(para){
			this.props.openTo(
				'/ssctp/sscchargeman/sscchargeman/list/index.html',
				{
					pagecode: '701002RWGL_list',
					appcode: '701002RWGL',
                    // "701001RWCL-0028": "作业调度"
					name: this.state.json['701001RWCL-0028'],
					process: para
				}
			)
		}else{
			this.props.openTo(
				'/ssctp/sscchargeman/sscchargeman/list/index.html',
				{
					pagecode: '701002RWGL_list',
					appcode: '701002RWGL',
                    // "701001RWCL-0028": "作业调度"
					name: this.state.json['701001RWCL-0028']
				}
			)
		}
	}
	render() {
		return (
			<div id="test" class="app1X2 platform-app number homework" >
				<div className="title" onClick={(e) => {this.open(e)}}>
                    {/*"701001RWCL-0028": "作业调度"*/}
                    {this.state.json['701001RWCL-0028']}
				</div>
				<div id='rwgl-select'>
					{
						this.state.activeKey
						?
						(
							<NCSelect
								defaultValue={this.state.activeKey}
								onChange={this.selectTabChange.bind(this)}
								style={{ width: 100, marginRight: 6 }}
								getPopupContainer={() => document.body}
							>
								{
									this.state.groups.map(one => (
										<NCOption value={one.groupid}>
											{one.groupname}
										</NCOption>
									))
								}
								
							</NCSelect>
						)
						:
						null
					}
				</div>

				<div id='rwgl-con' onClick={(e) => {this.open(e, null)}}>
					<div className="content clearfix">
                        <img id='rwgl-icon' className="img" src="/nccloud/resources/ssctp/public/image/ywicon4.png" onClick={(e) => {this.open(e)}} />
						<div className="manage" onClick={(e) => {this.open(e)}}>
							{/*"701001RWCL-0021": "待提取"*/}
                            <div className="content-one notakeNum" id= 'notake'>--</div>
							<div className="title-one word">{this.state.json['701001RWCL-0021']}</div>
						</div>
						<div className="manage" onClick={(e) => {this.open(e, 'ADJUST')}}>
							{/*"701001RWCL-0014": "待调整"*/}
                            <div className="content-one adjustNum" id= 'adjust'>--</div>
							<div className="title-one word">{this.state.json['701001RWCL-0014']}</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
Test1 = createPage({
    mutiLangCode: '7010'
})(Test1)
export default Test1
// ReactDOM.render(<Test1 />, document.querySelector('#app'));
