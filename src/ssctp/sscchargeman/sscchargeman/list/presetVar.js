import 'ssccommon/components/globalPresetVar';

/**页面全局变量 */
let currentVar = {
    /**页面ID */
    pageId: 'tasklist',
    /**列表区域ID */
    list: 'tasklist',
    /**缩略区域ID */
    card: 'taskcard',
    /**操作记录区域ID */
    operationArea: 'ssctaskhistory',
    /**页签区域 */
    tabArea:{
        /**待提取 */
        notake: 'notake',
        /**待分配 */
        adjust: 'adjust',
        /**已提取 */
        taken: 'taken',
    },
    /**列表按钮区域 */
    listbtnarea: {
        /**待提取区域按钮 */
        notake: 'notakeButton',
        /**待分配区域按钮 */
        adjust: 'adjustButton',
        /**已提取区域按钮 */
        taken: 'takenButton',
    },
    /**列表按钮 */
    listButtons: {
        /**打开单据 */
        OpenBill: 'OpenBill',
        /**分配  */
        Distribute: 'Distribute',
        /**强制分配 */
        ForceDistribute: 'ForceDistribute',
        /**重新分配 */
        Redistribute: 'Redistribute',
        /**紧急 */
        Urgent: 'Urgent',
        /**不紧急 */
        NotUrgent: 'NotUrgent',
        /**取回 */
        Fetchback: 'Fetchback',
        /**强制取回 */
        ForceFetchback: 'ForceFetchback',
        /**收回 */
        Giveback: 'Giveback',
        /** 操作记录*/
        QueryOperation: 'QueryOperation',
        QueryOperation2: 'QueryOperation2',
        QueryOperation3: 'QueryOperation3'
    },
    acctionFlags: {
        /**分配  */
        Distribute: 'distribute',
        /**强制分配 */
        ForceDistribute: 'forcedistribute',
        /**重新分配 */
        Redistribute: 'redistribute',
        /**紧急 */
        Urgent: 'Y',
        /**不紧急 */
        NotUrgent: 'N',
        /**取回 */
        Fetchback: 'fetchback',
        /**强制取回 */
        ForceFetchback: 'forcefetchback'
    },
    /**页面展示形态 */
    showType: {
        list: 'list',
        card: 'card'
    }
}
window.presetVar = {
    ...window.presetVar,
    ...currentVar
};
export default currentVar