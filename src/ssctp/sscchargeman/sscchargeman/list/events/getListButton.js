import {listButtonClick} from './index'
import currentVar from '../presetVar';

let getListButtonRender = function(props){
    let btns = [];
    let l_render;
    let multiLang = props.MutiInit.getIntl(7010); 
    switch(this.queryKey.takestatus){
        case 'notake':
            l_render = (text, record, index) =>{
                if(record.urgent != null && record.urgent.value == "Y"){
                    return props.button.createOprationButton([currentVar.listButtons.ForceDistribute, currentVar.listButtons.NotUrgent,currentVar.listButtons.QueryOperation], {
                        area: currentVar.listbtnarea.notake,
                        buttonLimit: 3,
                        onButtonClick: (props, btnKey) => {
                            listButtonClick.listButtonClick.call(this,btnKey, record, index);
                        }
                    });
                }else{
                    return props.button.createOprationButton([currentVar.listButtons.ForceDistribute,currentVar.listButtons.Urgent,currentVar.listButtons.QueryOperation], {
                        area: currentVar.listbtnarea.notake,
                        buttonLimit: 3,
                        onButtonClick: (props, btnKey) => {
                            listButtonClick.listButtonClick.call(this, btnKey, record, index);
                        }
                    });
                }
            }
        break;
        case 'adjust':
            btns = [currentVar.listButtons.Distribute, currentVar.listButtons.Fetchback, currentVar.listButtons.Giveback,currentVar.listButtons.QueryOperation];
            l_render = (text, record, index) =>{
                return props.button.createOprationButton(btns, {
                    area: currentVar.listbtnarea.adjust,
                    buttonLimit: 3,
                    onButtonClick: (props, btnKey) => {
                        listButtonClick.listButtonClick.call(this, btnKey, record, index);
                    }
                });
            }
        break;
        case 'taken':
            btns = [currentVar.listButtons.Redistribute, currentVar.listButtons.ForceFetchback,currentVar.listButtons.QueryOperation];
            l_render = (text, record, index) =>{
                return props.button.createOprationButton(btns, {
                    area: currentVar.listbtnarea.taken,
                    buttonLimit: 3,
                    onButtonClick: (props, btnKey) => {
                        listButtonClick.listButtonClick.call(this, btnKey, record, index);
                    }
                });
            }
        break;
    }
    let event = {
        label: multiLang && multiLang.get('701001RWCL-0060'),
        itemtype: 'customer',
        attrcode: 'opr',
        visible: true,
        width: '205px',
        fixed: 'right',  
        render: l_render
    };
    return event;
}
export default {getListButtonRender}