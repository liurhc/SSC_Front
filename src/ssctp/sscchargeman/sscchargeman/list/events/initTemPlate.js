import {getMultiLang, ajax} from 'nc-lightapp-front';
import requestApi from '../requestApi'
import currentVar from '../presetVar';
import {listButtonClick, fixedTemplet, getListButton, query} from './index'


export default function (props) {
    const that = this
    props.createUIDom(
        {
            pagecode: '701002RWGL_list',
            appcode: '701002RWGL'
        },
        function (data) {
            getMultiLang({moduleId: 7010, domainName: 'ssctp',currentLocale: 'zh-CN', callback: (json) => {
                props.button.setButtons(data.button);
                data.template[window.presetVar.list].pagination = false;
                data.template[window.presetVar.list].items.push(getListButton.getListButtonRender.call(that, props));
                // data.template[currentVar.operationArea] = data.template.ssctaskhistory
                let _idx = -1
                while(++_idx < data.template[window.presetVar.list].items.length) {
                    let item = data.template[window.presetVar.list].items[_idx]
                    if (item.attrcode === 'billno') {
                        item.width = '130px'
                    }
                    if( item.attrcode=='pk_sscuser'){
                    if(that.queryKey.takestatus=='notake'){
                        item.visible=false;
                    } else {
                        item.visible=true;
                    }
                    }
                }
            
                props.meta.setMeta(data.template);
                props.table.setTableRender(window.presetVar.list, "billno", (text, record, index)=>{
                    let ret = [
                        (
                            <a className="billnoa" onClick={() => {
                                listButtonClick.listButtonClick.call(that, currentVar.listButtons.OpenBill, record, index);
                            }}>{record.billno.value}</a>
                        )
                    ]
                    if(record.urgent && record.urgent.value == 'Y') {
                        ret.push(<span className='billno-urgent'>{json['701001RWCL-0056']}</span>)//紧急
                    }
                    if(record.ismereject && record.ismereject.value == 'Y'){
                        ret.push(<span className="billno-urgent">{json['701001RWCL-0057']}</span>)//本人驳回
                    }
                    if(record.isleaderreject && record.isleaderreject.value == 'Y'){
                        ret.push(<span className="billno-urgent">{json['701001RWCL-0058']}</span>)//上级驳回
                    }
                    if(record.isappointed && record.isappointed.value == 'Y') {
                        ret.push(<span className='billno-urgent'>{json['701001RWCL-0055']}</span>)//强制分配
                    }
                    if(record.isunapprove && record.isunapprove.value == 'Y'){
                        ret.push( <span className="billno-reject">{json['701001RWCL-0169']}</span>)//取消审批
                    }
                    return ret
                })
                // props.table.setTableRender(window.presetVar.list, "amount", (text, record, index)=>{
                //         return([
                //                 <span>{record.pk_currtype.display}</span>,
                //                 <br/>,
                //                 <span>{record.amount.value}</span>
                //             ]
                //         )
                // })
                that.searchAreaConfig = {
                    searchadjust: data.template.searchadjust.items,
                    searchnotake: data.template.searchnotake.items,
                    searchtaken: data.template.searchtaken.items
                }

                data.template[window.presetVar.list].items.map((item)=>{
                    if(item.attrcode==='billno' 
                        || item.attrcode==='pk_tradetype'
                        || item.attrcode==='pk_currtype'
                        || item.attrcode==='remark'
                        || item.attrcode==='amount'
                        || item.attrcode==='pk_sscuser'
                        || item.attrcode==='billdate'
                        || item.attrcode==='billmaker'){

                        item.isSort = true;
                    }else{
                     
                        item.isSort = false;
                    }
                })

                // 查询默认条件数据
                query.queryData.call(that);
            }})
        }
    )
}
