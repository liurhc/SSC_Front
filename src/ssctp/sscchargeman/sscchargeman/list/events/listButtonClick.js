import requestApi from '../requestApi'
import currentVar from '../presetVar'
import {query} from './index'

let listButtonClick = function(btnKey, data, index, actionFrom){
    switch(btnKey){
        // 打开单据
        case currentVar.listButtons.OpenBill:
            openBill.call(this, data, index, actionFrom);
        break;
        // 分配
        case currentVar.listButtons.Distribute:
            showDistributeUserList.call(this, data, index, actionFrom, currentVar.acctionFlags.Distribute);
        break;
        // 强制分配
        case currentVar.listButtons.ForceDistribute:
            showDistributeUserList.call(this, data, index, actionFrom, currentVar.acctionFlags.ForceDistribute);
        break;
        // 重新分配
        case currentVar.listButtons.Redistribute:
            showDistributeUserList.call(this, data, index, actionFrom, currentVar.acctionFlags.Redistribute);
        break;
        // 紧急
        case currentVar.listButtons.Urgent:
            setUrgent.call(this, data, index, actionFrom, currentVar.acctionFlags.Urgent);
        break;
        // 不紧急
        case currentVar.listButtons.NotUrgent:
            setUrgent.call(this, data, index, actionFrom, currentVar.acctionFlags.NotUrgent);
        break;
        // 取回
        case currentVar.listButtons.Fetchback:
            takeBack.call(this, data, index, actionFrom, currentVar.acctionFlags.Fetchback);
        break;
        // 强制取回
        case currentVar.listButtons.ForceFetchback:
            takeBack.call(this, data, index, actionFrom, currentVar.acctionFlags.ForceFetchback);
        break;
        // 收回
        case currentVar.listButtons.Giveback:
            giveBack.call(this, data, index, actionFrom);
        break;   
        // 收回
        case currentVar.listButtons.QueryOperation:
            handleQueryOperation.call(this, data, index, actionFrom)
        break;
        
    }
}

let openBill = function(data, index, actionFrom){
    let listData = data;
    if(actionFrom == "detail"){
        listData = this.data.listRows[index].values;
    }
    requestApi.openBill({
        data: {
            billtypeCode: listData.billtypecode.value, 
            transtypeCode: listData.transtypecode.value,
            billid: listData.busiid.value
        },
        success: (data) => {
            this.props.specialOpenTo(
                data.data.url,
                {
                    ...data.data.data,
                    scene: 'zycx'
                }
            )
        }
    })
}

let setUrgent = function(data, index, actionFrom, urgent){
    let listData = data;
    if(actionFrom == "detail"){
        listData = this.data.listRows[index].values;
    }
    requestApi.setUrgent({
        data: {pk_task: listData.pk_task.value, urgent: urgent, ts: listData.ts.value},
        success: (data) => {
            // window.open(data.data.url);
            this.data.listRows[index].values.urgent = {value : urgent};
            this.data.listRows[index].values.ts = {value : data.data.ts};
            // 设置列表数据
            this.props.table.setAllTableData(window.presetVar.list, {
                areacode: window.presetVar.list,
                rows: this.data.listRows
            });
        }
    })
}

let showDistributeUserList = function(data, index, actionFrom, type){
    let listData = data;
    if(actionFrom == "detail"){
        listData = this.data.listRows[index].values;
    }
    this.setState({
        showModalDistribute: true, 
        operationData: listData,
        distributeType: type
    });
} 

let takeBack = function(data, index, actionFrom, type){
    let listData = data;
    if(actionFrom == "detail"){
        listData = this.data.listRows[index].values;
    }
    requestApi.takeBack({
        data: {
            pks: listData.pk_task.value, 
            type: type
        },
        success: (data) => {
            query.queryData.call(this);
        }
    })
}

let giveBack = function(data, index, actionFrom){
    let listData = data;
    if(actionFrom == "detail"){
        listData = this.data.listRows[index].values;
    }
    requestApi.giveBack({
        data: {pks: listData.pk_task.value},
        success: (data) => {
            query.queryData.call(this);
        }
    })
}

const handleQueryOperation = function (data, index, actionFrom) {
    let listData = data
    if(actionFrom == "detail"){
        listData = this.data.listRows[index].values
    }
    this.setState({showOperationNote: true})
    requestApi.taskQueryOperationAction({
        data: {
            busiid: listData.busiid.value
        },
        success: (data) => {
            if(data.success && data.data.ssctaskhistory) {
                this.props.table.setAllTableData(
                    currentVar.operationArea, 
                    data.data.ssctaskhistory
                )
            }
        }
    })
}

export default {listButtonClick, openBill, setUrgent, takeBack, giveBack}

