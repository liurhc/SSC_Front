import requestApi from '../requestApi'
import currentVar from '../presetVar';
import {initTemplate} from './index'


/**查询条件选择变更 */
let handleConditionChange = function(attrcode, value){
    // 更新查询条件
    this.queryKey[attrcode]=value;
    // 查询数据
    queryData.call(this);
}

/**查询数据 */
let queryData = function(isAddPage){
    // 复制列表数据
    let copyData = function(data){
        let newData = {};
        newData.values = {};
        for(let child in data.values){
            newData.values[child] = {
                display: data.values[child].display,
                value: data.values[child].value,
                scale: data.values[child].scale 
            }
        }
        return newData;
    }
    if(isAddPage){
        // 更新查询条件
        this.queryKey.pageinfo.number++;
    }else{
        this.queryKey.pageinfo.number = 1;
    }
    requestApi.query({
        data: this.queryKey,
        success: (data) => {
            let listRows = [];
            if(isAddPage){
                this.data.listRows.map((one)=>{
                    listRows.push(copyData(one));
                })
            }else{
                this.detailRef.clearAllDetailData();
            }
            data.data[window.presetVar.list].rows.map((one)=>{
                listRows.push(copyData(one));
            })
            this.data.listRows = listRows;
            // 设置列表数据
            this.props.table.setAllTableData(window.presetVar.list, {
                areacode: window.presetVar.list,
                rows: listRows
            });
            // 设置缩略数据
            this.detailRef.addDetailData(data.data[window.presetVar.card]);
            let newState = {};
            newState = this.state
            // 更新数量
            if(data.data.adjust != null){
                newState.showNumbers.adjust=data.data.adjust;
            }
            if(data.data.notake != null){
                newState.showNumbers.notake=data.data.notake;
            }
            // 更新查询条件区域
            let conditions = null
            switch(this.queryKey.takestatus) {
                case 'notake':
                    conditions = this.searchAreaConfig.searchnotake
                break
                case 'taken':
                    conditions = this.searchAreaConfig.searchtaken
                break
                case 'adjust':
                    conditions = this.searchAreaConfig.searchadjust
                break
                default:
                    conditions = this.searchAreaConfig.searchnotake
                break
            }
            let idx = -1
            conditions.forEach((ele, index) => {
                if (ele.attrcode === 'pk_tradetype') {
                    idx = index
                }
            })
            if (idx != -1) {
                conditions[idx] = data.data.searchArea.items[0]
            }
            newState.searcharea.conditions = conditions
            // 更新页信息
            this.queryKey.pageinfo = data.data[window.presetVar.list].pageinfo;
            if(this.queryKey.pageinfo.totalPages > this.queryKey.pageinfo.number){
                newState.pageControll = 'notend';
            }else if(this.queryKey.pageinfo.totalPages == this.queryKey.pageinfo.number){
                newState.pageControll = 'end';
            }else{
                newState.pageControll = 'none';
            }
            this.setState(newState);
            setTimeout(()=>{this.canChangPageFlag=true;},100);
        }
    })
}

/**模糊查询输入内容变化 */
let onFuzzyChange = function(data){
    this.fuzzyKey = data;
    let newQueryKey = {};
    let multiLang = this.props.MutiInit.getIntl(7010); 
    for(let child in this.queryKey){
        newQueryKey[child] = this.queryKey[child];
    }
    newQueryKey.fuzzyQueryKey = data;
    setTimeout((that, data)=>{
        if(data == that.fuzzyKey){
            requestApi.queryFuzzyKey({
                data: newQueryKey,
                success: (data) => {
                    for(let childName in data.data){
                        data.data[childName].value = data.data[childName].value+"="+that.fuzzyKey;
                        data.data[childName].key = data.data[childName].key+(multiLang && multiLang.get('701001RWCL-0061'))+that.fuzzyKey;//包含
                    }
                    that.setState({fuzzyquerychild:data.data});
                }
            });
        }
    },500, this, data);
}
/**模糊查询选择 */
let onFuzzySelected = function(data){
    this.setState({fuzzyquerychild:[]})
    // 更新查询条件
    this.queryKey.fuzzyQueryKey=data;
    // 查询数据
    queryData.call(this);
}

/**已完成/未完成 页签切换 */
let selectTabChange = function(selectKey){
    switch(selectKey){
        case currentVar.tabArea.notake:
            this.setState({twoColNums: 3, detailButtonArea: currentVar.listbtnarea.notake});
        break;
        case currentVar.tabArea.adjust:
            this.setState({twoColNums: 3, detailButtonArea: currentVar.listbtnarea.adjust});
        break;
        case currentVar.tabArea.taken:
            this.setState({twoColNums: 4, detailButtonArea: currentVar.listbtnarea.taken});
        break;
    }
    // 更新查询条件
    this.queryKey.takestatus=selectKey;
    this.setState({currentKey: selectKey})
    // 初始化页面
    initTemplate.call(this, this.props)
}

export default {handleConditionChange, queryData, onFuzzyChange, onFuzzySelected, selectTabChange}