import currentVar from '../presetVar';
export default {
  /*    button:[
      {
            "id": "0001A41000000006J5B1",
            "type": "button_main",
            "key": currentVar.listButtons.ForceDistribute,
            "title": "强制分配",
            "area": currentVar.listbtnarea.notake,
            "children": []
        },
        {
            "id": "0001A41000000006J5B1",
            "type": "button_main",
            "key": currentVar.listButtons.Urgent,
            "title": "紧急",
            "area": currentVar.listbtnarea.notake,
            "children": []
        },
        {
            "id": "0001A41000000006J5B1",
            "type": "button_main",
            "key": currentVar.listButtons.NotUrgent,
            "title": "不紧急",
            "area": currentVar.listbtnarea.notake,
            "children": []
        },
        {
            "id": "0001A41000000006J5B1",
            "type": "button_main",
            "key": currentVar.listButtons.Distribute,
            "title": "分配",
            "area": currentVar.listbtnarea.adjust,
            "children": []
        },
                {
            "id": "0001A41000000006J5B1",
            "type": "button_main",
            "key": currentVar.listButtons.Fetchback,
            "title": "取回",
            "area": currentVar.listbtnarea.adjust,
            "children": []
        },
        {
            "id": "0001A41000000006J5B1",
            "type": "button_main",
            "key": currentVar.listButtons.Giveback,
            "title": "退回",
            "area": currentVar.listbtnarea.adjust,
            "children": []
        },
        {
            "id": "0001A41000000006J5B1",
            "type": "button_main",
            "key": currentVar.listButtons.Redistribute,
            "title": "重新分配",
            "area": currentVar.listbtnarea.taken,
            "children": []
        },
        {
            "id": "0001A41000000006J5B1",
            "type": "button_main",
            "key": currentVar.listButtons.ForceFetchback,
            "title": "强制取回",
            "area": currentVar.listbtnarea.taken,
            "children": []
        },
        {
            "id": "0001A41000000006J5B1",
            "type": "button_main",
            "key": currentVar.listButtons.QueryOperation,
            "title": "操作记录",
            "area": currentVar.listbtnarea.notake,
            "children": []
        },
        {
            "id": "0001A41000000006J5B1",
            "type": "button_main",
            "key": currentVar.listButtons.QueryOperation2,
            "title": "操作记录",
            "area": currentVar.listbtnarea.adjust,
            "children": []
        },
        {
            "id": "0001A41000000006J5B1",
            "type": "button_main",
            "key": currentVar.listButtons.QueryOperation3,
            "title": "操作记录",
            "area": currentVar.listbtnarea.taken,
            "children": []
        }
    ]
    
    addModel:{
        moduletype: 'form',
        items: [
            {
                "itemtype": "label",
                "label": "委托关系主键",
                "maxlength": "20",
                "disabled": true,
                "attrcode": "pk_ssclientage"
            },
            {
                "itemtype": "label",
                "label": "共享服务中心主键",
                "maxlength": "20",
                "disabled": true,
                "attrcode": "pk_sscunit"
            },
            {
                "itemtype": "input",
                "visible": true,
                "label": "业务单元编码",
                "maxlength": "20",
                "disabled": true,
                "attrcode": "pk_org.code"
            },
            {
                "itemtype": "refer",
                "visible": true,
                "label": "业务单元",
                "maxlength": "20",
                "refcode": "uapbd/refer/org/BusinessUnitTreeRef/index.js",
                "required": true,
                "attrcode": "pk_org"
            },
            {
                "itemtype": "input",
                "visible": true,
                "label": "所属集团",
                "maxlength": "20",
                "disabled": true,
                "attrcode": "pk_org.pk_group.name"
            },
            {
                "itemtype": "checkbox",
                "visible": true,
                "label": "费用管理",
                "maxlength": "1",
                "attrcode": "busiunittype1",
                "initialvalue": {//控件默认值
                    "value": false,
                    "display": ""
                }
            },
            {
                "itemtype": "checkbox",
                "visible": true,
                "label": "应收管理",
                "maxlength": "1",
                "attrcode": "busiunittype2",
                "initialvalue": {//控件默认值
                    "value": false,
                    "display": ""
                }
            },
            {
                "itemtype": "checkbox",
                "visible": false,
                "label": "应付管理",
                "maxlength": "1",
                "attrcode": "busiunittype3",
                "initialvalue": {//控件默认值
                    "value": false,
                    "display": ""
                },
            },
            {
                "itemtype": "checkbox",
                "visible": false,
                "label": "固定资产",
                "maxlength": "1",
                "attrcode": "busiunittype4",
                "initialvalue": {//控件默认值
                    "value": false,
                    "display": ""
                }
            },
            {
                "itemtype": "checkbox",
                "visible": false,
                "label": "总账",
                "maxlength": "1",
                "attrcode": "busiunittype7",
                "initialvalue": {//控件默认值
                    "value": false,
                    "display": ""
                }
            },
            {
                "itemtype": "checkbox",
                "visible": false,
                "label": "现金管理",
                "maxlength": "1",
                "attrcode": "busiunittype5",
                "initialvalue": {//控件默认值
                    "value": false,
                    "display": ""
                }
            },
            {
                "itemtype": "checkbox",
                "label": "存货核算",
                "maxlength": "1",
                "attrcode": "busiunittype8",
                "initialvalue": {//控件默认值
                    "value": false,
                    "display": ""
                }
            },
            {
                "itemtype": "checkbox",
                "label": "自由表单",
                "maxlength": "1",
                "attrcode": "busiunittype6",
                "initialvalue": {//控件默认值
                    "value": false,
                    "display": ""
                }
            },
            {
                "itemtype": "checkbox",
                "visible": true,
                "label": "到账通知",
                "maxlength": "1",
                "attrcode": "busiunittype9",
                "initialvalue": {//控件默认值
                    "value": false,
                    "display": ""
                }
            },
            {
                "itemtype": "checkbox",
                "visible": true,
                "label": "共享合同",
                "maxlength": "1",
                "attrcode": "busiunittype10",
                "initialvalue": {//控件默认值
                    "value": false,
                    "display": ""
                }
            },
            {
                "itemtype": "checkbox",
                "label": "业务分类11",
                "maxlength": "1",
                "attrcode": "busiunittype11",
                "initialvalue": {//控件默认值
                    "value": false,
                    "display": ""
                }
            },
            {
                "itemtype": "checkbox",
                "label": "业务分类12",
                "maxlength": "1",
                "attrcode": "busiunittype12",
                "initialvalue": {//控件默认值
                    "value": false,
                    "display": ""
                }
            },
            {
                "itemtype": "checkbox",
                "label": "业务分类13",
                "maxlength": "1",
                "attrcode": "busiunittype13",
                "initialvalue": {//控件默认值
                    "value": false,
                    "display": ""
                }
            },
            {
                "itemtype": "checkbox",
                "label": "业务分类14",
                "maxlength": "1",
                "attrcode": "busiunittype14",
                "initialvalue": {//控件默认值
                    "value": false,
                    "display": ""
                }
            },
            {
                "itemtype": "checkbox",
                "label": "业务分类15",
                "maxlength": "1",
                "attrcode": "busiunittype15",
                "initialvalue": {//控件默认值
                    "value": false,
                    "display": ""
                }
            },
            {
                "itemtype": "checkbox",
                "label": "业务分类16",
                "maxlength": "1",
                "attrcode": "busiunittype16",
                "initialvalue": {//控件默认值
                    "value": false,
                    "display": ""
                }
            },
            {
                "itemtype": "checkbox",
                "label": "业务分类17",
                "maxlength": "1",
                "attrcode": "busiunittype17",
                "initialvalue": {//控件默认值
                    "value": false,
                    "display": ""
                }
            },
            {
                "itemtype": "checkbox",
                "label": "业务分类18",
                "maxlength": "1",
                "attrcode": "busiunittype18",
                "initialvalue": {//控件默认值
                    "value": false,
                    "display": ""
                }
            },
            {
                "itemtype": "checkbox",
                "label": "业务分类19",
                "maxlength": "1",
                "attrcode": "busiunittype19",
                "initialvalue": {//控件默认值
                    "value": false,
                    "display": ""
                }
            },
            {
                "itemtype": "checkbox",
                "label": "业务分类20",
                "maxlength": "1",
                "attrcode": "busiunittype20",
                "initialvalue": {//控件默认值
                    "value": false,
                    "display": ""
                }
            },
            {
                "itemtype": "checkbox",
                "label": "启用状态",
                "maxlength": "1",
                "attrcode": "enablestate",
            },
            {
                "itemtype": "label",
                "label": "创建人",
                "maxlength": "20",
                "disabled": true,
                "attrcode": "creator"
            },
            {
                "itemtype": "label",
                "label": "创建时间",
                "maxlength": "19",
                "disabled": true,
                "attrcode": "creationtime"
            },
            {
                "itemtype": "label",
                "label": "最后修改人",
                "maxlength": "20",
                "disabled": true,
                "attrcode": "modifier"
            },
            {
                "itemtype": "label",
                "label": "最后修改时间",
                "maxlength": "19",
                "disabled": true,
                "attrcode": "modifiedtime"
            },
            {
                "itemtype": "label",
                "label": "共享服务类型名称",
                "maxlength": "200",
                "disabled": true,
                "attrcode": "ssctypename",
                "initialvalue": {//控件默认值
                    "value": "财务共享服务",
                    "display": "财务共享服务"
                }
            },
            {
                "itemtype": "label",
                "label": "时间戳",
                "maxlength": "19",
                "disabled": true,
                "attrcode": "ts"
            }
        ],
        status: 'edit',
    },
    distributeUserModel:{
        moduletype: 'form',
        items: [
            {
                "itemtype": "label",
                "label": "委托关系主键",
                "maxlength": "20",
                "disabled": true,
                "attrcode": "pk_ssclientage"
            }
        ]
    } */
}