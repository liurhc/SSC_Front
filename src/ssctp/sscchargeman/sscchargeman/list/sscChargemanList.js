import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {createPage, base, ajax} from 'nc-lightapp-front';
const {NCCheckbox,NCModal,NCButton, NCAffix} = base;
import {initTemplate, afterEvent, listButtonClick,query} from './events';
import requestApi from './requestApi'
import currentVar from './presetVar';
import WorkbenchTab from 'ssccommon/components/workbench-tab'
import WorkbenchSearcharea from 'ssccommon/components/workbench-searcharea'
import WorkbenchDetail from 'ssccommon/components/workbench-detail'
import DistributeUserList from './components/distributeUserList'
import OperationNote from './components/queryOperation'

import './index.less';

class SscChargemanList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            twoColNums: 3,
            showType: currentVar.showType.list,
            showNumbers:{},
            searcharea:{
                conditions:[]
            },
            pageControll: 'none',
            fuzzyquerychild: [],
            showModalDistribute: false,
            operationData: {},
            distributeType: '',
            detailButtonArea: currentVar.listbtnarea.notake,
            defaultKey: 'notake',
            currentKey: 'notake',
            showOperationNote: false
        };
        // 查询条件
        this.queryKey={
            // 待提取（notake）/待调整（adjust）/已提取（taken）
            takestatus: currentVar.tabArea.notake,
            fuzzyQueryKey: [],
            // 页信息
            pageinfo:{
                number: 1,
                size: 20,
                totalElements: 0,
                totalPages: 0
            }
        }
        // 缓存查询结果数据
        this.data={
            listRows:[],
            cardRows:[]
        }

        // 延时控制标记
        // 翻页
        this.canChangPageFlag = true;
        // 模糊查询
        this.fuzzyKey = '';

        this.detailRef = {};

        this.searchAreaConfig = {}

        // 页面初始化
        // initTemplate.call(this, props);
    }
    componentDidMount() {
        const that = this
        // Firefox
        window.addEventListener('DOMMouseScroll', (ev) => {
            ev = ev || window.event
            let onOff = null
            if(ev.detail < 0){
                onOff = true
            }else{
                onOff = false
            }
            let t = document.documentElement.scrollTop || document.body.scrollTop
            let h = document.documentElement.scrollHeight || document.body.scrollHeight
            let bh = document.documentElement.clientHeight || document.body.clientHeight
            if(
                !onOff 
                && h-bh-t < 30 
                && that.queryKey.pageinfo.totalPages > that.queryKey.pageinfo.number 
                && that.canChangPageFlag
            ) {
                that.canChangPageFlag = false
                setTimeout(() => { query.queryData.call(that, true) }, 300)
            }
        })
        // 其他浏览器
        window.onmousewheel = (ev) => {
            ev = ev || window.event
            let onOff = null
            if(ev.wheelDelta > 0){
                onOff = true
            }else{
                onOff = false
            }
            let t = document.documentElement.scrollTop || document.body.scrollTop
            let h = document.documentElement.scrollHeight || document.body.scrollHeight
            let bh = document.documentElement.clientHeight || document.body.clientHeight
            if(
                !onOff 
                && h-bh-t < 30 
                && that.queryKey.pageinfo.totalPages > that.queryKey.pageinfo.number 
                && that.canChangPageFlag
            ) {
                that.canChangPageFlag = false
                setTimeout(() => { query.queryData.call(that, true) }, 300)
            }
        }
    }
    componentWillMount() {
        // debugger
        const key = this.props.getUrlParam("process") ? this.props.getUrlParam("process").toLowerCase() : 'notake'
        this.setState(
            {defaultKey: key, currentKey: key},
            () => {
                this.queryKey.takestatus = key
                initTemplate.call(this, this.props)
            }
        )
    }

    // 列表/缩略切换
    showTypeClick(type){
        this.setState({showType: type});
    }

    // 去往优先级管理
    backToChargemanmanage = (key) => {
        switch (key) {
            case 'back':
                this.props.linkTo(
                    '/ssctp/sscchargeman/sscprioritymanage/list/',
                    {
                        pagecode: '700106RWGLYXJ_List',
                        appcode: '701002RWGL'
                    }
                )        
            break
            default: 
            break
        }
    }

    queryDataCallback = (data) => {
        let copyData = function(data) {
            let newData = {}
            newData.values = {}
            for(let child in data.values){
                newData.values[child] = {
                    display: data.values[child].display,
                    value: data.values[child].value,
                    scale: data.values[child].scale 
                }
            }
            return newData
        }

        let listRows = []
        this.detailRef.clearAllDetailData()

        data[window.presetVar.list].rows.map((one)=>{
            listRows.push(copyData(one))
        })
        this.data.listRows = listRows
        // 设置列表数据
        this.props.table.setAllTableData(window.presetVar.list, {
            areacode: window.presetVar.list,
            rows: listRows
        })
        // 设置缩略数据
        this.detailRef.addDetailData(data[window.presetVar.card]);
        let newState = {}
        newState = this.state
        // 更新数量
        if(data.adjust != null){
            newState.showNumbers.adjust=data.adjust;
        }
        if(data.notake != null){
            newState.showNumbers.notake=data.notake;
        }
        // 更新查询条件区域
        let conditions = null
        switch(this.queryKey.takestatus) {
            case 'notake':
                conditions = this.searchAreaConfig.searchnotake
            break
            case 'taken':
                conditions = this.searchAreaConfig.searchtaken
            break
            case 'adjust':
                conditions = this.searchAreaConfig.searchadjust
            break
            default:
                conditions = this.searchAreaConfig.searchnotake
            break
        }
        let idx = -1
        conditions.forEach((ele, index) => {
            if (ele.attrcode === 'pk_tradetype') {
                idx = index
            }
        })
        if (idx != -1) {
            conditions[idx] = data.searchArea.items[0]
        }
        newState.searcharea.conditions = conditions
        // 更新页信息
        this.queryKey.pageinfo = data[window.presetVar.list].pageinfo
        if(this.queryKey.pageinfo.totalPages > this.queryKey.pageinfo.number){
            newState.pageControll = 'notend'
        }else if(this.queryKey.pageinfo.totalPages == this.queryKey.pageinfo.number){
            newState.pageControll = 'end'
        }else{
            newState.pageControll = 'none'
        }
        this.setState(newState)
        setTimeout(()=>{this.canChangPageFlag=true},100);
    }

// 处理排序后的模板变化
handleTempletAfterSort (sortParam){
    if(sortParam[0].order=='flatscend'){
        this.queryKey.orderByInfo = [];
    }else {
        this.queryKey.orderByInfo = sortParam;
    }
    let sortObj = {};
    sortParam.forEach(item => {
         sortObj[item.field] = item;
    });
    let meta = this.props.meta.getMeta()
    meta[window.presetVar.list].items.forEach(item => {
        //保存返回的column状态，没有则终止order状态
        if(sortObj[item.attrcode]){
            item.order = sortObj[item.attrcode].order;
            item.orderNum = sortObj[item.attrcode].orderNum;
        }else {
            item.order = "flatscend";
            item.orderNum = "";
        }
    })
    this.props.meta.setMeta(meta);
}    

    render() {
        const {table} = this.props
        const {createSimpleTable} = table
        let multiLang = this.props.MutiInit.getIntl(7010); //this.moduleId
        let isShow = (type) =>{
            if(this.state.showType == currentVar.showType.list && type == currentVar.showType.list){
                return 'show';
            }else if(this.state.showType == currentVar.showType.card && type == currentVar.showType.card){
                return 'show';
            }else{
                return 'hide';
            }
        }
        let addMoreOnOff = this.data.listRows.length > 0 && this.queryKey.pageinfo.totalPages > this.queryKey.pageinfo.number
        let getDetailBtns = (data, index) =>{ 
            switch(this.queryKey.takestatus){
                case currentVar.tabArea.notake:
                    if(this.data.listRows[index].values.urgent != null && this.data.listRows[index].values.urgent.value == "Y"){
                        return [currentVar.listButtons.ForceDistribute, currentVar.listButtons.NotUrgent, currentVar.listButtons.QueryOperation];
                    }else{
                        return [currentVar.listButtons.ForceDistribute,currentVar.listButtons.Urgent, currentVar.listButtons.QueryOperation];
                    }
                case currentVar.tabArea.adjust:
                    return [currentVar.listButtons.Distribute, currentVar.listButtons.Fetchback, currentVar.listButtons.Giveback, currentVar.listButtons.QueryOperation];
                case currentVar.tabArea.taken:
                    return [currentVar.listButtons.Redistribute, currentVar.listButtons.ForceFetchback, currentVar.listButtons.QueryOperation];
            }
        }
        return (
            <div id="sscChargemanList" className={addMoreOnOff ? 'add-more-wrap' : ''}>
                <div className="container">
                    <NCAffix offsetTop={0}>
                        <div className="workbench-tab">
                            <WorkbenchTab
                                defaultActiveKey={this.state.defaultKey}
                                tabs={[
                                    // "701001RWCL-0014": "待调整"
                                    // "701001RWCL-0021": "待提取"
                                    // "701001RWCL-0026": "已提取"
                                    {attrcode: currentVar.tabArea.notake, label:multiLang && multiLang.get('701001RWCL-0021')},
                                    {attrcode: currentVar.tabArea.adjust, label:multiLang && multiLang.get('701001RWCL-0014')},
                                    {attrcode: currentVar.tabArea.taken, label:multiLang && multiLang.get('701001RWCL-0026')}
                                ]}
                                showNumbers={this.state.showNumbers}
                                selectTabChange={query.selectTabChange.bind(this)}
                                showBtnsRight={true}
                                btnsRight={
                                    [
                                        {
                                            // "701001RWCL-0027": "优先级管理"
                                            name: multiLang && multiLang.get('701001RWCL-0027'),
                                            color: 'default',
                                            key: 'back'
                                        }
                                    ]
                                }
                                btnsClick={this.backToChargemanmanage}
                                currentKey={this.state.currentKey}
                            />
                        </div>
                    </NCAffix>
                    <div className='workbench-searcharea'>
                        <WorkbenchSearcharea
                            {...this.props}
                            twoColNums={this.state.twoColNums}
                            cusType="tp_zycx"
                            conditions={this.state.searcharea.conditions}
                            handleConditionChange={query.handleConditionChange.bind(this)}
                            fuzzyquerychild = {this.state.fuzzyquerychild}
                            onFuzzyChange={query.onFuzzyChange.bind(this)}
                            onFuzzySelected={query.onFuzzySelected.bind(this)}
                            showTypeClick={this.showTypeClick.bind(this)}
                            barcodeApi={(opt) => {
                                ajax({
                                    url: '/nccloud/ssctp/sscbd/SSCTaskManageQueryAction.do',
                                    data: opt.data,
                                    success: (data) => {
                                        opt.success(data.data)
                                    }
                                })
                            }}
                            queryKey={this.queryKey}
                            queryDataCallback={this.queryDataCallback}
                        />
                    </div>
                    <div className={isShow(currentVar.showType.list)} style={{position: 'relative'}}>
                        {createSimpleTable(window.presetVar.list, {                            
                            showIndex:true,
                            adaptionHeight:true,
                            sort:{
								mode:'single',
                                backSource:true,
                                sortFun:(a)=> {
                                    this.handleTempletAfterSort(a);
                                    query.queryData.call(this, false);
                                }
                            }
                        })}
                        <div
                            className='scroll-add-more'
                            style={{display: addMoreOnOff ? 'block' : 'none'}}
                        >{multiLang && multiLang.get('701001RWCL-0062')}</div>{/*"701001RWCL-0062": "滑动加载更多"*/}
                    </div>
                    <div className={isShow(currentVar.showType.card)}>
                        <WorkbenchDetail  
                            buttonArea={this.state.detailButtonArea}
                            ref={(ref) => { this.detailRef = ref; }}
                            props={this.props}
                            doAction={listButtonClick.listButtonClick.bind(this)}
                            getBtns={(data, index)=>getDetailBtns(data, index)}
                        />
                        <div
                            className='scroll-add-more'
                            style={{display: addMoreOnOff ? 'block' : 'none'}}
                        >{multiLang && multiLang.get('701001RWCL-0062')}</div>
                    </div>
                </div>
                {/* 分配人员弹出框 */}
                <DistributeUserList
                    {...this.props} 
                    distributeType={this.state.distributeType}
                    showModal={this.state.showModalDistribute}
                    data={this.state.operationData}
                    that={this}
                />
                <OperationNote
                    {...this.props}
                    showModal={this.state.showOperationNote}
                    close={() => {
                        this.setState({showOperationNote: false})
                        this.props.table.setAllTableData(currentVar.operationArea, {rows: []})
                    }}
                />
            </div>
        )
    }
}
SscChargemanList = createPage({     
    mutiLangCode: '7010'
})(SscChargemanList);
export default SscChargemanList;