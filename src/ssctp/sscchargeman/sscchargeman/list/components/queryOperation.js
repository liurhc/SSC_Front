import React, {Component} from 'react'
import {base} from 'nc-lightapp-front'
const {NCModal,NCButton} = base
import currentVar from '../presetVar';
import './operateNote.less'

class OperationNote extends Component {
    constructor(props) {
        super(props)
    }

    close = () => {
        this.props.close()
    }

    render() {

        let {data, showModal, table} = this.props
        const {createSimpleTable} = table
        let multiLang = this.props.MutiInit.getIntl(7010);
        return (          
            <div>
                <NCModal show={showModal} className='query-operation'>
                    <NCModal.Header>
                        {/*"701001RWCL-0008": "操作记录"*/}
                        <NCModal.Title>{multiLang && multiLang.get('701001RWCL-0008')}</NCModal.Title>
                        <span 
                            className='viewer-close'
                            onClick={this.close}
                        >X</span>
                    </NCModal.Header>
                    <NCModal.Body>
                            {createSimpleTable(
                                currentVar.operationArea,
                                {
                                    showIndex:true
                                }
                            )}
                    </NCModal.Body>
                </NCModal>
            </div>
        )
    }
}

export default OperationNote