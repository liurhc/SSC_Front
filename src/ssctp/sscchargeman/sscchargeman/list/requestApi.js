import {ajax } from 'nc-lightapp-front';

let requestDomain =  '';

let requestApiOverwrite = {
    //单据模板接口
    tpl: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCTaskTemQryAction.do',
            data: opt.data,
            success: opt.success
        });
    },
    // 查询接口
    query: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCTaskManageQueryAction.do',
            data: opt.data,
            success: opt.success
        });
    },
    // 查询接口
    queryFuzzyKey: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCTaskBlurQueryAction.do',
            data: opt.data,
            success: opt.success
        });
    },
    // 查看单据
    openBill: (opt) => {
        ajax({
            url: '/nccloud/sscrp/rpbill/BrowseBillAction.do',
            data: opt.data,
            success: (data) => {
                opt.success(data)
            }
        });
    },
    // 设值紧急状态
    setUrgent: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCTaskUrgentAction.do',
            data: opt.data,
            success: opt.success
        });
    },
    takeBack: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCTaskManageFetchBackAction.do',
            data: opt.data,
            success: opt.success
        });
    },
    giveBack: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCTaskManageGiveBackAction.do',
            data: opt.data,
            success: opt.success
        });
    },
    // 作业任务联查操作记录
    taskQueryOperationAction: (opt) => { 
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCTaskHandleQueryOperationAction.do',
            data: opt.data,
            success: (data) => {
                opt.success(data)
            }
        })
    }
}

export default  requestApiOverwrite;