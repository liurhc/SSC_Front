import {listButtonClick} from './index'

export default function getListButtonRender(props){
    const that = this
    const {
        idx,
        prioritys
    } = this.state.showPriority
    let multiLang = props.MutiInit.getIntl(7010)
    return {
        label: multiLang && multiLang.get('701001RWCL-0060'),//操作
        itemtype: 'customer',
        attrcode: 'opr',
        visible: true,
        fixed: 'right',
        width: '200px',
        render: (text, record, index) =>{
            return (
                <div className='opr-actions'>
                    {
                        [
                            props.button.createOprationButton(
                                ['forcedistribute'],
                                {
                                    area: window.presetVar.list,
                                    itemtype: 'customer',
                                    onButtonClick: (props, btnKey) => {
                                        listButtonClick(props, btnKey, text, record, index, that)
                                    }
                                }
                            ),
                            props.button.createOprationButton(
                                ['TopPlacement'],
                                {
                                    area: window.presetVar.list,
                                    itemtype: 'customer',
                                    onButtonClick: (props, btnKey) => {
                                        listButtonClick(props, btnKey, text, record, index, that)
                                    }
                                }
                            ),
                            (
                                <span
                                    className="sort-btn"
                                    onClick={() => {
                                        listButtonClick(props, 'sort', text, record, index, that)
                                    }}
                                >
                                    {/*"701001RWCL-0063": "优先级设置*/}
                                    {multiLang && multiLang.get('701001RWCL-0063')}
                                    {
                                        index == idx
                                        &&
                                        (
                                            <div className='rank-update-priority'>
                                                {
                                                    prioritys.map(ele => (
                                                        <span
                                                            onClick={(e) => {
                                                                e = e || window.event
                                                                e.stopPropagation()
                                                                e.cancelBubble = true
                                                                listButtonClick(props, 'RankingUpdatePriority', text, record, index, that, ele.value)
                                                            }}
                                                            className={
                                                                ele.display === record.priority.display
                                                                ?
                                                                'active'
                                                                :
                                                                ''
                                                            }
                                                        >{ele.display}</span>
                                                    ))
                                                }
                                            </div>
                                        )
                                    }
                                </span>
                            )
                        ]
                    }
                    <i 
                        className={record.priority.display === that.state.currentpriorityName ? 'status-action active' : 'status-action'}
                    ></i>
                </div>
            )
        }
    }
}