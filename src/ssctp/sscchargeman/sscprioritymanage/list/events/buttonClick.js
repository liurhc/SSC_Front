import requestApi from '../requestApi'
import {initTemplate} from './index'

export default function buttonClick(key, index) {
    switch (key) {
        case 'GoBack':
            this.props.linkTo(
                '/ssctp/sscchargeman/sscchargeman/list/',
                {
                    pagecode: '701002RWGL_list',
                    appcode: '701002RWGL'
                }
            )        
        break
        case 'openBill':
            const that = this 
            let listData = this.data.listRows[index].values
            requestApi.openBill({
                data: {
                    billtypeCode: listData.billtypecode.value, 
                    transtypeCode: listData.transtypecode.value,
                    billid: listData.busiid.value
                },
                success: (data) => {
                    that.props.specialOpenTo(
                        data.data.url,
                        {
                            ...data.data.data
                        }
                    )
                }
            })
        break
        default: 
        break
    }
}