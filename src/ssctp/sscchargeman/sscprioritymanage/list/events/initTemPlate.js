import requestApi from '../requestApi'
import getListButtonRender from './getListBtn'
import fixedTemplate from './fixedTemplate'
import {buttonClick} from './index'

export default function (props) {
    window.presetVar = {
        ...window.presetVar,
        list: 'tasklist'
    }

    const _this = this
    props.createUIDom(
        {
            pagecode: '700106RWGLYXJ_List',
            appcode: '701002RWGL'
        },
        function (data) {
            props.button.setButtons(data.button)
            data.template[window.presetVar.list].pagination = false
            data.template[window.presetVar.list].items.push(
                getListButtonRender.call(_this, props)
            )
            props.meta.setMeta(data.template)

            // 单据编码列自定义render
            props.table.setTableRender(window.presetVar.list, "billno", (text, record, index)=>{
                return(
                    <a
                        className="billnoa"
                        onClick={buttonClick.bind(_this, 'openBill', index)}
                    >{record.billno.value}</a>
                )
            })
            // 查询作业组
            requestApi.qryManagePriority({
                data: {},
                success: (data) => {
                    if(!data.success) return
                    const grouplist = data.data.grouplist.map(ele => (
                        {
                            attrcode: ele.value,
                            label: ele.display
                        }
                    ))
        
                    _this.queryKey.pk_sscgroup =   
                        _this.queryKey.pk_sscgroup 
                        ||
                        (
                            grouplist.length > 0 
                            ?
                            grouplist[0].attrcode
                            :
                            ''
                        )
        
                    _this.setState({
                        grouplist,
                        pk_sscgroup: _this.queryKey.pk_sscgroup
                    })

                    // 查询列表数据
                    _this.queryData()
                }
            })
        }
    )
}
