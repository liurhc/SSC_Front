import requestApi from '../requestApi'
import {initTemplate} from './index'
import {base} from 'nc-lightapp-front'
const {NCMessage} = base

export default function listButtonClick(props, btnKey, text, record, index, that, priorityValue) {
    let multiLang = props.MutiInit.getIntl(7010); 
    switch(btnKey){
        case 'forcedistribute': //强制分配
             let listData = record;
       
             that.setState({
               showModalDistribute: true, 
               operationData: listData,
               distributeType: "forcedistribute"
          })
        break

        case 'TopPlacement': // 本级置顶
            requestApi.rankingSetTop({
                data: {
                    pk_ssctask: record.pk_task.value
                },
                success: (data) => {
                    if (data.success && data.data) {
                        //置顶成功
                        NCMessage.create({content: multiLang && multiLang.get('701001RWCL-0064'), color: 'success', position: 'bottomRight'});
                    } else {
                        //置顶失败
                        NCMessage.create({content: multiLang && multiLang.get('701001RWCL-0065'), color: 'warning', position: 'bottomRight'});
                    }
                }
            }) 
        break
        case 'sort':
            requestApi.queryPriority({
                data: {
                    pk_workinggroup: record.pk_sscgroup.value
                },
                success: (data) => {
                    if (data.data && data.data.priority) {
                        const {
                            data: {
                                priority
                            }
                        } = data
                        that.setState({
                            showPriority: {
                                idx: index,
                                prioritys: priority
                            }
                        })
                        initTemplate.call(that, that.props)
                    } else {
                        that.setState({
                            showPriority: {
                                idx: index,
                                prioritys: []
                            }
                        })
                        initTemplate.call(that, that.props)
                    }
                }
            })
        break
        case 'RankingUpdatePriority':
            requestApi.rankingUpdatePriority({
                data: {
                    pk_ssctask: record.pk_task.value,
                    pk_priority: priorityValue
                },
                success: (success) => {
                    if (success) {
                        //修改成功
                        NCMessage.create({content: multiLang && multiLang.get('701001RWCL-0066'), color: 'success', position: 'bottomRight'});
                    } else {
                        //修改失败
                        NCMessage.create({content: multiLang && multiLang.get('701001RWCL-0067'), color: 'warning', position: 'bottomRight'});
                    }
                    that.setState({
                        showPriority: {
                            idx: -1,
                            prioritys: []
                        }
                    })
                    initTemplate.call(that, that.props)
                }
            })
        break
        default:
        break
    }
}


