const buttons = [
    {
        "id": "0001A41000000006J5B1",
        "type": "button_main",
        "key": "forcedistribute",
        "title": "强制分配",
        "area": "tasklist",
        "children": []
    },
    {
        "id": "0001A41000000006J5B1",
        "type": "button_main",
        "key": "TopPlacement",
        "title": "本级置顶",
        "area": "tasklist",
        "children": []
    },
    {
        "id": "0001A41000000006J5B1",
        "type": "button_main",
        "key": "sort",
        "title": "优先级排序",
        "area": "tasklist",
        "children": []
    }
]

export default {
    buttons   
}