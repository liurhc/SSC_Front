import React, {Component} from 'react';
import {connect} from 'react-redux';
import {base, ajax} from 'nc-lightapp-front';
const {NCCheckbox,NCModal,NCButton, NCTextArea,NCMessage} = base;
import {query} from '../events/index'

import SSCWorkGroupUserTreeGridRef from '../../../../refer/sscbd/SSCWorkGroupUserTreeGridRef'

class DistributeUserList extends Component {
    constructor(props) {
        super();
        this.selectedUser={};
        this.reason='';
        this.state = {refname: null}
    }

    save = () =>{
        let multiLang = this.props.MutiInit.getIntl(7010); 
        if(this.selectedUser.refpk == null){
            NCMessage.create({content: multiLang && multiLang.get('701001RWCL-0052'), color: 'warning', position: 'bottomRight'});
            return;
        }
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCTaskManageDistributeAction.do',
            data: {
                pks: this.props.data.pk_task.value, 
                pk_user: this.selectedUser.refpk, 
                ts: this.props.data.ts.value, 
                type: this.props.distributeType,
                reason: this.reason
            },
            success: ()=>{
                this.selectedUser={};
                this.reason='';
                this.props.that.setState({showModalDistribute: false});
                query.queryData.call(this.props.that);
            }
        });
    }

    cancel = () => {
        this.selectedUser={};
        this.reason='';
        this.props.that.setState({showModalDistribute: false});
    }

    selectUser = (e) => {
        this.setState({refname: e})
        this.selectedUser = e;
    }

    render() {
        let {data} = this.props;
        let multiLang = this.props.that.props.MutiInit.getIntl(7010); 
        return (
            <div>
                <NCModal show = {this.props.showModal}  id="distributeUserList">
                    <NCModal.Header>
                        <NCModal.Title>{multiLang && multiLang.get('701001RWCL-0054')}</NCModal.Title>
                    </NCModal.Header>
                    <NCModal.Body>
                        <div>
                            <div className="distributeuserlist-user">
                                {SSCWorkGroupUserTreeGridRef({
                                    onChange: this.selectUser,
                                    queryCondition: {
                                        group: (data.pk_sscgroup || {}).value
                                    },
                                    value: this.state.refname                 
                                })}
                            </div>
                            <div>
                                <NCTextArea
                                    placeholder={multiLang && multiLang.get('701001RWCL-0053')}
                                    className=""
                                    showMax={true}
                                    max={200}
                                    onChange={(val)=>{this.reason=val}}
                                />
                            </div>
                        </div>
                    </NCModal.Body>
                    <NCModal.Footer>
                        <NCButton color="primary" onClick={this.save.bind(this)} >{multiLang && multiLang.get('7010-0001')}</NCButton>
                        <NCButton onClick={this.cancel.bind(this)}>{multiLang && multiLang.get('7010-0004')}</NCButton>
                    </NCModal.Footer>
                </NCModal>
            </div>
        )
    }
}

export default DistributeUserList;