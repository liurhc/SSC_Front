import React, {Component} from 'react'
import {createPage, base} from 'nc-lightapp-front'
const {NCButton, NCAffix} = base
import {buttonClick, initTemplate} from './events'
import requestApi from './requestApi'
import './index.css'
import './index.less'
import WorkbenchTab from 'ssccommon/components/workbench-tab'
import DistributeUserList from './components/distributeUserList'
import WorkbenchSearcharea from 'ssccommon/components/workbench-searcharea'

class sscPriorityManage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            showNumbers:{},
            searcharea:{
                conditions:[]
            },
            pageControll: 'none',
            fuzzyquerychild: [],
            showPriority: {
                idx: -1,
                prioritys: []
            },
            showModalDistribute: false,
            distributeType: '',
            operationData: {},
            grouplist: [],
            pk_sscgroup: '',
            prioritysConfig: [],
            currentpriorityName: '',
            currentKey: ''
        }
        // 查询条件
        this.queryKey={
            pk_sscgroup: '', 
            pk_priority: '',
            fuzzyQueryKey: [],
            // 页信息
            pageinfo:{
                number: 1,
                size: 999999,
                totalElements: 0,
                totalPages: 0
            }
        }
        // 缓存查询结果数据
        this.data={
            listRows:[]
        }
        // 延时控制标记 // 翻页
        this.canChangPageFlag = true
        // 模糊查询
        this.fuzzyKey = ''
        // 页面初始化
        initTemplate.call(this, props)
    }
    // 查询数据（参数：是否是翻页动作）
    queryData(isAddPage = false){
        if(isAddPage){
            this.queryKey.pageinfo.number++
        }else{
            this.queryKey.pageinfo.number = 1
        }
        requestApi.query({
            data: this.queryKey,
            success: (data) => {
                const {data: {tasklist}} = data
                let listRows = []
                if(isAddPage){
                    this.data.listRows.map((one)=>{
                        listRows.push(this.copyData(one))
                    })
                }
                tasklist.rows.map((one)=>{
                    listRows.push(this.copyData(one))
                })
                // listRows.sort((x, y) => x.values.ordernum.display - y.values.ordernum.display)
                this.data.listRows = listRows
                // 设置列表数据
                this.props.table.setAllTableData(window.presetVar.list, {
                    areacode: window.presetVar.list,
                    rows: listRows
                })
                let newState = {}
                newState = this.state
                for (let attr in data.data) {
                    if (typeof data.data[attr] === 'number') {
                        newState.showNumbers[attr] = data.data[attr]
                    }
                }
                // 更新页信息
                this.queryKey.pageinfo = tasklist.pageinfo
                if(this.queryKey.pageinfo.totalPages > this.queryKey.pageinfo.number){
                    newState.pageControll = 'notend'
                }else if(this.queryKey.pageinfo.totalPages == this.queryKey.pageinfo.number){
                    newState.pageControll = 'end'
                }else{
                    newState.pageControll = 'none'
                }
                newState.searcharea = {
                    conditions: JSON.parse(data.data.searchArea).items
                }
                this.setState(newState)
                setTimeout(()=>{this.canChangPageFlag=true;},100)
            }
        })
    }
    shouldComponentUpdate(nextProps, nextState){
        if (nextState.pk_sscgroup != this.state.pk_sscgroup) {
            requestApi.queryPriority({
                data: {pk_workinggroup: nextState.pk_sscgroup},
                success: (data) => {
                    if (data.data && data.data.priority) {
                        const {
                            data: {
                                priority
                            }
                        } = data
                        const prioritysConfig = priority.map(each => each)
                        this.setState({
                            prioritysConfig,
                            currentpriorityName: prioritysConfig[0].display
                        })
                    } else {
                        this.setState({
                            prioritysConfig: [],
                            currentpriorityName: ''
                        })
                    }
                }
            })
        }
        return true
    }
    // 复制列表数据
    copyData(data){
        let newData = {}
        newData.values = {}
        for(let child in data.values){
            newData.values[child] = {
                display: data.values[child].display,
                value: data.values[child].value,
                scale: data.values[child].scale 
            }
        }
        return newData
    }
    // 已完成/未完成 页签切换
    selectTabChange(selectKey){
        this.queryKey.pk_sscgroup = selectKey
        this.setState({currentKey: selectKey})
        initTemplate.call(this, this.props)
    }
    // 查询条件选择变更
    handleConditionChange(attrcode, value){
        // 更新查询条件
        this.queryKey[attrcode]=value
        // 查询数据
        this.queryData()
    }
    // 模糊查询输入内容变化
    onFuzzyChange(data){
        this.fuzzyKey = data
        let newQueryKey = {}
        let multiLang = this.props.MutiInit.getIntl(7010)
        for(let child in this.queryKey){
            newQueryKey[child] = this.queryKey[child]
        }
        newQueryKey.fuzzyQueryKey = data
        const that = this
        setTimeout((that, data)=>{
            if(data == that.fuzzyKey){
                requestApi.queryFuzzyKey({
                    data: newQueryKey,
                    success: (data) => {
                        for(let childName in data.data){
                            data.data[childName].value = data.data[childName].value+"="+that.fuzzyKey;
                            let aname = multiLang && multiLang.get('701001RWCL-0061')+that.fuzzyKey;//包含
                            data.data[childName].key = data.data[childName].key+aname;  //包含
                        }
                        that.setState({fuzzyquerychild:data.data})
                    }
                })
            }
        },500, this, data)
    }
    // 模糊查询选择
    onFuzzySelected(data){
        this.setState({fuzzyquerychild:[]})
        // 更新查询条件
        this.queryKey.fuzzyQueryKey = data
        // 查询数据
        this.queryData()
    }
    hidePriority = (e) => {
        e = e || window.event
        e.stopPropagation()
        e.cancelBubble = true
        this.setState({
            showPriority: {
                idx: -1,
                prioritys: []
            }
        })
        initTemplate.call(this, this.props)
    }
    setCurrentActivePrioritys = currentpriorityName => {
        this.setState({currentpriorityName})
    }

    render() {
        const {button, table} = this.props
        const {createSimpleTable} = table

        const { showPriority: {idx}, prioritysConfig, currentpriorityName} = this.state

        const targetBlankStyle = {
            height: idx == -1
                ?
                '0'
                :
                Math.max(document.body.scrollHeight, document.documentElement.scrollHeight) + 'px',
            width: Math.max(document.body.scrollWidth, document.documentElement.scrollWidth) + 'px'
        }
        let multiLang = this.props.MutiInit.getIntl(7010); //this.moduleId
        return (
            <div id="sscPriorityManage">
                <div className="container">
                    <NCAffix offsetTop={0}>
                        <div className="workbench-tab">
                            {
                                this.state.grouplist.length > 0
                                ?
                                (
                                    <WorkbenchTab
                                        tabs={
                                            this.state.grouplist
                                        }
                                        showNumbers={this.state.showNumbers}
                                        selectTabChange={this.selectTabChange.bind(this)}
                                        showBtnsRight={true}
                                        btnsRight={
                                            [
                                                {
                                                    // "701001RWCL-0034": "返回任务列表"
                                                    name: multiLang && multiLang.get('701001RWCL-0034'),
                                                    color: 'default',
                                                    key: 'GoBack'
                                                }
                                            ]
                                        }
                                        btnsClick={buttonClick.bind(this)}
                                        currentKey={this.state.currentKey}
                                    />
                                )
                                :
                                (
                                    <div className='replace-work-bench-tab'>
                                        <NCButton
                                            onClick={()=>{
                                                buttonClick.call(this, 'GoBack', -1)
                                            }}
                                            // "701001RWCL-0034": "返回任务列表"
                                        >{multiLang && multiLang.get('701001RWCL-0034')}</NCButton>
                                    </div>
                                )
                            }   
                        </div>
                    </NCAffix>
                    <div className='workbench-searcharea'>
                        <WorkbenchSearcharea
                            hideBarCode={true}
                            twoColNums="4"
                            cusType="tp"
                            conditions={this.state.searcharea.conditions}
                            handleConditionChange={this.handleConditionChange.bind(this)}
                            fuzzyquerychild = {this.state.fuzzyquerychild}
                            onFuzzyChange={(data) => {
                                this.onFuzzyChange.call(this, data)
                            }}
                            onFuzzySelected={this.onFuzzySelected.bind(this)}
                            disabledTypeChange={true}
                        />
                    </div>
                    <div className='main-table'>
                        {createSimpleTable(window.presetVar.list, {
                            adaptionHeight:true,
                            showIndex:true
                        })}
                    </div>
                    
                    {/* 点击关闭修改优先级下拉框 */}
                    <div
                        id='target-blank'
                        style={targetBlankStyle}
                        onClick={this.hidePriority}
                    ></div>

                     {/* 分配人员弹出框 */}
                    <DistributeUserList 
                        distributeType={this.state.distributeType}
                        showModal={this.state.showModalDistribute}
                        data={this.state.operationData}
                        that={this}
                    />
                    {/* 类型定位标 */}
                    {
                        prioritysConfig.length > 0
                        ?
                        <div className='fixed-priority'>
                            <span className='toggle-handle'>
                                <i className='iconfont icon-loucengtubiao'></i>
                            </span>
                            {
                                prioritysConfig.map((ele, idx) => (
                                    <a
                                        key={`${idx}_${ele.display}`}
                                        className={currentpriorityName === ele.display ? 'active' : ''}
                                        onClick={() => { this.setCurrentActivePrioritys(ele.display) }}
                                    >{ele.display}</a>
                                ))
                            }
                        </div>
                        :
                        null
                    }
                </div>
            </div>
        )
    }
}
sscPriorityManage = createPage({
    mutiLangCode: '7010'
})(sscPriorityManage)
export default sscPriorityManage