import {ajax } from 'nc-lightapp-front'

let requestApiOverwrite = {
    // 模板查询
    tpl: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCTaskManagePriorityTemAction.do',
            data: opt.data,
            success: opt.success
        })
    },
    // 列表查询
    query: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCTaskManagePriorityQueryAction.do',
            data: opt.data,
            success: opt.success
        })
    },
    // 模糊查询
    queryFuzzyKey: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCTaskBlurQueryAction.do',
            data: opt.data,
            success: data => {
                opt.success(data)
            }
        })
    },
    // 优先级查询
    queryPriority: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCPriorityQryForTaskManageAction.do',
            data: opt.data,
            success: data => {
                opt.success(data)
            }
        })
    },
    // 任务列表优先级修改
    rankingUpdatePriority: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/RankingUpdatePriorityAction.do',
            data: opt.data,
            success: data => {
                if (data.success) opt.success(data.data)
            }
        })
    },
    // 优先级置顶
    rankingSetTop: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/RankingSetTopAction.do',
            data: opt.data,
            success: data => {
                opt.success(data)
            }
        })
    },
    // 查看单据
    openBill: (opt) => {
        ajax({
            url: '/nccloud/sscrp/rpbill/BrowseBillAction.do',
            data: opt.data,
            success: (data) => {
                opt.success(data)
            }
        })
    },
    // 作业组查询
    qryManagePriority: (opt) => {
        ajax({
            url: '/nccloud/ssctp/sscbd/SSCTaskManagePriorityTemAction.do',
            data: opt.data,
            success: (data) => {
                opt.success(data)
            }
        })
    }
}

export default requestApiOverwrite