import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, base, toast, print, high, promptBox, createPageIcon } from 'nc-lightapp-front';
const { PrintOutput } = high;
import { ajax } from 'nc-lightapp-front';
//import Org from '../../../refer/org/AccountBookTreeRef/index';
import Org from '../../../refer/org/AdminOrgDefaultTreeRef';
import userGroup from '../../../../uap/refer/riart/userGroupTreeRef/index';
import createUIDom from "../../../public/utils/BDCreateUIDom";
import Utils from '../../../public/utils/index.js';
//import Org from '../../../../uapbd/refer/fiacc/AccountDefaultReferModelRef/index.js';
/**
 * 引入公共 js 工具，使用了 Array.prototype.arrRemoveAppoint 方法
 */
import '../../../public/uapbdutil/uapbd_js_common.js'

let { NCTable, NCSelect, NCTabs, NCButton, NCCheckbox, NCModal, NCBackBtn, NCAffix } = base;

import '../../../public/uapbdstyle/uapbd_style_common.less'
import './index.less';

//1,按要求去掉分页
//2,按要求编辑表单改成侧拉窗口, 需要修改simpleTable为editTable
//3,按要求数据行树小于某值查询按过滤处理，大于某值按查询处理.

var createUIDomParam = function (pagecode) {
    return { pagecode: pagecode };
};


class Psndoc extends Component {

    componentDidUpdate () {
        if (this.state.card.editMode == 'edit') {
            window.onbeforeunload = () => {//编辑态关闭页签或浏览器的提示
                return '';
            };
        } else {
            window.onbeforeunload = null;
        }
    }

    componentDidMount () {

    }

    constructor(props) {
        super(props);
        this.config = createUIDomParam('10140PSNMY_main');
        createUIDom(props)({ pagecode: '10140PSNMY_main' }, { moduleId: "10140PSN", domainName: 'uapbd' }, (data, lang) => {
            this.lang = lang;
            this.state = this.createState(data);
            this.state.buttons = data.button || [];
            this.setState(this.state, () => {
                this.initMeta(data);
                //添加分页
                data.template['psndoc_list'].pagination = false;
                props.meta.setMeta(data && data.template ? data.template : {});
                props.button.setButtons(data && data.button ? data.button : {}, () => {
                    // this.props.button.setPopContent('list_rowdelete', this.lang['10140PSN-000036']);/* 国际化处理： 确认要删除该信息吗？,确认要删除该信息吗*/
                    var context = data.context || {};
                    //处理超链接
                    let id = this.props.getUrlParam("id");
                    setTimeout(() => {
                        // if (context.pk_org) {
                        //     this.state.org.value = {
                        //         refpk: context.pk_org,
                        //         refname: context.org_Name
                        //     };
                        //     this.setState(this.state, () => {
                        //         if (id) this.browseFormPsndoc(id);
                        //     });
                        //     return;
                        // }
                        // if (id) this.browseFormPsndoc(id);
                        this.browseListPsndoc();
                    }, 0);
                });

            });
        });

        window.onbeforeunload = () => {
            if (this.state.editMode == 'edit') return;
            else return;
        };
    }

    createState (template) {
        var state = {  //添加当前选中组织到State
            showMode: 'list',
            buttons: [],
            pks: [],
            org: {
                value: undefined,
                showGroup: false,
                onChange: (val) => {
                    this.state.org.value = val;
                    this.setState(this.state, () => { });
                },
                queryCondition: () => {//组织权限
                    return {
                        AppCode: this.config.appcode || '',
                        TreeRefActionExt: 'nccloud.web.refer.sqlbuilder.PrimaryOrgSQLBuilder'
                    };
                }
            },
            search: {
                id: 'psndoc_search',
                defaultConditionsNum: 2,
                //oid:'1009Z0100000000021AZ',
                oid: template.template.psndoc_search.oid,
                // oid:this.props.search.getQueryInfo('psndoc_search').oid,
                clickSearchBtn: () => {
                    this.browseListPsndoc((data) => {
                        data && (data.psndoc_list.pageInfo.total > 0 || data.psndoc_list.pageInfo.total == '0') ? toast({ content: this.lang['10140PSN-000088'] + (data.psndoc_list.pageInfo.total) + this.lang['10140PSN-000089'], color: 'success' }) : toast({ title: this.lang['10140PSN-001091'], content: this.lang['10140PSN-001092'], color: 'warning' }); //提示 warning  ///* 国际化处理： 查询到,条数据*/
                    }, true);
                }
            },
            card: {
                psndoc: undefined,
                editMode: 'browse'
            }
        };
        return state;
    }

    initMeta (data) {
        var { template } = data,
            buttons = this.state.buttons;

        var onLinkClick = (record, index) => {
            return () => {
                this.onPsndocRowClick(record, index);
            }
        };

        template['psndoc_search'].items.forEach((item, key) => {  //添加超链接
            if (item.attrcode === 'psnjobs.pk_dept') {
                item.isShowDisabledData = true;
            }
            if (item.attrcode === 'psnjobs.pk_org') {
                item.isShowDisabledData = true;
            }
        });

        template['psndoc_list'].items.forEach((item, key) => {  //添加超链接
            item.width = '100px';
            if (item.attrcode === 'code') {
                item.render = (text, record, index) => {
                    return (
                        <span style={{ color: '#007ace', cursor: 'pointer' }} onClick={onLinkClick(record, index)}>
                            {record && record['code'] && record['code'].value}
                        </span>
                    );
                };
            }
        });
        template['psnjobs'].items.forEach((item, key) => {  //添加超链接
            if (item.attrcode == 'pk_group') { //任职集团
                // item.refcode = 'uapbd/refer/org/GroupDefaultTreeRef/index.js';
            }
            if (item.attrcode == 'pk_org') { //任职行政组织
                item.refcode = 'uapbd/refer/org/AdminOrgDefaultTreeRef/index.js';
                item.refName = '行政组织';
                item.isShowUnit = true;
                item.isShowDisabledData = false;
            }
            if (item.attrcode == 'pk_psncl') { //人员类别
                //  item.refcode = 'uapbd/refer/psninfo/PsnclTreeRef/index.js';
            }
            if (item.attrcode == 'pk_dept') { //任职部门
                //   item.refcode = 'uapbd/refer/org/DeptNCTreeRef/index.js';
                item.isShowUnit = false;
                item.isShowDisabledData = true,
                    item.queryCondition = () => {
                        return {
                            "pk_defdoclist": "1009ZZ100000000030SP"
                        }
                    }
            }

            if (item.attrcode == 'worktype') { //工种
                // item.refcode = 'uapbd/refer/userdef/DefdocGridRef/index.js';
                //由于平台不支持子表内自动适配参照自定义档案，此处手动传入
                item.queryCondition = () => {
                    return {
                        "pk_defdoclist": "1009ZZ100000000030SP"
                    }
                }
            }
            if (item.attrcode == 'pk_job') { //职务
                item.refcode = 'uapbd/psninfo/psndoc/RefPsndocJobGrid/index.js';
            }
            if (item.attrcode == 'pk_post') {//岗位
                item.refcode = 'uapbd/psninfo/psndoc/RefPsndocPostGrid/index.js';
            }
        });

        template['psndoc_card_head'].items.forEach((item, key) => {  //添加超链接
            if (item.attrcode == 'idtype') {
                item.refcode = 'uapbd/refer/pub/PsnidDefaultGridRef/index.js';
            }
        });

        template['psndoc_search'].items.forEach((item, key) => {  //添加超链接
            if (item.attrcode == 'psnjobs.pk_dept') {
                item.refcode = 'uapbd/refer/org/DeptTreeRef/index.js';
                item.isShowUnit = true;
            }
        });

        var listRowBtns = buttons.filter(btn => btn.area == 'list_row').map(b => b.key);
        template['psndoc_list'].items.push({
            attrcode: 'opr',
            label: this.lang['10140PSN-000048'],/* 国际化处理： 操作*/
            itemtype: 'customer',
            width: 200,
            fixed: 'right',
            className: 'table-opr',
            visible: true,
            render: (text, record, index) => {
                return this.props.button.createOprationButton(listRowBtns, {
                    area: 'list_row',
                    onButtonClick: (props, btncode) => {
                        this.onBtnOperation(props, btncode, 'list_row', { record, index });
                    }
                });
            }
        });

        var cardRowBtns = buttons.filter(btn => btn.area == 'card_list_row').map(b => b.key);
        template['psnjobs'].items.push({
            attrcode: 'opr',
            label: this.lang['10140PSN-000048'],/* 国际化处理： 操作*/
            itemtype: 'customer',
            width: 200,
            fixed: 'right',
            className: 'table-opr',
            visible: true,
            render: (text, record, index) => {
                var buttonAry = this.props.cardTable.getStatus('psnjobs') === 'browse' ? [''] : cardRowBtns;
                return this.props.button.createOprationButton(buttonAry, {
                    area: 'card_list_row',
                    onButtonClick: (props, btncode) => {
                        this.onBtnOperation(props, btncode, 'card_list_row', { record, index });
                    }
                });
            }
        });
    }

    onBtnOperation (prop, btncode, areacode, opt) {
        //debugger;
        if (areacode == 'list_row') {
            if (btncode == 'list_rowedit') {
                let that = this;
                //判断当前数据是否在当前组织内
                // if (opt.record.pk_org.value != this.state.org.value.refpk) {
                //     toast({ title: '人员不在当前组织内', color: 'success' });/* 国际化处理： 启用成功！*/
                //     return;
                // }
                //添加数据权限校验
                ajax({
                    data: { pk_psndoc: opt.record.pk_psndoc.value },
                    url: '/nccloud/uapbd/psndoc/CheckPsnEditPermAction.do',
                    success: function (res) {
                        that.editPsndoc(opt.record.pk_psndoc.value);
                    }
                });
            }
        }
        if (areacode == 'card') {
            let data = this.props.createMasterChildData(this.config.pagecode, 'psndoc_card_head', 'psnjobs'),
                psndoc = data.head.psndoc_card_head.rows[0],
                pk_psndoc = psndoc.values.pk_psndoc.value;
            if (btncode == 'card_edit') {
                let that = this;
                //添加数据权限校验
                ajax({
                    data: { pk_psndoc: pk_psndoc },
                    url: '/nccloud/uapbd/psndoc/CheckPsnEditPermAction.do',
                    success: function (res) {
                        that.editPsndoc(pk_psndoc);
                    }
                });
            }
            if (btncode == 'card_save') {
                this.savePsndoc(() => { this.updateButtonStatus() })
            }
            if (btncode == 'card_back') {
                this.browseListPsndoc();
            }
            if (btncode == 'card_cancel') { //编辑回到card 新增回到
                promptBox({
                    title: this.lang['10140PSN-000012'],/* 国际化处理： 确认取消*/
                    content: this.lang['10140PSN-000013'],/* 国际化处理：  确认是否取消?*/
                    color: 'warning',
                    rightBtnName: this.lang['10140PSN-000028'], //左侧按钮名称,默认关闭/* 国际化处理： 否*/
                    leftBtnName: this.lang['10140PSN-000027'], //右侧按钮名称， 默认确认/* 国际化处理： 是*/
                    beSureBtnClick: () => {
                        this.props.form.setFormStatus('psndoc_card_head', 'browse');
                        this.props.cardTable.setStatus('psnjobs', 'browse');
                        this.props.cardTable.resetTableData('psnjobs');
                        setTimeout(() => {
                            this.state.showMode = pk_psndoc ? 'card' : 'list';
                            this.setState(this.state, () => {
                                if (this.state.showMode == 'card') {
                                    this.browseFormPsndoc(pk_psndoc);
                                } else {
                                    this.browseListPsndoc();
                                }
                            });
                        }, 0);

                    }
                });
            }
        }

        if (areacode == 'list') {
            var psndocs = this.props.table.getCheckedRows('psndoc_list') || [];
            var pk_psndocs = psndocs.map(psndoc => {
                return psndoc.data.values.pk_psndoc.value;
            });
        }
    }

    /**
     *
     * @param callback
     * @param isClickSearch
     * @param isSelectedNodeChange  是否选择树节点加载数据，是的话需要清空pageInfo信息
     */
    loadListPsndoc (callback, isClickSearch, isSelectedNodeChange) { //加载人员数据
        let userId = JSON.parse(localStorage.getItem("STOREFORINFODATA")).userId;
        ajax({
            data: { cuserid: userId },
            url: '/nccloud/uapbd/psn/WebPsnServiceAction.do',
            success: (res) => {
                let pk_psndoc = res.data.pk_psndoc;
                ajax({
                    data: { pk_psndoc: pk_psndoc, ...this.config },
                    url: '/nccloud/uapbd/psndoc/LoadPsndocAction.do',
                    success: (res) => {
                        let rows = res.data.billData.head.psndoc_card_head.rows;
                        let pk_org = rows[0].values.pk_org.value;
                        this.state.org.value = { refpk: pk_org };
                        let data2 = {
                            querycondition: {
                                logic: "and",
                                conditions: [{ field: "code", oprtype: "=", datatype: "1", value: { firstvalue: rows[0].values.code.value } }]
                            },
                            queryAreaCode: this.state.search.id,
                            oid: this.state.search.oid,
                            pagecode: this.config.pagecode,
                            pageInfo: { pageIndex: 0, pageSize: "10" },
                            querytype: "tree",
                            pk_org: pk_org,
                            ...this.config
                        }
                        ajax({
                            data: data2,
                            url: '/nccloud/uapbd/psndoc/ListPsndocAction.do',
                            success: (res) => {
                                callback && callback(res.data);
                            }
                        })
                    }
                });

            }
        });

        // let searchVal = this.props.search.getAllSearchData('psndoc_search') || {};
        // let pageInfo = this.props.table.getTablePageInfo('psndoc_list');
        // if (isSelectedNodeChange) pageInfo = { pageIndex: 0, pageSize: 10 }
        // debugger;
        // var param = {
        //     querycondition: searchVal,
        //     pagecode: this.config.pagecode,
        //     queryAreaCode: this.state.search.id,
        //     oid: this.state.search.oid,
        //     pk_org: this.state.org.value ? this.state.org.value.refpk : null,
        //     pk_dept: 'root',
        //     querytype: 'tree',
        //     pageInfo: pageInfo,
        //     appCode: this.config.appcode,
        //     isClickSearch: isClickSearch,
        //     ...this.config
        // };
        // ajax({
        //     loading: true,
        //     data: param,
        //     url: '/nccloud/uapbd/psndoc/ListPsndocAction.do',
        //     success: function (res) {
        //         debugger;
        //         callback && callback(res.data);
        //     }
        // });
    }

    loadFormPsndoc (pkpsndoc, callback) {
        ajax({
            loading: true,
            data: {
                pk_psndoc: pkpsndoc || '',
                pk_org: this.state.org.value ? this.state.org.value.refpk : '',
                ...this.config
            },
            url: '/nccloud/uapbd/psndoc/LoadPsndocAction.do',
            success: (res) => {
                this.state.card.psndoc = res;
                this.setState(this.state, () => {
                    callback && callback(res.data.billData, {
                        isCodeEdit: res.data.isCodeEdit,
                        isCodeMust: res.data.isCodeMust,
                        isCodeEdit2: res.data.isCodeEdit2,
                        isCodeMust2: res.data.isCodeMust2,
                        ishr: res.data.ishr,
                        isshoporg: res.data.isshoporg
                    });
                });
            }
        });
    }

    fillListPsndoc (data) {
        // data.rows.map( (e) =>{ //设置启用禁用枚举值为 true or false
        //     e.values.enablestate.value =  e.values.enablestate.value == '2' ? true : false;
        // });
        // me.props.editTable.setTableData('psndoc', data);
        if (data)
            this.props.table.setAllTableData('psndoc_list', data['psndoc_list']);
        else
            this.props.table.setAllTableData('psndoc_list', {
                areacode: 'psndoc_list',
                pageInfo: {
                    pageIndex: 0,
                    pageSize: 10,
                    total: 0,
                    totalPage: 1
                },
                rows: []
            });
    }
    fillFormPsndoc (data, callback) {
        Utils.convertGridEnablestate(data.head['psndoc_card_head'].rows);
        this.props.form.setAllFormValue({ 'psndoc_card_head': data.head['psndoc_card_head'] });
        this.props.cardTable.setTableData('psnjobs', data.body['psnjobs'], false);
        this.props.form.resetItemWidth('psndoc');
        setTimeout(() => {
            callback && callback();
        }, 0);
    }

    onPsndocRowClick (record, index) {
        this.browseFormPsndoc(record.pk_psndoc.value);
    }

    editPsndoc (pkpsndoc) {
        //调整原有先setState后加载数据顺序，先加载数据 校验是否已启用HR
        this.loadFormPsndoc(pkpsndoc, (data, codeCfg) => {
            if (codeCfg.ishr) {
                toast({ title: this.lang['10140PSN-001093'], color: 'success' });/* 国际化处理： 启用HR了！*/
                return;
            }
            if (codeCfg.moreJobsInOtherOrg) {
                toast({ title: '人员的兼职信息不能在兼职的组织中维护', color: 'success' });/* 国际化处理： 启用HR了！*/
                return;
            }
            this.state.showMode = 'card';
            this.state.card.editMode = 'edit';
            //设置启用状态为123;//
            this.setState(this.state, () => {
                this.props.form.setFormStatus('psndoc_card_head', 'edit');
                //判定是否兼职编辑模式,判断当前人员组织与所在组织是否一致
                this.editHandlerMode = data.head.psndoc_card_head.rows[0].values.pk_org.value == this.state.org.value.refpk ? 'edit' : 'simpleedit';
                this.initEditMode(this.editHandlerMode, () => {
                    setTimeout(() => {
                        this.editHandlerMode != 'simpleedit' && this.props.form.setFormItemsDisabled('psndoc_card_head', { code: !codeCfg.isCodeEdit, isshopassist: !codeCfg.isshoporg });//编码规则控制是否可编辑
                        setTimeout(() => {
                            var meta = this.props.meta.getMeta();
                            meta['psndoc_card_head'].items.map((obj) => {
                                if (obj.attrcode === 'code') {
                                    obj.required = codeCfg.isCodeMust;
                                }
                            });
                            meta['psnjobs'].items.map((obj) => {
                                if (obj.attrcode === 'psncode') {
                                    obj.required = codeCfg.isCodeMust2;
                                }
                            });


                            this.props.meta.setMeta(meta, () => {
                                //this.props.cardTable.setStatus('psnjobs', 'edit');
                                Utils.convertGridEnablestate(data.head.psndoc_card_head.rows);
                                this.fillFormPsndoc(data);
                                this.updateButtonStatus()
                            });
                        }, 0);

                    }, 0);
                });


            });
        });
    }

    initEditMode (mode = 'simpleedit', callback) {
        var meta = this.props.meta.getMeta();
        var disableCfg = {};
        meta['psndoc_card_head'].items.map((obj) => {
            disableCfg[obj.attrcode] = obj.disabled ? obj.disabled : (mode == 'simpleedit' ? true : false);
            if ('enablestate' == obj.attrcode) {
                disableCfg[obj.attrcode] = true;
            }
        });
        this.props.form.setFormItemsDisabled('psndoc_card_head', disableCfg);
        setTimeout(() => {
            callback && callback();
        }, 0);
    }

    browseFormPsndoc (pkpsndoc, callback) {
        this.state.showMode = 'card';
        this.state.card.editMode = 'browse';
        this.setState(this.state, () => {
            this.loadFormPsndoc(pkpsndoc, (data) => {
                this.fillFormPsndoc(data, () => {
                    this.updateButtonStatus()
                    this.setState(this.state, () => {
                        callback && callback();
                    });
                });
            });
        });
    }
    browseListPsndoc (callback, isClickSearch) {
        this.state.showMode = 'list';
        this.state.card.editMode = 'browse';
        this.setState(this.state, () => {
            this.loadListPsndoc(data => {
                this.fillListPsndoc(data);
                setTimeout(() => {
                    callback && callback(data);
                }, 0);
            }, isClickSearch);
        });
    }

    pageInfoClick (props, config, pks) {
        setTimeout(() => {
            this.browseListPsndoc();
        }, 0);
    }

    savePsndoc (callback, checked = true) {
        var me = this;
        this.props.cardTable.filterEmptyRows('psnjobs');
        let psndoc = this.props.createMasterChildData(this.config.pagecode, 'psndoc_card_head', 'psnjobs');

        // Utils.convertGridEnablestate(psndoc.head.psndoc_card_head.rows);
        if (!this.props.form.isCheckNow('psndoc_card_head')) return;
        if (!this.props.cardTable.checkTableRequired('psnjobs')) return;

        var atferHandler = (pkpsndoc) => {
            toast({ title: this.lang['10140PSN-000030'], color: 'success' });/* 国际化处理： 保存成功！*/
            this.props.form.setFormStatus('psndoc_card_head', 'browse');
            this.props.cardTable.setStatus('psnjobs', 'browse');
            this.browseFormPsndoc(pkpsndoc, () => {
                callback && callback();
            });
        };

        var hander = () => {
            this.props.validateToSave(psndoc, () => {
                ajax({
                    url: '/nccloud/uapbd/psndoc/SavePsndocAction.do',
                    data: {
                        ...psndoc,
                        checked: checked
                    },
                    success: (res) => {
                        if (res.data.success) {
                            atferHandler(res.data.psndata.pk_psndoc);
                        } else {
                            //提示+重新提交
                            promptBox({
                                color: 'warning', // 提示类别默认"success"， "success"/"info"/"warning"/"danger",非必输
                                title: this.lang['10140PSN-100063'] || '提示',/* 国际化处理： 删除提醒*/
                                // content:'确定要删除数据吗？',
                                content: this.lang['10140PSN-100064'] + (res.data.errorinfo || ''),/* 国际化处理： 删除基础数据要做业务引用检查，非常耗时，建议使用封存功能，是否继续？,删除基础数据要做业务引用检查,非常耗时,建议使用封存功能,是否继续*/
                                beSureBtnClick: () => {
                                    this.savePsndoc(callback, false)
                                }
                            });
                        }

                    }
                });
            }, {
                psndoc_card_head: 'form',
                ['psnjobs']: 'cardTable'
            }, 'card');
        };


        //check cardno
        var idtype = psndoc.head.psndoc_card_head.rows[0].values.idtype;
        var cardno = psndoc.head.psndoc_card_head.rows[0].values.id;

        if (idtype && idtype.value && idtype.value == '1001Z01000000000AI36') {
            var checkCardLength = (callback) => {
                if (cardno && cardno.value && cardno.value.length != 15 && cardno.value.length != 18) {
                    promptBox({
                        title: this.lang['10140PSN-000002'],/* 国际化处理： 确认信息*/
                        content: this.lang['10140PSN-000031'],/* 国际化处理：  输入的身份证号不是15位或18位,是否保存?*/
                        rightBtnName: this.lang['10140PSN-000028'], //左侧按钮名称,默认关闭/* 国际化处理： 否*/
                        leftBtnName: this.lang['10140PSN-000027'], //右侧按钮名称， 默认确认/* 国际化处理： 是*/
                        beSureBtnClick: () => {
                            callback && callback();
                        }
                    });
                    return;
                } else {
                    callback && callback();
                }
            };

            var checkCardBrithdate = (callback) => {
                if (cardno && cardno.value) {
                    var brithdate = psndoc.head.psndoc_card_head.rows[0].values.birthdate.value || '';
                    var carddate = cardno.value.substring(6, 14);
                    carddate = carddate.substring(0, 4) + '-' + carddate.substring(4, 6) + '-' + carddate.substring(6, 8);
                    if (brithdate !== carddate) {
                        promptBox({
                            title: this.lang['10140PSN-000002'],/* 国际化处理： 确认信息*/
                            content: this.lang['10140PSN-000032'],/* 国际化处理：  输入的身份证号的出生日期和填写的出生日期不一致,是否保存?*/
                            rightBtnName: this.lang['10140PSN-000028'], //左侧按钮名称,默认关闭/* 国际化处理： 否*/
                            leftBtnName: this.lang['10140PSN-000027'], //右侧按钮名称， 默认确认/* 国际化处理： 是*/
                            beSureBtnClick: () => {
                                callback && callback();
                            }
                        });
                        return;
                    }
                    callback && callback();
                }
                callback && callback();
            }
            checkCardLength(() => {
                checkCardBrithdate(() => {
                    hander();
                });
            });
            return;
        }
        if (idtype && idtype.value && idtype.value == '1001Z01000000000CHUK') {
            if (cardno && cardno.value && cardno.value.length != 10 && cardno.value.length != 11) {
                // me.props.modal.show('modal',{
                promptBox({
                    title: this.lang['10140PSN-000002'],/* 国际化处理： 确认信息*/
                    content: this.lang['10140PSN-000033'],/* 国际化处理：  输入的香港居民身份证号不是10位或11位,是否保存?*/
                    rightBtnName: this.lang['10140PSN-000028'], //左侧按钮名称,默认关闭/* 国际化处理： 否*/
                    leftBtnName: this.lang['10140PSN-000027'], //右侧按钮名称， 默认确认/* 国际化处理： 是*/
                    beSureBtnClick: hander
                });
                return;
            }
        }
        if (idtype && idtype.value && idtype.value == '1001Z01000000000CHUM') {
            if (cardno && cardno.value && cardno.value.length != 10) {
                // me.props.modal.show('modal',{
                promptBox({
                    title: this.lang['10140PSN-000002'],/* 国际化处理： 确认信息*/
                    content: this.lang['10140PSN-000034'],/* 国际化处理：  输入的台湾居民身份证号不是10位,是否保存?*/
                    rightBtnName: this.lang['10140PSN-000028'], //左侧按钮名称,默认关闭/* 国际化处理： 否*/
                    leftBtnName: this.lang['10140PSN-000027'], //右侧按钮名称， 默认确认/* 国际化处理： 是*/
                    beSureBtnClick: hander
                });
                return;
            }
        }

        hander();
    }

    saveAddPsndoc () {
        this.savePsndoc(() => {
            this.editPsndoc();
        });
    }

    //更新按钮状态
    updateButtonStatus () {
        var showMode = this.state.showMode,
            editMode = this.state.card.editMode,
            isorg = this.state.org.value && this.state.org.value.refpk;
        this.props.button.setDisabled({
            card_edit: !(showMode == 'card' && isorg && editMode == 'browse'),
            card_back: !(showMode == 'card' && isorg && editMode == 'browse'),
            card_save: !(showMode == 'card' && isorg && editMode == 'edit'),
            card_cancel: !(showMode == 'card' && isorg && editMode == 'edit')
        });
        this.props.button.setButtonsVisible({
            card_edit: showMode == 'card' && editMode == 'browse',
            card_back: showMode == 'card' && editMode == 'browse',
            card_save: showMode == 'card' && editMode == 'edit',
            card_cancel: showMode == 'card' && editMode == 'edit'
        });
    }

    render () {
        if (!this.lang) return '';
        let { button, search, DragWidthCom, modal, table, form, cardTable } = this.props;
        let { createButtonApp } = button;
        let { NCCreateSearch } = search;
        let { createSimpleTable } = table;
        let { createModal } = modal;
        let { createForm } = form;
        let { createCardTable } = cardTable;

        //获取列表肩部信息
        var initTableButton = () => {
            return (
                <div className="shoulder-definition-area">
                    <div className="definition-icons">
                        {createButtonApp({
                            area: 'card_list_bear',
                            buttonLimit: 3,
                            onButtonClick: (props, btncode) => {
                                this.onBtnOperation(props, btncode, 'card_list_bear');
                            },
                            popContainer: document.querySelector('.card-childer-button')
                        })}
                        {this.props.cardTable.createBrowseIcons('psnjobs', {
                            iconArr: ['close', 'open', 'max', 'setCol'],
                            maxDestAreaId: 'nc-bill-card'
                        })}
                    </div>
                </div>
            )
        };

        var renderList = () => {
            if (this.state.showMode != 'list')
                return '';
            return (
                <div className='nc-bill-tree-table' style={{ height: '100%' }}>
                    <div className="header">
                        {createPageIcon()}
                        <div className="title">{this.lang['10140PSN-000068']/* 国际化处理： 人员*/}</div>
                        {/* <div className="search-box" style={{ width: 200, marginRight: 0 }}>{Org(this.state.org)}</div> */}
                        <div className=" btn-group btn-list">
                            {createButtonApp({
                                area: 'list',//按钮注册中的按钮区域
                                buttonLimit: 3,
                                onButtonClick: (props, btncode) => {
                                    this.onBtnOperation(props, btncode, 'list');
                                },
                                popContainer: document.querySelector('.list-base')
                            })}
                        </div>
                    </div>
                    {/* <div className="search-area search-list">{NCCreateSearch(this.state.search.id, this.state.search)}</div> */}
                    <div className="nc-singleTable-table-area">
                        {createSimpleTable('psndoc_list', {

                            tableModelConfirm: () => { },
                            onAfterEvent: () => { },
                            onSelected: () => {
                            },
                            onSelectedAll: () => {
                            },
                            onRowDoubleClick: (record, index) => {
                                this.browseFormPsndoc(record.pk_psndoc.value);
                            },
                            handlePageInfoChange: this.pageInfoClick.bind(this),
                            showCheck: true
                        })}
                    </div>
                </div>
            );
        };

        var renderCard = () => {
            if (this.state.showMode != 'card') return '';
            return (
                <div className="nc-bill-card">
                    <div className="nc-bill-top-area">
                        <NCAffix>
                            <div className='nc-bill-header-area'>
                                <div className='header-title-search-area'>
                                    {this.state.card.editMode == 'edit' ? '' : <NCBackBtn className='title-search-detail' style={{ marginRight: 0 }} onClick={this.onBtnOperation.bind(this, this.props, 'card_back', 'card')}></NCBackBtn>}
                                    {createPageIcon()}
                                    <h2 className='title-search-detail'>{this.lang['10140PSN-000068']/* 国际化处理： 人员*/}</h2>
                                </div>
                                <div className="header-button-area">
                                    {createButtonApp({
                                        area: 'card',//按钮注册中的按钮区域
                                        onButtonClick: (props, btncode) => {
                                            this.onBtnOperation(props, btncode, 'card');
                                        },
                                        popContainer: document.querySelector('.list-base')
                                    })}
                                </div>
                            </div>
                        </NCAffix>
                        <div className="nc-bill-form-area edit-input-list">
                            {createForm('psndoc_card_head', {
                                //onAfterEvent: this.onPsndocAfterEvent.bind(this)
                            })}
                        </div>
                    </div>
                    <div className="nc-bill-bottom-area">
                        <div className="nc-bill-table-area tabs-add">
                            {createCardTable('psnjobs', {
                                tableHead: initTableButton,
                                //onAfterEvent: this.onPsnjobAfterEdit.bind(this),
                                // onBeforeEvent: this.onPsnjobBeforeEdit.bind(this)
                                // modelSave:this.modelSave.bind(this),
                                // showIndex:true,
                                // showCheck:true
                            })}
                        </div>
                    </div>
                </div>
            );

        };



        return (
            <div>
                {createModal('modal', { noFooter: false })}
                {createModal('modal2', { noFooter: false })}
                {renderCard()}
                {renderList()}
                {createModal('tip')}
            </div>
        )
    }

    setPsnjoRefbNull (itemcodes = [], rowid) {
        itemcodes.forEach(itemcode => {
            this.props.cardTable.setValByKeyAndRowId('psnjobs', rowid, itemcode, { value: '', display: '' });
        });
    }

    onPsndocAfterEvent (props, areacode, attrname, value, hisvalues, index, rowRecord) {

    }

}
Psndoc = createPage({
    initTemplate: function () { },
    billinfo: [{
        billtype: 'card',
        pagecode: '10140PSN_main',
        headcode: 'psndoc_card_head',
        bodycode: 'psnjobs'
    }, {
        billtype: 'grid',
        pagecode: '10140PSN_main',
        bodycode: 'psndoc_list'
    }]
})(Psndoc)
ReactDOM.render(<Psndoc />, document.querySelector('#app'));
