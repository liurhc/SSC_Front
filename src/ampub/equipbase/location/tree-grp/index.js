import React from 'react';
import { createPage } from 'nc-lightapp-front';
import TreeCard from '../../components/treecard';
import { base_config } from '../tree-base';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { initMultiLangByModule } = multiLangUtils;
const config = {
	appcode: '450101020A',
	pageId: '450101020A_tree', // 功能注册节点id
	nodeType: 'GROUP_NODE', // 节点管控模式
	nodeName: '450101515A-000017' /* 国际化处理： 资产位置-集团*/, // 节点名称
	funcode: '4501004015',
	nodekey: 'cardPrint',
	context: {}
};

let LocationGroup = createPage({
	billinfo: {
		billtype: 'form',
		pagecode: config.pageId,
		bodycode: base_config.formId
	}
})(TreeCard);
initMultiLangByModule({ ampub: [ '450101515A', 'common' ] }, () => {
	ReactDOM.render(<LocationGroup config={Object.assign(config, base_config)} />, document.querySelector('#app'));
});
