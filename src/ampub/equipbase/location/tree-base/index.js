// 位置节点基本配置
export const base_config = {
	treeId: 'tree_head', // 树组件id
	formId: 'card_head', // 表单组件id
	areaCode: 'card_head', // 模板JSON的区域code
	vnodeName: '450101515A-000016' /* 国际化处理： 位置*/, // 虚根节点名称
	url: {
		// 节点请求的路径
		query: '/nccloud/ampub/location/query.do',
		add: '/nccloud/ampub/location/insert.do',
		del: '/nccloud/ampub/location/delete.do',
		update: '/nccloud/ampub/location/update.do',
		enable: '/nccloud/ampub/location/enable.do',
		disable: '/nccloud/ampub/location/disable.do',
		print: '/nccloud/ampub/location/print.do',
		querysub: '/nccloud/ampub/location/querysub.do', // 根据其它节点传的pk查询
		querycard: '/nccloud/ampub/location/querycard.do' // 查询卡片
	},
	mapToFields: {
		// 节点字段映射关系
		pk: 'pk_location',
		enable: 'enablestate',
		pid: 'fk_parent',
		code: 'location_code',
		name: 'location_name'
	},
	getDefaultData: getDefaultData, // 设置默认值函数，@props组件的参数, @item当前节点的values
	handleAfterEditEvent: undefined, // 编辑后事件，默认实现是停启用
	delcontent: '450101515A-000019' /* 国际化处理： 该操作将会删除当前位置及其子位置，是否继续？*/
};

function getDefaultData(props, item) {
	let formData = {
		[base_config.formId]: {
			rows: [
				{
					values: {
						fk_parent: {
							value: item.refpk == '~' ? '' : item.values.pk_location.value,
							display: item.refpk == '~' ? '' : item.values.location_name.value
						},
						install_limit: {
							value: '2'
						},
						location_type: {
							value: '1'
						},
						enablestate: {
							value: '2'
						},
						pk_org: {
							value: this.state.mainorg.refpk
						}
					}
				}
			]
		}
	};
	return formData;
}
