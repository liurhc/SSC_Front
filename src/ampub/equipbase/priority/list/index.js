import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, base, createPageIcon } from 'nc-lightapp-front';
import ampub from 'ampub';
import { buttonClick, initTemplate, afterEvent, rowSelected, onSealChange } from './events';
import { pageConfig } from './const.js';

const { utils } = ampub;
const { multiLangUtils, closeBrowserUtils } = utils;
const { initMultiLangByModule, getMultiLangByID } = multiLangUtils;
const { NCCheckbox } = base;

class SingleTable extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isshowseal: false
		};
		closeBrowserUtils(props, { editTable: [ pageConfig.tableId ] });
		initTemplate.call(this, props);
	}

	render() {
		const { editTable, button } = this.props;
		const { createEditTable } = editTable;
		const { createButtonApp } = button;

		let status = editTable.getStatus(pageConfig.tableId);

		return (
			<div className="nc-single-table">
				{/* 头部 header */}
				<div className="nc-singleTable-header-area">
					{/* 标题 title */}
					<div className="header-title-search-area">
						{createPageIcon()}
						<h2 className="title-search-detail">{getMultiLangByID(pageConfig.title) /* 国际化处理：关键程度 */}</h2>
						{/* 显示停用 showOff */}
						<div className="title-search-detail">
							<span className="showOff">
								<NCCheckbox
									onChange={onSealChange.bind(this)}
									checked={this.state.isshowseal}
									disabled={status == 'edit'}
								>
									{getMultiLangByID('amcommon-000004') /* 国际化处理：显示停用 */}
								</NCCheckbox>
							</span>
						</div>
					</div>
					{/* 按钮区 btn-group */}
					<div className="header-button-area">
						{createButtonApp({
							//按钮区域（在数据库中注册的按钮区域）
							area: 'list_head',
							//按钮数量限制，超出指定数量的按钮将放入更多显示
							buttonLimit: 3,
							onButtonClick: buttonClick.bind(this),
							//指定下拉弹窗渲染的位置，不设置的话，下拉弹窗随滚动条滚动
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				{/* 列表区 table-area */}
				<div className="nc-singleTable-table-area">
					{createEditTable(pageConfig.tableId, {
						showCheck: true,
						showIndex: true,
						onAfterEvent: afterEvent,
						onSelected: rowSelected.bind(this),
						onSelectedAll: rowSelected.bind(this)
					})}
				</div>
			</div>
		);
	}
}

SingleTable = createPage({
	billinfo: {
		billtype: pageConfig.billtype,
		pagecode: pageConfig.pagecode,
		bodycode: pageConfig.tableId
	}
})(SingleTable);

initMultiLangByModule({ ampub: [ '450101008A', 'common' ] }, () => {
	ReactDOM.render(<SingleTable />, document.querySelector('#app'));
});
