import ampub from 'ampub';
const { commonConst } = ampub;
const { StatusUtils } = commonConst;
const { VOSTATUS } = StatusUtils;
// 页面配置
const pageConfig = {
	// 小应用编码
	appcode: '450101008A',
	// 节点名称
	title: '450101008A-000001' /* 国际化处理：关键程度 */,
	// 表格区域编码
	tableId: 'list_head',
	//列表区域类型
	areaType: 'table',
	// 表格节点编码
	pagecode: '450101008A_list',
	// 功能节点编码，即模板编码
	printFuncode: '4501004025',
	// 单据类型
	billtype: 'grid',
	// VO状态
	vo_status: { ...VOSTATUS },
	// 打印模板节点标识
	printNodekey: '',
	// 浏览态按钮
	browseHeadBtns: {
		Add: true,
		Edit: true,
		Delete: true,
		Save: false,
		Cancel: false,
		Print: true,
		Output: true,
		Refresh: true
	},
	// 编辑态按钮
	editHeadBtns: {
		Add: true,
		Edit: false,
		Delete: true,
		Save: true,
		Cancel: true,
		Print: false,
		Output: false,
		Refresh: false
	},
	url: {
		printUrl: '/nccloud/ampub/equipbase/print.do'
	}
};

const btnUsability = {
	browseNoData: {
		Save: true,
		Add: false,
		Edit: false,
		Cancel: true,
		Delete: true,
		Print: true,
		Output: true
	},
	browseHasData: {
		Save: true,
		Add: false,
		Edit: false,
		Cancel: true,
		Delete: false,
		Print: false,
		Output: false
	},
	editNoData: {
		Save: false,
		Add: false,
		Edit: true,
		Cancel: false,
		Delete: true,
		Print: true,
		Output: true
	},
	editHasData: {
		Save: false,
		Add: false,
		Edit: true,
		Cancel: false,
		Delete: false,
		Print: true,
		Output: true
	}
};

export { pageConfig, btnUsability };
