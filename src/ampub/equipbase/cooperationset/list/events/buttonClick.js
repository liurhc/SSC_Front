import { ajax, print, output } from 'nc-lightapp-front';
import { pageConfig, btnUsability } from '../const.js';
import ampub from 'ampub';
const { utils, commonConst } = ampub;
const { multiLangUtils, msgUtils, tableUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { modifyBeforeAjax, modifyAfterAjax, modifyBeforeAjaxByIndex, modifyAfterAjaxByIndex } = tableUtils;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { UISTATE } = commonConst.StatusUtils;

const tableId = pageConfig.tableId;
const areaType = pageConfig.areaType;
const areacode = pageConfig.tableId;
const pageid = pageConfig.pagecode;
const editHeadBtns = pageConfig.editHeadBtns;
const browseHeadBtns = pageConfig.browseHeadBtns;
const filterFields = pageConfig.filterFields;

export default function(props, id) {
	switch (id) {
		case 'Add':
			addClick.call(this, props);
			break;
		case 'Cancel':
			cancelClick.call(this, props);
			break;
		case 'Save':
			saveClick.call(this, props);
			break;
		case 'Delete':
			batchDelClick.call(this, props);
			break;
		case 'Edit':
			editClick.call(this, props);
			break;
		case 'delRow':
			delRow.call(this, props);
			break;
		case 'singleDel':
			singleDel.call(this, props);
			break;
		case 'Refresh':
			refresh.call(this, props);
			break;
		default:
			break;
	}
}

export function getData(callBack) {
	let data = {
		pagecode: pageConfig.pagecode
	};
	ajax({
		url: '/nccloud/ampub/equipbase/CooperationSetQueryAll.do',
		data: data,
		success: (res) => {
			afterGetData.call(this, res.data);
			callBack && typeof callBack == 'function' && callBack.call(this);
		}
	});
}
function afterGetData(data) {
	if (data) {
		let allRows = data[pageConfig.tableId].rows;
		this.props.editTable.setTableData(pageConfig.tableId, { rows: allRows });
	} else {
		this.props.editTable.setTableData(pageConfig.tableId, { rows: [] });
	}
	setFiltrate.call(this);
}

//新增按钮点击事件
export function addClick(props, autoFocus = true) {
	let oldstatus = props.editTable.getStatus(tableId);
	if (oldstatus != UISTATE.edit) {
		setStatus.call(this, props, UISTATE.edit);
	}
	let defaultValue = {};
	defaultValue['enablestate'] = {
		value: '2'
	};
	// 设置协同类型
	defaultValue['cooperset_type'] = {
		// 设备协同设置
		value: '1'
	};
	props.editTable.addRow(tableId, undefined, autoFocus, defaultValue);
	setBatchBtnsEnable(props, tableId);
}

// 批量删除数据
function batchDelClick(props) {
	let checkedRows = props.editTable.getCheckedRows(tableId);
	if (checkedRows.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseDelete });
		return;
	}
	let indexArr = [];
	checkedRows.map((row) => {
		indexArr.push(row.index);
	});
	let status = props.editTable.getStatus(tableId);
	if (status == 'browse' || status == undefined) {
		showConfirm.call(this, props, {
			type: MsgConst.Type.DelSelect,
			beSureBtnClick: () => {
				delClick.call(this, props, tableId, indexArr);
			}
		});
	} else {
		delRow(props, indexArr);
	}
}

/**
 * 前台删除
 * @param {*} props 
 * @param {*} index 
 */
export function delRow(props, index) {
	props.editTable.deleteTableRowsByIndex(tableId, index);
	setBatchBtnsEnable(props, pageConfig.tableId);
}

/**
 * 删除
 * @param {*} props 
 */
function delClick(props, moduleId, index = []) {
	let checkedRows = props.editTable.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length == 0) {
		return;
	}
	// 平台没有克隆数据问题
	checkedRows = JSON.parse(JSON.stringify(checkedRows));
	let delRows = [];
	let checkedIndexs = [];
	// 设置删除状态
	checkedRows.map((item, index) => {
		modifyBeforeAjax(item.data);
		(item.data.status = '3'), delRows.push(item.data);
		checkedIndexs.push(item.data.flterIndex);
	});
	const data = {
		model: {
			areaType: 'table',
			areacode: tableId,
			rows: delRows
		},
		pageid: pageid
	};
	ajax({
		url: '/nccloud/ampub/equipbase/CooperationSetSave.do',
		data,
		success: (res) => {
			let { success } = res;
			if (success) {
				let allData = props.editTable.getAllData(tableId);
				allData.rows = allData.rows.filter((item, i) => !checkedIndexs.includes(i));
				props.editTable.setTableData(tableId, allData);
				setBatchBtnsEnable(props, pageConfig.tableId);
				setFiltrate.call(this);
				showMessage.call(this, props, { type: MsgConst.Type.DeleteSuccess });
			}
		}
	});
}

/**
 * 浏览态直接删除单个数据
 * @param {*} props 
 * @param {*} tableId 
 * @param {*} index 
 */
export function singleDel(props, index) {
	let tableData = props.editTable.getAllRows(tableId, false);
	// 将enablestate的值由boolean转换为int类型，以适配后端
	tableData.map((item) => {
		modifyBeforeAjax(item);
	});
	let changeRow = [];
	for (let i = 0; i < tableData.length; i++) {
		if (index == i) {
			changeRow.push(tableData[i]);
		}
	}
	changeRow[0].status = '3';
	const data = {
		model: {
			areaType,
			areacode,
			rows: []
		},
		pageid
	};
	data.model.rows = changeRow;
	ajax({
		url: '/nccloud/ampub/equipbase/CooperationSetSave.do',
		data,
		success: (res) => {
			let allRows = props.editTable.getAllRows(tableId);
			let newRows = [];
			for (let i = 0; i < allRows.length; i++) {
				if (index != i) {
					newRows.push(allRows[i]);
				}
			}
			let allData = props.editTable.getAllData(tableId);
			allData.rows = newRows;
			props.editTable.setTableData(tableId, allData);
			setFiltrate.call(this);
			showMessage.call(this, props, { type: MsgConst.Type.DeleteSuccess });
		}
	});
}

/**
 * 修改
 * @param {*} props 
 */
function editClick(props) {
	setStatus.call(this, props, UISTATE.edit);
	setBatchBtnsEnable(props, tableId);
}

//取消按钮点击事件
function cancelClick(props) {
	showConfirm.call(this, props, {
		type: MsgConst.Type.Cancel,
		beSureBtnClick: () => {
			props.editTable.cancelEdit(tableId, () => {
				props.button.setButtonsVisible(browseHeadBtns);
				// 设置按钮主次关系，新增设为主要按钮
				props.button.setMainButton('Add', true);
				// 行删除时悬浮框提示
				props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001')); /* 国际化处理：确定要删除吗？ */
				props.button.setButtonDisabled(btnUsability.browseNoData);
			});
		}
	});
}

//保存按钮点击事件
function saveClick(props) {
	//过滤空行
	props.editTable.filterEmptyRows(tableId, [ 'enablestate', 'pk_group', 'pk_org', 'cooperset_type', 'sponsor' ]);
	let allRows = props.editTable.getAllRows(tableId);
	let pass = props.editTable.checkRequired(tableId, allRows);
	// 必输项校验没过
	if (!pass) {
		return;
	}
	let changedRows = props.editTable.getChangedRows(tableId);
	// 没有修改行，变更状态
	if (!changedRows || changedRows.length == 0) {
		setStatus.call(this, props, UISTATE.browse);
		showMessage.call(this, props, { type: MsgConst.Type.SaveSuccess });
		return;
	}
	const data = {
		model: {
			areaType,
			areacode,
			rows: []
		},
		pageid
	};
	data.model.rows = allRows;
	props.validateToSave(data, () => {
		ajax({
			url: '/nccloud/ampub/equipbase/CooperationSetSave.do',
			data,
			success: (res) => {
				afterSave.call(this, res.data);
			}
		});
	});
}

function afterSave(data) {
	let retData = data[tableId];
	let newRows = [];
	let i = 0;
	let allData = this.props.editTable.getAllData(tableId);
	let allRows = allData.rows;
	allRows.map((item, index) => {
		// 0原始1修改2新增3删除
		if (
			item.status == pageConfig.vo_status.UPDATED ||
			item.status == pageConfig.vo_status.NEW ||
			item.status == pageConfig.vo_status.UNCHANGED
		) {
			if (retData && retData.rows && retData.rows.length > i) {
				modifyAfterAjax(retData.rows[i]);
				newRows.push(retData.rows[i]);
			}
			i++;
		} else if (item.status == pageConfig.vo_status.DELETED) {
			i++;
		}
	});
	allData.rows = newRows;
	setStatus.call(this, this.props, 'browse');
	this.props.editTable.setTableData(tableId, allData);
	setFiltrate.call(this);
	this.props.editTable.cancelEdit(tableId);
	this.props.button.setButtonsVisible(browseHeadBtns);
	setBatchBtnsEnable(this.props, pageConfig.tableId);
	showMessage.call(this, this.props, { type: MsgConst.Type.SaveSuccess });
}
/**
 * 设置筛选后的表格数据
 * @param {*} status 
 */
export function setFiltrate() {
	// 调用平台api过滤显示数据
	const { keyWord } = this.state;
	this.props.editTable.setFiltrateTableData(tableId, filterFields, keyWord);
}
/**
 * 设置界面状态
 * @param {*} props 
 * @param {*} status @default UISTATE.browse
 */
export function setStatus(props, status = UISTATE.browse) {
	// 获取选中的行
	let checkedRows = props.editTable.getCheckedRows(tableId);
	switch (status) {
		case UISTATE.edit:
			props.editTable.setStatus(tableId, UISTATE.edit);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonsVisible(editHeadBtns);
			// 如果有新增按钮，改为次要按钮
			props.button.setMainButton('Add', false);
			// 行删除时不需要悬浮框提示
			props.button.setPopContent('Delete', undefined);
			// 设置按钮可用性
			if (checkedRows && checkedRows.length == 0) {
				props.button.setButtonDisabled(btnUsability.editNoData);
			} else if (checkedRows && checkedRows.length != 0) {
				props.button.setButtonDisabled(btnUsability.editHasData);
			}
			modifyBeforeAjaxByIndex.call(this, props);
			break;
		default:
			// 设置表格状态
			props.editTable.setStatus(tableId, UISTATE.browse);
			// 设置按钮可见性
			props.button.setButtonsVisible(browseHeadBtns);
			// 设置按钮主次关系，新增设为主要按钮
			props.button.setMainButton('Add', true);
			// 行删除时悬浮框提示
			props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001')); /* 国际化处理：确定要删除吗？ */
			// 设置按钮可用性
			if (!checkedRows || checkedRows.length == 0) {
				props.button.setButtonDisabled(btnUsability.browseNoData);
			} else {
				props.button.setButtonDisabled(btnUsability.browseHasData);
			}
			modifyAfterAjaxByIndex.call(this, props);
			break;
	}
}

/**
 * 设置批量按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBatchBtnsEnable(props) {
	let status = props.editTable.getStatus(tableId);
	let checkedRows = props.editTable.getCheckedRows(tableId);
	if (status === UISTATE.browse) {
		if (!checkedRows || checkedRows.length == 0) {
			props.button.setButtonDisabled(btnUsability.browseNoData);
		} else {
			props.button.setButtonDisabled(btnUsability.browseHasData);
		}
	} else if (status == UISTATE.edit) {
		if (checkedRows && checkedRows.length == 0) {
			props.button.setButtonDisabled(btnUsability.editNoData);
		} else if (checkedRows && checkedRows.length != 0) {
			props.button.setButtonDisabled(btnUsability.editHasData);
		}
	}
}

/**
* 刷新
* @param {*} props 
*/
export function refresh(props) {
	getData.call(this, () => {
		showMessage.call(this, props, { type: MsgConst.Type.RefreshSuccess });
	});
}
