import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, ajax, base, toast } from 'nc-lightapp-front';
import { buttonClick, initTemplate, afterEvent, rowSelected, onSealChange } from './events';
import { pageConfig } from './const.js';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID, initMultiLangByModule } = multiLangUtils;
const { NCFormControl } = base;

class SingleTable extends Component {
	constructor(props) {
		super(props);
		this.state = {
			keyWord: '',
			isshowseal: false
		};
		initTemplate.call(this, props);
	}
	// 搜索关键词变化
	keyWordChange = (value) => {
		this.setState({ keyWord: value }, () => {
			// 调用平台api过滤显示数据
			let { filterFields, tableId } = pageConfig;
			let { editTable } = this.props;
			editTable.setFiltrateTableData(tableId, filterFields, value);
		});
	};
	render() {
		const { editTable, button } = this.props;
		const { createEditTable } = editTable;
		const { createButtonApp } = button;

		let { keyWord } = this.state;
		let status = editTable.getStatus(pageConfig.tableId);

		return (
			<div className="nc-single-table">
				{/* 头部 header */}
				<div className="nc-singleTable-header-area">
					{/* 标题 title */}
					<div className="header-title-search-area">
						<h2 className="title-search-detail">{getMultiLangByID(pageConfig.title) /* 国际化处理：资产协同设置 */}</h2>
						{/* 简单搜索 search-box */}
						<div className="title-search-detail">
							<NCFormControl
								placeholder={getMultiLangByID('450103504A-000002') /* 国际化处理： 请输入组织*/}
								value={keyWord}
								onChange={(value) => {
									this.keyWordChange(value);
								}}
								type="search"
								disabled={status == 'edit'}
							/>
						</div>
					</div>
					{/* 按钮区 btn-group */}
					<div className="header-button-area">
						{createButtonApp({
							//按钮区域（在数据库中注册的按钮区域）
							area: 'list_head',
							//按钮数量限制，超出指定数量的按钮将放入更多显示
							buttonLimit: 3,
							onButtonClick: buttonClick.bind(this),
							//指定下拉弹窗渲染的位置，不设置的话，下拉弹窗随滚动条滚动
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				{/* 列表区 table-area */}
				<div className="nc-singleTable-table-area">
					{createEditTable(pageConfig.tableId, {
						showCheck: true,
						showIndex: true,
						onAfterEvent: afterEvent,
						onSelected: rowSelected.bind(this),
						onSelectedAll: rowSelected.bind(this)
					})}
				</div>
			</div>
		);
	}
}

SingleTable = createPage({
	billinfo: {
		billtype: pageConfig.billtype,
		pagecode: pageConfig.pagecode,
		bodycode: pageConfig.tableId
	}
})(SingleTable);

initMultiLangByModule({ ampub: [ 'common', '450103504A' ] }, () => {
	ReactDOM.render(<SingleTable />, document.querySelector('#app'));
});
