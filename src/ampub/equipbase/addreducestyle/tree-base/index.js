// 增减方式公共配置
import { ajax, toast } from 'nc-lightapp-front';
import ampub from 'ampub';

const { utils } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

// 基础公共配置文件
export const base_config = {
	treeId: 'tree_head', // 树组件id
	formId: 'card_head', // 表单组件id
	areaCode: 'card_head', // 模板JSON的区域code
	vnodeName: '450101515A-000002' /* 国际化处理： 增减方式*/, // 虚根节点名称
	url: {
		// 节点请求的路径
		query: '/nccloud/ampub/addreducestyle/query.do',
		add: '/nccloud/ampub/addreducestyle/insert.do',
		update: '/nccloud/ampub/addreducestyle/update.do',
		del: '/nccloud/ampub/addreducestyle/delete.do',
		enable: '/nccloud/ampub/addreducestyle/enable.do',
		disable: '/nccloud/ampub/addreducestyle/disable.do',
		print: '/nccloud/ampub/addreducestyle/print.do',
		querycard: '/nccloud/ampub/addreducestyle/querycard.do'
	},
	mapToFields: {
		// 节点字段映射关系
		pk: 'pk_addreducestyle',
		enable: 'enablestate',
		pid: 'parent_id',
		code: 'style_code',
		name: 'style_name'
	},
	getDefaultData: undefined, // 设置默认值函数，@props组件的参数, @item当前节点的values
	handleAfterEditEvent: undefined, // 编辑后事件，默认实现是停启用
	handleMouseEnterEve: handleMouseEnterEve, // 鼠标移入树节点时触发，控制图标的按钮显示性
	addClick: addClick, // 新增方法
	beforeDelValidate: beforeDelValidate // 删除前校验
};

/**
 * 
 * @param {鼠标划入节点事件} key 
 */
function handleMouseEnterEve(key) {
	if (key === '~') {
		let obj = {
			delIcon: false, //false:隐藏； true:显示; 默认都为true显示
			editIcon: false,
			addIcon: false
		};
		this.props.syncTree.hideIcon(this.treeId, key, obj);
	} else {
		let obj = {
			delIcon: true, //false:隐藏； true:显示; 默认都为true显示
			editIcon: true,
			addIcon: true
		};
		this.props.syncTree.hideIcon(this.treeId, key, obj);
	}
}

/**
 * 新增事件，包括校验和设置默认值
 * @param {*} props 
 * @param {*} item 
 * @param {请求成功后的回调函数} callback
 */
function addClick(props, item, callback) {
	let level = item.values.stylelevel.value;
	if (parseInt(level) >= 5) {
		toast({ content: getMultiLangByID('450101515A-000003') /* 国际化处理：编码级次大于或等于5，不能增加 */, color: 'warning' });
		return;
	}
	if (item.values.enablestate.value === true) {
		item.values.enablestate.value = '2';
	} else {
		item.values.enablestate.value = '3';
	}
	let sendData = JSON.parse(JSON.stringify(item.values));
	ajax({
		url: '/nccloud/ampub/addreducestyle/add.do',
		data: {
			pageid: props.config.pageId,
			model: {
				areacode: props.config.areaCode,
				areaType: 'form',
				rows: [ { values: sendData } ]
			}
		},
		success: (res) => {
			let { success, data } = res;
			if (success && data) {
				let formid = this.props.config.formId;
				this.props.form.EmptyAllFormValue(formid);
				let item = data[formid].rows[0];
				if (item.values.enablestate.value == '2') {
					item.values.enablestate.value = true;
				} else {
					item.values[enable].value = false;
				}
				let formdata = { [formid]: { rows: [ { values: data[formid].rows[0].values } ] } };
				this.props.form.setAllFormValue(formdata);
				callback();
			}
		}
	});
}

/**
 * 删除前校验
 * 	原NC前台校验有3种
 *	1. 预置的增减方式除盘亏外均可以删除
 *	2. 验证是否被引用
 *	3. 判断是否是叶子节点，如果不是则不能删除
 *	这里2/3放在后台校验，1在这里校验
 * @param {*} props 
 * @param {*} item 
 * @return {通过:true, 失败:false}
 */
function beforeDelValidate(props, item) {
	let style_code = item.values['style_code'].value;
	let pre_flag = item.values['pre_flag'].value;
	if (style_code === '01' || style_code === '02') {
		toast({ content: getMultiLangByID('450101515A-000004') /* 国际化处理：预置的一级数据不能删除。 */, color: 'warning' });
		return false;
	}
	if (pre_flag === '1' && style_code === '0202') {
		toast({ content: getMultiLangByID('450101515A-000005') /* 国际化处理：不能删除预置为盘亏的数据。 */, color: 'warning' });
		return false;
	}
	return true;
}
