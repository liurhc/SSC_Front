import React from 'react';
import { createPage } from 'nc-lightapp-front';
import TreeCard from '../../components/treecard';
import { base_config } from '../tree-base';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { initMultiLangByModule } = multiLangUtils;
const config = {
	pageId: '450101515A_tree', // 功能注册节点id
	nodeType: 'GLOBE_NODE', // 节点管控模式
	nodeName: '450101515A-000000' /* 国际化处理： 增减方式-全局*/, // 节点名称
	funcode: '4501008016',
	nodekey: null
};

let AddReduceStyle = createPage({
	billinfo: {
		billtype: 'form',
		pagecode: config.pageId,
		bodycode: base_config.formId
	}
})(TreeCard);
initMultiLangByModule({ ampub: [ '450101515A', 'common' ] }, () => {
	ReactDOM.render(<AddReduceStyle config={Object.assign(config, base_config)} />, document.querySelector('#app'));
});
