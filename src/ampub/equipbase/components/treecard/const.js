const defaultButtons = {
	Save: false,
	Cancel: false,
	Print: true,
	Refresh: true
};

const editButtons = {
	Save: true,
	Cancel: true,
	Print: false,
	Refresh: false
};

export const buttonsLayout = {
	default: defaultButtons,
	edit: editButtons
};

export const browseBtns = [ 'Print', 'Output' ];
