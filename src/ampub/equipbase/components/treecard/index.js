import React, { Component } from 'react';
import { base, ajax, createPageIcon } from 'nc-lightapp-front';
import {
	initTemplate,
	dispatcher,
	handleAfterEditEvent,
	addClick,
	editClick,
	delClick,
	modifyAfterAjax,
	queryCard,
	setBrowse
} from './events';
import { browseBtns } from './const.js';

import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils, msgUtils, closeBrowserUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { MsgConst, showMessage } = msgUtils;
const { AMRefer, LoginContext } = components;
const { loginContextKeys, getContext } = LoginContext;

const { NCCheckbox } = base;

// const config = {
// 	pageId: '', // 功能注册节点id
// 	treeId: '', // 树组件id
// 	formId: '', // 表单组件id
// 	areaCode: '', // 模板JSON的区域id
// 	nodeType: '', // 节点管控模式
// 	nodeName: '', // 节点名称
// 	url: {
// 		// 节点请求的路径
// 		query: '',
//      querycard:'',
// 		add: '',
// 		del: '',
// 		save: '',
// 		update: '',
// 		enable: '',
// 		disable: ''
// 	},
// 	mapToFields: {
// 		// 节点字段映射关系
// 		pk: '',
// 		enable: '',
// 		pid: '',
// 		code: '',
// 		name: ''
// 	},
// 	getDefaultData: null, // 设置默认值函数，参数 @props, @item当前节点的values
// 	handleAfterEditEvent: null, // 设置form的编辑后事件处理函数，参数 @props, @moduleId(区域id), @key(操作的键), @value（当前值）
// 	handleMouseEnterEve: null // 设置树表移入节点热区时显示哪些操作图标，参数@key 当前节点的key
//  addClick: addClick // 新增方法, 参数 @props, @item当前节点的value
//  beforeDelValidate: beforeDelValidate // 删除前校验，通过返回true,失败返回false 参数 @props @item
// };

class TreeCard extends Component {
	static defaultProps = {};
	static propTypes = {};
	constructor(props) {
		super(props);
		this.treeId = props.config.treeId;
		this.formId = props.config.formId;
		this.state = {
			showOff: false, // 是否显示停用
			mainorg: { refpk: '' }
		};
		this.pk_group = '';
		closeBrowserUtils(this.props, { form: [ this.formId ] });
		initTemplate.call(this, props);
	}

	componentDidMount = () => {
		// 通过联查进入
		// if (this.props.getUrlParam('status') == 'query') {
		// 	this.getQueryTreeData();
		// } else {
		// 	this.getTreeData();
		// }
	};

	getTreeData = (pk_org = this.state.mainorg.refpk, isRefresh = false) => {
		ajax({
			url: this.props.config.url['query'],
			data: {
				pagecode: this.props.config.pageId,
				areacode: this.props.config.areaCode,
				nodeType: this.props.config.nodeType,
				isShowSeal: this.state.showOff ? 'Y' : 'N',
				pk_org
			},
			success: (res) => {
				let { data } = res;
				if (data) {
					data[this.formId].rows.map((item) => {
						modifyAfterAjax(this.props, item);
					});
					let treeData = this.buildTreeData(data[this.formId].rows);
					this.sortTree(treeData);
					this.props.syncTree.setSyncTreeData(this.treeId, treeData);
					this.props.syncTree.openNodeByPk(this.treeId, '~');
					// 默认选中第一个节点
					let first = treeData[0].children[0].refpk;
					this.handleSelect(first, { refpk: first });
					this.props.syncTree.setNodeSelected(this.treeId, first);
					this.props.form.setFormItemsDisabled(this.formId, { [this.props.config.mapToFields.enable]: true });
				} else {
					let vnode = {
						refname: getMultiLangByID(this.props.config.vnodeName),
						refpk: '~',
						refcode: '~',
						children: [],
						iconBox: []
					};
					this.props.syncTree.setSyncTreeData(this.treeId, [ vnode ]);
					this.handleSelect(vnode.refpk, vnode);
					this.props.syncTree.setNodeSelected(this.treeId, vnode.refpk);
					this.props.form.setFormItemsDisabled(this.formId, { [this.props.config.mapToFields.enable]: true });
				}
				setBrowse(this.props);
				if (isRefresh) {
					this.props.syncTree.getSearchChangeEven('', this.treeId);
					showMessage.call(this, this.props, { type: MsgConst.Type.RefreshSuccess });
				}
			}
		});
	};

	getQueryTreeData = () => {
		let pk = this.props.getUrlParam('id');
		ajax({
			url: this.props.config.url['querysub'],
			data: {
				pk,
				pagecode: this.props.config.pageId,
				areacode: this.props.config.areaCode,
				nodeType: this.props.config.nodeType
			},
			success: (res) => {
				let { data } = res;
				if (data) {
					data[this.formId].rows.map((item) => {
						modifyAfterAjax(this.props, item);
					});
					let treeData = this.buildTreeData(data[this.formId].rows);
					this.sortTree(treeData);
					this.props.syncTree.setSyncTreeData(this.treeId, treeData);
					let node = this.props.syncTree.getSyncTreeValue(this.treeId, pk);
					this.props.syncTree.openNodeByPk(this.treeId, node.pid);
					this.props.syncTree.setNodeSelected(this.treeId, pk);
					queryCard(this.props, pk, true);
					setBrowse(this.props);
					let tarloc = data[this.formId].rows.filter((item) => {
						return item.values[this.props.config.mapToFields.pk].value === pk;
					});
					// 将选中的位置的组织设置到组织参照
					let value = {
						refpk: tarloc[0].values.pk_org.value,
						refname: tarloc[0].values.pk_org.display
					};
					this.setState({
						mainorg: {
							defaultValue: value
						}
					});
				}
			}
		});
	};

	handleSelect(pk, item, ischange) {
		if (item.refpk == '~') {
			this.props.form.EmptyAllFormValue(this.formId);
			this.props.button.setButtonDisabled(browseBtns, true);
			return;
		}
		queryCard(this.props, pk, true);
		setBrowse(this.props);
		this.props.button.setButtonDisabled(browseBtns, false);
	}

	sortTree = (
		tree,
		sortfunc = (a, b) => {
			if (a.refname > b.refname) {
				return 1;
			} else if (a.refname < b.refname) {
				return -1;
			} else {
				return 0;
			}
		}
	) => {
		// 对上层树排序
		tree.sort(sortfunc);
		// 遍历所有节点，如果节点有子树，递归地对子树排序
		tree.map((item) => {
			if (item.hasOwnProperty('children') && item.children != null) {
				this.sortTree(item.children);
			}
		});
	};

	buildTreeData(data) {
		let { pk, pid, code, name } = this.props.config.mapToFields;
		let index = getContext(loginContextKeys.languageIndex);
		data.forEach((item) => {
			let values = item.values;
			if (!values.hasOwnProperty(pid) || !values[pid].hasOwnProperty('value') || values[pid].value == null) {
				item.values[pid] = {
					value: '~'
				};
			}
		});
		let formatData = data.map((item, key) => {
			let values = item.values;
			if (values.dr) {
				delete values.dr;
			}
			let dataType = '';
			if (values.pk_group.value == values.pk_org.value) {
				dataType = 'group';
			} else {
				dataType = 'org';
			}
			let refNameVal = values[name + index].value;
			if (!refNameVal) {
				refNameVal = values[name].value;
			}
			let data = {
				pid: values[pid].value,
				refcode: values[code].value,
				refname: values[code].value + ' ' + refNameVal,
				refpk: values[pk].value,
				id: values[pk].value,
				dataType,
				values: JSON.parse(JSON.stringify(values))
			};
			return data;
		});
		let vnode = {
			refname: getMultiLangByID(this.props.config.vnodeName),
			refpk: '~',
			refcode: '~',
			id: '~'
		};
		formatData.push(vnode);
		let treeData = this.props.syncTree.createTreeData(formatData);
		return treeData;
	}

	handleShowOffChange = () => {
		this.setState({ showOff: !this.state.showOff }, () => {
			this.getTreeData();
		});
	};

	handleMouseEnterEve(key) {
		if (key === '~') {
			let obj = {
				delIcon: false, //false:隐藏； true:显示; 默认都为true显示
				editIcon: false,
				addIcon: true
			};
			this.props.syncTree.hideIcon(this.treeId, key, obj);
		} else {
			let obj = {
				delIcon: true, //false:隐藏； true:显示; 默认都为true显示
				editIcon: true,
				addIcon: true
			};
			this.props.syncTree.hideIcon(this.treeId, key, obj);
		}
	}

	// 主组织切换
	handleMainOrgChange = (value = {}) => {
		this.setState({ mainorg: value }, () => {
			// 根据组织查询数据
			this.getTreeData(value.refpk);
		});
		// 部分代码只能取到props，取不到state，这里把主组织放到props中
		this.props.config.mainorg = value;
	};

	render() {
		const { button, form, syncTree, ncmodal, DragWidthCom } = this.props;
		const { createForm } = form;
		let { createButtonApp } = button;
		let { createSyncTree } = syncTree;
		let { createModal } = ncmodal;
		let status = this.props.form.getFormStatus(this.formId);
		let mainorgobj = {
			defaultValue: this.state.mainorg,
			disabled: status == 'edit',
			onChange: (value) => {
				this.handleMainOrgChange(value);
			},
			refcode: 'uapbd/refer/org/AssetOrgGridRef/index.js',
			queryCondition: () => {
				return { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter' };
			}
		};
		return (
			<div className="nc-bill-tree-card">
				<div className="header">
					{createPageIcon()}
					{/* 标题 title*/}
					<div className="title">{getMultiLangByID(this.props.config.nodeName)}</div>
					{/* 主组织 main-org */}
					{this.props.config.nodeType == 'ORG_NODE' && (
						<div className="search-box">
							<AMRefer
								className="ref title-search-detail"
								config={Object.assign(mainorgobj, this.state.mainorg)}
							/>
						</div>
					)}
					<span className="showOff">
						<NCCheckbox
							defaultChecked={false}
							checked={this.state.showOff}
							onChange={this.handleShowOffChange}
							disabled={status == 'edit'}
						>
							{getMultiLangByID('amcommon-000004') /* 国际化处理：显示停用 */}
						</NCCheckbox>
					</span>

					{/* 按钮组 btn-group*/}
					<div className="btn-group">
						{createButtonApp({
							area: 'card_head',
							buttonLimit: 4,
							onButtonClick: dispatcher.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				<div className="tree-card">
					<DragWidthCom
						// 左树区域
						leftDom={
							<div className="tree-area">
								{createSyncTree({
									treeId: this.treeId,
									needSearch: true,
									needEdit: true,
									onSelectEve: this.handleSelect.bind(this),
									onMouseEnterEve: this.props.config.handleMouseEnterEve
										? this.props.config.handleMouseEnterEve.bind(this)
										: this.handleMouseEnterEve.bind(this),
									clickEditIconEve: editClick.bind(this),
									clickAddIconEve: addClick.bind(this),
									clickDelIconEve: delClick.bind(this),
									showModal: false,
									searchType: 'filtration',
									disabledSearch: status == 'edit' ? true : false
								})}
							</div>
						}
						rightDom={
							<div className="card-area">
								{createForm(this.formId, {
									onAfterEvent: handleAfterEditEvent.bind(this)
								})}
							</div>
						}
						defLeftWid="280px" // 默认左侧区域宽度，px/百分比
					/>
				</div>
				{createModal('modal')}
			</div>
		);
	}
}

export default TreeCard;
