import { buttonClicks } from './buttonClick';

export default function handleAfterEditEvent(props, moduleId, key, value) {
	let { enable } = props.config.mapToFields;
	if (key === enable) {
		if (value.value) {
			//value表示编辑后的值（新值）
			buttonClicks.enable(props, moduleId, 'enable');
		} else {
			buttonClicks.disable(props, moduleId, 'disable');
		}
	}
}
