import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { LoginContext } = components;
const { loginContextKeys, getContext, loginContext } = LoginContext;
import { buttonClicks } from './buttonClick';

export default function(props) {
	props.createUIDom(
		{
			pagecode: props.config.pageId // 页面id
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.template) {
					let meta = data.template;
					modifiy(meta[props.config.formId], props);
					props.meta.setMeta(meta, () => {
						initAfter.call(this, props);
					});
				}
			}
		}
	);
}

function initAfter(props) {
	if (this.props.getUrlParam('status') == 'query') {
		this.getQueryTreeData();
	} else {
		// 默认主组织
		let pk_org = getContext(loginContextKeys.pk_org);
		let org_Name = getContext(loginContextKeys.org_Name);
		if (props.config.nodeType == 'ORG_NODE' && pk_org) {
			this.handleMainOrgChange({ refpk: pk_org, refname: org_Name });
		} else {
			this.getTreeData();
		}
	}
}

function modifiy(data, props) {
	let { pid, enable } = props.config.mapToFields;
	data.items.map((item) => {
		if (item.attrcode == pid) {
			item.queryCondition = () => {
				let data = props.form.getFormItemsValue(props.config.formId, 'pk_org').value;
				return { pk_org: data }; // 根据pk_org过滤
			};
		} else if (item.attrcode == enable) {
			item.onPopconfirmCont = getMultiLangByID('msgUtils-000012') /* 国际化处理： 是否确认启用？*/;
			item.offPopconfirmCont = getMultiLangByID('msgUtils-000013') /* 国际化处理： 是否确认停用？*/;
			// 默认不直接停启用，需要后台返回成功才会相应停启用
			item.onSwitchBefore = () => {
				let formData = props.form.getAllFormValue(props.config.formId) || {};
				let record = formData.rows[0];
				if (record && record.values) {
					props.config.map;
					if (record.values[item.attrcode].value === true || record.values[item.attrcode].value === '2') {
						buttonClicks.disable(props, 'disable');
					} else {
						buttonClicks.enable(props, 'enable');
					}
				}
				// 停启用状态不变，后台成功后再处理
				return false;
			};
		}
	});
}
