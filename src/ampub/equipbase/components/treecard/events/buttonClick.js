import { ajax, toast, print, output, promptBox } from 'nc-lightapp-front';
import ampub from 'ampub';
import { buttonsLayout } from '../const.js';

const { utils, components } = ampub;
const { msgUtils, multiLangUtils } = utils;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { Type } = MsgConst;
const { getMultiLangByID } = multiLangUtils;
const { LoginContext } = components;
const { loginContextKeys, getContext } = LoginContext;

export const buttonClicks = {
	add: addClick,
	edit: editClick,
	del: delClick,
	Save: saveClick,
	Cancel: cancelClick,
	enable: statusOperation,
	disable: statusOperation,
	Print: printClick,
	Output: outputClick,
	Refresh: refreshAction
};

export function refreshAction() {
	this.getTreeData(this.state.mainorg.refpk, true);
}

export function dispatcher(props, id) {
	buttonClicks[id].call(this, props);
}

// 新增图标点击事件
export function addClick(item) {
	if (this.props.config.nodeType == 'ORG_NODE' && this.state.mainorg.refpk == '') {
		toast({ content: getMultiLangByID('450101515A-000006') /* 国际化处理：请先选择组织 */, color: 'warning' });
		return;
	}
	if (this.props.config.addClick == undefined) {
		this.props.form.EmptyAllFormValue(this.props.config.formId);
		let formData = this.props.config.getDefaultData.call(this, this.props, item);
		this.props.form.setAllFormValue(formData);
		setEdit.call(this, this.props);
		afterSetFrom.call(this, this.props);
	} else if (typeof this.props.config.addClick === 'function') {
		this.props.config.addClick.call(this, this.props, item, () => {
			setEdit.call(this, this.props);
			afterSetFrom.call(this, this.props);
		});
	}
	if (this.props.config.nodeType == 'ORG_NODE') {
		this.props.form.setFormItemsValue(this.props.config.formId, {
			pk_org: {
				value: this.state.mainorg.refpk
			}
		});
	} else {
		this.props.form.setFormItemsValue(this.props.config.formId, {
			pk_org: {
				value: getContext(loginContextKeys.groupId)
			}
		});
	}
}

// 修改图标点击事件
export function editClick(item) {
	if (this.props.config.nodeType == 'ORG_NODE' && item.dataType == 'group') {
		toast({ content: getMultiLangByID('450101515A-000007') /* 国际化处理：组织节点不能编辑集团数据 */, color: 'warning' });
		return;
	}
	queryCard(this.props, item.refpk);
	setEdit(this.props);
}

// 删除图标点击事件
export function delClick(item) {
	let { pk } = this.props.config.mapToFields;
	if (this.props.config.nodeType == 'ORG_NODE' && item.dataType == 'group') {
		toast({ content: getMultiLangByID('450101515A-000008') /* 国际化处理：组织节点不能删除集团数据 */, color: 'warning' });
		return;
	}
	if (!beforeDelValidate(this.props, item)) {
		return;
	}
	modifyBeforeAjax(this.props, item);
	let sendData = JSON.parse(JSON.stringify(item.values));
	let modalConfig = {
		title: getMultiLangByID(MsgConst[Type.Delete].title),
		color: 'warning',
		content: getMultiLangByID(this.props.config.delcontent) || getMultiLangByID(MsgConst[Type.Delete].content),
		closeByClickBackDrop: false, //点击遮罩不关闭提示框
		//点击确定按钮事件
		beSureBtnClick: () => {
			ajax({
				url: this.props.config.url.del,
				data: {
					pageid: this.props.config.pageId,
					model: {
						areacode: this.props.config.areaCode,
						areaType: 'form',
						rows: [ { values: sendData } ]
					}
				},
				success: (res) => {
					let { success } = res;
					if (success) {
						this.props.syncTree.delNodeSuceess(this.props.config.treeId, item.values[pk].value);
						this.props.form.EmptyAllFormValue(this.props.config.formId);
						this.props.form.setFormItemsDisabled(this.formId, {
							[this.props.config.mapToFields.enable]: true
						});
						showMessage.call(this, this.props, { type: MsgConst.Type.DeleteSuccess });
					}
				}
			});
		}
	};
	promptBox(modalConfig);
}

function saveClick(props) {
	let formId = props.config.formId;
	if (!props.form.isCheckNow(formId)) {
		return;
	}
	let { pk, pid } = props.config.mapToFields;
	let formData = props.form.getAllFormValue(formId);
	let actionType; // 确定回调动作
	let urlType; //确定url
	let formValues = formData.rows[0].values;
	let oldNode = props.syncTree.getSyncTreeValue(props.config.treeId, formValues[pk].value);
	if (formValues.hasOwnProperty(pk) && formValues[pk].value != '' && formValues[pk].value != null) {
		if (oldNode.pid == formValues[pid].value) {
			actionType = 'update';
		} else {
			actionType = 'updatepid';
		}
		urlType = 'update';
	} else {
		actionType = 'add';
		urlType = 'add';
	}
	modifyBeforeAjax(props, formData.rows[0]);
	let data = {
		pageid: props.config.pageId,
		model: {
			areacode: props.config.areaCode,
			areaType: 'form',
			rows: [ formData.rows[0] ]
		}
	};
	// 保存前执行校验公式
	props.validateToSave(data, () => {
		ajax({
			url: props.config.url[urlType],
			data,
			success: (res) => {
				let { data } = res;
				let treeData = form2tree(props, data[formId].rows[0]);
				if (actionType == 'updatepid') {
					if (oldNode.hasOwnProperty('children')) {
						treeData['children'] = JSON.parse(JSON.stringify(oldNode.children));
					}
					props.syncTree.delNodeSuceess(props.config.treeId, treeData.refpk);
					props.syncTree.addNodeSuccess(props.config.treeId, treeData);
				} else if (actionType == 'update') {
					props.syncTree.editNodeSuccess(props.config.treeId, treeData);
				} else if (actionType == 'add') {
					//展开父节点
					props.syncTree.openNodeByPk(props.config.treeId, treeData.pid);
					//选中新增节点
					props.syncTree.setNodeSelected(props.config.treeId, treeData.refpk);
					props.syncTree.addNodeSuccess(props.config.treeId, treeData);
				}
				let formdata = { [formId]: data[formId] };
				props.form.setAllFormValue(formdata);
				changeEnableTips.call(this, props);
				props.syncTree.setNodeSelected(props.config.treeId, data[formId].rows[0].values[pk].value);
				setBrowse(props);
				showMessage.call(this, props, { type: MsgConst.Type.SaveSuccess });
			}
		});
	});
}

function cancelClick(props) {
	showConfirm(props, {
		type: Type.Cancel,
		beSureBtnClick: () => {
			_cancel.call(this, props);
		}
	});
}

function _cancel(props) {
	let { pk } = props.config.mapToFields;
	let formId = props.config.formId;
	let id = props.form.getFormItemsValue(formId, pk);
	if (!id || id.value == '' || id.value == null) {
		props.form.EmptyAllFormValue(props.config.formId);
	} else {
		props.form.cancel(formId);
	}
	setBrowse(props);
}

function statusOperation(props, operation) {
	let formData = props.form.getAllFormValue(props.config.formId);
	let item = formData.rows[0];
	let multiLangID = '';
	if (operation == 'enable') {
		multiLangID = '450101515A-000020'; /* 国际化处理：组织节点不能启用集团数据 */
	} else if (operation == 'disable') {
		multiLangID = '450101515A-000021'; /* 国际化处理：组织节点不能停用集团数据 */
	}
	// 组织节点不能停启用集团节点
	if (props.config.nodeType == 'ORG_NODE' && isGroupData(item)) {
		toast({ content: getMultiLangByID(multiLangID), color: 'warning' });
		return;
	}
	modifyBeforeAjax(props, item);
	operationAjax.call(this, props, operation, formData);
}

function operationAjax(props, operation, formData) {
	ajax({
		url: props.config.url[operation],
		data: {
			pageid: props.config.pageId,
			model: {
				areacode: props.config.areaCode,
				areaType: 'form',
				rows: [ formData.rows[0] ]
			}
		},
		success: (res) => {
			let { data, success } = res;
			if (success) {
				let formId = props.config.formId;
				data[formId].rows.map((row) => {
					let treeData = form2tree(props, row);
					props.syncTree.editNodeSuccess(props.config.treeId, treeData);
				});
				let formdata = { [formId]: data[formId] };
				props.form.setAllFormValue(formdata);
				changeEnableTips.call(this, props);
				removeChildLoaded(props, formData.rows[0].values[props.config.mapToFields.pk].value);
				if (operation == 'disable') {
					showMessage.call(this, this.props, { type: MsgConst.Type.DisableSuccess });
				} else {
					showMessage.call(this, this.props, { type: MsgConst.Type.EnableSuccess });
				}
			}
		}
	});
}

//校验子节点启用状态
export function checkChildrenStatus(syncTreeNode, validateRes) {
	// 遍历所有节点，如果节点有子树，递归地对子树进行校验
	if (syncTreeNode.children != null) {
		syncTreeNode.children.map((item) => {
			if (item.hasOwnProperty('children')) {
				checkChildrenStatus(item, validateRes);
			}
			if (item.values.enablestate.value != 3) {
				if (item.values.enablestate.value || item.values.enablestate.value == 2) {
					validateRes.valid = false;
					validateRes.msg = getMultiLangByID('450101515A-000012') /* 国际化处理：该档案存在已启用的下级，是否将所有下级同时停用？ */;
					return;
				}
			}
		});
	}
}

function printClick(props) {
	let config = props.config;
	let pks = getPrintPks(props);
	if (pks == null) {
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		config.url.print, // 后台打印服务url
		{
			filename: config.pageId, // 文件名称
			nodekey: config.nodekey, // 模板节点标识
			oids: pks // 需要打印数据主键
		}
	);
}

function outputClick(props) {
	let config = props.config;
	let pks = getPrintPks(props);
	if (pks == null) {
		return;
	}
	let printData = {
		filename: config.pageId, // 文件名称
		nodekey: config.nodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType: 'output' // 输出类型
	};
	output({
		url: config.url.print,
		data: printData
	});
}

/**
 * 从组装好的树的数据中获取pk
 * @param {树的数据} treeData 
 * @param {所有树节点的pk}} pks 
 */
function fetchPksfromTreeData(treeData, pks) {
	treeData.map((node) => {
		if (node.refpk != '~') {
			pks.push(node.refpk);
		}
		if (node.children) {
			fetchPksfromTreeData(node.children, pks);
		}
	});
	return pks;
}

/**
 * 根据不同节点获取打印功能需要的pk数组
 * @param {*} props 
 * @return {pk组成的数组} 
 */
function getPrintPks(props) {
	let config = props.config;
	let pks = [];
	// 位置节点是单条数据打印，增减方式是所有数据打印
	if (getMultiLangByID(config.vnodeName) != getMultiLangByID('450101515A-000002') /* 国际化处理：增减方式 */) {
		let pk = props.form.getFormItemsValue(config.formId, config.mapToFields.pk);
		if (!pk || !pk.value) {
			showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
			return null;
		}
		pks = [ pk.value ];
	} else {
		let alldata = props.syncTree.getSyncTreeValue(config.treeId);
		fetchPksfromTreeData(alldata, pks);
	}

	return pks;
}

/**
 * 根据pk查询卡片数据，并设置到界面上
 * @param {*} props 
 * @param {*} pk 
 * @param {是否为浏览态，浏览态不需要将停启用的值进行转换} isBrowse
 */
export function queryCard(props, pk, isBrowse = false) {
	let formId = props.config.formId;
	let node = props.syncTree.getSyncTreeValue(props.config.treeId, pk);
	if (node.hasLoaded) {
		let formData = {
			[formId]: {
				rows: [
					{
						values: JSON.parse(JSON.stringify(node.values))
					}
				]
			}
		};
		props.form.setAllFormValue(formData);
		changeEnableTips.call(this, props);
		if (!isBrowse) {
			afterSetFrom.call(this, props);
		} else {
			props.form.setFormItemsDisabled(formId, { [props.config.mapToFields.enable]: false });
		}
	} else {
		ajax({
			url: props.config.url['querycard'],
			data: {
				pagecode: props.config.pageId,
				pk
			},
			success: (res) => {
				let { success, data } = res;
				if (success && data) {
					data[formId].rows.map((item) => {
						modifyAfterAjax(props, item);
					});
					let formData = {
						[formId]: {
							rows: [
								{
									values: data[formId].rows[0].values
								}
							]
						}
					};
					props.form.setAllFormValue(formData);
					changeEnableTips.call(this, props);
					if (!isBrowse) {
						afterSetFrom.call(this, props);
					} else {
						props.form.setFormItemsDisabled(formId, { [props.config.mapToFields.enable]: false });
					}
					setLoaded(props, pk, data[formId].rows[0].values);
				}
			}
		});
	}
}

function afterSetFrom(props) {
	let formId = props.config.formId;
	let { enable } = props.config.mapToFields;
	let data = props.form.getAllFormValue(formId);
	if (data.rows[0].values[enable].value === true) {
		data.rows[0].values[enable].value = '2';
	} else if (data.rows[0].values[enable].value === false) {
		data.rows[0].values[enable].value = '3';
	}
	let formData = {
		[formId]: {
			rows: [
				{
					values: data.rows[0].values
				}
			]
		}
	};
	props.form.setAllFormValue(formData);
}

function changeEnableTips(props) {
	let validateRes = {};
	validateRes.valid = true; //校验是否通过,false是不通过，true是通过
	validateRes.msg = ''; //校验信息
	let { pk, enable } = props.config.mapToFields;
	let formData = props.form.getAllFormValue(props.config.formId);
	//停用校验
	let parent_id = formData.rows[0].values[pk].value;
	if (parent_id) {
		let parentValues = props.syncTree.getSyncTreeValue(props.config.treeId, parent_id == '' ? '~' : parent_id);
		checkChildrenStatus(parentValues, validateRes);
	}
	if (validateRes.valid) {
		props.form.setFormPopConfirmSwitchTips(props.config.formId, enable, [
			getMultiLangByID('msgUtils-000012') /* 国际化处理： 是否确认启用？*/,
			getMultiLangByID('msgUtils-000013') /* 国际化处理： 是否确认停用？*/
		]);
	} else {
		props.form.setFormPopConfirmSwitchTips(props.config.formId, enable, [
			getMultiLangByID('msgUtils-000012') /* 国际化处理： 是否确认启用？*/,
			validateRes.msg
		]);
	}
}

function beforeDelValidate(props, item) {
	let { beforeDelValidate } = props.config;
	if (typeof beforeDelValidate === 'function') {
		return beforeDelValidate(props, item);
	}
	return true;
}

/**
 * 给树的节点增加已加载标识，并把values存入树中
 * @param {*} props 
 * @param {*} refpk 
 * @param {*} values 
 */
export function setLoaded(props, refpk, values) {
	let node = props.syncTree.getSyncTreeValue(props.config.treeId, refpk);
	node.hasLoaded = true;
	node.values = JSON.parse(JSON.stringify(values));
}

/**
 * 将子节点的hasload标志设置为false
 * 用在停启用后更新子节点状态
 * @param {*} props 
 * @param {*} refpk 
 */
export function removeChildLoaded(props, refpk) {
	let node = props.syncTree.getSyncTreeValue(props.config.treeId, refpk);
	if (node.children) {
		node.children.map((item) => {
			item.hasLoaded = false;
		});
	}
}

/**
 * 设置编辑态
 * 
 * @export
 * @param {any} props 
 */
export function setEdit(props) {
	let { enable } = props.config.mapToFields;
	props.button.setButtonsVisible(buttonsLayout.edit);
	props.form.setFormStatus(props.config.formId, 'edit');
	props.syncTree.setNodeDisable(props.config.treeId, true);
	props.form.setFormItemsDisabled(props.config.formId, { [enable]: true });
}

/**
 * 设置浏览态
 * 
 * @export
 * @param {any} props 
 */
export function setBrowse(props) {
	let { enable } = props.config.mapToFields;
	props.button.setButtonsVisible(buttonsLayout.default);
	props.form.setFormStatus(props.config.formId, 'browse');
	props.syncTree.setNodeDisable(props.config.treeId, false);
	let formData = props.form.getAllFormValue(props.config.formId);
	let item = formData.rows[0];
	if (props.config.nodeType == 'ORG_NODE' && isGroupData(item)) {
		// 组织节点的集团数据停启用禁用
		props.form.setFormItemsDisabled(props.config.formId, { [enable]: true });
	} else {
		props.form.setFormItemsDisabled(props.config.formId, { [enable]: false });
	}
}

/**
 * 在发送网络请求之前对数据的预处理
 * 
 * @export
 * @param {any} props 
 * @param {any} item 
 */
export function modifyBeforeAjax(props, item) {
	let { pid, enable } = props.config.mapToFields;
	// 空值在后端转换时会报错
	for (let key of Object.keys(item.values)) {
		if (item.values[key].value == '') {
			item.values[key].value = null;
		}
	}
	// 有些操作当pid有值时会认为有父节点会去校验，实现中~表示没有父节点
	if (item.values[pid].value == '~') {
		item.values[pid].value = null;
	}
	// switch值的转换，严格等于true才进行转换
	if (item.values[enable].value === true) {
		item.values[enable].value = '2';
	} else if (item.values[enable].value === false) {
		item.values[enable].value = '3';
	}
}

/**
 * 在发送网络请求之后对数据的处理
 * 
 * @export
 * @param {any} props 
 * @param {any} item 
 */
export function modifyAfterAjax(props, item) {
	let { enable } = props.config.mapToFields;
	// display有值的时候switch会是开启状态
	delete item.values[enable].display;
	if (item.values[enable].value == '2') {
		item.values[enable].value = true;
	} else {
		item.values[enable].value = false;
	}
}

/**
 * 处理接收之后的数据并将其转换为树结构需要的格式
 * 
 * @export
 * @param {any} props 
 * @param {any} data 
 * @returns 
 */
export function form2tree(props, row) {
	let { pk, pid, code, name } = props.config.mapToFields;
	let index = getContext(loginContextKeys.languageIndex);
	modifyAfterAjax(props, row);
	let values = row.values;
	if (values.dr) {
		delete values.dr;
	}
	let refNameVal = values[name + index].value;
	if (!refNameVal) {
		refNameVal = values[name].value;
	}
	let treeData = {
		pid: values[pid].value ? values[pid].value : '~',
		refname: values[code].value + ' ' + refNameVal,
		refpk: values[pk].value,
		id: values[pk].value,
		isLeaf: false, // 新增节点肯定是叶子
		iconBox: {},
		values: JSON.parse(JSON.stringify(values))
	};
	return treeData;
}

/**
 * 停启用失败后，回滚停启用的值
 * @param {*} props 
 */
function rollbackEnableData(props) {
	let { enable } = props.config.mapToFields;
	let formId = props.config.formId;
	let val = props.form.getFormItemsValue(formId, enable);
	if (val.value === true) {
		props.form.setFormItemsValue(formId, { [enable]: { value: false } });
	} else if (val.value === false) {
		props.form.setFormItemsValue(formId, { [enable]: { value: true } });
	}
}

/**
 * 判断一条数据是否为集团数据
 *
 * @param {*} item formData.rows[0]
 * @returns
 */
function isGroupData(item) {
	return (
		item &&
		item.values &&
		item.values.pk_group &&
		item.values.pk_org &&
		item.values.pk_group.value == item.values.pk_org.value
	);
}
