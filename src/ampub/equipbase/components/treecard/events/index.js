import initTemplate from './initTemplate';
import {
	dispatcher,
	buttonClicks,
	addClick,
	editClick,
	delClick,
	modifyAfterAjax,
	queryCard,
	setBrowse
} from './buttonClick';
import handleAfterEditEvent from './afterEvent';

export {
	initTemplate,
	dispatcher,
	buttonClicks,
	handleAfterEditEvent,
	addClick,
	editClick,
	delClick,
	modifyAfterAjax,
	queryCard,
	setBrowse
};
