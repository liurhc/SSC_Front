import React from 'react';
import ReactDOM from 'react-dom';
import { createPage } from 'nc-lightapp-front';
import CategoryFaBase from '../list-base';
// import { initMultiLangByModule } from '../../../../ampub/common/utils/multiLangUtils';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { initMultiLangByModule } = multiLangUtils;

// 页面配置
const pageConfig = {
	// 小应用主键
	appid: '0001Z910000000000LO9',
	appcode: '450102008A',
	// 节点类型
	nodeType: 'group',
	title: '450102008A-000000' /* 国际化处理： 资产类别与固定资产类别对照-集团*/,
	// 表格区域id
	tableId: 'list_head',
	// 表格节点编码
	pagecode: '450102008A_list',

	// 编辑态按钮
	editBtns: [ 'Add', 'Delete', 'Save', 'Cancel' ],
	// 浏览态按钮
	browseBtns: [ 'Add', 'Edit', 'Delete', 'Print', 'Output', 'Refresh' ],

	// 打印模板所在NC节点及标识
	printFuncode: '450102008A',
	printNodekey: null,
	printFilename: '450102008A-000000' /* 国际化处理： 资产类别与固定资产类别对照-集团*/,
	// 所有按钮颜色
	btnColors: {
		add: 'main-button',
		edit: 'secondary-button',
		delete: 'secondary-button',
		save: 'main-button',
		cancel: 'secondary-button'
	}
};

const CategoryFaGrp = createPage({
	billinfo: {
		billtype: 'grid',
		pagecode: pageConfig.pagecode,
		bodycode: pageConfig.tableId
	}
})(CategoryFaBase);

initMultiLangByModule({ ampub: [ '450102008A', 'common' ] }, () => {
	// 显示页面
	ReactDOM.render(<CategoryFaGrp pageConfig={pageConfig} />, document.querySelector('#app'));
});
