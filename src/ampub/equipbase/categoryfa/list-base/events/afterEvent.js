import { addRow } from './buttonClick';
/**
 * 编辑后事件
 * @param {*} props 
 * @param {*} id 
 * @param {*} key 
 * @param {*} value 
 * @param {*} data 
 * @param {*} index 
 */
export default function(props, id, key, value, data, index, record, type, eventType) {
	let tableId = props.pageConfig.tableId;
	if (eventType == 'blur' || eventType == 'change') {
		// 平台的自动增行不会附默认值，这里调用自己的增行
		let num = props.editTable.getNumberOfRows(tableId);
		if (num == index + 1) {
			addRow.call(this, props, tableId, false);
		}
	}
	switch (key) {
		case 'pk_fa_category':
			//资产类别编码（固定资产）
			if (value && value.refcode) {
				props.editTable.setValByKeyAndIndex(tableId, index, 'pk_fa_category.cate_name', {
					value: value.refname,
					display: value.refname
				});
			} else {
				props.editTable.setValByKeyAndIndex(tableId, index, 'pk_fa_category.cate_name', {
					value: '',
					display: ''
				});
			}
			break;

		case 'pk_aim_category':
			//设备类别编码（资产）
			if (value && value.values) {
				props.editTable.setValByKeyAndIndex(tableId, index, 'pk_aim_category.category_name', {
					value: value.refname,
					display: value.refname
				});
			} else {
				props.editTable.setValByKeyAndIndex(tableId, index, 'pk_aim_category.category_name', {
					value: '',
					display: ''
				});
			}
			break;
		default:
			break;
	}
}
