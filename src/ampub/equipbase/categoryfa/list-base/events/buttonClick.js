import { toast, ajax, base, print, output } from 'nc-lightapp-front';

import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils, msgUtils, tableUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { modifyBeforeAjax, getGroupRowIndexs } = tableUtils;

/**
 * 按钮点击事件
 * @param {*} props 
 * @param {*} id 
 */
export default function buttonClick(props, id) {
	let mainorg = this.props.pageConfig.mainorg || {};
	let nodeType = this.props.pageConfig.nodeType;
	if (nodeType == 'org' && !mainorg.refpk && [ 'Add', 'Edit' ].includes(id)) {
		toast({ content: getMultiLangByID('450102008A-000003') /* 国际化处理：请先选择财务组织 */, color: 'warning' });
		return;
	}
	let tableId = this.props.pageConfig.tableId;
	switch (id) {
		case 'Add':
			addRow.call(this, this.props, tableId);
			break;
		case 'Edit':
			edit.call(this, this.props, tableId);
			break;
		case 'Delete':
			batchDelRow.call(this, this.props, tableId);
			break;
		case 'Save':
			save.call(this, this.props, tableId);
			break;
		case 'Cancel':
			cancelConfirm.call(this, props, tableId);
			// cancel.call(this, this.props, tableId);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'Refresh':
			refresh.call(this, props);
			break;
		default:
			break;
	}
}

/**
* 刷新
* @param {*} props 
*/
function refresh(props) {
	this.getData(this.state.mainorg.refpk, true);
}

/**
 * 打印
 * @param {*} props 
 */
export function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		/* 国际化处理： 请选择需要打印的数据*/
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		'/nccloud/ampub/categoryfa/printList.do', // 后台打印服务url
		printData
	);
}

/**
 * 输出
 * @param {*} props 
 */
export function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });
		/* 国际化处理： 请选择需要输出的数据*/
		return;
	}

	output({
		url: '/nccloud/ampub/categoryfa/printList.do',
		data: printData
	});
}

/* 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let { pageConfig } = props;
	let { tableId, printFilename, printNodekey } = pageConfig;
	let checkedRows = props.editTable.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length == 0) {
		return false;
	}
	let pks = [];
	checkedRows.map((item) => {
		pks.push(item.data.values['pk_aim_fa'].value);
	});
	let printData = {
		filename: getMultiLangByID(printFilename), // 文件名称
		nodekey: printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

/**
 * 设置界面状态
 * @param {*} props 
 * @param {*} status 
 */
export function setStatus(props, status) {
	let { button, pageConfig } = props;
	let { tableId, editBtns, browseBtns } = pageConfig;
	switch (status) {
		case 'edit':
			props.editTable.setStatus(tableId, 'edit');
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(browseBtns, false);
			props.button.setButtonVisible(editBtns, true);
			// 如果有新增按钮，改为次要按钮
			editBtns.map((item) => {
				if (item == 'Add') {
					props.button.setMainButton(item, false);
				}
				if (item == 'Save') {
					props.button.setMainButton(item, true);
				}
			});
			// 行删除时不需要悬浮框提示
			props.button.setPopContent('Delete', undefined);
			break;
		default:
			props.editTable.setStatus(tableId, 'browse');
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(editBtns, false);
			props.button.setButtonVisible(browseBtns, true);
			// 如果有新增按钮，改为主要按钮
			browseBtns.map((item) => {
				if (item == 'Add') {
					props.button.setMainButton(item, true);
				}
			});
			// 行删除时悬浮框提示
			props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001') /* 国际化处理： 确定要删除吗？*/);
			break;
	}
}

/**
 * 新增
 * @param {*} props 
 */
export function addRow(props, tableId, autoFocus = true) {
	setStatus.call(this, props, 'edit');
	props.editTable.addRow(tableId, undefined, autoFocus);
	let num = props.editTable.getNumberOfRows(tableId);
	let nodeType = props.pageConfig.nodeType;
	if (nodeType === 'org') {
		let mainorg = props.pageConfig.mainorg || {};
		props.editTable.setValByKeyAndIndex(tableId, num - 1, 'pk_org', {
			value: mainorg.refpk,
			display: mainorg.refname
		});

		// 集团数据不可编辑
		let groupRowIndexs = getGroupRowIndexs.call(this, props, tableId);
		if (groupRowIndexs && groupRowIndexs.length > 0) {
			props.editTable.setEditableRowByIndex(tableId, groupRowIndexs, false);
		}
		// 集团数据不可编辑
		// setGroupRowEditable.call(this, props, tableId, false);
	}
}

/**
 * 修改
 * @param {*} props 
 */
export const edit = (props, tableId) => {
	setStatus.call(this, props, 'edit');
	let nodeType = props.pageConfig.nodeType;
	if (nodeType === 'org') {
		// 集团数据不可编辑
		// setGroupRowEditable.call(this, props, tableId, false);
		// 集团数据不可编辑
		let groupRowIndexs = getGroupRowIndexs.call(this, props, tableId);
		if (groupRowIndexs && groupRowIndexs.length > 0) {
			props.editTable.setEditableRowByIndex(tableId, groupRowIndexs, false);
		}
	}
};

/**
 * 保存
 * @param {*} props 
 */
export function save(props, tableId) {
	let nodeType = props.pageConfig.nodeType;
	if (nodeType == 'group') {
		props.editTable.filterEmptyRows(tableId, [ 'numberindex' ]);
	} else {
		props.editTable.filterEmptyRows(tableId, [ 'numberindex', 'pk_org' ]);
	}

	let allRows = props.editTable.getAllRows(tableId, true);
	let flag = props.editTable.checkRequired(tableId, allRows);
	// 必输项校验没过
	if (!flag) {
		setBatchBtnsEnable.call(this, props, tableId);
		return;
	}

	let changeRows = props.editTable.getChangedRows(tableId);
	if (!changeRows || changeRows.length == 0) {
		setStatus.call(this, props, 'browse');
		setBatchBtnsEnable.call(this, props, tableId);
		showMessage.call(this, props, { type: MsgConst.Type.SaveSuccess });
		/* 国际化处理： 保存成功*/
		return;
	}

	let url = '/nccloud/ampub/categoryfa/save.do';
	const data = {
		pageid: props.pageConfig.pagecode,
		model: {
			areaType: 'table',
			pageinfo: null,
			areacode: tableId,
			rows: changeRows
		},
		userjson: '{"nodeType":"' + nodeType + '"}'
	};

	props.validateToSave(data, () => {
		ajax({
			url: url,
			data,
			success: function(res) {
				let { success, data } = res;
				if (success) {
					let retData = data[tableId];
					// 是否差异VO
					let isLightVO = false;
					let userjson = data.userjson;
					if (userjson) {
						let userObj = JSON.parse(userjson);
						if (userObj && userObj['isLightVO'] == 'Y') {
							isLightVO = true;
						}
					}
					if (!isLightVO) {
						let newRows = [];
						let i = 0;
						allRows.map((item, index) => {
							// 0原始1修改2新增3删除
							if (item.status == '1' || item.status == '2') {
								if (retData && retData.rows && retData.rows.length > i) {
									newRows.push(retData.rows[i]);
								}
								i++;
							} else if (item.status == '3') {
								i++;
							} else {
								newRows.push(item);
							}
						});
						let allData = props.editTable.getAllData(tableId);
						allData.rows = newRows;
						props.editTable.setTableData(tableId, allData);
					} else {
						props.editTable.updateDifference(tableId, retData);
					}

					setStatus.call(this, props, 'browse');
					setBatchBtnsEnable.call(this, props, tableId);
					showMessage.call(this, props, { type: MsgConst.Type.SaveSuccess });
					/* 国际化处理： 保存成功*/
				}
			},
			error: function(res) {
				setBatchBtnsEnable.call(this, props, tableId);
				toast({ color: 'danger', content: res.message });
			}
		});
	});
}

/**
* 取消
* @param {*} props 
*/
function cancelConfirm(props, tableId) {
	let cancelClick = () => {
		cancel.call(this, props, tableId);
	};
	showConfirm.call(this, props, {
		type: MsgConst.Type.Cancel,
		beSureBtnClick: cancelClick
	});
}

/**
 * 取消
 * @param {*} props 
 */
export function cancel(props, tableId) {
	props.editTable.cancelEdit(tableId);
	setStatus.call(this, props, 'browse');
	setBatchBtnsEnable.call(this, props, tableId);
}

/**
 * 前台删除
 * @param {*} props 
 * @param {*} tableId 
 * @param {*} index 
 */
export function delRow(props, tableId, index) {
	props.editTable.deleteTableRowsByIndex(tableId, index);
	setBatchBtnsEnable.call(this, props, tableId);
}

/**
 * 后台删除
 * @param {*} props 
 */
export function batchDel(props, tableId) {
	let checkedRows = props.editTable.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length == 0) {
		return;
	}
	let changedIndexs = [];
	let delRows = [];
	// 设置删除状态
	checkedRows.map((item, index) => {
		// 0原始1修改2新增3删除
		item.data.status = '3';
		changedIndexs.push(item.index);
		delRows.push(item.data);
	});
	const data = {
		pageid: props.pageConfig.pagecode,
		model: {
			areaType: 'table',
			areacode: tableId,
			rows: delRows
		}
	};
	ajax({
		url: '/nccloud/ampub/categoryfa/save.do',
		data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				props.editTable.deleteTableRowsByIndex(tableId, changedIndexs);
				setBatchBtnsEnable.call(this, props, tableId);
				showMessage.call(this, props, { type: MsgConst.Type.DeleteSuccess });
			}
		},
		error: (res) => {
			toast({ color: 'danger', content: res.message });
			checkedRows.map((item, index) => {
				// 0原始1修改2新增3删除
				item.data.status = '0';
			});
		}
	});
}

/**
 * 浏览态直接删除单个数据
 * @param {*} props 
 * @param {*} tableId 
 * @param {*} index 
 */
export const singleDel = (props, tableId, index) => {
	let allRows = props.editTable.getAllRows(tableId);
	allRows[index].status = '3';
	modifyBeforeAjax(allRows[index]);
	let changedRows = [ allRows[index] ];
	const data = {
		pageid: props.pageConfig.pagecode,
		model: {
			areaType: 'table',
			areacode: tableId,
			rows: changedRows
		}
	};
	ajax({
		url: '/nccloud/ampub/categoryfa/save.do',
		data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				let allRows = props.editTable.getAllRows(tableId);
				allRows.splice(index, 1);
				let allData = props.editTable.getAllData(tableId);
				allData.rows = allRows;
				props.editTable.setTableData(tableId, allData);
				showMessage.call(this, props, { type: MsgConst.Type.DeleteSuccess });
			}
		},
		error: (res) => {
			toast({ color: 'danger', content: res.message });
		}
	});
};

/**
 * 设置批量按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBatchBtnsEnable(props, moduleId) {
	let checkedRows = props.editTable.getCheckedRows(moduleId);
	const { pageConfig = {} } = props;
	const { batchBtns = [ 'Delete', 'Print', 'Output' ] } = pageConfig;
	props.button.setButtonDisabled(batchBtns, !(checkedRows && checkedRows.length > 0));
}

/**
 * 批量删除
 * @param {*} props 
 */
export function batchDelRow(props, tableId) {
	let checkedRows = props.editTable.getCheckedRows(tableId);
	if (checkedRows.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseDelete });
		return;
	}
	let indexArr = [];
	// 组织节点不允许删除集团数据
	let groupArr = [];
	let nodeType = props.pageConfig.nodeType;
	checkedRows.map((row) => {
		indexArr.push(row.index);

		let recordVal = row.data.values;
		let flag =
			nodeType == 'org' &&
			recordVal.pk_group &&
			recordVal.pk_org &&
			recordVal.pk_group.value == recordVal.pk_org.value;
		if (flag) {
			groupArr.push(row.index);
		}
	});
	if (groupArr.length > 0) {
		toast({ content: getMultiLangByID('450102008A-000002') /* 国际化处理：组织节点不能删除集团数据 */, color: 'warning' });
		return;
	}
	let status = props.editTable.getStatus(tableId);
	if (status == undefined || status == 'browse') {
		let batch = () => {
			batchDel.call(this, props, tableId, indexArr);
		};
		showConfirm.call(this, props, { type: MsgConst.Type.DelSelect, beSureBtnClick: batch });
	} else {
		delRow.call(this, props, tableId, indexArr);
	}
}
