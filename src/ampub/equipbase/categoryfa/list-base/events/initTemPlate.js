import { ajax, base, toast } from 'nc-lightapp-front';
const { NCPopconfirm } = base;
import { setStatus, setBatchBtnsEnable } from './buttonClick';
// import { getMultiLangByID } from '../../../../../ampub/common/utils/multiLangUtils';
// import { MsgConst, showMessage } from '../../../../../ampub/common/utils/msgUtils';
// import {
// 	loginContext,
// 	loginContextKeys,
// 	getContext
// } from '../../../../../ampub/common/components/AMInitInfo/loginContext';
import ampub from 'ampub';
const { utils, components } = ampub;
const { multiLangUtils, msgUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { MsgConst, showMessage } = msgUtils;
const { LoginContext } = components;
const { loginContext, loginContextKeys, getContext } = LoginContext;

export default function(props) {
	let pageConfig = props.pageConfig;
	let appcode = pageConfig.appcode;
	let appid = pageConfig.appid;
	let tableId = pageConfig.tableId;
	let pagecode = pageConfig.pagecode;
	let _this = this;
	props.createUIDom(
		{
			//页面id
			pagecode
		},
		function(data) {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button);
					props.button.setButtonVisible([ 'Cancel', 'Save' ], false);
				}
				if (data.template) {
					let meta = data.template;
					meta = filter(props, meta, tableId);
					modifierMeta.call(this, props, meta, tableId);
					props.meta.setMeta(meta, () => {
						afterInit.call(_this, props);
					});
				}
				setStatus.call(this, props, 'browse');
				setBatchBtnsEnable.call(this, props, tableId);
			}
		}
	);
}

/**
 * 模板初始化以后
 * @param {*} props 
 */
function afterInit(props) {
	let nodeType = props.pageConfig.nodeType;
	if (nodeType == 'org') {
		// 默认主组织
		let pk_org = getContext(loginContextKeys.pk_org);
		let org_Name = getContext(loginContextKeys.org_Name);
		if (pk_org) {
			this.mainorgChange({ refpk: pk_org, refname: org_Name });
		} else {
			this.getData();
		}
	} else {
		this.getData();
	}
}

//模板处理
function filter(props, meta, tableId) {
	meta[tableId].items.map((item) => {
		if (item['attrcode'] === 'pk_fa_category') {
			//只可选末级节点
			item.onlyLeafCanSelect = true;
		}
	});
	return meta;
}

function modifierMeta(props, meta, tableId) {
	let _props = props;
	meta[tableId].status = 'browse';
	// 添加表格操作列
	let oprCol = {
		label: getMultiLangByID('amcommon-000000') /* 国际化处理： 操作*/,
		visible: true,
		attrcode: 'opr',
		itemtype: 'customer',
		className: 'table-opr',
		// 锁定在右边
		fixed: 'right',
		width: '110px',
		render(text, record, index) {
			let recordVal = record.values;
			let status = props.editTable.getStatus(tableId);
			let nodeType = props.pageConfig.nodeType;
			// 组织节点不允许删除集团数据
			let flag =
				nodeType == 'org' &&
				recordVal.pk_group &&
				recordVal.pk_org &&
				recordVal.pk_group.value == recordVal.pk_org.value;
			let buttonAry = [];
			if (!flag) {
				buttonAry = [ 'Delete' ];
			}
			return props.button.createOprationButton(buttonAry, {
				area: 'list_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => tableButtonClick.call(this, _props, key, text, record, index)
			});
		}
	};
	meta[tableId].items.push(oprCol);

	return meta;
}

function tableButtonClick(props, key, text, record, index) {
	switch (key) {
		// 表格操作按钮
		case 'Delete':
			delRow.call(this, props, index);
			break;
		default:
			break;
	}
}

/**
 * 浏览态直接删除数据
 * @param {*} props 
 * @param {*} tableId 
 * @param {*} index 
 */
function delRow(props, index) {
	let { pageConfig = {} } = props;
	let { tableId, pagecode } = pageConfig;
	let status = props.editTable.getStatus(tableId);
	if (status == undefined || status == 'browse') {
		let allRows = props.editTable.getAllRows(tableId);
		allRows[index].status = '3';
		let changedRows = [ allRows[index] ];
		const data = {
			pageid: pagecode,
			model: {
				areaType: 'table',
				pageinfo: null,
				areacode: tableId,
				rows: changedRows
			}
		};
		ajax({
			url: '/nccloud/ampub/categoryfa/save.do',
			data,
			success: function(res) {
				let { success, data } = res;
				if (success) {
					let allData = props.editTable.getAllData(tableId);
					allData.rows.splice(index, 1);
					props.editTable.setTableData(tableId, allData);
					setBatchBtnsEnable.call(this, props, tableId);
					showMessage.call(this, props, { type: MsgConst.Type.DeleteSuccess });
				}
			}
		});
	} else {
		props.editTable.deleteTableRowsByIndex(tableId, index);
		setBatchBtnsEnable.call(this, props, tableId);
	}
}
