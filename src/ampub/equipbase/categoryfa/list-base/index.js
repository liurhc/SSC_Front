import React, { Component } from 'react';
import { ajax, createPageIcon } from 'nc-lightapp-front';
import { buttonClick, initTemplate, afterEvent, rowSelected, setBatchBtnsEnable } from './events';
// import { MsgConst, showMessage } from '../../../../ampub/common/utils/msgUtils';
// import { getMultiLangByID } from '../../../../ampub/common/utils/multiLangUtils';
// import closeBrowserUtils from '../../../../ampub/common/utils/closeBrowserUtils';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils, msgUtils, closeBrowserUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { MsgConst, showMessage } = msgUtils;

import './index.less';
// 财务组织(所有集团)(所有)
import AMRefer from '../../../common/components/AMRefer';
class CategoryFaBase extends Component {
	constructor(props) {
		super(props);
		this.state = {
			mainorg: {}
		};
		closeBrowserUtils(props, { editTable: [ props.pageConfig.tableId ] });
		initTemplate.call(this, props);
	}
	componentDidMount() {
		this.getData();
	}

	changeButton = (props) => {
		let tableId = props.pageConfig.tableId;
		let status = props.editTable.getStatus(tableId);
		props.button.setButtonVisible([ 'Cancel', 'Save' ], false);
		// 根据编辑状态显示按钮
		if (status === 'edit') {
			props.button.setButtonVisible([ 'Add', 'Delete', 'Cancel', 'Save' ], true);
			props.button.setButtonVisible([ 'Edit' ], false);
		} else {
			props.button.setButtonVisible([ 'Add', 'Delete', 'Edit' ], true);
			props.button.setButtonVisible([ 'Cancel', 'Save' ], false);
		}
	};

	getData = (pk_org = this.state.mainorg.refpk, isRefresh = false) => {
		let pageConfig = this.props.pageConfig || {};
		let nodeType = pageConfig.nodeType;
		let tableId = pageConfig.tableId;
		let pagecode = pageConfig.pagecode;
		ajax({
			url: '/nccloud/ampub/categoryfa/query.do',
			data: {
				nodeType: nodeType,
				pk_org: pk_org,
				nodeCode: pagecode,
				pagecode: pagecode
			},
			success: (res) => {
				let { success, data } = res;
				if (success && data && data[tableId] && data[tableId].rows) {
					this.props.editTable.setTableData(tableId, data[tableId]);
					if (nodeType === 'org') {
						let { mainorg = {} } = this.state;
						this.props.button.setButtonDisabled([ 'Add', 'Edit' ], !mainorg.refpk);
					}
					if (isRefresh) {
						showMessage.call(this, this.props, { type: MsgConst.Type.RefreshSuccess });
					}
				} else if (success && isRefresh) {
					// 刷新操作，后台请求无数据时成功提示
					showMessage.call(this, this.props, { type: MsgConst.Type.RefreshSuccess });
				}
				setBatchBtnsEnable.call(this, this.props, tableId);
			}
		});
	};

	// 主组织切换
	mainorgChange = (value = {}) => {
		// 部分代码只能取到props，取不到state，这里把主组织放到props中
		this.props.pageConfig.mainorg = value;
		this.setState({ mainorg: value }, () => {
			// 根据组织查询数据
			this.getData(value.refpk);
		});
	};

	render() {
		const { editTable, button, ncmodal, pageConfig = {} } = this.props;
		const { createEditTable } = editTable;
		const { createButtonApp } = button;
		const { mainorg } = this.state;
		let { createModal } = ncmodal;
		let { title, tableId, nodeType, orgRefCode } = pageConfig;

		let status = editTable.getStatus(tableId);
		// 根据编辑状态显示按钮
		let printUrl = '/nccloud/ampub/categoryfa/printList.do';
		this.props.pageConfig.mainorg = mainorg;

		let mainorgobj = {
			defaultValue: mainorg,
			disabled: status == 'edit',
			onChange: (value) => {
				this.mainorgChange(value);
			},
			refcode: orgRefCode,
			queryCondition: () => {
				return { TreeRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter' };
			}
		};

		return (
			<div className="nc-single-table">
				{/* 头部 header */}
				<div className="nc-singleTable-header-area">
					{/* 标题 title */}
					<div className="header-title-search-area">
						{createPageIcon()}
						<h2 className="title-search-detail">{getMultiLangByID(title)}</h2>
						{/* 主组织 main-org */}
						{nodeType == 'org' && <AMRefer className="ref title-search-detail" config={mainorgobj} />}

						{/* 显示停用 showOff */}
						<span className="showOff" />
						{/* 按钮区 btn-group */}
					</div>
					<div className="header-button-area">
						{createButtonApp({
							area: 'list_head',
							buttonLimit: 3,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>

				{/* 列表区 table-area */}
				<div className="nc-singleTable-table-area">
					{createEditTable(tableId, {
						onAfterEvent: afterEvent,
						showCheck: true,
						showIndex: true,
						onSelected: rowSelected.bind(this),
						onSelectedAll: rowSelected.bind(this)
					})}
				</div>
				{/* 弹框 */}
				{createModal(`${tableId}-del-confirm`, { color: 'warning' })}
			</div>
		);
	}
}

export default CategoryFaBase;
