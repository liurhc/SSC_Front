import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, base, createPageIcon } from 'nc-lightapp-front';
import { buttonClick, initTemplate, afterEvent, rowSelected, getData } from './events';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils, closeBrowserUtils } = utils;
const { getMultiLangByID, initMultiLangByModule } = multiLangUtils;
import { pageConfig } from './const';

const { tableId, filterFields, pagecode, title } = pageConfig;
const { NCFormControl, NCCheckbox } = base;

class CapitalSource extends Component {
	constructor(props) {
		super(props);
		closeBrowserUtils(props, { editTable: [ tableId ] });
		initTemplate.call(this, props);
		this.props = props;
		this.state = {
			keyWord: '',
			isShowOff: false // 是否显示停用
		};
	}

	// 搜索关键词变化
	searchOnChange = (value) => {
		this.setState({ keyWord: value }, () => {
			// 调用平台api过滤显示数据
			let { editTable } = this.props;
			editTable.setFiltrateTableData(tableId, filterFields, value);
		});
	};

	// 切换显示停用
	showOffChange = (value) => {
		this.setState({ isShowOff: value }, () => {
			getData.call(this, this.props, value);
		});
	};

	render() {
		let { editTable, button } = this.props;
		let { createEditTable } = editTable;
		let { createButtonApp } = button;
		let status = this.props.editTable.getStatus(tableId);
		return (
			<div className="nc-single-table">
				{/* 头部 */}
				<div className="nc-singleTable-header-area">
					{/* 标题 */}
					<div className="header-title-search-area">
						{createPageIcon()}
						<h2 className="title-search-detail">{getMultiLangByID(title) /* 国际化处理： 资金来源*/}</h2>
						{/* 简单搜索 */}
						<div className="title-search-detail">
							<NCFormControl
								placeholder={getMultiLangByID('amcommon-000003') /* 国际化处理： 请输入编码或名称*/}
								value={this.state.keyWord}
								onChange={this.searchOnChange}
								type="search"
								className={'search-box disabled'}
								disabled={status == 'edit'}
							/>
						</div>
						{/* 显示停用  showOff*/}
						<div className="title-search-detail">
							<span className="showOff">
								<NCCheckbox
									checked={this.state.isShowOff}
									onChange={this.showOffChange}
									disabled={status == 'edit'}
								>
									{getMultiLangByID('amcommon-000004') /* 国际化处理： 显示停用*/}
								</NCCheckbox>
							</span>
						</div>
					</div>
					{/* 按钮区 */}
					<div className="header-button-area">
						{createButtonApp({
							area: 'list_head',
							buttonLimit: 4,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>

				{/* 列表区 */}
				<div className="nc-singleTable-table-area">
					{createEditTable(tableId, {
						showCheck: true,
						showIndex: true,
						onAfterEvent: afterEvent.bind(this),
						selectedChange: rowSelected.bind(this)
					})}
				</div>
			</div>
		);
	}
}

CapitalSource = createPage({
	billinfo: {
		billtype: 'grid',
		pagecode: pagecode,
		bodycode: tableId
	}
})(CapitalSource);

let moduleIds = { ampub: [ '450101012A', 'common' ] };
initMultiLangByModule(moduleIds, () => {
	// 显示页面
	ReactDOM.render(<CapitalSource />, document.querySelector('#app'));
});
