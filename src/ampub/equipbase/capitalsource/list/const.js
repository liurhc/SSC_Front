export const pageConfig = {
	// 表体区域ID
	tableId: 'list_head',
	// 应用类型
	nodeType: 'group',
	// 应用编码
	appcode: '450101012A',
	// 页面编码
	pagecode: '450101012A_list',
	// 主表主键字段
	pkField: 'pk_capital_source',
	// 标题
	title: '450101012A-000000' /*国际化处理 资金来源 */,
	// 过滤数据字段
	filterFields: [ 'source_code', 'source_name' ],
	// 请求链接
	url: {
		saveUrl: '/nccloud/ampub/capitalsource/save.do',
		deleteUrl: '/nccloud/ampub/capitalsource/delete.do',
		unSealUrl: '/nccloud/ampub/capitalsource/enable.do',
		sealUrl: '/nccloud/ampub/capitalsource/disable.do',
		printUrl: '/nccloud/ampub/capitalsource/print.do',
		queryUrl: '/nccloud/ampub/capitalsource/query.do'
	},
	// 保存过滤空行校验无效字段
	exceptFields: [ 'enablestate' ],
	// 编辑态按钮
	editBtns: [ 'Add', 'Delete', 'Save', 'Cancel' ],
	// 浏览态按钮
	browseBtns: [ 'Add', 'Edit', 'Delete', 'Print', 'Output', 'Refresh' ],
	// 输出文件名称
	printFilename: '450101012A-000000' /*国际化处理 资金来源 */,
	// 打印模板节点标识
	printNodekey: null
};
