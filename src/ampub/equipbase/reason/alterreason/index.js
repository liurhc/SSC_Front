/*
* 变动原因入口
* created by cuixuh
* */
import ReactDOM from 'react-dom';
import { createPage } from 'nc-lightapp-front';
import Reason, { initTemplate } from '../basereason';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { initMultiLangByModule } = multiLangUtils;
//页面配置
const pageConfig = {
	nodecode: '4501008005', //变动原因的功能编码
	reasonType: '3', //原因类型——//减值：1，减少：2，变动：3，封存：4
	reasonTypeCN: '450101508A-000000' /* 国际化处理： 变动原因*/,
	pagecode: '450101508A_list',
	appcode: '450101508A',
	printNodekey: null,
	printFilename: '450101508A-000000' /* 国际化处理： 变动原因*/
};

const AlterReason = createPage({
	billinfo: {
		billtype: 'grid',
		pagecode: '450101508A_list',
		headcode: 'list_head'
	}
})(Reason);

let moduleIds = { ampub: [ '450101508A', 'common' ] };
initMultiLangByModule(moduleIds, () => {
	// 显示页面
	ReactDOM.render(<AlterReason pageConfig={pageConfig} />, document.querySelector('#app'));
});
