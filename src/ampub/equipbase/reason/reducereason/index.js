/*
* 减少原因入口
* created by cuixuh
* */
import ReactDOM from 'react-dom';
import { createPage } from 'nc-lightapp-front';
import Reason, { initTemplate } from '../basereason';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { initMultiLangByModule } = multiLangUtils;
//页面配置
const pageConfig = {
	nodecode: '4501008010', //减少原因的功能编码
	reasonType: '2', //原因类型——//减值：1，减少：2，变动：3，封存：4
	reasonTypeCN: '450101508A-000001' /* 国际化处理： 减少原因*/,
	pagecode: '450101512A_list',
	appcode: '450101512A',
	printNodekey: null,
	printFilename: '450101508A-000001' /* 国际化处理： 减少原因*/
};

const ReduceReason = createPage({
	billinfo: {
		billtype: 'grid',
		pagecode: '450101512A_list',
		headcode: 'list_head'
	}
})(Reason);

let moduleIds = { ampub: [ '450101508A', 'common' ] };
initMultiLangByModule(moduleIds, () => {
	// 显示页面
	ReactDOM.render(<ReduceReason pageConfig={pageConfig} />, document.querySelector('#app'));
});
