import { pageConfig } from '../constants';
import { delRow, singleDel } from './buttonClick';
const { tableId } = pageConfig;
import ampub from 'ampub';
const { commonConst } = ampub;
const { StatusUtils } = commonConst;
const { UISTATE } = StatusUtils;

/**
 * 表格操作按钮
 * @param {*} props 
 * @param {*} key 
 * @param {*} text 
 * @param {*} record 
 * @param {*} index 
 * @param {*} tableId 
 */
export default function(props, key, text, record, index, tableId) {
	switch (key) {
		case 'Delete':
			// 删除行
			delLine.call(this, props, record, index);
			break;
		default:
			break;
	}
}

/**
 * 表格行删除
 * @param {*} props 
 * @param {*} record 
 * @param {*} index 
 */
function delLine(props, record, index) {
	let status = props.editTable.getStatus(tableId) || UISTATE.browse;
	if (status == UISTATE.browse) {
		index = record.flterIndex;
		singleDel.call(this, props, index);
	} else {
		delRow.call(this, props, index);
	}
}
