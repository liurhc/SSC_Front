import { doAdd } from './buttonClick';

export default function(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	if (eventType == 'blur' && key != 'enablestate') {
		// 平台的自动增行不会附默认值，这里调用自己的增行
		let num = props.editTable.getNumberOfRows(moduleId);
		if (num == index + 1) {
			doAdd.call(this, props, true);
		}
	}
}
