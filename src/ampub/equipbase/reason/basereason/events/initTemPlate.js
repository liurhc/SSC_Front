import { enableRow, disableRow, setStatus, getData } from './buttonClick';
import { pageConfig } from '../constants';
import tableButtonClick from './tableButtonClick';
const { tableId } = pageConfig;
import ampub from 'ampub';
const { commonConst, utils } = ampub;
const { StatusUtils } = commonConst;
const { multiLangUtils, tableUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { UISTATE } = StatusUtils;
const { createEnableSwitch } = tableUtils;

export default function(props) {
	props.createUIDom(
		{
			pagecode: props.pageConfig.pagecode //页面id
		},
		(data) => {
			if (data) {
				if (data.button) {
					//设置按钮
					let buttons = data.button;
					props.button.setButtons(buttons);
				}
				if (data.template) {
					//修改模板
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta, () => {
						afterInit.call(this, props);
					});
				}
			}
		}
	);
}
/**
 * 模板初始化以后
 * @param {*} props 
 */
function afterInit(props) {
	let status = props.getUrlParam('status') || UISTATE.browse;
	setStatus.call(this, props, status);
	let data = {
		isShowSeal: this.state.isShowOff ? 'Y' : 'N',
		pagecode: this.props.pageConfig.pagecode,
		areacode: tableId,
		queryCondition: { nodecode: this.props.pageConfig.nodecode }
	};
	getData.call(this, data);
}

function modifierMeta(props, meta) {
	if (meta[tableId] && meta[tableId].items && meta[tableId].items.length > 0) {
		// 处理停启用开关
		createEnableSwitch.call(this, props, { meta, enableRow, disableRow });
		//添加表格操作列
		//let oprCol = createOprationColumn.call(this, props, { tableButtonClick });
		meta[tableId].items.push({
			label: getMultiLangByID('amcommon-000000') /* 国际化处理： 操作*/,
			visible: true,
			attrcode: 'opr',
			itemtype: 'customer',
			// 锁定在右边
			fixed: 'right',
			width: '110px',
			render: (text, record, index) => {
				let recordVal = record.values;
				let status = props.editTable.getStatus(tableId);
				let enableShow = false;
				let disableShow = false;
				let buttonAry = [ 'Delete' ];
				if (status == 'browse') {
					if (recordVal.enablestate.value == '2') {
						enableShow = false;
						disableShow = true;
					} else {
						enableShow = true;
						disableShow = false;
					}
				}
				return props.button.createOprationButton(buttonAry, {
					area: 'list_inner',
					buttonLimit: 3,
					onButtonClick: (props, key) => tableButtonClick.call(this, props, key, text, record, index)
				});
			}
		});
	}

	return meta;
}
