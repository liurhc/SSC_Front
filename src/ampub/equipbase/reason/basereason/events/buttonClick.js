import { ajax, print, output } from 'nc-lightapp-front';
import { pageConfig } from '../constants';
const { BrowseBtns, EditBtns, tableId, AreaType, URL, filterFields } = pageConfig;
import ampub from 'ampub';
const { commonConst, utils } = ampub;
const { StatusUtils } = commonConst;
const { multiLangUtils, msgUtils, tableUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { modifyBeforeAjax, modifyAfterAjax, modifyBeforeAjaxByIndex, modifyAfterAjaxByIndex } = tableUtils;
const { MsgConst, showMessage } = msgUtils;
const { UISTATE, VOSTATUS } = StatusUtils;

export default function buttonClick(props, id) {
	switch (id) {
		case 'Edit':
			edit.call(this, props);
			break;

		case 'Add':
			doAdd.call(this, props);
			break;

		case 'Delete':
			batchDelRow.call(this, props);
			break;

		case 'Save':
			save.call(this, props);
			break;

		case 'Cancel':
			cancelConfirm.call(this, props);
			break;

		case 'Print':
			printTemp.call(this, props);
			break;

		case 'Output':
			outputTemp.call(this, props);
			break;

		case 'Refresh':
			refresh.call(this, props);
			break;

		default:
			break;
	}
}

//修改
function edit(props) {
	setStatus.call(this, props, UISTATE.edit);
	//changeEnablestateValue.call(this, props);
}

//设置table的启用属性为字符串，为了编辑公式后事件
function changeEnablestateValue(props) {
	let allDatas = props.editTable.getAllRows(tableId, true);
	allDatas.map((item) => {
		//修改 enablestate 字段
		if (item.values.enablestate.value) {
			item.values.enablestate.value = '2';
		} else {
			item.values.enablestate.value = '3';
		}
	});
	let data = {
		rows: allDatas
	};
	props.editTable.setTableData(tableId, data);
}

//刷新数据
function refresh(props) {
	let data = {
		isShowSeal: this.state.isShowOff ? 'Y' : 'N',
		pagecode: this.props.pageConfig.pagecode,
		areacode: tableId,
		queryCondition: { nodecode: this.props.pageConfig.nodecode }
	};
	getData.call(this, data, true);
}

//输出
function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });
		return;
	}
	output({
		url: URL.printUrl,
		data: printData
	});
}

//打印
function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		URL.printUrl, // 后台打印服务url
		printData
	);
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType) {
	let checkedRows = props.editTable.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length == 0) {
		return false;
	}
	let pks = [];
	checkedRows.map((item) => {
		pks.push(item.data.values.pk_reason.value);
	});
	let printData = {
		filename: getMultiLangByID(props.pageConfig.printFilename) /* 国际化处理： 文件名称*/,
		nodekey: props.pageConfig.printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

//保存
function save(props) {
	//先过滤空行
	props.editTable.filterEmptyRows(tableId, [ 'enablestate', 'reason_type' ]);
	let visibleRows = props.editTable.getVisibleRows(tableId);
	let pass = props.editTable.checkRequired(tableId, visibleRows);
	// 必输项校验没过
	if (!pass) {
		return;
	}
	let changedRows = props.editTable.getChangedRows(tableId);
	// 没有修改行，变更状态
	if (!changedRows || changedRows.length == 0) {
		setStatus.call(this, props, UISTATE.browse);
		showMessage.call(this, props, { type: MsgConst.Type.SaveSuccess });
		return;
	}

	changedRows.map((item) => {
		modifyBeforeAjax(item);
	});

	const data = {
		pageid: props.pageConfig.pagecode,
		model: {
			areaType: AreaType,
			areacode: tableId,
			rows: changedRows
		}
	};
	// 保存前执行验证公式
	props.validateToSave(data, () => {
		ajax({
			url: URL.saveAllUrl,
			data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					let retData = data[tableId];
					let newRows = [];
					let i = 0;
					let allData = props.editTable.getAllData(tableId);
					let allRows = allData.rows;
					allRows.map((item, index) => {
						// 0原始1修改2新增3删除
						if (item.status == VOSTATUS.UPDATED || item.status == VOSTATUS.NEW) {
							if (retData && retData.rows && retData.rows.length > i) {
								modifyAfterAjax(retData.rows[i]);
								newRows.push(retData.rows[i]);
							}
							i++;
						} else if (item.status == VOSTATUS.DELETED) {
							i++;
						} else {
							newRows.push(item);
						}
					});
					allData.rows = newRows;
					setStatus.call(this, props, UISTATE.browse);
					setValue.call(this, props, { [tableId]: allData });
					setBatchBtnsEnable.call(this, props, tableId);
					showMessage.call(this, props, { type: MsgConst.Type.SaveSuccess });
				}
			}
		});
	});
}

//后台批量删除
function batchDel(props) {
	let checkedRows = props.editTable.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length == 0) {
		return;
	}
	// 平台没有克隆数据问题
	checkedRows = JSON.parse(JSON.stringify(checkedRows));
	let delRows = [];
	let checkedIndexs = [];
	// 设置删除状态
	checkedRows.map((item, index) => {
		modifyBeforeAjax(item.data);
		delRows.push(item.data);
		checkedIndexs.push(item.data.flterIndex);
	});
	const data = {
		pageid: props.pageConfig.pagecode,
		model: {
			areaType: AreaType,
			areacode: tableId,
			rows: delRows
		}
	};
	ajax({
		url: URL.deleteUrl,
		data,
		success: (res) => {
			let { success } = res;
			if (success) {
				let allData = props.editTable.getAllData(tableId);
				allData.rows = allData.rows.filter((item, i) => !checkedIndexs.includes(i));
				setValue.call(this, props, { [tableId]: allData });
				showMessage.call(this, props, { type: MsgConst.Type.DeleteSuccess });
			}
		}
	});
}

//前台删除
export function delRow(props, indexs) {
	props.editTable.deleteTableRowsByIndex(tableId, indexs);
}

/**
 * 浏览态直接删除单个数据
 * @param {*} props 
 * @param {*} tableId 
 * @param {*} index 
 */
export function singleDel(props, index) {
	let allRows = props.editTable.getAllRows(tableId);
	modifyBeforeAjax(allRows[index]);
	let changedRows = [ allRows[index] ];
	const data = {
		pageid: props.pageConfig.pagecode,
		model: {
			areaType: AreaType,
			areacode: tableId,
			rows: changedRows
		}
	};
	ajax({
		url: URL.deleteUrl,
		data,
		success: (res) => {
			let { success } = res;
			if (success) {
				let allData = props.editTable.getAllData(tableId);
				allData.rows = allData.rows.filter((item, i) => i != index);
				setValue.call(this, props, { [tableId]: allData });
				showMessage.call(this, props, { type: MsgConst.Type.DeleteSuccess });
			}
		}
	});
}

//新增
export function doAdd(props, isBlur = false) {
	const oldstatus = props.editTable.getStatus(tableId);
	if (oldstatus != UISTATE.edit) {
		setStatus.call(this, props, UISTATE.edit);
	}
	let defaultValue = {};
	defaultValue['enablestate'] = {
		value: '2'
	};
	defaultValue['reason_type'] = {
		value: props.pageConfig.reasonType
	};
	props.editTable.addRow(tableId, undefined, isBlur == false, defaultValue);
	//changeEnablestateValue.call(this, props);
}

//批量删除
function batchDelRow(props) {
	let checkedRows = props.editTable.getCheckedRows(tableId);
	if (checkedRows.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseDelete });
		return;
	}
	let indexArr = [];
	checkedRows.map((row) => {
		indexArr.push(row.index);
	});
	const status = props.editTable.getStatus(tableId);
	if (status == undefined || status == UISTATE.browse) {
		props.ncmodal.show(`${props.pageConfig.pagecode}-confirm`, {
			title: getMultiLangByID('msgUtils-000000') /* 国际化处理： 删除*/,
			content: getMultiLangByID('msgUtils-000004') /* 国际化处理： 确定要删除所选数据吗？*/,
			beSureBtnClick: () => {
				batchDel.call(this, props, indexArr);
			}
		});
	} else {
		delRow.call(this, props, indexArr);
	}
}

//取消
function cancelConfirm(props) {
	props.ncmodal.show(`${props.pageConfig.pagecode}-confirm`, {
		title: getMultiLangByID('msgUtils-000002') /* 国际化处理： 取消*/,
		content: getMultiLangByID('msgUtils-000003') /* 国际化处理： 确定要取消吗？*/,
		beSureBtnClick: () => {
			doCancel.call(this, props);
		}
	});
}

function doCancel(props) {
	props.editTable.cancelEdit(tableId, () => {
		setBatchBtnsEnable.call(this, props, tableId);
		setStatus.call(this, props, UISTATE.browse);
		// 调用平台api过滤显示数据
		let { keyWord } = this.state;
		props.editTable.setFiltrateTableData(tableId, filterFields, keyWord);
	});
}

/**
 * 设置界面状态
 * @param {*} props
 * @param {*} status
 */
export function setStatus(props, status) {
	switch (status) {
		case 'edit':
			props.editTable.setStatus(tableId, UISTATE.edit);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(BrowseBtns, false);
			props.button.setButtonVisible(EditBtns, true);

			// 新增按钮，改为次要按钮
			props.button.setMainButton('Add', false);

			// 行删除时不需要悬浮框提示
			props.button.setPopContent('Delete', undefined);
			modifyBeforeAjaxByIndex.call(this, props);
			break;
		default:
			props.editTable.setStatus(tableId, UISTATE.browse);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonVisible(EditBtns, false);
			props.button.setButtonVisible(BrowseBtns, true);

			// 新增按钮，改为主要按钮
			props.button.setMainButton('Add', true);

			// 行删除时悬浮框提示
			props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001') /* 国际化处理： 确定要删除吗？*/);
			modifyAfterAjaxByIndex.call(this, props);
			break;
	}
}

/**
 * 设置界面值
 * @param {*} props 
 * @param {*} status 
 */
export function setValue(props, data) {
	let status = props.editTable.getStatus(tableId) || UISTATE.browse;
	let isBrowse = status == UISTATE.browse;
	if (data && data[tableId] && data[tableId].rows) {
		if (status == UISTATE.browse) {
			modifyAfterAjax(data[tableId].rows);
		}
		props.editTable.setTableData(tableId, data[tableId], isBrowse);
		// 调用平台api过滤显示数据
		let { keyWord } = this.state;
		props.editTable.setFiltrateTableData(tableId, filterFields, keyWord);
	} else {
		props.editTable.setTableData(tableId, { rows: [] }, isBrowse);
	}
	setBatchBtnsEnable.call(this, props, tableId);
}

/**
 * 设置批量按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBatchBtnsEnable(props, tableId) {
	let checkedRows = props.editTable.getCheckedRows(tableId);
	let { pageConfig = {} } = props;
	let { batchBtns = [ 'Delete', 'Print', 'Output' ] } = pageConfig;
	props.button.setButtonDisabled(batchBtns, !(checkedRows && checkedRows.length > 0));
	//修改按钮是否可用
	//let allRows = props.editTable.getAllRows(tableId, true);
	//props.button.setButtonDisabled([ 'Edit' ], !(allRows && allRows.length > 0));
}

/**
 * 启用
 * @param {*} props
 * @param {*} tableId
 * @param {*} index
 */
export function enableRow(props, index) {
	let allData = props.editTable.getAllData(tableId);
	let allRows = allData.rows;
	modifyBeforeAjax(allRows[index]);

	const data = {
		pageid: props.pageConfig.pagecode,
		model: {
			areaType: AreaType,
			areacode: tableId,
			rows: [ allRows[index] ]
		}
	};
	ajax({
		url: URL.unsealUrl,
		data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				//更新数据
				let retData = data[tableId];
				allRows[index] = retData.rows[0];
				modifyAfterAjax(allRows[index]);
				allData.rows = allRows;
				setValue.call(this, props, { [tableId]: allData });
				showMessage.call(this, props, { type: MsgConst.Type.EnableSuccess });
			}
		}
	});
}

/**
 * 停用
 * @param {*} props
 * @param {*} tableId
 * @param {*} index
 */
export function disableRow(props, index) {
	let allData = props.editTable.getAllData(tableId);
	let allRows = allData.rows;
	modifyBeforeAjax(allRows[index]);
	const data = {
		pageid: props.pageConfig.pagecode,
		model: {
			areaType: AreaType,
			areacode: tableId,
			rows: [ allRows[index] ]
		}
	};
	ajax({
		url: URL.sealUrl,
		data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				// 更新数据
				let retData = data[tableId];
				allRows[index] = retData.rows[0];
				modifyAfterAjax(allRows[index]);
				allData.rows = allRows;
				setValue.call(this, props, { [tableId]: allData });
				showMessage.call(this, props, { type: MsgConst.Type.DisableSuccess });
			}
		}
	});
}

/**
 * 查询数据
 */
export function getData(data, isRefresh = false) {
	ajax({
		url: URL.queryUrl,
		data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				setValue.call(this, this.props, data);
				setBatchBtnsEnable.call(this, this.props, tableId);
				if (isRefresh) {
					showMessage.call(this, this.props, { type: MsgConst.Type.RefreshSuccess });
				}
			}
		}
	});
}
