export const pageConfig = {
	//浏览态按钮
	BrowseBtns: [ 'Add', 'Edit', 'Delete', 'Print', 'Output', 'Refresh' ],
	//编辑态按钮
	EditBtns: [ 'Save', 'Add', 'Cancel', 'Delete' ],
	//路径
	URL: {
		printUrl: '/nccloud/ampub/reason/printList.do',
		queryUrl: '/nccloud/ampub/reason/query.do',
		saveAllUrl: '/nccloud/ampub/reason/saveall.do',
		deleteUrl: '/nccloud/ampub/reason/delete.do',
		unsealUrl: '/nccloud/ampub/reason/unseal.do',
		sealUrl: '/nccloud/ampub/reason/seal.do'
	},
	//表格区域id
	tableId: 'list_head',
	//区域类型
	AreaType: 'table',
	// 模糊搜索过滤字段
	filterFields: [ 'reason_code', 'reason_name' ]
};
