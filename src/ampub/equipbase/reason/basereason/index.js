import React, { Component } from 'react';
import { base, createPageIcon } from 'nc-lightapp-front';
import { buttonClick, initTemplate, afterEvent, rowSelected, setValue, getData } from './events';
import { pageConfig } from './constants';
const { tableId, filterFields } = pageConfig;
const { NCCheckbox, NCFormControl } = base;
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils, closeBrowserUtils } = utils;
const { getMultiLangByID } = multiLangUtils;

class Reason extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isShowOff: false,
			keyWord: ''
		};
		closeBrowserUtils.call(this, props, { editTable: [ tableId ] });
		initTemplate.call(this, props);
	}

	// 搜索关键词变化
	keyWordChange = (value) => {
		this.setState({ keyWord: value }, () => {
			// 调用平台api过滤显示数据
			this.props.editTable.setFiltrateTableData(tableId, filterFields, value);
		});
	};

	// 切换显示停用
	showOffChange = (value) => {
		this.setState({ isShowOff: value }, () => {
			let data = {
				isShowSeal: value ? 'Y' : 'N',
				pagecode: this.props.pageConfig.pagecode,
				areacode: tableId,
				queryCondition: { nodecode: this.props.pageConfig.nodecode }
			};
			getData.call(this, data);
		});
	};

	render() {
		const { editTable, button, ncmodal } = this.props;
		const { createEditTable } = editTable;
		const { createButtonApp } = button;
		const { createModal } = ncmodal;
		let { keyWord } = this.state;
		const status = editTable.getStatus(tableId);
		return (
			<div className="nc-single-table">
				{/* 头部 header */}
				<div className="nc-singleTable-header-area">
					{/* 标题 title */}
					<div className="header-title-search-area">
						{createPageIcon()}
						<h2 className="title-search-detail">
							{getMultiLangByID(this.props.pageConfig.reasonTypeCN) /* 国际化处理： 原因档案名称*/}
						</h2>
						{/* 简单搜索  search-box*/}
						<div className="title-search-detail">
							<NCFormControl
								placeholder={getMultiLangByID('amcommon-000003') /* 国际化处理： 请输入编码或名称*/}
								value={keyWord}
								onChange={(value) => {
									this.keyWordChange(value);
								}}
								type="search"
								disabled={status == 'edit'}
							/>
						</div>
						{/* 显示停用  showOff*/}
						<div className="title-search-detail">
							<span className="showOff">
								<NCCheckbox
									checked={this.state.isShowOff}
									onChange={(value) => {
										this.showOffChange(value);
									}}
									disabled={status == 'edit'}
								>
									{getMultiLangByID('amcommon-000004') /* 国际化处理： 显示停用*/}
								</NCCheckbox>
							</span>
						</div>
					</div>

					{/* 按钮区 */}
					<div className="header-button-area">
						{createButtonApp({
							area: 'list_head',
							buttonLimit: 3,
							onButtonClick: (props, id) => {
								buttonClick.call(this, props, id);
							},
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>

				{/* 列表区 */}
				<div className="nc-singleTable-table-area">
					{createEditTable(tableId, {
						onAfterEvent: afterEvent.bind(this),
						showCheck: true,
						showIndex: true,
						selectedChange: rowSelected.bind(this)
					})}
				</div>
				{/* 弹出框 */}
				{createModal(`${this.props.pageConfig.pagecode}-confirm`, { color: 'warning' })}
			</div>
		);
	}
}

export default Reason;
