/*
* 减值原因入口
* created by cuixuh
* */
import ReactDOM from 'react-dom';
import { createPage } from 'nc-lightapp-front';
import Reason, { initTemplate } from '../basereason';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { initMultiLangByModule } = multiLangUtils;
//页面配置
const pageConfig = {
	nodecode: '2012004060', //减值原因的功能编码
	reasonType: '1', //原因类型——//减值：1，减少：2，变动：3，封存：4
	reasonTypeCN: '450101508A-000002' /* 国际化处理： 减值原因*/,
	pagecode: '201200528A_list',
	appcode: '201200528A', //小应用编码
	printNodekey: null,
	printFilename: '450101508A-000002' /* 国际化处理： 减值原因*/
};
const PredevaluateReason = createPage({
	billinfo: {
		billtype: 'grid',
		pagecode: '201200528A_list',
		headcode: 'list_head'
	}
})(Reason);

let moduleIds = { ampub: [ '450101508A', 'common' ] };
initMultiLangByModule(moduleIds, () => {
	// 显示页面
	ReactDOM.render(<PredevaluateReason pageConfig={pageConfig} />, document.querySelector('#app'));
});
