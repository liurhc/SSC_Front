import ampub from 'ampub';
const { commonConst } = ampub;
const { VOSTATUS } = commonConst.StatusUtils;
// 页面配置
const pageConfig = {
	// 小应用编码
	appcode: '450103508A',
	// 节点名称
	title: '资产费用项目' /* 国际化处理：资产费用项目 */,
	// 表格区域编码
	tableId: 'list_head',
	//列表区域类型
	areaType: 'table',
	// 表格节点编码
	pagecode: '450103508A_list',
	// 功能节点编码，即模板编码
	printFuncode: '4501004045',
	// 单据类型
	billtype: 'grid',
	// VO状态
	vo_status: { ...VOSTATUS },
	// 模糊搜索过滤字段
	filterFields: [ 'expensetype_code', 'expensetype_name' ],
	// 打印模板节点标识
	printNodekey: '',
	// 浏览态按钮
	browseHeadBtns: {
		Add: true,
		Edit: true,
		Delete: true,
		Save: false,
		Cancel: false,
		Print: true,
		Output: true,
		Refresh: true
	},
	// 编辑态按钮
	editHeadBtns: {
		Add: true,
		Edit: false,
		Delete: true,
		Save: true,
		Cancel: true,
		Print: false,
		Output: false,
		Refresh: false
	},
	url: {
		queryAllUrl: '/nccloud/ampub/leasebase/ExpenseTypeQueryAll.do',
		printUrl: '/nccloud/ampub/leasebase/print.do',
		saveUrl: '/nccloud/ampub/leasebase/ExpenseTypeSave.do',
		unSealUrl: '/nccloud/ampub/leasebase/ExpenseTypeUnSeal.do',
		sealUrl: '/nccloud/ampub/leasebase/ExpenseTypeSeal.do'
	}
};

const btnUsability = {
	browseNoData: {
		Save: true,
		Add: false,
		Edit: false,
		Cancel: true,
		Delete: true,
		Print: true,
		Output: true
	},
	browseHasData: {
		Save: true,
		Add: false,
		Edit: false,
		Cancel: true,
		Delete: false,
		Print: false,
		Output: false
	},
	editNoData: {
		Save: false,
		Add: false,
		Edit: true,
		Cancel: false,
		Delete: true,
		Print: true,
		Output: true
	},
	editHasData: {
		Save: false,
		Add: false,
		Edit: true,
		Cancel: false,
		Delete: false,
		Print: true,
		Output: true
	}
};

export { pageConfig, btnUsability };
