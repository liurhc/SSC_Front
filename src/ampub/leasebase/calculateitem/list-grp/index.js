import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage } from 'nc-lightapp-front';
import SingleTableBase, { baseConfig } from '../list-base';
import ampub from 'ampub';
const { utils } = ampub;
const { multiLangUtils } = utils;
const { initMultiLangByModule } = multiLangUtils;

// 页面配置
const pageConfig = {
	...baseConfig,
	// 应用编码
	appcode: '450103520A',
	// 应用类型
	nodeType: 'group',
	// 应用名称
	title: '租金计算项目-集团',
	// 页面编码
	pagecode: '450103520A_list',
	// 输出文件名称
	printFilename: '租金计算项目-集团',
	// 打印模板节点标识
	printNodekey: null
};

const SingleTable = createPage({
	billinfo: {
		billtype: 'grid',
		pagecode: pageConfig.pagecode,
		headcode: pageConfig.tableId,
		bodycode: pageConfig.tableId
	}
})(SingleTableBase);

let moduleIds = { ampub: [ '450101024A', 'common' ] };
initMultiLangByModule(moduleIds, () => {
	// 显示页面
	ReactDOM.render(<SingleTable pageConfig={pageConfig} />, document.querySelector('#app'));
});
