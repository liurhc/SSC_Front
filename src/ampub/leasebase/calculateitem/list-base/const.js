// 页面配置
export const pageConfig = {
	// 应用编码
	appcode: '450103520A',
	// 应用类型
	nodeType: 'group',
	// 应用名称
	title: '租金计算项目-集团' /* 国际化处理： 专业-集团*/,
	// 表格区域id
	tableId: 'list_head',
	// 页面编码
	pagecode: '450103520A_list',
	// 主键字段
	pkField: 'pk_cal_item',
	// 编辑态按钮
	editBtns: [ 'Add', 'Delete', 'Save', 'Cancel' ],
	// 浏览态按钮
	browseBtns: [ 'Add', 'Edit', 'Delete', 'Print', 'Output', 'Refresh' ],
	url: {
		queryUrl: '/nccloud/ampub/calculateitem/query.do',
		sealUrl: '/nccloud/ampub/calculateitem/seal.do',
		unSealUrl: '/nccloud/ampub/calculateitem/unseal.do',
		saveUrl: '/nccloud/ampub/calculateitem/save.do',
		deleteUrl: '/nccloud/ampub/calculateitem/delete.do',
		printUrl: '/nccloud/ampub/calculateitem/printList.do',
		setResValue: '/nccloud/ampub/calculateitem/setResValue.do',
		metaUrl: '/nccloud/ampub/formula/metainfo.do'
	},
	// 模糊搜索过滤字段
	filterFields: [ 'calitem_code', 'calitem_name' ],
	exceptFields: [ 'pk_group', 'pk_org', 'enablestate' ],
	// 输出文件名称
	printFilename: '450101024A-000000' /* 国际化处理： 专业-集团*/,
	// 打印模板节点标识
	printNodekey: null
};
