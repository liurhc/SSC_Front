import ampub from 'ampub';
import { setStatus, enableRow, disableRow, getData } from './buttonClick';
import tableButtonClick from './tableButtonClick';

const { commonConst, components, utils } = ampub;
const { tableUtils } = utils;
const { LoginContext } = components;
const { loginContext, loginContextKeys, getContext } = LoginContext;
const { StatusUtils } = commonConst;
const { UISTATE } = StatusUtils;
const { createOprationColumn, createEnableSwitch } = tableUtils;

/**
 * 初始化模板
 * @param {*} props 
 */
export default function(props) {
	const { pageConfig } = props;
	const { pagecode } = pageConfig;
	props.createUIDom(
		{
			//页面编码
			pagecode
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
				if (data.button) {
					// 设置按钮
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.template) {
					// 修改模板
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta, () => {
						afterInit.call(this, props);
					});
				}
			}
		}
	);
}

/**
 * 模板初始化以后
 * @param {*} props 
 */
function afterInit(props) {
	const { pageConfig } = props;
	const { nodeType } = pageConfig;
	let status = props.getUrlParam('status') || UISTATE.browse;
	setStatus.call(this, props, status);
	if (nodeType == 'org') {
		// 默认主组织
		let pk_org = getContext(loginContextKeys.pk_org);
		let org_Name = getContext(loginContextKeys.org_Name);
		if (pk_org) {
			this.mainorgChange({ refpk: pk_org, refname: org_Name });
		} else {
			props.button.setButtonDisabled([ 'Add', 'Edit' ], true);
			getData.call(this, props);
		}
	} else {
		getData.call(this, props);
	}
}

/**
 * 修改模板
 * @param {*} props 
 * @param {*} meta 
 */
function modifierMeta(props, meta) {
	const { pageConfig } = props;
	const { nodeType, tableId } = pageConfig;
	meta[tableId].items.map((item) => {
		if (item.attrcode == 'func_ref') {
			item.refcode = 'ampub/refer/leasebase/formulaeditor/index';
			item.itemtype = 'refer';
			item.isDataPowerEnable = false;
			item.isCacheable = false;
		}
	});
	if (meta[tableId] && meta[tableId].items && meta[tableId].items.length > 0) {
		// 处理停启用开关
		createEnableSwitch.call(this, props, { meta, enableRow, disableRow });

		// 添加表格操作列
		let oprCol = createOprationColumn.call(this, props, { nodeType, tableButtonClick });
		meta[tableId].items.push(oprCol);
	}

	return meta;
}
