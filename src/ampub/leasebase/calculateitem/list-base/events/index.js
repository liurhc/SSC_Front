import buttonClick, { setStatus, setValue, setBatchBtnsEnable, getData } from './buttonClick';
import initTemplate from './initTemplate';
import afterEvent from './afterEvent';
import rowSelected from './rowSelected';

/**
 * 页面所有事件
 */
export { buttonClick, initTemplate, afterEvent, setStatus, setValue, rowSelected, setBatchBtnsEnable, getData };
