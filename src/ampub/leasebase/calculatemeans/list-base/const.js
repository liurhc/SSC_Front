// 页面配置
export const pageConfig = {
	// 应用编码
	appcode: '450103528A',
	// 应用类型
	nodeType: 'group',
	// 应用名称
	title: '租金计算方法-集团',
	// 表格区域id
	tableId: 'list_head',
	// 页面编码
	pagecode: '450103528A_list',
	// 主键字段
	pkField: 'pk_cal_means',
	// 编辑态按钮
	editBtns: [ 'Add', 'Delete', 'Save', 'Cancel' ],
	// 浏览态按钮
	browseBtns: [ 'Add', 'Edit', 'Delete', 'Print', 'Output', 'Refresh' ],
	url: {
		queryUrl: '/nccloud/ampub/calculatemeans/query.do',
		sealUrl: '/nccloud/ampub/calculatemeans/seal.do',
		unSealUrl: '/nccloud/ampub/calculatemeans/unseal.do',
		saveUrl: '/nccloud/ampub/calculatemeans/save.do',
		deleteUrl: '/nccloud/ampub/calculatemeans/delete.do',
		printUrl: '/nccloud/ampub/calculatemeans/printList.do',
		setResValue: '/nccloud/ampub/calculatemeans/setResValue.do',
		metaUrl: '/nccloud/ampub/formula/metainfo.do'
	},
	// 模糊搜索过滤字段
	filterFields: [ 'method_code', 'method_name' ],
	exceptFields: [ 'pk_group', 'pk_org', 'enablestate' ],
	// 输出文件名称
	printFilename: '450101024A-000000',
	// 打印模板节点标识
	printNodekey: null
};
