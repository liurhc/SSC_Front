import { enableRow, disableRow, addClick } from './index';
import { pageConfig } from './../const.js';

const tableId = pageConfig.tableId;

export default function(props, id, key, value, changedrows, index, data, type, eventType) {
	if (eventType == 'blur' && key != 'enablestate') {
		// 平台的自动增行不会附默认值，这里调用自己的增行
		let num = props.editTable.getNumberOfRows(tableId);
		if (num == index + 1) {
			addClick.call(this, props, false);
		}
	}
}
