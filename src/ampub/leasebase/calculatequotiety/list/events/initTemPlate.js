import { setStatus, setBatchBtnsEnable } from './buttonClicks';
import tableButtonClick from './tableButtonClick';
import { pageConfig } from './../const.js';
import { getData, enableRow, disableRow } from './index';
import ampub from 'ampub';

const { commonConst, components, utils } = ampub;
const { loginContext, loginContextKeys, getContext } = components.LoginContext;
const { UISTATE } = commonConst.StatusUtils;
const { createEnableSwitch } = utils.tableUtils;
const { defRefCondition } = components.refInit;
const { getMultiLangByID } = utils.multiLangUtils;

const tableId = pageConfig.tableId;
const browseHeadBtns = pageConfig.browseHeadBtns;

export default function initTemplate(props) {
	let pagecode = pageConfig.pagecode;

	props.createUIDom(
		{
			//页面id
			pagecode
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
				if (data.button) {
					let button = data.button;
					props.button.setButtons(button, () => {});
				}
				if (data.template) {
					let meta = data.template;
					modifierMeta(props, meta);
					props.meta.setMeta(meta, () => {
						afterSetMeta.call(this);
					});
				}
			}
		}
	);
}

/**
 * 设置完模板后操作
 */
function afterSetMeta() {
	getData.call(this);
	setStatus.call(this, this.props, UISTATE.browse);
}

/**
 * 调整模板，主要作用：
 * 		1.添加列表操作列
 * 		2.参照过滤
 * @param {*} props 
 * @param {*} meta 
 */
function modifierMeta(props, meta) {
	meta[tableId].status = 'browse';
	// 处理停启用开关
	createEnableSwitch.call(this, props, { meta, enableRow, disableRow });
	// 添加列表操作列
	addOperationColumn.call(this, props, meta);
	// 参照过滤
	filterRefer.call(this, props, meta);
	return meta;
}

/**
 * 参照过滤（此处只有自定义项的过滤）
 * @param {*} props 
 * @param {*} meta 
 */
function filterRefer(props, meta) {
	meta[pageConfig.tableId].items.map((item, key) => {
		defRefCondition.call(this, props, item, pageConfig.tableId, getContext(loginContextKeys.groupId));
	});
}

/**
 * 添加列表操作列
 * @param {*} props 
 * @param {*} meta 
 */
function addOperationColumn(props, meta) {
	meta[tableId].items.push({
		label: getMultiLangByID('amcommon-000000') /* 国际化处理：操作 */,
		visible: true,
		attrcode: 'opr',
		itemtype: 'customer',
		// 锁定在右边
		fixed: 'right',
		width: '110px',
		render: (text, record, index) => {
			let recordVal = record.values;
			let status = props.editTable.getStatus(tableId);
			let enableShow;
			let disableShow;
			let buttonAry = [ 'Delete' ];
			if (status == 'browse') {
				if (recordVal.enablestate.value == '2') {
					enableShow = false;
					disableShow = true;
				} else {
					enableShow = true;
					disableShow = false;
				}
			}
			return props.button.createOprationButton(buttonAry, {
				area: 'list_inner',
				buttonLimit: 3,
				onButtonClick: (props, key) => tableButtonClick.call(this, props, key, text, record, index)
			});
		}
	});
}
