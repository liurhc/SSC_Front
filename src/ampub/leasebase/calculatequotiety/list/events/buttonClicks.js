import { ajax, print, output } from 'nc-lightapp-front';
import { pageConfig, btnUsability } from './../const.js';
import ampub from 'ampub';

const { utils, commonConst, components } = ampub;
const { tableUtils, msgUtils, multiLangUtils } = utils;
const { modifyBeforeAjax, modifyAfterAjax, modifyBeforeAjaxByIndex, modifyAfterAjaxByIndex } = tableUtils;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { getMultiLangByID } = multiLangUtils;
const { StatusUtils } = commonConst;
const { UISTATE } = StatusUtils;
const { getContext, loginContextKeys } = components.LoginContext;

const tableId = pageConfig.tableId;
const areaType = pageConfig.areaType;
const areacode = pageConfig.tableId;
const pageid = pageConfig.pagecode;
const editHeadBtns = pageConfig.editHeadBtns;
const browseHeadBtns = pageConfig.browseHeadBtns;

export default function(props, id) {
	switch (id) {
		case 'Add':
			addClick.call(this, props);
			break;
		case 'Cancel':
			cancelClick.call(this, props);
			break;
		case 'Save':
			saveClick.call(this, props);
			break;
		case 'enable':
			enableRow.call(this, props);
			break;
		case 'disable':
			disableRow.call(this, props);
			break;
		case 'Delete':
			batchDelClick.call(this, props);
			break;
		case 'Edit':
			editClick.call(this, props);
			break;
		case 'delRow':
			delRow.call(this, props);
			break;
		case 'singleDel':
			singleDel.call(this, props);
			break;
		case 'Print':
			printClick.call(this, props);
			break;
		case 'Output':
			outputClick.call(this, props);
			break;
		case 'Refresh':
			refresh.call(this, props);
			break;
		default:
			break;
	}
}

export function onSealChange() {
	getData.call(this, !this.state.isshowseal);
	this.setState({ isshowseal: !this.state.isshowseal });
}

export function getData(isShowSeal = this.state.isshowseal, callBack) {
	let data = {
		pagecode: pageConfig.pagecode,
		isShowSeal
	};
	ajax({
		url: '/nccloud/ampub/leasebase/CalculateQuotietyQueryAll.do',
		data: data,
		success: (res) => {
			afterGetData.call(this, res.data);
			callBack && typeof callBack == 'function' && callBack.call(this);
		}
	});
}
function afterGetData(data) {
	if (data) {
		let allRows = data[pageConfig.tableId].rows;
		modifyAfterAjax.call(this, allRows);
		this.props.editTable.setTableData(pageConfig.tableId, { rows: allRows });
	} else {
		this.props.editTable.setTableData(pageConfig.tableId, { rows: [] });
	}
}
/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let checkedRows = props.editTable.getCheckedRows(pageConfig.tableId);
	if (!checkedRows || checkedRows.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		return;
	}
	let pks = [];
	checkedRows.map((item) => {
		pks.push(item.data.values['pk_cal_quotiety'].value);
	});
	let printData = {
		filename: pageConfig.pagecode, // 文件名称
		nodekey: pageConfig.printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

//打印按钮点击事件
function printClick(props) {
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		return;
	}
	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		pageConfig.url.printUrl, // 后台打印服务url
		printData
	);
}

//输出按钮点击事件
function outputClick(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });
		return;
	}
	output({
		url: pageConfig.url.printUrl,
		data: printData
	});
}

//新增按钮点击事件
export function addClick(props, autoFocus = true) {
	let oldstatus = props.editTable.getStatus(tableId);
	if (oldstatus != UISTATE.edit) {
		setStatus.call(this, props, UISTATE.edit);
	}
	let defaultValue = {};
	defaultValue['enablestate'] = {
		value: '2'
	};
	// 设置集团
	defaultValue['pk_org'] = {
		value: getContext(loginContextKeys.pk_org)
	};
	props.editTable.addRow(tableId, undefined, autoFocus, defaultValue);
	setBatchBtnsEnable(props, tableId);
}

// 批量删除数据
function batchDelClick(props) {
	let checkedRows = props.editTable.getCheckedRows(tableId);
	if (checkedRows.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseDelete });
		return;
	}
	let indexArr = [];
	checkedRows.map((row) => {
		indexArr.push(row.index);
	});
	let status = props.editTable.getStatus(tableId);
	if (status == 'browse' || status == undefined) {
		showConfirm.call(this, props, {
			type: MsgConst.Type.DelSelect,
			beSureBtnClick: () => {
				delClick(props, tableId, indexArr);
			}
		});
	} else {
		delRow(props, indexArr);
	}
}

/**
 * 前台删除
 * @param {*} props 
 * @param {*} index 
 */
export function delRow(props, index) {
	props.editTable.deleteTableRowsByIndex(tableId, index);
	setBatchBtnsEnable(props, pageConfig.tableId);
}

/**
 * 删除
 * @param {*} props 
 */
function delClick(props, moduleId, index = []) {
	let allRows = props.editTable.getAllRows(tableId, false);
	let changedRows = [];
	for (let i = 0; i < allRows.length; i++) {
		if (index.includes(i)) {
			changedRows.push(allRows[i]);
		}
	}
	if (changedRows && changedRows.length != 0) {
		changedRows.map((row) => {
			modifyBeforeAjax(row);
			row['status'] = '3';
		});
		const data = {
			model: {
				areaType,
				areacode,
				rows: []
			},
			pageid
		};
		data.model.rows = changedRows;
		props.validateToSave(data, () => {
			ajax({
				url: '/nccloud/ampub/leasebase/CalculateQuotietySave.do',
				data,
				success: (res) => {
					let allRows = props.editTable.getAllRows(tableId, false);
					let newRows = [];
					for (let i = 0; i < allRows.length; i++) {
						if (!index.includes(i)) {
							newRows.push(allRows[i]);
						}
					}
					let allData = props.editTable.getAllData(tableId);
					allData.rows = newRows;
					props.editTable.setTableData(tableId, allData);
					setBatchBtnsEnable(props, pageConfig.tableId);
					showMessage.call(this, props, { type: MsgConst.Type.DeleteSuccess });
				}
			});
		});
	}
}

/**
 * 浏览态直接删除单个数据
 * @param {*} props 
 * @param {*} tableId 
 * @param {*} index 
 */
export function singleDel(props, index) {
	let tableData = props.editTable.getAllRows(tableId, false);
	// 将enablestate的值由boolean转换为int类型，以适配后端
	tableData.map((item) => {
		modifyBeforeAjax(item);
	});
	let changeRow = [];
	for (let i = 0; i < tableData.length; i++) {
		if (index == i) {
			changeRow.push(tableData[i]);
		}
	}
	changeRow[0].status = '3';
	const data = {
		model: {
			areaType,
			areacode,
			rows: []
		},
		pageid
	};
	data.model.rows = changeRow;
	props.validateToSave(data, () => {
		ajax({
			url: '/nccloud/ampub/leasebase/CalculateQuotietySave.do',
			data,
			success: (res) => {
				let allRows = props.editTable.getAllRows(tableId);
				let newRows = [];
				for (let i = 0; i < allRows.length; i++) {
					if (index != i) {
						newRows.push(allRows[i]);
					}
				}
				let allData = props.editTable.getAllData(tableId);
				allData.rows = newRows;
				props.editTable.setTableData(tableId, allData);
				showMessage.call(this, props, { type: MsgConst.Type.DeleteSuccess });
			}
		});
	});
}

/**
 * 修改
 * @param {*} props 
 */
function editClick(props) {
	setStatus.call(this, props, UISTATE.edit);
	setBatchBtnsEnable(props, tableId);
}

//取消按钮点击事件
function cancelClick(props) {
	showConfirm.call(this, props, {
		type: MsgConst.Type.Cancel,
		beSureBtnClick: () => {
			props.editTable.cancelEdit(tableId, () => {
				props.button.setButtonsVisible(browseHeadBtns);
				// 设置按钮主次关系，新增设为主要按钮
				props.button.setMainButton('Add', true);
				// 行删除时悬浮框提示
				props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001')); /* 国际化处理：确定要删除吗？ */
				props.button.setButtonDisabled(btnUsability.browseNoData);
			});
		}
	});
}

//保存按钮点击事件
function saveClick(props) {
	//过滤空行
	props.editTable.filterEmptyRows(tableId, [ 'enablestate', 'pk_group', 'pk_org' ]);
	let visibleRows = props.editTable.getVisibleRows(tableId);
	let pass = props.editTable.checkRequired(tableId, visibleRows);
	// 必输项校验没过
	if (!pass) {
		return;
	}
	let changedRows = props.editTable.getChangedRows(tableId);
	// 没有修改行，变更状态
	if (!changedRows || changedRows.length == 0) {
		setStatus.call(this, props, UISTATE.browse);
		showMessage.call(this, props, { type: MsgConst.Type.SaveSuccess });
		return;
	}
	// 将enablestate的值由boolean转换为int类型，以适配后端
	modifyBeforeAjax.call(this, changedRows);

	const data = {
		model: {
			areaType,
			areacode,
			rows: []
		},
		pageid
	};
	data.model.rows = changedRows;
	props.validateToSave(data, () => {
		ajax({
			url: '/nccloud/ampub/leasebase/CalculateQuotietySave.do',
			data,
			success: (res) => {
				afterSave.call(this, res.data);
			}
		});
	});
}

function afterSave(data) {
	let retData = data[tableId];
	let newRows = [];
	let i = 0;
	let allData = this.props.editTable.getAllData(tableId);
	let allRows = allData.rows;
	allRows.map((item, index) => {
		// 0原始1修改2新增3删除
		if (item.status == pageConfig.vo_status.UPDATED || item.status == pageConfig.vo_status.NEW) {
			if (retData && retData.rows && retData.rows.length > i) {
				newRows.push(retData.rows[i]);
			}
			i++;
		} else if (item.status == pageConfig.vo_status.DELETED) {
			i++;
		} else {
			newRows.push(item);
		}
	});
	allData.rows = newRows;
	modifyAfterAjax(allData.rows);
	setStatus.call(this, this.props, 'browse');
	this.props.editTable.setTableData(tableId, allData);
	this.props.editTable.cancelEdit(tableId);
	this.props.button.setButtonsVisible(browseHeadBtns);

	setBatchBtnsEnable(this.props, pageConfig.tableId);
	showMessage.call(this, this.props, { type: MsgConst.Type.SaveSuccess });
}

//启用
export function enableRow(props, index) {
	let allRows = props.editTable.getAllRows(tableId, false);
	allRows[index].values.enablestate.value = true;
	modifyBeforeAjax(allRows[index]);
	const data = {
		pageid,
		model: {
			areaType,
			rows: [ allRows[index] ]
		}
	};
	ajax({
		url: '/nccloud/ampub/leasebase/CalculateQuotietyUnSeal.do',
		data,
		success: function(res) {
			if (res) {
				// 更新数据
				let retData = res.data[tableId];
				allRows[index] = retData.rows[0];
				modifyAfterAjax(allRows[index]);
				let allData = props.editTable.getAllData(tableId);
				allData.rows = allRows;
				props.editTable.setTableData(tableId, allData);
				showMessage.call(this, props, { type: MsgConst.Type.EnableSuccess });
			}
		}
	});
}

//停用
export function disableRow(props, index) {
	let allRows = props.editTable.getAllRows(tableId, false);
	allRows[index].values.enablestate.value = false;
	modifyBeforeAjax(allRows[index]);
	const data = {
		pageid,
		model: {
			areaType,
			rows: [ allRows[index] ]
		}
	};
	ajax({
		url: '/nccloud/ampub/leasebase/CalculateQuotietySeal.do',
		data,
		success: function(res) {
			if (res) {
				// 更新数据
				let retData = res.data[tableId];
				allRows[index] = retData.rows[0];
				modifyAfterAjax(allRows[index]);
				let allData = props.editTable.getAllData(tableId);
				allData.rows = allRows;
				props.editTable.setTableData(tableId, allData);
				showMessage.call(this, props, { type: MsgConst.Type.DisableSuccess });
			}
		}
	});
}

/**
 * 设置界面状态
 * @param {*} props 
 * @param {*} status @default UISTATE.browse
 */
export function setStatus(props, status = UISTATE.browse) {
	// 获取选中的行
	let checkedRows = props.editTable.getCheckedRows(tableId);
	switch (status) {
		case UISTATE.edit:
			props.editTable.setStatus(tableId, UISTATE.edit);
			// 先隐藏后显示，解决浏览态编辑态都有的按钮清空
			props.button.setButtonsVisible(editHeadBtns);
			// 如果有新增按钮，改为次要按钮
			props.button.setMainButton('Add', false);
			// 行删除时不需要悬浮框提示
			props.button.setPopContent('Delete', undefined);
			// 设置按钮可用性
			if (checkedRows && checkedRows.length == 0) {
				props.button.setButtonDisabled(btnUsability.editNoData);
			} else if (checkedRows && checkedRows.length != 0) {
				props.button.setButtonDisabled(btnUsability.editHasData);
			}
			modifyBeforeAjaxByIndex.call(this, props);
			break;
		default:
			// 设置表格状态
			props.editTable.setStatus(tableId, UISTATE.browse);
			// 设置按钮可见性
			props.button.setButtonsVisible(browseHeadBtns);
			// 设置按钮主次关系，新增设为主要按钮
			props.button.setMainButton('Add', true);
			// 行删除时悬浮框提示
			props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001')); /* 国际化处理：确定要删除吗？ */
			// 设置按钮可用性
			if (!checkedRows || checkedRows.length == 0) {
				props.button.setButtonDisabled(btnUsability.browseNoData);
			} else {
				props.button.setButtonDisabled(btnUsability.browseHasData);
			}
			modifyAfterAjaxByIndex.call(this, props);
			break;
	}
}

/**
 * 设置批量按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBatchBtnsEnable(props) {
	let status = props.editTable.getStatus(tableId);
	let checkedRows = props.editTable.getCheckedRows(tableId);
	if (status === UISTATE.browse) {
		if (!checkedRows || checkedRows.length == 0) {
			props.button.setButtonDisabled(btnUsability.browseNoData);
		} else {
			props.button.setButtonDisabled(btnUsability.browseHasData);
		}
	} else if (status == UISTATE.edit) {
		if (checkedRows && checkedRows.length == 0) {
			props.button.setButtonDisabled(btnUsability.editNoData);
		} else if (checkedRows && checkedRows.length != 0) {
			props.button.setButtonDisabled(btnUsability.editHasData);
		}
	}
}

/**
* 刷新
* @param {*} props 
*/
export function refresh(props) {
	getData.call(this, this.state.isshowseal, () => {
		showMessage.call(this, props, { type: MsgConst.Type.RefreshSuccess });
	});
}
