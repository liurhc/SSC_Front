import { createPage, high, ajax } from 'nc-lightapp-front';
import './index.less';
import jsondata from './data.json';
const PopRefer = high.Refer.PopRefer;
const { FormulaEditor } = high;

class Refer extends PopRefer {
	constructor(props) {
		super(props);
		this.arritem = [];
		this.state = {
			...this.state,
			// 值
			value: {},
			// 选中的元数据
			selectedMeta: '',
			// 元数据信息
			metaItem: {},
			show: false
		};
	}

	componentDidMount() {
		this.initMeta();
	}

	// 初始化元数据页签数据
	initMeta = () => {
		const { config, rootprops, initialvalue } = this.props;
		//let { billtype, classid } = config;

		// 测试数据
		let billtype = '4A00,4A2A,4A3A';
		let classid = '205bdc55-d709-46f2-bd42-ec896ca51c28';

		ajax({
			url: '/nccloud/ampub/formula/metainfo.do',
			data: {
				pk_billtype: billtype,
				classid
			},
			success: (res) => {
				if (res.success) {
					let metaItem = res.data;
					this.setState({ selectedItem: '4A00', metaItem });
				}
			}
		});
		if (initialvalue) {
			// 获取计算项目数据
			this.getItemInfo();
			// 获取计算系数数据
			this.getquotietyInfo();
		}
	};

	getItemInfo = () => {
		ajax({
			url: '/nccloud/ampub/calculatemeans/queryitem.do',
			data: {
				pagecode: '450103520A_list'
			},
			success: (res) => {
				if (res.success) {
					let itemInfo = res.data;
					this.setState({ itemInfo });
				}
			}
		});
	};

	getquotietyInfo = () => {
		ajax({
			url: '/nccloud/ampub/calculatemeans/queryquotiety.do',
			data: {
				pagecode: '450103520A_list'
			},
			success: (res) => {
				if (res.success) {
					let quotietyInfo = res.data;
					this.setState({ quotietyInfo });
				}
			}
		});
	};

	// 获取公式或者项目
	getFormulaOrItem = (data, setName, setExplain, name) => {
		if (!data || data.length == 0) {
			return '';
		}
		return (
			<ul class="tab-content">
				{data.map((item) => (
					<li
						class="tab-content-item"
						onClick={() => {
							setExplain(item.explain);
						}}
						onDoubleClick={() => {
							let display = item.explain;
							if (item.name.indexOf('#CI') > -1) {
								display = '[' + item.explain + ']';
							} else {
								display = '$' + item.explain;
							}
							let itemMap = {
								value: item.name,
								display: display
							};
							this.arritem.push(itemMap);
							setName(item.name);
						}}
					>
						{item.label}
					</li>
				))}
			</ul>
		);
	};

	// 租赁页签
	getLeaseFormula = ({ setName, setExplain, name }) => {
		const data = jsondata.leaseformula;
		return this.getFormulaOrItem(data, setName, setExplain, name);
	};

	// 其他类型页签
	getOtherFormula = ({ setName, setExplain, name }) => {
		const data = jsondata.otherformula;
		return this.getFormulaOrItem(data, setName, setExplain, name);
	};

	// 单据项目页签
	getBillItem = ({ setName, setExplain, name }) => {
		const data = jsondata.billItem;
		return this.getFormulaOrItem(data, setName, setExplain, name);
	};

	// 计算系数页签
	getCalculatequotiety = ({ setName, setExplain, name }) => {
		let { quotietyInfo = {} } = this.state;
		const data = quotietyInfo.quotietyInfo;
		return this.getFormulaOrItem(data, setName, setExplain, name);
	};

	// 计算项目页签
	getCalculateitem = ({ setName, setExplain, name }) => {
		let { itemInfo = {} } = this.state;
		const data = itemInfo.itemInfo;
		return this.getFormulaOrItem(data, setName, setExplain, name);
	};

	// 元数据页签
	getMetaItem = ({ setName, setExplain, name }) => {
		let { selectedItem = '', metaItem = {} } = this.state;
		if (selectedItem == '') {
			return '';
		}
		return (
			<div className="content-box">
				<div className="box-left">
					{Object.keys(metaItem).map((item) => (
						<p
							className={item == selectedItem ? 'red' : ''}
							onClick={() => {
								this.setState({ selectedItem: item });
							}}
						>
							{metaItem[item].hintMsg}
						</p>
					))}
				</div>
				<div className="box-right">
					<div className="activeList">
						<ul>
							{metaItem[selectedItem].childNodes.map((item) => (
								<li
									class="tab-content-item"
									onClick={() => {
										setExplain(item.displayname);
									}}
									onDoubleClick={() => {
										let itemMap = {
											value: '$' + selectedItem + '.' + item.code + '@',
											display: '$' + item.displayname
										};
										this.arritem.push(itemMap);
										setName('$' + selectedItem + '.' + item.code + '@');
									}}
								>
									{item.displayname}
								</li>
							))}
						</ul>
					</div>
				</div>
			</div>
		);
	};

	show = () => {
		this.setState({
			isShow: false,
			show: true,
			isFirstShow: false,
			dropDownShow: false,
			isSelectedShow: false,
			selectedShow: false,
			max: false,
			isExpandLeftArea: false,
			isMultiSelectMode: false
		});
	};

	loadTableData = () => {};

	singleSelect = (item, del) => {
		document.body.style.overflow = this.prevOverFlow;
		delete this.prevOverFlow;
		// 单选
		let { onChange, isMultiSelectedEnabled, value: _value, foolValue, idKey } = this.props,
			value = foolValue ? this.foolValueToValue(foolValue) : _value,
			{ selectedValues, dropDownShow, referVal } = this.state;
		item = item || [ ...selectedValues.values() ][0];

		// 单选或者历史记录时收起，其余不收起
		if (!isMultiSelectedEnabled || (dropDownShow && !referVal)) {
			this.setState({
				isShow: false,
				dropDownShow: false,
				isSelectedShow: false
			});
		}

		// 存入已选择
		selectedValues = new Map(
			Object.entries({
				[item[idKey]]: item
			})
		);

		let foolDisplay = item.refname;
		referVal = item.refcode;
		typeof onChange === 'function' && onChange(item, { display: foolDisplay, value: referVal });
	};

	renderPopoverRight = () => {
		const { initialvalue } = this.props;
		let { value = {}, show, selectedItem = '', metaItem = {}, itemMap, selectedValues } = this.state;
		// 获取公式设置输入框的值，并且将值赋值给公式编辑器中
		let inputValue;
		if (selectedValues) {
			let valueMap = selectedValues.entries().next();
			let entValue = Object.entries(valueMap)[0];
			let resValue = entValue[1];
			if (resValue) {
				inputValue = resValue[1].value;
			}
		}
		return (
			<div>
				<FormulaEditor
					show={show}
					value={inputValue}
					// 公式页签
					formulaConfig={[
						{
							tab: '租赁',
							TabPaneContent: this.getLeaseFormula,
							key: 'leaseFormula',
							params: {}
						},
						{
							tab: '其他类型',
							TabPaneContent: this.getOtherFormula,
							key: 'otherFormula',
							params: {}
						}
					]}
					// 字段页签
					attrConfig={
						initialvalue == '450103528A' ? (
							[
								{
									tab: '单据项目',
									TabPaneContent: this.getBillItem,
									key: 'billItem',
									params: {}
								},
								{
									tab: '计算系数',
									TabPaneContent: this.getCalculatequotiety,
									key: 'calculatequotiety',
									params: {}
								},
								{
									tab: '计算项目',
									TabPaneContent: this.getCalculateitem,
									key: 'calculateitem',
									params: {}
								},
								{
									tab: '元数据',
									TabPaneContent: this.getMetaItem,
									key: 'metaItem',
									params: {}
								}
							]
						) : (
							[
								{
									tab: '单据项目',
									TabPaneContent: this.getBillItem,
									key: 'billItem',
									params: {}
								},
								{
									tab: '元数据',
									TabPaneContent: this.getMetaItem,
									key: 'metaItem',
									params: {}
								}
							]
						)
					}
					// 隐藏不需要显示的公式页签
					noShowFormula={[ '自定义' ]}
					// 隐藏不需要显示的字段页签
					noShowAttr={[ '元数据属性', '表和字段' ]}
					//点击确定回调
					onOk={(value) => {
						let newShow = false;
						let diaName = value;
						Object.keys(this.arritem).map((item) => {
							if (value.indexOf(this.arritem[item].value) > -1) {
								diaName = diaName.replace(this.arritem[item].value, this.arritem[item].display);
							}
						});
						let newValue = {
							refcode: value,
							refname: diaName,
							refpk: '',
							values: {
								code: { display: diaName, scale: '-1', value: value },
								name: { display: diaName, scale: '-1', value: value }
							}
						};
						this.singleSelect(newValue);
						this.setState({ show: newShow, value: newValue });
					}}
					onCancel={(a) => {
						this.setState({ show: false });
					}} //点击确定回调
					onHide={(a) => {
						this.setState({ show: false });
					}}
				/>
			</div>
		);
	};
}
let RefDom = createPage({})(Refer);

export default RefDom;
