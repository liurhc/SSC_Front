import Refer from './refer';

/**
 * 公式编辑器
 * @param {*} props 
 */
export default function(props = {}) {
	let conf = {
		showHistory: false,
		refType: 'grid',
		refName: '公式编辑器',
		isMultiSelectedEnabled: false, //是否多选
		isHasDisabledData: false, //是否有停用属性
		isCacheable: false
	};

	return <Refer {...Object.assign(conf, props)} />;
}
