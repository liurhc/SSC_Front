import { high } from 'nc-lightapp-front';

const { Refer } = high;

/**
 * 租金计算方法
 * @param {*} props 
 */
export default function CalculateMeansRef(props = {}) {
	let conf = {
		multiLang: {
			domainName: 'ampub',
			// currentLocale: 'simpchn',
			moduleId: 'refer_ampub'
		},
		refType: 'grid',
		refName: '租金计算方法',
		refCode: 'ampub.ref.CalculateMeansRef',
		queryGridUrl: '/nccloud/ampub/ref/CalculateMeansRef.do',
		isMultiSelectedEnabled: false, //是否多选
		isHasDisabledData: true, //是否有停用属性
		value: [], // 如果需要多选则必须有这个属性
		columnConfig: [
			{ name: [ 'refer_ampub-000000', 'refer_ampub-000001' ], code: [ 'refcode', 'refname' ] }
		] /*国际化处理：编码，名称*/
	};
	return <Refer {...Object.assign(conf, props)} />;
}
