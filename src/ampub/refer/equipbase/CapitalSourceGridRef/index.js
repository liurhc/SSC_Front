import { high } from 'nc-lightapp-front';

const { Refer } = high;

/**
 * 资金来源
 * @param {*} props 
 */
export default function CapitalSourceRefer(props = {}) {
	let conf = {
		multiLang: {
			domainName: 'ampub',
			// currentLocale: 'simpchn',
			moduleId: 'refer_ampub'
		},
		refType: 'grid',
		refName: 'refer_ampub-000008' /*国际化处理：资金来源*/,
		refCode: 'ampub.ref.capitalSourceGridRef',
		queryGridUrl: '/nccloud/ampub/ref/capitalSourceGridRef.do',
		isMultiSelectedEnabled: false, //是否多选
		isHasDisabledData: true, //是否有停用属性
		value: [], // 如果需要多选则必须有这个属性
		columnConfig: [
			{ name: [ 'refer_ampub-000000', 'refer_ampub-000001' ], code: [ 'refcode', 'refname' ] }
		] /*国际化处理：编码，名称*/
	};
	return <Refer {...Object.assign(conf, props)} />;
}
