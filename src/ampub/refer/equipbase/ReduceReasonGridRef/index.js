import { high } from 'nc-lightapp-front';

const { Refer } = high;

/**
 * 减少原因
 * @param {*} config 
 */
export default function ReduceRefer(config = {}) {
	let conf = {
		multiLang: {
			domainName: 'ampub',
			// currentLocale: 'simpchn',
			moduleId: 'refer_ampub'
		},
		refName: 'refer_ampub-000013' /*国际化处理：减少原因*/,
		refCode: 'ampub.refer.equipbase.ReduceReasonGridRef',
		queryGridUrl: '/nccloud/ampub/ref/reduceReasonGridRef.do',
		refType: 'grid',
		isMultiSelectedEnabled: false, //是否多选
		isHasDisabledData: true, //是否有停用属性
		columnConfig: [
			{ name: [ 'refer_ampub-000006', 'refer_ampub-000007' ], code: [ 'refcode', 'refname' ] }
		] /*国际化处理：原因编码，原因名称*/
	};
	return <Refer {...Object.assign(conf, config)} />;
}
