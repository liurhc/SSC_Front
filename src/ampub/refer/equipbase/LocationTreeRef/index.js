import { high } from 'nc-lightapp-front';
import { conf as unitConf } from '../../../../uapbd/refer/org/AssetOrgGridRef';

const { Refer } = high;

/**
 * 资产位置
 * @param {*} props 
 */
export default function LocationRefer(props = {}) {
	let conf = {
		multiLang: {
			domainName: 'ampub',
			// currentLocale: 'simpchn',
			moduleId: 'refer_ampub'
		},
		refType: 'tree',
		refName: 'refer_ampub-000009' /*国际化处理：资产位置*/,
		refCode: 'ampub.ref.locationTreeRef',
		rootNode: { refname: 'refer_ampub-000010', refpk: '~' } /*国际化处理：位置*/,
		treeConfig: {
			name: [ 'refer_ampub-000000', 'refer_ampub-000001' ],
			code: [ 'refcode', 'refname' ]
		} /*国际化处理：编码，名称*/,
		queryTreeUrl: '/nccloud/ampub/ref/locationTreeRef.do',
		isMultiSelectedEnabled: false, //是否多选
		isHasDisabledData: true, //是否有停用属性
		value: [], // 如果需要多选则必须有这个属性
		unitProps: unitConf
	};
	return <Refer {...Object.assign(conf, props)} />;
}
