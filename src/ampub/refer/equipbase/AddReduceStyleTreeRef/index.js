import { high } from 'nc-lightapp-front';

const { Refer } = high;

/**
 * 增减方式
 * @param {*} props 
 */
export default function AddReduceStyleRefer(props = {}) {
	let conf = {
		multiLang: {
			domainName: 'ampub',
			// currentLocale: 'simpchn',
			moduleId: 'refer_ampub'
		},
		refType: 'tree',
		refName: 'refer_ampub-000003' /*国际化处理：增减方式*/,
		refCode: 'ampub.ref.addReduceStyleTreeRef',
		rootNode: { refname: 'refer_ampub-000003', refpk: '~' } /*国际化处理：增减方式*/,
		queryTreeUrl: '/nccloud/ampub/ref/addReduceStyleTreeRef.do',
		isMultiSelectedEnabled: false, //是否多选
		isHasDisabledData: true, //是否有停用属性
		value: [], // 如果需要多选则必须有这个属性
		treeConfig: {
			name: [ 'refer_ampub-000000', 'refer_ampub-000001' ],
			code: [ 'refcode', 'refname' ]
		} /*国际化处理：编码，名称*/
	};
	return <Refer {...Object.assign(conf, props)} />;
}
