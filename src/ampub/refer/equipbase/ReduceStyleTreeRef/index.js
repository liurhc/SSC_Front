import { high } from 'nc-lightapp-front';

const { Refer } = high;

/**
 * 减少方式
 * @param {*} props 
 */
export default function ReduceStyleRefer(props = {}) {
	let conf = {
		multiLang: {
			domainName: 'ampub',
			// currentLocale: 'simpchn',
			moduleId: 'refer_ampub'
		},
		refType: 'tree',
		refName: 'refer_ampub-000014' /*国际化处理：减少方式*/,
		rootNode: { refname: 'refer_ampub-000014', refpk: '~' } /*国际化处理：减少方式*/,
		refCode: 'ampub.ref.ReduceStyleTreeRef',
		queryTreeUrl: '/nccloud/ampub/ref/ReduceStyleTreeRef.do',
		isMultiSelectedEnabled: false, //是否多选
		isHasDisabledData: true, //是否有停用属性
		treeConfig: {
			name: [ 'refer_ampub-000000', 'refer_ampub-000001' ],
			code: [ 'refcode', 'refname' ]
		} /*国际化处理：编码，名称*/,
		// onlyLeafCanSelect: true,
		value: [] // 如果需要多选则必须有这个属性
	};
	return <Refer {...Object.assign(conf, props)} />;
}
