import { high } from 'nc-lightapp-front';

const { Refer } = high;

export default function DisposemeansGridRef(props = {}) {
	let conf = {
		multiLang: {
			domainName: 'ampub',
			// currentLocale: 'simpchn',
			moduleId: 'refer_ampub'
		},
		refType: 'grid',
		refName: 'refer_ampub-000019' /*国际化处理：更换备件处理方法*/,
		refCode: 'ampub.ref.DisposemeansGridRef',
		queryGridUrl: '/nccloud/ampub/ref/DisposemeansGridRef.do',
		isMultiSelectedEnabled: false,
		value: [], // 如果需要多选则必须有这个属性
		columnConfig: [
			{ name: [ 'refer_ampub-000000', 'refer_ampub-000001' ], code: [ 'refcode', 'refname' ] }
		] /*国际化处理：编码，名称*/
	};
	return <Refer {...Object.assign(conf, props)} />;
}
