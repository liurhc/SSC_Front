import { high } from 'nc-lightapp-front';
import { conf as unitConf } from '../../../../uapbd/refer/org/BusinessUnitTreeRef';

const { Refer } = high;

/**
 * 班组
 * @param {*} props 
 */
export default function TeamGridRef(props = {}) {
	let conf = {
		multiLang: {
			domainName: 'ampub',
			moduleId: 'refer_ampub'
		},
		refType: 'grid',
		refName: 'refer_ampub-000020' /*国际化处理：班组*/,
		refCode: 'ampub.ref.TeamGridRef',
		queryGridUrl: '/nccloud/ampub/ref/TeamGridRef.do',
		isMultiSelectedEnabled: false, //是否多选
		isHasDisabledData: true, //是否有停用属性
		value: [], // 如果需要多选则必须有这个属性
		columnConfig: [
			{ name: [ 'refer_ampub-000000', 'refer_ampub-000001' ], code: [ 'refcode', 'refname' ] }
		] /*国际化处理：编码，名称*/,
		unitProps: unitConf,
		isHasDisabledData: true //有停用属性
	};
	return <Refer {...Object.assign(conf, props)} />;
}
