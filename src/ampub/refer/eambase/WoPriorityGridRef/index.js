import { high } from 'nc-lightapp-front';

const { Refer } = high;

/**
 * 工单优先级
 * @param {*} props 
 */
export default function WoPriorityGridRef(props = {}) {
	let conf = {
		multiLang: {
			domainName: 'ampub',
			moduleId: 'refer_ampub'
		},
		refType: 'grid',
		refName: 'refer_ampub-000018' /*国际化处理：工单优先级*/,
		refCode: 'ampub.ref.WoPriorityGridRef',
		queryGridUrl: '/nccloud/ampub/ref/WoPriorityGridRef.do',
		isMultiSelectedEnabled: false, //是否多选
		isHasDisabledData: true, //是否有停用属性
		value: [], // 如果需要多选则必须有这个属性
		columnConfig: [
			{ name: [ 'refer_ampub-000000', 'refer_ampub-000001' ], code: [ 'refcode', 'refname' ] }
		] /*国际化处理：编码，名称*/,
		isHasDisabledData: true //有停用属性
	};
	return <Refer {...Object.assign(conf, props)} />;
}
