import { high } from 'nc-lightapp-front';
import { conf as unitConf } from '../../../../uapbd/refer/org/BusinessUnitTreeRef';

const { Refer } = high;

/**
 * 工作类型
 * @param {*} props 
 */
export default function WorktypeGridRef(props = {}) {
	let conf = {
		multiLang: {
			domainName: 'ampub',
			moduleId: 'refer_ampub'
		},
		refType: 'grid',
		refName: 'refer_ampub-000016' /*国际化处理：工作类型*/,
		refCode: 'ampub.ref.WorkTypeGridRef',
		queryGridUrl: '/nccloud/ampub/ref/WorkTypeGridRef.do',
		isMultiSelectedEnabled: false, //是否多选
		isHasDisabledData: true, //是否有停用属性
		columnConfig: [
			{ name: [ 'refer_ampub-000000', 'refer_ampub-000001' ], code: [ 'refcode', 'refname' ] }
		] /*国际化处理：编码，名称*/,
		unitProps: unitConf,
		isHasDisabledData: true //有停用属性
	};
	return <Refer {...Object.assign(conf, props)} />;
}
