import { high } from 'nc-lightapp-front';
import { conf as unitConf } from '../../../../uapbd/refer/org/BusinessUnitTreeRef';

const { Refer } = high;

/**
 * 专业档案
 * @param {*} props 
 */
export default function SpecialtyGridRef(props = {}) {
	let conf = {
		multiLang: {
			domainName: 'ampub',
			// currentLocale: 'simpchn',
			moduleId: 'refer_ampub'
		},
		refType: 'grid',
		refName: 'refer_ampub-000002' /*国际化处理：专业档案*/,
		refCode: 'ampub.ref.SpecialtyGridRef',
		queryGridUrl: '/nccloud/ampub/ref/SpecialtyGridRef.do',
		isMultiSelectedEnabled: false, //是否多选
		isHasDisabledData: true, //是否有停用属性
		columnConfig: [
			{ name: [ 'refer_ampub-000000', 'refer_ampub-000001' ], code: [ 'refcode', 'refname' ] }
		] /*国际化处理：编码，名称*/,
		unitProps: unitConf
	};
	return <Refer {...Object.assign(conf, props)} />;
}
