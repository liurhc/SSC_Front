import Refer from './refer';

/**
 * 租金计算周期
 * @param {*} props 
 */
export default function(props = {}) {
	let conf = {
		multiLang: {
			domainName: 'ampub',
			moduleId: 'refer_ampub'
		},
		showHistory: false,
		refType: 'grid',
		refName: '租金计算周期',
		refCode: 'ampub.ref.CountCycle',
		isMultiSelectedEnabled: false, //是否多选
		isHasDisabledData: false //是否有停用属性
	};
	//popWindowClassName是添加的类名，对样式进行更多的操作
	return <Refer popWindowClassName={'accept-user-ref master-table'} {...Object.assign(conf, props)} />;
}
