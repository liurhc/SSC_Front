import React from 'react';
import { high, base } from 'nc-lightapp-front';
import { getContext, setContext } from '../../../common/components/AMInitInfo/loginContext';
import './index.less';

const { NCFormControl } = base;
const PopRefer = high.Refer.PopRefer;
const { NCButton } = base;
const { NCRadio } = base;

/**
 * 计租方式
 */
class Ref extends PopRefer {
	constructor(props) {
		super(props);
		this.state = {
			...this.state,
			//后台数据格式
			value: {
				type: 0,
				year: 0,
				halfYear: 0,
				season: 0,
				month: 0,
				xun: 0,
				week: 0,
				day: 0,
				leaseType: 0
			},
			showLists: [] //显示部分的数据数组
		};
	}

	//渲染弹出层内容区的右侧
	renderPopoverRight = () => {
		let { value } = this.state;
		return (
			<div>
				<div>选择日期间隔，然后选择“预览”查看日期</div>
				<div id="renderPopover">
					<div className="content-choose">
						<NCRadio.NCRadioGroup
							name="team"
							selectedValue={value.type}
							onChange={(value) => {
								this.handleChange(value);
							}}
							className="radioGroup"
						>
							<div>
								<NCRadio value={0}>单次</NCRadio>
							</div>
							<div>
								<NCRadio value={8}>工作量</NCRadio>
							</div>
							<div>
								<NCRadio value={1} />
								<span>每</span>
								<NCFormControl
									value={value.day}
									//判断是否禁用
									disabled={value.type == 1 ? false : true}
									min="1"
									max="365"
									type="number"
									onChange={(newVal) => {
										this.handleChange(1, 'day', newVal);
									}}
								/>
								<span>天</span>
							</div>
							<div>
								<NCRadio value={9} />
								<span>每</span>
								<NCFormControl
									value={value.month}
									disabled={value.type == 9 ? false : true}
									min="1"
									type="number"
									onChange={(newVal) => {
										this.handleChange(9, 'month', newVal);
									}}
								/>
								<span>月的第</span>
								<NCFormControl
									value={value.xun}
									disabled={value.type == 9 ? false : true}
									min="1"
									max="31"
									type="number"
									onChange={(newVal) => {
										this.handleChange(9, 'xun', newVal);
									}}
								/>
								<span>天</span>
							</div>
						</NCRadio.NCRadioGroup>
					</div>
					<div className="content-show">
						<div className="previewButton">
							<NCButton
								className="preview"
								shape="border"
								colors="primary"
								onClick={() => {
									this.preview(value.type);
								}}
							>
								预览
							</NCButton>
						</div>
						<div className="previewArea">
							{value.type == 0 && this.romance()}
							{value.type == 8 && this.romance()}
							{value.type == 1 && this.romance()}
							{value.type == 9 && this.romance()}
						</div>
					</div>
				</div>
			</div>
		);
	};

	//把要预览的内容渲染到预览区
	romance = () => {
		let { showLists } = this.state;
		return (
			<div>
				{showLists.length > 0 &&
					showLists.map((item) => {
						return <p>{item}</p>;
					})}
			</div>
		);
	};

	/**
	 *  单选框和上下调节     
	 * @param{*}type:代表选择第几个单选框
	 * @param{*}valueKey:代表对哪个输入框进行操作
	 * @param{*}newVal:代表输入框内目前的值
	 **/
	handleChange = (type = 0, valueKey, newVal = 1) => {
		let { value } = this.state;
		switch (valueKey) {
			case 'day':
				value.day = newVal;
				break;
			case 'month':
				value.month = newVal;
				break;
			case 'xun':
				value.xun = newVal;
				break;
			default:
				break;
		}
		value.type = type;
		this.setState({ value });
	};

	/**
	 * 
	 *  日期间隔方式预览情况     
	 **/
	//单次和工作量的 日期间隔方式预览情况
	styleOnce = (type) => {
		let { showLists = [] } = this.state;
		showLists = [];
		if (type == 0) {
			showLists.push('一次性缴纳');
		} else if (type == 8) {
			showLists.push('工作量计租');
		}
		this.setState({ showLists });
	};

	//每xxx天的 日期间隔方式预览情况
	styleEveryday = () => {
		let { value, showLists = [] } = this.state;
		showLists = [];
		//获取当前日期后一天的毫秒数
		let nowDate = new Date().getTime() + 24 * 60 * 60 * 1000;
		let addDay = value.day * 24 * 60 * 60 * 1000;
		for (let i = 0; i < 10; i++) {
			let date = new Date(nowDate + addDay * i);
			let year = date.getFullYear();
			let month = date.getMonth() + 1;
			let day = date.getDate();
			if (month >= 1 && month <= 9) {
				month = '0' + month;
			}
			if (day >= 1 && day <= 9) {
				day = '0' + day;
			}
			let currentdate = year + '-' + month + '-' + day;
			showLists.push(currentdate);
		}
		this.setState({ showLists });
	};

	//每xx月的第xx天的 日期间隔方式预览情况
	styleMonthday = () => {
		// let currentdate;
		let { value, showLists = [] } = this.state;
		showLists = [];
		let nowDate = new Date().getTime();
		let addDay = value.month * 24 * 60 * 60 * 1000 * 31;
		for (let i = 0; i < 10; i++) {
			let date = new Date(nowDate + addDay * i);
			let year = date.getFullYear();
			let month = date.getMonth() + 1;
			let day = value.xun;
			if (month >= 1 && month <= 9) {
				month = '0' + month;
			}
			if (day >= 1 && day <= 9) {
				day = '0' + day;
			}
			let currentdate = year + '-' + month + '-' + day;
			//计算每月的最后一天
			let dayCount = new Date(currentdate.split('-')[0], currentdate.split('-')[1], 0).getDate();
			//每月的第31天代表每月的最后一天，如果输入的天数大于这一月的最后一天则显示这月的最后一天
			if (day > 30) {
				showLists.push(year + '-' + month + '-' + dayCount);
			} else if (day > dayCount) {
				showLists.push(year + '-' + month + '-' + dayCount);
			} else {
				showLists.push(currentdate);
			}
		}
		this.setState({ showLists });
	};

	//预览按钮调用的方法：预览的内容
	preview = (type) => {
		let { value } = this.state;
		//自定义设置变量value.type的值
		setContext('valueKey', value.type, 'preview');
		//根据单选框的选择来决定预览哪部分的内容
		type == '0' && this.styleOnce(type);
		type == '8' && this.styleOnce(type);
		type == '1' && this.styleEveryday();
		type == '9' && this.styleMonthday();
	};

	//显示参照
	show = () => {
		let { value } = this.state;
		let tableVal = this.props.foolValue;
		let techvalues = {};
		value.day = 1;
		value.month = 1;
		value.xun = 1;
		if (tableVal && tableVal.value) {
			techvalues = JSON.parse(tableVal.value);
		}
		//再次打开参照显示上一次的操作内容
		if (JSON.stringify(techvalues) != '{}') {
			value.type = techvalues.type;
			if (value.type == 1) {
				value.day = techvalues.day;
			} else if (value.type == 9) {
				value.month = techvalues.month;
				value.xun = techvalues.xun;
			}
		}

		//在点击预览时候获取变量value.type的值
		let valueType = getContext('valueKey', 'preview');
		this.preview(valueType);

		this.setState({
			isShow: true,
			isFirstShow: false,
			dropDownShow: false,
			isSelectedShow: false,
			selectedShow: false,
			max: false
		});
	};

	//复写的空方法
	loadTableData = async () => {};
	dropDownSearch = () => {};

	//确定按钮（点击确定按钮会走这个方法）
	singleSelect = () => {
		let { value } = this.state;

		let showname;

		if (value.type == 0) {
			showname = '一次性缴纳';
			value.day = 0;
			value.month = 0;
			value.xun = 0;
			value.leaseType = 1;
		} else if (value.type == 8) {
			showname = '工作量计租';
			value.day = 0;
			value.month = 0;
			value.xun = 0;
			value.leaseType = 2;
		} else if (value.type == 1) {
			showname = '每' + value.day + '天';

			value.month = 0;
			value.xun = 0;
			value.leaseType = 3;
		} else if (value.type == 9) {
			showname = '每' + value.month + '月的第' + value.xun + '天';
			value.day = 0;
			// value.xun = 0;
			value.leaseType = 4;
		}
		let stringJson = JSON.stringify(value);
		let selections = {
			refname: showname, // display
			refpk: stringJson
		};

		//平台固定的写法
		let { onChange, onBlur } = this.props;
		this.setState(
			{
				referVal: '',
				isShow: false,
				isMoreButtonShow: false
			},
			() => {
				let foolDisplay = selections.refname,
					foolValue = selections.refpk;
				typeof onChange === 'function' && onChange(selections, { display: foolDisplay, value: foolValue });
				typeof onBlur === 'function' && onBlur(selections, { display: foolDisplay, value: foolValue });
			}
		);
	};
}

export default Ref;
