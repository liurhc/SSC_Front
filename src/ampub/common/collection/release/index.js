/**
 * 领域内公共方法、组件、常量 统一打包输出
 */
//components
import * as AMsessionStorage from '../../components/AMDB/AMsessionStorage';
import * as LoginContext from '../../components/AMInitInfo/loginContext';
import AMRefer from '../../components/AMRefer/AMRefer';
import ApprovalTrans from '../../components/ApprovalTrans/ApprovalTrans';
import ExportArea from '../../components/ExportArea/ExportArea';
import * as imageHandle from '../../components/ImageMng/imageHandle';
import loadAMapJs from '../../components/LoadAMapJs/loadAMapJs';
import * as ModuleConst from '../../components/ModuleInfo/ModuleConst';
import * as ModuleEnable from '../../components/ModuleInfo/ModuleEnable';
import * as assetOrgMultiRefFilter from '../../components/MultiRefFilter/AssetOrgMultiRefFilter';
import * as maintainOrgMultiRefFilter from '../../components/MultiRefFilter/MaintainOrgMultiRefFilter';
import * as OtherOrgMultiRefFilter from '../../components/MultiRefFilter/OtherOrgMultiRefFilter';
import * as OrgChangeEvent from '../../components/OrgChangeUtils/orgChangeEvent';
import * as faQueryAboutUtils from '../../components/QueryAbout/faQueryAboutUtils';
import * as queryAboutUtils from '../../components/QueryAbout/queryAboutUtils';
import * as queryVocherSrcUtils from '../../components/QueryVocher/queryVocherSrcUtils';
import * as queryVocherUtils from '../../components/QueryVocher/queryVocherUtils';
import * as EquipQueryCondition from '../../components/ReferInit/EquipQueryCondition';
import * as RefFilterForSearchArea from '../../components/ReferInit/RefFilterForSearchArea';
import * as refInit from '../../components/ReferInit/refInit';
import * as saveValidatorsUtil from '../../components/SaveValidator/saveValidatorsUtil';
import * as ScriptReturnUtils from '../../components/ScriptReturn/ScriptReturnUtils';

//const
import * as CommonKeys from '../../const/CommonKeys';
import * as StatusUtils from '../../const/StatusUtils';
import * as VoucherConst from '../../const/VoucherConst';

//transfer，禁止使用，请使用utils/transferUtils
// import * as Transfer from '../../transfer';
// import * as TansferUtils from '../../transfer/TansferUtils';

//utils
import * as cardUtils from '../../utils/cardUtils';
import closeBrowserUtils from '../../utils/closeBrowserUtils';
import * as listUtils from '../../utils/listUtils';
import * as msgUtils from '../../utils/msgUtils';
import * as multiLangUtils from '../../utils/multiLangUtils';
import * as reportDrillUtils from '../../utils/reportDrillUtils';
import * as tableUtils from '../../utils/tableUtils';
import * as billLinkQueryUtils from '../../utils/billLinkQueryUtils';
import * as transferUtils from '../../utils/transferUtils';

let components = {
	AMsessionStorage, //资产sessionStorage统一处理类(后续扩展加密等处理)
	LoginContext, //上下文变量的相关方法
	AMRefer, //参照组件
	ApprovalTrans, //提交指派
	ExportArea, //导入导出组件
	imageHandle, //扫描影像和影像查看
	loadAMapJs, //加载地图方法
	ModuleConst, //模块功能号常量
	ModuleEnable, //判断模块是否启用
	assetOrgMultiRefFilter, //业务单元参照过滤相关方法
	maintainOrgMultiRefFilter, //维修组织过滤相关方法
	OtherOrgMultiRefFilter, //财务组织过滤相关方法
	OrgChangeEvent, //组织切换事件弹窗
	faQueryAboutUtils, //联查固定资产卡片相关方法
	queryAboutUtils, //联查设备卡片相关方法
	queryVocherSrcUtils, //凭证联查来源单据
	queryVocherUtils, //联查凭证，调用凭证相关方法
	EquipQueryCondition, //初始化查询控件控制相关事件方法。
	RefFilterForSearchArea, //参照过滤相关方法
	refInit, //公共参照自定义参照处理相关方法
	saveValidatorsUtil, //保存前数据校验
	ScriptReturnUtils //各种返回刷新处理相关方法
};
let commonConst = {
	CommonKeys, //跨组织选部门选人员场景常量,按钮组常量类,转单相关常量,动作脚本相关常量
	StatusUtils, //视图界面状态常量,VO状态常量,单据状态常量
	VoucherConst //联查凭证通用页面配置
};
//transfer，禁止使用，请使用utils/transferUtils
// let transfer = {
// 	Transfer, //增加拉单按钮时设置业务类型的缓存的相关方法
// 	TansferUtils //转单查询页面的查询方法等相关方法
// };
let utils = {
	cardUtils, //卡片界面相关方法
	closeBrowserUtils, //浏览器关闭或者刷新时候弹出默认提示框
	listUtils, //列表界面相关方法
	msgUtils, //确定框，信息提示相关方法
	multiLangUtils, //获取、设置多语相关方法
	reportDrillUtils, //报表联查单据,报表联查固定资产卡片相关方法
	tableUtils, //提交前数据转换,停启用状态转换等相关方法
	billLinkQueryUtils, //单据联查相关方法
	transferUtils //转单相关方法
};
let ampub = {
	components,
	commonConst,
	// transfer,
	utils
};

export default ampub;

// import {
// 	getScriptListReturn,
// 	getScriptCardReturn,
// 	getScriptCardReturnData,
// 	getScriptExtCardReturn,
// 	setBatchBtnsEnable,
// 	getScriptListParam,
// 	getScriptCardParam,
// 	ReturnCooperateData
// } from '../../components/ScriptReturn/ScriptReturnUtils';
// import {
// 	openEquipCard,
// 	openEquipCardByPk,
// 	openBillTrack,
// 	openApprove,
// 	openListBillTrack,
// 	openListApprove,
// 	openReportEquip,
// 	LinkReportConst
// } from '../../components/QueryAbout/queryAboutUtils';
// import {
// 	doTansferSearch,
// 	queryAppCodeByTransType,
// 	pushToTransferPage,
// 	pushToTransferCard,
// 	addLinkToTemplate,
// 	addLinkToTemplateFull,
// 	addReturnBtn,
// 	commonMetaHandle,
// 	pushToTransferPageNew
// } from '../../transfer/TansferUtils';
// import {
// 	cardConst,
// 	setBillFlowBtnsVisible,
// 	createCardTitleArea,
// 	setHeadAreaData,
// 	createCardPaginationArea,
// 	createOprationColumn as cardCreateOprationColumn,
// 	setCardValue,
// 	afterModifyCardMeta,
// 	getCommonTableHeadLeft,
// 	commonHeadAfterEvent,
// 	commonBodyAfterEvent,
// 	createHeadAfterEventData,
// 	createBodyAfterEventData,
// 	updateHeadData,
// 	updateBodyData,
// 	setCooperateBtnEnable as cardSetCooperateBtnEnable
// } from '../../utils/cardUtils';
// import {
// 	listConst,
// 	setBillFlowBtnsEnable,
// 	createOprationColumn as listCreateOprationColumn,
// 	setListValue,
// 	setQueryInfoCache,
// 	batchRefresh,
// 	setCooperateBtnEnable as listSetCooperateBtnEnable
// } from '../../utils/listUtils';
// import {
// 	tableConst,
// 	modifyBeforeAjaxByIndex,
// 	modifyBeforeAjax,
// 	modifyAfterAjaxByIndex,
// 	modifyAfterAjax,
// 	getGroupRowIndexs,
// 	createOprationColumn,
// 	createEnableSwitch,
// 	setTableValue
// } from '../../utils/tableUtils';
// let components={
// openEquipCard,
// openEquipCardByPk,
// openBillTrack,
// openApprove,
// openListBillTrack,
// openListApprove,
// openReportEquip,
// LinkReportConst,
// getScriptListReturn,
// getScriptCardReturn,
// getScriptCardReturnData,
// getScriptExtCardReturn,
// setBatchBtnsEnable,
// getScriptListParam,
// getScriptCardParam,
// ReturnCooperateData,
// }
// let transfer={
// doTansferSearch,
// queryAppCodeByTransType,
// pushToTransferPage,
// pushToTransferCard,
// addLinkToTemplate,
// addLinkToTemplateFull,
// addReturnBtn,
// commonMetaHandle,
// pushToTransferPageNew,
// }
// let utils={
//cardConst,
// setBillFlowBtnsVisible,
// createCardTitleArea,
// setHeadAreaData,
// createCardPaginationArea,
// cardCreateOprationColumn,
// setCardValue,
// afterModifyCardMeta,
// getCommonTableHeadLeft,
// commonHeadAfterEvent,
// commonBodyAfterEvent,
// createHeadAfterEventData,
// createBodyAfterEventData,
// updateHeadData,
// updateBodyData,
//cardSetCooperateBtnEnable,
//listConst,
// setBillFlowBtnsEnable,
// listCreateOprationColumn,
// setListValue,
// setQueryInfoCache,
// batchRefresh,
// listSetCooperateBtnEnable,
// tableConst,
// modifyBeforeAjaxByIndex,
// modifyBeforeAjax,
// modifyAfterAjaxByIndex,
// modifyAfterAjax,
// getGroupRowIndexs,
// createOprationColumn,
// createEnableSwitch,
// setTableValue,
// }
