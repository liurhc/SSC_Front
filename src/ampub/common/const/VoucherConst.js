// 联查凭证通用页面配置
export const pageConfig = {
	appCode: '10170410',
	pageCode: '10170410_1017041001',

	linkCacheWord: '_LinkVouchar', // 联查凭证标识
	makeCacheWord: '_MadeBill2019', // 生成凭证标识
	previewCacheWord: '_Preview', // 预览凭证标识

	url: {
		queryVoucherUrl: '/nccloud/ampub/common/voucherQuery.do',
		generateVoucherUrl: '/nccloud/ampub/common/voucherGenerate.do',
		queryRelationIDUrl: '/nccloud/fa/fapub/queryResourceid.do',
		queryVoucherSrcUrl: '/nccloud/ampub/common/voucherSrcQuery.do'
	},
	//不需要特殊联查的单据
	billtypeNoLink: [
		'HQ', //减值准备
		'HE' //评估单据
	],
	//需要特殊处理的单据
	billtypeToHandle: [
		'H0', //原始卡片增加
		'H1' //新增卡片增加
	],
	//需要支持预览凭这个的单据
	billtypeToPreview: [
		'HF', //资产减少
		'HG' //资产变动
	],
	relatReg: 'h-z', //连接卡片主键和账簿主键的标记
	cardRouter: '/card' //卡片态路由
};
