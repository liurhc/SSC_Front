//跨组织选部门选人员场景（业务人员来源）
export const IBusiRoleConst = {
	ASSETORG: 'as', //资产
	MAINTAINORG: 'ma' //维修
};

//按钮组常量类（先抽取转单，单据常用较多暂时按照统一规范进行注册使用）
export const ButtonConst = {
	AddTransferGroup: 'AddTransferGroup' //转单按钮组
};

// 转单相关常量（url中变量的名字）
export const TransferConst = {
	src_appcode: 'src_appcode', //下游往上游传的参数，上游的appCode
	src_tradetype: 'src_tradetype', //下游往上游传的参数，查询用的transiType
	src_pagecode: 'src_pagecode',
	dest_billtype: 'dest_billtype',
	dest_tradetype: 'dest_tradetype',
	dest_pagecode: 'dest_pagecode',
	type: 'transfer', //上游往下游传的参数，转单类型
	dataSource: 'transfer.dataSource', //下下游之间传参用的datasource
	query_appcode: 'query_appcode', //后台查询appcode缓存用
	srctype: 'type', // 类型，用于区分是转单页面跳转过来的
	src_billtype_cache: 'src_billtype_cache', // 加载拉单按钮时缓存单据类型用
	src_transtype_cache: 'src_transtype_cache', // 加载拉单按钮时缓存交易类型用
	bodyPkAndPkEquip: 'bodyPkAndPkEquip' // 检验记录拉单专用
};
//动作脚本相关常量
export const approveConst = {
	ALLSUCCESS: 'allsuccess', //全部成功
	PARTIALSUCCESS: 'partialsuccess', //部分成功
	NOSUCCESS: 'nosuccess', //全部失败
	HASAPPROVEALTRAN: 'hasapprovealtran', //有指派信息
	APPROVEFLOW: 'approveflow', //审批流
	WORKFLOW: 'workflow' //工作流
};

//联查相关常量(与后端nccloud.pub.ampub.common.LinkQueryConst保持一致)
export const linkQueryConst = {
	ID: 'id',
	TRANSTYPE: 'transtype', //交易类型字段（后端接收使用）
	SCENE: 'scene', //区分场景字段
	SCENETYPE: {
		defaultsce: 'defaultsce', //联查场景：默认
		approvesce: 'approvesce', //联查场景：审批
		linksce: 'linksce', //联查场景：联查（单据追溯或领域内联查）
		othersce: 'othersce' //联查场景：其它
	},
	APPCODE: 'appcode',
	PAGECODE: 'pagecode',
	URL: 'url'
};
