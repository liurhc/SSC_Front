//视图界面状态
export const UISTATE = {
	add: 'add',
	edit: 'edit',
	browse: 'browse',
	blank:'blank',//卡片空白页
	copyAdd:'copyAdd',//复制新增
	transferAdd: 'transferAdd', //转单新增
	transferBorwse: 'transferBorwse', //转单浏览
	transferEdit: 'transferEdit' //转单编辑
};
//VO状态
export const VOSTATUS = {
	/** 初始 */
	UNCHANGED: '0',
	/** 已修改 */
	UPDATED: '1',
	/** 新增 */
	NEW: '2',
	/** 已删除 */
	DELETED: '3'
};
//单据状态
export const BILLSTATUS = {
	/** 暂存态 */
	temp_save: '-1',
	/** 自由态 */
	free_check: '0',
	/** 未审核 */
	un_check: '1',
	/** 审核中 */
	check_going: '2',
	/** 审核通过 */
	check_pass: '3',
	/** 审核未通过 */
	check_nopass: '4',
	/** 已确认 */
	// confirm: '5';
	/**关闭*/
	close: '6'
};
