import React, { Component } from 'react';
import { base, toast, ajax } from 'nc-lightapp-front';
import { getMultiLangByID } from '../../utils/multiLangUtils';
import './ExportArea.less';

const { NCUpload } = base;

/**
 * 导入导出组件
 */
export default class ExportArea extends Component {
	constructor(props) {
		super(props);
		this.state = {
			cardStatus: '', //小卡片状态
			waitingMsg: '.', //等待时候的信息
			responseMsg: {
				successMsg: 0,
				failMsg: 0
			}, //返回信息
			failvo: 'failvo'
		};
		this.searchinfo = {};
	}

	// 导出
	onExport = (props, pagecode, failvo, exportUrl) => {
		let data = {
			pagecode: pagecode,
			failvo: failvo ? JSON.stringify(failvo) : ''
		};
		props.downLoad({ data, url: exportUrl });
	};
	// 跳转到对应页面
	linkTo = (props, linkUrl, linkAppCode, linkPageCode) => {
		props.openTo(linkUrl, {
			appcode: linkAppCode,
			pagecode: linkPageCode
		});
	};
	// 跳转到配置模板界面
	configTemplate = (props, pagecode, failvo, exportUrl, fourAppCode) => {
		props.openTo('TemplateSetting', {
			appcode: '10180TM',
			pagecode: '10180TM_PAGE',
			// 给平台传递pagecode和菜单编码-fourAppCode，来定位跳转后需要显示的应用
			code: pagecode,
			fourAppCode: fourAppCode,
			// 给平台提供一个来源参数，让平台区分此请求是来源于资产导入界面的
			pageName: 'equiptool'
		});
	};

	render() {
		let { cardStatus, responseMsg, failvo, pk_org, pk_accbook, date } = this.state;
		let { importConfig, nowData = {} } = this.props;

		let {
			pagecode,
			importUrl,
			exportUrl,
			importTitle,
			exportTitle,
			configTemplate,
			linkApp,
			linkGlbUrl,
			linkGrpUrl,
			linkOrgUrl,
			linkGlbAppCode,
			linkGrpAppCode,
			linkOrgAppCode,
			linkPageCode,
			imgUrl,
			cardSearchArea,
			fourAppCode
		} = importConfig;
		let _this = this;
		let config = {
			showUploadList: false,
			name: 'file',
			accept: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel', //限制上传文件类型为*xls,*.xlsx
			data: {
				pagecode,
				pk_org: nowData.pk_org,
				pk_accbook: nowData.pk_accbook
			},
			action: importUrl,
			headers: {
				authorization: 'authorization-text'
			},
			resultInfo: {
				successLength: '0',
				failLength: '0',
				failvo
			},
			beforeUpload() {
				if (cardStatus == 'waiting') {
					toast({ content: getMultiLangByID('ExportArea-000000'), color: 'warning' }); /*国际化处理：'文件正在导入中'*/
					return false;
				}
				if (pagecode == '201201004A_asset') {
					if (!nowData.pk_org || !nowData.pk_accbook) {
						toast({
							content: getMultiLangByID('ExportArea-000001'),
							color: 'warning'
						}); /*国际化处理：'组织和账簿不能为空'*/
						return false;
					}
				}
			},
			NcUploadOnChange(info) {
				if (info.file.status === 'uploading') {
					_this.setState({ cardStatus: 'waiting' });
				} else if (info.file.status === 'done') {
					//记录成功或失败信息
					if (info.file.response.error) {
						if (info.file.response.error.message) {
							let message = info.file.response.error.message;
							//toast({ content: message, color: 'warning' });
							let arrmes = message.split('!');
							toast({
								duration: 'infinity',
								content: arrmes.shift(),
								color: 'danger',
								groupOperation: true,
								groupOperationMsg: arrmes,
								TextArr: [
									getMultiLangByID('amcommon-000005'),
									getMultiLangByID('amcommon-000006'),
									getMultiLangByID('amcommon-000007')
								] /*国际化处理：展开 ， 收起 ， 关闭*/
							});
							_this.setState({ cardStatus: '' });
						}
					}
					if (info.file.response.success) {
						let successMsg = info.file.response.data.success ? info.file.response.data.success.length : 0;
						let failMsg = info.file.response.data.fail ? info.file.response.data.fail.length : 0;
						let urlmode;
						let linkUrl;
						let linkAppCode;
						if (info.file.response.data.fail) {
							config.resultInfo.failLength = info.file.response.data.fail.length;
							let failvos = [ ...info.file.response.data.fail ];
							if (info.file.response.data.fail[0]) {
								if ((urlmode = info.file.response.data.fail[0].tempValueMap)) {
									urlmode = info.file.response.data.fail[0].tempValueMap.mode;
								}
							}
							_this.setState({ failvo: failvos });
						}
						if (!urlmode) {
							if (info.file.response.data.success[0]) {
								if ((urlmode = info.file.response.data.success[0].tempValueMap)) {
									urlmode = info.file.response.data.success[0].tempValueMap.mode;
								}
							}
						}
						if (urlmode == 1) {
							linkUrl = linkGlbUrl;
							linkAppCode = linkGlbAppCode;
						} else if (urlmode == 2 || urlmode == 5) {
							linkUrl = linkGrpUrl;
							linkAppCode = linkGrpAppCode;
						} else if (urlmode == 3 || urlmode == 4 || urlmode == 6) {
							linkUrl = linkOrgUrl;
							linkAppCode = linkOrgAppCode;
						}
						_this.setState({
							cardStatus: 'success',
							responseMsg: { successMsg, failMsg, linkUrl, linkAppCode }
						});
					}
					if (!info.file.response.success && !info.file.response.error) {
						_this.setState({ cardStatus: '' });
						toast({ content: getMultiLangByID('ExportArea-000005'), color: 'warning' }); /*国际化处理：导入失败*/
					}
				} else if (info.file.status === 'error') {
					_this.setState({ cardStatus: '' });
					toast({ content: getMultiLangByID('ExportArea-000005'), color: 'warning' }); /*国际化处理：导入失败*/
				}
			}
		};

		return (
			<div className="import-info">
				<div className="import-title">
					<p>{importTitle}</p>
					<div>
						<p
							onClick={() => {
								this.onExport(_this.props, pagecode, null, exportUrl);
							}}
						>
							{exportTitle}
						</p>
						<p
							onClick={() => {
								this.configTemplate(_this.props, pagecode, null, exportUrl, fourAppCode);
							}}
						>
							{configTemplate}
						</p>
					</div>
				</div>
				{cardSearchArea}
				<div className="import-content">
					<div className="content-card">
						<NCUpload {...config}>
							<div className="import-txt">
								<span className={`${imgUrl} content-img`} />
								<p>
									<span className="iconfont icon-tianjiayingyong" />
									{getMultiLangByID('ExportArea-000006')}
									{importTitle}
								</p>
								{/*国际化处理：导入*/}
							</div>
						</NCUpload>
						{cardStatus == 'waiting' ? <p className="disabled-node " /> : ''}
					</div>
					<div className="content-msg">
						{cardStatus === '' && (
							<div className="content-msg-info">
								<span className="content-img noData" />
								<span>{getMultiLangByID('ExportArea-000007')}</span>
								{/*国际化处理：暂无数据*/}
							</div>
						)}
						{cardStatus === 'waiting' && (
							<div className="waiting">
								<p>
									{importTitle}
									{getMultiLangByID('ExportArea-000008')}. . .
								</p>
								{/*国际化处理：正在导入中*/}
								<p>{getMultiLangByID('ExportArea-000009')}</p>
								{/*国际化处理：请耐心等待*/}
							</div>
						)}
						{cardStatus === 'success' && (
							<div className="success">
								<div className="success-info">
									<span className="iconfont icon-wancheng wancheng" />
									<span>
										{getMultiLangByID('ExportArea-000006')}
										{importTitle}
										{getMultiLangByID('ExportArea-000010')}
									</span>
									{/*国际化处理：请耐心等待*/}
								</div>
								<div>
									<p>
										{getMultiLangByID('ExportArea-000011', {
											num: responseMsg.successMsg + responseMsg.failMsg
										})}
									</p>
									{/*国际化处理：应导入{responseMsg.successMsg + responseMsg.failMsg} 条 */}
									<p
										className="success-p"
										title={getMultiLangByID('ExportArea-000002', {
											num: responseMsg.successMsg
										})}
									>
										{getMultiLangByID('ExportArea-000002', {
											num: responseMsg.successMsg
										})}：
										<span
											className={
												!linkGlbUrl && !linkGrpUrl && !linkOrgUrl ? 'equip-card-alert' : ''
											}
											onClick={() => {
												if (!linkGlbUrl && !linkGrpUrl && !linkOrgUrl) {
													return;
												}
												let { failvo } = config.resultInfo;
												this.linkTo(
													_this.props,
													responseMsg.linkUrl,
													responseMsg.linkAppCode,
													linkPageCode
												);
											}}
										>
											{/*国际化处理：成功{responseMsg.successMsg} 条 */}
											{linkApp}
										</span>
									</p>
									<p>
										{getMultiLangByID('ExportArea-000003', {
											num: responseMsg.failMsg
										})}：<span
											onClick={() => {
												let { failvo } = config.resultInfo;
												this.onExport(_this.props, pagecode, failvo, exportUrl);
											}}
										>
											{/*国际化处理：失败{responseMsg.failMsg} 条 */}
											{getMultiLangByID('ExportArea-000004')}
										</span>
										{/*国际化处理：下载 */}
									</p>
								</div>
							</div>
						)}
					</div>
				</div>
			</div>
		);
	}
}
