import { loadjs } from 'nc-lightapp-front';
/**
 * 加载高德地图，并进行初始化相关方法
 * @param {*} callback 
 */
export default function(callback) {
	if (typeof callback != 'function') {
		return;
	}
	loadjs('//webapi.amap.com/maps?v=1.4.5&key=614d9388e589a53d300a51cb8f54d6c5', () => {
		if (!global.AMap) {
			let timer = null;
			timer = setInterval(() => {
				if (global.AMap) {
					clearInterval(timer);
					timer = null;
					loadjs('//webapi.amap.com/ui/1.0/main.js', () => {
						callback();
					});
				}
			});
		} else {
			loadjs('//webapi.amap.com/ui/1.0/main.js', () => {
				callback();
			});
		}
	});
}
