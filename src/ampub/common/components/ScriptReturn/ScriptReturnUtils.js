import { toast, cardCache } from 'nc-lightapp-front';
import { approveConst } from '../../const/CommonKeys';
import { setCardValue } from '../../utils/cardUtils';
import { setBillFlowBtnsEnable, setCooperateBtnEnable } from '../../utils/listUtils';
import { getMultiLangByID } from '../../utils/multiLangUtils';

/**
 * 列表态 动作处理返回刷新处理
 * @param {*} params id,index 的数组
 * @param {*} res 动作处理返回结果
 * @param {*} props props
 * @param {*} pk_bill pk字段
 * @param {*} formId 卡片态的formId 用来取billcard里的head数据 
 * @param {*} tableId 列表态页面的 tableId
 * @param {*} isDelete 删除回刷标志 是删除传true 否则false
 * @param {*} dataSource 单页缓存的dataSource ，用来处理提交或收回的缓存
 */
export function getScriptListReturn(params, res, props, pk_bill, formId, tableId, isDelete, dataSource) {
	let pk = pk_bill;
	if (res.data.success === approveConst.HASAPPROVEALTRAN) {
		//指派信息
		let workflowData = res.data.approvealTranData;
		if (
			workflowData.workflow &&
			(workflowData.workflow == approveConst.APPROVEFLOW || workflowData.workflow == approveConst.WORKFLOW)
		) {
			if (params.length == 1) {
				props.table.selectAllRows(tableId, false); //先去掉之前勾选的行
				props.table.selectTableRows(tableId, [params[0].index], true);
				setBillFlowBtnsEnable.call(this, props, { tableId: tableId });
			}
			this.setState({
				compositedata: workflowData,
				compositedisplay: true
			});
		}
	} else {
		//数据回刷、缓存处理
		let Datas = res.data.cardVos;
		if (Datas != null) {
			let deleteIndexs = [];
			let updateIndexs = [];
			let successPk = []; //成功的pk
			//更新构造页面record
			Datas.map((v) => {
				let row = v.head[formId].rows[0].values;
				let index = queryArr(params, row[pk].value);
				let record = [
					{
						index,
						data: {
							status: '1',
							values: row
						}
					}
				];
				if (!isDelete) {
					props.table.updateDataByIndexs(tableId, record);
					updateIndexs.push(index);
				} else {
					deleteIndexs.push(index);
				}
				successPk.push(row[pk].value);
			});
			if (isDelete) {
				//删除页面数据
				props.table.deleteTableRowsByIndex(tableId, deleteIndexs);
				//删除缓存信息
				successPk.map((pk) => {
					props.table.deleteCacheId(tableId, pk);
				});
			} else {
				if (dataSource) {
					successPk.map((pk) => {
						cardCache.updateCache(pk_bill, pk, null, formId, dataSource);
					});
				}
				//设置成功的多选框不选中
				props.table.selectTableRows(tableId, updateIndexs, false, true);
			}
			//平台10-11晚上盘里会加入自动回调注册的绑定方法  之后这个可去掉
			setBillFlowBtnsEnable.call(this, props, { tableId: tableId });
		}
		//消息处理
		//全部成功
		if (res.data.success === approveConst.ALLSUCCESS) {
			// let resData = res.data.successMsg.split('<br/>');
			toast({
				title: res.data.successMsg.replace(new RegExp('<br/>', 'gm'), ''),
				color: 'success'
			});
			//有错误信息
		} else {
			let resData = res.data.errorMsg.split('<br/>');
			if (resData) {
				if (resData.length == 1 || resData.length == 2) {
					toast({ content: resData.shift(), color: 'danger', duration: 'infinity' });
				} else {
					toast({
						duration: 'infinity',
						content: resData.shift(),
						color: 'danger',
						groupOperation: true,
						groupOperationMsg: resData,
						TextArr: [
							getMultiLangByID('amcommon-000005') /*国际化处理：展开*/,
							getMultiLangByID('amcommon-000006') /*国际化处理：收起*/,
							getMultiLangByID('ScriptReturn-000000') /*国际化处理：我知道了*/
						]
					});
				}
			}
		}
	}
}
/**
 * 卡片态 动作处理返回刷新处理
 * @param {*} res 动作处理返回结果
 * @param {*} props 
 * @param {*} formId 卡片态的formId 用来取billcard里的head数据 
 * @param {*} tableId  列表态页面的 tableId
 */
export function getScriptCardReturn(res, props, formId, tableId) {
	if (res.data.success === approveConst.HASAPPROVEALTRAN) {
		//指派信息
		let workflowData = res.data.approvealTranData;
		if (
			workflowData.workflow &&
			(workflowData.workflow == approveConst.APPROVEFLOW || workflowData.workflow == approveConst.WORKFLOW)
		) {
			this.setState({
				compositedata: workflowData,
				compositedisplay: true
			});
		}
	} else {
		if (res.data.success === approveConst.ALLSUCCESS) {
			if (res.data.cardVos[0].head && res.data.cardVos[0].head[formId]) {
				props.form.setAllFormValue({ [formId]: res.data.cardVos[0].head[formId] });
			}
			if (res.data.cardVos[0].body && res.data.cardVos[0].body[tableId]) {
				props.cardTable.setTableData(tableId, res.data.cardVos[0].body[tableId]);
			}
			toast({ content: res.data.successMsg, color: 'success' });
		} else {
			toast({ content: res.data.errorMsg, color: 'danger' });
		}
	}
}

/**
 * 卡片态 动作处理返回刷新处理
 * @param {*} res 动作处理返回结果
 * @param {*} props 
 * @param {*} formId 卡片态的formId 用来取billcard里的head数据 
 * @param {*} tableId   tableId   一主多子传数组
 * @param {*} pk_bill  表头主键字段
 * @param {*} dataSource  单页dataSource
 * @param {*} setValueNew 页面赋值函数
 * @param {*} isDelete  是否是删除 删除 传true
 * @param {*} callback  成功后的回调函数
 * @param {*} pagecode  处理流量优化后造成缓存问题
 */
export function getScriptCardReturnData(
	res,
	props,
	formId,
	tableId,
	pk_bill,
	dataSource,
	setValueNew,
	isDelete,
	callback,
	pagecode
) {
	if (res.data.success === approveConst.HASAPPROVEALTRAN) {
		//指派信息
		let workflowData = res.data.approvealTranData;
		if (
			workflowData.workflow &&
			(workflowData.workflow == approveConst.APPROVEFLOW || workflowData.workflow == approveConst.WORKFLOW)
		) {
			this.setState({
				compositedata: workflowData,
				compositedisplay: true
			});
		}
	} else {
		if (res.data.success === approveConst.ALLSUCCESS) {
			let oldPk = props.form.getFormItemsValue(formId, pk_bill).value;
			if (isDelete && isDelete == true) {
				let newpk = cardCache.getNextId(oldPk, dataSource);
				cardCache.deleteCacheById(pk_bill, oldPk, dataSource);
				typeof callback === 'function' && callback.call(this, newpk);
			} else {
				//数据回刷
				if (setValueNew == null || setValueNew == '') {
					setCardValue.call(this, props, res.data.cardVos[0], formId, tableId);
				} else {
					typeof setValueNew === 'function' && setValueNew.call(this, res.data.cardVos[0]);
				}
				//缓存处理
				let pk = props.form.getFormItemsValue(formId, pk_bill).value;
				let status = props.form.getFormStatus(formId);
				let cachData;
				if (Array.isArray(tableId)) {
					cachData = props.createExtCardData(pagecode, formId, tableId);
				} else {
					cachData = props.createMasterChildData(pagecode, formId, tableId);
				}
				// if (status == 'add' || status == 'edit') {
				if (oldPk && oldPk != null && oldPk != '') {
					//修改保存提交
					cardCache.updateCache(pk_bill, pk, cachData, formId, dataSource);
				} else {
					//新增保存提交
					cardCache.addCache(pk, cachData, formId, dataSource);
				}
				// } else if (status == 'browse') {
				// 	//一般的提交 收回
				// 	cardCache.updateCache(pk_bill, pk, cachData, formId, dataSource);
				// }
				//成功后的回调  状态回刷等
				typeof callback === 'function' && callback.call(this, pk, res.data.cardVos[0]);
			}
			toast({ title: res.data.successMsg, color: 'success' });
		} else {
			toast({ content: res.data.errorMsg, color: 'danger' });
		}
	}
}

/**
 * 一主多子卡片态 动作处理返回刷新处理
 * @param {*} res 动作处理返回结果
 * @param {*} props 
 * @param {*} formId 卡片态的formId 用来取billcard里的head数据
 * @param {*} tableIds 所有子表 tableId 数组
 */
export function getScriptExtCardReturn(res, props, formId, tableIds) {
	if (res.data.success === approveConst.HASAPPROVEALTRAN) {
		//指派信息
		let workflowData = res.data.approvealTranData;
		if (
			workflowData.workflow &&
			(workflowData.workflow == approveConst.APPROVEFLOW || workflowData.workflow == approveConst.WORKFLOW)
		) {
			this.setState({
				compositedata: workflowData,
				compositedisplay: true
			});
		}
	} else {
		if (res.data.success === approveConst.ALLSUCCESS) {
			if (res.data.cardVos[0].head && res.data.cardVos[0].head[formId]) {
				props.form.setAllFormValue({ [formId]: res.data.cardVos[0].head[formId] });
			}
			if (res.data.cardVos[0].bodys) {
				tableIds.map((bodycode) => {
					props.cardTable.setTableData(bodycode, res.data.cardVos[0].bodys[bodycode]);
				});
			}
			toast({ content: res.data.successMsg, color: 'success' });
		} else {
			toast({ content: res.data.errorMsg, color: 'danger' });
		}
	}
}

const queryArr = (params, item) => {
	for (var i = 0; i < params.length; i++) {
		if (params[i].id === item) return params[i].index;
	}
	return -1;
};

export function setBatchBtnsEnable(props, moduleId) {
	let checkedRows = props.table.getCheckedRows(moduleId);
	let batchBtns = [
		'Delete',
		'Commit',
		'UnCommit',
		'Attachment',
		'QueryAbout',
		'QueryAboutVoucher',
		'QueryAboutBusiness',
		'QueryAboutBillFlow',
		'Print',
		'Preview',
		'Output',
		'CommitGroup'
	];
	props.button.setButtonDisabled(batchBtns, !(checkedRows && checkedRows.length > 0));
}

export function getScriptListParam(paramInfoMap, OperatorType, card_pageId, content) {
	let param = {
		isLightVO: true,
		paramInfoMap: paramInfoMap,
		dataType: 'listData',
		OperatorType: OperatorType,
		pageid: card_pageId,
		commitType: 'commit', //提交
		content: content
	};
	return param;
}

export function getScriptCardParam(pkField, formId, OperatorType, commitType, card_pageId, content, isLightVO = true) {
	let paramInfoMap = {};
	let pk = this.props.form.getFormItemsValue(formId, pkField).value;
	paramInfoMap[pk] = this.props.form.getFormItemsValue(formId, 'ts').value;
	let param = {
		isLightVO: isLightVO,
		paramInfoMap: paramInfoMap,
		dataType: 'cardData',
		OperatorType: OperatorType,
		pageid: card_pageId,
		commitType: commitType, //提交
		content: content
	};
	return param;
}

/**
 * 回刷协同后数据
 * @param {*} props 
 * @param {*} formId 
 * @param {*} tableId 
 * @param {*} data 
 */
export function ReturnCooperateData(props, index,formId = 'card_head',tableId = 'list_head', data) {
		let row = data.head[formId].rows[0].values;
		let record = [
			{
				index,
				data: {
					status: '1',
					values: row
				}
			}
		];
		setCooperateBtnEnable.call(this,props,tableId,row);
		props.table.updateDataByIndexs(tableId, record);
}
