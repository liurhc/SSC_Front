import { toast, ajax, cacheTools } from 'nc-lightapp-front';
import { linkQueryConst } from '../../const/CommonKeys';
import { pageConfig } from '../../const/VoucherConst';
import { getMultiLangByID } from '../../utils/multiLangUtils';
import { getContext, loginContextKeys } from '../AMInitInfo/loginContext';
const { cardRouter } = pageConfig;
/**
 * 联查卡片
 * author by wangwhf
 * 20180620
 */
/**
 * 联查固定资产卡片
 * 常规卡片态 子表选择卡片行后直接调用，选择多条时，默认查第一条
 * @param {*} e 
 * @param {*} props 
 * @param {*} tableId 
 */
export function openAssetCardByChooseRow(props, tableId) {
	let checkData = props.cardTable.getCheckedRows(tableId);
	if (checkData.length == 0) {
		toast({ content: getMultiLangByID('QueryAbout-000000'), color: 'warning' }); /*国际化处理：'请先选中需要联查的卡片数据'*/
		return;
	} else if (checkData.length > 1) {
		toast({ content: getMultiLangByID('QueryAbout-000005'), color: 'warning' }); /*国际化处理：'不支持选择多条表体联查'*/
		return;
	}
	let id = checkData[0].data.values.pk_card.value;
	if (!id) {
		toast({ content: getMultiLangByID('QueryAbout-000001'), color: 'warning' }); /*国际化处理：'当前资产尚未生成卡片'*/
		return;
	}
	// 如果建卡方式字段存在，并且是按数量拆分建卡
	let Create_card_style = props.form.getFormItemsValue('card_head', 'create_card_style');
	if (Create_card_style && Create_card_style.value && Create_card_style.value === '2') {
		let transasset_b = checkData[0].data.values.pk_transasset_b.value;
		queryAssetVOBybpk.call(this, props, transasset_b);
	} else {
		openAssetCardByPk.call(this, props, id);
	}
}
/**
 * 通过转固单表体主键查询卡片主键
 * @param {*} props 
 * @param {*} transasset_b 
 */
function queryAssetVOBybpk(props, transasset_b) {
	ajax({
		url: '/nccloud/fa/transasset/queryAssetVOBybpk.do',
		data: { pk: transasset_b },
		success: (res) => {
			if (!res.data || res.data.length === 0) {
				toast({ content: getMultiLangByID('QueryAbout-000001'), color: 'warning' }); /*国际化处理：'当前资产尚未生成卡片'*/
				return;
			} else if (res.data.length === 1) {
				openAssetCardByPk.call(this, props, res.data[0]);
			} else {
				openAssetCardByPk.call(this, props, res.data[0], { target: 'list', pk_cards: res.data });
			}
		}
	});
}
/**
 * 联查固定资产卡片，通过超链接主键
 * @param {*} props 
 * @param {*} pk_card 卡片主键
 * @param {*} para  = { status: 'browse', target: 'blank', pk_cardhistory: '' }
 */
export function openAssetCardByPk(
	props,
	pk_card,
	{ status = 'browse', target = 'blank', pk_cardhistory = '', pk_cards = [] } = {}
) {
	ajax({
		url: '/nccloud/fa/facard/linkQueryFaCard.do',
		data: { [linkQueryConst.ID]: pk_card },
		success: (res) => {
			if (res.data) {
				let linkData = res.data;
				linkData['status'] = status;
				linkData[linkQueryConst.SCENE] = linkQueryConst.SCENETYPE.linksce; // 设置联查场景
				if (pk_cardhistory) {
					linkData['pk_cardhistory'] = pk_cardhistory;
				}
				if (pk_cards && pk_cards.length) {
					linkData['pk_cards'] = pk_cards;
				}
				// 单个联查卡片
				let url = linkData[linkQueryConst.URL];
				//将联查的参数使用 cacheTools 进行缓存
				cacheTools.set('fa_facard_linkQuery_param', linkData);
				delete linkData[linkQueryConst.URL];
				delete linkData['pk_cardhistory'];
				delete linkData['pk_accbook'];
				delete linkData['accyear'];
				delete linkData['period'];
				delete linkData['pk_cards'];

				if (target == 'blank') {
					props.openTo(url, linkData);
				} else if (target == 'list') {
					// 多个联查列表
					url = url.replace('#/card', '#/list');
					props.openTo(url, linkData);
				} else {
					//当前页跳转
					props.pushTo(cardRouter, linkData);
				}
			} else {
				toast({ content: getMultiLangByID('QueryAbout-000002'), color: 'warning' }); /*国际化处理：'无权限'*/
			}
		}
	});
}

/**
 * 总账对账执行联查打开固定资产应用
 */
export function busireconLinkToFa(props, params) {
	let { appcode, pagecode, url, id, pk_accbook, endYear, endPeriod } = params;
	let cardcode = '#201201504A##201201508A##201201512A##201201516A#'; //和资产卡片有关的appcode
	let depcode = '201204004A';
	if (appcode) {
		if (cardcode.includes(`#${appcode}#`)) {
			//联查固定资产卡片
			openAssetCardByPk(props, id);
		} else if (appcode == depcode) {
			//联查折旧与摊销 需要根据账簿查询组织
			ajax({
				url: '/nccloud/fa/depreciation/depgather.do',
				data: {
					pk_depgather: id
				},
				success: (res) => {
					if (res && res.data) {
						props.openTo(url, {
							appcode,
							pagecode,
							pk_org: res.data['pk_org'], //财务组织
							pk_org_name: res.data['pk_org_name'],
							pk_accbook: res.data['pk_accbook'],
							pk_accbook_name: res.data['pk_accbook_name'], //资产账簿
							accyear: res.data['accyear'], //会计年
							period: res.data['period'] //会计期间
						});
					}
				},
				error: (res) => {
					if (res && res.message) {
						toast({ color: 'danger', content: res.message }); //提示错误信息
					}
				}
			});
		} else {
			//联查单据
			props.openTo(url, {
				appcode: appcode,
				pagecode: pagecode,
				id: id,
				status: 'browse'
			});
		}
	}
}
