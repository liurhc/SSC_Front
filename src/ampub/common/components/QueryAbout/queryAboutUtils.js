import { toast, ajax } from 'nc-lightapp-front';
import { linkQueryConst } from '../../const/CommonKeys';
import { amSessionStorage } from '../AMDB/AMsessionStorage';
import { getMultiLangByID } from '../../utils/multiLangUtils';

/**
 * 联查设备卡片
 * 常规卡片态 子表选择设备后直接调用
 * @param {*} e  this
 * @param {*} props 
 * @param {*} tableId 
 */
export const openEquipCard = (e, props, tableId) => {
	let checkData = props.cardTable.getCheckedRows(tableId);
	if (checkData.length == 0) {
		toast({ content: getMultiLangByID('QueryAbout-000003'), color: 'warning' }); /*国际化处理： '请先选中需要联查的设备数据'*/
		return;
	}
	let params = checkData.map((v) => {
		let id = v.data.values.pk_equip.value;
		return {
			id
		};
	});
	let pk_equip = params[0];
	openEquipCardByPk(props, pk_equip.id);
};

/**
 * 通过pk联查设备卡片
 * @param {*} props 
 * @param {*} pk_equip 设备pk
 */
export const openEquipCardByPk = (props, pk_equip) => {
	ajax({
		url: '/nccloud/aim/equip/linkQueryEquip.do',
		data: { [linkQueryConst.ID]: pk_equip },
		success: (res) => {
			if (res.data) {
				let linkData = res.data;
				linkData['status'] = 'browse';
				props.openTo(linkData[linkQueryConst.URL], linkData);
			} else {
				toast({ content: getMultiLangByID('QueryAbout-000002'), color: 'warning' }); /*国际化处理： '无权限'*/
			}
		},
		error: (res) => {
			toast({ content: res.message, color: 'warning' });
		}
	});
};

/**
 * 单据追溯按钮事件
 * @param {*} e 
 */
export const openBillTrack = (e) => {
	e.setState({
		show: true
	});
};

/**
 * 审批详情按钮事件
 * @param {*} e  this
 */
export const openApprove = (e) => {
	e.setState({
		showApprove: true
	});
};

/**
 * 列表态单据追溯
 * @param {*} e this
 * @param {*} props 
 * @param {*} tableId 
 * @param {*} pk_bill  pk字段名
 */
export const openListBillTrack = (e, props, tableId, pk_bill) => {
	let pk = pk_bill;
	let checkData = props.table.getCheckedRows(tableId);
	if (checkData.length == 0) {
		toast({ content: getMultiLangByID('QueryAbout-000004'), color: 'warning' }); /*国际化处理： '请先选中需要查询的单据'*/
		return;
	}
	let params = checkData.map((v) => {
		let id = v.data.values[pk].value;
		return {
			id
		};
	});
	e.setState({
		[pk_bill]: params[0].id,
		show: true
	});
};

/**
 * 列表态审批详情
 * @param {*} e 
 * @param {*} props 
 * @param {*} tableId 
 * @param {*} pk_bill pk字段名
 */
export const openListApprove = (e, record, pk_bill) => {
	let pk = record[pk_bill].value;
	e.setState({
		[pk_bill]: pk,
		transi_type: record.transi_type.value,
		showApprove: true
	});
};

/**
 * 联查资产卡片台账
 * 
 * @param {*} props 
 * @param {*} srctype  来源方式
 * @param {*} cachekey 缓存主键
 */
export const openReportEquip = (props, srctype = '', cachekey = '', querycondition = {}, querysql = {}) => {
	//拼写查询区
	amSessionStorage.setItem(`LinkReport`, JSON.stringify(querycondition));
	//拼写sql
	amSessionStorage.setItem(`LinkReportUserDefObj`, JSON.stringify(querysql));

	props.openTo('/aim/report/equip/list/index.html', {
		appcode: '451012508A',
		pagecode: '451012508A_list',
		srctype: srctype,
		cachekey: cachekey
	});
};

/**
 * 联查资产卡片台账常量
 */
export const LinkReportConst = {
	//展示图形
	GRAPHIC: '1',
	//展示报表
	REPORT: '0',
	GETSQLACTION: 'getSqlAction'
};
