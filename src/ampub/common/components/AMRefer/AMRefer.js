import React, { Component } from 'react';
import { createScript, withViewModel } from 'nc-lightapp-front';

/**
 * 参照控件
 * 参数config
 * 如果父组件自己维护参照的值则传递defaultValue，否则不要传递
 * 如果多选isMultiSelectedEnabled=true，defaultValue值需要是数组defaultValue=[]
 * {
		defaultValue: {refcode: '', refname: '', refpk: ''},
		disabled: false,
		onChange: (value) => {
			this.onChange(value);
		},
		refcode: refCode,
		queryCondition: () => {
			return { pk_org };
		},
		isMultiSelectedEnabled : false
	}
 */
class AMRefer extends Component {
	constructor(props) {
		super(props);
		this.state = {
			foolValue: {},
			Item: ''
		};
	}

	// 异步加载参照js
	componentDidMount() {
		let {
			moduletype = 'form',
			moduleId = 'head',
			attrcode = 'pk_field',
			refcode,
			defaultValue
		} = this.props.config;
		let { Item } = this.state;
		let renderItem = this.props.ViewModel.getData('am-ampub-common-amrefer') || {};
		Item = renderItem[refcode];
		if (!Item) {
			createScript.call(this, moduletype, moduleId, attrcode, refcode);
		}
		this.disposeDefaultValue(defaultValue);
	}

	componentWillReceiveProps(nextProps) {
		let { defaultValue } = nextProps.config;
		this.disposeDefaultValue(defaultValue);
	}

	//对默认值进行处理
	disposeDefaultValue = (defaultValue) => {
		if (defaultValue) {
			if (defaultValue instanceof Array && defaultValue.length > 0) {
				let display = [],
					value = [];
				defaultValue.map((item) => {
					display.push(item.refname);
					value.push(item.refpk2 || item.refpk);
				});
				display = display.join(',');
				value = value.join(',');
				this.setState({
					foolValue: { display, value }
				});
			} else {
				let display = defaultValue.refname;
				let value = defaultValue.refpk2 || defaultValue.refpk;
				this.setState({
					foolValue: { display, value }
				});
			}
		}
	};

	// 参照值改变事件
	onChange = (value, foolValue) => {
		this.setState({ foolValue: foolValue }, () => {
			if (typeof this.props.config.onChange == 'function') {
				this.props.config.onChange(value);
			}
		});
	};

	// 渲染参照控件
	renderItem(moduletype, moduleId, attrcode, newItem) {
		let { refcode } = this.props.config;
		let renderItem = this.props.ViewModel.getData('am-ampub-common-amrefer') || {};
		renderItem[refcode] = newItem;
		this.props.ViewModel.setData('am-ampub-common-amrefer', renderItem);
		this.setState({
			Item: newItem
		});
	}

	render() {
		let { Item, foolValue } = this.state;
		let { config, className } = this.props;
		let { refcode } = config;
		let renderItem = this.props.ViewModel.getData('am-ampub-common-amrefer');
		Item = Item || (renderItem && renderItem && renderItem && renderItem[refcode]) || '';
		if (Item) {
			Item = {
				...Item,
				props: {
					...Item.props,
					...config,
					foolValue,
					onChange: (value, foolValue) => {
						this.onChange(value, foolValue);
					}
				}
			};
		}
		return <div className={className}>{Item}</div>;
	}
}

export default withViewModel(AMRefer);
