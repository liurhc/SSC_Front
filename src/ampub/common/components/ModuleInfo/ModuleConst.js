/**
 * 模块功能号常量
 */

export const MODULE_FUNCODE = {

    /** 共享服务管理：sscrp模块 */
    SSCRP_FUNCODE: "1056",

    /** 共享服务管理：ssctp模块 */
    SSCTP_FUNCODE: "7010",


    /** 管理会计：RESA模块  */
    RESA_FUNCCODE: "3820",

    /** 收付报：AP模块 */
    AP_FUNCCODE: "2008",

    /** 会计平台：FIP模块*/
    FIP_FUNCCODE: "1017",

    /** 收付报：AR模块 */
    AR_FUNCCODE: "2006",

    /** 资产管理：FA模块 */
    FA_FUNCCODE: "2012",

    /** 资产管理 */
    AM_FUNCCODE: "45",

    /** 资产管理：资产公共 */
    AMPUB_FUNCCODE: "4501",

    /** 资产管理：AIM模块 */
    AIM_FUNCCODE: "4510",

    /** 资产管理：AUM模块 */
    AUM_FUNCCODE: "4520",

    /** 资产管理：ALM模块 */
    ALM_FUNCCODE: "4530",

    /** 资产管理：ALO模块 */
    ALO_FUNCCODE: "4530",

    /** 资产管理：ALI模块 */
    ALI_FUNCCODE: "4532",

    /** 维修维护：EOM模块 */
    EOM_FUNCCODE: "4540",

    /** 维修维护：EMM模块 */
    EMM_FUNCCODE: "4550",

    /** 维修维护：EWM模块 */
    EWM_FUNCCODE: "4560",

    /**周转材：易耗品管理模块 */
    RUM_FUNCCODE: "4580",

    /**周转材：周转材租出模块 */
    ROM_FUNCCODE: "4583",

    /**周转材：周转材租入模块 */
    RLM_FUNCCODE: "4585",


    /** 供应链：IC模块 */
    IC_FUNCCODE: "4008",

    /** 供应链：SCMPUB模块 */
    SCMPUB_FUNCCODE: "4001",

    /** 供应链：PU模块 */
    PU_FUNCCODE: "4004",

    /** 预算：TB模块 */
    TB_FUNCCODE: "1812",

    /** 项目管理：项目综合管理模块 */
    PIM_FUNCCODE: "4810",

    /** 供应链：序列号管理模块 */
    SN: "4016",

    /** 影像管理：imag模块 */
    IMAG: "1054",
}



