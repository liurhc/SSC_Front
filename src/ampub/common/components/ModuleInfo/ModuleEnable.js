import { ajax, toast } from 'nc-lightapp-front';
import { MODULE_FUNCODE } from './ModuleConst';

/**
 * 判断模块是否启用
 */
export const MODULEENABLE = {
    //财务模块：应收管理
    isAREnabled: () => {
        return isModuleEnabled.call(this, MODULE_FUNCODE.AR_FUNCCODE);
    },
    //财务模块：应付管理
    isAPEnabled: () => {
        return isModuleEnabled.call(this, MODULE_FUNCODE.AP_FUNCCODE);
    },
    //财务模块：会计平台
    isFIPEnabled: () => {
        return isModuleEnabled.call(this, MODULE_FUNCODE.FIP_FUNCCODE);
    },
    //管理会计：责任会计
    isRESAEnabled: () => {
        return isModuleEnabled.call(this, MODULE_FUNCODE.RESA_FUNCCODE);
    },
    //项目管理：项目综合管理
    isPIMEnabled: () => {
        return isModuleEnabled.call(this, MODULE_FUNCODE.PIM_FUNCCODE);
    },
    //财务模块：固定资产
    isFAEnabled: () => {
        return isModuleEnabled.call(this, MODULE_FUNCODE.FA_FUNCCODE);
    },
    //资产管理：AIM模块
    isAIMEnabled: () => {
        return isModuleEnabled.call(this, MODULE_FUNCODE.AIM_FUNCCODE);
    },
    //资产管理：AUM模块
    isAUMEnabled: () => {
        return isModuleEnabled.call(this, MODULE_FUNCODE.AUM_FUNCCODE);
    },
    //租赁管理：ALM模块
    isALMEnabled: () => {
        return isModuleEnabled.call(this, MODULE_FUNCODE.ALM_FUNCCODE);
    },
    //资产租出管理：ALO模块
    isALOEnabled: () => {
        return isModuleEnabled.call(this, MODULE_FUNCODE.ALO_FUNCCODE);
    },
    //资产租入管理：ALI模块
    isALIEnabled: () => {
        return isModuleEnabled.call(this, MODULE_FUNCODE.ALI_FUNCCODE);
    },
    //供应链模块：库存管理
    isICEnabled: () => {
        return isModuleEnabled.call(this, MODULE_FUNCODE.IC_FUNCCODE);
    },
    //供应链模块：采购管理
    isPUEnabled: () => {
        return isModuleEnabled.call(this, MODULE_FUNCODE.PU_FUNCCODE);
    },
    //供应链模块：供应链基础设置
    isSCMPUBEnabled: () => {
        return isModuleEnabled.call(this, MODULE_FUNCODE.SCMPUB_FUNCCODE);
    },
    //资产管理模块：运行管理
    isEOMEnabled: () => {
        return isModuleEnabled.call(this, MODULE_FUNCODE.EOM_FUNCCODE);
    },
    //资产管理模块：维修维护
    isEMMEnabled: () => {
        return isModuleEnabled.call(this, MODULE_FUNCODE.EMM_FUNCCODE);
    },
    //资产管理模块：工单
    isEWMEnabled: () => {
        return isModuleEnabled.call(this, MODULE_FUNCODE.EWM_FUNCCODE);
    },
    //资产管理模块：周转材租入模块
    isRLMEnabled: () => {
        return isModuleEnabled.call(this, MODULE_FUNCODE.RLM_FUNCCODE);
    },
    //资产管理模块：周转材租出模块
    isROMEnabled: () => {
        return isModuleEnabled.call(this, MODULE_FUNCODE.ROM_FUNCCODE);
    },
    //资产管理模块：周转材易耗品
    isRUMEnabled: () => {
        return isModuleEnabled.call(this, MODULE_FUNCODE.RUM_FUNCCODE);
    },
    //序列号管理模块：序列号
    isSNEnabled: () => {
        return isModuleEnabled.call(this, MODULE_FUNCODE.SN);
    },
    //影像管理模块：IMAG
    isImagEnabled: () => {
        return isModuleEnabled.call(this, MODULE_FUNCODE.IMAG);
    },
    //报账人门户：SSCRP
    isSscrpEnabled: () => {
        return isModuleEnabled.call(this, MODULE_FUNCODE.SSCRP_FUNCODE);
    },
    //作业平台：SSCTP
    isSsctpEnabled: () => {
        return isModuleEnabled.call(this, MODULE_FUNCODE.SSCTP_FUNCODE);
    }

}

/**
 * 走后台查询模块是否启用
 * @param {*} funcode 
 */
function isModuleEnabled(funcode) {
    let flag = false;
    ajax({
        url: '/nccloud/ampub/common/moduleInfoQuery.do',
        data: {
            funcode
        },
        async: false,
        success: (res) => {
            let { success, data } = res;
            if (success) {
                flag = data;
            }
        },
        error: (res) => {
            toast({ content: res.message, color: 'danger' });
        }
    });
    return flag;
}

