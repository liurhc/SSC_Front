import { AssetOrgMultiRefFilter } from '../../../../ampub/common/components/MultiRefFilter/AssetOrgMultiRefFilter';
import { IBusiRoleConst } from '../../../../ampub/common/const/CommonKeys';
import { commonRefCondition, defRefCondition } from '../../../../ampub/common/components/ReferInit/refInit';
import { getContext, loginContextKeys } from '../../../../ampub/common/components/AMInitInfo/loginContext';

/**
 * 公共查询区参照过滤。
 * 包括：1.子表.设备.明细字段过滤；2.自定义项过滤；3.添加【执行时包含下级】默认勾选
 * 
 * 其他字段过滤需调用commonRefFilter方法处理
 * @param {*} props 
 * @param {*} item 过滤的数据 
 * @param {*} searchAreaId 查询区id
 * @param {*} defOrgField 自定义项过滤参照的组织字段，默认为pk_org
 * @param {*} defPrefix 表头自定义项前缀,默认为def
 * @param {*} bodyDefPrefix 表体自定义项前缀,一主多子可传数组，默认为bodyvos.def
 * @param {*} multiRefFilter 查询区域时需要传递主组织多选处理的回调方法，默认是AssetOrgMultiRefFilter，具体根据当前单据的主组织属性决定选择哪个
 */
export function refCommonFilterForSearchArea(
	props,
	item,
	searchAreaId,
	defOrgField = 'pk_org',
	defPrefix = 'def',
	bodyDefPrefix = 'bodyvos.def',
	multiRefFilter = AssetOrgMultiRefFilter
) {
	// 初始化上下文变量后获取集团ID
	const pk_group = getContext(loginContextKeys.groupId);
	let itemcode = item.attrcode;
	if (itemcode.includes('.pk_equip.')) {
		// 设备明细字段过滤
		let bodyPrefix = itemcode.split('.')[0];
		equipBodyRefCondition.call(this, props, item, searchAreaId, bodyPrefix);
	}
	// 自定义项过滤
	commonDefCondition.call(
		this,
		props,
		item,
		searchAreaId,
		pk_group,
		defOrgField,
		defPrefix,
		bodyDefPrefix,
		multiRefFilter
	);
	// 添加【执行时包含下级】默认勾选和添加【显示停用】勾选框
	commonRefCondition.call(this, props, item);

	return item;
}

/**
 * 常用字段过滤的过滤方法，已处理部门人员来源过滤、设置业务单元参照框、显示停用3种场景。
 * 其余特殊过滤处理需传keyFilter
 * @param {*} props 
 * @param {*} item 
 * @param {*} searchAreaId 查询区ID
 * @param {*} currentFields 当前字段,可以传数组或者字符串
 * @param {*} refField 参照字段
 * @param {*} returnKey 传入后台相应参照字段值得key值 默认取值为pk_org
 * @param {*} keyFilter 需要特殊处理的参照字段:对应的filter。
 * 			  示例：{'bodyvos.pk_warehouse_in': {GridRefActionExt: refFilter.wareHouseRefFilter}}
 */
export function commonRefFilter(props, item, searchAreaId, currentFields, refField, returnKey = 'pk_org', keyFilter) {
	let itemcode = item.attrcode;
	if (Array.isArray(currentFields)) {
		currentFields.map((currentField) => {
			if (itemcode == currentField) {
				detailRefFilter.call(this, props, item, currentField, searchAreaId, refField, keyFilter, returnKey);
			}
		});
	} else {
		if (itemcode == currentFields) {
			detailRefFilter.call(this, props, item, currentFields, searchAreaId, refField, keyFilter, returnKey);
		}
	}
	return item;
}
/**
 * 参照字段是否多选设置业务单元参照框
 * @param {*} props 
 * @param {*} item 
 * @param {*} currentField 当前需要处理的参照字段
 * @param {*} searchAreaId 查询区ID
 * @param {*} refField 参照字段
 * @param {*} keyFilter 需要处理的参照字段:对应的filter
 * @param {*} returnKey 传入后台相应参照字段值得key值 默认取值为pk_org
 */
function detailRefFilter(props, item, currentField, searchAreaId, refField, keyFilter, returnKey) {
	let filter = {};
	if (keyFilter && keyFilter[currentField]) {
		filter = keyFilter[currentField];
	}
	// 根据参照的字段是否多选设置业务单元框
	AssetOrgMultiRefFilter.call(this, props, searchAreaId, item, refField);
	// 部门或人员过滤
	psnAndDeptRefCondition.call(this, item, filter);
	// 设置过滤条件
	setQueryCondition.call(this, props, item, searchAreaId, refField, filter, returnKey);
}

/**
 * 自定义项过滤
 * @param {*} props 
 * @param {*} item 
 * @param {*} searchAreaId 查询区ID
 * @param {*} pk_group 集团
 * @param {*} orgField 组织字段
 * @param {*} defPrefix 表头自定义项前缀
 * @param {*} bodyDefPrefix 表体自定义项前缀
 * @param {*} multiRefFilter 组织多选回调函数
 */
function commonDefCondition(props, item, searchAreaId, pk_group, orgField, defPrefix, bodyDefPrefix, multiRefFilter) {
	// 表头自定义项过滤
	defRefCondition.call(this, props, item, searchAreaId, pk_group, true, orgField, defPrefix, multiRefFilter);
	// 表体自定义项过滤
	if (Array.isArray(bodyDefPrefix)) {
		bodyDefPrefix.map((bodyPre) => {
			defRefCondition.call(this, props, item, searchAreaId, pk_group, true, orgField, bodyPre, multiRefFilter);
		});
	} else {
		defRefCondition.call(this, props, item, searchAreaId, pk_group, true, orgField, bodyDefPrefix, multiRefFilter);
	}
}

/**
 * 设备表体关联字段过滤
 * @param {*} props 
 * @param {*} item 
 * @param {*} searchAreaId 
 * @param {*} bodyPrefix 子表前缀
 */
function equipBodyRefCondition(props, item, searchAreaId, bodyPrefix) {
	// 管理部门、管理人根据货主管理组织过滤
	commonRefFilter.call(
		this,
		props,
		item,
		searchAreaId,
		[ bodyPrefix + '.pk_equip.pk_mandept', bodyPrefix + '.pk_equip.pk_manager' ],
		bodyPrefix + '.pk_equip.pk_ownerorg'
	);
	// 使用部门、使用人根据使用权过滤
	commonRefFilter.call(
		this,
		props,
		item,
		searchAreaId,
		[ bodyPrefix + '.pk_equip.pk_usedept', bodyPrefix + '.pk_equip.pk_user' ],
		bodyPrefix + '.pk_equip.pk_used_unit'
	);
	// 位置、项目、专业\供应商、物料等根据资产组织过滤
	commonRefFilter.call(
		this,
		props,
		item,
		searchAreaId,
		[
			bodyPrefix + '.pk_equip.pk_jobmngfil',
			bodyPrefix + '.pk_equip.pk_location',
			bodyPrefix + '.pk_equip.pk_specialty',
			bodyPrefix + '.pk_equip.pk_supplier',
			bodyPrefix + '.pk_equip.pk_material'
		],
		'pk_org'
	);
}

/**
 * 组织权限过滤字段。
 * 若不传过滤条件，财务组织为树形过滤，其他组织表型过滤，
 * 若传过滤条件则走自己过滤条件
 * @param {*} item 
 * @param {*} filter 过滤条件，
 */
export function appPermissionOrgRefFilter(item, filter) {
	if (filter && (filter.GridRefActionExt || filter.TreeRefActionExt)) {
		item.queryCondition = () => {
			return filter;
		};
	} else if (
		item.refcode == 'uapbd/refer/org/FinanceOrgTreeRef/index' ||
		item.refcode == 'uapbd/refer/org/LiabilityCenterOrgTreeRef/index'
	) {
		// 财务组织、利润中心树型过滤
		item.queryCondition = () => {
			return { TreeRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter' };
		};
	} else {
		// 其他组织表型过滤
		item.queryCondition = () => {
			return { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter' };
		};
	}
}

/**
 * 部门人员参照过滤
 * @param {*} item 
 * @param {*} filter 
 */
function psnAndDeptRefCondition(item, filter) {
	if (
		item.refcode == 'uapbd/refer/org/DeptNCTreeRef/index' ||
		item.refcode == 'uapbd/refer/psninfo/PsndocTreeGridRef/index'
	) {
		// 添加业务人员来源过滤条件
		filter['busifuncode'] = IBusiRoleConst.ASSETORG;
	}
	return filter;
}

/** 
 * *获取查询条件的值
 */
function getSearchValue(props, searchAreaId, field) {
	let data = props.search.getSearchValByField(searchAreaId, field);
	if (data && data.value && data.value.firstvalue) {
		let firstvalue = data.value.firstvalue;
		if (firstvalue.split(',').length == 1) {
			return firstvalue;
		}
	}
}

/**
 * 设置字段的过滤条件
 */
function setQueryCondition(props, item, searchAreaId, refField, filter, returnKey) {
	item.queryCondition = () => {
		let refFieldValue = getSearchValue.call(this, props, searchAreaId, refField); //主组织
		if (refFieldValue) {
			filter[returnKey] = refFieldValue;
		}
		return filter;
	};
}
