import { IBusiRoleConst } from '../../const/CommonKeys';
import { AssetOrgMultiRefFilter } from '../MultiRefFilter/AssetOrgMultiRefFilter';
/**
 * 处理自定义项——参照类型的组织条件传递
 * @param props 
 * @param item 对应模板中的某个自定义项元素
 * @param areaId 表头区域编码/查询区域编码
 * @param pk_group 集团主键
 * @param isQuery 是否查询区域，默认false
 * @param orgField 组织字段,默认pk_org(用于获取指定区域组织的值)
 * @param defPrefix 自定义项字段前缀，默认def
 * @param multiRefFilter  查询区域时需要传递主组织多选处理的回调方法，默认是AssetOrgMultiRefFilter，具体根据当前单据的主组织属性决定选择哪个
*/
export function defRefCondition(
	props,
	item,
	areaId,
	pk_group,
	isQuery = false,
	orgField = 'pk_org',
	defPrefix = 'def',
	multiRefFilter = AssetOrgMultiRefFilter
) {
	if (item.attrcode && defPrefix && item.attrcode.indexOf(defPrefix) == 0) {
		if (isQuery) {
			multiRefFilter.call(this, props, areaId, item);
			item.queryCondition = () => {
				return { pk_group, busifuncode: IBusiRoleConst.ASSETORG };
			};
		} else {
			item.queryCondition = () => {
				let pk_org = props.form.getFormItemsValue(areaId, orgField).value;
				return { pk_org, pk_group, busifuncode: IBusiRoleConst.ASSETORG };
			};
		}
	}
}

/**
 * 查询区参照公共处理
 * @param {*} props 
 * @param {*} item 
 */
export function commonRefCondition(props, item) {
	// 添加【执行时包含下级】默认勾选
	addRunWithChildren.call(this, props, item);
	// 添加【显示停用】勾选框
	addShowDisabledData.call(this, props, item);
}

/**
 * 添加【执行时包含下级】默认勾选
 * @param {*} props 
 * @param {*} item 
 */
function addRunWithChildren(props, item) {
	if (item && item.itemtype == 'refer') {
		let refCodes = [
			'uapbd/refer/am/CategoryTreeRef/index', //资产类别
			'ampub/refer/equipbase/AddReduceStyleTreeRef/index', //增减方式
			'ampub/refer/equipbase/LocationTreeRef/index', //位置
			'fa/refer/fabase/category/index', //固定资产类别
			'fa/refer/fabase/usingstatus/index' //使用状况
		];
		if (refCodes.includes(item.refcode)) {
			item.isRunWithChildren = true; //显示
			item.defaultRunWithChildren = true; //默认勾选
			// 使用部门或管理部门(只适用【使用或管理部门】字段编码包含【pk_mandept、pk_usedept】的，如不是还得自己适配)
		} else if (item.attrcode.includes('pk_mandept') || item.attrcode.includes('pk_usedept')) {
			// 添加【执行时包含下级】默认勾选
			item.isRunWithChildren = true;
			item.defaultRunWithChildren = true;
		}
	}
}

/**
* 添加【显示停用】勾选框
* @param {*} props 
* @param {*} item 
*/
function addShowDisabledData(props, item) {
	if (item && item.itemtype == 'refer') {
		item.isShowDisabledData = true;
	}
}
