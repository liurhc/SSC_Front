import { IBusiRoleConst } from '../../../../ampub/common/const/CommonKeys';
import { AssetOrgMultiRefFilter } from '../../../../ampub/common/components/MultiRefFilter/AssetOrgMultiRefFilter';
import { FinanceOrgMultiRefFilter } from '../../../../ampub/common/components/MultiRefFilter/OtherOrgMultiRefFilter';
import { commonRefCondition } from '../../../../ampub/common/components/ReferInit/refInit';

/**
 * 初始化查询控件控制相关事件处理类。
 * @param {*} props 
 * @param {*} meta 过滤的数据 
 * @param {*} searchId 查询区id
 * @param {*} prefix 前缀
 */
export function initCriteriaChangedHandlers(props, meta, searchId, prefix = '') {
	let items = meta[searchId].items;
	items.forEach((item) => {
		let itemcode = item.attrcode;
		appPermissionOrgRefFilter.call(this, item, props);
		//根据货主管理组织过滤
		if (
			itemcode.includes(prefix === '' ? 'pk_mandept' : prefix + '.pk_mandept') ||
			itemcode.includes(prefix === '' ? 'pk_manager' : prefix + '.pk_manager')
		) {
			filter.call(this, props, item, searchId, prefix === '' ? 'pk_ownerorg' : prefix + '.pk_ownerorg');
			if (itemcode.includes('pk_mandept')) {
				// 添加【执行时包含下级】默认勾选
				item.isRunWithChildren = true;
				item.defaultRunWithChildren = true;
			}
		}
		//根据使用权过滤
		if (
			itemcode.includes(prefix === '' ? 'pk_usedept' : prefix + '.pk_usedept') ||
			itemcode.includes(prefix === '' ? 'pk_user' : prefix + '.pk_user')
		) {
			filter.call(this, props, item, searchId, prefix === '' ? 'pk_usedunit' : prefix + '.pk_usedunit');
			if (itemcode.includes('pk_usedept')) {
				// 添加【执行时包含下级】默认勾选
				item.isRunWithChildren = true;
				item.defaultRunWithChildren = true;
			}
		}
		//根据使用组织过滤
		if (
			itemcode.includes('pk_jobmngfil') ||
			itemcode.includes('pk_supplier') ||
			itemcode.includes('pk_location') ||
			itemcode.includes('fk_parent')
		) {
			AssetOrgMultiRefFilter.call(
				this,
				props,
				searchId,
				item,
				prefix === '' ? 'pk_usedorg' : prefix + '.pk_usedorg'
			);
		}
		//根据财务组织过滤
		if (itemcode.includes('pk_card')) {
			FinanceOrgMultiRefFilter.call(
				this,
				props,
				searchId,
				item,
				prefix === '' ? 'pk_fiorg' : prefix + '.pk_fiorg'
			);
		}
		commonRefCondition.call(this, props, item);
	});
}
/**
 *  给查询参照增加业务单元过滤
 *  初始化没有编辑组织，直接编辑需要业务单元过滤的字段，则显示所有有权限的业务单元
 * @param {*} props 
 * @param {*} searchId 查询区域id
 * @param {*} item 操作的字段
 * @param {*}key 过滤参照项的key，比如设备卡片的是货主管理组织key传‘pk_ownerorg’，一般为‘pk_org’
 */
export function AssetOrgMultiRefFilter1(props, searchId, item, key = 'pk_org') {
	item.isShowUnit = true;
	item.unitCondition = () => {
		let pk_org = props.search.getSearchValByField(searchId, key).value.firstvalue; // 调用相应组件的取值API
		return {
			unit_pks: pk_org,
			TreeRefActionExt: 'nccloud.web.fa.fabase.ref.refCodition.UnitSqlBuilder'
		};
	};
}
/**
 * 权限组织过滤包括资产组织，财务组织，货主管理组织，使用管理组织
 */
function appPermissionOrgRefFilter(item, props) {
	// 字段编码
	let itemcode = item.attrcode;
	if (itemcode.endsWith('pk_org') || itemcode.endsWith('pk_ownerorg') || itemcode.endsWith('pk_usedorg')) {
		item.queryCondition = () => {
			return { GridRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter' };
		};
	}

	if (itemcode.endsWith('pk_fiorg') || itemcode.endsWith('pk_org_fa')) {
		item.queryCondition = () => {
			return { TreeRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter' };
		};
	}
}

/**
 * 特殊字段根据组织过滤
 * @param {*} props 
 * @param {*} meta 过滤的数据 
 * @param {*} searchId 查询区id
 * @param {*} fieldArry 需要过滤的数组的字段
 * @param {*} org 过滤的组织
　* @param {*} flag 是否根据　IBusiRoleConst.ASSETORG　过滤
 */
export function filterByOrg(props, meta, searchId, fieldArry, org, flag) {
	let items = meta[searchId].items;
	if (typeof fieldArry == 'object' && fieldArry.constructor == Array) {
		fieldArry.forEach((field) => {
			items.map((item) => {
				if (item.attrcode == field) {
					if (flag) {
						filter.call(this, props, item, searchId, org);
					} else {
						AssetOrgMultiRefFilter.call(this, props, searchId, item, org);
					}
				}
			});
		});
	}
}
//过滤
function filter(props, item, searchId, org) {
	AssetOrgMultiRefFilter.call(this, props, searchId, item, org);
	item.queryCondition = () => {
		let pk_org = getSearchValue.call(this, props, searchId, org); //主组织
		let filter = { busifuncode: IBusiRoleConst.ASSETORG };
		if (pk_org) {
			filter['pk_org'] = pk_org;
		}
		return filter; // 根据pk_org过滤
	};
}
//获取查询条件的值
function getSearchValue(props, searchId, org) {
	let data = props.search.getSearchValByField(searchId, org);
	if (data && data.value && data.value.firstvalue) {
		let contion = data.value.firstvalue.split(',');
		if (contion.length == 1) {
			return data.value.firstvalue;
		}
	}
}
