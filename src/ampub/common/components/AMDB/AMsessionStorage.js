import React, { Component } from 'react';
/**
 * 资产sessionStorage统一处理类(后续扩展加密等处理)
 * */
export const amSessionStorage ={
    setItem : (sessonKey, obj) =>{
        sessionStorage.setItem(sessonKey, obj);
    },
    getItem : (key) => {
       return sessionStorage.getItem(key);
    },
    removeItem: (key) =>{
        sessionStorage.removeItem(key);
    }
}