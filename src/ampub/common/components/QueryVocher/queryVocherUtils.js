import { ajax, cacheTools } from 'nc-lightapp-front';
import { pageConfig } from '../../const/VoucherConst';
import { BILLSTATUS } from '../../const/StatusUtils';
import { MsgConst, showMessage } from '../../../../ampub/common/utils/msgUtils';

const {
	appCode,
	pageCode,
	linkCacheWord,
	previewCacheWord,
	url,
	billtypeNoLink,
	billtypeToHandle,
	billtypeToPreview,
	relatReg
} = pageConfig;

/**
 * 联查凭证
 * @param {*} props
 * @param {*} tableId 列表表体编码
 * @param {*} pk 单据主键
 * @param {*} appcode 应用appcode
 */
export function queryAboutVoucher(
	props,
	tableId,
	pk,
	appcode,
	islist = false,
	isAlter = false,
	isUnique = false,
	pks,
	billtype,
	pk_group,
	pk_org
) {
	let appId_LinkVouchar;
	let dataArray;
	let viewData;
	// 1. 如果为固定资产折旧联查凭证，则特殊处理
	if (isUnique) {
		dataArray = [];
		if (pk_org.multi) {
			// 如果是集中计提联查凭证  需要遍历各个组织信息
			for (var orgvalue in pk_org.pks) {
				for (let pk of pk_org.pks[orgvalue]) {
					let obj = {
						pk_billtype: billtype,
						relationID: pk,
						pk_group: pk_group,
						pk_org: orgvalue
					};
					dataArray.push(obj);
				}
			}
		} else {
			for (let pk of pks) {
				let obj = {
					// pk_billtype 固定资产实际传入的是交易类型
					pk_billtype: billtype,
					relationID: pk,
					pk_group: pk_group,
					pk_org: pk_org
				};
				dataArray.push(obj);
			}
		}
		// 命名格式为 appid_LinkVouchar,其中appid为自己小应用的应用编码
		appId_LinkVouchar = appcode + linkCacheWord;

		// 2. 常规联查凭证处理
	} else {
		// 获取选择数据
		let data;

		// 列表态联查
		if (islist) {
			let checkedData = props.table.getCheckedRows(tableId);
			if (checkedData && checkedData.length > 0) {
				data = checkedData[0].data;
			} else {
				showMessage.call(this, props, { type: MsgConst.Type.ChooseLinkQuery }); /*国际化处理：请选择需要联查的数据*/
				return;
			}
			// 卡片态联查
		} else {
			let form_data = props.form.getAllFormValue(tableId);
			if (form_data && form_data.rows && form_data.rows.length > 0 && form_data.rows[0]) {
				data = form_data.rows[0];
			} else {
				showMessage.call(this, props, { type: MsgConst.Type.ChooseLinkQuery }); /*国际化处理：请选择需要联查的数据*/
				return;
			}
		}

		dataArray = [];
		
		if (data) {
			// 加载关联id
			let relationIDs = loadRelationID(data, pk);
			// 预览凭证标示
			let previewFlag = false;

			// 当前单据支持预览凭证 且 未审批通过的走预览处理
			if (
				billtypeToPreview.indexOf(data.values.bill_type.value) != -1 &&
				BILLSTATUS.check_pass != data.values.bill_status.value
			) {
				previewFlag = true;
			}

			for (let relationID of relationIDs) {
					//数据组装
					let obj = {
						// pk_billtype 固定资产实际传入的是交易类型
						pk_billtype: null, //联查凭证不需要pk_billtye , 否则部分单据会查不到凭证
						relationID: relationID,
						pk_group: data.values.pk_group.value,
						pk_org: data.values.pk_org.value
					};
					dataArray.push(obj);				
			}

			// 命名格式为 appid_LinkVouchar,其中appid为自己小应用的应用编码
			appId_LinkVouchar = appcode + linkCacheWord;

			// 预览处理
			if (previewFlag) {
				appId_LinkVouchar = appcode + previewCacheWord;

				viewData = {
					messagevo: [
						{
							pk_system: 'fa', //系统编码
							relationid: data.values[pk].value, //单据主键
							pk_org: data.values.pk_org.value, //组织主键
							pk_group: data.values.pk_group.value, //集团主键
							pk_billtype: data.values.transi_type.value //交易类型
						}
					],
					desbilltype: [ 'C0' ], //目标单据类型,C0为总账凭证,目前只支持总张凭证
					srcbilltype: data.values.bill_type.value == 'HG' ? 'HZ' : data.values.bill_type.value //业务组单据类型,变动单特殊处理一下
				};
			}
		}
	}
	//调转凭证界面
	openToVouchar(props, appId_LinkVouchar, dataArray,viewData);
}

//调转凭证界面
/**
 * 
 * @param {*} props 
 * @param {*} appId_LinkVouchar 
 * @param {*} querydata  查询参数
							let querydata = [{
								pk_group: , //集团主键
								pk_org: , //组织主键
								relationID: , //单据主键
								pk_billtype: //交易类型
							}];
 * @param {*} viewData 预览数据
							var viewData = {
								"messagevo": [{
									"pk_system": , //系统编码
									"relationid": ,//单据主键
									"busidate": ,//单据日期
									"pk_org": ,//组织主键 
									"pk_group": ,//集团主键
									"pk_billtype": //交易类型
								}],//支持多笔预览
								"desbilltype": ["C0"],//目标单据类型,C0为总账凭证,目前只支持总张凭证
								"srcbilltype": //业务组交易类型
							}
 */
export function openToVouchar(props, appId_LinkVouchar, querydata ,viewData) {	

	ajax({
		url: url.queryVoucherUrl,
		data: querydata,
		async: false,
		success: (res) => {
			if (res.success) {	
				let srcCode = res.data.src;
				if ('_LinkVouchar2019' == srcCode) {
                    //走联查
                    if(res.data.des){//跳转到凭证界面
                        if(res.data.pklist){
                            if(res.data.pklist.length == 1){//单笔联查
                                props.openTo(res.data.url, {
                                    status: 'browse',
                                    appcode: res.data.appcode,
                                    pagecode: res.data.pagecode,
                                    id:res.data.pklist[0],
                                    backflag:'noback'
                                });
                                return;
                            }else{//多笔联查
                                cacheTools.set("checkedData", res.data.pklist);
                                props.openTo(res.data.url, {
                                    status: 'browse',
                                    appcode: res.data.appcode,
                                    pagecode: res.data.pagecode,                                                                        
                                });
                                return;
                            }
                            
                        }
                    }else{//跳转到会计平台 这里的appcode是业务组的小应用编码
                        cacheTools.set(appId_LinkVouchar, res.data.pklist);
                    }
                    
                } else if ('_Preview2019' == srcCode) {
					//走预览 这里的appcode是业务组的小应用编码
					if(viewData){
						cacheTools.set(appId_LinkVouchar, viewData);
					}
				}
				if(viewData){
					//打开凭证节点
					props.openTo(res.data.url, {
						status: 'browse',
						appcode: res.data.appcode,
						pagecode: res.data.pagecode,
						scene: appId_LinkVouchar                    
					});
				} else {
					showMessage.call(this, props, { type: MsgConst.Type.LinkQueryNoData }); /*国际化处理：未联查出数据*/
				}            
			} else {
				showMessage.call(this, props, { type: MsgConst.Type.LinkQueryNoData }); /*国际化处理：未联查出数据*/
			}
		}
	});
}

//加载关联项id
function loadRelationID(item, pk) {
	let bill_type = item.values.bill_type.value;
	let pk_bill = item.values[pk].value;
	//不需要联查的直接返回（所有资产单据不需要联查，同时固定资产减值及评估不需要联查）
	let result = [];
	if (bill_type.indexOf('H') != 0 || billtypeNoLink.indexOf(bill_type) != -1) {
		result.push(pk_bill);
		return result;
	}
	let pk_group = item.values.pk_group.value;
	let pk_org = item.values.pk_org.value;
	let pk_accbook = '';
	if (item.values.pk_accbook) {
		pk_accbook = item.values.pk_accbook.value;
	}

	//卡片可以特殊处理的直接组装数据返回
	if (billtypeToHandle.indexOf(bill_type) != -1) {
		let relationID = pk_bill + relatReg + pk_accbook;
		result.push(relationID);
		return result;
	}

	//其余单据走接口查询
	//接口参数
	let urlData = {
		bill_type: bill_type,
		pk_org: pk_org,
		pk_bill: pk_bill,
		pk_accbook: pk_accbook,
		pk_group: pk_group
	};
	ajax({
		url: url.queryRelationIDUrl,
		data: urlData,
		async: false,
		success: (res) => {
			if (res && res.data) {
				result = res.data;
			}
		}
	});

	return result;
}
