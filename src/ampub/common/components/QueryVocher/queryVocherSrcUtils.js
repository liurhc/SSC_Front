import { ajax, cacheTools } from 'nc-lightapp-front';
import { pageConfig } from '../../const/VoucherConst';
import { UISTATE } from '../../const/StatusUtils';
import { openAssetCardByPk } from '../QueryAbout/faQueryAboutUtils';
import { MsgConst, showMessage } from '../../../../ampub/common/utils/msgUtils';

const { url, cardRouter } = pageConfig;

/**
 * 凭证联查来源单据
 * @param {*} props
 * @param {*} tableId 列表表体编码
 * @param {*} pkField 单据主键字段
 * @param {*} pagecode 应用pagecode
 * @param {*} queryByPksUrl 通过pk单据查询扩展 (如果为空走默认处理)
 */
export function queryVoucherSrc(props, tableId, pkField, pagecode, queryByPksUrl) {
	//凭证联查单据
	let src = props.getUrlParam('scene'); //获取联查场景
	if (src) {
		if ('fip' == src) {
			//fip代表会计平台
			//执行第2步
			let checkedData = [];
			checkedData = cacheTools.get('checkedData');
			let data = {
				checkedData,
				pagecode
			};
			ajax({
				url: url.queryVoucherSrcUrl,
				data: data,
				success: (res) => {
					let { success, data } = res;
					if (success) {
						if (data) {
							//如果只有一行,则在返回到列表界面之后再次跳转到卡片界面
							if (data[tableId].rows.length == 1) {
								let pk = data[tableId].rows[0].values[pkField].value;
								//固定资产卡片跳转特殊处理
								if ('pk_card' == pkField) {
									//当前页签跳转
									openAssetCardByPk(props, pk, { status: UISTATE.browse, target: 'self' });
								} else {
									props.table.setAllTableData(tableId, data[tableId]);
									props.pushTo(cardRouter, {
										pagecode: pagecode.replace('list', 'card'),
										status: UISTATE.browse,
										id: pk ? pk : ''
									});
								}
							} else {
								//多行数据 保持列表态
								//扩展支持通过pk进行单据自定义查询
								if (queryByPksUrl) {
									let pks = new Array();
									for (let item of data[tableId].rows) {
										pks.push(item.values[pkField].value);
									}
									let AMQueryPageInfo = {
										allpks: pks,
										pagecode
									};
									ajax({
										url: queryByPksUrl,
										data: AMQueryPageInfo,
										success: (res) => {
											let { success, data } = res;
											if (success) {
												if (data) {
													props.table.setAllTableData(tableId, data[tableId]);
												}
											}
										}
									});
								} else {
									props.table.setAllTableData(tableId, data[tableId]);
								}
							}
						} else {
							props.table.setAllTableData(tableId, { rows: [] });
							showMessage.call(this, props, { type: MsgConst.Type.LinkQueryNoData }); /*国际化处理：未联查出数据*/
						}
					}
				}
			});
		}
	}
}
