/**
 *  给查询参照增加业务单元过滤
 *  初始化没有编辑组织，直接编辑需要业务单元过滤的字段，则显示所有有权限的业务单元
 * @param {*} props 
 * @param {*} searchId 查询区域id
 * @param {*} item 操作的字段
 * @param {*}key 过滤参照项的key，比如设备卡片的是货主管理组织key传‘pk_ownerorg’，一般为‘pk_org’
 */
export function AssetOrgMultiRefFilter(props, searchId, item, key = 'pk_org') {
	item.isShowUnit = true;
	item.unitCondition = () => {
		let pk_org = props.search.getSearchValByField(searchId, key).value.firstvalue; // 调用相应组件的取值API
		return {
			unit_pks: pk_org,
			GridRefActionExt: 'nccloud.web.ampub.common.refCodition.UnitSqlBuilder',
			TreeRefActionExt: 'nccloud.web.ampub.common.refCodition.UnitSqlBuilder'
		};
	};
}

/**
 * 根据组织字段是否多选控制其他参照是否有业务单元过滤的参照框
 * @param {*} props 
 * @param {*} value 
 * @param {*} searchId 查询区id
 * @param {*} fieldArry 需要参照过滤的字段数组
 */
export function isMultiCorpRefHandler(props, value, searchId, fieldArry) {
	let meta = props.meta.getMeta();
	//遍历传入的字段，分别给字段添加业务单元过滤，如果组织多选，则有业务单元过滤，如果是
	//单选则没有，组织不选的情况下参照所有的业务单元
	if (typeof fieldArry == 'object' && fieldArry.constructor == Array) {
		if (value && value.length == 1) {
			fieldArry.forEach((field) => {
				meta[searchId].items.map((item) => {
					if (item.attrcode == field) {
						item.isShowUnit = false;
					}
					return item;
				});
			});
		} else {
			fieldArry.forEach((field) => {
				meta[searchId].items.map((item) => {
					if (item.attrcode == field) {
						item.isShowUnit = true;
					}
					return item;
				});
			});
		}
	}
	props.meta.setMeta(meta);
}
