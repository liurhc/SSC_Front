/**
 *  财务组织过滤
 *  初始化没有编辑组织，直接编辑需要业务单元过滤的字段，则显示所有有权限的业务单元
 * @param {*} props 
 * @param {*} searchId 查询区域id
 * @param {*} item 操作的字段
 */
export function FinanceOrgMultiRefFilter(props, searchId, item, key = [ 'pk_org' ]) {
	item.isShowUnit = true;
	item.unitProps = {
		multiLang: {
			domainName: 'uapbd',
			// currentLocale: 'simpchn',
			moduleId: 'refer_uapbd'
		},
		refType: 'tree',
		refName: 'refer-000201' /* 国际化处理： 业务单元*/,
		placeholder: 'refer-000201' /* 国际化处理： 业务单元*/,
		// type: 'dropDown',
		refcode: 'uapbd/refer/org/FinanceOrgTreeRef/index',
		queryTreeUrl: '/nccloud/uapbd/org/FinanceOrgTreeRef.do',
		isMultiSelectedEnabled: false,
		isShowDisabledData: true,
		onlyLeafCanSelect: false,
		key: 'pk_org'
	};
	item.unitCondition = () => {
		let retData = {};
		for (let i = 0; i < key.length; i++) {
			let pk_org_value = props.search.getSearchValByField(searchId, key[i]); // 调用相应组件的取值API
			if (pk_org_value && pk_org_value.value && pk_org_value.value.firstvalue) {
				retData['pk_org'] = pk_org_value.value.firstvalue;
				break;
			}
		}
		retData['TreeRefActionExt'] = 'nccloud.web.ampub.common.refCodition.FinanceOrgSqlBuilder';
		return retData;
	};
}
