/**
 *  给查询参照增加业务单元过滤
 *  初始化没有编辑组织，直接编辑需要业务单元过滤的字段，则显示所有有权限的业务单元
 * @param {*} props 
 * @param {*} searchId 查询区域id
 * @param {*} formId 表单区域id
 * @param {*} item 操作的字段
 * @param {*}key 过滤参照项的key，比如设备卡片的是货主管理组织key传‘pk_ownerorg’，一般为‘pk_org’
 */
function MaintainOrgMultiRefFilter(props, searchId, formId, item, key = 'pk_org') {
	item.isShowUnit = true;
	item.unitCondition = () => {
		let pk_org = undefined;
		if (searchId) {
			pk_org = props.search.getSearchValByField(searchId, key).value.firstvalue;
		} else if (formId) {
			pk_org = props.form.getFormItemsValue(formId, key).value;
		}
		return {
			unit_pks: pk_org,
			GridRefActionExt: 'nccloud.web.ampub.common.refCodition.MaintainOrgSqlBuilder',
			TreeRefActionExt: 'nccloud.web.ampub.common.refCodition.MaintainOrgSqlBuilder'
		};
	};
}

export { MaintainOrgMultiRefFilter };
