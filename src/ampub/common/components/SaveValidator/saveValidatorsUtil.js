import { MsgConst, showMessage } from '../../../../ampub/common/utils/msgUtils';

/**
 * 保存前数据校验
 * @param {*} props 
 * @param {*} formId 
 * @param {*} tableId 
 * @param {*} keys 空行不需要判断的字段，至少传递主表主键和子表主键
 * @param {*} needBody 
 */
export function beforeSaveValidator(props, formId, tableId, keys = [], needBody = true) {
	if (tableId) {
		props.cardTable.closeModel(tableId);
	}

	// 校验表头必输项
	if (formId) {
		let headFlg = props.form.isCheckNow(formId);
		if (!headFlg) {
			return false;
		}
	}
	// 如果是后台默认添加的行，则【dr、rowno】会有值，因此要把这两个值排除在外。
	if (tableId) {
		let mustKeys = [ 'ts', 'dr', 'rowno', 'pseudocolumn' ];
		keys = [ ...keys, ...mustKeys ];
		// 清除空表体白行
		props.cardTable.filterEmptyRows(tableId, keys);
		if (needBody) {
			// 校验表体行是否有数据
			let visibleRows = props.cardTable.getVisibleRows(tableId);
			if (visibleRows && visibleRows.length == 0) {
				showMessage.call(this, props, { type: MsgConst.Type.BodyNotNull });
				return false;
			}
		}
		// 校验表体必输项
		let bodyFlg = props.cardTable.checkTableRequired(tableId);

		if (!bodyFlg) {
			return false;
		}
	}

	return true;
}
