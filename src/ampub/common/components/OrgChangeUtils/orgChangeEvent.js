import { promptBox } from 'nc-lightapp-front';
import { getMultiLangByID } from '../../utils/multiLangUtils';

/**
 * 组织切换事件弹窗
 * @param {*} props 
 * @param {*} formId 页面表头区域
 * @param {*} tableId 页面表体区域 ,可以穿单个子表或多子表传数组，不需要清空表体的传 null
 * @param {*} key 编辑的字段
 * @param {*} value 字段的新值
 * @param {*} oldValue 字段的旧值
 * @param {*} fields 表头需要清空的字段,需要全部清空的传null,不需要前台处理清空，后台清空传false
 * @param {*} tableBtns 组织为空需要置灰的按钮
 * @param {*} callback 确认按钮成功的回调，向后台发送请求处理组织编辑事件,默认增行等的处理
 */
export function orgChangeEvent(props, pagecode, formId, tableId, key, value, oldValue, fields, tableBtns, callback) {
	//弹窗参数
	let modalConfig = orgChangeModalConfig.call(
		this,
		props,
		formId,
		tableId,
		key,
		value,
		oldValue,
		fields,
		tableBtns,
		callback
	);

	if (value && typeof value.value == 'string') {
		// 恢复字段的可编辑性
		if (tableBtns) {
			props.button.setButtonDisabled(tableBtns, false);
		}
		if (!oldValue || !oldValue.value) {
			props.resMetaAfterPkorgEdit();
			if (typeof callback == 'function') {
				callback();
			}
		}
		if (oldValue && oldValue.value && oldValue.value != '' && oldValue.value != value.value) {
			// props.ncmodal.show(`${pagecode}-confirm`, modalConfig);
			promptBox(modalConfig);
		}
	} else {
		// props.ncmodal.show(`${pagecode}-confirm`, modalConfig);
		promptBox(modalConfig);
	}
}

/**
 * 创建弹窗参数 
 */
function orgChangeModalConfig(props, formId, tableId, key, value, oldValue, fields, tableBtns, callback) {
	let config = {
		color: 'warning',
		title: getMultiLangByID('orgChange-000001') /** 确认修改*/,
		content: getMultiLangByID('orgChange-000000') /**'是否修改组织，这样会清空您录入的信息?' */,
		closeByClickBackDrop: false, //点击遮罩不关闭提示框
		//点击确定按钮事件
		beSureBtnClick: () => {
			if (!value || !value.value) {
				if (oldValue && oldValue.value) {
					if (tableBtns) {
						props.button.setButtonDisabled(tableBtns, true);
					}
					props.initMetaByPkorg(key);
				}
			}
			// 清空表头字段数据及表体数据,不需要前台处理的，tableid或field为false
			if (fields !== false) {
				clearHeadBodyData.call(this, props, formId, tableId, key, value, fields);
			}
			//组织切换事件走后台清空数据
			if (typeof callback == 'function') {
				callback();
			}
		},
		//取消按钮事件回调
		cancelBtnClick: () => {
			resetData.call(this, props, formId, key, oldValue);
		}
	};
	return config;
}

/**
 * 确认按钮
 * @param {*} fields 需要清空的表头字段，不传的话全部清空
 */
function clearHeadBodyData(props, formId, tableId, key, value, fields) {
	//清空表体数据
	if (typeof tableId == 'string') {
		delLine.call(this, props, tableId);
		// props.cardTable.setTableData(tableId, { rows: [] });
	} else if (Array.isArray(tableId)) {
		//多子表
		tableId.forEach((body) => {
			delLine.call(this, props, body);
		});
	}
	if (fields) {
		fields.forEach((field) => {
			props.form.setFormItemsValue(formId, { [field]: { value: null, display: null } });
		});
	} else {
		props.form.EmptyAllFormValue(formId);
		//清空界面重新赋值
		resetData.call(this, props, formId, key, value);
	}
}

/**
 * 取消按钮
 */
function resetData(props, formId, key, oldValue) {
	props.form.setFormItemsValue(formId, { [key]: oldValue });
	let data = {
		type: 'form',
		areaCode: formId,
		key: key,
		value: oldValue,
		callback: () => {},
		changedrows: null,
		index: null,
		formMetaCode: formId
	};
	props.handleRelationItems(data);
	return;
}

/**删出表体所有行 */
function delLine(props, moduleId) {
	let allrow = props.cardTable.getNumberOfRows(moduleId);
	let indexs = [];
	for (let i = 0; i < allrow; i++) {
		indexs.push(i);
	}
	props.cardTable.delRowsByIndex(moduleId, indexs);
}
