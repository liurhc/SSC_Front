/*
  影像扫描和影像查看
*/
import { showMessage } from '../../../common/utils/msgUtils';
import { imageScan, imageView } from 'sscrp/rppub/components/image';
// import { imageScan, imageView } from '../../../../sscrp/rppub/components/image';
import { getMultiLangByID } from '../../../../ampub/common/utils/multiLangUtils';

//影像扫描
function amImageScan(props, imageData = {}) {
	let { pagecode, formId, tableId, pkField } = imageData;

	//空判断
	if (!props || !pagecode || !formId || !pkField) {
		showMessage.call(this, props, {
			color: 'error',
			content: getMultiLangByID('amcommon-000008') /* 国际化处理： 参数不可为空！*/
		});
		return;
	}

	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let billdata = props.createMasterChildData(pagecode, formId, tableId);
	let headData = billdata.head[formId].rows[0].values;

	let billInfoMap = {};

	//基础字段 单据pk，单据类型，交易类型，单据的组织
	billInfoMap.pk_billid = pk;
	billInfoMap.pk_billtype = headData.bill_type.value;
	billInfoMap.pk_tradetype = headData.transi_type.value;
	billInfoMap.pk_org = headData.pk_org.value;

	//影像所需 FieldMap
	billInfoMap.BillType = headData.transi_type.value;
	billInfoMap.BillDate = headData.billmaketime.value;
	billInfoMap.Busi_Serial_No = pk;
	billInfoMap.OrgNo = headData.pk_org.value;
	billInfoMap.BillCode = headData.bill_code.value;
	billInfoMap.OrgName = headData.pk_org.display;
	billInfoMap.Cash = '0.00';

	imageScan(billInfoMap, 'iweb');
}

//影像查看
function amImageView(props, imageData = {}) {
	let { pagecode, formId, tableId, pkField } = imageData;
	//空判断
	if (!props || !pagecode || !formId || !pkField) {
		showMessage.call(this, props, {
			color: 'error',
			content: getMultiLangByID('amcommon-000008') /* 国际化处理： 参数不可为空！*/
		});
		return;
	}

	let pk = props.form.getFormItemsValue(formId, pkField).value;
	let billdata = props.createMasterChildData(pagecode, formId, tableId);
	let headData = billdata.head[formId].rows[0].values;

	let billInfoMap = {};

	//基础字段 单据pk，单据类型，交易类型，单据的组织
	billInfoMap.pk_billid = pk;
	billInfoMap.pk_billtype = headData.bill_type.value;
	billInfoMap.pk_tradetype = headData.transi_type.value;
	billInfoMap.pk_org = headData.pk_org.value;

	imageView(billInfoMap, 'iweb');
}

export { amImageScan, amImageView };
