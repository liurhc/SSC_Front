import { MsgConst, showMessage } from './msgUtils';
import { UISTATE } from '../const/StatusUtils';
import { getMultiLangByID } from './multiLangUtils';

/**
 * 列表常量
 */
export const tableConst = {
	QUERYINFOKEY: {
		dataSource: 'queryInfoCache',
		isQueryed: 'isQueryed',
		queryInfo: 'queryInfo'
	},
	SETVALUETYPE: {
		default: 'default', // 默认：不提示
		query: 'query', // 查询：提示查询成功，共多少条
		refresh: 'refresh' // 刷新：提示刷新成功
	},
	TABLEID: 'list_head',
	TABLEINNERID: 'list_inner',
	FIELDS: {
		enablestate: 'enablestate'
	}
};

/**
 * 提交前数据转换
 * 停启用状态转换
 * @param {*} props 
 */
export function modifyBeforeAjaxByIndex(props, config = {}) {
	let { tableId = tableConst.TABLEID, index } = config;
	let num = props.editTable.getNumberOfRows(tableId);
	if (num < 0) {
		return;
	}
	if (typeof index == 'undefined') {
		let oldData = props.editTable.getColValue(tableId, 'enablestate');
		oldData.value.map((item, i) => {
			if (item === true) {
				props.editTable.setValByKeyAndIndex(
					tableId,
					i,
					'enablestate',
					{
						value: '2',
						display: oldData.display[i]
					},
					false
				);
			} else if (item === false || item === null || item === undefined) {
				props.editTable.setValByKeyAndIndex(
					tableId,
					i,
					'enablestate',
					{
						value: '3',
						display: oldData.display[i]
					},
					false
				);
			}
		});
	} else if (Array.isArray(index)) {
		let oldData = props.editTable.getColValue(tableId, 'enablestate');
		oldData.value.map((item, i) => {
			if (index.includes(i)) {
				if (item === true) {
					props.editTable.setValByKeyAndIndex(
						tableId,
						i,
						'enablestate',
						{
							value: '2',
							display: oldData.display[i]
						},
						false
					);
				} else if (item === false || item === null || item === undefined) {
					props.editTable.setValByKeyAndIndex(
						tableId,
						i,
						'enablestate',
						{
							value: '3',
							display: oldData.display[i]
						},
						false
					);
				}
			}
		});
	} else {
		let oldVal = props.editTable.getValByKeyAndIndex(tableId, index, 'enablestate');
		let newVal = {};
		if (oldVal.value === true) {
			newVal.value = '2';
		} else if (oldVal.value === false || oldVal.value === null || oldVal.value === undefined) {
			newVal.value = '3';
		}
		newVal.display = oldVal.display;
		if (newVal.hasOwnProperty('value')) {
			props.editTable.setValByKeyAndIndex(tableId, index, 'enablestate', newVal, false);
		}
	}
}

/**
 * 提交前数据转换
 * 停启用状态转换
 * @param {*} row 
 */
export function modifyBeforeAjax(row) {
	if (Array.isArray(row)) {
		row.map((item) => {
			modifyBeforeAjaxSingle(item);
		});
	} else {
		modifyBeforeAjaxSingle(row);
	}
}

/**
 * 提交前数据转换
 * 停启用状态转换
 * @param {*} row 
 */
function modifyBeforeAjaxSingle(row) {
	if (row && row.values && row.values['enablestate']) {
		let value = row.values['enablestate'].value;
		if (value === true) {
			row.values['enablestate'].value = '2';
		} else if (value === false || value === null || value === undefined) {
			row.values['enablestate'].value = '3';
		}
	}
}

/**
 * 提交后数据转换
 * 停启用状态转换
 * @param {*} row 
 */
export function modifyAfterAjaxByIndex(props, config = {}) {
	let { tableId = tableConst.TABLEID, index } = config;
	let num = props.editTable.getNumberOfRows(tableId);
	if (num < 0) {
		return;
	}
	if (typeof index == 'undefined') {
		let oldData = props.editTable.getColValue(tableId, 'enablestate');
		oldData.value.map((item, i) => {
			if (item === '2') {
				props.editTable.setValByKeyAndIndex(
					tableId,
					i,
					'enablestate',
					{
						value: true,
						display: oldData.display[i]
					},
					false
				);
			} else if (item === '1' || item === '3') {
				props.editTable.setValByKeyAndIndex(
					tableId,
					i,
					'enablestate',
					{
						value: false,
						display: oldData.display[i]
					},
					false
				);
			}
		});
	} else if (Array.isArray(index)) {
		let oldData = props.editTable.getColValue(tableId, 'enablestate');
		oldData.value.map((item, i) => {
			if (index.includes(i)) {
				if (item === '2') {
					props.editTable.setValByKeyAndIndex(
						tableId,
						i,
						'enablestate',
						{
							value: true,
							display: oldData.display[i]
						},
						false
					);
				} else if (item === '1' || item === '3') {
					props.editTable.setValByKeyAndIndex(
						tableId,
						i,
						'enablestate',
						{
							value: false,
							display: oldData.display[i]
						},
						false
					);
				}
			}
		});
	} else {
		let oldVal = { value: oldData.value[index], display: oldData.display[index] };
		let newVal = {};
		if (oldVal.value === '2') {
			newVal.value = true;
		} else if (oldVal.value === '1' || oldVal.value === '3') {
			newVal.value = false;
		}
		newVal.display = oldVal.display;
		if (newVal.hasOwnProperty('value')) {
			props.editTable.setValByKeyAndIndex(tableId, index, 'enablestate', newVal, false);
		}
	}
}

/**
 * 提交后数据转换
 * 停启用状态转换
 * @param {*} row 
 */
export function modifyAfterAjax(row) {
	if (Array.isArray(row)) {
		row.map((item) => {
			modifyAfterAjaxSingle(item);
		});
	} else {
		modifyAfterAjaxSingle(row);
	}
}

/**
 * 提交后数据转换
 * 停启用状态转换
 * @param {*} row 
 */
function modifyAfterAjaxSingle(row) {
	if (row && row.values && row.values['enablestate']) {
		let value = row.values['enablestate'].value;
		if (value === '2') {
			row.values['enablestate'].value = true;
		} else if (value === '1' || value === '3') {
			row.values['enablestate'].value = false;
		}
	}
}

/**
 * 获取集团数据行
 * @param {*} row 
 */
export function getGroupRowIndexs(props, tableId) {
	let groupRowIndexs = [];
	let allRows = props.editTable.getAllRows(tableId, true);
	if (!allRows || allRows.length == 0) {
		return groupRowIndexs;
	}
	allRows.map((item, index) => {
		let values = item.values;
		if (item.status != '3' && values) {
			if (
				values.pk_group &&
				values.pk_org &&
				values.pk_group.value == values.pk_org.value &&
				values.pk_group.value != 'GLOBLE00000000000000' &&
				values.pk_group.value != 'globle00000000000000'
			) {
				groupRowIndexs.push(index);
			}
		}
	});
	return groupRowIndexs;
}

/**
 * 单表创建操作列
 * @param {*} props 
 * @param {*} config 必须
 *               tableId:表格区id，默认list_head
 *               areaId:操作列按钮区id，默认list_inner
 *               width:宽度，默认110
 *               fixed:是否固定，默认right固定在右边
 *               getInnerBtns:显示按钮组，需要返回数组，默认显示所有按钮
 *               tableButtonClick:按钮点击事件
 *               nodeType:节点类型（全局globle/集团group/组织org，没有的话不用传）
 */
export function createOprationColumn(props, config = {}) {
	const tableId = config.tableId || tableConst.TABLEID;
	const areaId = config.areaId || tableConst.TABLEINNERID;
	const nodeType = config.nodeType;
	let width = config.width;
	let fixed = config.fixed || 'right';
	let getInnerBtns = config.getInnerBtns;
	let tableButtonClick = config.tableButtonClick;
	if (width && width.length > 0) {
		width = width.trim();
		if (!width.endsWith('px') && !width.endsWith('%')) {
			width += 'px';
		}
	} else {
		width = '110px';
	}

	// 添加表格操作列
	let oprCol = {
		label: getMultiLangByID('amcommon-000000') /* 国际化处理： 操作*/,
		visible: true,
		attrcode: 'opr',
		itemtype: 'customer',
		className: 'table-opr',
		// 锁定
		fixed,
		width,
		render: (text, record, index) => {
			let buttonAry;
			if (typeof getInnerBtns == 'function') {
				buttonAry = getInnerBtns.call(this, props, text, record, index);
			} else {
				if (record && record.values) {
					let recordVal = record.values;
					// 组织节点不允许删除集团数据
					let flag = false;
					if (nodeType == 'group') {
						// 集团节点的全局数据，不让删除
						flag =
							recordVal.pk_group &&
							(recordVal.pk_group.value == 'GLOBLE00000000000000' ||
								recordVal.pk_group.value == 'globle00000000000000');
					} else if (nodeType == 'org') {
						// 组织节点的全局或集团数据，不让删除
						flag =
							(recordVal.pk_group &&
								(recordVal.pk_group.value == 'GLOBLE00000000000000' ||
									recordVal.pk_group.value == 'globle00000000000000')) ||
							(recordVal.pk_group &&
								recordVal.pk_org &&
								recordVal.pk_group.value == recordVal.pk_org.value);
					}
					if (!flag) {
						buttonAry = [ 'Delete' ];
					}
				} else {
					buttonAry = [];
				}
			}
			return props.button.createOprationButton(buttonAry, {
				area: areaId,
				buttonLimit: 3,
				onButtonClick: (props, key) => {
					if (typeof tableButtonClick == 'function') {
						// 如果需要筛选前行序号(实际行序号)，请使用record.flterIndex，如果需要筛选后行序号(显示行序号)，请直接用index
						// 如果没有表格应用未添加筛选功能，可直接用index
						tableButtonClick.call(this, props, key, text, record, index, tableId);
					}
				}
			});
		}
	};
	return oprCol;
}

/**
 * 单表处理停启用状态开关
 * @param {*} props 
 * @param {*} config 必须
 *               tableId:表格区id，默认list_head
 *               meta:列表模板
 *               enableRow:启用方法，参数props, index
 *               disableRow:停用方法，参数props, index
 *               enableStateField:单据状态字段，默认enablestate
 */
export function createEnableSwitch(props, config = {}) {
	const tableId = config.tableId || tableConst.TABLEID;
	const meta = config.meta || {};
	const tableMeta = meta[tableId];
	const enableRow = config.enableRow;
	const disableRow = config.disableRow;
	const enableStateField = config.enableStateField || tableConst.FIELDS.enablestate;
	tableMeta.items.map((item) => {
		if (item.attrcode == enableStateField) {
			item.onPopconfirmCont = getMultiLangByID('msgUtils-000012') /* 国际化处理： 是否确认启用？*/;
			item.offPopconfirmCont = getMultiLangByID('msgUtils-000013') /* 国际化处理： 是否确认停用？*/;
			// 默认不直接停启用，需要后台返回成功才会相应停启用
			item.onSwitchBefore = () => {
				let rowData = props.editTable.getClickRowIndex(tableId) || {};
				let { record, index } = rowData;
				let flterIndex = record.flterIndex;
				if (flterIndex) {
					index = flterIndex;
				}
				if (record && record.values) {
					if (
						record.values[enableStateField].value === true ||
						record.values[enableStateField].value === '2'
					) {
						typeof disableRow == 'function' && disableRow.call(this, props, index);
					} else {
						typeof enableRow == 'function' && enableRow.call(this, props, index);
					}
				}
				// 停启用状态不变，后台成功后再处理
				return false;
			};
		}
		return item;
	});

	return meta;
}

/**
 * 列表表格设置值
 * @param {*} props 
 * @param {*} res 必须
 * @param {*} tableId:表格区id，默认list_head
 * @param {*} scene:使用场景，默认Default，可以传Query和Refresh
 */
export function setTableValue(props, res = {}, tableId = tableConst.TABLEID, scene = tableConst.SETVALUETYPE.default) {
	let data;
	if (res && res.hasOwnProperty('data')) {
		data = res.data;
	} else {
		data = res;
	}
	let status = props.editTable.getStatus(tableId) || UISTATE.browse;
	let isBrowse = status == UISTATE.browse;
	if (data && data[tableId] && data[tableId].rows && data[tableId].rows.length > 0) {
		let num = 0;
		if (data[tableId].hasOwnProperty('allpks')) {
			num = data[tableId]['allpks'].length;
		} else {
			num = data[tableId].rows.length;
		}
		if (status == UISTATE.browse) {
			modifyAfterAjax(data[tableId].rows);
		} else {
			modifyBeforeAjax(data[tableId].rows);
		}
		props.editTable.setTableData(tableId, data[tableId], isBrowse);
		if (scene == tableConst.SETVALUETYPE.query) {
			showMessage.call(this, props, {
				color: 'success',
				content: getMultiLangByID('msgUtils-000028', { num }) /* 国际化处理： 查询成功，共 {num} 条*/
			});
		} else if (scene == tableConst.SETVALUETYPE.refresh) {
			showMessage.call(this, props, { type: MsgConst.Type.RefreshSuccess });
		}
	} else {
		props.editTable.setTableData(tableId, { rows: [] }, isBrowse);
		if (scene == tableConst.SETVALUETYPE.query) {
			showMessage.call(this, props, { type: MsgConst.Type.QueryNoData });
		} else if (scene == tableConst.SETVALUETYPE.refresh) {
			showMessage.call(this, props, { type: MsgConst.Type.RefreshSuccess });
		}
	}
}
