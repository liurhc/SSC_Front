import { promptBox, toast } from 'nc-lightapp-front';
import { getMultiLangByID } from './multiLangUtils';
/**
 * 确定框用常量
 */
export const MsgConst = {
	Type: {
		Delete: 'Delete', // 卡片页面删除
		Cancel: 'Cancel', // 卡片页面取消
		DelSelect: 'DelSelect', // 列表页面的删除按钮
		Quit: 'Quit', // 退出转单
		Back: 'Back', // 转单下游的返回
		SaveSuccess: 'SaveSuccess', // 保存成功
		DeleteSuccess: 'DeleteSuccess', // 删除成功
		CopySuccess: 'CopySuccess', // 复制成功
		QuerySuccess: 'QuerySuccess', // 查询成功
		CommitSuccess: 'CommitSuccess', // 提交成功
		UnCommitSuccess: 'UnCommitSuccess', // 收回成功
		RefreshSuccess: 'RefreshSuccess', // 刷新成功
		EnableSuccess: 'EnableSuccess', // 启用成功
		DisableSuccess: 'DisableSuccess', // 停用成功
		SaveFailed: 'SaveFailed', // 保存失败
		DeleteFailed: 'DeleteFailed', // 删除失败
		QueryFailed: 'RefreshFailed', // 查询失败
		CommitFailed: 'CommitFailed', // 提交失败
		UnCommitFailed: 'UnCommitFailed', // 收回失败
		RefreshFailed: 'RefreshFailed', // 刷新失败
		EnableFailed: 'EnableFailed', // 启用失败
		DisableFailed: 'DisableFailed', // 停用失败
		ChoosePrint: 'ChoosePrint', // 请选择需要打印的数据
		ChooseOutput: 'ChooseOutput', // 请选择需要输出的数据
		ChooseDelete: 'ChooseDelete', // 请选择需要删除的数据
		ChooseCopy: 'ChooseCopy', // 请选择需要复制的数据
		ChooseOne: 'ChooseOne', // 请选择一行数据
		DataDeleted: 'DataDeleted', // 数据已经被删除
		QueryNoData: 'QueryNoData', // 未查询出符合条件的数据
		BodyNotNull: 'BodyNotNull', // 表体行必须要一条数据
		ChooseLinkQuery: 'ChooseLinkQuery', // 请选择需要联查的数据
		LinkQueryNoData: 'LinkQueryNoData', // 未联查出数据
		Close: 'Close', // 卡片页面关闭
		Open: 'Open' // 卡片页面关闭
	},
	Delete: { title: 'msgUtils-000000', content: 'msgUtils-000001' } /*国际化处理：删除  确定要删除吗？*/,
	Cancel: { title: 'msgUtils-000002', content: 'msgUtils-000003' } /*国际化处理：取消  确定要取消吗？*/,
	Close: { title: 'msgUtils-000033', content: 'msgUtils-000034' } /*国际化处理：关闭  确定要关闭吗？*/,
	Open: { title: 'msgUtils-000035', content: 'msgUtils-000036' } /*国际化处理：打开  确定要打开吗？*/,
	DelSelect: { title: 'msgUtils-000000', content: 'msgUtils-000004' } /*国际化处理：删除 确定要删除所选数据吗？*/,
	Quit: { title: 'msgUtils-000005', content: 'msgUtils-000006' } /*国际化处理：退出转单  当前存在未处理完成的单据，确定要退出转单吗？*/,
	Back: { title: 'msgUtils-000007', content: 'msgUtils-000008' } /*国际化处理：返回 当前存在未处理完成的单据，确定要返回吗？*/,
	SaveSuccess: { color: 'success', title: 'msgUtils-000014' } /*国际化处理：保存成功*/,
	DeleteSuccess: { color: 'success', title: 'msgUtils-000015' } /*国际化处理：删除成功*/,
	CopySuccess: { color: 'success', title: 'msgUtils-000016' } /*国际化处理：复制成功*/,
	QuerySuccess: { color: 'success', title: 'msgUtils-000017' } /*国际化处理：查询成功*/,
	CommitSuccess: { color: 'success', title: 'msgUtils-000018' } /*国际化处理：提交成功*/,
	UnCommitSuccess: { color: 'success', title: 'msgUtils-000019' } /*国际化处理：收回成功*/,
	RefreshSuccess: { color: 'success', title: 'msgUtils-000020' } /*国际化处理：刷新成功*/,
	EnableSuccess: { color: 'success', title: 'msgUtils-000010' } /*国际化处理：启用成功*/,
	DisableSuccess: { color: 'success', title: 'msgUtils-000011' } /*国际化处理：停用成功*/,
	ChoosePrint: { color: 'warning', content: 'msgUtils-000021' } /*国际化处理：请选择需要打印的数据*/,
	ChooseOutput: { color: 'warning', content: 'msgUtils-000022' } /*国际化处理：请选择需要输出的数据*/,
	ChooseDelete: { color: 'warning', content: 'msgUtils-000023' } /*国际化处理：请选择需要删除的数据*/,
	ChooseCopy: { color: 'warning', content: 'msgUtils-000024' } /*国际化处理：请选择需要复制的数据*/,
	ChooseOne: { color: 'warning', content: 'msgUtils-000025' } /*国际化处理：请选择一行数据*/,
	DataDeleted: { color: 'warning', content: 'msgUtils-000026' } /*国际化处理：数据已经被删除*/,
	QueryNoData: { color: 'warning', content: 'msgUtils-000027' } /*国际化处理：未查询出符合条件的数据*/,
	BodyNotNull: { color: 'warning', content: 'msgUtils-000029' } /*国际化处理：表体不能为空*/,
	ChooseLinkQuery: { color: 'warning', content: 'msgUtils-000031' } /*国际化处理：请选择需要联查的数据*/,
	LinkQueryNoData: { color: 'warning', content: 'msgUtils-000032' } /*国际化处理：未联查出数据*/
};

/**
 * 确定框
 * @param {*} props 
 * @param {*} config 
 * config值说明：｛
 * 		type：用来表明是哪种提示框，
 * 		beSureBtnClick：点击确认时的回调方法，
 * 		cancelBtnClick：点击取消时的回调方法（不需要的不用传）,
 * 		// 有type的话下面三个参数可以不传，下面三个参数是在没有type对应的提示框的情况下自定义使用的
 * 		title：确定框标题，
 * 		content：确定框提示内容，
 * 		color：确定框类型
 * ｝
 */
export function showConfirm(props, config = {}) {
	let { type, beSureBtnClick, cancelBtnClick, title, content, color } = config;
	if (type) {
		content = getMultiLangByID(MsgConst[type].content);
		title = getMultiLangByID(MsgConst[type].title);
		color = 'warning';
	}

	let modalConfig = {
		title,
		color,
		content,
		closeByClickBackDrop: false, //点击遮罩不关闭提示框
		//点击确定按钮事件
		beSureBtnClick: () => {
			if (typeof beSureBtnClick === 'function') {
				beSureBtnClick.call(this, props);
			}
		},
		//取消按钮事件回调
		cancelBtnClick: () => {
			if (typeof cancelBtnClick === 'function') {
				cancelBtnClick.call(this, props);
			}
		}
	};
	promptBox(modalConfig);
}

/**
 * 消息提示
 * @param {*} props 
 * @param {*} config 
 */
export function showMessage(props, config = {}) {
	const { type } = config;
	if (!type) {
		toast(config);
	} else {
		const color = MsgConst[type].color;
		const title = getMultiLangByID(MsgConst[type].title);
		const content = getMultiLangByID(MsgConst[type].content);
		toast({ color, title, content });
	}
}
