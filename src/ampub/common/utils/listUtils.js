import { MsgConst, showMessage } from './msgUtils';
import { BILLSTATUS } from '../const/StatusUtils';
import { getContext, setContext } from '../components/AMInitInfo/loginContext';
import { getMultiLangByID } from './multiLangUtils';

/**
 * 列表常量
 */
export const listConst = {
	QUERYINFOKEY: {
		dataSource: 'queryInfoCache',
		isQueryed: 'isQueryed',
		queryInfo: 'queryInfo'
	},
	SETVALUETYPE: {
		default: 'default', // 默认：不提示
		query: 'query', // 查询：提示查询成功，共多少条
		refresh: 'refresh' // 刷新：提示刷新成功
	},
	TABLEID: 'list_head',
	TABLEINNERID: 'list_inner',
	COMMONKEYS: {
		cooperate_bill: 'cooperate_bill',
		coop_pk_bill: 'coop_pk_bill',
		affirm_flag: 'affirm_flag'
	}
};

/**
 * 列表设置按钮是否可用
 * @param {*} props 
 * @param {*} config 必须
 *               formId 必须，表头区域id
 *               pkField 必须，主键字段
 *               billStatusField 非必须，单据类型字段，不传默认是bill_status
 */
export function setBillFlowBtnsEnable(props, config = {}) {
	let tableId = config.tableId || listConst.TABLEID;
	let billStatusField = config.billStatusField || 'bill_status';
	// 根据选择数据禁用按钮
	let commonBtns = [
		'Delete',
		'Copy',
		'Commit',
		'UnCommit',
		'Attachment',
		'QueryAboutBillSrc',
		'QueryAboutBusiness',
		'QueryAboutBillFlow',
		'QueryAboutVoucher',
		'FABillQueryAboutVoucher',
		'queryAboutBudget',
		'Print',
		'Output',
		'FAReceiptScan',
		'FAReceiptShow',
		'ReceiptScan',
		'ReceiptShow',
		'PrintList',
		'PrintDetail',
		'CooperateAffirm',
		'CancelAffirm'
	];
	let isQueryed = getContext(listConst.QUERYINFOKEY.isQueryed, listConst.QUERYINFOKEY.dataSource);
	let queryInfo = getContext(listConst.QUERYINFOKEY.queryInfo, listConst.QUERYINFOKEY.dataSource);
	if (isQueryed && queryInfo) {
		props.button.setButtonDisabled('Refresh', false);
	} else {
		props.button.setButtonDisabled('Refresh', true);
	}
	let checkedRows = props.table.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length == 0) {
		props.button.setButtonDisabled(commonBtns, true);
	} else {
		props.button.setButtonDisabled(commonBtns, false);

		let newDisabledBtns = [ 'Delete', 'Commit', 'UnCommit' ];
		if (checkedRows.length == 1) {
			let billStatusVal = checkedRows[0].data.values[billStatusField];
			if (billStatusVal) {
				let bill_status = billStatusVal.value;
				if (bill_status === BILLSTATUS.free_check) {
					// 自由
					newDisabledBtns.splice(newDisabledBtns.indexOf('Delete'), 1);
					newDisabledBtns.splice(newDisabledBtns.indexOf('Commit'), 1);
				} else if (bill_status === BILLSTATUS.un_check) {
					// 已提交
					newDisabledBtns.splice(newDisabledBtns.indexOf('UnCommit'), 1);
				} else if (bill_status === BILLSTATUS.check_going) {
					// 审批中
				} else if (bill_status === BILLSTATUS.check_pass) {
					// 审批通过
					newDisabledBtns.splice(newDisabledBtns.indexOf('UnCommit'), 1);
				} else if (bill_status === BILLSTATUS.check_nopass) {
					// 审批不通过
					newDisabledBtns.splice(newDisabledBtns.indexOf('Delete'), 1);
					newDisabledBtns.splice(newDisabledBtns.indexOf('Commit'), 1);
				}
			}
			props.button.setButtonDisabled(newDisabledBtns, true);
		}
	}
}

/**
 * 主子表列表创建操作列
 * @param {*} props 
 * @param {*} config 必须
 *               tableId:表格区id，默认list_head
 *               areaId:操作列按钮区id，默认list_inner
 *               width:宽度，默认200
 *               fixed:是否固定，默认right固定在右边
 *               getInnerBtns:显示按钮组，需要返回数组，默认显示所有按钮
 *               tableButtonClick:按钮点击事件
 */
export function createOprationColumn(props, config = {}) {
	let tableId = config.tableId || listConst.TABLEID;
	let areaId = config.areaId || listConst.TABLEINNERID;
	let billStatusField = config.billStatusField || 'bill_status';
	let coopField = config.coopField || listConst.COMMONKEYS.coop_pk_bill;
	let affirmField = config.affirmField || listConst.COMMONKEYS.affirm_flag;
	let width = config.width;
	let fixed = config.fixed || 'right';
	let getInnerBtns = config.getInnerBtns;
	let tableButtonClick = config.tableButtonClick;
	if (width && width.length > 0) {
		width = width.trim();
		if (!width.endsWith('px') && !width.endsWith('%')) {
			width += 'px';
		}
	} else {
		width = '200px';
	}
	// 添加表格操作列
	let oprCol = {
		label: getMultiLangByID('amcommon-000000') /* 国际化处理： 操作*/,
		visible: true,
		attrcode: 'opr',
		itemtype: 'customer',
		className: 'table-opr',
		// 锁定
		fixed,
		width,
		render: (text, record, index) => {
			let buttonAry;
			if (typeof getInnerBtns == 'function') {
				buttonAry = getInnerBtns.call(this, props, text, record, index);
			} else {
				if (record) {
					let bill_status = record[billStatusField].value;
					let coop_pk_bill = record[coopField] || false;
					let affirm_flag = record[affirmField] || false;
					if (bill_status === BILLSTATUS.free_check) {
						// 自由
						buttonAry = [ 'Commit', 'Edit', 'Delete' ];
						if (coop_pk_bill && coop_pk_bill.value) {
							buttonAry = [ 'Commit', 'Edit' ];
							if (affirm_flag && affirm_flag.value == false) {
								buttonAry = [ 'Edit' ];
							}
						}
					} else if (bill_status === BILLSTATUS.un_check) {
						// 已提交
						buttonAry = [ 'UnCommit', 'QueryAboutBillFlow' ];
					} else if (bill_status === BILLSTATUS.check_going) {
						// 审批中
						buttonAry = [ 'QueryAboutBillFlow' ];
					} else if (bill_status === BILLSTATUS.check_pass) {
						// 审批通过
						buttonAry = [ 'UnCommit', 'QueryAboutBillFlow' ];
					} else if (bill_status === BILLSTATUS.check_nopass) {
						// 审批不通过
						buttonAry = [ 'Edit', 'Delete', 'QueryAboutBillFlow' ];
						if (coop_pk_bill) {
							buttonAry = [ 'Edit', 'QueryAboutBillFlow' ];
						}
					}
				} else {
					buttonAry = [];
				}
			}
			return props.button.createOprationButton(buttonAry, {
				area: areaId,
				buttonLimit: 3,
				onButtonClick: (props, key) => {
					if (typeof tableButtonClick == 'function') {
						tableButtonClick.call(this, props, key, text, record, index, tableId);
					}
				}
			});
		}
	};
	return oprCol;
}

/**
 * 列表表格设置值
 * @param {*} props 
 * @param {*} res 必须
 * @param {*} tableId:表格区id，默认list_head
 * @param {*} scene:使用场景，默认Default，可以传Query和Refresh
 */
export function setListValue(props, res = {}, tableId = listConst.TABLEID, scene = listConst.SETVALUETYPE.default) {
	let data;
	if (res && res.hasOwnProperty('data')) {
		data = res.data;
	} else {
		data = res;
	}
	if (data && data[tableId] && data[tableId].rows && data[tableId].rows.length > 0) {
		let num = 0;
		if (data[tableId].allpks) {
			num = data[tableId].allpks.length;
		} else {
			num = data[tableId].rows.length;
		}
		props.table.setAllTableData(tableId, data[tableId]);
		if (scene == listConst.SETVALUETYPE.query) {
			showMessage.call(this, props, {
				color: 'success',
				content: getMultiLangByID('msgUtils-000028', { num }) /* 国际化处理： 查询成功，共 {num} 条*/
			});
		} else if (scene == listConst.SETVALUETYPE.refresh) {
			showMessage.call(this, props, { type: MsgConst.Type.RefreshSuccess });
		}
	} else {
		let pageInfo = props.table.getTablePageInfo(tableId);
		props.table.setAllTableData(tableId, {
			rows: [],
			allpks: [],
			pageInfo: { total: 0, totalPage: 1, pageIndex: 0, pageSize: pageInfo.pageSize }
		});
		if (scene == listConst.SETVALUETYPE.query) {
			showMessage.call(this, props, { type: MsgConst.Type.QueryNoData });
		} else if (scene == listConst.SETVALUETYPE.refresh) {
			showMessage.call(this, props, { type: MsgConst.Type.RefreshSuccess });
		}
	}
}

/**
 * 列表查询条件缓存
 * @param {*} queryInfo 
 * @param {*} isRefresh 
 * @param {*} type 
 */
export function setQueryInfoCache(queryInfo, isRefresh, type) {
	queryInfo.type = type;
	// 缓存查询条件
	if (!isRefresh) {
		setContext(listConst.QUERYINFOKEY.isQueryed, true, listConst.QUERYINFOKEY.dataSource);
		setContext(listConst.QUERYINFOKEY.queryInfo, queryInfo, listConst.QUERYINFOKEY.dataSource);
	}
}

/**
 * 列表刷新
 * @param {*} props 
 * @param {*} searchBtnClick 
 */
export function batchRefresh(props, searchBtnClick) {
	let isQueryed = getContext(listConst.QUERYINFOKEY.isQueryed, listConst.QUERYINFOKEY.dataSource);
	let queryInfo = getContext(listConst.QUERYINFOKEY.queryInfo, listConst.QUERYINFOKEY.dataSource);
	if (isQueryed && queryInfo && typeof searchBtnClick == 'function') {
		searchBtnClick.call(this, props, queryInfo.querycondition, queryInfo.type, queryInfo, true);
	}
}

export function setCooperateBtnEnable(props, tableId) {
	let checkedRows = props.table.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length < 1) {
		props.button.setButtonDisabled('CooperateAffirm', true);
		props.button.setButtonDisabled('CancelAffirm', true);
		return;
	}
	let record = checkedRows[0].data.values;
	let affirm_flag = record[listConst.COMMONKEYS.affirm_flag].value || false;
	let coop_pk_bill = record[listConst.COMMONKEYS.coop_pk_bill].value || false;
	let cooperate_bill = record[listConst.COMMONKEYS.cooperate_bill].value || false;
	if (checkedRows.length == 1 && coop_pk_bill) {
		props.button.setButtonDisabled('Delete', true);
	}
	//判断协同确认按钮状态
	if (false == affirm_flag && coop_pk_bill && true == cooperate_bill) {
		props.button.setButtonDisabled('CooperateAffirm', false);
		props.button.setButtonDisabled('Commit', true);
	} else {
		props.button.setButtonDisabled('CooperateAffirm', true);
	}
	//判断取消协同按钮状态
	if (true == affirm_flag && coop_pk_bill && true == cooperate_bill) {
		props.button.setButtonDisabled('CancelAffirm', false);
	} else {
		props.button.setButtonDisabled('CancelAffirm', true);
	}
}

/**
 * 字段编辑后清空相关联字段
 * @param {*} props 
 * @param {*} searchId 
 * @param {*} editField 
 * @param {*} clearConfig 
 */
export function searchAreaLinkClear(props, searchId, editField, clearConfig) {
	let nullData = { value: '', display: '' };
	let needClearFields = clearConfig[editField];
	if (needClearFields) {
		needClearFields.map((needClearField) => {
			props.search.setSearchValByField(searchId, needClearField, nullData);
		});
	}
}
