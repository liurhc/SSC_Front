import { UISTATE } from '../const/StatusUtils';
const { add, edit } = UISTATE;
/**
 * 浏览器关闭或者刷新时候弹出默认提示框
 *  props 
 *  moduleId :{form:Array,editTable:Array,cardTable:Array}
 *          moduleId必须为Object ,属性根据自己页面需要添加，其属性值必须为一个数组
 *  例子： {form:["formID1","formID2",...],editTable:["editTableID1","editTableID2",...],cardTable:["cardTableID1","cardTableID2",...]}
 */
export default function(props, moduleId = {}) {
	window.onbeforeunload = () => {
		for (let key in moduleId) {
			for (let i = 0; i < moduleId[key].length; i++) {
				if (
					key == 'form' &&
					props.form &&
					(props[key].getFormStatus(moduleId[key][i]) == edit ||
						props[key].getFormStatus(moduleId[key][i]) == add)
				) {
					return '';
				}
				if (props[key] && props[key].getStatus(moduleId[key][i]) == edit) {
					return '';
				}
			}
		}
	};
}
