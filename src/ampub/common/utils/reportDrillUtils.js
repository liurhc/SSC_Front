import { ajax } from 'nc-lightapp-front';
import { MsgConst, showMessage } from '../../../ampub/common/utils/msgUtils';
import { openAssetCardByPk } from '../../../ampub/common/components/QueryAbout/faQueryAboutUtils';

const url = {
	reportDrillToBillUrl: '/nccloud/ampub/report/drillToBill.do',
	reportDrillToFaCardUrl: '/nccloud/ampub/report/reportDrillToFaCard.do',
	reportLinkQueryDocUrl: '/nccloud/ampub/report/reportlinkquerydoc.do'
};

/**
 * 报表联查单据
 * @param {*} props 
 * @param {*} data 
 * @param {*} pkCode 主键字段，如：“pk_assign”
 * @param {*} billTypeCode 单据类型字段,如："bill_type
 * @param {*} callback 
 */
function reportDrillToBill(props, data, pkCode, billTypeCode, transSaveObject, voClass, callback) {
	let defaultCallBack = (res) => {
		if (res.success) {
			if (res && res.data) {
				//联查资产模块其它单据
				props.openTo(res.data.url, {
					...res.data,
					status: 'browse'
				});
			} else {
				showMessage.call(this, props, { type: MsgConst.Type.LinkQueryNoData }); /*国际化处理：未联查出数据*/
			}
		}
	};

	data['transSaveObject'] = transSaveObject;
	data['bill_type'] = billTypeCode;
	data['primary_key'] = pkCode;
	data['voClass'] = voClass;
	ajax({
		url: url.reportDrillToBillUrl,
		data,
		success: callback || defaultCallBack
	});
}

/**
 * 报表联查固定资产卡片
 * @param {*} props 
 * @param {*} data 
 * @param {*} pkCode 主键字段，如：“pk_card”
 * @param {*} billTypeCode 单据类型字段,如："bill_type
 * @param {*} callback 
 */
function reportDrillToFaCard(props, data, pkCode, billTypeCode, transSaveObject, voClass, callback) {
	let defaultCallBack = (res) => {
		if (res.success) {
			//联查固定资产卡片
			if (res && res.data) {
				openAssetCardByPk.call(this, props, res.data.id);
			} else {
				showMessage.call(this, props, { type: MsgConst.Type.LinkQueryNoData }); /*国际化处理：未联查出数据*/
			}
		}
	};

	data['transSaveObject'] = transSaveObject;
	data['bill_type'] = billTypeCode;
	data['primary_key'] = pkCode;
	data['voClass'] = voClass;
	ajax({
		url: url.reportDrillToFaCardUrl,
		data,
		success: callback || defaultCallBack
	});
}

function reportDrillToDoc(props, data, metaPath, primary_key, transSaveObject, callback) {
	let defaultCallBack = (res) => {
		let { success, data } = res;
		if (success) {
			if (data && data.url && data.appcode && data.pagecode) {
				// 打开应用
				props.openTo(data.url, {
					status: 'browse',
					appcode: data.appcode,
					pagecode: data.pagecode,
					id: data.pk_data
				});
			} else {
				showMessage.call(this, props, { type: MsgConst.Type.LinkQueryNoData }); /*国际化处理：未联查出数据*/
			}
		}
	};
	data['transSaveObject'] = transSaveObject;
	data['metaPath'] = metaPath;
	data['primary_key'] = primary_key;
	ajax({
		url: url.reportLinkQueryDocUrl,
		data: data,
		success: callback || defaultCallBack
	});
}

/**
 * 报表联查单据、档案
 * @param {*} props 
 * @param {*} props 
 * @param {*} data 
 * @param {*} transSaveObject 
 * @param {*} linkQueryConfig 
 */
function reportLinkQuery(props, data, transSaveObject, linkQueryConfig, callback) {
	// 获取字段联查配置
	let { transi_type, metaPath, primary_key, voClass } = linkQueryConfig[data.fldName];
	// 联查
	if (transi_type) {
		// 存在交易类型，则单据联查
		reportDrillToBill.call(this, props, data, primary_key, transi_type, transSaveObject, voClass, callback);
	} else if (metaPath) {
		// 元数据路径，则联查档案
		reportDrillToDoc.call(this, props, data, metaPath, primary_key, transSaveObject, callback);
	} else {
		// 其他类型 待补充
	}
}

export { reportDrillToBill, reportDrillToFaCard, reportDrillToDoc, reportLinkQuery };
