import { viewModel, getMultiLang } from 'nc-lightapp-front';

/**
 * 
 * @param {*} moduleIds ：Object  传入需要合并的多语文件名称
 * @param {*} callback ：Function  回调函数
 * 
 */
export function initMultiLangByModule(moduleIds = {}, callback) {
	// let keysArr = Object.keys(moduleIds);
	// recursion(moduleIds, keysArr, 0, callback);
	let deal = (json, status) => {
		if (status) {
			let jsonObj = viewModel.getData('AM_MultiLang') || {};
			viewModel.setData('AM_MultiLang', { ...jsonObj, ...json });
		}
		if (typeof callback == 'function') {
			callback();
		}
	};
	getMultiLang({
		moduleId: moduleIds,
		// currentLocale: 'simpchn',
		// domainName: arr[index],
		callback: deal
	});
}

/**
 * 回调函数
 * @param {*} moduleIds：Object 传入需要引用的多语文件对象
 * @param {*} arr： Array 多语对象的属性生成的数组
 * @param {*} index：Number 递归变量
 * @param {*} callback：Function 回调函数
 */
function recursion(moduleIds, arr, index, callback) {
	let deal = (json, status) => {
		let jsonObj = viewModel.getData('AM_MultiLang') || {};
		viewModel.setData('AM_MultiLang', { ...jsonObj, ...json });
		if (index == arr.length - 1) {
			if (typeof callback == 'function') {
				callback();
			}
		} else {
			recursion(moduleIds, arr, index + 1, callback);
		}
	};
	getMultiLang({
		moduleId: moduleIds[arr[index]],
		// currentLocale: 'simpchn',
		domainName: arr[index],
		callback: deal
	});
}

//根据应用编码获取当前所有的多语文件
export function getAllMultiLangByModule() {
	let jsonObj = viewModel.getData('AM_MultiLang') || {};
	return jsonObj;
}

/**
 * 根据应用编码和ID获取具体的多语字段
 * @param {*} resId 多语编码字段
 * @param {*} varValues 变量对象
 */
export function getMultiLangByID(resId = '', varValues) {
	let json = viewModel.getData('AM_MultiLang') || {};
	let jsonStr = json[resId] || resId;
	if (varValues && jsonStr) {
		for (let key in varValues) {
			jsonStr = jsonStr.replace('{' + key + '}', varValues[key]);
		}
	}
	return jsonStr;
}
