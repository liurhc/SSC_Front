import { ajax } from 'nc-lightapp-front';
import { MsgConst, showMessage } from './msgUtils';

const url = {
	queryLinkDocDataUrl: '/nccloud/platform/pub/gethyperlink.do',
	queryLinkBillDataUrl: '/nccloud/ampub/common/amLinkQuery.do'
};

/**
 * 档案联查/超链接
 * 调用平台档案模板超链接接口
 * @param {*} props 
 * @param {*} metaPath 
 * @param {*} templetConfig 
 * @param {*} pk_data 
 * @param {*} callback 
 */
function baseDocLink(props, metaPath, templetConfig, pk_data, callback) {
	let data = undefined;
	if (metaPath) {
		data = {
			dataId: pk_data,
			metaPath: metaPath
		};
	} else {
		data = {
			templetId: templetConfig.templetId,
			areaCode: templetConfig.areaCode,
			code: templetConfig.fieldCode,
			dataId: pk_data
		};
	}
	// 默认回调 打开节点
	let defaultCallBack = (res) => {
		let { success, data } = res;
		if (success) {
			if (data && data.pageurl && data.parentcode && data.pagecode) {
				// 打开应用
				props.openTo(data.pageurl, {
					status: 'browse',
					appcode: data.parentcode,
					pagecode: data.pagecode,
					id: pk_data
				});
			} else {
				showMessage.call(this, props, { type: MsgConst.Type.LinkQueryNoData }); /*国际化处理：未联查出数据*/
			}
		}
	};
	// 查询链接页面信息
	ajax({
		url: url.queryLinkDocDataUrl,
		data: data,
		success: callback || defaultCallBack
	});
}

/**
 * 档案联查/超链接
 * 调用平台档案模板超链接接口
 * @param {*} props 
 * @param {*} templetId 模板id
 * @param {*} areaCode 区域编码
 * @param {*} fieldCode 字段编码
 * @param {*} pk_data 档案主键
 * @param {*} callback 回调函数
 */
function docLink(props, templetId, areaCode, fieldCode, pk_data, callback) {
	// 参数校验
	if (!props || !templetId || !areaCode || !fieldCode || !pk_data) {
		return;
	}
	// 使用模板定位字段联查
	baseDocLink.call(
		this,
		props,
		undefined,
		{ templetId: templetId, areaCode: areaCode, fieldCode: fieldCode },
		pk_data,
		callback
	);
}

/**
 * 通过元数据路径（命名空间.元数据名，例：位置-ampub.location）档案联查/超链接
 * 调用平台档案模板超链接接口
 * @param {*} props 
 * @param {*} metaPath 元数据路径
 * @param {*} pk_data 主键
 * @param {*} callback 回调函数
 */
function docLinkByMetaPath(props, metaPath, pk_data, callback) {
	// 参数校验
	if (!props || !metaPath || !pk_data) {
		return;
	}
	// 使用元数据路径定位联查
	baseDocLink.call(this, props, metaPath, undefined, pk_data, callback);
}

/**
 * 单据联查
 * @param {*} props 
 * @param {*} transi_type 单据交易类型/单据类型
 * @param {*} pk_bill 单据主键
 * @param {*} callback 回调函数
 */
function billLink(props, transi_type, pk_bill, callback) {
	// 参数校验
	if (!props || !transi_type || !pk_bill) {
		return;
	}
	// 默认回调 打开节点
	let defaultCallBack = (res) => {
		let { success, data } = res;
		if (success) {
			if (data && data.url && data.appcode && data.pagecode) {
				// 打开应用
				props.openTo(data.url, {
					status: 'browse',
					appcode: data.appcode,
					pagecode: data.pagecode,
					id: pk_bill
				});
			} else {
				showMessage.call(this, props, { type: MsgConst.Type.LinkQueryNoData }); /*国际化处理：未联查出数据*/
			}
		}
	};
	ajax({
		url: url.queryLinkBillDataUrl,
		data: {
			transtype: transi_type,
			id: pk_bill
		},
		success: callback || defaultCallBack
	});
}

export { docLink, docLinkByMetaPath, billLink };
