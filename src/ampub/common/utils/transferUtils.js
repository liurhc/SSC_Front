import { ajax, toast, base, cardCache } from 'nc-lightapp-front';
import { TransferConst } from '../const/CommonKeys';
import { getContext, setContext, loginContextKeys } from '../components/AMInitInfo/loginContext';
import { UISTATE } from '../const/StatusUtils';
import { openEquipCardByPk } from '../components/QueryAbout/queryAboutUtils';
import { getMultiLangByID } from './multiLangUtils';
import { MsgConst, showMessage } from './msgUtils';
import { commonRefCondition } from '../components/ReferInit/refInit';

const { NCBackBtn } = base;
let { getDefData, setDefData } = cardCache;

/**
 * 转单查询页面的查询方法
 * @param {*} url 
 * @param {*} props 
 * @param {*} searchCode 
 * @param {*} billtype 
 * @param {*} pagecode 
 * @param {*} headCode 
 * @param {*} bodyCode 
 * @param {*} head_pkfield 
 * @param {*} body_pkfield 
 */
export function doTansferSearch(
	url,
	props,
	searchCode,
	billtype,
	pagecode,
	headCode,
	bodyCode,
	head_pkfield,
	body_pkfield,
	appCode
) {
	let data = null;
	if (this && !this.isTransfer) {
		// 清除查询区数据
		props.search.clearSearchArea(searchCode);
		// 获取查询条件（false不走校验，例如必输校验）
		data = props.search.getQueryInfo(searchCode, false);
		let conditions = [];
		// 构造设置默认查询条件（查询区没有显示的字段无法设值，所以要自己构造conditions）
		for (let filed in props.searchValue) {
			let condition = {
				datatype: '204',
				isIncludeSub: false,
				oprtype: '=',
				refurl: '',
				value: { firstvalue: '', secondvalue: '' }
			};
			condition['field'] = filed;
			condition['display'] = props.searchValue[filed].display;
			condition['value'].firstvalue = props.searchValue[filed].value;
			conditions.push(condition);
		}
		data.querycondition.conditions = conditions;
	} else {
		data = props.search.getQueryInfo(searchCode, true);
	}

	let transtype = props.getUrlParam(TransferConst.src_tradetype);
	// 参照增行的时候取值
	if (this && !this.isTransfer) {
		transtype = props.param[TransferConst.src_tradetype];
	}
	// 从缓存中获取业务类型（资产领域内设置的缓存）
	let busitypes = getContext(billtype + transtype, TransferConst.dataSource);
	// 如果业务类型没有取到，说明是财务那边的转单查询，所以再取下财务设置的缓存业务类型
	if (!busitypes) {
		busitypes = getDefData(billtype + transtype, TransferConst.dataSource);
	}
	let appcodeCache = getContext(TransferConst.query_appcode, TransferConst.dataSource);
	if (appcodeCache) {
		appCode = appcodeCache;
	}
	let destBillType = props.getUrlParam(TransferConst.dest_billtype);
	let destTransiType = props.getUrlParam(TransferConst.dest_tradetype);
	// 参照增行的时候取值
	if (props.param) {
		destBillType = props.param[TransferConst.dest_billtype];
		destTransiType = props.param[TransferConst.dest_tradetype];
	}

	if (data) {
		data.appcode = appCode;
		data.pagecode = pagecode;
		data.billtype = billtype;
		data.transtype = transtype;
		data.userdefObj = {
			busiTypes: busitypes,
			headCode: headCode,
			bodyCode: bodyCode,
			destBillType,
			destTransiType
		};
	}
	ajax({
		url,
		data,
		success: (res) => {
			if (res.data) {
				let num = res.data.length;
				props.transferTable.setTransferTableValue(headCode, bodyCode, res.data, head_pkfield, body_pkfield);
				toast({
					color: 'success',
					content: getMultiLangByID('msgUtils-000028', { num: num })
				}); /*国际化处理：`查询成功，共 ${num} 条。`*/
			} else {
				props.transferTable.setTransferTableValue(headCode, bodyCode, [], head_pkfield, body_pkfield);
				showMessage.call(this, props, { type: MsgConst.Type.QueryNoData });
			} /*国际化处理：'未查询出符合条件的数据！'*/
		}
	});
}

/**
 * 通过上游的单据类型或交易类型获取应用编码，初始化模板用
 * @param {*} props 
 * @param {*} callBack 
 */
export function queryAppCodeByTransType(props, bill_type, callBack) {
	let transi_type = props.getUrlParam(TransferConst.src_tradetype);
	// 参照增行的时候取值
	if (props.param) {
		transi_type = props.param[TransferConst.src_tradetype];
	}
	if (!transi_type) {
		transi_type = bill_type;
	}
	ajax({
		url: '/nccloud/ampub/common/amDefaultAppQuery.do',
		data: {
			transtype: transi_type
		},
		success: (res) => {
			if (res.data) {
				setContext(TransferConst.query_appcode, res.data.appcode, TransferConst.dataSource);
				callBack.call(this, props);
			}
		}
	});
}

/**
 * 拉单按钮点击处理，跳转到上游查询页面
 * @param {*} transferRouter 上游查询页面路由
 * @param {*} src_billtype 上游单据类型
 * @param {*} src_tradetype 上游交易类型
 * @param {*} dest_billtype 下游单据类型
 * @param {*} dest_tradetype 下游交易类型
 * @param {*} dest_pagecode 下游转单页面的pagecode
 */
export function pushToTransferPage(
	transferRouter,
	src_billtype,
	src_tradetype,
	dest_billtype,
	dest_tradetype,
	dest_pagecode
) {
	this.props.pushTo(transferRouter, {
		src_billtype,
		src_tradetype,
		dest_billtype,
		dest_tradetype,
		dest_pagecode,
		from_type: 'AM' // 用于区分是资产领域跳转过来还是财务领域跳转过来
	});
}

/**
 * 转单查询页面点击生成单据跳转下游转单页面方法处理
 * @param {*} cardRouter 转单卡片页面路由
 * @param {*} srcBilltype 上游单据类型
 * @param {*} dataSource 转单用dataSource
 */
export function pushToTransferCard(cardRouter, srcBilltype, dataSource, src_pagecode) {
	// 获取下游转单卡片页面的pagecode
	let pagecode = this.props.getUrlParam(TransferConst.dest_tradetype);
	if (!pagecode) {
		pagecode = this.props.getUrlParam(TransferConst.dest_pagecode);
	}

	// 根据领域来源，获取相应的status
	let status = 'add';
	let from_type = this.props.getUrlParam('from_type');
	if (from_type) {
		status = UISTATE.transferAdd;
	}
	// 缓存上游的appcode和pagecode（财务返回上游的时候用）
	let src_appcode = getContext(TransferConst.query_appcode, TransferConst.dataSource);
	setDefData(TransferConst.src_appcode, dataSource, src_appcode);
	setDefData(TransferConst.src_pagecode, dataSource, src_pagecode);
	this.props.pushTo(cardRouter, {
		status,
		type: TransferConst.type,
		srcBilltype,
		dataSource,
		pagecode
	});
}

/**
 * 给主子展示页面的单号添加超链接
 * @param {*} props 
 * @param {*} meta 
 * @param {*} headCode 
 * @param {*} bodyCode 
 * @param {*} sourceUrl 
 * @param {*} sourcePageCode 
 * @param {*} sourceAppCode 
 * @param {*} headPkField 
 */
export function addLinkToTemplate(
	props,
	meta,
	headCode,
	bodyCode,
	sourceUrl,
	sourcePageCode,
	sourceAppCode,
	headPkField
) {
	meta[headCode].items.map((item, key) => {
		item.width = 150;
		if (item.attrcode == 'bill_code') {
			item.render = (text, record, index) => {
				return (
					<span
						className="code-detail-link"
						onClick={() => {
							props.openTo(sourceUrl, {
								pagecode: sourcePageCode,
								status: UISTATE.browse,
								appcode: sourceAppCode,
								id: record[headPkField].value
							});
						}}
					>
						{record.bill_code && record.bill_code.value}
					</span>
				);
			};
		}
	});
	meta[bodyCode].items.map((item, key) => {
		item.width = 170;
		if (item.attrcode == 'pk_equip') {
			item.render = (text, record, index) => {
				return (
					<span
						className="code-detail-link"
						onClick={() => {
							openEquipCardByPk(props, record.pk_equip.value);
						}}
					>
						{record.pk_equip && record.pk_equip.display}
					</span>
				);
			};
		}
	});
	return meta;
}

/**
 * 给主子拉平展示页面的单号添加超链接
 * @param {*} props 
 * @param {*} meta 
 * @param {*} mainCode 
 * @param {*} sourceUrl 
 * @param {*} sourcePageCode 
 * @param {*} sourceAppCode 
 * @param {*} headPkField 
 */
export function addLinkToTemplateFull(props, meta, mainCode, sourceUrl, sourcePageCode, sourceAppCode, headPkField) {
	meta[mainCode].items.map((item, key) => {
		if (item.attrcode == 'bill_code') {
			item.width = 150;
			item.render = (text, record, index) => {
				return (
					<span
						className="code-detail-link"
						onClick={() => {
							props.openTo(sourceUrl, {
								pagecode: sourcePageCode,
								status: UISTATE.browse,
								appcode: sourceAppCode,
								id: record[headPkField].value
							});
						}}
					>
						{record.bill_code && record.bill_code.value}
					</span>
				);
			};
		}
		if (item.attrcode == 'pk_equip') {
			item.width = 170;
			item.render = (text, record, index) => {
				return (
					<span
						className="code-detail-link"
						onClick={() => {
							openEquipCardByPk(props, record.pk_equip.value);
						}}
					>
						{record.pk_equip && record.pk_equip.display}
					</span>
				);
			};
		}
	});
	return meta;
}

/**
 * 给转单查询页面添加返回按钮（返回到下游列表页面，默认路由为"/list"）
 * @param {*} listRouter 
 */
export function addReturnBtn(listRouter = '/list') {
	return (
		<span>
			<span>
				<NCBackBtn
					onClick={() => {
						let dest_billtype = this.props.getUrlParam(TransferConst.dest_billtype);
						// 资产获取传过来的pagecode
						let pagecode = this.props.getUrlParam(TransferConst.dest_pagecode);
						// 财务根据单据类型取获取pagecode
						if (dest_billtype) {
							if (dest_billtype == 'F0') {
								pagecode = '20060RBM_LIST'; //应收单列表
							} else if (dest_billtype == 'F1') {
								pagecode = '20080PBM_LIST'; //应付单列表
							} else if (dest_billtype == 'F2') {
								pagecode = '20060GBM_LIST'; //收款单列表
							} else if (dest_billtype == 'F3') {
								pagecode = '20080EBM_LIST'; //付款单列表
							}
						}
						this.props.pushTo(listRouter, {
							pagecode: pagecode
						});
					}}
				/>
				<span className="bill-info-title">{getMultiLangByID('TansferUtils-000000')}</span>
				{/*国际化处理：选择单据*/}
			</span>
		</span>
	);
}

/**
 * 转单模板处理
 * @param {*} props 
 * @param {*} meta 
 * @param {*} areaId 
 */
export function commonMetaHandle(props, meta, areaId) {
	if (meta) {
		meta[areaId].items.map((item) => {
			if (
				item.attrcode == 'bodyvos.pk_equip.pk_usedept' ||
				item.attrcode == 'bodyvos.pk_equip.pk_mandept' ||
				item.attrcode == 'pk_usedept'
			) {
				// 添加【执行时包含下级】默认勾选
				item.isRunWithChildren = true;
				item.defaultRunWithChildren = true;
			}
			// 添加【执行时包含下级】默认勾选
			commonRefCondition.call(this, props, item);
		});
	}
}

/**
 * 跳转到拉单页面
 * (可以是点击拉单按钮跳转，也可以是下游点击返回按钮跳转。
 * 返回跳转不需要传id)
 * @param {*} props 
 * @param {*} id 形式【单据类型+交易类型】，例如'4A27+4A27-01'
 * config说明：transferRouter：拉单页面的路由路径
 * 			   bill_type：下游的单据类型
 * 			   transferDataSource：拉单页面的dataSource
 * 			   transferCard: 下游转单卡片页面的pagecode
 */
export function pushToTransferPageNew(props, id = '', config = {}) {
	const { transferRouter, bill_type, transferDataSource, transferCard } = config;
	if (!id) {
		id =
			getContext(TransferConst.src_billtype_cache, TransferConst.dataSource) +
			'+' +
			getContext(TransferConst.src_transtype_cache, TransferConst.dataSource);
	} else {
		//清掉上游缓存(退出转单和从上游返回下游然后再到上游的时候)
		props.transferTable.deleteCache(transferDataSource);
	}
	// //默认是动态加载的拉单按钮，拉单按钮的code为“单据类型+交易类型”
	let srcinfo = id.split('+');
	pushToTransferPage.call(
		this,
		transferRouter, //上游查询页面的路由
		srcinfo[0], // 上游的bill_type
		srcinfo[1], // 上游的transtype
		bill_type, // 下游的bill_type
		getContext(loginContextKeys.transtype), // 下游的transtype
		transferCard // 下游转单页面的pagecode
	);
}

let isAddExist = false;
//根据
export function conactAddButton(props, billtype, transtype, area, callback) {
	let buttons = props.button.getButtons();
	let buttonArea;
	if (area) {
		buttonArea = area;
	}
	//补充拉单按钮的信息
	ajax({
		url: '/nccloud/ampub/common/querySrcActionInfo.do',
		data: {
			billtype,
			transtype
		},
		success: (res) => {
			if (res.data) {
				isAddExist = false;
				//和需求UE确认拉单按钮放在新增按钮前，暂不放在新增按钮后
				//getOldAddButton(buttons,res.data);
				if (!isAddExist) {
					buttons.unshift(getNewAddButtons(res.data, buttonArea));
				}
				props.button.setButtons(buttons, callback);
			}
		}
	});
}

function getOldAddButton(buttons, srcinfo) {
	for (let i = 0, len = buttons.length; i < len; i++) {
		if (buttons[i].key === 'Add') {
			let addButton = buttons[i];
			isAddExist = true;
			let addGroup = getNewAddButtons(srcinfo, addButton.area);
			addGroup.children.unshift(addButton);
			buttons[i] = addGroup;
			break;
		} else if (buttons[i].children != null && buttons[i].children.length > 0) {
			getOldAddButton(buttons[i].children, srcinfo);
		}
	}
}

//获取新增按钮组
function getNewAddButtons(srcinfo, area) {
	srcinfo = JSON.parse(srcinfo);
	let addGroup = {
		key: 'AddTransferGroup', //与CommonKeys中的ButtonConst保持一致
		type: 'buttongroup',
		area,
		title: null
	};
	let newAddButtons = new Array(); //前端拉单按钮json数组
	for (let n = 0; n < srcinfo.length; n++) {
		let temp = srcinfo[n];
		if (temp.actionCode != null) {
			let billType = temp.billType;
			let transType = temp.transType;
			setContext(billType + transType, temp.busitypes, TransferConst.dataSource);
			setContext(TransferConst.src_billtype_cache, billType, TransferConst.dataSource);
			setContext(TransferConst.src_transtype_cache, transType, TransferConst.dataSource);
			newAddButtons[n] = {
				key: temp.actionCode,
				title: temp.actionName,
				area,
				parentCode: 'AddTransferGroup',
				btncolor: 'button_secondary',
				type: 'general_btn'
			};
		}
	}
	addGroup.children = newAddButtons;
	return addGroup;
}

/**
 * 根据上游来源单据信息获取下游单据信息
 * @param {*} props 
 * @param {*} url  后台请求路径
 * @param {*} tableId 子表的区域ID
 * @param {*} pagecode 下游转单页面的pagecode
 * @param {*} bill_type 下游的单据类型
 * @param {*} transitype 下游交易类型
 * @param {*} srcPkBillB 下游子表的来源单据子表主键
 * @param {*} srcbilltype 上游单据类型
 * @param {*} srctranstype 上游的交易类型
 * @param {*} showRefAddline 下游控制模态框显示性的state
 * @param {*} callBack 回调函数，用来处理返回的数据
 */
export function getData4ReferAddLine(
	props,
	url,
	tableId,
	pagecode,
	nextbilltype,
	nexttranstype,
	srcPkBillB,
	srcbilltype,
	srctranstype,
	showRefAddline,
	callBack
) {
	let pks = this.props.transferTable.getTransferTableSelectedId();
	ajax({
		url: url,
		data: {
			pks: pks,
			pageid: pagecode,
			srcbilltype,
			srctranstype,
			nextbilltype,
			nexttranstype
		},
		success: (res) => {
			if (res.data) {
				if (typeof callBack == 'function') {
					callBack.call(this, this.props, res.data);
				}
				let lastIndex = this.props.cardTable.getNumberOfRows(tableId);
				// 过滤掉已经存在的表体行
				let addRows = referAddlineFilterBodys.call(this, res.data, tableId, srcPkBillB);
				// 粘贴到末行
				this.props.cardTable.insertRowsAfterIndex(tableId, addRows, lastIndex);
			}
			// 关闭模态框
			this.setState({ [showRefAddline]: false });
		}
	});
}

/**
 * 参照增行过滤掉已经存在的表体行
 * @param {*} data 
 * @param {*} tableId 
 * @param {*} field 
 */
function referAddlineFilterBodys(data, tableId, field) {
	// 获取表体的来源单据子表oid pk_contract_b_oid
	let visibleRows = this.props.cardTable.getVisibleRows(tableId);
	let src_b_oids = visibleRows.map((row) => {
		return row.values[field].value;
	});
	let addRows = [];
	if (data[0] && data[0].body && data[0].body[tableId] && data[0].body[tableId].rows) {
		data[0].body[tableId].rows.map((row) => {
			if (!src_b_oids.includes(row.values[field].value)) {
				addRows.push(row);
			}
		});
	}
	return addRows;
}
