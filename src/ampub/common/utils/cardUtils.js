import { ajax } from 'nc-lightapp-front';
// import { createTitleArea, setTitleData } from '../components/AMTitleArea';
import { BILLSTATUS, UISTATE, VOSTATUS } from '../const/StatusUtils';
import { linkQueryConst, TransferConst } from '../const/CommonKeys';
import { getMultiLangByID } from './multiLangUtils';
import { setContext, getContext } from '../components/AMInitInfo/loginContext';

/**
 * 卡片常量
 */
export const cardConst = {
	SETVALUETYPE: {
		default: 'default', // 默认：不提示
		query: 'query', // 查询：提示查询成功，共多少条
		refresh: 'refresh' // 刷新：提示刷新成功
	},
	FORMID: 'card_head',
	TABLEID: 'bodyvos',
	TABLEINNERID: 'card_body_inner',
	COMMONKEYS: {
		affirm_flag: 'affirm_flag',
		coop_pk_bill: 'coop_pk_bill',
		cooperate_bill: 'cooperate_bill'
	},
	COPYEDLINES: 'copyedLines', // 用来保存复制行的缓存主键
	PASTELINETYPE: {
		// 粘贴行的类型
		PasteLine: 'PasteLine', // 粘贴行
		PasteLineToTail: 'PasteLineToTail' // 粘贴行到末行
	}
};

/**
 * 浏览态设置审批流按钮显示隐藏
 * @param {*} props 
 * @param {*} formId 必须，表头区域id
 * @param {*} pkField 必须，主键字段
 * @param {*} billStatusField 非必须，单据类型字段，不传默认是bill_status
 */
export function setBillFlowBtnsVisible(props, formId = cardConst.FORMID, pkField, billStatusField = 'bill_status') {
	let status = props.form.getFormStatus(formId) || UISTATE.browse;
	let isBrowse = [ UISTATE.browse, UISTATE.blank ].includes(status);
	if (isBrowse) {
		// 根据单据状态隐藏按钮
		let commonBtns = [
			'Edit',
			'Delete',
			'Copy',
			'Commit',
			'UnCommit',
			'Attachment',
			'QueryAboutBillSrc',
			'QueryAboutBusiness',
			'QueryAboutBillFlow',
			'QueryAboutVoucher',
			'FABillQueryAboutVoucher',
			'Print',
			'Output',
			'ReceiptScan',
			'ReceiptShow',
			'FAReceiptScan',
			'FAReceiptShow',
			'queryAboutBudget',
			'Refresh'
		];
		let pkVal = props.form.getFormItemsValue(formId, pkField);
		if (!pkVal || !pkVal.value) {
			props.button.setButtonVisible(commonBtns, false);
		} else {
			let btnObj = {};
			commonBtns.map((item) => {
				btnObj[item] = true;
			});

			let newHiddenBtns = [
				'Edit',
				'Delete',
				'Commit',
				'UnCommit',
				'QueryAboutVoucher',
				'FABillQueryAboutVoucher'
			];
			newHiddenBtns.map((item) => {
				btnObj[item] = false;
			});
			let billStatusVal = props.form.getFormItemsValue(formId, billStatusField);
			if (billStatusVal) {
				let bill_status = billStatusVal.value;
				if (bill_status === BILLSTATUS.free_check) {
					// 自由
					btnObj['Edit'] = true;
					btnObj['Delete'] = true;
					btnObj['Commit'] = true;
				} else if (bill_status === BILLSTATUS.un_check) {
					// 已提交
					btnObj['UnCommit'] = true;
				} else if (bill_status === BILLSTATUS.check_going) {
					// 审批中
				} else if (bill_status === BILLSTATUS.check_pass) {
					// 审批通过
					btnObj['UnCommit'] = true;
					btnObj['QueryAboutVoucher'] = true;
					btnObj['FABillQueryAboutVoucher'] = true;
				} else if (bill_status === BILLSTATUS.check_nopass) {
					// 审批不通过
					btnObj['Edit'] = true;
					btnObj['Delete'] = true;
				}
			}
			props.button.setButtonVisible(btnObj);
		}
	}
}

/**
 * 创建卡片标题区
 * @param {*} props 
 * @param {*} config 必须:
 * title
 * formId
 * backBtnClick:backToList
 * showBackBtn:true显示/false不显示/不传浏览态显示
 * showBillCode:true显示/false不显示/不传浏览态显示
 */
export function createCardTitleArea(props, config = {}) {
	// config.type = 'card';
	// return createTitleArea.call(this, props, config);
	let { backBtnClick } = config;
	return (
		<span>
			{props.BillHeadInfo.createBillHeadInfo({
				title: config.title,
				backBtnClick: () => {
					//返回按钮的点击事件
					if (typeof backBtnClick == 'function') {
						backBtnClick.call(this, props);
					}
				}
			})}
		</span>
	);
}

/**
 * 设置卡片标题区数据
 * @param {*} props 
 * @param {*} data bill_code，单据号，可选；status，UI状态，可选；
 * showBackBtn，是否强制显示返回按钮，与一般显示规则不同时可选；showBillCode，是否强制显示单据号，与一般显示规则不同时可选
 */
export function setHeadAreaData(props, data = {}) {
	// setTitleData.call(this, data);
	// let flag = false;
	let newState = {};
	let { showBackBtn, showBillCode } = data;
	// 页面状态
	if (data.hasOwnProperty('status')) {
		let isBrowse = [ UISTATE.browse, UISTATE.blank, UISTATE.transferBorwse ].includes(data.status);
		newState.showBillCode = isBrowse;

		// 审批联查不显示返回按钮和翻页按钮
		let scene = props.getUrlParam(linkQueryConst.SCENE);
		let isApprovesce = scene == linkQueryConst.SCENETYPE.approvesce;
		let flag = isBrowse && !isApprovesce;
		newState.showBackBtn = flag;

		// 设置翻页按钮显示
		props.cardPagination.setCardPaginationVisible([], flag);
	}
	// 单据号
	if (data.hasOwnProperty('bill_code')) {
		newState.billCode = data.bill_code;
	}
	// 转单页面默认显示返回按钮
	let srctype = props.getUrlParam(TransferConst.srctype);
	if (srctype == TransferConst.type) {
		newState.showBackBtn = true;
	}
	// 强制控制返回按钮显隐性
	if (showBackBtn === true || showBackBtn === false) {
		newState.showBackBtn = showBackBtn;
	}
	// 强制控制单据号显隐性
	if (showBillCode === true || showBillCode === false) {
		newState.showBillCode = showBillCode;
	}
	// 设置单据号和返回按钮显示
	if (Object.keys(newState).length > 0) {
		props.BillHeadInfo.setBillHeadInfoVisible(newState);
	}
}

/**
 * 创建卡片翻页区
 * @param {*} props 
 * @param {*} config 必须:dataSource,pageInfoClick
 */
export function createCardPaginationArea(props, config = {}) {
	const { pageInfoClick, dataSource } = config;
	return (
		<div className="header-cardPagination-area">
			{props.cardPagination.createCardPagination({
				handlePageInfoChange: typeof pageInfoClick == 'function' ? pageInfoClick.bind(this) : undefined,
				dataSource
			})}
		</div>
	);
}

/**
 * 主子表卡片创建操作列
 * @param {*} props 
 * @param {*} config 必须
 *               formId:表单区id，默认card_head
 *               tableId:表格区id，默认bodyvos
 *               areaId:操作列按钮区id，默认card_body_inner
 *               width:宽度，默认110
 *               fixed:是否固定，默认right固定在右边
 *               getInnerBtns:显示按钮组，需要返回数组，默认显示所有按钮
 *               tableButtonClick:按钮点击事件
 */
export function createOprationColumn(props, config = {}) {
	let formId = config.formId || cardConst.FORMID;
	let tableId = config.tableId || cardConst.TABLEID;
	let areaId = config.areaId || cardConst.TABLEINNERID;
	let width = config.width;
	let fixed = config.fixed || 'right';
	let getInnerBtns = config.getInnerBtns;
	let tableButtonClick = config.tableButtonClick;
	if (width && width.length > 0) {
		width = width.trim();
		if (!width.endsWith('px') && !width.endsWith('%')) {
			width += 'px';
		}
	} else {
		width = '130px';
	}
	// 添加表格操作列
	let oprCol = {
		label: getMultiLangByID('amcommon-000000') /* 国际化处理： 操作*/,
		visible: true,
		attrcode: 'opr',
		itemtype: 'customer',
		className: 'table-opr',
		// 锁定
		fixed,
		width,
		render: (text, record, index) => {
			let buttonAry;
			if (typeof getInnerBtns == 'function') {
				buttonAry = getInnerBtns.call(this, props, text, record, index);
			} else {
				let status = props.cardTable.getStatus(tableId) || UISTATE.browse;
				if (status == UISTATE.edit) {
					buttonAry = [ 'OpenCard', 'CopyLine', 'DelLine' ];
				} else {
					buttonAry = [ 'OpenCard' ];
				}
			}
			return props.button.createOprationButton(buttonAry, {
				area: areaId,
				buttonLimit: 3,
				onButtonClick: (props, key) => {
					if (typeof tableButtonClick == 'function') {
						tableButtonClick.call(this, props, key, text, record, index, tableId);
					}
				}
			});
		}
	};
	return oprCol;
}

/**
 * 设置界面值
 * @param {*} props 
 * @param {*} data 
 * @param {*} formId 
 * @param {*} tableId 
 */
export function setCardValue(props, data, formId = cardConst.FORMID, tableId = cardConst.TABLEID) {
	let status = props.form.getFormStatus(formId) || UISTATE.browse;
	let isBrowse = [ UISTATE.browse, UISTATE.blank ].includes(status);

	let bill_code = '';

	if (data) {
		// 获取单据号
		if (data.head && data.head[formId] && data.head[formId].rows && data.head[formId].rows[0].values) {
			let billCodeVal = data.head[formId].rows[0].values.bill_code;
			if (billCodeVal && billCodeVal.value) {
				bill_code = billCodeVal.value;
			}
		}

		// 是否差异VO
		let isLightVO = false;
		let userjson = data.userjson;
		if (userjson) {
			let userObj = JSON.parse(userjson);
			if (userObj && userObj['isLightVO'] == 'Y') {
				isLightVO = true;
			}
		}

		if (!isLightVO) {
			if (data.head) {
				props.form.setAllFormValue({ [formId]: data.head[formId] }, isBrowse);
			} else {
				props.form.EmptyAllFormValue(formId);
			}
			if (Array.isArray(tableId)) {
				if (data.bodys) {
					tableId.map((item) => {
						if (data.bodys[item]) {
							props.cardTable.setTableData(item, data.bodys[item], undefined, isBrowse);
						} else {
							props.cardTable.setTableData(item, { rows: [] }, undefined, isBrowse);
						}
					});
				} else {
					tableId.map((item) => {
						props.cardTable.setTableData(item, { rows: [] }, undefined, isBrowse);
					});
				}
			} else {
				if (data.body && data.body[tableId]) {
					props.cardTable.setTableData(tableId, data.body[tableId], undefined, isBrowse);
				} else {
					props.cardTable.setTableData(tableId, { rows: [] }, undefined, isBrowse);
				}
			}
		} else {
			updateHeadData.call(this, props, { data, formId });
			updateBodyData.call(this, props, { data, tableId });
		}
	} else {
		props.form.EmptyAllFormValue(formId);
		if (Array.isArray(tableId)) {
			tableId.map((item) => {
				props.cardTable.setTableData(item, { rows: [] }, undefined, isBrowse);
			});
		} else {
			props.cardTable.setTableData(tableId, { rows: [] }, undefined, isBrowse);
		}
	}
	setHeadAreaData.call(this, props, { bill_code });
}

/**
 * 卡片模板修改后公共处理
 * @param {*} props 
 * @param {*} meta 卡片模板 
 * @param {*} config
 *               tableId:表格区id，默认bodyvos
 */
export function afterModifyCardMeta(props, meta = {}, config = {}) {
	const tableId = config.tableId || cardConst.TABLEID;
	const metaProps = [
		'queryCondition',
		'isMultiSelectedEnabled',
		'isRunWithChildren',
		'defaultRunWithChildren',
		'onlyLeafCanSelect',
		'isShowUnit',
		'unitProps',
		'unitCondition'
	];
	meta = copyItemMetas.call(this, meta, tableId, metaProps);
	return meta;
}

/**
 * 将表格参照字段的过滤条件、参照多选等属性，复制给侧拉框中
 * @param {*} meta 卡片模板 
 * @param {*} tableId:表格区id，默认bodyvos
 */
function copyItemMetas(meta, tableId = cardConst.TABLEID, metaProps) {
	// 子表和他的侧拉编辑模板对应关系
	let gridrelation = meta['gridrelation'];
	if (!gridrelation) {
		return;
	}
	if (Array.isArray(tableId)) {
		tableId.map((item) => {
			copyItemMeta.call(this, meta, item, metaProps);
		});
	} else {
		copyItemMeta.call(this, meta, tableId, metaProps);
	}
	return meta;
}

function copyItemMeta(meta, tableId, metaProps) {
	let gridrelation = meta['gridrelation'];
	let tableMeta = meta[tableId];
	if (tableMeta && gridrelation[tableId] && gridrelation[tableId]['destEditAreaCode']) {
		let copyMetas = {};
		tableMeta.items.map((item) => {
			metaProps.map((prop) => {
				if (item.hasOwnProperty(prop)) {
					copyMetas[item.attrcode + '@' + prop] = item[prop];
				}
			});
		});
		meta[gridrelation[tableId]['destEditAreaCode']].items.map((item) => {
			metaProps.map((prop) => {
				if (copyMetas.hasOwnProperty(item.attrcode + '@' + prop)) {
					item[prop] = copyMetas[item.attrcode + '@' + prop];
				}
			});
		});
	}
}

/**
 * 主子表卡片创建统计信息
 * @param {*} props 
 * @param {*} config 必须
 *               tableId:表格区id，默认bodyvos
 */
export function getCommonTableHeadLeft(props, config = {}) {
	const tableId = config.tableId || cardConst.TABLEID;
	let status = props.cardTable.getStatus(tableId) || UISTATE.browse;
	let rowCount = 0;
	let allRows = props.cardTable.getVisibleRows(tableId);
	if (allRows) {
		rowCount = allRows.length;
	}
	let str = getMultiLangByID('cardUtils-000000', { num: rowCount }); /* 国际化处理： 总计 :{num}行*/
	let messages = [];
	if (str) {
		messages = str.split(rowCount);
	}
	return (
		<div className="definition-count">
			{status == UISTATE.browse && (
				<div className="definition-count-wrap">
					<span className="definition-search-count">{messages && messages.length > 0 && messages[0]}</span>
					<span className="countNum">{rowCount}</span>
					<span className="definition-search-unit">{messages && messages.length > 1 && messages[1]}</span>
				</div>
			)}
		</div>
	);
}

/**
 * 公共主子表单据主表编辑事件
 * @param {*} props 
 * @param {*} config 必须
 *               pagecode:页面编码
 *               formId:表单区id，默认card_head
 *               tableId:表格区id，默认bodyvos
 *               key:编辑的字段
 *               value:新值
 *               oldValue:旧值
 *               keys:需要后台处理编辑后事件的所有字段，不传表示都后台处理
 *               callback:回调方法
 */
export function commonHeadAfterEvent(props, config = {}) {
	const afterEditUrl = config.afterEditUrl;
	const pagecode = config.pagecode;
	const formId = config.formId || cardConst.FORMID;
	const tableId = config.tableId || cardConst.TABLEID;
	const key = config.key;
	const value = config.value;
	const oldValue = config.oldValue;
	const keys = config.keys;
	const callback = config.callback;
	// keys有值并且key不在keys内，则不需要后台处理编辑后事件
	if (keys && keys.length > 0 && !keys.includes(key)) {
		return;
	}

	let eventData = createHeadAfterEventData.call(this, props, config);
	ajax({
		url: afterEditUrl,
		data: eventData,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				// 性能优化，关闭表单和表格渲染
				props.beforeUpdatePage();
				updateHeadData.call(this, props, { data, formId });
				typeof callback == 'function' && callback(data);
				// 性能优化，表单和表格统一渲染
				props.updatePage(formId, false);
			}
		}
	});
}

/**
 * 公共主子表单据子表编辑事件
 * @param {*} props 
 * @param {*} config 必须
 *               pagecode:页面编码
 *               formId:表单区id，默认card_head
 *               tableId:表格区id，默认bodyvos
 *               key:编辑的字段
 *               record:编辑行数据
 *               changedRows:新值和旧值
 *               index:编辑行
 *               keys:需要后台处理编辑后事件的所有字段，不传表示都后台处理
 *               callback:回调方法
 */
export function commonBodyAfterEvent(props, config = {}) {
	const afterEditUrl = config.afterEditUrl;
	const pagecode = config.pagecode;
	const formId = config.formId || cardConst.FORMID;
	const tableId = config.tableId || cardConst.TABLEID;
	const key = config.key;
	const record = config.record;
	const changedRows = config.changedRows;
	const index = config.index;
	const keys = config.keys;
	const callback = config.callback;
	// keys有值并且key不在keys内，则不需要后台处理编辑后事件
	if (keys && keys.length > 0 && !keys.includes(key)) {
		return;
	}

	let eventData = createBodyAfterEventData.call(this, props, config);
	ajax({
		url: afterEditUrl,
		data: eventData,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				// 性能优化，关闭表单和表格渲染
				props.beforeUpdatePage();
				updateBodyData.call(this, props, { data, tableId });
				typeof callback == 'function' && callback(data);
				// 性能优化，表单和表格统一渲染
				props.updatePage(false, tableId);
			}
		}
	});
}

/**
 * 创建主子表单据主表编辑事件对象
 * @param {*} props 
 * @param {*} config 必须
 *               pagecode:页面编码
 *               formId:表单区id，默认card_head
 *               tableId:表格区id，默认bodyvos
 *               moduleId:编辑表格区id，默认card_head
 *               key:编辑的字段
 *               value:新值
 *               oldValue:旧值
 */
export function createHeadAfterEventData(props, config = {}) {
	const pagecode = config.pagecode;
	const formId = config.formId || cardConst.FORMID;
	const tableId = config.tableId || cardConst.TABLEID;
	const moduleId = config.moduleId || cardConst.FORMID;
	const key = config.key;
	const value = config.value;
	const oldValue = config.oldValue;
	let cardData = {
		pageid: pagecode,
		head: {}
	};
	let metaObj = props.meta.getMeta();
	cardData.templetid = metaObj.pageid;
	if (metaObj[formId] && metaObj[formId].moduletype && metaObj[formId].moduletype === 'form') {
		cardData.head[formId] = props.form.getAllFormValueSimple(formId);
		cardData.head[formId].areacode = formId;
	}
	let eventData = {
		attrcode: key,
		newvalue: value,
		oldvalue: oldValue,
		card: cardData
	};
	return eventData;
}

/**
 * 创建主子表单据子表编辑事件对象
 * @param {*} props 
 * @param {*} config 必须
 *               pagecode:页面编码
 *               formId:表单区id，默认card_head
 *               tableId:所有表格区id，默认bodyvos
 *               moduleId:编辑表格区id，默认bodyvos（不带'_edit'侧拉框）
 *               key:编辑的字段
 *               record:编辑行数据
 *               changedRows:新值和旧值
 *               index:编辑行
 */
export function createBodyAfterEventData(props, config = {}) {
	const pagecode = config.pagecode;
	const formId = config.formId || cardConst.FORMID;
	const tableId = config.tableId || cardConst.TABLEID;
	const moduleId = config.moduleId || cardConst.TABLEID;
	const key = config.key;
	const record = config.record;
	const changedRows = config.changedRows;
	const index = config.index;
	let cardData = {
		pageid: pagecode,
		head: {}
	};
	let metaObj = props.meta.getMeta();
	cardData.templetid = metaObj.pageid;
	if (metaObj[formId] && metaObj[formId].moduletype && metaObj[formId].moduletype === 'form') {
		cardData.head[formId] = props.form.getAllFormValueSimple(formId);
		cardData.head[formId].areacode = formId;
	}
	if (metaObj[moduleId] && metaObj[moduleId].moduletype && metaObj[moduleId].moduletype === 'table') {
		if (Array.isArray(tableId)) {
			cardData.bodys = { [moduleId]: {} };
			cardData.bodys[moduleId].rows = [ record ];
			cardData.bodys[moduleId].areaType = 'table';
			cardData.bodys[moduleId].areacode = moduleId;
		} else {
			cardData.body = { [moduleId]: {} };
			cardData.body[moduleId].rows = [ record ];
			cardData.body[moduleId].areaType = 'table';
			cardData.body[moduleId].areacode = moduleId;
		}
	}
	// 编辑时候传单行数据，防止多选索引错误，这里设置为0
	let eventData = {
		areacode: moduleId,
		attrcode: key,
		changedrows: changedRows,
		card: cardData,
		index: 0
	};
	return eventData;
}

/**
 * 更新表头数据
 * @param {*} props 
 * @param {*} config 必须
 *               pagecode:页面编码
 *               formId:表单区id，默认card_head
 *               tableId:表格区id，默认bodyvos
 *               moduleId:编辑表格区id，默认bodyvos（不带'_edit'侧拉框）
 *               key:编辑的字段
 *               record:编辑行数据
 *               changedRows:新值和旧值
 *               index:编辑行
 *               keys:需要后台处理编辑后事件的所有字段，不传表示都后台处理
 *               callback:回调方法
 */
export function updateHeadData(props, config = {}) {
	const formId = config.formId || cardConst.FORMID;
	const data = config.data;
	let status = props.form.getFormStatus(formId) || UISTATE.browse;
	let isBrowse = [ UISTATE.browse, UISTATE.blank ].includes(status);

	props.form.setAllFormValue({ [formId]: data.head[formId] }, isBrowse);
}

/**
 * 更新表体数据
 * @param {*} props 
 * @param {*} config 必须
 *               pagecode:页面编码
 *               formId:表单区id，默认card_head
 *               tableId:表格区id，默认bodyvos
 *               moduleId:编辑表格区id，默认bodyvos（不带'_edit'侧拉框）
 *               key:编辑的字段
 *               record:编辑行数据
 *               changedRows:新值和旧值
 *               index:编辑行
 *               keys:需要后台处理编辑后事件的所有字段，不传表示都后台处理
 *               callback:回调方法
 */
export function updateBodyData(props, config = {}) {
	const tableId = config.tableId || cardConst.TABLEID;
	const data = config.data;

	// 是否差异VO
	let removeDeletedBodys = false;
	let userjson = data.userjson;
	if (userjson) {
		let userObj = JSON.parse(userjson);
		if (userObj && userObj['removeDeletedBodys'] == 'Y') {
			removeDeletedBodys = true;
		}
	}
	if (Array.isArray(tableId)) {
		if (data.bodys) {
			tableId.map((item) => {
				if (data.bodys[item]) {
					let status = props.cardTable.getStatus(item) || UISTATE.browse;
					let isBrowse = [ UISTATE.browse ].includes(status);
					props.cardTable.updateDataByRowId(item, data.bodys[item], isBrowse || removeDeletedBodys);
					let newRows = data.bodys[item].rows.filter((v) => {
						if (!v.rowid) {
							return v;
						}
					});
					if (newRows && newRows.length > 0) {
						let index = props.cardTable.getCurrentIndex(item);
						props.cardTable.insertRowsAfterIndex(item, newRows, index);
					}
				}
			});
		}
	} else {
		if (data.body && data.body[tableId]) {
			let status = props.cardTable.getStatus(tableId) || UISTATE.browse;
			let isBrowse = [ UISTATE.browse ].includes(status);
			props.cardTable.updateDataByRowId(tableId, data.body[tableId], isBrowse || removeDeletedBodys);
			let newRows = data.body[tableId].rows.filter((item) => {
				if (!item.rowid) {
					return item;
				}
			});
			if (newRows && newRows.length > 0) {
				let index = props.cardTable.getCurrentIndex(tableId);
				props.cardTable.insertRowsAfterIndex(tableId, newRows, index);
			}
		}
	}
}

/**
 * 设置协同按钮可用性
 * @param {*} props 
 * @param {*} formId 
 */
export function setCooperateBtnEnable(props, formId = 'card_head') {
	let status = this.props.form.getFormStatus(formId);
	let affirm_flag = this.props.form.getFormItemsValue(formId, cardConst.COMMONKEYS.affirm_flag).value || false;
	let coop_pk_bill = this.props.form.getFormItemsValue(formId, cardConst.COMMONKEYS.coop_pk_bill).value || false;
	let cooperate_bill = this.props.form.getFormItemsValue(formId, cardConst.COMMONKEYS.cooperate_bill).value || false;
	if (UISTATE.edit == status) {
		//生成协同单据修改判断保存提交是否可用
		if (false == affirm_flag && coop_pk_bill && true == cooperate_bill) {
			props.button.setButtonVisible('SaveCommit', false);
		} else if (true == affirm_flag && coop_pk_bill && true == cooperate_bill) {
			props.button.setButtonVisible('SaveCommit', true);
		}
		return;
	}
	//判断协同确认按钮状态
	if (false == affirm_flag && coop_pk_bill && true == cooperate_bill) {
		props.button.setButtonDisabled('CooperateAffirm', false);
		props.button.setButtonDisabled('Commit', true);
	} else {
		props.button.setButtonDisabled('CooperateAffirm', true);
	}
	//判断取消协同按钮状态
	if (true == affirm_flag && coop_pk_bill && true == cooperate_bill) {
		props.button.setButtonDisabled('CancelAffirm', false);
		props.button.setButtonDisabled('Commit', false);
	} else {
		props.button.setButtonDisabled('CancelAffirm', true);
	}
}

/**
 * 复制行
 * @param {*} props 
 * @param {*} tableId 
 */
export function copyLine(props, tableId) {
	// 设置复选框不可编辑
	props.cardTable.setAllCheckboxAble(tableId, false);
}

/**
 * 粘贴行
 * @param {*} props 
 * @param {*} config 
 * config说明：
 * ｛
 * 		pasteLineType：粘贴行类型：粘贴行、粘贴行到末行
 * 		tableId：表格区域code
 * 		clearFields：粘贴行时需要清空的字段
 * 		currentIndex：粘贴至此的行的index
 * 		field：行号字段，默认为‘rowno’
 * 		oriStep：步长，默认为10
 * 		isStepDouble：行号是否可为小数，默认true：可以
 * ｝
 */
export function pasteLine(props, config = {}) {
	let pasteLineType = config.pasteLineType || cardConst.PASTELINETYPE.PasteLine;
	let tableId = config.tableId || cardConst.TABLEID;
	let clearFields = config.clearFields || [];
	let currentIndex = config.currentIndex || 0;
	let field = config.field || 'rowno';
	let oriStep = config.oriStep || 10;
	let isStepDouble = config.isStepDouble || true;
	// 获取选中的行
	let copyedRows = props.cardTable.getCheckedRows(tableId);
	// 默认粘贴到首行
	let preNum = 0;
	if (currentIndex != 0) {
		// 若当前index不为0，则是粘贴到中间行，获取前一行的行号
		preNum = parseFloat(props.cardTable.getValByKeyAndIndex(tableId, currentIndex - 1, field).value);
	}

	// 获取当前行的行号
	let nextNum = parseFloat(props.cardTable.getValByKeyAndIndex(tableId, currentIndex, field).value);
	// 如果为粘贴行到末行
	if (pasteLineType == cardConst.PASTELINETYPE.PasteLineToTail) {
		// 则设置当前行的行号为0（计算行号用）
		nextNum = 0;
		// 获取最后一行的行号
		let lastIndex = props.cardTable.getVisibleRows(tableId).length - 1;
		preNum = parseFloat(props.cardTable.getValByKeyAndIndex(tableId, lastIndex, field).value);
	}
	// 获取粘贴行的行数
	let newRowsLength = copyedRows.length;
	// 计算新增的行的行号
	let rownos = getNewRowsNum(preNum, nextNum, newRowsLength, oriStep, isStepDouble);

	let rows = [];
	let pasteData = [];
	// 构造粘贴行的数据
	copyedRows.map((item, index) => {
		let row = {};
		row.selected = false;
		row.status = VOSTATUS.NEW; // 粘贴的行都为新增态
		row.values = {};
		for (let idx in item.data.values) {
			row.values[idx] = { ...item.data.values[idx] };
		}
		// 设置行号
		row.values[field] = { display: null, scale: '-1', value: String(rownos[index]) };
		// 清空指定字段
		clearFields.map((field) => {
			row.values[field] = { display: null, scale: '-1', value: null };
		});
		// 构造粘贴到末行数据
		rows.push(row);
		// 构造粘贴行数据
		pasteData.push({ index: currentIndex + index, data: row });
	});
	if (pasteLineType == cardConst.PASTELINETYPE.PasteLineToTail) {
		let lastIndex = props.cardTable.getVisibleRows(tableId).length - 1;
		// 粘贴到末行
		props.cardTable.insertRowsAfterIndex(tableId, rows, lastIndex);
	} else {
		// 粘贴行
		props.cardTable.insertDataByIndexs(tableId, pasteData);
	}
	// 恢复复选框的可编辑性
	props.cardTable.setAllCheckboxAble(tableId, true);
	// 复选框都变为未选中状态
	props.cardTable.selectAllRows(tableId, false);
}

/**
 * 重排行号
 * @param {*} props 
 * @param {*} tableId 
 */
export function handlRowNo(props, tableId, field) {
	let vrows = props.cardTable.getVisibleRows(tableId);
	vrows.map((row, index) => {
		props.cardTable.setValByKeyAndIndex(tableId, index, field, { value: ((index + 1) * 10).toString() });
	});
}

/**
 * 
 * @param {*} props 
 * @param {*} copyBtns 复制行按钮组
 * @param {*} pasteBtns 粘贴行按钮组
 * @param {*} flag true：复制行按钮组显示 false：粘贴行按钮组显示
 */
export function setCardBodyBtnVisible(props, copyBtns, pasteBtns, flag) {
	let btnsObj = {};
	copyBtns.map((btn) => {
		btnsObj[btn] = flag;
	});
	pasteBtns.map((btn) => {
		btnsObj[btn] = !flag;
	});
	props.button.setButtonVisible(btnsObj);
}

/**
 * 获得新设置行的行号
 * @param {*} preNum 粘贴行时，当前行的前一行的行号；粘贴行到末行是，最后一行的行号
 * @param {*} nextNum 粘贴行时，当前行的行号
 * @param {*} newRowsLength 增加的行的数量
 * @param {*} oriStep 默认步长
 * @param {*} isStepDouble 行号是否支持小数
 */
export function getNewRowsNum(preNum, nextNum, newRowsLength, oriStep, isStepDouble) {
	let newNum = [];
	if (nextNum == 0) {
		// 当前行的行号为0，则为新行增至行末（增行或粘贴行到末行）
		for (let i = 0; i < newRowsLength; i++) {
			newNum.push(String(preNum + oriStep * (i + 1)));
		}
	} else {
		// 下一行有数据，新行插入中间或行首
		// 步长最多保留8位小数
		let tempNextNum = nextNum.toString().split('.');
		// 获取当前行号的小数位数
		let nextNumPointLen = tempNextNum.length == 2 ? tempNextNum[1].length : 0;
		let tempPreNum = nextNum.toString().split('.');
		// 获取上一行行号的小数位数
		let preNumPointLen = tempPreNum.length == 2 ? tempPreNum[1].length : 0;
		// 取其中较大的位数
		let pointLen = Math.max(nextNumPointLen, preNumPointLen);
		// 相减时保留较大的位数再去计算步长
		let step =
			Math.floor(parseFloat((nextNum - preNum).toFixed(pointLen)) / (newRowsLength + 1) * Math.pow(10, 9)) /
			Math.pow(10, 9); // 获得插入行步长
		if (!isStepDouble) {
			// 步长取整
			step = Math.trunc(step);
		}
		// 设置新序号为空
		for (let i = 0; i < newRowsLength; i++) {
			if (step == 0) {
				// 步长为小于1的小数且不允许序号为小数的情况，将序号设置空字符串
				if (isStepDouble) {
					newNum.push(String(preNum));
				} else {
					newNum.push('');
				}
			} else {
				newNum.push(String(preNum + step * (i + 1)));
			}
		}
		if (isStepDouble) {
			// 行号允许为小数时，计算行号的精度
			newNum = newNum.map((num) => {
				return parseFloat(num);
			});
			newNum = setScale(newNum, preNum, nextNum);
		}
	}
	return newNum;
}

/**
 * 
 * @param {*} rownos 保存行号的数据，数值中为数值型
 * @param {*} len 行号保留的小数的位数，默认为0
 */
function setScale(rownos, preNum, nextNum, len = 0) {
	// 定义临时行号的数组
	let temRownos = [];
	//
	let maxLenRownos = [];
	// 遍历行号，保留【len】位小数存入临时数组
	rownos.map((rowno) => {
		let num = Math.floor(rowno * Math.pow(10, len)) / Math.pow(10, len);
		if (!temRownos.includes(num)) {
			// 保留【len】位小数后，过滤重复的行号。
			temRownos.push(num);
		}
		if (len == 9 && rownos.length > 1) {
			maxLenRownos.push(preNum);
		} else if (len == 9 && rownos.length == 1) {
			maxLenRownos.push(nextNum);
		}
	});
	if (len == 9) {
		return maxLenRownos;
	}
	// 如果原数组与临时数组的长度一致，且第一个行号与前一个行号不等,
	// 则临时数组中的行号满足条件，直接返回。
	if (rownos.length == temRownos.length && temRownos[0] > preNum) {
		return temRownos;
	} else {
		// 如果不一致，则小数位数加1，继续对行号进行精度处理。
		len++;
		return setScale(rownos, preNum, nextNum, len);
	}
}
