import { ajax } from 'nc-lightapp-front';
import { setContext, getContext } from '../components/AMInitInfo/loginContext';
import { TransferConst } from '../const/CommonKeys';

let isAddExist = false;
//根据
export default function conactAddButton(props, billtype, transtype, area, callback) {
	let buttons = props.button.getButtons();
	let buttonArea;
	if (area) {
		buttonArea = area;
	}
	//补充拉单按钮的信息
	ajax({
		url: '/nccloud/ampub/common/querySrcActionInfo.do',
		data: {
			billtype,
			transtype
		},
		success: (res) => {
			if (res.data) {
				isAddExist = false;
				//和需求UE确认拉单按钮放在新增按钮前，暂不放在新增按钮后
				//getOldAddButton(buttons,res.data);
				if (!isAddExist) {
					buttons.unshift(getnewAddButtons(res.data, buttonArea));
				}
				props.button.setButtons(buttons, callback);
			}
		}
	});
}
function getOldAddButton(buttons, srcinfo) {
	for (let i = 0, len = buttons.length; i < len; i++) {
		if (buttons[i].key === 'Add') {
			let addButton = buttons[i];
			isAddExist = true;
			let addGroup = getnewAddButtons(srcinfo, addButton.area);
			addGroup.children.unshift(addButton);
			buttons[i] = addGroup;
			break;
		} else if (buttons[i].children != null && buttons[i].children.length > 0) {
			getOldAddButton(buttons[i].children, srcinfo);
		}
	}
}

//获取新增按钮组
function getnewAddButtons(srcinfo, area) {
	srcinfo = JSON.parse(srcinfo);
	let addGroup = {
		key: 'AddTransferGroup', //与CommonKeys中的ButtonConst保持一致
		type: 'buttongroup',
		area,
		title: null
	};
	let newAddButtons = new Array(); //前端拉单按钮json数组
	for (let n = 0; n < srcinfo.length; n++) {
		let temp = srcinfo[n];
		if (temp.actionCode != null) {
			let billType = temp.billType;
			let transType = temp.transType;
			setContext(billType + transType, temp.busitypes, TransferConst.dataSource);
			setContext(TransferConst.src_billtype_cache, billType, TransferConst.dataSource);
			setContext(TransferConst.src_transtype_cache, transType, TransferConst.dataSource);
			newAddButtons[n] = {
				key: temp.actionCode,
				title: temp.actionName,
				area,
				parentCode: 'AddTransferGroup',
				btncolor: 'button_secondary',
				type: 'general_btn'
			};
		}
	}
	addGroup.children = newAddButtons;
	return addGroup;
}
