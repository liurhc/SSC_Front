// 页面配置
export const pageConfig = {
	// 应用主键
	appid: '0001Z9100000000012L3',
	// 应用编码
	appcode: '450103044A',
	// 应用类型
	nodeType: 'group',
	// 应用名称
	title: '450103044A-000000' /* 国际化处理： 工种*/,
	// 表格区域id
	tableId: 'list_head',
	// 页面编码
	pagecode: '450103044A_list',
	// 主键字段
	pkField: 'pk_craft',
	// 编辑态按钮
	editBtns: [ 'Add', 'Delete', 'Save', 'Cancel' ],
	// 浏览态按钮
	browseBtns: [ 'Add', 'Edit', 'Delete', 'Attachment', 'Print', 'Output', 'Refresh' ], //加了一个附件
	url: {
		queryUrl: '/nccloud/ampub/craft/query.do',
		saveUrl: '/nccloud/ampub/craft/save.do',
		deleteUrl: '/nccloud/ampub/craft/delete.do',
		sealUrl: '/nccloud/ampub/craft/seal.do',
		unSealUrl: '/nccloud/ampub/craft/unseal.do',
		printUrl: '/nccloud/ampub/craft/printList.do'
	},
	// 模糊搜索过滤字段
	filterFields: [ 'craft_code', 'craft_name' ],
	exceptFields: [ 'pk_group', 'pk_org', 'enablestate' ],
	// 输出文件名称
	printFilename: '450103044A-000001' /* 国际化处理： 工种*/,
	// 打印模板节点标识
	printNodekey: null
};
