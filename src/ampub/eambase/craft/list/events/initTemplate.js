import ampub from 'ampub';
import { setStatus, enableRow, disableRow, getData } from './buttonClick';
import tableButtonClick from './tableButtonClick';
import { pageConfig } from '../const';

const { commonConst, components, utils } = ampub;
const { tableUtils } = utils;
const { LoginContext } = components;
const { loginContext } = LoginContext;
const { StatusUtils } = commonConst;
const { UISTATE } = StatusUtils;
const { createOprationColumn, createEnableSwitch } = tableUtils;

const { pagecode, tableId, nodeType } = pageConfig;

/**
 * 初始化模板
 * @param {*} props 
 */
export default function(props) {
	props.createUIDom(
		{
			//页面编码
			pagecode
		},
		(data) => {
			if (data) {
				if (data.context) {
					// 初始化上下文变量
					loginContext(data.context);
				}
				if (data.button) {
					// 设置按钮
					let button = data.button;
					props.button.setButtons(button);
				}
				if (data.template) {
					let meta = data.template;
					meta = modifierMeta.call(this, props, meta);
					props.meta.setMeta(meta, () => {
						afterInit.call(this, props);
					});
				}
			}
		}
	);
}

/**
 * 模板初始化以后
 * @param {*} props 
 */
function afterInit(props) {
	let status = props.getUrlParam('status') || UISTATE.browse;
	setStatus.call(this, props, status);
	getData.call(this, props);
}

/**
 * 修改模板
 * @param {*} props 
 * @param {*} meta 
 */
function modifierMeta(props, meta) {
	if (meta[tableId] && meta[tableId].items && meta[tableId].items.length > 0) {
		// 处理停启用开关
		createEnableSwitch.call(this, props, { meta, enableRow, disableRow });

		// 添加表格操作列
		let oprCol = createOprationColumn.call(this, props, { nodeType, tableButtonClick });
		meta[tableId].items.push(oprCol);
	}

	return meta;
}
