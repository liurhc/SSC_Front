export const pageConfig = {
	// 表体区域ID
	tableId: 'list_head',
	// 应用类型
	nodeType: 'group',
	// 应用编码
	appcode: '450103036A',
	// 页面编码
	pagecode: '450103036A_list',
	// 主表主键字段
	pkField: 'pk_priority',
	// 标题
	title: '450103036A-000000' /*国际化处理 工单优先级 */,
	// 过滤数据字段
	filterFields: [ 'prior_code', 'prior_name' ],
	// 请求链接
	url: {
		saveUrl: '/nccloud/ampub/wopriority/save.do',
		deleteUrl: '/nccloud/ampub/wopriority/delete.do',
		unSealUrl: '/nccloud/ampub/wopriority/unseal.do',
		sealUrl: '/nccloud/ampub/wopriority/seal.do',
		printUrl: '/nccloud/ampub/wopriority/printList.do',
		queryUrl: '/nccloud/ampub/wopriority/query.do'
	},
	// 保存过滤空行校验无效字段
	exceptFields: [ 'enablestate' ],
	// 编辑态按钮
	editBtns: [ 'Add', 'Delete', 'Save', 'Cancel' ],
	// 浏览态按钮
	browseBtns: [ 'Add', 'Edit', 'Delete', 'Print', 'Output', 'Refresh' ],
	// 输出文件名称
	printFilename: '450103036A-000000' /*国际化处理 工单优先级 */,
	// 打印模板节点标识
	printNodekey: null
};
