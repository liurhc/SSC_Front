export const pageConfig = {
	// 表体区域ID
	tableId: 'list_head',
	// 应用类型
	nodeType: 'group',
	// 应用编码
	appcode: '450103040A',
	// 页面编码
	pagecode: '450103040A_list',
	// 主表主键字段
	pkField: 'pk_means',
	// 标题
	title: '450103040A-000000' /*国际化处理 更换备件处理方法*/,
	// 过滤数据字段
	filterFields: [ 'means_code', 'means_name' ],
	// 请求链接
	url: {
		saveUrl: '/nccloud/ampub/disposemeans/save.do',
		deleteUrl: '/nccloud/ampub/disposemeans/delete.do',
		unSealUrl: '/nccloud/ampub/disposemeans/unseal.do',
		sealUrl: '/nccloud/ampub/disposemeans/seal.do',
		printUrl: '/nccloud/ampub/disposemeans/printList.do',
		queryUrl: '/nccloud/ampub/disposemeans/query.do'
	},
	// 保存过滤空行校验无效字段
	exceptFields: [ 'enablestate' ],
	// 编辑态按钮
	editBtns: [ 'Add', 'Delete', 'Save', 'Cancel' ],
	// 浏览态按钮
	browseBtns: [ 'Add', 'Edit', 'Delete', 'Print', 'Output', 'Refresh' ],
	// 输出文件名称
	printFilename: '450103040A-000000' /*国际化处理 更换备件处理方法 */,
	// 打印模板节点标识
	printNodekey: null
};
