import { ajax, print, output } from 'nc-lightapp-front';
import ampub from 'ampub';
import { pageConfig } from '../const';

const { utils, commonConst } = ampub;
const { tableUtils, msgUtils, multiLangUtils } = utils;
const {
	modifyBeforeAjax,
	modifyAfterAjax,
	setTableValue,
	modifyBeforeAjaxByIndex,
	modifyAfterAjaxByIndex
} = tableUtils;
const { showConfirm, MsgConst, showMessage } = msgUtils;
const { getMultiLangByID } = multiLangUtils;
const { StatusUtils } = commonConst;
const { UISTATE, VOSTATUS } = StatusUtils;

const {
	tableId,
	pagecode,
	url,
	exceptFields,
	filterFields,
	editBtns,
	browseBtns,
	pkField,
	printFilename,
	printNodekey
} = pageConfig;

/**
 * 
 * 按钮点击事件
 * @param {*} props 
 * @param {*} id 
 * @param {*} tableId 
 */
export default function buttonClick(props, id, tableId) {
	switch (id) {
		case 'Add':
			addRow.call(this, props);
			break;
		case 'Edit':
			edit.call(this, props);
			break;
		case 'Delete':
			batchDelRow.call(this, props);
			break;
		case 'Save':
			save.call(this, props);
			break;
		case 'Cancel':
			cancelConfirm.call(this, props);
			break;
		case 'Print':
			printTemp.call(this, props);
			break;
		//  case 'Attachment':
		// 	attachment.call(this, props);
		// 	break;
		case 'Output':
			outputTemp.call(this, props);
			break;
		case 'Refresh':
			refresh.call(this, props);
			break;
		default:
			break;
	}
}

/**
 * 新增
 * @param {*} props 
 */
export function addRow(props, isBlur = false) {
	let oldstatus = props.editTable.getStatus(tableId);
	if (oldstatus != UISTATE.edit) {
		setStatus.call(this, props, UISTATE.edit);
	}

	let defaultValue = {};
	defaultValue['enablestate'] = {
		value: '2'
	};
	props.editTable.addRow(tableId, undefined, isBlur == false, defaultValue);
}

/**
 * 修改
 * @param {*} props 
 */
function edit(props) {
	setStatus.call(this, props, UISTATE.edit);
}

/**
 * 批量删除
 * @param {*} props 
 */
function batchDelRow(props) {
	let checkedRows = props.editTable.getCheckedRows(tableId);
	if (checkedRows.length == 0) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseDelete });
		return;
	}
	let indexArr = [];
	checkedRows.map((row) => {
		indexArr.push(row.index);
	});

	let status = props.editTable.getStatus(tableId);
	if (status == undefined || status == UISTATE.browse) {
		showConfirm.call(this, props, { type: MsgConst.Type.DelSelect, beSureBtnClick: batchDel });
	} else {
		delRow.call(this, props, indexArr);
	}
}

/**
 * 前台删除
 * @param {*} props 
 * @param {*} index 
 */
export function delRow(props, index) {
	props.editTable.deleteTableRowsByIndex(tableId, index);
}

/**
 * 浏览态直接删除单个数据
 * @param {*} props 
 * @param {*} tableId 
 * @param {*} index 
 */
export function singleDel(props, index) {
	let allRows = props.editTable.getAllRows(tableId);
	modifyBeforeAjax(allRows[index]);
	let changedRows = [ allRows[index] ];
	const data = {
		pageid: pagecode,
		model: {
			areaType: 'table',
			areacode: tableId,
			rows: changedRows
		}
	};
	ajax({
		url: url.deleteUrl,
		data,
		success: (res) => {
			let { success } = res;
			if (success) {
				let allData = props.editTable.getAllData(tableId);
				allData.rows = allData.rows.filter((item, i) => i != index);
				setValue.call(this, props, { [tableId]: allData });
				showMessage.call(this, props, { type: MsgConst.Type.DeleteSuccess });
			}
		}
	});
}

/**
 * 后台删除
 * @param {*} props 
 */
function batchDel(props) {
	let checkedRows = props.editTable.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length == 0) {
		return;
	}
	// 平台没有克隆数据问题
	checkedRows = JSON.parse(JSON.stringify(checkedRows));
	let delRows = [];
	let checkedIndexs = [];
	// 设置删除状态
	checkedRows.map((item, index) => {
		modifyBeforeAjax(item.data);
		delRows.push(item.data);
		checkedIndexs.push(item.data.flterIndex);
	});
	const data = {
		pageid: pagecode,
		model: {
			areaType: 'table',
			areacode: tableId,
			rows: delRows
		}
	};
	ajax({
		url: url.deleteUrl,
		data,
		success: (res) => {
			let { success } = res;
			if (success) {
				let allData = props.editTable.getAllData(tableId);
				allData.rows = allData.rows.filter((item, i) => !checkedIndexs.includes(i));
				setValue.call(this, props, { [tableId]: allData });
				showMessage.call(this, props, { type: MsgConst.Type.DeleteSuccess });
			}
		}
	});
}

/**
 * 保存
 * @param {*} props 
 */
function save(props) {
	props.editTable.filterEmptyRows(tableId, exceptFields);
	let visibleRows = props.editTable.getVisibleRows(tableId);
	let pass = props.editTable.checkRequired(tableId, visibleRows);
	// 必输项校验没过
	if (!pass) {
		return;
	}
	let changedRows = props.editTable.getChangedRows(tableId);
	// 没有修改行，变更状态
	if (!changedRows || changedRows.length == 0) {
		setStatus.call(this, props, UISTATE.browse); //设置表格状态
		showMessage.call(this, props, { type: MsgConst.Type.SaveSuccess });
		return;
	}
	changedRows.map((item) => {
		modifyBeforeAjax(item);
	});
	const data = {
		pageid: pagecode,
		model: {
			areaType: 'table',
			areacode: tableId,
			rows: changedRows
		}
	};
	// 保存前执行验证公式
	props.validateToSave(data, () => {
		// 调用Ajax保存数据
		ajax({
			url: url.saveUrl,
			data,
			success: (res) => {
				let { success, data } = res;
				if (success) {
					let retData = data[tableId];
					let newRows = [];
					let i = 0;
					let allData = props.editTable.getAllData(tableId);
					let allRows = allData.rows;
					allRows.map((item, index) => {
						// 0原始1修改2新增3删除
						if (item.status == VOSTATUS.UPDATED || item.status == VOSTATUS.NEW) {
							if (retData && retData.rows && retData.rows.length > i) {
								modifyAfterAjax(retData.rows[i]);
								newRows.push(retData.rows[i]);
							}
							i++;
						} else if (item.status == VOSTATUS.DELETED) {
							i++;
						} else {
							newRows.push(item);
						}
					});
					allData.rows = newRows;
					setStatus.call(this, props, UISTATE.browse);
					setValue.call(this, props, { [tableId]: allData });
					setBatchBtnsEnable.call(this, props, tableId);
					showMessage.call(this, props, { type: MsgConst.Type.SaveSuccess });
				}
			}
		});
	});
}

/**
* 取消
* @param {*} props 
*/
function cancelConfirm(props) {
	showConfirm.call(this, props, { type: MsgConst.Type.Cancel, beSureBtnClick: cancel });
}

/**
 * 取消
 * @param {*} props 
 */
function cancel(props) {
	props.editTable.cancelEdit(tableId);
	setStatus.call(this, props, UISTATE.browse);
}

/**
 * 启用
 * @param {*} props 
 * @param {*} tableId 
 * @param {*} index 
 */
export function enableRow(props, index) {
	let allData = props.editTable.getAllData(tableId);
	let allRows = allData.rows;
	modifyBeforeAjax(allRows[index]);
	const data = {
		pageid: pagecode,
		model: {
			areaType: 'table',
			areacode: tableId,
			rows: [ allRows[index] ]
		}
	};
	ajax({
		url: url.unSealUrl,
		data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				// 更新数据
				let retData = data[tableId];
				allRows[index] = retData.rows[0];
				modifyAfterAjax(allRows[index]);
				allData.rows = allRows;
				setValue.call(this, props, { [tableId]: allData });
				showMessage.call(this, props, { type: MsgConst.Type.EnableSuccess });
			}
		}
	});
}

/**
 * 停用
 * @param {*} props 
 * @param {*} tableId 
 * @param {*} index 
 */
export function disableRow(props, index) {
	let allData = props.editTable.getAllData(tableId);
	let allRows = allData.rows;
	modifyBeforeAjax(allRows[index]);
	const data = {
		pageid: pagecode,
		model: {
			areaType: 'table',
			areacode: tableId,
			rows: [ allRows[index] ]
		}
	};
	ajax({
		url: url.sealUrl,
		data,
		success: (res) => {
			let { success, data } = res;
			if (success) {
				// 更新数据
				let retData = data[tableId];
				allRows[index] = retData.rows[0];
				modifyAfterAjax(allRows[index]);
				allData.rows = allRows;
				setValue.call(this, props, { [tableId]: allData });
				showMessage.call(this, props, { type: MsgConst.Type.DisableSuccess });
			}
		}
	});
}

/**
 * 设置界面状态
 * @param {*} props 
 * @param {*} status 
 */
export function setStatus(props, status) {
	switch (status) {
		case 'edit':
			props.editTable.setStatus(tableId, UISTATE.edit);
			setBtnsVisible.call(this, props);
			modifyBeforeAjaxByIndex.call(this, props);
			break;
		default:
			props.editTable.setStatus(tableId, UISTATE.browse);
			setBtnsVisible.call(this, props);
			modifyAfterAjaxByIndex.call(this, props);
			break;
	}
}

/**
 * 设置界面值
 * @param {*} props 
 * @param {*} status 
 */
export function setValue(props, data) {
	setTableValue.call(this, props, data);
	if (data && data[tableId] && data[tableId].rows) {
		// 调用平台api过滤显示数据
		let { keyWord } = this.state;
		props.editTable.setFiltrateTableData(tableId, filterFields, keyWord);
	}
}

/**
 * 设置按钮显示隐藏
 * @param {*} props 
 */
function setBtnsVisible(props) {
	let status = props.editTable.getStatus(tableId) || UISTATE.browse;
	let btnObj = {};
	if (status != UISTATE.browse) {
		browseBtns.map((item) => {
			btnObj[item] = false;
		});
		editBtns.map((item) => {
			btnObj[item] = true;
		});
		// 如果有新增按钮，改为次要按钮
		props.button.setMainButton('Add', false);
		// 行删除时不需要悬浮框提示
		props.button.setPopContent('Delete', undefined);
	} else {
		editBtns.map((item) => {
			btnObj[item] = false;
		});
		browseBtns.map((item) => {
			btnObj[item] = true;
		});
		// 如果有新增按钮，改为主要按钮
		props.button.setMainButton('Add', true);
		// 行删除时悬浮框提示
		props.button.setPopContent('Delete', getMultiLangByID('msgUtils-000001')); /* 国际化处理：确定要删除吗？ SVG*/
	}
	props.button.setButtonVisible(btnObj);
}

/**
 * 设置批量按钮是否可用
 * @param {*} props 
 * @param {*} moduleId 
 */
export function setBatchBtnsEnable(props, moduleId) {
	let checkedRows = props.editTable.getCheckedRows(moduleId);
	const { batchBtns = [ 'Delete', 'Print', 'Output' ] } = pageConfig;
	props.button.setButtonDisabled(batchBtns, !(checkedRows && checkedRows.length > 0)); //批量大于0
}

/**
 * 打印
 * @param {*} props 
 */
function printTemp(props) {
	let printData = getPrintData.call(this, props, 'print');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChoosePrint });
		return;
	}

	print(
		'pdf', // 支持两类：'html'为模板打印，'pdf'为pdf打印
		url.printUrl, // 后台打印服务url
		printData
	);
}

/**
 * 输出
 * @param {*} props 
 */
function outputTemp(props) {
	let printData = getPrintData.call(this, props, 'output');
	if (!printData) {
		showMessage.call(this, props, { type: MsgConst.Type.ChooseOutput });
		return;
	}
	output({
		url: url.printUrl,
		data: printData
	});
}

/**
 * 获取打印数据
 * @param {*} props 
 * @param {*} outputType 
 */
function getPrintData(props, outputType = 'print') {
	let checkedRows = props.editTable.getCheckedRows(tableId);
	if (!checkedRows || checkedRows.length == 0) {
		return false;
	}
	let pks = [];
	checkedRows.map((item) => {
		pks.push(item.data.values[pkField].value);
	});
	let printData = {
		filename: getMultiLangByID(printFilename), // 文件名称
		nodekey: printNodekey, // 模板节点标识
		oids: pks, // 需要打印数据主键
		outputType // 输出类型
	};
	return printData;
}

/**
* 刷新
* @param {*} props 
*/
function refresh(props) {
	getData.call(this, props, undefined, true);
}

/**
 * 获取数据
 * @param {*} props 
 * @param {*} isShowOff 
 * @param {*} isRefresh 
 */
export function getData(props, isShowOff = this.state.isShowOff, isRefresh) {
	ajax({
		url: url.queryUrl,

		data: {
			pagecode,
			nodeType: 'GROUP_NODE',
			isShowSeal: isShowOff ? 'Y' : 'N',
			areacode: tableId
		},
		success: (res) => {
			let { success, data } = res;
			if (success) {
				if (data) {
					setValue.call(this, this.props, data);
					if (data[tableId].rows && data[tableId].rows.length > 0) {
						setBatchBtnsEnable.call(this, this.props, tableId);
					}
					if (isRefresh) {
						showMessage.call(this, props, { type: MsgConst.Type.RefreshSuccess });
					}
				}
			}
		}
	});
}
