import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage, base } from 'nc-lightapp-front';
import ampub from 'ampub';
import { pageConfig } from './const';
import { buttonClick, initTemplate, afterEvent, rowSelected, getData } from './events';

const { utils } = ampub;
const { multiLangUtils, closeBrowserUtils } = utils;
const { getMultiLangByID, initMultiLangByModule } = multiLangUtils;

const { tableId, filterFields, pagecode, title } = pageConfig;
const { NCFormControl, NCCheckbox } = base;

class Tool extends Component {
	constructor(props) {
		super(props);
		this.state = {
			keyWord: '',
			isShowOff: false // 是否显示停用
		};
		closeBrowserUtils.call(this, props, { form: [ tableId ] });
		initTemplate.call(this, props);
	}

	// 搜索关键词变化
	searchOnChange = (value) => {
		this.setState({ keyWord: value }, () => {
			// 调用平台api过滤显示数据
			let { editTable } = this.props;
			editTable.setFiltrateTableData(tableId, filterFields, value);
		});
	};

	// 切换显示停用
	showOffChange = (value) => {
		this.setState({ isShowOff: value }, () => {
			getData.call(this, this.props, value);
		});
	};

	render() {
		let { editTable, button } = this.props;
		let { createEditTable } = editTable;
		let { createButtonApp } = button;
		let status = this.props.editTable.getStatus(tableId);
		let { keyWord, isShowOff } = this.state; //解构赋值

		return (
			<div className="nc-single-table" id="ampub-eambase-tool-list">
				{/* 头部 */}
				<div className="nc-singleTable-header-area">
					{/* 标题 */}
					<div className="header-title-search-area">
						<h2 className="title-search-detail">{getMultiLangByID(title) /* 国际化处理： 工具*/}</h2>
						{/* 简单搜索 */}
						<div className="title-search-detail">
							<NCFormControl
								id="tool-form-control"
								placeholder={getMultiLangByID('amcommon-000003') /* 国际化处理： 请输入名称或编码*/}
								value={keyWord}
								onChange={this.searchOnChange}
								type="search"
								className={'search-box disabled'}
								disabled={status == 'edit'}
							/>
						</div>
						{/* 显示停用  showOff*/}
						<div className="title-search-detail">
							<span className="showOff">
								<NCCheckbox
									id="tool-checkbox"
									checked={isShowOff}
									onChange={this.showOffChange}
									disabled={status == 'edit'}
								>
									{getMultiLangByID('amcommon-000004') /* 国际化处理： 显示停用*/}
								</NCCheckbox>
							</span>
						</div>
					</div>
					{/* 按钮区 */}
					<div className="header-button-area">
						{createButtonApp({
							area: 'list_head',
							buttonLimit: 4,
							onButtonClick: buttonClick.bind(this),
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>

				{/* 列表区 */}
				<div className="nc-singleTable-table-area">
					{createEditTable(tableId, {
						showCheck: true,
						showIndex: true,
						onAfterEvent: afterEvent.bind(this),
						selectedChange: rowSelected.bind(this)
					})}
				</div>
			</div>
		);
	}
}

Tool = createPage({
	billinfo: {
		billtype: 'grid', //单表为grid
		pagecode: pagecode,
		bodycode: tableId
	}
})(Tool);

let moduleIds = { ampub: [ 'common' ,'450103048A'] }; //引入多语json

initMultiLangByModule(moduleIds, () => {
	// 显示页面
	ReactDOM.render(<Tool />, document.querySelector('#app'));
});
