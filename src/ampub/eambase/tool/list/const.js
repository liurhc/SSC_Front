// 页面配置
export const pageConfig = {
	// 应用主键
	appid: '0001Z9100000000012L3',
	// 应用编码
	appcode: '450103048A',
	// 应用类型
	nodeType: 'group',
	// 应用名称
	title: '450103048A-000000' /* 国际化处理： 工具*/,
	// 表格区域id
	tableId: 'list_head',
	// 页面编码
	pagecode: '450103048A_list',
	// 主键字段
	pkField: 'pk_tool',
	// 编辑态按钮
	editBtns: [ 'Add', 'Delete', 'Save', 'Cancel' ],
	// 浏览态按钮
	browseBtns: [ 'Add', 'Edit', 'Delete', 'Attachment', 'Print', 'Output', 'Refresh' ], //加了一个附件
	url: {
		queryUrl: '/nccloud/ampub/tools/query.do',
		saveUrl: '/nccloud/ampub/tools/save.do',
		deleteUrl: '/nccloud/ampub/tools/delete.do',
		sealUrl: '/nccloud/ampub/tools/seal.do',
		unSealUrl: '/nccloud/ampub/tools/unseal.do',
		printUrl: '/nccloud/ampub/tools/printList.do'
	},
	// 模糊搜索过滤字段
	filterFields: [ 'tool_code', 'tool_name' ],
	exceptFields: [ 'pk_group', 'pk_org', 'enablestate' ],
	// 输出文件名称
	printFilename: '450103048A-000001' /* 国际化处理： 工具*/,
	// 打印模板节点标识
	printNodekey: null
};
