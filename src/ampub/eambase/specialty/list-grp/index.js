import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createPage } from 'nc-lightapp-front';
import ampub from 'ampub';
import SingleTableBase, { baseConfig } from '../list-base';

const { utils } = ampub;
const { multiLangUtils } = utils;
const { initMultiLangByModule } = multiLangUtils;

// 页面配置
const pageConfig = {
	...baseConfig,
	// 应用主键
	appid: '0001Z9100000000012L3',
	// 应用编码
	appcode: '450101024A',
	// 应用类型
	nodeType: 'group',
	// 应用名称
	title: '450101024A-000000' /* 国际化处理： 专业-集团*/,
	// 页面编码
	pagecode: '450101024A_list',
	// 输出文件名称
	printFilename: '450101024A-000000' /* 国际化处理： 专业-集团*/,
	// 打印模板节点标识
	printNodekey: null
};

const SingleTable = createPage({
	billinfo: {
		billtype: 'grid',
		pagecode: pageConfig.pagecode,
		headcode: pageConfig.tableId,
		bodycode: pageConfig.tableId
	}
})(SingleTableBase);

let moduleIds = { ampub: [ '450101024A', 'common' ] };
initMultiLangByModule(moduleIds, () => {
	// 显示页面
	ReactDOM.render(<SingleTable pageConfig={pageConfig} />, document.querySelector('#app'));
});
