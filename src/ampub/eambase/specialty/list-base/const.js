// 页面配置
export const pageConfig = {
	// 应用主键
	appid: '0001Z9100000000012L3',
	// 应用编码
	appcode: '450101024A',
	// 应用类型
	nodeType: 'group',
	// 应用名称
	title: '450101024A-000000' /* 国际化处理： 专业-集团*/,
	// 表格区域id
	tableId: 'list_head',
	// 页面编码
	pagecode: '450101024A_list',
	// 主键字段
	pkField: 'pk_specialty',
	// 编辑态按钮
	editBtns: [ 'Add', 'Delete', 'Save', 'Cancel' ],
	// 浏览态按钮
	browseBtns: [ 'Add', 'Edit', 'Delete', 'Print', 'Output', 'Refresh' ],
	url: {
		queryUrl: '/nccloud/ampub/specialty/query.do',
		saveUrl: '/nccloud/ampub/specialty/save.do',
		deleteUrl: '/nccloud/ampub/specialty/delete.do',
		sealUrl: '/nccloud/ampub/specialty/seal.do',
		unSealUrl: '/nccloud/ampub/specialty/unseal.do',
		printUrl: '/nccloud/ampub/specialty/printList.do'
	},
	// 模糊搜索过滤字段
	filterFields: [ 'special_code', 'special_name' ],
	exceptFields: [ 'pk_group', 'pk_org', 'enablestate' ],
	// 输出文件名称
	printFilename: '450101024A-000000' /* 国际化处理： 专业-集团*/,
	// 打印模板节点标识
	printNodekey: null
};
