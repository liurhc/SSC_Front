import React, { Component } from 'react';
import { base, createPageIcon } from 'nc-lightapp-front';
import ampub from 'ampub';
import { buttonClick, initTemplate, afterEvent, rowSelected, getData } from './events';
import { pageConfig } from './const';

const { components, utils } = ampub;
const { AMRefer } = components;
const { multiLangUtils, closeBrowserUtils } = utils;
const { getMultiLangByID } = multiLangUtils;
const { NCFormControl, NCCheckbox } = base;

/**
 * 页面入口文件
 */
class SingleTableBase extends Component {
	constructor(props) {
		super(props);
		this.state = {
			mainorg: {},
			keyWord: '',
			isShowOff: false
		};
		closeBrowserUtils(props, { editTable: [ pageConfig.tableId ] });
		initTemplate.call(this, props);
	}

	// 主组织切换
	mainorgChange = (value = {}) => {
		this.props.button.setButtonDisabled([ 'Add', 'Edit' ], !value.refpk);
		this.setState({ mainorg: value }, () => {
			// 根据组织查询数据
			getData.call(this, this.props, value.refpk);
		});
	};

	// 搜索关键词变化
	keyWordChange = (value) => {
		this.setState({ keyWord: value }, () => {
			// 调用平台api过滤显示数据
			let { editTable, pageConfig = {} } = this.props;
			let { tableId, filterFields } = pageConfig;
			editTable.setFiltrateTableData(tableId, filterFields, value);
		});
	};

	// 切换显示停用
	showOffChange = (value) => {
		this.setState({ isShowOff: value }, () => {
			getData.call(this, this.props, undefined, value);
		});
	};

	render() {
		const { editTable, button, pageConfig = {} } = this.props;
		const { createEditTable } = editTable;
		const { createButtonApp } = button;
		const { title, tableId, nodeType, orgRefCode } = pageConfig;
		let { mainorg, keyWord, isShowOff } = this.state;
		let status = editTable.getStatus(tableId);
		let mainorgobj = {
			defaultValue: mainorg,
			disabled: status == 'edit',
			onChange: (value) => {
				this.mainorgChange(value);
			},
			refcode: orgRefCode,
			queryCondition: () => {
				return { TreeRefActionExt: 'nccloud.web.ampub.common.refCodition.AppPermissionOrgRefFilter' };
			}
		};

		return (
			<div className="nc-single-table">
				{/* 头部 header */}
				<div className="nc-singleTable-header-area">
					{/* 标题 title */}
					<div className="header-title-search-area">
						{createPageIcon()}
						<h2 className="title-search-detail">{getMultiLangByID(title) /* 国际化处理： 专业-集团*/}</h2>
						{/* 主组织 main-org */}
						{nodeType == 'org' && <AMRefer className="ref title-search-detail" config={mainorgobj} />}
						{/* 简单搜索 search-box */}
						<div className="title-search-detail">
							<NCFormControl
								placeholder={getMultiLangByID('amcommon-000003') /* 国际化处理： 请输入编码或名称*/}
								value={keyWord}
								onChange={(value) => {
									this.keyWordChange(value);
								}}
								type="search"
								disabled={status == 'edit'}
							/>
						</div>
						{/* 显示停用 showOff */}
						<div className="title-search-detail">
							<span className="showOff">
								<NCCheckbox
									checked={isShowOff}
									onChange={(value) => {
										this.showOffChange(value);
									}}
									disabled={status == 'edit'}
								>
									{getMultiLangByID('amcommon-000004') /* 国际化处理： 显示停用*/}
								</NCCheckbox>
							</span>
						</div>
					</div>
					{/* 按钮区 btn-area */}
					<div className="header-button-area">
						{createButtonApp({
							area: 'list_head',
							buttonLimit: 3,
							onButtonClick: (props, id) => {
								buttonClick.call(this, props, id, tableId);
							},
							popContainer: document.querySelector('.header-button-area')
						})}
					</div>
				</div>
				{/* 列表区 table-area */}
				<div className="nc-singleTable-table-area">
					{createEditTable(tableId, {
						onAfterEvent: afterEvent.bind(this),
						showCheck: true,
						showIndex: true,
						selectedChange: rowSelected.bind(this)
					})}
				</div>
			</div>
		);
	}
}

export default SingleTableBase;

export const baseConfig = pageConfig;
