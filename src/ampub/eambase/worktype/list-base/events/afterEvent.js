import { addRow } from './buttonClick';

/**
 * 编辑后事件
 * @param {*} props 
 * @param {*} moduleId 
 * @param {*} key 
 * @param {*} value 
 * @param {*} changedRows 
 * @param {*} index 
 * @param {*} record 
 * @param {*} type 
 * @param {*} eventType 
 */
export default function(props, moduleId, key, value, changedRows, index, record, type, eventType) {
	let { pageConfig } = props;
	let { tableId } = pageConfig;

	if (eventType == 'blur' && key != 'enablestate') {
		// 平台的自动增行不会附默认值，这里调用自己的增行
		let num = props.editTable.getNumberOfRows(tableId);
		if (num == index + 1) {
			addRow.call(this, props, true);
		}
	}
}
