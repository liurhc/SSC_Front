import ampub from 'ampub';
import { delRow, singleDel } from './buttonClick';

const { utils, commonConst } = ampub;
const { msgUtils, multiLangUtils } = utils;
const { showMessage } = msgUtils;
const { getMultiLangByID } = multiLangUtils;
const { StatusUtils } = commonConst;
const { UISTATE } = StatusUtils;

/**
 * 表格操作按钮
 * @param {*} props 
 * @param {*} key 
 * @param {*} text 
 * @param {*} record 
 * @param {*} index 
 * @param {*} tableId 
 */
export default function(props, key, text, record, index, tableId) {
	switch (key) {
		case 'Delete':
			// 删除行
			delLine.call(this, props, record, index);
			break;
		default:
			break;
	}
}

/**
 * 表格行删除
 * @param {*} props 
 * @param {*} record 
 * @param {*} index 
 */
function delLine(props, record, index) {
	let { pageConfig = {} } = props;
	let { tableId, nodeType } = pageConfig;
	let recordVal = record.values;
	// 组织节点不允许删除集团数据
	let flag =
		nodeType == 'org' &&
		recordVal.pk_group &&
		recordVal.pk_org &&
		recordVal.pk_group.value == recordVal.pk_org.value;

	if (flag) {
		showMessage.call(this, props, {
			content: getMultiLangByID('450102508A-000003') /* 国际化处理： 业务单元应用不能删除集团数据*/,
			color: 'warning'
		});
		return;
	}
	let status = props.editTable.getStatus(tableId) || UISTATE.browse;
	if (status == UISTATE.browse) {
		singleDel.call(this, props, record.flterIndex);
	} else {
		delRow.call(this, props, index);
	}
}
