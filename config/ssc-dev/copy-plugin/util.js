'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function escape(context, from) {
    if (from && _path2.default.isAbsolute(from)) {
        return from;
    } else {
        // Ensure context is escaped before globbing
        // Handles special characters in paths
        var absoluteContext = _path2.default.resolve(context).replace(/[\*|\?|\!|\(|\)|\[|\]|\{|\}]/g, function (substring) {
            return '[' + substring + ']';
        });

        if (!from) {
            return absoluteContext;
        }

        // Cannot use path.join/resolve as it "fixes" the path separators
        if (absoluteContext.endsWith('/')) {
            return '' + absoluteContext + from;
        } else {
            return absoluteContext + '/' + from;
        }
    }
}


var promisify = {

}

promisify.stat = function stat(inputFileSystem, path) {
    return new Promise(function (resolve, reject) {
        inputFileSystem.stat(path, function (err, stats) {
            if (err) {
                reject(err);
            }
            resolve(stats);
        });
    });
};

promisify.readFile = function readFile(inputFileSystem, path) {
    return new Promise(function (resolve, reject) {
        inputFileSystem.readFile(path, function (err, stats) {
            if (err) {
                reject(err);
            }
            resolve(stats);
        });
    });
};

function isObject (val) {
    return Object.prototype.toString.call(val) === '[object Object]' ? true : false;
};

exports.escape = escape;
exports.isObject = isObject;
exports.promisify = promisify;