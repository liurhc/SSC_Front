# 开发环境脚手架优化说明

## 声明
本次优化对平台提供的脚手架没有影响。

## 目的
文件change后，降低webpack rebuild的时间

## 使用方式
1. pull SSC_Front项目
2. 以 `npm run ssc-dev` 命令启动脚手架

## 优化效果
使用我本地机器测试。config.json配置如下。  
```
{
	"buildEntryPath": [
        "./src/ssctp/*/*/*/index.js",
        "./src/sscrp/*/*/*/index.js",
        "./src/erm/*/*/*/index.js"
	],
	"proxy": "http://172.20.53.106:8888",
	"buildWithoutHTML": [ "uapbd/refer", "uap/refer", "sscrp/test" ],
	"devPort": 3006
}
```
以 npm run dev 方式启动，文件change后，rebuild完成的时间约 41s。  
以 npm run ssc-dev 启动，文件change后，rebuild完的时间约为 6s。  
性能提升约7倍。

## 优化的内容

- devtool配置  
  使用速度快的`cheap-module-eval-source-map`配置选项 (重要，至少可减少一半rebuild耗时。)

- watchOptions配置  
    使用`aggregateTimeout`配置选项，避免多次触发rebuild。如1s内进行了两次文件change，只会触发一次rebuild。  习惯使用ide自动保存功能的同学，可根据需要自行修改该配置的时间（默认1000ms）。
  
- copy插件及拷贝目录设置  
    - 问题分析：每次rebuild都会触发copyPlugin的一系列操作。而实际应用中，需要被copy的文件都是被独立引用的资源文件（如public、plafrom、uap等目录下的文件），这些文件被改动的频率非常低，不需要每次rebuild都进行copy操作。  
    - 解决：1、精确配置需要被copy的文件目录，减少无用拷贝。2、改变copy插件的处理逻辑，仅在启动的时候执行一次拷贝。  
    - 注意： 因为copy操作仅在开发环境脚手架启动时候执行一次，所以`如有独立资源文件改动，需要重新启动脚手架，才能生效`  

