'use strict';

var _path = require('path');

var _isGlob = require('is-glob');

var _pLimit = require('p-limit');

var _globby = require('globby');

var _minimatch = require('minimatch');

var util = require('./util.js');

function CopyWebpackPlugin() {
    var patterns = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    if (!Array.isArray(patterns)) {
        throw new Error('[copy-plugin] patterns must be an array');
    }

    var apply = function apply(compiler) {
        var isDone = false;
        var emit = function emit(compilation, cb) {
            if (isDone) {
                cb();
                return;
            }
            var fileDependencies = void 0;
            var contextDependencies = void 0;
            var written = {};

            var context = void 0;

            if (!options.context) {
                context = compiler.options.context;
            } else if (!_path.isAbsolute(options.context)) {
                context = _path.join(compiler.options.context, options.context);
            } else {
                context = options.context;
            }


                context = compiler.options.context;
            fileDependencies = [];
            contextDependencies = [];

            var globalRef = {
                compilation: compilation,
                written: written,
                fileDependencies: fileDependencies,
                contextDependencies: contextDependencies,
                context: context,
                inputFileSystem: compiler.inputFileSystem,
                output: compiler.options.output.path,
                ignore: options.ignore || [],
                copyUnmodified: options.copyUnmodified,
                concurrency: options.concurrency
            };

            if (globalRef.output === '/' && compiler.options.devServer && compiler.options.devServer.outputPath) {
                globalRef.output = compiler.options.devServer.outputPath;
            }

            var tasks = [];

            patterns.forEach(function (pattern) {
                tasks.push(Promise.resolve().then(function () {
                    return (0, preProcessPattern)(globalRef, pattern);
                })
                    // Every source (from) is assumed to exist here
                    .then(function (pattern) {
                        return (0, processPattern)(globalRef, pattern);
                    }));
            });

            Promise.all(tasks).then(() => {
                isDone = true;
                cb();
            })
        }

        var plugin = { name: 'CopyPlugin' };

        compiler.hooks.emit.tapAsync(plugin, emit);
    };

    return {
        apply: apply
    };
}

var isTemplateLike = /(\[ext\])|(\[name\])|(\[path\])|(\[folder\])|(\[emoji(:\d+)?\])|(\[(\w+:)?hash(:\w+)?(:\d+)?\])|(\[\d+\])/;

function preProcessPattern(globalRef, pattern) {
    var context = globalRef.context,
        inputFileSystem = globalRef.inputFileSystem,
        fileDependencies = globalRef.fileDependencies,
        contextDependencies = globalRef.contextDependencies,


    pattern = typeof pattern === 'string' ? {
        from: pattern
    } : Object.assign({}, pattern);
    pattern.to = pattern.to || '';
    pattern.context = pattern.context || context;
    if (!_path.isAbsolute(pattern.context)) {
        pattern.context = _path.join(context, pattern.context);
    }
    pattern.ignore = globalRef.ignore.concat(pattern.ignore || []);


    switch (true) {
        case !!pattern.toType:
            // if toType already exists
            break;
        case isTemplateLike.test(pattern.to):
            pattern.toType = 'template';
            break;
        case _path.extname(pattern.to) === '' || pattern.to.slice(-1) === '/':
            pattern.toType = 'dir';
            break;
        default:
            pattern.toType = 'file';
    }

    // If we know it's a glob, then bail early
    if ((0, util.isObject)(pattern.from) && pattern.from.glob) {
        pattern.fromType = 'glob';

        var fromArgs = Object.assign({}, pattern.from);
        delete fromArgs.glob;

        pattern.fromArgs = fromArgs;
        pattern.glob = (0, util.escape)(pattern.context, pattern.from.glob);
        pattern.absoluteFrom = _path.resolve(pattern.context, pattern.from.glob);
        return Promise.resolve(pattern);
    }


    if (_path.isAbsolute(pattern.from)) {
        pattern.absoluteFrom = pattern.from;
    } else {
        pattern.absoluteFrom = _path.resolve(pattern.context, pattern.from);
    }


    return (0, util.promisify.stat)(inputFileSystem, pattern.absoluteFrom).catch(function () {
        // If from doesn't appear to be a glob, then log a warning
        if ((0, _isGlob)(pattern.from) || pattern.from.indexOf('*') !== -1) {
            pattern.fromType = 'glob';
            pattern.glob = (0, util.escape)(pattern.context, pattern.from);
        } else {
            pattern.fromType = 'nonexistent';
        }
    }).then(function (stat) {
        if (!stat) {
            return pattern;
        }

        if (stat.isDirectory()) {
            pattern.fromType = 'dir';
            pattern.context = pattern.absoluteFrom;
            contextDependencies.push(pattern.absoluteFrom);
            pattern.glob = (0, util.escape)(pattern.absoluteFrom, '**/*');
            pattern.absoluteFrom = _path.join(pattern.absoluteFrom, '**/*');
            pattern.fromArgs = {
                dot: true
            };
        } else if (stat.isFile()) {
            pattern.fromType = 'file';
            pattern.context = _path.dirname(pattern.absoluteFrom);
            pattern.glob = (0, util.escape)(pattern.absoluteFrom);
            pattern.fromArgs = {
                dot: true
            };
            fileDependencies.push(pattern.absoluteFrom);
        } else if (!pattern.fromType) {
            console.log('Unrecognized file type for ' + pattern.from);
        }
        return pattern;
    });
}

function processPattern(globalRef, pattern) {
    var output = globalRef.output,
        concurrency = globalRef.concurrency,
        contextDependencies = globalRef.contextDependencies;

    var globArgs = Object.assign({
        cwd: pattern.context
    }, pattern.fromArgs || {});

    if (pattern.fromType === 'nonexistent') {
        return Promise.resolve();
    }

    var limit = (0, _pLimit)(concurrency || 100);

    return (0, _globby)(pattern.glob, globArgs).then(function (paths) {
        return Promise.all(paths.map(function (from) {
            return limit(function () {
                var file = {
                    force: pattern.force,
                    absoluteFrom: _path.resolve(pattern.context, from)
                };
                file.relativeFrom = _path.relative(pattern.context, file.absoluteFrom);

                if (pattern.flatten) {
                    file.relativeFrom = _path.basename(file.relativeFrom);
                }

                // Check the ignore list
                var il = pattern.ignore.length;
                while (il--) {
                    var ignoreGlob = pattern.ignore[il];

                    var globParams = {
                        dot: true,
                        matchBase: true
                    };

                    var glob = void 0;
                    if (typeof ignoreGlob === 'string') {
                        glob = ignoreGlob;
                    } else if ((0, util.isObject)(ignoreGlob)) {
                        glob = ignoreGlob.glob || '';
                        var ignoreGlobParams = Object.assign({}, ignoreGlob);
                        delete ignoreGlobParams.glob;

                        // Overwrite minimatch defaults
                        globParams = Object.assign(globParams, ignoreGlobParams);
                    } else {
                        glob = '';
                    }

                    if ((0, _minimatch)(file.relativeFrom, glob, globParams)) {
                        return Promise.resolve();
                    } else {
                        console.log(glob + ' doesn\'t match ' + file.relativeFrom);
                    }
                }

                // Change the to path to be relative for webpack
                if (pattern.toType === 'dir') {
                    file.webpackTo = _path.join(pattern.to, file.relativeFrom);
                } else if (pattern.toType === 'file') {
                    file.webpackTo = pattern.to || file.relativeFrom;
                } else if (pattern.toType === 'template') {
                    file.webpackTo = pattern.to;
                    file.webpackToRegExp = pattern.test;
                }

                if (_path.isAbsolute(file.webpackTo)) {
                    if (output === '/') {
                        throw '[copy-plugin] Using older versions of webpack-dev-server, devServer.outputPath must be defined to write to absolute paths';
                    }

                    file.webpackTo = _path.relative(output, file.webpackTo);
                }

                // ensure forward slashes
                file.webpackTo = file.webpackTo.replace(/\\/g, '/');

                return writeFile(globalRef, pattern, file);
            });
        }));
    });
}

function writeFile(globalRef, pattern, file) {
    var compilation = globalRef.compilation,
        inputFileSystem = globalRef.inputFileSystem;


    return (0, util.promisify.stat)(inputFileSystem, file.absoluteFrom).then(function (stat) {
        // We don't write empty directories
        if (stat.isDirectory()) {
            return;
        }
        return (0, util.promisify.readFile)(inputFileSystem, file.absoluteFrom).then(function (content) {
            return content;
        }).then(function (content) {

            compilation.assets[file.webpackTo] = {
                size: function size() {
                    return stat.size;
                },
                source: function source() {
                    return content;
                }
            };
        });
    });
}

CopyWebpackPlugin['default'] = CopyWebpackPlugin;
module.exports = CopyWebpackPlugin;